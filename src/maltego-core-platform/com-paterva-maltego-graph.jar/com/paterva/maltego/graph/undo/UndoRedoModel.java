/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Bundle;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.IndexedCommand;
import com.paterva.maltego.graph.undo.UndoRedoCommandMerger;
import com.paterva.maltego.graph.undo.UndoRedoExecutor;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;

public class UndoRedoModel {
    private static final Logger LOG = Logger.getLogger(UndoRedoModel.class.getName());
    private static final int UNDO_DEPTH = Integer.decode(Bundle.UndoDepth());
    private Deque<IndexedCommand> _undoStack = new ArrayDeque<IndexedCommand>();
    private Deque<IndexedCommand> _redoStack = new ArrayDeque<IndexedCommand>();
    private final ChangeSupport _undoChangeSupport;
    private int _highestIndex;

    public UndoRedoModel() {
        this._undoChangeSupport = new ChangeSupport((Object)this);
        this._highestIndex = -1;
    }

    public void store(Command command) {
        this._redoStack.clear();
        this.addToUndo(new IndexedCommand(command));
        this._undoChangeSupport.fireChange();
    }

    public void store(Command command, Integer n) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("--Before--");
            this.printStacks();
        }
        if (this.tryMerge(command, n)) {
            this._redoStack.clear();
            if (n > this._highestIndex) {
                this._highestIndex = n;
            }
        } else {
            IndexedCommand indexedCommand = new IndexedCommand(command, n);
            if (n == null) {
                this.store(command);
            } else if (n > this._highestIndex) {
                this._redoStack.clear();
                this.addToUndo(indexedCommand);
                this._highestIndex = n;
            } else {
                Deque<IndexedCommand> deque = this.tryInsert(this.reverse(this._undoStack), indexedCommand);
                if (deque != null) {
                    this._undoStack = this.reverse(deque);
                    if (this._undoStack.peekFirst() == indexedCommand) {
                        this._redoStack.clear();
                    }
                    while (this.getSignificantUndoDepth() > UNDO_DEPTH) {
                        this._undoStack.removeLast();
                    }
                } else {
                    deque = this.tryInsert(this._redoStack, indexedCommand);
                    if (deque != null) {
                        this._redoStack = deque;
                    }
                }
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("--After--");
            this.printStacks();
        }
        this._undoChangeSupport.fireChange();
    }

    private boolean tryMerge(Command command, Integer n) {
        for (UndoRedoCommandMerger undoRedoCommandMerger : Lookup.getDefault().lookupAll(UndoRedoCommandMerger.class)) {
            if (!undoRedoCommandMerger.merge(this._undoStack, command, n)) continue;
            return true;
        }
        return false;
    }

    private Deque<IndexedCommand> tryInsert(Deque<IndexedCommand> deque, IndexedCommand indexedCommand) {
        boolean bl = false;
        boolean bl2 = false;
        int n = indexedCommand.getIndex() - 1;
        ArrayDeque<IndexedCommand> arrayDeque = new ArrayDeque<IndexedCommand>();
        for (IndexedCommand indexedCommand2 : deque) {
            Integer n2 = indexedCommand2.getIndex();
            if (!bl && bl2 && n2 != null && !n2.equals(indexedCommand.getIndex())) {
                arrayDeque.add(indexedCommand);
                bl = true;
            }
            arrayDeque.add(indexedCommand2);
            if (bl || bl2 || n2 == null || n != n2) continue;
            bl2 = true;
        }
        if (!bl && bl2) {
            arrayDeque.add(indexedCommand);
            bl = true;
        }
        return bl ? arrayDeque : null;
    }

    private Deque<IndexedCommand> reverse(Deque<IndexedCommand> deque) {
        ArrayList<IndexedCommand> arrayList = new ArrayList<IndexedCommand>(deque);
        Collections.reverse(arrayList);
        return new ArrayDeque<IndexedCommand>(arrayList);
    }

    private void addToUndo(IndexedCommand indexedCommand) {
        this._undoStack.push(indexedCommand);
        while (this.getSignificantUndoDepth() > UNDO_DEPTH) {
            this._undoStack.removeLast();
        }
    }

    public boolean canUndo() {
        return this.getSignificantUndoDepth() > 0;
    }

    public boolean canRedo() {
        return this.getSignificantRedoDepth() > 0;
    }

    public void undo() throws CannotUndoException {
        boolean bl = false;
        while (!bl) {
            IndexedCommand indexedCommand = this.popUndoCommand();
            this._redoStack.push(indexedCommand);
            boolean bl2 = indexedCommand.getCommand().isSignificant();
            UndoRedoExecutor.getDefault().undo(indexedCommand.getCommand());
            bl = this._undoStack.isEmpty() || bl2;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("--After Undo--");
            this.printStacks();
        }
        this._undoChangeSupport.fireChange();
    }

    private IndexedCommand popUndoCommand() {
        return this._undoStack.pop();
    }

    public void redo() throws CannotRedoException {
        boolean bl = false;
        while (!bl) {
            IndexedCommand indexedCommand = this._redoStack.pop();
            this.addToUndo(indexedCommand);
            UndoRedoExecutor.getDefault().redo(indexedCommand.getCommand());
            bl = this._redoStack.isEmpty() || this._redoStack.peekFirst().getCommand().isSignificant();
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("--After Redo--");
            this.printStacks();
        }
        this._undoChangeSupport.fireChange();
    }

    protected int getSignificantUndoDepth() {
        return this.getSignificantDepth(this._undoStack);
    }

    protected int getSignificantRedoDepth() {
        return this.getSignificantDepth(this._redoStack);
    }

    private int getSignificantDepth(Deque<IndexedCommand> deque) {
        int n = 0;
        for (IndexedCommand indexedCommand : deque) {
            if (!indexedCommand.getCommand().isSignificant()) continue;
            ++n;
        }
        return n;
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._undoChangeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._undoChangeSupport.removeChangeListener(changeListener);
    }

    public Command[] getUndoStack() {
        return this.getCommandArray(this._undoStack);
    }

    public Command[] getRedoStack() {
        return this.getCommandArray(this._redoStack);
    }

    private Command[] getCommandArray(Deque<IndexedCommand> deque) {
        Command[] arrcommand = new Command[deque.size()];
        int n = 0;
        for (IndexedCommand indexedCommand : deque) {
            arrcommand[n++] = indexedCommand.getCommand();
        }
        return arrcommand;
    }

    private void printStacks() {
        LOG.fine("-=<UNDO>=-");
        for (IndexedCommand indexedCommand2 : this._undoStack) {
            LOG.log(Level.FINE, "{0} -> {1}", new Object[]{indexedCommand2.getIndex(), indexedCommand2.getCommand()});
        }
        LOG.fine("-=<REDO>=-");
        for (IndexedCommand indexedCommand2 : this._redoStack) {
            LOG.log(Level.FINE, "{0} -> {1}", new Object[]{indexedCommand2.getIndex(), indexedCommand2.getCommand()});
        }
    }
}

