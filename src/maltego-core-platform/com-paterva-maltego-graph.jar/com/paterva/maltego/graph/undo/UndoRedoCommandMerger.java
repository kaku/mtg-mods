/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.IndexedCommand;
import java.util.Deque;

public interface UndoRedoCommandMerger {
    public boolean merge(Deque<IndexedCommand> var1, Command var2, Integer var3);
}

