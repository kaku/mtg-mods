/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import java.util.EventListener;

public interface GraphLogListener
extends EventListener {
    public void logMessage(GraphID var1, String var2);
}

