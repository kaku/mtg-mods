/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphFreezable;
import java.beans.PropertyChangeListener;
import org.openide.util.Lookup;

public abstract class GraphFreezableRegistry {
    private static GraphFreezableRegistry _default;

    public static synchronized GraphFreezableRegistry getDefault() {
        if (_default == null && (GraphFreezableRegistry._default = (GraphFreezableRegistry)Lookup.getDefault().lookup(GraphFreezableRegistry.class)) == null) {
            _default = new TrivialFreezableRegistry();
        }
        return _default;
    }

    public abstract GraphFreezable forGraph(GraphID var1);

    private static class TrivialFreezableRegistry
    extends GraphFreezableRegistry {
        private final GraphFreezable _dummy;

        private TrivialFreezableRegistry() {
            this._dummy = new GraphFreezable(){

                @Override
                public void setFrozen(boolean bl) {
                }

                @Override
                public boolean isFrozen() {
                    return false;
                }

                @Override
                public boolean hasUpdates() {
                    return false;
                }

                @Override
                public void updateNow() {
                }

                @Override
                public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
                }

                @Override
                public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
                }
            };
        }

        @Override
        public GraphFreezable forGraph(GraphID graphID) {
            return this._dummy;
        }

    }

}

