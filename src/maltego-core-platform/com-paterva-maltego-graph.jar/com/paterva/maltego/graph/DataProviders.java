/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.K
 */
package com.paterva.maltego.graph;

import yguard.A.A.K;

public class DataProviders {
    private DataProviders() {
    }

    public static class Singleton<T>
    implements K {
        private T _t;

        public Singleton(T t) {
            this._t = t;
        }

        public T get() {
            return this._t;
        }

        public Object get(Object object) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public int getInt(Object object) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public double getDouble(Object object) {
            throw new UnsupportedOperationException("Not supported.");
        }

        public boolean getBool(Object object) {
            throw new UnsupportedOperationException("Not supported.");
        }
    }

}

