/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.metadata;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import org.openide.util.Lookup;

public abstract class GraphMetadataManager {
    private static GraphMetadataManager _instance;

    public static synchronized GraphMetadataManager getDefault() {
        if (_instance == null) {
            _instance = (GraphMetadataManager)Lookup.getDefault().lookup(GraphMetadataManager.class);
        }
        return _instance;
    }

    public abstract GraphMetadata get(GraphID var1);

    public abstract void set(GraphID var1, GraphMetadata var2);
}

