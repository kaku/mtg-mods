/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.metadata;

import java.util.Date;

public interface GraphMetadata {
    public String getAuthor();

    public Date getCreated();

    public Date getModified();

    public void setAuthor(String var1);

    public void setCreated(Date var1);

    public void setModified(Date var1);
}

