/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph;

import java.beans.PropertyChangeListener;

public interface GraphFreezable {
    public static final String PROP_FREEZABLE_CHANGED = "freezableChanged";
    public static final String PROP_UPDATE_PENDING = "updatePending";

    public void setFrozen(boolean var1);

    public boolean isFrozen();

    public boolean hasUpdates();

    public void updateNow();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

