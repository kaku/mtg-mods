/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaltegoGraphSnippet {
    private final Collection<MaltegoEntity> _entities = new ArrayList<MaltegoEntity>();
    private final Collection<MaltegoLink> _links = new ArrayList<MaltegoLink>();
    private final Map<LinkID, LinkEntityIDs> _connections = new HashMap<LinkID, LinkEntityIDs>();
    private final Map<String, Map<EntityID, Point>> _centers = new HashMap<String, Map<EntityID, Point>>();
    private final Map<String, Map<LinkID, List<Point>>> _paths = new HashMap<String, Map<LinkID, List<Point>>>();

    public Collection<MaltegoEntity> getEntities() {
        return this._entities;
    }

    public Collection<MaltegoLink> getLinks() {
        return this._links;
    }

    public Map<LinkID, LinkEntityIDs> getConnections() {
        return this._connections;
    }

    public Map<String, Map<EntityID, Point>> getCenters() {
        return this._centers;
    }

    public Map<String, Map<LinkID, List<Point>>> getPaths() {
        return this._paths;
    }
}

