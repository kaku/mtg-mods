/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.Version
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.util.Version;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

public class GraphUserData
extends HashMap<String, Object> {
    private static final Map<GraphID, GraphUserData> GRAPH_USER_DATAS = new HashMap<GraphID, GraphUserData>();
    private static final List<Long> _closedGraphs = new ArrayList<Long>();

    private GraphUserData() {
    }

    public static synchronized GraphUserData forGraph(GraphID graphID) {
        return GraphUserData.forGraph(graphID, true);
    }

    public static synchronized GraphUserData forGraph(GraphID graphID, boolean bl) {
        GraphUserData graphUserData = GRAPH_USER_DATAS.get((Object)graphID);
        if (graphUserData == null && bl) {
            GraphUserData.checkGraphClosed(graphID);
            graphUserData = new GraphUserData();
            GRAPH_USER_DATAS.put(graphID, graphUserData);
        }
        return graphUserData;
    }

    public static synchronized Map<GraphID, GraphUserData> getAll() {
        return Collections.unmodifiableMap(GRAPH_USER_DATAS);
    }

    private static synchronized void remove(GraphID graphID) {
        GraphUserData graphUserData = GRAPH_USER_DATAS.remove((Object)graphID);
        if (graphUserData != null) {
            graphUserData.clear();
        }
        _closedGraphs.add(graphID.getValue());
    }

    private static void checkGraphClosed(GraphID graphID) throws IllegalStateException {
        if (Version.current().isDevBuild()) {
            try {
                for (Long l : _closedGraphs) {
                    if (graphID.getValue() != l.longValue()) continue;
                }
            }
            catch (IllegalStateException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
    }

    static {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    final GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            GraphUserData.remove(graphID);
                        }
                    });
                }
            }

        });
    }

}

