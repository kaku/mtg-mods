/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;

public class GraphLifeCycleManager {
    private static final Logger LOG = Logger.getLogger(GraphLifeCycleManager.class.getName());
    public static final String PROP_GRAPH_OPENING = "graphOpening";
    public static final String PROP_GRAPH_LOADING = "graphLoading";
    public static final String PROP_GRAPH_LOADED = "graphLoaded";
    public static final String PROP_GRAPH_OPENED = "graphOpened";
    public static final String PROP_GRAPH_SHOWING = "graphShowing";
    public static final String PROP_GRAPH_CLOSING = "graphClosing";
    public static final String PROP_GRAPH_CLOSED = "graphClosed";
    private static GraphLifeCycleManager _default;
    private final PropertyChangeSupport _changeSupport;

    public GraphLifeCycleManager() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public static synchronized GraphLifeCycleManager getDefault() {
        if (_default == null && (GraphLifeCycleManager._default = (GraphLifeCycleManager)Lookup.getDefault().lookup(GraphLifeCycleManager.class)) == null) {
            _default = new GraphLifeCycleManager();
        }
        return _default;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public void fireGraphOpening(GraphID graphID) {
        this.fire("graphOpening", graphID);
    }

    public void fireGraphLoading(GraphID graphID) {
        this.fire("graphLoading", graphID);
    }

    public void fireGraphLoaded(GraphID graphID) {
        this.fire("graphLoaded", graphID);
    }

    public void fireGraphOpened(GraphID graphID) {
        this.fire("graphOpened", graphID);
    }

    public void fireGraphShowing(GraphID graphID) {
        this.fire("graphShowing", graphID);
    }

    public void fireGraphClosing(GraphID graphID) {
        this.fire("graphClosing", graphID);
    }

    public void fireGraphClosed(GraphID graphID) {
        this.fire("graphClosed", graphID);
    }

    public void fire(final String string, final GraphID graphID) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                LOG.log(Level.FINE, "Firing {0} for {1}", new Object[]{string, graphID});
                GraphLifeCycleManager.this._changeSupport.firePropertyChange(string, null, (Object)graphID);
            }
        };
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(runnable);
        } else {
            runnable.run();
        }
    }

}

