/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph;

import com.paterva.maltego.core.GraphID;
import org.openide.util.Lookup;
import yguard.A.I.SA;

public abstract class GraphViewManager {
    private static GraphViewManager _registry;

    public static synchronized GraphViewManager getDefault() {
        if (_registry == null && (GraphViewManager._registry = (GraphViewManager)Lookup.getDefault().lookup(GraphViewManager.class)) == null) {
            throw new IllegalStateException("Graph View Manager not found.");
        }
        return _registry;
    }

    public abstract SA createViewGraph(GraphID var1);

    public abstract SA getViewGraph(GraphID var1);

    public abstract GraphID getGraphID(SA var1);
}

