/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.PropertyDescriptor;

public interface TypedPropertyBag
extends PropertyBag {
    public static final String PROP_VALUE = "value";
    public static final String PROP_DISPLAY_VALUE = "displayValue";
    public static final String PROP_TYPE = "type";

    public String getTypeName();

    public void setTypeName(String var1);

    public String getValuePropertyName();

    public PropertyDescriptor getValueProperty();

    public void setValueProperty(PropertyDescriptor var1);

    public String getDisplayValuePropertyName();

    public PropertyDescriptor getDisplayValueProperty();

    public void setDisplayValueProperty(PropertyDescriptor var1);

    public Object getValue();

    public void setValue(Object var1);

    public Object getDisplayValue();

    public void setDisplayValue(Object var1);
}

