/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.util.StringUtilities;

public class EntityID
extends Guid {
    protected EntityID(long l) {
        super(l);
    }

    protected EntityID() {
    }

    public static EntityID create() {
        return new EntityID();
    }

    public static EntityID create(long l) {
        return new EntityID(l);
    }

    public static EntityID parse(String string) {
        long l = StringUtilities.parseUnsignedLong((String)string.toLowerCase(), (int)36);
        return new EntityID(l);
    }
}

