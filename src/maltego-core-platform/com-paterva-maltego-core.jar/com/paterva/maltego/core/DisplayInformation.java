/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.core;

import org.openide.util.Utilities;

public class DisplayInformation {
    private String _name;
    private String _value;

    public DisplayInformation() {
        this("", "");
    }

    public DisplayInformation(String string, String string2) {
        this._name = string;
        this._value = string2;
    }

    public boolean isCopy(DisplayInformation displayInformation) {
        if (displayInformation == null) {
            return false;
        }
        if (!Utilities.compareObjects((Object)this._name, (Object)displayInformation._name) || !Utilities.compareObjects((Object)this._value, (Object)displayInformation._value)) {
            return false;
        }
        return true;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getValue() {
        return this._value;
    }

    public void setValue(String string) {
        this._value = string;
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    public String toString() {
        return this.getName();
    }

    public boolean equals(Object object) {
        if (object instanceof DisplayInformation) {
            return this.equals((DisplayInformation)object);
        }
        return false;
    }

    public boolean equals(DisplayInformation displayInformation) {
        return displayInformation.getName().equals(this.getName());
    }
}

