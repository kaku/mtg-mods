/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.util.StringUtilities;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class DisplayInformationCollection
implements Collection<DisplayInformation> {
    private final LinkedList<DisplayInformation> _list = new LinkedList();

    public boolean isCopy(DisplayInformationCollection displayInformationCollection) {
        if (displayInformationCollection == null) {
            return this.isEmpty();
        }
        if (this.isEmpty() && displayInformationCollection.isEmpty()) {
            return true;
        }
        if (this.size() != displayInformationCollection.size()) {
            return false;
        }
        Iterator<DisplayInformation> iterator = this._list.iterator();
        Iterator<DisplayInformation> iterator2 = displayInformationCollection._list.iterator();
        while (iterator.hasNext()) {
            DisplayInformation displayInformation = iterator.next();
            DisplayInformation displayInformation2 = iterator2.next();
            if (!(displayInformation != null ? !displayInformation.isCopy(displayInformation2) : displayInformation != displayInformation2)) continue;
            return false;
        }
        return true;
    }

    public void add(String string, String string2) {
        this.add(new DisplayInformation(string, string2));
    }

    public DisplayInformation get(String string) {
        for (DisplayInformation displayInformation : this) {
            if (!StringUtilities.areEqual((String)displayInformation.getName(), (String)string)) continue;
            return displayInformation;
        }
        return null;
    }

    public boolean contains(String string) {
        return this.get(string) != null;
    }

    @Override
    public int size() {
        return this._list.size();
    }

    @Override
    public boolean isEmpty() {
        return this._list.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        return this._list.contains(object);
    }

    @Override
    public Iterator<DisplayInformation> iterator() {
        return this._list.iterator();
    }

    @Override
    public Object[] toArray() {
        return this._list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arrT) {
        return this._list.toArray(arrT);
    }

    @Override
    public boolean add(DisplayInformation displayInformation) {
        return this._list.add(displayInformation);
    }

    @Override
    public boolean remove(Object object) {
        return this._list.remove(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return this._list.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends DisplayInformation> collection) {
        return this._list.addAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return this._list.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return this._list.retainAll(collection);
    }

    @Override
    public void clear() {
        this._list.clear();
    }

    public String toString() {
        return this._list.toString();
    }
}

