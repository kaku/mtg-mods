/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.util.StringUtilities;

public class LinkID
extends Guid {
    protected LinkID(long l) {
        super(l);
    }

    protected LinkID() {
    }

    public static LinkID create() {
        return new LinkID();
    }

    public static LinkID create(long l) {
        return new LinkID(l);
    }

    public static LinkID parse(String string) {
        long l = StringUtilities.parseUnsignedLong((String)string.toLowerCase(), (int)36);
        return new LinkID(l);
    }
}

