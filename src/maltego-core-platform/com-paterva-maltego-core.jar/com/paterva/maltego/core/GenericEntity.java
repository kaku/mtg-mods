/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericPart;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.util.StringUtilities;
import org.openide.util.Utilities;

public class GenericEntity
extends GenericPart<EntityID>
implements MaltegoEntity {
    private String _imagePropertyName;
    private Integer _weight;
    private Object _imageCached;

    public GenericEntity(EntityID entityID) {
        super(entityID);
    }

    public GenericEntity(EntityID entityID, String string, PropertyDescriptorCollection propertyDescriptorCollection) {
        super(entityID, string, propertyDescriptorCollection);
        this.initialize();
    }

    public GenericEntity(EntityID entityID, String string) {
        super(entityID, string);
        this.initialize();
    }

    public GenericEntity(MaltegoEntity maltegoEntity) {
        super(maltegoEntity);
        this._imagePropertyName = maltegoEntity.getImagePropertyName();
        this._weight = maltegoEntity.getWeightValue();
        this._imageCached = maltegoEntity.getImageCachedObject();
    }

    private void initialize() {
        this._weight = 0;
        this._imagePropertyName = "";
    }

    protected Object clone() throws CloneNotSupportedException {
        return this.createClone();
    }

    @Override
    public GenericEntity createCopy() {
        GenericEntity genericEntity = new GenericEntity(this);
        genericEntity.setId(EntityID.create());
        return genericEntity;
    }

    @Override
    public GenericEntity createClone() {
        return new GenericEntity(this);
    }

    @Override
    public boolean isCopy(MaltegoPart<EntityID> maltegoPart) {
        if (maltegoPart instanceof GenericEntity) {
            GenericEntity genericEntity = (GenericEntity)maltegoPart;
            if (!Utilities.compareObjects((Object)this._imagePropertyName, (Object)genericEntity._imagePropertyName) || !Utilities.compareObjects((Object)this._weight, (Object)genericEntity._weight)) {
                return false;
            }
        } else {
            return false;
        }
        return GenericPart.super.isCopy(maltegoPart);
    }

    @Override
    public Integer getWeightValue() {
        return this._weight;
    }

    @Override
    public int getWeight() {
        return this._weight != null ? this._weight : 0;
    }

    @Override
    public void setWeight(Integer n) {
        this._weight = n;
    }

    @Override
    public String getImagePropertyName() {
        return this._imagePropertyName;
    }

    @Override
    public PropertyDescriptor getImageProperty() {
        return this.getSpecialProperty(this._imagePropertyName);
    }

    @Override
    public void setImageProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl;
        if (propertyDescriptor == null) {
            bl = !StringUtilities.isNullOrEmpty((String)this._imagePropertyName);
            this._imagePropertyName = "";
        } else {
            bl = !propertyDescriptor.getName().equals(this._imagePropertyName);
            this._imagePropertyName = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("image");
        }
    }

    @Override
    public Object getImage() {
        return this.getSpecialPropertyValue(this._imagePropertyName);
    }

    @Override
    public void setImage(Object object) {
        if (StringUtilities.isNullOrEmpty((String)this._imagePropertyName)) {
            this._imagePropertyName = "image";
        }
        this.setSpecialPropertyValue(this._imagePropertyName, object);
    }

    @Override
    public Object getImageCachedObject() {
        return this._imageCached;
    }

    @Override
    public void setImageCachedObject(Object object) {
        this._imageCached = object;
    }

    @Override
    protected void checkSpecialPropertyUpdate(PropertyDescriptor propertyDescriptor) {
        GenericPart.super.checkSpecialPropertyUpdate(propertyDescriptor);
        if (propertyDescriptor.getName().equals(this._imagePropertyName)) {
            this.firePropertyChange("image");
        }
    }
}

