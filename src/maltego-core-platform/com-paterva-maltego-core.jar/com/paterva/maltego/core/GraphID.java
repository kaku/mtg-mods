/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.Guid;

public class GraphID
extends Guid {
    protected GraphID(long l) {
        super(l);
    }

    protected GraphID() {
    }

    public static GraphID create() {
        return new GraphID();
    }
}

