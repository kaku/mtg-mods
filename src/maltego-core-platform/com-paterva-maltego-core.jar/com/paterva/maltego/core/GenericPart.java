/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptorSet
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorSet;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.StringUtilities;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.openide.util.Utilities;
import sun.reflect.Reflection;

public abstract class GenericPart<ID extends Guid>
implements MaltegoPart<ID> {
    private static final boolean CHECK_SETTERS = false;
    private GraphID _graphID;
    private String _typeName;
    private String _valuePropertyName;
    private String _displayValuePropertyName;
    private PropertyDescriptorSet _properties;
    private DataSource _data;
    private PropertyChangeSupport _changeSupport;
    private PropertyChangeSupport _notesChangeSupport;
    private PropertyChangeSupport _bookmarkChangeSupport;
    private ID _id;
    private DisplayInformationCollection _displayInformation;
    private String _notes;
    private Boolean _showNotes;
    private Integer _bookmark;
    private int _propertiesUpdating = 0;
    private PropertyDescriptorSet _updatedProperties;
    private String _cachedValueStr;
    private String _cachedDisplayStr;
    private Boolean _cachedHasAttachments;
    private boolean _labelReadonly = true;

    public GenericPart(ID ID) {
        this._id = ID;
        this._data = new DataSources.Map();
    }

    public GenericPart(ID ID, String string) {
        this(ID);
        this._typeName = string;
        this._showNotes = Boolean.FALSE;
        this._bookmark = -1;
        this._notes = "";
        this._valuePropertyName = "";
        this._displayValuePropertyName = "";
    }

    public GenericPart(ID ID, String string, PropertyDescriptorCollection propertyDescriptorCollection) {
        this(ID, string);
        this.getProperties().addAll(propertyDescriptorCollection);
    }

    protected GenericPart(MaltegoPart<ID> maltegoPart) {
        this._typeName = maltegoPart.getTypeName();
        this._valuePropertyName = maltegoPart.getValuePropertyName();
        this._displayValuePropertyName = maltegoPart.getDisplayValuePropertyName();
        this._id = maltegoPart.getID();
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        this.getProperties().addAll(propertyDescriptorCollection);
        DataSources.Map map = new DataSources.Map();
        DataSource dataSource = maltegoPart.getData();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            Object object = dataSource.getValue(propertyDescriptor);
            if (object == null) continue;
            map.put((Object)propertyDescriptor, object);
        }
        this._data = map;
        if (maltegoPart.getDisplayInformation() != null) {
            this.getOrCreateDisplayInformation().addAll(maltegoPart.getDisplayInformation());
        }
        this._notes = maltegoPart.getNotes();
        this._showNotes = maltegoPart.isShowNotesValue();
        this._bookmark = maltegoPart.getBookmarkValue();
        if (maltegoPart instanceof GenericPart) {
            GenericPart genericPart = (GenericPart)maltegoPart;
            this._cachedValueStr = genericPart._cachedValueStr;
            this._cachedDisplayStr = genericPart._cachedDisplayStr;
            this._cachedHasAttachments = genericPart._cachedHasAttachments;
            this._labelReadonly = genericPart._labelReadonly;
        }
    }

    @Override
    public void setGraphID(GraphID graphID) {
        this._graphID = graphID;
    }

    @Override
    public DataSource getData() {
        return this._data;
    }

    @Override
    public void setData(DataSource dataSource) {
        this._data = dataSource;
    }

    protected void setId(ID ID) {
        this._id = ID;
    }

    @Override
    public boolean isCopy(MaltegoPart<ID> maltegoPart) {
        if (maltegoPart instanceof GenericPart) {
            GenericPart genericPart = (GenericPart)maltegoPart;
            if (!(Utilities.compareObjects((Object)this._typeName, (Object)genericPart._typeName) && Utilities.compareObjects((Object)this._valuePropertyName, (Object)genericPart._valuePropertyName) && Utilities.compareObjects((Object)this._displayValuePropertyName, (Object)genericPart._displayValuePropertyName) && this.isCopy(this._notes, genericPart._notes) && Utilities.compareObjects((Object)this._showNotes, (Object)genericPart._showNotes) && Utilities.compareObjects((Object)this._bookmark, (Object)genericPart._bookmark))) {
                return false;
            }
            if (!this.isCopy(this._displayInformation, genericPart._displayInformation)) {
                return false;
            }
            if (!(this._properties == genericPart._properties || this._properties != null && this._properties.isCopy(genericPart._properties))) {
                return false;
            }
            if (this._properties != null) {
                for (PropertyDescriptor propertyDescriptor : this._properties) {
                    if (Utilities.compareObjects((Object)this.getData().getValue(propertyDescriptor), (Object)genericPart.getData().getValue(propertyDescriptor))) continue;
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public boolean isCopy(String string, String string2) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return StringUtilities.isNullOrEmpty((String)string2);
        }
        if (StringUtilities.isNullOrEmpty((String)string2)) {
            return StringUtilities.isNullOrEmpty((String)string);
        }
        return string.equals(string2);
    }

    public boolean isCopy(DisplayInformationCollection displayInformationCollection, DisplayInformationCollection displayInformationCollection2) {
        return displayInformationCollection == displayInformationCollection2 || displayInformationCollection != null && displayInformationCollection.isCopy(displayInformationCollection2) || displayInformationCollection2 != null && displayInformationCollection2.isCopy(displayInformationCollection);
    }

    private boolean isUpdating() {
        return this._propertiesUpdating > 0;
    }

    protected Object getSpecialPropertyValue(String string) {
        PropertyDescriptor propertyDescriptor = this.getSpecialProperty(string);
        if (propertyDescriptor == null) {
            return null;
        }
        return this.getValue(propertyDescriptor);
    }

    protected void setSpecialPropertyValue(String string, Object object) {
        PropertyDescriptor propertyDescriptor = this.getSpecialProperty(string);
        if (propertyDescriptor == null) {
            if (object != null) {
                PropertyDescriptor propertyDescriptor2 = new PropertyDescriptor(object.getClass(), string);
                this.addProperty(propertyDescriptor2);
                this.setValue(propertyDescriptor2, object);
            }
        } else {
            this.setValue(propertyDescriptor, object);
        }
    }

    protected PropertyDescriptor getSpecialProperty(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        return this.getProperties().get(string);
    }

    @Override
    public String getTypeName() {
        return this._typeName;
    }

    @Override
    public void setTypeName(String string) {
        this._typeName = string;
        this.firePropertyChange("type");
    }

    @Override
    public String getValuePropertyName() {
        return this._valuePropertyName;
    }

    @Override
    public PropertyDescriptor getValueProperty() {
        return this.getSpecialProperty(this._valuePropertyName);
    }

    @Override
    public void setValueProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl = false;
        if (propertyDescriptor == null) {
            bl = !StringUtilities.isNullOrEmpty((String)this._valuePropertyName);
            this._valuePropertyName = "";
        } else {
            bl = !propertyDescriptor.getName().equals(this._valuePropertyName);
            this._valuePropertyName = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("value");
        }
    }

    @Override
    public String getDisplayValuePropertyName() {
        return this._displayValuePropertyName;
    }

    @Override
    public PropertyDescriptor getDisplayValueProperty() {
        return this.getSpecialProperty(this._displayValuePropertyName);
    }

    @Override
    public void setDisplayValueProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl = false;
        if (propertyDescriptor == null) {
            bl = !StringUtilities.isNullOrEmpty((String)this._displayValuePropertyName);
            this._displayValuePropertyName = "";
        } else {
            bl = !propertyDescriptor.getName().equals(this._displayValuePropertyName);
            this._displayValuePropertyName = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("displayValue");
        }
    }

    @Override
    public Object getValue() {
        return this.getSpecialPropertyValue(this._valuePropertyName);
    }

    @Override
    public void setValue(Object object) {
        if (StringUtilities.isNullOrEmpty((String)this._valuePropertyName)) {
            this._valuePropertyName = "value";
        }
        this.setSpecialPropertyValue(this._valuePropertyName, object);
    }

    @Override
    public Object getDisplayValue() {
        Object object = this.getSpecialPropertyValue(this._displayValuePropertyName);
        if (!this.isNullOrEmpty(object)) {
            return object;
        }
        return this.getValue();
    }

    @Override
    public void setDisplayValue(Object object) {
        if (StringUtilities.isNullOrEmpty((String)this._displayValuePropertyName)) {
            this._displayValuePropertyName = "display-value";
        }
        this.setSpecialPropertyValue(this._displayValuePropertyName, object);
    }

    @Override
    public void addProperty(PropertyDescriptor propertyDescriptor) {
        if (this.getProperties().add(propertyDescriptor) && !this.isUpdating()) {
            this.firePropertiesChanged();
        }
    }

    @Override
    public void removeProperty(String string) {
        PropertyDescriptor propertyDescriptor = this.getProperties().get(string);
        if (propertyDescriptor != null) {
            this.setValue(propertyDescriptor, null);
            if (this.getProperties().remove(propertyDescriptor)) {
                this.firePropertiesChanged();
            }
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.getChangeSupport().addPropertyChangeListener(propertyChangeListener);
    }

    private PropertyChangeSupport getChangeSupport() {
        if (this._changeSupport == null) {
            this._changeSupport = new PropertyChangeSupport(this);
        }
        return this._changeSupport;
    }

    protected void firePropertiesChanged() {
        this.firePropertyChange("properties", null, (Object)this.getProperties());
    }

    protected void firePropertyChange(String string) {
        this.firePropertyChange(string, null, null);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        if (!this.isUpdating()) {
            this.getChangeSupport().firePropertyChange(string, object, object2);
        }
    }

    protected void checkSpecialPropertyUpdate(PropertyDescriptor propertyDescriptor) {
        if (propertyDescriptor.getName().equals(this._valuePropertyName)) {
            this.firePropertyChange("value");
        } else if (propertyDescriptor.getName().equals(this._displayValuePropertyName)) {
            this.firePropertyChange("displayValue");
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.getChangeSupport().removePropertyChangeListener(propertyChangeListener);
    }

    public PropertyDescriptorSet getProperties() {
        if (this._properties == null) {
            this._properties = new PropertyDescriptorSet();
        }
        return this._properties;
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this.getData().getValue(propertyDescriptor);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this.setValue(propertyDescriptor, object, true, false);
    }

    @Override
    public void setValue(PropertyDescriptor propertyDescriptor, Object object, boolean bl, boolean bl2) {
        Object object2;
        PropertyDescriptor propertyDescriptor2 = this.getProperties().get(propertyDescriptor.getName());
        if (propertyDescriptor2 != null) {
            propertyDescriptor = propertyDescriptor2;
        }
        if (object != null) {
            object2 = object;
            boolean bl3 = Converter.isAssignableFrom((Class)propertyDescriptor.getType(), object.getClass());
            if (!bl3 && object instanceof String && !propertyDescriptor.getType().equals(String.class)) {
                boolean bl4 = bl3 = (object = Converter.convertFrom((String)((String)object), (Class)propertyDescriptor.getType())) != null;
            }
            if (!bl3) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("The property \"");
                stringBuilder.append(propertyDescriptor.getName());
                stringBuilder.append("\" of type \"");
                stringBuilder.append(propertyDescriptor.getType().getSimpleName());
                stringBuilder.append("\" is not compatible with value \"");
                stringBuilder.append(object2);
                stringBuilder.append("\" of type \"");
                stringBuilder.append(object2.getClass().getSimpleName());
                stringBuilder.append("\"");
                throw new IllegalArgumentException(stringBuilder.toString());
            }
        }
        if (!GenericPart.areEqual(object, object2 = this.getData().getValue(propertyDescriptor))) {
            if (propertyDescriptor2 == null) {
                this.getProperties().add(propertyDescriptor);
            }
            this.getData().setValue(propertyDescriptor, object);
            PropertyDescriptorSet propertyDescriptorSet = new PropertyDescriptorSet();
            propertyDescriptorSet.add(propertyDescriptor);
            if (bl) {
                this.refreshProperties(propertyDescriptor, propertyDescriptorSet, object2, bl2);
            }
            if (!this.isUpdating()) {
                this.firePropertiesUpdated(propertyDescriptorSet);
            } else {
                this._updatedProperties.addAll((PropertyDescriptorCollection)propertyDescriptorSet);
            }
        }
    }

    protected void refreshProperties(PropertyDescriptor propertyDescriptor, PropertyDescriptorSet propertyDescriptorSet, Object object, boolean bl) {
        List list = propertyDescriptor.getLinkedProperties();
        for (PropertyDescriptor propertyDescriptor2 : this.getProperties()) {
            if (propertyDescriptor == propertyDescriptor2) continue;
            List list2 = null;
            boolean bl2 = list.contains((Object)propertyDescriptor2);
            if (!bl2 && bl && (!list.isEmpty() || this.isSampleValue(propertyDescriptor, object)) && this.isSampleValue(propertyDescriptor2)) {
                list2 = propertyDescriptor2.getLinkedProperties();
                bl2 = list2.isEmpty();
            }
            if (bl2) {
                this.clearProperty(propertyDescriptor2);
                propertyDescriptorSet.add(propertyDescriptor2);
                continue;
            }
            if (list2 == null) {
                list2 = propertyDescriptor2.getLinkedProperties();
            }
            if (!list2.contains((Object)propertyDescriptor)) continue;
            propertyDescriptor2.refreshValues(this.getData());
            propertyDescriptorSet.add(propertyDescriptor2);
        }
    }

    private boolean isSampleValue(PropertyDescriptor propertyDescriptor, Object object) {
        if (propertyDescriptor instanceof DisplayDescriptor) {
            DisplayDescriptor displayDescriptor = (DisplayDescriptor)propertyDescriptor;
            return Utilities.compareObjects((Object)displayDescriptor.getSampleValue(), (Object)object);
        }
        return false;
    }

    private boolean isSampleValue(PropertyDescriptor propertyDescriptor) {
        return this.isSampleValue(propertyDescriptor, this.getData().getValue(propertyDescriptor));
    }

    private void clearProperty(PropertyDescriptor propertyDescriptor) {
        this.getData().setValue(propertyDescriptor, TypeRegistry.getDefault().getType(propertyDescriptor.getType()).getDefaultValue());
    }

    private static boolean areEqual(Object object, Object object2) {
        if (object == object2) {
            return true;
        }
        if (object != null) {
            return object.equals(object2);
        }
        return object2.equals(object);
    }

    @Override
    public DisplayInformationCollection getDisplayInformation() {
        return this._displayInformation;
    }

    @Override
    public DisplayInformationCollection getOrCreateDisplayInformation() {
        if (this._displayInformation == null) {
            this._displayInformation = new DisplayInformationCollection();
        }
        return this._displayInformation;
    }

    @Override
    public void setDisplayInformation(DisplayInformationCollection displayInformationCollection) {
        this._displayInformation = displayInformationCollection;
        this.firePropertyChange("displayInformationChanged");
    }

    @Override
    public ID getID() {
        return this._id;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof GenericPart)) {
            return false;
        }
        GenericPart genericPart = (GenericPart)object;
        return this._id == genericPart._id || this._id != null && this._id.equals(genericPart._id);
    }

    public int hashCode() {
        return this._id.hashCode();
    }

    private boolean isNullOrEmpty(Object object) {
        if (object == null) {
            return true;
        }
        if (object instanceof String) {
            return ((String)object).isEmpty();
        }
        return false;
    }

    @Override
    public String getNotes() {
        return this._notes;
    }

    @Override
    public void setNotes(String string) {
        if (!GenericPart.areEqual(string, this._notes)) {
            String string2 = this._notes;
            this._notes = string;
            this.fireNotesChange("notesChanged", string2, this._notes);
        }
    }

    @Override
    public Boolean isShowNotesValue() {
        return this._showNotes;
    }

    @Override
    public boolean isShowNotes() {
        return this._showNotes != null ? this._showNotes : false;
    }

    @Override
    public void setShowNotes(Boolean bl) {
        if (!Utilities.compareObjects((Object)this._showNotes, (Object)bl)) {
            Boolean bl2 = this._showNotes;
            this._showNotes = bl;
            this.fireNotesChange("showNotesChanged", bl2, this._showNotes);
        }
    }

    protected void fireNotesChange(String string, Object object, Object object2) {
        if (!this.isUpdating()) {
            this.getNotesChangeSupport().firePropertyChange(string, object, object2);
        }
    }

    @Override
    public void addNotesListener(PropertyChangeListener propertyChangeListener) {
        this.getNotesChangeSupport().addPropertyChangeListener(propertyChangeListener);
    }

    private PropertyChangeSupport getNotesChangeSupport() {
        if (this._notesChangeSupport == null) {
            this._notesChangeSupport = new PropertyChangeSupport(this);
        }
        return this._notesChangeSupport;
    }

    @Override
    public void removeNotesListener(PropertyChangeListener propertyChangeListener) {
        this.getNotesChangeSupport().removePropertyChangeListener(propertyChangeListener);
    }

    @Override
    public Integer getBookmarkValue() {
        return this._bookmark;
    }

    @Override
    public int getBookmark() {
        return this._bookmark != null ? this._bookmark : -1;
    }

    @Override
    public void setBookmark(Integer n) {
        if (!GenericPart.areEqual(n, this._bookmark)) {
            Integer n2 = this._bookmark;
            this._bookmark = n;
            this.fireBookmarkChange("bookmarkChanged", n2, this._bookmark);
        }
    }

    protected void fireBookmarkChange(String string, Object object, Object object2) {
        if (!this.isUpdating()) {
            this.getBookmarkChangeSupport().firePropertyChange(string, object, object2);
        }
    }

    @Override
    public void addBookmarkListener(PropertyChangeListener propertyChangeListener) {
        this.getBookmarkChangeSupport().addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removeBookmarkListener(PropertyChangeListener propertyChangeListener) {
        this.getBookmarkChangeSupport().removePropertyChangeListener(propertyChangeListener);
    }

    private PropertyChangeSupport getBookmarkChangeSupport() {
        if (this._bookmarkChangeSupport == null) {
            this._bookmarkChangeSupport = new PropertyChangeSupport(this);
        }
        return this._bookmarkChangeSupport;
    }

    @Override
    public void beginPropertyUpdating() {
        ++this._propertiesUpdating;
        if (this._updatedProperties == null) {
            this._updatedProperties = new PropertyDescriptorSet();
        }
    }

    @Override
    public void endPropertyUpdating(boolean bl) {
        --this._propertiesUpdating;
        if (!this.isUpdating()) {
            if (bl) {
                this.firePropertiesUpdated(this._updatedProperties);
            }
            this._updatedProperties = null;
        }
    }

    private void firePropertiesUpdated(PropertyDescriptorSet propertyDescriptorSet) {
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorSet) {
            this.firePropertyChange(propertyDescriptor.getName());
        }
        this.firePropertyChange("value");
        this.firePropertyChange("displayValue");
    }

    @Override
    public void setValueString(String string) {
        this._cachedValueStr = string;
    }

    @Override
    public String getValueString() {
        return this._cachedValueStr;
    }

    @Override
    public void setDisplayString(String string) {
        this._cachedDisplayStr = string;
    }

    @Override
    public String getDisplayString() {
        return this._cachedDisplayStr;
    }

    @Override
    public void setHasAttachments(Boolean bl) {
        this._cachedHasAttachments = bl;
    }

    @Override
    public boolean hasAttachments() {
        return this._cachedHasAttachments;
    }

    @Override
    public void setLabelReadonly(boolean bl) {
        this._labelReadonly = bl;
    }

    @Override
    public boolean isLabelReadonly() {
        return this._labelReadonly;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        String string = Integer.toString(System.identityHashCode(this), 36).toUpperCase();
        stringBuilder.append(this.getTypeName()).append("@").append(this.getID()).append(":").append(string);
        boolean bl = true;
        stringBuilder.append("\n");
        String string2 = "   ";
        Object object = this.getProperties().iterator();
        while (object.hasNext()) {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)object.next();
            String string3 = propertyDescriptor.getName();
            Object object2 = this.getValue(propertyDescriptor);
            stringBuilder.append("   ").append(string3).append("=").append(object2).append("\n");
        }
        if (this.getNotes() != null) {
            stringBuilder.append("   ").append("notes=").append(this.getNotes()).append("\n");
        }
        if (this.isShowNotesValue() != null) {
            stringBuilder.append("   ").append("showNotes=").append(this.isShowNotesValue()).append("\n");
        }
        if (this.getBookmarkValue() != null) {
            stringBuilder.append("   ").append("bookmark=").append(this.getBookmarkValue()).append("\n");
        }
        if (this.getValuePropertyName() != null) {
            stringBuilder.append("   ").append("valuePropertyName=").append(this.getValuePropertyName()).append("\n");
        }
        if (this.getDisplayValuePropertyName() != null) {
            stringBuilder.append("   ").append("displayValuePropertyName=").append(this.getDisplayValuePropertyName()).append("\n");
        }
        if (this instanceof MaltegoEntity && (object = (MaltegoEntity)((Object)this)).getImagePropertyName() != null) {
            stringBuilder.append("   ").append("imagePropertyName=").append(object.getImagePropertyName()).append("\n");
        }
        String string4 = stringBuilder.toString();
        return string4;
    }

    private void checkSetterCaller() {
        boolean bl = false;
        for (int i = 3; i < 6; ++i) {
            if (!Reflection.getCallerClass(i).getName().contains("GraphDataStore")) continue;
            bl = true;
            break;
        }
        try {
            if (this._graphID == null && bl) {
                throw new IllegalStateException("Setter called for non-graph part from inside graph data store: " + this.getID());
            }
            if (this._graphID != null && !bl) {
                throw new IllegalStateException("Setter called for graph part from outside graph data store: " + this.getID());
            }
        }
        catch (IllegalStateException var2_3) {
            NormalException.logStackTrace((Throwable)var2_3);
        }
    }
}

