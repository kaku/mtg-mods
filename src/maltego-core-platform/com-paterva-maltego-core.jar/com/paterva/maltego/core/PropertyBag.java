/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyProvider
 */
package com.paterva.maltego.core;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyProvider;
import java.beans.PropertyChangeListener;

public interface PropertyBag
extends PropertyProvider {
    public static final String PROP_PROPERTIES = "properties";

    public void addProperty(PropertyDescriptor var1);

    public void removeProperty(String var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

