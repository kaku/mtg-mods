/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.util.StringUtilities;
import java.util.UUID;

public class Guid
implements Comparable<Guid> {
    protected static final int RADIX = 36;
    private final long _value;
    private static long _lastValue = UUID.randomUUID().getLeastSignificantBits();
    private static final Object LOCK = new Object();

    protected Guid(long l) {
        this._value = l;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected Guid() {
        Object object = LOCK;
        synchronized (object) {
            this._value = _lastValue++;
        }
    }

    public static synchronized Guid create() {
        return new Guid();
    }

    public static Guid parse(String string) {
        long l = StringUtilities.parseUnsignedLong((String)string.toLowerCase(), (int)36);
        return new Guid(l);
    }

    public long getValue() {
        return this._value;
    }

    @Override
    public int compareTo(Guid guid) {
        return Long.compare(this._value, guid._value);
    }

    public int hashCode() {
        return (int)this._value;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Guid guid = (Guid)object;
        return this._value == guid._value;
    }

    public String toString() {
        return StringUtilities.toStringUnsignedLong((long)this._value, (int)36).toLowerCase();
    }
}

