/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;

public class EntityUpdate
extends GenericEntity {
    public EntityUpdate(EntityID entityID) {
        super(entityID);
    }

    public EntityUpdate(EntityID entityID, String string) {
        super(entityID);
        this.setTypeName(string);
    }

    public EntityUpdate(MaltegoEntity maltegoEntity) {
        this((EntityID)maltegoEntity.getID(), maltegoEntity.getTypeName());
    }
}

