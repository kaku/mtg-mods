/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.LinkID;

public class GraphLink
extends GraphPart<LinkID> {
    public GraphLink(GraphID graphID, LinkID linkID) {
        super(graphID, linkID);
    }
}

