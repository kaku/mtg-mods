/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.BookmarkProvider;
import com.paterva.maltego.core.DisplayInformationProvider;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.IdentityProvider;
import com.paterva.maltego.core.NotesProvider;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;

public interface MaltegoPart<ID extends Guid>
extends TypedPropertyBag,
DisplayInformationProvider,
IdentityProvider<ID>,
NotesProvider,
BookmarkProvider {
    public void setGraphID(GraphID var1);

    public boolean isCopy(MaltegoPart<ID> var1);

    public void setValue(PropertyDescriptor var1, Object var2, boolean var3, boolean var4);

    public void beginPropertyUpdating();

    public void endPropertyUpdating(boolean var1);

    public DataSource getData();

    public void setData(DataSource var1);

    public void setValueString(String var1);

    public String getValueString();

    public void setDisplayString(String var1);

    public String getDisplayString();

    public void setHasAttachments(Boolean var1);

    public boolean hasAttachments();

    public void setLabelReadonly(boolean var1);

    public boolean isLabelReadonly();
}

