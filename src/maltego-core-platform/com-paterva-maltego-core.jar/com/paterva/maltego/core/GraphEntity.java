/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;

public class GraphEntity
extends GraphPart<EntityID> {
    public GraphEntity(GraphID graphID, EntityID entityID) {
        super(graphID, entityID);
    }
}

