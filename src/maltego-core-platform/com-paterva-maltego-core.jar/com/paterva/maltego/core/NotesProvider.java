/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import java.beans.PropertyChangeListener;

public interface NotesProvider {
    public static final String PROP_NOTES = "notesChanged";
    public static final String PROP_SHOW_NOTES = "showNotesChanged";

    public String getNotes();

    public void setNotes(String var1);

    public Boolean isShowNotesValue();

    public boolean isShowNotes();

    public void setShowNotes(Boolean var1);

    public void addNotesListener(PropertyChangeListener var1);

    public void removeNotesListener(PropertyChangeListener var1);
}

