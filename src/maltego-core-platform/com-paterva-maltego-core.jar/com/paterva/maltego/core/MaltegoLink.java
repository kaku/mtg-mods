/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.awt.Color;

public interface MaltegoLink
extends MaltegoPart<LinkID> {
    public static final String[] LineStyle = new String[]{"Solid", "Dashed", "Dotted", "Dashed dotted"};
    public static final String[] ShowLabel = new String[]{"Use Global Setting", "Yes", "No"};
    public static final int SHOW_LABEL_GLOBAL = 0;
    public static final int SHOW_LABEL_YES = 1;
    public static final int SHOW_LABEL_NO = 2;
    public static final String PROP_SHOW_LABEL = "show-label";
    public static final String PROP_COLOR = "color";
    public static final String PROP_STYLE = "style";
    public static final String PROP_THICKNESS = "thickness";

    public MaltegoLink createCopy();

    public MaltegoLink createClone();

    public PropertyDescriptor getShowLabelProperty();

    public void setShowLabelProperty(PropertyDescriptor var1);

    public void setShowLabel(int var1);

    public int getShowLabel();

    public PropertyDescriptor getColorProperty();

    public void setColorProperty(PropertyDescriptor var1);

    public Color getColor();

    public void setColor(Color var1);

    public PropertyDescriptor getStyleProperty();

    public void setStyleProperty(PropertyDescriptor var1);

    public void setStyle(Integer var1);

    public Integer getStyle();

    public PropertyDescriptor getThicknessProperty();

    public void setThicknessProperty(PropertyDescriptor var1);

    public void setThickness(Integer var1);

    public Integer getThickness();

    public Boolean isReversed();

    public void setReversed(Boolean var1);
}

