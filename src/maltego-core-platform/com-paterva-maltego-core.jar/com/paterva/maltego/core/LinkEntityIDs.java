/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.EntityID;
import java.util.Objects;

public class LinkEntityIDs {
    private EntityID _sourceID;
    private EntityID _targetID;

    public LinkEntityIDs(LinkEntityIDs linkEntityIDs) {
        this(linkEntityIDs.getSourceID(), linkEntityIDs.getTargetID());
    }

    public LinkEntityIDs(EntityID entityID, EntityID entityID2) {
        if (entityID == null) {
            throw new IllegalArgumentException("Source may not be null");
        }
        if (entityID2 == null) {
            throw new IllegalArgumentException("Target may not be null");
        }
        this._sourceID = entityID;
        this._targetID = entityID2;
    }

    public EntityID getSourceID() {
        return this._sourceID;
    }

    public void setSourceID(EntityID entityID) {
        this._sourceID = entityID;
    }

    public EntityID getTargetID() {
        return this._targetID;
    }

    public void setTargetID(EntityID entityID) {
        this._targetID = entityID;
    }

    public String toString() {
        return "LinkEntityIDs{_sourceID=" + this._sourceID + ", _targetID=" + this._targetID + '}';
    }

    public int hashCode() {
        int n = 7;
        n = 73 * n + Objects.hashCode(this._sourceID);
        n = 73 * n + Objects.hashCode(this._targetID);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        LinkEntityIDs linkEntityIDs = (LinkEntityIDs)object;
        if (!Objects.equals(this._sourceID, linkEntityIDs._sourceID)) {
            return false;
        }
        if (!Objects.equals(this._targetID, linkEntityIDs._targetID)) {
            return false;
        }
        return true;
    }
}

