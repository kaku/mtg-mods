/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.DisplayInformationCollection;

public interface DisplayInformationProvider {
    public static final String PROP_DISPLAY_INFO = "displayInformationChanged";

    public DisplayInformationCollection getDisplayInformation();

    public DisplayInformationCollection getOrCreateDisplayInformation();

    public void setDisplayInformation(DisplayInformationCollection var1);
}

