/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.Guid;

public interface IdentityProvider<ID extends Guid> {
    public ID getID();
}

