/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;

public abstract class GraphPart<ID extends Guid> {
    private final GraphID _graphID;
    private final ID _partID;

    public GraphPart(GraphID graphID, ID ID) {
        this._graphID = graphID;
        this._partID = ID;
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public ID getID() {
        return this._partID;
    }

    public int hashCode() {
        int n = 7;
        n = 17 * n + (this._graphID != null ? this._graphID.hashCode() : 0);
        n = 17 * n + (this._partID != null ? this._partID.hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GraphPart graphPart = (GraphPart)object;
        if (!(this._graphID == graphPart._graphID || this._graphID != null && this._graphID.equals(graphPart._graphID))) {
            return false;
        }
        return this._partID == graphPart._partID || this._partID != null && this._partID.equals(graphPart._partID);
    }

    public String toString() {
        return this.getGraphID() + "->" + this.getID();
    }
}

