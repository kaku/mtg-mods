/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.GenericLink;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;

public class LinkUpdate
extends GenericLink {
    public LinkUpdate(LinkID linkID) {
        super(linkID);
    }

    public LinkUpdate(LinkID linkID, String string) {
        super(linkID);
        this.setTypeName(string);
    }

    public LinkUpdate(MaltegoLink maltegoLink) {
        this((LinkID)maltegoLink.getID(), maltegoLink.getTypeName());
    }
}

