/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.core;

import java.beans.PropertyChangeListener;

public interface BookmarkProvider {
    public static final String PROP_BOOKMARK = "bookmarkChanged";

    public Integer getBookmarkValue();

    public int getBookmark();

    public void setBookmark(Integer var1);

    public void addBookmarkListener(PropertyChangeListener var1);

    public void removeBookmarkListener(PropertyChangeListener var1);
}

