/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.GenericPart;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import java.awt.Color;
import org.openide.util.Utilities;

public class GenericLink
extends GenericPart<LinkID>
implements MaltegoLink {
    private String _propNameShowLabel;
    private String _propNameColor;
    private String _propNameStyle;
    private String _propNameThickness;
    private Boolean _reversed;

    public GenericLink(LinkID linkID) {
        super(linkID);
    }

    public GenericLink(LinkID linkID, String string) {
        super(linkID, string);
    }

    public GenericLink(LinkID linkID, String string, PropertyDescriptorCollection propertyDescriptorCollection) {
        super(linkID, string, propertyDescriptorCollection);
    }

    public GenericLink(MaltegoLink maltegoLink) {
        super(maltegoLink);
        if (maltegoLink instanceof GenericLink) {
            GenericLink genericLink = (GenericLink)maltegoLink;
            this._propNameShowLabel = genericLink._propNameShowLabel;
            this._propNameColor = genericLink._propNameColor;
            this._propNameStyle = genericLink._propNameStyle;
            this._propNameThickness = genericLink._propNameThickness;
            this._reversed = genericLink._reversed;
        }
    }

    protected Object clone() throws CloneNotSupportedException {
        return this.createClone();
    }

    @Override
    public GenericLink createCopy() {
        return this.createCopy(LinkID.create());
    }

    public GenericLink createCopy(LinkID linkID) {
        GenericLink genericLink = new GenericLink(this);
        genericLink.setId(linkID);
        return genericLink;
    }

    @Override
    public GenericLink createClone() {
        return new GenericLink(this);
    }

    @Override
    public boolean isCopy(MaltegoPart<LinkID> maltegoPart) {
        if (maltegoPart instanceof GenericLink) {
            GenericLink genericLink = (GenericLink)maltegoPart;
            if (!Utilities.compareObjects((Object)this._reversed, (Object)genericLink._reversed)) {
                return false;
            }
        } else {
            return false;
        }
        return GenericPart.super.isCopy(maltegoPart);
    }

    @Override
    public PropertyDescriptor getShowLabelProperty() {
        return this.getSpecialProperty(this._propNameShowLabel);
    }

    @Override
    public void setShowLabelProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl;
        if (propertyDescriptor == null) {
            bl = this._propNameShowLabel != null;
            this._propNameShowLabel = null;
        } else {
            bl = !propertyDescriptor.getName().equals(this._propNameShowLabel);
            this._propNameShowLabel = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("show-label");
        }
    }

    @Override
    public int getShowLabel() {
        Object object = this.getSpecialPropertyValue(this._propNameShowLabel);
        return object != null ? (Integer)object : 0;
    }

    @Override
    public void setShowLabel(int n) {
        if (this._propNameShowLabel == null) {
            this._propNameShowLabel = "show-label";
        }
        if (n >= 0 && n < MaltegoLink.ShowLabel.length) {
            this.setSpecialPropertyValue(this._propNameShowLabel, n);
        }
    }

    @Override
    public PropertyDescriptor getColorProperty() {
        return this.getSpecialProperty(this._propNameColor);
    }

    @Override
    public void setColorProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl;
        if (propertyDescriptor == null) {
            bl = this._propNameColor != null;
            this._propNameColor = null;
        } else {
            bl = !propertyDescriptor.getName().equals(this._propNameColor);
            this._propNameColor = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("color");
        }
    }

    @Override
    public Color getColor() {
        Color color = (Color)this.getSpecialPropertyValue(this._propNameColor);
        return color;
    }

    @Override
    public void setColor(Color color) {
        if (this._propNameColor == null) {
            this._propNameColor = "color";
        }
        this.setSpecialPropertyValue(this._propNameColor, color);
    }

    @Override
    public PropertyDescriptor getStyleProperty() {
        return this.getSpecialProperty(this._propNameStyle);
    }

    @Override
    public void setStyleProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl;
        if (propertyDescriptor == null) {
            bl = this._propNameStyle != null;
            this._propNameStyle = null;
        } else {
            bl = !propertyDescriptor.getName().equals(this._propNameStyle);
            this._propNameStyle = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("style");
        }
    }

    @Override
    public Integer getStyle() {
        return (Integer)this.getSpecialPropertyValue(this._propNameStyle);
    }

    @Override
    public void setStyle(Integer n) {
        if (this._propNameStyle == null) {
            this._propNameStyle = "style";
        }
        this.setSpecialPropertyValue(this._propNameStyle, n);
    }

    @Override
    public PropertyDescriptor getThicknessProperty() {
        return this.getSpecialProperty(this._propNameThickness);
    }

    @Override
    public void setThicknessProperty(PropertyDescriptor propertyDescriptor) {
        boolean bl;
        if (propertyDescriptor == null) {
            bl = this._propNameThickness != null;
            this._propNameThickness = null;
        } else {
            bl = !propertyDescriptor.getName().equals(this._propNameThickness);
            this._propNameThickness = propertyDescriptor.getName();
        }
        if (bl) {
            this.firePropertyChange("thickness");
        }
    }

    @Override
    public Integer getThickness() {
        return (Integer)this.getSpecialPropertyValue(this._propNameThickness);
    }

    @Override
    public void setThickness(Integer n) {
        if (this._propNameThickness == null) {
            this._propNameThickness = "thickness";
        }
        this.setSpecialPropertyValue(this._propNameThickness, n);
    }

    @Override
    public Boolean isReversed() {
        return this._reversed;
    }

    @Override
    public void setReversed(Boolean bl) {
        this._reversed = bl;
    }
}

