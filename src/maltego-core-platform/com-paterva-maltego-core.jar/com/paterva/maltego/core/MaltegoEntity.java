/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.core;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.awt.datatransfer.DataFlavor;

public interface MaltegoEntity
extends MaltegoPart<EntityID> {
    public static final DataFlavor FLAVOR = new DataFlavor(MaltegoEntity.class, "Maltego Entity");
    public static final String PROP_IMAGE = "image";

    public MaltegoEntity createCopy();

    public MaltegoEntity createClone();

    public Integer getWeightValue();

    public int getWeight();

    public void setWeight(Integer var1);

    public String getImagePropertyName();

    public PropertyDescriptor getImageProperty();

    public void setImageProperty(PropertyDescriptor var1);

    public void setImage(Object var1);

    public Object getImage();

    public void setImageCachedObject(Object var1);

    public Object getImageCachedObject();
}

