/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.adapter.TransformAdapter
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCallback
 *  com.paterva.maltego.typing.DataSource
 */
package com.paterva.maltego.transform.protocol.v2;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.adapter.TransformAdapter;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.typing.DataSource;
import java.util.Map;

public class RemoteTransformAdapterV2
implements TransformAdapter {
    private final TransformAdapter _delegate = new com.paterva.maltego.transform.protocol.v2api.RemoteTransformAdapterV2();

    public void run(GraphID graphID, Map<EntityID, MaltegoEntity> map, TransformDescriptor transformDescriptor, String string, DataSource dataSource, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory, TransformCallback transformCallback) {
        this._delegate.run(graphID, map, transformDescriptor, string, dataSource, entityRegistry, entityFactory, linkFactory, transformCallback);
    }

    public void cancel() {
        this._delegate.cancel();
    }
}

