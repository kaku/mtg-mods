/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

public class MaltegoClientException
extends Exception {
    public MaltegoClientException() {
    }

    public MaltegoClientException(String string) {
        super(string);
    }

    public MaltegoClientException(String string, Exception exception) {
        super(string, exception);
    }
}

