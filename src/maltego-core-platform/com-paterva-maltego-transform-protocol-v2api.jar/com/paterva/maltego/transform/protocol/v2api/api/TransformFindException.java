/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.MaltegoClientException;

public class TransformFindException
extends MaltegoClientException {
    public TransformFindException() {
    }

    public TransformFindException(String string) {
        super(string);
    }

    public TransformFindException(String string, Exception exception) {
        super(string, exception);
    }
}

