/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XMLEscapeUtils
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.transform.protocol.v2api.EntityPropertyMapper;
import com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalField;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalFieldCollection;
import com.paterva.maltego.transform.protocol.v2api.messaging.DisplayInfo;
import com.paterva.maltego.transform.protocol.v2api.messaging.DisplayInfoCollection;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EntityTranslator {
    private static final PropertyDescriptor ICON_URL_PROPERTY = new DisplayDescriptor(FastURL.class, "icon-url", "Image");
    private EntityPropertyMapper[] _mappings = new EntityPropertyMapper[]{new EntityPropertyMapper.Type("maltego.Person", new String[]{"person.firstnames", "person.lastname"}, new String[]{"firstname", "lastname"}), new EntityPropertyMapper.Type("maltego.Domain", new String[]{"whois-info"}, new String[]{"whois"}), new EntityPropertyMapper.Type("maltego.IPv4Address", new String[]{"whois-info"}, new String[]{"whois"}), new EntityPropertyMapper.Type("maltego.URL", new String[]{"short-title", "url", "title"}, new String[]{"maltego.v2.value.property", "theurl", "fulltitle"}), new EntityPropertyMapper.Type("maltego.Document", new String[]{"title", "url", "document.meta-data"}, new String[]{"maltego.v2.value.property", "link", "metainfo"}), new EntityPropertyMapper.Type("maltego.Location", new String[]{"country", "city", "location.area", "countrycode", "longitude", "latitude"}, new String[]{"country", "city", "area", "countrysc", "long", "lat"}), new EntityPropertyMapper.Type("maltego.PhoneNumber", new String[]{"phonenumber.countrycode", "phonenumber.citycode", "phonenumber.areacode", "phonenumber.lastnumbers"}, new String[]{"countrycode", "citycode", "areacode", "lastnumbers"}), new EntityPropertyMapper.Type("maltego.affiliation.Spock", new String[]{"affiliation.network", "affiliation.uid", "affiliation.profile-url", "spock.websites"}, new String[]{"network", "uid", "profile_url", "spock_websites"}), new EntityPropertyMapper.Affiliation(new String[]{"affiliation.network", "affiliation.uid", "affiliation.profile-url"}, new String[]{"network", "uid", "profile_url"}), new EntityPropertyMapper.Type("maltego.Service", new String[]{"banner.text", "port.number"}, new String[]{"banner", "port"}), new EntityPropertyMapper.Type("maltego.Alias", new String[]{"alias"}, new String[]{"properties.alias"}), new EntityPropertyMapper.Type("maltego.Device", new String[]{"device"}, new String[]{"properties.device"}), new EntityPropertyMapper.Type("maltego.GPS", new String[]{"gps.coordinate"}, new String[]{"properties.gps"}), new EntityPropertyMapper.Type("maltego.CircularArea", new String[]{"radius"}, new String[]{"area"}), new EntityPropertyMapper.Type("maltego.Image", new String[]{"description", "url"}, new String[]{"properties.image", "fullImage"}), new EntityPropertyMapper.Type("maltego.NominatimLocation", new String[]{"nominatimlocation"}, new String[]{"properties.nominatimlocation"}), new EntityPropertyMapper.Type("maltego.BuiltWithTechnology", new String[]{"builtwith.technology"}, new String[]{"properties.builtwithtechnology"}), new EntityPropertyMapper.Type("maltego.FacebookObject", new String[]{"facebook.object"}, new String[]{"properties.facebookobject"})};
    private static EntityTranslator _instance;

    private EntityTranslator() {
    }

    public static synchronized EntityTranslator instance() {
        if (_instance == null) {
            _instance = new EntityTranslator();
        }
        return _instance;
    }

    public Collection<MaltegoEntity> toEntities(Collection<EntityDescriptor> collection, EntityFactory entityFactory, EntityRegistry entityRegistry) throws TypeInstantiationException {
        if (collection == null || collection.isEmpty()) {
            return new ArrayList<MaltegoEntity>(0);
        }
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(collection.size());
        for (EntityDescriptor entityDescriptor : collection) {
            MaltegoEntity maltegoEntity = this.translate(entityDescriptor, entityFactory, entityRegistry);
            if (InheritanceHelper.getValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity) == null) continue;
            arrayList.add(maltegoEntity);
        }
        return arrayList;
    }

    public Collection<EntityDescriptor> translate(Collection<MaltegoEntity> collection, EntityRegistry entityRegistry) {
        if (collection == null || collection.isEmpty()) {
            return new ArrayList<EntityDescriptor>(0);
        }
        ArrayList<EntityDescriptor> arrayList = new ArrayList<EntityDescriptor>(collection.size());
        for (MaltegoEntity maltegoEntity : collection) {
            arrayList.add(this.translate(maltegoEntity, entityRegistry));
        }
        return arrayList;
    }

    public MaltegoEntity translate(EntityDescriptor entityDescriptor, EntityFactory entityFactory, EntityRegistry entityRegistry) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = (MaltegoEntity)entityFactory.createInstance(EntitySpecTranslator.getV3TypeName(entityDescriptor.getTypeName()), false, true);
        maltegoEntity.setWeight(Integer.valueOf(entityDescriptor.getWeight()));
        if (entityDescriptor.getIconUrl() != null) {
            maltegoEntity.addProperty(ICON_URL_PROPERTY);
            maltegoEntity.setValue(ICON_URL_PROPERTY, (Object)entityDescriptor.getIconUrl());
            maltegoEntity.setImageProperty(ICON_URL_PROPERTY);
        }
        this.copyDisplayInformation(maltegoEntity.getOrCreateDisplayInformation(), entityDescriptor.getDisplayInformation());
        String string = entityDescriptor.getValue();
        String string2 = this.v3PropertyName(entityDescriptor, "maltego.v2.value.property", null, entityRegistry);
        PropertyDescriptor propertyDescriptor = null;
        if (string2 != null) {
            propertyDescriptor = maltegoEntity.getProperties().get(string2);
        }
        if (propertyDescriptor == null) {
            propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity, (boolean)false);
        }
        if (propertyDescriptor != null) {
            Object object = Converter.convertFrom((String)string, (Class)propertyDescriptor.getType());
            maltegoEntity.setValue(propertyDescriptor, object);
        }
        this.copyProperties(maltegoEntity, entityDescriptor, entityRegistry);
        return maltegoEntity;
    }

    private void copyDisplayInformation(DisplayInformationCollection displayInformationCollection, DisplayInfoCollection displayInfoCollection) {
        for (DisplayInfo displayInfo : displayInfoCollection) {
            displayInformationCollection.add(displayInfo.getName(), XMLEscapeUtils.unescape((String)displayInfo.getValue()));
        }
    }

    public EntityDescriptor translate(MaltegoEntity maltegoEntity, EntityRegistry entityRegistry) {
        Object object;
        String string;
        EntityDescriptor entityDescriptor = new EntityDescriptor();
        entityDescriptor.setTypeName(EntitySpecTranslator.getV2TypeName(maltegoEntity.getTypeName()));
        this.copyProperties(entityDescriptor, maltegoEntity, entityRegistry);
        String string2 = this.v3PropertyName(entityDescriptor, "maltego.v2.value.property", null, entityRegistry);
        PropertyDescriptor propertyDescriptor = null;
        if (string2 != null) {
            propertyDescriptor = maltegoEntity.getProperties().get(string2);
        }
        if (propertyDescriptor == null) {
            propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity, (boolean)true);
        }
        if (propertyDescriptor != null && !StringUtilities.isNullOrEmpty((String)(string = Converter.convertTo((Object)(object = maltegoEntity.getValue(propertyDescriptor)), (Class)propertyDescriptor.getType())))) {
            entityDescriptor.setValue(string);
        }
        entityDescriptor.setWeight(maltegoEntity.getWeight());
        return entityDescriptor;
    }

    private void copyProperties(MaltegoEntity maltegoEntity, EntityDescriptor entityDescriptor, EntityRegistry entityRegistry) {
        AdditionalFieldCollection additionalFieldCollection = entityDescriptor.getFields();
        if (additionalFieldCollection != null) {
            for (AdditionalField additionalField : entityDescriptor.getFields()) {
                PropertyDescriptor propertyDescriptor = maltegoEntity.getProperties().get(this.v3PropertyName(entityDescriptor, additionalField.getName(), additionalField.getName(), entityRegistry));
                if (propertyDescriptor == null) {
                    propertyDescriptor = "maltego.automation.dob".equals(additionalField.getName()) ? new PropertyDescriptor(DateTime.class, additionalField.getName(), additionalField.getDisplayName()) : new PropertyDescriptor(String.class, additionalField.getName(), additionalField.getDisplayName());
                    maltegoEntity.addProperty(propertyDescriptor);
                }
                maltegoEntity.setValue(propertyDescriptor, Converter.convertFrom((String)additionalField.getValue(), (Class)propertyDescriptor.getType()));
            }
        }
    }

    public String v3PropertyName(EntityDescriptor entityDescriptor, String string, String string2, EntityRegistry entityRegistry) {
        String string3 = EntitySpecTranslator.getV3TypeName(entityDescriptor.getTypeName());
        String string4 = null;
        List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string3);
        for (EntityPropertyMapper entityPropertyMapper : this._mappings) {
            for (String string5 : list) {
                if (!entityPropertyMapper.matches(string5)) continue;
                string4 = entityPropertyMapper.toNew(string);
                break;
            }
            if (string4 != null) break;
        }
        if (string4 == null) {
            return string2;
        }
        return string4;
    }

    public String v2PropertyName(MaltegoEntity maltegoEntity, String string, String string2, EntityRegistry entityRegistry) {
        String string3 = maltegoEntity.getTypeName();
        String string4 = null;
        List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string3);
        for (EntityPropertyMapper entityPropertyMapper : this._mappings) {
            for (String string5 : list) {
                if (!entityPropertyMapper.matches(string5)) continue;
                string4 = entityPropertyMapper.toOld(string);
                break;
            }
            if (string4 != null) break;
        }
        if (string4 == null) {
            return string2;
        }
        return string4;
    }

    private void copyProperties(EntityDescriptor entityDescriptor, MaltegoEntity maltegoEntity, EntityRegistry entityRegistry) {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
        if (propertyDescriptorCollection != null) {
            for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                String string;
                Object object = maltegoEntity.getValue(propertyDescriptor);
                if (object == null || StringUtilities.isNullOrEmpty((String)(string = Converter.convertTo((Object)object, (Class)propertyDescriptor.getType())))) continue;
                AdditionalField additionalField = new AdditionalField();
                additionalField.setName(this.v2PropertyName(maltegoEntity, propertyDescriptor.getName(), propertyDescriptor.getName(), entityRegistry));
                additionalField.setDisplayName(propertyDescriptor.getDisplayName());
                additionalField.setValue(string);
                entityDescriptor.getFields().add(additionalField);
            }
        }
    }
}

