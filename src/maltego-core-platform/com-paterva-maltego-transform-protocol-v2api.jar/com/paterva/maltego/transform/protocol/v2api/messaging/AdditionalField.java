/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.DisplayInfo;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="Field", strict=0)
public class AdditionalField {
    private String _name = "";
    private String _displayName = "";
    private String _value = "";
    private String _matchingRule = null;
    private boolean _isKey = false;

    public AdditionalField() {
    }

    public AdditionalField(String string, String string2) {
        this(string, string, string2);
    }

    public AdditionalField(String string, String string2, String string3) {
        this(string, string2, string3, null);
    }

    public AdditionalField(String string, String string2, String string3, String string4) {
        this._name = string;
        this._value = string3;
        this._displayName = string2;
        this._matchingRule = string4;
    }

    @Attribute(name="Name")
    public String getName() {
        return this._name;
    }

    @Attribute(name="Name")
    public void setName(String string) {
        this._name = string;
    }

    @Attribute(name="DisplayName", required=0)
    public String getDisplayName() {
        return this._displayName;
    }

    @Attribute(name="DisplayName", required=0)
    public void setDisplayName(String string) {
        this._displayName = string;
    }

    @Attribute(name="MatchingRule", required=0)
    public String getMatchingRule() {
        return this._matchingRule;
    }

    @Attribute(name="MatchingRule", required=0)
    public void setMatchingRule(String string) {
        this._matchingRule = string;
    }

    @Text(required=0)
    public String getValue() {
        return this._value;
    }

    @Text(required=0)
    public void setValue(String string) {
        this._value = string;
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    public String toString() {
        return this.getName();
    }

    public boolean equals(Object object) {
        if (object instanceof DisplayInfo) {
            return this.equals((DisplayInfo)object);
        }
        return false;
    }

    public boolean equals(DisplayInfo displayInfo) {
        return displayInfo.getName().equals(this.getName());
    }
}

