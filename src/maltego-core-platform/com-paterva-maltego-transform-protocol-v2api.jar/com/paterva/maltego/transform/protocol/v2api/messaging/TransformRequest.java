/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="MaltegoTransformRequestMessage", strict=0)
public class TransformRequest {
    @ElementList(name="Entities", type=EntityDescriptor.class)
    private Collection<EntityDescriptor> _entities;
    @ElementList(name="TransformFields", type=Input.class, required=0)
    private Collection<Input> _inputs;
    private Limit _limits = new Limit();

    @Element(name="Limits", required=0)
    public Limit getLimits() {
        return this._limits;
    }

    @Element(name="Limits", required=0)
    public void setLimits(Limit limit) {
        this._limits = limit;
    }

    public Collection<EntityDescriptor> getEntities() {
        if (this._entities == null) {
            this._entities = new ArrayList<EntityDescriptor>();
        }
        return this._entities;
    }

    public Collection<Input> getInputs() {
        if (this._inputs == null) {
            this._inputs = new ArrayList<Input>();
        }
        return this._inputs;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (EntityDescriptor entityDescriptor : this.getEntities()) {
            stringBuffer.append(entityDescriptor.toString());
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    public static class Limit {
        private int _hard;
        private int _soft;

        public Limit() {
            this(0, 0);
        }

        public Limit(int n, int n2) {
            this._hard = n2;
            this._soft = n;
        }

        @Attribute(name="SoftLimit")
        public int getSoftLimit() {
            return this._soft;
        }

        @Attribute(name="SoftLimit")
        public void setSoftLimit(int n) {
            this._soft = n;
        }

        @Attribute(name="HardLimit")
        public int getHardLimit() {
            return this._hard;
        }

        @Attribute(name="HardLimit")
        public void setHardLimit(int n) {
            this._hard = n;
        }
    }

    @Root(name="Field", strict=0)
    public static class Input {
        private String _name;
        private String _value;

        @Text
        public String getValue() {
            return this._value;
        }

        @Text
        public void setValue(String string) {
            this._value = string;
        }

        @Attribute(name="Name")
        public String getName() {
            return this._name;
        }

        @Attribute(name="Name")
        public void setName(String string) {
            this._name = string;
        }
    }

}

