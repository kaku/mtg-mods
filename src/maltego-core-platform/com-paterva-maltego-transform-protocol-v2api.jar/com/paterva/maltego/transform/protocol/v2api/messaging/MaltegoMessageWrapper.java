/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.DiscoveryResponse;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListResponse;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformRequest;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformResponse;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="MaltegoMessage", strict=0)
public class MaltegoMessageWrapper {
    private DiscoveryResponse _discovery;
    private TransformListResponse _listResponse;
    private TransformRequest _transformRequest;
    private TransformResponse _transformResponse;
    private ExceptionResponse _exceptionResponse;

    public MaltegoMessageWrapper() {
    }

    public MaltegoMessageWrapper(DiscoveryResponse discoveryResponse) {
        this._discovery = discoveryResponse;
    }

    public MaltegoMessageWrapper(TransformListResponse transformListResponse) {
        this._listResponse = transformListResponse;
    }

    public MaltegoMessageWrapper(TransformRequest transformRequest) {
        this._transformRequest = transformRequest;
    }

    public MaltegoMessageWrapper(TransformResponse transformResponse) {
        this._transformResponse = transformResponse;
    }

    public MaltegoMessageWrapper(ExceptionResponse exceptionResponse) {
        this._exceptionResponse = exceptionResponse;
    }

    @Element(name="MaltegoTransformDiscoveryMessage", required=0)
    public DiscoveryResponse getDiscoveryResponse() {
        return this._discovery;
    }

    @Element(name="MaltegoTransformDiscoveryMessage", required=0)
    public void setDiscoveryResponse(DiscoveryResponse discoveryResponse) {
        this._discovery = discoveryResponse;
    }

    @Element(name="MaltegoTransformListMessage", required=0)
    public TransformListResponse getListResponse() {
        return this._listResponse;
    }

    @Element(name="MaltegoTransformListMessage", required=0)
    public void setListResponse(TransformListResponse transformListResponse) {
        this._listResponse = transformListResponse;
    }

    @Element(name="MaltegoTransformRequestMessage", required=0)
    public TransformRequest getTransformRequest() {
        return this._transformRequest;
    }

    @Element(name="MaltegoTransformRequestMessage", required=0)
    public void setTransformRequest(TransformRequest transformRequest) {
        this._transformRequest = transformRequest;
    }

    @Element(name="MaltegoTransformResponseMessage", required=0)
    public TransformResponse getTransformResponse() {
        return this._transformResponse;
    }

    @Element(name="MaltegoTransformResponseMessage", required=0)
    public void setTransformResponse(TransformResponse transformResponse) {
        this._transformResponse = transformResponse;
    }

    @Element(name="MaltegoTransformExceptionMessage", required=0)
    public ExceptionResponse getExceptionResponse() {
        return this._exceptionResponse;
    }

    @Element(name="MaltegoTransformExceptionMessage", required=0)
    public void setExceptionResponse(ExceptionResponse exceptionResponse) {
        this._exceptionResponse = exceptionResponse;
    }
}

