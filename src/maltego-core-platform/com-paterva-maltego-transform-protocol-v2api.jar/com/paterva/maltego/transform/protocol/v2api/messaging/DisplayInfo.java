/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="Label", strict=0)
public class DisplayInfo {
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final String MIME_TEXT_HTML = "text/html";
    private String _type;
    private String _name;
    private String _value;

    public DisplayInfo() {
        this("", "", "text/html");
    }

    public DisplayInfo(String string, String string2) {
        this(string, string2, "text/html");
    }

    public DisplayInfo(String string, String string2, String string3) {
        this._type = string3;
        this._name = string;
        this._value = string2;
    }

    @Attribute(name="Type", required=0)
    public String getMimeType() {
        return this._type;
    }

    @Attribute(name="Type", required=0)
    public void setMimeType(String string) {
        this._type = string;
    }

    @Attribute(name="Name")
    public String getName() {
        return this._name;
    }

    @Attribute(name="Name")
    public void setName(String string) {
        this._name = string;
    }

    @Text(data=1, required=0)
    public String getValue() {
        return this._value;
    }

    @Text(data=1, required=0)
    public void setValue(String string) {
        this._value = string;
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    public String toString() {
        return this.getName();
    }

    public boolean equals(Object object) {
        if (object instanceof DisplayInfo) {
            return this.equals((DisplayInfo)object);
        }
        return false;
    }

    public boolean equals(DisplayInfo displayInfo) {
        return displayInfo.getName().equals(this.getName());
    }
}

