/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.NotificationType;

public class Notification {
    private NotificationType _type;
    private String _message;

    public Notification(NotificationType notificationType, String string) {
        this._message = string;
        this._type = notificationType;
    }

    public String getMessage() {
        return this._message;
    }

    public NotificationType getType() {
        return this._type;
    }

    public boolean equals(Object object) {
        if (object instanceof Notification) {
            return this.equals((Notification)object);
        }
        return false;
    }

    public boolean equals(Notification notification) {
        if (this.getType() != notification.getType()) {
            return false;
        }
        if (this.getMessage() != null) {
            return this.getMessage().equals(notification.getMessage());
        }
        return this.getMessage() == notification.getMessage();
    }

    public int hashCode() {
        int n = 3;
        n = 53 * n + (this._type != null ? this._type.hashCode() : 0);
        n = 53 * n + (this._message != null ? this._message.hashCode() : 0);
        return n;
    }

    public String toString() {
        return "[" + (Object)((Object)this.getType()) + "] " + this.getMessage();
    }
}

