/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.TransformApplicationDescriptor;
import java.util.Collection;
import java.util.LinkedList;

public class DiscoveryResult {
    private Collection<TransformApplicationDescriptor> _apps;
    private final Collection<Exception> _errors;

    public DiscoveryResult(Collection<TransformApplicationDescriptor> collection) {
        this(collection, new LinkedList<Exception>());
    }

    public DiscoveryResult(Collection<TransformApplicationDescriptor> collection, Collection<Exception> collection2) {
        this._apps = collection;
        this._errors = collection2;
    }

    public Collection<TransformApplicationDescriptor> getApplications() {
        return this._apps;
    }

    public Collection<Exception> getErrors() {
        return this._errors;
    }
}

