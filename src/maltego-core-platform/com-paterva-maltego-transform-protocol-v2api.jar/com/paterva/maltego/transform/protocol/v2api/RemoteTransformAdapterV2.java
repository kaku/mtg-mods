/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.transform.descriptor.CompoundConstraint
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.EntityConstraint
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.transform.descriptor.CompoundConstraint;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.EntityConstraint;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.protocol.v2api.AbstractTransformAdapterV2;
import com.paterva.maltego.transform.protocol.v2api.RemoteTransformRunner;
import com.paterva.maltego.transform.protocol.v2api.TransformRunner;
import com.paterva.maltego.transform.protocol.v2api.TransformTranslator;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInput;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyFactory;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.FastURL;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class RemoteTransformAdapterV2
extends AbstractTransformAdapterV2 {
    @Override
    protected TransformRunner runner(TransformDescriptor transformDescriptor, String string, DataSource dataSource, EntityFactory entityFactory, EntityRegistry entityRegistry) throws TransformRunException {
        if (string == null) {
            throw new IllegalArgumentException("target cannot be null");
        }
        String string2 = TransformTranslator.getV2Name(transformDescriptor.getName());
        Proxy proxy = this.getProxy(string);
        TransformInput transformInput = this.createInput(string, transformDescriptor, dataSource);
        String string3 = null;
        string3 = this.getV2HackForcedTypeName(transformDescriptor, string3);
        return new RemoteTransformRunner(entityFactory, entityRegistry, proxy, string2, transformInput, string3);
    }

    private String getV2HackForcedTypeName(TransformDescriptor transformDescriptor, String string) {
        if (transformDescriptor.getInputConstraint() instanceof CompoundConstraint) {
            Object e;
            CompoundConstraint compoundConstraint = (CompoundConstraint)transformDescriptor.getInputConstraint();
            Iterator iterator = compoundConstraint.iterator();
            if (iterator.hasNext() && (e = iterator.next()) instanceof EntityConstraint) {
                EntityConstraint entityConstraint = (EntityConstraint)e;
                string = entityConstraint.getTypeName();
            }
        } else if (transformDescriptor.getInputConstraint() instanceof EntityConstraint) {
            EntityConstraint entityConstraint = (EntityConstraint)transformDescriptor.getInputConstraint();
            string = entityConstraint.getTypeName();
        }
        return string;
    }

    private TransformInput createInput(String string, TransformDescriptor transformDescriptor, DataSource dataSource) throws TransformRunException {
        try {
            TransformInput transformInput = new TransformInput();
            FastURL fastURL = new FastURL(string);
            fastURL.getURL();
            TransformServerInfo transformServerInfo = TransformServerRegistry.getDefault().get(fastURL);
            if (transformServerInfo == null) {
                throw new TransformRunException("No server found for target " + string);
            }
            transformInput.setKey(transformServerInfo.getAuthentication().getToken(string));
            int n = this.getLimit(dataSource);
            transformInput.setSoftLimit(n);
            transformInput.setHardLimit(n);
            Map<String, Object> map = transformInput.getTransformParameters();
            for (DisplayDescriptor displayDescriptor : transformDescriptor.getProperties()) {
                map.put(displayDescriptor.getName(), dataSource.getValue((PropertyDescriptor)displayDescriptor));
            }
            return transformInput;
        }
        catch (MalformedURLException var4_5) {
            throw new TransformRunException("Invalid URL: " + string);
        }
    }

    private Proxy getProxy(String string) {
        Proxy proxy = ProxyFactory.getDefault().createProxy(string);
        return proxy;
    }

    @Override
    protected boolean applyAdvancedProperties() {
        return true;
    }
}

