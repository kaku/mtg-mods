/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="MaltegoTransformDiscoveryMessage", strict=0)
public class DiscoveryResponse
extends MaltegoMessage {
    private String _serverName = "Unknown";
    @ElementList(name="OtherSeedServers", type=SeedServerDescriptor.class, required=0)
    private ArrayList<SeedServerDescriptor> _seedServers;
    @ElementList(name="TransformApplications", type=TransformApplicationDescriptor.class, required=1)
    private ArrayList<TransformApplicationDescriptor> _applications;

    @Attribute(name="source", required=0)
    public String getServerName() {
        return this._serverName;
    }

    @Attribute(name="source", required=0)
    public void setServerName(String string) {
        this._serverName = string;
    }

    public ArrayList<SeedServerDescriptor> getSeedServers() {
        if (this._seedServers == null) {
            this._seedServers = new ArrayList();
        }
        return this._seedServers;
    }

    public ArrayList<TransformApplicationDescriptor> getTransformApplications() {
        if (this._applications == null) {
            this._applications = new ArrayList();
        }
        return this._applications;
    }

    @Root(name="TransformApplication", strict=0)
    public static class TransformApplicationDescriptor {
        private URL _registrationUrl;
        private String _url;
        private boolean _requiresKey = false;
        private String _name = "Unknown";

        public TransformApplicationDescriptor() {
        }

        public TransformApplicationDescriptor(String string) {
            this.setUrl(string);
        }

        @Attribute(name="registrationURL", required=0)
        public void setRegistrationUrl(URL uRL) {
            this._registrationUrl = uRL;
        }

        @Attribute(name="registrationURL", required=0)
        public URL getRegistrationUrl() {
            return this._registrationUrl;
        }

        @Attribute(name="requireAPIKey", required=0)
        public void setRequiresKey(boolean bl) {
            this._requiresKey = bl;
        }

        @Attribute(name="requireAPIKey", required=0)
        public boolean getRequiresKey() {
            return this._requiresKey;
        }

        @Attribute(name="name", required=0)
        public void setName(String string) {
            this._name = string;
        }

        @Attribute(name="name", required=0)
        public String getName() {
            return this._name;
        }

        @Attribute(name="URL")
        public String getUrl() {
            return this._url;
        }

        @Attribute(name="URL")
        public void setUrl(String string) {
            this._url = string;
        }
    }

    @Root(name="OtherSeedServer", strict=0)
    public static class SeedServerDescriptor {
        private URL _url;

        public SeedServerDescriptor() {
        }

        public SeedServerDescriptor(URL uRL) {
            this.setUrl(uRL);
        }

        public SeedServerDescriptor(String string) throws MalformedURLException {
            this(new URL(string));
        }

        @Attribute(name="URL")
        public URL getUrl() {
            return this._url;
        }

        @Attribute(name="URL")
        public void setUrl(URL uRL) {
            this._url = uRL;
        }
    }

}

