/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import java.util.HashMap;
import java.util.Map;

public class ListTransformsCache {
    private static final Map<String, MaltegoMessageWrapper> _map = new HashMap<String, MaltegoMessageWrapper>();

    public static void clear() {
        _map.clear();
    }

    public static MaltegoMessageWrapper get(String string) {
        return _map.get(string);
    }

    public static void put(String string, MaltegoMessageWrapper maltegoMessageWrapper) {
        _map.put(string, maltegoMessageWrapper);
    }
}

