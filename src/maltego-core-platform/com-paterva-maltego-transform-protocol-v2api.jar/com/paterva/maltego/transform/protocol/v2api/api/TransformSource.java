/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateMismatchException
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformFindException;
import com.paterva.maltego.transform.protocol.v2api.api.TransformListInfo;
import com.paterva.maltego.util.http.CertificateMismatchException;

public interface TransformSource {
    public DiscoveryInfo getDiscoveryInfo(String var1, String var2, boolean var3) throws TransformFindException, CertificateMismatchException;

    public TransformListInfo getTransforms(String var1) throws TransformFindException, CertificateMismatchException;
}

