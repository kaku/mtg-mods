/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryServer;
import com.paterva.maltego.transform.protocol.v2api.api.TransformApplicationDescriptor;
import java.util.ArrayList;
import java.util.Collection;

public class DiscoveryInfo {
    private Collection<TransformApplicationDescriptor> _apps;
    private Collection<DiscoveryServer> _servers;
    private String _serverName;

    public DiscoveryInfo(String string) {
        this._serverName = string;
    }

    public Collection<TransformApplicationDescriptor> getApplications() {
        if (this._apps == null) {
            this._apps = new ArrayList<TransformApplicationDescriptor>();
        }
        return this._apps;
    }

    public Collection<DiscoveryServer> getServers() {
        if (this._servers == null) {
            this._servers = new ArrayList<DiscoveryServer>();
        }
        return this._servers;
    }

    public String getServerName() {
        return this._serverName;
    }
}

