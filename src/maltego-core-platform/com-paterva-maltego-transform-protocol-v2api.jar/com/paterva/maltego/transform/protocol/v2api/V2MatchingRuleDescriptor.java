/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.matching.TypeToPropertiesMapMatchingRuleDescriptor
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.matching.TypeToPropertiesMapMatchingRuleDescriptor;
import com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator;
import com.paterva.maltego.transform.protocol.v2api.EntityTranslator;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalField;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalFieldCollection;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

public class V2MatchingRuleDescriptor
extends TypeToPropertiesMapMatchingRuleDescriptor {
    public void update(Collection<EntityDescriptor> collection, EntityRegistry entityRegistry) {
        TreeSet<String> treeSet = new TreeSet<String>();
        for (EntityDescriptor entityDescriptor : collection) {
            if (treeSet.contains(entityDescriptor.getTypeName())) continue;
            this.update(entityDescriptor, entityRegistry);
            treeSet.add(entityDescriptor.getTypeName());
        }
    }

    private void update(EntityDescriptor entityDescriptor, EntityRegistry entityRegistry) {
        ArrayList<String> arrayList = new ArrayList<String>();
        EntityTranslator entityTranslator = EntityTranslator.instance();
        String string = EntitySpecTranslator.getV3TypeName(entityDescriptor.getTypeName());
        for (AdditionalField additionalField : entityDescriptor.getFields()) {
            if (!"strict".equals(additionalField.getMatchingRule())) continue;
            arrayList.add(entityTranslator.v3PropertyName(entityDescriptor, additionalField.getName(), additionalField.getName(), entityRegistry));
        }
        this.update(string, arrayList, true);
    }

    public boolean equals(Object object) {
        if (object == null || object instanceof V2MatchingRuleDescriptor) {
            return true;
        }
        return false;
    }
}

