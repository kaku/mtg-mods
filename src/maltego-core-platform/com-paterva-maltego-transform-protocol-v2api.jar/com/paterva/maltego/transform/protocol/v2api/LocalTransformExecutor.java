/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.transform.protocol.v2api.DefaultLocalTransformExecutor;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import org.openide.util.Lookup;

public abstract class LocalTransformExecutor {
    private static LocalTransformExecutor _default;

    public static LocalTransformExecutor getDefault() {
        if (_default == null && (LocalTransformExecutor._default = (LocalTransformExecutor)Lookup.getDefault().lookup(LocalTransformExecutor.class)) == null) {
            _default = new DefaultLocalTransformExecutor();
        }
        return _default;
    }

    public abstract String execute(String var1, File var2, Collection<String> var3, boolean var4) throws TransformRunException, InterruptedException, ExecutionException, IOException;
}

