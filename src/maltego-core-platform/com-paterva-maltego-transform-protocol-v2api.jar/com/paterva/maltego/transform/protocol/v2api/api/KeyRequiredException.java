/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.CodedTransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;

public class KeyRequiredException
extends CodedTransformRunException {
    public KeyRequiredException() {
        super(ExceptionResponse.KEY_REQUIRED);
    }

    public KeyRequiredException(String string) {
        super(string, ExceptionResponse.KEY_REQUIRED);
    }
}

