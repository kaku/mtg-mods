/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.transform.protocol.v2api.cert;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;

public class CertificateMismatchDisplayer {
    public static TrustResult showTrustPrompt(JPanel jPanel) {
        String string = "Warning - Certificate Changed";
        final JButton jButton = new JButton("Trust");
        final JButton jButton2 = new JButton("Don't Trust");
        JButton jButton3 = new JButton("Cancel");
        Object[] arrobject = new Object[]{jButton, jButton2, jButton3};
        final AtomicReference atomicReference = new AtomicReference();
        final AtomicReference<Dialog> atomicReference2 = new AtomicReference<Dialog>();
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getSource() == jButton) {
                    atomicReference.set(TrustResult.TRUST);
                } else if (actionEvent.getSource() == jButton2) {
                    atomicReference.set(TrustResult.DONT_TRUST);
                } else {
                    atomicReference.set(TrustResult.CANCEL);
                }
                Dialog dialog = (Dialog)atomicReference2.get();
                dialog.setVisible(false);
                dialog.dispose();
            }
        };
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)jPanel, string, true, arrobject, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, actionListener);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        atomicReference2.set(dialog);
        dialog.setVisible(true);
        TrustResult trustResult = (TrustResult)((Object)atomicReference.get());
        return trustResult != null ? trustResult : TrustResult.CANCEL;
    }

    public static enum TrustResult {
        TRUST,
        DONT_TRUST,
        CANCEL;
        

        private TrustResult() {
        }
    }

}

