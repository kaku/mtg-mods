/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import org.openide.util.Lookup;

public abstract class ProxyFactory {
    public static ProxyFactory getDefault() {
        ProxyFactory proxyFactory = (ProxyFactory)Lookup.getDefault().lookup(ProxyFactory.class);
        if (proxyFactory == null) {
            return new EmptyProxyFactory();
        }
        return proxyFactory;
    }

    public abstract Proxy createProxy(String var1);

    private static class EmptyProxyFactory
    extends ProxyFactory {
        private EmptyProxyFactory() {
        }

        @Override
        public Proxy createProxy(String string) {
            throw new UnsupportedOperationException("No Proxy Factory registered.");
        }
    }

}

