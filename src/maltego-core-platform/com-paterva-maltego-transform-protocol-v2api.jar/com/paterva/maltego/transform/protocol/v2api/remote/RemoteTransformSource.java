/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateMismatchException
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryInfo;
import com.paterva.maltego.transform.protocol.v2api.api.OAuthAuthenticatorInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformFindException;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformListInfo;
import com.paterva.maltego.transform.protocol.v2api.messaging.DiscoveryResponse;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;
import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyException;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyFactory;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListResponse;
import com.paterva.maltego.transform.protocol.v2api.remote.AuthenticatorTranslator;
import com.paterva.maltego.transform.protocol.v2api.remote.DiscoveryInfoTranslator;
import com.paterva.maltego.transform.protocol.v2api.remote.MacroExpander;
import com.paterva.maltego.transform.protocol.v2api.remote.RemoteTransformFindException;
import com.paterva.maltego.transform.protocol.v2api.remote.RemoteTransformTranslator;
import com.paterva.maltego.util.http.CertificateMismatchException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RemoteTransformSource {
    public DiscoveryInfo getDiscoveryInfo(String string, String string2, boolean bl) throws TransformFindException, CertificateMismatchException {
        try {
            Proxy proxy = this.getProxy(string2);
            DiscoveryInfoTranslator discoveryInfoTranslator = new DiscoveryInfoTranslator(string);
            MaltegoMessageWrapper maltegoMessageWrapper = proxy.listTransforms(bl);
            DiscoveryResponse discoveryResponse = maltegoMessageWrapper.getDiscoveryResponse();
            if (discoveryResponse == null) {
                ExceptionResponse exceptionResponse = maltegoMessageWrapper.getExceptionResponse();
                if (exceptionResponse == null) {
                    throw new RemoteTransformFindException("NULL response received");
                }
                throw new RemoteTransformFindException(exceptionResponse);
            }
            this.replaceMacros(discoveryResponse, string2);
            try {
                return discoveryInfoTranslator.translate(discoveryResponse);
            }
            catch (Exception var8_10) {
                throw new TransformFindException("Invalid message format: " + var8_10.getMessage(), var8_10);
            }
        }
        catch (ProxyException var4_5) {
            throw new TransformFindException("Error during remote execution: " + var4_5.getMessage(), var4_5);
        }
    }

    public TransformListInfo getTransforms(String string) throws TransformFindException, CertificateMismatchException {
        try {
            Proxy proxy = this.getProxy(string);
            MaltegoMessageWrapper maltegoMessageWrapper = proxy.listTransforms(true);
            TransformListResponse transformListResponse = maltegoMessageWrapper.getListResponse();
            if (transformListResponse == null) {
                ExceptionResponse exceptionResponse = maltegoMessageWrapper.getExceptionResponse();
                if (exceptionResponse == null) {
                    throw new RemoteTransformFindException("NULL response received");
                }
                throw new RemoteTransformFindException(exceptionResponse);
            }
            try {
                TransformListInfo transformListInfo = new TransformListInfo();
                AuthenticatorTranslator authenticatorTranslator = new AuthenticatorTranslator();
                transformListInfo.getOAuthAuthenticators().addAll(authenticatorTranslator.translateOAuth(transformListResponse.getAuthenticators()));
                RemoteTransformTranslator remoteTransformTranslator = new RemoteTransformTranslator();
                transformListInfo.getTransforms().addAll(remoteTransformTranslator.translate(transformListResponse.getTransforms()));
                return transformListInfo;
            }
            catch (Exception var5_8) {
                var5_8.printStackTrace();
                throw new TransformFindException("Invalid message format", var5_8);
            }
        }
        catch (ProxyException var2_3) {
            throw new TransformFindException("Error during remote execution", var2_3);
        }
    }

    protected Proxy getProxy(String string) {
        return ProxyFactory.getDefault().createProxy(string);
    }

    private void replaceMacros(DiscoveryResponse discoveryResponse, String string) {
        MacroExpander macroExpander = new MacroExpander();
        try {
            URL uRL = new URL(string);
            macroExpander.add("${this}", uRL.getHost());
            macroExpander.expand(discoveryResponse);
        }
        catch (MalformedURLException var5_5) {
            // empty catch block
        }
    }
}

