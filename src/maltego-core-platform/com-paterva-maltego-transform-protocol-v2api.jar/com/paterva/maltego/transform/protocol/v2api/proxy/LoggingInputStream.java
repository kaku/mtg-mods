/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public class LoggingInputStream
extends InputStream {
    private InputStream _is;
    private ByteArrayOutputStream _logBuffer;

    public LoggingInputStream(InputStream inputStream) {
        this._is = inputStream;
        this._logBuffer = new ByteArrayOutputStream();
    }

    @Override
    public int read() throws IOException {
        int n = this._is.read();
        this._logBuffer.write(n);
        return n;
    }

    @Override
    public void close() throws IOException {
        System.out.println("Input");
        System.out.println("==============================");
        System.out.println(this._logBuffer.toString("UTF-8"));
        this._logBuffer.close();
        super.close();
    }
}

