/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.matching.MatchingRule
 *  com.paterva.maltego.matching.MatchingRuleFactory
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.InheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.adapter.TransformAdapter
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCallback
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 *  com.paterva.maltego.transform.runner.api.impl.TransformMergeSettings
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.types.DateTime
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.adapter.TransformAdapter;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.protocol.v2api.TransformRunner;
import com.paterva.maltego.transform.protocol.v2api.V2EntityMerger;
import com.paterva.maltego.transform.protocol.v2api.V2MatchingRuleDescriptor;
import com.paterva.maltego.transform.protocol.v2api.api.CodedTransformRunException;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import com.paterva.maltego.transform.runner.api.impl.TransformMergeSettings;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.types.DateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public abstract class AbstractTransformAdapterV2
implements TransformAdapter {
    private static final Logger LOG = Logger.getLogger(AbstractTransformAdapterV2.class.getName());
    private static final PropertyDescriptor SLIDER = new PropertyDescriptor(Integer.class, "maltego.global.slider");
    private boolean _cancelled = false;
    private ResultHandler _handler;
    private int _limit;
    private TransformCompleteNotifier _completeNotifier;

    protected abstract boolean applyAdvancedProperties();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void run(GraphID graphID, Map<EntityID, MaltegoEntity> map, TransformDescriptor transformDescriptor, String string, DataSource dataSource, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory, TransformCallback transformCallback) {
        block12 : {
            try {
                TransformRunner transformRunner = this.runner(transformDescriptor, string, dataSource, entityFactory, entityRegistry);
                this._limit = this.getLimit(dataSource);
                this._handler = new ResultHandler(transformDescriptor, map.size(), entityRegistry, linkFactory, transformCallback);
                this._completeNotifier = new TransformCompleteNotifier(transformDescriptor.getDisplayName(), map, transformCallback);
                String string2 = "Running transform " + transformDescriptor.getDisplayName() + " on " + map.size() + " entities.";
                LOG.fine(string2);
                transformCallback.resultReceived(TransformResult.info((String)string2, (boolean)true), map);
                if (map.isEmpty()) {
                    transformCallback.resultReceived(TransformResult.empty(), map);
                    break block12;
                }
                for (Map.Entry<EntityID, MaltegoEntity> object2 : map.entrySet()) {
                    MaltegoEntity maltegoEntity = object2.getValue();
                    this._handler.add(transformRunner.run(maltegoEntity, this._handler));
                }
                boolean bl = false;
                ResultHandler resultHandler = this._handler;
                synchronized (resultHandler) {
                    try {
                        if (Thread.interrupted()) {
                            bl = true;
                        } else {
                            this._handler.wait();
                        }
                    }
                    catch (InterruptedException var14_18) {
                        bl = true;
                    }
                }
                if (bl) {
                    transformCallback.cancelled();
                }
            }
            catch (Exception var10_11) {
                transformCallback.resultReceived(TransformResult.error((String)var10_11.getMessage()), map);
            }
        }
    }

    protected abstract TransformRunner runner(TransformDescriptor var1, String var2, DataSource var3, EntityFactory var4, EntityRegistry var5) throws TransformRunException;

    public void cancel() {
        this._cancelled = true;
        if (this._handler != null) {
            this._handler.cancel();
        }
    }

    protected int getLimit(DataSource dataSource) {
        Integer n = (Integer)dataSource.getValue(SLIDER);
        return n != null ? n : 50;
    }

    private static enum LinkDirection {
        INPUT_TO_OUTPUT("input-to-output"),
        OUTPUT_TO_INPUT("output-to-input"),
        BIDIRECTIONAL("bidirectional");
        
        private final String _value;

        private LinkDirection(String string2) {
            this._value = string2;
        }

        public String getValue() {
            return this._value;
        }

        public static LinkDirection parse(String string) {
            for (LinkDirection linkDirection : LinkDirection.values()) {
                if (!linkDirection.getValue().equalsIgnoreCase(string)) continue;
                return linkDirection;
            }
            return null;
        }
    }

    private static class RegistryInheritedTypesProvider
    implements InheritedTypesProvider {
        private final EntityRegistry _registry;

        public RegistryInheritedTypesProvider(EntityRegistry entityRegistry) {
            this._registry = entityRegistry;
        }

        public List<String> getAllInheritedTypes(String string) {
            return InheritanceHelper.getInheritanceList((SpecRegistry)this._registry, (String)string);
        }
    }

    private class ResultHandler
    implements TransformRunner.Callback {
        private final TransformCallback _cb;
        private final int _total;
        private int _complete;
        private final TransformDescriptor _transform;
        private final List<Cancellable> _cancellables;
        private final EntityRegistry _registry;
        private final LinkFactory _linkFactory;
        private String _lastError;
        private final Map<EntityID, MaltegoEntity> _inputEntitiesAll;

        public ResultHandler(TransformDescriptor transformDescriptor, int n, EntityRegistry entityRegistry, LinkFactory linkFactory, TransformCallback transformCallback) {
            this._cancellables = new ArrayList<Cancellable>();
            this._inputEntitiesAll = new HashMap<EntityID, MaltegoEntity>();
            this._transform = transformDescriptor;
            this._cb = transformCallback;
            this._total = n;
            this._complete = 0;
            this._registry = entityRegistry;
            this._linkFactory = linkFactory;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void exception(Exception exception, MaltegoEntity maltegoEntity) {
            ResultHandler resultHandler = this;
            synchronized (resultHandler) {
                HashMap<EntityID, MaltegoEntity> hashMap = new HashMap<EntityID, MaltegoEntity>();
                hashMap.put((EntityID)maltegoEntity.getID(), maltegoEntity);
                String string = exception.getMessage();
                if (!Utilities.compareObjects((Object)string, (Object)this._lastError)) {
                    if (exception instanceof CodedTransformRunException) {
                        this._cb.resultReceived(TransformResult.nonInterruptingError((String)string, (int)((CodedTransformRunException)exception).getCode()), hashMap);
                    } else {
                        this._cb.resultReceived(TransformResult.nonInterruptingError((String)string), hashMap);
                    }
                }
                this._lastError = string;
                String string2 = "Transform " + this._transform.getDisplayName() + " returned with an error: " + string;
                LOG.fine(string2);
                this._cb.resultReceived(TransformResult.warning((String)string2), hashMap);
                ++this._complete;
                this.checkFinished(hashMap);
                AbstractTransformAdapterV2.this._completeNotifier.transformComplete();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void success(TransformRunner.RunResult runResult) {
            ResultHandler resultHandler = this;
            synchronized (resultHandler) {
                Object object;
                Object object2;
                MaltegoEntity maltegoEntity = runResult.getInput();
                EntityID entityID = (EntityID)maltegoEntity.getID();
                if (AbstractTransformAdapterV2.this._cancelled) {
                    this.notify();
                    return;
                }
                Object object3 = this._transform;
                synchronized (object3) {
                    object2 = this._transform.getMatchingRule();
                    if (object2 instanceof V2MatchingRuleDescriptor) {
                        object = (V2MatchingRuleDescriptor)((Object)object2);
                    } else {
                        object = new V2MatchingRuleDescriptor();
                        this._transform.setMatchingRule((MatchingRuleDescriptor)object);
                    }
                    object.update(runResult.getV2Entities(), this._registry);
                    object2 = object;
                    this._inputEntitiesAll.put(entityID, maltegoEntity);
                }
                object3 = new HashMap();
                object3.put(entityID, maltegoEntity);
                if (runResult.getMessages().size() > 0) {
                    this._cb.resultReceived(TransformResult.messages(runResult.getMessages()), (Map)object3);
                }
                object = this.getMerged(runResult.getEntities(), (MatchingRuleDescriptor)object2);
                this.convertNonProperties(object.keySet());
                Map<MaltegoEntity, DataSources.Map> map = this.extractLinkProperties(object.keySet());
                GraphID graphID = this.buildResultGraph(maltegoEntity.createClone(), map, (Map<MaltegoEntity, List<MaltegoEntity>>)object);
                int n = runResult.getEntities().size();
                String string = "Transform " + this._transform.getDisplayName() + " returned with " + n + " entities.";
                LOG.fine(string);
                this._cb.resultReceived(TransformResult.info((String)string, (boolean)false), (Map)object3);
                this._cb.resultReceived(TransformResult.progress((String)string, (int)this.getProgressPercentage(), (GraphID)graphID, (int)n, (boolean)false, (EntityID)entityID, (TransformCompleteNotifier)AbstractTransformAdapterV2.this._completeNotifier), (Map)object3);
                ++this._complete;
                this.checkFinished(this._inputEntitiesAll);
            }
        }

        private int getProgressPercentage() {
            return Math.min(100 * this._complete / this._total, 100);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void checkFinished(Map<EntityID, MaltegoEntity> map) {
            ResultHandler resultHandler = this;
            synchronized (resultHandler) {
                if (this._complete == this._total) {
                    String string = "Transform " + this._transform.getDisplayName() + " done";
                    LOG.fine(string);
                    this._cb.resultReceived(TransformResult.info((String)string, (boolean)false), map);
                    this.notify();
                }
            }
        }

        private void convertNonProperties(Iterable<MaltegoEntity> iterable) {
            for (MaltegoEntity maltegoEntity : iterable) {
                PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
                ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
                for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                    Object object;
                    String string = propertyDescriptor.getName();
                    if ("notes#".equals(string)) {
                        if (AbstractTransformAdapterV2.this.applyAdvancedProperties() && (object = maltegoEntity.getValue(propertyDescriptor)) != null) {
                            maltegoEntity.setNotes(object.toString());
                            maltegoEntity.setShowNotes(Boolean.valueOf(true));
                        }
                        arrayList.add(propertyDescriptor);
                    }
                    if (!"bookmark#".equals(string)) continue;
                    if (AbstractTransformAdapterV2.this.applyAdvancedProperties()) {
                        object = maltegoEntity.getValue(propertyDescriptor);
                        Integer n = null;
                        if (object instanceof Integer) {
                            n = (Integer)object;
                        } else if (object instanceof String) {
                            try {
                                n = Integer.decode((String)object);
                            }
                            catch (NumberFormatException var11_11) {
                                Exceptions.printStackTrace((Throwable)var11_11);
                            }
                        }
                        if (n != null) {
                            if (n == -1) {
                                n = null;
                            }
                            maltegoEntity.setBookmark(n);
                        }
                    }
                    arrayList.add(propertyDescriptor);
                }
                propertyDescriptorCollection.removeAll(arrayList);
            }
        }

        private Map<MaltegoEntity, DataSources.Map> extractLinkProperties(Iterable<MaltegoEntity> iterable) {
            HashMap<MaltegoEntity, DataSources.Map> hashMap = new HashMap<MaltegoEntity, DataSources.Map>();
            for (MaltegoEntity maltegoEntity : iterable) {
                DataSources.Map map = new DataSources.Map();
                PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
                ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
                for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                    String string = propertyDescriptor.getName();
                    String string2 = "link#";
                    if (!string.startsWith("link#") || string.length() <= "link#".length()) continue;
                    if (AbstractTransformAdapterV2.this.applyAdvancedProperties()) {
                        string = string.substring("link#".length());
                        PropertyDescriptor propertyDescriptor2 = new PropertyDescriptor(propertyDescriptor.getType(), string, propertyDescriptor.getDisplayName());
                        propertyDescriptor2.setHidden(propertyDescriptor.isHidden());
                        propertyDescriptor2.setNullable(propertyDescriptor.isNullable());
                        propertyDescriptor2.setReadonly(propertyDescriptor.isReadonly());
                        map.put((Object)propertyDescriptor2, maltegoEntity.getValue(propertyDescriptor));
                    }
                    arrayList.add(propertyDescriptor);
                }
                propertyDescriptorCollection.removeAll(arrayList);
                hashMap.put(maltegoEntity, map);
            }
            return hashMap;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private GraphID buildResultGraph(MaltegoEntity maltegoEntity, Map<MaltegoEntity, DataSources.Map> map, Map<MaltegoEntity, List<MaltegoEntity>> map2) {
            GraphID graphID;
            int n = 0;
            graphID = GraphID.create();
            GraphStore graphStore = null;
            try {
                EntityRegistry.associate((GraphID)graphID, (EntityRegistry)EntityRegistry.getDefault());
                LinkRegistry.associate((GraphID)graphID, (LinkRegistry)LinkRegistry.getDefault());
                graphStore = GraphStoreFactory.getDefault().create(graphID, true);
                graphStore.beginUpdate();
                if (!map.isEmpty()) {
                    GraphStoreWriter.addEntity((GraphID)graphID, (MaltegoEntity)maltegoEntity);
                }
                for (Map.Entry<MaltegoEntity, DataSources.Map> entry : map.entrySet()) {
                    PropertyDescriptor propertyDescriptor;
                    MaltegoEntity maltegoEntity2 = entry.getKey();
                    DataSources.Map map3 = entry.getValue();
                    GraphStoreWriter.addEntity((GraphID)graphID, (MaltegoEntity)maltegoEntity2);
                    MaltegoLink maltegoLink = this._linkFactory.createInstance(MaltegoLinkSpec.getTransformSpec(), true);
                    LinkDirection linkDirection = LinkDirection.INPUT_TO_OUTPUT;
                    for (reference var14_1522 : map3.entrySet()) {
                        propertyDescriptor = (PropertyDescriptor)var14_1522.getKey();
                        Object v = var14_1522.getValue();
                        if ("maltego.link.direction".equalsIgnoreCase(propertyDescriptor.getName())) {
                            if (!(v instanceof String)) continue;
                            linkDirection = LinkDirection.parse((String)v);
                            continue;
                        }
                        maltegoLink.setValue(propertyDescriptor, v);
                    }
                    PropertyDescriptorCollection propertyDescriptorCollection = maltegoLink.getProperties();
                    maltegoLink.setValue(propertyDescriptorCollection.get("maltego.link.transform.name"), (Object)this._transform.getName());
                    maltegoLink.setValue(propertyDescriptorCollection.get("maltego.link.transform.display-name"), (Object)this._transform.getDisplayName());
                    maltegoLink.setValue(propertyDescriptorCollection.get("maltego.link.transform.version"), (Object)this._transform.getVersion());
                    maltegoLink.setValue(propertyDescriptorCollection.get("maltego.link.transform.run-date"), (Object)new DateTime());
                    for (reference var14_1522 = (reference)false ? 1 : 0; var14_1522 < this.getLinkCount(map2.get((Object)maltegoEntity2)); ++var14_1522) {
                        if (LinkDirection.INPUT_TO_OUTPUT.equals((Object)linkDirection)) {
                            GraphStoreWriter.addLink((GraphID)graphID, (MaltegoLink)maltegoLink.createCopy(), (LinkEntityIDs)new LinkEntityIDs((EntityID)maltegoEntity.getID(), (EntityID)maltegoEntity2.getID()));
                            continue;
                        }
                        if (LinkDirection.OUTPUT_TO_INPUT.equals((Object)linkDirection)) {
                            propertyDescriptor = maltegoLink.createCopy();
                            propertyDescriptor.setReversed(Boolean.valueOf(true));
                            GraphStoreWriter.addLink((GraphID)graphID, (MaltegoLink)propertyDescriptor, (LinkEntityIDs)new LinkEntityIDs((EntityID)maltegoEntity2.getID(), (EntityID)maltegoEntity.getID()));
                            continue;
                        }
                        GraphStoreWriter.addLink((GraphID)graphID, (MaltegoLink)maltegoLink.createCopy(), (LinkEntityIDs)new LinkEntityIDs((EntityID)maltegoEntity.getID(), (EntityID)maltegoEntity2.getID()));
                        propertyDescriptor = maltegoLink.createCopy();
                        propertyDescriptor.setReversed(Boolean.valueOf(true));
                        GraphStoreWriter.addLink((GraphID)graphID, (MaltegoLink)propertyDescriptor, (LinkEntityIDs)new LinkEntityIDs((EntityID)maltegoEntity2.getID(), (EntityID)maltegoEntity.getID()));
                    }
                    if (++n != AbstractTransformAdapterV2.this._limit) continue;
                    break;
                }
            }
            catch (GraphStoreException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
            finally {
                if (graphStore != null) {
                    graphStore.endUpdate((Object)null);
                }
            }
            return graphID;
        }

        private int getLinkCount(List<MaltegoEntity> list) {
            return TransformMergeSettings.isMergeLinks() ? 1 : list.size();
        }

        private Map<MaltegoEntity, List<MaltegoEntity>> getMerged(Iterable<MaltegoEntity> iterable, MatchingRuleDescriptor matchingRuleDescriptor) {
            MatchingRule matchingRule = MatchingRuleFactory.createFrom((MatchingRuleDescriptor)matchingRuleDescriptor);
            HashMap<MaltegoEntity, List<MaltegoEntity>> hashMap = new HashMap<MaltegoEntity, List<MaltegoEntity>>();
            for (MaltegoEntity maltegoEntity : iterable) {
                MaltegoEntity maltegoEntity2 = this.find(hashMap.keySet(), maltegoEntity, matchingRule);
                if (maltegoEntity2 == null) {
                    ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
                    arrayList.add(maltegoEntity);
                    hashMap.put(maltegoEntity, arrayList);
                    continue;
                }
                V2EntityMerger.merge(maltegoEntity, maltegoEntity2);
                hashMap.get((Object)maltegoEntity2).add(maltegoEntity);
            }
            return hashMap;
        }

        private MaltegoEntity find(Iterable<MaltegoEntity> iterable, MaltegoEntity maltegoEntity, MatchingRule matchingRule) {
            for (MaltegoEntity maltegoEntity2 : iterable) {
                int n;
                if (matchingRule == null) {
                    matchingRule = MatchingRule.Default;
                }
                if ((n = matchingRule.match((SpecRegistry)this._registry, (TypedPropertyBag)maltegoEntity2, (TypedPropertyBag)maltegoEntity)) != 1) continue;
                return maltegoEntity2;
            }
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void add(Cancellable cancellable) {
            List<Cancellable> list = this._cancellables;
            synchronized (list) {
                this._cancellables.add(cancellable);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public synchronized void cancel() {
            List<Cancellable> list = this._cancellables;
            synchronized (list) {
                for (Cancellable cancellable : this._cancellables) {
                    cancellable.cancel();
                }
            }
        }
    }

}

