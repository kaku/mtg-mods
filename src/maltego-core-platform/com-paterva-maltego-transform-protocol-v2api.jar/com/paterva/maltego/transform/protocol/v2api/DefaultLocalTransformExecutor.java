/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.transform.protocol.v2api.LocalTransformExecutor;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.util.StringUtilities;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DefaultLocalTransformExecutor
extends LocalTransformExecutor {
    @Override
    public String execute(String string, File file, Collection<String> collection, boolean bl) throws TransformRunException, InterruptedException, ExecutionException, IOException {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add(string);
        arrayList.addAll(collection);
        ProcessBuilder processBuilder = new ProcessBuilder(arrayList).directory(file);
        processBuilder.redirectErrorStream(true);
        Process process = processBuilder.start();
        InputStream inputStream = process.getInputStream();
        return StringUtilities.toString((InputStream)inputStream, (String)"UTF-8");
    }
}

