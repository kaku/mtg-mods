/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="MaltegoTransformResponseMessage", strict=0)
public class TransformResponse {
    @ElementList(name="Entities", type=EntityDescriptor.class)
    private Collection<EntityDescriptor> _entities;
    @ElementList(name="UIMessages", type=Notification.class, required=0)
    private Collection<Notification> _messages;

    public Collection<EntityDescriptor> getEntities() {
        if (this._entities == null) {
            this._entities = new ArrayList<EntityDescriptor>();
        }
        return this._entities;
    }

    public Collection<Notification> getMessages() {
        if (this._messages == null) {
            this._messages = new ArrayList<Notification>();
        }
        return this._messages;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (EntityDescriptor entityDescriptor : this.getEntities()) {
            stringBuffer.append(entityDescriptor.toString());
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    @Root(name="UIMessage", strict=0)
    public static class Notification {
        private String _type;
        private String _value;

        @Text
        public String getValue() {
            return this._value;
        }

        @Text
        public void setValue(String string) {
            this._value = string;
        }

        @Attribute(name="MessageType")
        public String getType() {
            return this._type;
        }

        @Attribute(name="MessageType")
        public void setType(String string) {
            this._type = string;
        }
    }

}

