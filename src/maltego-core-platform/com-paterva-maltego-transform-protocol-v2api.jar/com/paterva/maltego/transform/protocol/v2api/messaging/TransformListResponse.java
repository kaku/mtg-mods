/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListInputDescriptor;
import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="MaltegoTransformListMessage", strict=0)
public class TransformListResponse {
    @ElementList(name="Transforms", type=TransformDescriptor.class, required=0)
    private ArrayList<TransformDescriptor> _transforms;
    @Element(name="Authenticators", required=0)
    private AuthenticatorsDescriptor _authenticators;

    public ArrayList<TransformDescriptor> getTransforms() {
        if (this._transforms == null) {
            this._transforms = new ArrayList();
        }
        return this._transforms;
    }

    public AuthenticatorsDescriptor getAuthenticators() {
        return this._authenticators;
    }

    public void setAuthenticators(AuthenticatorsDescriptor authenticatorsDescriptor) {
        this._authenticators = authenticatorsDescriptor;
    }

    @Root(name="OAuthAuthenticator", strict=0)
    public static class OAuthDescriptor {
        @Attribute(name="Name")
        private String _name;
        @Attribute(name="DisplayName")
        private String _displayName;
        @Element(name="Description", required=0)
        private String _description;
        @Element(name="OAuthVersion")
        private String _oauthVersion;
        @Element(name="CallbackPort", required=0)
        private Integer _callbackPort;
        @Element(name="AccessTokenEndpoint")
        private String _accessTokenEndpoint;
        @Element(name="RequestTokenEndpoint", required=0)
        private String _requestTokenEndpoint;
        @Element(name="AuthorizationUrl")
        private String _authorizationUrl;
        @Element(name="AppKey")
        private String _appKey;
        @Element(name="AppSecret")
        private String _appSecret;
        @Element(name="Icon")
        private String _icon;
        @Element(name="AccessTokenInput")
        private String _accessTokenInput;
        @Element(name="AccessTokenPublicKey")
        private String _accessTokenPublicKey;

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }

        public String getDisplayName() {
            return this._displayName;
        }

        public void setDisplayName(String string) {
            this._displayName = string;
        }

        public String getDescription() {
            return this._description;
        }

        public void setDescription(String string) {
            this._description = string;
        }

        public String getOAuthVersion() {
            return this._oauthVersion;
        }

        public void setOAuthVersion(String string) {
            this._oauthVersion = string;
        }

        public Integer getCallbackPort() {
            return this._callbackPort;
        }

        public void setCallbackPort(Integer n) {
            this._callbackPort = n;
        }

        public String getAccessTokenInput() {
            return this._accessTokenInput;
        }

        public void setAccessTokenInput(String string) {
            this._accessTokenInput = string;
        }

        public String getAccessTokenEndpoint() {
            return this._accessTokenEndpoint;
        }

        public void setAccessTokenEndpoint(String string) {
            this._accessTokenEndpoint = string;
        }

        public String getRequestTokenEndpoint() {
            return this._requestTokenEndpoint;
        }

        public void setRequestTokenEndpoint(String string) {
            this._requestTokenEndpoint = string;
        }

        public String getAuthorizationUrl() {
            return this._authorizationUrl;
        }

        public void setAuthorizationUrl(String string) {
            this._authorizationUrl = string;
        }

        public String getAppKey() {
            return this._appKey;
        }

        public void setAppKey(String string) {
            this._appKey = string;
        }

        public String getAppSecret() {
            return this._appSecret;
        }

        public void setAppSecret(String string) {
            this._appSecret = string;
        }

        public String getIcon() {
            return this._icon;
        }

        public void setIcon(String string) {
            this._icon = string;
        }

        public String getAccessTokenPublicKey() {
            return this._accessTokenPublicKey;
        }

        public void setAccessTokenPublicKey(String string) {
            this._accessTokenPublicKey = string;
        }
    }

    public static class AuthenticatorsDescriptor {
        @ElementList(name="OAuthAuthenticators", type=OAuthDescriptor.class, required=0)
        private ArrayList<OAuthDescriptor> _oauthAuthenticators;

        public ArrayList<OAuthDescriptor> getOAuthAuthenticators() {
            if (this._oauthAuthenticators == null) {
                this._oauthAuthenticators = new ArrayList();
            }
            return this._oauthAuthenticators;
        }
    }

    @Root(name="OutputEntity", strict=0)
    public static class TransformOutputEntityDescriptor {
        @Text
        public String Name;

        public TransformOutputEntityDescriptor() {
        }

        public TransformOutputEntityDescriptor(String string) {
            this.Name = string;
        }
    }

    @Root(name="Transform", strict=0)
    public static class TransformDescriptor {
        private String _name;
        private String _displayName;
        private String _description;
        private String _author;
        private String _owner;
        private String _disclaimer;
        private String _locationRelevance;
        private String _version;
        private String _inputEntity;
        private String _minimumClientVersion;
        private String _authenticator;
        private int _maxInputs;
        private int _maxOutputs;
        @ElementList(name="OutputEntities", type=TransformOutputEntityDescriptor.class, required=1)
        private ArrayList<TransformOutputEntityDescriptor> _outputEntities;
        @ElementList(name="UIInputRequirements", type=TransformListInputDescriptor.class, required=0)
        private ArrayList<TransformListInputDescriptor> _inputs;

        public TransformDescriptor() {
        }

        public TransformDescriptor(String string) {
            this._name = string;
        }

        public ArrayList<TransformListInputDescriptor> getInputs() {
            if (this._inputs == null) {
                this._inputs = new ArrayList();
            }
            return this._inputs;
        }

        public ArrayList<TransformOutputEntityDescriptor> getOuputEntities() {
            if (this._outputEntities == null) {
                this._outputEntities = new ArrayList();
            }
            return this._outputEntities;
        }

        @Attribute(name="TransformName")
        public String getName() {
            return this._name;
        }

        @Attribute(name="TransformName")
        public void setName(String string) {
            this._name = string;
        }

        @Attribute(name="UIDisplayName")
        public String getDisplayName() {
            return this._displayName;
        }

        @Attribute(name="UIDisplayName")
        public void setDisplayName(String string) {
            this._displayName = string;
        }

        @Attribute(name="Description", required=0)
        public String getDescription() {
            return this._description;
        }

        @Attribute(name="Description", required=0)
        public void setDescription(String string) {
            this._description = string;
        }

        @Attribute(name="Author")
        public String getAuthor() {
            return this._author;
        }

        @Attribute(name="Author")
        public void setAuthor(String string) {
            this._author = string;
        }

        @Attribute(name="Owner", required=0)
        public String getOwner() {
            return this._owner;
        }

        @Attribute(name="Owner", required=0)
        public void setOwner(String string) {
            this._owner = string;
        }

        @Attribute(name="Disclaimer", required=0)
        public String getDisclaimer() {
            return this._disclaimer;
        }

        @Attribute(name="Disclaimer", required=0)
        public void setDisclaimer(String string) {
            this._disclaimer = string;
        }

        @Attribute(name="LocationRelevance", required=0)
        public String getLocationRelevance() {
            return this._locationRelevance;
        }

        @Attribute(name="LocationRelevance", required=0)
        public void setLocationRelevance(String string) {
            this._locationRelevance = string;
        }

        @Attribute(name="Version")
        public String getVersion() {
            return this._version;
        }

        @Attribute(name="Version")
        public void setVersion(String string) {
            this._version = string;
        }

        @Element(name="InputEntity")
        public String getInputEntity() {
            return this._inputEntity;
        }

        @Element(name="InputEntity")
        public void setInputEntity(String string) {
            this._inputEntity = string;
        }

        @Attribute(name="MaxEntityInputCount", required=0)
        public int getMaxInputs() {
            return this._maxInputs;
        }

        @Attribute(name="MaxEntityInputCount", required=0)
        public void setMaxInputs(int n) {
            this._maxInputs = n;
        }

        @Attribute(name="MaxEntityOutputCount", required=0)
        public int getMaxOutputs() {
            return this._maxOutputs;
        }

        @Attribute(name="MaxEntityOutputCount", required=0)
        public void setMaxOutputs(int n) {
            this._maxOutputs = n;
        }

        @Attribute(name="Authenticator", required=0)
        public String getAuthenticator() {
            return this._authenticator;
        }

        @Attribute(name="Authenticator", required=0)
        public void setAuthenticator(String string) {
            this._authenticator = string;
        }

        public String getMinimumClientVersion() {
            return this._minimumClientVersion;
        }

        public void setMinimumClientVersion(String string) {
            this._minimumClientVersion = string;
        }
    }

}

