/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.CodedTransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;

public class InvalidKeyException
extends CodedTransformRunException {
    public InvalidKeyException() {
        super(ExceptionResponse.INVALID_KEY);
    }

    public InvalidKeyException(String string) {
        super(string, ExceptionResponse.INVALID_KEY);
    }
}

