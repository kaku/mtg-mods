/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api;

public class MaltegoHelper {
    public static String getUserAgent() {
        return String.format("Maltego GUI v%s %s ; %s %s", System.getProperty("maltego.fullversion"), MaltegoHelper.getProductTypeName(), System.getProperty("os.name"), System.getProperty("os.version"));
    }

    public static String getProductTypeName() {
        return "CE";
    }

    public static String getOS() {
        return System.getProperty("os.name") + ";" + System.getProperty("os.version");
    }

    public static String getPlatform() {
        String string = System.getProperty("os.name");
        if (string != null && string.toLowerCase().startsWith("windows")) {
            return "windows";
        }
        return "unix";
    }

    public static String getCountry() {
        String string = System.getProperty("user.country");
        if (string == null) {
            return "";
        }
        return string;
    }

    public static String getOSName() {
        String string = System.getProperty("os.name");
        if (string == null) {
            return "";
        }
        return string;
    }

    public static String getOSVersion() {
        String string = System.getProperty("os.version");
        if (string == null) {
            return "";
        }
        return string;
    }

    public static String getOSPatchLevel() {
        String string = System.getProperty("sun.os.patch.level");
        if (string == null) {
            return "";
        }
        return string;
    }
}

