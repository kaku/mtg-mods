/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.transform.protocol.v2api.api.OAuthAuthenticatorInfo;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListResponse;
import java.util.ArrayList;
import java.util.Collection;

public class AuthenticatorTranslator {
    public Collection<OAuthAuthenticatorInfo> translateOAuth(TransformListResponse.AuthenticatorsDescriptor authenticatorsDescriptor) {
        ArrayList<OAuthAuthenticatorInfo> arrayList = new ArrayList<OAuthAuthenticatorInfo>();
        if (authenticatorsDescriptor != null) {
            for (TransformListResponse.OAuthDescriptor oAuthDescriptor : authenticatorsDescriptor.getOAuthAuthenticators()) {
                arrayList.add(this.translate(oAuthDescriptor));
            }
        }
        return arrayList;
    }

    private OAuthAuthenticatorInfo translate(TransformListResponse.OAuthDescriptor oAuthDescriptor) {
        OAuthAuthenticatorInfo oAuthAuthenticatorInfo = new OAuthAuthenticatorInfo();
        oAuthAuthenticatorInfo.setName(oAuthDescriptor.getName());
        oAuthAuthenticatorInfo.setDisplayName(oAuthDescriptor.getDisplayName());
        oAuthAuthenticatorInfo.setDescription(oAuthDescriptor.getDescription());
        oAuthAuthenticatorInfo.setOAuthVersion(oAuthDescriptor.getOAuthVersion());
        oAuthAuthenticatorInfo.setAccessTokenEndpoint(oAuthDescriptor.getAccessTokenEndpoint());
        oAuthAuthenticatorInfo.setRequestTokenEndpoint(oAuthDescriptor.getRequestTokenEndpoint());
        oAuthAuthenticatorInfo.setAuthorizationUrl(oAuthDescriptor.getAuthorizationUrl());
        oAuthAuthenticatorInfo.setAppKey(oAuthDescriptor.getAppKey());
        oAuthAuthenticatorInfo.setAppSecret(oAuthDescriptor.getAppSecret());
        oAuthAuthenticatorInfo.setIcon(oAuthDescriptor.getIcon());
        oAuthAuthenticatorInfo.setCallbackPort(oAuthDescriptor.getCallbackPort());
        oAuthAuthenticatorInfo.setAccessTokenInput(oAuthDescriptor.getAccessTokenInput());
        oAuthAuthenticatorInfo.setAccessTokenPublicKey(oAuthDescriptor.getAccessTokenPublicKey());
        return oAuthAuthenticatorInfo;
    }
}

