/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryInfo;
import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryServer;
import com.paterva.maltego.transform.protocol.v2api.api.TransformApplicationDescriptor;
import com.paterva.maltego.transform.protocol.v2api.messaging.DiscoveryResponse;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class DiscoveryInfoTranslator {
    private final String _seedUrl;

    public DiscoveryInfoTranslator(String string) {
        this._seedUrl = string;
    }

    public DiscoveryInfo translate(DiscoveryResponse discoveryResponse) {
        DiscoveryInfo discoveryInfo = new DiscoveryInfo(discoveryResponse.getServerName());
        this.addServers(discoveryInfo.getServers(), discoveryResponse.getSeedServers());
        this.addApplications(discoveryInfo.getApplications(), discoveryResponse.getTransformApplications());
        return discoveryInfo;
    }

    private void addApplications(Collection<TransformApplicationDescriptor> collection, ArrayList<DiscoveryResponse.TransformApplicationDescriptor> arrayList) {
        for (DiscoveryResponse.TransformApplicationDescriptor transformApplicationDescriptor : arrayList) {
            collection.add(this.translate(transformApplicationDescriptor));
        }
    }

    private TransformApplicationDescriptor translate(DiscoveryResponse.TransformApplicationDescriptor transformApplicationDescriptor) {
        TransformApplicationDescriptor transformApplicationDescriptor2 = new TransformApplicationDescriptor(transformApplicationDescriptor.getName(), transformApplicationDescriptor.getUrl(), this._seedUrl);
        transformApplicationDescriptor2.setRequiresKey(transformApplicationDescriptor.getRequiresKey());
        transformApplicationDescriptor2.setRegistrationUrl(transformApplicationDescriptor.getRegistrationUrl());
        return transformApplicationDescriptor2;
    }

    private void addServers(Collection<DiscoveryServer> collection, ArrayList<DiscoveryResponse.SeedServerDescriptor> arrayList) {
        for (DiscoveryResponse.SeedServerDescriptor seedServerDescriptor : arrayList) {
            collection.add(this.translate(seedServerDescriptor));
        }
    }

    private DiscoveryServer translate(DiscoveryResponse.SeedServerDescriptor seedServerDescriptor) {
        DiscoveryServer discoveryServer = new DiscoveryServer();
        discoveryServer.setUrl(seedServerDescriptor.getUrl());
        return discoveryServer;
    }
}

