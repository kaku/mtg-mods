/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

public class ProxyException
extends Exception {
    public ProxyException() {
    }

    public ProxyException(String string) {
        super(string);
    }

    public ProxyException(String string, Exception exception) {
        super(string, exception);
    }
}

