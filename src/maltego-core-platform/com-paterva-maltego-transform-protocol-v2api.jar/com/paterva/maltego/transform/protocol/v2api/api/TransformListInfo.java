/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.OAuthAuthenticatorInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInfo;
import java.util.ArrayList;
import java.util.List;

public class TransformListInfo {
    private List<TransformInfo> _transforms;
    private List<OAuthAuthenticatorInfo> _authenticators;

    public List<TransformInfo> getTransforms() {
        if (this._transforms == null) {
            this._transforms = new ArrayList<TransformInfo>();
        }
        return this._transforms;
    }

    public List<OAuthAuthenticatorInfo> getOAuthAuthenticators() {
        if (this._authenticators == null) {
            this._authenticators = new ArrayList<OAuthAuthenticatorInfo>();
        }
        return this._authenticators;
    }

    public String toString() {
        return "TransformListInfo{\n   transforms=" + this._transforms + ",\n   authenticators=" + this._authenticators + "\n}";
    }
}

