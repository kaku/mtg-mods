/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;

public class CodedTransformRunException
extends TransformRunException {
    private int _code;

    public CodedTransformRunException(int n) {
        this._code = n;
    }

    public CodedTransformRunException(String string, int n) {
        super(string);
        this._code = n;
    }

    public int getCode() {
        return this._code;
    }
}

