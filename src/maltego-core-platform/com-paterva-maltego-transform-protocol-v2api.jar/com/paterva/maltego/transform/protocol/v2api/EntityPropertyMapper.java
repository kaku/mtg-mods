/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api;

abstract class EntityPropertyMapper {
    public static final String VALUE_PROPERTY = "maltego.v2.value.property";
    private String[] _new;
    private String[] _old;

    public EntityPropertyMapper(String[] arrstring, String[] arrstring2) {
        this._new = arrstring;
        this._old = arrstring2;
    }

    public String toNew(String string) {
        return EntityPropertyMapper.find(string, this._old, this._new);
    }

    public String toOld(String string) {
        return EntityPropertyMapper.find(string, this._new, this._old);
    }

    public abstract boolean matches(String var1);

    private static String find(String string, String[] arrstring, String[] arrstring2) {
        for (int i = 0; i < arrstring.length; ++i) {
            if (!arrstring[i].equals(string)) continue;
            return arrstring2[i];
        }
        return null;
    }

    static class Affiliation
    extends EntityPropertyMapper {
        public Affiliation(String[] arrstring, String[] arrstring2) {
            super(arrstring, arrstring2);
        }

        @Override
        public boolean matches(String string) {
            return string.startsWith("maltego.affiliation.");
        }
    }

    static class Type
    extends EntityPropertyMapper {
        private String _type;

        public Type(String string, String[] arrstring, String[] arrstring2) {
            super(arrstring, arrstring2);
            this._type = string;
        }

        @Override
        public boolean matches(String string) {
            return this._type.equals(string);
        }
    }

}

