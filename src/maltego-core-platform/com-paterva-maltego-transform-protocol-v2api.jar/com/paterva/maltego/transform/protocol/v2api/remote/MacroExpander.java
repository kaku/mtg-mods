/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.transform.protocol.v2api.messaging.DiscoveryResponse;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class MacroExpander {
    public static final String THIS = "${this}";
    private Map<String, String> _macros = new HashMap<String, String>();

    MacroExpander() {
    }

    public void add(String string, String string2) {
        this._macros.put(string, string2);
    }

    public void expand(DiscoveryResponse discoveryResponse) {
        if (discoveryResponse != null) {
            Object object;
            for (DiscoveryResponse.SeedServerDescriptor object22 : discoveryResponse.getSeedServers()) {
                if (object22 == null || (object = object22.getUrl()) == null) continue;
                object22.setUrl(this.expand((URL)object));
            }
            for (DiscoveryResponse.TransformApplicationDescriptor transformApplicationDescriptor : discoveryResponse.getTransformApplications()) {
                if (transformApplicationDescriptor == null || (object = transformApplicationDescriptor.getUrl()) == null) continue;
                transformApplicationDescriptor.setUrl(this.expand((String)object));
            }
        }
    }

    public URL expand(URL uRL) {
        String string = this.expand(uRL.toString());
        try {
            return new URL(string);
        }
        catch (MalformedURLException var3_3) {
            return uRL;
        }
    }

    public String expand(String string) {
        for (Map.Entry<String, String> entry : this._macros.entrySet()) {
            string = string.replace(entry.getKey(), entry.getValue());
        }
        return string;
    }
}

