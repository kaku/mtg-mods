/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.proxy;

public class ProxySettings {
    private String _proxyServer;
    private int _proxyPort = 0;

    public ProxySettings(String string, int n) {
        this._proxyPort = n;
        this._proxyServer = string;
    }

    public String getProxyServer() {
        return this._proxyServer;
    }

    public void setProxyServer(String string) {
        this._proxyServer = string;
    }

    public int getProxyPort() {
        return this._proxyPort;
    }

    public void setProxyPort(int n) {
        this._proxyPort = n;
    }
}

