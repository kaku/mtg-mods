/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateMismatchException
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyException;
import com.paterva.maltego.util.http.CertificateMismatchException;

public interface Proxy {
    public MaltegoMessageWrapper listTransforms(boolean var1) throws ProxyException, CertificateMismatchException;

    public MaltegoMessageWrapper doTransform(String var1, MaltegoMessageWrapper var2, String var3, String var4, int var5) throws ProxyException;

    public byte[] getMtzConfig() throws ProxyException;

    public int getLoad() throws ProxyException;
}

