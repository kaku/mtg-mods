/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalField;
import java.util.ArrayList;

public class AdditionalFieldCollection
extends ArrayList<AdditionalField> {
    public void add(String string, String string2, String string3) {
        this.add(new AdditionalField(string, string2, string3));
    }

    public String get(String string) {
        for (AdditionalField additionalField : this) {
            if (additionalField.getName() != string) continue;
            return additionalField.getValue();
        }
        return null;
    }
}

