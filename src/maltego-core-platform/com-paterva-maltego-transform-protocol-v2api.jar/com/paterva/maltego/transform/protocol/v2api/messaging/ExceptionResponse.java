/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="MaltegoTransformExceptionMessage", strict=0)
public class ExceptionResponse {
    public static int INVALID_KEY = 500;
    public static int TKS_NOT_FOUND = 501;
    public static int KEY_LIMIT_REACHED = 502;
    public static int KEY_REQUIRED = 600;
    @ElementList(name="Exceptions", type=MaltegoException.class, required=0)
    private ArrayList<MaltegoException> _exceptions;

    public ExceptionResponse() {
    }

    public ExceptionResponse(Exception exception) {
        this();
        this.getExceptions().add(new MaltegoException(exception));
    }

    public ArrayList<MaltegoException> getExceptions() {
        if (this._exceptions == null) {
            this._exceptions = new ArrayList();
        }
        return this._exceptions;
    }

    @Root(name="Exception", strict=0)
    public static class MaltegoException {
        private String _message;
        private int _code = 0;

        public MaltegoException() {
        }

        public MaltegoException(String string) {
            this._message = string;
        }

        public MaltegoException(Exception exception) {
            this(MaltegoException.toString(exception));
        }

        private static String toString(Exception exception) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(exception.toString());
            stringBuffer.append("\n");
            for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
                stringBuffer.append("             ");
                stringBuffer.append(stackTraceElement);
                stringBuffer.append("\n");
            }
            return stringBuffer.toString();
        }

        @Text
        public String getMessage() {
            return this._message;
        }

        @Text
        public void setMessage(String string) {
            this._message = string;
        }

        @Attribute(name="code", required=0)
        public int getCode() {
            return this._code;
        }

        @Attribute(name="code", required=0)
        public void setCode(int n) {
            this._code = n;
        }
    }

}

