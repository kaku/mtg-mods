/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import java.net.URL;

public class TransformApplicationDescriptor {
    private String _url;
    private URL _registrationUrl;
    private String _name;
    private boolean _requiresKey = false;
    private final String _seedUrl;

    public TransformApplicationDescriptor(String string, String string2, String string3) {
        this._url = string2;
        this._name = string;
        this._seedUrl = string3;
    }

    public boolean requiresKey() {
        return this._requiresKey;
    }

    public void setRequiresKey(boolean bl) {
        this._requiresKey = bl;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public URL getRegistrationUrl() {
        return this._registrationUrl;
    }

    public void setRegistrationUrl(URL uRL) {
        this._registrationUrl = uRL;
    }

    public String getUrl() {
        return this._url;
    }

    public void setUrl(String string) {
        this._url = string;
    }

    public String getSeedUrl() {
        return this._seedUrl;
    }

    public boolean equals(Object object) {
        if (object instanceof TransformApplicationDescriptor) {
            return this.equals((TransformApplicationDescriptor)object);
        }
        return false;
    }

    public int hashCode() {
        int n = 7;
        n = 53 * n + (this._url != null ? this._url.hashCode() : 0);
        return n;
    }

    public boolean equals(TransformApplicationDescriptor transformApplicationDescriptor) {
        return this.getUrl().equals(transformApplicationDescriptor.getUrl());
    }

    public String toString() {
        return this.getName() + ":" + this.getUrl();
    }
}

