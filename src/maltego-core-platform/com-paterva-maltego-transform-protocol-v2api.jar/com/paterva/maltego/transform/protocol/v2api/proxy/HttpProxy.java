/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.runner.api.TransformRunRequestDecorator
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.http.CertificateMismatchException
 *  com.paterva.maltego.util.http.CertificateUtils
 *  com.paterva.maltego.util.http.HttpAgent
 *  com.paterva.maltego.util.http.ServerCertificates
 */
package com.paterva.maltego.transform.protocol.v2api.proxy;

import com.paterva.maltego.transform.protocol.v2api.cert.CertificateMismatchDisplayer;
import com.paterva.maltego.transform.protocol.v2api.cert.CertificateMismatchPanel;
import com.paterva.maltego.transform.protocol.v2api.messaging.CanceledMessage;
import com.paterva.maltego.transform.protocol.v2api.messaging.ListTransformsCache;
import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageInputStream;
import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.transform.protocol.v2api.messaging.MessagingHelper;
import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyException;
import com.paterva.maltego.transform.protocol.v2api.proxy.ProxySettings;
import com.paterva.maltego.transform.runner.api.TransformRunRequestDecorator;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.http.CertificateMismatchException;
import com.paterva.maltego.util.http.CertificateUtils;
import com.paterva.maltego.util.http.HttpAgent;
import com.paterva.maltego.util.http.ServerCertificates;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public class HttpProxy
implements Proxy {
    private static final Logger LOG = Logger.getLogger(HttpProxy.class.getName());
    private String _transformApp;
    private String _userAgent;
    private ProxySettings _proxy;

    public HttpProxy(String string) {
        this(string, null);
    }

    public HttpProxy(String string, ProxySettings proxySettings) {
        this._transformApp = string;
        this._proxy = proxySettings;
    }

    public ProxySettings getProxySettings() {
        return this._proxy;
    }

    public void setProxySettings(ProxySettings proxySettings) {
        this._proxy = proxySettings;
    }

    public String getUserAgent() {
        return this._userAgent;
    }

    public void setUserAgent(String string) {
        this._userAgent = string;
    }

    private URL getRunRequestUrl(String string, String string2, String string3) throws MalformedURLException {
        StringBuilder stringBuilder = new StringBuilder(this._transformApp);
        stringBuilder.append("?Command=_RUN&TransformToRun=");
        stringBuilder.append(string);
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            stringBuilder.append("&Key=");
            stringBuilder.append(string2);
        }
        stringBuilder.append("&OEM=");
        stringBuilder.append(System.getProperty("maltego.oem", ""));
        return new URL(TransformRunRequestDecorator.decorate((String)stringBuilder.toString(), (String)string3));
    }

    @Override
    public MaltegoMessageWrapper listTransforms(boolean bl) throws ProxyException, CertificateMismatchException {
        String string = this._transformApp + "?Command=_TRANSFORMS";
        MaltegoMessageWrapper maltegoMessageWrapper = ListTransformsCache.get(string);
        if (maltegoMessageWrapper == null && (maltegoMessageWrapper = this.doGet(string, bl)) != null) {
            ListTransformsCache.put(string, maltegoMessageWrapper);
        }
        return maltegoMessageWrapper;
    }

    private MaltegoMessageWrapper doGet(String string, boolean bl) throws ProxyException, CertificateMismatchException {
        MaltegoMessageWrapper maltegoMessageWrapper;
        maltegoMessageWrapper = null;
        HttpAgent httpAgent = null;
        try {
            LOG.log(Level.FINE, "Get from {0}", string);
            httpAgent = new HttpAgent(new URL(string));
            httpAgent.setUserAgent(this.getUserAgent());
            httpAgent.setConnectTimeout(30000);
            httpAgent.setReadTimeout(30000);
            httpAgent.setAcceptEncoding("gzip");
            X509Certificate x509Certificate = httpAgent.getServerCertificate();
            if (x509Certificate != null) {
                ServerCertificates serverCertificates = ServerCertificates.getDefault();
                if (serverCertificates.isBlacklisted(string, x509Certificate)) {
                    MaltegoMessageWrapper maltegoMessageWrapper2 = new MaltegoMessageWrapper();
                    return maltegoMessageWrapper2;
                }
                serverCertificates.checkSameAsBefore(string, x509Certificate);
            } else if (bl) {
                this.throwNoHttps(string);
            }
            httpAgent.doGet();
            maltegoMessageWrapper = MessagingHelper.read(httpAgent.getInputStream(), httpAgent.getContentEncoding());
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(MessagingHelper.toString(maltegoMessageWrapper));
            }
        }
        catch (CertificateMismatchException var5_6) {
            throw var5_6;
        }
        catch (IOException var5_7) {
            throw new ProxyException("HTTP error for " + string + ": " + var5_7.getMessage(), var5_7);
        }
        catch (Exception var5_8) {
            throw new ProxyException("Message parsing exception for " + string + ": " + var5_8.getMessage(), var5_8);
        }
        finally {
            if (httpAgent != null) {
                httpAgent.disconnect();
            }
        }
        return maltegoMessageWrapper;
    }

    @Override
    public MaltegoMessageWrapper doTransform(String string, MaltegoMessageWrapper maltegoMessageWrapper, String string2, String string3, int n) throws ProxyException {
        MaltegoMessageWrapper maltegoMessageWrapper2;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "--==<Transform input>==--\n{0}", MessagingHelper.toString(maltegoMessageWrapper));
        }
        maltegoMessageWrapper2 = null;
        HttpAgent httpAgent = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            URL uRL = this.getRunRequestUrl(string, string2, string3);
            LOG.log(Level.FINE, "Transform request url: {0}", uRL);
            httpAgent = new HttpAgent(uRL);
            httpAgent.setUserAgent(this.getUserAgent());
            httpAgent.setAcceptEncoding("gzip");
            httpAgent.setConnectTimeout(n);
            httpAgent.setReadTimeout(n);
            String string4 = this.checkValidCertificate(httpAgent);
            if (string4 != null) {
                CanceledMessage canceledMessage = new CanceledMessage(string4);
                return canceledMessage;
            }
            outputStream = httpAgent.doPost("text/xml");
            MessagingHelper.write(maltegoMessageWrapper, outputStream);
            String string5 = httpAgent.getContentEncoding();
            inputStream = string5 != null && string5.contains("gzip") ? new GZIPInputStream(httpAgent.getInputStream()) : httpAgent.getInputStream();
            inputStream = new MaltegoMessageInputStream(inputStream);
            maltegoMessageWrapper2 = MessagingHelper.read(inputStream);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "--==<Transform result>==--\n{0}", MessagingHelper.toString(maltegoMessageWrapper2));
            }
        }
        catch (IOException var10_13) {
            throw new ProxyException("HTTP error: " + var10_13.getMessage(), var10_13);
        }
        catch (Exception var10_14) {
            throw new ProxyException("Message parsing exception: " + var10_14.getMessage(), var10_14);
        }
        finally {
            if (httpAgent != null) {
                httpAgent.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException var15_21) {
                    Logger.getLogger(HttpProxy.class.getName()).log(Level.INFO, null, var15_21);
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                }
                catch (IOException var15_22) {
                    Logger.getLogger(HttpProxy.class.getName()).log(Level.INFO, null, var15_22);
                }
            }
        }
        return maltegoMessageWrapper2;
    }

    @Override
    public int getLoad() throws ProxyException {
        HttpAgent httpAgent = null;
        int n = 0;
        try {
            httpAgent = new HttpAgent(new URL(this._transformApp + "?Command=_PERFORMANCE"));
            httpAgent.setUserAgent(this.getUserAgent());
            httpAgent.doGet();
            String string = httpAgent.getContentAsString();
            n = Integer.parseInt(string);
        }
        catch (IOException var3_4) {
            throw new ProxyException("HTTP error: " + var3_4.getMessage(), var3_4);
        }
        catch (Exception var3_5) {
            throw new ProxyException("Message parsing exception: " + var3_5.getMessage(), var3_5);
        }
        finally {
            if (httpAgent != null) {
                httpAgent.disconnect();
            }
        }
        return n;
    }

    @Override
    public byte[] getMtzConfig() throws ProxyException {
        HttpAgent httpAgent = null;
        byte[] arrby = null;
        try {
            httpAgent = new HttpAgent(new URL(this._transformApp + "?Command=_CONFIG"));
            httpAgent.setUserAgent(this.getUserAgent());
            httpAgent.doGet();
            arrby = httpAgent.getContentAsBytes();
        }
        catch (IOException var3_3) {
            throw new ProxyException("HTTP error: " + var3_3.getMessage(), var3_3);
        }
        catch (Exception var3_4) {
            throw new ProxyException("Message parsing exception: " + var3_4.getMessage(), var3_4);
        }
        finally {
            if (httpAgent != null) {
                httpAgent.disconnect();
            }
        }
        return arrby;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String toString(InputStream inputStream) {
        StringBuffer stringBuffer;
        stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String string = null;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuffer.append(string);
                stringBuffer.append("\n");
            }
        }
        catch (UnsupportedEncodingException var4_6) {
            var4_6.printStackTrace();
        }
        catch (IOException var4_8) {
            var4_8.printStackTrace();
        }
        finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                }
                catch (Exception var4_9) {}
            }
        }
        return stringBuffer.toString();
    }

    private String checkValidCertificate(HttpAgent httpAgent) throws IOException, GeneralSecurityException {
        X509Certificate x509Certificate = httpAgent.getServerCertificate();
        if (x509Certificate != null) {
            ServerCertificates serverCertificates = ServerCertificates.getDefault();
            if (serverCertificates.isBlacklisted(this._transformApp, x509Certificate)) {
                return "Transform canceled, server not trusted";
            }
            if (!serverCertificates.isSameAsBefore(this._transformApp, x509Certificate)) {
                X509Certificate x509Certificate2 = serverCertificates.get(this._transformApp);
                String string = "The certificate for the server is not the same as before.\n\nAre you sure you want to run the transform?";
                CertificateMismatchPanel certificateMismatchPanel = new CertificateMismatchPanel(string, x509Certificate2, x509Certificate);
                CertificateMismatchDisplayer.TrustResult trustResult = CertificateMismatchDisplayer.showTrustPrompt(certificateMismatchPanel);
                switch (trustResult) {
                    case TRUST: {
                        serverCertificates.add(this._transformApp, x509Certificate);
                        break;
                    }
                    case DONT_TRUST: {
                        serverCertificates.blacklist(this._transformApp, x509Certificate);
                    }
                    default: {
                        return "Transform canceled due to certificate mismatch";
                    }
                }
            }
        } else {
            this.throwNoHttps(this._transformApp);
        }
        return null;
    }

    private void throwNoHttps(String string) throws IOException {
        throw new IOException("Only HTTPS (SSL/TLS) Transform Servers are allowed, but found: " + CertificateUtils.stripUrl((String)string));
    }

}

