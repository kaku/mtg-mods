/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import java.util.EventObject;

public class ProgressEvent
extends EventObject {
    private String _message;

    public ProgressEvent(Object object, String string) {
        super(object);
        this._message = string;
    }

    public String getMessage() {
        return this._message;
    }
}

