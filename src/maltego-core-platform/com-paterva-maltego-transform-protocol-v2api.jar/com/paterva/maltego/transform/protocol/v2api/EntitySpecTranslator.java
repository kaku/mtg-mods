/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.registry.Level1EntityRegistry
 *  com.paterva.maltego.entity.registry.Level2EntityRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.Level1EntityRegistry;
import com.paterva.maltego.entity.registry.Level2EntityRegistry;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInfo;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EntitySpecTranslator {
    private static EntityRegistry _level1Registry;
    private static EntityRegistry _level2Registry;
    private static Map<String, String> _2to3;
    private static Map<String, String> _3to2;
    private static final String[] _mapping;

    public static String getV3TypeName(String string) {
        if ("maltego.image".equals(string)) {
            return "maltego.Image";
        }
        String string2 = EntitySpecTranslator.map2to3().get(string);
        if (string2 == null) {
            string2 = string;
        }
        return string2;
    }

    public static String getV2TypeName(String string) {
        if ("maltego.Image".equals(string)) {
            return "maltego.image";
        }
        String string2 = EntitySpecTranslator.map3to2().get(string);
        if (string2 == null) {
            string2 = string;
        }
        return string2;
    }

    public static Set<MaltegoEntitySpec> getEntities(TransformInfo transformInfo) {
        HashSet<MaltegoEntitySpec> hashSet = new HashSet<MaltegoEntitySpec>();
        hashSet.addAll(EntitySpecTranslator.level1Registry().getAll());
        MaltegoEntitySpec maltegoEntitySpec = EntitySpecTranslator.getEntity(transformInfo.getInputEntityType());
        if (maltegoEntitySpec != null) {
            hashSet.add(maltegoEntitySpec);
        }
        for (String string2 : transformInfo.getOutputEntityTypes()) {
            MaltegoEntitySpec maltegoEntitySpec2 = EntitySpecTranslator.getEntity(string2);
            if (maltegoEntitySpec2 == null) continue;
            hashSet.add(maltegoEntitySpec2);
        }
        HashSet hashSet2 = new HashSet();
        for (MaltegoEntitySpec maltegoEntitySpec3 : hashSet) {
            hashSet2.addAll(maltegoEntitySpec3.getBaseEntitySpecs());
        }
        Iterator iterator = hashSet2.iterator();
        while (iterator.hasNext()) {
            String string2;
            String string3 = (String)iterator.next();
            if (string3 == null || (string2 = (MaltegoEntitySpec)EntitySpecTranslator.level2Registry().get(string3)) == null) continue;
            hashSet.add((MaltegoEntitySpec)string2);
        }
        return hashSet;
    }

    public static MaltegoEntitySpec getEntity(String string) {
        return (MaltegoEntitySpec)EntitySpecTranslator.level2Registry().get(EntitySpecTranslator.getV3TypeName(string));
    }

    private static synchronized Map<String, String> map2to3() {
        if (_2to3 == null) {
            _2to3 = new HashMap<String, String>();
            for (int i = 0; i < _mapping.length; i += 2) {
                _2to3.put(_mapping[i], _mapping[i + 1]);
            }
        }
        return _2to3;
    }

    private static synchronized Map<String, String> map3to2() {
        if (_3to2 == null) {
            _3to2 = new HashMap<String, String>();
            for (int i = 0; i < _mapping.length; i += 2) {
                _3to2.put(_mapping[i + 1], _mapping[i]);
            }
        }
        return _3to2;
    }

    private static synchronized EntityRegistry level1Registry() {
        if (_level1Registry == null) {
            _level1Registry = new Level1EntityRegistry();
        }
        return _level1Registry;
    }

    private static synchronized EntityRegistry level2Registry() {
        if (_level2Registry == null) {
            _level2Registry = new Level2EntityRegistry();
        }
        return _level2Registry;
    }

    static {
        _mapping = new String[]{"ASNumber", "maltego.AS", "AffiliationBebo", "maltego.affiliation.Bebo", "AffiliationFacebook", "maltego.affiliation.Facebook", "AffiliationFlickr", "maltego.affiliation.Flickr", "AffiliationLinkedin", "maltego.affiliation.LinkedIn", "AffiliationMySpace", "maltego.affiliation.Myspace", "AffiliationOrkut", "maltego.affiliation.Orkut", "AffiliationSpock", "maltego.affiliation.Spock", "AffiliationTwitter", "maltego.affiliation.Twitter", "IPAddress", "maltego.IPv4Address", "NSrecord", "maltego.NSRecord", "MXrecord", "maltego.MXRecord", "Document", "maltego.Document", "EmailAddress", "maltego.EmailAddress", "Person", "maltego.Person", "PhoneNumber", "maltego.PhoneNumber", "Phrase", "maltego.Phrase", "Twit", "maltego.Twit", "DNSName", "maltego.DNSName", "Domain", "maltego.Domain", "Location", "maltego.Location", "Netblock", "maltego.Netblock", "URL", "maltego.URL", "Website", "maltego.Website", "Webtitle", "maltego.WebTitle", "Webdir", "maltego.WebDir", "Vuln", "maltego.Vulnerability", "Service", "maltego.Service", "Port", "maltego.Port", "Banner", "maltego.Banner", "Alias", "maltego.Alias", "Device", "maltego.Device", "GPS", "maltego.GPS", "CircularArea", "maltego.CircularArea", "Image", "maltego.Image", "NominatimLocation", "maltego.NominatimLocation", "BuiltWith.Technology", "maltego.BuiltWithTechnology", "facebook.Object", "maltego.FacebookObject"};
    }
}

