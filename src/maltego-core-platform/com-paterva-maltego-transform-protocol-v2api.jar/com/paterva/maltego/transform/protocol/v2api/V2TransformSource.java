/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateMismatchException
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformFindException;
import com.paterva.maltego.transform.protocol.v2api.api.TransformListInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformSource;
import com.paterva.maltego.transform.protocol.v2api.remote.RemoteTransformSource;
import com.paterva.maltego.util.http.CertificateMismatchException;

public class V2TransformSource
implements TransformSource {
    @Override
    public DiscoveryInfo getDiscoveryInfo(String string, String string2, boolean bl) throws TransformFindException, CertificateMismatchException {
        RemoteTransformSource remoteTransformSource = new RemoteTransformSource();
        return remoteTransformSource.getDiscoveryInfo(string, string2, bl);
    }

    @Override
    public TransformListInfo getTransforms(String string) throws TransformFindException, CertificateMismatchException {
        RemoteTransformSource remoteTransformSource = new RemoteTransformSource();
        return remoteTransformSource.getTransforms(string);
    }
}

