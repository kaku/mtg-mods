/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.RequestProcessor
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.transform.protocol.v2api.EntityTranslator;
import com.paterva.maltego.transform.protocol.v2api.LocalTransformExecutor;
import com.paterva.maltego.transform.protocol.v2api.TransformRunner;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalField;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalFieldCollection;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.transform.protocol.v2api.messaging.MessagingHelper;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformResponse;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.StringUtilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.util.RequestProcessor;

class LocalTransformRunner
extends TransformRunner {
    private static RequestProcessor _processor = new RequestProcessor("V2 Local Transform Throttle", 10, true);
    private String _command;
    private String _params;
    private File _workingDir;
    private boolean _debug;

    public LocalTransformRunner(EntityFactory entityFactory, EntityRegistry entityRegistry, String string) {
        super(entityFactory, entityRegistry);
        if (StringUtilities.isNullOrEmpty((String)string)) {
            throw new IllegalArgumentException("Command cannot be null");
        }
        this._command = string;
    }

    @Override
    protected MaltegoMessageWrapper runTransform(MaltegoEntity maltegoEntity) throws TransformRunException, IOException {
        String string = null;
        try {
            PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)this.getRegistry(), (TypedPropertyBag)maltegoEntity, (boolean)true);
            String string2 = LocalTransformRunner.toString(maltegoEntity.getValue(propertyDescriptor), propertyDescriptor);
            String string3 = this.getAdditionalFields(maltegoEntity);
            String string4 = this._command;
            File file = this._workingDir;
            ArrayList<String> arrayList = new ArrayList<String>();
            if (!StringUtilities.isNullOrEmpty((String)this._params)) {
                Pattern pattern = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*'");
                Matcher matcher = pattern.matcher(this._params);
                while (matcher.find()) {
                    arrayList.add(matcher.group());
                }
            }
            if (string2 != null) {
                arrayList.add(string2);
            }
            if (string3 != null) {
                arrayList.add(string3);
            }
            boolean bl = this._debug;
            string = LocalTransformExecutor.getDefault().execute(string4, file, arrayList, bl);
        }
        catch (InterruptedException var3_4) {
            TransformResponse.Notification notification = new TransformResponse.Notification();
            notification.setType("PartialError");
            notification.setValue("Transform " + this._command + " cancelled");
            TransformResponse transformResponse = new TransformResponse();
            transformResponse.getMessages().add(notification);
            return new MaltegoMessageWrapper(transformResponse);
        }
        catch (ExecutionException var3_5) {
            var3_5.printStackTrace();
            throw new TransformRunException(LocalTransformRunner.getInnermostException(var3_5).getMessage());
        }
        return MessagingHelper.read(string);
    }

    private static Throwable getInnermostException(Throwable throwable) {
        if (throwable.getCause() == null) {
            return throwable;
        }
        return LocalTransformRunner.getInnermostException(throwable.getCause());
    }

    private String getAdditionalFields(MaltegoEntity maltegoEntity) {
        EntityDescriptor entityDescriptor = EntityTranslator.instance().translate(maltegoEntity, this.getRegistry());
        if (entityDescriptor.getFields().isEmpty()) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (AdditionalField additionalField : entityDescriptor.getFields()) {
            stringBuffer.append(additionalField.getName());
            stringBuffer.append("=");
            stringBuffer.append(this.escape(additionalField.getValue()));
            stringBuffer.append("#");
        }
        return stringBuffer.substring(0, stringBuffer.length() - 1);
    }

    private static String toString(Object object, PropertyDescriptor propertyDescriptor) {
        if (object == null) {
            return null;
        }
        if (propertyDescriptor == null) {
            return object.toString();
        }
        return Converter.convertTo((Object)object, (Class)propertyDescriptor.getType());
    }

    public void setParameters(String string) {
        this._params = string;
    }

    public void setWorkingDir(File file) {
        this._workingDir = file;
    }

    public void setDebug(boolean bl) {
        this._debug = bl;
    }

    public String getCommand() {
        return this._command;
    }

    public void setCommand(String string) {
        this._command = string;
    }

    public String getParams() {
        return this._params;
    }

    public File getWorkingDir() {
        return this._workingDir;
    }

    public boolean isDebug() {
        return this._debug;
    }

    @Override
    protected RequestProcessor processor() {
        return _processor;
    }

    private String escape(String string) {
        if (string == null) {
            return null;
        }
        return string.replace("\\", "\\\\").replace("=", "\\=").replace("#", "\\#");
    }
}

