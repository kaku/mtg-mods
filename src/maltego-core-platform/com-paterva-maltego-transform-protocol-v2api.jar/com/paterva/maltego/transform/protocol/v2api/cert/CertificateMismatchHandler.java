/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateUtils
 *  com.paterva.maltego.util.http.ServerCertificates
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.protocol.v2api.cert;

import com.paterva.maltego.transform.protocol.v2api.cert.CertificateMismatchDisplayer;
import com.paterva.maltego.transform.protocol.v2api.cert.CertificateMismatchPanel;
import com.paterva.maltego.util.http.CertificateUtils;
import com.paterva.maltego.util.http.ServerCertificates;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.cert.X509Certificate;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;

public class CertificateMismatchHandler {
    private static final int HIDE_DELAY_MILLIS = 300000;

    public static void showNotification(String string, X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        String string2 = "Certificate Mismatch";
        String string3 = "Server certificate changed. Click here to accept new certificate.";
        ImageIcon imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/transform/finder/DiscoverTransforms.png", (boolean)true);
        NotificationListener notificationListener = new NotificationListener(string, x509Certificate, x509Certificate2);
        Notification notification = NotificationDisplayer.getDefault().notify(string2, (Icon)imageIcon, string3, (ActionListener)notificationListener);
        Timer timer = new Timer(300000, new ClearNotificationListener(notification));
        timer.setRepeats(false);
        timer.start();
    }

    static class ClearNotificationListener
    implements ActionListener {
        private final Notification _notification;

        public ClearNotificationListener(Notification notification) {
            this._notification = notification;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this._notification.clear();
        }
    }

    private static class NotificationListener
    implements ActionListener {
        private final String _url;
        private final X509Certificate _oldCert;
        private final X509Certificate _newCert;

        public NotificationListener(String string, X509Certificate x509Certificate, X509Certificate x509Certificate2) {
            this._url = CertificateUtils.stripUrl((String)string);
            this._oldCert = x509Certificate;
            this._newCert = x509Certificate2;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String string = "The certificate for the following URL is not the same as before:\n\n" + this._url;
            CertificateMismatchPanel certificateMismatchPanel = new CertificateMismatchPanel(string, this._oldCert, this._newCert);
            CertificateMismatchDisplayer.TrustResult trustResult = CertificateMismatchDisplayer.showTrustPrompt(certificateMismatchPanel);
            ServerCertificates serverCertificates = ServerCertificates.getDefault();
            switch (trustResult) {
                case TRUST: {
                    serverCertificates.add(this._url, this._newCert);
                    break;
                }
                case DONT_TRUST: {
                    serverCertificates.blacklist(this._url, this._newCert);
                    break;
                }
            }
        }
    }

}

