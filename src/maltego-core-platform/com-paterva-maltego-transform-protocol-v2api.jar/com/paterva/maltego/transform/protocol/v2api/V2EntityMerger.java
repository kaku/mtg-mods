/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.merging.MergeUtils
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.merging.MergeUtils;

class V2EntityMerger {
    private V2EntityMerger() {
    }

    public static MaltegoEntity merge(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        MergeUtils.addMissingProperties((MaltegoPart)maltegoEntity2, (MaltegoPart)maltegoEntity);
        MergeUtils.mergePropertyValues((MaltegoPart)maltegoEntity2, (MaltegoPart)maltegoEntity, (boolean)true);
        MergeUtils.mergeSpecialPropertyMapping((MaltegoPart)maltegoEntity2, (MaltegoPart)maltegoEntity, (boolean)false);
        MergeUtils.mergeDisplayInformation((MaltegoPart)maltegoEntity2, (MaltegoPart)maltegoEntity, (boolean)true);
        MergeUtils.mergeNotes((MaltegoPart)maltegoEntity2, (MaltegoPart)maltegoEntity, (boolean)false);
        return maltegoEntity2;
    }
}

