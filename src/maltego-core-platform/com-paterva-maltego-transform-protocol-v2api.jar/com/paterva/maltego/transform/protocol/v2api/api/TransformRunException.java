/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

public class TransformRunException
extends Exception {
    public TransformRunException() {
    }

    public TransformRunException(String string) {
        super(string);
    }

    public TransformRunException(String string, Exception exception) {
        super(string, exception);
    }

    public TransformRunException(Exception exception) {
        this(exception.getMessage(), exception);
    }
}

