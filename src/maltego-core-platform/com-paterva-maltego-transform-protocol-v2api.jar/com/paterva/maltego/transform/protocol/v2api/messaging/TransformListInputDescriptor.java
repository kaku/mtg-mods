/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Input", strict=0)
public class TransformListInputDescriptor {
    @Attribute(name="Display")
    @SerializedName(value="Display")
    private String _display;
    @Attribute(name="Type")
    @SerializedName(value="Type")
    private String _type;
    @Attribute(name="Name")
    @SerializedName(value="Name")
    private String _name;
    @Attribute(name="Optional", required=0)
    @SerializedName(value="Optional")
    private boolean _optional = false;
    @Attribute(name="Popup", required=0)
    @SerializedName(value="Popup")
    private boolean _popup = false;
    @Attribute(name="DefaultValue", required=0)
    @SerializedName(value="DefaultValue")
    private String _defaultValue;
    @Attribute(name="Auth", required=0)
    @SerializedName(value="Auth")
    private boolean _auth = false;

    public boolean getOptional() {
        return this._optional;
    }

    public void setOptional(boolean bl) {
        this._optional = bl;
    }

    public boolean isPopup() {
        return this._popup;
    }

    public void setPopup(boolean bl) {
        this._popup = bl;
    }

    public String getDefaultValue() {
        return this._defaultValue;
    }

    public void setDefaultValue(String string) {
        this._defaultValue = string;
    }

    public String getDisplayName() {
        return this._display;
    }

    public void setDisplayName(String string) {
        this._display = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getTypeName() {
        return this._type;
    }

    public void setTypeName(String string) {
        this._type = string;
    }

    public boolean isAuth() {
        return this._auth;
    }

    public void setAuth(boolean bl) {
        this._auth = bl;
    }
}

