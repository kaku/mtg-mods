/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.cert;

import com.paterva.maltego.transform.protocol.v2api.cert.CertificateInfoPanel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.security.cert.X509Certificate;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class CertificateMismatchPanel
extends JPanel {
    private final X509Certificate _oldCert;
    private final X509Certificate _newCert;
    private CertificateInfoPanel _certificateInfoPanel;
    private JTextArea _descriptionTextArea;

    public CertificateMismatchPanel(String string, X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        this._oldCert = x509Certificate;
        this._newCert = x509Certificate2;
        this.initComponents();
        this._descriptionTextArea.setText(string);
    }

    private void initComponents() {
        JScrollPane jScrollPane = new JScrollPane();
        this._descriptionTextArea = new JTextArea();
        this._certificateInfoPanel = new CertificateInfoPanel(this._oldCert, this._newCert);
        this.setLayout(new GridBagLayout());
        this._descriptionTextArea.setEditable(false);
        this._descriptionTextArea.setColumns(20);
        this._descriptionTextArea.setLineWrap(true);
        this._descriptionTextArea.setRows(3);
        this._descriptionTextArea.setWrapStyleWord(true);
        this._descriptionTextArea.setOpaque(false);
        jScrollPane.setViewportView(this._descriptionTextArea);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)jScrollPane, gridBagConstraints);
        this._certificateInfoPanel.setPreferredSize(new Dimension(600, 400));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this._certificateInfoPanel, gridBagConstraints);
    }
}

