/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalField;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalFieldCollection;
import com.paterva.maltego.transform.protocol.v2api.messaging.DisplayInfo;
import com.paterva.maltego.transform.protocol.v2api.messaging.DisplayInfoCollection;
import com.paterva.maltego.util.FastURL;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Entity", strict=0)
public class EntityDescriptor {
    private String _typeName = "";
    private String _value = "";
    private FastURL _iconUrl;
    private int _weight;
    @ElementList(name="AdditionalFields", type=AdditionalField.class, required=0)
    private AdditionalFieldCollection _fields;
    @ElementList(name="DisplayInformation", type=DisplayInfo.class, required=0)
    private DisplayInfoCollection _displayInfo;

    public EntityDescriptor() {
    }

    public EntityDescriptor(String string) {
        this._typeName = string;
    }

    public AdditionalFieldCollection getFields() {
        if (this._fields == null) {
            this._fields = new AdditionalFieldCollection();
        }
        return this._fields;
    }

    public DisplayInfoCollection getDisplayInformation() {
        if (this._displayInfo == null) {
            this._displayInfo = new DisplayInfoCollection();
        }
        return this._displayInfo;
    }

    @Attribute(name="Type")
    public String getTypeName() {
        return this._typeName;
    }

    @Attribute(name="Type")
    public void setTypeName(String string) {
        this._typeName = string;
    }

    @Element(name="Weight", required=0)
    public int getWeight() {
        return this._weight;
    }

    @Element(name="Weight", required=0)
    public void setWeight(int n) {
        this._weight = n;
    }

    @Element(name="IconURL", required=0)
    public String getIconUrl() {
        return this._iconUrl != null ? this._iconUrl.toString() : null;
    }

    @Element(name="IconURL", required=0)
    public void setIconUrl(String string) {
        this._iconUrl = string != null ? new FastURL(string) : null;
    }

    @Element(name="Value", required=0)
    public String getValue() {
        return this._value;
    }

    @Element(name="Value", required=0)
    public void setValue(String string) {
        this._value = string;
    }

    public int hashCode() {
        return (this.getTypeName() + this.getValue()).hashCode();
    }

    public String toString() {
        return this.getTypeName() + "[" + this.getValue() + "]";
    }

    public boolean equals(Object object) {
        if (object instanceof EntityDescriptor) {
            return this.equals((EntityDescriptor)object);
        }
        return false;
    }

    public boolean equals(EntityDescriptor entityDescriptor) {
        return entityDescriptor.getTypeName().equals(this.getTypeName()) && entityDescriptor.getValue().equals(this.getValue());
    }
}

