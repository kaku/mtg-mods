/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.CodedTransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;

public class KeyLimitReachedException
extends CodedTransformRunException {
    public KeyLimitReachedException() {
        super(ExceptionResponse.KEY_LIMIT_REACHED);
    }

    public KeyLimitReachedException(String string) {
        super(string, ExceptionResponse.KEY_LIMIT_REACHED);
    }
}

