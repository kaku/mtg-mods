/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.matching.MatchingRule
 *  com.paterva.maltego.matching.MatchingRule$TypeToPropertiesMap
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator;
import com.paterva.maltego.transform.protocol.v2api.EntityTranslator;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalField;
import com.paterva.maltego.transform.protocol.v2api.messaging.AdditionalFieldCollection;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

class V2MatchingRule
extends MatchingRule.TypeToPropertiesMap {
    V2MatchingRule() {
    }

    public void update(Collection<EntityDescriptor> collection, EntityRegistry entityRegistry) {
        TreeSet<String> treeSet = new TreeSet<String>();
        for (EntityDescriptor entityDescriptor : collection) {
            if (treeSet.contains(entityDescriptor.getTypeName())) continue;
            this.update(entityDescriptor, entityRegistry);
            treeSet.add(entityDescriptor.getTypeName());
        }
    }

    private void update(EntityDescriptor entityDescriptor, EntityRegistry entityRegistry) {
        ArrayList<String> arrayList = new ArrayList<String>();
        EntityTranslator entityTranslator = EntityTranslator.instance();
        String string = EntitySpecTranslator.getV3TypeName(entityDescriptor.getTypeName());
        for (AdditionalField additionalField : entityDescriptor.getFields()) {
            if (!"strict".equals(additionalField.getMatchingRule())) continue;
            arrayList.add(entityTranslator.v3PropertyName(entityDescriptor, additionalField.getName(), additionalField.getName(), entityRegistry));
        }
        this.update(string, arrayList, arrayList.isEmpty());
    }

    public boolean equals(Object object) {
        if (object == null || object instanceof V2MatchingRule) {
            return true;
        }
        return false;
    }
}

