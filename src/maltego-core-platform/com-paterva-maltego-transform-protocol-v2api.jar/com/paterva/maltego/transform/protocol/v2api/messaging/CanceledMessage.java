/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;

public class CanceledMessage
extends MaltegoMessageWrapper {
    private final String _message;

    public CanceledMessage(String string) {
        this._message = string;
    }

    public String getMessage() {
        return this._message;
    }
}

