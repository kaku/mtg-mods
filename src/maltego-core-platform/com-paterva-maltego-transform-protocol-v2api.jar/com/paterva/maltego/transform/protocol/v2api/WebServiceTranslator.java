/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.OAuthAuthenticatorBuilder
 *  com.paterva.maltego.pws.api.OAuthVersion
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServiceFactory
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthAuthenticatorBuilder;
import com.paterva.maltego.pws.api.OAuthVersion;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServiceFactory;
import com.paterva.maltego.transform.protocol.v2api.api.OAuthAuthenticatorInfo;
import java.util.Collections;
import java.util.Map;

public class WebServiceTranslator {
    public Map<PublicWebService, String> translate(OAuthAuthenticatorInfo oAuthAuthenticatorInfo) {
        OAuthAuthenticator oAuthAuthenticator = this.translateAuthenticator(oAuthAuthenticatorInfo);
        PublicWebService publicWebService = PublicWebServiceFactory.getDefault().create(oAuthAuthenticator);
        return Collections.singletonMap(publicWebService, oAuthAuthenticatorInfo.getIcon());
    }

    private OAuthAuthenticator translateAuthenticator(OAuthAuthenticatorInfo oAuthAuthenticatorInfo) {
        OAuthAuthenticatorBuilder oAuthAuthenticatorBuilder = OAuthAuthenticatorBuilder.getDefault();
        oAuthAuthenticatorBuilder.name(oAuthAuthenticatorInfo.getName());
        oAuthAuthenticatorBuilder.displayName(oAuthAuthenticatorInfo.getDisplayName());
        oAuthAuthenticatorBuilder.description(oAuthAuthenticatorInfo.getDescription());
        oAuthAuthenticatorBuilder.appKey(oAuthAuthenticatorInfo.getAppKey());
        oAuthAuthenticatorBuilder.appSecret(oAuthAuthenticatorInfo.getAppSecret());
        oAuthAuthenticatorBuilder.oAuthVersion(OAuthVersion.parse((String)oAuthAuthenticatorInfo.getOauthVersion()));
        oAuthAuthenticatorBuilder.accessTokenEndpoint(oAuthAuthenticatorInfo.getAccessTokenEndpoint());
        oAuthAuthenticatorBuilder.accessTokenInput(oAuthAuthenticatorInfo.getAccessTokenInput());
        oAuthAuthenticatorBuilder.requestTokenEndpoint(oAuthAuthenticatorInfo.getRequestTokenEndpoint());
        oAuthAuthenticatorBuilder.authorizationUrl(oAuthAuthenticatorInfo.getAuthorizationUrl());
        oAuthAuthenticatorBuilder.callbackPort(oAuthAuthenticatorInfo.getCallbackPort());
        oAuthAuthenticatorBuilder.accessTokenPublicKey(oAuthAuthenticatorInfo.getAccessTokenPublicKey());
        return oAuthAuthenticatorBuilder.build();
    }
}

