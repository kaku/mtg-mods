/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.ProgressEvent;
import java.util.EventListener;

public interface ProgressListener
extends EventListener {
    public void handleProgress(ProgressEvent var1);
}

