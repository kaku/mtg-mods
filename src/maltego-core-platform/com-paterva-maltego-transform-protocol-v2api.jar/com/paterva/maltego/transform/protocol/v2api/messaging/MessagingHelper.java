/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;

public class MessagingHelper {
    private static final boolean ENABLE_RECEIVE_LOGGING = false;
    private static final boolean ENABLE_SEND_LOGGING = false;

    private MessagingHelper() {
    }

    public static MaltegoMessageWrapper read(InputStream inputStream, String string) throws IOException {
        if (string != null && string.contains("gzip")) {
            GZIPInputStream gZIPInputStream = new GZIPInputStream(inputStream);
            return MessagingHelper.read(gZIPInputStream);
        }
        return MessagingHelper.read(inputStream);
    }

    public static MaltegoMessageWrapper read(InputStream inputStream) throws IOException {
        MaltegoMessageWrapper maltegoMessageWrapper = (MaltegoMessageWrapper)MessagingHelper.serializer().read(MaltegoMessageWrapper.class, inputStream);
        return maltegoMessageWrapper;
    }

    public static void write(MaltegoMessageWrapper maltegoMessageWrapper, OutputStream outputStream) throws IOException {
        MessagingHelper.serializer().write((Object)maltegoMessageWrapper, outputStream);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MaltegoMessageWrapper read(String string) throws XmlSerializationException, IOException {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            throw new IOException("<Empty Response>");
        }
        StringReader stringReader = null;
        try {
            MaltegoMessageWrapper maltegoMessageWrapper;
            stringReader = new StringReader(string);
            MaltegoMessageWrapper maltegoMessageWrapper2 = maltegoMessageWrapper = (MaltegoMessageWrapper)MessagingHelper.serializer().read(MaltegoMessageWrapper.class, (Reader)stringReader);
            return maltegoMessageWrapper2;
        }
        finally {
            if (stringReader != null) {
                try {
                    stringReader.close();
                }
                catch (IOException var4_4) {}
            }
        }
    }

    private static XmlSerializer serializer() {
        return new XmlSerializer();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public static String toString(MaltegoMessageWrapper maltegoMessageWrapper) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ByteArrayInputStream byteArrayInputStream = null;
        String string = null;
        MessagingHelper.write(maltegoMessageWrapper, byteArrayOutputStream);
        byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        string = MessagingHelper.toString(byteArrayInputStream);
        try {
            byteArrayOutputStream.close();
            if (byteArrayInputStream == null) return string;
            byteArrayInputStream.close();
            return string;
        }
        catch (IOException var4_4) {
            return string;
        }
        catch (Exception exception) {
            try {
                byteArrayOutputStream.close();
                if (byteArrayInputStream == null) return string;
                byteArrayInputStream.close();
                return string;
            }
            catch (IOException var4_6) {
                return string;
            }
            catch (Throwable throwable) {
                try {
                    byteArrayOutputStream.close();
                    if (byteArrayInputStream == null) throw throwable;
                    byteArrayInputStream.close();
                    throw throwable;
                }
                catch (IOException var6_8) {
                    // empty catch block
                }
                throw throwable;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String toString(InputStream inputStream) {
        StringBuffer stringBuffer;
        stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String string = null;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuffer.append(string);
                stringBuffer.append("\n");
            }
        }
        catch (UnsupportedEncodingException var3_5) {
            var3_5.printStackTrace();
        }
        catch (IOException var3_7) {
            var3_7.printStackTrace();
        }
        finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                }
                catch (Exception var3_8) {}
            }
        }
        return stringBuffer.toString();
    }
}

