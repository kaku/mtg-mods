/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

public class TransformCategory {
    private String _name;

    public TransformCategory(String string) {
        this._name = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public boolean equals(Object object) {
        if (object instanceof TransformCategory) {
            return this.equals((TransformCategory)object);
        }
        return false;
    }

    public boolean equals(TransformCategory transformCategory) {
        return this.getName().equals(transformCategory.getName());
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    public String toString() {
        return this.getName();
    }
}

