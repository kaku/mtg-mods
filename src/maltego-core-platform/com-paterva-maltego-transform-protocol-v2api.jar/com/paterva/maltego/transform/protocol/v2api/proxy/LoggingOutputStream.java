/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class LoggingOutputStream
extends OutputStream {
    private OutputStream _os;
    private ByteArrayOutputStream _logBuffer;

    public LoggingOutputStream(OutputStream outputStream) {
        this._os = outputStream;
        this._logBuffer = new ByteArrayOutputStream();
    }

    @Override
    public void write(int n) throws IOException {
        this._logBuffer.write(n);
        this._os.write(n);
    }

    @Override
    public void close() throws IOException {
        System.out.println(this._logBuffer.toString("UTF-8"));
        this._logBuffer.close();
        super.close();
    }
}

