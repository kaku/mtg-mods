/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.transform.protocol.v2api.api.TransformFindException;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;
import java.util.ArrayList;

public class RemoteTransformFindException
extends TransformFindException {
    public RemoteTransformFindException(ExceptionResponse exceptionResponse) {
        super(RemoteTransformFindException.getExceptions(exceptionResponse));
    }

    public RemoteTransformFindException(String string) {
        super(string);
    }

    private static String getExceptions(ExceptionResponse exceptionResponse) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Exceptions on remote server: \n");
        for (ExceptionResponse.MaltegoException maltegoException : exceptionResponse.getExceptions()) {
            stringBuffer.append(maltegoException.getMessage());
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }
}

