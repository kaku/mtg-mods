/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import com.paterva.maltego.transform.protocol.v2api.messaging.DisplayInfo;
import java.util.ArrayList;

public class DisplayInfoCollection
extends ArrayList<DisplayInfo> {
    public void add(String string, String string2, String string3) {
        this.add(new DisplayInfo(string, string2, string3));
    }

    public void add(String string, String string2) {
        this.add(new DisplayInfo(string, string2));
    }

    public void addHtml(String string, String string2) {
        this.add(new DisplayInfo(string, string2, "text/html"));
    }

    public void addText(String string, String string2) {
        this.add(new DisplayInfo(string, string2, "text/plain"));
    }
}

