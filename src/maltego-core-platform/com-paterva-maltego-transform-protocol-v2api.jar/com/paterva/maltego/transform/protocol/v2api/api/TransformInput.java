/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import java.util.HashMap;
import java.util.Map;

public class TransformInput {
    private String _key;
    private String _proxyServer;
    private int _proxyPort = 0;
    private int _softLimit = 10;
    private int _hardLimit = 10;
    private Map<String, Object> _inputValues;
    private String _userAgent;

    public Map<String, Object> getTransformParameters() {
        if (this._inputValues == null) {
            this._inputValues = new HashMap<String, Object>(5);
        }
        return this._inputValues;
    }

    public String getUserAgent() {
        return this._userAgent;
    }

    public void setUserAgent(String string) {
        this._userAgent = string;
    }

    public String getProxyServer() {
        return this._proxyServer;
    }

    public void setProxyServer(String string) {
        this._proxyServer = string;
    }

    public int getProxyPort() {
        return this._proxyPort;
    }

    public void setProxyPort(int n) {
        this._proxyPort = n;
    }

    public String getKey() {
        return this._key;
    }

    public void setKey(String string) {
        this._key = string;
    }

    public int getHardLimit() {
        return this._hardLimit;
    }

    public void setHardLimit(int n) {
        this._hardLimit = n;
    }

    public int getSoftLimit() {
        return this._softLimit;
    }

    public void setSoftLimit(int n) {
        this._softLimit = n;
    }
}

