/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateMismatchException
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryInfo;
import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryResult;
import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryServer;
import com.paterva.maltego.transform.protocol.v2api.api.ProgressEvent;
import com.paterva.maltego.transform.protocol.v2api.api.ProgressListener;
import com.paterva.maltego.transform.protocol.v2api.api.TransformApplicationDescriptor;
import com.paterva.maltego.transform.protocol.v2api.api.TransformSource;
import com.paterva.maltego.transform.protocol.v2api.cert.CertificateMismatchHandler;
import com.paterva.maltego.util.http.CertificateMismatchException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DiscoveryStrategy {
    private static final Logger LOG = Logger.getLogger(DiscoveryStrategy.class.getName());
    private final List<ProgressListener> _listeners = Collections.synchronizedList(new LinkedList());
    private final ExecutorService _executor = Executors.newCachedThreadPool();

    public DiscoveryResult doDiscovery(TransformSource transformSource, Collection<DiscoveryServer> collection) {
        Set<TransformApplicationDescriptor> set = Collections.synchronizedSet(new HashSet());
        Set<DiscoveryServer> set2 = Collections.synchronizedSet(new HashSet());
        List<Exception> list = Collections.synchronizedList(new ArrayList());
        for (DiscoveryServer discoveryServer : collection) {
            this.findServers(discoveryServer.getUrl().toString(), transformSource, discoveryServer, set2, set, list);
        }
        this._executor.shutdown();
        try {
            this._executor.awaitTermination(1, TimeUnit.HOURS);
        }
        catch (InterruptedException var6_7) {
            Thread.currentThread().interrupt();
        }
        return new DiscoveryResult(set, list);
    }

    public void addProgressListener(ProgressListener progressListener) {
        this._listeners.add(progressListener);
    }

    public void removeProgressListener(ProgressListener progressListener) {
        this._listeners.remove(progressListener);
    }

    private void fireProgress(String string) {
        ProgressListener[] arrprogressListener = this._listeners.toArray(new ProgressListener[0]);
        for (int i = 0; i < arrprogressListener.length; ++i) {
            arrprogressListener[i].handleProgress(new ProgressEvent(this, string));
        }
    }

    private void findServers(final String string, final TransformSource transformSource, final DiscoveryServer discoveryServer, final Set<DiscoveryServer> set, final Set<TransformApplicationDescriptor> set2, final List<Exception> list) {
        this._executor.execute(new Runnable(){

            @Override
            public void run() {
                try {
                    if (set.add(discoveryServer)) {
                        DiscoveryStrategy.this.fireProgress("Trying... " + discoveryServer);
                        String string2 = discoveryServer.getUrl().toString();
                        boolean bl = string.equals(string2);
                        DiscoveryInfo discoveryInfo = transformSource.getDiscoveryInfo(string, string2, !bl);
                        int n = 0;
                        set2.addAll(discoveryInfo.getApplications());
                        n = discoveryInfo.getApplications().size();
                        DiscoveryStrategy.this.fireProgress("Found " + n + " applications.");
                        for (DiscoveryServer discoveryServer2 : discoveryInfo.getServers()) {
                            try {
                                DiscoveryStrategy.this.findServers(string, transformSource, discoveryServer2, set, set2, list);
                            }
                            catch (Exception var7_9) {
                                list.add(var7_9);
                            }
                        }
                    }
                }
                catch (CertificateMismatchException var1_2) {
                    list.add(var1_2);
                    CertificateMismatchHandler.showNotification(var1_2.getUrl(), var1_2.getOldCertificate(), var1_2.getNewCertificate());
                }
                catch (Exception var1_3) {
                    list.add(var1_3);
                    LOG.log(Level.WARNING, "Error during discovery: {0}", var1_3.getMessage());
                }
            }
        });
    }

}

