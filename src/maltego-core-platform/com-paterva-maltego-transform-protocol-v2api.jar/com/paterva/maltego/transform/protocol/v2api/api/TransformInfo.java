/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.transform.protocol.v2api.api.TransformCategory;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInputDescriptor;
import com.paterva.maltego.util.StringUtilities;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TransformInfo {
    private String _helpUrl;
    private String _name;
    private String _displayName;
    private String _description;
    private String _author;
    private String _owner;
    private String _disclaimer;
    private String _locationRelevance;
    private String _version;
    private String _inputEntity;
    private String _authenticator;
    private int _maxInputs;
    private int _maxOutputs;
    private TransformCategory _category;
    private boolean _enabled = true;
    private boolean _disclaimerAccepted = false;
    private String[] _outputEntities;
    private Set<TransformInputDescriptor> _inputs;
    private List _listeners = Collections.synchronizedList(new LinkedList());

    public TransformInfo() {
    }

    public TransformInfo(TransformInfo transformInfo) {
        this();
        this.setAuthor(transformInfo.getAuthor());
        this.setCategory(transformInfo.getCategory());
        this.setDescription(transformInfo.getDescription());
        this.setDisclaimer(transformInfo.getDisclaimer());
        this.setDisplayName(transformInfo.getDisplayName());
        this.setEnabled(transformInfo.getEnabled());
        this.setInputEntityType(transformInfo.getInputEntityType());
        this.setLocationRelevance(transformInfo.getLocationRelevance());
        this.setMaxInputs(transformInfo.getMaxInputs());
        this.setMaxOutputs(transformInfo.getMaxOutputs());
        this.setName(transformInfo.getName());
        this.setOwner(transformInfo.getOwner());
        this.setVersion(transformInfo.getVersion());
        this.setHelpUrl(transformInfo.getHelpUrl());
        this.setAuthenticator(transformInfo.getAuthenticator());
        String[] arrstring = new String[transformInfo.getOutputEntityTypes().length];
        int n = 0;
        for (String string : transformInfo.getOutputEntityTypes()) {
            arrstring[n++] = new String(string);
        }
        this.setOutputEntityTypes(arrstring);
        for (TransformInputDescriptor transformInputDescriptor : transformInfo.getInputs()) {
            this.getInputs().add(transformInputDescriptor.clone());
        }
    }

    public boolean equals(Object object) {
        if (object instanceof TransformInfo) {
            return this.equals((TransformInfo)object);
        }
        return false;
    }

    public boolean equals(TransformInfo transformInfo) {
        boolean bl = this.getName().equalsIgnoreCase(transformInfo.getName());
        boolean bl2 = this.getVersion().equalsIgnoreCase(transformInfo.getVersion());
        boolean bl3 = this.getDisplayName().compareTo(transformInfo.getDisplayName()) == 0;
        return bl && bl2 && bl3;
    }

    public boolean hasDisclaimer() {
        return !StringUtilities.isNullOrEmpty((String)this.getDisclaimer());
    }

    public int hashCode() {
        return (this.getName() + this.getVersion()).hashCode();
    }

    public String toString() {
        return this.getName() + "(" + this.getVersion() + ")";
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._listeners.add(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._listeners.remove(propertyChangeListener);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        PropertyChangeListener[] arrpropertyChangeListener = this._listeners.toArray(new PropertyChangeListener[0]);
        for (int i = 0; i < arrpropertyChangeListener.length; ++i) {
            arrpropertyChangeListener[i].propertyChange(new PropertyChangeEvent(this, string, object, object2));
        }
    }

    public Set<TransformInputDescriptor> getInputs() {
        if (this._inputs == null) {
            this._inputs = new HashSet<TransformInputDescriptor>(5);
        }
        return this._inputs;
    }

    public String[] getOutputEntityTypes() {
        return this._outputEntities;
    }

    public void setOutputEntityTypes(String[] arrstring) {
        this._outputEntities = arrstring;
    }

    public String getHelpUrl() {
        return this._helpUrl;
    }

    public void setHelpUrl(String string) {
        this._helpUrl = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public TransformCategory getCategory() {
        return this._category;
    }

    public void setCategory(TransformCategory transformCategory) {
        this._category = transformCategory;
    }

    public boolean getEnabled() {
        return this._enabled;
    }

    public void setEnabled(boolean bl) {
        if (bl != this._enabled) {
            this._enabled = bl;
            this.firePropertyChange("enabled", !bl, bl);
        }
        this.firePropertyChange("enabled", !bl, bl);
    }

    public boolean isDisclaimerAccepted() {
        return this._disclaimerAccepted;
    }

    public void setDisclaimerAccepted(boolean bl) {
        if (bl != this._disclaimerAccepted) {
            this._disclaimerAccepted = bl;
            this.firePropertyChange("disclaimerAccepted", !bl, bl);
        }
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getAuthor() {
        return this._author;
    }

    public void setAuthor(String string) {
        this._author = string;
    }

    public String getOwner() {
        return this._owner;
    }

    public void setOwner(String string) {
        this._owner = string;
    }

    public String getDisclaimer() {
        return this._disclaimer;
    }

    public void setDisclaimer(String string) {
        this._disclaimer = string;
    }

    public String getLocationRelevance() {
        return this._locationRelevance;
    }

    public void setLocationRelevance(String string) {
        this._locationRelevance = string;
    }

    public String getVersion() {
        return this._version;
    }

    public void setVersion(String string) {
        this._version = string;
    }

    public String getInputEntityType() {
        return this._inputEntity;
    }

    public void setInputEntityType(String string) {
        this._inputEntity = string;
    }

    public int getMaxInputs() {
        return this._maxInputs;
    }

    public void setMaxInputs(int n) {
        this._maxInputs = n;
    }

    public int getMaxOutputs() {
        return this._maxOutputs;
    }

    public void setMaxOutputs(int n) {
        this._maxOutputs = n;
    }

    public String getAuthenticator() {
        return this._authenticator;
    }

    public void setAuthenticator(String string) {
        this._authenticator = string;
    }

    public boolean requiresInput() {
        for (TransformInputDescriptor transformInputDescriptor : this.getInputs()) {
            if (!transformInputDescriptor.requiresInput()) continue;
            return true;
        }
        return false;
    }
}

