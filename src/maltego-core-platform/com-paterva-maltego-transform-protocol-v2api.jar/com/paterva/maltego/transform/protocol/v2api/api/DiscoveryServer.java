/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import java.net.URL;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Server", strict=0)
public class DiscoveryServer {
    private URL _url;
    private String _name;

    public DiscoveryServer() {
    }

    public DiscoveryServer(URL uRL) {
        this(uRL, "");
    }

    public DiscoveryServer(URL uRL, String string) {
        this._url = uRL;
        this._name = string;
    }

    public boolean isEmpty() {
        return this.getUrl() == null;
    }

    @Attribute(name="name", required=0)
    public String getName() {
        return this._name;
    }

    @Attribute(name="name", required=0)
    public void setName(String string) {
        this._name = string;
    }

    @Attribute(name="url")
    public URL getUrl() {
        return this._url;
    }

    @Attribute(name="url")
    public void setUrl(URL uRL) {
        this._url = uRL;
    }

    public boolean equals(Object object) {
        if (object instanceof DiscoveryServer) {
            return this.equals((DiscoveryServer)object);
        }
        return false;
    }

    private String getSitefromURL(String string) {
        if (string.length() > 0 && string.indexOf("/") > 0) {
            String string2 = "";
            String[] arrstring = string.split("/");
            string2 = arrstring[2];
            if (string2.indexOf(" ") > 0) {
                string2 = string2.substring(0, string2.indexOf(" "));
            }
            return string2;
        }
        return "";
    }

    public int hashCode() {
        int n = 7;
        n = 53 * n + (this._url != null ? this._url.hashCode() : 0);
        return n;
    }

    public boolean equals(DiscoveryServer discoveryServer) {
        return this.getUrl().equals(discoveryServer.getUrl());
    }

    public String toString() {
        return this.getName() + ":" + this.getUrl();
    }
}

