/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.transform.protocol.v2api.api.TransformInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInputDescriptor;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListInputDescriptor;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformListResponse;
import com.paterva.maltego.transform.protocol.v2api.remote.ValueConverter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class RemoteTransformTranslator {
    public TransformInfo translate(TransformListResponse.TransformDescriptor transformDescriptor) throws Exception {
        TransformInfo transformInfo = new TransformInfo();
        transformInfo.setAuthor(transformDescriptor.getAuthor());
        transformInfo.setDescription(transformDescriptor.getDescription());
        transformInfo.setDisclaimer(transformDescriptor.getDisclaimer());
        transformInfo.setDisplayName(transformDescriptor.getDisplayName());
        transformInfo.setInputEntityType(transformDescriptor.getInputEntity());
        transformInfo.setLocationRelevance(transformDescriptor.getLocationRelevance());
        transformInfo.setMaxInputs(transformDescriptor.getMaxInputs());
        transformInfo.setMaxOutputs(transformDescriptor.getMaxOutputs());
        transformInfo.setName(transformDescriptor.getName());
        transformInfo.setAuthenticator(transformDescriptor.getAuthenticator());
        String[] arrstring = new String[transformDescriptor.getOuputEntities().size()];
        for (int i = 0; i < arrstring.length; ++i) {
            arrstring[i] = transformDescriptor.getOuputEntities().get((int)i).Name;
        }
        transformInfo.setOutputEntityTypes(arrstring);
        transformInfo.setOwner(transformDescriptor.getOwner());
        transformInfo.setVersion(transformDescriptor.getVersion());
        this.copyInputs(transformInfo, transformDescriptor.getInputs());
        return transformInfo;
    }

    private void copyInputs(TransformInfo transformInfo, Collection<TransformListInputDescriptor> collection) throws Exception {
        if (collection != null) {
            Set<TransformInputDescriptor> set = transformInfo.getInputs();
            for (TransformInputDescriptor transformInputDescriptor : this.translateInputs(collection)) {
                set.add(transformInputDescriptor);
            }
        }
    }

    public Set<TransformInputDescriptor> translateInputs(Collection<TransformListInputDescriptor> collection) throws Exception {
        HashSet<TransformInputDescriptor> hashSet = new HashSet<TransformInputDescriptor>();
        if (collection != null) {
            for (TransformListInputDescriptor transformListInputDescriptor : collection) {
                hashSet.add(this.translate(transformListInputDescriptor));
            }
        }
        return hashSet;
    }

    private TransformInputDescriptor translate(TransformListInputDescriptor transformListInputDescriptor) throws Exception {
        TransformInputDescriptor transformInputDescriptor = new TransformInputDescriptor();
        transformInputDescriptor.setName(transformListInputDescriptor.getName());
        transformInputDescriptor.setDisplayName(transformListInputDescriptor.getDisplayName());
        transformInputDescriptor.setRequired(!transformListInputDescriptor.getOptional());
        transformInputDescriptor.setPopup(transformListInputDescriptor.isPopup());
        transformInputDescriptor.setAuth(transformListInputDescriptor.isAuth());
        Class class_ = ValueConverter.getType(transformListInputDescriptor.getTypeName());
        if (class_ == null) {
            throw new Exception("Could not get class for xml type " + transformListInputDescriptor.getTypeName());
        }
        transformInputDescriptor.setType(class_);
        if (transformListInputDescriptor.getDefaultValue() != null) {
            transformInputDescriptor.setDefaultValue(ValueConverter.read(transformListInputDescriptor.getDefaultValue(), class_));
        }
        return transformInputDescriptor;
    }

    public Collection<TransformInfo> translate(Collection<TransformListResponse.TransformDescriptor> collection) throws Exception {
        ArrayList<TransformInfo> arrayList = new ArrayList<TransformInfo>();
        for (TransformListResponse.TransformDescriptor transformDescriptor : collection) {
            arrayList.add(this.translate(transformDescriptor));
        }
        return arrayList;
    }
}

