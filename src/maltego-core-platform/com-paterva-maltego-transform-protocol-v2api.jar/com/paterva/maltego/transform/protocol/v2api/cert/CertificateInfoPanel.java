/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.http.CertificateUtils
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.protocol.v2api.cert;

import com.paterva.maltego.util.http.CertificateUtils;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.security.cert.X509Certificate;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class CertificateInfoPanel
extends JPanel {
    private JTextArea _newCertTextArea;
    private JTextArea _oldCertTextArea;

    public CertificateInfoPanel() {
    }

    public CertificateInfoPanel(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        this.initComponents();
        this._oldCertTextArea.setText(CertificateUtils.toPrettyString2((X509Certificate)x509Certificate));
        this._newCertTextArea.setText(CertificateUtils.toPrettyString2((X509Certificate)x509Certificate2));
        this._oldCertTextArea.setCaretPosition(0);
        this._newCertTextArea.setCaretPosition(0);
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        JScrollPane jScrollPane = new JScrollPane();
        this._oldCertTextArea = new JTextArea();
        JLabel jLabel2 = new JLabel();
        JScrollPane jScrollPane2 = new JScrollPane();
        this._newCertTextArea = new JTextArea();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(CertificateInfoPanel.class, (String)"CertificateInfoPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 0, 3, 0);
        this.add((Component)jLabel, gridBagConstraints);
        this._oldCertTextArea.setEditable(false);
        this._oldCertTextArea.setColumns(20);
        this._oldCertTextArea.setRows(5);
        jScrollPane.setViewportView(this._oldCertTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.add((Component)jScrollPane, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel2, (String)NbBundle.getMessage(CertificateInfoPanel.class, (String)"CertificateInfoPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 6, 3, 0);
        this.add((Component)jLabel2, gridBagConstraints);
        this._newCertTextArea.setEditable(false);
        this._newCertTextArea.setColumns(20);
        this._newCertTextArea.setRows(5);
        jScrollPane2.setViewportView(this._newCertTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        this.add((Component)jScrollPane2, gridBagConstraints);
    }
}

