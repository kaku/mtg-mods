/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

public class OAuthAuthenticatorInfo {
    private String _name;
    private String _displayName;
    private String _description;
    private String _oauthVersion;
    private Integer _callbackPort;
    private String _accessTokenEndpoint;
    private String _requestTokenEndpoint;
    private String _authorizationUrl;
    private String _appKey;
    private String _appSecret;
    private String _icon;
    private String _accessTokenInput;
    private String _accessTokenPublicKey;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getOauthVersion() {
        return this._oauthVersion;
    }

    public void setOAuthVersion(String string) {
        this._oauthVersion = string;
    }

    public String getAccessTokenEndpoint() {
        return this._accessTokenEndpoint;
    }

    public void setAccessTokenEndpoint(String string) {
        this._accessTokenEndpoint = string;
    }

    public String getRequestTokenEndpoint() {
        return this._requestTokenEndpoint;
    }

    public void setRequestTokenEndpoint(String string) {
        this._requestTokenEndpoint = string;
    }

    public String getAuthorizationUrl() {
        return this._authorizationUrl;
    }

    public void setAuthorizationUrl(String string) {
        this._authorizationUrl = string;
    }

    public String getAppKey() {
        return this._appKey;
    }

    public void setAppKey(String string) {
        this._appKey = string;
    }

    public String getAppSecret() {
        return this._appSecret;
    }

    public void setAppSecret(String string) {
        this._appSecret = string;
    }

    public String getIcon() {
        return this._icon;
    }

    public void setIcon(String string) {
        this._icon = string;
    }

    public Integer getCallbackPort() {
        return this._callbackPort;
    }

    public void setCallbackPort(Integer n) {
        this._callbackPort = n;
    }

    public String getAccessTokenInput() {
        return this._accessTokenInput;
    }

    public void setAccessTokenInput(String string) {
        this._accessTokenInput = string;
    }

    public String getAccessTokenPublicKey() {
        return this._accessTokenPublicKey;
    }

    public void setAccessTokenPublicKey(String string) {
        this._accessTokenPublicKey = string;
    }

    public String toString() {
        return "OAuthAuthenticatorInfo{\n      name=" + this._name + ",\n      displayName=" + this._displayName + ",\n      description=" + this._description + ",\n      oauthVersion=" + this._oauthVersion + ",\n      callbackPort=" + this._callbackPort + ",\n      accessTokenEndpoint=" + this._accessTokenEndpoint + ",\n      requestTokenEndpoint=" + this._requestTokenEndpoint + ",\n      authorizationUrl=" + this._authorizationUrl + ",\n      appKey=" + this._appKey + ",\n      appSecret=" + this._appSecret + ",\n      icon=" + this._icon + ",\n      accessTokenInput=" + this._accessTokenInput + ",\n      accessTokenPublicKey=" + this._accessTokenPublicKey + "\n   }";
    }
}

