/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.EntityConstraint
 *  com.paterva.maltego.transform.descriptor.PersistenceMode
 *  com.paterva.maltego.transform.descriptor.StealthLevel
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.EntityConstraint;
import com.paterva.maltego.transform.descriptor.PersistenceMode;
import com.paterva.maltego.transform.descriptor.StealthLevel;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator;
import com.paterva.maltego.transform.protocol.v2api.RemoteTransformAdapterV2;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInputDescriptor;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import java.net.URL;
import java.util.ArrayList;
import java.util.Set;

public class TransformTranslator {
    private final Set<TransformSet> _sets;

    public TransformTranslator(Set<TransformSet> set) {
        this._sets = set;
    }

    public TransformDescriptor translate(TransformInfo transformInfo, URL uRL) {
        TransformDescriptor transformDescriptor = new TransformDescriptor(RemoteTransformAdapterV2.class.getName(), TransformTranslator.getV3Name(transformInfo.getName()), null, TransformTranslator.translate(transformInfo.getInputs()));
        transformDescriptor.setAbstract(false);
        transformDescriptor.setAuthor(transformInfo.getAuthor());
        transformDescriptor.setDescription(transformInfo.getDescription());
        transformDescriptor.setDisclaimer(transformInfo.getDisclaimer());
        transformDescriptor.setDisplayName(transformInfo.getDisplayName());
        transformDescriptor.setHelpUrl(transformInfo.getHelpUrl());
        transformDescriptor.setLocationRelevance(transformInfo.getLocationRelevance());
        transformDescriptor.setOwner(transformInfo.getOwner());
        transformDescriptor.setRequireDisplayInfo(false);
        transformDescriptor.setTemplate(false);
        transformDescriptor.setVersion(transformInfo.getVersion());
        transformDescriptor.setVisibility(Visibility.Public);
        transformDescriptor.setHelpUrl(uRL.getProtocol() + "://" + uRL.getHost() + "/cgi-bin/transformhelp.pl?transform=" + transformInfo.getName());
        transformDescriptor.setHelpText("Click on the link above for help about this transform");
        transformDescriptor.setAuthenticator(transformInfo.getAuthenticator());
        transformDescriptor.setStealthLevel(null);
        String string = EntitySpecTranslator.getV3TypeName(transformInfo.getInputEntityType());
        transformDescriptor.setInputConstraint((Constraint)new EntityConstraint(string));
        for (String string2 : transformInfo.getOutputEntityTypes()) {
            transformDescriptor.getOutputEntities().add(EntitySpecTranslator.getV3TypeName(string2));
        }
        transformDescriptor.setDefaultSets(this.findSets(transformDescriptor.getName()));
        return transformDescriptor;
    }

    private String[] findSets(String string) {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (TransformSet transformSet : this._sets) {
            if (!transformSet.contains(string)) continue;
            arrayList.add(transformSet.getName());
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public static String getV2Name(String string) {
        return string.substring(11);
    }

    public static String getV3Name(String string) {
        return "paterva.v2." + string;
    }

    public static DisplayDescriptorCollection translate(Set<TransformInputDescriptor> set) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        for (TransformInputDescriptor transformInputDescriptor : set) {
            TransformPropertyDescriptor transformPropertyDescriptor = new TransformPropertyDescriptor(transformInputDescriptor.getType(), transformInputDescriptor.getName(), transformInputDescriptor.getDisplayName());
            transformPropertyDescriptor.setDescription(transformInputDescriptor.getDescription());
            transformPropertyDescriptor.setDefaultValue(transformInputDescriptor.getDefaultValue());
            transformPropertyDescriptor.setHidden(false);
            transformPropertyDescriptor.setNullable(!transformInputDescriptor.isRequired());
            transformPropertyDescriptor.setPopup(transformInputDescriptor.isPopup());
            transformPropertyDescriptor.setReadonly(false);
            transformPropertyDescriptor.setAbstract(false);
            transformPropertyDescriptor.setPersistence(PersistenceMode.Normal);
            transformPropertyDescriptor.setVisibility(Visibility.Public);
            transformPropertyDescriptor.setAuth(transformInputDescriptor.isAuth());
            displayDescriptorList.add((DisplayDescriptor)transformPropertyDescriptor);
        }
        return displayDescriptorList;
    }
}

