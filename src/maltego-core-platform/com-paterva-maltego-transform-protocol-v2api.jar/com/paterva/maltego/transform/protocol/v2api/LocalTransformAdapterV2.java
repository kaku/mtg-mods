/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.protocol.v2api.AbstractTransformAdapterV2;
import com.paterva.maltego.transform.protocol.v2api.LocalTransformRunner;
import com.paterva.maltego.transform.protocol.v2api.TransformRunner;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.io.File;

public class LocalTransformAdapterV2
extends AbstractTransformAdapterV2 {
    private static PropertyDescriptor COMMAND = new PropertyDescriptor(String.class, "transform.local.command");
    private static PropertyDescriptor PARAMS = new PropertyDescriptor(String.class, "transform.local.parameters");
    private static PropertyDescriptor WORKING_DIR = new PropertyDescriptor(String.class, "transform.local.working-directory");
    private static PropertyDescriptor DEBUG = new PropertyDescriptor(Boolean.TYPE, "transform.local.debug");

    @Override
    protected TransformRunner runner(TransformDescriptor transformDescriptor, String string, DataSource dataSource, EntityFactory entityFactory, EntityRegistry entityRegistry) throws TransformRunException {
        String string2 = (String)dataSource.getValue(COMMAND);
        String string3 = (String)dataSource.getValue(PARAMS);
        String string4 = (String)dataSource.getValue(WORKING_DIR);
        Object object = dataSource.getValue(DEBUG);
        boolean bl = false;
        if (object != null) {
            bl = (Boolean)object;
        }
        LocalTransformRunner localTransformRunner = new LocalTransformRunner(entityFactory, entityRegistry, string2);
        localTransformRunner.setParameters(string3);
        localTransformRunner.setWorkingDir(new File(string4));
        localTransformRunner.setDebug(bl);
        return localTransformRunner;
    }

    @Override
    protected boolean applyAdvancedProperties() {
        return true;
    }
}

