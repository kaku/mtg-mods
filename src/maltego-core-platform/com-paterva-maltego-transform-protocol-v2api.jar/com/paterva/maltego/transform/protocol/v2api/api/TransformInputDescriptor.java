/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.protocol.v2api.api;

import com.paterva.maltego.util.StringUtilities;

public class TransformInputDescriptor {
    private String _display;
    private Class _type;
    private String _name;
    private boolean _required = false;
    private boolean _popup = false;
    private String _description;
    private Object _defaultValue;
    private Object _value;
    private boolean _auth = false;

    public boolean isRequired() {
        return this._required;
    }

    public void setRequired(boolean bl) {
        this._required = bl;
    }

    public boolean isPopup() {
        return this._popup;
    }

    public void setPopup(boolean bl) {
        this._popup = bl;
    }

    public Object getValue() {
        return this._value;
    }

    public void setValue(Object object) {
        this._value = object;
    }

    public Object getDefaultValue() {
        return this._defaultValue;
    }

    public void setDefaultValue(Object object) {
        this._defaultValue = object;
    }

    public String getDisplayName() {
        return this._display;
    }

    public void setDisplayName(String string) {
        this._display = string;
    }

    public String getDescription() {
        return this.getDisplayName();
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public Class getType() {
        return this._type;
    }

    public void setType(Class class_) {
        this._type = class_;
    }

    public boolean isAuth() {
        return this._auth;
    }

    public void setAuth(boolean bl) {
        this._auth = bl;
    }

    public TransformInputDescriptor clone() {
        TransformInputDescriptor transformInputDescriptor = new TransformInputDescriptor();
        transformInputDescriptor.setName(this.getName());
        transformInputDescriptor.setDisplayName(this.getDisplayName());
        transformInputDescriptor.setRequired(this.isRequired());
        transformInputDescriptor.setPopup(this.isPopup());
        transformInputDescriptor.setAuth(this.isAuth());
        Class class_ = this.getType();
        if (class_ == null) {
            // empty if block
        }
        transformInputDescriptor.setType(class_);
        if (this.getDefaultValue() != null) {
            transformInputDescriptor.setDefaultValue(this.getDefaultValue());
        }
        if (this.getValue() != null) {
            transformInputDescriptor.setValue(this.getValue());
        }
        return transformInputDescriptor;
    }

    public boolean equals(Object object) {
        if (object instanceof TransformInputDescriptor) {
            return this.equals((TransformInputDescriptor)object);
        }
        return false;
    }

    public boolean equals(TransformInputDescriptor transformInputDescriptor) {
        return this.getName().equals(transformInputDescriptor.getName());
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    public String toString() {
        return this.getName() + "=" + this.getValue() + "(" + this.getType().getName() + ")";
    }

    public boolean requiresInput() {
        if (this.isRequired() && (this.getType() == String.class ? StringUtilities.isNullOrEmpty((String)((String)this.getDefaultValue())) && StringUtilities.isNullOrEmpty((String)((String)this.getValue())) : this.getDefaultValue() == null && this.getValue() == null)) {
            return true;
        }
        return false;
    }
}

