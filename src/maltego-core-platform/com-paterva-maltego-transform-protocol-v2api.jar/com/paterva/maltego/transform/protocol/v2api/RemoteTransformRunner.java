/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.transform.runner.api.TransformTimeout
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.RequestProcessor
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator;
import com.paterva.maltego.transform.protocol.v2api.EntityTranslator;
import com.paterva.maltego.transform.protocol.v2api.TransformRunner;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInput;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformRequest;
import com.paterva.maltego.transform.protocol.v2api.remote.ValueConverter;
import com.paterva.maltego.transform.runner.api.TransformTimeout;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.openide.util.RequestProcessor;

class RemoteTransformRunner
extends TransformRunner {
    private static RequestProcessor _processor = new RequestProcessor("V2 Transform Server", 5, true);
    private Proxy _proxy;
    private String _transformName;
    private TransformInput _input;
    private String _forcedTypeName;
    private String _derivedTypeNameV2;

    public RemoteTransformRunner(EntityFactory entityFactory, EntityRegistry entityRegistry, Proxy proxy, String string, TransformInput transformInput, String string2) {
        super(entityFactory, entityRegistry);
        this._input = transformInput;
        this._transformName = string;
        this._proxy = proxy;
        this._forcedTypeName = string2;
    }

    @Override
    protected MaltegoMessageWrapper runTransform(MaltegoEntity maltegoEntity) throws TransformRunException, IOException {
        try {
            MaltegoMessageWrapper maltegoMessageWrapper = this.createRequest(maltegoEntity, this._input);
            if (Thread.interrupted()) {
                return null;
            }
            MaltegoMessageWrapper maltegoMessageWrapper2 = this._proxy.doTransform(this._transformName, maltegoMessageWrapper, this._input.getKey(), this._derivedTypeNameV2, this.getTimeout());
            if (Thread.interrupted()) {
                return null;
            }
            return maltegoMessageWrapper2;
        }
        catch (Exception var2_3) {
            if (var2_3 instanceof TransformRunException) {
                throw (TransformRunException)var2_3;
            }
            throw new TransformRunException(var2_3);
        }
    }

    private int getTimeout() {
        Object object = this._input.getTransformParameters().get("maltego.transform.timeout");
        int n = -1;
        if (object instanceof Integer) {
            n = (Integer)object;
        }
        return n >= 0 ? n : TransformTimeout.getTimeout();
    }

    private MaltegoMessageWrapper createRequest(MaltegoEntity maltegoEntity, TransformInput transformInput) throws Exception {
        TransformRequest transformRequest = new TransformRequest();
        if (!StringUtilities.isNullOrEmpty((String)this._forcedTypeName)) {
            if (!this._forcedTypeName.equals(maltegoEntity.getTypeName())) {
                this._derivedTypeNameV2 = EntitySpecTranslator.getV2TypeName(maltegoEntity.getTypeName());
            }
            maltegoEntity = maltegoEntity.createClone();
            maltegoEntity.setTypeName(this._forcedTypeName);
        }
        EntityDescriptor entityDescriptor = EntityTranslator.instance().translate(maltegoEntity, this.getRegistry());
        transformRequest.getEntities().add(entityDescriptor);
        transformRequest.setLimits(new TransformRequest.Limit(transformInput.getSoftLimit(), transformInput.getHardLimit()));
        RemoteTransformRunner.copyInputs(transformRequest, transformInput.getTransformParameters());
        return new MaltegoMessageWrapper(transformRequest);
    }

    private static void copyInputs(TransformRequest transformRequest, Map<String, Object> map) throws Exception {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            TransformRequest.Input input = new TransformRequest.Input();
            input.setName(entry.getKey());
            Object object = entry.getValue();
            input.setValue(object == null ? "" : ValueConverter.write(object, object.getClass()));
            transformRequest.getInputs().add(input);
        }
    }

    @Override
    protected RequestProcessor processor() {
        return _processor;
    }
}

