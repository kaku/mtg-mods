/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.api;

public enum NotificationType {
    Debug,
    Info,
    Error,
    Fatal;
    

    private NotificationType() {
    }
}

