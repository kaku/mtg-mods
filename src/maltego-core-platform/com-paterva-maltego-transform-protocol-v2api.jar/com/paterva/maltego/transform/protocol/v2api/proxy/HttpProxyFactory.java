/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.proxy;

import com.paterva.maltego.transform.protocol.v2api.MaltegoHelper;
import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyFactory;
import com.paterva.maltego.transform.protocol.v2api.proxy.HttpProxy;

public class HttpProxyFactory
extends ProxyFactory {
    @Override
    public Proxy createProxy(String string) {
        HttpProxy httpProxy = new HttpProxy(string);
        httpProxy.setUserAgent(MaltegoHelper.getUserAgent());
        return httpProxy;
    }
}

