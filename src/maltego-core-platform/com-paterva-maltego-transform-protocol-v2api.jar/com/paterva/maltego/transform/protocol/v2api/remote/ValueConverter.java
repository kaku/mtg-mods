/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DefaultStringConverter
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateRangePresets
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.FixedDateRange
 *  com.paterva.maltego.typing.types.PresetsRelative
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.protocol.v2api.remote;

import com.paterva.maltego.typing.DefaultStringConverter;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateRangePresets;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.FixedDateRange;
import com.paterva.maltego.typing.types.PresetsRelative;
import com.paterva.maltego.util.FastURL;
import java.text.DecimalFormat;
import java.util.Date;

public class ValueConverter {
    private ValueConverter() {
    }

    public static Object read(String string, Class class_) throws Exception {
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(class_);
        if (typeDescriptor != null) {
            return typeDescriptor.convert(string);
        }
        return DefaultStringConverter.instance().convertFrom(string, class_);
    }

    public static String write(Object object, Class class_) throws Exception {
        if (object instanceof DateRange) {
            return ValueConverter.toFixedDateRange((DateRange)object);
        }
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(class_);
        if (typeDescriptor != null) {
            return typeDescriptor.convert(object);
        }
        return DefaultStringConverter.instance().convertTo(object, class_);
    }

    private static String toFixedDateRange(DateRange dateRange) {
        if (dateRange.isRelative()) {
            FixedDateRange fixedDateRange = DateRangePresets.getPresetDateRange((PresetsRelative)dateRange.getRelativeItem());
            dateRange = new DateRange(fixedDateRange);
        }
        return ValueConverter.formatDateRangeForTransforms(dateRange);
    }

    private static String formatDateRangeForTransforms(DateRange dateRange) {
        String string = ValueConverter.formatAsUnixTimeWithSubSeconds(dateRange.getFromDate());
        String string2 = ValueConverter.formatAsUnixTimeWithSubSeconds(dateRange.getToDate());
        return string + "-" + string2;
    }

    private static String formatAsUnixTimeWithSubSeconds(DateTime dateTime) {
        DecimalFormat decimalFormat = new DecimalFormat(".000");
        double d = (double)dateTime.getTime() / 1000.0;
        String string = decimalFormat.format(d);
        return string;
    }

    public static String getTypeName(Class class_) {
        if (class_ == String.class) {
            return "string";
        }
        if (class_ == Date.class) {
            return "date";
        }
        if (class_ == DateTime.class) {
            return "datetime";
        }
        if (class_ == DateRange.class) {
            return "daterange";
        }
        if (class_ == Integer.TYPE) {
            return "int";
        }
        if (class_ == Long.TYPE) {
            return "long";
        }
        if (class_ == Boolean.TYPE) {
            return "boolean";
        }
        if (class_ == Double.TYPE) {
            return "double";
        }
        if (class_ == FastURL.class) {
            return "url";
        }
        return null;
    }

    public static Class getType(String string) {
        if (string == null) {
            return null;
        }
        if (string.equals("string")) {
            return String.class;
        }
        if (string.equals("dateTime")) {
            return Date.class;
        }
        if (string.equals("date")) {
            return Date.class;
        }
        if (string.equals("datetime")) {
            return DateTime.class;
        }
        if (string.equals("daterange")) {
            return DateRange.class;
        }
        if (string.equals("int")) {
            return Integer.TYPE;
        }
        if (string.equals("long")) {
            return Long.TYPE;
        }
        if (string.equals("boolean")) {
            return Boolean.TYPE;
        }
        if (string.equals("double")) {
            return Double.TYPE;
        }
        if (string.equals("url")) {
            return FastURL.class;
        }
        return null;
    }
}

