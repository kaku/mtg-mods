/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class V2TransformSetRepository {
    private static final String MALTEGO_FOLDER = "Maltego/V2Compatibility";
    private static final String SET_FOLDER = "TransformSets";
    private static final String ATTR_DESCRIPTION = "description";
    private Set<TransformSet> _sets;

    private Set<TransformSet> sets() {
        if (this._sets == null) {
            this._sets = new HashSet<TransformSet>();
            try {
                FileObject fileObject = V2TransformSetRepository.getOrCreateSetFolder();
                if (fileObject != null) {
                    for (FileObject fileObject2 : fileObject.getChildren()) {
                        TransformSet transformSet = V2TransformSetRepository.loadSet(fileObject2);
                        transformSet.markClean();
                        this._sets.add(transformSet);
                    }
                }
            }
            catch (IOException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
        return this._sets;
    }

    public Set<TransformSet> allSets() {
        return this.sets();
    }

    public boolean contains(String string) {
        TransformSet transformSet = this.get(string);
        if (transformSet != null) {
            return true;
        }
        return false;
    }

    public TransformSet get(String string) {
        for (TransformSet transformSet : this.sets()) {
            if (!transformSet.getName().equals(string)) continue;
            return transformSet;
        }
        return null;
    }

    private static TransformSet loadSet(FileObject fileObject) {
        TransformSet transformSet = new TransformSet(fileObject.getNameExt());
        transformSet.setDescription((String)fileObject.getAttribute("description"));
        for (FileObject fileObject2 : fileObject.getChildren()) {
            transformSet.addTransform(fileObject2.getNameExt());
        }
        return transformSet;
    }

    private static FileObject getOrCreateSetFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)V2TransformSetRepository.getOrCreateMaltegoFolder(), (String)"TransformSets");
    }

    private static FileObject getOrCreateMaltegoFolder() throws IOException {
        FileObject fileObject = FileUtil.getConfigRoot();
        return FileUtilities.getOrCreate((FileObject)fileObject, (String)"Maltego/V2Compatibility");
    }
}

