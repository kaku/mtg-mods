/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

class NotificationTranslator {
    public TransformMessage translate(TransformResponse.Notification notification) {
        TransformMessage transformMessage = new TransformMessage(new Date(), this.translate(notification.getType()), notification.getValue());
        return transformMessage;
    }

    private TransformMessage.Severity translate(String string) {
        if (string == null) {
            throw new IllegalArgumentException("Type cannot be null");
        }
        if (string.equals("Debug")) {
            return TransformMessage.Severity.Debug;
        }
        if (string.equals("Inform")) {
            return TransformMessage.Severity.Info;
        }
        if (string.equals("PartialError")) {
            return TransformMessage.Severity.Warning;
        }
        if (string.equals("FatalError")) {
            return TransformMessage.Severity.Error;
        }
        throw new IllegalArgumentException("Invalid type " + string);
    }

    public List<TransformMessage> translate(Collection<TransformResponse.Notification> collection) {
        if (collection == null || collection.size() == 0) {
            return new ArrayList<TransformMessage>(0);
        }
        ArrayList<TransformMessage> arrayList = new ArrayList<TransformMessage>(collection.size());
        for (TransformResponse.Notification notification : collection) {
            arrayList.add(this.translate(notification));
        }
        return arrayList;
    }
}

