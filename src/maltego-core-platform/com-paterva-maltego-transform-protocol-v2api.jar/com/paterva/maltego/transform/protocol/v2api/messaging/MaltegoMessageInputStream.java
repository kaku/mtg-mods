/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.protocol.v2api.messaging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class MaltegoMessageInputStream
extends InputStream {
    private static final String SIGNATURE = "<MaltegoMessage>";
    private static final int VALID_UNKNOWN = 0;
    private static final int VALID_YES = 1;
    private static final int XML_HEADER_MAX = 50;
    private static final int VALIDATE_SIZE = 50 + "<MaltegoMessage>".length();
    private int _valid = 0;
    private InputStream _delegate;
    private ByteArrayOutputStream _os = new ByteArrayOutputStream();

    public MaltegoMessageInputStream(InputStream inputStream) {
        this._delegate = inputStream;
    }

    @Override
    public int read() throws IOException {
        int n = this._delegate.read();
        if (this._valid == 0) {
            if (n != -1) {
                this._os.write(n);
                this.checkValid();
            } else if (this._os.size() == 0 || this._os.toString().trim().isEmpty()) {
                throw new IOException("The transform response was empty.");
            }
        }
        return n;
    }

    private void checkValid() throws IOException {
        block4 : {
            if (this._os.size() >= VALIDATE_SIZE) {
                try {
                    String string = this._os.toString("UTF-8").replaceAll("\\s", "");
                    string = string.replaceAll("<\\?[^>]+$", "");
                    string = string.replaceAll("<!--[^>]+$", "");
                    string = string.replaceAll("<\\?[^>]+?\\?>", "");
                    string = string.replaceAll("<!--[^>]+?-->", "");
                    if (string.length() < "<MaltegoMessage>".length()) break block4;
                    if (string.startsWith("<MaltegoMessage>")) {
                        this.setValid();
                        break block4;
                    }
                    throw new IOException("Transform response does not start with \"<MaltegoMessage>\" but with: " + string);
                }
                catch (UnsupportedEncodingException var1_2) {
                    this.setValid();
                }
            }
        }
    }

    private void setValid() {
        this._valid = 1;
        this._os = null;
    }
}

