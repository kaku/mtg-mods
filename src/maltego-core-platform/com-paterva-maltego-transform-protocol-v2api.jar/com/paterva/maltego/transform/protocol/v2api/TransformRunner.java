/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.NormalException
 *  org.openide.util.Cancellable
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.transform.protocol.v2api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.protocol.v2api.EntityTranslator;
import com.paterva.maltego.transform.protocol.v2api.NotificationTranslator;
import com.paterva.maltego.transform.protocol.v2api.api.InvalidKeyException;
import com.paterva.maltego.transform.protocol.v2api.api.KeyLimitReachedException;
import com.paterva.maltego.transform.protocol.v2api.api.KeyRequiredException;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import com.paterva.maltego.transform.protocol.v2api.messaging.CanceledMessage;
import com.paterva.maltego.transform.protocol.v2api.messaging.EntityDescriptor;
import com.paterva.maltego.transform.protocol.v2api.messaging.ExceptionResponse;
import com.paterva.maltego.transform.protocol.v2api.messaging.MaltegoMessageWrapper;
import com.paterva.maltego.transform.protocol.v2api.messaging.TransformResponse;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.NormalException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.openide.util.Cancellable;
import org.openide.util.RequestProcessor;

public abstract class TransformRunner {
    private static final Logger LOG = Logger.getLogger(TransformRunner.class.getName());
    private EntityFactory _factory;
    private EntityRegistry _registry;
    private long _lastLicenseCheck = 0;
    private static final int MinutesBetweenChecks = 40;
    private static final int MillisBetweenChecks = 2400000;

    public TransformRunner(EntityFactory entityFactory, EntityRegistry entityRegistry) {
        this._factory = entityFactory;
        this._registry = entityRegistry;
    }

    public EntityFactory getFactory() {
        return this._factory;
    }

    public EntityRegistry getRegistry() {
        return this._registry;
    }

    public Cancellable run(final MaltegoEntity maltegoEntity, final Callback callback) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                try {
                    RunResult runResult = TransformRunner.this.doTransform(maltegoEntity);
                    callback.success(runResult);
                }
                catch (TransformRunException | IOException var1_2) {
                    String string = var1_2.getMessage();
                    if (string != null && (string.contains("timed out") || string.contains("No response received"))) {
                        LOG.warning(string);
                    } else {
                        NormalException.logStackTrace((Throwable)var1_2);
                    }
                    callback.exception(var1_2, maltegoEntity);
                }
            }
        };
        return this.processor().post(runnable);
    }

    protected abstract RequestProcessor processor();

    private RunResult doTransform(MaltegoEntity maltegoEntity) throws TransformRunException, IOException {
        if (!this.validateLicense()) {
            throw new TransformRunException("Maltego needs to be activated before it can run transforms!");
        }
        try {
            MaltegoMessageWrapper maltegoMessageWrapper = this.runTransform(maltegoEntity);
            if (maltegoMessageWrapper != null) {
                if (maltegoMessageWrapper instanceof CanceledMessage) {
                    CanceledMessage canceledMessage = (CanceledMessage)maltegoMessageWrapper;
                    RunResult runResult = new RunResult(maltegoEntity, Collections.EMPTY_SET);
                    TransformMessage transformMessage = new TransformMessage(canceledMessage.getMessage());
                    transformMessage.setSeverity(TransformMessage.Severity.Warning);
                    runResult.getMessages().add(transformMessage);
                    return runResult;
                }
                TransformResponse transformResponse = maltegoMessageWrapper.getTransformResponse();
                if (transformResponse == null) {
                    ExceptionResponse.MaltegoException maltegoException;
                    ExceptionResponse exceptionResponse = maltegoMessageWrapper.getExceptionResponse();
                    if (exceptionResponse == null) {
                        throw new TransformRunException("NULL response received");
                    }
                    ArrayList<ExceptionResponse.MaltegoException> arrayList = exceptionResponse.getExceptions();
                    ExceptionResponse.MaltegoException maltegoException2 = maltegoException = arrayList.isEmpty() ? null : arrayList.get(0);
                    if (maltegoException != null) {
                        if (maltegoException.getCode() == ExceptionResponse.INVALID_KEY) {
                            throw new InvalidKeyException(maltegoException.getMessage());
                        }
                        if (maltegoException.getCode() == ExceptionResponse.KEY_LIMIT_REACHED) {
                            throw new KeyLimitReachedException(maltegoException.getMessage());
                        }
                        if (maltegoException.getCode() == ExceptionResponse.KEY_REQUIRED) {
                            throw new KeyRequiredException(maltegoException.getMessage());
                        }
                        throw new TransformRunException(maltegoException.getMessage());
                    }
                    throw new TransformRunException("Empty exception response received");
                }
                return TransformRunner.translateResponse(maltegoEntity, transformResponse, this._factory, this._registry);
            }
            throw new TransformRunException("No response received");
        }
        catch (Exception var2_3) {
            if (var2_3 instanceof TransformRunException) {
                throw (TransformRunException)var2_3;
            }
            throw new TransformRunException(var2_3);
        }
    }

    private boolean validateLicense() {
        return true;
    }

    protected abstract MaltegoMessageWrapper runTransform(MaltegoEntity var1) throws TransformRunException, IOException;

    protected static RunResult translateResponse(MaltegoEntity maltegoEntity, TransformResponse transformResponse, EntityFactory entityFactory, EntityRegistry entityRegistry) throws TypeInstantiationException {
        RunResult runResult = new RunResult(maltegoEntity, transformResponse.getEntities());
        NotificationTranslator notificationTranslator = new NotificationTranslator();
        runResult.getEntities().addAll(EntityTranslator.instance().toEntities(transformResponse.getEntities(), entityFactory, entityRegistry));
        runResult.getMessages().addAll(notificationTranslator.translate(transformResponse.getMessages()));
        return runResult;
    }

    static class RunResult {
        private MaltegoEntity _input;
        private List<MaltegoEntity> _entities = new ArrayList<MaltegoEntity>();
        private List<TransformMessage> _notifications;
        private Collection<EntityDescriptor> _result;

        public RunResult(MaltegoEntity maltegoEntity, Collection<EntityDescriptor> collection) {
            this._input = maltegoEntity;
            this._result = collection;
        }

        public Collection<EntityDescriptor> getV2Entities() {
            return this._result;
        }

        public MaltegoEntity getInput() {
            return this._input;
        }

        public List<MaltegoEntity> getEntities() {
            return this._entities;
        }

        public List<TransformMessage> getMessages() {
            if (this._notifications == null) {
                this._notifications = new LinkedList<TransformMessage>();
            }
            return this._notifications;
        }
    }

    static interface Callback {
        public void exception(Exception var1, MaltegoEntity var2);

        public void success(RunResult var1);
    }

}

