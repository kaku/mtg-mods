/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

import java.awt.Color;
import java.awt.Image;

public interface ToolboxItem {
    public Color getColor();

    public void setColor(Color var1);

    public Image getSmallIcon();

    public Image getLargeIcon();

    public void setSmallIcon(Image var1);

    public void setLargeIcon(Image var1);

    public boolean isToolboxItem();

    public void setToolboxItem(boolean var1);

    public String getHelpUrl();

    public void setHelpUrl(String var1);

    public String getHelp();

    public void setHelp(String var1);

    public String getDescription();

    public void setDescription(String var1);

    public String getDisplayName();

    public void setDisplayName(String var1);
}

