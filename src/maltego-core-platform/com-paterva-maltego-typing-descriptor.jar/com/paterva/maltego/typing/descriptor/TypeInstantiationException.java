/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

public class TypeInstantiationException
extends Exception {
    public TypeInstantiationException() {
    }

    public TypeInstantiationException(String string) {
        super(string);
    }

    public TypeInstantiationException(String string, Exception exception) {
        super(string, exception);
    }
}

