/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.typing.descriptor.TypeInstantiationException;

public abstract class SpecFactory<TSpec, TInstance> {
    public abstract TInstance createInstance(String var1, boolean var2, boolean var3) throws TypeInstantiationException;

    public abstract TInstance createInstance(TSpec var1, boolean var2, boolean var3) throws TypeInstantiationException;
}

