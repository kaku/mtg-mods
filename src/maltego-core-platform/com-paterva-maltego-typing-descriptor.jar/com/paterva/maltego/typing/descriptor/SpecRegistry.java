/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.typing.descriptor.RegistryEvent;
import com.paterva.maltego.typing.descriptor.RegistryListener;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeSet;

public abstract class SpecRegistry<T extends TypeSpec> {
    private Collection<RegistryListener> _listeners = Collections.synchronizedCollection(new LinkedList());

    public void addRegistryListener(RegistryListener registryListener) {
        this._listeners.add(registryListener);
    }

    public void removeRegistryListener(RegistryListener registryListener) {
        this._listeners.remove(registryListener);
    }

    protected void fireTypeAdded(T t) {
        RegistryEvent<T> registryEvent = new RegistryEvent<T>(this, t);
        for (RegistryListener registryListener : this._listeners) {
            registryListener.typeAdded(registryEvent);
        }
    }

    protected void fireTypeRemoved(T t) {
        RegistryEvent<T> registryEvent = new RegistryEvent<T>(this, t);
        for (RegistryListener registryListener : this._listeners) {
            registryListener.typeRemoved(registryEvent);
        }
    }

    protected void fireTypeUpdated(T t) {
        RegistryEvent<T> registryEvent = new RegistryEvent<T>(this, t);
        for (RegistryListener registryListener : this._listeners) {
            registryListener.typeUpdated(registryEvent);
        }
    }

    public abstract T get(String var1);

    public abstract Collection<T> getAll();

    public abstract void put(T var1);

    public abstract void put(T var1, String var2);

    public abstract void remove(String var1);

    public abstract boolean contains(String var1);

    public abstract String[] allCategories();

    public Collection<T> getAllVisible() {
        Collection<T> collection = this.getAll();
        ArrayList<TypeSpec> arrayList = new ArrayList<TypeSpec>();
        for (TypeSpec typeSpec : collection) {
            if (!typeSpec.isVisible()) continue;
            arrayList.add(typeSpec);
        }
        return arrayList;
    }

    public static class Proxy<T extends TypeSpec>
    extends SpecRegistry<T> {
        private SpecRegistry<T>[] _registries;

        public /* varargs */ Proxy(SpecRegistry<T> ... arrspecRegistry) {
            this._registries = arrspecRegistry;
        }

        @Override
        public T get(String string) {
            for (SpecRegistry<T> specRegistry : this._registries) {
                T t = specRegistry.get(string);
                if (t == null) continue;
                return t;
            }
            return null;
        }

        @Override
        public Collection<T> getAll() {
            HashSet<T> hashSet = new HashSet<T>();
            for (SpecRegistry<T> specRegistry : this._registries) {
                hashSet.addAll(specRegistry.getAll());
            }
            return hashSet;
        }

        @Override
        public void put(T t) {
            throw new UnsupportedOperationException("Object is read-only");
        }

        @Override
        public void put(T t, String string) {
            throw new UnsupportedOperationException("Object is read-only");
        }

        @Override
        public void remove(String string) {
            throw new UnsupportedOperationException("Object is read-only");
        }

        @Override
        public boolean contains(String string) {
            for (SpecRegistry<T> specRegistry : this._registries) {
                if (!specRegistry.contains(string)) continue;
                return true;
            }
            return false;
        }

        @Override
        public String[] allCategories() {
            TreeSet<String> treeSet = new TreeSet<String>();
            for (SpecRegistry<T> specRegistry : this._registries) {
                for (String string : specRegistry.allCategories()) {
                    treeSet.add(string);
                }
            }
            return treeSet.toArray(new String[treeSet.size()]);
        }
    }

    public static class Memory<T extends TypeSpec>
    extends SpecRegistry<T> {
        private static final String DEFAULT_FOLDER = "Miscellaneous";
        private HashMap<String, T> _items = new HashMap();
        private HashMap<String, String> _folders = new HashMap();

        public Memory() {
        }

        public Memory(Collection<T> collection) {
            for (TypeSpec typeSpec : collection) {
                this.put(typeSpec);
            }
        }

        public Memory(T[] arrT) {
            for (T t : arrT) {
                this.put(t);
            }
        }

        @Override
        public T get(String string) {
            return (T)((TypeSpec)this._items.get(string));
        }

        @Override
        public Collection<T> getAll() {
            return this._items.values();
        }

        @Override
        public void put(T t) {
            this.put(t, t.getDefaultCategory());
        }

        @Override
        public void put(T t, String string) {
            if (string == null) {
                string = "Miscellaneous";
            }
            this._items.put(t.getTypeName(), t);
            this._folders.put(t.getTypeName(), string);
            this.fireTypeAdded(t);
        }

        @Override
        public void remove(String string) {
            T t = this.get(string);
            if (t != null) {
                this._items.remove(string);
                this.fireTypeRemoved(t);
            }
        }

        @Override
        public boolean contains(String string) {
            return this._items.containsKey(string);
        }

        @Override
        public String[] allCategories() {
            return this._folders.values().toArray(new String[this._folders.size()]);
        }
    }

}

