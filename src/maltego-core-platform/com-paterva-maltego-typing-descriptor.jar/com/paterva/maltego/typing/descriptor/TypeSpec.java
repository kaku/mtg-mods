/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.util.ListMap
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.util.ListMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class TypeSpec {
    private String _typeName;
    private String _displayName;
    private Map<String, DisplayDescriptor> _specialProperties;
    private DisplayDescriptorCollection _properties;
    private GroupDefinitions _propertyGroups;
    private String _defaultCategory;
    private boolean _visible = true;

    public TypeSpec() {
        this("", (DisplayDescriptorCollection)new DisplayDescriptorList());
    }

    public TypeSpec(String string, DisplayDescriptorCollection displayDescriptorCollection) {
        this(string, displayDescriptorCollection, null);
    }

    public TypeSpec(String string, DisplayDescriptorCollection displayDescriptorCollection, GroupDefinitions groupDefinitions) {
        this._typeName = string;
        this._properties = displayDescriptorCollection;
        this._propertyGroups = groupDefinitions;
    }

    public TypeSpec(String string, PropertyConfiguration propertyConfiguration) {
        this._typeName = string;
        if (propertyConfiguration != null) {
            this._properties = new DisplayDescriptorList((Iterable)propertyConfiguration.getProperties());
            this._propertyGroups = propertyConfiguration.getGroups();
            this._specialProperties = propertyConfiguration.getSpecialProperties();
        } else {
            this._properties = new DisplayDescriptorList();
        }
    }

    public TypeSpec(TypeSpec typeSpec) {
        this._typeName = typeSpec._typeName;
        this._displayName = typeSpec._displayName;
        if (typeSpec._properties != null) {
            this._properties = new DisplayDescriptorList();
            for (DisplayDescriptor object : typeSpec._properties) {
                this._properties.add(new DisplayDescriptor(object));
            }
        }
        if (typeSpec._specialProperties != null) {
            this._specialProperties = new HashMap<String, DisplayDescriptor>();
            for (Map.Entry<String, DisplayDescriptor> entry : typeSpec._specialProperties.entrySet()) {
                this._specialProperties.put(entry.getKey(), new DisplayDescriptor(entry.getValue()));
            }
        }
        this._propertyGroups = typeSpec._propertyGroups;
        this._defaultCategory = typeSpec._defaultCategory;
        this._visible = typeSpec._visible;
    }

    public boolean isCopy(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof TypeSpec)) {
            return false;
        }
        TypeSpec typeSpec = (TypeSpec)object;
        if (this._typeName == null ? typeSpec._typeName != null : !this._typeName.equals(typeSpec._typeName)) {
            return false;
        }
        if (this._displayName == null ? typeSpec._displayName != null : !this._displayName.equals(typeSpec._displayName)) {
            return false;
        }
        if (this._defaultCategory == null ? typeSpec._defaultCategory != null : !this._defaultCategory.equals(typeSpec._defaultCategory)) {
            return false;
        }
        if (this._visible != typeSpec._visible) {
            return false;
        }
        if (!(this._specialProperties == typeSpec._specialProperties || this._specialProperties != null && this._specialProperties.equals(typeSpec._specialProperties))) {
            return false;
        }
        if (this._properties != typeSpec._properties) {
            if (this._properties == null || typeSpec._properties == null) {
                return false;
            }
            if (this._properties.size() != typeSpec._properties.size()) {
                return false;
            }
            if (!this._properties.containsAll((Collection)typeSpec._properties) || !typeSpec._properties.containsAll((Collection)this._properties)) {
                return false;
            }
        }
        return true;
    }

    public String getDefaultCategory() {
        return this._defaultCategory;
    }

    public void setDefaultCategory(String string) {
        this._defaultCategory = string;
    }

    public DisplayDescriptor getSpecialProperty(String string) {
        if (this._specialProperties == null) {
            return null;
        }
        return this._specialProperties.get(string);
    }

    public void setSpecialProperty(String string, DisplayDescriptor displayDescriptor) {
        if (this._specialProperties == null) {
            this._specialProperties = new ListMap();
        }
        this._specialProperties.put(string, displayDescriptor);
    }

    public PropertyConfiguration getPropertyConfiguration() {
        return new PropertyConfiguration((DisplayDescriptorEnumeration)this._properties, this._propertyGroups, this._specialProperties);
    }

    public String getTypeName() {
        return this._typeName;
    }

    public void setTypeName(String string) {
        this._typeName = string;
    }

    public String getID() {
        return TypeSpec.parseTypeName(this._typeName)[1];
    }

    public String getNamespace() {
        return TypeSpec.parseTypeName(this._typeName)[0];
    }

    private static String[] parseTypeName(String string) {
        if (string == null) {
            return new String[]{"", ""};
        }
        int n = string.lastIndexOf(46);
        if (n < 0) {
            return new String[]{"", string};
        }
        String string2 = string.substring(0, n);
        String string3 = n + 1 > string.length() ? "" : string.substring(n + 1, string.length());
        return new String[]{string2, string3};
    }

    public int hashCode() {
        int n = 3;
        n = 71 * n + (this.getTypeName() != null ? this.getTypeName().hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof TypeSpec)) {
            return false;
        }
        TypeSpec typeSpec = (TypeSpec)object;
        if (!Objects.equals(this.getTypeName(), typeSpec.getTypeName())) {
            return false;
        }
        return true;
    }

    public String getDisplayName() {
        if (this._displayName == null) {
            return this.getTypeName();
        }
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public DisplayDescriptorCollection getProperties() {
        return this._properties;
    }

    public GroupDefinitions getPropertyGroups() {
        if (this._propertyGroups == null) {
            this._propertyGroups = new GroupDefinitions();
        }
        return this._propertyGroups;
    }

    public boolean isVisible() {
        return this._visible;
    }

    public void setVisible(boolean bl) {
        this._visible = bl;
    }
}

