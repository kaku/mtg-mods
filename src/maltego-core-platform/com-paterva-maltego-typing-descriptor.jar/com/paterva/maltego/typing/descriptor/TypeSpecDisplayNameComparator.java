/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.io.Serializable;
import java.util.Comparator;

public class TypeSpecDisplayNameComparator
implements Comparator<TypeSpec>,
Serializable {
    @Override
    public int compare(TypeSpec typeSpec, TypeSpec typeSpec2) {
        return typeSpec.getDisplayName().compareToIgnoreCase(typeSpec2.getDisplayName());
    }
}

