/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.descriptor.ToolboxItem;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.awt.Image;

public class ToolboxItemSpec<TInstance>
extends TypeSpec
implements ToolboxItem {
    private String _description;
    private String _helpUrl;
    private String _helpText;
    private Class<? extends TInstance> _class;
    private boolean _canBeRoot = true;
    private Image _smallIcon;
    private Image _largeIcon;
    private String _smallIconResource;
    private String _largeIconResource;
    private Color _color;
    private static final String VALUE_PROPERTY = "value";
    private static final String DISPLAY_VALUE_PROPERTY = "displayValue";

    public ToolboxItemSpec() {
    }

    public ToolboxItemSpec(String string, DisplayDescriptorCollection displayDescriptorCollection) {
        super(string, displayDescriptorCollection);
    }

    public ToolboxItemSpec(String string, DisplayDescriptorCollection displayDescriptorCollection, GroupDefinitions groupDefinitions) {
        super(string, displayDescriptorCollection, groupDefinitions);
    }

    public ToolboxItemSpec(String string, PropertyConfiguration propertyConfiguration) {
        super(string, propertyConfiguration);
    }

    public ToolboxItemSpec(ToolboxItemSpec toolboxItemSpec) {
        super(toolboxItemSpec);
        this._description = toolboxItemSpec._description;
        this._helpUrl = toolboxItemSpec._helpUrl;
        this._helpText = toolboxItemSpec._helpText;
        this._class = toolboxItemSpec._class;
        this._canBeRoot = toolboxItemSpec._canBeRoot;
        this._smallIcon = toolboxItemSpec._smallIcon;
        this._largeIcon = toolboxItemSpec._largeIcon;
        this._smallIconResource = toolboxItemSpec._smallIconResource;
        this._largeIconResource = toolboxItemSpec._largeIconResource;
        this._color = toolboxItemSpec._color;
    }

    @Override
    public boolean isCopy(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof ToolboxItemSpec)) {
            return false;
        }
        ToolboxItemSpec toolboxItemSpec = (ToolboxItemSpec)object;
        if (this._description == null ? toolboxItemSpec._description != null : !this._description.equals(toolboxItemSpec._description)) {
            return false;
        }
        if (this._helpUrl == null ? toolboxItemSpec._helpUrl != null : !this._helpUrl.equals(toolboxItemSpec._helpUrl)) {
            return false;
        }
        if (this._helpText == null ? toolboxItemSpec._helpText != null : !this._helpText.equals(toolboxItemSpec._helpText)) {
            return false;
        }
        if (!(this._class == toolboxItemSpec._class || this._class != null && this._class.equals(toolboxItemSpec._class))) {
            return false;
        }
        if (this._canBeRoot != toolboxItemSpec._canBeRoot) {
            return false;
        }
        if (!(this._smallIcon == toolboxItemSpec._smallIcon || this._smallIcon != null && this._smallIcon.equals(toolboxItemSpec._smallIcon))) {
            return false;
        }
        if (!(this._largeIcon == toolboxItemSpec._largeIcon || this._largeIcon != null && this._largeIcon.equals(toolboxItemSpec._largeIcon))) {
            return false;
        }
        String string = this.getSmallIconResource();
        String string2 = toolboxItemSpec.getSmallIconResource();
        if (string == null ? string2 != null : !string.equals(string2)) {
            return false;
        }
        if (this._largeIconResource == null ? toolboxItemSpec._largeIconResource != null : !this._largeIconResource.equals(toolboxItemSpec._largeIconResource)) {
            return false;
        }
        if (!(this._color == toolboxItemSpec._color || this._color != null && this._color.equals(toolboxItemSpec._color))) {
            return false;
        }
        return super.isCopy(toolboxItemSpec);
    }

    public void setDisplayValueProperty(DisplayDescriptor displayDescriptor) {
        if (displayDescriptor == this.getValueProperty()) {
            displayDescriptor = null;
        }
        this.setSpecialProperty("displayValue", displayDescriptor);
    }

    public void setValueProperty(DisplayDescriptor displayDescriptor) {
        this.setSpecialProperty("value", displayDescriptor);
    }

    public DisplayDescriptor getDisplayValueProperty() {
        DisplayDescriptor displayDescriptor = this.getSpecialProperty("displayValue");
        if (displayDescriptor == null) {
            return this.getValueProperty();
        }
        return displayDescriptor;
    }

    public DisplayDescriptor getValueProperty() {
        return this.getSpecialProperty("value");
    }

    @Override
    public String getHelpUrl() {
        return this._helpUrl;
    }

    @Override
    public void setHelpUrl(String string) {
        this._helpUrl = string;
    }

    public void setInstanceClass(Class<? extends TInstance> class_) {
        this._class = class_;
    }

    public Class<? extends TInstance> getInstanceClass() {
        return this._class;
    }

    @Override
    public boolean isToolboxItem() {
        return this._canBeRoot;
    }

    @Override
    public void setToolboxItem(boolean bl) {
        this._canBeRoot = bl;
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    @Override
    public void setDescription(String string) {
        this._description = string;
    }

    public String toString() {
        return this.getDisplayName();
    }

    public String getHelpText() {
        return this._helpText;
    }

    public void setHelpText(String string) {
        this._helpText = string;
    }

    @Override
    public Image getSmallIcon() {
        return this._smallIcon;
    }

    @Override
    public Image getLargeIcon() {
        return this._largeIcon;
    }

    @Override
    public void setSmallIcon(Image image) {
        if (image != null) {
            this._smallIconResource = null;
        }
        this._smallIcon = image;
    }

    @Override
    public void setLargeIcon(Image image) {
        if (image != null) {
            this._largeIconResource = null;
        }
        this._largeIcon = image;
    }

    @Override
    public String getHelp() {
        return this._helpText;
    }

    @Override
    public void setHelp(String string) {
        this._helpText = string;
    }

    @Override
    public Color getColor() {
        return this._color;
    }

    @Override
    public void setColor(Color color) {
        this._color = color;
    }

    public String getSmallIconResource() {
        if (StringUtilities.isNullOrEmpty((String)this._smallIconResource)) {
            return this.getLargeIconResource();
        }
        return this._smallIconResource;
    }

    public void setSmallIconResource(String string) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            this._smallIconResource = string;
            this._smallIcon = null;
        }
    }

    public String getLargeIconResource() {
        return this._largeIconResource;
    }

    public void setLargeIconResource(String string) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            this._largeIconResource = string;
            this._largeIcon = null;
        }
    }

    public Image getIcon(int n) {
        return this.getIcon(n, n);
    }

    public Image getIcon(int n, int n2) {
        if (n2 >= 0 && n2 <= 20) {
            if (!StringUtilities.isNullOrEmpty((String)this.getSmallIconResource())) {
                return ToolboxItemSpec.getImageFromResource(this.getSmallIconResource(), n, n2);
            }
            return this._smallIcon;
        }
        if (!StringUtilities.isNullOrEmpty((String)this.getLargeIconResource())) {
            return ToolboxItemSpec.getImageFromResource(this.getLargeIconResource(), n, n2);
        }
        return this._largeIcon;
    }

    private static Image getImageFromResource(String string, int n, int n2) {
        Image image = ImageFactory.getDefault().getImage((Object)string, n, n2, null);
        return image;
    }
}

