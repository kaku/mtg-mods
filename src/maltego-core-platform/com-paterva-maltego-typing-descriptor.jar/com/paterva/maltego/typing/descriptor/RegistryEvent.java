/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.EventObject;

public class RegistryEvent<T extends TypeSpec>
extends EventObject {
    private T _spec;

    public RegistryEvent(SpecRegistry specRegistry, T t) {
        super(specRegistry);
        this._spec = t;
    }

    public SpecRegistry getRegistry() {
        return (SpecRegistry)this.getSource();
    }

    public T getSpec() {
        return this._spec;
    }

    public void setSpec(T t) {
        this._spec = t;
    }
}

