/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.descriptor;

import com.paterva.maltego.typing.descriptor.RegistryEvent;
import java.util.EventListener;

public interface RegistryListener
extends EventListener {
    public void typeAdded(RegistryEvent var1);

    public void typeRemoved(RegistryEvent var1);

    public void typeUpdated(RegistryEvent var1);
}

