/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.matching.api;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;

public class GraphMatchStrategy {
    private final MatchingRuleDescriptor _entityRule;
    private final MatchingRuleDescriptor _linkRule;

    public GraphMatchStrategy(MatchingRuleDescriptor matchingRuleDescriptor, MatchingRuleDescriptor matchingRuleDescriptor2) {
        this._entityRule = matchingRuleDescriptor;
        this._linkRule = matchingRuleDescriptor2;
    }

    public MatchingRuleDescriptor getEntityMatchingRule() {
        return this._entityRule;
    }

    public MatchingRuleDescriptor getLinkMatchingRule() {
        return this._linkRule;
    }
}

