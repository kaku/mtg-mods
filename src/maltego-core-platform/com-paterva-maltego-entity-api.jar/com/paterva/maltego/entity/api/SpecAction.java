/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.descriptor.SpecRegistry;

public abstract class SpecAction {
    public abstract String getTypeName();

    public abstract boolean isEnabledFor(SpecRegistry var1, MaltegoPart var2, String var3);

    public abstract void perform(SpecRegistry var1, MaltegoPart var2, String var3);
}

