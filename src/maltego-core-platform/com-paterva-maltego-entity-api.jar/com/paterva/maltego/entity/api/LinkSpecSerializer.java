/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.util.Lookup;

public abstract class LinkSpecSerializer {
    public static LinkSpecSerializer getDefault() {
        LinkSpecSerializer linkSpecSerializer = (LinkSpecSerializer)Lookup.getDefault().lookup(LinkSpecSerializer.class);
        if (linkSpecSerializer == null) {
            linkSpecSerializer = new NullSerializer();
        }
        return linkSpecSerializer;
    }

    public abstract void write(MaltegoLinkSpec var1, OutputStream var2) throws XmlSerializationException;

    public abstract MaltegoLinkSpec read(InputStream var1) throws XmlSerializationException, TypeInstantiationException;

    private static class NullSerializer
    extends LinkSpecSerializer {
        private NullSerializer() {
        }

        @Override
        public void write(MaltegoLinkSpec maltegoLinkSpec, OutputStream outputStream) throws XmlSerializationException {
            throw new XmlSerializationException("No link spec serializer registered");
        }

        @Override
        public MaltegoLinkSpec read(InputStream inputStream) throws XmlSerializationException, TypeInstantiationException {
            throw new XmlSerializationException("No link spec serializer registered");
        }
    }

}

