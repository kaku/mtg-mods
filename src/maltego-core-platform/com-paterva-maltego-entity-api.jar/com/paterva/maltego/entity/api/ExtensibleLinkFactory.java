/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.LinkDecorator;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.util.Lookup;

public class ExtensibleLinkFactory
extends LinkFactory.Registry {
    private static final List<LinkDecorator> _decorators = new ArrayList<LinkDecorator>(Lookup.getDefault().lookupAll(LinkDecorator.class));
    private final GraphID _graphID;

    public ExtensibleLinkFactory(LinkRegistry linkRegistry, GraphID graphID) {
        super(linkRegistry);
        this._graphID = graphID;
    }

    @Override
    public MaltegoLink createInstance(String string, boolean bl, boolean bl2) throws TypeInstantiationException {
        MaltegoLink maltegoLink = super.createInstance(string, bl, bl2);
        maltegoLink = this.decorate(maltegoLink, bl2);
        return maltegoLink;
    }

    @Override
    public MaltegoLink createInstance(MaltegoLinkSpec maltegoLinkSpec, boolean bl, boolean bl2) throws TypeInstantiationException {
        MaltegoLink maltegoLink = super.createInstance(maltegoLinkSpec, bl, bl2);
        maltegoLink = this.decorate(maltegoLink, bl2);
        return maltegoLink;
    }

    @Override
    public MaltegoLink createInstance(String string, boolean bl) {
        MaltegoLink maltegoLink = super.createInstance(string, bl);
        maltegoLink = this.decorate(maltegoLink, bl);
        return maltegoLink;
    }

    @Override
    public MaltegoLink createInstance(String string, LinkID linkID, boolean bl) {
        MaltegoLink maltegoLink = super.createInstance(string, linkID, bl);
        maltegoLink = this.decorate(maltegoLink, bl);
        return maltegoLink;
    }

    @Override
    public MaltegoLink createInstance(MaltegoLinkSpec maltegoLinkSpec, boolean bl) {
        MaltegoLink maltegoLink = super.createInstance(maltegoLinkSpec, bl);
        maltegoLink = this.decorate(maltegoLink, bl);
        return maltegoLink;
    }

    @Override
    public MaltegoLink createInstance(MaltegoLinkSpec maltegoLinkSpec, LinkID linkID, boolean bl) {
        MaltegoLink maltegoLink = super.createInstance(maltegoLinkSpec, linkID, bl);
        maltegoLink = this.decorate(maltegoLink, bl);
        return maltegoLink;
    }

    private MaltegoLink decorate(MaltegoLink maltegoLink, boolean bl) {
        if (bl && this._graphID != null) {
            for (LinkDecorator linkDecorator : _decorators) {
                maltegoLink = linkDecorator.decorate(this._graphID, maltegoLink);
            }
        }
        return maltegoLink;
    }
}

