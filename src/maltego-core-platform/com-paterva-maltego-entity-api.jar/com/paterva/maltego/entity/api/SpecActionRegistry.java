/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.entity.api.SpecAction;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class SpecActionRegistry {
    private static SpecActionRegistry _instance;

    public static SpecActionRegistry getDefault() {
        if (_instance == null && (SpecActionRegistry._instance = (SpecActionRegistry)Lookup.getDefault().lookup(SpecActionRegistry.class)) == null) {
            _instance = new DefaultSpecActionRegistry();
        }
        return _instance;
    }

    public abstract Collection<SpecAction> getActions();

    public abstract SpecAction getAction(String var1);

    private static class DefaultSpecActionRegistry
    extends SpecActionRegistry {
        private Map<String, SpecAction> _actions;

        public DefaultSpecActionRegistry() {
            Collection collection = Lookup.getDefault().lookupAll(SpecAction.class);
            this._actions = new HashMap<String, SpecAction>(collection.size());
            for (SpecAction specAction : collection) {
                this._actions.put(specAction.getTypeName(), specAction);
            }
        }

        @Override
        public Collection<SpecAction> getActions() {
            return Collections.unmodifiableCollection(this._actions.values());
        }

        @Override
        public SpecAction getAction(String string) {
            SpecAction specAction = this._actions.get(string);
            if (specAction == null) {
                System.out.println("Warning - The following spec action was not found: " + string);
            }
            return specAction;
        }
    }

}

