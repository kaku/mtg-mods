/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.A
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.J
 *  yguard.A.A.K
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.D.D
 *  yguard.A.F.B
 *  yguard.A.F.H
 *  yguard.A.F.I
 */
package com.paterva.maltego.entity.api.inheritance;

import com.paterva.maltego.entity.api.inheritance.InheritanceProvider;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import yguard.A.A.A;
import yguard.A.A.D;
import yguard.A.A.J;
import yguard.A.A.K;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.F.B;
import yguard.A.F.H;
import yguard.A.F.I;

public class MultiInheritanceAdapter<T> {
    private static final boolean DEBUG = false;
    private final InheritanceProvider<T> _provider;
    private T _item;
    private Map<T, List<T>> _inheritanceMap;
    private D _graph;
    private Map<T, Y> _itemToNodeMap;
    private Map<Y, T> _nodeToItemMap;

    public MultiInheritanceAdapter(InheritanceProvider<T> inheritanceProvider) {
        this._provider = inheritanceProvider;
    }

    public List<T> getTopologicalInheritanceList(T t) {
        T t2;
        List<T> list = this._provider.getBaseItems(t);
        if (list.isEmpty()) {
            return Arrays.asList(t);
        }
        if (list.size() == 1 && (t2 = list.get(0)) != null && t2.equals(this._provider.getRootItem())) {
            return Arrays.asList(t, t2);
        }
        this._item = t;
        this.createInheritanceMap();
        this.createInheritanceGraph();
        this.removeCycles();
        return this.createTopologicalList();
    }

    private void createInheritanceMap() {
        this._inheritanceMap = new HashMap<T, List<T>>();
        T t = this._provider.getRootItem();
        if (t != null) {
            this._inheritanceMap.put(t, this._provider.getBaseItems(t));
        }
        this.createInheritanceMapRecursive(this._item);
    }

    private void createInheritanceMapRecursive(T t) {
        List<T> list;
        if (!this._inheritanceMap.containsKey(t) && (list = this._provider.getBaseItems(t)) != null) {
            List<T> list2 = this.getValidBaseItems(list);
            this._inheritanceMap.put(t, list2);
            for (T t2 : list2) {
                this.createInheritanceMapRecursive(t2);
            }
        }
    }

    private List<T> getValidBaseItems(List<T> list) {
        ArrayList<T> arrayList = new ArrayList<T>();
        for (T t : list) {
            if (this._provider.getBaseItems(t) == null) continue;
            arrayList.add(t);
        }
        if (arrayList.isEmpty() && this._provider.getRootItem() != null) {
            arrayList.add(this._provider.getRootItem());
        }
        return arrayList;
    }

    private void createInheritanceGraph() {
        Object object;
        this._graph = new D();
        this._itemToNodeMap = new HashMap<T, Y>();
        this._nodeToItemMap = new HashMap<Y, T>();
        for (T t2 : this._inheritanceMap.keySet()) {
            object = this._graph.createNode();
            this._itemToNodeMap.put(t2, (Y)object);
            this._nodeToItemMap.put((Y)object, t2);
        }
        for (T t2 : this._inheritanceMap.keySet()) {
            object = this._inheritanceMap.get(t2);
            Iterator iterator = object.iterator();
            while (iterator.hasNext()) {
                Object e = iterator.next();
                this._graph.createEdge(this._itemToNodeMap.get(t2), this._itemToNodeMap.get(e));
            }
        }
    }

    private void removeCycles() {
        boolean bl;
        Y y = this._itemToNodeMap.get(this._item);
        Y y2 = this._itemToNodeMap.get(this._provider.getRootItem());
        FurthestEdge furthestEdge = new FurthestEdge(this._graph, y);
        do {
            bl = false;
            J j = this._graph.createEdgeMap();
            I.A((D)this._graph, (J)j, (K)furthestEdge);
            Double d = null;
            yguard.A.A.H h = null;
            for (yguard.A.A.H h2 : this._graph.getEdgeArray()) {
                if (!j.getBool((Object)h2)) continue;
                double d2 = furthestEdge.getDouble((Object)h2);
                if (d != null && d2 >= d) continue;
                d = d2;
                h = h2;
            }
            this._graph.disposeEdgeMap(j);
            if (h == null) continue;
            T t = this._nodeToItemMap.get((Object)h.X());
            T t2 = this._nodeToItemMap.get((Object)h.V());
            System.out.println("WARNING: Cyclic inheritance detected for " + this._item + "! " + t + " -> " + t2 + " ignored.");
            List<T> list = this._inheritanceMap.get(t);
            list.remove(t2);
            if (list.isEmpty() && this._provider.getRootItem() != null) {
                list.add(this._provider.getRootItem());
                this._graph.changeEdge(h, h.X(), y2);
            } else {
                this._graph.removeEdge(h);
            }
            bl = true;
        } while (bl);
    }

    private List<T> createTopologicalList() {
        X x = B.A((D)this._graph);
        ArrayList<T> arrayList = new ArrayList<T>();
        for (Y y : x.\u00e2()) {
            arrayList.add(this._nodeToItemMap.get((Object)y));
        }
        return arrayList;
    }

    private static class FurthestEdge
    extends yguard.A.D.D {
        private final D _graph;
        private final Y _startNode;

        public FurthestEdge(D d, Y y) {
            this._graph = d;
            this._startNode = y;
        }

        public double getDouble(Object object) {
            yguard.A.A.H h = (yguard.A.A.H)object;
            Y y = h.X();
            if (this._startNode.equals((Object)y)) {
                return 1.0;
            }
            A a = H.A((D)this._graph, (Y)this._startNode, (Y)y, (boolean)true);
            return 1.0 / (double)a.size();
        }
    }

}

