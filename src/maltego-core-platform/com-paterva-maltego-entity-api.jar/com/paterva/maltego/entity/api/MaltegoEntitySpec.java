/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.entity.api.MaltegoPartSpec;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.util.Utilities;

public class MaltegoEntitySpec
extends MaltegoPartSpec<MaltegoEntity> {
    public static final String ROOT_ENTITY_SPEC = "maltego.Unknown";
    private int _conversionOrder = Integer.MAX_VALUE;
    private List<String> _baseEntitySpecs;
    private EntityConverter _converter;
    private static final String IMAGE_PROPERTY = "image";

    public MaltegoEntitySpec() {
    }

    public MaltegoEntitySpec(String string, DisplayDescriptorCollection displayDescriptorCollection) {
        super(string, displayDescriptorCollection);
    }

    public MaltegoEntitySpec(String string, DisplayDescriptorCollection displayDescriptorCollection, GroupDefinitions groupDefinitions) {
        super(string, displayDescriptorCollection, groupDefinitions);
    }

    public MaltegoEntitySpec(String string, PropertyConfiguration propertyConfiguration) {
        super(string, propertyConfiguration);
    }

    public MaltegoEntitySpec(MaltegoEntitySpec maltegoEntitySpec) {
        super(maltegoEntitySpec);
        this._conversionOrder = maltegoEntitySpec.getConversionOrder();
        this._baseEntitySpecs = new ArrayList<String>(maltegoEntitySpec.getBaseEntitySpecs());
        this._converter = maltegoEntitySpec.getConverter();
    }

    @Override
    public boolean isCopy(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof MaltegoEntitySpec)) {
            return false;
        }
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)((Object)object);
        if (this.getConversionOrder() != maltegoEntitySpec.getConversionOrder()) {
            return false;
        }
        if (this.getBaseEntitySpecs().size() != maltegoEntitySpec.getBaseEntitySpecs().size()) {
            return false;
        }
        for (int i = 0; i < this.getBaseEntitySpecs().size(); ++i) {
            if (Utilities.compareObjects((Object)this.getBaseEntitySpecs().get(i), (Object)maltegoEntitySpec.getBaseEntitySpecs().get(i))) continue;
            return false;
        }
        return MaltegoPartSpec.super.isCopy((Object)maltegoEntitySpec);
    }

    @Override
    public List<String> getBaseEntitySpecs() {
        if (this._baseEntitySpecs == null) {
            this.initBaseEntitySpecs();
        }
        return Collections.unmodifiableList(this._baseEntitySpecs);
    }

    public void setBaseEntitySpecs(List<String> list) {
        if (!this.getTypeName().equals("maltego.Unknown")) {
            if (list == null || list.isEmpty()) {
                this.initBaseEntitySpecs();
            } else {
                this._baseEntitySpecs = list;
            }
        }
    }

    private void initBaseEntitySpecs() {
        this._baseEntitySpecs = new ArrayList<String>();
        if (!this.getTypeName().equals("maltego.Unknown")) {
            this._baseEntitySpecs.add("maltego.Unknown");
        }
    }

    public void setImageProperty(DisplayDescriptor displayDescriptor) {
        this.setSpecialProperty("image", displayDescriptor);
    }

    public DisplayDescriptor getImageProperty() {
        return this.getSpecialProperty("image");
    }

    public int hashCode() {
        int n = 3;
        n = 23 * n + (this.getTypeName() != null ? this.getTypeName().hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)((Object)object);
        if (this.getTypeName() == null ? maltegoEntitySpec.getTypeName() != null : !this.getTypeName().equals(maltegoEntitySpec.getTypeName())) {
            return false;
        }
        return true;
    }

    public boolean equals(MaltegoPartSpec maltegoPartSpec) {
        return maltegoPartSpec.getTypeName().equals(this.getTypeName());
    }

    public EntityConverter getConverter() {
        return this._converter;
    }

    public void setConverter(EntityConverter entityConverter) {
        this._converter = entityConverter;
    }

    public int getConversionOrder() {
        return this._conversionOrder;
    }

    public void setConversionOrder(int n) {
        this._conversionOrder = n;
    }

    public void mergeWith(MaltegoEntitySpec maltegoEntitySpec) {
        DisplayDescriptorCollection displayDescriptorCollection = this.getProperties();
        DisplayDescriptorCollection displayDescriptorCollection2 = maltegoEntitySpec.getProperties();
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection2) {
            if (displayDescriptor == null || displayDescriptorCollection.contains(displayDescriptor)) continue;
            displayDescriptorCollection.add(displayDescriptor);
        }
    }
}

