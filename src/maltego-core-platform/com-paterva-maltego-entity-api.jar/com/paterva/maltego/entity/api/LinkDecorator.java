/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoLink;

public interface LinkDecorator {
    public MaltegoLink decorate(GraphID var1, MaltegoLink var2);
}

