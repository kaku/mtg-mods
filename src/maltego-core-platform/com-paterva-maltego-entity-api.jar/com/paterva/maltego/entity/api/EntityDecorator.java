/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;

public interface EntityDecorator {
    public MaltegoEntity decorate(GraphID var1, MaltegoEntity var2);
}

