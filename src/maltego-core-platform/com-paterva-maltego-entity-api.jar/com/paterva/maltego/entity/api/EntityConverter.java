/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.typing.PropertyDescriptor;

public interface EntityConverter {
    public boolean canConvertFrom(Object var1, PropertyDescriptor var2);

    public boolean canConvertTo(Class var1, PropertyDescriptor var2);

    public void init(String var1);

    public MaltegoEntity convertFrom(Object var1, MaltegoEntity var2, PropertyDescriptor var3, boolean var4);

    public Object convertTo(Class var1, MaltegoEntity var2, PropertyDescriptor var3);

    public int getPriority();
}

