/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GenericEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.ExtensibleEntityFactory;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.List;
import org.openide.util.Lookup;

public abstract class EntityFactory
extends SpecFactory<MaltegoEntitySpec, MaltegoEntity> {
    public static synchronized EntityFactory getDefault() {
        EntityFactory entityFactory = (EntityFactory)((Object)Lookup.getDefault().lookup(EntityFactory.class));
        if (entityFactory == null) {
            entityFactory = new ExtensibleEntityFactory(EntityRegistry.getDefault(), null);
        }
        return entityFactory;
    }

    public static synchronized EntityFactory forGraphID(GraphID graphID) {
        EntityRegistry entityRegistry = EntityRegistry.forGraphID(graphID);
        if (entityRegistry == null) {
            return null;
        }
        return new ExtensibleEntityFactory(entityRegistry, graphID);
    }

    public abstract MaltegoEntity createInstance(String var1, boolean var2, EntityID var3, boolean var4) throws TypeInstantiationException;

    public MaltegoEntity createInstance(MaltegoEntitySpec maltegoEntitySpec, boolean bl, EntityID entityID, boolean bl2) throws TypeInstantiationException {
        return this.createInstanceInternal(maltegoEntitySpec, bl, entityID, bl2);
    }

    protected MaltegoEntity createInstanceInternal(MaltegoEntitySpec maltegoEntitySpec, boolean bl, EntityID entityID, boolean bl2) throws TypeInstantiationException {
        if (maltegoEntitySpec.getInstanceClass() == null) {
            if (entityID == null) {
                entityID = EntityID.create();
            }
        } else {
            try {
                MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoEntitySpec.getInstanceClass().newInstance();
            }
            catch (InstantiationException var6_7) {
                throw new TypeInstantiationException("Could not instantiate class " + maltegoEntitySpec.getInstanceClass(), (Exception)var6_7);
            }
            catch (IllegalAccessException var6_8) {
                throw new TypeInstantiationException("Could not instantiate class " + maltegoEntitySpec.getInstanceClass(), (Exception)var6_8);
            }
            throw new TypeInstantiationException("This functionality is broken. We need to find a way to set the Guid of a created entity.");
        }
        GenericEntity genericEntity = new GenericEntity(entityID, maltegoEntitySpec.getTypeName());
        DisplayDescriptorCollection displayDescriptorCollection = this.getProperties(maltegoEntitySpec);
        if (displayDescriptorCollection != null) {
            genericEntity.beginPropertyUpdating();
            for (DisplayDescriptor displayDescriptor2 : displayDescriptorCollection) {
                genericEntity.addProperty((PropertyDescriptor)displayDescriptor2);
            }
            for (DisplayDescriptor displayDescriptor2 : displayDescriptorCollection) {
                if (!displayDescriptor2.getLinkedProperties().isEmpty()) continue;
                if (bl && displayDescriptor2.getSampleValue() != null) {
                    genericEntity.setValue((PropertyDescriptor)displayDescriptor2, displayDescriptor2.getSampleValue());
                    continue;
                }
                genericEntity.setValue((PropertyDescriptor)displayDescriptor2, displayDescriptor2.getDefaultValue());
            }
            genericEntity.endPropertyUpdating(false);
        }
        return genericEntity;
    }

    public MaltegoEntity createInstance(MaltegoEntitySpec maltegoEntitySpec, boolean bl, boolean bl2) throws TypeInstantiationException {
        return this.createInstanceInternal(maltegoEntitySpec, bl, null, bl2);
    }

    protected abstract DisplayDescriptorCollection getProperties(MaltegoEntitySpec var1);

    public static class Registry
    extends EntityFactory {
        private EntityRegistry _registry;

        public Registry(EntityRegistry entityRegistry) {
            this._registry = entityRegistry;
        }

        @Override
        public MaltegoEntity createInstance(String string, boolean bl, EntityID entityID, boolean bl2) throws TypeInstantiationException {
            return this.createInstanceInternal(string, bl, entityID, bl2);
        }

        protected MaltegoEntity createInstanceInternal(String string, boolean bl, EntityID entityID, boolean bl2) throws TypeInstantiationException {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)this._registry.get(string);
            if (maltegoEntitySpec == null) {
                MaltegoEntitySpec maltegoEntitySpec2 = (MaltegoEntitySpec)this._registry.get("maltego.Unknown");
                if (maltegoEntitySpec2 != null) {
                    MaltegoEntity maltegoEntity = this.createInstanceInternal(maltegoEntitySpec2, bl, entityID, bl2);
                    maltegoEntity.setTypeName(string);
                    return maltegoEntity;
                }
                return new GenericEntity(entityID, string);
            }
            return this.createInstanceInternal(maltegoEntitySpec, bl, entityID, bl2);
        }

        public MaltegoEntity createInstance(String string, boolean bl, boolean bl2) throws TypeInstantiationException {
            return this.createInstanceInternal(string, bl, null, bl2);
        }

        @Override
        protected DisplayDescriptorCollection getProperties(MaltegoEntitySpec maltegoEntitySpec) {
            return InheritanceHelper.getAggregatedProperties((SpecRegistry)this._registry, maltegoEntitySpec.getTypeName());
        }
    }

}

