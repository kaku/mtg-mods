/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.Group
 *  com.paterva.maltego.typing.GroupCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.GroupSet
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.ToolboxItemSpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.entity.api.inheritance;

import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.MaltegoPartSpec;
import com.paterva.maltego.entity.api.SpecActionDescriptor;
import com.paterva.maltego.entity.api.inheritance.InheritanceProvider;
import com.paterva.maltego.entity.api.inheritance.MultiInheritanceAdapter;
import com.paterva.maltego.entity.api.inheritance.RegistryInheritanceProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.GroupSet;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.ToolboxItemSpec;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.StringUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class InheritanceHelper {
    private InheritanceHelper() {
    }

    public static PropertyDescriptor getValueProperty(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, boolean bl) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag.getTypeName());
        if (propertyDescriptor == null && (propertyDescriptor = typedPropertyBag.getValueProperty()) == null) {
            if (bl) {
                propertyDescriptor = InheritanceHelper.getFirstStringProperty(typedPropertyBag.getProperties());
            }
            if (propertyDescriptor == null) {
                propertyDescriptor = InheritanceHelper.createValueProperty(typedPropertyBag);
            }
        }
        return propertyDescriptor;
    }

    public static PropertyDescriptor getDisplayValueProperty(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag) {
        PropertyDescriptor propertyDescriptor = typedPropertyBag.getDisplayValueProperty();
        if (propertyDescriptor == null && (propertyDescriptor = InheritanceHelper.getDisplayValueProperty(specRegistry, typedPropertyBag.getTypeName())) == null) {
            propertyDescriptor = InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag, true);
        }
        return propertyDescriptor;
    }

    public static PropertyDescriptor getImageProperty(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity) {
        PropertyDescriptor propertyDescriptor = maltegoEntity.getImageProperty();
        if (propertyDescriptor != null || (propertyDescriptor = InheritanceHelper.getImageProperty(entityRegistry, maltegoEntity.getTypeName())) == null) {
            // empty if block
        }
        return propertyDescriptor;
    }

    public static Object getValue(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag) {
        return typedPropertyBag.getValue(InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag, true));
    }

    public static Object getDisplayValue(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag) {
        return typedPropertyBag.getValue(InheritanceHelper.getDisplayValueProperty(specRegistry, typedPropertyBag));
    }

    public static Object getImage(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity) {
        return maltegoEntity.getValue(InheritanceHelper.getImageProperty(entityRegistry, maltegoEntity));
    }

    public static void setValue(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, Object object) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag, true);
        typedPropertyBag.setValue(propertyDescriptor, object);
    }

    public static void setDisplayValue(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, Object object) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getDisplayValueProperty(specRegistry, typedPropertyBag);
        typedPropertyBag.setValue(propertyDescriptor, object);
    }

    public static void setImage(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, Object object) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getDisplayValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        maltegoEntity.setValue(propertyDescriptor, object);
    }

    public static boolean isValueProperty(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, PropertyDescriptor propertyDescriptor) {
        return InheritanceHelper.namesEqual(propertyDescriptor, InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag, true));
    }

    public static boolean isDisplayValueProperty(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag, PropertyDescriptor propertyDescriptor) {
        return InheritanceHelper.namesEqual(propertyDescriptor, InheritanceHelper.getDisplayValueProperty(specRegistry, typedPropertyBag));
    }

    public static boolean isImageProperty(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor) {
        return InheritanceHelper.namesEqual(propertyDescriptor, InheritanceHelper.getImageProperty(entityRegistry, maltegoEntity));
    }

    public static String getValueString(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag, true);
        String string = null;
        if (propertyDescriptor != null) {
            string = Converter.convertTo((Object)typedPropertyBag.getValue(propertyDescriptor), (Class)propertyDescriptor.getType());
        }
        return string != null ? string : "";
    }

    public static String getDisplayString(SpecRegistry specRegistry, TypedPropertyBag typedPropertyBag) {
        Object object;
        String string = null;
        PropertyDescriptor propertyDescriptor = typedPropertyBag.getDisplayValueProperty();
        if (propertyDescriptor != null) {
            object = typedPropertyBag.getValue(propertyDescriptor);
            string = Converter.convertTo((Object)object, (Class)propertyDescriptor.getType());
        }
        if (StringUtilities.isNullOrEmpty((String)string) && (propertyDescriptor = InheritanceHelper.getDisplayValueProperty(specRegistry, typedPropertyBag.getTypeName())) != null) {
            object = typedPropertyBag.getValue(propertyDescriptor);
            string = Converter.convertTo((Object)object, (Class)propertyDescriptor.getType());
        }
        if (StringUtilities.isNullOrEmpty((String)string) && (propertyDescriptor = InheritanceHelper.getValueProperty(specRegistry, typedPropertyBag, true)) != null) {
            object = typedPropertyBag.getValue(propertyDescriptor);
            string = Converter.convertTo((Object)object, (Class)propertyDescriptor.getType());
        }
        if (string == null) {
            return "";
        }
        return string;
    }

    public static DisplayDescriptorCollection getAggregatedProperties(SpecRegistry specRegistry, GraphPart graphPart) {
        return InheritanceHelper.getAggregatedProperties(specRegistry, GraphStoreHelper.getPart((GraphPart)graphPart).getTypeName());
    }

    public static DisplayDescriptorCollection getAggregatedProperties(SpecRegistry specRegistry, String string) {
        if (specRegistry == null) {
            return null;
        }
        List<String> list = InheritanceHelper.getInheritanceList(specRegistry, string);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (String string2 : list) {
            TypeSpec typeSpec = specRegistry.get(string2);
            if (typeSpec == null) continue;
            linkedHashSet.addAll(typeSpec.getProperties());
        }
        return new DisplayDescriptorList(linkedHashSet);
    }

    public static GroupDefinitions getAggregatedPropertyGroups(SpecRegistry specRegistry, GraphPart graphPart) {
        return InheritanceHelper.getAggregatedPropertyGroups(specRegistry, GraphStoreHelper.getPart((GraphPart)graphPart).getTypeName());
    }

    public static GroupDefinitions getAggregatedPropertyGroups(SpecRegistry specRegistry, String string) {
        if (specRegistry == null) {
            return null;
        }
        List<String> list = InheritanceHelper.getInheritanceList(specRegistry, string);
        GroupSet groupSet = new GroupSet();
        GroupSet groupSet2 = new GroupSet();
        Collections.reverse(list);
        for (String string2 : list) {
            TypeSpec typeSpec = specRegistry.get(string2);
            if (typeSpec == null) continue;
            GroupDefinitions groupDefinitions = typeSpec.getPropertyGroups();
            for (Group group2 : groupDefinitions.getTopLevelGroups()) {
                groupSet.add(group2);
            }
            for (Group group2 : groupDefinitions.getTopLevelSuperGroups()) {
                groupSet2.add(group2);
            }
        }
        return new GroupDefinitions((GroupCollection)groupSet, (GroupCollection)groupSet2);
    }

    public static List<String> getInheritanceList(SpecRegistry specRegistry, String string) {
        if (specRegistry == null) {
            return null;
        }
        MultiInheritanceAdapter<String> multiInheritanceAdapter = new MultiInheritanceAdapter<String>(new RegistryInheritanceProvider(specRegistry));
        List<String> list = multiInheritanceAdapter.getTopologicalInheritanceList(string);
        return list;
    }

    public static List<String> getInheritedList(SpecRegistry<? extends TypeSpec> specRegistry, String string) {
        List list;
        if (specRegistry == null) {
            return null;
        }
        MultiInheritanceAdapter<String> multiInheritanceAdapter = new MultiInheritanceAdapter<String>(new RegistryInheritanceProvider(specRegistry));
        TreeMap<Integer, ArrayList<String>> treeMap = new TreeMap<Integer, ArrayList<String>>();
        for (Object object : specRegistry.getAll()) {
            String object2 = object.getTypeName();
            list = multiInheritanceAdapter.getTopologicalInheritanceList(object2);
            if (!list.contains(string)) continue;
            int n = list.size();
            ArrayList<String> arrayList = (ArrayList<String>)treeMap.get(n);
            if (arrayList == null) {
                arrayList = new ArrayList<String>();
                treeMap.put(n, arrayList);
            }
            arrayList.add(object2);
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry : treeMap.entrySet()) {
            list = (List)entry.getValue();
            arrayList.addAll(list);
        }
        Collections.reverse(arrayList);
        return arrayList;
    }

    public static EntityConverter getConverter(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, String string, PropertyDescriptor propertyDescriptor) {
        if (entityRegistry == null) {
            return null;
        }
        List<String> list = InheritanceHelper.getInheritanceList(entityRegistry, maltegoEntity.getTypeName());
        EntityConverter entityConverter = null;
        for (String string2 : list) {
            EntityConverter entityConverter2;
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string2);
            if (maltegoEntitySpec == null || (entityConverter2 = maltegoEntitySpec.getConverter()) == null || !entityConverter2.canConvertFrom(string, propertyDescriptor) || entityConverter != null && entityConverter2.getPriority() >= entityConverter.getPriority()) continue;
            entityConverter = entityConverter2;
        }
        return entityConverter;
    }

    public static Collection<SpecActionDescriptor> getAggregatedActions(SpecRegistry specRegistry, String string) {
        if (specRegistry == null) {
            return null;
        }
        List<String> list = InheritanceHelper.getInheritanceList(specRegistry, string);
        HashMap<String, SpecActionDescriptor> hashMap = new HashMap<String, SpecActionDescriptor>();
        for (String string2 : list) {
            List<SpecActionDescriptor> list2;
            MaltegoPartSpec maltegoPartSpec;
            TypeSpec typeSpec = specRegistry.get(string2);
            if (!(typeSpec instanceof MaltegoPartSpec) || (list2 = (maltegoPartSpec = (MaltegoPartSpec)typeSpec).getActions()) == null) continue;
            for (SpecActionDescriptor specActionDescriptor : list2) {
                if (hashMap.containsKey(specActionDescriptor.getName())) continue;
                hashMap.put(specActionDescriptor.getName(), specActionDescriptor);
            }
        }
        return hashMap.values();
    }

    private static PropertyDescriptor getFirstStringProperty(PropertyDescriptorCollection propertyDescriptorCollection) {
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            if (!String.class.equals((Object)propertyDescriptor.getType())) continue;
            return propertyDescriptor;
        }
        return null;
    }

    public static PropertyDescriptor getValueProperty(SpecRegistry specRegistry, String string) {
        TypeSpec typeSpec;
        String string2;
        if (specRegistry == null) {
            return null;
        }
        DisplayDescriptor displayDescriptor = null;
        List<String> list = InheritanceHelper.getInheritanceList(specRegistry, string);
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext() && (!((typeSpec = specRegistry.get(string2 = iterator.next())) instanceof ToolboxItemSpec) || (displayDescriptor = ((ToolboxItemSpec)typeSpec).getValueProperty()) == null)) {
        }
        return displayDescriptor;
    }

    public static PropertyDescriptor getDisplayValueProperty(SpecRegistry specRegistry, String string) {
        TypeSpec typeSpec;
        String string2;
        if (specRegistry == null) {
            return null;
        }
        DisplayDescriptor displayDescriptor = null;
        List<String> list = InheritanceHelper.getInheritanceList(specRegistry, string);
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext() && (!((typeSpec = specRegistry.get(string2 = iterator.next())) instanceof ToolboxItemSpec) || (displayDescriptor = ((ToolboxItemSpec)typeSpec).getDisplayValueProperty()) == null)) {
        }
        return displayDescriptor;
    }

    public static PropertyDescriptor getImageProperty(EntityRegistry entityRegistry, String string) {
        MaltegoEntitySpec maltegoEntitySpec;
        String string2;
        if (entityRegistry == null) {
            return null;
        }
        DisplayDescriptor displayDescriptor = null;
        List<String> list = InheritanceHelper.getInheritanceList(entityRegistry, string);
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext() && ((maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string2 = iterator.next())) == null || (displayDescriptor = maltegoEntitySpec.getImageProperty()) == null)) {
        }
        return displayDescriptor;
    }

    private static PropertyDescriptor createValueProperty(TypedPropertyBag typedPropertyBag) {
        String string = InheritanceHelper.getUniquePropertyName(typedPropertyBag.getProperties(), "properties.temp");
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(String.class, string, "Temp");
        typedPropertyBag.addProperty((PropertyDescriptor)displayDescriptor);
        typedPropertyBag.setValueProperty((PropertyDescriptor)displayDescriptor);
        return displayDescriptor;
    }

    private static String getUniquePropertyName(PropertyDescriptorCollection propertyDescriptorCollection, String string) {
        ArrayList<String> arrayList = new ArrayList<String>(propertyDescriptorCollection.size());
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            arrayList.add(propertyDescriptor.getName());
        }
        return StringUtilities.createUniqueString(arrayList, (String)string);
    }

    private static boolean namesEqual(PropertyDescriptor propertyDescriptor, PropertyDescriptor propertyDescriptor2) {
        if (propertyDescriptor != null && propertyDescriptor2 != null) {
            return propertyDescriptor.getName().equals(propertyDescriptor2.getName());
        }
        return false;
    }
}

