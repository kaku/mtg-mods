/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.util.Lookup;

public abstract class EntitySpecSerializer {
    public static EntitySpecSerializer getDefault() {
        EntitySpecSerializer entitySpecSerializer = (EntitySpecSerializer)Lookup.getDefault().lookup(EntitySpecSerializer.class);
        if (entitySpecSerializer == null) {
            entitySpecSerializer = new NullSerializer();
        }
        return entitySpecSerializer;
    }

    public abstract void write(MaltegoEntitySpec var1, OutputStream var2) throws XmlSerializationException;

    public abstract MaltegoEntitySpec read(InputStream var1) throws XmlSerializationException, TypeInstantiationException;

    private static class NullSerializer
    extends EntitySpecSerializer {
        private NullSerializer() {
        }

        @Override
        public void write(MaltegoEntitySpec maltegoEntitySpec, OutputStream outputStream) throws XmlSerializationException {
            throw new XmlSerializationException("No entity spec serializer registered");
        }

        @Override
        public MaltegoEntitySpec read(InputStream inputStream) throws XmlSerializationException, TypeInstantiationException {
            throw new XmlSerializationException("No entity spec serializer registered");
        }
    }

}

