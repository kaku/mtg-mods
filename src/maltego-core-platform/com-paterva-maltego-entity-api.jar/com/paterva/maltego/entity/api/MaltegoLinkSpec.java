/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.EditorDescriptor
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.editors.OptionEditorDescriptor
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.util.ColorUtilities
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.MaltegoPartSpec;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.editors.OptionEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.util.ColorUtilities;
import java.awt.Color;
import java.awt.Image;
import org.openide.util.ImageUtilities;
import org.openide.util.NbPreferences;

public class MaltegoLinkSpec
extends MaltegoPartSpec<MaltegoLink> {
    public static final String PROP_NAME_LABEL = "maltego.link.label";
    public static final String PROP_NAME_MANUAL_TYPE = "maltego.link.manual.type";
    public static final String PROP_NAME_MANUAL_DESCRIPTION = "maltego.link.manual.description";
    public static final String PROP_NAME_TRANSFORM_NAME = "maltego.link.transform.name";
    public static final String PROP_NAME_TRANSFORM_DISPLAY = "maltego.link.transform.display-name";
    public static final String PROP_NAME_TRANSFORM_VERSION = "maltego.link.transform.version";
    public static final String PROP_NAME_TRANSFORM_DATE = "maltego.link.transform.run-date";
    public static final String PROP_NAME_SHOW_LABEL = "maltego.link.show-label";
    public static final String PROP_NAME_COLOR = "maltego.link.color";
    public static final String PROP_NAME_STYLE = "maltego.link.style";
    public static final String PROP_NAME_THICKNESS = "maltego.link.thickness";
    public static final String PROP_NAME_ATTACHMENTS = "maltego.link.attachments";
    public static final String SPEC_NAME_MANUAL = "maltego.link.manual-link";
    public static final String SPEC_NAME_TRANSFORM = "maltego.link.transform-link";
    public static final int PROP_STYLE_THICKNESS_DEFAULT = -1;
    private static final String SPEC_DISPLAY_NAME_MANUAL = "Manual Link";
    private static final String SPEC_DISPLAY_NAME_TRANSFORM = "Transform Link";
    private static final String PREF_MANUAL_LINK_COLOR = "maltego.defaultManualLinkColor";
    private static final String PREF_TRANSFORM_LINK_COLOR = "maltego.defaultTransformLinkColor";
    private static final String DEFAULT_CATEGORY = "Default";
    private static MaltegoLinkSpec _manualLinkSpec;
    private static MaltegoLinkSpec _transformLinkSpec;

    public MaltegoLinkSpec() {
    }

    public MaltegoLinkSpec(String string, DisplayDescriptorCollection displayDescriptorCollection) {
        super(string, displayDescriptorCollection);
    }

    public MaltegoLinkSpec(String string, DisplayDescriptorCollection displayDescriptorCollection, GroupDefinitions groupDefinitions) {
        super(string, displayDescriptorCollection, groupDefinitions);
    }

    public MaltegoLinkSpec(String string, PropertyConfiguration propertyConfiguration) {
        super(string, propertyConfiguration);
    }

    public Image getIcon(int n) {
        String string = this.getTypeName().equals("maltego.link.transform-link") ? "com/paterva/maltego/entity/api/transform_link" : "com/paterva/maltego/entity/api/manual_link";
        if (n > 32) {
            string = string + "48";
        } else if (n > 24) {
            string = string + "32";
        } else if (n > 16) {
            string = string + "24";
        }
        string = string + ".png";
        return ImageUtilities.loadImage((String)string);
    }

    public static synchronized MaltegoLinkSpec getManualSpec() {
        if (_manualLinkSpec == null) {
            DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
            displayDescriptorList.add(new DisplayDescriptor(String.class, "maltego.link.manual.type", "Label"));
            MaltegoLinkSpec.addDefaultLinkProperties((DisplayDescriptorCollection)displayDescriptorList);
            displayDescriptorList.add(new DisplayDescriptor(String.class, "maltego.link.manual.description", "Description"));
            _manualLinkSpec = new MaltegoLinkSpec("maltego.link.manual-link", (DisplayDescriptorCollection)displayDescriptorList);
            _manualLinkSpec.setDisplayName("Manual Link");
            _manualLinkSpec.setDefaultCategory("Default");
            MaltegoLinkSpec.setPropertyDefaults(_manualLinkSpec);
            MaltegoLinkSpec.setPropertyMappings(_manualLinkSpec);
            MaltegoLinkSpec.setPropertyEditors(_manualLinkSpec);
        }
        return _manualLinkSpec;
    }

    public static synchronized MaltegoLinkSpec getTransformSpec() {
        if (_transformLinkSpec == null) {
            DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
            displayDescriptorList.add(new DisplayDescriptor(String.class, "maltego.link.label", "Label"));
            MaltegoLinkSpec.addDefaultLinkProperties((DisplayDescriptorCollection)displayDescriptorList);
            displayDescriptorList.add(new DisplayDescriptor(String.class, "maltego.link.transform.name", "Transform"));
            displayDescriptorList.add(new DisplayDescriptor(String.class, "maltego.link.transform.display-name", "Transform name"));
            displayDescriptorList.add(new DisplayDescriptor(String.class, "maltego.link.transform.version", "Transform version"));
            displayDescriptorList.add(new DisplayDescriptor(DateTime.class, "maltego.link.transform.run-date", "Date run"));
            displayDescriptorList.get("maltego.link.transform.name").setHidden(true);
            displayDescriptorList.get("maltego.link.transform.name").setReadonly(true);
            displayDescriptorList.get("maltego.link.transform.display-name").setReadonly(true);
            displayDescriptorList.get("maltego.link.transform.version").setReadonly(true);
            displayDescriptorList.get("maltego.link.transform.run-date").setReadonly(true);
            _transformLinkSpec = new MaltegoLinkSpec("maltego.link.transform-link", (DisplayDescriptorCollection)displayDescriptorList);
            _transformLinkSpec.setDisplayName("Transform Link");
            _transformLinkSpec.setDefaultCategory("Default");
            MaltegoLinkSpec.setPropertyDefaults(_transformLinkSpec);
            MaltegoLinkSpec.setPropertyMappings(_transformLinkSpec);
            MaltegoLinkSpec.setPropertyEditors(_transformLinkSpec);
        }
        return _transformLinkSpec;
    }

    private static Color getDefaultLinkColor(String string) {
        String string2 = NbPreferences.forModule(MaltegoLinkSpec.class).get(string, null);
        Color color = null;
        if (string2 != null) {
            color = ColorUtilities.decode((String)string2);
        }
        return color;
    }

    public static Color getDefaultManualLinkColor() {
        return MaltegoLinkSpec.getDefaultLinkColor("maltego.defaultManualLinkColor");
    }

    public static Color getDefaultTransformLinkColor() {
        return MaltegoLinkSpec.getDefaultLinkColor("maltego.defaultTransformLinkColor");
    }

    private static void setDefaultLinkColor(String string, Color color) {
        NbPreferences.forModule(MaltegoLinkSpec.class).put(string, ColorUtilities.encode((Color)color));
    }

    public static void setDefaultManualLinkColor(Color color) {
        MaltegoLinkSpec.setDefaultLinkColor("maltego.defaultManualLinkColor", color);
    }

    public static void setDefaultTransformLinkColor(Color color) {
        MaltegoLinkSpec.setDefaultLinkColor("maltego.defaultTransformLinkColor", color);
    }

    private static void addDefaultLinkProperties(DisplayDescriptorCollection displayDescriptorCollection) {
        displayDescriptorCollection.add(MaltegoLinkSpec.createDefaultLinkProperty(Integer.TYPE, "maltego.link.show-label", "Show Label"));
        displayDescriptorCollection.add(MaltegoLinkSpec.createDefaultLinkProperty(Color.class, "maltego.link.color", "Color"));
        displayDescriptorCollection.add(MaltegoLinkSpec.createDefaultLinkProperty(Integer.TYPE, "maltego.link.style", "Style"));
        displayDescriptorCollection.add(MaltegoLinkSpec.createDefaultLinkProperty(Integer.TYPE, "maltego.link.thickness", "Thickness"));
    }

    private static DisplayDescriptor createDefaultLinkProperty(Class class_, String string, String string2) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(class_, string, string2);
        displayDescriptor.setGroupName("");
        return displayDescriptor;
    }

    private static void setPropertyDefaults(MaltegoLinkSpec maltegoLinkSpec) {
        DisplayDescriptorCollection displayDescriptorCollection = maltegoLinkSpec.getProperties();
        if (maltegoLinkSpec == _manualLinkSpec) {
            displayDescriptorCollection.get("maltego.link.manual.type").setDefaultValue((Object)"");
            displayDescriptorCollection.get("maltego.link.manual.description").setDefaultValue((Object)"");
        } else {
            displayDescriptorCollection.get("maltego.link.label").setDefaultValue((Object)"");
        }
        displayDescriptorCollection.get("maltego.link.color").setDefaultValue((Object)null);
        displayDescriptorCollection.get("maltego.link.thickness").setDefaultValue((Object)-1);
        displayDescriptorCollection.get("maltego.link.show-label").setDefaultValue((Object)0);
        displayDescriptorCollection.get("maltego.link.style").setDefaultValue((Object)-1);
    }

    private static void setPropertyMappings(MaltegoLinkSpec maltegoLinkSpec) {
        DisplayDescriptorCollection displayDescriptorCollection = maltegoLinkSpec.getProperties();
        if (maltegoLinkSpec == _manualLinkSpec) {
            maltegoLinkSpec.setValueProperty(displayDescriptorCollection.get("maltego.link.manual.type"));
        } else if (maltegoLinkSpec == _transformLinkSpec) {
            maltegoLinkSpec.setValueProperty(displayDescriptorCollection.get("maltego.link.transform.display-name"));
            maltegoLinkSpec.setDisplayValueProperty(displayDescriptorCollection.get("maltego.link.label"));
        }
        maltegoLinkSpec.setShowLabelProperty(displayDescriptorCollection.get("maltego.link.show-label"));
        maltegoLinkSpec.setColorProperty(displayDescriptorCollection.get("maltego.link.color"));
        maltegoLinkSpec.setStyleProperty(displayDescriptorCollection.get("maltego.link.style"));
        maltegoLinkSpec.setThicknessProperty(displayDescriptorCollection.get("maltego.link.thickness"));
    }

    private static void setPropertyEditors(MaltegoLinkSpec maltegoLinkSpec) {
        int n;
        DisplayDescriptorCollection displayDescriptorCollection = maltegoLinkSpec.getProperties();
        DisplayDescriptor displayDescriptor = displayDescriptorCollection.get("maltego.link.show-label");
        OptionEditorDescriptor optionEditorDescriptor = new OptionEditorDescriptor(displayDescriptor.getType());
        for (n = 0; n < MaltegoLink.ShowLabel.length; ++n) {
            optionEditorDescriptor.getItems().add(MaltegoLink.ShowLabel[n], (Object)n);
        }
        displayDescriptor.setEditor((EditorDescriptor)optionEditorDescriptor);
        displayDescriptor = displayDescriptorCollection.get("maltego.link.style");
        optionEditorDescriptor = new OptionEditorDescriptor(displayDescriptor.getType());
        optionEditorDescriptor.getItems().add("<Default>", (Object)-1);
        for (n = 0; n < MaltegoLink.LineStyle.length; ++n) {
            optionEditorDescriptor.getItems().add(MaltegoLink.LineStyle[n], (Object)n);
        }
        displayDescriptor.setEditor((EditorDescriptor)optionEditorDescriptor);
        displayDescriptor = displayDescriptorCollection.get("maltego.link.thickness");
        optionEditorDescriptor = new OptionEditorDescriptor(displayDescriptor.getType());
        optionEditorDescriptor.getItems().add("<Default>", (Object)-1);
        for (n = 1; n <= 5; ++n) {
            optionEditorDescriptor.getItems().add(Integer.toString(n), (Object)n);
        }
        displayDescriptor.setEditor((EditorDescriptor)optionEditorDescriptor);
    }

    public void setShowLabelProperty(DisplayDescriptor displayDescriptor) {
        this.setSpecialProperty("show-label", displayDescriptor);
    }

    public DisplayDescriptor getShowLabelProperty() {
        return this.getSpecialProperty("show-label");
    }

    public void setColorProperty(DisplayDescriptor displayDescriptor) {
        this.setSpecialProperty("color", displayDescriptor);
    }

    public DisplayDescriptor getColorProperty() {
        return this.getSpecialProperty("color");
    }

    public void setStyleProperty(DisplayDescriptor displayDescriptor) {
        this.setSpecialProperty("style", displayDescriptor);
    }

    public DisplayDescriptor getStyleProperty() {
        return this.getSpecialProperty("style");
    }

    public void setThicknessProperty(DisplayDescriptor displayDescriptor) {
        this.setSpecialProperty("thickness", displayDescriptor);
    }

    public DisplayDescriptor getThicknessProperty() {
        return this.getSpecialProperty("thickness");
    }
}

