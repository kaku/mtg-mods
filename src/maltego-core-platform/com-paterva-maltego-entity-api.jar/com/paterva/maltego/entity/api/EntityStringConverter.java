/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import java.util.Collection;

public interface EntityStringConverter {
    public String convert(GraphID var1, Collection<EntityID> var2, boolean var3, boolean var4);

    public String convert(Collection<MaltegoEntity> var1, boolean var2, boolean var3);

    public String convert(MaltegoEntity var1, boolean var2, boolean var3);

    public Collection<MaltegoEntity> convert(String var1);
}

