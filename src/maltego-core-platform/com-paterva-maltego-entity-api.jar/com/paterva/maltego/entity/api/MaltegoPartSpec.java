/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.descriptor.ToolboxItemSpec
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.SpecActionDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.descriptor.ToolboxItemSpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MaltegoPartSpec<T extends MaltegoPart>
extends ToolboxItemSpec<T> {
    private String _pluralName;
    private List<SpecActionDescriptor> _actions;

    public MaltegoPartSpec() {
    }

    public MaltegoPartSpec(String string, DisplayDescriptorCollection displayDescriptorCollection) {
        super(string, displayDescriptorCollection);
    }

    public MaltegoPartSpec(String string, DisplayDescriptorCollection displayDescriptorCollection, GroupDefinitions groupDefinitions) {
        super(string, displayDescriptorCollection, groupDefinitions);
    }

    public MaltegoPartSpec(String string, PropertyConfiguration propertyConfiguration) {
        super(string, propertyConfiguration);
    }

    public MaltegoPartSpec(MaltegoPartSpec maltegoPartSpec) {
        super((ToolboxItemSpec)maltegoPartSpec);
        this._pluralName = maltegoPartSpec.getDisplayNamePlural();
        if (maltegoPartSpec.getActions() != null) {
            this._actions = new ArrayList<SpecActionDescriptor>(maltegoPartSpec.getActions());
        }
    }

    public boolean isCopy(Object object) {
        List<SpecActionDescriptor> list;
        if (object == null) {
            return false;
        }
        if (!(object instanceof MaltegoPartSpec)) {
            return false;
        }
        MaltegoPartSpec maltegoPartSpec = (MaltegoPartSpec)((Object)object);
        if (!this.getDisplayNamePlural().equals(maltegoPartSpec.getDisplayNamePlural())) {
            return false;
        }
        List<SpecActionDescriptor> list2 = this.getActions();
        if (list2 != (list = maltegoPartSpec.getActions())) {
            if (list2 == null || list == null) {
                return false;
            }
            if (list2.size() != list.size()) {
                return false;
            }
            if (!list2.containsAll(list) || !list.containsAll(list2)) {
                return false;
            }
        }
        return ToolboxItemSpec.super.isCopy((Object)maltegoPartSpec);
    }

    public String getDisplayNamePlural() {
        if (this._pluralName == null) {
            if (this.getDisplayName().endsWith("s")) {
                return this.getDisplayName() + "es";
            }
            return this.getDisplayName() + "s";
        }
        return this._pluralName;
    }

    public void setDisplayNamePlural(String string) {
        this._pluralName = string;
    }

    public List<String> getBaseEntitySpecs() {
        return new ArrayList<String>();
    }

    public void setActions(List<SpecActionDescriptor> list) {
        this._actions = list;
    }

    public List<SpecActionDescriptor> getActions() {
        if (this._actions != null) {
            return Collections.unmodifiableList(this._actions);
        }
        return null;
    }
}

