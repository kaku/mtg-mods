/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GenericLink
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecFactory
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.GenericLink;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.ExtensibleLinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecFactory;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import org.openide.util.Lookup;

public class LinkFactory
extends SpecFactory<MaltegoLinkSpec, MaltegoLink> {
    private static LinkFactory _default;
    private final GenericLink _templateManual;
    private final GenericLink _templateTransform;

    public LinkFactory() {
        this._templateManual = this.createTemplate(MaltegoLinkSpec.getManualSpec());
        this._templateTransform = this.createTemplate(MaltegoLinkSpec.getTransformSpec());
    }

    public static synchronized LinkFactory getDefault() {
        if (_default == null && (LinkFactory._default = (LinkFactory)((Object)Lookup.getDefault().lookup(LinkFactory.class))) == null) {
            _default = new ExtensibleLinkFactory(LinkRegistry.getDefault(), null);
        }
        return _default;
    }

    public static synchronized LinkFactory forGraphID(GraphID graphID) {
        LinkRegistry linkRegistry = LinkRegistry.forGraphID(graphID);
        if (linkRegistry == null) {
            return null;
        }
        return new ExtensibleLinkFactory(linkRegistry, graphID);
    }

    public MaltegoLink createInstance(String string, boolean bl, boolean bl2) throws TypeInstantiationException {
        return this.createInstanceInternal(string, null, bl2);
    }

    public MaltegoLink createInstance(MaltegoLinkSpec maltegoLinkSpec, boolean bl, boolean bl2) throws TypeInstantiationException {
        return this.createInstanceInternal(maltegoLinkSpec, null, bl2);
    }

    public MaltegoLink createInstance(String string, boolean bl) {
        return this.createInstanceInternal(string, null, bl);
    }

    public MaltegoLink createInstance(String string, LinkID linkID, boolean bl) {
        return this.createInstanceInternal(string, linkID, bl);
    }

    public MaltegoLink createInstance(MaltegoLinkSpec maltegoLinkSpec, boolean bl) {
        return this.createInstanceInternal(maltegoLinkSpec, null, bl);
    }

    public MaltegoLink createInstance(MaltegoLinkSpec maltegoLinkSpec, LinkID linkID, boolean bl) {
        return this.createInstanceInternal(maltegoLinkSpec, linkID, bl);
    }

    protected MaltegoLink createInstanceInternal(LinkRegistry linkRegistry, String string, LinkID linkID, boolean bl) {
        MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)linkRegistry.get(string);
        if (maltegoLinkSpec == null) {
            return new GenericLink(linkID, string);
        }
        return this.createInstanceInternal(maltegoLinkSpec, linkID, bl);
    }

    protected MaltegoLink createInstanceInternal(String string, LinkID linkID, boolean bl) {
        return this.createInstanceInternal(LinkRegistry.getDefault(), string, linkID, bl);
    }

    protected MaltegoLink createInstanceInternal(MaltegoLinkSpec maltegoLinkSpec, LinkID linkID, boolean bl) {
        if (linkID == null) {
            linkID = LinkID.create();
        }
        GenericLink genericLink = maltegoLinkSpec.equals((Object)MaltegoLinkSpec.getManualSpec()) ? this._templateManual : this._templateTransform;
        GenericLink genericLink2 = genericLink.createCopy(linkID);
        return genericLink2;
    }

    protected GenericLink createTemplate(MaltegoLinkSpec maltegoLinkSpec) {
        GenericLink genericLink = new GenericLink(LinkID.create(), maltegoLinkSpec.getTypeName());
        genericLink.beginPropertyUpdating();
        if (maltegoLinkSpec.getProperties() != null) {
            for (DisplayDescriptor displayDescriptor : maltegoLinkSpec.getProperties()) {
                genericLink.addProperty((PropertyDescriptor)displayDescriptor);
                genericLink.setValue((PropertyDescriptor)displayDescriptor, displayDescriptor.getDefaultValue());
            }
            genericLink.setDisplayValueProperty((PropertyDescriptor)maltegoLinkSpec.getDisplayValueProperty());
            genericLink.setValueProperty((PropertyDescriptor)maltegoLinkSpec.getValueProperty());
            genericLink.setShowLabelProperty((PropertyDescriptor)maltegoLinkSpec.getShowLabelProperty());
            genericLink.setColorProperty((PropertyDescriptor)maltegoLinkSpec.getColorProperty());
            genericLink.setStyleProperty((PropertyDescriptor)maltegoLinkSpec.getStyleProperty());
            genericLink.setThicknessProperty((PropertyDescriptor)maltegoLinkSpec.getThicknessProperty());
        }
        genericLink.endPropertyUpdating(false);
        return genericLink;
    }

    public static class Registry
    extends LinkFactory {
        private LinkRegistry _registry;

        public Registry(LinkRegistry linkRegistry) {
            this._registry = linkRegistry;
        }

        @Override
        protected MaltegoLink createInstanceInternal(String string, LinkID linkID, boolean bl) {
            return this.createInstanceInternal(this._registry, string, linkID, bl);
        }
    }

}

