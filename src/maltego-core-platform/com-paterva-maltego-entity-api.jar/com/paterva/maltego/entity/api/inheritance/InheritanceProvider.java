/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.entity.api.inheritance;

import java.util.List;

public interface InheritanceProvider<T> {
    public List<T> getBaseItems(T var1);

    public T getRootItem();
}

