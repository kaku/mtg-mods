/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.entity.api.inheritance;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoPartSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceProvider;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.List;

public class RegistryInheritanceProvider
implements InheritanceProvider<String> {
    private SpecRegistry _registry;

    public RegistryInheritanceProvider(SpecRegistry specRegistry) {
        this._registry = specRegistry;
    }

    @Override
    public List<String> getBaseItems(String string) {
        List<String> list = null;
        TypeSpec typeSpec = this._registry.get(string);
        if (typeSpec instanceof MaltegoPartSpec) {
            list = ((MaltegoPartSpec)typeSpec).getBaseEntitySpecs();
        }
        if (list == null) {
            list = new ArrayList<String>();
            if (this.getRootItem() != null) {
                list.add(this.getRootItem());
            }
        }
        return list;
    }

    @Override
    public String getRootItem() {
        if (this._registry instanceof EntityRegistry) {
            return "maltego.Unknown";
        }
        return null;
    }
}

