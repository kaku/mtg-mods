/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Lookup
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import org.openide.util.Lookup;
import yguard.A.A.D;
import yguard.A.I.SA;

public abstract class EntityRegistry
extends SpecRegistry<MaltegoEntitySpec> {
    public static final String MALTEGO_ENTITIES_EXT = "entity";
    private static EntityRegistry _default;

    public static synchronized EntityRegistry getDefault() {
        if (_default == null && (EntityRegistry._default = (EntityRegistry)((Object)Lookup.getDefault().lookup(EntityRegistry.class))) == null) {
            _default = new DefaultEntityRegistry();
        }
        return _default;
    }

    public static synchronized EntityRegistry forGraph(D d) {
        return EntityRegistry.forGraphID(GraphIDProvider.forGraph((SA)((SA)d)));
    }

    public static synchronized EntityRegistry forGraphID(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = EntityRegistry.class.getName();
        return (EntityRegistry)((Object)graphUserData.get((Object)string));
    }

    public static synchronized void associate(GraphID graphID, EntityRegistry entityRegistry) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = EntityRegistry.class.getName();
        graphUserData.put((Object)string, (Object)entityRegistry);
    }

    public static class Memory
    extends EntityRegistry {
        private static final String DEFAULT_FOLDER = "Miscellaneous";
        private HashMap<String, MaltegoEntitySpec> _items = new HashMap();
        private HashMap<String, String> _folders = new HashMap();

        public Memory() {
        }

        public Memory(MaltegoEntitySpec[] arrmaltegoEntitySpec) {
            for (MaltegoEntitySpec maltegoEntitySpec : arrmaltegoEntitySpec) {
                this.put(maltegoEntitySpec);
            }
        }

        public MaltegoEntitySpec get(String string) {
            return this._items.get(string);
        }

        public Collection<MaltegoEntitySpec> getAll() {
            return this._items.values();
        }

        public void put(MaltegoEntitySpec maltegoEntitySpec) {
            this.put(maltegoEntitySpec, maltegoEntitySpec.getDefaultCategory());
        }

        public void put(MaltegoEntitySpec maltegoEntitySpec, String string) {
            if (string == null) {
                string = "Miscellaneous";
            }
            this._items.put(maltegoEntitySpec.getTypeName(), maltegoEntitySpec);
            this._folders.put(maltegoEntitySpec.getTypeName(), string);
            this.fireTypeAdded((TypeSpec)maltegoEntitySpec);
        }

        public void remove(String string) {
            MaltegoEntitySpec maltegoEntitySpec = this.get(string);
            if (maltegoEntitySpec != null) {
                this._items.remove(string);
                this.fireTypeRemoved((TypeSpec)maltegoEntitySpec);
            }
        }

        public boolean contains(String string) {
            return this._items.containsKey(string);
        }

        public String[] allCategories() {
            return this._folders.values().toArray(new String[this._folders.size()]);
        }
    }

    public static class Proxy
    extends EntityRegistry {
        private EntityRegistry[] _registries;
        private EntityRegistry _writeDelegate;

        public /* varargs */ Proxy(EntityRegistry ... arrentityRegistry) {
            this._registries = arrentityRegistry;
        }

        public MaltegoEntitySpec get(String string) {
            for (EntityRegistry entityRegistry : this._registries) {
                MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
                if (maltegoEntitySpec == null) continue;
                return maltegoEntitySpec;
            }
            return null;
        }

        public Collection<MaltegoEntitySpec> getAll() {
            HashSet<MaltegoEntitySpec> hashSet = new HashSet<MaltegoEntitySpec>();
            for (EntityRegistry entityRegistry : this._registries) {
                hashSet.addAll(entityRegistry.getAll());
            }
            return hashSet;
        }

        public void put(MaltegoEntitySpec maltegoEntitySpec) {
            if (this._writeDelegate == null) {
                throw new UnsupportedOperationException("Object is read-only");
            }
            this._writeDelegate.put((TypeSpec)maltegoEntitySpec);
        }

        public void put(MaltegoEntitySpec maltegoEntitySpec, String string) {
            if (this._writeDelegate == null) {
                throw new UnsupportedOperationException("Object is read-only");
            }
            this._writeDelegate.put((TypeSpec)maltegoEntitySpec, string);
        }

        public void remove(String string) {
            if (this._writeDelegate == null) {
                throw new UnsupportedOperationException("Object is read-only");
            }
            this._writeDelegate.remove(string);
        }

        public boolean contains(String string) {
            for (EntityRegistry entityRegistry : this._registries) {
                if (!entityRegistry.contains(string)) continue;
                return true;
            }
            return false;
        }

        public String[] allCategories() {
            TreeSet<String> treeSet = new TreeSet<String>();
            for (EntityRegistry entityRegistry : this._registries) {
                for (String string : entityRegistry.allCategories()) {
                    treeSet.add(string);
                }
            }
            return treeSet.toArray(new String[treeSet.size()]);
        }

        public EntityRegistry getWriteDelegate() {
            return this._writeDelegate;
        }

        public void setWriteDelegate(EntityRegistry entityRegistry) {
            this._writeDelegate = entityRegistry;
        }
    }

    private static class DefaultEntityRegistry
    extends EntityRegistry {
        private DefaultEntityRegistry() {
        }

        public MaltegoEntitySpec get(String string) {
            return null;
        }

        public Collection<MaltegoEntitySpec> getAll() {
            return new ArrayList<MaltegoEntitySpec>();
        }

        public void put(MaltegoEntitySpec maltegoEntitySpec, String string) {
        }

        public void remove(String string) {
        }

        public boolean contains(String string) {
            return false;
        }

        public String[] allCategories() {
            return new String[0];
        }

        public void put(MaltegoEntitySpec maltegoEntitySpec) {
        }
    }

}

