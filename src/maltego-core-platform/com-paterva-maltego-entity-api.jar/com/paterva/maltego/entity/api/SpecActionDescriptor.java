/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.entity.api;

public class SpecActionDescriptor {
    private String _displayName;
    private String _name;
    private String _type;
    private String _config;

    public SpecActionDescriptor(String string, String string2, String string3, String string4) {
        this._displayName = string3;
        this._name = string2;
        this._type = string;
        this._config = string4;
    }

    public String getConfig() {
        return this._config;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public String getName() {
        return this._name;
    }

    public String getType() {
        return this._type;
    }

    public int hashCode() {
        int n = 7;
        n = 79 * n + (this._displayName != null ? this._displayName.hashCode() : 0);
        n = 79 * n + (this._name != null ? this._name.hashCode() : 0);
        n = 79 * n + (this._type != null ? this._type.hashCode() : 0);
        n = 79 * n + (this._config != null ? this._config.hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        SpecActionDescriptor specActionDescriptor = (SpecActionDescriptor)object;
        if (this._displayName == null ? specActionDescriptor._displayName != null : !this._displayName.equals(specActionDescriptor._displayName)) {
            return false;
        }
        if (this._name == null ? specActionDescriptor._name != null : !this._name.equals(specActionDescriptor._name)) {
            return false;
        }
        if (this._type == null ? specActionDescriptor._type != null : !this._type.equals(specActionDescriptor._type)) {
            return false;
        }
        if (this._config == null ? specActionDescriptor._config != null : !this._config.equals(specActionDescriptor._config)) {
            return false;
        }
        return true;
    }
}

