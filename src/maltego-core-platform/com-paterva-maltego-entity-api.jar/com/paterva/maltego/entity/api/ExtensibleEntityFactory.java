/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityDecorator;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import java.util.Collection;
import org.openide.util.Lookup;

public class ExtensibleEntityFactory
extends EntityFactory.Registry {
    private final GraphID _graphID;
    private final Collection<? extends EntityDecorator> _decorators = Lookup.getDefault().lookupAll(EntityDecorator.class);

    public ExtensibleEntityFactory(EntityRegistry entityRegistry, GraphID graphID) {
        super(entityRegistry);
        this._graphID = graphID;
    }

    @Override
    public MaltegoEntity createInstance(MaltegoEntitySpec maltegoEntitySpec, boolean bl, EntityID entityID, boolean bl2) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = super.createInstance(maltegoEntitySpec, bl, entityID, bl2);
        maltegoEntity = this.decorate(maltegoEntity, bl2);
        return maltegoEntity;
    }

    @Override
    public MaltegoEntity createInstance(MaltegoEntitySpec maltegoEntitySpec, boolean bl, boolean bl2) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = super.createInstance(maltegoEntitySpec, bl, bl2);
        maltegoEntity = this.decorate(maltegoEntity, bl2);
        return maltegoEntity;
    }

    @Override
    public MaltegoEntity createInstance(String string, boolean bl, EntityID entityID, boolean bl2) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = super.createInstance(string, bl, entityID, bl2);
        maltegoEntity = this.decorate(maltegoEntity, bl2);
        return maltegoEntity;
    }

    @Override
    public MaltegoEntity createInstance(String string, boolean bl, boolean bl2) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = super.createInstance(string, bl, bl2);
        maltegoEntity = this.decorate(maltegoEntity, bl2);
        return maltegoEntity;
    }

    private MaltegoEntity decorate(MaltegoEntity maltegoEntity, boolean bl) {
        if (bl && this._graphID != null) {
            for (EntityDecorator entityDecorator : this._decorators) {
                maltegoEntity = entityDecorator.decorate(this._graphID, maltegoEntity);
            }
        }
        return maltegoEntity;
    }
}

