/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Lookup
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.entity.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import org.openide.util.Lookup;
import yguard.A.A.D;
import yguard.A.I.SA;

public abstract class LinkRegistry
extends SpecRegistry<MaltegoLinkSpec> {
    private static LinkRegistry _default;

    public static synchronized LinkRegistry getDefault() {
        if (_default == null && (LinkRegistry._default = (LinkRegistry)((Object)Lookup.getDefault().lookup(LinkRegistry.class))) == null) {
            throw new IllegalStateException("No link registry found.");
        }
        return _default;
    }

    public static synchronized LinkRegistry forGraph(D d) {
        return LinkRegistry.forGraphID(GraphIDProvider.forGraph((SA)((SA)d)));
    }

    public static synchronized LinkRegistry forGraphID(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = LinkRegistry.class.getName();
        return (LinkRegistry)((Object)graphUserData.get((Object)string));
    }

    public static synchronized void associate(GraphID graphID, LinkRegistry linkRegistry) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = LinkRegistry.class.getName();
        graphUserData.put((Object)string, (Object)linkRegistry);
    }

    public static class Composite
    extends LinkRegistry {
        private LinkRegistry[] _registries;
        private LinkRegistry _writeDelegate;

        public /* varargs */ Composite(LinkRegistry ... arrlinkRegistry) {
            this._registries = arrlinkRegistry;
            this._writeDelegate = this._registries[0];
        }

        public LinkRegistry[] getRegistries() {
            return this._registries;
        }

        public void setWriteDelegate(LinkRegistry linkRegistry) {
            this._writeDelegate = linkRegistry;
        }

        public LinkRegistry getWriteDelegate() {
            return this._writeDelegate;
        }

        public MaltegoLinkSpec get(String string) {
            for (LinkRegistry linkRegistry : this._registries) {
                MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)linkRegistry.get(string);
                if (maltegoLinkSpec == null) continue;
                return maltegoLinkSpec;
            }
            return null;
        }

        public Collection<MaltegoLinkSpec> getAll() {
            HashSet<MaltegoLinkSpec> hashSet = new HashSet<MaltegoLinkSpec>();
            for (int i = this._registries.length - 1; i >= 0; --i) {
                LinkRegistry linkRegistry = this._registries[i];
                hashSet.addAll(linkRegistry.getAll());
            }
            return hashSet;
        }

        public void put(MaltegoLinkSpec maltegoLinkSpec) {
            this._writeDelegate.put((TypeSpec)maltegoLinkSpec);
        }

        public void put(MaltegoLinkSpec maltegoLinkSpec, String string) {
            this._writeDelegate.put((TypeSpec)maltegoLinkSpec, string);
        }

        public void remove(String string) {
            this._writeDelegate.remove(string);
        }

        public boolean contains(String string) {
            for (LinkRegistry linkRegistry : this._registries) {
                if (!linkRegistry.contains(string)) continue;
                return true;
            }
            return false;
        }

        public String[] allCategories() {
            HashSet<String> hashSet = new HashSet<String>();
            for (LinkRegistry linkRegistry : this._registries) {
                hashSet.addAll(Arrays.asList(linkRegistry.allCategories()));
            }
            return hashSet.toArray(new String[hashSet.size()]);
        }
    }

    public static class Memory
    extends LinkRegistry {
        private static final String DEFAULT_FOLDER = "Miscellaneous";
        private HashMap<String, MaltegoLinkSpec> _items = new HashMap();
        private HashMap<String, String> _folders = new HashMap();

        public MaltegoLinkSpec get(String string) {
            return this._items.get(string);
        }

        public Collection<MaltegoLinkSpec> getAll() {
            return this._items.values();
        }

        public void put(MaltegoLinkSpec maltegoLinkSpec) {
            this.put(maltegoLinkSpec, maltegoLinkSpec.getDefaultCategory());
        }

        public void put(MaltegoLinkSpec maltegoLinkSpec, String string) {
            if (string == null) {
                string = "Miscellaneous";
            }
            this._items.put(maltegoLinkSpec.getTypeName(), maltegoLinkSpec);
            this._folders.put(maltegoLinkSpec.getTypeName(), string);
            this.fireTypeAdded((TypeSpec)maltegoLinkSpec);
        }

        public void remove(String string) {
            MaltegoLinkSpec maltegoLinkSpec = this.get(string);
            if (maltegoLinkSpec != null) {
                this._items.remove(string);
                this.fireTypeRemoved((TypeSpec)maltegoLinkSpec);
            }
        }

        public boolean contains(String string) {
            return this._items.containsKey(string);
        }

        public String[] allCategories() {
            return this._folders.values().toArray(new String[this._folders.size()]);
        }
    }

}

