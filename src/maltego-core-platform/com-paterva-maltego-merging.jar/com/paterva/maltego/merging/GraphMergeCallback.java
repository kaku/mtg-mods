/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;

public interface GraphMergeCallback {
    public void onEntityTransferred(MaltegoEntity var1, MaltegoEntity var2);

    public void onEntitiesMerged(MaltegoEntity var1, MaltegoEntity var2);

    public void onLinkTransferred(MaltegoLink var1, MaltegoLink var2);

    public void onLinksMerged(MaltegoLink var1, MaltegoLink var2);
}

