/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.merging.MergeUtils;

public abstract class PartMergeStrategy {
    public static final PartMergeStrategy KeepOriginal = new KeepOriginal();
    public static final PartMergeStrategy PreferOriginal = new PreferOriginal();
    public static final PartMergeStrategy PreferNew = new PreferNew();
    public static final PartMergeStrategy Replace = new Replace();
    public static final PartMergeStrategy Update = new Update();
    public static final PartMergeStrategy AddProperties = new AddProperties();
    public static final PartMergeStrategy DeleteProperties = new DeleteProperties();
    public static final PartMergeStrategy UpdateWithoutProperties = new UpdateWithoutProperties();

    public abstract MaltegoPart merge(MaltegoPart var1, MaltegoPart var2);

    private static class DeleteProperties
    extends PartMergeStrategy {
        private DeleteProperties() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            MergeUtils.removeProperties(maltegoPart, maltegoPart2);
            return maltegoPart;
        }
    }

    private static class AddProperties
    extends PartMergeStrategy {
        private AddProperties() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            MergeUtils.removeProperties(maltegoPart, maltegoPart2);
            MergeUtils.addMissingProperties(maltegoPart, maltegoPart2);
            MergeUtils.replacePropertyValues(maltegoPart, maltegoPart2, false);
            return maltegoPart;
        }
    }

    private static class UpdateWithoutProperties
    extends Update {
        private UpdateWithoutProperties() {
            super();
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            this.updateTypeName(maltegoPart, maltegoPart2);
            this.updateOther(maltegoPart, maltegoPart2);
            return maltegoPart;
        }
    }

    private static class Update
    extends PartMergeStrategy {
        private Update() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            this.updateTypeName(maltegoPart, maltegoPart2);
            MergeUtils.addMissingProperties(maltegoPart, maltegoPart2);
            MergeUtils.replacePropertyValues(maltegoPart, maltegoPart2, false);
            MergeUtils.updateSpecialPropertyMapping(maltegoPart, maltegoPart2);
            this.updateOther(maltegoPart, maltegoPart2);
            return maltegoPart;
        }

        protected void updateTypeName(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            if (maltegoPart2.getTypeName() != null) {
                maltegoPart.setTypeName(maltegoPart2.getTypeName());
            }
        }

        protected void updateOther(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            Integer n;
            String string;
            Boolean bl;
            DisplayInformationCollection displayInformationCollection = maltegoPart2.getDisplayInformation();
            if (displayInformationCollection != null) {
                MergeUtils.replaceDisplayInformation(maltegoPart, maltegoPart2);
            }
            if ((string = maltegoPart2.getNotes()) != null) {
                maltegoPart.setNotes(string);
            }
            if ((bl = maltegoPart2.isShowNotesValue()) != null) {
                maltegoPart.setShowNotes(bl);
            }
            if ((n = maltegoPart2.getBookmarkValue()) != null) {
                maltegoPart.setBookmark(n);
            }
            if (maltegoPart instanceof MaltegoEntity && maltegoPart2 instanceof MaltegoEntity) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
                MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart2;
                Integer n2 = maltegoEntity2.getWeightValue();
                if (n2 != null) {
                    maltegoEntity.setWeight(n2);
                }
            } else if (maltegoPart instanceof MaltegoLink && maltegoPart2 instanceof MaltegoLink) {
                MaltegoLink maltegoLink = (MaltegoLink)maltegoPart;
                MaltegoLink maltegoLink2 = (MaltegoLink)maltegoPart2;
                Boolean bl2 = maltegoLink2.isReversed();
                if (bl2 != null) {
                    maltegoLink.setReversed(bl2);
                }
            }
        }
    }

    private static class Replace
    extends PartMergeStrategy {
        private Replace() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            maltegoPart.setTypeName(maltegoPart2.getTypeName());
            MergeUtils.replaceProperties(maltegoPart, maltegoPart2);
            MergeUtils.replacePropertyValues(maltegoPart, maltegoPart2, false);
            MergeUtils.copySpecialPropertyMapping(maltegoPart, maltegoPart2, true);
            MergeUtils.replaceDisplayInformation(maltegoPart, maltegoPart2);
            maltegoPart.setNotes(maltegoPart2.getNotes());
            maltegoPart.setShowNotes(maltegoPart2.isShowNotesValue());
            maltegoPart.setBookmark(Integer.valueOf(maltegoPart2.getBookmark()));
            if (maltegoPart instanceof MaltegoEntity && maltegoPart2 instanceof MaltegoEntity) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
                MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart2;
                maltegoEntity.setWeight(maltegoEntity2.getWeightValue());
            } else if (maltegoPart instanceof MaltegoLink && maltegoPart2 instanceof MaltegoLink) {
                MaltegoLink maltegoLink = (MaltegoLink)maltegoPart;
                MaltegoLink maltegoLink2 = (MaltegoLink)maltegoPart2;
                maltegoLink.setReversed(maltegoLink2.isReversed());
            }
            return maltegoPart;
        }
    }

    private static class PreferNew
    extends PartMergeStrategy {
        private PreferNew() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            MergeUtils.addMissingProperties(maltegoPart, maltegoPart2);
            MergeUtils.mergePropertyValues(maltegoPart, maltegoPart2, true);
            MergeUtils.mergeSpecialPropertyMapping(maltegoPart, maltegoPart2, true);
            MergeUtils.mergeDisplayInformation(maltegoPart, maltegoPart2, true);
            MergeUtils.mergeNotes(maltegoPart, maltegoPart2, true);
            MergeUtils.mergeWeight(maltegoPart, maltegoPart2);
            MergeUtils.mergeReversed(maltegoPart, maltegoPart2, true);
            if (maltegoPart2.getBookmark() >= 0) {
                maltegoPart.setBookmark(Integer.valueOf(maltegoPart2.getBookmark()));
            }
            return maltegoPart;
        }
    }

    private static class PreferOriginal
    extends PartMergeStrategy {
        private PreferOriginal() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            MergeUtils.addMissingProperties(maltegoPart, maltegoPart2);
            MergeUtils.mergePropertyValues(maltegoPart, maltegoPart2, false);
            MergeUtils.mergeSpecialPropertyMapping(maltegoPart, maltegoPart2, false);
            MergeUtils.mergeDisplayInformation(maltegoPart, maltegoPart2, false);
            MergeUtils.mergeNotes(maltegoPart, maltegoPart2, false);
            MergeUtils.mergeWeight(maltegoPart, maltegoPart2);
            MergeUtils.mergeReversed(maltegoPart, maltegoPart2, false);
            return maltegoPart;
        }
    }

    private static class KeepOriginal
    extends PartMergeStrategy {
        private KeepOriginal() {
        }

        public MaltegoPart merge(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            return maltegoPart;
        }
    }

}

