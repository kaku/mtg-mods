/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.merging.EntityFilter;
import java.util.Map;

public interface GraphMerger {
    public void setGraphs(GraphID var1, GraphID var2, EntityFilter var3);

    public Map<MaltegoEntity, MaltegoEntity> getMatches();

    public boolean append();

    public Map<MaltegoEntity, MaltegoEntity> getEntityMapping();

    public Map<MaltegoLink, MaltegoLink> getLinkMapping();
}

