/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.merging.DefaultGraphMergerFactory;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.util.SimilarStrings;
import org.openide.util.Lookup;

public abstract class GraphMergerFactory {
    private static GraphMergerFactory _default;

    public static GraphMergerFactory getDefault() {
        if (_default == null && (GraphMergerFactory._default = (GraphMergerFactory)Lookup.getDefault().lookup(GraphMergerFactory.class)) == null) {
            _default = new DefaultGraphMergerFactory();
        }
        return _default;
    }

    public abstract GraphMerger create(SimilarStrings var1, GraphMatchStrategy var2, GraphMergeStrategy var3, GraphMergeCallback var4, boolean var5, boolean var6);
}

