/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Utilities;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class MergeUtils {
    private static final Logger LOG = Logger.getLogger(MergeUtils.class.getName());

    private MergeUtils() {
    }

    public static void addMissingProperties(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        for (PropertyDescriptor propertyDescriptor : maltegoPart2.getProperties()) {
            if (maltegoPart.getProperties().contains(propertyDescriptor)) continue;
            maltegoPart.addProperty(propertyDescriptor);
        }
    }

    public static void removeExtraProperties(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor : maltegoPart.getProperties()) {
            if (maltegoPart2.getProperties().contains(propertyDescriptor)) continue;
            arrayList.add(propertyDescriptor);
        }
        MergeUtils.removeProperties(maltegoPart, arrayList);
    }

    public static void removeProperties(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor : maltegoPart.getProperties()) {
            if (!maltegoPart2.getProperties().contains(propertyDescriptor)) continue;
            arrayList.add(propertyDescriptor);
        }
        MergeUtils.removeProperties(maltegoPart, arrayList);
    }

    public static void removeProperties(MaltegoPart maltegoPart, List<PropertyDescriptor> list) {
        for (PropertyDescriptor propertyDescriptor : list) {
            maltegoPart.removeProperty(propertyDescriptor.getName());
        }
    }

    public static void replaceProperties(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        maltegoPart.getProperties().clear();
        maltegoPart.getProperties().addAll(maltegoPart2.getProperties());
    }

    public static void mergePropertyValues(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        MergeUtils.copyCalculatedPropertyValues(maltegoPart, maltegoPart2, bl, false);
        MergeUtils.copyNonCalculatedPropertyValues(maltegoPart, maltegoPart2, bl, false);
    }

    public static void replacePropertyValues(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        if (bl) {
            MergeUtils.copyCalculatedPropertyValues(maltegoPart, maltegoPart2, true, true);
            MergeUtils.copyNonCalculatedPropertyValues(maltegoPart, maltegoPart2, true, true);
        } else {
            MergeUtils.copyPropertyValuesNoRefresh(maltegoPart, maltegoPart2);
        }
    }

    public static void copyCalculatedPropertyValues(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl, boolean bl2) {
        for (PropertyDescriptor propertyDescriptor : maltegoPart2.getProperties()) {
            PropertyDescriptor propertyDescriptor2 = maltegoPart.getProperties().get(propertyDescriptor.getName());
            if (!MergeUtils.isCalculated(propertyDescriptor2)) continue;
            Object object = maltegoPart2.getValue(propertyDescriptor);
            Object object2 = maltegoPart.getValue(propertyDescriptor2);
            object = MergeUtils.convert(object, propertyDescriptor, propertyDescriptor2);
            if (!bl2 && MergeUtils.destIsBetter(object, object2) || !bl && !MergeUtils.isNullOrEmpty(object2)) continue;
            maltegoPart.setValue(propertyDescriptor2, object);
        }
    }

    public static void copyNonCalculatedPropertyValues(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl, boolean bl2) {
        for (PropertyDescriptor propertyDescriptor : maltegoPart2.getProperties()) {
            PropertyDescriptor propertyDescriptor2 = maltegoPart.getProperties().get(propertyDescriptor.getName());
            if (MergeUtils.isCalculated(propertyDescriptor2)) continue;
            Object object = maltegoPart2.getValue(propertyDescriptor);
            Object object2 = maltegoPart.getValue(propertyDescriptor2);
            object = MergeUtils.convert(object, propertyDescriptor, propertyDescriptor2);
            if (!bl2 && MergeUtils.destIsBetter(object, object2)) continue;
            if (!bl2 && Attachments.class.equals((Object)propertyDescriptor2.getType()) && object2 != null) {
                MergeUtils.mergeAttachments((Attachments)object2, (Attachments)object, bl);
                continue;
            }
            if (!bl && !MergeUtils.isNullOrEmpty(object2)) continue;
            maltegoPart.setValue(propertyDescriptor2, object);
        }
    }

    public static void copyPropertyValuesNoRefresh(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        for (PropertyDescriptor propertyDescriptor : maltegoPart2.getProperties()) {
            PropertyDescriptor propertyDescriptor2 = maltegoPart.getProperties().get(propertyDescriptor.getName());
            Object object = maltegoPart2.getValue(propertyDescriptor);
            Object object2 = maltegoPart.getValue(propertyDescriptor2);
            object = MergeUtils.convert(object, propertyDescriptor, propertyDescriptor2);
            if (Attachments.class.equals((Object)propertyDescriptor2.getType()) && object2 != null && !(maltegoPart2 instanceof EntityUpdate)) {
                MergeUtils.mergeAttachments((Attachments)object2, (Attachments)object, true);
                continue;
            }
            maltegoPart.setValue(propertyDescriptor2, object, false, false);
        }
    }

    private static Object convert(Object object, PropertyDescriptor propertyDescriptor, PropertyDescriptor propertyDescriptor2) throws IllegalArgumentException {
        if (object != null && !propertyDescriptor2.getType().equals(propertyDescriptor.getType())) {
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor2.getType());
            if (!(object instanceof String)) {
                TypeDescriptor typeDescriptor2 = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
                object = typeDescriptor2.convert(object);
            }
            if (object instanceof String) {
                object = typeDescriptor.convert((String)object);
            }
        }
        return object;
    }

    private static void mergeAttachments(Attachments attachments, Attachments attachments2, boolean bl) {
        for (Attachment attachment : attachments2) {
            if (attachments.contains((Object)attachment)) continue;
            attachments.add((Object)attachment);
        }
        if (bl) {
            attachments.setPrimaryImage(attachments2.getPrimaryImage());
        }
    }

    public static void mergeSpecialPropertyMapping(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        maltegoPart.setDisplayValueProperty(MergeUtils.nonNull(maltegoPart.getDisplayValueProperty(), maltegoPart2.getDisplayValueProperty(), bl));
        maltegoPart.setValueProperty(MergeUtils.nonNull(maltegoPart.getValueProperty(), maltegoPart2.getValueProperty(), bl));
        if (maltegoPart instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart2;
            maltegoEntity.setImageProperty(MergeUtils.nonNull(maltegoEntity.getImageProperty(), maltegoEntity2.getImageProperty(), bl));
        }
    }

    public static void copySpecialPropertyMapping(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        maltegoPart.setDisplayValueProperty(maltegoPart2.getDisplayValueProperty());
        maltegoPart.setValueProperty(maltegoPart2.getValueProperty());
        if (maltegoPart instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart2;
            maltegoEntity.setImageProperty(maltegoEntity2.getImageProperty());
        }
    }

    public static void updateSpecialPropertyMapping(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        String string;
        MaltegoEntity maltegoEntity;
        String string2;
        String string3 = maltegoPart2.getDisplayValuePropertyName();
        if (string3 != null) {
            maltegoPart.setDisplayValueProperty(maltegoPart.getProperties().get(string3));
        }
        if ((string = maltegoPart2.getValuePropertyName()) != null) {
            maltegoPart.setValueProperty(maltegoPart.getProperties().get(string));
        }
        if (maltegoPart instanceof MaltegoEntity && (string2 = (maltegoEntity = (MaltegoEntity)maltegoPart2).getImagePropertyName()) != null) {
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart;
            maltegoEntity2.setImageProperty(maltegoPart.getProperties().get(string2));
        }
    }

    public static void replaceDisplayInformation(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        DisplayInformationCollection displayInformationCollection;
        DisplayInformationCollection displayInformationCollection2 = maltegoPart2.getDisplayInformation();
        if (displayInformationCollection2 == null) {
            displayInformationCollection = null;
        } else {
            displayInformationCollection = maltegoPart.getOrCreateDisplayInformation();
            displayInformationCollection.clear();
            displayInformationCollection.addAll((Collection)displayInformationCollection2);
        }
        maltegoPart.setDisplayInformation(displayInformationCollection);
    }

    public static void mergeDisplayInformation(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        DisplayInformationCollection displayInformationCollection = maltegoPart2.getDisplayInformation();
        if (displayInformationCollection != null) {
            DisplayInformationCollection displayInformationCollection2 = maltegoPart.getOrCreateDisplayInformation();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Display Info Before: {0}", displayInformationCollection2.toString());
            }
            DisplayInformationCollection displayInformationCollection3 = bl ? displayInformationCollection : displayInformationCollection2;
            DisplayInformationCollection displayInformationCollection4 = bl ? displayInformationCollection2 : displayInformationCollection;
            for (DisplayInformation displayInformation : displayInformationCollection4) {
                boolean bl2 = false;
                for (DisplayInformation displayInformation2 : displayInformationCollection3) {
                    if (!displayInformation.getName().equals(displayInformation2.getName()) || !Utilities.compareObjects((Object)displayInformation.getValue(), (Object)displayInformation2.getValue())) continue;
                    bl2 = true;
                    break;
                }
                if (bl2) continue;
                displayInformationCollection3.add(displayInformation);
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Display Info After: {0}", displayInformationCollection3.toString());
            }
            maltegoPart.setDisplayInformation(displayInformationCollection3);
        }
    }

    public static void mergeNotes(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        String string;
        String string2 = bl ? maltegoPart.getNotes() : maltegoPart2.getNotes();
        String string3 = string = bl ? maltegoPart2.getNotes() : maltegoPart.getNotes();
        if (Utilities.compareObjects((Object)string2, (Object)string)) {
            return;
        }
        String string4 = "";
        if (!MergeUtils.isNullOrEmpty(string2)) {
            string4 = string2;
        }
        if (!MergeUtils.isNullOrEmpty(string)) {
            string4 = !MergeUtils.isNullOrEmpty(string4) ? string4 + "\n" + string : string;
        }
        if (!MergeUtils.isNullOrEmpty(string4) && (maltegoPart.isShowNotes() || maltegoPart2.isShowNotes())) {
            maltegoPart.setShowNotes(Boolean.valueOf(true));
        }
        maltegoPart.setNotes(string4);
    }

    public static void mergeWeight(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        if (maltegoPart instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart2;
            int n = (maltegoEntity.getWeight() + maltegoEntity2.getWeight()) / 2;
            maltegoEntity.setWeight(Integer.valueOf(n));
        }
    }

    public static void mergeReversed(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, boolean bl) {
        if (bl && maltegoPart instanceof MaltegoLink) {
            MaltegoLink maltegoLink = (MaltegoLink)maltegoPart;
            MaltegoLink maltegoLink2 = (MaltegoLink)maltegoPart2;
            maltegoLink.setReversed(maltegoLink2.isReversed());
        }
    }

    public static boolean isCalculated(PropertyDescriptor propertyDescriptor) {
        return propertyDescriptor.getLinkedProperties().size() > 0;
    }

    public static PropertyDescriptor nonNull(PropertyDescriptor propertyDescriptor, PropertyDescriptor propertyDescriptor2, boolean bl) {
        PropertyDescriptor propertyDescriptor3 = bl ? propertyDescriptor2 : propertyDescriptor;
        PropertyDescriptor propertyDescriptor4 = bl ? propertyDescriptor : propertyDescriptor2;
        return propertyDescriptor3 != null ? propertyDescriptor3 : propertyDescriptor4;
    }

    public static boolean destIsBetter(Object object, Object object2) {
        return object == null || MergeUtils.isEmpty(object) && object2 != null;
    }

    public static boolean isNullOrEmpty(Object object) {
        return object == null || MergeUtils.isEmpty(object);
    }

    public static boolean isEmpty(Object object) {
        if (object instanceof String) {
            return ((String)object).isEmpty();
        }
        return false;
    }
}

