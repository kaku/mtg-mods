/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.GraphMergerFactory;
import com.paterva.maltego.merging.StrategicGraphMerger;
import com.paterva.maltego.util.SimilarStrings;

public class DefaultGraphMergerFactory
extends GraphMergerFactory {
    public GraphMerger create(SimilarStrings similarStrings, GraphMatchStrategy graphMatchStrategy, GraphMergeStrategy graphMergeStrategy, GraphMergeCallback graphMergeCallback, boolean bl, boolean bl2) {
        return new StrategicGraphMerger(graphMatchStrategy, graphMergeStrategy, graphMergeCallback, bl2);
    }
}

