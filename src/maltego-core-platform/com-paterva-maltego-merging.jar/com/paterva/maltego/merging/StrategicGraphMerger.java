/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.matching.MatchingRule
 *  com.paterva.maltego.matching.MatchingRuleFactory
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.EntityLimitNotifier
 *  com.paterva.maltego.util.ProductRestrictions
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.EntityLimitNotifier;
import com.paterva.maltego.util.ProductRestrictions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class StrategicGraphMerger
implements GraphMerger {
    private static final Logger LOG = Logger.getLogger(StrategicGraphMerger.class.getName());
    private GraphID _destGraphID;
    private GraphID _srcGraphID;
    private GraphStore _srcGraphStore;
    private EntityRegistry _destEntityRegistry;
    private final GraphMatchStrategy _graphMatchStrat;
    private final GraphMergeStrategy _graphMergeStrat;
    private final GraphMergeCallback _cb;
    private final boolean _useGraphStoreMatching;
    private Map<MaltegoEntity, MaltegoEntity> _entityMatches;
    private Map<MaltegoEntity, MaltegoEntity> _entityMap;
    private Map<MaltegoEntity, MaltegoEntity> _newEntityMap;
    private Map<MaltegoLink, MaltegoLink> _linkMap;
    private Map<MaltegoLink, MaltegoLink> _newLinkMap;
    private EntityFilter _filter;

    protected StrategicGraphMerger(GraphMatchStrategy graphMatchStrategy, GraphMergeStrategy graphMergeStrategy, GraphMergeCallback graphMergeCallback, boolean bl) {
        this._graphMatchStrat = graphMatchStrategy;
        this._graphMergeStrat = graphMergeStrategy;
        this._cb = graphMergeCallback;
        this._useGraphStoreMatching = bl;
    }

    @Override
    public void setGraphs(GraphID graphID, GraphID graphID2, EntityFilter entityFilter) {
        this._destGraphID = graphID;
        this._srcGraphID = graphID2;
        this._entityMatches = null;
        this._entityMap = null;
        this._newEntityMap = null;
        this._linkMap = null;
        this._newLinkMap = null;
        this._filter = entityFilter;
        try {
            this._srcGraphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
        }
        catch (GraphStoreException var4_4) {
            Exceptions.printStackTrace((Throwable)var4_4);
        }
        this._destEntityRegistry = EntityRegistry.forGraphID((GraphID)this._destGraphID);
    }

    public GraphID getDestGraphID() {
        return this._destGraphID;
    }

    public GraphID getSrcGraphID() {
        return this._srcGraphID;
    }

    @Override
    public Map<MaltegoEntity, MaltegoEntity> getMatches() {
        if (this._entityMatches == null) {
            Object object;
            GraphDataStoreReader graphDataStoreReader2;
            Map map;
            MatchingRuleDescriptor matchingRuleDescriptor = this._graphMatchStrat.getEntityMatchingRule();
            Set set = GraphStoreHelper.getMaltegoEntities((GraphID)this._srcGraphID);
            this._entityMatches = new HashMap<MaltegoEntity, MaltegoEntity>();
            if (this._useGraphStoreMatching) {
                LOG.fine("Matching in graph store");
                try {
                    object = this._destGraphID;
                    GraphStore object2 = GraphStoreRegistry.getDefault().forGraphID((GraphID)object);
                    graphDataStoreReader2 = object2.getGraphDataStore().getDataStoreReader();
                    map = Collections.EMPTY_MAP;
                    try {
                        map = graphDataStoreReader2.getEntityMatches((Collection)set, matchingRuleDescriptor);
                    }
                    catch (Exception var7_12) {
                        Exceptions.printStackTrace((Throwable)var7_12);
                    }
                    EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
                    entitiesDataQuery.setIDs(new HashSet(map.values()));
                    EntityDataQuery entityDataQuery = new EntityDataQuery();
                    entityDataQuery.setAllProperties(true);
                    entityDataQuery.setAllSections(true);
                    entitiesDataQuery.setPartDataQuery((PartDataQuery)entityDataQuery);
                    Map map2 = graphDataStoreReader2.getEntities(entitiesDataQuery);
                    for (Map.Entry entry : map.entrySet()) {
                        MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getKey();
                        EntityID entityID = (EntityID)entry.getValue();
                        MaltegoEntity maltegoEntity2 = (MaltegoEntity)map2.get((Object)entityID);
                        this._entityMatches.put(maltegoEntity, maltegoEntity2);
                    }
                }
                catch (GraphStoreException var3_4) {
                    Exceptions.printStackTrace((Throwable)var3_4);
                }
            } else {
                LOG.fine("Matching in memory");
                object = MatchingRuleFactory.createFrom((MatchingRuleDescriptor)matchingRuleDescriptor);
                for (GraphDataStoreReader graphDataStoreReader2 : set) {
                    map = this.findEntity((MatchingRule)object, (MaltegoEntity)graphDataStoreReader2);
                    if (map == null) continue;
                    this._entityMatches.put((MaltegoEntity)graphDataStoreReader2, (MaltegoEntity)map);
                }
            }
            if (LOG.isLoggable(Level.FINEST)) {
                if (this._entityMatches.isEmpty()) {
                    LOG.finest("No entity matches");
                } else {
                    LOG.finest("Entity matches: ");
                    for (Map.Entry entry : this._entityMatches.entrySet()) {
                        graphDataStoreReader2 = (MaltegoEntity)entry.getKey();
                        map = (MaltegoEntity)entry.getValue();
                        LOG.log(Level.FINE, "{0}->{1}", new Object[]{graphDataStoreReader2, map});
                    }
                }
            }
        }
        return this._entityMatches;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean append() {
        boolean bl;
        LOG.log(Level.FINE, "Merging graph {0} with {1}", new Object[]{this._srcGraphID, this._destGraphID});
        bl = true;
        GraphStore graphStore = null;
        try {
            if (!GraphStoreRegistry.getDefault().isExistingAndOpen(this._destGraphID) || !GraphStoreRegistry.getDefault().isExistingAndOpen(this._srcGraphID)) {
                bl = false;
            } else {
                graphStore = GraphStoreRegistry.getDefault().forGraphID(this._destGraphID);
                graphStore.beginUpdate();
                GraphStore graphStore2 = GraphStoreRegistry.getDefault().forGraphID(this._srcGraphID);
                int n = this.getEntityCount(graphStore);
                int n2 = this.getEntityCount(graphStore2);
                if (n + n2 > ProductRestrictions.getGraphSizeLimit()) {
                    EntityLimitNotifier entityLimitNotifier = (EntityLimitNotifier)Lookup.getDefault().lookup(EntityLimitNotifier.class);
                    entityLimitNotifier.notifyLimitExceeded();
                    bl = false;
                }
            }
            if (bl) {
                this.appendEntities();
                this.appendLinks();
            }
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)null);
            }
        }
        return bl;
    }

    @Override
    public Map<MaltegoEntity, MaltegoEntity> getEntityMapping() {
        return this._entityMap;
    }

    @Override
    public Map<MaltegoLink, MaltegoLink> getLinkMapping() {
        return this._linkMap;
    }

    private void appendEntities() throws GraphStoreException {
        MaltegoEntity maltegoEntity22;
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)this._srcGraphID);
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("Entities to merge: ");
            for (MaltegoEntity maltegoEntity22 : set) {
                LOG.finest(maltegoEntity22.toString());
            }
        }
        this._entityMap = new HashMap<MaltegoEntity, MaltegoEntity>(set.size());
        this._newEntityMap = new HashMap<MaltegoEntity, MaltegoEntity>();
        for (MaltegoEntity maltegoEntity22 : set) {
            if (this.isInputEntity(maltegoEntity22)) {
                this.addEntity(maltegoEntity22);
                continue;
            }
            arrayList.add(maltegoEntity22);
        }
        for (MaltegoEntity maltegoEntity22 : arrayList) {
            this.addEntity(maltegoEntity22);
        }
        GraphStoreWriter.addEntities((GraphID)this._destGraphID, this._newEntityMap.keySet());
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._destGraphID);
        maltegoEntity22 = graphStore.getGraphDataStore().getDataStoreWriter();
        maltegoEntity22.updateEntities(this._entityMap.values());
        this.transferPinned();
        for (Map.Entry<MaltegoEntity, MaltegoEntity> entry : this._newEntityMap.entrySet()) {
            MaltegoEntity maltegoEntity3 = entry.getKey();
            MaltegoEntity maltegoEntity4 = entry.getValue();
            this.onEntityAdded(maltegoEntity3, maltegoEntity4);
        }
    }

    private void transferPinned() throws GraphStoreException {
        Object object;
        HashSet<Guid> hashSet = new HashSet<Guid>();
        for (Map.Entry<MaltegoEntity, MaltegoEntity> object22 : this._newEntityMap.entrySet()) {
            object = object22.getValue();
            hashSet.add(object.getID());
        }
        Map map = GraphStoreHelper.isPinned((GraphID)this._srcGraphID, hashSet);
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : this._newEntityMap.entrySet()) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getKey();
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)entry.getValue();
            hashMap.put(maltegoEntity.getID(), map.get((Object)maltegoEntity2.getID()));
        }
        object = GraphStoreRegistry.getDefault().forGraphID(this._destGraphID);
        GraphStructureWriter graphStructureWriter = object.getGraphStructureStore().getStructureWriter();
        graphStructureWriter.setPinned(hashMap);
    }

    private void addEntity(MaltegoEntity maltegoEntity) {
        if (this._filter == null || this._filter.accept(maltegoEntity)) {
            MaltegoEntity maltegoEntity2 = this.getMatches().get((Object)maltegoEntity);
            MaltegoEntity maltegoEntity3 = null;
            PartMergeStrategy partMergeStrategy = null;
            if (maltegoEntity2 != null && (partMergeStrategy = this._graphMergeStrat.getEntityMergeStrategy(maltegoEntity2, maltegoEntity)) != null) {
                maltegoEntity3 = this.tryMergeEntities(maltegoEntity2, maltegoEntity, partMergeStrategy);
            }
            if (maltegoEntity3 == null && maltegoEntity.getTypeName() != null) {
                maltegoEntity3 = partMergeStrategy == null ? maltegoEntity.createCopy() : maltegoEntity.createClone();
                this._newEntityMap.put(maltegoEntity3, maltegoEntity);
            }
            if (maltegoEntity3 != null) {
                this._entityMap.put(maltegoEntity, maltegoEntity3);
            }
        }
    }

    private boolean isInputEntity(MaltegoEntity maltegoEntity) throws GraphStoreException {
        EntityID entityID;
        GraphStructureReader graphStructureReader = this._srcGraphStore.getGraphStructureStore().getStructureReader();
        return graphStructureReader.getIncoming(entityID = (EntityID)maltegoEntity.getID()).isEmpty() && graphStructureReader.getOutgoing(entityID).size() > 0;
    }

    private void appendLinks() throws GraphStoreException {
        MaltegoLink maltegoLink;
        MaltegoLink maltegoLink2;
        GraphStructureReader graphStructureReader = this._srcGraphStore.getGraphStructureStore().getStructureReader();
        this._linkMap = new HashMap<MaltegoLink, MaltegoLink>(graphStructureReader.getLinkCount());
        this._newLinkMap = new HashMap<MaltegoLink, MaltegoLink>();
        MatchingRuleDescriptor matchingRuleDescriptor = this._graphMatchStrat.getLinkMatchingRule();
        MatchingRule matchingRule = matchingRuleDescriptor != null ? MatchingRuleFactory.createFrom((MatchingRuleDescriptor)matchingRuleDescriptor) : null;
        GraphDataStoreReader graphDataStoreReader = this._srcGraphStore.getGraphDataStore().getDataStoreReader();
        Map map = graphDataStoreReader.getLinks(new LinksDataQuery());
        for (Map.Entry iterator2 : map.entrySet()) {
            MaltegoLink maltegoLink3 = (MaltegoLink)iterator2.getValue();
            MaltegoLink maltegoLink4 = maltegoLink2 = matchingRule != null ? this.tryMergeLink(matchingRule, maltegoLink3) : null;
            if (maltegoLink2 == null && maltegoLink3.getTypeName() != null) {
                maltegoLink2 = maltegoLink3.createCopy();
                this._newLinkMap.put(maltegoLink2, maltegoLink3);
            }
            if (maltegoLink2 == null) continue;
            this._linkMap.put(maltegoLink3, maltegoLink2);
        }
        HashMap hashMap = new HashMap(this._newLinkMap.size());
        for (Map.Entry<MaltegoLink, MaltegoLink> entry : this._newLinkMap.entrySet()) {
            maltegoLink2 = entry.getKey();
            maltegoLink = entry.getValue();
            MaltegoEntity maltegoEntity = this.getDestEntityForSrcID(this._entityMap, graphStructureReader.getSource((LinkID)maltegoLink.getID()));
            MaltegoEntity maltegoEntity2 = this.getDestEntityForSrcID(this._entityMap, graphStructureReader.getTarget((LinkID)maltegoLink.getID()));
            if (maltegoEntity == null || maltegoEntity2 == null || maltegoEntity.equals((Object)maltegoEntity2)) continue;
            hashMap.put(maltegoLink2, new LinkEntityIDs((EntityID)maltegoEntity.getID(), (EntityID)maltegoEntity2.getID()));
        }
        if (LOG.isLoggable(Level.FINEST)) {
            for (Map.Entry entry2 : hashMap.entrySet()) {
                maltegoLink2 = (MaltegoLink)entry2.getKey();
                maltegoLink = (LinkEntityIDs)entry2.getValue();
                LOG.log(Level.FINEST, "Adding link {0}: {1}->{2}", new Object[]{maltegoLink2.getID(), maltegoLink.getSourceID(), maltegoLink.getTargetID()});
            }
        }
        GraphStoreWriter.addLinks((GraphID)this._destGraphID, (Map)hashMap);
        Iterator iterator = hashMap.keySet().iterator();
        while (iterator.hasNext()) {
            MaltegoLink maltegoLink5;
            maltegoLink2 = maltegoLink5 = (MaltegoLink)iterator.next();
            maltegoLink = this._newLinkMap.get((Object)maltegoLink2);
            this.onLinkAdded(maltegoLink2, maltegoLink);
        }
    }

    private MaltegoEntity getDestEntityForSrcID(Map<MaltegoEntity, MaltegoEntity> map, EntityID entityID) {
        EntityUpdate entityUpdate = new EntityUpdate(entityID);
        return map.get((Object)entityUpdate);
    }

    private MaltegoEntity tryMergeEntities(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2, PartMergeStrategy partMergeStrategy) {
        MaltegoEntity maltegoEntity3 = maltegoEntity.createClone();
        partMergeStrategy.merge((MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2);
        this.onEntitiesMerged(maltegoEntity3, maltegoEntity, maltegoEntity2);
        return maltegoEntity;
    }

    private MaltegoLink tryMergeLink(MatchingRule matchingRule, MaltegoLink maltegoLink) {
        PartMergeStrategy partMergeStrategy;
        MaltegoLink maltegoLink2 = this.findLink(matchingRule, maltegoLink);
        if (maltegoLink2 != null && (partMergeStrategy = this._graphMergeStrat.getLinkMergeStrategy(maltegoLink2, maltegoLink)) != null) {
            MaltegoLink maltegoLink3 = maltegoLink2.createClone();
            partMergeStrategy.merge((MaltegoPart)maltegoLink2, (MaltegoPart)maltegoLink);
            this.onLinksMerged(maltegoLink3, maltegoLink2, maltegoLink);
            return maltegoLink2;
        }
        return null;
    }

    private MaltegoEntity findEntity(MatchingRule matchingRule, MaltegoEntity maltegoEntity) {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._destGraphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            EntityID entityID = (EntityID)maltegoEntity.getID();
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            if (graphStructureReader.exists(entityID)) {
                GraphDataStoreReader graphDataStoreReader2 = graphDataStoreReader;
                return graphDataStoreReader2.getEntity(entityID);
            }
            Map map = graphDataStoreReader.getEntities(new EntitiesDataQuery());
            for (Map.Entry entry : map.entrySet()) {
                MaltegoEntity maltegoEntity2 = (MaltegoEntity)entry.getValue();
                if (!this.isMatch((MaltegoPart)maltegoEntity2, (MaltegoPart)maltegoEntity, matchingRule)) continue;
                return maltegoEntity2;
            }
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return null;
    }

    private MaltegoLink findLink(MatchingRule matchingRule, MaltegoLink maltegoLink) {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._srcGraphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            GraphStore graphStore2 = GraphStoreRegistry.getDefault().forGraphID(this._destGraphID);
            GraphDataStoreReader graphDataStoreReader = graphStore2.getGraphDataStore().getDataStoreReader();
            GraphStructureReader graphStructureReader2 = graphStore2.getGraphStructureStore().getStructureReader();
            MaltegoEntity maltegoEntity = this.getDestEntityForSrcID(this._entityMap, graphStructureReader.getSource((LinkID)maltegoLink.getID()));
            MaltegoEntity maltegoEntity2 = this.getDestEntityForSrcID(this._entityMap, graphStructureReader.getTarget((LinkID)maltegoLink.getID()));
            if (maltegoEntity != null && maltegoEntity2 != null) {
                for (LinkID linkID : graphStructureReader2.getOutgoing((EntityID)maltegoEntity.getID())) {
                    for (LinkID linkID2 : graphStructureReader2.getIncoming((EntityID)maltegoEntity2.getID())) {
                        MaltegoLink maltegoLink2;
                        if (!linkID.equals((Object)linkID2) || !this.isMatch((MaltegoPart)(maltegoLink2 = graphDataStoreReader.getLink(linkID)), (MaltegoPart)maltegoLink, matchingRule)) continue;
                        return maltegoLink2;
                    }
                }
            }
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return null;
    }

    private boolean isMatch(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, MatchingRule matchingRule) {
        SpecRegistry specRegistry;
        int n;
        if (matchingRule != null && (n = matchingRule.match(specRegistry = this.getRegistry(maltegoPart), (TypedPropertyBag)maltegoPart, (TypedPropertyBag)maltegoPart2)) == 1) {
            return true;
        }
        return false;
    }

    private SpecRegistry getRegistry(MaltegoPart maltegoPart) {
        EntityRegistry entityRegistry = null;
        if (maltegoPart instanceof MaltegoEntity) {
            entityRegistry = this._destEntityRegistry;
        } else if (maltegoPart instanceof MaltegoLink) {
            entityRegistry = LinkRegistry.getDefault();
        }
        return entityRegistry;
    }

    protected void onEntityAdded(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        if (this._cb != null) {
            this._cb.onEntityTransferred(maltegoEntity, maltegoEntity2);
        }
    }

    protected void onEntitiesMerged(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2, MaltegoEntity maltegoEntity3) {
        if (this._cb != null) {
            this._cb.onEntitiesMerged(maltegoEntity2, maltegoEntity3);
        }
    }

    protected void onLinkAdded(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        if (this._cb != null) {
            this._cb.onLinkTransferred(maltegoLink, maltegoLink2);
        }
    }

    protected void onLinksMerged(MaltegoLink maltegoLink, MaltegoLink maltegoLink2, MaltegoLink maltegoLink3) {
        if (this._cb != null) {
            this._cb.onLinksMerged(maltegoLink2, maltegoLink3);
        }
    }

    private int getEntityCount(GraphStore graphStore) throws GraphStoreException {
        return graphStore.getGraphStructureStore().getStructureReader().getEntityCount();
    }
}

