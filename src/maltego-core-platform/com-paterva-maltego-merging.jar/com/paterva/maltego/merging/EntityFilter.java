/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.MaltegoEntity;

public interface EntityFilter {
    public boolean accept(MaltegoEntity var1);
}

