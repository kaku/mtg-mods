/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.merging;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.merging.PartMergeStrategy;

public interface GraphMergeStrategy {
    public PartMergeStrategy getEntityMergeStrategy(MaltegoEntity var1, MaltegoEntity var2);

    public PartMergeStrategy getLinkMergeStrategy(MaltegoLink var1, MaltegoLink var2);

    public static class PreferNew
    extends Basic {
        public PreferNew() {
            super(PartMergeStrategy.PreferNew, PartMergeStrategy.PreferNew);
        }
    }

    public static class Basic
    implements GraphMergeStrategy {
        private final PartMergeStrategy _entityMergeStrat;
        private final PartMergeStrategy _linkMergeStrat;

        public Basic(PartMergeStrategy partMergeStrategy, PartMergeStrategy partMergeStrategy2) {
            this._entityMergeStrat = partMergeStrategy;
            this._linkMergeStrat = partMergeStrategy2;
        }

        public PartMergeStrategy getEntityMergeStrategy(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
            return this._entityMergeStrat;
        }

        public PartMergeStrategy getLinkMergeStrategy(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
            return this._linkMergeStrat;
        }
    }

}

