/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DefaultGraphModelViewMappings
implements GraphModelViewMappings {
    private final GraphStructureReader _modelStructureReader;

    public DefaultGraphModelViewMappings(GraphStructureReader graphStructureReader) {
        this._modelStructureReader = graphStructureReader;
    }

    protected GraphStructureReader getModelStructureReader() {
        return this._modelStructureReader;
    }

    @Override
    public Map<EntityID, Set<EntityID>> getModelEntitiesMap(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getModelEntities(entityID));
        }
        return hashMap;
    }

    @Override
    public Set<EntityID> getModelEntities(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : collection) {
            hashSet.addAll(this.getModelEntities(entityID));
        }
        return hashSet;
    }

    @Override
    public Set<EntityID> getModelEntities(EntityID entityID) throws GraphStoreException {
        return Collections.singleton(entityID);
    }

    @Override
    public Map<LinkID, Set<LinkID>> getModelLinksMap(Collection<LinkID> collection) throws GraphStoreException {
        HashMap<LinkID, Set<LinkID>> hashMap = new HashMap<LinkID, Set<LinkID>>();
        for (LinkID linkID : collection) {
            hashMap.put(linkID, this.getModelLinks(linkID));
        }
        return hashMap;
    }

    @Override
    public Set<LinkID> getModelLinks(Collection<LinkID> collection) throws GraphStoreException {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (LinkID linkID : collection) {
            hashSet.addAll(this.getModelLinks(linkID));
        }
        return hashSet;
    }

    @Override
    public Set<LinkID> getModelLinks(LinkID linkID) throws GraphStoreException {
        return Collections.singleton(linkID);
    }

    @Override
    public EntityID getViewEntity(EntityID entityID) throws GraphStoreException {
        return entityID;
    }

    @Override
    public LinkID getViewLink(LinkID linkID) throws GraphStoreException {
        return linkID;
    }

    @Override
    public boolean isModelAndViewEntity(EntityID entityID) throws GraphStoreException {
        return this._modelStructureReader.exists(entityID);
    }

    @Override
    public boolean isModelEntity(EntityID entityID) throws GraphStoreException {
        return this._modelStructureReader.exists(entityID);
    }

    @Override
    public boolean isOnlyViewEntity(EntityID entityID) throws GraphStoreException {
        return false;
    }

    @Override
    public boolean isOnlyModelEntity(EntityID entityID) throws GraphStoreException {
        return false;
    }

    @Override
    public boolean isViewEntity(EntityID entityID) throws GraphStoreException {
        return this._modelStructureReader.exists(entityID);
    }

    @Override
    public boolean isModelAndViewLink(LinkID linkID) throws GraphStoreException {
        return this._modelStructureReader.exists(linkID);
    }

    @Override
    public boolean isModelLink(LinkID linkID) throws GraphStoreException {
        return this._modelStructureReader.exists(linkID);
    }

    @Override
    public boolean isViewLink(LinkID linkID) throws GraphStoreException {
        return this._modelStructureReader.exists(linkID);
    }

    @Override
    public boolean isOnlyViewLink(LinkID linkID) throws GraphStoreException {
        return false;
    }

    @Override
    public boolean isOnlyModelLink(LinkID linkID) throws GraphStoreException {
        return false;
    }
}

