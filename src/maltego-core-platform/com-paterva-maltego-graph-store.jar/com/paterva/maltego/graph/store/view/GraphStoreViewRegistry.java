/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.view.DefaultGraphStoreViewRegistry;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import org.openide.util.Lookup;

public abstract class GraphStoreViewRegistry {
    public static final String DEFAULT_VIEW = "Main";
    private static GraphStoreViewRegistry _default;

    public static synchronized GraphStoreViewRegistry getDefault() {
        if (_default == null && (GraphStoreViewRegistry._default = (GraphStoreViewRegistry)Lookup.getDefault().lookup(GraphStoreViewRegistry.class)) == null) {
            _default = new DefaultGraphStoreViewRegistry();
        }
        return _default;
    }

    public abstract void register(String var1, GraphID var2, GraphStoreView var3);

    public abstract GraphStoreView getDefaultView(GraphID var1);

    public abstract GraphStoreView getView(String var1, GraphID var2);
}

