/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.copy;

import com.paterva.maltego.graph.store.copy.GraphCopyContext;
import com.paterva.maltego.graph.store.data.GraphStoreException;

public interface GraphCopier {
    public void copy(GraphCopyContext var1) throws GraphStoreException;
}

