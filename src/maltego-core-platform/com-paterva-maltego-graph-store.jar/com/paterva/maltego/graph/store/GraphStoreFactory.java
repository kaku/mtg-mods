/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import org.openide.util.Lookup;

public abstract class GraphStoreFactory {
    private static GraphStoreFactory _instance;

    public static synchronized GraphStoreFactory getDefault() {
        if (_instance == null && (GraphStoreFactory._instance = (GraphStoreFactory)Lookup.getDefault().lookup(GraphStoreFactory.class)) == null) {
            throw new IllegalStateException("Graph Store Factory instance not found.");
        }
        return _instance;
    }

    public abstract GraphStore create(GraphID var1, boolean var2) throws GraphStoreException;
}

