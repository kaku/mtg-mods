/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.store.copy;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.copy.GraphCopier;
import com.paterva.maltego.graph.store.copy.GraphCopyContext;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DefaultGraphCopier
implements GraphCopier {
    @Override
    public void copy(GraphCopyContext graphCopyContext) throws GraphStoreException {
        this.preCopy(graphCopyContext);
        this.doCopy(graphCopyContext);
        this.postCopy(graphCopyContext);
    }

    protected void preCopy(GraphCopyContext graphCopyContext) {
    }

    protected void postCopy(GraphCopyContext graphCopyContext) {
    }

    public void doCopy(GraphCopyContext graphCopyContext) throws GraphStoreException {
        this.copyLayout(graphCopyContext);
        this.copyStructure(graphCopyContext);
        this.copyData(graphCopyContext);
    }

    private void copyLayout(GraphCopyContext graphCopyContext) throws GraphStoreException {
        Set<EntityID> set = graphCopyContext.getEntities();
        Set<LinkID> set2 = graphCopyContext.getLinks();
        GraphLayoutReader graphLayoutReader = graphCopyContext.getSrcLayoutReader();
        Map<EntityID, Point> map = graphLayoutReader.getCenters(set);
        Map<LinkID, List<Point>> map2 = graphLayoutReader.getPaths(set2);
        GraphLayoutWriter graphLayoutWriter = graphCopyContext.getDestLayoutWriter();
        graphLayoutWriter.addEntities(map);
        graphLayoutWriter.addLinks(map2);
    }

    private void copyStructure(GraphCopyContext graphCopyContext) throws GraphStoreException {
        Set<EntityID> set = graphCopyContext.getEntities();
        Set<LinkID> set2 = graphCopyContext.getLinks();
        GraphStructureReader graphStructureReader = graphCopyContext.getSrcStructureReader();
        Map<LinkID, LinkEntityIDs> map = graphStructureReader.getEntities(set2);
        Map<EntityID, Boolean> map2 = graphStructureReader.getPinned(set);
        GraphStructureWriter graphStructureWriter = graphCopyContext.getDestStructureWriter();
        graphStructureWriter.beginUpdate();
        graphStructureWriter.add(set);
        graphStructureWriter.connect(map);
        graphStructureWriter.setPinned(map2);
        graphStructureWriter.endUpdate(null);
    }

    private void copyData(GraphCopyContext graphCopyContext) throws GraphStoreException {
        GraphDataStoreWriter graphDataStoreWriter = graphCopyContext.getDestDataWriter();
        graphDataStoreWriter.beginUpdate();
        this.copyEntityData(graphCopyContext);
        this.copyLinkData(graphCopyContext);
        graphDataStoreWriter.endUpdate();
    }

    private void copyEntityData(GraphCopyContext graphCopyContext) throws GraphStoreException {
        Set<EntityID> set = graphCopyContext.getEntities();
        EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
        entitiesDataQuery.setIDs(set);
        GraphDataStoreReader graphDataStoreReader = graphCopyContext.getSrcDataReader();
        Map<EntityID, MaltegoEntity> map = graphDataStoreReader.getEntities(entitiesDataQuery);
        GraphDataStoreWriter graphDataStoreWriter = graphCopyContext.getDestDataWriter();
        graphDataStoreWriter.putEntities(map.values());
    }

    private void copyLinkData(GraphCopyContext graphCopyContext) throws GraphStoreException {
        Set<LinkID> set = graphCopyContext.getLinks();
        LinksDataQuery linksDataQuery = new LinksDataQuery();
        linksDataQuery.setIDs(set);
        GraphDataStoreReader graphDataStoreReader = graphCopyContext.getSrcDataReader();
        Map<LinkID, MaltegoLink> map = graphDataStoreReader.getLinks(linksDataQuery);
        GraphDataStoreWriter graphDataStoreWriter = graphCopyContext.getDestDataWriter();
        graphDataStoreWriter.putLinks(map.values());
    }
}

