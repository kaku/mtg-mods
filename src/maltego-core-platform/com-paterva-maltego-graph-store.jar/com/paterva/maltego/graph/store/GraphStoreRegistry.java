/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import org.openide.util.Lookup;

public abstract class GraphStoreRegistry {
    private static GraphStoreRegistry _instance;

    public static synchronized GraphStoreRegistry getDefault() {
        if (_instance == null && (GraphStoreRegistry._instance = (GraphStoreRegistry)Lookup.getDefault().lookup(GraphStoreRegistry.class)) == null) {
            throw new IllegalStateException("Graph Store Registry instance not found.");
        }
        return _instance;
    }

    public abstract void register(GraphID var1, GraphStore var2) throws GraphStoreException;

    public abstract GraphStore forGraphID(GraphID var1) throws GraphStoreException;

    public abstract boolean isExistingAndOpen(GraphID var1) throws GraphStoreException;

    public abstract void closeAll();
}

