/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.graph.store.BatchUpdatable;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractBatchUpdatable<T>
implements BatchUpdatable<T> {
    private int _updateCounter = 0;
    private final Logger LOG;

    public AbstractBatchUpdatable() {
        this.LOG = Logger.getLogger(this.getClass().getName());
    }

    protected abstract void fireEvent();

    @Override
    public void beginUpdate() {
        ++this._updateCounter;
        this.LOG.log(Level.FINER, "Update counter {0}", this._updateCounter);
    }

    @Override
    public void endUpdate() {
        --this._updateCounter;
        this.LOG.log(Level.FINER, "Update counter {0}", this._updateCounter);
        this.checkFireEvent();
    }

    @Override
    public void endUpdate(T t) {
        --this._updateCounter;
        this.LOG.log(Level.FINER, "Update counter {0}", this._updateCounter);
        this.checkFireEvent();
    }

    private void checkFireEvent() {
        if (this._updateCounter < 0) {
            throw new IllegalStateException("More endUpdate()s called than beginUpdate()s");
        }
        if (this._updateCounter == 0) {
            this.fireEvent();
        }
    }
}

