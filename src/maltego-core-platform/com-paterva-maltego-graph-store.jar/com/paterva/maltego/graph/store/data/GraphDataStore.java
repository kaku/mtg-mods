/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.data;

import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import java.beans.PropertyChangeListener;

public interface GraphDataStore {
    public static final String PROP_PARTS_CHANGED = "partsChanged";

    public GraphDataStoreReader getDataStoreReader();

    public GraphDataStoreWriter getDataStoreWriter();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

