/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.data.sort;

public class SortField {
    private final int _field;
    private final boolean _ascending;

    public SortField(int n, boolean bl) {
        this._field = n;
        this._ascending = bl;
    }

    public int getField() {
        return this._field;
    }

    public boolean isAscending() {
        return this._ascending;
    }

    public String toString() {
        return "SortField{field=" + this._field + ", ascending=" + this._ascending + '}';
    }

    public int hashCode() {
        int n = 7;
        n = 89 * n + this._field;
        n = 89 * n + (this._ascending ? 1 : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        SortField sortField = (SortField)object;
        if (this._field != sortField._field) {
            return false;
        }
        return this._ascending == sortField._ascending;
    }
}

