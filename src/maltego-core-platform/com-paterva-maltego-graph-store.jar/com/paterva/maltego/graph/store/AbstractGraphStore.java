/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.graph.store.AbstractBatchUpdatable;
import com.paterva.maltego.graph.store.GraphMods;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractGraphStore
extends AbstractBatchUpdatable<Boolean>
implements GraphStore {
    private static final Logger LOG = Logger.getLogger(AbstractGraphStore.class.getName());
    private final boolean _batchEvents;
    private final PropertyChangeSupport _changeSupport;
    private PropertyChangeListener _subStoreListener;
    private GraphMods _eventMods;
    private boolean _open;

    public AbstractGraphStore(boolean bl) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._open = false;
        this._batchEvents = bl;
    }

    @Override
    public boolean isOpen() {
        return this._open;
    }

    @Override
    public void open() throws GraphStoreException {
        if (this._batchEvents) {
            this._subStoreListener = new SubStoreListener();
            this.getGraphDataStore().addPropertyChangeListener(this._subStoreListener);
            this.getGraphStructureStore().addPropertyChangeListener(this._subStoreListener);
            this.getGraphLayoutStore().addPropertyChangeListener(this._subStoreListener);
        }
        this._open = true;
    }

    @Override
    public void close(boolean bl) throws GraphStoreException {
        this._open = false;
        if (this._batchEvents) {
            this.getGraphDataStore().removePropertyChangeListener(this._subStoreListener);
            this.getGraphStructureStore().removePropertyChangeListener(this._subStoreListener);
            this.getGraphLayoutStore().removePropertyChangeListener(this._subStoreListener);
            this._subStoreListener = null;
        }
    }

    @Override
    public void beginUpdate() {
        this.checkIsOpen();
        AbstractBatchUpdatable.super.beginUpdate();
        this.getGraphDataStore().getDataStoreWriter().beginUpdate();
        this.getGraphStructureStore().getStructureWriter().beginUpdate();
        this.getGraphLayoutStore().getLayoutWriter().beginUpdate();
    }

    @Override
    public void endUpdate() {
        this.endUpdate(null);
    }

    @Override
    public void endUpdate(Boolean bl) {
        this.checkIsOpen();
        this.getGraphDataStore().getDataStoreWriter().endUpdate();
        this.getGraphStructureStore().getStructureWriter().endUpdate(bl);
        this.getGraphLayoutStore().getLayoutWriter().endUpdate();
        AbstractBatchUpdatable.super.endUpdate(bl);
    }

    @Override
    protected void fireEvent() {
        this.checkIsOpen();
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphMods graphMods = this._eventMods;
            this._eventMods = null;
            LOG.log(Level.FINE, "Fire events: {0}", graphMods);
            graphMods.mergeMods();
            this._changeSupport.firePropertyChange("graphModified", null, graphMods);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private GraphMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphMods();
        }
        return this._eventMods;
    }

    private void checkIsOpen() throws IllegalStateException {
        if (!this.isOpen()) {
            throw new IllegalStateException("Graph store is not open");
        }
    }

    private class SubStoreListener
    implements PropertyChangeListener {
        private SubStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            AbstractGraphStore.this.beginUpdate();
            switch (propertyChangeEvent.getPropertyName()) {
                case "partsChanged": {
                    AbstractGraphStore.this.getEventMods().setDataMods((GraphDataMods)propertyChangeEvent.getNewValue());
                    break;
                }
                case "structureModified": {
                    AbstractGraphStore.this.getEventMods().setStructureMods((GraphStructureMods)propertyChangeEvent.getNewValue());
                    break;
                }
                case "layoutModified": {
                    AbstractGraphStore.this.getEventMods().setLayoutMods((GraphLayoutMods)propertyChangeEvent.getNewValue());
                    break;
                }
            }
            AbstractGraphStore.this.endUpdate();
        }
    }

}

