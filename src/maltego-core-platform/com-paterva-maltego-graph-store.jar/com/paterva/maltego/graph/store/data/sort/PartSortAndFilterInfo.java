/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.data.sort;

import com.paterva.maltego.graph.store.data.sort.SortField;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class PartSortAndFilterInfo {
    public static final PartSortAndFilterInfo DEFAULT = new PartSortAndFilterInfo(Collections.EMPTY_LIST, null);
    private final List<SortField> _fields;
    private final String _filterText;

    public PartSortAndFilterInfo(List<SortField> list, String string) {
        this._fields = list;
        this._filterText = string;
    }

    public List<SortField> getFields() {
        return this._fields;
    }

    public String getFilterText() {
        return this._filterText;
    }

    public String toString() {
        return "PartSortAndFilterInfo{" + this._filterText + "," + this._fields + '}';
    }

    public int hashCode() {
        int n = 3;
        n = 67 * n + Objects.hashCode(this._fields);
        n = 67 * n + Objects.hashCode(this._filterText);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        PartSortAndFilterInfo partSortAndFilterInfo = (PartSortAndFilterInfo)object;
        if (!Objects.equals(this._filterText, partSortAndFilterInfo._filterText)) {
            return false;
        }
        return Objects.equals(this._fields, partSortAndFilterInfo._fields);
    }
}

