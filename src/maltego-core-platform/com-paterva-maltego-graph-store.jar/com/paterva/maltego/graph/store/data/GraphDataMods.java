/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.store.data;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GraphDataMods {
    private Map<EntityID, MaltegoEntity> _entitiesUpdated = new HashMap<EntityID, MaltegoEntity>();
    private Map<LinkID, MaltegoLink> _linksUpdated = new HashMap<LinkID, MaltegoLink>();

    public static GraphDataMods weakCopy(GraphDataMods graphDataMods) {
        GraphDataMods graphDataMods2 = new GraphDataMods();
        graphDataMods2._entitiesUpdated.putAll(graphDataMods._entitiesUpdated);
        graphDataMods2._linksUpdated.putAll(graphDataMods._linksUpdated);
        return graphDataMods2;
    }

    public boolean isEmpty() {
        return this._entitiesUpdated.isEmpty() && this._linksUpdated.isEmpty();
    }

    public Map<EntityID, MaltegoEntity> getEntitiesUpdated() {
        return this._entitiesUpdated;
    }

    public Map<LinkID, MaltegoLink> getLinksUpdated() {
        return this._linksUpdated;
    }

    public void makeReadOnly() {
        this._entitiesUpdated = Collections.unmodifiableMap(this._entitiesUpdated);
        this._linksUpdated = Collections.unmodifiableMap(this._linksUpdated);
    }

    public String toString() {
        return "GraphDataMods{entitiesUpdated=" + this._entitiesUpdated + ", linksUpdated=" + this._linksUpdated + '}';
    }
}

