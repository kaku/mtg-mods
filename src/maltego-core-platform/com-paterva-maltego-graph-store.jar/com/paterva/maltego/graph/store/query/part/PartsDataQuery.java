/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 */
package com.paterva.maltego.graph.store.query.part;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import java.util.Collections;
import java.util.Set;

public class PartsDataQuery<T extends PartSectionsQuery, U extends PartDataQuery<T>, ID extends Guid> {
    private boolean _allIDs = true;
    private Set<ID> _ids;
    private U _partDataQuery;

    public boolean isAllIDs() {
        return this._allIDs;
    }

    public void setAllIDs(boolean bl) {
        this._allIDs = bl;
    }

    public Set<ID> getIDs() {
        return this._ids == null ? this._ids : Collections.unmodifiableSet(this._ids);
    }

    public void setIDs(Set<ID> set) {
        this._allIDs = false;
        this._ids = set;
    }

    public U getPartDataQuery() {
        return this._partDataQuery;
    }

    public void setPartDataQuery(U u) {
        this._partDataQuery = u;
    }
}

