/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import java.awt.Point;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphMods {
    private GraphDataMods _dataMods;
    private GraphStructureMods _structureMods;
    private GraphLayoutMods _layoutMods;

    public GraphDataMods getDataMods() {
        return this._dataMods;
    }

    public void setDataMods(GraphDataMods graphDataMods) {
        if (this._dataMods != null) {
            throw new IllegalStateException("Data mods already set");
        }
        this._dataMods = graphDataMods;
    }

    public GraphStructureMods getStructureMods() {
        return this._structureMods;
    }

    public void setStructureMods(GraphStructureMods graphStructureMods) {
        if (this._structureMods != null) {
            throw new IllegalStateException("Structure mods already set");
        }
        this._structureMods = graphStructureMods;
    }

    public GraphLayoutMods getLayoutMods() {
        return this._layoutMods;
    }

    public void setLayoutMods(GraphLayoutMods graphLayoutMods) {
        if (this._layoutMods != null) {
            throw new IllegalStateException("Layout mods already set");
        }
        this._layoutMods = graphLayoutMods;
    }

    public boolean isEmpty() {
        return !(this._dataMods != null && !this._dataMods.isEmpty() || this._structureMods != null && !this._structureMods.isEmpty() || this._layoutMods != null && !this._layoutMods.isEmpty());
    }

    public void mergeMods() {
        if (this._structureMods != null) {
            Set<EntityID> set = this._structureMods.getEntitiesRemoved();
            Map<LinkID, LinkEntityIDs> map = this._structureMods.getLinksRemoved();
            if (!set.isEmpty() || !map.isEmpty()) {
                if (this._dataMods != null && !this._dataMods.isEmpty()) {
                    this._dataMods = this.removeRemovedParts(this._dataMods, set, map);
                }
                if (this._layoutMods != null && !this._layoutMods.isEmpty()) {
                    this._layoutMods = this.removeRemovedParts(this._layoutMods, set, map);
                }
            }
        }
    }

    private GraphDataMods removeRemovedParts(GraphDataMods graphDataMods, Set<EntityID> set, Map<LinkID, LinkEntityIDs> map) {
        MaltegoEntity maltegoEntity;
        GraphDataMods graphDataMods2 = new GraphDataMods();
        Map<EntityID, MaltegoEntity> map2 = graphDataMods2.getEntitiesUpdated();
        for (Map.Entry<EntityID, MaltegoEntity> object : graphDataMods.getEntitiesUpdated().entrySet()) {
            EntityID entityID = object.getKey();
            maltegoEntity = object.getValue();
            if (set.contains((Object)entityID)) continue;
            map2.put(entityID, maltegoEntity);
        }
        Map<LinkID, MaltegoLink> map3 = graphDataMods2.getLinksUpdated();
        for (Map.Entry<LinkID, MaltegoLink> entry : graphDataMods.getLinksUpdated().entrySet()) {
            maltegoEntity = entry.getKey();
            MaltegoLink maltegoLink = entry.getValue();
            if (map.containsKey((Object)maltegoEntity)) continue;
            map3.put((LinkID)maltegoEntity, maltegoLink);
        }
        graphDataMods2.makeReadOnly();
        return graphDataMods2;
    }

    private GraphLayoutMods removeRemovedParts(GraphLayoutMods graphLayoutMods, Set<EntityID> set, Map<LinkID, LinkEntityIDs> map) {
        Point point;
        GraphLayoutMods graphLayoutMods2 = new GraphLayoutMods();
        Map<EntityID, Point> map2 = graphLayoutMods2.getCenters();
        for (Map.Entry<EntityID, Point> object : graphLayoutMods.getCenters().entrySet()) {
            EntityID entityID = object.getKey();
            point = object.getValue();
            if (set.contains((Object)entityID)) continue;
            map2.put(entityID, point);
        }
        Map<LinkID, List<Point>> map3 = graphLayoutMods2.getPaths();
        for (Map.Entry<LinkID, List<Point>> entry : graphLayoutMods.getPaths().entrySet()) {
            point = entry.getKey();
            List<Point> list = entry.getValue();
            if (map.containsKey(point)) continue;
            map3.put((LinkID)point, list);
        }
        graphLayoutMods2.makeReadOnly();
        return graphLayoutMods2;
    }

    public String toString() {
        return "GraphMods{\n   data=" + this._dataMods + ",\n   structure=" + this._structureMods + ",\n   layout=" + this._layoutMods + "\n}";
    }
}

