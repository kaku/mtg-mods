/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.store.data;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.BatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import java.util.Collection;
import java.util.Map;

public interface GraphDataStoreWriter
extends BatchUpdatable<Boolean> {
    public void putEntities(Collection<MaltegoEntity> var1) throws GraphStoreException;

    public void putLinks(Collection<MaltegoLink> var1) throws GraphStoreException;

    public void putEntity(MaltegoEntity var1) throws GraphStoreException;

    public void putLink(MaltegoLink var1) throws GraphStoreException;

    public void removeEntities(Collection<EntityID> var1) throws GraphStoreException;

    public void removeEntity(EntityID var1) throws GraphStoreException;

    public void removeLinks(Collection<LinkID> var1) throws GraphStoreException;

    public void removeLink(LinkID var1) throws GraphStoreException;

    public void updateEntities(Collection<MaltegoEntity> var1) throws GraphStoreException;

    public void updateEntity(MaltegoEntity var1) throws GraphStoreException;

    public void updateLinks(Collection<MaltegoLink> var1) throws GraphStoreException;

    public void updateLink(MaltegoLink var1) throws GraphStoreException;

    public void convertAttachmentIDsToPaths(Map<Integer, String> var1) throws GraphStoreException;

    public void convertAttachmentPathsToIDs(Map<Integer, String> var1) throws GraphStoreException;
}

