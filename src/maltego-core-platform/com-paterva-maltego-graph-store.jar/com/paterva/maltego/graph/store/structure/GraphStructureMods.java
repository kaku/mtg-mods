/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.structure;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.structure.CollectionMods;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GraphStructureMods {
    private Set<EntityID> _entitiesAdded = new HashSet<EntityID>();
    private Set<EntityID> _entitiesRemoved = new HashSet<EntityID>();
    private Map<EntityID, CollectionMods> _collectionMods = new HashMap<EntityID, CollectionMods>();
    private Set<EntityID> _entitiesPinned = new HashSet<EntityID>();
    private Set<EntityID> _entitiesUnpinned = new HashSet<EntityID>();
    private Set<LinkID> _linksAdded = new HashSet<LinkID>();
    private Map<LinkID, LinkEntityIDs> _linksRemoved = new HashMap<LinkID, LinkEntityIDs>();
    private boolean _layoutNew = false;

    public static GraphStructureMods weakCopy(GraphStructureMods graphStructureMods) {
        GraphStructureMods graphStructureMods2 = new GraphStructureMods();
        graphStructureMods2._entitiesAdded.addAll(graphStructureMods._entitiesAdded);
        graphStructureMods2._entitiesRemoved.addAll(graphStructureMods._entitiesRemoved);
        graphStructureMods2._collectionMods.putAll(graphStructureMods._collectionMods);
        graphStructureMods2._entitiesPinned.addAll(graphStructureMods._entitiesPinned);
        graphStructureMods2._entitiesUnpinned.addAll(graphStructureMods._entitiesUnpinned);
        graphStructureMods2._linksAdded.addAll(graphStructureMods._linksAdded);
        graphStructureMods2._linksRemoved.putAll(graphStructureMods._linksRemoved);
        graphStructureMods2._layoutNew = graphStructureMods._layoutNew;
        return graphStructureMods2;
    }

    public static synchronized GraphStructureMods create(Set<EntityID> set, Set<EntityID> set2, Map<LinkID, LinkEntityIDs> map, Map<LinkID, LinkEntityIDs> map2) {
        GraphStructureMods graphStructureMods = new GraphStructureMods();
        Set<EntityID> set3 = graphStructureMods.getEntitiesAdded();
        Set<EntityID> set4 = graphStructureMods.getEntitiesRemoved();
        Set<LinkID> set5 = graphStructureMods.getLinksAdded();
        Map<LinkID, LinkEntityIDs> map3 = graphStructureMods.getLinksRemoved();
        set3.addAll(set2);
        set3.removeAll(set);
        set4.addAll(set);
        set4.removeAll(set2);
        set5.addAll(map2.keySet());
        set5.removeAll(map.keySet());
        map3.putAll(map);
        map3.keySet().removeAll(map2.keySet());
        return graphStructureMods;
    }

    public boolean isEmpty() {
        return this._entitiesAdded.isEmpty() && this._entitiesRemoved.isEmpty() && this._collectionMods.isEmpty() && this._linksAdded.isEmpty() && this._linksRemoved.isEmpty() && this._entitiesPinned.isEmpty() && this._entitiesUnpinned.isEmpty();
    }

    public Set<EntityID> getEntitiesAdded() {
        return this._entitiesAdded;
    }

    public Set<EntityID> getEntitiesRemoved() {
        return this._entitiesRemoved;
    }

    public Map<EntityID, CollectionMods> getCollectionMods() {
        return this._collectionMods;
    }

    public Set<EntityID> getEntitiesPinned() {
        return this._entitiesPinned;
    }

    public Set<EntityID> getEntitiesUnpinned() {
        return this._entitiesUnpinned;
    }

    public Set<LinkID> getLinksAdded() {
        return this._linksAdded;
    }

    public Map<LinkID, LinkEntityIDs> getLinksRemoved() {
        return this._linksRemoved;
    }

    public boolean isLayoutNew() {
        return this._layoutNew;
    }

    public void setLayoutNew(boolean bl) {
        this._layoutNew = bl;
    }

    public void makeReadOnly() {
        this._entitiesAdded = Collections.unmodifiableSet(this._entitiesAdded);
        this._entitiesRemoved = Collections.unmodifiableSet(this._entitiesRemoved);
        this._collectionMods = Collections.unmodifiableMap(this._collectionMods);
        this._entitiesPinned = Collections.unmodifiableSet(this._entitiesPinned);
        this._entitiesUnpinned = Collections.unmodifiableSet(this._entitiesUnpinned);
        this._linksAdded = Collections.unmodifiableSet(this._linksAdded);
        this._linksRemoved = Collections.unmodifiableMap(this._linksRemoved);
    }

    public String toString() {
        return "GraphStructureMods{_entitiesAdded=" + this._entitiesAdded + ", _entitiesRemoved=" + this._entitiesRemoved + ", _collectionMods=" + this._collectionMods + ", _entitiesPinned=" + this._entitiesPinned + ", _entitiesUnpinned=" + this._entitiesUnpinned + ", _linksAdded=" + this._linksAdded + ", _linksRemoved=" + this._linksRemoved + ", _layoutNew=" + this._layoutNew + '}';
    }
}

