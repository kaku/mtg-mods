/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store;

public interface BatchUpdatable<T> {
    public void beginUpdate();

    public void endUpdate();

    public void endUpdate(T var1);
}

