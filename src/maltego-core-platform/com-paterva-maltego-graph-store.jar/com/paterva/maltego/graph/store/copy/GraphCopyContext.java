/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.copy;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class GraphCopyContext {
    private final GraphID _srcGraphID;
    private final GraphID _destGraphID;
    private Set<EntityID> _entities;
    private Set<LinkID> _links;
    private boolean _withLinks;
    private GraphStore _srcGraphStore;
    private GraphStore _destGraphStore;
    private GraphLayoutReader _srcLayoutReader;
    private GraphLayoutWriter _destLayoutWriter;
    private GraphStructureReader _srcStructureReader;
    private GraphStructureWriter _destStructureWriter;
    private GraphDataStoreReader _srcDataReader;
    private GraphDataStoreWriter _destDataWriter;

    public GraphCopyContext(GraphID graphID, GraphID graphID2) {
        this(graphID, graphID2, true);
    }

    public GraphCopyContext(GraphID graphID, GraphID graphID2, boolean bl) {
        this(graphID, graphID2, null, null, bl);
    }

    public GraphCopyContext(GraphID graphID, GraphID graphID2, Collection<EntityID> collection, boolean bl) {
        this(graphID, graphID2, collection, null, bl);
    }

    public GraphCopyContext(GraphID graphID, GraphID graphID2, Collection<EntityID> collection, Collection<LinkID> collection2) {
        this(graphID, graphID2, collection, collection2, false);
    }

    public GraphCopyContext(GraphID graphID, GraphID graphID2, Collection<EntityID> collection, Collection<LinkID> collection2, boolean bl) {
        this._srcGraphID = graphID;
        this._destGraphID = graphID2;
        if (collection != null) {
            this._entities = new HashSet<EntityID>(collection);
        }
        if (collection2 != null) {
            this._links = new HashSet<LinkID>(collection2);
        }
        this._withLinks = bl;
    }

    public GraphID getSrcGraphID() {
        return this._srcGraphID;
    }

    public GraphID getDestGraphID() {
        return this._destGraphID;
    }

    public Set<EntityID> getEntities() throws GraphStoreException {
        if (this._entities == null) {
            this._entities = this.getSrcStructureReader().getEntities();
        }
        return Collections.unmodifiableSet(this._entities);
    }

    public Set<LinkID> getLinks() throws GraphStoreException {
        if (this._links == null) {
            if (!this._withLinks) {
                this._links = Collections.EMPTY_SET;
            } else {
                Set<EntityID> set = this.getEntities();
                GraphStructureReader graphStructureReader = this.getSrcStructureReader();
                this._links = graphStructureReader.getLinksBetween(set);
            }
        }
        return Collections.unmodifiableSet(this._links);
    }

    public GraphStore getSrcGraphStore() throws GraphStoreException {
        if (this._srcGraphStore == null) {
            this._srcGraphStore = GraphStoreRegistry.getDefault().forGraphID(this._srcGraphID);
        }
        return this._srcGraphStore;
    }

    public GraphStore getDestGraphStore() throws GraphStoreException {
        if (this._destGraphStore == null) {
            this._destGraphStore = GraphStoreRegistry.getDefault().forGraphID(this._destGraphID);
        }
        return this._destGraphStore;
    }

    public GraphLayoutReader getSrcLayoutReader() throws GraphStoreException {
        if (this._srcLayoutReader == null) {
            this._srcLayoutReader = this.getSrcGraphStore().getGraphLayoutStore().getLayoutReader();
        }
        return this._srcLayoutReader;
    }

    public GraphLayoutWriter getDestLayoutWriter() throws GraphStoreException {
        if (this._destLayoutWriter == null) {
            this._destLayoutWriter = this.getDestGraphStore().getGraphLayoutStore().getLayoutWriter();
        }
        return this._destLayoutWriter;
    }

    public GraphStructureReader getSrcStructureReader() throws GraphStoreException {
        if (this._srcStructureReader == null) {
            this._srcStructureReader = this.getSrcGraphStore().getGraphStructureStore().getStructureReader();
        }
        return this._srcStructureReader;
    }

    public GraphStructureWriter getDestStructureWriter() throws GraphStoreException {
        if (this._destStructureWriter == null) {
            this._destStructureWriter = this.getDestGraphStore().getGraphStructureStore().getStructureWriter();
        }
        return this._destStructureWriter;
    }

    public GraphDataStoreReader getSrcDataReader() throws GraphStoreException {
        if (this._srcDataReader == null) {
            this._srcDataReader = this.getSrcGraphStore().getGraphDataStore().getDataStoreReader();
        }
        return this._srcDataReader;
    }

    public GraphDataStoreWriter getDestDataWriter() throws GraphStoreException {
        if (this._destDataWriter == null) {
            this._destDataWriter = this.getDestGraphStore().getGraphDataStore().getDataStoreWriter();
        }
        return this._destDataWriter;
    }
}

