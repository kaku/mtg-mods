/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.AbstractGraphStore;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import java.beans.PropertyChangeListener;
import java.io.OutputStream;

public abstract class GraphStoreView
extends AbstractGraphStore {
    private final GraphStore _model;

    public GraphStoreView(GraphID graphID) throws GraphStoreException {
        super(false);
        this._model = GraphStoreRegistry.getDefault().forGraphID(graphID);
    }

    public GraphStore getModel() {
        return this._model;
    }

    public abstract GraphModelViewMappings getModelViewMappings();

    public abstract void writeCache(OutputStream var1) throws GraphStoreException;

    public abstract void syncWithModel();

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

