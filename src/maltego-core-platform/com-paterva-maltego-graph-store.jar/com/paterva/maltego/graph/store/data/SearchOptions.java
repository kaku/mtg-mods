/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.data;

public class SearchOptions {
    private final String _text;
    private final boolean _negate;
    private final String _type;
    private final boolean _searchValue;
    private final boolean _searchProperties;
    private final boolean _searchNotes;
    private final boolean _searchDisplayInfo;

    public SearchOptions(String string, boolean bl, String string2, boolean bl2, boolean bl3, boolean bl4, boolean bl5) {
        this._text = string;
        this._negate = bl;
        this._type = string2;
        this._searchValue = bl2;
        this._searchProperties = bl3;
        this._searchNotes = bl4;
        this._searchDisplayInfo = bl5;
    }

    public String getText() {
        return this._text;
    }

    public boolean isNegate() {
        return this._negate;
    }

    public String getType() {
        return this._type;
    }

    public boolean isSearchValue() {
        return this._searchValue;
    }

    public boolean isSearchProperties() {
        return this._searchProperties;
    }

    public boolean isSearchNotes() {
        return this._searchNotes;
    }

    public boolean isSearchDisplayInfo() {
        return this._searchDisplayInfo;
    }

    public String toString() {
        return "SearchOptions{text=" + this._text + ", negate=" + this._negate + ", type=" + this._type + ", value=" + this._searchValue + ", props=" + this._searchProperties + ", notes=" + this._searchNotes + ", display info=" + this._searchDisplayInfo + '}';
    }
}

