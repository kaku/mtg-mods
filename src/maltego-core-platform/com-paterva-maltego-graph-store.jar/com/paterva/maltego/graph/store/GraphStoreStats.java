/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class GraphStoreStats {
    private static final Logger LOG = Logger.getLogger(GraphStoreStats.class.getName());

    public static void logStats(GraphID graphID) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            stringBuilder.append("Graph:");
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
            GraphStoreStats.write(stringBuilder, "D", graphDataStoreReader.getEntityCount(), graphDataStoreReader.getLinkCount());
            GraphStoreStats.write(stringBuilder, "S", graphStructureReader.getEntityCount(), graphStructureReader.getLinkCount());
            GraphStoreStats.write(stringBuilder, "L", graphLayoutReader.getEntityCount(), graphLayoutReader.getLinkCount());
            LOG.info(stringBuilder.toString());
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    private static void write(StringBuilder stringBuilder, String string, int n, int n2) {
        stringBuilder.append(" ").append(string).append(":");
        stringBuilder.append(" E: ").append(n);
        stringBuilder.append(" L: ").append(n2);
    }
}

