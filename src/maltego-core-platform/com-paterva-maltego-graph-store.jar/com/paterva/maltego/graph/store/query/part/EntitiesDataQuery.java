/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 */
package com.paterva.maltego.graph.store.query.part;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartsDataQuery;

public class EntitiesDataQuery
extends PartsDataQuery<EntitySectionsQuery, EntityDataQuery, EntityID> {
}

