/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultGraphStoreViewRegistry
extends GraphStoreViewRegistry {
    private static final Logger LOG = Logger.getLogger(DefaultGraphStoreViewRegistry.class.getName());

    @Override
    public void register(String string, GraphID graphID, GraphStoreView graphStoreView) {
        LOG.log(Level.FINE, "Registering: {0}->{1}", new Object[]{string, graphID});
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string2 = GraphStoreViewRegistry.class.getName();
        HashMap<String, GraphStoreView> hashMap = (HashMap<String, GraphStoreView>)graphUserData.get((Object)string2);
        if (hashMap != null) {
            if (hashMap.containsKey(string)) {
                throw new IllegalArgumentException("View '" + string + "' already exists for graph " + (Object)graphID);
            }
        } else {
            hashMap = new HashMap<String, GraphStoreView>();
            graphUserData.put((Object)string2, hashMap);
        }
        hashMap.put(string, graphStoreView);
    }

    @Override
    public GraphStoreView getDefaultView(GraphID graphID) {
        return this.getView("Main", graphID);
    }

    @Override
    public GraphStoreView getView(String string, GraphID graphID) {
        String string2;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        Map map = (Map)graphUserData.get((Object)(string2 = GraphStoreViewRegistry.class.getName()));
        if (map == null || !map.containsKey(string)) {
            throw new IllegalArgumentException("View '" + string + "' does not exist for graph " + (Object)graphID + ". Found view '" + map + "'.");
        }
        return (GraphStoreView)map.get(string);
    }
}

