/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.layout;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import java.awt.Point;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphLayoutMods {
    private Map<EntityID, Point> _centers = new HashMap<EntityID, Point>();
    private Map<LinkID, List<Point>> _paths = new HashMap<LinkID, List<Point>>();

    public boolean isEmpty() {
        return this._centers.isEmpty() && this._paths.isEmpty();
    }

    public Map<EntityID, Point> getCenters() {
        return this._centers;
    }

    public Map<LinkID, List<Point>> getPaths() {
        return this._paths;
    }

    public void makeReadOnly() {
        this._centers = Collections.unmodifiableMap(this._centers);
        this._paths = Collections.unmodifiableMap(this._paths);
    }

    public String toString() {
        return "GraphLayoutMods{centers=" + this._centers + ", paths=" + this._paths + '}';
    }
}

