/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.DefaultGraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import java.io.OutputStream;

public class DefaultGraphStoreView
extends GraphStoreView {
    private final GraphModelViewMappings _mappings;

    public DefaultGraphStoreView(GraphID graphID) throws GraphStoreException {
        super(graphID);
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        this._mappings = new DefaultGraphModelViewMappings(graphStore.getGraphStructureStore().getStructureReader());
    }

    @Override
    public boolean isInMemory() {
        return this.getModel().isInMemory();
    }

    @Override
    public GraphDataStore getGraphDataStore() {
        return this.getModel().getGraphDataStore();
    }

    @Override
    public GraphStructureStore getGraphStructureStore() {
        return this.getModel().getGraphStructureStore();
    }

    @Override
    public GraphLayoutStore getGraphLayoutStore() {
        return this.getModel().getGraphLayoutStore();
    }

    @Override
    public GraphModelViewMappings getModelViewMappings() {
        return this._mappings;
    }

    @Override
    public void syncWithModel() {
    }

    @Override
    public void writeCache(OutputStream outputStream) throws GraphStoreException {
    }
}

