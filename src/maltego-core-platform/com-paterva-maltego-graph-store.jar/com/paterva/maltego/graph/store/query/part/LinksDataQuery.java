/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.query.part;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinkSectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartsDataQuery;

public class LinksDataQuery
extends PartsDataQuery<LinkSectionsQuery, LinkDataQuery, LinkID> {
}

