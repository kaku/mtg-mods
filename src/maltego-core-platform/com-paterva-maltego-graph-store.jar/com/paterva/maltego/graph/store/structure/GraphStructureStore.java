/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.structure;

import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.beans.PropertyChangeListener;

public interface GraphStructureStore {
    public static final String PROP_STRUCTURE_MODIFIED = "structureModified";

    public GraphStructureReader getStructureReader();

    public GraphStructureWriter getStructureWriter();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

