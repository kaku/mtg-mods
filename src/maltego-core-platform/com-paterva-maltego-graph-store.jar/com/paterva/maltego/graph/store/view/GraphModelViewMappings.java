/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface GraphModelViewMappings {
    public Map<EntityID, Set<EntityID>> getModelEntitiesMap(Collection<EntityID> var1) throws GraphStoreException;

    public Set<EntityID> getModelEntities(Collection<EntityID> var1) throws GraphStoreException;

    public Set<EntityID> getModelEntities(EntityID var1) throws GraphStoreException;

    public Map<LinkID, Set<LinkID>> getModelLinksMap(Collection<LinkID> var1) throws GraphStoreException;

    public Set<LinkID> getModelLinks(Collection<LinkID> var1) throws GraphStoreException;

    public Set<LinkID> getModelLinks(LinkID var1) throws GraphStoreException;

    public EntityID getViewEntity(EntityID var1) throws GraphStoreException;

    public LinkID getViewLink(LinkID var1) throws GraphStoreException;

    public boolean isModelAndViewEntity(EntityID var1) throws GraphStoreException;

    public boolean isModelEntity(EntityID var1) throws GraphStoreException;

    public boolean isViewEntity(EntityID var1) throws GraphStoreException;

    public boolean isOnlyViewEntity(EntityID var1) throws GraphStoreException;

    public boolean isOnlyModelEntity(EntityID var1) throws GraphStoreException;

    public boolean isModelAndViewLink(LinkID var1) throws GraphStoreException;

    public boolean isModelLink(LinkID var1) throws GraphStoreException;

    public boolean isViewLink(LinkID var1) throws GraphStoreException;

    public boolean isOnlyViewLink(LinkID var1) throws GraphStoreException;

    public boolean isOnlyModelLink(LinkID var1) throws GraphStoreException;
}

