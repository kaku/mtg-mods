/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.data;

import java.io.IOException;

public class GraphStoreException
extends IOException {
    public GraphStoreException(String string) {
        super(string);
    }

    public GraphStoreException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public GraphStoreException(Throwable throwable) {
        super(throwable);
    }
}

