/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.layout;

import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import java.beans.PropertyChangeListener;

public interface GraphLayoutStore {
    public static final String PROP_LAYOUT_MODIFIED = "layoutModified";

    public GraphLayoutReader getLayoutReader();

    public GraphLayoutWriter getLayoutWriter();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

