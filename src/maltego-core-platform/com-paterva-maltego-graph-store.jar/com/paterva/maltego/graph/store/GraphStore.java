/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.graph.store.BatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.beans.PropertyChangeListener;

public interface GraphStore
extends BatchUpdatable<Boolean> {
    public static final String PROP_GRAPH_MODIFIED = "graphModified";

    public boolean isInMemory();

    public boolean isOpen();

    public void open() throws GraphStoreException;

    public void close(boolean var1) throws GraphStoreException;

    public GraphDataStore getGraphDataStore();

    public GraphStructureStore getGraphStructureStore();

    public GraphLayoutStore getGraphLayoutStore();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

