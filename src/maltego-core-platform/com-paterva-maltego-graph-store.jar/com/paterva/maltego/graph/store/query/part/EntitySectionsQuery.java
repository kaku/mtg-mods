/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.query.part;

import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;

public class EntitySectionsQuery
extends PartSectionsQuery {
    private boolean _queryWeight = false;
    private boolean _queryImagePropertyName = false;

    public boolean isQueryWeight() {
        return this._queryWeight;
    }

    public void setQueryWeight(boolean bl) {
        this._queryWeight = bl;
    }

    public boolean isQueryImagePropertyName() {
        return this._queryImagePropertyName;
    }

    public void setQueryImagePropertyName(boolean bl) {
        this._queryImagePropertyName = bl;
    }
}

