/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.query.part;

import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import java.util.Map;
import java.util.Set;

public class PartDataQuery<T extends PartSectionsQuery> {
    private boolean _allSections = false;
    private T _sections;
    private boolean _allProperties = false;
    private Set<String> _propertyNames;
    private Map<String, Object> _matchProperties;

    public boolean isAllSections() {
        return this._allSections;
    }

    public void setAllSections(boolean bl) {
        this._allSections = bl;
    }

    public T getSections() {
        return this._sections;
    }

    public void setSections(T t) {
        this._sections = t;
    }

    public boolean isAllProperties() {
        return this._allProperties;
    }

    public void setAllProperties(boolean bl) {
        this._allProperties = bl;
    }

    public Set<String> getPropertyNames() {
        return this._propertyNames;
    }

    public void setPropertyNames(Set<String> set) {
        this._propertyNames = set;
    }

    public Map<String, Object> getMatchProperties() {
        return this._matchProperties;
    }

    public void setMatchProperties(Map<String, Object> map) {
        this._matchProperties = map;
    }
}

