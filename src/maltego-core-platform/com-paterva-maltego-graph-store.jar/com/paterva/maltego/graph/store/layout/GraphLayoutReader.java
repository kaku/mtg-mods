/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.layout;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface GraphLayoutReader {
    public int getEntityCount() throws GraphStoreException;

    public int getLinkCount() throws GraphStoreException;

    public boolean exists(EntityID var1) throws GraphStoreException;

    public boolean exists(LinkID var1) throws GraphStoreException;

    public Map<EntityID, Point> getAllCenters() throws GraphStoreException;

    public Map<LinkID, List<Point>> getAllPaths() throws GraphStoreException;

    public Map<EntityID, Point> getCenters(Collection<EntityID> var1) throws GraphStoreException;

    public Point getCenter(EntityID var1) throws GraphStoreException;

    public Map<LinkID, List<Point>> getPaths(Collection<LinkID> var1) throws GraphStoreException;

    public List<Point> getPath(LinkID var1) throws GraphStoreException;
}

