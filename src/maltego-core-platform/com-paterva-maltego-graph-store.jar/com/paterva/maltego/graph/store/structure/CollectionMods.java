/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.structure;

public class CollectionMods {
    private final int _entitiesAdded;
    private final int _entitiesRemoved;
    private final int _entityCount;

    public CollectionMods(int n, int n2, int n3) {
        this._entitiesAdded = n;
        this._entitiesRemoved = n2;
        this._entityCount = n3;
    }

    public int getEntitiesAdded() {
        return this._entitiesAdded;
    }

    public int getEntitiesRemoved() {
        return this._entitiesRemoved;
    }

    public int getEntityCount() {
        return this._entityCount;
    }

    public String toString() {
        return "CollectionMods{added=" + this._entitiesAdded + ", removed=" + this._entitiesRemoved + ", count=" + this._entityCount + '}';
    }
}

