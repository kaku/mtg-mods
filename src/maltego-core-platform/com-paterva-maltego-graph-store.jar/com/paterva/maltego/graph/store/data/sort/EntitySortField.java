/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.data.sort;

public class EntitySortField {
    public static final int DISPLAY_STRING = 0;
    public static final int SELECTED = 1;
    public static final int INSPECT = 2;
    public static final int BOOKMARK = 3;
    public static final int PINNED = 4;
    public static final int COLLECTED = 5;
    public static final int INCOMING = 6;
    public static final int OUTGOING = 7;
    public static final int WEIGHT = 8;
    public static final int TYPE = 9;
    public static final int ID = 10;
}

