/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 */
package com.paterva.maltego.graph.store.data;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.SearchOptions;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GraphDataStoreReader {
    public int getEntityCount() throws GraphStoreException;

    public Set<EntityID> getEntityIDs() throws GraphStoreException;

    public Set<LinkID> getLinkIDs() throws GraphStoreException;

    public Map<EntityID, MaltegoEntity> getEntities(EntitiesDataQuery var1) throws GraphStoreException;

    public MaltegoEntity getEntity(EntityID var1, EntityDataQuery var2) throws GraphStoreException;

    public MaltegoEntity getEntity(EntityID var1) throws GraphStoreException;

    public int getLinkCount() throws GraphStoreException;

    public Map<EntityID, String> getEntityTypes(Collection<EntityID> var1) throws GraphStoreException;

    public String getEntityType(EntityID var1) throws GraphStoreException;

    public Set<String> getEntityTypes() throws GraphStoreException;

    public Map<LinkID, MaltegoLink> getLinks(LinksDataQuery var1) throws GraphStoreException;

    public MaltegoLink getLink(LinkID var1, LinkDataQuery var2) throws GraphStoreException;

    public MaltegoLink getLink(LinkID var1) throws GraphStoreException;

    public Map<MaltegoEntity, EntityID> getEntityMatches(Collection<MaltegoEntity> var1, MatchingRuleDescriptor var2) throws GraphStoreException;

    public Collection<EntityID> getFilteredEntities(String var1, Collection<EntityID> var2) throws GraphStoreException;

    public /* varargs */ List<EntityID> getSortedEntities(Collection<EntityID> var1, SortField ... var2) throws GraphStoreException;

    public Collection<LinkID> getFilteredLinks(String var1, Collection<LinkID> var2) throws GraphStoreException;

    public List<LinkID> getSortedLinks(SortField var1, Collection<LinkID> var2) throws GraphStoreException;

    public Set<EntityID> searchEntities(SearchOptions var1) throws GraphStoreException;

    public Set<LinkID> searchLinks(SearchOptions var1) throws GraphStoreException;
}

