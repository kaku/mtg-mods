/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.copy;

import com.paterva.maltego.graph.store.copy.DefaultGraphCopier;
import com.paterva.maltego.graph.store.copy.GraphCopier;
import com.paterva.maltego.graph.store.copy.GraphCopyFactory;

public class DefaultGraphCopyFactory
extends GraphCopyFactory {
    @Override
    public GraphCopier create() {
        return new DefaultGraphCopier();
    }
}

