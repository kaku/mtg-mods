/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.store.copy;

import com.paterva.maltego.graph.store.copy.DefaultGraphCopyFactory;
import com.paterva.maltego.graph.store.copy.GraphCopier;
import org.openide.util.Lookup;

public abstract class GraphCopyFactory {
    private static GraphCopyFactory _default;

    public static synchronized GraphCopyFactory getDefault() {
        if (_default == null && (GraphCopyFactory._default = (GraphCopyFactory)Lookup.getDefault().lookup(GraphCopyFactory.class)) == null) {
            _default = new DefaultGraphCopyFactory();
        }
        return _default;
    }

    public abstract GraphCopier create();
}

