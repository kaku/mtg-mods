/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.layout;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.BatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface GraphLayoutWriter
extends BatchUpdatable<Boolean> {
    public void addEntities(Map<EntityID, Point> var1) throws GraphStoreException;

    public void addEntities(Collection<EntityID> var1) throws GraphStoreException;

    public void addEntity(EntityID var1) throws GraphStoreException;

    public void addLinks(Map<LinkID, List<Point>> var1) throws GraphStoreException;

    public void addLinks(Collection<LinkID> var1) throws GraphStoreException;

    public void addLink(LinkID var1) throws GraphStoreException;

    public void setCenters(Map<EntityID, Point> var1) throws GraphStoreException;

    public void setCenter(EntityID var1, Point var2) throws GraphStoreException;

    public void setPaths(Map<LinkID, List<Point>> var1) throws GraphStoreException;

    public void setPath(LinkID var1, List<Point> var2) throws GraphStoreException;

    public void remove(Collection<EntityID> var1, Collection<LinkID> var2) throws GraphStoreException;

    public void removeEntities(Collection<EntityID> var1) throws GraphStoreException;

    public void removeEntity(EntityID var1) throws GraphStoreException;

    public void removeLinks(Collection<LinkID> var1) throws GraphStoreException;

    public void removeLink(LinkID var1) throws GraphStoreException;

    public void clear() throws GraphStoreException;
}

