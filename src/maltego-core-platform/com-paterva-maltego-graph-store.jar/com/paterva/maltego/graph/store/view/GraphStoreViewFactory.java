/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.store.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import org.openide.util.Lookup;

public abstract class GraphStoreViewFactory {
    private static GraphStoreViewFactory _instance;

    public static synchronized GraphStoreViewFactory getDefault() {
        if (_instance == null && (GraphStoreViewFactory._instance = (GraphStoreViewFactory)Lookup.getDefault().lookup(GraphStoreViewFactory.class)) == null) {
            throw new IllegalStateException("Factory not found.");
        }
        return _instance;
    }

    public abstract GraphStoreView create(GraphID var1) throws GraphStoreException;
}

