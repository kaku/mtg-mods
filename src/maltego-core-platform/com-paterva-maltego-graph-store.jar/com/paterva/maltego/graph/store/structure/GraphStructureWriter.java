/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.structure;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.BatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface GraphStructureWriter
extends BatchUpdatable<Boolean> {
    public void add(Collection<EntityID> var1) throws GraphStoreException;

    public void add(EntityID var1) throws GraphStoreException;

    public void connect(Map<LinkID, LinkEntityIDs> var1) throws GraphStoreException;

    public void connect(LinkID var1, LinkEntityIDs var2) throws GraphStoreException;

    public void connect(LinkID var1, EntityID var2, EntityID var3) throws GraphStoreException;

    public Set<LinkID> removeEntities(Collection<EntityID> var1) throws GraphStoreException;

    public void removeLinks(Collection<LinkID> var1) throws GraphStoreException;

    public void remove(Collection<EntityID> var1, Collection<LinkID> var2) throws GraphStoreException;

    public void clear() throws GraphStoreException;

    public void setPinned(Collection<EntityID> var1, boolean var2) throws GraphStoreException;

    public void setPinned(Map<EntityID, Boolean> var1) throws GraphStoreException;
}

