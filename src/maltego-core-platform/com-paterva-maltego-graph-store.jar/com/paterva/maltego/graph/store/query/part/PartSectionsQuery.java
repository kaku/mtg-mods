/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.query.part;

public abstract class PartSectionsQuery {
    private boolean _queryType = false;
    private boolean _queryCachedValueStr = false;
    private boolean _queryCachedDisplayStr = false;
    private boolean _queryCachedImageValue = false;
    private boolean _queryCachedHasAttachments = false;
    private boolean _queryBookmark = false;
    private boolean _queryNotes = false;
    private boolean _queryDisplayInfo = false;
    private boolean _queryValuePropertyName = false;
    private boolean _queryDisplayPropertyName = false;

    public boolean isQueryType() {
        return this._queryType;
    }

    public void setQueryType(boolean bl) {
        this._queryType = bl;
    }

    public boolean isQueryCachedValueStr() {
        return this._queryCachedValueStr;
    }

    public void setQueryCachedValueStr(boolean bl) {
        this._queryCachedValueStr = bl;
    }

    public boolean isQueryCachedDisplayStr() {
        return this._queryCachedDisplayStr;
    }

    public void setQueryCachedDisplayStr(boolean bl) {
        this._queryCachedDisplayStr = bl;
    }

    public boolean isQueryCachedImageValue() {
        return this._queryCachedImageValue;
    }

    public void setQueryCachedImageValue(boolean bl) {
        this._queryCachedImageValue = bl;
    }

    public boolean isQueryCachedHasAttachments() {
        return this._queryCachedHasAttachments;
    }

    public void setQueryCachedHasAttachments(boolean bl) {
        this._queryCachedHasAttachments = bl;
    }

    public boolean isQueryValuePropertyName() {
        return this._queryValuePropertyName;
    }

    public void setQueryValuePropertyName(boolean bl) {
        this._queryValuePropertyName = bl;
    }

    public boolean isQueryDisplayPropertyName() {
        return this._queryDisplayPropertyName;
    }

    public void setQueryDisplayPropertyName(boolean bl) {
        this._queryDisplayPropertyName = bl;
    }

    public boolean isQueryDisplayInfo() {
        return this._queryDisplayInfo;
    }

    public void setQueryDisplayInfo(boolean bl) {
        this._queryDisplayInfo = bl;
    }

    public boolean isQueryNotes() {
        return this._queryNotes;
    }

    public void setQueryNotes(boolean bl) {
        this._queryNotes = bl;
    }

    public boolean isQueryBookmark() {
        return this._queryBookmark;
    }

    public void setQueryBookmark(boolean bl) {
        this._queryBookmark = bl;
    }
}

