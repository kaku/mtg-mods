/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph.store;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;

public abstract class ClipboardGraphID
implements GraphStore {
    private static final GraphID GRAPH_ID = GraphID.create();

    public static GraphID get() {
        return GRAPH_ID;
    }
}

