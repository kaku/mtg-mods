/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.PropertiesStub;
import com.paterva.entity.serializer.PropertyStub;
import com.paterva.entity.serializer.PropertyTranslator;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class PropertiesSerializer {
    public void write(OutputStream outputStream, PropertyDescriptorCollection propertyDescriptorCollection, DataSource dataSource) throws XmlSerializationException {
        PropertyTranslator propertyTranslator = new PropertyTranslator();
        List<PropertyStub> list = propertyTranslator.translate(propertyDescriptorCollection, dataSource);
        PropertiesStub propertiesStub = new PropertiesStub(list);
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)propertiesStub, outputStream);
    }

    public void read(InputStream inputStream, PropertyDescriptorCollection propertyDescriptorCollection, DataSource dataSource) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        PropertiesStub propertiesStub = (PropertiesStub)xmlSerializer.read(PropertiesStub.class, inputStream);
        if (propertiesStub != null) {
            PropertyTranslator propertyTranslator = new PropertyTranslator();
            propertyTranslator.translate(propertiesStub.getPropertiess(), propertyDescriptorCollection, dataSource);
        }
    }
}

