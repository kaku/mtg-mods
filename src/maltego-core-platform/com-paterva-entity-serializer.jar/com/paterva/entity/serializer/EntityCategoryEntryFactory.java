/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.EntityCategoryEntry;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;

public class EntityCategoryEntryFactory
implements EntryFactory<EntityCategoryEntry> {
    public EntityCategoryEntry create(String string) {
        return new EntityCategoryEntry(string);
    }

    public String getFolderName() {
        return "EntityCategories";
    }

    public String getExtension() {
        return "category";
    }
}

