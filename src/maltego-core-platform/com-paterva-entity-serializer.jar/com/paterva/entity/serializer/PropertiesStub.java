/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.PropertyStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Properties", strict=0)
class PropertiesStub {
    @Attribute(name="image", required=0)
    private String _imageProperty;
    @Attribute(name="value", required=0)
    private String _valueProperty;
    @Attribute(name="displayValue", required=0)
    private String _displayValueProperty;
    @ElementList(inline=1, type=PropertyStub.class, required=0)
    private List<PropertyStub> _fields;

    public PropertiesStub() {
    }

    public PropertiesStub(List<PropertyStub> list) {
        this._fields = list;
    }

    public List<PropertyStub> getPropertiess() {
        return this._fields;
    }

    public void setProperties(List<PropertyStub> list) {
        this._fields = list;
    }
}

