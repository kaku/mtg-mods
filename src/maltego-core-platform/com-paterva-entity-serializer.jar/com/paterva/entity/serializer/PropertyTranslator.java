/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.serializer.UnresolvedReferenceException
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.PropertyStub;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import java.util.LinkedList;
import java.util.List;

class PropertyTranslator {
    PropertyTranslator() {
    }

    public List<PropertyStub> translate(PropertyDescriptorCollection propertyDescriptorCollection, DataSource dataSource) {
        LinkedList<PropertyStub> linkedList = new LinkedList<PropertyStub>();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            Object object = dataSource.getValue(propertyDescriptor);
            if (object == null) continue;
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
            PropertyStub propertyStub = this.translate(propertyDescriptor, typeDescriptor);
            propertyStub.setValue(typeDescriptor.convert(object));
            linkedList.add(propertyStub);
        }
        return linkedList;
    }

    private PropertyStub translate(PropertyDescriptor propertyDescriptor, TypeDescriptor typeDescriptor) {
        PropertyStub propertyStub = new PropertyStub();
        propertyStub.setName(propertyDescriptor.getName());
        propertyStub.setDisplayName(propertyDescriptor.getDisplayName());
        propertyStub.setType(typeDescriptor.getTypeName());
        propertyStub.setHidden(propertyDescriptor.isHidden());
        propertyStub.setNullable(propertyDescriptor.isNullable());
        propertyStub.setReadonly(propertyDescriptor.isReadonly());
        return propertyStub;
    }

    public void translate(List<PropertyStub> list, PropertyDescriptorCollection propertyDescriptorCollection, DataSource dataSource) throws UnresolvedReferenceException {
        if (list != null) {
            for (PropertyStub propertyStub : list) {
                String string = propertyStub.getType();
                TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string);
                if (typeDescriptor == null) {
                    throw new UnresolvedReferenceException("The data type " + string + " is not known.");
                }
                PropertyDescriptor propertyDescriptor = this.translate(propertyStub, typeDescriptor);
                Object object = typeDescriptor.convert(propertyStub.getValue());
                dataSource.setValue(propertyDescriptor, object);
                propertyDescriptorCollection.add(propertyDescriptor);
            }
        }
    }

    private PropertyDescriptor translate(PropertyStub propertyStub, TypeDescriptor typeDescriptor) {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), propertyStub.getName(), propertyStub.getDisplayName());
        propertyDescriptor.setHidden(propertyStub.isHidden());
        propertyDescriptor.setNullable(propertyStub.isNullable());
        propertyDescriptor.setReadonly(propertyStub.isReadonly());
        return propertyDescriptor;
    }
}

