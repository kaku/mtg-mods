/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.PropertiesStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="MaltegoEntity", strict=0)
class EntityStub {
    @Attribute(name="type")
    private String _type;
    @Element(name="Properties", required=0)
    private PropertiesStub _properties;

    EntityStub() {
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public PropertiesStub getProperties() {
        return this._properties;
    }
}

