/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.util.Lookup
 */
package com.paterva.entity.serializer;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.util.Lookup;

public abstract class EntitySerializer {
    public static EntitySerializer getDefault() {
        EntitySerializer entitySerializer = (EntitySerializer)Lookup.getDefault().lookup(EntitySerializer.class);
        if (entitySerializer == null) {
            entitySerializer = new NullSerializer();
        }
        return entitySerializer;
    }

    public abstract void write(MaltegoEntity var1, OutputStream var2) throws XmlSerializationException;

    public abstract MaltegoEntity read(InputStream var1) throws XmlSerializationException;

    private static class NullSerializer
    extends EntitySerializer {
        private NullSerializer() {
        }

        @Override
        public void write(MaltegoEntity maltegoEntity, OutputStream outputStream) throws XmlSerializationException {
            throw new XmlSerializationException("No entity spec serializer registered");
        }

        @Override
        public MaltegoEntity read(InputStream inputStream) throws XmlSerializationException {
            throw new XmlSerializationException("No entity spec serializer registered");
        }
    }

}

