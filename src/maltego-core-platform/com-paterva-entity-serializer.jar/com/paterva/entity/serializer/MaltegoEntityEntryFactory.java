/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.MaltegoEntityEntry;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;

public class MaltegoEntityEntryFactory
implements EntryFactory<MaltegoEntityEntry> {
    public MaltegoEntityEntry create(String string) {
        return new MaltegoEntityEntry(string);
    }

    public String getFolderName() {
        return "Entities";
    }

    public String getExtension() {
        return "entity";
    }
}

