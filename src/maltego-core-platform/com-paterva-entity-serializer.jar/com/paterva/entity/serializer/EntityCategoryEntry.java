/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.CategorySerializer;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class EntityCategoryEntry
extends Entry<String> {
    public static final String DefaultFolder = "EntityCategories";
    public static final String Type = "category";

    public EntityCategoryEntry(String string, String string2) {
        super((Object)string, "EntityCategories", string2 + "." + "category", string);
    }

    public EntityCategoryEntry(String string) {
        super(string);
    }

    protected String read(InputStream inputStream) throws IOException {
        try {
            return CategorySerializer.read(inputStream);
        }
        catch (XmlSerializationException var2_2) {
            throw new IOException((Throwable)var2_2);
        }
    }

    protected void write(String string, OutputStream outputStream) throws IOException {
        CategorySerializer.write(string, outputStream);
    }
}

