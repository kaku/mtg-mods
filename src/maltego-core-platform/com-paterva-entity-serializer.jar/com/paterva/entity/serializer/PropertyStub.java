/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.entity.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Property", strict=0)
public class PropertyStub {
    @Attribute(name="name")
    private String _name;
    private String _displayName;
    @Attribute(name="type")
    private String _type;
    @Attribute(name="nullable", required=0)
    private boolean _nullable = true;
    @Attribute(name="hidden", required=0)
    private boolean _hidden = false;
    @Attribute(name="readonly", required=0)
    private boolean _readonly = false;
    @Element(name="Value", required=0)
    private String _value;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    @Attribute(name="displayName", required=0)
    public String getDisplayName() {
        if (this.getName().equals(this._displayName)) {
            return null;
        }
        return this._displayName;
    }

    @Attribute(name="displayName", required=0)
    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public boolean isNullable() {
        return this._nullable;
    }

    public void setNullable(boolean bl) {
        this._nullable = bl;
    }

    public boolean isHidden() {
        return this._hidden;
    }

    public void setHidden(boolean bl) {
        this._hidden = bl;
    }

    public boolean isReadonly() {
        return this._readonly;
    }

    public void setReadonly(boolean bl) {
        this._readonly = bl;
    }

    public String getValue() {
        return this._value;
    }

    public void setValue(String string) {
        this._value = string;
    }
}

