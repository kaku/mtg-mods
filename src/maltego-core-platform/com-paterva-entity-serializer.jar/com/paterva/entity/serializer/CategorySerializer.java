/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.entity.serializer;

import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

public class CategorySerializer {
    public static void write(String string, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        EntityCategoryStub entityCategoryStub = new EntityCategoryStub();
        entityCategoryStub.setName(string);
        xmlSerializer.write((Object)entityCategoryStub, outputStream);
    }

    public static String read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        EntityCategoryStub entityCategoryStub = (EntityCategoryStub)xmlSerializer.read(EntityCategoryStub.class, inputStream);
        return entityCategoryStub.getName();
    }

    @Root(name="EntityCategory", strict=0)
    static class EntityCategoryStub {
        @Attribute(name="name")
        private String _name;

        EntityCategoryStub() {
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }
    }

}

