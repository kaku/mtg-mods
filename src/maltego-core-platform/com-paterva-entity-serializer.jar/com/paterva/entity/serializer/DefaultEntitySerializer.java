/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GenericEntity
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.entity.serializer;

import com.paterva.entity.serializer.EntitySerializer;
import com.paterva.entity.serializer.EntityStub;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;

public class DefaultEntitySerializer
extends EntitySerializer {
    @Override
    public void write(MaltegoEntity maltegoEntity, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        EntityStub entityStub = this.translate(maltegoEntity);
        xmlSerializer.write((Object)entityStub, outputStream);
    }

    @Override
    public MaltegoEntity read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        EntityStub entityStub = (EntityStub)xmlSerializer.read(EntityStub.class, inputStream);
        return this.translate(entityStub);
    }

    private EntityStub translate(MaltegoEntity maltegoEntity) {
        EntityStub entityStub = new EntityStub();
        entityStub.setType(maltegoEntity.getTypeName());
        return entityStub;
    }

    private MaltegoEntity translate(EntityStub entityStub) {
        GenericEntity genericEntity = new GenericEntity(EntityID.create(), entityStub.getType());
        return genericEntity;
    }
}

