/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.entity.api.EntitySpecSerializer
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.entity.serializer;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.entity.api.EntitySpecSerializer;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MaltegoEntityEntry
extends Entry<MaltegoEntitySpec> {
    static final String DefaultFolder = "Entities";
    public static final String Type = "entity";

    public MaltegoEntityEntry(MaltegoEntitySpec maltegoEntitySpec) {
        super((Object)maltegoEntitySpec, "Entities", maltegoEntitySpec.getTypeName() + "." + "entity", maltegoEntitySpec.getDisplayName());
    }

    public MaltegoEntityEntry(String string) {
        super(string);
    }

    protected MaltegoEntitySpec read(InputStream inputStream) throws IOException {
        try {
            return EntitySpecSerializer.getDefault().read(inputStream);
        }
        catch (XmlSerializationException var2_2) {
            throw new IOException((Throwable)var2_2);
        }
        catch (TypeInstantiationException var2_3) {
            throw new IOException((Throwable)var2_3);
        }
    }

    protected void write(MaltegoEntitySpec maltegoEntitySpec, OutputStream outputStream) throws IOException {
        EntitySpecSerializer.getDefault().write(maltegoEntitySpec, outputStream);
    }
}

