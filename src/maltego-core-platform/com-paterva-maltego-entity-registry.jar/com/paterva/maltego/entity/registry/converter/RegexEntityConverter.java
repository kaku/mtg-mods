/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.entity.registry.converter;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.registry.converter.AbstractEntityConverter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.util.StringUtilities;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexEntityConverter
extends AbstractEntityConverter {
    private Pattern _pattern;
    private String _patternString;
    private List<String> _properties;
    private String _mainProperty;

    public void init(String string) {
        this._patternString = string;
        this._pattern = Pattern.compile(string);
    }

    public List<String> getProperties() {
        return this._properties;
    }

    public void setProperties(List<String> list) {
        this._properties = list;
    }

    public String getMainProperty() {
        return this._mainProperty;
    }

    public void setMainProperty(String string) {
        this._mainProperty = string;
    }

    @Override
    public boolean canConvertFrom(Object object, PropertyDescriptor propertyDescriptor) {
        if (object instanceof String && this._pattern.matcher((String)object).matches()) {
            return true;
        }
        return false;
    }

    @Override
    public MaltegoEntity convertFrom(Object object, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor, boolean bl) {
        if (object instanceof String) {
            if (this._properties == null || this._properties.isEmpty()) {
                PropertyDescriptor propertyDescriptor2 = maltegoEntity.getProperties().get(this._mainProperty);
                if (propertyDescriptor2 != null) {
                    maltegoEntity.setValue(propertyDescriptor2, object, true, bl);
                }
            } else {
                this.setPropertiesFromGroups(object, maltegoEntity);
            }
        }
        return maltegoEntity;
    }

    private void setPropertiesFromGroups(Object object, MaltegoEntity maltegoEntity) {
        Matcher matcher = this._pattern.matcher((String)object);
        if (matcher.matches()) {
            for (int i = 1; i <= matcher.groupCount() && i <= this._properties.size(); ++i) {
                PropertyDescriptor propertyDescriptor;
                String string = this._properties.get(i - 1);
                if (StringUtilities.isNullOrEmpty((String)string) || (propertyDescriptor = maltegoEntity.getProperties().get(string)) == null) continue;
                String string2 = matcher.group(i);
                maltegoEntity.setValue(propertyDescriptor, (Object)(string2 != null ? string2 : ""));
            }
        }
    }

    public String toString() {
        return this._patternString;
    }

    public int getPriority() {
        return 10;
    }
}

