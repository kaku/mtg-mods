/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.FileSystemRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.Collection;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class DefaultEntityRegistry
extends EntityRegistry {
    private static final String MALTEGO_ENTITIES_FOLDER = "Maltego/Entities";
    private static final String DEFAULT_FOLDER = "Miscellaneous";
    private FileSystemRegistry<MaltegoEntitySpec> _delegate;

    public DefaultEntityRegistry() {
        this("Maltego/Entities");
    }

    public DefaultEntityRegistry(FileObject fileObject) {
        this(fileObject, "Maltego/Entities");
    }

    public DefaultEntityRegistry(String string) {
        this(FileUtil.getConfigRoot(), string);
    }

    public DefaultEntityRegistry(FileObject fileObject, String string) {
        this._delegate = new FileSystemRegistry(fileObject, string, "entity");
    }

    public MaltegoEntitySpec get(String string) {
        return this._delegate.get(string);
    }

    public Collection<MaltegoEntitySpec> getAll() {
        return this._delegate.getAll();
    }

    public void put(MaltegoEntitySpec maltegoEntitySpec) {
        this.put(maltegoEntitySpec, maltegoEntitySpec.getDefaultCategory());
    }

    public void put(MaltegoEntitySpec maltegoEntitySpec, String string) {
        if (string == null) {
            string = "Miscellaneous";
        }
        if (this._delegate.contains(maltegoEntitySpec.getTypeName())) {
            this._delegate.update(maltegoEntitySpec);
            this.fireTypeUpdated((TypeSpec)maltegoEntitySpec);
        } else {
            this._delegate.add(maltegoEntitySpec, string);
            this.fireTypeAdded((TypeSpec)maltegoEntitySpec);
        }
    }

    public void remove(String string) {
        MaltegoEntitySpec maltegoEntitySpec = this.get(string);
        if (maltegoEntitySpec != null) {
            this._delegate.remove(string);
            this.fireTypeRemoved((TypeSpec)maltegoEntitySpec);
        }
    }

    public boolean contains(String string) {
        return this._delegate.contains(string);
    }

    public String[] allCategories() {
        return this._delegate.getFolders();
    }
}

