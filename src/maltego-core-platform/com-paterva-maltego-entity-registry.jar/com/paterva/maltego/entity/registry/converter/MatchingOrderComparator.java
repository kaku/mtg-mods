/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 */
package com.paterva.maltego.entity.registry.converter;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import java.util.Comparator;

public class MatchingOrderComparator
implements Comparator<MaltegoEntitySpec> {
    @Override
    public int compare(MaltegoEntitySpec maltegoEntitySpec, MaltegoEntitySpec maltegoEntitySpec2) {
        if (maltegoEntitySpec.getConversionOrder() == maltegoEntitySpec2.getConversionOrder()) {
            return 0;
        }
        if (maltegoEntitySpec.getConversionOrder() < maltegoEntitySpec2.getConversionOrder()) {
            return -1;
        }
        return 1;
    }
}

