/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.entity.registry.serializer.RegexGroup;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Converter", strict=0)
class ConverterStub {
    @Element(name="Value", required=0, data=1)
    public String Regex;
    @ElementList(name="RegexGroups", type=RegexGroup.class, required=0)
    public List<RegexGroup> Groups;

    ConverterStub() {
    }
}

