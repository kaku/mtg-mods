/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityConverter
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.SpecActionDescriptor
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.serializer.FieldsSerializer
 *  com.paterva.maltego.typing.serializer.PropertiesStub
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.SpecActionDescriptor;
import com.paterva.maltego.entity.registry.converter.AllEntityConverter;
import com.paterva.maltego.entity.registry.converter.RegexEntityConverter;
import com.paterva.maltego.entity.registry.serializer.ActionStub;
import com.paterva.maltego.entity.registry.serializer.BaseEntityStub;
import com.paterva.maltego.entity.registry.serializer.ConverterStub;
import com.paterva.maltego.entity.registry.serializer.EntitySpecStub;
import com.paterva.maltego.entity.registry.serializer.RegexGroup;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.serializer.FieldsSerializer;
import com.paterva.maltego.typing.serializer.PropertiesStub;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

class EntitySpecTranslator {
    EntitySpecTranslator() {
    }

    public MaltegoEntitySpec translate(EntitySpecStub entitySpecStub) throws TypeInstantiationException, XmlSerializationException {
        MaltegoEntitySpec maltegoEntitySpec;
        block16 : {
            FieldsSerializer fieldsSerializer = new FieldsSerializer();
            PropertyConfiguration propertyConfiguration = fieldsSerializer.readSerializationStub(entitySpecStub.Properties);
            maltegoEntitySpec = new MaltegoEntitySpec(entitySpecStub.ID, propertyConfiguration);
            maltegoEntitySpec.setBaseEntitySpecs(this.translateBaseEntityStubs(entitySpecStub.BaseEntities));
            maltegoEntitySpec.setDescription(entitySpecStub.Description);
            maltegoEntitySpec.setDisplayName(entitySpecStub.Name);
            maltegoEntitySpec.setDisplayNamePlural(entitySpecStub.PluralName);
            if (!StringUtilities.isNullOrEmpty((String)entitySpecStub.Category)) {
                maltegoEntitySpec.setDefaultCategory(entitySpecStub.Category);
            } else {
                maltegoEntitySpec.setDefaultCategory("My Entities");
            }
            if (entitySpecStub.LargeIconResource != null) {
                maltegoEntitySpec.setLargeIconResource(entitySpecStub.LargeIconResource);
            } else if (entitySpecStub.LargeIcon != null) {
                maltegoEntitySpec.setLargeIcon(this.decodeIcon(entitySpecStub.LargeIcon));
            }
            if (maltegoEntitySpec.getLargeIconResource() == null && maltegoEntitySpec.getLargeIcon() == null) {
                maltegoEntitySpec.setLargeIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/entity/registry/UnknownEntity24.png"));
            }
            if (entitySpecStub.SmallIconResource != null) {
                maltegoEntitySpec.setSmallIconResource(entitySpecStub.SmallIconResource);
            } else if (entitySpecStub.SmallIcon != null) {
                maltegoEntitySpec.setSmallIcon(this.decodeIcon(entitySpecStub.SmallIcon));
            }
            if (maltegoEntitySpec.getSmallIconResource() == null && maltegoEntitySpec.getSmallIcon() == null) {
                maltegoEntitySpec.setSmallIcon((Image)ImageUtils.smartSize((BufferedImage)ImageUtils.createBufferedImage((Image)maltegoEntitySpec.getLargeIcon()), (double)16.0));
            }
            maltegoEntitySpec.setToolboxItem(entitySpecStub.IsAllowedRoot);
            maltegoEntitySpec.setVisible(entitySpecStub.Visible);
            maltegoEntitySpec.setHelpUrl(entitySpecStub.HelpUrl);
            maltegoEntitySpec.setHelpText(entitySpecStub.HelpText);
            if (!StringUtilities.isNullOrEmpty((String)entitySpecStub.ClassName)) {
                try {
                    ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    Class class_ = Class.forName(entitySpecStub.ClassName, false, classLoader);
                    if (MaltegoEntity.class.isAssignableFrom(class_)) {
                        maltegoEntitySpec.setInstanceClass(class_);
                        break block16;
                    }
                    throw new TypeInstantiationException("Class " + entitySpecStub.ClassName + " does not implement interface MaltegoEntity.");
                }
                catch (ClassNotFoundException var5_6) {
                    throw new TypeInstantiationException("Could not find class " + entitySpecStub.ClassName, (Exception)var5_6);
                }
            }
        }
        if (entitySpecStub.ConversionOrder != null) {
            maltegoEntitySpec.setConversionOrder(Integer.parseInt(entitySpecStub.ConversionOrder));
        }
        maltegoEntitySpec.setConverter(this.getConverter(entitySpecStub.Converter, entitySpecStub.Properties));
        maltegoEntitySpec.setColor(EntitySpecTranslator.getColor(entitySpecStub.Color));
        if (entitySpecStub.Actions != null) {
            maltegoEntitySpec.setActions(this.getActionsFromStubs(entitySpecStub.Actions));
        }
        return maltegoEntitySpec;
    }

    private List<String> translateBaseEntityStubs(List<BaseEntityStub> list) {
        ArrayList<String> arrayList = new ArrayList<String>();
        if (list != null) {
            for (BaseEntityStub baseEntityStub : list) {
                arrayList.add(baseEntityStub.getText().trim());
            }
        }
        return arrayList;
    }

    private EntityConverter getConverter(ConverterStub converterStub, PropertiesStub propertiesStub) {
        if (converterStub != null && propertiesStub != null && converterStub.Regex != null) {
            RegexEntityConverter regexEntityConverter = new RegexEntityConverter();
            regexEntityConverter.init(converterStub.Regex);
            regexEntityConverter.setMainProperty(propertiesStub.getValueProperty());
            if (converterStub.Groups != null) {
                ArrayList<String> arrayList = new ArrayList<String>();
                for (RegexGroup regexGroup : converterStub.Groups) {
                    arrayList.add(regexGroup.Property);
                }
                regexEntityConverter.setProperties(arrayList);
            }
            return regexEntityConverter;
        }
        return AllEntityConverter.instance();
    }

    private static Color getColor(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        return Color.decode(string);
    }

    private Image decodeIcon(String string) {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageUtils.base64Decode((String)string);
        }
        catch (IOException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
        return bufferedImage;
    }

    private List<SpecActionDescriptor> getActionsFromStubs(List<ActionStub> list) {
        ArrayList<SpecActionDescriptor> arrayList = new ArrayList<SpecActionDescriptor>(list.size());
        for (ActionStub actionStub : list) {
            SpecActionDescriptor specActionDescriptor = new SpecActionDescriptor(actionStub.Type, actionStub.Name, actionStub.DisplayName, actionStub.Config);
            arrayList.add(specActionDescriptor);
        }
        return arrayList;
    }

    public EntitySpecStub translate(MaltegoEntitySpec maltegoEntitySpec) throws XmlSerializationException {
        EntitySpecStub entitySpecStub = new EntitySpecStub();
        if (maltegoEntitySpec.getInstanceClass() != null) {
            entitySpecStub.ClassName = maltegoEntitySpec.getInstanceClass().getName();
        }
        entitySpecStub.BaseEntities = this.translateBaseEntities(maltegoEntitySpec.getBaseEntitySpecs());
        entitySpecStub.ConversionOrder = String.valueOf(maltegoEntitySpec.getConversionOrder());
        if (maltegoEntitySpec.getConverter() instanceof RegexEntityConverter) {
            entitySpecStub.Converter = this.translateConverter((RegexEntityConverter)maltegoEntitySpec.getConverter());
        }
        entitySpecStub.Description = maltegoEntitySpec.getDescription();
        entitySpecStub.HelpText = maltegoEntitySpec.getHelpText();
        entitySpecStub.HelpUrl = maltegoEntitySpec.getHelpUrl();
        entitySpecStub.ID = maltegoEntitySpec.getTypeName();
        entitySpecStub.IsAllowedRoot = maltegoEntitySpec.isToolboxItem();
        entitySpecStub.Visible = maltegoEntitySpec.isVisible();
        entitySpecStub.Name = maltegoEntitySpec.getDisplayName();
        entitySpecStub.PluralName = maltegoEntitySpec.getDisplayNamePlural();
        entitySpecStub.Category = maltegoEntitySpec.getDefaultCategory();
        entitySpecStub.SmallIconResource = maltegoEntitySpec.getSmallIconResource();
        if (entitySpecStub.SmallIconResource == null && maltegoEntitySpec.getSmallIcon() != null) {
            entitySpecStub.SmallIcon = EntitySpecTranslator.encodeIcon(maltegoEntitySpec.getSmallIcon());
        }
        entitySpecStub.LargeIconResource = maltegoEntitySpec.getLargeIconResource();
        if (entitySpecStub.LargeIconResource == null && maltegoEntitySpec.getLargeIcon() != null) {
            entitySpecStub.LargeIcon = EntitySpecTranslator.encodeIcon(maltegoEntitySpec.getLargeIcon());
        }
        entitySpecStub.Color = EntitySpecTranslator.getColorName(maltegoEntitySpec.getColor());
        FieldsSerializer fieldsSerializer = new FieldsSerializer();
        entitySpecStub.Properties = fieldsSerializer.createSerializationStub(maltegoEntitySpec.getPropertyConfiguration());
        entitySpecStub.Actions = this.getStubsFromActions(maltegoEntitySpec.getActions());
        return entitySpecStub;
    }

    private List<BaseEntityStub> translateBaseEntities(List<String> list) {
        ArrayList<BaseEntityStub> arrayList = new ArrayList<BaseEntityStub>();
        for (String string : list) {
            if ("maltego.Unknown".equals(string)) continue;
            BaseEntityStub baseEntityStub = new BaseEntityStub();
            baseEntityStub.setText(string);
            arrayList.add(baseEntityStub);
        }
        return arrayList.isEmpty() ? null : arrayList;
    }

    private static String encodeIcon(Image image) {
        if (image != null) {
            try {
                return ImageUtils.base64Encode((Image)image, (String)"png");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return null;
    }

    private ConverterStub translateConverter(RegexEntityConverter regexEntityConverter) {
        if (regexEntityConverter != null) {
            ConverterStub converterStub = new ConverterStub();
            converterStub.Regex = regexEntityConverter.toString();
            List<String> list = regexEntityConverter.getProperties();
            if (list != null) {
                converterStub.Groups = new LinkedList<RegexGroup>();
                for (int i = 0; i < list.size(); ++i) {
                    RegexGroup regexGroup = new RegexGroup();
                    regexGroup.Property = list.get(i);
                    converterStub.Groups.add(regexGroup);
                }
            }
            return converterStub;
        }
        return null;
    }

    private static String getColorName(Color color) {
        if (color == null) {
            return null;
        }
        return color.toString();
    }

    private List<ActionStub> getStubsFromActions(List<SpecActionDescriptor> list) {
        ArrayList<ActionStub> arrayList = null;
        if (list != null) {
            arrayList = new ArrayList<ActionStub>(list.size());
            for (SpecActionDescriptor specActionDescriptor : list) {
                ActionStub actionStub = new ActionStub();
                actionStub.Type = specActionDescriptor.getType();
                actionStub.Name = specActionDescriptor.getName();
                actionStub.DisplayName = specActionDescriptor.getDisplayName();
                actionStub.Config = specActionDescriptor.getConfig();
                arrayList.add(actionStub);
            }
        }
        return arrayList;
    }
}

