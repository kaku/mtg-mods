/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconUtils
 *  com.paterva.maltego.util.IconSize
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.imgfactoryapi.IconUtils;
import com.paterva.maltego.util.IconSize;
import java.awt.Image;
import java.io.IOException;
import java.util.EnumMap;
import org.openide.util.Exceptions;

public class EmbeddedIconExtractor {
    public static boolean extract(MaltegoEntitySpec maltegoEntitySpec, IconRegistry iconRegistry) {
        boolean bl = false;
        Image image = maltegoEntitySpec.getSmallIcon();
        Image image2 = maltegoEntitySpec.getLargeIcon();
        if (image != null || image2 != null) {
            String string;
            String string2 = IconUtils.createUniqueIconName((IconRegistry)iconRegistry, (String)maltegoEntitySpec.getDisplayName());
            EnumMap enumMap = new EnumMap(IconSize.class);
            if (image != null) {
                IconUtils.resizeAndAddIcon(enumMap, (Image)image, (IconSize)IconSize.TINY);
            }
            if (image2 != null) {
                IconUtils.resizeAndAddIcon(enumMap, (Image)image2, (IconSize)IconSize.LARGE);
            }
            boolean bl2 = false;
            String string3 = IconUtils.findIconName((IconRegistry)iconRegistry, (Image)image, (IconSize)IconSize.TINY);
            if (string3 != null && (string = IconUtils.findIconName((IconRegistry)iconRegistry, (Image)image2, (IconSize)IconSize.LARGE)) != null && string3.equals(string)) {
                bl2 = true;
                string2 = string3;
            }
            if (!bl2) {
                IconUtils.createMissingIcons(enumMap);
                try {
                    iconRegistry.add("Custom", string2, (Image)enumMap.get((Object)IconSize.TINY), (Image)enumMap.get((Object)IconSize.SMALL), (Image)enumMap.get((Object)IconSize.MEDIUM), (Image)enumMap.get((Object)IconSize.LARGE));
                }
                catch (IOException var9_10) {
                    Exceptions.printStackTrace((Throwable)var9_10);
                }
            }
            maltegoEntitySpec.setSmallIconResource(string2);
            maltegoEntitySpec.setLargeIconResource(string2);
            bl = true;
        }
        return bl;
    }
}

