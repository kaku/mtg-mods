/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityConverter
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.EntityStringConverter
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.entity.registry.converter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.EntityStringConverter;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.entity.registry.converter.MatchingOrderComparator;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.StringUtilities;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class DefaultEntityStringConverter
implements EntityStringConverter {
    public String convert(GraphID graphID, Collection<EntityID> collection, boolean bl, boolean bl2) {
        StringBuilder stringBuilder = new StringBuilder();
        EntitySkeletonProvider entitySkeletonProvider = SkeletonProviders.entitiesForGraph((GraphID)graphID);
        for (EntityID entityID : collection) {
            try {
                MaltegoEntity maltegoEntity = entitySkeletonProvider.getEntitySkeleton(entityID);
                if (bl) {
                    String string = maltegoEntity.getTypeName();
                    stringBuilder.append(string);
                    stringBuilder.append("#");
                }
                stringBuilder.append(maltegoEntity.getValueString());
                if (bl2) {
                    stringBuilder.append("#");
                    stringBuilder.append(maltegoEntity.getWeight());
                }
            }
            catch (GraphStoreException var9_10) {
                Exceptions.printStackTrace((Throwable)var9_10);
            }
            stringBuilder.append(StringUtilities.newLine());
        }
        return stringBuilder.toString();
    }

    public String convert(Collection<MaltegoEntity> collection, boolean bl, boolean bl2) {
        StringBuilder stringBuilder = new StringBuilder();
        for (MaltegoEntity maltegoEntity : collection) {
            stringBuilder.append(this.convert(maltegoEntity, bl, bl2));
            stringBuilder.append(StringUtilities.newLine());
        }
        return stringBuilder.toString();
    }

    public String convert(MaltegoEntity maltegoEntity, boolean bl, boolean bl2) {
        StringBuilder stringBuilder = new StringBuilder();
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        Object object = InheritanceHelper.getValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (object != null) {
            if (bl) {
                stringBuilder.append(maltegoEntity.getTypeName());
                stringBuilder.append("#");
            }
            stringBuilder.append(object.toString());
            if (bl2) {
                stringBuilder.append("#");
                stringBuilder.append(maltegoEntity.getWeight());
            }
        }
        return stringBuilder.toString();
    }

    public Collection<MaltegoEntity> convert(String string) {
        EntityRegistry entityRegistry = (EntityRegistry)Lookup.getDefault().lookup(EntityRegistry.class);
        assert (entityRegistry != null);
        MaltegoEntitySpec[] arrmaltegoEntitySpec = this.getSpecList(entityRegistry);
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
        if (string != null) {
            String[] arrstring;
            string = string.trim();
            for (String string2 : arrstring = string.split("\r|\n|\r\n|\t")) {
                MaltegoEntity maltegoEntity;
                if ((string2 = string2.trim()).isEmpty() || (maltegoEntity = this.convertOneImpl(string2, entityRegistry, arrmaltegoEntitySpec)) == null) continue;
                arrayList.add(maltegoEntity);
            }
        }
        return arrayList;
    }

    public MaltegoEntity convertOne(String string) {
        EntityRegistry entityRegistry = (EntityRegistry)Lookup.getDefault().lookup(EntityRegistry.class);
        assert (entityRegistry != null);
        MaltegoEntity maltegoEntity = this.convertOneImpl(string, entityRegistry, this.getSpecList(entityRegistry));
        return maltegoEntity;
    }

    private MaltegoEntity convertOneImpl(String string, EntityRegistry entityRegistry, MaltegoEntitySpec[] arrmaltegoEntitySpec) {
        Object object;
        Object object2;
        MaltegoEntitySpec maltegoEntitySpec = null;
        MaltegoEntity maltegoEntity = null;
        String[] arrstring = string.split("#");
        if (arrstring.length > 1 && (maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get((String)(object = arrstring[0]))) != null) {
            object2 = new String[arrstring.length - 1];
            for (int i = 0; i < arrstring.length - 1; ++i) {
                object2[i] = arrstring[i + 1];
            }
            arrstring = object2;
        }
        object = null;
        String string2 = string = arrstring.length > 0 ? arrstring[0] : "";
        if (arrstring.length > 1) {
            object2 = arrstring[arrstring.length - 1];
            try {
                object = Integer.parseInt((String)object2);
            }
            catch (NumberFormatException var9_10) {
                // empty catch block
            }
            int n = object == null ? arrstring.length : arrstring.length - 1;
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < n; ++i) {
                if (i != 0) {
                    stringBuilder.append("#");
                }
                stringBuilder.append((String)arrstring[i]);
            }
            string = stringBuilder.toString();
        }
        if (maltegoEntitySpec == null) {
            maltegoEntitySpec = this.findMatch(string, arrmaltegoEntitySpec);
        }
        if (maltegoEntitySpec != null) {
            maltegoEntity = this.createEntity(maltegoEntitySpec, string);
        }
        if (maltegoEntity != null && object != null) {
            maltegoEntity.setWeight((Integer)object);
        }
        return maltegoEntity;
    }

    private MaltegoEntity createEntity(MaltegoEntitySpec maltegoEntitySpec, String string) {
        MaltegoEntity maltegoEntity = null;
        EntityFactory entityFactory = (EntityFactory)Lookup.getDefault().lookup(EntityFactory.class);
        try {
            maltegoEntity = entityFactory.createInstance(maltegoEntitySpec, false, true);
        }
        catch (TypeInstantiationException var5_5) {
            Exceptions.printStackTrace((Throwable)var5_5);
        }
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity, (boolean)true);
        EntityConverter entityConverter = InheritanceHelper.getConverter((EntityRegistry)entityRegistry, (MaltegoEntity)maltegoEntity, (String)string, (PropertyDescriptor)propertyDescriptor);
        if (entityConverter != null) {
            maltegoEntity = entityConverter.convertFrom((Object)string, maltegoEntity, propertyDescriptor, false);
        } else {
            maltegoEntity.setValue(propertyDescriptor, (Object)string);
        }
        return maltegoEntity;
    }

    private MaltegoEntitySpec[] getSpecList(EntityRegistry entityRegistry) {
        Collection collection = entityRegistry.getAll();
        MaltegoEntitySpec[] arrmaltegoEntitySpec = collection.toArray((T[])new MaltegoEntitySpec[collection.size()]);
        Arrays.sort(arrmaltegoEntitySpec, new MatchingOrderComparator());
        return arrmaltegoEntitySpec;
    }

    private MaltegoEntitySpec findMatch(String string, MaltegoEntitySpec[] arrmaltegoEntitySpec) {
        for (MaltegoEntitySpec maltegoEntitySpec : arrmaltegoEntitySpec) {
            EntityConverter entityConverter = maltegoEntitySpec.getConverter();
            if (entityConverter == null || !entityConverter.canConvertFrom((Object)string, (PropertyDescriptor)maltegoEntitySpec.getValueProperty())) continue;
            return maltegoEntitySpec;
        }
        return null;
    }
}

