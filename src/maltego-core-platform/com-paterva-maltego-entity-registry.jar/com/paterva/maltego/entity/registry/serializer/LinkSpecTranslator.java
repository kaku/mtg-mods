/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.serializer.FieldsSerializer
 *  com.paterva.maltego.typing.serializer.PropertiesStub
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.registry.serializer.LinkSpecStub;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.serializer.FieldsSerializer;
import com.paterva.maltego.typing.serializer.PropertiesStub;
import com.paterva.maltego.util.XmlSerializationException;

class LinkSpecTranslator {
    LinkSpecTranslator() {
    }

    public MaltegoLinkSpec translate(LinkSpecStub linkSpecStub) throws TypeInstantiationException, XmlSerializationException {
        DisplayDescriptor displayDescriptor;
        DisplayDescriptor displayDescriptor2;
        DisplayDescriptor displayDescriptor3;
        FieldsSerializer fieldsSerializer = new FieldsSerializer();
        PropertyConfiguration propertyConfiguration = fieldsSerializer.readSerializationStub(linkSpecStub.Properties);
        MaltegoLinkSpec maltegoLinkSpec = new MaltegoLinkSpec(linkSpecStub.ID, propertyConfiguration);
        maltegoLinkSpec.setDescription(linkSpecStub.Description);
        maltegoLinkSpec.setDisplayName(linkSpecStub.Name);
        maltegoLinkSpec.setDisplayNamePlural(linkSpecStub.PluralName);
        maltegoLinkSpec.setDefaultCategory(linkSpecStub.Category);
        DisplayDescriptorEnumeration displayDescriptorEnumeration = propertyConfiguration.getProperties();
        DisplayDescriptor displayDescriptor4 = displayDescriptorEnumeration.get("maltego.link.show-label");
        if (displayDescriptor4 != null) {
            maltegoLinkSpec.setShowLabelProperty(displayDescriptor4);
        }
        if ((displayDescriptor3 = displayDescriptorEnumeration.get("maltego.link.color")) != null) {
            maltegoLinkSpec.setColorProperty(displayDescriptor3);
        }
        if ((displayDescriptor2 = displayDescriptorEnumeration.get("maltego.link.style")) != null) {
            maltegoLinkSpec.setStyleProperty(displayDescriptor2);
        }
        if ((displayDescriptor = displayDescriptorEnumeration.get("maltego.link.thickness")) != null) {
            maltegoLinkSpec.setThicknessProperty(displayDescriptor);
        }
        return maltegoLinkSpec;
    }

    public LinkSpecStub translate(MaltegoLinkSpec maltegoLinkSpec) throws XmlSerializationException {
        LinkSpecStub linkSpecStub = new LinkSpecStub();
        linkSpecStub.Description = maltegoLinkSpec.getDescription();
        linkSpecStub.ID = maltegoLinkSpec.getTypeName();
        linkSpecStub.Name = maltegoLinkSpec.getDisplayName();
        linkSpecStub.PluralName = maltegoLinkSpec.getDisplayNamePlural();
        linkSpecStub.Category = maltegoLinkSpec.getDefaultCategory();
        FieldsSerializer fieldsSerializer = new FieldsSerializer();
        linkSpecStub.Properties = fieldsSerializer.createSerializationStub(maltegoLinkSpec.getPropertyConfiguration());
        return linkSpecStub;
    }
}

