/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.typing.descriptor.SpecFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.typing.descriptor.SpecFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.descriptor.TypeSpec;

class RegistrySpecFactory<TSpec extends TypeSpec, TInstance extends TypedPropertyBag>
extends SpecFactory<TSpec, TInstance> {
    private SpecRegistry<TSpec> _registry;

    public RegistrySpecFactory(SpecRegistry<TSpec> specRegistry) {
        this._registry = specRegistry;
    }

    public TInstance createInstance(TSpec TSpec, boolean bl, boolean bl2) throws TypeInstantiationException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public TInstance createInstance(String string, boolean bl, boolean bl2) throws TypeInstantiationException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

