/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityConverter
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.entity.registry.converter;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityConverter;
import com.paterva.maltego.typing.PropertyDescriptor;

public abstract class AbstractEntityConverter
implements EntityConverter {
    public boolean canConvertFrom(Object object, PropertyDescriptor propertyDescriptor) {
        if (object instanceof String) {
            return true;
        }
        return false;
    }

    public boolean canConvertTo(Class class_, PropertyDescriptor propertyDescriptor) {
        return class_ == String.class;
    }

    public void setValue(String string) {
    }

    public MaltegoEntity convertFrom(Object object, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor, boolean bl) {
        if (object instanceof String) {
            maltegoEntity.setValue(propertyDescriptor, object, true, bl);
        }
        return maltegoEntity;
    }

    public Object convertTo(Class class_, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor) {
        if (class_ == String.class) {
            return maltegoEntity.getValue(propertyDescriptor);
        }
        return null;
    }
}

