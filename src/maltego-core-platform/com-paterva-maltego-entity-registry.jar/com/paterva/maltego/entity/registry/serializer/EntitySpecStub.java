/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.serializer.PropertiesStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.entity.registry.serializer.ActionStub;
import com.paterva.maltego.entity.registry.serializer.BaseEntityStub;
import com.paterva.maltego.entity.registry.serializer.ConverterStub;
import com.paterva.maltego.typing.serializer.PropertiesStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="MaltegoEntity", strict=0)
public class EntitySpecStub {
    @Attribute(name="id")
    public String ID;
    @Attribute(name="displayName")
    public String Name;
    @Attribute(name="displayNamePlural", required=0)
    public String PluralName;
    @Attribute(name="description", required=0)
    public String Description;
    @Attribute(name="category", required=0)
    public String Category;
    @Attribute(name="entityClass", required=0)
    public String ClassName;
    @Attribute(name="smallIconResource", required=0)
    public String SmallIconResource;
    @Attribute(name="largeIconResource", required=0)
    public String LargeIconResource;
    @Attribute(name="helpURL", required=0)
    public String HelpUrl;
    @Attribute(name="allowedRoot", required=0)
    public boolean IsAllowedRoot = true;
    @Attribute(name="conversionOrder", required=0)
    public String ConversionOrder;
    @Attribute(name="defaultColor", required=0)
    public String Color;
    @Attribute(name="visible", required=0)
    public boolean Visible = true;
    @Element(name="Icon", required=0)
    public String LargeIcon;
    @Element(name="SmallIcon", required=0)
    public String SmallIcon;
    @Element(name="HelpText", required=0, data=1)
    public String HelpText;
    @ElementList(name="BaseEntities", type=BaseEntityStub.class, required=0)
    public List<BaseEntityStub> BaseEntities;
    @Element(name="Converter", required=0)
    public ConverterStub Converter;
    @Element(name="Properties", required=0)
    public PropertiesStub Properties;
    @ElementList(name="Actions", type=ActionStub.class, required=0)
    public List<ActionStub> Actions;
}

