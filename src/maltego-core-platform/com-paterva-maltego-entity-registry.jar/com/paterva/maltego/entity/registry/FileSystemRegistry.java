/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntitySpecSerializer
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.typing.descriptor.ToolboxItemSpec
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.entity.api.EntitySpecSerializer;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.EmbeddedIconExtractor;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.typing.descriptor.ToolboxItemSpec;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

class FileSystemRegistry<T extends ToolboxItemSpec> {
    private String _folderName;
    private String _extension;
    private Map<String, T> _map;
    private final Object _lock = new String();
    private final EntitySpecSerializer _serializer = EntitySpecSerializer.getDefault();
    private FileObject _configRoot;

    public FileSystemRegistry(String string, String string2) {
        this(FileUtil.getConfigRoot(), string, string2);
    }

    public FileSystemRegistry(FileObject fileObject, String string, String string2) {
        this._folderName = string;
        this._extension = string2;
        this._configRoot = fileObject;
    }

    private Map<String, T> map() {
        if (this._map == null) {
            this._map = this.load();
        }
        return this._map;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public T get(String string) {
        Object object = this._lock;
        synchronized (object) {
            return (T)((ToolboxItemSpec)this.map().get(string));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<T> getAll() {
        Object object = this._lock;
        synchronized (object) {
            return this.map().values();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void add(T t, String string) {
        try {
            Object object = this._lock;
            synchronized (object) {
                this.addEntity(t, string);
                this.map().put(t.getTypeName(), t);
            }
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void remove(String string) {
        Object object = this._lock;
        synchronized (object) {
            if (this.contains(string)) {
                try {
                    this.removeEntity(string);
                    this.map().remove(string);
                }
                catch (IOException var3_3) {
                    Exceptions.printStackTrace((Throwable)var3_3);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean contains(String string) {
        Object object = this._lock;
        synchronized (object) {
            return this.map().containsKey(string);
        }
    }

    private Map<String, T> load() {
        HashMap<String, T> hashMap = new HashMap<String, T>();
        FileObject fileObject = this._configRoot.getFileObject(this._folderName);
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getFolders(false);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                for (FileObject fileObject3 : fileObject2.getChildren()) {
                    if (fileObject3.isFolder()) continue;
                    try {
                        MaltegoEntitySpec maltegoEntitySpec;
                        T t = this.loadEntity(fileObject3);
                        t.setDefaultCategory(fileObject2.getName());
                        hashMap.put(t.getTypeName(), t);
                        if (!(t instanceof MaltegoEntitySpec) || !EmbeddedIconExtractor.extract(maltegoEntitySpec = (MaltegoEntitySpec)t, IconRegistry.getDefault())) continue;
                        this.updateEntity(t);
                        continue;
                    }
                    catch (IOException var9_10) {
                        Exceptions.printStackTrace((Throwable)var9_10);
                        continue;
                    }
                    catch (TypeInstantiationException var9_11) {
                        Exceptions.printStackTrace((Throwable)var9_11);
                    }
                }
            }
        }
        return hashMap;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized T loadEntity(FileObject fileObject) throws FileNotFoundException, XmlSerializationException, TypeInstantiationException, IOException {
        InputStream inputStream = null;
        try {
            inputStream = fileObject.getInputStream();
            MaltegoEntitySpec maltegoEntitySpec = this.serializer().read(inputStream);
            return (T)maltegoEntitySpec;
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException var4_4) {
                    Exceptions.printStackTrace((Throwable)var4_4);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void addEntity(T t, String string) throws IOException, XmlSerializationException {
        FileObject fileObject = FileUtilities.getOrCreate((String)this._folderName);
        FileObject fileObject2 = FileUtilities.getOrCreate((FileObject)fileObject, (String)string);
        FileObject fileObject3 = fileObject2.getFileObject(t.getTypeName(), this._extension);
        if (fileObject3 == null) {
            fileObject3 = fileObject2.createData(t.getTypeName(), this._extension);
        }
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject3.getOutputStream());
            this.serializer().write((MaltegoEntitySpec)t, (OutputStream)bufferedOutputStream);
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateEntity(T t) throws IOException, XmlSerializationException {
        FileObject fileObject = this._configRoot.getFileObject(this._folderName);
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getChildren(true);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                if (fileObject2.isFolder() || !fileObject2.getName().equals(t.getTypeName())) continue;
                FileLock fileLock = null;
                OutputStream outputStream = null;
                try {
                    fileLock = fileObject2.lock();
                    outputStream = fileObject2.getOutputStream(fileLock);
                    this.serializer().write((MaltegoEntitySpec)t, outputStream);
                    continue;
                }
                finally {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    if (fileLock != null) {
                        fileLock.releaseLock();
                    }
                    continue;
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeEntity(String string) throws IOException {
        FileObject fileObject = this._configRoot.getFileObject(this._folderName);
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getChildren(true);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                if (fileObject2.isFolder() || !fileObject2.getName().equals(string)) continue;
                FileLock fileLock = null;
                try {
                    fileLock = fileObject2.lock();
                    fileObject2.delete(fileLock);
                    continue;
                }
                finally {
                    if (fileLock != null) {
                        fileLock.releaseLock();
                    }
                    continue;
                }
            }
        }
    }

    private EntitySpecSerializer serializer() {
        return this._serializer;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void update(T t) {
        try {
            Object object = this._lock;
            synchronized (object) {
                this.updateEntity(t);
                this.map().put(t.getTypeName(), t);
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    public String[] getFolders() {
        ArrayList<String> arrayList = new ArrayList<String>();
        FileObject fileObject = this._configRoot.getFileObject(this._folderName);
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getFolders(false);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                arrayList.add(fileObject2.getName());
            }
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }
}

