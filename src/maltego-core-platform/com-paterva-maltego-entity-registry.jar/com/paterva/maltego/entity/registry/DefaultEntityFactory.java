/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.ExtensibleEntityFactory
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.ExtensibleEntityFactory;

public class DefaultEntityFactory
extends ExtensibleEntityFactory {
    public DefaultEntityFactory() {
        super(EntityRegistry.getDefault(), null);
    }
}

