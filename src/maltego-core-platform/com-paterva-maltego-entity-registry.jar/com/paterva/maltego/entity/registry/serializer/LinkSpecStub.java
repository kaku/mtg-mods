/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.serializer.PropertiesStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.typing.serializer.PropertiesStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="MaltegoLink", strict=0)
public class LinkSpecStub {
    @Attribute(name="id")
    public String ID;
    @Attribute(name="displayName")
    public String Name;
    @Attribute(name="displayNamePlural", required=0)
    public String PluralName;
    @Attribute(name="description", required=0)
    public String Description;
    @Attribute(name="category", required=0)
    public String Category;
    @Element(name="Properties", required=0)
    public PropertiesStub Properties;
}

