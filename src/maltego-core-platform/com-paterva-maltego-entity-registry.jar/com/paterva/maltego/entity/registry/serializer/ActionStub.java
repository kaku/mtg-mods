/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.entity.registry.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Action", strict=0)
class ActionStub {
    @Attribute(name="displayName", required=1)
    public String DisplayName;
    @Attribute(name="name", required=1)
    public String Name;
    @Attribute(name="type", required=1)
    public String Type;
    @Element(name="Config", required=0, data=1)
    public String Config;

    ActionStub() {
    }
}

