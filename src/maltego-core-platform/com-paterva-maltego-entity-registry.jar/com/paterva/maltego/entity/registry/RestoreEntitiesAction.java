/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.Level1EntityRegistry;
import com.paterva.maltego.entity.registry.Level2EntityRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.awt.event.ActionEvent;
import java.util.Collection;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public class RestoreEntitiesAction
extends SystemAction {
    public String getName() {
        return "Reset Entities";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.perform(true);
    }

    public void perform(boolean bl) {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        this.copyEntities(entityRegistry, new Level1EntityRegistry(), bl);
        this.copyEntities(entityRegistry, new Level2EntityRegistry(), bl);
    }

    private void copyEntities(EntityRegistry entityRegistry, EntityRegistry entityRegistry2, boolean bl) {
        for (MaltegoEntitySpec maltegoEntitySpec : entityRegistry2.getAll()) {
            boolean bl2 = entityRegistry.contains(maltegoEntitySpec.getTypeName());
            if (!bl2) {
                entityRegistry.put((TypeSpec)maltegoEntitySpec);
                continue;
            }
            if (!bl) continue;
            entityRegistry.remove(maltegoEntitySpec.getTypeName());
            entityRegistry.put((TypeSpec)maltegoEntitySpec);
        }
    }
}

