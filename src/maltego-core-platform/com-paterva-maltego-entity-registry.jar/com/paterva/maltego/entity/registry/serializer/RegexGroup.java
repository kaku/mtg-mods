/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.entity.registry.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="RegexGroup", strict=0)
public class RegexGroup {
    @Attribute(name="property")
    public String Property;
}

