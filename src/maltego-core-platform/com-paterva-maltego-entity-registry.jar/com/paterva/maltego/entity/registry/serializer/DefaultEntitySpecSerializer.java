/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntitySpecSerializer
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.entity.api.EntitySpecSerializer;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.registry.serializer.EntitySpecStub;
import com.paterva.maltego.entity.registry.serializer.EntitySpecTranslator;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;

public class DefaultEntitySpecSerializer
extends EntitySpecSerializer {
    public void write(MaltegoEntitySpec maltegoEntitySpec, OutputStream outputStream) throws XmlSerializationException {
        EntitySpecTranslator entitySpecTranslator = new EntitySpecTranslator();
        EntitySpecStub entitySpecStub = entitySpecTranslator.translate(maltegoEntitySpec);
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)entitySpecStub, outputStream);
    }

    public MaltegoEntitySpec read(InputStream inputStream) throws XmlSerializationException, TypeInstantiationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        EntitySpecStub entitySpecStub = (EntitySpecStub)xmlSerializer.read(EntitySpecStub.class, inputStream);
        EntitySpecTranslator entitySpecTranslator = new EntitySpecTranslator();
        MaltegoEntitySpec maltegoEntitySpec = entitySpecTranslator.translate(entitySpecStub);
        return maltegoEntitySpec;
    }
}

