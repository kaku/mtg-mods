/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.LinkSpecSerializer
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.entity.registry.serializer;

import com.paterva.maltego.entity.api.LinkSpecSerializer;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.registry.serializer.LinkSpecStub;
import com.paterva.maltego.entity.registry.serializer.LinkSpecTranslator;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;

public class DefaultLinkSpecSerializer
extends LinkSpecSerializer {
    public void write(MaltegoLinkSpec maltegoLinkSpec, OutputStream outputStream) throws XmlSerializationException {
        LinkSpecTranslator linkSpecTranslator = new LinkSpecTranslator();
        LinkSpecStub linkSpecStub = linkSpecTranslator.translate(maltegoLinkSpec);
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)linkSpecStub, outputStream);
    }

    public MaltegoLinkSpec read(InputStream inputStream) throws XmlSerializationException, TypeInstantiationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        LinkSpecStub linkSpecStub = (LinkSpecStub)xmlSerializer.read(LinkSpecStub.class, inputStream);
        LinkSpecTranslator linkSpecTranslator = new LinkSpecTranslator();
        MaltegoLinkSpec maltegoLinkSpec = linkSpecTranslator.translate(linkSpecStub);
        return maltegoLinkSpec;
    }
}

