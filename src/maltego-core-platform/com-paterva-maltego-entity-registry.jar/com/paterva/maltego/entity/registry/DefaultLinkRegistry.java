/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry$Memory
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 */
package com.paterva.maltego.entity.registry;

import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;

public class DefaultLinkRegistry
extends LinkRegistry.Memory {
    public DefaultLinkRegistry() {
        this.init();
    }

    private void init() {
        this.put(MaltegoLinkSpec.getManualSpec());
        this.put(MaltegoLinkSpec.getTransformSpec());
    }
}

