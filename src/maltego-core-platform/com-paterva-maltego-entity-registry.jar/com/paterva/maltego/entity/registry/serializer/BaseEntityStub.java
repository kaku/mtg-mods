/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.entity.registry.serializer;

import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="BaseEntity", strict=0)
class BaseEntityStub {
    private String _text;

    BaseEntityStub() {
    }

    @Text
    public String getText() {
        return this._text;
    }

    @Text
    public void setText(String string) {
        this._text = string;
    }
}

