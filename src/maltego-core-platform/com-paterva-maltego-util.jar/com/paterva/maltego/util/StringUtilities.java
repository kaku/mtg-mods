/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.Base64;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.text.Segment;
import javax.swing.text.Utilities;

public class StringUtilities {
    private static final String _newline = System.getProperty("line.separator");

    private StringUtilities() {
    }

    public static String chomp(String string, int n) {
        return StringUtilities.chomp(string, n, "...");
    }

    public static String chomp(String string, int n, String string2) {
        int n2;
        if (string.length() > n && (n2 = n - string2.length()) > 0) {
            string = string.substring(0, n2);
            string = string + string2;
        }
        return string;
    }

    public static String toString(InputStream inputStream, String string) throws IOException {
        return StringUtilities.toString(inputStream, Charset.forName(string != null ? string : "UTF-8"));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String toString(InputStream inputStream, Charset charset) throws IOException {
        StringBuilder stringBuilder;
        stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset));
        try {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                stringBuilder.append(string);
                stringBuilder.append(StringUtilities.newLine());
            }
        }
        finally {
            bufferedReader.close();
        }
        return stringBuilder.toString();
    }

    public static String capitalize(String string) {
        if (StringUtilities.isNullOrEmpty(string)) {
            return string;
        }
        String string2 = string.substring(0, 1);
        return string2.toUpperCase() + string.substring(1);
    }

    public static boolean isNullString(Object object) {
        if (object == null) {
            return true;
        }
        if (object instanceof String) {
            return ((String)object).isEmpty();
        }
        return false;
    }

    public static String toCommaList(String[] arrstring) {
        StringBuilder stringBuilder = new StringBuilder();
        if (arrstring != null) {
            for (String string : arrstring) {
                stringBuilder.append(string);
            }
        }
        return stringBuilder.toString();
    }

    public static String[] fromCommaList(String string) {
        String[] arrstring = string.split(",");
        for (int i = 0; i < arrstring.length; ++i) {
            arrstring[i] = arrstring[i].trim();
        }
        return arrstring;
    }

    public static String newLine() {
        return _newline;
    }

    public static boolean areEqual(String string, String string2) {
        if (string != null) {
            return string.equals(string2);
        }
        return string2 == null;
    }

    public static boolean isEmpty(String string) {
        if (string == null) {
            return false;
        }
        return string.length() == 0;
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public static String trim(String string) {
        if (string != null) {
            string = string.trim();
        }
        return string;
    }

    public static String createUniqueString(Collection<String> collection, String string) {
        int n = 1;
        String string2 = string;
        while (collection.contains(string2)) {
            string2 = string + n;
            ++n;
        }
        return string2;
    }

    public static String createRedPostfixHtmlString(String string, String string2) {
        return "<html><body>" + string + " <font color=\"#FF0000\">" + string2 + "</font></body></html>";
    }

    public static String bytesToString(long l) {
        if (l <= 0) {
            return "0 B";
        }
        String[] arrstring = new String[]{"B", "KB", "MB", "GB", "TB"};
        int n = (int)(Math.log10(l) / Math.log10(1024.0));
        return new DecimalFormat("#,##0.#").format((double)l / Math.pow(1024.0, n)) + " " + arrstring[n];
    }

    public static String countToString(long l) {
        if (l <= 0) {
            return "0";
        }
        String[] arrstring = new String[]{"", "K", "M"};
        int n = (int)(Math.log10(l) / Math.log10(1000.0));
        return new DecimalFormat("##0.#").format((double)l / Math.pow(1000.0, n)) + arrstring[n];
    }

    public static String toStringUnsignedLong(long l, int n) {
        BigInteger bigInteger = BigInteger.valueOf(l);
        if (bigInteger.signum() < 0) {
            bigInteger = bigInteger.add(BigInteger.ONE.shiftLeft(64));
        }
        return bigInteger.toString(n);
    }

    public static long parseUnsignedLong(String string, int n) {
        BigInteger bigInteger = new BigInteger(string, n);
        long l = bigInteger.longValue();
        if (bigInteger.testBit(64)) {
            l |= Long.MIN_VALUE;
        }
        return l;
    }

    public static String replaceDigitsWithWords(String string) {
        string = string.replace("0", "zero").replace("1", "one");
        string = string.replace("2", "two").replace("3", "three");
        string = string.replace("4", "four").replace("5", "five");
        string = string.replace("6", "six").replace("7", "seven");
        string = string.replace("8", "eight").replace("9", "nine");
        return string;
    }

    public static String keepOnlyAlphaNumeric(String string) {
        return string.replaceAll("[^a-zA-Z0-9]", "");
    }

    public static String basicEncrypt(String string) throws UnsupportedEncodingException, GeneralSecurityException {
        byte[] arrby = string.getBytes("UTF-8");
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(1, (Key)StringUtilities.getBasicSecretKey(), StringUtilities.getBasicIV());
        return Base64.encodeBytes(cipher.doFinal(arrby));
    }

    public static String basicDecrypt(String string) throws IOException, GeneralSecurityException {
        byte[] arrby = Base64.decode(string);
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(2, (Key)StringUtilities.getBasicSecretKey(), StringUtilities.getBasicIV());
        return new String(cipher.doFinal(arrby), "UTF-8");
    }

    private static SecretKey getBasicSecretKey() throws UnsupportedEncodingException, GeneralSecurityException {
        DESedeKeySpec dESedeKeySpec = new DESedeKeySpec("!blUeM0nkeysE@tThoughT5!".getBytes("UTF8"));
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DESede");
        return secretKeyFactory.generateSecret(dESedeKeySpec);
    }

    private static IvParameterSpec getBasicIV() throws UnsupportedEncodingException {
        return new IvParameterSpec("q!U@mtun".getBytes("UTF-8"));
    }

    public static String listToString(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : list) {
            stringBuilder.append(string).append("|");
        }
        return stringBuilder.toString();
    }

    public static List<String> listFromString(String string) {
        ArrayList<String> arrayList = new ArrayList<String>();
        if (string.contains("|")) {
            String[] arrstring = string.split("\\|", -1);
            arrayList.addAll(Arrays.asList(arrstring));
            arrayList.remove(arrayList.size() - 1);
        }
        return arrayList;
    }

    public static String getCtrlShortcut(String string) {
        return (org.openide.util.Utilities.isMac() ? "Cmd+" : "Ctrl+") + string;
    }

    public static String getApproximateMemSize(String string) {
        return StringUtilities.bytesToString(string.length() * 2);
    }

    public static double getStringWidth(Graphics graphics, String string) {
        return StringUtilities.getStringWidth(graphics, graphics.getFont(), string);
    }

    public static double getStringWidth(Graphics graphics, Font font, String string) {
        FontMetrics fontMetrics = graphics.getFontMetrics(font);
        double d = org.openide.util.Utilities.isMac() ? (double)fontMetrics.stringWidth(string) : fontMetrics.getStringBounds(string, graphics).getWidth();
        return d;
    }

    public static String truncate(Graphics2D graphics2D, String string, double d, double d2) {
        Rectangle rectangle = new Rectangle(0, 0, (int)d, (int)d2);
        Rectangle rectangle2 = new Rectangle();
        rectangle2.setRect(rectangle);
        Rectangle rectangle3 = new Rectangle();
        rectangle3.setRect(rectangle);
        Rectangle rectangle4 = new Rectangle();
        rectangle4.setRect(rectangle);
        string = SwingUtilities.layoutCompoundLabel(graphics2D.getFontMetrics(), string, null, 1, 2, 1, 2, rectangle2, rectangle3, rectangle4, 0);
        return string;
    }

    public static String getLongestWord(String string) {
        String string2 = "";
        String string3 = "";
        int n = 0;
        boolean bl = false;
        string = string + " ";
        int n2 = string.length();
        for (int i = 0; i < n2; ++i) {
            char c = string.charAt(i);
            if (c != ' ') {
                string2 = string2 + c;
                continue;
            }
            int n3 = string2.length();
            if (n3 > n) {
                n = n3;
                string3 = string2;
            }
            string2 = "";
        }
        return string3;
    }

    public static int getWrappedLines(JTextArea jTextArea, int n) {
        String string = jTextArea.getText();
        FontMetrics fontMetrics = jTextArea.getFontMetrics(jTextArea.getFont());
        PlainDocument plainDocument = new PlainDocument();
        Segment segment = new Segment();
        try {
            plainDocument.insertString(0, string, null);
        }
        catch (BadLocationException var6_6) {
            // empty catch block
        }
        int n2 = 0;
        int n3 = 0;
        while (n3 < string.length()) {
            try {
                plainDocument.getText(n3, string.length() - n3, segment);
            }
            catch (BadLocationException var8_10) {
                // empty catch block
            }
            int n4 = 1;
            int n5 = Utilities.getBreakLocation(segment, fontMetrics, 0, n - n4, null, 0);
            n3 += n5;
            ++n2;
        }
        return n2;
    }

    public static String getShortcutKeyDelimiter() {
        String string = "-";
        String string2 = UIManager.getLookAndFeelDefaults().getString("MenuItem.acceleratorDelimiter");
        return string2 != null ? string2 : string;
    }

    public static String indent(String string, char c, char c2, String string2) {
        StringBuilder stringBuilder = new StringBuilder(string.length());
        int n = 0;
        boolean bl = true;
        for (int i = 0; i < string.length(); ++i) {
            char c3 = string.charAt(i);
            boolean bl2 = true;
            if (c3 == c) {
                if (bl && n > 0) {
                    stringBuilder.append(string2);
                    bl = false;
                }
                ++n;
            } else if (c3 == c2) {
                --n;
            } else if (c3 == '\n') {
                stringBuilder.append(c3);
                for (int j = 0; j < n - 1; ++j) {
                    stringBuilder.append(string2);
                }
                bl2 = false;
                bl = true;
            } else if (c3 == ' ') {
                bl2 = !bl;
            } else if (c3 != ' ' && bl) {
                stringBuilder.append(string2);
                bl = false;
            }
            if (!bl2) continue;
            stringBuilder.append(c3);
        }
        return stringBuilder.toString();
    }
}

