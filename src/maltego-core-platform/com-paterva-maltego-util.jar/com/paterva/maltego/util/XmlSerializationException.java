/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.io.IOException;

public class XmlSerializationException
extends IOException {
    public XmlSerializationException() {
    }

    public XmlSerializationException(String string) {
        super(string);
    }

    public XmlSerializationException(String string, Exception exception) {
        super(string, exception);
    }
}

