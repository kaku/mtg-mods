/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang.StringEscapeUtils
 */
package com.paterva.maltego.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;

public class XMLEscapeUtils {
    private XMLEscapeUtils() {
    }

    public static String escape(String string) {
        if (string == null) {
            return null;
        }
        string = XMLEscapeUtils.escapeUnicode(string);
        return StringEscapeUtils.escapeXml((String)string);
    }

    public static String unescape(String string) {
        if (string == null) {
            return null;
        }
        string = StringEscapeUtils.unescapeXml((String)string);
        return XMLEscapeUtils.unescapeUnicode(string);
    }

    public static String escapeUnicode(String string) {
        if (string == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < string.length(); ++i) {
            char c = string.charAt(i);
            if (c < ' ' || c >= '' || c == '[' || c == ']') {
                char c2 = c;
                stringBuilder.append("&#");
                stringBuilder.append((int)c2);
                stringBuilder.append(';');
                continue;
            }
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    public static String unescapeUnicode(String string) {
        if (string == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder(string.length());
        Pattern pattern = Pattern.compile("&#(\\d+);", 8);
        Matcher matcher = pattern.matcher(string);
        int n = 0;
        while (matcher.find()) {
            String string2 = string.substring(n, matcher.start());
            stringBuilder.append(string2);
            stringBuilder.append((char)Integer.parseInt(matcher.group(1)));
            n = matcher.end();
        }
        stringBuilder.append(string.substring(n));
        return stringBuilder.toString();
    }
}

