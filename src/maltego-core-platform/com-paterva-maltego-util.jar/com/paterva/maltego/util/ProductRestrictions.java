/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.net.URL;

public class ProductRestrictions {
    public static final String PATERVA_DOMAIN_REGEX = ".*\\.paterva\\.com";

    public static boolean urlAllowed(URL uRL) {
        return uRL.getHost().matches(".*\\.paterva\\.com") || uRL.getHost().matches("localhost");
    }

    public static boolean urlAllowed(String string) {
        return string.matches(".*\\.paterva\\.com.*") || string.matches(".*localhost.*");
    }

    public static boolean isGraphSaveEnabled() {
        return true;
    }

    public static int getGraphSizeLimit() {
        return 10000;
    }
}

