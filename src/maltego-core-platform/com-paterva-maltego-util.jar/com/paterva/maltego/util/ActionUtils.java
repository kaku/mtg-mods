/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.Lookups
 */
package com.paterva.maltego.util;

import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public class ActionUtils {
    public static Action getAction(String string, String string2) {
        Lookup.Template template;
        String string3 = "Actions/" + string + "/";
        Lookup lookup = Lookups.forPath((String)string3);
        Lookup.Item item = lookup.lookupItem(template = new Lookup.Template(Action.class, string3 + string2, (Object)null));
        if (item != null) {
            return (Action)item.getInstance();
        }
        return null;
    }
}

