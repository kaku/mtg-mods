/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class FileExtensionFileFilter
extends FileFilter {
    private String _description;
    private String[] _extensions;
    private String _title;

    public FileExtensionFileFilter() {
        this(null);
    }

    public FileExtensionFileFilter(String[] arrstring) {
        this(arrstring, null);
    }

    public FileExtensionFileFilter(String string, String string2) {
        this(new String[]{string}, string2);
    }

    public FileExtensionFileFilter(String[] arrstring, String string) {
        this._title = string;
        this.setExtensions(arrstring);
    }

    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        if (this.getExtensions() == null) {
            return true;
        }
        String string = file.getName();
        for (String string2 : this.getExtensions()) {
            if (!string.endsWith(string2)) continue;
            return true;
        }
        return false;
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    public String getTitle() {
        if (this._title == null) {
            return "Supported files";
        }
        return this._title;
    }

    public void setTitle(String string) {
        this._title = string;
        this.setExtensions(this._extensions);
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String[] getExtensions() {
        return this._extensions;
    }

    public void setExtensions(String[] arrstring) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.getTitle());
        stringBuffer.append(" (");
        this._extensions = arrstring;
        if (arrstring != null) {
            this._extensions = new String[arrstring.length];
            for (int i = 0; i < arrstring.length; ++i) {
                String string = arrstring[i];
                if (!string.startsWith(".")) {
                    string = "." + string;
                }
                this._extensions[i] = string;
                stringBuffer.append("*");
                stringBuffer.append(string);
                stringBuffer.append(",");
            }
        }
        this._description = stringBuffer.substring(0, stringBuffer.length() - 1) + ")";
    }
}

