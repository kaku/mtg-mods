/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.ColorUtilities;
import java.awt.Color;

public class HtmlUtils {
    public static String addSkeleton(String string) {
        return "<html>" + string + "</html>";
    }

    public static String addColor(String string, Color color) {
        return "<font color=\"" + ColorUtilities.encode(color) + "\">" + string + "</font>";
    }

    public static Color getColor(String string) {
        String string2 = string.replaceAll("\\s+", " ").toLowerCase();
        int n = string2.indexOf("font color");
        if ((n = string2.indexOf("#", n)) != -1) {
            try {
                String string3 = string2.substring(n, n + 7);
                return Color.decode(string3);
            }
            catch (IndexOutOfBoundsException | NumberFormatException var3_4) {
                // empty catch block
            }
        }
        return null;
    }

    public static String makeBold(String string) {
        return "<b>" + string + "</b>";
    }

    public static String removeSkeleton(String string) {
        string = string.replaceFirst("(?msi)^.*<html>(.*)</html>.*$", "$1");
        string = string.replaceFirst("(?msi)^.*<body>(.*)</body>.*$", "$1");
        return string.trim();
    }

    public static String addHrefs(String string) {
        string = string.replaceAll("(?mi)\\b((\\w+://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_(|])", "<a href=\"$1\">$1</a>");
        return string;
    }

    public static String removeHrefs(String string) {
        string = string.replaceAll("(?msi)<a href[^>]+>(.*?)</a>", "$1");
        return string;
    }
}

