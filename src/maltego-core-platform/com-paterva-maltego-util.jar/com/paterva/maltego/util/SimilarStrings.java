/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

public class SimilarStrings {
    private static final Object[] NO_ARGS = new Object[0];
    private String _formatString;
    private Object[] _oneArgs;
    private Object[] _twoArgs;

    public SimilarStrings(String string, Object[] arrobject, Object[] arrobject2) {
        this._formatString = string;
        this._oneArgs = arrobject;
        this._twoArgs = arrobject2;
    }

    public SimilarStrings(String string, String string2, String string3) {
        this._formatString = string;
        this._oneArgs = new Object[]{string2};
        this._twoArgs = new Object[]{string3};
    }

    public SimilarStrings(String string) {
        this._formatString = string;
        this._oneArgs = NO_ARGS;
        this._twoArgs = NO_ARGS;
    }

    public SimilarStrings(String string, String string2) {
        this._formatString = "%s";
        this._oneArgs = new Object[]{string};
        this._twoArgs = new Object[]{string2};
    }

    public SimilarStrings createInverse() {
        return new SimilarStrings(this._formatString, this._twoArgs, this._oneArgs);
    }

    public String getStringOne() {
        return String.format(this._formatString, this._oneArgs);
    }

    public String getStringTwo() {
        return String.format(this._formatString, this._twoArgs);
    }

    public String getFormatString() {
        return this._formatString;
    }

    public Object[] getOneArgs() {
        return this._oneArgs;
    }

    public Object[] getTwoArgs() {
        return this._twoArgs;
    }
}

