/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.Places
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.Version;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.spi.AbstractInterruptibleChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.Places;

public class FileUtilities {
    public static final String TYPE_IMAGE = "Image";
    public static final String TYPE_TEXT = "Text";
    public static final String TYPE_BINARY = "Binary";
    private static String ENV_ALLUSERSPROFILE = "ALLUSERSPROFILE";

    private FileUtilities() {
    }

    public static void writeTo(InputStream inputStream, File file) throws IOException {
        Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    public static void copy(File file, OutputStream outputStream) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            FileUtil.copy((InputStream)fileInputStream, (OutputStream)outputStream);
        }
        finally {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
        }
    }

    public static FileObject getOrCreate(String string) throws IOException {
        return FileUtilities.getOrCreate(FileUtil.getConfigRoot(), string);
    }

    public static FileObject getOrCreate(FileObject fileObject, String string) throws IOException {
        String[] arrstring;
        for (String string2 : arrstring = string.split("/")) {
            fileObject = FileUtilities.getOrCreateSingle(fileObject, string2);
        }
        return fileObject;
    }

    private static FileObject getOrCreateSingle(FileObject fileObject, String string) throws IOException {
        FileObject fileObject2 = fileObject.getFileObject(string);
        if (fileObject2 == null && fileObject.canWrite()) {
            fileObject2 = fileObject.createFolder(string);
        }
        return fileObject.getFileObject(string);
    }

    public static String getFileExtension(File file) {
        if (file == null) {
            return null;
        }
        return FileUtilities.getFileExtension(file.getName());
    }

    public static String getFileExtension(String string) {
        if (string == null) {
            return null;
        }
        int n = string.lastIndexOf(46);
        if (n < 0 || n >= string.length() - 1) {
            return "";
        }
        return string.substring(n + 1);
    }

    public static File getUserHome() {
        String string = System.getProperty("user.home");
        return new File(string);
    }

    public static File getAllUsersSettings() {
        if (FileUtilities.isWindows()) {
            File file = new File(FileUtilities.getEnvVar(ENV_ALLUSERSPROFILE), "Paterva");
            file = new File(file, "Maltego");
            file.mkdirs();
            file.setWritable(true, false);
            return file;
        }
        File file = Places.getUserDirectory();
        return file;
    }

    public static void windowsFullControl(File file) {
    }

    private static String getEnvVar(String string) {
        String string2 = System.getenv(string);
        if (string.equalsIgnoreCase(ENV_ALLUSERSPROFILE) && FileUtilities.isWindowsXP()) {
            string2 = string2.concat(System.getProperty("file.separator"));
            string2 = string2.concat(FileUtilities.getAppdataFolderName());
        }
        return string2;
    }

    private static boolean isWindows() {
        String string = System.getProperty("os.name");
        if (string.toLowerCase().contains("windows")) {
            return true;
        }
        return false;
    }

    private static boolean isWindowsXP() {
        String string = System.getProperty("os.name");
        if (string.contains("Windows XP")) {
            return true;
        }
        return false;
    }

    private static String getAppdataFolderName() {
        String string = System.getenv("APPDATA");
        String[] arrstring = string.split("\\" + System.getProperty("file.separator"));
        return arrstring[arrstring.length - 1];
    }

    public static FileObject createUniqueFile(FileObject fileObject, String string, String string2) throws IOException {
        FileObject fileObject2;
        String string3;
        string = string.toLowerCase();
        int n = 0;
        do {
            string3 = n > 0 ? string + n : string;
            string3 = string3 + "." + string2;
            fileObject2 = fileObject.getFileObject(string3);
            ++n;
        } while (fileObject2 != null);
        return fileObject.createData(string3);
    }

    public static String replaceIllegalChars(String string) {
        return string.replaceAll("[^A-Za-z0-9_ ]", "_");
    }

    public static String createUniqueLegalFilename(Collection<String> collection, String string) {
        string = string.toLowerCase();
        return StringUtilities.createUniqueString(collection, FileUtilities.replaceIllegalChars(string));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void copyFile(File file, File file2) throws IOException {
        long l = 0xA00000;
        if (!file2.exists()) {
            file2.createNewFile();
        }
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        AbstractInterruptibleChannel abstractInterruptibleChannel = null;
        AbstractInterruptibleChannel abstractInterruptibleChannel2 = null;
        try {
            fileInputStream = new FileInputStream(file);
            abstractInterruptibleChannel = fileInputStream.getChannel();
            fileOutputStream = new FileOutputStream(file2);
            abstractInterruptibleChannel2 = fileOutputStream.getChannel();
            long l2 = abstractInterruptibleChannel.size();
            for (long i = 0; i < l2; i += abstractInterruptibleChannel2.transferFrom((ReadableByteChannel)abstractInterruptibleChannel, (long)i, (long)0xA00000)) {
            }
        }
        finally {
            if (abstractInterruptibleChannel != null) {
                abstractInterruptibleChannel.close();
            } else if (fileInputStream != null) {
                fileInputStream.close();
            }
            if (abstractInterruptibleChannel2 != null) {
                abstractInterruptibleChannel2.close();
            } else if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
    }

    public static File getTempDir(String string) {
        return new File(FileUtilities.getMaltegoTempDir(), string);
    }

    public static File createTempDir(String string) {
        File file = FileUtilities.getTempDir(string);
        try {
            file.mkdirs();
        }
        catch (SecurityException var2_2) {
            Logger.getLogger(FileUtilities.class.getName()).log(Level.SEVERE, null, var2_2);
        }
        return file;
    }

    public static File getMaltegoTempDir() {
        String string = System.getProperty("java.io.tmpdir");
        String string2 = System.getProperty("maltego.fullversion") + Version.getBranding();
        return new File(string, "Maltego" + string2);
    }

    public static String getFileType(String string) {
        Object[] arrobject = new String[]{"csv", "htm", "html", "ini", "text", "txt"};
        Object[] arrobject2 = ImageIO.getReaderFileSuffixes();
        Arrays.sort(arrobject);
        Arrays.sort(arrobject2);
        String string2 = FileUtilities.getFileExtension(string).toLowerCase();
        if (Arrays.binarySearch(arrobject, string2) >= 0) {
            return "Text";
        }
        if (Arrays.binarySearch(arrobject2, string2) >= 0) {
            return "Image";
        }
        return "Binary";
    }

    public static boolean delete(File file) {
        if (!file.exists()) {
            return true;
        }
        FileUtilities.deleteContents(file);
        return file.delete();
    }

    public static void deleteContents(File file) {
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                if (FileUtilities.delete(file2)) continue;
                Logger.getLogger(FileUtilities.class.getName()).log(Level.SEVERE, null, "Failed to delete " + file2);
            }
        }
    }

    public static List<File> removeDirectories(List<File> list) {
        if (list == null) {
            return null;
        }
        ArrayList<File> arrayList = new ArrayList<File>();
        for (File file : list) {
            if (file.isDirectory()) continue;
            arrayList.add(file);
        }
        return arrayList;
    }

    public static File createRandomDir(File file) {
        Random random = new Random();
        File file2 = null;
        while (file2 == null || file2.exists()) {
            file2 = new File(file, Integer.toString(random.nextInt(1000000)));
        }
        file2.mkdirs();
        return file2;
    }

    public static String insertBeforeSuffix(String string, String string2) {
        String string3 = string;
        String string4 = "";
        if (string.lastIndexOf(46) >= 0) {
            string3 = string.substring(0, string.lastIndexOf(46));
            string4 = string.substring(string.lastIndexOf(46), string.length());
        }
        return string3 + string2 + string4;
    }

    public static boolean isRemoteFileURL(URL uRL) {
        if (uRL == null) {
            return false;
        }
        String string = uRL.getProtocol();
        String string2 = uRL.getHost();
        return "file".equalsIgnoreCase(string) && !StringUtilities.isNullOrEmpty(string2) && !"localhost".equals(string2) && !"127.0.0.1".equals(string2);
    }
}

