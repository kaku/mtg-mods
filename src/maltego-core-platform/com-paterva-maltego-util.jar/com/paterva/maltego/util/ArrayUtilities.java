/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

public class ArrayUtilities {
    public static boolean[] toPrimitiveArray(Boolean[] arrboolean) {
        if (arrboolean == null) {
            return null;
        }
        boolean[] arrbl = new boolean[arrboolean.length];
        for (int i = 0; i < arrboolean.length; ++i) {
            arrbl[i] = arrboolean[i];
        }
        return arrbl;
    }

    public static byte[] toPrimitiveArray(Byte[] arrbyte) {
        if (arrbyte == null) {
            return null;
        }
        byte[] arrby = new byte[arrbyte.length];
        for (int i = 0; i < arrbyte.length; ++i) {
            arrby[i] = arrbyte[i].byteValue();
        }
        return arrby;
    }

    public static char[] toPrimitiveArray(Character[] arrcharacter) {
        if (arrcharacter == null) {
            return null;
        }
        char[] arrc = new char[arrcharacter.length];
        for (int i = 0; i < arrcharacter.length; ++i) {
            arrc[i] = arrcharacter[i].charValue();
        }
        return arrc;
    }

    public static short[] toPrimitiveArray(Short[] arrshort) {
        if (arrshort == null) {
            return null;
        }
        short[] arrs = new short[arrshort.length];
        for (int i = 0; i < arrshort.length; ++i) {
            arrs[i] = arrshort[i];
        }
        return arrs;
    }

    public static int[] toPrimitiveArray(Integer[] arrinteger) {
        if (arrinteger == null) {
            return null;
        }
        int[] arrn = new int[arrinteger.length];
        for (int i = 0; i < arrinteger.length; ++i) {
            arrn[i] = arrinteger[i];
        }
        return arrn;
    }

    public static long[] toPrimitiveArray(Long[] arrlong) {
        if (arrlong == null) {
            return null;
        }
        long[] arrl = new long[arrlong.length];
        for (int i = 0; i < arrlong.length; ++i) {
            arrl[i] = arrlong[i];
        }
        return arrl;
    }

    public static double[] toPrimitiveArray(Double[] arrdouble) {
        if (arrdouble == null) {
            return null;
        }
        double[] arrd = new double[arrdouble.length];
        for (int i = 0; i < arrdouble.length; ++i) {
            arrd[i] = arrdouble[i];
        }
        return arrd;
    }

    public static float[] toPrimitiveArray(Float[] arrfloat) {
        if (arrfloat == null) {
            return null;
        }
        float[] arrf = new float[arrfloat.length];
        for (int i = 0; i < arrfloat.length; ++i) {
            arrf[i] = arrfloat[i].floatValue();
        }
        return arrf;
    }
}

