/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;

public class Installer
extends ModuleInstall {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void restored() {
        InputStream inputStream = this.getClass().getResourceAsStream("Maltego.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        finally {
            try {
                inputStream.close();
            }
            catch (IOException var3_5) {}
        }
        String string = properties.getProperty("maltego.version", "3.0.0");
        String string2 = properties.getProperty("maltego.buildnumber", "000");
        String string3 = properties.getProperty("maltego.version-subtitle", "");
        String string4 = properties.getProperty("maltego.oem", "");
        String string5 = (string + " " + string3).trim();
        if (!StringUtilities.isNullOrEmpty(string4)) {
            string5 = string5 + " (" + string4 + ")";
        }
        String string6 = string + "." + string2;
        System.setProperty("maltego.version", string);
        System.setProperty("maltego.buildnumber", string2);
        System.setProperty("maltego.fullversion", string6);
        System.setProperty("maltego.displayversion", string5);
        System.setProperty("maltego.version-subtitle", string3);
        System.setProperty("maltego.oem", string4);
        System.setProperty("netbeans.buildnumber", string5);
        Logger.getLogger(Installer.class.getName()).log(Level.INFO, "Version: {0} {1} ({2})", new Object[]{string6, string3, string4});
    }
}

