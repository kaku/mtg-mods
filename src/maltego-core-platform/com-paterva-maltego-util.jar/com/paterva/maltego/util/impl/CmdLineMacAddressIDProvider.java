/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.impl;

import com.paterva.maltego.util.MachineIDProvider;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CmdLineMacAddressIDProvider
extends MachineIDProvider {
    @Override
    public String[] getIDCandidates() {
        try {
            return this.getIDCandidatesImpl();
        }
        catch (IOException var1_1) {
            Logger.getLogger(CmdLineMacAddressIDProvider.class.getName()).log(Level.SEVERE, null, var1_1);
            return new String[0];
        }
    }

    private String[] getIDCandidatesImpl() throws IOException {
        ArrayList<String> arrayList = CmdLineMacAddressIDProvider.getInterfacesImpl();
        HashSet<String> hashSet = new HashSet<String>(arrayList);
        hashSet.remove("00-00-00-00-00-00");
        return hashSet.toArray(new String[hashSet.size()]);
    }

    private static ArrayList<String> getInterfacesImpl() throws IOException {
        String string = System.getProperty("os.name");
        ArrayList arrayList = new ArrayList();
        String[] arrstring = new String[]{"/sbin/ifconfig", "/usr/local/sbin/ifconfig", "/usr/sbin/ifconfig", "/usr/bin/ifconfig", "/bin/ifconfig", "/usr/local/bin/ifconfig"};
        if (string.toLowerCase().contains("windows")) {
            arrayList = CmdLineMacAddressIDProvider.parseInterfaces(CmdLineMacAddressIDProvider.runExt("netstat -nr"), string);
            for (int i = 0; i < arrayList.size(); ++i) {
                arrayList.set(i, ((String)arrayList.get(i)).replace(' ', '-'));
            }
        } else {
            String string2 = CmdLineMacAddressIDProvider.checkIfconfigexists(arrstring);
            if (string2.equals("no")) {
                throw new IOException("Could not find ifconfig on this system!");
            }
            String string3 = CmdLineMacAddressIDProvider.runExt(string2 + " -a");
            if (string3.contains("CMD_ERROR")) {
                arrayList = CmdLineMacAddressIDProvider.parseInterfaces(string3, string);
                if (arrayList.size() <= 0) {
                    if (string3.toUpperCase().indexOf("PERMISSION") >= 0) {
                        throw new IOException("An permission problem occured while running the command:" + string3);
                    }
                    throw new IOException("An error occured while running the command:" + string3);
                }
                for (int i = 0; i < arrayList.size(); ++i) {
                    arrayList.set(i, ((String)arrayList.get(i)).replace(':', '-'));
                }
                return arrayList;
            }
            arrayList = CmdLineMacAddressIDProvider.parseInterfaces(string3, string);
            for (int i = 0; i < arrayList.size(); ++i) {
                arrayList.set(i, ((String)arrayList.get(i)).replace(':', '-'));
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    private static ArrayList<String> parseInterfaces(String string, String string2) throws IOException {
        try {
            ArrayList<String> arrayList = new ArrayList<String>();
            string = string.toUpperCase().replaceAll("\r", "\n");
            String[] arrstring = string.split("\n");
            if (!string2.toLowerCase().contains("windows")) {
                for (String string3 : arrstring) {
                    if (string3.startsWith("PAN") || string3.contains("VMNET") || string3.contains("VIRTUALBOX") || string3.contains("LOOPBACK") || string3.contains("DUMMY")) continue;
                    Pattern pattern = Pattern.compile("\\w{2}:\\w{2}:\\w{2}:\\w{2}:\\w{2}:\\w{2}");
                    Matcher matcher = pattern.matcher(string3);
                    while (matcher.find()) {
                        String string4 = matcher.group().trim();
                        if ("00 00 00 00 00 00".equals(string4)) continue;
                        arrayList.add(string4);
                    }
                }
            } else {
                for (String string5 : arrstring) {
                    if (string5.contains("VMWARE VIRTUAL") || string5.contains("VIRTUALBOX") || string5.contains("LOOPBACK")) continue;
                    Pattern pattern = Pattern.compile("\\w{2} \\w{2} \\w{2} \\w{2} \\w{2} \\w{2}");
                    Matcher matcher = pattern.matcher(string5);
                    while (matcher.find()) {
                        arrayList.add(matcher.group());
                    }
                }
            }
            return arrayList;
        }
        catch (StringIndexOutOfBoundsException var2_3) {
            throw new IOException("It was in here" + var2_3.toString());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String runExt(String string) throws IOException {
        BufferedReader bufferedReader = null;
        BufferedReader bufferedReader2 = null;
        try {
            String string2 = null;
            String string3 = "";
            Process process = Runtime.getRuntime().exec(string);
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            bufferedReader2 = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((string2 = bufferedReader.readLine()) != null) {
                string3 = string3 + string2 + "\n";
            }
            while ((string2 = bufferedReader2.readLine()) != null) {
                string3 = string3 + "CMD_ERROR:" + string2 + "\n";
            }
            String string4 = string3;
            return string4;
        }
        finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                }
                catch (IOException var7_7) {}
            }
            if (bufferedReader2 != null) {
                try {
                    bufferedReader2.close();
                }
                catch (IOException var7_8) {}
            }
        }
    }

    private static String checkIfconfigexists(String[] arrstring) {
        for (String string : arrstring) {
            File file = new File(string);
            if (!file.exists()) continue;
            return string;
        }
        return "no";
    }
}

