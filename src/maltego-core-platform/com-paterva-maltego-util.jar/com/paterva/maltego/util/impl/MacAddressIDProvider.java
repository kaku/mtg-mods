/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.impl;

import com.paterva.maltego.util.MachineIDProvider;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MacAddressIDProvider
extends MachineIDProvider {
    private static String[] _macAddresses;

    @Override
    public synchronized String[] getIDCandidates() {
        if (_macAddresses == null) {
            _macAddresses = this.findMacAddresses();
        }
        return _macAddresses;
    }

    private String[] findMacAddresses() {
        String[] arrstring = null;
        try {
            arrstring = MacAddressIDProvider.findByHostName(true);
        }
        catch (UnknownHostException var2_2) {
            // empty catch block
        }
        if (arrstring != null && arrstring.length > 0) {
            return arrstring;
        }
        try {
            arrstring = MacAddressIDProvider.findByInterface(true);
        }
        catch (SocketException var2_3) {
            // empty catch block
        }
        if (arrstring != null && arrstring.length > 0) {
            return arrstring;
        }
        try {
            arrstring = MacAddressIDProvider.findByInterface(false);
        }
        catch (SocketException var2_4) {
            // empty catch block
        }
        if (arrstring != null && arrstring.length > 0) {
            return arrstring;
        }
        return new String[0];
    }

    private static String[] findByInterface(boolean bl) throws SocketException {
        Enumeration<NetworkInterface> enumeration = NetworkInterface.getNetworkInterfaces();
        ArrayList<String> arrayList = new ArrayList<String>();
        while (enumeration.hasMoreElements()) {
            MacAddressIDProvider.addMacAddress(enumeration.nextElement(), arrayList, bl);
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    private static String[] findByHostName(boolean bl) throws UnknownHostException {
        InetAddress[] arrinetAddress = InetAddress.getAllByName(InetAddress.getLocalHost().getHostName());
        ArrayList<String> arrayList = new ArrayList<String>();
        for (InetAddress inetAddress : arrinetAddress) {
            try {
                NetworkInterface networkInterface = NetworkInterface.getByInetAddress(inetAddress);
                MacAddressIDProvider.addMacAddress(networkInterface, arrayList, bl);
                continue;
            }
            catch (SocketException var7_8) {
                // empty catch block
            }
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    private static void addMacAddress(NetworkInterface networkInterface, List<String> list, boolean bl) throws SocketException {
        String string = null;
        if (networkInterface != null) {
            if (bl) {
                String string2 = networkInterface.getDisplayName();
                if (!(string2 == null || (string2 = string2.toLowerCase()).contains("virtual") || string2.contains("vmware") || networkInterface.isVirtual())) {
                    string = MacAddressIDProvider.getMac(networkInterface);
                }
            } else {
                string = MacAddressIDProvider.getMac(networkInterface);
            }
        }
        if (string != null && !"00-00-00-00-00-00".equals(string) && !list.contains(string)) {
            list.add(string);
        }
    }

    private static String getMac(NetworkInterface networkInterface) throws SocketException {
        byte[] arrby = networkInterface.getHardwareAddress();
        if (arrby == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrby.length; ++i) {
            Object[] arrobject = new Object[2];
            arrobject[0] = Byte.valueOf(arrby[i]);
            arrobject[1] = i < arrby.length - 1 ? "-" : "";
            stringBuffer.append(String.format("%02X%s", arrobject));
        }
        return stringBuffer.toString();
    }
}

