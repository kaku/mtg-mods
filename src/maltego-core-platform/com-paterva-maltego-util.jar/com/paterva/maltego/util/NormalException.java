/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class NormalException
extends Exception {
    private static final Logger LOGGER = Logger.getLogger(NormalException.class.getName());
    private boolean _showStackTrace = false;

    public static boolean isShowStackTrace(Throwable throwable) {
        if (throwable instanceof NormalException) {
            return ((NormalException)throwable).isShowStackTrace();
        }
        return true;
    }

    public static void showStackTrace(Throwable throwable) {
        if (NormalException.isShowStackTrace(throwable)) {
            Exceptions.printStackTrace((Throwable)throwable);
        } else {
            NormalException.logStackTrace(throwable);
        }
    }

    public static void logStackTrace(Throwable throwable) {
        LOGGER.log(Level.INFO, throwable.getMessage(), throwable);
    }

    public NormalException(boolean bl) {
        this._showStackTrace = bl;
    }

    public NormalException(NormalException normalException) {
        this(normalException, normalException.isShowStackTrace());
    }

    public NormalException(String string, boolean bl) {
        super(string);
        this._showStackTrace = bl;
    }

    public NormalException(String string, Throwable throwable, boolean bl) {
        super(string, throwable);
        this._showStackTrace = bl;
    }

    public NormalException(Throwable throwable, boolean bl) {
        super(throwable);
        this._showStackTrace = bl;
    }

    public boolean isShowStackTrace() {
        return this._showStackTrace;
    }
}

