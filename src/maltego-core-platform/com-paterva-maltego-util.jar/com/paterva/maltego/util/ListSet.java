/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class ListSet<T>
extends AbstractSet<T> {
    private LinkedList<T> _list = new LinkedList();

    public ListSet() {
    }

    public ListSet(Collection<T> collection) {
        AbstractSet.super.addAll(collection);
    }

    @Override
    public boolean add(T t) {
        if (!this._list.contains(t)) {
            this._list.add(t);
            return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return this._list.iterator();
    }

    @Override
    public int size() {
        return this._list.size();
    }
}

