/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class SmartExceptionFilterLogger {
    private static final long RESET_INTERVAL = 600000;
    private static final long LOG_MSG_INTERVAL = 10000;
    private static SmartExceptionFilterLogger _instance;
    private final Map<String, LogData> _exceptionsLogged = new HashMap<String, LogData>();
    private long _lastListResetTime = 0;

    public static synchronized SmartExceptionFilterLogger getDefault() {
        if (_instance == null) {
            _instance = new SmartExceptionFilterLogger();
        }
        return _instance;
    }

    public void log(Class class_, Exception exception) {
        LogData logData;
        String string;
        long l = System.currentTimeMillis();
        if (l - this._lastListResetTime > 600000) {
            this._exceptionsLogged.clear();
            this._lastListResetTime = l;
        }
        if ((logData = this._exceptionsLogged.get(string = exception.toString())) == null) {
            Exceptions.printStackTrace((Throwable)exception);
            this._exceptionsLogged.put(string, new LogData(l));
        } else {
            int n = logData.getCount();
            if (l - logData.getLastLogTime() > 10000) {
                if (n > 1) {
                    string = string + " (" + n + ")";
                }
                Logger.getLogger(class_.getName()).severe(string);
                logData.setCount(0);
                logData.setLastLogTime(l);
            } else {
                logData.setCount(n + 1);
            }
        }
    }

    private static class LogData {
        private long _lastLogTime = 0;
        private int _count = 0;

        public LogData(long l) {
            this._lastLogTime = l;
        }

        public long getLastLogTime() {
            return this._lastLogTime;
        }

        public void setLastLogTime(long l) {
            this._lastLogTime = l;
        }

        public void setCount(int n) {
            this._count = n;
        }

        public int getCount() {
            return this._count;
        }
    }

}

