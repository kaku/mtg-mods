/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.FileExtensionFileFilter;

public class ImageFileFilter
extends FileExtensionFileFilter {
    public ImageFileFilter() {
        super(new String[]{".png", ".gif", ".jpg", ".jpeg", ".tif", ".bmp"});
        this.setDescription("Image files (*.png, *.gif, *.jpg, *.tif, *.bmp)");
    }
}

