/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 */
package com.paterva.maltego.util;

import org.openide.awt.StatusDisplayer;

public class BulkStatusDisplayer {
    private final String _msg;
    private int _count = 0;
    private long _lastUpdate = 0;

    public BulkStatusDisplayer(String string) {
        this._msg = string;
    }

    public void increment() {
        ++this._count;
        long l = System.currentTimeMillis();
        if (l - this._lastUpdate > 100) {
            StatusDisplayer.getDefault().setStatusText(String.format(this._msg, this._count));
            this._lastUpdate = l;
        }
    }

    public void clear() {
        StatusDisplayer.getDefault().setStatusText("");
    }
}

