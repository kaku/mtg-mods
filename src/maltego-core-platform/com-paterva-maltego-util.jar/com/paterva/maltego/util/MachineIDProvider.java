/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public abstract class MachineIDProvider {
    private static MachineIDProvider _provider;

    public static synchronized MachineIDProvider getDefault() {
        if (_provider == null) {
            MachineIDProvider machineIDProvider = (MachineIDProvider)Lookup.getDefault().lookup(MachineIDProvider.class);
            if (machineIDProvider == null) {
                machineIDProvider = new TrivialMachineIDProvider();
            }
            _provider = new Persisted(machineIDProvider);
        }
        return _provider;
    }

    public String getUniqueID() {
        String string = null;
        String[] arrstring = this.getIDCandidates();
        if (arrstring != null && arrstring.length > 0) {
            string = arrstring[0];
        }
        return string;
    }

    public abstract String[] getIDCandidates();

    private static class Persisted
    extends MachineIDProvider {
        private static final String PREF_MAC_ID = "maltego.mac.id";
        private static final String PREF_MAC_ID_COUNT = "maltego.mac.id.count";
        private final MachineIDProvider _delegate;

        public Persisted(MachineIDProvider machineIDProvider) {
            this._delegate = machineIDProvider;
        }

        @Override
        public synchronized String[] getIDCandidates() {
            String[] arrstring = this.loadIDs();
            if (arrstring == null || arrstring.length == 0) {
                arrstring = this._delegate.getIDCandidates();
                if (arrstring != null && arrstring.length > 0) {
                    this.persistIDs(arrstring);
                } else {
                    Logger.getLogger(Persisted.class.getName()).log(Level.WARNING, "No machine ID found.");
                }
            }
            return arrstring;
        }

        private String[] loadIDs() {
            Preferences preferences = this.getPrefs();
            int n = preferences.getInt("maltego.mac.id.count", 0);
            ArrayList<String> arrayList = new ArrayList<String>();
            for (int i = 0; i < n; ++i) {
                String string = preferences.get(Persisted.getIdPrefStr(i), null);
                if (string == null) continue;
                arrayList.add(string);
            }
            return arrayList.toArray(new String[arrayList.size()]);
        }

        private void persistIDs(String[] arrstring) {
            Preferences preferences = this.getPrefs();
            preferences.putInt("maltego.mac.id.count", arrstring.length);
            for (int i = 0; i < arrstring.length; ++i) {
                String string = arrstring[i];
                preferences.put(Persisted.getIdPrefStr(i), string);
            }
        }

        private static String getIdPrefStr(int n) {
            return "maltego.mac.id." + n;
        }

        private Preferences getPrefs() {
            return NbPreferences.forModule(this.getClass());
        }
    }

    private static class TrivialMachineIDProvider
    extends MachineIDProvider {
        private TrivialMachineIDProvider() {
        }

        @Override
        public String[] getIDCandidates() {
            throw new UnsupportedOperationException("No MachineIDProvider registered.");
        }
    }

}

