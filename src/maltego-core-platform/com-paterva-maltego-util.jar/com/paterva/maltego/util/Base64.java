/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Base64 {
    public static final int NO_OPTIONS = 0;
    public static final int ENCODE = 1;
    public static final int DECODE = 0;
    public static final int GZIP = 2;
    public static final int DO_BREAK_LINES = 8;
    public static final int URL_SAFE = 16;
    public static final int ORDERED = 32;
    private static final int MAX_LINE_LENGTH = 76;
    private static final byte EQUALS_SIGN = 61;
    private static final byte NEW_LINE = 10;
    private static final String PREFERRED_ENCODING = "UTF-8";
    private static final byte WHITE_SPACE_ENC = -5;
    private static final byte EQUALS_SIGN_ENC = -1;
    private static final byte[] _STANDARD_ALPHABET = new byte[]{65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] _STANDARD_DECODABET = new byte[]{-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    private static final byte[] _URL_SAFE_ALPHABET = new byte[]{65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] _URL_SAFE_DECODABET = new byte[]{-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    private static final byte[] _ORDERED_ALPHABET = new byte[]{45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] _ORDERED_DECODABET = new byte[]{-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, -9, -9, -9, -9};

    private static final byte[] getAlphabet(int n) {
        if ((n & 16) == 16) {
            return _URL_SAFE_ALPHABET;
        }
        if ((n & 32) == 32) {
            return _ORDERED_ALPHABET;
        }
        return _STANDARD_ALPHABET;
    }

    private static final byte[] getDecodabet(int n) {
        if ((n & 16) == 16) {
            return _URL_SAFE_DECODABET;
        }
        if ((n & 32) == 32) {
            return _ORDERED_DECODABET;
        }
        return _STANDARD_DECODABET;
    }

    private Base64() {
    }

    private static byte[] encode3to4(byte[] arrby, byte[] arrby2, int n, int n2) {
        Base64.encode3to4(arrby2, 0, n, arrby, 0, n2);
        return arrby;
    }

    private static byte[] encode3to4(byte[] arrby, int n, int n2, byte[] arrby2, int n3, int n4) {
        byte[] arrby3 = Base64.getAlphabet(n4);
        int n5 = (n2 > 0 ? arrby[n] << 24 >>> 8 : 0) | (n2 > 1 ? arrby[n + 1] << 24 >>> 16 : 0) | (n2 > 2 ? arrby[n + 2] << 24 >>> 24 : 0);
        switch (n2) {
            case 3: {
                arrby2[n3] = arrby3[n5 >>> 18];
                arrby2[n3 + 1] = arrby3[n5 >>> 12 & 63];
                arrby2[n3 + 2] = arrby3[n5 >>> 6 & 63];
                arrby2[n3 + 3] = arrby3[n5 & 63];
                return arrby2;
            }
            case 2: {
                arrby2[n3] = arrby3[n5 >>> 18];
                arrby2[n3 + 1] = arrby3[n5 >>> 12 & 63];
                arrby2[n3 + 2] = arrby3[n5 >>> 6 & 63];
                arrby2[n3 + 3] = 61;
                return arrby2;
            }
            case 1: {
                arrby2[n3] = arrby3[n5 >>> 18];
                arrby2[n3 + 1] = arrby3[n5 >>> 12 & 63];
                arrby2[n3 + 2] = 61;
                arrby2[n3 + 3] = 61;
                return arrby2;
            }
        }
        return arrby2;
    }

    public static void encode(ByteBuffer byteBuffer, ByteBuffer byteBuffer2) {
        byte[] arrby = new byte[3];
        byte[] arrby2 = new byte[4];
        while (byteBuffer.hasRemaining()) {
            int n = Math.min(3, byteBuffer.remaining());
            byteBuffer.get(arrby, 0, n);
            Base64.encode3to4(arrby2, arrby, n, 0);
            byteBuffer2.put(arrby2);
        }
    }

    public static void encode(ByteBuffer byteBuffer, CharBuffer charBuffer) {
        byte[] arrby = new byte[3];
        byte[] arrby2 = new byte[4];
        while (byteBuffer.hasRemaining()) {
            int n = Math.min(3, byteBuffer.remaining());
            byteBuffer.get(arrby, 0, n);
            Base64.encode3to4(arrby2, arrby, n, 0);
            for (int i = 0; i < 4; ++i) {
                charBuffer.put((char)(arrby2[i] & 255));
            }
        }
    }

    public static String encodeObject(Serializable serializable) throws IOException {
        return Base64.encodeObject(serializable, 0);
    }

    public static String encodeObject(Serializable serializable, int n) throws IOException {
        if (serializable == null) {
            throw new NullPointerException("Cannot serialize a null object.");
        }
        ByteArrayOutputStream byteArrayOutputStream = null;
        java.io.OutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            outputStream = new OutputStream(byteArrayOutputStream, 1 | n);
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(serializable);
        }
        catch (IOException var5_8) {
            throw var5_8;
        }
        finally {
            try {
                objectOutputStream.close();
            }
            catch (Exception var7_11) {}
            try {
                outputStream.close();
            }
            catch (Exception var7_12) {}
            try {
                byteArrayOutputStream.close();
            }
            catch (Exception var7_13) {}
        }
        try {
            return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
        }
        catch (UnsupportedEncodingException var5_9) {
            return new String(byteArrayOutputStream.toByteArray());
        }
    }

    public static String encodeBytes(byte[] arrby) {
        String string;
        block3 : {
            string = null;
            try {
                string = Base64.encodeBytes(arrby, 0, arrby.length, 0);
            }
            catch (IOException var2_2) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError((Object)var2_2.getMessage());
            }
        }
        assert (string != null);
        return string;
    }

    public static String encodeBytes(byte[] arrby, int n) throws IOException {
        return Base64.encodeBytes(arrby, 0, arrby.length, n);
    }

    public static String encodeBytes(byte[] arrby, int n, int n2) {
        String string;
        block3 : {
            string = null;
            try {
                string = Base64.encodeBytes(arrby, n, n2, 0);
            }
            catch (IOException var4_4) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError((Object)var4_4.getMessage());
            }
        }
        assert (string != null);
        return string;
    }

    public static String encodeBytes(byte[] arrby, int n, int n2, int n3) throws IOException {
        byte[] arrby2 = Base64.encodeBytesToBytes(arrby, n, n2, n3);
        try {
            return new String(arrby2, "UTF-8");
        }
        catch (UnsupportedEncodingException var5_5) {
            return new String(arrby2);
        }
    }

    public static byte[] encodeBytesToBytes(byte[] arrby) {
        byte[] arrby2;
        block2 : {
            arrby2 = null;
            try {
                arrby2 = Base64.encodeBytesToBytes(arrby, 0, arrby.length, 0);
            }
            catch (IOException var2_2) {
                if ($assertionsDisabled) break block2;
                throw new AssertionError((Object)("IOExceptions only come from GZipping, which is turned off: " + var2_2.getMessage()));
            }
        }
        return arrby2;
    }

    public static byte[] encodeBytesToBytes(byte[] arrby, int n, int n2, int n3) throws IOException {
        if (arrby == null) {
            throw new NullPointerException("Cannot serialize a null array.");
        }
        if (n < 0) {
            throw new IllegalArgumentException("Cannot have negative offset: " + n);
        }
        if (n2 < 0) {
            throw new IllegalArgumentException("Cannot have length offset: " + n2);
        }
        if (n + n2 > arrby.length) {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", n, n2, arrby.length));
        }
        if ((n3 & 2) > 0) {
            ByteArrayOutputStream byteArrayOutputStream = null;
            DeflaterOutputStream deflaterOutputStream = null;
            OutputStream outputStream = null;
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                outputStream = new OutputStream(byteArrayOutputStream, 1 | n3);
                deflaterOutputStream = new GZIPOutputStream(outputStream);
                deflaterOutputStream.write(arrby, n, n2);
                deflaterOutputStream.close();
            }
            catch (IOException var7_13) {
                throw var7_13;
            }
            finally {
                try {
                    deflaterOutputStream.close();
                }
                catch (Exception var9_17) {}
                try {
                    outputStream.close();
                }
                catch (Exception var9_18) {}
                try {
                    byteArrayOutputStream.close();
                }
                catch (Exception var9_19) {}
            }
            return byteArrayOutputStream.toByteArray();
        }
        boolean bl = (n3 & 8) > 0;
        int n4 = n2 * 4 / 3;
        byte[] arrby2 = new byte[n4 + (n2 % 3 > 0 ? 4 : 0) + (bl ? n4 / 76 : 0)];
        int n5 = 0;
        int n6 = 0;
        int n7 = n2 - 2;
        int n8 = 0;
        while (n5 < n7) {
            Base64.encode3to4(arrby, n5 + n, 3, arrby2, n6, n3);
            if (bl && (n8 += 4) == 76) {
                arrby2[n6 + 4] = 10;
                ++n6;
                n8 = 0;
            }
            n5 += 3;
            n6 += 4;
        }
        if (n5 < n2) {
            Base64.encode3to4(arrby, n5 + n, n2 - n5, arrby2, n6, n3);
            n6 += 4;
        }
        byte[] arrby3 = new byte[n6];
        System.arraycopy(arrby2, 0, arrby3, 0, n6);
        return arrby3;
    }

    private static int decode4to3(byte[] arrby, int n, byte[] arrby2, int n2, int n3) {
        if (arrby == null) {
            throw new NullPointerException("Source array was null.");
        }
        if (arrby2 == null) {
            throw new NullPointerException("Destination array was null.");
        }
        if (n < 0 || n + 3 >= arrby.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", arrby.length, n));
        }
        if (n2 < 0 || n2 + 2 >= arrby2.length) {
            throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", arrby2.length, n2));
        }
        byte[] arrby3 = Base64.getDecodabet(n3);
        if (arrby[n + 2] == 61) {
            int n4 = (arrby3[arrby[n]] & 255) << 18 | (arrby3[arrby[n + 1]] & 255) << 12;
            arrby2[n2] = (byte)(n4 >>> 16);
            return 1;
        }
        if (arrby[n + 3] == 61) {
            int n5 = (arrby3[arrby[n]] & 255) << 18 | (arrby3[arrby[n + 1]] & 255) << 12 | (arrby3[arrby[n + 2]] & 255) << 6;
            arrby2[n2] = (byte)(n5 >>> 16);
            arrby2[n2 + 1] = (byte)(n5 >>> 8);
            return 2;
        }
        int n6 = (arrby3[arrby[n]] & 255) << 18 | (arrby3[arrby[n + 1]] & 255) << 12 | (arrby3[arrby[n + 2]] & 255) << 6 | arrby3[arrby[n + 3]] & 255;
        arrby2[n2] = (byte)(n6 >> 16);
        arrby2[n2 + 1] = (byte)(n6 >> 8);
        arrby2[n2 + 2] = (byte)n6;
        return 3;
    }

    public static byte[] decode(byte[] arrby) {
        byte[] arrby2;
        block2 : {
            arrby2 = null;
            try {
                arrby2 = Base64.decode(arrby, 0, arrby.length, 0);
            }
            catch (IOException var2_2) {
                if ($assertionsDisabled) break block2;
                throw new AssertionError((Object)("IOExceptions only come from GZipping, which is turned off: " + var2_2.getMessage()));
            }
        }
        return arrby2;
    }

    public static byte[] decode(byte[] arrby, int n, int n2, int n3) throws IOException {
        if (arrby == null) {
            throw new NullPointerException("Cannot decode null source array.");
        }
        if (n < 0 || n + n2 > arrby.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and process %d bytes.", arrby.length, n, n2));
        }
        if (n2 == 0) {
            return new byte[0];
        }
        if (n2 < 4) {
            throw new IllegalArgumentException("Base64-encoded string must have at least four characters, but length specified was " + n2);
        }
        byte[] arrby2 = Base64.getDecodabet(n3);
        int n4 = n2 * 3 / 4;
        byte[] arrby3 = new byte[n4];
        int n5 = 0;
        byte[] arrby4 = new byte[4];
        int n6 = 0;
        int n7 = 0;
        byte by = 0;
        byte by2 = 0;
        for (n7 = n; n7 < n + n2; ++n7) {
            by = (byte)(arrby[n7] & 127);
            by2 = arrby2[by];
            if (by2 >= -5) {
                if (by2 < -1) continue;
                arrby4[n6++] = by;
                if (n6 <= 3) continue;
                n5 += Base64.decode4to3(arrby4, 0, arrby3, n5, n3);
                n6 = 0;
                if (by != 61) continue;
                break;
            }
            throw new IOException(String.format("Bad Base64 input character '%c' in array position %d", Byte.valueOf(arrby[n7]), n7));
        }
        byte[] arrby5 = new byte[n5];
        System.arraycopy(arrby3, 0, arrby5, 0, n5);
        return arrby5;
    }

    public static byte[] decode(String string) throws IOException {
        return Base64.decode(string, 0);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public static byte[] decode(String string, int n) throws IOException {
        byte[] arrby;
        if (string == null) {
            throw new NullPointerException("Input string was null.");
        }
        try {
            arrby = string.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException var3_3) {
            arrby = string.getBytes();
        }
        arrby = Base64.decode(arrby, 0, arrby.length, n);
        if (arrby == null) return arrby;
        if (arrby.length < 4) return arrby;
        int n2 = arrby[0] & 255 | arrby[1] << 8 & 65280;
        if (35615 != n2) return arrby;
        ByteArrayInputStream byteArrayInputStream = null;
        GZIPInputStream gZIPInputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        byte[] arrby2 = new byte[2048];
        int n3 = 0;
        byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayInputStream = new ByteArrayInputStream(arrby);
        gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        while ((n3 = gZIPInputStream.read(arrby2)) >= 0) {
            byteArrayOutputStream.write(arrby2, 0, n3);
        }
        arrby = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        }
        catch (Exception var9_10) {
            // empty catch block
        }
        try {
            gZIPInputStream.close();
        }
        catch (Exception var9_11) {
            // empty catch block
        }
        try {
            byteArrayInputStream.close();
            return arrby;
        }
        catch (Exception var9_12) {
            return arrby;
        }
        catch (IOException iOException) {
            try {
                byteArrayOutputStream.close();
            }
            catch (Exception var9_14) {
                // empty catch block
            }
            try {
                gZIPInputStream.close();
            }
            catch (Exception var9_15) {
                // empty catch block
            }
            try {
                byteArrayInputStream.close();
                return arrby;
            }
            catch (Exception var9_16) {
                return arrby;
            }
            catch (Throwable throwable) {
                try {
                    byteArrayOutputStream.close();
                }
                catch (Exception var11_18) {
                    // empty catch block
                }
                try {
                    gZIPInputStream.close();
                }
                catch (Exception var11_19) {
                    // empty catch block
                }
                try {
                    byteArrayInputStream.close();
                    throw throwable;
                }
                catch (Exception var11_20) {
                    // empty catch block
                }
                throw throwable;
            }
        }
    }

    public static Object decodeToObject(String string) throws IOException, ClassNotFoundException {
        byte[] arrby = Base64.decode(string);
        ByteArrayInputStream byteArrayInputStream = null;
        ObjectInputStream objectInputStream = null;
        Object object = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(arrby);
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (IOException var5_7) {
            throw var5_7;
        }
        catch (ClassNotFoundException var5_8) {
            throw var5_8;
        }
        finally {
            try {
                byteArrayInputStream.close();
            }
            catch (Exception var7_10) {}
            try {
                objectInputStream.close();
            }
            catch (Exception var7_11) {}
        }
        return object;
    }

    public static void encodeToFile(byte[] arrby, String string) throws IOException {
        if (arrby == null) {
            throw new NullPointerException("Data to encode was null.");
        }
        OutputStream outputStream = null;
        try {
            outputStream = new OutputStream(new FileOutputStream(string), 1);
            outputStream.write(arrby);
        }
        catch (IOException var3_4) {
            throw var3_4;
        }
        finally {
            try {
                outputStream.close();
            }
            catch (Exception var5_6) {}
        }
    }

    public static void decodeToFile(String string, String string2) throws IOException {
        OutputStream outputStream = null;
        try {
            outputStream = new OutputStream(new FileOutputStream(string2), 0);
            outputStream.write(string.getBytes("UTF-8"));
        }
        catch (IOException var3_4) {
            throw var3_4;
        }
        finally {
            try {
                outputStream.close();
            }
            catch (Exception var5_6) {}
        }
    }

    public static byte[] decodeFromFile(String string) throws IOException {
        byte[] arrby = null;
        FilterInputStream filterInputStream = null;
        try {
            File file = new File(string);
            byte[] arrby2 = null;
            int n = 0;
            int n2 = 0;
            if (file.length() > Integer.MAX_VALUE) {
                throw new IOException("File is too big for this convenience method (" + file.length() + " bytes).");
            }
            arrby2 = new byte[(int)file.length()];
            filterInputStream = new InputStream(new BufferedInputStream(new FileInputStream(file)), 0);
            while ((n2 = filterInputStream.read(arrby2, n, 4096)) >= 0) {
                n += n2;
            }
            arrby = new byte[n];
            System.arraycopy(arrby2, 0, arrby, 0, n);
        }
        catch (IOException var3_5) {
            throw var3_5;
        }
        finally {
            try {
                filterInputStream.close();
            }
            catch (Exception var8_10) {}
        }
        return arrby;
    }

    public static String encodeFromFile(String string) throws IOException {
        String string2 = null;
        FilterInputStream filterInputStream = null;
        try {
            File file = new File(string);
            byte[] arrby = new byte[Math.max((int)((double)file.length() * 1.4), 40)];
            int n = 0;
            int n2 = 0;
            filterInputStream = new InputStream(new BufferedInputStream(new FileInputStream(file)), 1);
            while ((n2 = filterInputStream.read(arrby, n, 4096)) >= 0) {
                n += n2;
            }
            string2 = new String(arrby, 0, n, "UTF-8");
        }
        catch (IOException var3_5) {
            throw var3_5;
        }
        finally {
            try {
                filterInputStream.close();
            }
            catch (Exception var8_10) {}
        }
        return string2;
    }

    public static void encodeFileToFile(String string, String string2) throws IOException {
        String string3 = Base64.encodeFromFile(string);
        java.io.OutputStream outputStream = null;
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(string2));
            outputStream.write(string3.getBytes("US-ASCII"));
        }
        catch (IOException var4_5) {
            throw var4_5;
        }
        finally {
            try {
                outputStream.close();
            }
            catch (Exception var6_7) {}
        }
    }

    public static void decodeFileToFile(String string, String string2) throws IOException {
        byte[] arrby = Base64.decodeFromFile(string);
        java.io.OutputStream outputStream = null;
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(string2));
            outputStream.write(arrby);
        }
        catch (IOException var4_5) {
            throw var4_5;
        }
        finally {
            try {
                outputStream.close();
            }
            catch (Exception var6_7) {}
        }
    }

    public static class OutputStream
    extends FilterOutputStream {
        private boolean encode;
        private int position;
        private byte[] buffer;
        private int bufferLength;
        private int lineLength;
        private boolean breakLines;
        private byte[] b4;
        private boolean suspendEncoding;
        private int options;
        private byte[] alphabet;
        private byte[] decodabet;

        public OutputStream(java.io.OutputStream outputStream) {
            this(outputStream, 1);
        }

        public OutputStream(java.io.OutputStream outputStream, int n) {
            super(outputStream);
            this.breakLines = (n & 8) > 0;
            this.encode = (n & 1) > 0;
            this.bufferLength = this.encode ? 3 : 4;
            this.buffer = new byte[this.bufferLength];
            this.position = 0;
            this.lineLength = 0;
            this.suspendEncoding = false;
            this.b4 = new byte[4];
            this.options = n;
            this.alphabet = Base64.getAlphabet(n);
            this.decodabet = Base64.getDecodabet(n);
        }

        @Override
        public void write(int n) throws IOException {
            if (this.suspendEncoding) {
                this.out.write(n);
                return;
            }
            if (this.encode) {
                this.buffer[this.position++] = (byte)n;
                if (this.position >= this.bufferLength) {
                    this.out.write(Base64.encode3to4(this.b4, this.buffer, this.bufferLength, this.options));
                    this.lineLength += 4;
                    if (this.breakLines && this.lineLength >= 76) {
                        this.out.write(10);
                        this.lineLength = 0;
                    }
                    this.position = 0;
                }
            } else if (this.decodabet[n & 127] > -5) {
                this.buffer[this.position++] = (byte)n;
                if (this.position >= this.bufferLength) {
                    int n2 = Base64.decode4to3(this.buffer, 0, this.b4, 0, this.options);
                    this.out.write(this.b4, 0, n2);
                    this.position = 0;
                }
            } else if (this.decodabet[n & 127] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        @Override
        public void write(byte[] arrby, int n, int n2) throws IOException {
            if (this.suspendEncoding) {
                this.out.write(arrby, n, n2);
                return;
            }
            for (int i = 0; i < n2; ++i) {
                this.write(arrby[n + i]);
            }
        }

        public void flushBase64() throws IOException {
            if (this.position > 0) {
                if (this.encode) {
                    this.out.write(Base64.encode3to4(this.b4, this.buffer, this.position, this.options));
                    this.position = 0;
                } else {
                    throw new IOException("Base64 input not properly padded.");
                }
            }
        }

        @Override
        public void flush() throws IOException {
            this.flushBase64();
            super.flush();
        }

        @Override
        public void close() throws IOException {
            this.flush();
            super.close();
            this.buffer = null;
            this.out = null;
        }

        public void suspendEncoding() throws IOException {
            this.flushBase64();
            this.suspendEncoding = true;
        }

        public void resumeEncoding() {
            this.suspendEncoding = false;
        }
    }

    public static class InputStream
    extends FilterInputStream {
        private boolean encode;
        private int position;
        private byte[] buffer;
        private int bufferLength;
        private int numSigBytes;
        private int lineLength;
        private boolean breakLines;
        private int options;
        private byte[] alphabet;
        private byte[] decodabet;

        public InputStream(java.io.InputStream inputStream) {
            this(inputStream, 0);
        }

        public InputStream(java.io.InputStream inputStream, int n) {
            super(inputStream);
            this.options = n;
            this.breakLines = (n & 8) > 0;
            this.encode = (n & 1) > 0;
            this.bufferLength = this.encode ? 4 : 3;
            this.buffer = new byte[this.bufferLength];
            this.position = -1;
            this.lineLength = 0;
            this.alphabet = Base64.getAlphabet(n);
            this.decodabet = Base64.getDecodabet(n);
        }

        /*
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @Override
        public int read() throws IOException {
            if (this.position < 0) {
                byte[] arrby;
                if (this.encode) {
                    int n;
                    arrby = new byte[3];
                    int n2 = 0;
                    for (int i = 0; i < 3 && (n = this.in.read()) >= 0; ++i) {
                        arrby[i] = (byte)n;
                        ++n2;
                    }
                    if (n2 <= 0) return -1;
                    Base64.encode3to4(arrby, 0, n2, this.buffer, 0, this.options);
                    this.position = 0;
                    this.numSigBytes = 4;
                } else {
                    int n;
                    block11 : {
                        arrby = new byte[4];
                        n = 0;
                        n = 0;
                        while (n < 4) {
                            int n3 = 0;
                            while ((n3 = this.in.read()) >= 0 && this.decodabet[n3 & 127] <= -5) {
                            }
                            if (n3 >= 0) {
                                arrby[n] = (byte)n3;
                                ++n;
                                continue;
                            }
                            break block11;
                        }
                        return -1;
                    }
                    if (n == 4) {
                        this.numSigBytes = Base64.decode4to3(arrby, 0, this.buffer, 0, this.options);
                        this.position = 0;
                    } else {
                        if (n != 0) throw new IOException("Improperly padded Base64 input.");
                        return -1;
                    }
                }
            }
            if (this.position < 0) throw new IOException("Error in Base64 code reading stream.");
            if (this.position >= this.numSigBytes) {
                return -1;
            }
            if (this.encode && this.breakLines && this.lineLength >= 76) {
                this.lineLength = 0;
                return 10;
            }
            ++this.lineLength;
            byte by = this.buffer[this.position++];
            if (this.position < this.bufferLength) return by & 255;
            this.position = -1;
            return by & 255;
        }

        @Override
        public int read(byte[] arrby, int n, int n2) throws IOException {
            int n3;
            for (n3 = 0; n3 < n2; ++n3) {
                int n4 = this.read();
                if (n4 < 0) {
                    if (n3 != 0) break;
                    return -1;
                }
                arrby[n + n3] = (byte)n4;
            }
            return n3;
        }
    }

}

