/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class FileStore {
    private static FileStore _instance;

    public static FileStore getDefault() {
        if (_instance == null && (FileStore._instance = (FileStore)Lookup.getDefault().lookup(FileStore.class)) == null) {
            _instance = new DefaultFileStore();
            _instance.clear();
        }
        return _instance;
    }

    public abstract int add(File var1) throws IOException;

    public abstract int add(InputStream var1, String var2) throws IOException;

    public abstract File get(int var1) throws FileNotFoundException;

    public abstract long getSize(int var1);

    public abstract void remove(int var1);

    public abstract void clear();

    public abstract String getRelativePath(int var1) throws FileNotFoundException;

    public static class DefaultFileStore
    extends FileStore {
        private final Map<Integer, File> _files = new HashMap<Integer, File>();
        private final File _tempDir = FileUtilities.getTempDir("Files");
        private int _counter = 0;

        @Override
        public int add(File file) throws IOException {
            File file2 = this.getUniqueTempPath(file.getName());
            FileUtilities.copyFile(file, file2);
            this._files.put(++this._counter, file2);
            return this._counter;
        }

        @Override
        public int add(InputStream inputStream, String string) throws IOException {
            File file = this.getUniqueTempPath(string);
            FileUtilities.writeTo(inputStream, file);
            this._files.put(++this._counter, file);
            return this._counter;
        }

        private File getUniqueTempPath(String string) {
            File file = null;
            int n = 0;
            do {
                File file2 = n <= 0 ? this._tempDir : new File(this._tempDir, Integer.toString(n));
                file2.mkdirs();
                file = new File(file2, string);
                ++n;
            } while (file.exists());
            return file;
        }

        @Override
        public File get(int n) throws FileNotFoundException {
            return this._files.get(n);
        }

        @Override
        public long getSize(int n) {
            return this._files.get(n).length();
        }

        @Override
        public void remove(int n) {
            Integer n2 = n;
            this._files.get(n2).delete();
            this._files.remove(n2);
        }

        @Override
        public final void clear() {
            FileUtilities.delete(this._tempDir);
        }

        @Override
        public String getRelativePath(int n) throws FileNotFoundException {
            return this.get(n).getAbsolutePath().substring(this._tempDir.getAbsolutePath().length());
        }
    }

}

