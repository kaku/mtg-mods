/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.StringUtilities;
import java.net.MalformedURLException;
import java.net.URL;

public class FastURL {
    private final String _url;

    public FastURL(String string) {
        if (StringUtilities.isNullOrEmpty(string)) {
            throw new IllegalArgumentException("URL may not be null or empty");
        }
        if (!string.contains(":/")) {
            string = "http://" + string;
        }
        this._url = string;
    }

    public URL getURL() throws MalformedURLException {
        return new URL(this._url);
    }

    public String toString() {
        return this._url;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        FastURL fastURL = (FastURL)object;
        if (this._url == null ? fastURL._url != null : !this._url.equals(fastURL._url)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int n = 3;
        n = 17 * n + (this._url != null ? this._url.hashCode() : 0);
        return n;
    }
}

