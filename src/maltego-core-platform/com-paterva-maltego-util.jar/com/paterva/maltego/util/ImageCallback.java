/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

public interface ImageCallback {
    public void imageReady(Object var1, Object var2);

    public void imageFailed(Object var1, Exception var2);

    public boolean needAwtThread();
}

