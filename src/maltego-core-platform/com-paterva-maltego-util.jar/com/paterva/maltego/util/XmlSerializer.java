/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Serializer
 *  org.simpleframework.xml.core.Persister
 *  org.simpleframework.xml.strategy.Strategy
 *  org.simpleframework.xml.strategy.TreeStrategy
 *  org.simpleframework.xml.strategy.Type
 *  org.simpleframework.xml.strategy.Value
 *  org.simpleframework.xml.stream.InputNode
 *  org.simpleframework.xml.stream.Node
 *  org.simpleframework.xml.stream.NodeMap
 *  org.simpleframework.xml.stream.OutputNode
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.XmlSerializationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.util.Map;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;
import org.simpleframework.xml.strategy.TreeStrategy;
import org.simpleframework.xml.strategy.Type;
import org.simpleframework.xml.strategy.Value;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.Node;
import org.simpleframework.xml.stream.NodeMap;
import org.simpleframework.xml.stream.OutputNode;

public class XmlSerializer {
    public void write(Object object, OutputStream outputStream) throws XmlSerializationException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(Serializer.class.getClassLoader());
            Serializer serializer = XmlSerializer.createSerializer();
            serializer.write(object, outputStream);
        }
        catch (Exception var4_5) {
            throw new XmlSerializationException(var4_5.getMessage(), var4_5);
        }
        finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    public String toString(Object object) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.write(object, byteArrayOutputStream);
        return byteArrayOutputStream.toString("UTF-8");
    }

    public <T> T read(Class<? extends T> class_, InputStream inputStream) throws XmlSerializationException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(Serializer.class.getClassLoader());
            Serializer serializer = XmlSerializer.createSerializer();
            Object object = serializer.read(class_, inputStream);
            return (T)object;
        }
        catch (Exception var4_5) {
            throw new XmlSerializationException(var4_5.getMessage(), var4_5);
        }
        finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    public <T> T read(Class<? extends T> class_, Reader reader) throws XmlSerializationException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(Serializer.class.getClassLoader());
            Serializer serializer = XmlSerializer.createSerializer();
            Object object = serializer.read(class_, reader);
            return (T)object;
        }
        catch (Exception var4_5) {
            throw new XmlSerializationException(var4_5.getMessage(), var4_5);
        }
        finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    public <T> T read(Class<? extends T> class_, String string) throws XmlSerializationException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(Serializer.class.getClassLoader());
            Serializer serializer = XmlSerializer.createSerializer();
            Object object = serializer.read(class_, string);
            return (T)object;
        }
        catch (Exception var4_5) {
            throw new XmlSerializationException(var4_5.getMessage(), var4_5);
        }
        finally {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    private static Serializer createSerializer() {
        return new Persister((Strategy)new RemoveClassStrategy());
    }

    private static class RemoveClassStrategy
    implements Strategy {
        private TreeStrategy _delegate = new TreeStrategy();

        private RemoveClassStrategy() {
        }

        public Value read(Type type, NodeMap<InputNode> nodeMap, Map map) throws Exception {
            return this._delegate.read(type, nodeMap, map);
        }

        public boolean write(Type type, Object object, NodeMap<OutputNode> nodeMap, Map map) throws Exception {
            boolean bl = this._delegate.write(type, object, nodeMap, map);
            nodeMap.remove("class");
            return bl;
        }
    }

}

