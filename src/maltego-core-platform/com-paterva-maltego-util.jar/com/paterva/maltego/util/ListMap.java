/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.ListSet;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

public class ListMap<K, V>
extends AbstractMap<K, V> {
    private ListSet<Map.Entry<K, V>> _set = new ListSet();

    @Override
    public V put(K k, V v) {
        for (Map.Entry<K, V> entry : this._set) {
            if (!entry.getKey().equals(k)) continue;
            V v2 = entry.getValue();
            entry.setValue(v);
            return v2;
        }
        this._set.add(new AbstractMap.SimpleEntry<K, V>(k, v));
        return null;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return this._set;
    }
}

