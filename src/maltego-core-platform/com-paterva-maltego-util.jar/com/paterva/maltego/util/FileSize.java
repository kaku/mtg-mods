/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.StringUtilities;

public class FileSize
implements Comparable<FileSize> {
    long _size;

    public FileSize(long l) {
        this._size = l;
    }

    public String toString() {
        return StringUtilities.bytesToString(this._size);
    }

    @Override
    public int compareTo(FileSize fileSize) {
        if (fileSize == null) {
            return 0;
        }
        if (this._size == fileSize._size) {
            return 0;
        }
        return this._size > fileSize._size ? 1 : -1;
    }
}

