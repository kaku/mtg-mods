/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.output;

import com.paterva.maltego.util.output.MessageLinkListener;

public class MessageChunk {
    private String _text;
    private MessageLinkListener _listener;

    public MessageChunk(String string) {
        this(string, null);
    }

    public MessageChunk(String string, MessageLinkListener messageLinkListener) {
        this._text = string;
        this._listener = messageLinkListener;
    }

    public String getText() {
        return this._text;
    }

    public MessageLinkListener getLinkListener() {
        return this._listener;
    }

    public String toString() {
        return this.getText();
    }
}

