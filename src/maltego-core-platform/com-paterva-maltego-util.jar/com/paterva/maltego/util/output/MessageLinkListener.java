/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.output;

import java.util.EventListener;

public interface MessageLinkListener
extends EventListener {
    public void linkSelected();

    public void linkAction();

    public void linkCleared();

    public String getTranslatorID();
}

