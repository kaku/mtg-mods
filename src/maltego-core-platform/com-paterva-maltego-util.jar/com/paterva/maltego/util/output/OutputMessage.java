/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.output;

import com.paterva.maltego.util.output.MessageChunk;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class OutputMessage {
    private List<MessageChunk> _chunks = new ArrayList<MessageChunk>();

    public OutputMessage() {
    }

    public OutputMessage(String string) {
        this.init(string);
    }

    private void init(String string) {
        this.addChunk(new MessageChunk(string));
    }

    public void addChunk(MessageChunk messageChunk) {
        this._chunks.add(messageChunk);
    }

    public void addChunks(Collection<MessageChunk> collection) {
        this._chunks.addAll(collection);
    }

    public List<MessageChunk> getChunks() {
        return Collections.unmodifiableList(this._chunks);
    }
}

