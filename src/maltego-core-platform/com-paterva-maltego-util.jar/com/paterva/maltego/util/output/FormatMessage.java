/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 */
package com.paterva.maltego.util.output;

import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.output.MessageChunk;
import com.paterva.maltego.util.output.MessageLinkAdapter;
import com.paterva.maltego.util.output.MessageLinkListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.awt.HtmlBrowser;

public class FormatMessage {
    public static List<MessageChunk> highlightUrls(List<MessageChunk> list) {
        Pattern pattern = Pattern.compile("(?<=^|\\s+)((?:http[s]?://|www\\.)[-\\w\\.\\:/]+(?:/[^\\s]*)*)(?=\\s+|$)");
        ArrayList<MessageChunk> arrayList = new ArrayList<MessageChunk>();
        for (MessageChunk messageChunk : list) {
            String string;
            if (messageChunk.getLinkListener() != null) {
                arrayList.add(messageChunk);
                continue;
            }
            String string2 = messageChunk.getText();
            Matcher matcher = pattern.matcher(string2);
            int n = 0;
            while (matcher.find()) {
                String string3;
                string = string2.substring(n, matcher.start());
                String string4 = matcher.group(1);
                URL uRL = null;
                try {
                    string3 = string4;
                    if (string3.startsWith("www.")) {
                        string3 = "http://" + string3;
                    }
                    uRL = new URL(string3);
                }
                catch (MalformedURLException var11_12) {
                    // empty catch block
                }
                if (uRL == null) {
                    string = string + string4;
                }
                if (!string.isEmpty()) {
                    arrayList.add(new MessageChunk(string));
                }
                if ((string3 = uRL) != null) {
                    MessageLinkAdapter messageLinkAdapter = new MessageLinkAdapter((URL)((Object)string3)){
                        final /* synthetic */ URL val$browseUrl;

                        @Override
                        public void linkAction() {
                            if (!FileUtilities.isRemoteFileURL(this.val$browseUrl)) {
                                HtmlBrowser.URLDisplayer.getDefault().showURL(this.val$browseUrl);
                            }
                        }
                    };
                    arrayList.add(new MessageChunk(string4, messageLinkAdapter));
                }
                n = matcher.end();
            }
            string = string2.substring(n);
            if (string.isEmpty()) continue;
            arrayList.add(new MessageChunk(string));
        }
        return arrayList;
    }

}

