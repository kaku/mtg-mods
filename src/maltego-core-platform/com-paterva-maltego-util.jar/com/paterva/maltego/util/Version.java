/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Version
implements Comparable<Version> {
    public static final String PROP_VERSION_READY = "versionReady";
    public static final String PROP_OEM = "maltego.oem";
    public static final String PROP_VERSION = "maltego.version";
    public static final String PROP_BRANDING = "maltego.branding-code";
    public static final String PROP_FULL_VERSION = "maltego.fullversion";
    public static final String PROP_BUILD_NUMBER = "maltego.buildnumber";
    public static final String PROP_VERSION_SUBTITLE = "maltego.version-subtitle";
    public static final String PROP_DISPLAY_VERSION = "maltego.displayversion";
    private static boolean _ready = false;
    private static PropertyChangeSupport _changeSupport = new PropertyChangeSupport(new Object());
    private int _release = 0;
    private int _major = 0;
    private int _minor = 0;
    private int _build = 0;

    public Version(int n, int n2, int n3, int n4) {
        this._release = n;
        this._major = n2;
        this._minor = n3;
        this._build = n4;
    }

    private Version(String string) {
        String[] arrstring = string.split("\\.");
        if (arrstring.length > 0) {
            this._release = this.parseInt(arrstring[0]);
        }
        if (arrstring.length > 1) {
            this._major = this.parseInt(arrstring[1]);
        }
        if (arrstring.length > 2) {
            this._minor = this.parseInt(arrstring[2]);
        }
        if (arrstring.length > 3) {
            this._build = this.parseInt(arrstring[3]);
        }
    }

    public static void setReady(boolean bl) {
        if (_ready != bl) {
            _ready = bl;
            _changeSupport.firePropertyChange("versionReady", !_ready, _ready);
        }
    }

    public static boolean isReady() {
        return _ready;
    }

    private static void checkReady() {
        if (!Version.isReady()) {
            throw new IllegalStateException("Version is not ready.");
        }
    }

    public static Version parse(String string) {
        Version.checkReady();
        return new Version(string);
    }

    public static Version current() {
        return Version.parse(System.getProperty("maltego.fullversion"));
    }

    public static String getBranding() {
        return System.getProperty("maltego.branding-code", "");
    }

    public int release() {
        return this._release;
    }

    public int major() {
        return this._major;
    }

    public int minor() {
        return this._minor;
    }

    public int build() {
        return this._build;
    }

    public boolean isDevBuild() {
        return this.build() == 0 || System.getProperty("maltego.version-subtitle", "").toLowerCase().contains("dev");
    }

    public static void appendToUrl(StringBuilder stringBuilder) {
        Version.checkReady();
        stringBuilder.append("?branding=");
        stringBuilder.append(Version.getBranding());
        stringBuilder.append("&version=");
        stringBuilder.append(System.getProperty("maltego.version", ""));
        stringBuilder.append("&build=");
        stringBuilder.append(System.getProperty("maltego.buildnumber", ""));
        stringBuilder.append("&subtitle=");
        stringBuilder.append(System.getProperty("maltego.version-subtitle", ""));
        stringBuilder.append("&oem=");
        stringBuilder.append(System.getProperty("maltego.oem", ""));
    }

    public String toNiceString() {
        String string = this.toString();
        while (string.endsWith(".0")) {
            string = string.replaceAll(".0$", "");
        }
        return string;
    }

    public String toString() {
        return String.format("%d.%d.%d.%d", this._release, this._major, this._minor, this._build);
    }

    public boolean equals(Object object) {
        if (object instanceof Version) {
            Version version = (Version)object;
            return version.release() == this.release() && version.major() == this.major() && version.minor() == this.minor() && version.build() == this.build();
        }
        return false;
    }

    public int hashCode() {
        int n = 7;
        n = 37 * n + this._release;
        n = 37 * n + this._major;
        n = 37 * n + this._minor;
        n = 37 * n + this._build;
        return n;
    }

    @Override
    public int compareTo(Version version) {
        if (version.release() != this.release()) {
            return this.compareInts(this.release(), version.release());
        }
        if (version.major() != this.major()) {
            return this.compareInts(this.major(), version.major());
        }
        if (version.minor() != this.minor()) {
            return this.compareInts(this.minor(), version.minor());
        }
        return this.compareInts(this.build(), version.build());
    }

    private int compareInts(int n, int n2) {
        return Integer.valueOf(n).compareTo(n2);
    }

    private int parseInt(String string) {
        try {
            return Integer.valueOf(string);
        }
        catch (NumberFormatException var2_2) {
            return 0;
        }
    }

    public static void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        _changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public static void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        _changeSupport.removePropertyChangeListener(propertyChangeListener);
    }
}

