/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.Base64;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ConvolveOp;
import java.awt.image.ImageObserver;
import java.awt.image.IndexColorModel;
import java.awt.image.Kernel;
import java.awt.image.PixelGrabber;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.filesystems.FileObject;
import sun.awt.image.BufferedImageGraphicsConfig;

public class ImageUtils {
    public static final int DO_NOT_SCALE = 0;
    public static final int G2D_SCALE_IF_NEEDED = 1;
    public static final int NEW_IMAGE_SIZE_IF_NEEDED = 2;

    private ImageUtils() {
    }

    public static String base64Encode(Image image, String string) throws IOException {
        return ImageUtils.base64Encode(image, string, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String base64Encode(Image image, String string, boolean bl) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ImageUtils.base64Encode(image, string, byteArrayOutputStream);
            String string2 = byteArrayOutputStream.toString();
            if (bl) {
                String string3 = string2;
                return string3;
            }
            String string4 = string2;
            Pattern pattern = Pattern.compile("([^=]*)(=+)(.*)");
            Matcher matcher = pattern.matcher(string4);
            if (matcher.find()) {
                string4 = matcher.group(1) + matcher.group(3) + matcher.group(2);
            }
            String string5 = string4;
            return string5;
        }
        finally {
            byteArrayOutputStream.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void base64Encode(Image image, String string, OutputStream outputStream) throws IOException {
        BufferedImage bufferedImage = ImageUtils.createBufferedImage(image);
        Base64.OutputStream outputStream2 = null;
        try {
            outputStream2 = new Base64.OutputStream(outputStream);
            ImageIO.write((RenderedImage)bufferedImage, string, outputStream2);
        }
        finally {
            if (outputStream2 != null) {
                outputStream2.close();
            }
        }
    }

    public static BufferedImage base64Decode(InputStream inputStream) throws IOException {
        Base64.InputStream inputStream2 = null;
        try {
            inputStream2 = new Base64.InputStream(inputStream);
            BufferedImage bufferedImage = ImageIO.read(inputStream2);
            return bufferedImage;
        }
        finally {
            if (inputStream2 != null) {
                inputStream2.close();
            }
        }
    }

    public static BufferedImage base64Decode(String string) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(string.getBytes());
        try {
            BufferedImage bufferedImage = ImageUtils.base64Decode(byteArrayInputStream);
            return bufferedImage;
        }
        finally {
            byteArrayInputStream.close();
        }
    }

    public static ImageIcon toImageIcon(Icon icon) {
        if (icon == null) {
            return null;
        }
        if (icon instanceof ImageIcon) {
            return (ImageIcon)icon;
        }
        return new ImageIcon(ImageUtils.toImage(icon));
    }

    public static Image toImage(Icon icon) {
        if (icon == null) {
            return null;
        }
        if (icon instanceof ImageIcon) {
            return ((ImageIcon)icon).getImage();
        }
        int n = icon.getIconWidth();
        int n2 = icon.getIconHeight();
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
        BufferedImage bufferedImage = graphicsConfiguration.createCompatibleImage(n, n2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        icon.paintIcon(null, graphics2D, 0, 0);
        graphics2D.dispose();
        return bufferedImage;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static BufferedImage createBufferedImage(Image image) {
        int n;
        if (image instanceof BufferedImage) {
            return (BufferedImage)image;
        }
        image = new ImageIcon(image).getImage();
        boolean bl = ImageUtils.hasAlpha(image);
        BufferedImage bufferedImage = null;
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            n = 1;
            if (bl) {
                n = 2;
            }
            GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
            GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
            bufferedImage = graphicsConfiguration.createCompatibleImage(image.getWidth(null), image.getHeight(null), n);
        }
        catch (HeadlessException var4_5) {
            var4_5.printStackTrace();
        }
        if (bufferedImage == null) {
            n = 1;
            if (bl) {
                n = 2;
            }
            bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), n);
        }
        Graphics2D graphics2D = null;
        try {
            graphics2D = bufferedImage.createGraphics();
            graphics2D.drawImage(image, 0, 0, null);
        }
        finally {
            if (graphics2D != null) {
                graphics2D.dispose();
            }
        }
        return bufferedImage;
    }

    private static boolean hasAlpha(Image image) {
        if (image instanceof BufferedImage) {
            return ((BufferedImage)image).getColorModel().hasAlpha();
        }
        PixelGrabber pixelGrabber = new PixelGrabber(image, 0, 0, 1, 1, false);
        try {
            pixelGrabber.grabPixels();
        }
        catch (InterruptedException var2_2) {
            // empty catch block
        }
        return pixelGrabber.getColorModel().hasAlpha();
    }

    public static Image setAlpha(Image image, float f) {
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 2);
        Graphics2D graphics2D = (Graphics2D)bufferedImage.getGraphics();
        graphics2D.setComposite(AlphaComposite.SrcOver.derive(f));
        graphics2D.drawImage(image, 0, 0, null);
        return bufferedImage;
    }

    public static BufferedImage loadThumb(Object object, int n) throws IOException {
        ImageInputStream imageInputStream = ImageIO.createImageInputStream(object);
        BufferedImage bufferedImage = ImageUtils.subsampleImage(imageInputStream, n);
        if (bufferedImage != null && (bufferedImage.getWidth(null) > n || bufferedImage.getHeight(null) > n)) {
            bufferedImage = ImageUtils.smartSize(bufferedImage, n);
        }
        return bufferedImage;
    }

    public static Image loadAndSize(File file, double d, double d2) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        return ImageUtils.smartSize(bufferedImage, d, d2);
    }

    public static Image loadAndSize(File file, double d) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        return ImageUtils.smartSize(bufferedImage, d);
    }

    private static BufferedImage resize(BufferedImage bufferedImage, double d, double d2) {
        int n = bufferedImage.getType() == 0 ? 2 : bufferedImage.getType();
        BufferedImage bufferedImage2 = new BufferedImage((int)d, (int)d2, n);
        Graphics2D graphics2D = bufferedImage2.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(bufferedImage, 0, 0, (int)d, (int)d2, null);
        graphics2D.dispose();
        return bufferedImage2;
    }

    public static BufferedImage smartClip(BufferedImage bufferedImage, double d) {
        if ((double)bufferedImage.getWidth() > d || (double)bufferedImage.getHeight() > d) {
            return ImageUtils.smartSize(bufferedImage, d);
        }
        return bufferedImage;
    }

    public static BufferedImage smartClip(BufferedImage bufferedImage, double d, double d2) {
        if ((double)bufferedImage.getWidth() > d || (double)bufferedImage.getHeight() > d2) {
            return ImageUtils.smartSize(bufferedImage, d, d2);
        }
        return bufferedImage;
    }

    public static BufferedImage smartSize(BufferedImage bufferedImage, double d) {
        double d2;
        int n;
        double d3;
        int n2 = bufferedImage.getWidth();
        if (n2 > (n = bufferedImage.getHeight())) {
            d3 = d;
            double d4 = d / (double)n2;
            d2 = (double)n * d4;
        } else {
            d2 = d;
            double d5 = d / (double)n;
            d3 = (double)n2 * d5;
        }
        return ImageUtils.smartSize(bufferedImage, d3, d2);
    }

    public static BufferedImage smartSize(BufferedImage bufferedImage, double d, double d2) {
        bufferedImage = ImageUtils.createCompatibleImage(bufferedImage);
        bufferedImage = ImageUtils.resize(bufferedImage, d * 4.0, d2 * 4.0);
        bufferedImage = ImageUtils.blurImage(bufferedImage);
        return ImageUtils.resize(bufferedImage, d, d2);
    }

    public static BufferedImage blurImage(BufferedImage bufferedImage) {
        float f = 0.11111111f;
        float[] arrf = new float[]{f, f, f, f, f, f, f, f, f};
        HashMap<RenderingHints.Key, Object> hashMap = new HashMap<RenderingHints.Key, Object>();
        hashMap.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        hashMap.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        hashMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        RenderingHints renderingHints = new RenderingHints(hashMap);
        ConvolveOp convolveOp = new ConvolveOp(new Kernel(3, 3, arrf), 1, renderingHints);
        BufferedImage bufferedImage2 = ImageUtils.createCompatibleDestImage(bufferedImage, null);
        return convolveOp.filter(bufferedImage, bufferedImage2);
    }

    public static BufferedImage createCompatibleDestImage(BufferedImage bufferedImage, ColorModel colorModel) {
        int n = bufferedImage.getWidth();
        int n2 = bufferedImage.getHeight();
        WritableRaster writableRaster = null;
        if (colorModel == null) {
            colorModel = bufferedImage.getColorModel();
            if (colorModel instanceof IndexColorModel) {
                colorModel = ColorModel.getRGBdefault();
            } else {
                writableRaster = bufferedImage.getData().createCompatibleWritableRaster(n, n2);
            }
        }
        if (writableRaster == null) {
            writableRaster = colorModel.createCompatibleWritableRaster(n, n2);
        }
        BufferedImage bufferedImage2 = new BufferedImage(colorModel, writableRaster, colorModel.isAlphaPremultiplied(), null);
        return bufferedImage2;
    }

    private static BufferedImage createCompatibleImage(BufferedImage bufferedImage) {
        BufferedImageGraphicsConfig bufferedImageGraphicsConfig = BufferedImageGraphicsConfig.getConfig(bufferedImage);
        int n = bufferedImage.getWidth();
        int n2 = bufferedImage.getHeight();
        BufferedImage bufferedImage2 = bufferedImageGraphicsConfig.createCompatibleImage(n, n2, 3);
        Graphics2D graphics2D = bufferedImage2.createGraphics();
        graphics2D.drawRenderedImage(bufferedImage, null);
        graphics2D.dispose();
        return bufferedImage2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String getImageFormatName(Object object) {
        String string;
        ImageInputStream imageInputStream = null;
        try {
            imageInputStream = ImageIO.createImageInputStream(object);
            Iterator<ImageReader> iterator = ImageIO.getImageReaders(imageInputStream);
            if (!iterator.hasNext()) {
                String string2 = null;
                return string2;
            }
            ImageReader imageReader = iterator.next();
            string = imageReader.getFormatName();
        }
        catch (IOException var2_3) {}
        finally {
            if (imageInputStream != null) {
                try {
                    imageInputStream.close();
                }
                catch (IOException var4_7) {}
            }
        }
        return string;
        return null;
    }

    public static boolean equals(BufferedImage bufferedImage, BufferedImage bufferedImage2) {
        if (bufferedImage == bufferedImage2) {
            return true;
        }
        if (bufferedImage == null || bufferedImage2 == null) {
            return false;
        }
        if (bufferedImage.getWidth() != bufferedImage2.getWidth()) {
            return false;
        }
        if (bufferedImage.getHeight() != bufferedImage2.getHeight()) {
            return false;
        }
        for (int i = 0; i < bufferedImage.getWidth(); ++i) {
            for (int j = 0; j < bufferedImage.getHeight(); ++j) {
                if (bufferedImage.getRGB(i, j) == bufferedImage2.getRGB(i, j)) continue;
                return false;
            }
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void write(Image image, String string, FileObject fileObject) throws IOException {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject.getOutputStream());
            ImageIO.write((RenderedImage)ImageUtils.createBufferedImage(image), string, bufferedOutputStream);
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }

    public static BufferedImage subsampleImage(ImageInputStream imageInputStream, int n) throws IOException {
        Iterator<ImageReader> iterator = ImageIO.getImageReaders(imageInputStream);
        if (!iterator.hasNext()) {
            throw new IOException("No reader available for supplied image stream.");
        }
        ImageReader imageReader = iterator.next();
        ImageReadParam imageReadParam = imageReader.getDefaultReadParam();
        imageReader.setInput(imageInputStream);
        Dimension dimension = new Dimension(imageReader.getWidth(0), imageReader.getHeight(0));
        Dimension dimension2 = new Dimension(n, n);
        int n2 = (int)ImageUtils.scaleSubsamplingMaintainAspectRatio(dimension, dimension2);
        imageReadParam.setSourceSubsampling(n2, n2, 0, 0);
        return imageReader.read(0, imageReadParam);
    }

    public static long scaleSubsamplingMaintainAspectRatio(Dimension dimension, Dimension dimension2) {
        int n = 1;
        if (dimension.getWidth() > dimension2.getWidth()) {
            n = (int)Math.ceil(dimension.getWidth() / dimension2.getWidth());
        } else if (dimension.getHeight() > dimension2.getHeight()) {
            n = (int)Math.ceil(dimension.getHeight() / dimension2.getHeight());
        }
        return n;
    }

    public static Color getDominantColor(BufferedImage bufferedImage) {
        return ImageUtils.getDominantColor(bufferedImage, true);
    }

    public static Color getDominantColor(BufferedImage bufferedImage, boolean bl) {
        if (bufferedImage == null) {
            return new Color(255, 255, 255, 255);
        }
        int[] arrn = new int[36];
        int n = -1;
        float[] arrf = new float[36];
        float[] arrf2 = new float[36];
        float[] arrf3 = new float[36];
        float[] arrf4 = new float[3];
        int n2 = bufferedImage.getHeight();
        int n3 = bufferedImage.getWidth();
        for (int i = 0; i < n2; ++i) {
            for (int j = 0; j < n3; ++j) {
                int n4 = bufferedImage.getRGB(j, i);
                if ((n4 >> 24 & 255) < 128) continue;
                Color.RGBtoHSB(n4 >> 16 & 255, n4 >> 8 & 255, n4 & 255, arrf4);
                if (bl && (arrf4[1] <= 0.35f || arrf4[2] <= 0.35f)) continue;
                int n5 = (int)Math.floor(arrf4[0] / 10.0f);
                arrf[n5] = arrf[n5] + arrf4[0];
                arrf2[n5] = arrf2[n5] + arrf4[1];
                arrf3[n5] = arrf3[n5] + arrf4[2];
                int[] arrn2 = arrn;
                int n6 = n5;
                arrn2[n6] = arrn2[n6] + 1;
                if (n >= 0 && arrn[n5] <= arrn[n]) continue;
                n = n5;
            }
        }
        if (n < 0) {
            return new Color(255, 255, 255, 255);
        }
        arrf4[0] = arrf[n] / (float)arrn[n];
        arrf4[1] = arrf2[n] / (float)arrn[n];
        arrf4[2] = arrf3[n] / (float)arrn[n];
        return new Color(Color.HSBtoRGB(arrf4[0], arrf4[1], arrf4[2]));
    }

    public static Dimension calcScaledSize(Image image, double d) {
        int n = image.getWidth(null);
        int n2 = image.getHeight(null);
        Dimension dimension = new Dimension();
        dimension.setSize(d, d);
        double d2 = (double)n / (double)n2;
        if (d2 > 1.0) {
            dimension.setSize(d, d / d2);
        } else if (d2 < 1.0) {
            dimension.setSize(d * d2, d);
        }
        return dimension;
    }

    public static void drawImageDouble(Graphics2D graphics2D, Image image, double d, double d2, double d3, double d4, int n) {
        AffineTransform affineTransform = graphics2D.getTransform();
        graphics2D.translate(d, d2);
        switch (n) {
            case 0: {
                break;
            }
            case 1: {
                if (!ImageUtils.mustScale(d3, image.getWidth(null)) && !ImageUtils.mustScale(d4, image.getHeight(null))) break;
                graphics2D.scale(d3 / (double)image.getWidth(null), d4 / (double)image.getHeight(null));
                break;
            }
            case 2: {
                if (!ImageUtils.mustScale(d3, image.getWidth(null)) && !ImageUtils.mustScale(d4, image.getHeight(null))) break;
                image = ImageUtils.resize(ImageUtils.createBufferedImage(image), d3, d4);
            }
        }
        graphics2D.drawImage(image, 0, 0, null);
        graphics2D.setTransform(affineTransform);
    }

    private static boolean mustScale(double d, double d2) {
        return Math.abs(d - d2) > 0.1;
    }
}

