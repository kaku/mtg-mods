/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.awt.Color;
import java.awt.Composite;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

public class ColorUtilities {
    private ColorUtilities() {
    }

    public static Color darker(Color color, int n) {
        return ColorUtilities.brighter(color, - n);
    }

    public static Color brighter(Color color, int n) {
        return ColorUtilities.brighterTranslucent(color, n, 255);
    }

    public static Color brighterTranslucent(Color color, int n, int n2) {
        int n3 = color.getRed() + n;
        int n4 = color.getBlue() + n;
        int n5 = color.getGreen() + n;
        int n6 = n2;
        if (n6 < 0) {
            n6 = 0;
        }
        if (n6 > 255) {
            n6 = 255;
        }
        if (n3 > 255) {
            n3 = 255;
        }
        if (n3 < 0) {
            n3 = 0;
        }
        if (n5 > 255) {
            n5 = 255;
        }
        if (n5 < 0) {
            n5 = 0;
        }
        if (n4 > 255) {
            n4 = 255;
        }
        if (n4 < 0) {
            n4 = 0;
        }
        return new Color(n3, n5, n4, n6);
    }

    public static Color darkerTranslucent(Color color, int n, int n2) {
        return ColorUtilities.brighterTranslucent(color, - n, n2);
    }

    public static Color makeTranslucent(Color color, int n) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), n);
    }

    public static Color decode(String string) {
        if (string == null || string.isEmpty()) {
            return Color.black;
        }
        return Color.decode(string);
    }

    public static String encode(Color color) {
        char[] arrc = new char[7];
        arrc[0] = 35;
        String string = Integer.toHexString(color.getRed());
        if (string.length() == 1) {
            arrc[1] = 48;
            arrc[2] = string.charAt(0);
        } else {
            arrc[1] = string.charAt(0);
            arrc[2] = string.charAt(1);
        }
        string = Integer.toHexString(color.getGreen());
        if (string.length() == 1) {
            arrc[3] = 48;
            arrc[4] = string.charAt(0);
        } else {
            arrc[3] = string.charAt(0);
            arrc[4] = string.charAt(1);
        }
        string = Integer.toHexString(color.getBlue());
        if (string.length() == 1) {
            arrc[5] = 48;
            arrc[6] = string.charAt(0);
        } else {
            arrc[5] = string.charAt(0);
            arrc[6] = string.charAt(1);
        }
        return String.valueOf(arrc);
    }

    public static Color invert(Color color) {
        float[] arrf = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), arrf);
        int n = Color.HSBtoRGB(1.0f - arrf[0], arrf[1], arrf[2]);
        return new Color(n);
    }

    public static double getColorDistance(Color color, Color color2) {
        int n = color.getRed();
        int n2 = color2.getRed();
        int n3 = n + n2 >> 1;
        int n4 = n - n2;
        int n5 = color.getGreen() - color2.getGreen();
        int n6 = color.getBlue() - color2.getBlue();
        return Math.sqrt(((512 + n3) * n4 * n4 >> 8) + 4 * n5 * n5 + ((767 - n3) * n6 * n6 >> 8));
    }

    public static String getColorTone(Color color) {
        float[] arrf = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), arrf);
        return ColorUtilities.getColorTone(arrf);
    }

    public static String getColorTone(int n) {
        float[] arrf = new float[3];
        int n2 = n >> 16 & 255;
        int n3 = n >> 8 & 255;
        int n4 = n & 255;
        Color.RGBtoHSB(n2, n3, n4, arrf);
        return ColorUtilities.getColorTone(arrf);
    }

    public static String getColorTone(float[] arrf) {
        float f;
        String string = (double)arrf[1] < 0.1 && (double)arrf[2] > 0.9 ? "nearlyWhite" : ((double)arrf[2] < 0.1 ? "nearlyBlack" : ((f = arrf[0] * 360.0f) >= 0.0f && f < 30.0f ? "red" : (f >= 30.0f && f < 90.0f ? "yellow" : (f >= 90.0f && f < 150.0f ? "green" : (f >= 150.0f && f < 210.0f ? "cyan" : (f >= 210.0f && f < 270.0f ? "blue" : (f >= 270.0f && f < 330.0f ? "magenta" : "red")))))));
        return string;
    }

    public static Color getXorColor(Color color, Color color2) {
        int[] arrn = new int[3];
        int[] arrn2 = new int[3];
        arrn[0] = color.getRed();
        arrn[1] = color.getGreen();
        arrn[2] = color.getBlue();
        arrn2[0] = color2.getRed();
        arrn2[1] = color2.getGreen();
        arrn2[2] = color2.getBlue();
        for (int i = 0; i < 3; ++i) {
            int[] arrn3 = arrn;
            int n = i;
            arrn3[n] = arrn3[n] ^ arrn2[i];
        }
        return new Color(arrn[0], arrn[1], arrn[2]);
    }

    public static Color getSelectionColor(Color color, Color color2, Color color3) {
        Color color4 = ColorUtilities.getXorColor(color, color2);
        String string = ColorUtilities.getColorTone(color4);
        if (!string.equalsIgnoreCase("blue")) {
            color3 = color4;
        }
        return color3;
    }

    private static class XorContext
    implements CompositeContext {
        @Override
        public void compose(Raster raster, Raster raster2, WritableRaster writableRaster) {
            int n = Math.min(raster.getWidth(), raster2.getWidth());
            int n2 = Math.min(raster.getHeight(), raster2.getHeight());
            int[] arrn = new int[]{-50, -50, -50, -50};
            int[] arrn2 = new int[]{-50, -50, -50, -50};
            int n3 = raster.getPixel(0, 0, arrn)[3];
            int n4 = raster2.getPixel(0, 0, arrn2)[3];
            n3 = n3 == -50 ? 3 : 4;
            n4 = n4 == -50 ? 3 : 4;
            int[] arrn3 = new int[4 * n];
            int[] arrn4 = new int[4 * n];
            for (int i = 0; i < n2; ++i) {
                raster.getPixels(0, i, n, 1, arrn3);
                raster2.getPixels(0, i, n, 1, arrn4);
                for (int j = 0; j < n; ++j) {
                    int n5 = j * n3;
                    int n6 = j * n4;
                    for (int k = 0; k < 3; ++k) {
                        int[] arrn5 = arrn4;
                        int n7 = n6 + k;
                        arrn5[n7] = arrn5[n7] ^ arrn3[n5 + k];
                    }
                }
                writableRaster.setPixels(0, i, n, 1, arrn4);
            }
        }

        @Override
        public void dispose() {
        }
    }

    public static class XorComposite
    implements Composite {
        public static XorComposite INSTANCE = new XorComposite();
        private final XorContext context = new XorContext();

        @Override
        public CompositeContext createContext(ColorModel colorModel, ColorModel colorModel2, RenderingHints renderingHints) {
            return this.context;
        }
    }

}

