/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.collections;

import com.paterva.maltego.util.collections.CompoundIterator;
import java.util.Collection;
import java.util.Iterator;

public class CompoundReadonlyCollection<T>
implements Collection<T> {
    private Collection<T>[] _collections;

    public /* varargs */ CompoundReadonlyCollection(Collection<T> ... arrcollection) {
        this._collections = arrcollection;
    }

    @Override
    public int size() {
        int n = 0;
        for (Collection<T> collection : this._collections) {
            n += collection.size();
        }
        return n;
    }

    @Override
    public boolean isEmpty() {
        for (Collection<T> collection : this._collections) {
            if (collection.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public Object[] toArray() {
        Object[] arrobject = new Object[this.size()];
        int n = 0;
        for (Collection<T> collection : this._collections) {
            Object[] arrobject2 = collection.toArray();
            CompoundReadonlyCollection.copy(arrobject2, arrobject, n);
            n += arrobject2.length;
        }
        return arrobject;
    }

    private static <T> void copy(T[] arrT, T[] arrT2, int n) {
        System.arraycopy(arrT, 0, arrT2, n, arrT.length);
    }

    @Override
    public <A> A[] toArray(A[] arrA) {
        int n = 0;
        for (Collection<T> collection : this._collections) {
            Object[] arrobject = collection.toArray();
            CompoundReadonlyCollection.copy(arrobject, arrA, n);
            n += arrobject.length;
        }
        return arrA;
    }

    @Override
    public void clear() {
    }

    @Override
    public Iterator<T> iterator() {
        return new CompoundIterator<T>(this._collections);
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object object) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean contains(Object object) {
        for (Collection<T> collection : this._collections) {
            if (!collection.contains(object)) continue;
            return true;
        }
        return false;
    }
}

