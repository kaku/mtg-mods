/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.collections;

import java.util.Iterator;

public class CompoundIterator<T>
implements Iterator<T> {
    private Iterable<T>[] _collections;
    private int _currentIndex = 0;
    private Iterator<T> _currentIterator;

    public /* varargs */ CompoundIterator(Iterable<T> ... arriterable) {
        this._collections = arriterable;
        if (this._collections.length > 0) {
            this._currentIterator = this._collections[0].iterator();
        }
    }

    @Override
    public boolean hasNext() {
        if (this._currentIterator == null) {
            return false;
        }
        if (this._currentIterator.hasNext()) {
            return true;
        }
        while (this._currentIndex < this._collections.length - 1) {
            ++this._currentIndex;
            this._currentIterator = this._collections[this._currentIndex].iterator();
            if (!this._currentIterator.hasNext()) continue;
            return true;
        }
        return false;
    }

    @Override
    public T next() {
        return this._currentIterator.next();
    }

    @Override
    public void remove() {
        this._currentIterator.remove();
    }
}

