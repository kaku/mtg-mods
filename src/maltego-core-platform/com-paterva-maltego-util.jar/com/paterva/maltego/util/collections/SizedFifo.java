/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class SizedFifo<T>
implements Queue<T> {
    private int _limit;
    private LinkedList<T> _list = new LinkedList();

    public SizedFifo(int n) {
        this._limit = n;
    }

    @Override
    public boolean add(T t) {
        if (this.size() == this.getLimit()) {
            this.remove();
        }
        return this._list.add(t);
    }

    @Override
    public boolean offer(T t) {
        if (this.size() == this.getLimit()) {
            return false;
        }
        return this._list.add(t);
    }

    @Override
    public T remove() {
        return this._list.remove();
    }

    @Override
    public T poll() {
        return this._list.poll();
    }

    @Override
    public T element() {
        return this._list.element();
    }

    @Override
    public T peek() {
        return this._list.peek();
    }

    @Override
    public int size() {
        return this._list.size();
    }

    @Override
    public boolean isEmpty() {
        return this._list.isEmpty();
    }

    @Override
    public boolean contains(Object object) {
        return this._list.contains(object);
    }

    @Override
    public Iterator<T> iterator() {
        return this._list.iterator();
    }

    @Override
    public Object[] toArray() {
        return this._list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arrT) {
        return this._list.toArray(arrT);
    }

    @Override
    public boolean remove(Object object) {
        return this._list.remove(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return this._list.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        boolean bl = false;
        for (T t : collection) {
            if (!this.add(t)) continue;
            bl = true;
        }
        return bl;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return this._list.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void clear() {
        this._list.clear();
    }

    public int getLimit() {
        return this._limit;
    }

    public void setLimit(int n) {
        this._limit = n;
    }
}

