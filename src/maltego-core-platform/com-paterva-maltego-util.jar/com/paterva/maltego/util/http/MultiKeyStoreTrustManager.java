/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.http;

import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class MultiKeyStoreTrustManager
implements X509TrustManager {
    protected ArrayList<X509TrustManager> x509TrustManagers = new ArrayList();

    protected /* varargs */ MultiKeyStoreTrustManager(KeyStore ... arrkeyStore) {
        ArrayList<TrustManagerFactory> arrayList = new ArrayList<TrustManagerFactory>();
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore)null);
            arrayList.add(trustManagerFactory);
            for (KeyStore keyStore : arrkeyStore) {
                TrustManagerFactory trustManagerFactory2 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory2.init(keyStore);
                arrayList.add(trustManagerFactory2);
            }
        }
        catch (Exception var3_4) {
            throw new RuntimeException(var3_4);
        }
        for (TrustManagerFactory trustManagerFactory : arrayList) {
            for (TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
                if (!(trustManager instanceof X509TrustManager)) continue;
                this.x509TrustManagers.add((X509TrustManager)trustManager);
            }
        }
        if (this.x509TrustManagers.isEmpty()) {
            throw new RuntimeException("Couldn't find any X509TrustManagers");
        }
    }

    @Override
    public void checkClientTrusted(X509Certificate[] arrx509Certificate, String string) throws CertificateException {
        X509TrustManager x509TrustManager = this.x509TrustManagers.get(0);
        x509TrustManager.checkClientTrusted(arrx509Certificate, string);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] arrx509Certificate, String string) throws CertificateException {
        CertificateException certificateException = null;
        for (X509TrustManager x509TrustManager : this.x509TrustManagers) {
            try {
                x509TrustManager.checkServerTrusted(arrx509Certificate, string);
                return;
            }
            catch (CertificateException var6_6) {
                certificateException = var6_6;
                continue;
            }
        }
        throw certificateException;
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        ArrayList<X509Certificate> arrayList = new ArrayList<X509Certificate>();
        for (X509TrustManager x509TrustManager : this.x509TrustManagers) {
            arrayList.addAll(Arrays.asList(x509TrustManager.getAcceptedIssuers()));
        }
        return arrayList.toArray(new X509Certificate[arrayList.size()]);
    }
}

