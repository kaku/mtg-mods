/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.http;

import com.paterva.maltego.util.http.CertificateUtils;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.X509TrustManager;

class SimpleX509TrustManager
implements X509TrustManager {
    private static final Logger LOG = Logger.getLogger(SimpleX509TrustManager.class.getName());
    private X509TrustManager _delegate;
    private boolean _allowAllCerts;
    private String _requireInCN;

    public SimpleX509TrustManager(X509TrustManager x509TrustManager) throws NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        this(x509TrustManager, true);
    }

    public SimpleX509TrustManager(X509TrustManager x509TrustManager, boolean bl) throws NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        LOG.log(Level.FINE, "{0} New Trust Manager - trust all: {1}", new Object[]{this.getHash(), bl});
        this._allowAllCerts = bl;
        this._delegate = x509TrustManager;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] arrx509Certificate, String string) throws CertificateException {
        this._delegate.checkClientTrusted(arrx509Certificate, string);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] arrx509Certificate, String string) throws CertificateException {
        if (!this._allowAllCerts) {
            LOG.log(Level.FINE, "{0} Check server trusted: {1}", new Object[]{this.getHash(), string});
            try {
                try {
                    this._delegate.checkServerTrusted(arrx509Certificate, string);
                }
                catch (CertificateExpiredException var3_3) {
                    LOG.log(Level.FINE, "{0} Cert expired: {1}", new Object[]{this.getHash(), var3_3});
                }
                if (this._requireInCN != null && !SimpleX509TrustManager.checkSubject(arrx509Certificate, this._requireInCN)) {
                    LOG.log(Level.FINE, "{0} Invalid subject", new Object[]{this.getHash()});
                    throw new CertificateException("Invalid subject, required: " + this._requireInCN);
                }
            }
            catch (CertificateException var3_4) {
                LOG.log(Level.FINE, "{0} Cert NOT trusted", new Object[]{this.getHash()});
                throw var3_4;
            }
            LOG.log(Level.FINE, "{0} Cert trusted", new Object[]{this.getHash()});
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return this._delegate.getAcceptedIssuers();
    }

    public boolean trustAllCerts() {
        return this._allowAllCerts;
    }

    public void setTrustAllCerts(boolean bl) {
        this._allowAllCerts = bl;
        LOG.log(Level.FINE, "{0} Trust all certs: {1}", new Object[]{this.getHash(), bl});
    }

    public String getRequireInCN() {
        return this._requireInCN;
    }

    public void setRequireInCN(String string) {
        this._requireInCN = string;
        LOG.log(Level.FINE, "{0} Required in CN: {1}", new Object[]{this.getHash(), string});
    }

    private static boolean checkSubject(X509Certificate[] arrx509Certificate, String string) {
        if (arrx509Certificate != null && arrx509Certificate.length > 0) {
            X509Certificate x509Certificate = arrx509Certificate[0];
            String string2 = CertificateUtils.getSubjectCommonName(x509Certificate);
            if (string2 == null) {
                return false;
            }
            return string2.endsWith(string);
        }
        return false;
    }

    private String getHash() {
        return LOG.isLoggable(Level.FINE) ? Integer.toString(System.identityHashCode(this), 36).toUpperCase() : "";
    }
}

