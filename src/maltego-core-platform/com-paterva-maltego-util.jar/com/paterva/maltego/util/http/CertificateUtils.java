/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.http;

import com.paterva.maltego.util.Base64;
import com.paterva.maltego.util.StringUtilities;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CertificateUtils {
    private static final String SUBJECT_COMMON_NAME = "cn";

    private CertificateUtils() {
    }

    public static String stripUrl(String string) {
        return string.replaceFirst("^(https?:/+[^/\\?]+).*", "$1");
    }

    public static String getSubjectCommonName(X509Certificate x509Certificate) {
        return CertificateUtils.getSubjectEntry(x509Certificate, "cn");
    }

    public static String getSubjectEntry(X509Certificate x509Certificate, String string) {
        String[] arrstring;
        String string2 = x509Certificate.getSubjectDN().getName();
        for (String string3 : arrstring = string2.split(",")) {
            if (!string3.toLowerCase().startsWith(string + "=")) continue;
            return string3.substring(string.length() + 1);
        }
        return null;
    }

    public static X509Certificate decode(String string) throws IOException, CertificateException {
        byte[] arrby = Base64.decode(string);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrby);
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        return (X509Certificate)certificateFactory.generateCertificate(byteArrayInputStream);
    }

    public static String toPrettyString(X509Certificate x509Certificate) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("For:\n   ").append(x509Certificate.getSubjectDN()).append("\n");
        stringBuilder.append("Issued by:\n   ").append(x509Certificate.getIssuerDN()).append("\n");
        stringBuilder.append("Valid from ").append(CertificateUtils.toDateOnlyString(x509Certificate.getNotBefore()));
        stringBuilder.append(" to ").append(CertificateUtils.toDateOnlyString(x509Certificate.getNotAfter())).append("\n");
        stringBuilder.append("Serial Number: ").append(x509Certificate.getSerialNumber()).append("\n");
        stringBuilder.append("Generated with ").append(x509Certificate.getSigAlgName());
        return stringBuilder.toString();
    }

    public static String toPrettyString2(X509Certificate x509Certificate) {
        String string = x509Certificate.toString();
        string = string.replaceAll("qualifier: 0000", "qualifier:\n0000");
        string = StringUtilities.indent(string, '[', ']', "  ");
        string = string.replaceAll("(\\d{60})(\\d)", "$1\n             $2");
        return string;
    }

    private static String toDateOnlyString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }
}

