/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.http;

import com.paterva.maltego.util.Base64;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.http.CertificateMismatchException;
import com.paterva.maltego.util.http.CertificateUtils;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public abstract class ServerCertificates {
    private static final Logger LOG = Logger.getLogger(ServerCertificates.class.getName());
    private static ServerCertificates _default;

    public static synchronized ServerCertificates getDefault() {
        if (_default == null && (ServerCertificates._default = (ServerCertificates)Lookup.getDefault().lookup(ServerCertificates.class)) == null) {
            _default = new DefaultServerCertificates();
        }
        return _default;
    }

    public abstract void add(String var1, X509Certificate var2);

    public abstract X509Certificate get(String var1);

    public abstract void remove(String var1);

    public abstract boolean isSameAsBefore(String var1, X509Certificate var2);

    public abstract void blacklist(String var1, X509Certificate var2);

    public abstract boolean isBlacklisted(String var1, X509Certificate var2);

    public void checkSameAsBefore(String string, X509Certificate x509Certificate) throws CertificateMismatchException {
        if (!this.isSameAsBefore(string, x509Certificate)) {
            X509Certificate x509Certificate2 = this.get(string);
            throw new CertificateMismatchException("Certificate changed for " + string, string, x509Certificate2, x509Certificate);
        }
    }

    private static class UrlCertPair {
        private final String _url;
        private final X509Certificate _cert;

        public UrlCertPair(String string, X509Certificate x509Certificate) {
            this._url = string;
            this._cert = x509Certificate;
        }

        public String getUrl() {
            return this._url;
        }

        public X509Certificate getCert() {
            return this._cert;
        }

        public String toString() {
            return "UrlCertPair{" + this._url + ": " + this._cert.getSubjectDN() + '}';
        }

        public int hashCode() {
            int n = 3;
            n = 11 * n + Objects.hashCode(this._url);
            n = 11 * n + Objects.hashCode(this._cert);
            return n;
        }

        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || this.getClass() != object.getClass()) {
                return false;
            }
            UrlCertPair urlCertPair = (UrlCertPair)object;
            if (!Objects.equals(this._url, urlCertPair._url)) {
                return false;
            }
            return Objects.equals(this._cert, urlCertPair._cert);
        }
    }

    private static class DefaultServerCertificates
    extends ServerCertificates {
        private static final String PREF_CERTS = "maltego.certs";
        private static final String PREF_CERT_BLACKLIST = "maltego.cert-blacklisted";
        private Map<String, X509Certificate> _certs;
        private Set<UrlCertPair> _blacklisted;

        private DefaultServerCertificates() {
        }

        @Override
        public synchronized void add(String string, X509Certificate x509Certificate) {
            string = CertificateUtils.stripUrl(string);
            String string2 = CertificateUtils.getSubjectCommonName(x509Certificate);
            LOG.log(Level.FINE, "Adding cert for: {0} -> {1}", new Object[]{string, string2});
            this.getCerts().put(string, x509Certificate);
            this.save();
        }

        @Override
        public X509Certificate get(String string) {
            string = CertificateUtils.stripUrl(string);
            return this._certs.get(string);
        }

        @Override
        public synchronized boolean isSameAsBefore(String string, X509Certificate x509Certificate) {
            string = CertificateUtils.stripUrl(string);
            boolean bl = true;
            LOG.log(Level.FINE, "Checking cert for: {0}", string);
            X509Certificate x509Certificate2 = this.getCerts().get(string);
            if (x509Certificate2 == null) {
                LOG.log(Level.FINE, "No cert for: {0}", string);
                this.add(string, x509Certificate);
            } else {
                String string2 = CertificateUtils.getSubjectCommonName(x509Certificate);
                String string3 = CertificateUtils.getSubjectCommonName(x509Certificate2);
                if (x509Certificate2.equals(x509Certificate)) {
                    LOG.log(Level.FINE, "New cert ({0}) same as before ({1})", new Object[]{string2, string3});
                } else {
                    LOG.log(Level.FINE, "New cert ({0}) NOT same as before ({1})", new Object[]{string2, string3});
                    bl = false;
                }
            }
            return bl;
        }

        @Override
        public synchronized void remove(String string) {
            string = CertificateUtils.stripUrl(string);
            LOG.log(Level.FINE, "Removing cert for: {0}", new Object[]{string});
            this.getCerts().remove(string);
            this.removeFromBlacklisted(string);
            this.save();
            this.saveBlacklisted();
        }

        private void removeFromBlacklisted(String string) {
            Iterator<UrlCertPair> iterator = this.getBlacklisted().iterator();
            while (iterator.hasNext()) {
                UrlCertPair urlCertPair = iterator.next();
                if (!urlCertPair.getUrl().equals(string)) continue;
                iterator.remove();
            }
        }

        @Override
        public void blacklist(String string, X509Certificate x509Certificate) {
            string = CertificateUtils.stripUrl(string);
            LOG.log(Level.FINE, "Blacklisting: {0}", new Object[]{string});
            this.getBlacklisted().add(new UrlCertPair(string, x509Certificate));
            this.saveBlacklisted();
        }

        @Override
        public boolean isBlacklisted(String string, X509Certificate x509Certificate) {
            string = CertificateUtils.stripUrl(string);
            return this.getBlacklisted().contains(new UrlCertPair(string, x509Certificate));
        }

        private Map<String, X509Certificate> getCerts() {
            if (this._certs == null) {
                this._certs = this.load();
            }
            return this._certs;
        }

        private Set<UrlCertPair> getBlacklisted() {
            if (this._blacklisted == null) {
                this._blacklisted = this.loadBlacklisted();
            }
            return this._blacklisted;
        }

        private void save() {
            LOG.log(Level.FINER, "Saving: {0}", this._certs);
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<String, X509Certificate> entry : this._certs.entrySet()) {
                try {
                    String string = entry.getKey();
                    X509Certificate x509Certificate = entry.getValue();
                    this.write(string, x509Certificate, stringBuilder);
                }
                catch (Exception var4_5) {
                    Exceptions.printStackTrace((Throwable)var4_5);
                }
            }
            this.writeAsBytes(stringBuilder, "maltego.certs");
        }

        private Map<String, X509Certificate> load() {
            HashMap<String, X509Certificate> hashMap = new HashMap<String, X509Certificate>();
            Set<UrlCertPair> set = this.loadUrlCertPairs("maltego.certs");
            for (UrlCertPair urlCertPair : set) {
                hashMap.put(urlCertPair.getUrl(), urlCertPair.getCert());
            }
            LOG.log(Level.FINER, "Loaded: {0}", hashMap);
            return hashMap;
        }

        private void saveBlacklisted() {
            LOG.log(Level.FINE, "Saving blacklisted: {0}", this._blacklisted);
            StringBuilder stringBuilder = new StringBuilder();
            for (UrlCertPair urlCertPair : this._blacklisted) {
                try {
                    String string = urlCertPair.getUrl();
                    X509Certificate x509Certificate = urlCertPair.getCert();
                    this.write(string, x509Certificate, stringBuilder);
                }
                catch (Exception var4_5) {
                    Exceptions.printStackTrace((Throwable)var4_5);
                }
            }
            this.writeAsBytes(stringBuilder, "maltego.cert-blacklisted");
        }

        private Set<UrlCertPair> loadUrlCertPairs(String string) {
            HashSet<UrlCertPair> hashSet = new HashSet<UrlCertPair>();
            byte[] arrby = this.getPrefs().getByteArray(string, null);
            if (arrby != null) {
                String string2 = new String(arrby, StandardCharsets.UTF_8);
                String[] arrstring = string2.split("\\|");
                for (int i = 0; i < arrstring.length / 2; ++i) {
                    try {
                        String string3 = arrstring[i * 2];
                        String string4 = arrstring[i * 2 + 1];
                        X509Certificate x509Certificate = CertificateUtils.decode(string4);
                        hashSet.add(new UrlCertPair(string3, x509Certificate));
                        continue;
                    }
                    catch (Exception var7_8) {
                        Exceptions.printStackTrace((Throwable)var7_8);
                    }
                }
            }
            return hashSet;
        }

        private Set<UrlCertPair> loadBlacklisted() {
            Set<UrlCertPair> set = this.loadUrlCertPairs("maltego.cert-blacklisted");
            LOG.log(Level.FINER, "Loaded blacklisted: {0}", set);
            return set;
        }

        private Preferences getPrefs() {
            return NbPreferences.forModule(this.getClass());
        }

        private void writeAsBytes(StringBuilder stringBuilder, String string) {
            this.getPrefs().putByteArray(string, stringBuilder.toString().getBytes(StandardCharsets.UTF_8));
        }

        private void write(String string, X509Certificate x509Certificate, StringBuilder stringBuilder) throws CertificateEncodingException {
            if (!StringUtilities.isNullOrEmpty(string) && x509Certificate != null) {
                String string2 = Base64.encodeBytes(x509Certificate.getEncoded());
                stringBuilder.append(string);
                stringBuilder.append("|");
                stringBuilder.append(string2);
                stringBuilder.append("|");
            }
        }
    }

}

