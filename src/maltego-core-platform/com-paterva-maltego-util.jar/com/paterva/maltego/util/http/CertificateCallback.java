/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.http;

import java.security.cert.X509Certificate;

public interface CertificateCallback {
    public void onCertificate(X509Certificate var1);
}

