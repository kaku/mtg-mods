/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util.http;

import com.paterva.maltego.util.http.CertificateCallback;
import com.paterva.maltego.util.http.HttpAgent;
import java.net.URL;
import java.security.cert.X509Certificate;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

public class CertificateDownloader {
    public void get(final URL uRL, final CertificateCallback certificateCallback) {
        Thread thread = new Thread(new Runnable(){

            @Override
            public void run() {
                HttpAgent httpAgent;
                X509Certificate x509Certificate = null;
                try {
                    httpAgent = new HttpAgent(uRL);
                    x509Certificate = httpAgent.getServerCertificate();
                }
                catch (Exception var2_3) {
                    Exceptions.printStackTrace((Throwable)var2_3);
                }
                httpAgent = x509Certificate;
                SwingUtilities.invokeLater(new Runnable((X509Certificate)((Object)httpAgent)){
                    final /* synthetic */ X509Certificate val$finalCert;

                    @Override
                    public void run() {
                        certificateCallback.onCertificate(this.val$finalCert);
                    }
                });
            }

        }, "Certificate Downloader");
        thread.start();
    }

}

