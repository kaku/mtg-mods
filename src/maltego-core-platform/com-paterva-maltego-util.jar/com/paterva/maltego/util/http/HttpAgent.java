/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.IOUtils
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.http;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.http.MaltegoKeyStore;
import com.paterva.maltego.util.http.MultiKeyStoreTrustManager;
import com.paterva.maltego.util.http.SimpleX509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.IOUtils;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class HttpAgent {
    private static final Logger LOG = Logger.getLogger(HttpAgent.class.getName());
    private final URL _url;
    private URLConnection _cn;
    private int _readTimeout = -1;
    private int _connectTimeout = -1;
    private String _userAgent;
    private String _mimeType;
    private String _characterSet;
    private int _contentLength;
    private boolean _responseParsed = false;
    private boolean _trustAllCerts = false;
    private String _requireCNSuffix = null;
    private String _contentEncoding;
    private String _acceptEncoding;
    private boolean _allowNonHttp = false;

    public HttpAgent(URL uRL) {
        LOG.log(Level.FINE, "{0} URL: {1}", new Object[]{this.getHash(), uRL});
        this._url = uRL;
        this._userAgent = this.getDefaultUserAgent();
        HttpURLConnection.setFollowRedirects(true);
    }

    public boolean trustAllCerts() {
        return this._trustAllCerts;
    }

    public void setTrustAllCerts(boolean bl) {
        this._trustAllCerts = bl;
    }

    public boolean isAllowNonHttp() {
        return this._allowNonHttp;
    }

    public void setAllowNonHttp(boolean bl) {
        this._allowNonHttp = bl;
    }

    public void doGet() throws IOException {
        this.connect(false);
        this.parseResponse();
    }

    public OutputStream doPost(String string) throws IOException {
        this.connect(true);
        this._cn.setRequestProperty("Content-Type", string);
        return this._cn.getOutputStream();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String doPostString(String string, String string2) throws IOException {
        OutputStream outputStream = this.doPost(string);
        try {
            outputStream.write(string2.getBytes("UTF-8"));
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
        return StringUtilities.toString(this.getInputStream(), this._characterSet);
    }

    public String getContentAsString() throws IOException {
        this.parseResponse();
        return StringUtilities.toString(this.getInputStream(), this._characterSet);
    }

    public byte[] getContentAsBytes() throws IOException {
        this.parseResponse();
        return IOUtils.toByteArray((InputStream)this.getInputStream());
    }

    private void connect(boolean bl) throws IOException {
        LOG.log(Level.FINE, "{0} Connect - doPost: {1}", new Object[]{this.getHash(), bl});
        this._responseParsed = false;
        URLConnection uRLConnection = this._url.openConnection();
        if (!this._allowNonHttp && !(uRLConnection instanceof HttpURLConnection)) {
            throw new IllegalArgumentException("URL protocol must be HTTP or HTTPS.");
        }
        if (uRLConnection instanceof HttpsURLConnection) {
            try {
                this.configureHttpsConnection(uRLConnection);
            }
            catch (Exception var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
        this._cn = uRLConnection;
        if (this.getConnectTimeout() > -1) {
            this._cn.setConnectTimeout(this.getConnectTimeout());
        }
        if (this.getReadTimeout() > -1) {
            this._cn.setReadTimeout(this.getReadTimeout());
        }
        this._cn.setRequestProperty("User-Agent", ": " + this.getUserAgent());
        if (this._acceptEncoding != null) {
            this._cn.setRequestProperty("accept-encoding", this._acceptEncoding);
        }
        this._cn.setDoOutput(bl);
        this._cn.setUseCaches(false);
        this._cn.setDoInput(true);
        if (this._cn instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection)this._cn;
            if (bl) {
                httpURLConnection.setRequestMethod("POST");
            } else {
                httpURLConnection.setRequestMethod("GET");
            }
        }
    }

    private HttpsURLConnection configureHttpsConnection(URLConnection uRLConnection) throws NoSuchAlgorithmException, NoSuchProviderException, IOException, KeyStoreException, KeyManagementException, GeneralSecurityException {
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection)uRLConnection;
        SSLContext sSLContext = SSLContext.getInstance("SSL");
        KeyStore keyStore = MaltegoKeyStore.get();
        MultiKeyStoreTrustManager multiKeyStoreTrustManager = new MultiKeyStoreTrustManager(keyStore);
        SimpleX509TrustManager simpleX509TrustManager = new SimpleX509TrustManager(multiKeyStoreTrustManager, this._trustAllCerts);
        simpleX509TrustManager.setRequireInCN(this._requireCNSuffix);
        sSLContext.init(null, new TrustManager[]{simpleX509TrustManager}, null);
        httpsURLConnection.setSSLSocketFactory(sSLContext.getSocketFactory());
        httpsURLConnection.setHostnameVerifier(new HostnameVerifier(){

            @Override
            public boolean verify(String string, SSLSession sSLSession) {
                if (HttpAgent.this._trustAllCerts) {
                    return true;
                }
                return string.equalsIgnoreCase(sSLSession.getPeerHost());
            }
        });
        return httpsURLConnection;
    }

    public InputStream getInputStream() throws IOException {
        this.parseResponse();
        return this._cn.getInputStream();
    }

    public void disconnect() {
        if (this._cn instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection)this._cn;
            httpURLConnection.disconnect();
        }
    }

    private void tryParseResponse() {
        if (!this._responseParsed) {
            try {
                this.parseResponse();
            }
            catch (IOException var1_1) {
                // empty catch block
            }
        }
    }

    private void parseResponse() throws IOException {
        Object object;
        int n;
        LOG.log(Level.FINE, "{0} Parse response", new Object[]{this.getHash()});
        if (this._cn instanceof HttpURLConnection && (n = (object = (HttpURLConnection)this._cn).getResponseCode()) != 200) {
            throw new IOException("Unexpected HTTP response: " + n);
        }
        this._contentLength = this._cn.getContentLength();
        this._contentEncoding = this._cn.getContentEncoding();
        object = this._cn.getContentType();
        String string = null;
        if (object != null) {
            String[] arrstring = object.split(";");
            this._mimeType = arrstring[0].trim();
            for (int i = 1; i < arrstring.length && string == null; ++i) {
                String string2 = arrstring[i].trim();
                int n2 = string2.toLowerCase().indexOf("charset=");
                if (n2 == -1) continue;
                string = string2.substring(n2 + 8);
            }
        }
        this._characterSet = string;
    }

    public URL getLocation() {
        URL uRL = null;
        String string = this._cn.getHeaderField("Location");
        if (string != null) {
            try {
                uRL = new URL(string);
            }
            catch (MalformedURLException var3_3) {
                // empty catch block
            }
        }
        return uRL;
    }

    private Object readStream(int n, InputStream inputStream, String string) throws IOException {
        int n2 = Math.max(1024, Math.max(n, inputStream.available()));
        byte[] arrby = new byte[n2];
        byte[] arrby2 = null;
        int n3 = inputStream.read(arrby);
        while (n3 != -1) {
            if (arrby2 == null) {
                arrby2 = arrby;
                arrby = new byte[n2];
            } else {
                byte[] arrby3 = new byte[arrby2.length + n3];
                System.arraycopy(arrby2, 0, arrby3, 0, arrby2.length);
                System.arraycopy(arrby, 0, arrby3, arrby2.length, n3);
                arrby2 = arrby3;
            }
            n3 = inputStream.read(arrby);
        }
        if (string == null) {
            return arrby2;
        }
        try {
            return new String(arrby2, string);
        }
        catch (UnsupportedEncodingException var7_8) {
            return arrby2;
        }
    }

    public int getReadTimeout() {
        return this._readTimeout;
    }

    public void setReadTimeout(int n) {
        this._readTimeout = n;
    }

    public int getConnectTimeout() {
        return this._connectTimeout;
    }

    public void setConnectTimeout(int n) {
        this._connectTimeout = n;
    }

    public String getUserAgent() {
        return this._userAgent;
    }

    public void setUserAgent(String string) {
        this._userAgent = string;
    }

    public String getMimeType() {
        this.tryParseResponse();
        return this._mimeType;
    }

    public String getCharacterSet() {
        this.tryParseResponse();
        return this._characterSet;
    }

    public int getContentLength() {
        this.tryParseResponse();
        return this._contentLength;
    }

    public String getContentEncoding() {
        this.tryParseResponse();
        return this._contentEncoding;
    }

    private String getDefaultUserAgent() {
        return String.format("%s v%s.%s ; %s %s", System.getProperty("maltego.product-name"), System.getProperty("maltego.version"), System.getProperty("maltego.buildnumber"), System.getProperty("os.name"), System.getProperty("os.version"));
    }

    public void setRequireCNSuffix(String string) {
        this._requireCNSuffix = string;
    }

    public String getRequireCNSuffix() {
        return this._requireCNSuffix;
    }

    public void setAcceptEncoding(String string) {
        this._acceptEncoding = string;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public X509Certificate getServerCertificate() throws IOException, GeneralSecurityException {
        X509Certificate x509Certificate;
        x509Certificate = null;
        URLConnection uRLConnection = this._url.openConnection();
        if (uRLConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection)uRLConnection;
            try {
                if (uRLConnection instanceof HttpsURLConnection) {
                    HttpsURLConnection httpsURLConnection = this.configureHttpsConnection(uRLConnection);
                    httpsURLConnection.connect();
                    Certificate[] arrcertificate = httpsURLConnection.getServerCertificates();
                    if (arrcertificate != null && arrcertificate.length > 0 && arrcertificate[0] instanceof X509Certificate) {
                        x509Certificate = (X509Certificate)arrcertificate[0];
                    }
                }
            }
            finally {
                httpURLConnection.disconnect();
            }
        }
        return x509Certificate;
    }

    private String getHash() {
        return LOG.isLoggable(Level.FINE) ? Integer.toString(System.identityHashCode(this), 36).toUpperCase() : "";
    }

    static {
        Authenticator authenticator = (Authenticator)Lookup.getDefault().lookup(Authenticator.class);
        if (authenticator != null) {
            Authenticator.setDefault(authenticator);
        }
    }

}

