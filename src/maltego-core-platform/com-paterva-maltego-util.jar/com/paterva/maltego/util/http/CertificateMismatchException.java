/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.http;

import java.security.cert.X509Certificate;

public class CertificateMismatchException
extends Exception {
    private final String _url;
    private final X509Certificate _oldCert;
    private final X509Certificate _newCert;

    public CertificateMismatchException(String string, String string2, X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        super(string);
        this._url = string2;
        this._oldCert = x509Certificate;
        this._newCert = x509Certificate2;
    }

    public String getUrl() {
        return this._url;
    }

    public X509Certificate getOldCertificate() {
        return this._oldCert;
    }

    public X509Certificate getNewCertificate() {
        return this._newCert;
    }
}

