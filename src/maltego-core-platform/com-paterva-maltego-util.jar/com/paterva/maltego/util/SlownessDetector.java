/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

public class SlownessDetector {
    private static final Logger LOG = Logger.getLogger(SlownessDetector.class.getName());
    private static final int INVOKE_DELAY = 1000;
    private static final int DUMP_INTERVAL = 3600000;
    private static final int MAX_DELAY = 10000;
    private static final int LOG_INTERVAL = 10000;
    private static final int SERIOUS_FACTOR = 10;
    private static final AtomicLong _invokeStartTime = new AtomicLong();
    private static final AtomicLong _invokeDoneTime = new AtomicLong();
    private static final AtomicLong _logTime = new AtomicLong();
    private static long _lastDumpTime = 0;
    private static AtomicBoolean _enabled = new AtomicBoolean(true);

    public static void setEnabled(boolean bl) {
        _enabled.set(bl);
        _invokeDoneTime.set(_invokeStartTime.get());
    }

    public static void start() {
        LOG.fine("Starting slowness detector");
        Timer timer = new Timer("Slowness detector timer", true);
        timer.scheduleAtFixedRate(new TimerTask(){

            @Override
            public void run() {
                if (_enabled.get()) {
                    LOG.fine("Checking slowness...");
                    boolean bl = true;
                    long l = _invokeStartTime.get();
                    if (l > 0) {
                        long l2 = _invokeDoneTime.get();
                        LOG.log(Level.FINE, "startTime: {0}", l);
                        LOG.log(Level.FINE, "doneTime: {0}", l2);
                        bl = l2 >= l;
                        long l3 = bl ? l2 - l : System.currentTimeMillis() - l;
                        LOG.log(Level.FINE, "delayTime: {0}", l3);
                        long l4 = System.currentTimeMillis() - _logTime.get();
                        if (l3 > 10000 && l4 > 10000) {
                            SlownessDetector.dumpThreads(l3);
                            _logTime.set(System.currentTimeMillis());
                        }
                    }
                    if (bl) {
                        SlownessDetector.scheduleInvoke();
                    }
                }
            }
        }, 1000, 1000);
    }

    private static void scheduleInvoke() {
        LOG.fine("Scheduling invoke");
        _invokeStartTime.set(System.currentTimeMillis());
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                LOG.fine("Invoke done");
                _invokeDoneTime.set(System.currentTimeMillis());
            }
        });
    }

    private static synchronized void dumpThreads(long l) {
        boolean bl;
        LOG.log(Level.WARNING, "EDT slowness detected: {0}s", l / 1000);
        boolean bl2 = bl = l > 90000 && l < 110000;
        if (bl || System.currentTimeMillis() - _lastDumpTime > 3600000) {
            String string = SlownessDetector.getThreadDump();
            LOG.warning(string);
            _lastDumpTime = System.currentTimeMillis();
        }
    }

    public static String getThreadDump() {
        ThreadInfo[] arrthreadInfo;
        StringBuilder stringBuilder = new StringBuilder();
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        for (ThreadInfo threadInfo : arrthreadInfo = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100)) {
            StackTraceElement[] arrstackTraceElement;
            if (threadInfo == null) continue;
            stringBuilder.append('\"');
            stringBuilder.append(threadInfo.getThreadName());
            stringBuilder.append("\": ");
            Thread.State state = threadInfo.getThreadState();
            stringBuilder.append((Object)state);
            for (StackTraceElement stackTraceElement : arrstackTraceElement = threadInfo.getStackTrace()) {
                stringBuilder.append("\n   at ");
                stringBuilder.append(stackTraceElement);
            }
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }

}

