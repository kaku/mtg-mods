/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util;

import com.paterva.maltego.util.StringUtilities;

public enum IconSize {
    TINY(16),
    SMALL(24),
    MEDIUM(32),
    LARGE(48);
    
    private final int _size;

    private IconSize(int n2) {
        this._size = n2;
    }

    public int getSize() {
        return this._size;
    }

    public String getPostfix() {
        return TINY.equals((Object)this) ? "" : Integer.toString(this._size);
    }

    public String addPostfix(String string) {
        String string2 = this.getPostfix();
        if (string == null || string2.isEmpty()) {
            return string;
        }
        return string.replaceFirst("(\\.[^\\.]*)$", string2 + "$1");
    }

    public static IconSize getSize(String string) {
        if (StringUtilities.isNullOrEmpty(string)) {
            return TINY;
        }
        return IconSize.getSize(Integer.valueOf(string));
    }

    public static IconSize getSize(int n, int n2) {
        return IconSize.getSize(IconSize.positiveMin(n, n2));
    }

    public static IconSize getSize(int n) {
        if (n >= 0) {
            if (n <= 20) {
                return TINY;
            }
            if (n <= 28) {
                return SMALL;
            }
            if (n <= 38) {
                return MEDIUM;
            }
        }
        return LARGE;
    }

    private static int positiveMin(int n, int n2) {
        if (n < 0 && n2 < 0) {
            return -1;
        }
        if (n < 0 && n2 > 0) {
            return n2;
        }
        if (n > 0 && n2 < 0) {
            return n;
        }
        return Math.min(n, n2);
    }
}

