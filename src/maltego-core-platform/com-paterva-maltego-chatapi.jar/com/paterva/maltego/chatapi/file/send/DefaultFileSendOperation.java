/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file.send;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.DefaultFileOperation;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.io.File;

public class DefaultFileSendOperation
extends DefaultFileOperation
implements FileSendOperation {
    private File _file;

    public DefaultFileSendOperation(ChatRoom chatRoom, File file) {
        super(chatRoom, file.getName());
        this._file = file;
    }

    @Override
    public File getFile() {
        return this._file;
    }
}

