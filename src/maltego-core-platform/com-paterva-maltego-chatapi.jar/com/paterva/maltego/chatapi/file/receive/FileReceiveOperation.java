/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file.receive;

import com.paterva.maltego.chatapi.file.FileOperation;

public interface FileReceiveOperation
extends FileOperation {
    public static final String PROP_RESPONSE = "userResponseChanged";

    public int getSizeInBytes();

    public Boolean getUserResponse();

    public void setUserResponse(boolean var1);
}

