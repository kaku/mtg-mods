/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.file.send;

import com.paterva.maltego.chatapi.file.send.DefaultFileSendExecutor;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import org.openide.util.Lookup;

public abstract class FileSendExecutor {
    private static FileSendExecutor _default;

    public static synchronized FileSendExecutor getDefault() {
        if (_default == null && (FileSendExecutor._default = (FileSendExecutor)Lookup.getDefault().lookup(FileSendExecutor.class)) == null) {
            _default = new DefaultFileSendExecutor();
        }
        return _default;
    }

    public abstract Object execute(FileSendOperation var1);

    public abstract void cancel(Object var1);
}

