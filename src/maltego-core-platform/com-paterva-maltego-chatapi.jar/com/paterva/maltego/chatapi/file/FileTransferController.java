/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file;

import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.beans.PropertyChangeListener;
import java.util.List;

public interface FileTransferController {
    public static final String PROP_SEND_NEW = "newFileSendOperation";
    public static final String PROP_SEND_DISCARD = "discardFileSendOperation";
    public static final String PROP_RECEIVE_NEW = "newFileReceiveOperation";
    public static final String PROP_RECEIVE_DISCARD = "discardFileReceiveOperation";

    public List<FileSendOperation> getFileSendOperations();

    public List<FileReceiveOperation> getFileReceiveOperations();

    public void startSendFile(FileSendOperation var1);

    public void startReceiveFile(FileReceiveOperation var1);

    public void discard(FileSendOperation var1);

    public void discard(FileReceiveOperation var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

