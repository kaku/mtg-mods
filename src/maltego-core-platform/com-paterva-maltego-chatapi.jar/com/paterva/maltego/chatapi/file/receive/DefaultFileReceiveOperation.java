/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file.receive;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.DefaultFileOperation;
import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;

public class DefaultFileReceiveOperation
extends DefaultFileOperation
implements FileReceiveOperation {
    private int _sizeInBytes;
    private Boolean _accept = null;

    public DefaultFileReceiveOperation(ChatRoom chatRoom, String string, int n) {
        super(chatRoom, string);
        this._sizeInBytes = n;
    }

    @Override
    public int getSizeInBytes() {
        return this._sizeInBytes;
    }

    @Override
    public Boolean getUserResponse() {
        return this._accept;
    }

    @Override
    public void setUserResponse(boolean bl) {
        if (this._accept == null || this._accept != bl) {
            Boolean bl2 = this._accept;
            this._accept = bl;
            this.firePropertyChange("userResponseChanged", bl2, this._accept);
        }
    }
}

