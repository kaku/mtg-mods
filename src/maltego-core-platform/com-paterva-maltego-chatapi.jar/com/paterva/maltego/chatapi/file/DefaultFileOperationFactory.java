/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.FileOperationFactory;
import com.paterva.maltego.chatapi.file.receive.DefaultFileReceiveOperation;
import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;
import com.paterva.maltego.chatapi.file.send.DefaultFileSendOperation;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.io.File;

public class DefaultFileOperationFactory
extends FileOperationFactory {
    @Override
    public DefaultFileSendOperation createSendOperation(ChatRoom chatRoom, File file) {
        return new DefaultFileSendOperation(chatRoom, file);
    }

    @Override
    public DefaultFileReceiveOperation createReceiveOperation(ChatRoom chatRoom, String string, int n) {
        return new DefaultFileReceiveOperation(chatRoom, string, n);
    }
}

