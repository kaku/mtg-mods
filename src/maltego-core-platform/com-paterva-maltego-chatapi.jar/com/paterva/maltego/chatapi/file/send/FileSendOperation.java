/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file.send;

import com.paterva.maltego.chatapi.file.FileOperation;
import java.io.File;

public interface FileSendOperation
extends FileOperation {
    public File getFile();
}

