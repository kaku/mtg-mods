/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.chatapi.file.send;

import com.paterva.maltego.chatapi.file.send.FileSendExecutor;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import com.paterva.maltego.chatapi.file.send.FileSendRunner;
import com.paterva.maltego.chatapi.file.send.FileSendRunnerFactory;
import org.openide.util.RequestProcessor;

public class DefaultFileSendExecutor
extends FileSendExecutor {
    private RequestProcessor _processor;

    @Override
    public Object execute(FileSendOperation fileSendOperation) {
        FileSendRunner fileSendRunner = FileSendRunnerFactory.getDefault().create(fileSendOperation);
        this.getRequestProcessor().post((Runnable)fileSendRunner);
        return fileSendRunner;
    }

    @Override
    public void cancel(Object object) {
        if (!(object instanceof FileSendRunner)) {
            throw new IllegalArgumentException("Handle must be a FileTransferRunner");
        }
        FileSendRunner fileSendRunner = (FileSendRunner)object;
        fileSendRunner.cancel();
    }

    private RequestProcessor getRequestProcessor() {
        if (this._processor == null) {
            this._processor = new RequestProcessor("File Sending", 10);
        }
        return this._processor;
    }
}

