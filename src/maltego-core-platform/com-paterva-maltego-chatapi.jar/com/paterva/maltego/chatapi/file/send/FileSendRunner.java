/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package com.paterva.maltego.chatapi.file.send;

import com.paterva.maltego.chatapi.file.FileOperationState;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import org.openide.util.Cancellable;

public abstract class FileSendRunner
implements Runnable,
Cancellable {
    private FileSendOperation _fileSendOperation;

    public FileSendRunner(FileSendOperation fileSendOperation) {
        this._fileSendOperation = fileSendOperation;
    }

    public abstract void sendFile();

    public FileSendOperation getFileSendOperation() {
        return this._fileSendOperation;
    }

    @Override
    public void run() {
        this.sendFile();
    }

    public boolean cancel() {
        this.getFileSendOperation().setState(FileOperationState.Cancelled);
        return true;
    }
}

