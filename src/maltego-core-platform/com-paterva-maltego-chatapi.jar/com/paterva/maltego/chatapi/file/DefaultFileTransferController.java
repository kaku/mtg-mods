/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file;

import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;
import com.paterva.maltego.chatapi.file.send.FileSendExecutor;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DefaultFileTransferController
implements FileTransferController {
    private List<FileSendOperation> _fileSendOperations = new ArrayList<FileSendOperation>();
    private List<FileReceiveOperation> _fileReceiveOperations = new ArrayList<FileReceiveOperation>();
    private PropertyChangeSupport _changeSupport;

    public DefaultFileTransferController() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    @Override
    public List<FileSendOperation> getFileSendOperations() {
        return Collections.unmodifiableList(this._fileSendOperations);
    }

    @Override
    public List<FileReceiveOperation> getFileReceiveOperations() {
        return Collections.unmodifiableList(this._fileReceiveOperations);
    }

    @Override
    public void startSendFile(FileSendOperation fileSendOperation) {
        this._fileSendOperations.add(fileSendOperation);
        this.firePropertyChange("newFileSendOperation", null, fileSendOperation);
        FileSendExecutor.getDefault().execute(fileSendOperation);
    }

    @Override
    public void startReceiveFile(FileReceiveOperation fileReceiveOperation) {
        this._fileReceiveOperations.add(fileReceiveOperation);
        this.firePropertyChange("newFileReceiveOperation", null, fileReceiveOperation);
    }

    @Override
    public void discard(FileSendOperation fileSendOperation) {
        this._fileSendOperations.remove(fileSendOperation);
        this.firePropertyChange("discardFileSendOperation", fileSendOperation, null);
    }

    @Override
    public void discard(FileReceiveOperation fileReceiveOperation) {
        this._fileReceiveOperations.remove(fileReceiveOperation);
        this.firePropertyChange("discardFileReceiveOperation", fileReceiveOperation, null);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }
}

