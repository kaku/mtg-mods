/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.file.send;

import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import com.paterva.maltego.chatapi.file.send.FileSendRunner;
import org.openide.util.Lookup;

public abstract class FileSendRunnerFactory {
    private static FileSendRunnerFactory _default;

    public static FileSendRunnerFactory getDefault() {
        if (_default == null && (FileSendRunnerFactory._default = (FileSendRunnerFactory)Lookup.getDefault().lookup(FileSendRunnerFactory.class)) == null) {
            throw new IllegalStateException("No file send runner factories found");
        }
        return _default;
    }

    public abstract FileSendRunner create(FileSendOperation var1);
}

