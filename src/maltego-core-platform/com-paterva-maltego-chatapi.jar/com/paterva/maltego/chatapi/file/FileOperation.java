/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.FileOperationState;
import java.beans.PropertyChangeListener;

public interface FileOperation {
    public static final String PROP_STATE = "stateChanged";
    public static final String PROP_STATUS = "statusChanged";
    public static final String PROP_PROGRESS = "progressChanged";

    public ChatRoom getChatRoom();

    public String getFilename();

    public void setState(FileOperationState var1);

    public FileOperationState getState();

    public void setStatus(String var1);

    public String getStatus();

    public void setProgress(int var1);

    public int getProgress();

    public boolean isCancelled();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

