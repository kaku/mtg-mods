/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.file;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.DefaultFileOperationFactory;
import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.io.File;
import org.openide.util.Lookup;

public abstract class FileOperationFactory {
    private static FileOperationFactory _default;

    public static FileOperationFactory getDefault() {
        if (_default == null && (FileOperationFactory._default = (FileOperationFactory)Lookup.getDefault().lookup(FileOperationFactory.class)) == null) {
            _default = new DefaultFileOperationFactory();
        }
        return _default;
    }

    public abstract FileSendOperation createSendOperation(ChatRoom var1, File var2);

    public abstract FileReceiveOperation createReceiveOperation(ChatRoom var1, String var2, int var3);
}

