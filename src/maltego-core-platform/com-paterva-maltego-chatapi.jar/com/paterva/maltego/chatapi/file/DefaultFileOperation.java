/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.FileOperation;
import com.paterva.maltego.chatapi.file.FileOperationState;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public abstract class DefaultFileOperation
implements FileOperation {
    private ChatRoom _chatRoom;
    private String _filename;
    private FileOperationState _state;
    private String _status = "";
    private int _progress = 0;
    private PropertyChangeSupport _changeSupport;

    public DefaultFileOperation(ChatRoom chatRoom, String string) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._chatRoom = chatRoom;
        this._filename = string;
    }

    @Override
    public ChatRoom getChatRoom() {
        return this._chatRoom;
    }

    @Override
    public String getFilename() {
        return this._filename;
    }

    @Override
    public void setState(FileOperationState fileOperationState) {
        if (this._state != fileOperationState) {
            FileOperationState fileOperationState2 = this._state;
            this._state = fileOperationState;
            this.firePropertyChange("stateChanged", (Object)fileOperationState2, (Object)this._state);
        }
    }

    @Override
    public FileOperationState getState() {
        return this._state;
    }

    @Override
    public void setStatus(String string) {
        if (!string.equals(this._status)) {
            String string2 = this._status;
            this._status = string;
            this.firePropertyChange("statusChanged", string2, this._status);
        }
    }

    @Override
    public String getStatus() {
        return this._status;
    }

    @Override
    public void setProgress(int n) {
        if (this._progress != n) {
            int n2 = this._progress;
            this._progress = n;
            this.firePropertyChange("progressChanged", n2, this._progress);
        }
    }

    @Override
    public int getProgress() {
        return this._progress;
    }

    @Override
    public boolean isCancelled() {
        return this._state == FileOperationState.Cancelled;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }
}

