/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.file;

public enum FileOperationState {
    Prompt,
    Waiting,
    Transferring,
    Done,
    Cancelled;
    

    private FileOperationState() {
    }
}

