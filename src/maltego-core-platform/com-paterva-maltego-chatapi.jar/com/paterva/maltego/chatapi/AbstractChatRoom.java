/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.OutputMessage
 */
package com.paterva.maltego.chatapi;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.DefaultFileTransferController;
import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.msg.ChatMessagePropagator;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.chatapi.user.UserPresence;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractChatRoom
implements ChatRoom {
    private String _name;
    private String _dnsName;
    private Map<User, UserPresence> _users = new HashMap<User, UserPresence>();
    private User _clientUser;
    private PropertyChangeSupport _changeSupport;
    private FileTransferController _fileTransferController;
    private Date _created;

    public AbstractChatRoom() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._fileTransferController = new DefaultFileTransferController();
        this._created = new Date();
    }

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public void setName(String string) {
        this._name = string;
    }

    @Override
    public String getDnsName() {
        return this._dnsName;
    }

    @Override
    public void setDnsName(String string) {
        this._dnsName = string;
    }

    public void add(User user, UserPresence userPresence) {
        this._users.put(user, userPresence);
        this.firePropertyChanged("chatRoomUserAdded", null, user);
    }

    public void remove(User user) {
        this._users.remove(user);
        this.firePropertyChanged("chatRoomUserRemoved", user, null);
    }

    @Override
    public List<User> getUsers() {
        return new ArrayList<User>(this._users.keySet());
    }

    public void setClientUser(User user) {
        this._clientUser = user;
    }

    @Override
    public User getClientUser() {
        return this._clientUser;
    }

    @Override
    public UserPresence getPresence(User user) {
        return this._users.get(user);
    }

    public void setPresence(User user, UserPresence userPresence) {
        this._users.put(user, userPresence);
        this.firePropertyChanged("chatRoomUserPresence", null, user);
    }

    @Override
    public String getAlias(User user) {
        return user.equals(this.getClientUser()) ? String.format("%s (you)", user.getAlias()) : user.getAlias();
    }

    @Override
    public String getCreator() {
        return "<unknown>";
    }

    @Override
    public Date getCreatedDate() {
        return this._created;
    }

    @Override
    public void logMessage(LogMessageLevel logMessageLevel, OutputMessage outputMessage, Date date) {
        this.fireLogMessage(logMessageLevel, outputMessage, date);
    }

    @Override
    public void logMessage(LogMessageLevel logMessageLevel, OutputMessage outputMessage, Color color, Date date) {
        this.fireLogMessage(logMessageLevel, outputMessage, color, date);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    @Override
    public FileTransferController getFileTransferController() {
        return this._fileTransferController;
    }

    protected void fireReceivedChatMessage(User user, OutputMessage outputMessage, Date date) {
        ChatMessagePropagator.chatMessage(this, user, outputMessage, date);
    }

    protected void fireLogMessage(LogMessageLevel logMessageLevel, OutputMessage outputMessage, Date date) {
        ChatMessagePropagator.logMessage(this, logMessageLevel, outputMessage, date);
    }

    protected void fireLogMessage(LogMessageLevel logMessageLevel, OutputMessage outputMessage, Color color, Date date) {
        ChatMessagePropagator.logMessage(this, logMessageLevel, outputMessage, color, date);
    }

    protected void fireUserChanged(User user) {
        this.firePropertyChanged("chatRoomUserChanged", null, user);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }
}

