/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.reconnect;

public enum ReconnectState {
    NONE,
    RECONNECTING,
    WAITING;
    

    private ReconnectState() {
    }
}

