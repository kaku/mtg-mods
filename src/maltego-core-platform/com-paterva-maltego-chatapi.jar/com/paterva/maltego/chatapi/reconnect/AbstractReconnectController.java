/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.chatapi.reconnect;

import com.paterva.maltego.chatapi.reconnect.ReconnectController;
import com.paterva.maltego.chatapi.reconnect.ReconnectProgress;
import com.paterva.maltego.chatapi.reconnect.ReconnectState;
import com.paterva.maltego.util.NormalException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.swing.Timer;
import org.openide.util.RequestProcessor;

public abstract class AbstractReconnectController
implements ReconnectController {
    private ReconnectProgress _progress = new ReconnectProgress();
    private volatile boolean _cancelled = false;
    private RequestProcessor.Task _task;
    private RequestProcessor _rp;
    private int _waitSeconds = 0;
    private Timer _waitTimer;

    public AbstractReconnectController() {
        this._progress.setDescription("Reconnecting");
    }

    protected abstract void doWork() throws Exception;

    protected abstract boolean isConnected();

    @Override
    public void start() {
        if (this._progress.getState() == ReconnectState.RECONNECTING) {
            throw new IllegalStateException("Already trying to reconnect.");
        }
        this._cancelled = false;
        this.stopWaiting();
        this._progress.setProgress(-1);
        this._progress.setState(ReconnectState.RECONNECTING);
        this._task = this.getProcessor().create(new Runnable(){

            @Override
            public void run() {
                block5 : {
                    try {
                        AbstractReconnectController.this._progress.setProgressText("Initializing");
                        if (!AbstractReconnectController.this._cancelled) {
                            AbstractReconnectController.this.doWork();
                        }
                    }
                    catch (Exception var1_1) {
                        if (AbstractReconnectController.this._cancelled) break block5;
                        NormalException.showStackTrace((Throwable)var1_1);
                    }
                }
                if (!AbstractReconnectController.this.isConnected() && !AbstractReconnectController.this._cancelled) {
                    AbstractReconnectController.this.startWaiting();
                } else {
                    AbstractReconnectController.this._progress.setState(ReconnectState.NONE);
                }
            }
        });
        this._task.schedule(0);
    }

    private void startWaiting() {
        if (this._progress.getState() == ReconnectState.WAITING) {
            throw new IllegalStateException("Already waiting to reconnect.");
        }
        this._waitSeconds += 10;
        this._progress.setState(ReconnectState.WAITING);
        this._progress.setWaitTime(this._waitSeconds);
        this._waitTimer = new Timer(1000, new WaitTimerListener(this._waitSeconds));
        this._waitTimer.setRepeats(true);
        this._waitTimer.start();
    }

    private void stopWaiting() {
        if (this._waitTimer != null) {
            this._waitTimer.stop();
            this._waitTimer = null;
        }
    }

    public boolean cancel() {
        System.out.println("CANCELLED");
        this.stopWaiting();
        this._waitSeconds = 0;
        if (this._progress.getState() == ReconnectState.WAITING) {
            this._progress.setState(ReconnectState.NONE);
        }
        this._cancelled = true;
        if (this._task != null) {
            this._task.cancel();
        }
        return true;
    }

    public boolean isCancelled() {
        return this._cancelled;
    }

    @Override
    public ReconnectProgress getReconnectProgress() {
        return this._progress;
    }

    private RequestProcessor getProcessor() {
        if (this._rp == null) {
            this._rp = new RequestProcessor(this.toString(), 1, true);
        }
        return this._rp;
    }

    private class WaitTimerListener
    implements ActionListener {
        private int _secondsLeft;

        private WaitTimerListener(int n) {
            this._secondsLeft = n;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            --this._secondsLeft;
            if (this._secondsLeft > 0) {
                AbstractReconnectController.this._progress.setWaitTime(this._secondsLeft);
            } else {
                AbstractReconnectController.this.start();
            }
        }
    }

}

