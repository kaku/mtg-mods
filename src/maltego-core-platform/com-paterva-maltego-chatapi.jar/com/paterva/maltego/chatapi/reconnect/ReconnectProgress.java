/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.chatapi.reconnect;

import com.paterva.maltego.chatapi.reconnect.ReconnectState;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.util.Utilities;

public class ReconnectProgress {
    public static final String PROP_STATE_CHANGED = "reconnectStateChanged";
    public static final String PROP_DESCRIPTION_CHANGED = "reconnectDescriptionChanged";
    public static final String PROP_PROGRESS_TEXT_CHANGED = "reconnectProgressTextChanged";
    public static final String PROP_PROGRESS_CHANGED = "reconnectProgressChanged";
    public static final String PROP_WAIT_TIME_CHANGED = "reconnectWaitTimeChanged";
    private ReconnectState _state = ReconnectState.NONE;
    private String _description = "";
    private String _progressText = "";
    private int _progress = -1;
    private int _waitTime = 0;
    private PropertyChangeSupport _changeSupport;

    public ReconnectProgress() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public synchronized ReconnectState getState() {
        return this._state;
    }

    public synchronized String getDescription() {
        return this._description;
    }

    public synchronized String getProgressText() {
        return this._progressText;
    }

    public synchronized boolean isDeterminate(int n) {
        return n >= 0;
    }

    public synchronized int getProgress() {
        return this._progress;
    }

    public synchronized int getWaitSeconds() {
        return this._waitTime;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void firePropertyChanged(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }

    public synchronized void setState(ReconnectState reconnectState) {
        if (!Utilities.compareObjects((Object)((Object)this._state), (Object)((Object)reconnectState))) {
            ReconnectState reconnectState2 = this._state;
            this._state = reconnectState;
            this.firePropertyChanged("reconnectStateChanged", (Object)reconnectState2, (Object)this._state);
        }
    }

    public synchronized void setDescription(String string) {
        if (!Utilities.compareObjects((Object)this._description, (Object)string)) {
            String string2 = this._description;
            this._description = string;
            this.firePropertyChanged("reconnectDescriptionChanged", string2, this._description);
        }
    }

    public synchronized void setProgressText(String string) {
        if (!Utilities.compareObjects((Object)this._progressText, (Object)string)) {
            String string2 = this._progressText;
            this._progressText = string;
            this.firePropertyChanged("reconnectProgressTextChanged", string2, this._progressText);
        }
    }

    public synchronized void setProgress(int n) {
        if (this._progress != n) {
            int n2 = this._progress;
            this._progress = n;
            this.firePropertyChanged("reconnectProgressChanged", n2, this._progress);
        }
    }

    public synchronized void setWaitTime(int n) {
        if (this._waitTime != n) {
            int n2 = this._waitTime;
            this._waitTime = n;
            this.firePropertyChanged("reconnectWaitTimeChanged", n2, this._waitTime);
        }
    }
}

