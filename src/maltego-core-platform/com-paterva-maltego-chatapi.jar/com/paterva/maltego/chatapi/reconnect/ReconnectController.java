/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package com.paterva.maltego.chatapi.reconnect;

import com.paterva.maltego.chatapi.reconnect.ReconnectProgress;
import org.openide.util.Cancellable;

public interface ReconnectController
extends Cancellable {
    public void start();

    public ReconnectProgress getReconnectProgress();
}

