/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.conn;

public enum ConnectionStatus {
    None,
    Connecting,
    Connected,
    Blocked,
    Disconnecting,
    Offline;
    

    private ConnectionStatus() {
    }

    public String getDisplayName() {
        String string = this.name();
        if (this == Connecting || this == Disconnecting) {
            string = string + "...";
        }
        return string;
    }

    public static boolean isConnected(ConnectionStatus connectionStatus) {
        return connectionStatus == Connected || connectionStatus == Blocked;
    }
}

