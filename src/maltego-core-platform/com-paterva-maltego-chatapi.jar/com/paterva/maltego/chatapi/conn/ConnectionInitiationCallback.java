/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.conn;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;

public interface ConnectionInitiationCallback {
    public void progress(LogMessageLevel var1, String var2, int var3);

    public void progress(LogMessageLevel var1, String var2);

    public boolean isCancelled();

    public void debug(String var1);
}

