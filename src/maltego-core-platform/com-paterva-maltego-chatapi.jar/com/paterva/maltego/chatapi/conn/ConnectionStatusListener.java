/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.conn;

import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;

public interface ConnectionStatusListener {
    public void statusChanged(ConnectionStatusEvent var1);
}

