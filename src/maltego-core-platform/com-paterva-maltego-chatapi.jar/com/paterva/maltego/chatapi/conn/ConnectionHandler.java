/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.conn;

import com.paterva.maltego.chatapi.conn.ConnectionException;
import com.paterva.maltego.chatapi.conn.ConnectionInfo;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;

public interface ConnectionHandler {
    public void connect(ConnectionInfo var1, ConnectionInitiationCallback var2) throws ConnectionException;

    public void reconnect(ConnectionInitiationCallback var1) throws ConnectionException;

    public void disconnect() throws ConnectionException;

    public void setBlocked(boolean var1) throws ConnectionException;

    public void addConnectionStatusListener(ConnectionStatusListener var1);

    public void removeConnectionStatusListener(ConnectionStatusListener var1);

    public ConnectionStatus getConnectionStatus();
}

