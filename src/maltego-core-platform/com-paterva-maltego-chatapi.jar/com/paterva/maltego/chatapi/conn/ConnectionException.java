/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.chatapi.conn;

import com.paterva.maltego.util.NormalException;

public class ConnectionException
extends NormalException {
    public ConnectionException(NormalException normalException) {
        super(normalException);
    }

    public ConnectionException(String string, boolean bl) {
        super(string, bl);
    }

    public ConnectionException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public ConnectionException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

