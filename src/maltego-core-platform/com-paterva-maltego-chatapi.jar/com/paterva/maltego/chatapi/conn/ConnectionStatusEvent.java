/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.conn;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import java.util.Date;

public class ConnectionStatusEvent {
    private final ConnectionStatus _oldStatus;
    private final ConnectionStatus _newStatus;
    private String _sessionCreatorID;
    private Date _sessionStartDate;

    public ConnectionStatusEvent(ConnectionStatus connectionStatus, ConnectionStatus connectionStatus2) {
        this._oldStatus = connectionStatus;
        this._newStatus = connectionStatus2;
    }

    public ConnectionStatus getOldStatus() {
        return this._oldStatus;
    }

    public ConnectionStatus getNewStatus() {
        return this._newStatus;
    }

    public String getSessionOwnerID() {
        return this._sessionCreatorID;
    }

    public Date getSessionStartDate() {
        return this._sessionStartDate;
    }

    public void setSessionCreatorID(String string) {
        this._sessionCreatorID = string;
    }

    public void setSessionStartDate(Date date) {
        this._sessionStartDate = date;
    }
}

