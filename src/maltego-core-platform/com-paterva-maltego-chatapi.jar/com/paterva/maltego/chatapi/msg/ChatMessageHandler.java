/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.OutputMessage
 */
package com.paterva.maltego.chatapi.msg;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.util.Date;

public interface ChatMessageHandler {
    public void chatMessage(ChatRoom var1, User var2, OutputMessage var3, Date var4);

    public void logMessage(ChatRoom var1, LogMessageLevel var2, OutputMessage var3, Date var4);

    public void logMessage(ChatRoom var1, LogMessageLevel var2, OutputMessage var3, Color var4, Date var5);
}

