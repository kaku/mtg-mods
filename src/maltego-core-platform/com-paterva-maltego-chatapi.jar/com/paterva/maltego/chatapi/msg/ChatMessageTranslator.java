/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.MessageChunk
 *  com.paterva.maltego.util.output.MessageLinkListener
 *  com.paterva.maltego.util.output.OutputMessage
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.msg;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.msg.ChatLinkTranslator;
import com.paterva.maltego.chatapi.msg.ChatLinkTranslators;
import com.paterva.maltego.util.output.MessageChunk;
import com.paterva.maltego.util.output.MessageLinkListener;
import com.paterva.maltego.util.output.OutputMessage;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.util.Lookup;

public class ChatMessageTranslator {
    private static ChatMessageTranslator _default;
    private static final String SPECIAL = "|$()";
    private static final String NOT_SPECIAL = "[^|$()]";
    private static final String ESCAPED_CHAR = "(?:\\$.)";
    private static final String ESAPED_AND_OTHER = "((?:[^|$()]||(?:\\$.))++)";
    private static final Pattern LNK_PATTERN;

    public static synchronized ChatMessageTranslator getDefault() {
        if (_default == null && (ChatMessageTranslator._default = (ChatMessageTranslator)Lookup.getDefault().lookup(ChatMessageTranslator.class)) == null) {
            _default = new ChatMessageTranslator();
        }
        return _default;
    }

    public String translate(ChatRoom chatRoom, OutputMessage outputMessage) {
        StringBuilder stringBuilder = new StringBuilder();
        for (MessageChunk messageChunk : outputMessage.getChunks()) {
            ChatLinkTranslator chatLinkTranslator;
            String string = messageChunk.getText();
            MessageLinkListener messageLinkListener = messageChunk.getLinkListener();
            boolean bl = true;
            if (messageLinkListener != null && (chatLinkTranslator = ChatLinkTranslators.get(messageLinkListener.getTranslatorID())) != null) {
                bl = false;
                stringBuilder.append("$lnk(").append(this.escape(string)).append("|");
                stringBuilder.append(this.escape(chatLinkTranslator.getID())).append("|");
                stringBuilder.append(this.escape(chatLinkTranslator.translate(chatRoom, messageLinkListener)));
                stringBuilder.append(")");
            }
            if (!bl) continue;
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    public OutputMessage translate(ChatRoom chatRoom, String string) {
        String string2;
        int n = 0;
        Matcher matcher = LNK_PATTERN.matcher(string);
        OutputMessage outputMessage = new OutputMessage();
        while (matcher.find()) {
            string2 = string.substring(n, matcher.start());
            outputMessage.addChunk(new MessageChunk(string2));
            String string3 = this.unescape(matcher.group(1));
            String string4 = this.unescape(matcher.group(2));
            String string5 = this.unescape(matcher.group(3));
            MessageLinkListener messageLinkListener = this.translate(chatRoom, string4, string5);
            if (messageLinkListener != null) {
                outputMessage.addChunk(new MessageChunk(string3, messageLinkListener));
            } else {
                outputMessage.addChunk(new MessageChunk(matcher.group(0)));
            }
            n = matcher.end();
        }
        string2 = string.substring(n, string.length());
        outputMessage.addChunk(new MessageChunk(string2));
        return outputMessage;
    }

    private MessageLinkListener translate(ChatRoom chatRoom, String string, String string2) {
        MessageLinkListener messageLinkListener = null;
        try {
            ChatLinkTranslator chatLinkTranslator = ChatLinkTranslators.get(string);
            if (chatLinkTranslator != null) {
                messageLinkListener = chatLinkTranslator.translate(chatRoom, string2);
            }
        }
        catch (Exception var5_6) {
            // empty catch block
        }
        return messageLinkListener;
    }

    private String escape(String string) {
        return string.replace("$", "$$").replace("|", "$|").replace("(", "$(").replace(")", "$)");
    }

    private String unescape(String string) {
        return string.replace("$$", "$").replace("$|", "|").replace("$(", "(").replace("$)", ")");
    }

    static {
        LNK_PATTERN = Pattern.compile("^\\$lnk\\(((?:[^|$()]||(?:\\$.))++)\\|((?:[^|$()]||(?:\\$.))++)\\|((?:[^|$()]||(?:\\$.))++)\\)$");
    }
}

