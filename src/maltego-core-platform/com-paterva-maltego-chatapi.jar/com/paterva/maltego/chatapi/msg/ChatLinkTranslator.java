/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.MessageLinkListener
 */
package com.paterva.maltego.chatapi.msg;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.util.output.MessageLinkListener;

public interface ChatLinkTranslator {
    public String getID();

    public String translate(ChatRoom var1, MessageLinkListener var2);

    public MessageLinkListener translate(ChatRoom var1, String var2);
}

