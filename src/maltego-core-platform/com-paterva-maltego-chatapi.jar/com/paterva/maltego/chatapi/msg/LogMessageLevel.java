/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.msg;

public enum LogMessageLevel {
    Debug,
    Info,
    Warning,
    Error;
    

    private LogMessageLevel() {
    }
}

