/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.OutputMessage
 */
package com.paterva.maltego.chatapi.msg;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.msg.ChatMessageHandler;
import com.paterva.maltego.chatapi.msg.ChatMessageHandlerRegistry;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.util.Collection;
import java.util.Date;
import javax.swing.SwingUtilities;

public class ChatMessagePropagator {
    private ChatMessagePropagator() {
    }

    public static void chatMessage(final ChatRoom chatRoom, final User user, final OutputMessage outputMessage, final Date date) {
        ChatMessagePropagator.runInEDT(new Runnable(){

            @Override
            public void run() {
                Collection<ChatMessageHandler> collection = ChatMessageHandlerRegistry.getDefault().getAll();
                for (ChatMessageHandler chatMessageHandler : collection) {
                    chatMessageHandler.chatMessage(chatRoom, user, outputMessage, date);
                }
            }
        });
    }

    public static void logMessage(final ChatRoom chatRoom, final LogMessageLevel logMessageLevel, final OutputMessage outputMessage, final Date date) {
        ChatMessagePropagator.runInEDT(new Runnable(){

            @Override
            public void run() {
                Collection<ChatMessageHandler> collection = ChatMessageHandlerRegistry.getDefault().getAll();
                for (ChatMessageHandler chatMessageHandler : collection) {
                    chatMessageHandler.logMessage(chatRoom, logMessageLevel, outputMessage, date);
                }
            }
        });
    }

    public static void logMessage(final ChatRoom chatRoom, final LogMessageLevel logMessageLevel, final OutputMessage outputMessage, final Color color, final Date date) {
        ChatMessagePropagator.runInEDT(new Runnable(){

            @Override
            public void run() {
                Collection<ChatMessageHandler> collection = ChatMessageHandlerRegistry.getDefault().getAll();
                for (ChatMessageHandler chatMessageHandler : collection) {
                    chatMessageHandler.logMessage(chatRoom, logMessageLevel, outputMessage, color, date);
                }
            }
        });
    }

    private static void runInEDT(Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

}

