/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.msg;

import com.paterva.maltego.chatapi.msg.ChatMessageHandler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.util.Lookup;

public abstract class ChatMessageHandlerRegistry {
    private static ChatMessageHandlerRegistry _default;

    public static synchronized ChatMessageHandlerRegistry getDefault() {
        if (_default == null && (ChatMessageHandlerRegistry._default = (ChatMessageHandlerRegistry)Lookup.getDefault().lookup(ChatMessageHandlerRegistry.class)) == null) {
            _default = new DefaultChatMessageHandlerRegistry();
        }
        return _default;
    }

    public abstract void add(ChatMessageHandler var1);

    public abstract void remove(ChatMessageHandler var1);

    public abstract Collection<ChatMessageHandler> getAll();

    private static class DefaultChatMessageHandlerRegistry
    extends ChatMessageHandlerRegistry {
        private List<ChatMessageHandler> _handlers = new ArrayList<ChatMessageHandler>();

        private DefaultChatMessageHandlerRegistry() {
        }

        @Override
        public void add(ChatMessageHandler chatMessageHandler) {
            this._handlers.add(chatMessageHandler);
        }

        @Override
        public void remove(ChatMessageHandler chatMessageHandler) {
            this._handlers.remove(chatMessageHandler);
        }

        @Override
        public Collection<ChatMessageHandler> getAll() {
            return Collections.unmodifiableList(this._handlers);
        }
    }

}

