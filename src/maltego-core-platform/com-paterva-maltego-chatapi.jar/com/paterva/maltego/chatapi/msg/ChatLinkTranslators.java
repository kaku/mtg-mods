/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.msg;

import com.paterva.maltego.chatapi.msg.ChatLinkTranslator;
import java.util.Collection;
import org.openide.util.Lookup;

public class ChatLinkTranslators {
    public static Collection<? extends ChatLinkTranslator> getAll() {
        return Lookup.getDefault().lookupAll(ChatLinkTranslator.class);
    }

    public static ChatLinkTranslator get(String string) {
        for (ChatLinkTranslator chatLinkTranslator : ChatLinkTranslators.getAll()) {
            if (!chatLinkTranslator.getID().equals(string)) continue;
            return chatLinkTranslator;
        }
        return null;
    }
}

