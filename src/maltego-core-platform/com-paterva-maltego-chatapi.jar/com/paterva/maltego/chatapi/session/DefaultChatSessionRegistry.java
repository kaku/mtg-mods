/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.session;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.session.ChatSessionRegistry;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DefaultChatSessionRegistry
extends ChatSessionRegistry {
    private Set<ChatRoom> _chatRooms = new HashSet<ChatRoom>();
    private PropertyChangeSupport _support;

    public DefaultChatSessionRegistry() {
        this._support = new PropertyChangeSupport(this);
    }

    @Override
    public void addChatRoom(ChatRoom chatRoom) {
        this._chatRooms.add(chatRoom);
        this.firePropertyChanged("sessionAdded", null, chatRoom);
    }

    @Override
    public void removeChatRoom(ChatRoom chatRoom) {
        this._chatRooms.remove(chatRoom);
        this.firePropertyChanged("sessionRemoved", null, chatRoom);
    }

    @Override
    public Set<ChatRoom> getChatRooms() {
        return Collections.unmodifiableSet(this._chatRooms);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }
}

