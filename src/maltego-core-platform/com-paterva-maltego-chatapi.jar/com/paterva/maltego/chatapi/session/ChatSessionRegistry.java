/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.session;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.session.DefaultChatSessionRegistry;
import java.beans.PropertyChangeListener;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class ChatSessionRegistry {
    public static final String PROP_ADDED = "sessionAdded";
    public static final String PROP_REMOVED = "sessionRemoved";
    private static ChatSessionRegistry _default;

    public static synchronized ChatSessionRegistry getDefault() {
        if (_default == null && (ChatSessionRegistry._default = (ChatSessionRegistry)Lookup.getDefault().lookup(ChatSessionRegistry.class)) == null) {
            _default = new DefaultChatSessionRegistry();
        }
        return _default;
    }

    public abstract void addChatRoom(ChatRoom var1);

    public abstract void removeChatRoom(ChatRoom var1);

    public abstract Set<ChatRoom> getChatRooms();

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

