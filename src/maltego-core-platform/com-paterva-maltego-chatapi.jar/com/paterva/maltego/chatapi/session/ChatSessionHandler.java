/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chatapi.session;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.session.ChatSessionRegistry;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.Lookup;

public abstract class ChatSessionHandler
implements PropertyChangeListener {
    private static ChatSessionHandler _default;

    public static ChatSessionHandler getDefault() {
        if (_default == null && (ChatSessionHandler._default = (ChatSessionHandler)Lookup.getDefault().lookup(ChatSessionHandler.class)) == null) {
            throw new IllegalStateException("No ChatSessionHandler found!");
        }
        return _default;
    }

    public abstract void sessionAdded(ChatRoom var1);

    public abstract void sessionRemoved(ChatRoom var1);

    public ChatSessionHandler() {
        this.init();
    }

    private void init() {
        ChatSessionRegistry.getDefault().addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("sessionAdded".equals(propertyChangeEvent.getPropertyName())) {
            this.sessionAdded((ChatRoom)propertyChangeEvent.getNewValue());
        } else if ("sessionRemoved".equals(propertyChangeEvent.getPropertyName())) {
            this.sessionRemoved((ChatRoom)propertyChangeEvent.getNewValue());
        }
    }
}

