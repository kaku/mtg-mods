/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.chatapi;

import com.paterva.maltego.chatapi.ChatRoom;
import org.openide.nodes.Node;

public class ChatRoomCookie
implements Node.Cookie {
    private ChatRoom _chatRoom;

    public ChatRoomCookie(ChatRoom chatRoom) {
        this._chatRoom = chatRoom;
    }

    public ChatRoom getChatRoom() {
        return this._chatRoom;
    }
}

