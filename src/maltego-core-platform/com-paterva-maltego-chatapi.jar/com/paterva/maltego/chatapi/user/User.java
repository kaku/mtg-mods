/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.user;

import java.util.Date;

public class User {
    private String _alias;
    private String _id;
    private Date _lastSeen;
    private String _statusText;
    private String _clientVersion;

    public User(String string, String string2) {
        this._alias = string2;
        this._id = string;
    }

    public String getID() {
        return this._id;
    }

    public String getAlias() {
        return this._alias;
    }

    public void setAlias(String string) {
        this._alias = string;
    }

    public String toString() {
        return this._alias;
    }

    public boolean equals(Object object) {
        if (object instanceof User) {
            return this.equals((User)object);
        }
        return false;
    }

    public boolean equals(User user) {
        if (user != null) {
            return this._id.equals(user._id);
        }
        return false;
    }

    public int hashCode() {
        int n = 7;
        n = 47 * n + (this._id != null ? this._id.hashCode() : 0);
        return n;
    }

    public void setLastSeen(Date date) {
        this._lastSeen = date;
    }

    public Date getLastSeen() {
        return this._lastSeen;
    }

    public String getStatusText() {
        return this._statusText;
    }

    public void setStatusText(String string) {
        this._statusText = string;
    }

    public String getClientVersion() {
        return this._clientVersion;
    }

    public void setClientVersion(String string) {
        this._clientVersion = string;
    }
}

