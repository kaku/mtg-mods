/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.user;

public enum UserPresence {
    AVAILABLE,
    BUSY,
    AWAY,
    OFFLINE,
    ERROR;
    

    private UserPresence() {
    }
}

