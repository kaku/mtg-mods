/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chatapi.user;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.chatapi.user.UserPresence;
import java.util.Comparator;

public class UserComparator
implements Comparator<User> {
    private ChatRoom _chatRoom;

    public UserComparator() {
        this(null);
    }

    public UserComparator(ChatRoom chatRoom) {
        this._chatRoom = chatRoom;
    }

    @Override
    public int compare(User user, User user2) {
        if (user.equals(user2)) {
            return 0;
        }
        if (this._chatRoom != null) {
            UserPresence userPresence;
            User user3 = this._chatRoom.getClientUser();
            if (user.equals(user3)) {
                return -1;
            }
            if (user2.equals(user3)) {
                return 1;
            }
            UserPresence userPresence2 = this._chatRoom.getPresence(user);
            if (userPresence2 != (userPresence = this._chatRoom.getPresence(user2))) {
                return userPresence2.compareTo(userPresence);
            }
        }
        return user.getAlias().compareTo(user2.getAlias());
    }
}

