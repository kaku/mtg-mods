/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.OutputMessage
 */
package com.paterva.maltego.chatapi;

import com.paterva.maltego.chatapi.conn.ConnectionHandler;
import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.chatapi.user.UserPresence;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

public interface ChatRoom
extends ConnectionHandler {
    public static final String PROP_USER_ADDED = "chatRoomUserAdded";
    public static final String PROP_USER_REMOVED = "chatRoomUserRemoved";
    public static final String PROP_USER_PRESENCE = "chatRoomUserPresence";
    public static final String PROP_USER_CHANGED = "chatRoomUserChanged";
    public static final String PROP_SESSION_INFO_CHANGED = "chatRoomSessionInfoChanged";

    public String getName();

    public void setName(String var1);

    public List<User> getUsers();

    public User getClientUser();

    public UserPresence getPresence(User var1);

    public String getAlias(User var1);

    public String getCreator();

    public Date getCreatedDate();

    public String getDnsName();

    public void setDnsName(String var1);

    public String getServerName();

    public String getServerVersion();

    public String getServerOSVersion();

    public void sendChat(User var1, OutputMessage var2);

    public void sendInfoNotification(String var1);

    public void sendNotification(User var1, String var2, OutputMessage var3);

    public void setStatus(String var1);

    public void logMessage(LogMessageLevel var1, OutputMessage var2, Date var3);

    public void logMessage(LogMessageLevel var1, OutputMessage var2, Color var3, Date var4);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);

    public FileTransferController getFileTransferController();

    public void close();
}

