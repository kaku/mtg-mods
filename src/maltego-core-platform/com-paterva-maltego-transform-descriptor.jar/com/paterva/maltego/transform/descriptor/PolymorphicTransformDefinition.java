/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.PolymorphicDisplayDescriptorCollection;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;

public class PolymorphicTransformDefinition
extends TransformDefinition {
    public PolymorphicTransformDefinition(TransformDescriptor transformDescriptor, TransformSettings transformSettings) {
        super(transformDescriptor, transformSettings);
    }

    @Override
    public DisplayDescriptorCollection getProperties() {
        DisplayDescriptorCollection displayDescriptorCollection = super.getProperties();
        TransformDefinition transformDefinition = this.getBaseDefinition();
        if (transformDefinition != null) {
            return new PolymorphicDisplayDescriptorCollection(displayDescriptorCollection, transformDefinition.getProperties());
        }
        return displayDescriptorCollection;
    }

    protected TransformDefinition getBaseDefinition() {
        TransformRepository transformRepository = this.getRepository();
        if (transformRepository != null && this.getBaseName() != null) {
            return transformRepository.get(this.getBaseName());
        }
        return null;
    }

    protected TransformRepository getRepository() {
        if (this.getRepositoryName() == null) {
            return null;
        }
        return TransformRepositoryRegistry.getDefault().getRepository(this.getRepositoryName());
    }

    @Override
    public Object getValue(PropertyDescriptor propertyDescriptor) {
        TransformDefinition transformDefinition;
        Object object = super.getValue(propertyDescriptor);
        if (object == null && (transformDefinition = this.getBaseDefinition()) != null) {
            object = transformDefinition.getValue(propertyDescriptor);
        }
        return object;
    }
}

