/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Constraint {
    public boolean isSatisfiedByAny(GraphID var1, Set<EntityID> var2, InheritedTypesProvider var3);

    public Set<EntityID> getSatisfiedBy(GraphID var1, Set<EntityID> var2, InheritedTypesProvider var3);

    public Map<EntityID, MaltegoEntity> getSatisfiedBy(GraphID var1, Map<EntityID, MaltegoEntity> var2, InheritedTypesProvider var3);

    public boolean isSatisfiedByAny(Iterable<? extends TypedPropertyBag> var1, InheritedTypesProvider var2);

    public boolean isSatisfiedByInheritedTypes(List<String> var1);

    public String getDisplay();
}

