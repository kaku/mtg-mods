/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

public enum Status {
    Ok,
    Disabled,
    RequiresDisclaimerAccept,
    RequiresKey,
    Untested;
    

    private Status() {
    }

    public String toString() {
        if (this.equals((Object)Ok)) {
            return "Ready";
        }
        if (this.equals((Object)Disabled)) {
            return "Disabled";
        }
        if (this.equals((Object)RequiresDisclaimerAccept)) {
            return "Disclaimer not accepted";
        }
        if (this.equals((Object)RequiresKey)) {
            return "Key required";
        }
        if (this.equals((Object)Untested)) {
            return "Untested";
        }
        return this.name();
    }
}

