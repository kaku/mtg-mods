/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import java.util.EventObject;

public class RepositoryEvent<T>
extends EventObject {
    private T _data;

    public RepositoryEvent(Object object, T t) {
        super(object);
        this._data = t;
    }
}

