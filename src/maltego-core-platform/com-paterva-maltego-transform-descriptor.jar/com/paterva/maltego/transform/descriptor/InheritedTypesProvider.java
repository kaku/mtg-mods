/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import java.util.List;

public interface InheritedTypesProvider {
    public List<String> getAllInheritedTypes(String var1);
}

