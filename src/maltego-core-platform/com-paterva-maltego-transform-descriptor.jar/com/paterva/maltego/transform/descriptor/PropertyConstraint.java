/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;

public class PropertyConstraint {
    private Class _type;
    private boolean _nullable;
    private String _name;

    public PropertyConstraint(Class class_) {
        this(class_, null);
    }

    public PropertyConstraint(String string) {
        this(null, string);
    }

    public PropertyConstraint(Class class_, String string) {
        this(class_, string, true);
    }

    public PropertyConstraint(Class class_, String string, boolean bl) {
        this._type = class_;
        this._name = string;
        this._nullable = bl;
    }

    public Class getType() {
        return this._type;
    }

    public void setType(Class class_) {
        this._type = class_;
    }

    public boolean isNullable() {
        return this._nullable;
    }

    public void setNullable(boolean bl) {
        this._nullable = bl;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public boolean isSatisfiedBy(TypedPropertyBag typedPropertyBag) {
        for (PropertyDescriptor propertyDescriptor : typedPropertyBag.getProperties()) {
            if (this._type != null && propertyDescriptor.getType() != this._type || this._name != null && !this._name.equals(propertyDescriptor.getName())) continue;
            if (this._nullable) {
                return true;
            }
            Object object = typedPropertyBag.getValue(propertyDescriptor);
            boolean bl = object != null;
            return bl;
        }
        return false;
    }

    public String getDisplay() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this._name != null) {
            stringBuilder.append(this._name);
        }
        if (this._name != null && this._type != null) {
            stringBuilder.append(" ");
        }
        if (this._type != null) {
            stringBuilder.append("(" + this._type + ")");
        }
        if (this._nullable) {
            stringBuilder.append("?");
        }
        return stringBuilder.toString();
    }

    public String toString() {
        return this.getDisplay();
    }
}

