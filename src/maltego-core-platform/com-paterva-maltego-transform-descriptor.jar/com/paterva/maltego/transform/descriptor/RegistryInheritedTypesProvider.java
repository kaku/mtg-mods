/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistryInheritedTypesProvider
implements InheritedTypesProvider {
    private final EntityRegistry _registry;
    private final Map<String, List<String>> _cache = new HashMap<String, List<String>>();

    public RegistryInheritedTypesProvider(EntityRegistry entityRegistry) {
        this._registry = entityRegistry;
    }

    @Override
    public List<String> getAllInheritedTypes(String string) {
        List list = this._cache.get(string);
        if (list == null) {
            list = InheritanceHelper.getInheritanceList((SpecRegistry)this._registry, (String)string);
            this._cache.put(string, list);
        }
        return list;
    }
}

