/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import java.util.Comparator;

public class TransformDescriptorComparer
implements Comparator<TransformDescriptor> {
    @Override
    public int compare(TransformDescriptor transformDescriptor, TransformDescriptor transformDescriptor2) {
        return transformDescriptor.getDisplayName().compareTo(transformDescriptor2.getDisplayName());
    }
}

