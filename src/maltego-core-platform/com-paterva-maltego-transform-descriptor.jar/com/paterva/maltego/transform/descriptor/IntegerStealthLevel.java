/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.StealthLevel;

public class IntegerStealthLevel
extends StealthLevel {
    private int _levelNumber;

    public IntegerStealthLevel(int n) {
        this._levelNumber = n;
    }

    public int getLevelNumber() {
        return this._levelNumber;
    }

    public void setLevelNumber(int n) {
        this._levelNumber = n;
    }
}

