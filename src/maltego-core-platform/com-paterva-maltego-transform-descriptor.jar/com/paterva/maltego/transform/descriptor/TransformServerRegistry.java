/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.AbstractRepository;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.util.FastURL;
import java.util.Collection;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformServerRegistry
extends AbstractRepository {
    public static TransformServerRegistry getDefault() {
        TransformServerRegistry transformServerRegistry = (TransformServerRegistry)Lookup.getDefault().lookup(TransformServerRegistry.class);
        if (transformServerRegistry == null) {
            throw new IllegalStateException("Transform Server Registry not found");
        }
        return transformServerRegistry;
    }

    public abstract Set<TransformServerInfo> findServers(String var1, boolean var2);

    public abstract boolean exists(String var1, boolean var2);

    public abstract void put(TransformServerInfo var1);

    public abstract TransformServerInfo get(FastURL var1);

    public abstract Collection<TransformServerInfo> getAll();

    public abstract void remove(FastURL var1);

    public abstract void removeSeedUrl(String var1);

    public abstract void updateSeedUrl(String var1, String var2);
}

