/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

public final class ProtocolVersion
implements Comparable<ProtocolVersion> {
    public static final ProtocolVersion V2_0 = new ProtocolVersion(2, 0);
    private int _major;
    private int _minor;

    public ProtocolVersion(int n, int n2) {
        this._major = n;
        this._minor = n2;
    }

    public static ProtocolVersion get(String string) throws NumberFormatException {
        String[] arrstring = (string = string.trim()).split("\\.");
        if (arrstring.length == 0 || arrstring.length == 1) {
            return new ProtocolVersion(Integer.valueOf(string), 0);
        }
        if (arrstring.length == 2) {
            return new ProtocolVersion(Integer.valueOf(arrstring[0]), Integer.valueOf(arrstring[1]));
        }
        throw new NumberFormatException("Illegal version: " + string);
    }

    public String toString() {
        return "" + this._major + "." + this._minor;
    }

    public int hashCode() {
        int n = 5;
        n = 23 * n + this._major;
        n = 23 * n + this._minor;
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        ProtocolVersion protocolVersion = (ProtocolVersion)object;
        if (this._major != protocolVersion._major) {
            return false;
        }
        if (this._minor != protocolVersion._minor) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(ProtocolVersion protocolVersion) {
        if (this._major == protocolVersion._major) {
            return ProtocolVersion.compare(this._minor, protocolVersion._minor);
        }
        return ProtocolVersion.compare(this._major, protocolVersion._major);
    }

    private static int compare(int n, int n2) {
        if (n > n2) {
            return 1;
        }
        if (n < n2) {
            return -1;
        }
        return 0;
    }
}

