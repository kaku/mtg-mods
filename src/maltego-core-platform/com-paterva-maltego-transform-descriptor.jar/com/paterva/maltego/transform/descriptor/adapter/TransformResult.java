/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 */
package com.paterva.maltego.transform.descriptor.adapter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TransformResult {
    private List<TransformMessage> _messages;
    private int _lastProgress = -1;
    private String _lastProgressMessage;
    private int _entityCount;
    private GraphID _graphID;
    private boolean _transformComplete = false;
    private EntityID _entityID;
    private TransformCompleteNotifier _completeNotifier;
    private static TransformResult EMPTY = new TransformResult();

    public List<TransformMessage> getMessages() {
        return this._messages;
    }

    public int getLastProgress() {
        return this._lastProgress;
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public EntityID getInputEntityID() {
        return this._entityID;
    }

    public int getEntityCount() {
        return this._entityCount;
    }

    public boolean transformComplete() {
        return this._transformComplete;
    }

    public TransformCompleteNotifier getTransformCompleteNotifier() {
        return this._completeNotifier;
    }

    public static TransformResult message(TransformMessage transformMessage) {
        TransformResult transformResult = new TransformResult();
        transformResult._messages = Collections.singletonList(transformMessage);
        return transformResult;
    }

    public static TransformResult messages(List<TransformMessage> list) {
        TransformResult transformResult = new TransformResult();
        transformResult._messages = list;
        return transformResult;
    }

    public static TransformResult info(String string, boolean bl) {
        return TransformResult.message(new TransformMessage(new Date(), TransformMessage.Severity.Info, string, bl));
    }

    public static TransformResult warning(String string) {
        return TransformResult.message(new TransformMessage(new Date(), TransformMessage.Severity.Warning, string));
    }

    public static TransformResult info(String string, int n) {
        TransformResult transformResult = new TransformResult();
        transformResult._messages = Collections.singletonList(new TransformMessage(new Date(), TransformMessage.Severity.Info, string));
        transformResult._lastProgress = n;
        transformResult._lastProgressMessage = string;
        return transformResult;
    }

    public static TransformResult error(Exception exception) {
        return TransformResult.error(exception.getMessage());
    }

    public static TransformResult error(Exception exception, int n) {
        return TransformResult.error(exception.getMessage(), n);
    }

    public static TransformResult error(String string) {
        TransformResult transformResult = TransformResult.message(new TransformMessage(new Date(), TransformMessage.Severity.Error, string));
        transformResult._transformComplete = true;
        return transformResult;
    }

    public static TransformResult error(String string, int n) {
        TransformResult transformResult = TransformResult.message(new TransformMessage(new Date(), TransformMessage.Severity.Error, string, n));
        transformResult._transformComplete = true;
        return transformResult;
    }

    public static TransformResult nonInterruptingError(String string, int n) {
        TransformResult transformResult = TransformResult.message(new TransformMessage(new Date(), TransformMessage.Severity.Error, string, n));
        return transformResult;
    }

    public static TransformResult nonInterruptingError(String string) {
        TransformResult transformResult = TransformResult.message(new TransformMessage(new Date(), TransformMessage.Severity.Error, string));
        return transformResult;
    }

    public static TransformResult progress(String string, int n, GraphID graphID, int n2, boolean bl, EntityID entityID, TransformCompleteNotifier transformCompleteNotifier) {
        TransformResult transformResult = new TransformResult();
        transformResult._graphID = graphID;
        transformResult._entityCount = n2;
        transformResult._lastProgress = n;
        transformResult._lastProgressMessage = string;
        transformResult._transformComplete = bl;
        transformResult._entityID = entityID;
        transformResult._completeNotifier = transformCompleteNotifier;
        return transformResult;
    }

    public static TransformResult empty() {
        return EMPTY;
    }

    public String getLastProgressMessage() {
        return this._lastProgressMessage;
    }

    public String toString() {
        return "" + this._lastProgress + " " + this._lastProgressMessage;
    }

    static {
        TransformResult.EMPTY._transformComplete = true;
    }
}

