/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import java.io.IOException;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformRepositoryRegistry {
    public static final String REMOTE = "Remote";
    public static final String LOCAL = "Local";

    public static TransformRepositoryRegistry getDefault() {
        TransformRepositoryRegistry transformRepositoryRegistry = (TransformRepositoryRegistry)Lookup.getDefault().lookup(TransformRepositoryRegistry.class);
        if (transformRepositoryRegistry == null) {
            transformRepositoryRegistry = new DefaultTransformRepositoryRegistry();
        }
        return transformRepositoryRegistry;
    }

    public abstract TransformDefinition findTransform(String var1);

    public abstract TransformRepository getRepository(String var1);

    public abstract TransformRepository getOrCreateRepository(String var1) throws IOException;

    public abstract Set<TransformDefinition> find(TransformFilter var1);

    public abstract boolean removeTransform(String var1);

    private static class DefaultTransformRepositoryRegistry
    extends TransformRepositoryRegistry {
        private DefaultTransformRepositoryRegistry() {
        }

        @Override
        public TransformDefinition findTransform(String string) {
            throw new UnsupportedOperationException("No Transform Repository Registry found.");
        }

        @Override
        public TransformRepository getRepository(String string) {
            throw new UnsupportedOperationException("No Transform Repository Registry found.");
        }

        @Override
        public Set<TransformDefinition> find(TransformFilter transformFilter) {
            throw new UnsupportedOperationException("No Transform Repository Registry found.");
        }

        @Override
        public TransformRepository getOrCreateRepository(String string) throws IOException {
            throw new UnsupportedOperationException("No Transform Repository Registry found.");
        }

        @Override
        public boolean removeTransform(String string) {
            throw new UnsupportedOperationException("No Transform Repository Registry found.");
        }
    }

}

