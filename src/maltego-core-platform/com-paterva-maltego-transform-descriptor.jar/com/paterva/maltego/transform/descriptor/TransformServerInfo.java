/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.util.FastURL;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TransformServerInfo
implements StatusItem {
    private boolean _enabled = true;
    private boolean _dirty = true;
    private String _name;
    private FastURL _url;
    private Date _lastSync;
    private ProtocolVersion _protocolVersion;
    private Set<String> _transforms;
    private String _description;
    private TransformServerAuthentication _authentication = TransformServerAuthentication.None;
    private List<String> _seedUrls;

    public TransformServerInfo(String string, FastURL fastURL, String string2) {
        this(Collections.singletonList(string), fastURL, string2, "");
    }

    public TransformServerInfo(List<String> list, FastURL fastURL, String string, String string2) {
        this._seedUrls = new ArrayList<String>(list);
        this._url = fastURL;
        this._name = string;
        this._description = string2;
    }

    public List<String> getSeedUrls() {
        return Collections.unmodifiableList(this._seedUrls);
    }

    public void addSeedUrl(String string) {
        if (!this._seedUrls.contains(string)) {
            this._seedUrls.add(string);
            this._dirty = true;
        }
    }

    public boolean removeSeedUrl(String string) {
        boolean bl = this._seedUrls.remove(string);
        if (bl) {
            this._dirty = true;
        }
        return bl;
    }

    public boolean isDirty() {
        return this._dirty;
    }

    public void markClean() {
        this._dirty = false;
    }

    public String getDefaultRepository() {
        return "Remote";
    }

    public String getHelpUrl(TransformDefinition transformDefinition) {
        try {
            return this._url.getURL().getProtocol() + "://" + this._url.getURL().getHost() + "/cgi-bin/transformhelp.pl?transform=" + transformDefinition.getName();
        }
        catch (MalformedURLException var2_2) {
            return transformDefinition.getHelpUrl();
        }
    }

    @Override
    public String getDisplayName() {
        return this._name;
    }

    @Override
    public void setDisplayName(String string) {
        if (this._name == null || !this._name.equals(string)) {
            this._name = string;
            this._dirty = true;
        }
    }

    public FastURL getUrl() {
        return this._url;
    }

    public void setUrl(FastURL fastURL) {
        if (this._url == null || !this._url.equals((Object)fastURL)) {
            this._url = fastURL;
            this._dirty = true;
        }
    }

    public Date getLastSync() {
        return this._lastSync;
    }

    public void setLastSync(Date date) {
        if (this._lastSync == null || !this._lastSync.equals(date)) {
            this._lastSync = date;
            this._dirty = true;
        }
    }

    public ProtocolVersion getProtocolVersion() {
        return this._protocolVersion;
    }

    public void setProtocolVersion(ProtocolVersion protocolVersion) {
        if (this._protocolVersion != protocolVersion) {
            this._protocolVersion = protocolVersion;
            this._dirty = true;
        }
    }

    public Set<String> getTransforms() {
        if (this._transforms == null) {
            this._transforms = new HashSet<String>();
        }
        return this._transforms;
    }

    public boolean equals(Object object) {
        if (object instanceof TransformServerInfo) {
            return this.equals((TransformServerInfo)object);
        }
        return false;
    }

    public int hashCode() {
        int n = 7;
        n = 83 * n + (this._url != null ? this._url.hashCode() : 0);
        return n;
    }

    public String toString() {
        return this.getDisplayName() + ": " + (Object)this.getUrl();
    }

    public boolean equals(TransformServerInfo transformServerInfo) {
        if (transformServerInfo == null) {
            return false;
        }
        return transformServerInfo.getUrl().equals((Object)this.getUrl());
    }

    @Override
    public boolean isEnabled() {
        return this._enabled;
    }

    @Override
    public void setEnabled(boolean bl) {
        if (this._enabled != bl) {
            this._enabled = bl;
            this._dirty = true;
        }
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    @Override
    public void setDescription(String string) {
        if (this._description == null || !this._description.equals(string)) {
            this._description = string;
            this._dirty = true;
        }
    }

    public TransformServerAuthentication getAuthentication() {
        return this._authentication;
    }

    public void setAuthentication(TransformServerAuthentication transformServerAuthentication) {
        if (this._authentication != transformServerAuthentication) {
            this._authentication = transformServerAuthentication;
            this._dirty = true;
        }
    }

    @Override
    public Status getStatus() {
        if (this._enabled) {
            return Status.Ok;
        }
        return Status.Disabled;
    }

    public void setDirty() {
        this._dirty = true;
    }

    @Override
    public String getHtmlDisplayName() {
        return this.getDisplayName();
    }
}

