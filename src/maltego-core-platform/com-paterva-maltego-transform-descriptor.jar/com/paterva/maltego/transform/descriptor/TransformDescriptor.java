/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.util.ListSet
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.transform.descriptor.CompoundConstraint;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.EntityConstraint;
import com.paterva.maltego.transform.descriptor.IntegerStealthLevel;
import com.paterva.maltego.transform.descriptor.StealthLevel;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.util.ListSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class TransformDescriptor
implements Comparable<TransformDescriptor> {
    private String _name;
    private String _base;
    private boolean _abstract = false;
    private boolean _template = false;
    private Visibility _visibility = Visibility.Public;
    private String _displayName;
    private String _description;
    private String _helpUrl;
    private String _helpText;
    private String _author;
    private String _owner;
    private String _disclaimer;
    private String _locationRelevance;
    private String _version;
    private String[] _defaultSets;
    private boolean _requireDisplayInfo;
    private StealthLevel _stealthLevel;
    private DisplayDescriptorCollection _inputs;
    private Set<String> _outputEntities;
    private Constraint _inputConstraint;
    private String _transformAdapterClass;
    private MatchingRuleDescriptor _matchingRule;
    private String _authenticator;

    protected TransformDescriptor(TransformDescriptor transformDescriptor) {
        this(transformDescriptor.getTransformAdapterClass(), transformDescriptor.getName(), transformDescriptor.getBaseName(), transformDescriptor.getProperties());
        this._abstract = transformDescriptor._abstract;
        this._template = transformDescriptor._template;
        this._visibility = transformDescriptor._visibility;
        this._displayName = transformDescriptor._displayName;
        this._description = transformDescriptor._description;
        this._helpUrl = transformDescriptor._helpUrl;
        this._helpText = transformDescriptor._helpText;
        this._author = transformDescriptor._author;
        this._disclaimer = transformDescriptor._disclaimer;
        this._owner = transformDescriptor._owner;
        this._locationRelevance = transformDescriptor._locationRelevance;
        this._version = transformDescriptor._version;
        this._defaultSets = transformDescriptor._defaultSets;
        this._requireDisplayInfo = transformDescriptor._requireDisplayInfo;
        this._stealthLevel = transformDescriptor._stealthLevel;
        this._outputEntities = new ListSet(transformDescriptor.getOutputEntities());
        this._inputConstraint = transformDescriptor._inputConstraint;
        this._matchingRule = transformDescriptor._matchingRule;
        this._authenticator = transformDescriptor._authenticator;
    }

    public TransformDescriptor(String string, String string2, String string3) {
        this(string, string2, string3, null);
    }

    public TransformDescriptor(String string, String string2, String string3, DisplayDescriptorCollection displayDescriptorCollection) {
        this._name = string2;
        this._inputs = displayDescriptorCollection;
        this._base = string3;
        this._transformAdapterClass = string;
    }

    public String getName() {
        return this._name;
    }

    public String toString() {
        return this._name;
    }

    public DisplayDescriptorCollection getProperties() {
        if (this._inputs == null) {
            this._inputs = new DisplayDescriptorList();
        }
        return this._inputs;
    }

    public String getBaseName() {
        return this._base;
    }

    public void setBaseName(String string) {
        this._base = string;
    }

    public Constraint getInputConstraint() {
        return this._inputConstraint;
    }

    public void setInputConstraint(Constraint constraint) {
        this._inputConstraint = constraint;
    }

    public Set<String> getOutputEntities() {
        if (this._outputEntities == null) {
            this._outputEntities = new ListSet();
        }
        return this._outputEntities;
    }

    public boolean isAbstract() {
        return this._abstract;
    }

    public void setAbstract(boolean bl) {
        this._abstract = bl;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getHelpUrl() {
        return this._helpUrl;
    }

    public void setHelpUrl(String string) {
        this._helpUrl = string;
    }

    public String getHelpText() {
        return this._helpText;
    }

    public void setHelpText(String string) {
        this._helpText = string;
    }

    public String getAuthor() {
        return this._author;
    }

    public void setAuthor(String string) {
        this._author = string;
    }

    public String getOwner() {
        return this._owner;
    }

    public void setOwner(String string) {
        this._owner = string;
    }

    public String getDisclaimer() {
        return this._disclaimer;
    }

    public void setDisclaimer(String string) {
        this._disclaimer = string;
    }

    public String getLocationRelevance() {
        return this._locationRelevance;
    }

    public void setLocationRelevance(String string) {
        this._locationRelevance = string;
    }

    public String getVersion() {
        return this._version;
    }

    public void setVersion(String string) {
        this._version = string;
    }

    public String[] getDefaultSets() {
        return this._defaultSets;
    }

    public void setDefaultSets(String[] arrstring) {
        this._defaultSets = arrstring;
    }

    public boolean isRequireDisplayInfo() {
        return this._requireDisplayInfo;
    }

    public void setRequireDisplayInfo(boolean bl) {
        this._requireDisplayInfo = bl;
    }

    public StealthLevel getStealthLevel() {
        return this._stealthLevel;
    }

    public void setStealthLevel(StealthLevel stealthLevel) {
        this._stealthLevel = stealthLevel;
    }

    public boolean isTemplate() {
        return this._template;
    }

    public void setTemplate(boolean bl) {
        this._template = bl;
    }

    public Visibility getVisibility() {
        if (this._visibility == null) {
            return Visibility.Public;
        }
        return this._visibility;
    }

    public void setVisibility(Visibility visibility) {
        this._visibility = visibility;
    }

    public int hashCode() {
        int n = 7;
        n = 89 * n + (this._name != null ? this._name.hashCode() : 0);
        return n;
    }

    public boolean equals(TransformDescriptor transformDescriptor) {
        if (transformDescriptor == null) {
            return false;
        }
        return transformDescriptor._name.equals(this._name);
    }

    public boolean equals(Object object) {
        if (object instanceof TransformDescriptor) {
            return this.equals((TransformDescriptor)object);
        }
        return false;
    }

    public String getTransformAdapterClass() {
        return this._transformAdapterClass;
    }

    @Override
    public int compareTo(TransformDescriptor transformDescriptor) {
        if (transformDescriptor == null) {
            return 1;
        }
        return this.getName().compareTo(transformDescriptor.getName());
    }

    public MatchingRuleDescriptor getMatchingRule() {
        return this._matchingRule;
    }

    public void setMatchingRule(MatchingRuleDescriptor matchingRuleDescriptor) {
        this._matchingRule = matchingRuleDescriptor;
    }

    public String getAuthenticator() {
        return this._authenticator;
    }

    public void setAuthenticator(String string) {
        this._authenticator = string;
    }

    public boolean isCopy(TransformDescriptor transformDescriptor) {
        if (transformDescriptor == null) {
            return false;
        }
        if (!TransformDescriptor.compareStr(this._name, transformDescriptor._name)) {
            return false;
        }
        if (this._inputs == null ? transformDescriptor._inputs != null : !this._inputs.equals((Object)transformDescriptor._inputs)) {
            return false;
        }
        if (this._outputEntities == null ? transformDescriptor._outputEntities != null : !this._outputEntities.equals(transformDescriptor._outputEntities)) {
            return false;
        }
        if (this._stealthLevel != null && transformDescriptor._stealthLevel != null ? !this._stealthLevel.equals(transformDescriptor._stealthLevel) : (this._stealthLevel == null && transformDescriptor._stealthLevel != null ? ((IntegerStealthLevel)transformDescriptor._stealthLevel).getLevelNumber() != 0 : this._stealthLevel != null && transformDescriptor._stealthLevel == null && ((IntegerStealthLevel)this._stealthLevel).getLevelNumber() != 0)) {
            return false;
        }
        if (this._inputConstraint != null && transformDescriptor._inputConstraint != null) {
            CompoundConstraint compoundConstraint = null;
            EntityConstraint entityConstraint = null;
            if (this._inputConstraint.getClass().equals(transformDescriptor._inputConstraint.getClass())) {
                if (this._inputConstraint == null ? transformDescriptor._inputConstraint != null : !this._inputConstraint.equals(transformDescriptor._inputConstraint)) {
                    return false;
                }
            } else if (this._inputConstraint instanceof CompoundConstraint && transformDescriptor._inputConstraint instanceof EntityConstraint) {
                compoundConstraint = (CompoundConstraint)this._inputConstraint;
                entityConstraint = (EntityConstraint)transformDescriptor._inputConstraint;
            } else if (this._inputConstraint instanceof EntityConstraint && transformDescriptor._inputConstraint instanceof CompoundConstraint) {
                compoundConstraint = (CompoundConstraint)transformDescriptor._inputConstraint;
                entityConstraint = (EntityConstraint)this._inputConstraint;
            } else {
                return false;
            }
            if (compoundConstraint != null) {
                Iterator iterator = compoundConstraint.iterator();
                if (!iterator.hasNext()) {
                    return false;
                }
                if (!((EntityConstraint)iterator.next()).equals(entityConstraint)) {
                    return false;
                }
                if (iterator.hasNext()) {
                    return false;
                }
            }
        } else if (this._inputConstraint != transformDescriptor._inputConstraint) {
            return false;
        }
        return this._abstract == transformDescriptor._abstract && this._template == transformDescriptor._template && this._requireDisplayInfo == transformDescriptor._requireDisplayInfo && TransformDescriptor.compareStr(this._name, transformDescriptor._name) && TransformDescriptor.compareStr(this._base, transformDescriptor._base) && TransformDescriptor.compareStr(this._displayName, transformDescriptor._displayName) && TransformDescriptor.compareStr(this._description, transformDescriptor._description) && TransformDescriptor.compareStr(this._helpUrl, transformDescriptor._helpUrl) && TransformDescriptor.compareStr(this._helpText, transformDescriptor._helpText) && TransformDescriptor.compareStr(this._author, transformDescriptor._author) && TransformDescriptor.compareStr(this._owner, transformDescriptor._owner) && TransformDescriptor.compareStr(this._disclaimer, transformDescriptor._disclaimer) && TransformDescriptor.compareStr(this._locationRelevance, transformDescriptor._locationRelevance) && TransformDescriptor.compareStr(this._version, transformDescriptor._version) && TransformDescriptor.compareStr(this._transformAdapterClass, transformDescriptor._transformAdapterClass) && TransformDescriptor.compareStr(this._authenticator, transformDescriptor._authenticator) && Arrays.equals(this._defaultSets, transformDescriptor._defaultSets);
    }

    private static boolean compareStr(String string, String string2) {
        if (string == null) {
            if (string2 == null) {
                return true;
            }
            if (string2.isEmpty()) {
                return true;
            }
        } else if (string2 == null && string.isEmpty()) {
            return true;
        }
        return string.equals(string2);
    }
}

