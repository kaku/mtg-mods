/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.AbstractRepository;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import java.util.Set;

public abstract class TransformRepository
extends AbstractRepository {
    public abstract void updateSettings(TransformDefinition var1);

    public abstract boolean remove(String var1);

    public abstract Set<TransformDefinition> getAll();

    public abstract TransformDefinition get(String var1);

    public abstract Set<TransformDefinition> find(TransformFilter var1);

    public abstract void put(TransformDescriptor var1);
}

