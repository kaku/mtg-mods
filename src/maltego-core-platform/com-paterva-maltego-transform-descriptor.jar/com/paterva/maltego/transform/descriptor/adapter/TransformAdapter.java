/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.typing.DataSource
 */
package com.paterva.maltego.transform.descriptor.adapter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.typing.DataSource;
import java.util.Map;

public interface TransformAdapter {
    public void run(GraphID var1, Map<EntityID, MaltegoEntity> var2, TransformDescriptor var3, String var4, DataSource var5, EntityRegistry var6, EntityFactory var7, LinkFactory var8, TransformCallback var9);

    public void cancel();
}

