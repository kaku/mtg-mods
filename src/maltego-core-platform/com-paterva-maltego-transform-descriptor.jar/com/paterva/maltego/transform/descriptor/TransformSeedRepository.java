/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.util.FastURL;
import java.io.IOException;
import org.openide.util.Lookup;

public abstract class TransformSeedRepository {
    public abstract void add(TransformSeed var1);

    public abstract void update(Iterable<TransformSeed> var1);

    public abstract void remove(FastURL var1);

    public abstract void updateUrl(String var1, String var2);

    public abstract TransformSeed[] getAll();

    public abstract TransformSeed[] getEnabled();

    public abstract TransformSeed get(FastURL var1);

    public abstract void set(TransformSeed[] var1) throws IOException;

    public static synchronized TransformSeedRepository getDefault() {
        TransformSeedRepository transformSeedRepository = (TransformSeedRepository)Lookup.getDefault().lookup(TransformSeedRepository.class);
        if (transformSeedRepository == null) {
            throw new IllegalStateException("Transform Seed Repository not found.");
        }
        return transformSeedRepository;
    }
}

