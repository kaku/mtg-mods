/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.AbstractRepository;
import com.paterva.maltego.transform.descriptor.TransformSet;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformSetRepository
extends AbstractRepository {
    public abstract boolean put(TransformSet var1);

    public abstract boolean remove(String var1);

    public abstract Set<TransformSet> allSets();

    public abstract TransformSet get(String var1);

    public abstract boolean contains(String var1);

    public static synchronized TransformSetRepository getDefault() {
        TransformSetRepository transformSetRepository = (TransformSetRepository)Lookup.getDefault().lookup(TransformSetRepository.class);
        if (transformSetRepository == null) {
            transformSetRepository = new DefaultTransformSetRepository();
        }
        return transformSetRepository;
    }

    private static class DefaultTransformSetRepository
    extends TransformSetRepository {
        private DefaultTransformSetRepository() {
        }

        @Override
        public boolean put(TransformSet transformSet) {
            throw new UnsupportedOperationException("No set repository registered.");
        }

        @Override
        public boolean remove(String string) {
            throw new UnsupportedOperationException("No set repository registered.");
        }

        @Override
        public Set<TransformSet> allSets() {
            throw new UnsupportedOperationException("No set repository registered.");
        }

        @Override
        public TransformSet get(String string) {
            throw new UnsupportedOperationException("No set repository registered.");
        }

        @Override
        public boolean contains(String string) {
            throw new UnsupportedOperationException("No set repository registered.");
        }
    }

}

