/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.collections.CompoundDisplayDescriptorCollection
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.collections.CompoundDisplayDescriptorCollection;

class PolymorphicDisplayDescriptorCollection
extends CompoundDisplayDescriptorCollection {
    public /* varargs */ PolymorphicDisplayDescriptorCollection(DisplayDescriptorCollection ... arrdisplayDescriptorCollection) {
        super(arrdisplayDescriptorCollection);
    }
}

