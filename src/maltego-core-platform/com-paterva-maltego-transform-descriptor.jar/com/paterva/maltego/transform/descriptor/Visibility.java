/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

public enum Visibility {
    Public,
    Internal,
    Protected,
    Hidden;
    

    private Visibility() {
    }
}

