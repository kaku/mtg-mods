/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;

public interface TransformSettings
extends DataSource {
    public boolean isDirty();

    public void markClean();

    public boolean isEnabled();

    public void setEnabled(boolean var1);

    public boolean isDisclaimerAccepted();

    public void setDisclaimerAccepted(boolean var1);

    public boolean showHelp();

    public void setShowHelp(boolean var1);

    public Popup getPopup(PropertyDescriptor var1);

    public void setPopup(PropertyDescriptor var1, Popup var2);

    public Visibility getVisibility(PropertyDescriptor var1);

    public void setVisibility(PropertyDescriptor var1, Visibility var2);

    public boolean isRunWithAll();

    public void setRunWithAll(boolean var1);

    public boolean isFavorite();

    public void setFavorite(boolean var1);
}

