/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.descriptor.favs;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.util.Lookup;

public class TransformFavorites {
    public static final String PROP_FAVORITES_CHANGED = "transforms.favorites";
    public static TransformFavorites _default;
    private final PropertyChangeSupport _changeSupport;

    public TransformFavorites() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public static synchronized TransformFavorites getDefault() {
        if (_default == null && (TransformFavorites._default = (TransformFavorites)Lookup.getDefault().lookup(TransformFavorites.class)) == null) {
            _default = new TransformFavorites();
        }
        return _default;
    }

    public void setFavorite(TransformDefinition transformDefinition, boolean bl) {
        if (transformDefinition.isFavorite() != bl) {
            transformDefinition.setFavorite(bl);
            TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
            TransformRepository transformRepository = transformRepositoryRegistry.getRepository(transformDefinition.getRepositoryName());
            transformRepository.updateSettings(transformDefinition);
            this._changeSupport.firePropertyChange("transforms.favorites", null, null);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }
}

