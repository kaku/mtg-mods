/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.TransformDefinition;

public interface TransformFilter {
    public boolean matches(TransformDefinition var1);
}

