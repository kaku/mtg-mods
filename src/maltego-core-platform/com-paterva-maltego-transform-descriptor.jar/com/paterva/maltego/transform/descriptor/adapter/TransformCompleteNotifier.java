/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.transform.descriptor.adapter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransformCompleteNotifier {
    private static final Logger LOG = Logger.getLogger(TransformCompleteNotifier.class.getName());
    private final String _transformName;
    private final Map<EntityID, MaltegoEntity> _inputEntities;
    private final TransformCallback _cb;
    private int _transformsComplete = 0;

    public TransformCompleteNotifier(String string, Map<EntityID, MaltegoEntity> map, TransformCallback transformCallback) {
        this._transformName = string;
        this._inputEntities = map;
        this._cb = transformCallback;
    }

    public void transformComplete() {
        ++this._transformsComplete;
        LOG.log(Level.FINE, "{0} complete {1} of {2}", new Object[]{this._transformName, this._transformsComplete, this._inputEntities.size()});
        if (this._inputEntities.size() == this._transformsComplete) {
            this._cb.resultReceived(TransformResult.progress(null, 100, null, 0, true, null, null), this._inputEntities);
        }
    }
}

