/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.PersistenceMode;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;

public class TransformPropertyDescriptor
extends DisplayDescriptor {
    private boolean _abstract;
    private PersistenceMode _persistence;
    private Visibility _visibility;
    private boolean _auth;

    public TransformPropertyDescriptor(Class class_, String string) {
        super(class_, string);
    }

    public TransformPropertyDescriptor(Class class_, String string, String string2) {
        super(class_, string, string2);
    }

    public TransformPropertyDescriptor(PropertyDescriptor propertyDescriptor) {
        super(propertyDescriptor);
    }

    public TransformPropertyDescriptor(DisplayDescriptor displayDescriptor) {
        super(displayDescriptor);
    }

    public TransformPropertyDescriptor(TransformPropertyDescriptor transformPropertyDescriptor) {
        super((DisplayDescriptor)transformPropertyDescriptor);
        this._abstract = transformPropertyDescriptor._abstract;
        this._persistence = transformPropertyDescriptor._persistence;
        this._visibility = transformPropertyDescriptor._visibility;
        this._auth = transformPropertyDescriptor._auth;
    }

    public boolean isAbstract() {
        return this._abstract;
    }

    public void setAbstract(boolean bl) {
        this._abstract = bl;
    }

    public PersistenceMode getPersistence() {
        if (this._persistence == null) {
            return PersistenceMode.Normal;
        }
        return this._persistence;
    }

    public void setPersistence(PersistenceMode persistenceMode) {
        this._persistence = persistenceMode;
    }

    public Visibility getVisibility() {
        if (this._visibility == null) {
            return Visibility.Public;
        }
        return this._visibility;
    }

    public void setVisibility(Visibility visibility) {
        this._visibility = visibility;
    }

    public boolean isInternal() {
        return this.getVisibility() == Visibility.Internal;
    }

    public boolean isAuth() {
        return this._auth;
    }

    public void setAuth(boolean bl) {
        this._auth = bl;
    }
}

