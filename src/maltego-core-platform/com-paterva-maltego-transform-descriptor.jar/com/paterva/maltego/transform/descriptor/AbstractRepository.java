/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AbstractRepository {
    private final List<RepositoryListener> _listeners = Collections.synchronizedList(new LinkedList());

    public void addRepositoryListener(RepositoryListener repositoryListener) {
        this._listeners.add(repositoryListener);
    }

    public void removeRepositoryListener(RepositoryListener repositoryListener) {
        this._listeners.remove(repositoryListener);
    }

    protected void fireItemAdded(Object object) {
        RepositoryEvent<Object> repositoryEvent = new RepositoryEvent<Object>(this, object);
        List<RepositoryListener> list = this.getListeners();
        for (RepositoryListener repositoryListener : list) {
            repositoryListener.itemAdded(repositoryEvent);
        }
    }

    protected void fireItemRemoved(Object object) {
        RepositoryEvent<Object> repositoryEvent = new RepositoryEvent<Object>(this, object);
        List<RepositoryListener> list = this.getListeners();
        for (RepositoryListener repositoryListener : list) {
            repositoryListener.itemRemoved(repositoryEvent);
        }
    }

    protected void fireItemChanged(Object object) {
        RepositoryEvent<Object> repositoryEvent = new RepositoryEvent<Object>(this, object);
        List<RepositoryListener> list = this.getListeners();
        for (RepositoryListener repositoryListener : list) {
            repositoryListener.itemChanged(repositoryEvent);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<RepositoryListener> getListeners() {
        ArrayList<RepositoryListener> arrayList;
        List<RepositoryListener> list = this._listeners;
        synchronized (list) {
            arrayList = new ArrayList<RepositoryListener>(this._listeners);
        }
        return arrayList;
    }
}

