/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.Status;

public interface StatusItem {
    public String getDisplayName();

    public String getHtmlDisplayName();

    public void setDisplayName(String var1);

    public String getDescription();

    public void setDescription(String var1);

    public boolean isEnabled();

    public void setEnabled(boolean var1);

    public Status getStatus();
}

