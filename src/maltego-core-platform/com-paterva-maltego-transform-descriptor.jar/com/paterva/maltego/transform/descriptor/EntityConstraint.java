/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.PropertyConstraint;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public class EntityConstraint
implements Constraint {
    private String _typeName;
    private int _minimum = 0;
    private int _maximum = 0;
    private Collection<PropertyConstraint> _properties;

    public EntityConstraint() {
    }

    public EntityConstraint(String string) {
        this(string, 1, 1);
    }

    public EntityConstraint(String string, int n, int n2) {
        this(string, n, n2, null);
    }

    public EntityConstraint(String string, int n, int n2, Collection<PropertyConstraint> collection) {
        this._typeName = string;
        this._minimum = n;
        this._maximum = n2;
        this._properties = collection;
    }

    public String getTypeName() {
        return this._typeName;
    }

    public void setTypeName(String string) {
        this._typeName = string;
    }

    public int getMinimum() {
        return this._minimum;
    }

    public void setMinimum(int n) {
        this._minimum = n;
    }

    public int getMaximum() {
        return this._maximum;
    }

    public void setMaximum(int n) {
        this._maximum = n;
    }

    public Collection<PropertyConstraint> getProperties() {
        if (this._properties == null) {
            this._properties = new LinkedList<PropertyConstraint>();
        }
        return this._properties;
    }

    public void setProperties(Collection<PropertyConstraint> collection) {
        this._properties = collection;
    }

    @Override
    public boolean isSatisfiedByAny(Iterable<? extends TypedPropertyBag> iterable, InheritedTypesProvider inheritedTypesProvider) {
        int n = this._minimum < 1 ? 1 : this._minimum;
        int n2 = 0;
        for (TypedPropertyBag typedPropertyBag : iterable) {
            if (n2 >= n) {
                return true;
            }
            if (!this.isSatisfiedByType(typedPropertyBag.getTypeName(), inheritedTypesProvider) || !this.isSatisfiedByProperties(typedPropertyBag)) continue;
            ++n2;
        }
        return n2 >= n;
    }

    @Override
    public boolean isSatisfiedByAny(GraphID graphID, Set<EntityID> set, InheritedTypesProvider inheritedTypesProvider) {
        int n = this._minimum < 1 ? 1 : this._minimum;
        int n2 = 0;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            for (EntityID entityID : set) {
                if (this.isSatisfiedByAny(entityID, graphDataStoreReader, inheritedTypesProvider)) {
                    ++n2;
                }
                if (n2 < n) continue;
                return true;
            }
        }
        catch (GraphStoreException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        return n2 >= n;
    }

    private boolean isSatisfiedByAny(EntityID entityID, GraphDataStoreReader graphDataStoreReader, InheritedTypesProvider inheritedTypesProvider) throws GraphStoreException {
        String string = graphDataStoreReader.getEntityType(entityID);
        boolean bl = false;
        if (this.isSatisfiedByType(string, inheritedTypesProvider)) {
            if (this._properties == null) {
                bl = true;
            } else {
                MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
                bl = this.isSatisfiedByProperties((TypedPropertyBag)maltegoEntity);
            }
        }
        return bl;
    }

    private boolean isSatisfiedByAny(MaltegoEntity maltegoEntity, InheritedTypesProvider inheritedTypesProvider) throws GraphStoreException {
        String string = maltegoEntity.getTypeName();
        boolean bl = false;
        if (this.isSatisfiedByType(string, inheritedTypesProvider)) {
            bl = this._properties == null ? true : this.isSatisfiedByProperties((TypedPropertyBag)maltegoEntity);
        }
        return bl;
    }

    @Override
    public Set<EntityID> getSatisfiedBy(GraphID graphID, Set<EntityID> set, InheritedTypesProvider inheritedTypesProvider) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            for (EntityID entityID : set) {
                if (!this.isSatisfiedByAny(entityID, graphDataStoreReader, inheritedTypesProvider)) continue;
                hashSet.add(entityID);
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return hashSet;
    }

    @Override
    public Map<EntityID, MaltegoEntity> getSatisfiedBy(GraphID graphID, Map<EntityID, MaltegoEntity> map, InheritedTypesProvider inheritedTypesProvider) {
        HashMap<EntityID, MaltegoEntity> hashMap = new HashMap<EntityID, MaltegoEntity>();
        try {
            for (Map.Entry<EntityID, MaltegoEntity> entry : map.entrySet()) {
                EntityID entityID = entry.getKey();
                MaltegoEntity maltegoEntity = entry.getValue();
                if (!this.isSatisfiedByAny(maltegoEntity, inheritedTypesProvider)) continue;
                hashMap.put(entityID, maltegoEntity);
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return hashMap;
    }

    private boolean isSatisfiedByType(String string, InheritedTypesProvider inheritedTypesProvider) {
        if (this._typeName == null) {
            return true;
        }
        if (inheritedTypesProvider != null) {
            List<String> list = inheritedTypesProvider.getAllInheritedTypes(string);
            return list.contains(this._typeName);
        }
        return this._typeName.equals(string);
    }

    @Override
    public boolean isSatisfiedByInheritedTypes(List<String> list) {
        if (this._typeName == null) {
            return true;
        }
        return list.contains(this._typeName);
    }

    private boolean isSatisfiedByProperties(TypedPropertyBag typedPropertyBag) {
        if (this._properties == null) {
            return true;
        }
        for (PropertyConstraint propertyConstraint : this._properties) {
            boolean bl = propertyConstraint.isSatisfiedBy(typedPropertyBag);
            if (bl) continue;
            return false;
        }
        return true;
    }

    @Override
    public String getDisplay() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this._typeName != null) {
            stringBuilder.append(this._typeName);
        }
        if (this._typeName == null && (this._properties == null || this._properties.isEmpty())) {
            stringBuilder.append("Any");
        }
        if (this._minimum == this._maximum) {
            if (this._minimum != 1) {
                stringBuilder.append("(").append(this._minimum).append(")");
            }
        } else {
            stringBuilder.append("(").append(this._minimum).append(":").append(this._maximum).append(")");
        }
        if (this._properties != null && !this._properties.isEmpty()) {
            stringBuilder.append("[");
            int n = 0;
            for (PropertyConstraint propertyConstraint : this._properties) {
                stringBuilder.append(propertyConstraint.getDisplay());
                if (n < this._properties.size() - 1) {
                    stringBuilder.append(" ");
                }
                ++n;
            }
            stringBuilder.append("]");
        }
        return stringBuilder.toString();
    }

    public String toString() {
        return this.getDisplay();
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        EntityConstraint entityConstraint = (EntityConstraint)object;
        if (this._typeName == null ? entityConstraint._typeName != null : !this._typeName.equals(entityConstraint._typeName)) {
            return false;
        }
        if (this._minimum != entityConstraint._minimum) {
            return false;
        }
        if (this._maximum != entityConstraint._maximum) {
            return false;
        }
        return this.getProperties().equals(entityConstraint.getProperties());
    }

    public int hashCode() {
        int n = 7;
        n = 97 * n + (this._typeName != null ? this._typeName.hashCode() : 0);
        n = 97 * n + this._minimum;
        n = 97 * n + this._maximum;
        n = 97 * n + this.getProperties().hashCode();
        return n;
    }
}

