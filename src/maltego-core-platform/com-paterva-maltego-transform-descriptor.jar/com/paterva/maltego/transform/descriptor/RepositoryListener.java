/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import java.util.EventListener;

public interface RepositoryListener
extends EventListener {
    public void itemAdded(RepositoryEvent var1);

    public void itemChanged(RepositoryEvent var1);

    public void itemRemoved(RepositoryEvent var1);
}

