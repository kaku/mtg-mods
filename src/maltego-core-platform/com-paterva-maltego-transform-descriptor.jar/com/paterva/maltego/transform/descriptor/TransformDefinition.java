/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class TransformDefinition
extends TransformDescriptor
implements TransformSettings,
StatusItem {
    public static final String PROP_ENABLED = "enabled";
    public static final String PROP_DIRTY = "dirty";
    public static final String PROP_DISCLAIMER_ACCEPTED = "disclaimerAccepted";
    public static final String PROP_INPUT_VISIBILITY_CHANGED = "inputVisibilityChanged";
    public static final String PROP_INPUT_POPUP_CHANGED = "inputPopupChanged";
    public static final String PROP_RUN_WITH_ALL = "runWithAll";
    public static final String PROP_FAVORITE = "favorite";
    private final TransformSettings _settings;
    private String _repositoryName;
    private Collection<PropertyChangeListener> _listeners;

    public TransformDefinition(TransformDescriptor transformDescriptor, TransformSettings transformSettings) {
        super(transformDescriptor);
        this._settings = transformSettings;
    }

    @Override
    public boolean isEnabled() {
        return this._settings.isEnabled();
    }

    @Override
    public void setEnabled(boolean bl) {
        if (this.isEnabled() != bl) {
            this._settings.setEnabled(bl);
            this.firePropertyChange("enabled", !bl, bl);
        }
    }

    @Override
    public boolean isDisclaimerAccepted() {
        return this._settings.isDisclaimerAccepted();
    }

    @Override
    public void setDisclaimerAccepted(boolean bl) {
        if (this.isDisclaimerAccepted() != bl) {
            this._settings.setDisclaimerAccepted(bl);
            this.firePropertyChange("disclaimerAccepted", !bl, bl);
        }
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        DisplayDescriptor displayDescriptor;
        Object object = this._settings.getValue(propertyDescriptor);
        if (object == null && (displayDescriptor = this.getProperties().get(propertyDescriptor.getName())) != null) {
            return displayDescriptor.getDefaultValue();
        }
        return object;
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        Object object2 = this.getValue(propertyDescriptor);
        if (object != object2) {
            this._settings.setValue(propertyDescriptor, object);
            this.firePropertyChange(propertyDescriptor.getName(), object2, object);
        }
    }

    @Override
    public Popup getPopup(PropertyDescriptor propertyDescriptor) {
        return this._settings.getPopup(propertyDescriptor);
    }

    private boolean showPopup(PropertyDescriptor propertyDescriptor, DataSource dataSource) {
        if (propertyDescriptor.isHidden()) {
            return false;
        }
        Popup popup = this._settings.getPopup(propertyDescriptor);
        if (popup == Popup.Yes) {
            return true;
        }
        DisplayDescriptor displayDescriptor = this.getProperties().get(propertyDescriptor.getName());
        if (displayDescriptor != null) {
            return !displayDescriptor.isNullable() && this.isNull(this.getValue((PropertyDescriptor)displayDescriptor)) && this.isNull(dataSource.getValue(propertyDescriptor));
        }
        return true;
    }

    public DisplayDescriptorCollection getPopupProperties(DataSource dataSource) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        for (DisplayDescriptor displayDescriptor : this.getProperties()) {
            if (displayDescriptor == null || !this.showPopup((PropertyDescriptor)displayDescriptor, dataSource)) continue;
            displayDescriptorList.add(displayDescriptor);
        }
        return displayDescriptorList;
    }

    @Override
    public void setPopup(PropertyDescriptor propertyDescriptor, Popup popup) {
        Popup popup2 = this.getPopup(propertyDescriptor);
        if (popup != popup2) {
            this._settings.setPopup(propertyDescriptor, popup);
            this.firePropertyChange("inputPopupChanged", null, null);
        }
    }

    @Override
    public boolean showHelp() {
        return this._settings.showHelp();
    }

    @Override
    public void setShowHelp(boolean bl) {
        this._settings.setShowHelp(bl);
    }

    @Override
    public Visibility getVisibility(PropertyDescriptor propertyDescriptor) {
        Visibility visibility = this._settings.getVisibility(propertyDescriptor);
        return visibility;
    }

    @Override
    public void setVisibility(PropertyDescriptor propertyDescriptor, Visibility visibility) {
        Visibility visibility2 = this.getVisibility(propertyDescriptor);
        if (visibility != visibility2) {
            this._settings.setVisibility(propertyDescriptor, visibility);
            this.firePropertyChange("inputVisibilityChanged", null, null);
        }
    }

    @Override
    public Status getStatus() {
        if (!this.isEnabled()) {
            return Status.Disabled;
        }
        if (!StringUtilities.isNullOrEmpty((String)this.getDisclaimer()) && !this.isDisclaimerAccepted()) {
            return Status.RequiresDisclaimerAccept;
        }
        return Status.Ok;
    }

    @Override
    public String getHtmlDisplayName() {
        String string;
        String string2 = "<html><font color=\"%s\">" + this.getDisplayName() + "</font></html>";
        Status status = this.getStatus();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        switch (status) {
            case Disabled: {
                string = ColorUtilities.encode((Color)uIDefaults.getColor("7-gray-50"));
                break;
            }
            case RequiresDisclaimerAccept: {
                string = ColorUtilities.encode((Color)uIDefaults.getColor("7-darkest-red"));
                break;
            }
            default: {
                string = ColorUtilities.encode((Color)uIDefaults.getColor("7-black"));
            }
        }
        return String.format(string2, string);
    }

    public String getRepositoryName() {
        return this._repositoryName;
    }

    public void setRepositoryName(String string) {
        this._repositoryName = string;
    }

    @Override
    public boolean isRunWithAll() {
        return this._settings.isRunWithAll();
    }

    @Override
    public void setRunWithAll(boolean bl) {
        if (this.isRunWithAll() != bl) {
            this._settings.setRunWithAll(bl);
            this.firePropertyChange("runWithAll", !bl, bl);
        }
    }

    @Override
    public boolean isFavorite() {
        return this._settings.isFavorite();
    }

    @Override
    public void setFavorite(boolean bl) {
        if (this.isFavorite() != bl) {
            this._settings.setFavorite(bl);
            this.firePropertyChange("favorite", !bl, bl);
        }
    }

    @Override
    public int hashCode() {
        int n = 7;
        n = 97 * n + super.hashCode();
        n = 97 * n + (this.getRepositoryName() != null ? this.getRepositoryName().hashCode() : 0);
        return n;
    }

    public boolean equals(TransformDefinition transformDefinition) {
        if (transformDefinition == null) {
            return false;
        }
        return super.equals(transformDefinition) && transformDefinition.getRepositoryName().equals(this.getRepositoryName());
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof TransformDefinition) {
            return this.equals((TransformDefinition)object);
        }
        return false;
    }

    @Override
    public boolean isDirty() {
        return this._settings.isDirty();
    }

    @Override
    public void markClean() {
        boolean bl = this.isDirty();
        this._settings.markClean();
        boolean bl2 = this.isDirty();
        if (bl != bl2) {
            this.firePropertyChange("dirty", bl, bl2);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (this._listeners == null) {
            this._listeners = Collections.synchronizedCollection(new LinkedList());
        }
        this._listeners.add(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (this._listeners != null) {
            this._listeners.remove(propertyChangeListener);
        }
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        if (this._listeners != null) {
            PropertyChangeEvent propertyChangeEvent = new PropertyChangeEvent(this, string, object, object2);
            for (PropertyChangeListener propertyChangeListener : this._listeners) {
                propertyChangeListener.propertyChange(propertyChangeEvent);
            }
        }
    }

    private boolean isNull(Object object) {
        if (object instanceof String) {
            return StringUtilities.isNullOrEmpty((String)((String)object));
        }
        return object == null;
    }

}

