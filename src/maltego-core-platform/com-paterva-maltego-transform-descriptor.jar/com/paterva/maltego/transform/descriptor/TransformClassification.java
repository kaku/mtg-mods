/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.descriptor;

public enum TransformClassification {
    Search("search"),
    Enumeration("enumeration");
    
    private String _name;

    private TransformClassification(String string2) {
        this._name = string2;
    }

    public String getTypeName() {
        return this._name;
    }
}

