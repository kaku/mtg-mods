/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CompoundConstraint<T extends Constraint>
implements Constraint,
Iterable<T> {
    private List<T> _constraints;

    public CompoundConstraint() {
    }

    public CompoundConstraint(CompoundConstraint<T> compoundConstraint) {
        this.constraints().addAll(CompoundConstraint.super.constraints());
    }

    public void add(T t) {
        this.constraints().add(t);
    }

    @Override
    public Iterator<T> iterator() {
        return this.constraints().iterator();
    }

    private List<T> constraints() {
        if (this._constraints == null) {
            this._constraints = new LinkedList<T>();
        }
        return this._constraints;
    }

    public void remove(T t) {
        if (this._constraints != null) {
            this._constraints.remove(t);
        }
    }

    @Override
    public boolean isSatisfiedByAny(Iterable<? extends TypedPropertyBag> iterable, InheritedTypesProvider inheritedTypesProvider) {
        if (this._constraints == null) {
            return true;
        }
        for (Constraint constraint : this._constraints) {
            boolean bl = constraint.isSatisfiedByAny(iterable, inheritedTypesProvider);
            if (bl) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean isSatisfiedByAny(GraphID graphID, Set<EntityID> set, InheritedTypesProvider inheritedTypesProvider) {
        if (this._constraints == null) {
            return true;
        }
        for (Constraint constraint : this._constraints) {
            boolean bl = constraint.isSatisfiedByAny(graphID, set, inheritedTypesProvider);
            if (bl) continue;
            return false;
        }
        return true;
    }

    @Override
    public Set<EntityID> getSatisfiedBy(GraphID graphID, Set<EntityID> set, InheritedTypesProvider inheritedTypesProvider) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        if (this._constraints == null) {
            for (EntityID entityID : set) {
                hashSet.add(entityID);
            }
        } else {
            for (Constraint constraint : this._constraints) {
                hashSet.addAll(constraint.getSatisfiedBy(graphID, set, inheritedTypesProvider));
            }
        }
        return hashSet;
    }

    @Override
    public Map<EntityID, MaltegoEntity> getSatisfiedBy(GraphID graphID, Map<EntityID, MaltegoEntity> map, InheritedTypesProvider inheritedTypesProvider) {
        HashMap<EntityID, MaltegoEntity> hashMap = new HashMap<EntityID, MaltegoEntity>();
        if (this._constraints == null) {
            for (Map.Entry entry : hashMap.entrySet()) {
                EntityID entityID = (EntityID)entry.getKey();
                MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getValue();
                hashMap.put(entityID, maltegoEntity);
            }
        } else {
            for (Constraint constraint : this._constraints) {
                hashMap.putAll(constraint.getSatisfiedBy(graphID, map, inheritedTypesProvider));
            }
        }
        return hashMap;
    }

    @Override
    public boolean isSatisfiedByInheritedTypes(List<String> list) {
        if (this._constraints == null) {
            return true;
        }
        for (Constraint constraint : this._constraints) {
            boolean bl = constraint.isSatisfiedByInheritedTypes(list);
            if (bl) continue;
            return false;
        }
        return true;
    }

    @Override
    public String getDisplay() {
        if (this._constraints == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        int n = 0;
        for (Constraint constraint : this._constraints) {
            stringBuilder.append(constraint.getDisplay());
            if (n < this._constraints.size() - 1) {
                stringBuilder.append(", ");
            }
            ++n;
        }
        return stringBuilder.toString();
    }

    public String toString() {
        return this.getDisplay();
    }
}

