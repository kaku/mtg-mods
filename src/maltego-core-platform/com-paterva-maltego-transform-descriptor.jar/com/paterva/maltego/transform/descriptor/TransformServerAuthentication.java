/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.MachineIDProvider
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.transform.descriptor.TransformApiKeyProvider;
import com.paterva.maltego.util.MachineIDProvider;

public abstract class TransformServerAuthentication {
    public static final TransformServerAuthentication None = new NoAuth();
    public static final TransformServerAuthentication MAC = new MacAuth();
    public static final TransformServerAuthentication License = new LicenseAuth();
    private String _type;
    private String _displayName;

    protected TransformServerAuthentication(String string, String string2) {
        this._type = string;
        this._displayName = string2;
    }

    public String getTypeName() {
        return this._type;
    }

    public String toString() {
        return this.getDisplayName();
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public abstract String getToken(String var1);

    public static TransformServerAuthentication get(String string) {
        if (MAC.getTypeName().equals(string)) {
            return MAC;
        }
        if (License.getTypeName().equals(string)) {
            return License;
        }
        return None;
    }

    public static class LicenseAuth
    extends TransformServerAuthentication {
        public LicenseAuth() {
            super("license", "License");
        }

        @Override
        public String getToken(String string) {
            return TransformApiKeyProvider.getDefault().getKey(string);
        }
    }

    private static class MacAuth
    extends TransformServerAuthentication {
        public MacAuth() {
            super("mac", "MAC Address");
        }

        @Override
        public String getToken(String string) {
            return MachineIDProvider.getDefault().getUniqueID();
        }
    }

    private static class NoAuth
    extends TransformServerAuthentication {
        public NoAuth() {
            super("none", "None");
        }

        @Override
        public String getToken(String string) {
            return "";
        }
    }

}

