/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.transform.descriptor.adapter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import java.util.Map;

public interface TransformCallback {
    public void resultReceived(TransformResult var1, Map<EntityID, MaltegoEntity> var2);

    public void cancelled();
}

