/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ListMap
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.util.ListMap;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.util.Lookup;

public abstract class TransformApiKeyProvider {
    public static synchronized TransformApiKeyProvider getDefault() {
        TransformApiKeyProvider transformApiKeyProvider = (TransformApiKeyProvider)Lookup.getDefault().lookup(TransformApiKeyProvider.class);
        if (transformApiKeyProvider == null) {
            transformApiKeyProvider = new TrivialTransformApiKeyProvider();
        }
        return transformApiKeyProvider;
    }

    public abstract String getKey(String var1);

    public static class UrlWildcard {
        private String _wildcard;
        private Pattern _hostPattern;
        private Pattern _portPattern;
        private Pattern _pathPattern;
        private static final String HOST_STAR = "[\\w\\.\\-]*";
        private static final String PORT_STAR = "[\\w\\.\\-]*";
        private static final String PATH_STAR = "[\\w\\.\\-\\\\/]*";

        public UrlWildcard(String string) {
            if (string == null) {
                throw new IllegalArgumentException("Invalid urlWildcard - cannot be null.");
            }
            String string2 = UrlWildcard.stripProtocol(string);
            if (string2.startsWith("http:") || string2.startsWith("https:") || string2.startsWith("ftp:")) {
                throw new IllegalArgumentException("Invalid urlWildcard - wildcard must be specified without a protocol.");
            }
            this._wildcard = string2;
            this.parseWildcard();
        }

        private static String stripProtocol(String string) {
            int n = string.indexOf("://");
            if (n >= 0) {
                return string.substring(n + 3, string.length());
            }
            return string;
        }

        private void parseWildcard() {
            Object object;
            Pattern pattern = Pattern.compile("\\A[^:/\\\\]+");
            Matcher matcher = pattern.matcher(this._wildcard);
            String string = "";
            if (matcher.find()) {
                string = UrlWildcard.wildcardToRegex(matcher.group(), "[\\w\\.\\-]*");
            }
            this._hostPattern = Pattern.compile(string);
            pattern = Pattern.compile(":[^:/\\\\]*");
            Matcher matcher2 = pattern.matcher(this._wildcard);
            String string2 = "";
            if (matcher2.find()) {
                object = matcher2.group().substring(1);
                string2 = UrlWildcard.wildcardToRegex((String)object, "[\\w\\.\\-]*");
            }
            this._portPattern = Pattern.compile(string2);
            pattern = Pattern.compile("[/\\\\].+");
            object = pattern.matcher(this._wildcard);
            String string3 = "";
            if (object.find()) {
                String string4 = object.group().substring(1);
                string3 = UrlWildcard.wildcardToRegex(string4, "[\\w\\.\\-\\\\/]*");
            }
            this._pathPattern = Pattern.compile(string3);
        }

        private static String wildcardToRegex(String string, String string2) {
            Pattern pattern = Pattern.compile("[^\\*]+");
            Matcher matcher = pattern.matcher(string);
            StringBuffer stringBuffer = new StringBuffer();
            while (matcher.find()) {
                matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(Pattern.quote(matcher.group())));
            }
            matcher.appendTail(stringBuffer);
            String string3 = stringBuffer.toString();
            string3 = string3.replaceAll("\\*", Matcher.quoteReplacement(string2));
            return string3;
        }

        public boolean matches(URL uRL) {
            return this.matchesHost(uRL) && this.matchesPort(uRL) && this.matchesPath(uRL);
        }

        public boolean matchesHost(URL uRL) {
            String string = uRL.getHost();
            return this._hostPattern.matcher(string).matches();
        }

        public boolean matchesPort(URL uRL) {
            if (this._portPattern.toString().isEmpty()) {
                return uRL.getPort() < 0 || uRL.getPort() == uRL.getDefaultPort();
            }
            String string = "";
            if (uRL.getPort() > -1) {
                string = String.valueOf(uRL.getPort());
            } else if (uRL.getDefaultPort() > -1) {
                string = String.valueOf(uRL.getDefaultPort());
            }
            return this._portPattern.matcher(string).matches();
        }

        public boolean matchesPath(URL uRL) {
            String string = uRL.getPath();
            if (string.length() > 0) {
                string = string.substring(1);
            }
            return this._pathPattern.matcher(string).matches();
        }

        public String getWildcard() {
            return this._wildcard;
        }

        public Pattern getHostPattern() {
            return this._hostPattern;
        }

        public Pattern getPortPattern() {
            return this._portPattern;
        }

        public Pattern getPathPattern() {
            return this._pathPattern;
        }

        public String toString() {
            return this._wildcard;
        }

        public int hashCode() {
            return this._wildcard.hashCode();
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            UrlWildcard urlWildcard = (UrlWildcard)object;
            if (this._wildcard == null ? urlWildcard._wildcard != null : !this._wildcard.equals(urlWildcard._wildcard)) {
                return false;
            }
            return true;
        }
    }

    public static class Mapped
    extends TransformApiKeyProvider {
        private Map<UrlWildcard, String> _map;

        public static synchronized Mapped instance() {
            TransformApiKeyProvider transformApiKeyProvider = Mapped.getDefault();
            if (transformApiKeyProvider instanceof Mapped) {
                return (Mapped)transformApiKeyProvider;
            }
            return null;
        }

        @Override
        public String getKey(String string) {
            Map<UrlWildcard, String> map = this.getKeyMap();
            try {
                URL uRL = new URL(string);
                for (Map.Entry<UrlWildcard, String> entry : map.entrySet()) {
                    if (!entry.getKey().matches(uRL)) continue;
                    return entry.getValue();
                }
            }
            catch (MalformedURLException var3_4) {
                // empty catch block
            }
            return this.getDefaultKey(string);
        }

        protected Map<String, String> getDefaultMap() {
            return Collections.emptyMap();
        }

        protected String getDefaultKey(String string) {
            return null;
        }

        protected synchronized Map<UrlWildcard, String> getKeyMap() {
            if (this._map == null) {
                this.setKeyMap(this.getDefaultMap());
            }
            return this._map;
        }

        public void setKeyMap(Map<String, String> map) {
            this._map = new ListMap();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                this._map.put(new UrlWildcard(entry.getKey()), entry.getValue());
            }
        }
    }

    private static class TrivialTransformApiKeyProvider
    extends TransformApiKeyProvider {
        private TrivialTransformApiKeyProvider() {
        }

        @Override
        public String getKey(String string) {
            return null;
        }
    }

}

