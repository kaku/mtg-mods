/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.descriptor;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.FastURL;

public class TransformSeed {
    private String _name;
    private String _description;
    private FastURL _url;
    private boolean _enabled = true;
    private DisplayDescriptorCollection _globalTxProperties;
    private DataSource _globalTxSettings;

    public TransformSeed(FastURL fastURL, String string) {
        this(fastURL, string, "", null, null);
    }

    public TransformSeed(FastURL fastURL, String string, String string2, DisplayDescriptorCollection displayDescriptorCollection, DataSource dataSource) {
        this._url = fastURL;
        this._name = string;
        this._description = string2;
        this._globalTxProperties = displayDescriptorCollection;
        this._globalTxSettings = new DataSources.Map();
        if (displayDescriptorCollection != null && dataSource != null) {
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                this._globalTxSettings.setValue((PropertyDescriptor)displayDescriptor, dataSource.getValue((PropertyDescriptor)displayDescriptor));
            }
        }
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public FastURL getUrl() {
        return this._url;
    }

    public void setUrl(FastURL fastURL) {
        if (fastURL == null) {
            throw new IllegalArgumentException("URL cannot be null.");
        }
        this._url = fastURL;
    }

    public DisplayDescriptorCollection getGlobalTxProperties() {
        return this._globalTxProperties;
    }

    public void setGlobalTxProperties(DisplayDescriptorCollection displayDescriptorCollection) {
        this._globalTxProperties = displayDescriptorCollection;
    }

    public DataSource getGlobalTxSettings() {
        return this._globalTxSettings;
    }

    public String toString() {
        return this.getName() + ": " + (Object)this.getUrl();
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public void setEnabled(boolean bl) {
        this._enabled = bl;
    }
}

