/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.pws.api;

import com.paterva.maltego.pws.api.SignInFailedCallback;
import java.beans.PropertyChangeListener;
import java.util.Set;

public abstract class PublicWebService
implements Comparable<PublicWebService> {
    public static final String PROP_SIGNING_IN = "signingIn";
    public static final String PROP_ACCOUNT_ADDED = "accountAdded";
    public static final String PROP_ACCOUNT_REMOVED = "accountRemoved";
    private String _iconName;

    public abstract String getName();

    public abstract String getDisplayName();

    public abstract String getDescription();

    public abstract String getTransformInputPropertyName();

    public abstract void signIn(SignInFailedCallback var1);

    public abstract boolean isSigningIn();

    public abstract void cancelSigningIn();

    public abstract void signOut(String var1);

    public abstract Set<String> getSignedIn();

    public abstract boolean isSignedIn(String var1);

    public abstract boolean isCopyOf(PublicWebService var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    public String getIconName() {
        return this._iconName;
    }

    public void setIconName(String string) {
        this._iconName = string;
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    public boolean equals(Object object) {
        if (!(object instanceof PublicWebService)) {
            return false;
        }
        String string = this.getName();
        String string2 = ((PublicWebService)object).getName();
        return string == null ? string2 == null : string.equals(string2);
    }
}

