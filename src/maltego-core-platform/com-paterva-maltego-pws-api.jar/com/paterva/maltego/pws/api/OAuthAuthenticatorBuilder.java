/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.pws.api;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthVersion;
import org.openide.util.Lookup;

public abstract class OAuthAuthenticatorBuilder {
    private static OAuthAuthenticatorBuilder _default;

    public static synchronized OAuthAuthenticatorBuilder getDefault() {
        if (_default == null && (OAuthAuthenticatorBuilder._default = (OAuthAuthenticatorBuilder)Lookup.getDefault().lookup(OAuthAuthenticatorBuilder.class)) == null) {
            throw new IllegalStateException("OAuthAuthenticatorBuilder service not found.");
        }
        return _default;
    }

    public abstract OAuthAuthenticator build();

    public abstract OAuthAuthenticatorBuilder accessTokenEndpoint(String var1);

    public abstract OAuthAuthenticatorBuilder accessTokenInput(String var1);

    public abstract OAuthAuthenticatorBuilder appKey(String var1);

    public abstract OAuthAuthenticatorBuilder appSecret(String var1);

    public abstract OAuthAuthenticatorBuilder authorizationUrl(String var1);

    public abstract OAuthAuthenticatorBuilder callbackPort(Integer var1);

    public abstract OAuthAuthenticatorBuilder description(String var1);

    public abstract OAuthAuthenticatorBuilder displayName(String var1);

    public abstract OAuthAuthenticatorBuilder name(String var1);

    public abstract OAuthAuthenticatorBuilder oAuthVersion(OAuthVersion var1);

    public abstract OAuthAuthenticatorBuilder requestTokenEndpoint(String var1);

    public abstract OAuthAuthenticatorBuilder accessTokenPublicKey(String var1);
}

