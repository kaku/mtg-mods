/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.pws.api;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.PublicWebService;
import org.openide.util.Lookup;

public abstract class PublicWebServiceFactory {
    private static PublicWebServiceFactory _default;

    public static synchronized PublicWebServiceFactory getDefault() {
        if (_default == null && (PublicWebServiceFactory._default = (PublicWebServiceFactory)Lookup.getDefault().lookup(PublicWebServiceFactory.class)) == null) {
            _default = new TrivialPublicWebServiceFactory();
        }
        return _default;
    }

    public abstract PublicWebService create(OAuthAuthenticator var1);

    private static class TrivialPublicWebServiceFactory
    extends PublicWebServiceFactory {
        private TrivialPublicWebServiceFactory() {
        }

        @Override
        public PublicWebService create(OAuthAuthenticator oAuthAuthenticator) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

