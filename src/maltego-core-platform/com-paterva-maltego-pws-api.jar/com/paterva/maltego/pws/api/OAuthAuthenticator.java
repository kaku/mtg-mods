/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.pws.api;

import com.paterva.maltego.pws.api.OAuthVersion;

public interface OAuthAuthenticator {
    public boolean isCopyOf(OAuthAuthenticator var1);

    public String getName();

    public String getDisplayName();

    public String getDescription();

    public OAuthVersion getOAuthVersion();

    public String getAccessTokenEndpoint();

    public String getRequestTokenEndpoint();

    public String getAuthorizationUrl();

    public String getAppKey();

    public String getAppSecret();

    public String getIcon();

    public Integer getCallbackPort();

    public String getAccessTokenInput();

    public String getAccessTokenPublicKey();
}

