/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.pws.api;

public enum OAuthVersion {
    V1a("1.0a"),
    V2("2.0");
    
    private final String _version;

    private OAuthVersion(String string2) {
        this._version = string2;
    }

    public static OAuthVersion parse(String string) {
        for (OAuthVersion oAuthVersion : OAuthVersion.values()) {
            if (!oAuthVersion.toString().equals(string)) continue;
            return oAuthVersion;
        }
        throw new IllegalArgumentException(String.format("Not a valid %s: '%s'", OAuthVersion.class.getSimpleName(), string));
    }

    public String toString() {
        return this._version;
    }
}

