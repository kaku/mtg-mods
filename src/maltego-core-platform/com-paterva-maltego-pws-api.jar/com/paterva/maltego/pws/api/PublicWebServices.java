/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.pws.api;

import com.paterva.maltego.pws.api.PublicWebService;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class PublicWebServices {
    public static final String PROP_ADDED = "serviceAdded";
    public static final String PROP_REMOVED = "serviceRemoved";
    private static PublicWebServices _default;

    public static synchronized PublicWebServices getDefault() {
        if (_default == null && (PublicWebServices._default = (PublicWebServices)Lookup.getDefault().lookup(PublicWebServices.class)) == null) {
            throw new IllegalStateException("Web services registry not found.");
        }
        return _default;
    }

    public abstract Set<String> getServiceTypes();

    public abstract Set<PublicWebService> getAll();

    public abstract Set<String> getNames();

    public abstract Set<PublicWebService> getAll(String var1);

    public abstract Set<String> getNames(String var1);

    public abstract PublicWebService get(String var1);

    public abstract boolean replace(String var1, String var2, PublicWebService var3) throws IOException;

    public abstract void add(String var1, String var2, PublicWebService var3) throws IOException;

    public abstract void remove(String var1) throws IOException;

    public abstract int size();

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

