/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileExtensionFileFilter
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.util.FileExtensionFileFilter;

public class GraphFileFilter
extends FileExtensionFileFilter {
    private final GraphFileType _type;

    public GraphFileFilter(GraphFileType graphFileType) {
        super(graphFileType.getExtension(), graphFileType.getDescription());
        this._type = graphFileType;
    }

    public GraphFileType getType() {
        return this._type;
    }
}

