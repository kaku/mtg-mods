/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.model.ZipParameters
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.FileEntry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveOutputStream;
import com.paterva.maltego.archive.mtz.MtzVersion;
import com.paterva.maltego.archive.mtz.MtzVersionEntry;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

public class MaltegoArchiveWriter {
    private MaltegoArchiveOutputStream _stream;
    private ZipFile _zipFile;
    private ZipParameters _params;
    private final Set<String> _entries = new HashSet<String>();

    public MaltegoArchiveWriter(MtzVersion mtzVersion, OutputStream outputStream) throws IOException {
        this._stream = new MaltegoArchiveOutputStream(outputStream);
        this.writeVersion(mtzVersion);
    }

    public MaltegoArchiveWriter(MtzVersion mtzVersion, ZipFile zipFile, ZipParameters zipParameters) throws IOException {
        this._zipFile = zipFile;
        this._params = zipParameters;
        this.writeVersion(mtzVersion);
    }

    public <T> void write(Entry<T> entry) throws IOException {
        String string = entry.getName();
        if (!this._entries.contains(string)) {
            if (this._stream != null) {
                this.writeNormal(entry);
            } else {
                this.writeEncrypted(entry);
            }
            this._entries.add(string);
        }
    }

    private <T> void writeNormal(Entry<T> entry) throws IOException {
        try {
            this._stream.addEntry(entry);
            entry.write(entry.getData(), this._stream);
        }
        finally {
            this._stream.closeEntry();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <T> void writeEncrypted(Entry<T> entry) throws IOException {
        Object object;
        File file = null;
        boolean bl = false;
        if (entry instanceof FileEntry) {
            object = (FileEntry)entry;
            file = object.getFile();
        } else {
            object = FileUtilities.createTempDir((String)"Zip");
            FileUtilities.deleteContents((File)object);
            file = new File((File)object, entry.getTypeName() + "." + entry.getType());
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(file);
                entry.write(entry.getData(), fileOutputStream);
            }
            finally {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            }
            bl = true;
        }
        this._params.setRootFolderInZip(entry.getFolder());
        try {
            this._zipFile.addFile(file, this._params);
        }
        catch (ZipException var4_5) {
            throw new IOException((Throwable)var4_5);
        }
        finally {
            if (bl && file != null) {
                file.delete();
            }
        }
    }

    public void close() throws IOException {
        if (this._stream != null) {
            this._stream.close();
        }
    }

    private void writeVersion(MtzVersion mtzVersion) throws IOException {
        this.write(new MtzVersionEntry(mtzVersion));
    }
}

