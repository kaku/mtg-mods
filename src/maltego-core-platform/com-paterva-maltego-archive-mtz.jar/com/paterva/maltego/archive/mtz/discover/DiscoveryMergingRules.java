/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.archive.mtz.discover;

import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public abstract class DiscoveryMergingRules {
    private static DiscoveryMergingRules _default;

    public static synchronized DiscoveryMergingRules getDefault() {
        if (_default == null && (DiscoveryMergingRules._default = (DiscoveryMergingRules)Lookup.getDefault().lookup(DiscoveryMergingRules.class)) == null) {
            _default = new Default();
        }
        return _default;
    }

    public abstract EntityRule getEntityRule();

    public abstract void setEntityRule(EntityRule var1);

    public abstract IconRule getIconRule();

    public abstract void setIconRule(IconRule var1);

    private static class Default
    extends DiscoveryMergingRules {
        private final String PREF_ENTITY_RULE = "maltego.discover.match.entity";
        private final String PREF_ICON_RULE = "maltego.discover.match.icon";

        private Default() {
        }

        @Override
        public EntityRule getEntityRule() {
            return EntityRule.valueOf(this.getPrefs().get("maltego.discover.match.entity", EntityRule.MERGE.name()));
        }

        @Override
        public void setEntityRule(EntityRule entityRule) {
            this.getPrefs().put("maltego.discover.match.entity", entityRule.name());
        }

        @Override
        public IconRule getIconRule() {
            return IconRule.valueOf(this.getPrefs().get("maltego.discover.match.icon", IconRule.IGNORE.name()));
        }

        @Override
        public void setIconRule(IconRule iconRule) {
            this.getPrefs().put("maltego.discover.match.icon", iconRule.name());
        }

        private Preferences getPrefs() {
            return NbPreferences.forModule(DiscoveryMergingRules.class);
        }
    }

    public static enum IconRule {
        REPLACE,
        IGNORE;
        

        private IconRule() {
        }
    }

    public static enum EntityRule {
        REPLACE,
        MERGE,
        IGNORE;
        

        private EntityRule() {
        }
    }

}

