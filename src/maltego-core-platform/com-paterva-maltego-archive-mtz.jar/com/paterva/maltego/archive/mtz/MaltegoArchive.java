/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.io.ZipInputStream
 *  net.lingala.zip4j.model.FileHeader
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;

class MaltegoArchive {
    private final ZipFile _file;
    private Map<String, FileHeader> _paths;

    public MaltegoArchive(ZipFile zipFile) {
        this._file = zipFile;
    }

    public <T> ZipInputStream getInputStream(String string) throws ZipException {
        return this._file.getInputStream(this.getPaths().get(string));
    }

    public boolean containsEntry(String string) throws ZipException {
        return this.getPaths().containsKey(string);
    }

    public <TEntry extends Entry> List<TEntry> createEntries(EntryFactory<TEntry> entryFactory, String string) throws ZipException {
        ArrayList<TEntry> arrayList = new ArrayList<TEntry>();
        List<String> list = this.getPaths(entryFactory, string);
        for (String string2 : list) {
            arrayList.add(entryFactory.create(string2));
        }
        return arrayList;
    }

    public <TEntry extends Entry> List<String> getPaths(EntryFactory<TEntry> entryFactory, String string) throws ZipException {
        return this.getPaths(entryFactory.getFolderName(), entryFactory.getExtension());
    }

    public List<String> getPaths(String string, String string2) throws ZipException {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (String string3 : this.getPaths().keySet()) {
            String[] arrstring = Entry.parse(string3);
            String string4 = arrstring[0];
            String string5 = arrstring[2];
            if (!string4.startsWith(string) || string2 != null && !string2.equals(string5)) continue;
            arrayList.add(string3);
        }
        return arrayList;
    }

    private Map<String, FileHeader> getPaths() throws ZipException {
        if (this._paths == null) {
            this._paths = new HashMap<String, FileHeader>();
            List list = this._file.getFileHeaders();
            for (Object e : list) {
                FileHeader fileHeader;
                if (!(e instanceof FileHeader) || (fileHeader = (FileHeader)e).isDirectory()) continue;
                this._paths.put(fileHeader.getFileName(), fileHeader);
            }
        }
        return this._paths;
    }
}

