/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ComparableVersion
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.util.ComparableVersion;
import java.util.Properties;
import org.openide.util.Utilities;

public class MtzVersion
extends Properties {
    public static final String MTZ_VERSION = "1.0";
    public static final String GRAPH_VERSION_GRAPHML = "1.0";
    public static final String GRAPH_VERSION_PANDORA = "1.2";
    private static final String PROP_MTZ_VERSION = "maltego.mtz.version";
    private static final String PROP_GRAPH_VERSION = "maltego.graph.version";
    private static final String PROP_PANDORA_VERSION = "maltego.pandora.version";

    public static MtzVersion getBase() {
        MtzVersion mtzVersion = new MtzVersion();
        mtzVersion.put("maltego.client.name", System.getProperty("maltego.product-name", "Maltego"));
        mtzVersion.put("maltego.client.version", System.getProperty("maltego.fullversion", ""));
        mtzVersion.put("maltego.client.subtitle", System.getProperty("maltego.version-subtitle", ""));
        return mtzVersion;
    }

    public static MtzVersion getCurrent() {
        MtzVersion mtzVersion = MtzVersion.getBase();
        mtzVersion.put("maltego.mtz.version", "1.0");
        mtzVersion.put("maltego.graph.version", "1.2");
        mtzVersion.put("maltego.pandora.version", "1.4.2");
        return mtzVersion;
    }

    public static MtzVersion getMTZ() {
        MtzVersion mtzVersion = MtzVersion.getCurrent();
        mtzVersion.put("maltego.mtz.version", "1.0");
        return mtzVersion;
    }

    public static MtzVersion getMTGX() {
        MtzVersion mtzVersion = MtzVersion.getMTZ();
        mtzVersion.put("maltego.graph.version", "1.0");
        return mtzVersion;
    }

    public static boolean isMtzVersionSupported(MtzVersion mtzVersion) {
        return MtzVersion.compareVersions("1.0", MtzVersion.getMtzVersion(mtzVersion)) >= 0;
    }

    public static boolean isGraphVersionSupported(MtzVersion mtzVersion) {
        return MtzVersion.isGraphVersionSupported(MtzVersion.getGraphVersion(mtzVersion));
    }

    public static boolean isGraphVersionSupported(String string) {
        return MtzVersion.compareVersions("1.2", string) >= 0;
    }

    public static String getMtzVersion(MtzVersion mtzVersion) {
        return mtzVersion == null ? null : mtzVersion.getProperty("maltego.mtz.version");
    }

    public static String getGraphVersion(MtzVersion mtzVersion) {
        return mtzVersion == null ? null : mtzVersion.getProperty("maltego.graph.version");
    }

    static int compareVersions(String string, String string2) {
        if (Utilities.compareObjects((Object)string, (Object)string2)) {
            return 0;
        }
        if (string == null) {
            return -1;
        }
        if (string2 == null) {
            return 1;
        }
        ComparableVersion comparableVersion = new ComparableVersion(string);
        ComparableVersion comparableVersion2 = new ComparableVersion(string2);
        return comparableVersion.compareTo(comparableVersion2);
    }
}

