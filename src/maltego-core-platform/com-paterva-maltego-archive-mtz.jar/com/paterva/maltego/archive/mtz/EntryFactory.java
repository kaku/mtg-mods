/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;

public interface EntryFactory<T extends Entry> {
    public T create(String var1);

    public String getFolderName();

    public String getExtension();
}

