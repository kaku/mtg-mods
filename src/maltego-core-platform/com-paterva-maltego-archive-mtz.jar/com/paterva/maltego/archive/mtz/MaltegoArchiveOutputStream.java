/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

class MaltegoArchiveOutputStream
extends FilterOutputStream {
    public MaltegoArchiveOutputStream(OutputStream outputStream) throws IOException {
        super(new JarOutputStream(outputStream));
    }

    protected JarOutputStream delegate() {
        return (JarOutputStream)this.out;
    }

    public void addEntry(Entry entry) throws IOException {
        this.delegate().putNextEntry(entry);
    }

    public void closeEntry() throws IOException {
        this.delegate().closeEntry();
    }

    @Override
    public void write(int n) throws IOException {
        this.delegate().write(n);
    }

    @Override
    public void write(byte[] arrby) throws IOException {
        this.delegate().write(arrby);
    }

    @Override
    public void write(byte[] arrby, int n, int n2) throws IOException {
        this.delegate().write(arrby, n, n2);
    }
}

