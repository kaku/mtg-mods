/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz.discover;

import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;

public abstract class MtzDiscoveryItems {
    private final MtzDiscoveryProvider _provider;
    private final DiscoveryContext _context;

    public MtzDiscoveryItems(MtzDiscoveryProvider mtzDiscoveryProvider, DiscoveryContext discoveryContext) {
        this._provider = mtzDiscoveryProvider;
        this._context = discoveryContext;
    }

    public MtzDiscoveryProvider getProvider() {
        return this._provider;
    }

    public DiscoveryContext getContext() {
        return this._context;
    }

    public abstract String getDescription();

    public abstract int size();

    public boolean isEmpty() {
        return this.size() <= 0;
    }
}

