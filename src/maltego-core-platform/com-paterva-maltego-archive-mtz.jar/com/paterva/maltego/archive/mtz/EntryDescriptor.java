/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz;

class EntryDescriptor {
    private String _name;
    private String _displayName;

    public EntryDescriptor(String string) {
        this(string, null);
    }

    public EntryDescriptor(String string, String string2) {
        this._name = string;
        this._displayName = string2;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        if (this._displayName == null) {
            return this.getName();
        }
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public boolean equals(Object object) {
        if (object instanceof EntryDescriptor) {
            return this.equals((EntryDescriptor)object);
        }
        return false;
    }

    public int hashCode() {
        int n = 5;
        n = 97 * n + (this._name != null ? this._name.hashCode() : 0);
        return n;
    }

    public boolean equals(EntryDescriptor entryDescriptor) {
        if (entryDescriptor == null) {
            return false;
        }
        return entryDescriptor.getName().equals(this.getName());
    }

    public String toString() {
        return this.getDisplayName();
    }
}

