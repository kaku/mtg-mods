/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.EntryDescriptor;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;

public abstract class Entry<T>
extends ZipEntry {
    private T _data;

    public Entry(T t, String string, String string2) {
        this(t, string, string2, null);
    }

    public Entry(T t, String string, String string2, String string3) {
        super(StringUtilities.isNullOrEmpty((String)string) ? string2 : string + "/" + string2);
        this.setDisplayName(string3);
        this._data = t;
    }

    public Entry(String string) {
        super(string);
    }

    public T getData() {
        return this._data;
    }

    public String getTypeName() {
        String[] arrstring = Entry.parse(this.getName());
        return arrstring[1];
    }

    public String getType() {
        String[] arrstring = Entry.parse(this.getName());
        return arrstring[2];
    }

    public String getFolder() {
        String[] arrstring = Entry.parse(this.getName());
        return arrstring[0];
    }

    public String getFileName() {
        String string = this.getTypeName();
        String string2 = this.getType();
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            string = string + "." + string2;
        }
        return string;
    }

    public static String[] parse(String string) {
        String[] arrstring = new String[3];
        if (string == null) {
            return null;
        }
        int n = string.lastIndexOf("/");
        if (n < 0 || n >= string.length() - 1) {
            arrstring[0] = "";
            n = -1;
        } else {
            arrstring[0] = string.substring(0, n);
        }
        int n2 = string.lastIndexOf(".");
        if (n2 < 0) {
            arrstring[2] = "";
            n2 = string.length();
        } else if (n2 >= string.length() - 1) {
            arrstring[2] = "";
            n2 = string.length() - 1;
        } else {
            arrstring[2] = string.substring(n2 + 1);
        }
        arrstring[1] = n2 > n ? string.substring(n + 1, n2) : "";
        return arrstring;
    }

    public String getDisplayName() {
        if (StringUtilities.isNullOrEmpty((String)this.getComment())) {
            return this.getTypeName();
        }
        return this.getComment();
    }

    public void setDisplayName(String string) {
        if (string != null) {
            this.setComment(string);
        } else {
            this.setComment("");
        }
    }

    public boolean equals(EntryDescriptor entryDescriptor) {
        if (entryDescriptor == null) {
            return false;
        }
        return entryDescriptor.getName().equals(this.getName());
    }

    @Override
    public String toString() {
        return this.getDisplayName();
    }

    protected abstract T read(InputStream var1) throws IOException;

    protected abstract void write(T var1, OutputStream var2) throws IOException;
}

