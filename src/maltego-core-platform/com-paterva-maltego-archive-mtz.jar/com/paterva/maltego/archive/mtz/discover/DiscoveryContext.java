/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz.discover;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;

public class DiscoveryContext {
    private final String _seedUrl;
    private final DiscoveryMergingRules _rules;

    public DiscoveryContext(String string, DiscoveryMergingRules discoveryMergingRules) {
        this._seedUrl = string;
        this._rules = discoveryMergingRules;
    }

    public String getSeedUrl() {
        return this._seedUrl;
    }

    public DiscoveryMergingRules getMergingRules() {
        return this._rules;
    }
}

