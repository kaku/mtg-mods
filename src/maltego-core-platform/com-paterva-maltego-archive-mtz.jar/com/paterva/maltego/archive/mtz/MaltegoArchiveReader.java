/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.io.ZipInputStream
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchive;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;

public class MaltegoArchiveReader {
    private final MaltegoArchive _archive;

    public MaltegoArchiveReader(ZipFile zipFile) {
        this._archive = new MaltegoArchive(zipFile);
    }

    public <T> T read(Entry<T> entry) throws IOException {
        ZipInputStream zipInputStream = null;
        try {
            if (!this._archive.containsEntry(entry.getName())) {
                T t = null;
                return t;
            }
            zipInputStream = this._archive.getInputStream(entry.getName());
            T t = entry.read((InputStream)zipInputStream);
            return t;
        }
        catch (ZipException var3_5) {
            throw new IOException((Throwable)var3_5);
        }
        finally {
            if (zipInputStream != null) {
                zipInputStream.close(true);
            }
        }
    }

    public <T, TEntry extends Entry<T>> List<T> readAll(EntryFactory<TEntry> entryFactory, String string) throws IOException {
        ArrayList<T> arrayList = new ArrayList<T>();
        try {
            List<TEntry> list = this._archive.createEntries(entryFactory, string);
            for (Entry entry : list) {
                T t = this.read(entry);
                if (t == null) continue;
                arrayList.add(t);
            }
        }
        catch (ZipException var4_5) {
            throw new IOException((Throwable)var4_5);
        }
        return arrayList;
    }
}

