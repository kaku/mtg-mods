/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MtzVersion;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MtzVersionEntry
extends Entry<MtzVersion> {
    public static final String Name = "version";
    public static final String Type = "properties";

    public MtzVersionEntry(MtzVersion mtzVersion) {
        super(mtzVersion, "", "version.properties", "Version properties");
    }

    public MtzVersionEntry() {
        super("version.properties");
    }

    @Override
    protected MtzVersion read(InputStream inputStream) throws IOException {
        MtzVersion mtzVersion = new MtzVersion();
        mtzVersion.load(inputStream);
        return mtzVersion;
    }

    @Override
    protected void write(MtzVersion mtzVersion, OutputStream outputStream) throws IOException {
        mtzVersion.store(outputStream, "");
    }
}

