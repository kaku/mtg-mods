/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.archive.mtz.discover;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeSpec;

public class DiscoveryEntityMerger {
    public static boolean merge(EntityRegistry entityRegistry, MaltegoEntitySpec maltegoEntitySpec, DiscoveryMergingRules.EntityRule entityRule) {
        if ((maltegoEntitySpec = DiscoveryEntityMerger.getNewEntity(entityRegistry, entityRule, maltegoEntitySpec)) != null) {
            entityRegistry.put((TypeSpec)maltegoEntitySpec, maltegoEntitySpec.getDefaultCategory());
        }
        return maltegoEntitySpec != null;
    }

    public static MaltegoEntitySpec getNewEntity(EntityRegistry entityRegistry, DiscoveryMergingRules.EntityRule entityRule, MaltegoEntitySpec maltegoEntitySpec) {
        MaltegoEntitySpec maltegoEntitySpec2 = null;
        MaltegoEntitySpec maltegoEntitySpec3 = (MaltegoEntitySpec)entityRegistry.get(maltegoEntitySpec.getTypeName());
        switch (entityRule) {
            case REPLACE: {
                if (maltegoEntitySpec.isCopy((Object)maltegoEntitySpec3)) break;
                maltegoEntitySpec2 = maltegoEntitySpec;
                break;
            }
            case IGNORE: {
                if (maltegoEntitySpec3 != null) break;
                maltegoEntitySpec2 = maltegoEntitySpec;
                break;
            }
            case MERGE: {
                if (maltegoEntitySpec3 == null) {
                    maltegoEntitySpec2 = maltegoEntitySpec;
                    break;
                }
                MaltegoEntitySpec maltegoEntitySpec4 = new MaltegoEntitySpec(maltegoEntitySpec3);
                maltegoEntitySpec4.mergeWith(maltegoEntitySpec);
                if (maltegoEntitySpec4.isCopy((Object)maltegoEntitySpec3)) break;
                maltegoEntitySpec2 = maltegoEntitySpec4;
                break;
            }
            default: {
                throw new IllegalStateException("Unknown entity rule " + (Object)((Object)entityRule));
            }
        }
        return maltegoEntitySpec2;
    }

}

