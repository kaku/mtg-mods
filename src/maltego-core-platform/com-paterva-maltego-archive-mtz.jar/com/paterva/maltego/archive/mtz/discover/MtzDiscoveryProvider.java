/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.archive.mtz.discover;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class MtzDiscoveryProvider<T extends MtzDiscoveryItems> {
    public static synchronized Collection<MtzDiscoveryProvider> getAll() {
        Collection collection = Lookup.getDefault().lookupAll(MtzDiscoveryProvider.class);
        return new ArrayList<MtzDiscoveryProvider>(collection);
    }

    public abstract T read(DiscoveryContext var1, MaltegoArchiveReader var2) throws IOException;

    public abstract T getNewAndMerged(T var1);

    public abstract void apply(T var1);
}

