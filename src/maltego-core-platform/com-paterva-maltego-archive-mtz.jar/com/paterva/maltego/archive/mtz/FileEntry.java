/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class FileEntry<T>
extends Entry<T> {
    public FileEntry(T t, String string, String string2) {
        super(t, string, string2);
    }

    public FileEntry(T t, String string, String string2, String string3) {
        super(t, string, string2, string3);
    }

    public FileEntry(String string) {
        super(string);
    }

    @Override
    protected T read(InputStream inputStream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void write(T t, OutputStream outputStream) throws IOException {
        FileUtilities.copy((File)this.getFile(), (OutputStream)outputStream);
    }

    public abstract File getFile() throws IOException;
}

