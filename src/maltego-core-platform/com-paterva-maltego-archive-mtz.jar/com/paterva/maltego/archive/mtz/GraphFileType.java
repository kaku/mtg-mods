/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.archive.mtz;

import com.paterva.maltego.archive.mtz.MtzVersion;
import java.io.File;
import java.util.ArrayList;

public enum GraphFileType {
    GRAPHML("mtgx", "Maltego Graph (old format)", MtzVersion.getMTGX()),
    PANDORA("mtgl", "Maltego Graph", MtzVersion.getCurrent());
    
    private final String _ext;
    private final String _description;
    private final MtzVersion _mtzVersion;

    private GraphFileType(String string2, String string3, MtzVersion mtzVersion) {
        this._ext = string2;
        this._description = string3;
        this._mtzVersion = mtzVersion;
    }

    public String getExtension() {
        return this._ext;
    }

    public String getDescription() {
        return this._description;
    }

    public String toString() {
        return this._ext;
    }

    public MtzVersion getMtzVersion() {
        return this._mtzVersion;
    }

    public static GraphFileType getDefaultType() {
        return PANDORA;
    }

    public static GraphFileType forFile(File file) {
        return GraphFileType.forFile(file.getName());
    }

    public static GraphFileType forFile(String string) {
        for (GraphFileType graphFileType : GraphFileType.values()) {
            if (!string.toLowerCase().endsWith("." + graphFileType.getExtension().toLowerCase())) continue;
            return graphFileType;
        }
        return GraphFileType.getDefaultType();
    }

    public static GraphFileType forExtension(String string) {
        for (GraphFileType graphFileType : GraphFileType.values()) {
            if (!graphFileType.getExtension().equalsIgnoreCase(string)) continue;
            return graphFileType;
        }
        return GraphFileType.getDefaultType();
    }

    public static String[] getAllExtensions() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (GraphFileType graphFileType : GraphFileType.values()) {
            arrayList.add(graphFileType.getExtension());
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }
}

