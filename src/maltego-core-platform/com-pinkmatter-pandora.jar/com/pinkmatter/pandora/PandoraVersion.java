/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ComparableVersion
 */
package com.pinkmatter.pandora;

import com.paterva.maltego.util.ComparableVersion;

public class PandoraVersion {
    public static final String VERSION = "1.4.2";

    public static boolean isNewer(String string) {
        ComparableVersion comparableVersion = new ComparableVersion(string);
        ComparableVersion comparableVersion2 = new ComparableVersion("1.4.2");
        return comparableVersion.compareTo(comparableVersion2) > 0;
    }
}

