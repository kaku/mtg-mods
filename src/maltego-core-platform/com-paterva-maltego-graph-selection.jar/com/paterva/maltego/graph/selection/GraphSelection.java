/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphUserData
 */
package com.paterva.maltego.graph.selection;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.selection.GraphSelectionFactory;
import com.paterva.maltego.graph.selection.SelectionState;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Set;

public abstract class GraphSelection {
    public static final String PROP_SELECTION_CHANGED = "selectionChanged";

    public static synchronized GraphSelection forGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphSelection graphSelection = (GraphSelection)graphUserData.get((Object)(string = GraphSelection.class.getName()));
        if (graphSelection == null) {
            graphSelection = GraphSelectionFactory.getDefault().create(graphID);
            graphUserData.put((Object)string, (Object)graphSelection);
        }
        return graphSelection;
    }

    public abstract boolean hasSelection();

    public abstract boolean hasSelectedEntities();

    public abstract boolean hasSelectedLinks();

    public abstract int getSelectedModelEntityCount();

    public abstract int getSelectedViewEntityCount();

    public abstract int getSelectedModelLinkCount();

    public abstract int getSelectedViewLinkCount();

    public abstract Set<EntityID> getSelectedViewEntities();

    public abstract Set<EntityID> getSelectedModelEntities();

    public abstract Set<EntityID> getSelectedModelEntities(EntityID var1);

    public abstract boolean isSelectedInModel(EntityID var1);

    public abstract SelectionState getViewSelectionState(EntityID var1);

    public abstract Set<LinkID> getSelectedViewLinks();

    public abstract Set<LinkID> getSelectedModelLinks();

    public abstract Set<LinkID> getSelectedModelLinks(LinkID var1);

    public abstract boolean isSelectedInModel(LinkID var1);

    public abstract boolean isSelectedInView(LinkID var1);

    public abstract SelectionState getViewSelectionState(LinkID var1);

    public abstract void setSelectedModelEntities(Collection<EntityID> var1);

    public abstract void setSelectedViewEntities(Collection<EntityID> var1);

    public abstract void setSelectedModelLinks(Collection<LinkID> var1);

    public abstract void setSelectedViewLinks(Collection<LinkID> var1);

    public abstract void setModelEntitiesSelected(Collection<EntityID> var1, boolean var2);

    public abstract void setViewEntitiesSelected(Collection<EntityID> var1, boolean var2);

    public abstract void setModelLinksSelected(Collection<LinkID> var1, boolean var2);

    public abstract void setViewLinksSelected(Collection<LinkID> var1, boolean var2);

    public abstract void addSelectedModelEntities(Collection<EntityID> var1);

    public abstract void addSelectedViewEntities(Collection<EntityID> var1);

    public abstract void addSelectedModelLinks(Collection<LinkID> var1);

    public abstract void addSelectedViewLinks(Collection<LinkID> var1);

    public abstract void selectAllEntities();

    public abstract void selectAllLinks();

    public abstract void invertSelection(boolean var1);

    public abstract void clearSelection();

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

