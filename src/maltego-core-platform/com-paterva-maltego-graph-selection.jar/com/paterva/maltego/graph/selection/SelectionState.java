/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.selection;

public enum SelectionState {
    YES,
    NO,
    PARTIAL;
    

    private SelectionState() {
    }
}

