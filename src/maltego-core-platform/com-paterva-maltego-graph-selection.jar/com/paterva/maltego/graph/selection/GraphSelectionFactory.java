/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.selection;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import org.openide.util.Lookup;

public abstract class GraphSelectionFactory {
    private static GraphSelectionFactory _default;

    public static synchronized GraphSelectionFactory getDefault() {
        if (_default == null && (GraphSelectionFactory._default = (GraphSelectionFactory)Lookup.getDefault().lookup(GraphSelectionFactory.class)) == null) {
            throw new IllegalStateException("Graph Selection Factory not found");
        }
        return _default;
    }

    public abstract GraphSelection create(GraphID var1);
}

