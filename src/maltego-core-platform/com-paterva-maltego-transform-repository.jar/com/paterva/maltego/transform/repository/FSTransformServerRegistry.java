/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ListSet
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.http.ServerCertificates
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.repository.TransformRepositoriesLock;
import com.paterva.maltego.transform.repository.serializer.TransformServerInfoSerializer;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ListSet;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.http.ServerCertificates;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class FSTransformServerRegistry
extends TransformServerRegistry {
    private static final String MALTEGO_FOLDER = "Maltego";
    private static final String SEED_FOLDER = "Servers";
    private static final String EXTENSION = "tas";
    private Map<FastURL, TransformServerFile> _map;
    private final FileObject _configRoot;

    public FSTransformServerRegistry() {
        this(FileUtil.getConfigRoot());
    }

    public FSTransformServerRegistry(FileObject fileObject) {
        this._configRoot = fileObject;
    }

    public Set<TransformServerInfo> findServers(String string, boolean bl) {
        ListSet listSet = new ListSet();
        for (TransformServerInfo transformServerInfo : this.getAll()) {
            if (!FSTransformServerRegistry.enabled(transformServerInfo, bl) || !transformServerInfo.getTransforms().contains(string)) continue;
            listSet.add(transformServerInfo);
        }
        return listSet;
    }

    public boolean exists(String string, boolean bl) {
        for (TransformServerInfo transformServerInfo : this.getAll()) {
            if (!FSTransformServerRegistry.enabled(transformServerInfo, bl) || !transformServerInfo.getTransforms().contains(string)) continue;
            return true;
        }
        return false;
    }

    private static boolean enabled(TransformServerInfo transformServerInfo, boolean bl) {
        if (!bl) {
            return true;
        }
        return transformServerInfo.isEnabled();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void put(TransformServerInfo transformServerInfo) {
        if (transformServerInfo.isDirty()) {
            try {
                boolean bl;
                Object object = TransformRepositoriesLock.LOCK;
                synchronized (object) {
                    TransformServerFile transformServerFile = this.map().get((Object)transformServerInfo.getUrl());
                    if (transformServerFile == null) {
                        transformServerFile = new TransformServerFile(this, transformServerInfo, this.addServer(transformServerInfo));
                        bl = true;
                    } else {
                        this.updateServer(transformServerFile.getFile(), transformServerInfo);
                        bl = false;
                    }
                    this.map().put(transformServerInfo.getUrl(), transformServerFile);
                    transformServerInfo.markClean();
                }
                if (bl) {
                    this.fireItemAdded((Object)transformServerInfo);
                } else {
                    this.fireItemChanged((Object)transformServerInfo);
                }
            }
            catch (IOException var2_5) {
                Exceptions.printStackTrace((Throwable)var2_5);
            }
        }
    }

    private FileObject addServer(TransformServerInfo transformServerInfo) throws IOException, XmlSerializationException {
        FileObject fileObject = this.getOrCreateTasFolder();
        FileObject fileObject2 = FileUtilities.createUniqueFile((FileObject)fileObject, (String)FileUtilities.replaceIllegalChars((String)transformServerInfo.getDisplayName()), (String)"tas");
        this.updateServer(fileObject2, transformServerInfo);
        return fileObject2;
    }

    private void updateServer(FileObject fileObject, TransformServerInfo transformServerInfo) throws IOException {
        this.save(fileObject, transformServerInfo);
        TransformServerFile transformServerFile = this.map().get((Object)transformServerInfo.getUrl());
        if (transformServerFile == null) {
            transformServerFile = new TransformServerFile(this, transformServerInfo, fileObject);
            this.map().put(transformServerInfo.getUrl(), transformServerFile);
        } else {
            transformServerFile.setInfo(transformServerInfo);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void save(FileObject fileObject, TransformServerInfo transformServerInfo) throws IOException {
        FileLock fileLock = null;
        OutputStream outputStream = null;
        try {
            fileLock = fileObject.lock();
            outputStream = fileObject.getOutputStream(fileLock);
            TransformServerInfoSerializer.write(transformServerInfo, outputStream);
            transformServerInfo.markClean();
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
            if (fileLock != null) {
                fileLock.releaseLock();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformServerInfo get(FastURL fastURL) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            TransformServerFile transformServerFile = this.map().get((Object)fastURL);
            if (transformServerFile == null) {
                return null;
            }
            return transformServerFile.getInfo();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<TransformServerInfo> getAll() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            LinkedList<TransformServerInfo> linkedList = new LinkedList<TransformServerInfo>();
            for (TransformServerFile transformServerFile : this.map().values()) {
                linkedList.add(transformServerFile.getInfo());
            }
            return linkedList;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean exists(FastURL fastURL) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            return this.map().containsKey((Object)fastURL);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void remove(FastURL fastURL) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            TransformServerFile transformServerFile = this.map().get((Object)fastURL);
            if (transformServerFile != null) {
                try {
                    transformServerFile.getFile().delete();
                    this.map().remove((Object)fastURL);
                    this.removeRogueTransforms();
                    this.fireItemRemoved((Object)transformServerFile);
                }
                catch (IOException var4_4) {
                    Exceptions.printStackTrace((Throwable)var4_4);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeSeedUrl(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            ArrayList<FastURL> arrayList = new ArrayList<FastURL>();
            for (TransformServerInfo transformServerInfo2 : this.getAll()) {
                boolean bl = transformServerInfo2.removeSeedUrl(string);
                if (!bl || !transformServerInfo2.getSeedUrls().isEmpty()) continue;
                arrayList.add(transformServerInfo2.getUrl());
            }
            for (FastURL fastURL : arrayList) {
                ServerCertificates.getDefault().remove(fastURL.toString());
                this.remove(fastURL);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateSeedUrl(String string, String string2) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            Collection<TransformServerInfo> collection = this.getAll();
            for (TransformServerInfo transformServerInfo : collection) {
                List list = transformServerInfo.getSeedUrls();
                if (list.contains(string)) {
                    transformServerInfo.removeSeedUrl(string);
                    transformServerInfo.addSeedUrl(string2);
                }
                this.put(transformServerInfo);
            }
        }
    }

    private void removeRogueTransforms() {
        TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getRepository("Remote");
        ArrayList<String> arrayList = new ArrayList<String>();
        for (TransformDefinition object2 : transformRepository.getAll()) {
            String string = object2.getName();
            Set<TransformServerInfo> set = this.findServers(string, false);
            if (!set.isEmpty()) continue;
            arrayList.add(string);
        }
        for (String string : arrayList) {
            transformRepository.remove(string);
        }
    }

    private FileObject getOrCreateTasFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this.getOrCreateMaltegoFolder(), (String)"Servers");
    }

    private FileObject getOrCreateMaltegoFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego");
    }

    private FileObject getTasFolder() {
        FileObject fileObject = this._configRoot.getFileObject("Maltego");
        if (fileObject != null) {
            return fileObject.getFileObject("Servers");
        }
        return null;
    }

    private Map<FastURL, TransformServerFile> map() {
        if (this._map == null) {
            this._map = this.load();
        }
        return this._map;
    }

    private Map<FastURL, TransformServerFile> load() {
        HashMap<FastURL, TransformServerFile> hashMap = new HashMap<FastURL, TransformServerFile>();
        FileObject fileObject = this.getTasFolder();
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getData(false);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                if (!"tas".equals(fileObject2.getExt())) continue;
                try {
                    TransformServerInfo transformServerInfo = this.load(fileObject2);
                    transformServerInfo.markClean();
                    hashMap.put(transformServerInfo.getUrl(), new TransformServerFile(this, transformServerInfo, fileObject2));
                }
                catch (FileNotFoundException var5_6) {
                    Exceptions.printStackTrace((Throwable)var5_6);
                }
                catch (XmlSerializationException var5_7) {
                    Exceptions.printStackTrace((Throwable)var5_7);
                }
            }
        }
        return hashMap;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected TransformServerInfo load(FileObject fileObject) throws FileNotFoundException, XmlSerializationException {
        InputStream inputStream = null;
        try {
            inputStream = fileObject.getInputStream();
            TransformServerInfo transformServerInfo = this.serializer().read(inputStream);
            return transformServerInfo;
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException var4_4) {
                    Exceptions.printStackTrace((Throwable)var4_4);
                }
            }
        }
    }

    private TransformServerInfoSerializer serializer() {
        return new TransformServerInfoSerializer();
    }

    protected class TransformServerFile {
        private TransformServerInfo _info;
        private FileObject _file;
        final /* synthetic */ FSTransformServerRegistry this$0;

        public TransformServerFile(FSTransformServerRegistry fSTransformServerRegistry) {
            this.this$0 = fSTransformServerRegistry;
        }

        public TransformServerFile(FSTransformServerRegistry fSTransformServerRegistry, TransformServerInfo transformServerInfo) {
            this(fSTransformServerRegistry, transformServerInfo, null);
        }

        public TransformServerFile(FSTransformServerRegistry fSTransformServerRegistry, TransformServerInfo transformServerInfo, FileObject fileObject) {
            this.this$0 = fSTransformServerRegistry;
            this._info = transformServerInfo;
            this._file = fileObject;
        }

        public TransformServerInfo getInfo() {
            return this._info;
        }

        public void setInfo(TransformServerInfo transformServerInfo) {
            this._info = transformServerInfo;
        }

        public FileObject getFile() {
            return this._file;
        }

        public void setFile(FileObject fileObject) {
            this._file = fileObject;
        }
    }

}

