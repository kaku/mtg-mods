/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.repository.serializer.TransformSetStub;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformSetSerializer {
    public static TransformSetSerializer getDefault() {
        TransformSetSerializer transformSetSerializer = (TransformSetSerializer)Lookup.getDefault().lookup(TransformSetSerializer.class);
        if (transformSetSerializer == null) {
            transformSetSerializer = new DefaultTransformSetSerializer();
        }
        return transformSetSerializer;
    }

    public abstract void write(TransformSet var1, OutputStream var2) throws XmlSerializationException;

    public abstract TransformSet read(InputStream var1) throws XmlSerializationException;

    private static class DefaultTransformSetSerializer
    extends TransformSetSerializer {
        private DefaultTransformSetSerializer() {
        }

        @Override
        public void write(TransformSet transformSet, OutputStream outputStream) throws XmlSerializationException {
            XmlSerializer xmlSerializer = new XmlSerializer();
            TransformSetStub transformSetStub = this.translate(transformSet);
            xmlSerializer.write((Object)transformSetStub, outputStream);
        }

        @Override
        public TransformSet read(InputStream inputStream) throws XmlSerializationException {
            XmlSerializer xmlSerializer = new XmlSerializer();
            TransformSetStub transformSetStub = (TransformSetStub)xmlSerializer.read(TransformSetStub.class, inputStream);
            return this.translate(transformSetStub);
        }

        private TransformSetStub translate(TransformSet transformSet) {
            TransformSetStub transformSetStub = new TransformSetStub();
            transformSetStub.setName(transformSet.getName());
            transformSetStub.setDescription(transformSet.getDescription());
            transformSetStub.setTransforms(new ArrayList<String>(transformSet.getAllTransforms()));
            return transformSetStub;
        }

        private TransformSet translate(TransformSetStub transformSetStub) {
            TransformSet transformSet = new TransformSet(transformSetStub.getName());
            transformSet.setDescription(transformSetStub.getDescription());
            for (String string : transformSetStub.getTransforms()) {
                transformSet.addTransform(string);
            }
            return transformSet;
        }
    }

}

