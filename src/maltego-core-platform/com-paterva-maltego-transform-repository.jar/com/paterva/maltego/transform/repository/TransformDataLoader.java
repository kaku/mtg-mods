/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.ExtensionList
 *  org.openide.loaders.FileEntry
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.repository.TransformDataObject;
import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.ExtensionList;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;

public class TransformDataLoader
extends MultiFileLoader {
    public static final String MIME_TYPE = "text/maltegotransform+xml";
    public static final String TRANSFORM_EXTENSION = "transform";
    public static final String SETTINGS_EXTENSION = "transformsettings";
    public static final String PROP_EXTENSIONS = "extensions";

    public TransformDataLoader() {
        super("com.paterva.maltego.transform.repository.TransformDataObject");
    }

    protected String defaultDisplayName() {
        return "Transform Data Loader";
    }

    protected void initialize() {
        super.initialize();
        this.getExtensions();
    }

    protected MultiDataObject createMultiObject(FileObject fileObject) throws DataObjectExistsException, IOException {
        return new TransformDataObject(fileObject, FileUtil.findBrother((FileObject)fileObject, (String)"transformsettings"), this);
    }

    protected FileObject findPrimaryFile(FileObject fileObject) {
        if (fileObject.isFolder()) {
            return null;
        }
        String string = fileObject.getExt();
        if (string.equalsIgnoreCase("transformsettings")) {
            FileObject fileObject2 = FileUtil.findBrother((FileObject)fileObject, (String)"transform");
            return fileObject2;
        }
        if (string.equalsIgnoreCase("transform")) {
            return fileObject;
        }
        return null;
    }

    protected String actionsContext() {
        return "Loaders/text/maltegotransform+xml/Actions";
    }

    protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject multiDataObject, FileObject fileObject) {
        return new FileEntry(multiDataObject, fileObject);
    }

    protected MultiDataObject.Entry createSecondaryEntry(MultiDataObject multiDataObject, FileObject fileObject) {
        assert ("transformsettings".equals(fileObject.getExt()));
        FileEntry fileEntry = new FileEntry(multiDataObject, fileObject);
        ((TransformDataObject)multiDataObject).setSettingsFileEntry(fileEntry);
        return fileEntry;
    }

    public ExtensionList getExtensions() {
        ExtensionList extensionList = (ExtensionList)this.getProperty((Object)"extensions");
        if (extensionList == null) {
            extensionList = new ExtensionList();
            extensionList.addExtension("transform");
            extensionList.addExtension("transformsettings");
            this.putProperty("extensions", (Object)extensionList, false);
        }
        return extensionList;
    }
}

