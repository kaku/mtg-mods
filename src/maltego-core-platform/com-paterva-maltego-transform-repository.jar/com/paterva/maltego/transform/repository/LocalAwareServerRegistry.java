/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.repository.FSTransformServerRegistry;
import com.paterva.maltego.transform.repository.TransformRepositoriesLock;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class LocalAwareServerRegistry
extends FSTransformServerRegistry {
    private static String LOCAL = "maltego.LocalTAS.tas";
    private static TransformServerInfo _localTas;

    @Override
    protected TransformServerInfo load(FileObject fileObject) throws FileNotFoundException, XmlSerializationException {
        if (LOCAL.equals(fileObject.getNameExt())) {
            return this.localTas(super.load(fileObject));
        }
        return super.load(fileObject);
    }

    @Override
    protected void save(FileObject fileObject, TransformServerInfo transformServerInfo) throws IOException {
        if (LOCAL.equals(fileObject.getNameExt())) {
            super.save(fileObject, new SaveProxy(transformServerInfo));
        } else {
            super.save(fileObject, transformServerInfo);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TransformServerInfo localTas(TransformServerInfo transformServerInfo) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            if (_localTas == null) {
                _localTas = new LocalTransformServerInfo(transformServerInfo);
            }
            return _localTas;
        }
    }

    private static class SaveProxy
    extends LocalTransformServerInfo {
        public SaveProxy(TransformServerInfo transformServerInfo) {
            super(transformServerInfo);
        }

        @Override
        public Set<String> getTransforms() {
            return Collections.emptySet();
        }
    }

    private static class LocalTransformServerInfo
    extends TransformServerInfo {
        public LocalTransformServerInfo(TransformServerInfo transformServerInfo) {
            super("", transformServerInfo.getUrl(), transformServerInfo.getDisplayName());
            this.setEnabled(transformServerInfo.isEnabled());
            this.setDescription(transformServerInfo.getDescription());
        }

        public TransformServerAuthentication getAuthentication() {
            return TransformServerAuthentication.None;
        }

        public Date getLastSync() {
            return new Date();
        }

        public ProtocolVersion getProtocolVersion() {
            return super.getProtocolVersion();
        }

        public String getDefaultRepository() {
            return "Local";
        }

        public Set<String> getTransforms() {
            HashSet<String> hashSet = new HashSet<String>();
            try {
                TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getOrCreateRepository("Local");
                for (TransformDefinition transformDefinition : transformRepository.getAll()) {
                    hashSet.add(transformDefinition.getName());
                }
            }
            catch (IOException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
            return hashSet;
        }
    }

}

