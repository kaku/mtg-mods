/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformDefinition;

interface TransformDefinitionCookie {
    public TransformDefinition getTransformDefinition();
}

