/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.repository.TransformRepositoriesLock;
import com.paterva.maltego.transform.repository.serializer.TransformSeedSerializer;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class FSTransformSeedRepository
extends TransformSeedRepository {
    private static final String MALTEGO_FOLDER = "Maltego";
    private static final String SEED_FOLDER = "Seeds";
    private static final String EXTENSION = "seed";
    private static final String ATTR_DISPLAY_NAME = "displayName";
    private static final String ATTR_DESCRIPTION = "description";
    private static final String ATTR_URL = "url";
    private static final String ATTR_ENABLED = "enabled";
    private final FileObject _configRoot;
    private Map<FastURL, TransformSeed> _seeds;

    public FSTransformSeedRepository() {
        this(FileUtil.getConfigRoot());
    }

    public FSTransformSeedRepository(FileObject fileObject) {
        this._configRoot = fileObject;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void add(TransformSeed transformSeed) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            Map<FastURL, TransformSeed> map = this.getSeeds();
            map.put(transformSeed.getUrl(), transformSeed);
            this.save(transformSeed);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void save(TransformSeed transformSeed) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            try {
                FileObject fileObject = this.getFile(transformSeed.getUrl());
                if (fileObject != null) {
                    FSTransformSeedRepository.update(fileObject, transformSeed);
                } else {
                    FileObject fileObject2 = this.getOrCreateSeedFolder();
                    FSTransformSeedRepository.update(FileUtilities.createUniqueFile((FileObject)fileObject2, (String)FileUtilities.replaceIllegalChars((String)transformSeed.getName()), (String)"seed"), transformSeed);
                }
            }
            catch (IOException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }
    }

    private FileObject getOrCreateSeedFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this.getOrCreateMaltegoFolder(), (String)"Seeds");
    }

    private FileObject getOrCreateMaltegoFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void update(FileObject fileObject, TransformSeed transformSeed) throws IOException {
        TransformSeedSerializer transformSeedSerializer = TransformSeedSerializer.getDefault();
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject.getOutputStream());
            transformSeedSerializer.write(transformSeed, bufferedOutputStream, false);
            fileObject.setAttribute("url", (Object)transformSeed.getUrl().toString());
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void update(Iterable<TransformSeed> iterable) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            this.deleteSeedFiles();
            this._seeds = null;
            for (TransformSeed transformSeed : iterable) {
                this.add(transformSeed);
            }
        }
    }

    private void deleteSeedFiles() {
        FileObject[] arrfileObject;
        for (FileObject fileObject : arrfileObject = this.getSeedFiles()) {
            try {
                fileObject.delete();
                continue;
            }
            catch (IOException var6_6) {
                Exceptions.printStackTrace((Throwable)var6_6);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void deleteSeedFolder() {
        FileObject fileObject = this.getSeedFolder();
        if (fileObject != null) {
            FileLock fileLock = null;
            try {
                fileLock = fileObject.lock();
                fileObject.delete(fileLock);
            }
            catch (IOException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            finally {
                if (fileLock != null) {
                    fileLock.releaseLock();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void remove(FastURL fastURL) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            for (FileObject fileObject : this.getSeedFiles()) {
                if (!fastURL.toString().equals(fileObject.getAttribute("url"))) continue;
                FileLock fileLock = null;
                try {
                    fileLock = fileObject.lock();
                    fileObject.delete(fileLock);
                    if (this._seeds == null) continue;
                    this._seeds.remove((Object)fastURL);
                }
                catch (IOException var8_8) {
                    Exceptions.printStackTrace((Throwable)var8_8);
                }
                finally {
                    if (fileLock != null) {
                        fileLock.releaseLock();
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateUrl(String string, String string2) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            FastURL fastURL = new FastURL(string);
            TransformSeed transformSeed = this.get(fastURL);
            if (transformSeed == null) {
                throw new IllegalArgumentException("Transform Seed does not exist with URL: " + string);
            }
            transformSeed.setUrl(new FastURL(string2));
            this.remove(fastURL);
            this.add(transformSeed);
        }
    }

    private FileObject getSeedFolder() {
        FileObject fileObject = this._configRoot.getFileObject("Maltego");
        if (fileObject != null) {
            return fileObject.getFileObject("Seeds");
        }
        return null;
    }

    private FileObject[] getSeedFiles() {
        ArrayList<FileObject> arrayList = new ArrayList<FileObject>();
        FileObject fileObject = this.getSeedFolder();
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getChildren(false);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                if (fileObject2.isFolder() || !"seed".equals(fileObject2.getExt())) continue;
                arrayList.add(fileObject2);
            }
        }
        return arrayList.toArray((T[])new FileObject[arrayList.size()]);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformSeed[] getAll() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            Map<FastURL, TransformSeed> map = this.getSeeds();
            Collection<TransformSeed> collection = map.values();
            TransformSeed[] arrtransformSeed = collection.toArray((T[])new TransformSeed[0]);
            return arrtransformSeed;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static TransformSeed createSeed(FileObject fileObject) throws MalformedURLException {
        TransformSeed transformSeed = null;
        long l = fileObject.getSize();
        if (l > 0) {
            TransformSeedSerializer transformSeedSerializer = TransformSeedSerializer.getDefault();
            InputStream inputStream = null;
            try {
                inputStream = fileObject.getInputStream();
                transformSeed = transformSeedSerializer.read(inputStream);
            }
            catch (Exception var6_8) {
                Exceptions.printStackTrace((Throwable)var6_8);
            }
            finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    }
                    catch (IOException var6_9) {}
                }
            }
        }
        String string = (String)fileObject.getAttribute("displayName");
        String string2 = (String)fileObject.getAttribute("url");
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            transformSeed = new TransformSeed(new FastURL(string2), string);
            transformSeed.setDescription((String)fileObject.getAttribute("description"));
            Object object = fileObject.getAttribute("enabled");
            transformSeed.setEnabled(object == null ? true : (Boolean)object);
        }
        return transformSeed;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformSeed get(FastURL fastURL) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            return this.getSeeds().get((Object)fastURL);
        }
    }

    private FileObject getFile(FastURL fastURL) {
        for (FileObject fileObject : this.getSeedFiles()) {
            if (!fastURL.toString().equals(fileObject.getAttribute("url"))) continue;
            return fileObject;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void set(TransformSeed[] arrtransformSeed) throws IOException {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            if (arrtransformSeed != null && arrtransformSeed.length != 0) {
                this.deleteSeedFiles();
                this.update(Arrays.asList(arrtransformSeed));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformSeed[] getEnabled() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            ArrayList<TransformSeed> arrayList = new ArrayList<TransformSeed>();
            for (Map.Entry<FastURL, TransformSeed> entry : this.getSeeds().entrySet()) {
                TransformSeed transformSeed = entry.getValue();
                if (!transformSeed.isEnabled()) continue;
                arrayList.add(transformSeed);
            }
            return arrayList.toArray((T[])new TransformSeed[arrayList.size()]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Map<FastURL, TransformSeed> getSeeds() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            if (this._seeds == null) {
                this.load();
            }
            return this._seeds;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void load() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            this._seeds = new HashMap<FastURL, TransformSeed>();
            for (FileObject fileObject : this.getSeedFiles()) {
                try {
                    TransformSeed transformSeed = FSTransformSeedRepository.createSeed(fileObject);
                    if (transformSeed == null) continue;
                    this._seeds.put(transformSeed.getUrl(), transformSeed);
                    continue;
                }
                catch (MalformedURLException var6_7) {
                    Exceptions.printStackTrace((Throwable)var6_7);
                }
            }
        }
    }
}

