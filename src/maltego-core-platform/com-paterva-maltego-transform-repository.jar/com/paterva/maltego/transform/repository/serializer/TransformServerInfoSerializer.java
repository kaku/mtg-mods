/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.repository.serializer.TransformServerInfoStub;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.openide.util.Exceptions;

public class TransformServerInfoSerializer {
    public TransformServerInfo read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformServerInfoStub transformServerInfoStub = (TransformServerInfoStub)xmlSerializer.read(TransformServerInfoStub.class, inputStream);
        return TransformServerInfoSerializer.translate(transformServerInfoStub);
    }

    private static TransformServerInfo translate(TransformServerInfoStub transformServerInfoStub) {
        TransformServerInfo transformServerInfo = new TransformServerInfo(transformServerInfoStub.getSeedUrls(), new FastURL(transformServerInfoStub.getUrl().toString()), transformServerInfoStub.getName(), transformServerInfoStub.getDescription());
        transformServerInfo.setEnabled(transformServerInfoStub.isEnabled());
        transformServerInfo.setProtocolVersion(ProtocolVersion.get((String)transformServerInfoStub.getProtocolVersion()));
        transformServerInfo.getTransforms().addAll(transformServerInfoStub.getTransforms());
        transformServerInfo.setLastSync(transformServerInfoStub.getLastSync());
        if (transformServerInfoStub.getAuthentication() != null) {
            transformServerInfo.setAuthentication(TransformServerAuthentication.get((String)transformServerInfoStub.getAuthentication().getType()));
        }
        transformServerInfo.markClean();
        return transformServerInfo;
    }

    private static URL getUrl(String string) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            try {
                return new URL(string);
            }
            catch (MalformedURLException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return null;
    }

    public static void write(TransformServerInfo transformServerInfo, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformServerInfoStub transformServerInfoStub = TransformServerInfoSerializer.translate(transformServerInfo);
        xmlSerializer.write((Object)transformServerInfoStub, outputStream);
    }

    private static TransformServerInfoStub translate(TransformServerInfo transformServerInfo) {
        TransformServerInfoStub transformServerInfoStub = new TransformServerInfoStub();
        transformServerInfoStub.setEnabled(transformServerInfo.isEnabled());
        transformServerInfoStub.setName(transformServerInfo.getDisplayName());
        try {
            transformServerInfoStub.setUrl(transformServerInfo.getUrl().getURL());
        }
        catch (MalformedURLException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        transformServerInfoStub.setDescription(transformServerInfo.getDescription());
        transformServerInfoStub.setProtocolVersion(transformServerInfo.getProtocolVersion() == null ? "0.0" : transformServerInfo.getProtocolVersion().toString());
        if (transformServerInfo.getTransforms() != null) {
            for (String string : transformServerInfo.getTransforms()) {
                transformServerInfoStub.addTransform(string);
            }
        }
        transformServerInfoStub.setLastSync(transformServerInfo.getLastSync());
        transformServerInfoStub.setAuthentication(new TransformServerInfoStub.Authentication(transformServerInfo.getAuthentication().getTypeName()));
        transformServerInfoStub.setSeedUrls(transformServerInfo.getSeedUrls());
        return transformServerInfoStub;
    }
}

