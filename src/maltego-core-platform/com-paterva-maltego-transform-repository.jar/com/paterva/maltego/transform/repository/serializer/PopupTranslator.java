/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Popup
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.Popup;

class PopupTranslator {
    PopupTranslator() {
    }

    public static Popup get(boolean bl) {
        if (bl) {
            return Popup.Yes;
        }
        return Popup.No;
    }

    public static boolean get(Popup popup) {
        switch (popup) {
            case Yes: {
                return true;
            }
            case No: {
                return false;
            }
        }
        return false;
    }

}

