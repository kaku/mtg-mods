/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.repository.serializer.TransformDefinitionStub;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorStub;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsSerializer;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsStub;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformDefinitionSerializer {
    public TransformDefinition read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformDefinitionStub transformDefinitionStub = (TransformDefinitionStub)xmlSerializer.read(TransformDefinitionStub.class, inputStream);
        return this.translate(transformDefinitionStub);
    }

    private TransformDefinition translate(TransformDefinitionStub transformDefinitionStub) throws XmlSerializationException {
        TransformDescriptor transformDescriptor = TransformDescriptorSerializer.translate(transformDefinitionStub.getDescriptor());
        TransformSettings transformSettings = TransformSettingsSerializer.translate(transformDefinitionStub.getSettings());
        return new TransformDefinition(transformDescriptor, transformSettings);
    }

    public void write(TransformDefinition transformDefinition, OutputStream outputStream, boolean bl) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformDefinitionStub transformDefinitionStub = this.translate(transformDefinition, bl);
        xmlSerializer.write((Object)transformDefinitionStub, outputStream);
    }

    private TransformDefinitionStub translate(TransformDefinition transformDefinition, boolean bl) throws XmlSerializationException {
        TransformDescriptorStub transformDescriptorStub = TransformDescriptorSerializer.translate((TransformDescriptor)transformDefinition);
        TransformSettingsStub transformSettingsStub = TransformSettingsSerializer.translate((TransformSettings)transformDefinition, transformDefinition.getProperties(), bl);
        TransformDefinitionStub transformDefinitionStub = new TransformDefinitionStub();
        transformDefinitionStub.setDescriptor(transformDescriptorStub);
        transformDefinitionStub.setSettings(transformSettingsStub);
        return transformDefinitionStub;
    }
}

