/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.repository.serializer.PropertiesStub;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="MaltegoTransform", strict=0)
public class TransformDescriptorStub {
    @Attribute(name="name")
    private String _name;
    @Attribute(name="displayName")
    private String _displayName;
    @Element(name="TransformAdapter")
    private String _transformAdapter;
    @Element(name="Properties", required=0)
    private PropertiesStub _properties;
    @Element(name="Extends", required=0)
    private ExtendsStub _extends;
    @ElementList(name="InputConstraints", type=EntityReferenceStub.class, required=0)
    private List<EntityReferenceStub> _inputConstraints;
    @ElementList(name="OutputEntities", type=EntityReferenceStub.class, required=0)
    private List<EntityReferenceStub> _outputEntities;
    @Attribute(name="abstract", required=0)
    private boolean _abstract;
    @Attribute(name="template", required=0)
    private boolean _template;
    @Attribute(name="visibility", required=0)
    private String _visibility;
    @Attribute(name="description", required=0)
    private String _description;
    @Attribute(name="helpURL", required=0)
    private String _helpUrl;
    @Element(name="Help", data=1, required=0)
    private String _helpText;
    @Attribute(name="author", required=0)
    private String _author;
    @Attribute(name="owner", required=0)
    private String _owner;
    @Element(name="Disclaimer", data=1, required=0)
    private String _disclaimer;
    @Attribute(name="locationRelevance", required=0)
    private String _locationRelevance;
    @Attribute(name="version", required=0)
    private String _version;
    @ElementList(name="defaultSets", type=SetReferenceStub.class, required=0)
    private List<SetReferenceStub> _defaultSets;
    @Attribute(name="requireDisplayInfo", required=0)
    private boolean _requireDisplayInfo;
    @Element(name="StealthLevel", required=0)
    private int _stealthLevel;
    @Element(name="Authenticator", required=0)
    private String _authenticator;

    public String getBaseName() {
        if (this._extends != null) {
            return this._extends.getTransform();
        }
        return null;
    }

    public void setBaseName(String string) {
        if (this._extends == null) {
            this._extends = new ExtendsStub();
        }
        this._extends.setTransform(string);
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public PropertiesStub getProperties() {
        return this._properties;
    }

    public void setProperties(PropertiesStub propertiesStub) {
        this._properties = propertiesStub;
    }

    public List<EntityReferenceStub> getInputConstraints() {
        if (this._inputConstraints == null) {
            this._inputConstraints = new LinkedList<EntityReferenceStub>();
        }
        return this._inputConstraints;
    }

    public Collection<String> getOutputEntities() {
        LinkedList<String> linkedList = new LinkedList<String>();
        if (this._outputEntities != null) {
            for (EntityReferenceStub entityReferenceStub : this._outputEntities) {
                if (entityReferenceStub.getTypeName() == null) continue;
                linkedList.add(entityReferenceStub.getTypeName());
            }
        }
        return linkedList;
    }

    public void setOutputEntities(Collection<String> collection) {
        this._outputEntities = new LinkedList<EntityReferenceStub>();
        if (collection != null && collection.size() > 0) {
            for (String string : collection) {
                this._outputEntities.add(new EntityReferenceStub(string));
            }
        }
    }

    public boolean isAbstract() {
        return this._abstract;
    }

    public void setAbstract(boolean bl) {
        this._abstract = bl;
    }

    public String getVisibility() {
        return this._visibility;
    }

    public void setVisibility(String string) {
        this._visibility = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getHelpUrl() {
        return this._helpUrl;
    }

    public void setHelpUrl(String string) {
        this._helpUrl = string;
    }

    public String getHelpText() {
        return this._helpText;
    }

    public void setHelpText(String string) {
        this._helpText = string;
    }

    public String getAuthor() {
        return this._author;
    }

    public void setAuthor(String string) {
        this._author = string;
    }

    public String getOwner() {
        return this._owner;
    }

    public void setOwner(String string) {
        this._owner = string;
    }

    public String getDisclaimer() {
        return this._disclaimer;
    }

    public void setDisclaimer(String string) {
        this._disclaimer = string;
    }

    public String getLocationRelevance() {
        return this._locationRelevance;
    }

    public void setLocationRelevance(String string) {
        this._locationRelevance = string;
    }

    public String getVersion() {
        return this._version;
    }

    public void setVersion(String string) {
        this._version = string;
    }

    public String[] getDefaultSets() {
        if (this._defaultSets == null) {
            return new String[0];
        }
        String[] arrstring = new String[this._defaultSets.size()];
        int n = 0;
        for (SetReferenceStub setReferenceStub : this._defaultSets) {
            arrstring[n] = setReferenceStub.getName();
            ++n;
        }
        return arrstring;
    }

    public void setDefaultSets(String[] arrstring) {
        this._defaultSets = new LinkedList<SetReferenceStub>();
        if (arrstring != null) {
            for (String string : arrstring) {
                this._defaultSets.add(new SetReferenceStub(string));
            }
        }
    }

    public boolean requiresDisplayInfo() {
        return this._requireDisplayInfo;
    }

    public void setRequireDisplayInfo(boolean bl) {
        this._requireDisplayInfo = bl;
    }

    public int getStealthLevel() {
        return this._stealthLevel;
    }

    public void setStealthLevel(int n) {
        this._stealthLevel = n;
    }

    public boolean isTemplate() {
        return this._template;
    }

    public void setTemplate(boolean bl) {
        this._template = bl;
    }

    public String getTransformAdapter() {
        return this._transformAdapter;
    }

    public void setTransformAdapter(String string) {
        this._transformAdapter = string;
    }

    public String getAuthenticator() {
        return this._authenticator;
    }

    public void setAuthenticator(String string) {
        this._authenticator = string;
    }

    @Root(name="Property", strict=0)
    static class PropertyConstraintStub {
        @Attribute(name="name", required=0)
        private String _name;
        @Attribute(name="type", required=0)
        private String _type;
        @Attribute(name="nullable", required=0)
        private boolean _nullable;

        PropertyConstraintStub() {
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }

        public String getType() {
            return this._type;
        }

        public void setType(String string) {
            this._type = string;
        }

        public boolean isNullable() {
            return this._nullable;
        }

        public void setNullable(boolean bl) {
            this._nullable = bl;
        }
    }

    @Root(name="Entity", strict=0)
    static class EntityReferenceStub {
        @Attribute(name="type", required=0)
        private String _typeName;
        @Attribute(name="min", required=0)
        private int _minimum = 0;
        @Attribute(name="max", required=0)
        private int _maximum = 0;
        @ElementList(inline=1, type=PropertyConstraintStub.class, required=0)
        private List<PropertyConstraintStub> _properties;

        public EntityReferenceStub() {
        }

        public EntityReferenceStub(String string) {
            this._typeName = string;
        }

        public String getTypeName() {
            return this._typeName;
        }

        public void setTypeName(String string) {
            this._typeName = string;
        }

        public int getMinimum() {
            return this._minimum;
        }

        public void setMinimum(int n) {
            this._minimum = n;
        }

        public int getMaximum() {
            return this._maximum;
        }

        public void setMaximum(int n) {
            this._maximum = n;
        }

        public List<PropertyConstraintStub> getProperties() {
            if (this._properties == null) {
                this._properties = new LinkedList<PropertyConstraintStub>();
            }
            return this._properties;
        }
    }

    @Root(name="Set", strict=0)
    static class SetReferenceStub {
        @Attribute(name="name", required=1)
        private String _setName;

        public SetReferenceStub() {
        }

        public SetReferenceStub(String string) {
            this._setName = string;
        }

        public String getName() {
            return this._setName;
        }

        public void setName(String string) {
            this._setName = string;
        }
    }

    @Root(name="Extends", strict=0)
    static class ExtendsStub {
        @Attribute(name="transform")
        private String _transform;

        public String getTransform() {
            return this._transform;
        }

        public void setTransform(String string) {
            this._transform = string;
        }
    }

}

