/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.repository.TransformRepositoriesLock;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class FSTransformSetRepository
extends TransformSetRepository {
    private static final String MALTEGO_FOLDER = "Maltego";
    private static final String SET_FOLDER = "TransformSets";
    private static final String ATTR_DESCRIPTION = "description";
    private Set<TransformSet> _sets;
    private final FileObject _configRoot;

    public FSTransformSetRepository() {
        this(FileUtil.getConfigRoot());
    }

    public FSTransformSetRepository(FileObject fileObject) {
        this._configRoot = fileObject;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean put(TransformSet transformSet) {
        try {
            TransformSet transformSet2;
            Object object = TransformRepositoriesLock.LOCK;
            synchronized (object) {
                this.replaceSetFolder(transformSet);
                transformSet.markClean();
                transformSet2 = this.get(transformSet.getName());
                if (transformSet2 != null) {
                    this.sets().remove((Object)transformSet2);
                    this.sets().add(transformSet);
                } else {
                    this.sets().add(transformSet);
                }
            }
            if (transformSet2 != null) {
                this.fireItemChanged((Object)transformSet);
            } else {
                this.fireItemAdded((Object)transformSet);
            }
            return true;
        }
        catch (IOException var2_4) {
            Exceptions.printStackTrace((Throwable)var2_4);
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean remove(String string) {
        try {
            FileObject fileObject;
            TransformSet transformSet = null;
            Object object = TransformRepositoriesLock.LOCK;
            synchronized (object) {
                FileObject fileObject2 = this.getOrCreateSetFolder();
                fileObject = fileObject2.getFileObject(string);
                if (fileObject != null) {
                    fileObject.delete();
                    transformSet = this.get(string);
                    this.sets().remove((Object)transformSet);
                }
            }
            if (fileObject != null) {
                this.fireItemRemoved((Object)transformSet);
                return true;
            }
        }
        catch (IOException var2_6) {
            Exceptions.printStackTrace((Throwable)var2_6);
        }
        return false;
    }

    private Set<TransformSet> sets() {
        if (this._sets == null) {
            this._sets = new HashSet<TransformSet>();
            try {
                FileObject fileObject = this.getOrCreateSetFolder();
                if (fileObject != null) {
                    for (FileObject fileObject2 : fileObject.getChildren()) {
                        TransformSet transformSet = FSTransformSetRepository.loadSet(fileObject2);
                        transformSet.markClean();
                        this._sets.add(transformSet);
                    }
                }
            }
            catch (IOException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
        return this._sets;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<TransformSet> allSets() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            return Collections.unmodifiableSet(this.sets());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean contains(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            TransformSet transformSet = this.get(string);
            return transformSet != null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformSet get(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            for (TransformSet transformSet : this.sets()) {
                if (!transformSet.getName().equals(string)) continue;
                return transformSet;
            }
            return null;
        }
    }

    private static TransformSet loadSet(FileObject fileObject) {
        TransformSet transformSet = new TransformSet(fileObject.getNameExt());
        transformSet.setDescription((String)fileObject.getAttribute("description"));
        for (FileObject fileObject2 : fileObject.getChildren()) {
            transformSet.addTransform(fileObject2.getNameExt());
        }
        return transformSet;
    }

    private FileObject getOrCreateSetFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this.getOrCreateMaltegoFolder(), (String)"TransformSets");
    }

    private FileObject getOrCreateMaltegoFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego");
    }

    private boolean replaceSetFolder(TransformSet transformSet) throws IOException {
        FileObject fileObject = this.getOrCreateSetFolder();
        FileObject fileObject2 = fileObject.getFileObject(transformSet.getName());
        boolean bl = false;
        if (fileObject2 != null) {
            this.deleteFolder(fileObject2);
            bl = true;
        }
        fileObject2 = fileObject.createFolder(transformSet.getName());
        fileObject2.setAttribute("description", (Object)(transformSet.getDescription() == null ? "" : transformSet.getDescription()));
        for (String string : transformSet.getAllTransforms()) {
            fileObject2.createData(string);
        }
        return bl;
    }

    private void deleteFolder(FileObject fileObject) throws IOException {
        FileLock fileLock = null;
        try {
            fileLock = fileObject.lock();
            fileObject.delete(fileLock);
        }
        finally {
            if (fileLock != null) {
                fileLock.releaseLock();
            }
        }
    }
}

