/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.repository.serializer.PropertiesStub;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsPropertyStub;
import java.net.URL;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="TransformSeed", strict=0)
class TransformSeedStub {
    @Attribute(name="url")
    private URL _url;
    @Attribute(name="name")
    private String _name;
    @Attribute(name="description", required=0)
    private String _description;
    @Attribute(name="enabled")
    private boolean _enabled;
    @Element(name="GlobalTransformProperties", required=0)
    private PropertiesStub _globalTxProperties;
    @ElementList(name="GlobalTransformSettings", type=TransformSettingsPropertyStub.class, required=0)
    private List<TransformSettingsPropertyStub> _globalTxSettings;

    TransformSeedStub() {
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public URL getUrl() {
        return this._url;
    }

    public void setUrl(URL uRL) {
        this._url = uRL;
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public void setEnabled(boolean bl) {
        this._enabled = bl;
    }

    public PropertiesStub getGlobalTxProperties() {
        return this._globalTxProperties;
    }

    public void setGlobalTxProperties(PropertiesStub propertiesStub) {
        this._globalTxProperties = propertiesStub;
    }

    public List<TransformSettingsPropertyStub> getGlobalTxSettings() {
        return this._globalTxSettings;
    }

    public void setGlobalTxSettings(List<TransformSettingsPropertyStub> list) {
        this._globalTxSettings = list;
    }
}

