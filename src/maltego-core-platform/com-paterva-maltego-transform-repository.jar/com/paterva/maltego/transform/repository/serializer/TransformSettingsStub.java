/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.repository.serializer.TransformSettingsPropertyStub;
import java.util.LinkedList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="TransformSettings", strict=0)
public class TransformSettingsStub {
    @Attribute(name="enabled", required=0)
    private boolean _enabled = true;
    @Attribute(name="disclaimerAccepted", required=0)
    private boolean _disclaimerAccepted = false;
    @Attribute(name="showHelp", required=0)
    private boolean _showHelp = false;
    @ElementList(name="Properties", type=TransformSettingsPropertyStub.class, required=0)
    private List<TransformSettingsPropertyStub> _fields;
    @Attribute(name="runWithAll", required=0)
    private boolean _runWithAll = true;
    @Attribute(name="favorite", required=0)
    private boolean _favorite = false;

    public TransformSettingsStub() {
    }

    public TransformSettingsStub(List<TransformSettingsPropertyStub> list) {
        this._fields = list;
    }

    public List<TransformSettingsPropertyStub> getProperties() {
        if (this._fields == null) {
            this._fields = new LinkedList<TransformSettingsPropertyStub>();
        }
        return this._fields;
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public void setEnabled(boolean bl) {
        this._enabled = bl;
    }

    public boolean isDisclaimerAccepted() {
        return this._disclaimerAccepted;
    }

    public void setDisclaimerAccepted(boolean bl) {
        this._disclaimerAccepted = bl;
    }

    public boolean isShowHelp() {
        return this._showHelp;
    }

    public void setShowHelp(boolean bl) {
        this._showHelp = bl;
    }

    public boolean isRunWithAll() {
        return this._runWithAll;
    }

    public void setRunWithAll(boolean bl) {
        this._runWithAll = bl;
    }

    public boolean isFavorite() {
        return this._favorite;
    }

    public void setFavorite(boolean bl) {
        this._favorite = bl;
    }
}

