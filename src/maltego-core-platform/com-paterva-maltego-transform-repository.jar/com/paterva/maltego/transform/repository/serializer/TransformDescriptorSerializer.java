/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.StatelessMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.transform.descriptor.CompoundConstraint
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.EntityConstraint
 *  com.paterva.maltego.transform.descriptor.IntegerStealthLevel
 *  com.paterva.maltego.transform.descriptor.PersistenceMode
 *  com.paterva.maltego.transform.descriptor.PropertyConstraint
 *  com.paterva.maltego.transform.descriptor.StealthLevel
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.serializer.DisplayDescriptorTranslator
 *  com.paterva.maltego.typing.serializer.FieldGroupsStub
 *  com.paterva.maltego.typing.serializer.FieldStub
 *  com.paterva.maltego.typing.serializer.UnresolvedReferenceException
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.transform.descriptor.CompoundConstraint;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.EntityConstraint;
import com.paterva.maltego.transform.descriptor.IntegerStealthLevel;
import com.paterva.maltego.transform.descriptor.PersistenceMode;
import com.paterva.maltego.transform.descriptor.PropertyConstraint;
import com.paterva.maltego.transform.descriptor.StealthLevel;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.transform.repository.serializer.PropertiesStub;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorStub;
import com.paterva.maltego.transform.repository.serializer.TransformPropertyStub;
import com.paterva.maltego.transform.repository.serializer.VisibilityTranslator;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.serializer.DisplayDescriptorTranslator;
import com.paterva.maltego.typing.serializer.FieldGroupsStub;
import com.paterva.maltego.typing.serializer.FieldStub;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TransformDescriptorSerializer {
    public TransformDescriptor read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformDescriptorStub transformDescriptorStub = (TransformDescriptorStub)xmlSerializer.read(TransformDescriptorStub.class, inputStream);
        return TransformDescriptorSerializer.translate(transformDescriptorStub);
    }

    public static TransformDescriptor translate(TransformDescriptorStub transformDescriptorStub) throws XmlSerializationException {
        TransformDescriptor transformDescriptor = new TransformDescriptor(transformDescriptorStub.getTransformAdapter(), transformDescriptorStub.getName(), transformDescriptorStub.getBaseName(), TransformDescriptorSerializer.translate(transformDescriptorStub.getProperties()));
        transformDescriptor.setAbstract(transformDescriptorStub.isAbstract());
        transformDescriptor.setTemplate(transformDescriptorStub.isTemplate());
        transformDescriptor.setAuthor(transformDescriptorStub.getAuthor());
        transformDescriptor.setDefaultSets(transformDescriptorStub.getDefaultSets());
        transformDescriptor.setDescription(transformDescriptorStub.getDescription());
        transformDescriptor.setDisclaimer(transformDescriptorStub.getDisclaimer());
        transformDescriptor.setDisplayName(transformDescriptorStub.getDisplayName());
        transformDescriptor.setHelpText(transformDescriptorStub.getHelpText());
        transformDescriptor.setHelpUrl(transformDescriptorStub.getHelpUrl());
        transformDescriptor.setVisibility(VisibilityTranslator.get(transformDescriptorStub.getVisibility()));
        transformDescriptor.setLocationRelevance(transformDescriptorStub.getLocationRelevance());
        transformDescriptor.setOwner(transformDescriptorStub.getOwner());
        transformDescriptor.setRequireDisplayInfo(transformDescriptorStub.requiresDisplayInfo());
        transformDescriptor.setStealthLevel((StealthLevel)new IntegerStealthLevel(transformDescriptorStub.getStealthLevel()));
        transformDescriptor.setVersion(transformDescriptorStub.getVersion());
        if (transformDescriptorStub.getInputConstraints() != null) {
            transformDescriptor.setInputConstraint(TransformDescriptorSerializer.translate(transformDescriptorStub.getInputConstraints()));
        }
        if (transformDescriptorStub.getOutputEntities() != null) {
            transformDescriptor.getOutputEntities().addAll(transformDescriptorStub.getOutputEntities());
        }
        transformDescriptor.setMatchingRule(StatelessMatchingRuleDescriptor.Value);
        transformDescriptor.setAuthenticator(transformDescriptorStub.getAuthenticator());
        return transformDescriptor;
    }

    private static Constraint translate(List<TransformDescriptorStub.EntityReferenceStub> list) {
        if (list.size() > 0) {
            CompoundConstraint compoundConstraint = new CompoundConstraint();
            for (TransformDescriptorStub.EntityReferenceStub entityReferenceStub : list) {
                EntityConstraint entityConstraint = new EntityConstraint();
                entityConstraint.setMinimum(entityReferenceStub.getMinimum());
                entityConstraint.setMaximum(entityReferenceStub.getMaximum());
                entityConstraint.setTypeName(entityReferenceStub.getTypeName());
                LinkedList<PropertyConstraint> linkedList = new LinkedList<PropertyConstraint>();
                if (entityReferenceStub.getProperties() != null) {
                    for (TransformDescriptorStub.PropertyConstraintStub propertyConstraintStub : entityReferenceStub.getProperties()) {
                        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyConstraintStub.getType());
                        if (typeDescriptor == null) continue;
                        PropertyConstraint propertyConstraint = new PropertyConstraint(typeDescriptor.getType(), propertyConstraintStub.getName(), propertyConstraintStub.isNullable());
                        linkedList.add(propertyConstraint);
                    }
                }
                entityConstraint.setProperties(linkedList);
                compoundConstraint.add((Constraint)entityConstraint);
            }
            return compoundConstraint;
        }
        return null;
    }

    static DisplayDescriptorCollection translate(PropertiesStub propertiesStub) throws UnresolvedReferenceException {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        if (propertiesStub != null) {
            for (TransformPropertyStub transformPropertyStub : propertiesStub.getFields()) {
                TransformPropertyDescriptor transformPropertyDescriptor = TransformDescriptorSerializer.translate(transformPropertyStub);
                displayDescriptorList.add((DisplayDescriptor)transformPropertyDescriptor);
            }
        }
        return displayDescriptorList;
    }

    public static TransformPropertyDescriptor translate(TransformPropertyStub transformPropertyStub) throws UnresolvedReferenceException {
        DisplayDescriptorTranslator displayDescriptorTranslator = new DisplayDescriptorTranslator();
        TransformPropertyDescriptor transformPropertyDescriptor = new TransformPropertyDescriptor(displayDescriptorTranslator.translate((FieldStub)transformPropertyStub));
        transformPropertyDescriptor.setAbstract(transformPropertyStub.isAbstract());
        transformPropertyDescriptor.setPersistence(TransformDescriptorSerializer.getPersistence(transformPropertyStub.getPersistence()));
        transformPropertyDescriptor.setVisibility(VisibilityTranslator.get(transformPropertyStub.getVisibility()));
        transformPropertyDescriptor.setPopup(transformPropertyStub.isPopup());
        transformPropertyDescriptor.setAuth(transformPropertyStub.isAuth());
        return transformPropertyDescriptor;
    }

    private static PersistenceMode getPersistence(String string) {
        if ("no-export".equals(string)) {
            return PersistenceMode.NoExport;
        }
        if ("none".equals(string)) {
            return PersistenceMode.None;
        }
        return PersistenceMode.Normal;
    }

    private static String getPersistence(PersistenceMode persistenceMode) {
        switch (persistenceMode) {
            case NoExport: {
                return "no-export";
            }
            case None: {
                return "none";
            }
        }
        return null;
    }

    public void write(TransformDescriptor transformDescriptor, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformDescriptorStub transformDescriptorStub = TransformDescriptorSerializer.translate(transformDescriptor);
        xmlSerializer.write((Object)transformDescriptorStub, outputStream);
    }

    public static TransformDescriptorStub translate(TransformDescriptor transformDescriptor) throws XmlSerializationException {
        TransformDescriptorStub transformDescriptorStub = new TransformDescriptorStub();
        transformDescriptorStub.setName(transformDescriptor.getName());
        transformDescriptorStub.setTransformAdapter(transformDescriptor.getTransformAdapterClass());
        transformDescriptorStub.setAbstract(transformDescriptor.isAbstract());
        transformDescriptorStub.setTemplate(transformDescriptor.isTemplate());
        transformDescriptorStub.setAuthor(transformDescriptor.getAuthor());
        transformDescriptorStub.setDescription(transformDescriptor.getDescription());
        transformDescriptorStub.setDisclaimer(transformDescriptor.getDisclaimer());
        transformDescriptorStub.setDisplayName(transformDescriptor.getDisplayName());
        transformDescriptorStub.setHelpText(transformDescriptor.getHelpText());
        transformDescriptorStub.setHelpUrl(transformDescriptor.getHelpUrl());
        transformDescriptorStub.setVisibility(VisibilityTranslator.get(transformDescriptor.getVisibility()));
        transformDescriptorStub.setLocationRelevance(transformDescriptor.getLocationRelevance());
        transformDescriptorStub.setOwner(transformDescriptor.getOwner());
        transformDescriptorStub.setRequireDisplayInfo(transformDescriptor.isRequireDisplayInfo());
        int n = 0;
        if (transformDescriptor.getStealthLevel() != null) {
            IntegerStealthLevel integerStealthLevel = (IntegerStealthLevel)transformDescriptor.getStealthLevel();
            n = integerStealthLevel.getLevelNumber();
        }
        transformDescriptorStub.setStealthLevel(n);
        transformDescriptorStub.setVersion(transformDescriptor.getVersion());
        if (transformDescriptor.getDefaultSets() != null) {
            transformDescriptorStub.setDefaultSets(transformDescriptor.getDefaultSets());
        }
        if (transformDescriptor.getInputConstraint() != null) {
            transformDescriptorStub.getInputConstraints().addAll(TransformDescriptorSerializer.translate(transformDescriptor.getInputConstraint()));
        }
        if (transformDescriptor.getOutputEntities() != null) {
            transformDescriptorStub.setOutputEntities(transformDescriptor.getOutputEntities());
        }
        if (transformDescriptor.getProperties() != null) {
            transformDescriptorStub.setProperties(TransformDescriptorSerializer.translate(transformDescriptor.getProperties()));
        }
        transformDescriptorStub.setAuthenticator(transformDescriptor.getAuthenticator());
        return transformDescriptorStub;
    }

    static PropertiesStub translate(DisplayDescriptorCollection displayDescriptorCollection) throws UnresolvedReferenceException {
        ArrayList<TransformPropertyStub> arrayList = new ArrayList<TransformPropertyStub>();
        if (displayDescriptorCollection != null) {
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                if (!(displayDescriptor instanceof TransformPropertyDescriptor)) continue;
                TransformPropertyDescriptor transformPropertyDescriptor = (TransformPropertyDescriptor)displayDescriptor;
                TransformPropertyStub transformPropertyStub = TransformDescriptorSerializer.translate(transformPropertyDescriptor);
                arrayList.add(transformPropertyStub);
            }
        }
        PropertiesStub propertiesStub = new PropertiesStub(arrayList, null);
        return propertiesStub;
    }

    public static TransformPropertyStub translate(TransformPropertyDescriptor transformPropertyDescriptor) throws UnresolvedReferenceException {
        DisplayDescriptorTranslator displayDescriptorTranslator = new DisplayDescriptorTranslator();
        TransformPropertyStub transformPropertyStub = new TransformPropertyStub();
        displayDescriptorTranslator.translate((DisplayDescriptor)transformPropertyDescriptor, (FieldStub)transformPropertyStub);
        transformPropertyStub.setAbstract(transformPropertyDescriptor.isAbstract());
        transformPropertyStub.setPopup(transformPropertyDescriptor.isPopup());
        transformPropertyStub.setPersistence(TransformDescriptorSerializer.getPersistence(transformPropertyDescriptor.getPersistence()));
        transformPropertyStub.setVisibility(VisibilityTranslator.get(transformPropertyDescriptor.getVisibility()));
        transformPropertyStub.setAuth(transformPropertyDescriptor.isAuth());
        return transformPropertyStub;
    }

    private static Collection<TransformDescriptorStub.EntityReferenceStub> translate(Constraint constraint) {
        ArrayList<TransformDescriptorStub.EntityReferenceStub> arrayList = new ArrayList<TransformDescriptorStub.EntityReferenceStub>();
        if (constraint instanceof EntityConstraint) {
            arrayList.add(TransformDescriptorSerializer.translate((EntityConstraint)constraint));
        } else if (constraint instanceof CompoundConstraint) {
            CompoundConstraint compoundConstraint = (CompoundConstraint)constraint;
            for (EntityConstraint entityConstraint : compoundConstraint) {
                arrayList.add(TransformDescriptorSerializer.translate(entityConstraint));
            }
        }
        return arrayList;
    }

    private static TransformDescriptorStub.EntityReferenceStub translate(EntityConstraint entityConstraint) {
        TransformDescriptorStub.EntityReferenceStub entityReferenceStub = new TransformDescriptorStub.EntityReferenceStub(entityConstraint.getTypeName());
        entityReferenceStub.setMaximum(entityConstraint.getMaximum());
        entityReferenceStub.setMinimum(entityConstraint.getMinimum());
        if (entityConstraint.getProperties() != null) {
            for (PropertyConstraint propertyConstraint : entityConstraint.getProperties()) {
                TransformDescriptorStub.PropertyConstraintStub propertyConstraintStub = new TransformDescriptorStub.PropertyConstraintStub();
                propertyConstraintStub.setName(propertyConstraint.getName());
                propertyConstraintStub.setType(TypeRegistry.getDefault().getType(propertyConstraint.getType()).getTypeName());
                propertyConstraintStub.setNullable(propertyConstraint.isNullable());
                entityReferenceStub.getProperties().add(propertyConstraintStub);
            }
        }
        return entityReferenceStub;
    }

}

