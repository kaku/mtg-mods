/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.repository.serializer;

import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="Seed", strict=0)
public class TransformServerSeedStub {
    @Text(required=1)
    private String _seedUrl;

    public TransformServerSeedStub() {
    }

    public TransformServerSeedStub(String string) {
        this._seedUrl = string;
    }

    public String getSeedUrl() {
        return this._seedUrl;
    }

    public void setSeedUrl(String string) {
        this._seedUrl = string;
    }
}

