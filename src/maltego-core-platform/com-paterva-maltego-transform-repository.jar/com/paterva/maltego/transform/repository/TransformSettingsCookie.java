/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformSettings;

interface TransformSettingsCookie {
    public TransformSettings getTransformSettings();
}

