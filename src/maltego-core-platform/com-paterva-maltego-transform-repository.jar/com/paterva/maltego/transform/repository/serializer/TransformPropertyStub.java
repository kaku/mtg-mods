/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.serializer.FieldStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.typing.serializer.FieldStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Property", strict=0)
public class TransformPropertyStub
extends FieldStub {
    @Attribute(name="popup", required=0)
    private boolean _popup;
    @Attribute(name="abstract", required=0)
    private boolean _abstract;
    @Attribute(name="persistence", required=0)
    private String _persistence;
    @Attribute(name="visibility", required=0)
    private String _visibility;
    @Attribute(name="auth", required=0)
    private boolean _auth = false;

    public boolean isAbstract() {
        return this._abstract;
    }

    public void setAbstract(boolean bl) {
        this._abstract = bl;
    }

    public String getPersistence() {
        return this._persistence;
    }

    public void setPersistence(String string) {
        this._persistence = string;
    }

    public String getVisibility() {
        return this._visibility;
    }

    public void setVisibility(String string) {
        this._visibility = string;
    }

    public boolean isPopup() {
        return this._popup;
    }

    public void setPopup(boolean bl) {
        this._popup = bl;
    }

    public boolean isAuth() {
        return this._auth;
    }

    public void setAuth(boolean bl) {
        this._auth = bl;
    }
}

