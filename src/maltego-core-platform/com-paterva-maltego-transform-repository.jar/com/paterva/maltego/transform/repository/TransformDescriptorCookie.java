/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;

interface TransformDescriptorCookie {
    public TransformDescriptor getTransformDescriptor();
}

