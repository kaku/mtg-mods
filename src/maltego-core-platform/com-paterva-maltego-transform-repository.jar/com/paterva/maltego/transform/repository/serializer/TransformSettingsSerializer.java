/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.PersistenceMode
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.PersistenceMode;
import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.transform.repository.ListMapTransformSettings;
import com.paterva.maltego.transform.repository.serializer.PopupTranslator;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsPropertyStub;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsStub;
import com.paterva.maltego.transform.repository.serializer.VisibilityTranslator;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class TransformSettingsSerializer {
    public TransformSettings read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformSettingsStub transformSettingsStub = (TransformSettingsStub)xmlSerializer.read(TransformSettingsStub.class, inputStream);
        return TransformSettingsSerializer.translate(transformSettingsStub);
    }

    public void write(TransformDefinition transformDefinition, OutputStream outputStream, boolean bl) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TransformSettingsStub transformSettingsStub = TransformSettingsSerializer.translate((TransformSettings)transformDefinition, transformDefinition.getProperties(), bl);
        xmlSerializer.write((Object)transformSettingsStub, outputStream);
    }

    public static TransformSettings translate(TransformSettingsStub transformSettingsStub) {
        ListMapTransformSettings listMapTransformSettings = new ListMapTransformSettings();
        listMapTransformSettings.setEnabled(transformSettingsStub.isEnabled());
        listMapTransformSettings.setDisclaimerAccepted(transformSettingsStub.isDisclaimerAccepted());
        listMapTransformSettings.setShowHelp(transformSettingsStub.isShowHelp());
        listMapTransformSettings.setRunWithAll(transformSettingsStub.isRunWithAll());
        listMapTransformSettings.setFavorite(transformSettingsStub.isFavorite());
        for (TransformSettingsPropertyStub transformSettingsPropertyStub : transformSettingsStub.getProperties()) {
            Object object = Converter.convertFrom((String)transformSettingsPropertyStub.getValue(), (String)transformSettingsPropertyStub.getType());
            listMapTransformSettings.setValue(transformSettingsPropertyStub.getName(), object);
            listMapTransformSettings.setPopup(transformSettingsPropertyStub.getName(), PopupTranslator.get(transformSettingsPropertyStub.isPopup()));
            listMapTransformSettings.setVisibility(transformSettingsPropertyStub.getName(), VisibilityTranslator.get(transformSettingsPropertyStub.getVisibility()));
        }
        listMapTransformSettings.markClean();
        return listMapTransformSettings;
    }

    public static TransformSettingsStub translate(TransformSettings transformSettings, Iterable<DisplayDescriptor> iterable, boolean bl) {
        TransformSettingsStub transformSettingsStub = new TransformSettingsStub();
        transformSettingsStub.setEnabled(transformSettings.isEnabled());
        transformSettingsStub.setDisclaimerAccepted(transformSettings.isDisclaimerAccepted());
        transformSettingsStub.setShowHelp(transformSettings.showHelp());
        transformSettingsStub.setRunWithAll(transformSettings.isRunWithAll());
        transformSettingsStub.setFavorite(transformSettings.isFavorite());
        for (DisplayDescriptor displayDescriptor : iterable) {
            Object object;
            Object object2;
            boolean bl2 = TransformSettingsSerializer.shouldPersistValue(displayDescriptor, bl);
            TransformSettingsPropertyStub transformSettingsPropertyStub = new TransformSettingsPropertyStub(displayDescriptor.getName(), displayDescriptor.getTypeDescriptor().getTypeName());
            Object object3 = object2 = bl2 ? transformSettings.getValue((PropertyDescriptor)displayDescriptor) : null;
            if (object2 != null && !object2.equals(displayDescriptor.getDefaultValue())) {
                object = displayDescriptor.getTypeDescriptor().convert(object2);
                transformSettingsPropertyStub.setValue((String)object);
            }
            object = transformSettings.getPopup((PropertyDescriptor)displayDescriptor);
            boolean bl3 = bl2 ? PopupTranslator.get((Popup)object) : true;
            transformSettingsPropertyStub.setPopup(bl3);
            Visibility visibility = transformSettings.getVisibility((PropertyDescriptor)displayDescriptor);
            if (visibility != Visibility.Public) {
                String string = VisibilityTranslator.get(visibility);
                transformSettingsPropertyStub.setVisibility(string);
            }
            transformSettingsStub.getProperties().add(transformSettingsPropertyStub);
        }
        return transformSettingsStub;
    }

    static boolean shouldPersistValue(DisplayDescriptor displayDescriptor, boolean bl) {
        boolean bl2 = true;
        if (displayDescriptor instanceof TransformPropertyDescriptor) {
            TransformPropertyDescriptor transformPropertyDescriptor = (TransformPropertyDescriptor)displayDescriptor;
            PersistenceMode persistenceMode = transformPropertyDescriptor.getPersistence();
            switch (persistenceMode) {
                case None: {
                    bl2 = false;
                    break;
                }
                case NoExport: {
                    bl2 = !bl;
                    break;
                }
                default: {
                    bl2 = !bl || !transformPropertyDescriptor.isAuth();
                }
            }
        }
        return bl2;
    }

}

