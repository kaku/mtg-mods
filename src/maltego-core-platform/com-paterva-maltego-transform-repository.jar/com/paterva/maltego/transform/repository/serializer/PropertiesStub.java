/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.serializer.FieldGroupsStub
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.repository.serializer.TransformPropertyStub;
import com.paterva.maltego.typing.serializer.FieldGroupsStub;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Properties", strict=0)
public class PropertiesStub {
    @Element(name="Groups", required=0)
    private FieldGroupsStub _groups;
    @ElementList(name="Fields", type=TransformPropertyStub.class, required=0)
    private List<TransformPropertyStub> _fields;

    public PropertiesStub() {
    }

    public PropertiesStub(List<TransformPropertyStub> list, FieldGroupsStub fieldGroupsStub) {
        this._groups = fieldGroupsStub;
        this._fields = list;
    }

    public FieldGroupsStub getGroups() {
        return this._groups;
    }

    public void setGroups(FieldGroupsStub fieldGroupsStub) {
        this._groups = fieldGroupsStub;
    }

    public List<TransformPropertyStub> getFields() {
        return this._fields;
    }

    public void setFields(List<TransformPropertyStub> list) {
        this._fields = list;
    }
}

