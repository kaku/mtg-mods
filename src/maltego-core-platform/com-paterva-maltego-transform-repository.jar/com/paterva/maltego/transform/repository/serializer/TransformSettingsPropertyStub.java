/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.transform.repository.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="Property", strict=0)
public class TransformSettingsPropertyStub {
    @Attribute(name="name")
    private String _name;
    @Attribute(name="type")
    private String _type;
    @Attribute(name="popup", required=0)
    private boolean _popup;
    @Attribute(name="persistence", required=0)
    private String _persistence;
    @Attribute(name="visibility", required=0)
    private String _visibility;
    @Text(required=0)
    private String _value;

    public TransformSettingsPropertyStub() {
        this(null, null);
    }

    public TransformSettingsPropertyStub(String string, String string2) {
        this._name = string;
        this._type = string2;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public boolean isPopup() {
        return this._popup;
    }

    public void setPopup(boolean bl) {
        this._popup = bl;
    }

    public String getValue() {
        return this._value;
    }

    public void setValue(String string) {
        this._value = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public String getPersistence() {
        return this._persistence;
    }

    public void setPersistence(String string) {
        this._persistence = string;
    }

    public String getVisibility() {
        return this._visibility;
    }

    public void setVisibility(String string) {
        this._visibility = string;
    }
}

