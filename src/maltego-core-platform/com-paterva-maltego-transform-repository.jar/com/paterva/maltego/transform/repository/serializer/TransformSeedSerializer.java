/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.serializer.UnresolvedReferenceException
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.repository.serializer.PropertiesStub;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer;
import com.paterva.maltego.transform.repository.serializer.TransformSeedStub;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsPropertyStub;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsSerializer;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public abstract class TransformSeedSerializer {
    public static TransformSeedSerializer getDefault() {
        TransformSeedSerializer transformSeedSerializer = (TransformSeedSerializer)Lookup.getDefault().lookup(TransformSeedSerializer.class);
        if (transformSeedSerializer == null) {
            transformSeedSerializer = new DefaultTransformSeedSerializer();
        }
        return transformSeedSerializer;
    }

    public abstract void write(TransformSeed var1, OutputStream var2, boolean var3) throws XmlSerializationException;

    public abstract TransformSeed read(InputStream var1) throws XmlSerializationException;

    private static class DefaultTransformSeedSerializer
    extends TransformSeedSerializer {
        private DefaultTransformSeedSerializer() {
        }

        @Override
        public void write(TransformSeed transformSeed, OutputStream outputStream, boolean bl) throws XmlSerializationException {
            XmlSerializer xmlSerializer = new XmlSerializer();
            TransformSeedStub transformSeedStub = this.translate(transformSeed, bl);
            xmlSerializer.write((Object)transformSeedStub, outputStream);
        }

        @Override
        public TransformSeed read(InputStream inputStream) throws XmlSerializationException {
            XmlSerializer xmlSerializer = new XmlSerializer();
            TransformSeedStub transformSeedStub = (TransformSeedStub)xmlSerializer.read(TransformSeedStub.class, inputStream);
            return this.translate(transformSeedStub);
        }

        private TransformSeedStub translate(TransformSeed transformSeed, boolean bl) {
            TransformSeedStub transformSeedStub = new TransformSeedStub();
            try {
                transformSeedStub.setUrl(transformSeed.getUrl().getURL());
            }
            catch (MalformedURLException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
            transformSeedStub.setName(transformSeed.getName());
            transformSeedStub.setDescription(transformSeed.getDescription());
            transformSeedStub.setEnabled(transformSeed.isEnabled());
            DisplayDescriptorCollection displayDescriptorCollection = transformSeed.getGlobalTxProperties();
            try {
                transformSeedStub.setGlobalTxProperties(TransformDescriptorSerializer.translate(displayDescriptorCollection));
            }
            catch (UnresolvedReferenceException var5_6) {
                Exceptions.printStackTrace((Throwable)var5_6);
            }
            transformSeedStub.setGlobalTxSettings(this.translateGlobalTxSettings(transformSeed, bl));
            return transformSeedStub;
        }

        private ArrayList<TransformSettingsPropertyStub> translateGlobalTxSettings(TransformSeed transformSeed, boolean bl) {
            DataSource dataSource = transformSeed.getGlobalTxSettings();
            ArrayList<TransformSettingsPropertyStub> arrayList = new ArrayList<TransformSettingsPropertyStub>();
            DisplayDescriptorCollection displayDescriptorCollection = transformSeed.getGlobalTxProperties();
            if (displayDescriptorCollection != null) {
                for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                    boolean bl2 = TransformSettingsSerializer.shouldPersistValue(displayDescriptor, bl);
                    TransformSettingsPropertyStub transformSettingsPropertyStub = new TransformSettingsPropertyStub(displayDescriptor.getName(), displayDescriptor.getTypeDescriptor().getTypeName());
                    if (dataSource != null) {
                        Object object;
                        Object object2 = object = bl2 ? dataSource.getValue((PropertyDescriptor)displayDescriptor) : null;
                        if (object != null && !object.equals(displayDescriptor.getDefaultValue())) {
                            String string = displayDescriptor.getTypeDescriptor().convert(object);
                            transformSettingsPropertyStub.setValue(string);
                        }
                    }
                    arrayList.add(transformSettingsPropertyStub);
                }
            }
            return arrayList;
        }

        private TransformSeed translate(TransformSeedStub transformSeedStub) {
            FastURL fastURL = new FastURL(transformSeedStub.getUrl().toString());
            String string = transformSeedStub.getName();
            String string2 = transformSeedStub.getDescription();
            DisplayDescriptorCollection displayDescriptorCollection = null;
            try {
                displayDescriptorCollection = TransformDescriptorSerializer.translate(transformSeedStub.getGlobalTxProperties());
            }
            catch (UnresolvedReferenceException var6_6) {
                Exceptions.printStackTrace((Throwable)var6_6);
            }
            DataSource dataSource = this.translateGlobalTxSettings(displayDescriptorCollection, transformSeedStub.getGlobalTxSettings());
            TransformSeed transformSeed = new TransformSeed(fastURL, string, string2, displayDescriptorCollection, dataSource);
            transformSeed.setEnabled(transformSeedStub.isEnabled());
            return transformSeed;
        }

        private DataSource translateGlobalTxSettings(DisplayDescriptorCollection displayDescriptorCollection, List<TransformSettingsPropertyStub> list) {
            if (list == null) {
                return DataSources.empty();
            }
            DataSources.Map map = new DataSources.Map();
            for (TransformSettingsPropertyStub transformSettingsPropertyStub : list) {
                Object object = Converter.convertFrom((String)transformSettingsPropertyStub.getValue(), (String)transformSettingsPropertyStub.getType());
                DisplayDescriptor displayDescriptor = displayDescriptorCollection.get(transformSettingsPropertyStub.getName());
                if (displayDescriptor == null) continue;
                map.put((Object)displayDescriptor, object);
            }
            return map;
        }
    }

}

