/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.repository.serializer.TransformDescriptorStub;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsStub;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="TransformDefinition", strict=0)
class TransformDefinitionStub {
    @Element(name="Descriptor")
    private TransformDescriptorStub _descriptor;
    @Element(name="Settings", required=0)
    private TransformSettingsStub _settings;

    TransformDefinitionStub() {
    }

    public TransformDescriptorStub getDescriptor() {
        return this._descriptor;
    }

    public void setDescriptor(TransformDescriptorStub transformDescriptorStub) {
        this._descriptor = transformDescriptorStub;
    }

    public TransformSettingsStub getSettings() {
        return this._settings;
    }

    public void setSettings(TransformSettingsStub transformSettingsStub) {
        this._settings = transformSettingsStub;
    }
}

