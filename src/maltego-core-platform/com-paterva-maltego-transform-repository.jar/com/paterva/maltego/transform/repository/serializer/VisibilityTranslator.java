/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Visibility
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.descriptor.Visibility;

class VisibilityTranslator {
    VisibilityTranslator() {
    }

    public static Visibility get(String string) {
        if ("protected".equals(string)) {
            return Visibility.Protected;
        }
        if ("internal".equals(string)) {
            return Visibility.Internal;
        }
        if ("hidden".equals(string)) {
            return Visibility.Hidden;
        }
        return Visibility.Public;
    }

    public static String get(Visibility visibility) {
        switch (visibility) {
            case Public: {
                return "public";
            }
            case Protected: {
                return "protected";
            }
            case Internal: {
                return "internal";
            }
            case Hidden: {
                return "hidden";
            }
        }
        return "public";
    }

}

