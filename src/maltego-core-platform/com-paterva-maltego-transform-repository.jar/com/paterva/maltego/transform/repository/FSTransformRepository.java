/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.filesystems.FileAlreadyLockedException
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.repository.ListMapTransformSettings;
import com.paterva.maltego.transform.repository.TransformDefinitionCookie;
import com.paterva.maltego.transform.repository.TransformRepositoriesLock;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsSerializer;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.openide.filesystems.FileAlreadyLockedException;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class FSTransformRepository
extends TransformRepository {
    private String _name;
    private FileObject _folder;
    private Set<TransformDefinition> _transforms;
    private static final String TRANSFORM_EXT = "transform";
    private static final String SETTINGS_EXT = "transformsettings";

    public FSTransformRepository(String string, FileObject fileObject) {
        this._folder = fileObject;
        this._name = string;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Set<TransformDefinition> transforms() {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            if (this._transforms == null) {
                this._transforms = new HashSet<TransformDefinition>();
                for (FileObject fileObject : this._folder.getChildren()) {
                    TransformDefinition transformDefinition;
                    if (fileObject.isFolder() || (transformDefinition = this.get(fileObject)) == null) continue;
                    this._transforms.add(transformDefinition);
                }
            }
            return this._transforms;
        }
    }

    public Set<TransformDefinition> getAll() {
        return Collections.unmodifiableSet(this.transforms());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformDefinition get(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            for (TransformDefinition transformDefinition : this.transforms()) {
                if (!transformDefinition.getName().equals(string)) continue;
                return transformDefinition;
            }
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TransformDefinition get(FileObject fileObject) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            if (fileObject != null) {
                try {
                    TransformDefinitionCookie transformDefinitionCookie;
                    DataObject dataObject = DataObject.find((FileObject)fileObject);
                    if (dataObject != null && (transformDefinitionCookie = (TransformDefinitionCookie)dataObject.getLookup().lookup(TransformDefinitionCookie.class)) != null) {
                        TransformDefinition transformDefinition = transformDefinitionCookie.getTransformDefinition();
                        transformDefinition.setRepositoryName(this._name);
                        transformDefinition.markClean();
                        return transformDefinition;
                    }
                }
                catch (DataObjectNotFoundException var3_4) {
                    Exceptions.printStackTrace((Throwable)var3_4);
                }
            }
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<TransformDefinition> find(TransformFilter transformFilter) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
            this.find(transformFilter, hashSet);
            return hashSet;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void find(TransformFilter transformFilter, Set<TransformDefinition> set) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            for (TransformDefinition transformDefinition : this.transforms()) {
                if (!transformFilter.matches(transformDefinition)) continue;
                set.add(transformDefinition);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void put(TransformDescriptor transformDescriptor) {
        OutputStream outputStream = null;
        try {
            TransformDefinition transformDefinition;
            TransformDefinition transformDefinition2 = null;
            Object object = TransformRepositoriesLock.LOCK;
            synchronized (object) {
                Object object2;
                boolean bl;
                transformDefinition2 = this.get(transformDescriptor.getName());
                if (transformDescriptor.isCopy((TransformDescriptor)transformDefinition2)) {
                    transformDefinition2.markClean();
                    return;
                }
                FileObject fileObject = this._folder.getFileObject(transformDescriptor.getName(), "transform");
                try {
                    if (fileObject == null) {
                        fileObject = this._folder.createData(transformDescriptor.getName(), "transform");
                    }
                    outputStream = new BufferedOutputStream(fileObject.getOutputStream());
                    object2 = new TransformDescriptorSerializer();
                    object2.write(transformDescriptor, outputStream);
                }
                finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    }
                    catch (IOException var7_9) {
                        Exceptions.printStackTrace((Throwable)var7_9);
                    }
                }
                object2 = transformDefinition2 == null && transformDescriptor instanceof TransformDefinition ? (TransformDefinition)transformDescriptor : this.get(fileObject);
                if (object2 == null) {
                    object2 = new ListMapTransformSettings();
                }
                boolean bl2 = bl = transformDefinition2 == null;
                if (bl) {
                    Iterator iterator = transformDescriptor.getProperties().iterator();
                    while (iterator.hasNext()) {
                        DisplayDescriptor displayDescriptor;
                        object2.setPopup((PropertyDescriptor)displayDescriptor, (displayDescriptor = (DisplayDescriptor)iterator.next()).isPopup() ? Popup.Yes : Popup.No);
                    }
                }
                transformDefinition = new TransformDefinition(transformDescriptor, (TransformSettings)object2);
                if (transformDefinition2 != null) {
                    this.transforms().remove((Object)transformDefinition2);
                }
                transformDefinition.setRepositoryName(this._name);
                transformDefinition.markClean();
                this.transforms().add(transformDefinition);
                if (bl) {
                    this.saveSettings(transformDefinition);
                }
            }
            if (transformDefinition2 != null) {
                this.fireItemRemoved((Object)transformDefinition2);
            }
            this.fireItemAdded((Object)transformDefinition);
        }
        catch (FileAlreadyLockedException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        catch (IOException var3_5) {
            Exceptions.printStackTrace((Throwable)var3_5);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateSettings(TransformDefinition transformDefinition) {
        boolean bl = false;
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            if (transformDefinition.isDirty() && this.get(transformDefinition.getName()) != null) {
                bl = this.saveSettings(transformDefinition);
            }
        }
        if (bl) {
            transformDefinition.markClean();
            this.fireItemChanged((Object)transformDefinition);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean saveSettings(TransformDefinition transformDefinition) {
        boolean bl = false;
        OutputStream outputStream = null;
        try {
            FileObject fileObject = this._folder.getFileObject(transformDefinition.getName(), "transformsettings");
            if (fileObject == null) {
                fileObject = this._folder.createData(transformDefinition.getName(), "transformsettings");
            }
            outputStream = new BufferedOutputStream(fileObject.getOutputStream());
            TransformSettingsSerializer transformSettingsSerializer = new TransformSettingsSerializer();
            transformSettingsSerializer.write(transformDefinition, outputStream, false);
            bl = true;
        }
        catch (FileAlreadyLockedException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
        catch (IOException var4_8) {
            Exceptions.printStackTrace((Throwable)var4_8);
        }
        finally {
            try {
                outputStream.close();
            }
            catch (IOException var4_9) {
                Exceptions.printStackTrace((Throwable)var4_9);
            }
        }
        return bl;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean remove(String string) {
        TransformDefinition transformDefinition;
        boolean bl = false;
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            FileObject fileObject = this._folder.getFileObject(string, "transform");
            FileObject fileObject2 = this._folder.getFileObject(string, "transformsettings");
            if (fileObject != null) {
                try {
                    fileObject.delete();
                    bl = true;
                }
                catch (IOException var7_6) {
                    Exceptions.printStackTrace((Throwable)var7_6);
                }
            }
            if (fileObject2 != null) {
                try {
                    fileObject2.delete();
                }
                catch (IOException var7_7) {
                    Exceptions.printStackTrace((Throwable)var7_7);
                }
            }
            if ((transformDefinition = this.get(string)) != null) {
                this.transforms().remove((Object)transformDefinition);
            }
        }
        if (transformDefinition != null) {
            this.fireItemRemoved((Object)transformDefinition);
        }
        return bl;
    }
}

