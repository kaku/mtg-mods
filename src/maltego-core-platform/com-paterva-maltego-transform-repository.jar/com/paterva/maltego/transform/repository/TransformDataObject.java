/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.PolymorphicTransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.FileEntry
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.PolymorphicTransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.repository.ListMapTransformSettings;
import com.paterva.maltego.transform.repository.TransformDefinitionCookie;
import com.paterva.maltego.transform.repository.TransformDescriptorCookie;
import com.paterva.maltego.transform.repository.TransformSettingsCookie;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class TransformDataObject
extends MultiDataObject
implements TransformDefinitionCookie,
TransformDescriptorCookie,
TransformSettingsCookie {
    private TransformDefinition _definition;
    private FileEntry _settingsEntry;

    public TransformDataObject(FileObject fileObject, FileObject fileObject2, MultiFileLoader multiFileLoader) throws DataObjectExistsException, IOException {
        super(fileObject, multiFileLoader);
        if (!fileObject.equals((Object)fileObject2)) {
            this._settingsEntry = (FileEntry)this.registerEntry(fileObject2);
        }
    }

    FileEntry getSettingsEntry() {
        return this._settingsEntry;
    }

    void setSettingsFileEntry(FileEntry fileEntry) {
        this._settingsEntry = fileEntry;
    }

    protected Node createNodeDelegate() {
        return new DataNode((DataObject)this, Children.LEAF, this.getLookup());
    }

    public Lookup getLookup() {
        return this.getCookieSet().getLookup();
    }

    @Override
    public TransformDescriptor getTransformDescriptor() {
        return this.getTransformDefinition();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TransformDescriptor loadTransformDescriptor() {
        FileObject fileObject = this.getPrimaryFile();
        InputStream inputStream = null;
        try {
            inputStream = fileObject.getInputStream();
            TransformDescriptorSerializer transformDescriptorSerializer = new TransformDescriptorSerializer();
            TransformDescriptor transformDescriptor = transformDescriptorSerializer.read(inputStream);
            return transformDescriptor;
        }
        catch (XmlSerializationException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        catch (FileNotFoundException var3_6) {
            Exceptions.printStackTrace((Throwable)var3_6);
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException var3_7) {
                    Exceptions.printStackTrace((Throwable)var3_7);
                }
            }
        }
        return null;
    }

    @Override
    public TransformDefinition getTransformDefinition() {
        if (this._definition == null) {
            this._definition = new PolymorphicTransformDefinition(this.loadTransformDescriptor(), this.loadTransformSettings());
        }
        return this._definition;
    }

    @Override
    public TransformSettings getTransformSettings() {
        return this.getTransformDefinition();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TransformSettings loadTransformSettings() {
        FileObject fileObject = this._settingsEntry.getFile();
        if (fileObject != null && "transformsettings".equals(fileObject.getExt())) {
            InputStream inputStream = null;
            try {
                inputStream = fileObject.getInputStream();
                TransformSettingsSerializer transformSettingsSerializer = new TransformSettingsSerializer();
                TransformSettings transformSettings = transformSettingsSerializer.read(inputStream);
                return transformSettings;
            }
            catch (IOException var3_4) {
                Exceptions.attachMessage((Throwable)var3_4, (String)("Error while loading file " + fileObject.getPath()));
                Exceptions.printStackTrace((Throwable)var3_4);
            }
            finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    }
                    catch (IOException var3_5) {
                        Exceptions.printStackTrace((Throwable)var3_5);
                    }
                }
            }
        }
        return new ListMapTransformSettings();
    }
}

