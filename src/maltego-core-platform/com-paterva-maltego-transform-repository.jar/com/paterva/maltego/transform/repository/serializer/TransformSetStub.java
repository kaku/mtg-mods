/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="TransformSet", strict=0)
class TransformSetStub {
    @Attribute(name="name")
    private String _name;
    @Attribute(name="description", required=0)
    private String _description;
    @ElementList(name="Transforms", type=Transform.class, required=0)
    private ArrayList<Transform> _transforms;

    TransformSetStub() {
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public List<String> getTransforms() {
        ArrayList<String> arrayList = new ArrayList<String>(this._transforms.size());
        for (Transform transform : this._transforms) {
            arrayList.add(transform.getName());
        }
        return arrayList;
    }

    public void setTransforms(Collection<String> collection) {
        this._transforms = new ArrayList();
        for (String string : collection) {
            Transform transform = new Transform();
            transform.setName(string);
            this._transforms.add(transform);
        }
    }

    @Root(name="Transform", strict=0)
    public static class Transform {
        @Attribute(name="name")
        private String _name;

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }
    }

}

