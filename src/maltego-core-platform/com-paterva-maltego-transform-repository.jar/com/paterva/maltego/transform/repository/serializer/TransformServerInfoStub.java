/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.transform.repository.serializer;

import com.paterva.maltego.transform.repository.serializer.TransformServerSeedStub;
import com.paterva.maltego.util.StringUtilities;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="MaltegoServer", strict=0)
class TransformServerInfoStub {
    @Attribute(name="name", required=0)
    private String _name;
    @Attribute(name="enabled", required=0)
    private boolean _enabled = true;
    @Attribute(name="description", required=0)
    private String _description;
    @Attribute(name="url")
    private URL _url;
    @Element(name="LastSync", required=0)
    private Date _lastSync;
    @Element(name="Protocol", required=0)
    private Protocol _protocol;
    @Element(name="Authentication", required=0)
    private Authentication _authentication;
    @ElementList(name="Transforms", type=TransformReferenceStub.class, required=0)
    private List<TransformReferenceStub> _transforms = new ArrayList<TransformReferenceStub>();
    @ElementList(name="Seeds", type=TransformServerSeedStub.class, required=0)
    private List<TransformServerSeedStub> _seedUrls;

    TransformServerInfoStub() {
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public void setEnabled(boolean bl) {
        this._enabled = bl;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public URL getUrl() {
        return this._url;
    }

    public void setUrl(URL uRL) {
        this._url = uRL;
    }

    public Date getLastSync() {
        return this._lastSync;
    }

    public void setLastSync(Date date) {
        this._lastSync = date;
    }

    public String getProtocolVersion() {
        if (this._protocol == null) {
            return null;
        }
        return this._protocol.getVersion();
    }

    public void setProtocolVersion(String string) {
        this._protocol = string != null ? new Protocol(string) : null;
    }

    public List<String> getTransforms() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (TransformReferenceStub transformReferenceStub : this._transforms) {
            arrayList.add(transformReferenceStub.getName());
        }
        return arrayList;
    }

    public void addTransform(String string) {
        this._transforms.add(new TransformReferenceStub(string));
    }

    public Authentication getAuthentication() {
        return this._authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this._authentication = authentication;
    }

    public List<String> getSeedUrls() {
        ArrayList<String> arrayList = new ArrayList<String>();
        if (this._seedUrls != null) {
            for (TransformServerSeedStub transformServerSeedStub : this._seedUrls) {
                String string = StringUtilities.trim((String)transformServerSeedStub.getSeedUrl());
                if (StringUtilities.isNullOrEmpty((String)string)) continue;
                arrayList.add(string);
            }
        }
        return arrayList;
    }

    public void setSeedUrls(List<String> list) {
        this._seedUrls = new ArrayList<TransformServerSeedStub>(list.size());
        for (String string : list) {
            if (StringUtilities.isNullOrEmpty((String)(string = StringUtilities.trim((String)string)))) continue;
            this._seedUrls.add(new TransformServerSeedStub(string));
        }
    }

    @Root(name="Transform", strict=0)
    static class TransformReferenceStub {
        @Attribute(name="name")
        private String _name;

        public TransformReferenceStub() {
        }

        private TransformReferenceStub(String string) {
            this._name = string;
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }
    }

    static class Authentication {
        @Attribute(name="type")
        private String _type;

        public Authentication() {
        }

        public Authentication(String string) {
            this._type = string;
        }

        public String getType() {
            return this._type;
        }

        public void setType(String string) {
            this._type = string;
        }
    }

    static class Protocol {
        @Attribute(name="version")
        private String _version;

        public Protocol() {
        }

        private Protocol(String string) {
            this._version = string;
        }

        public String getVersion() {
            return this._version;
        }

        public void setVersion(String string) {
            this._version = string;
        }
    }

}

