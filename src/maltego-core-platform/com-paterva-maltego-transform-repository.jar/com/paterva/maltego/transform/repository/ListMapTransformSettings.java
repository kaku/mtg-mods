/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ListMap
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.StringUtilities;
import java.util.Map;

public class ListMapTransformSettings
implements TransformSettings {
    private boolean _enabled = true;
    private ListMap<String, TransformPropertySetting> _map;
    private boolean _accepted = false;
    private boolean _showHelp = true;
    private boolean _dirty = true;
    private boolean _runWithAll = true;
    private boolean _favorite = false;

    private Map<String, TransformPropertySetting> map() {
        if (this._map == null) {
            this._map = new ListMap();
        }
        return this._map;
    }

    public boolean isDirty() {
        return this._dirty;
    }

    public void markClean() {
        this._dirty = false;
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public void setEnabled(boolean bl) {
        if (this._enabled != bl) {
            this._enabled = bl;
            this._dirty = true;
        }
    }

    public boolean isDisclaimerAccepted() {
        return this._accepted;
    }

    public void setDisclaimerAccepted(boolean bl) {
        if (this._accepted != bl) {
            this._accepted = bl;
            this._dirty = true;
        }
    }

    public Popup getPopup(PropertyDescriptor propertyDescriptor) {
        return this.isPopup(propertyDescriptor.getName());
    }

    public Popup isPopup(String string) {
        TransformPropertySetting transformPropertySetting = this.map().get(string);
        if (transformPropertySetting == null) {
            return Popup.NotSet;
        }
        return transformPropertySetting.popup;
    }

    public void setPopup(PropertyDescriptor propertyDescriptor, Popup popup) {
        this.setPopup(propertyDescriptor.getName(), popup);
    }

    public void setPopup(String string, Popup popup) {
        TransformPropertySetting transformPropertySetting = this.getOrMakeSetting(string);
        if (transformPropertySetting.popup != popup) {
            transformPropertySetting.popup = popup;
            this._dirty = true;
        }
    }

    public Object getValue(String string) {
        TransformPropertySetting transformPropertySetting = this.map().get(string);
        if (transformPropertySetting == null) {
            return null;
        }
        return transformPropertySetting.value;
    }

    public void setValue(String string, Object object) {
        TransformPropertySetting transformPropertySetting = this.getOrMakeSetting(string);
        if (transformPropertySetting.value == null || !transformPropertySetting.value.equals(object)) {
            transformPropertySetting.value = StringUtilities.isNullString((Object)object) ? null : object;
            this._dirty = true;
        }
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this.getValue(propertyDescriptor.getName());
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this.setValue(propertyDescriptor.getName(), object);
    }

    public boolean showHelp() {
        return this._showHelp;
    }

    public void setShowHelp(boolean bl) {
        if (bl != this._showHelp) {
            this._showHelp = bl;
            this._dirty = true;
        }
    }

    public Visibility getVisibility(PropertyDescriptor propertyDescriptor) {
        TransformPropertySetting transformPropertySetting = this.map().get(propertyDescriptor.getName());
        if (transformPropertySetting != null && transformPropertySetting.visibility != null) {
            return transformPropertySetting.visibility;
        }
        return Visibility.Public;
    }

    public void setVisibility(PropertyDescriptor propertyDescriptor, Visibility visibility) {
        this.setVisibility(propertyDescriptor.getName(), visibility);
    }

    public void setVisibility(String string, Visibility visibility) {
        TransformPropertySetting transformPropertySetting = this.getOrMakeSetting(string);
        if (transformPropertySetting.visibility != visibility) {
            transformPropertySetting.visibility = visibility;
            this._dirty = true;
        }
    }

    private TransformPropertySetting getOrMakeSetting(String string) {
        TransformPropertySetting transformPropertySetting = this.map().get(string);
        if (transformPropertySetting == null) {
            transformPropertySetting = new TransformPropertySetting();
            this.map().put(string, transformPropertySetting);
            this._dirty = true;
        }
        return transformPropertySetting;
    }

    public boolean isRunWithAll() {
        return this._runWithAll;
    }

    public void setRunWithAll(boolean bl) {
        if (this._runWithAll != bl) {
            this._runWithAll = bl;
            this._dirty = true;
        }
    }

    public boolean isFavorite() {
        return this._favorite;
    }

    public void setFavorite(boolean bl) {
        if (this._favorite != bl) {
            this._favorite = bl;
            this._dirty = true;
        }
    }

    private class TransformPropertySetting {
        public Popup popup;
        public Object value;
        public Visibility visibility;

        private TransformPropertySetting() {
            this.popup = Popup.NotSet;
            this.value = null;
            this.visibility = Visibility.Public;
        }
    }

}

