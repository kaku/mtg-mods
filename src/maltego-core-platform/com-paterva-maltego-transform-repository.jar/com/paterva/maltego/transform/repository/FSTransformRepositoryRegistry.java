/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.util.ListMap
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.transform.repository;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.repository.FSTransformRepository;
import com.paterva.maltego.transform.repository.TransformRepositoriesLock;
import com.paterva.maltego.util.ListMap;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class FSTransformRepositoryRegistry
extends TransformRepositoryRegistry {
    private Map<String, FSTransformRepository> _repositories;
    private final FileObject _configRoot;

    public FSTransformRepositoryRegistry() {
        this(FileUtil.getConfigRoot());
    }

    public FSTransformRepositoryRegistry(FileObject fileObject) {
        this._configRoot = fileObject;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TransformDefinition findTransform(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            for (FSTransformRepository fSTransformRepository : this.repositories().values()) {
                TransformDefinition transformDefinition = fSTransformRepository.get(string);
                if (transformDefinition == null) continue;
                return transformDefinition;
            }
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<TransformDefinition> find(TransformFilter transformFilter) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
            for (FSTransformRepository fSTransformRepository : this.repositories().values()) {
                fSTransformRepository.find(transformFilter, hashSet);
            }
            return hashSet;
        }
    }

    private Map<String, FSTransformRepository> repositories() {
        if (this._repositories == null) {
            try {
                this._repositories = new ListMap();
                for (FileObject fileObject : this.getRoot().getChildren()) {
                    if (!fileObject.isFolder()) continue;
                    this._repositories.put(fileObject.getName(), new FSTransformRepository(fileObject.getName(), fileObject));
                }
            }
            catch (IOException var1_2) {
                // empty catch block
            }
        }
        return this._repositories;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public FSTransformRepository getRepository(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            FileObject fileObject;
            FSTransformRepository fSTransformRepository = this.repositories().get(string);
            if (fSTransformRepository == null && (fileObject = this.findRepository(string)) != null) {
                fSTransformRepository = new FSTransformRepository(string, fileObject);
                this.repositories().put(string, fSTransformRepository);
            }
            return fSTransformRepository;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public FSTransformRepository getOrCreateRepository(String string) throws IOException {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            FSTransformRepository fSTransformRepository = this.getRepository(string);
            if (fSTransformRepository == null) {
                fSTransformRepository = this.createRepository(string);
                this.repositories().put(string, fSTransformRepository);
            }
            return fSTransformRepository;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FSTransformRepository createRepository(String string) throws IOException {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            FileObject fileObject = this.getRoot().createFolder(string);
            return new FSTransformRepository(string, fileObject);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FileObject findRepository(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            try {
                return this.getRoot().getFileObject(string);
            }
            catch (IOException var3_3) {
                return null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FileObject getRoot() throws IOException {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            FileObject fileObject;
            FileObject fileObject2 = this._configRoot.getFileObject("Maltego");
            if (fileObject2 == null) {
                fileObject2 = this._configRoot.createFolder("Maltego");
            }
            if ((fileObject = fileObject2.getFileObject("TransformRepositories")) == null) {
                fileObject = fileObject2.createFolder("TransformRepositories");
            }
            return fileObject;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean removeTransform(String string) {
        Object object = TransformRepositoriesLock.LOCK;
        synchronized (object) {
            boolean bl = false;
            for (FSTransformRepository fSTransformRepository : this.repositories().values()) {
                if (!fSTransformRepository.remove(string)) continue;
                bl = true;
            }
            return bl;
        }
    }
}

