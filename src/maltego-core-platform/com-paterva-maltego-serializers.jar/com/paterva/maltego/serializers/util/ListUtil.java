/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.serializers.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ListUtil {
    public static <T> List<T> get(List<T> list) {
        return list != null ? Collections.unmodifiableList(list) : Collections.EMPTY_LIST;
    }

    public static <T> List<T> add(List<T> list, T t) {
        if (list == null) {
            list = new ArrayList<T>();
        }
        list.add(t);
        return list;
    }

    public static <T> List<T> addAll(List<T> list, Collection<T> collection) {
        if (list == null) {
            list = new ArrayList<T>();
        }
        list.addAll(collection);
        return list;
    }

    public static <T> List<T> remove(List<T> list, T t) {
        if (list != null) {
            list.remove(t);
            if (list.isEmpty()) {
                list = null;
            }
        }
        return list;
    }

    public static <T> List<T> removeAll(List<T> list, Collection<T> collection) {
        if (list != null) {
            list.removeAll(collection);
            if (list.isEmpty()) {
                list = null;
            }
        }
        return list;
    }
}

