/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.serializers.map;

import com.paterva.maltego.serializers.compact.LinkNameMappings;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PartMapTranslator {
    private final LinkNameMappings _linkMappings = new LinkNameMappings();
    protected static final String DEGREE_CHAR = "\u00b0";

    public LinkNameMappings getLinkMappings() {
        return this._linkMappings;
    }

    public Map<String, Object> escapeDots(Map<String, Object> map) {
        return this.replaceInKeys(map, "[.]", "\u00b0");
    }

    public String escapeDots(String string) {
        return string.replaceAll("[.]", "\u00b0");
    }

    public Map<String, Object> unescapeDots(Map<String, Object> map) {
        return this.replaceInKeys(map, "\u00b0", ".");
    }

    public String unescapeDots(String string) {
        return string.replaceAll("\u00b0", ".");
    }

    public Map<String, Object> replaceInKeys(Map<String, Object> map, String string, String string2) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String string3 = entry.getKey().replaceAll(string, string2);
            Map<String, Object> map2 = entry.getValue();
            if (map2 instanceof Map) {
                Map map3 = map2;
                map2 = this.replaceInKeys(map3, string, string2);
            }
            hashMap.put(string3, map2);
        }
        return hashMap;
    }
}

