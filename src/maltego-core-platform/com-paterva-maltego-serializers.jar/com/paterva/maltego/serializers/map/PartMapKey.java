/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.serializers.map;

public class PartMapKey {
    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String VALUE_STR = "valueStr";
    public static final String DISPLAY_STR = "displayValueStr";
    public static final String IMG_VALUE = "imageValue";
    public static final String HAS_ATTACHMENTS = "hasAttachments";
    public static final String LABEL_READONLY = "labelReadOnly";
    public static final String BOOKMARK = "bookmark";
    public static final String NOTES = "notes";
    public static final String NOTES_VALUE = "value";
    public static final String NOTES_SHOW = "show";
    public static final String WEIGHT = "weight";
    public static final String REVERSED = "reversed";
    public static final String PROPERTIES_TEXT = "propText";
    public static final String PROPERTIES = "properties";
    public static final String PROPERTY_DISPLAY_NAME = "displayName";
    public static final String PROPERTY_TYPE = "type";
    public static final String PROPERTY_VALUE = "value";
    public static final String PROPERTY_HIDDEN = "hidden";
    public static final String PROPERTY_READONLY = "readonly";
    public static final String PROPERTY_NULLABLE = "nullable";
    public static final String VALUE_PROP_NAME = "valuePropName";
    public static final String DISPLAY_PROP_NAME = "displayPropName";
    public static final String IMG_PROP_NAME = "imagePropName";
    public static final String DISPLAY_INFO = "displayInfo";
    public static final String DISPLAY_INFO_NAME = "name";
    public static final String DISPLAY_INFO_VALUE = "value";
    public static final String ATTACHMENTS_PRIMARY = "primary";
    public static final String ATTACHMENTS_MAP = "atts";

    private PartMapKey() {
    }
}

