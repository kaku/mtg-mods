/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.InternalFile
 *  com.paterva.maltego.typing.types.TimeSpan
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.serializers.map;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.compact.LinkNameMappings;
import com.paterva.maltego.serializers.map.PartMapTranslator;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PartMapReader
extends PartMapTranslator {
    private static final Logger LOG = Logger.getLogger(PartMapReader.class.getName());

    public Set<MaltegoEntity> fromMaps(EntityRegistry entityRegistry, EntityFactory entityFactory, Collection<Map<String, Object>> collection) throws GraphSerializationException {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        for (Map<String, Object> map : collection) {
            hashSet.add(this.entityFromMap(entityRegistry, entityFactory, map));
        }
        return hashSet;
    }

    public Set<MaltegoLink> fromMaps(LinkRegistry linkRegistry, LinkFactory linkFactory, Collection<Map<String, Object>> collection) throws GraphSerializationException {
        HashSet<MaltegoLink> hashSet = new HashSet<MaltegoLink>();
        for (Map<String, Object> map : collection) {
            hashSet.add(this.linkFromMap(linkRegistry, linkFactory, map));
        }
        return hashSet;
    }

    public MaltegoEntity entityFromMap(EntityRegistry entityRegistry, EntityFactory entityFactory, Map<String, Object> map) throws GraphSerializationException {
        MaltegoEntity maltegoEntity = (MaltegoEntity)this.fromMap((SpecRegistry)entityRegistry, (SpecFactory)entityFactory, map);
        LOG.log(Level.FINE, "Map:\n{0}\nEntity:\n{1}", new Object[]{map, maltegoEntity});
        return maltegoEntity;
    }

    public MaltegoLink linkFromMap(LinkRegistry linkRegistry, LinkFactory linkFactory, Map<String, Object> map) throws GraphSerializationException {
        MaltegoLink maltegoLink = (MaltegoLink)this.fromMap((SpecRegistry)linkRegistry, (SpecFactory)linkFactory, map);
        LOG.log(Level.FINE, "Map:\n{0}\nLink:\n{1}", new Object[]{map, maltegoLink});
        return maltegoLink;
    }

    public MaltegoPart fromMap(SpecRegistry specRegistry, SpecFactory specFactory, Map<String, Object> map) throws GraphSerializationException {
        boolean bl = specRegistry instanceof EntityRegistry;
        String string = (String)map.get("type");
        if (!bl) {
            string = this.getLinkMappings().linkTypeNameToLongName(string);
        }
        MaltegoEntity maltegoEntity = bl ? this.createEntity(map, (EntityFactory)specFactory, string) : this.createLink(map, (LinkFactory)specFactory, string);
        maltegoEntity.beginPropertyUpdating();
        this.setProperties((MaltegoPart)maltegoEntity, map, specRegistry, string);
        this.setBookmark((MaltegoPart)maltegoEntity, map);
        this.setNotes((MaltegoPart)maltegoEntity, map);
        this.setDisplayInfos((MaltegoPart)maltegoEntity, map);
        if (bl) {
            MaltegoEntity maltegoEntity2 = maltegoEntity;
            this.setWeight(maltegoEntity2, map);
            this.setPropertyMappings(maltegoEntity2, map);
        } else {
            MaltegoLink maltegoLink = (MaltegoLink)maltegoEntity;
            this.setReversed(maltegoLink, map);
        }
        this.setCachedValues((MaltegoPart)maltegoEntity, map);
        maltegoEntity.endPropertyUpdating(false);
        return maltegoEntity;
    }

    private MaltegoEntity createEntity(Map<String, Object> map, EntityFactory entityFactory, String string) throws GraphSerializationException {
        try {
            EntityID entityID = EntityID.parse((String)((String)map.get("id")));
            MaltegoEntity maltegoEntity = entityFactory.createInstance(string, false, entityID, false);
            return maltegoEntity;
        }
        catch (TypeInstantiationException var4_5) {
            throw new GraphSerializationException((Throwable)var4_5, true);
        }
    }

    private MaltegoLink createLink(Map<String, Object> map, LinkFactory linkFactory, String string) {
        LinkID linkID = LinkID.parse((String)((String)map.get("id")));
        MaltegoLink maltegoLink = linkFactory.createInstance(string, linkID, false);
        return maltegoLink;
    }

    private void setProperties(MaltegoPart maltegoPart, Map<String, Object> map, SpecRegistry specRegistry, String string) throws GraphSerializationException {
        DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)specRegistry, (String)string);
        Map map2 = (HashMap<String, Object>)map.get("properties");
        if (map2 != null) {
            if (maltegoPart instanceof MaltegoLink) {
                map2 = this.shortToLongNames(map2, "maltego.link.manual-link".equals(maltegoPart.getTypeName()));
            }
            map2 = new HashMap<String, Object>(map2);
            this.setStaticProperties(maltegoPart, displayDescriptorCollection, map2);
            this.setDynamicProperties(maltegoPart, map2);
            if (maltegoPart instanceof MaltegoLink) {
                this.setLinkDefaultValues(maltegoPart);
            }
        }
    }

    private void setStaticProperties(MaltegoPart maltegoPart, DisplayDescriptorCollection displayDescriptorCollection, Map<String, Object> map) throws GraphSerializationException {
        Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String string = entry.getKey();
            DisplayDescriptor displayDescriptor = displayDescriptorCollection.get(string);
            if (displayDescriptor == null) continue;
            Object object = entry.getValue();
            if (object instanceof Map) {
                Map map2 = (Map)object;
                Object object2 = this.translatePropertyValue(displayDescriptor.getTypeDescriptor(), map2.get("value"));
                maltegoPart.addProperty((PropertyDescriptor)displayDescriptor);
                maltegoPart.setValue((PropertyDescriptor)displayDescriptor, object2, false, false);
            }
            iterator.remove();
        }
    }

    private void setDynamicProperties(MaltegoPart maltegoPart, Map<String, Object> map) throws GraphSerializationException {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String string = entry.getKey();
            Object object = entry.getValue();
            if (!(object instanceof Map)) continue;
            Map map2 = (Map)object;
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType((String)map2.get("type"));
            Object object2 = map2.get("value");
            String string2 = (String)map2.get("displayName");
            Boolean bl = (Boolean)map2.get("hidden");
            Boolean bl2 = (Boolean)map2.get("readonly");
            Boolean bl3 = (Boolean)map2.get("nullable");
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), string, string2 != null ? string2 : string);
            propertyDescriptor.setHidden(bl != null ? bl : false);
            propertyDescriptor.setReadonly(bl2 != null ? bl2 : false);
            propertyDescriptor.setNullable(bl3 != null ? bl3 : true);
            object2 = this.translatePropertyValue(typeDescriptor, object2);
            maltegoPart.addProperty(propertyDescriptor);
            maltegoPart.setValue(propertyDescriptor, object2, false, false);
        }
    }

    private void setBookmark(MaltegoPart maltegoPart, Map<String, Object> map) {
        Integer n = (Integer)map.get("bookmark");
        if (n != null) {
            maltegoPart.setBookmark(n);
        }
    }

    private void setWeight(MaltegoEntity maltegoEntity, Map<String, Object> map) {
        Integer n = (Integer)map.get("weight");
        if (n != null) {
            maltegoEntity.setWeight(n);
        }
    }

    private void setReversed(MaltegoLink maltegoLink, Map<String, Object> map) {
        Boolean bl = (Boolean)map.get("reversed");
        if (bl != null) {
            maltegoLink.setReversed(bl);
        }
    }

    private void setNotes(MaltegoPart maltegoPart, Map<String, Object> map) {
        Map map2 = (Map)map.get("notes");
        if (map2 != null) {
            Boolean bl;
            String string = (String)map2.get("value");
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                maltegoPart.setNotes(string);
            }
            if ((bl = (Boolean)map2.get("show")) != null) {
                maltegoPart.setShowNotes(bl);
            }
        }
    }

    private void setDisplayInfos(MaltegoPart maltegoPart, Map<String, Object> map) {
        Object object = map.get("displayInfo");
        if (object != null) {
            if (object.getClass().isArray()) {
                this.setDisplayInfo(maltegoPart, (Map[])object);
            } else if (object instanceof Map) {
                Map map2 = (Map)object;
                if (map2.get("name") != null) {
                    this.setDisplayInfo(maltegoPart, new Map[]{map2});
                } else {
                    this.setDisplayInfo(maltegoPart, map2);
                }
            }
        }
    }

    private void setDisplayInfo(MaltegoPart maltegoPart, Map<String, String>[] arrmap) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Display Info Array: {0}", Arrays.toString(arrmap));
        }
        for (Map<String, String> map : arrmap) {
            String string = map.get("name");
            String string2 = map.get("value");
            maltegoPart.getOrCreateDisplayInformation().add(string, string2);
        }
    }

    private void setDisplayInfo(MaltegoPart maltegoPart, Map<String, Object> map) {
        LOG.log(Level.FINE, "Display Info Map: {0}", map);
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String[] arrstring;
            String string = entry.getKey();
            Object object = entry.getValue();
            if (object == null) continue;
            if (!object.getClass().isArray()) {
                maltegoPart.getOrCreateDisplayInformation().add(string, (String)object);
                continue;
            }
            Object[] arrobject = (Object[])object;
            for (String string2 : arrstring = Arrays.asList(arrobject).toArray(new String[arrobject.length])) {
                maltegoPart.getOrCreateDisplayInformation().add(string, string2);
            }
        }
    }

    private void setPropertyMappings(MaltegoEntity maltegoEntity, Map<String, Object> map) {
        String string;
        String string2;
        String string3 = (String)map.get("valuePropName");
        if (!StringUtilities.isNullOrEmpty((String)string3)) {
            maltegoEntity.setValueProperty(new PropertyDescriptor(String.class, string3));
        }
        if (!StringUtilities.isNullOrEmpty((String)(string = (String)map.get("displayPropName")))) {
            maltegoEntity.setDisplayValueProperty(new PropertyDescriptor(String.class, string));
        }
        if (!StringUtilities.isNullOrEmpty((String)(string2 = (String)map.get("imagePropName")))) {
            maltegoEntity.setImageProperty(new PropertyDescriptor(String.class, string2));
        }
    }

    private Map<String, Object> shortToLongNames(Map<String, Object> map, boolean bl) {
        LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<String, Object>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            linkedHashMap.put(this.getLinkMappings().linkPropNameToLongName(entry.getKey(), bl), entry.getValue());
        }
        return linkedHashMap;
    }

    private void setLinkDefaultValues(MaltegoPart maltegoPart) {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            Object object;
            String string = propertyDescriptor.getName();
            if (!"maltego.link.transform.version".equals(string) || (object = maltegoPart.getValue(propertyDescriptor)) != null) continue;
            maltegoPart.setValue(propertyDescriptor, (Object)"1.0.0");
        }
    }

    private void setCachedValues(MaltegoPart maltegoPart, Map<String, Object> map) {
        maltegoPart.setValueString((String)map.get("valueStr"));
        maltegoPart.setDisplayString((String)map.get("displayValueStr"));
        maltegoPart.setHasAttachments((Boolean)map.get("hasAttachments"));
        Boolean bl = (Boolean)map.get("labelReadOnly");
        maltegoPart.setLabelReadonly(bl != null ? bl : false);
        if (maltegoPart instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            maltegoEntity.setImageCachedObject(this.translateImageCacheObject(map.get("imageValue")));
        }
    }

    private Object translateImageCacheObject(Object object) {
        if (object != null && Map.class.isAssignableFrom(object.getClass())) {
            object = this.attachmentsFromMap((Map)object);
        }
        return object;
    }

    private Object translatePropertyValue(TypeDescriptor typeDescriptor, Object object) throws GraphSerializationException {
        if (object != null) {
            Class<String> class_ = typeDescriptor.getType();
            Class class_2 = class_;
            if (Byte.TYPE.equals((Object)class_)) {
                class_2 = String.class;
            } else if (Character.TYPE.equals(class_)) {
                class_2 = String.class;
            } else if (DateTime.class.equals(class_)) {
                class_2 = Long.TYPE;
            } else if (TimeSpan.class.equals(class_)) {
                class_2 = Long.class;
            } else if (FastURL.class.equals(class_)) {
                class_2 = String.class;
            } else if (InternalFile.class.equals(class_)) {
                class_2 = Integer.class;
            } else if (Color.class.equals(class_)) {
                class_2 = String.class;
            } else if (Attachments.class.equals(class_)) {
                class_2 = Map.class;
            } else if (File.class.equals(class_)) {
                class_2 = String.class;
            } else if (DateRange.class.equals(class_)) {
                class_2 = String.class;
            }
            if (TypeRegistry.getDefault().getType(class_2) != null) {
                object = Converter.convert((Object)object, class_2);
            }
            if (!class_2.isArray() && class_2.isPrimitive()) {
                class_2 = Converter.getReferenceType((Class)class_2);
            }
            this.checkType(class_2, object);
            if (Byte.TYPE.equals(class_)) {
                object = Byte.valueOf(Byte.parseByte((String)object));
            } else if (Character.TYPE.equals(class_)) {
                object = Character.valueOf(((String)object).charAt(0));
            } else if (DateTime.class.equals(class_)) {
                object = new DateTime(((Long)object).longValue());
            } else if (TimeSpan.class.equals(class_)) {
                object = new TimeSpan(((Long)object).longValue());
            } else if (FastURL.class.equals(class_)) {
                object = new FastURL((String)object);
            } else if (InternalFile.class.equals(class_)) {
                object = new InternalFile(((Integer)object).intValue());
            } else if (Color.class.equals(class_)) {
                object = ColorUtilities.decode((String)((String)object));
            } else if (Attachments.class.equals(class_)) {
                object = this.attachmentsFromMap((Map)object);
            } else if (File.class.equals(class_)) {
                object = new File((String)object);
            } else if (DateRange.class.equals(class_)) {
                object = DateRange.parse((String)((String)object));
            }
        }
        return object;
    }

    private void checkType(Class<? extends Object> class_, Object object) throws GraphSerializationException {
        Class class_2 = object.getClass();
        if (!class_.isAssignableFrom(class_2)) {
            throw new GraphSerializationException("Invalid type, expected " + class_ + " but found " + class_2, true);
        }
    }

    private Attachments attachmentsFromMap(Map map) {
        Attachments attachments = new Attachments();
        Object v = map.get("primary");
        Map map2 = (Map)map.get("atts");
        if (map2 != null) {
            for (Map.Entry entry : map2.entrySet()) {
                int n = Integer.parseInt((String)entry.getKey());
                String string = (String)entry.getValue();
                Attachment attachment = new Attachment(n, new FastURL(string));
                attachments.add((Object)attachment);
                if (v == null || (Integer)v != n) continue;
                attachments.setPrimaryImage(attachment);
            }
        }
        return attachments;
    }
}

