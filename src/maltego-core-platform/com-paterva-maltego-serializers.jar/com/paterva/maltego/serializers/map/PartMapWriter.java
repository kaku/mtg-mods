/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.InternalFile
 *  com.paterva.maltego.typing.types.TimeSpan
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.serializers.map;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.serializers.compact.LinkNameMappings;
import com.paterva.maltego.serializers.map.PartMapTranslator;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PartMapWriter
extends PartMapTranslator {
    private static final Logger LOG = Logger.getLogger(PartMapWriter.class.getName());

    public <ID extends Guid, Part extends MaltegoPart<ID>> List<Map<String, Object>> toMaps(SpecRegistry specRegistry, Collection<Part> collection, boolean bl) {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<Map<String, Object>>(collection.size());
        for (MaltegoPart maltegoPart : collection) {
            arrayList.add(this.toMap(specRegistry, maltegoPart, bl));
        }
        return arrayList;
    }

    public Map<String, Object> toMap(SpecRegistry specRegistry, MaltegoPart maltegoPart, boolean bl) {
        MaltegoEntity maltegoEntity;
        LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<String, Object>(13);
        linkedHashMap.put("id", maltegoPart.getID().toString());
        linkedHashMap.put("type", this.getTypeName(maltegoPart));
        linkedHashMap.put("valueStr", maltegoPart.getValueString());
        linkedHashMap.put("displayValueStr", maltegoPart.getDisplayString());
        if (maltegoPart instanceof MaltegoEntity) {
            maltegoEntity = (MaltegoEntity)maltegoPart;
            this.putIfNotNullOrEmpty(linkedHashMap, "imageValue", this.translateValue(maltegoEntity.getImageCachedObject()), bl);
        }
        linkedHashMap.put("hasAttachments", maltegoPart.hasAttachments());
        linkedHashMap.put("labelReadOnly", maltegoPart.isLabelReadonly());
        if (bl || maltegoPart.getBookmark() >= 0) {
            linkedHashMap.put("bookmark", maltegoPart.getBookmarkValue());
        }
        if (bl || !StringUtilities.isNullOrEmpty((String)maltegoPart.getNotes()) || maltegoPart.isShowNotes()) {
            linkedHashMap.put("notes", this.notesToMap(maltegoPart));
        }
        if (maltegoPart instanceof MaltegoEntity) {
            maltegoEntity = (MaltegoEntity)maltegoPart;
            if (bl || maltegoEntity.getWeight() > 0) {
                linkedHashMap.put("weight", maltegoEntity.getWeightValue());
            }
        }
        if (maltegoPart instanceof MaltegoLink) {
            maltegoEntity = (MaltegoLink)maltegoPart;
            if (bl || Boolean.TRUE.equals(maltegoEntity.isReversed())) {
                linkedHashMap.put("reversed", maltegoEntity.isReversed());
            }
        }
        if (maltegoPart instanceof MaltegoEntity) {
            maltegoEntity = (MaltegoEntity)maltegoPart;
            this.putIfNotNullOrEmpty(linkedHashMap, "valuePropName", maltegoPart.getValuePropertyName(), bl);
            this.putIfNotNullOrEmpty(linkedHashMap, "displayPropName", maltegoPart.getDisplayValuePropertyName(), bl);
            this.putIfNotNullOrEmpty(linkedHashMap, "imagePropName", maltegoEntity.getImagePropertyName(), bl);
        }
        this.putIfNotNullOrEmpty(linkedHashMap, "properties", this.propertiesToMap(maltegoPart, bl), bl);
        linkedHashMap.put("propText", this.propertiesToText(maltegoPart, bl));
        this.putIfNotNullOrEmpty(linkedHashMap, "displayInfo", this.displayInfoToArray(maltegoPart), bl);
        LOG.log(Level.FINE, "Part:\n{0}\nMap:\n{1}", new Object[]{maltegoPart, linkedHashMap});
        return linkedHashMap;
    }

    private String getTypeName(MaltegoPart maltegoPart) {
        if (maltegoPart instanceof MaltegoEntity) {
            return maltegoPart.getTypeName();
        }
        return this.getLinkMappings().linkTypeNameToShortName(maltegoPart.getTypeName());
    }

    private void putIfNotNullOrEmpty(Map<String, Object> map, String string, Object object, boolean bl) {
        if (bl || object != null && !StringUtilities.isNullString((Object)object)) {
            map.put(string, object);
        }
    }

    private Map<String, Object> notesToMap(MaltegoPart maltegoPart) {
        LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<String, Object>(2);
        linkedHashMap.put("value", maltegoPart.getNotes());
        linkedHashMap.put("show", maltegoPart.isShowNotesValue());
        return linkedHashMap;
    }

    private Map[] displayInfoToArray(MaltegoPart maltegoPart) {
        DisplayInformationCollection displayInformationCollection = maltegoPart.getDisplayInformation();
        ArrayList arrayList = null;
        if (displayInformationCollection != null && !displayInformationCollection.isEmpty()) {
            arrayList = new ArrayList();
            for (DisplayInformation displayInformation : displayInformationCollection) {
                String string = displayInformation.getName();
                String string2 = displayInformation.getValue();
                if (StringUtilities.isNullOrEmpty((String)string) || StringUtilities.isNullOrEmpty((String)string2)) continue;
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("name", string);
                hashMap.put("value", string2);
                arrayList.add(hashMap);
            }
        }
        LOG.log(Level.FINE, "Display Info: {0}", arrayList);
        return arrayList == null ? null : arrayList.toArray(new Map[arrayList.size()]);
    }

    private Map<String, Object> displayInfoToMap(MaltegoPart maltegoPart) {
        DisplayInformationCollection displayInformationCollection = maltegoPart.getDisplayInformation();
        LinkedHashMap<String, ArrayList<String>> linkedHashMap = null;
        if (displayInformationCollection != null && !displayInformationCollection.isEmpty()) {
            String string;
            Object object;
            LinkedHashMap<String, ArrayList<String>> linkedHashMap2 = new LinkedHashMap<String, ArrayList<String>>();
            for (DisplayInformation object22 : displayInformationCollection) {
                string = object22.getName();
                object = (ArrayList<String>)linkedHashMap2.get(string);
                if (object == null) {
                    object = new ArrayList<String>(1);
                    linkedHashMap2.put(string, (ArrayList<String>)object);
                }
                object.add(object22.getValue());
            }
            linkedHashMap = new LinkedHashMap<String, ArrayList<String>>();
            for (Map.Entry entry : linkedHashMap2.entrySet()) {
                string = (String)entry.getKey();
                object = entry.getValue();
                if (object instanceof List) {
                    object = ((List)object).toArray();
                }
                linkedHashMap.put(string, (ArrayList<String>)object);
            }
        }
        return linkedHashMap;
    }

    private Map<String, Object> propertiesToMap(MaltegoPart maltegoPart, boolean bl) {
        LinkedHashMap<String, Map<String, Object>> linkedHashMap = null;
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        if (propertyDescriptorCollection != null && !propertyDescriptorCollection.isEmpty()) {
            linkedHashMap = new LinkedHashMap<String, Map<String, Object>>(propertyDescriptorCollection.size());
            for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                if (!bl && this.isLinkPropertyDefaultValue(maltegoPart, propertyDescriptor)) continue;
                linkedHashMap.put(this.getPropertyName(maltegoPart, propertyDescriptor), this.propertyToMap(maltegoPart, propertyDescriptor, bl));
            }
        }
        return linkedHashMap;
    }

    private String[] propertiesToText(MaltegoPart maltegoPart, boolean bl) {
        LinkedList<String> linkedList = new LinkedList<String>();
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        if (propertyDescriptorCollection != null && !propertyDescriptorCollection.isEmpty()) {
            for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                Object object;
                if (!bl && this.isLinkPropertyDefaultValue(maltegoPart, propertyDescriptor) || (object = maltegoPart.getValue(propertyDescriptor)) == null) continue;
                linkedList.add(object.toString());
            }
        }
        return linkedList.toArray(new String[linkedList.size()]);
    }

    private String getPropertyName(MaltegoPart maltegoPart, PropertyDescriptor propertyDescriptor) {
        if (maltegoPart instanceof MaltegoEntity) {
            return propertyDescriptor.getName();
        }
        return this.getLinkMappings().linkPropNameToShortName(propertyDescriptor.getName());
    }

    private Map<String, Object> propertyToMap(MaltegoPart maltegoPart, PropertyDescriptor propertyDescriptor, boolean bl) {
        LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<String, Object>();
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
        if (bl || !propertyDescriptor.getName().equals(propertyDescriptor.getDisplayName())) {
            linkedHashMap.put("displayName", propertyDescriptor.getDisplayName());
        }
        linkedHashMap.put("value", this.translateValue(typeDescriptor.getType(), maltegoPart.getValue(propertyDescriptor)));
        linkedHashMap.put("type", typeDescriptor.getTypeName());
        if (bl || propertyDescriptor.isHidden()) {
            linkedHashMap.put("hidden", propertyDescriptor.isHidden());
        }
        if (bl || propertyDescriptor.isReadonly()) {
            linkedHashMap.put("readonly", propertyDescriptor.isReadonly());
        }
        if (bl || !propertyDescriptor.isNullable()) {
            linkedHashMap.put("nullable", propertyDescriptor.isNullable());
        }
        return linkedHashMap;
    }

    private boolean isLinkPropertyDefaultValue(MaltegoPart maltegoPart, PropertyDescriptor propertyDescriptor) {
        if (maltegoPart instanceof MaltegoLink) {
            String string = propertyDescriptor.getName();
            Object object = maltegoPart.getValue(propertyDescriptor);
            if ("maltego.link.color".equals(string)) {
                return object == null;
            }
            if ("maltego.link.label".equals(string) || "maltego.link.manual.description".equals(string) || "maltego.link.manual.type".equals(string)) {
                return StringUtilities.isNullString((Object)object);
            }
            if ("maltego.link.show-label".equals(string)) {
                return object == null || Integer.valueOf(0).equals(object);
            }
            if ("maltego.link.style".equals(string) || "maltego.link.thickness".equals(string)) {
                return object == null || Integer.valueOf(-1).equals(object);
            }
            if ("maltego.link.transform.version".equals(string)) {
                return object == null || "1.0.0".equals(object);
            }
        }
        return false;
    }

    private Object translateValue(Object object) {
        if (object != null) {
            object = this.translateValue(object.getClass(), object);
        }
        return object;
    }

    public Object translateValue(Class class_, Object object) {
        if (object != null) {
            if (Byte.TYPE.equals((Object)class_)) {
                object = Byte.toString(((Byte)object).byteValue());
            } else if (Character.TYPE.equals((Object)class_)) {
                object = Character.toString(((Character)object).charValue());
            } else if (DateTime.class.equals((Object)class_)) {
                object = ((DateTime)object).getDate().getTime();
            } else if (TimeSpan.class.equals((Object)class_)) {
                object = ((TimeSpan)object).getMilliseconds();
            } else if (FastURL.class.equals((Object)class_)) {
                object = object.toString();
            } else if (InternalFile.class.equals((Object)class_)) {
                object = ((InternalFile)object).fileStoreIndex;
            } else if (Color.class.equals((Object)class_)) {
                object = ColorUtilities.encode((Color)((Color)object));
            } else if (Attachments.class.equals((Object)class_)) {
                object = this.toMap((Attachments)object);
            } else if (File.class.equals((Object)class_)) {
                object = object.toString();
            } else if (DateRange.class.equals((Object)class_)) {
                object = object.toString();
            }
        }
        return object;
    }

    private Map<String, Object> toMap(Attachments attachments) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        Attachment attachment = attachments.getPrimaryImage();
        if (attachment != null) {
            int n = attachment.getId();
            hashMap.put("primary", n);
        }
        HashMap<String, String> hashMap2 = new HashMap<String, String>();
        for (Attachment attachment2 : attachments) {
            hashMap2.put(Integer.toString(attachment2.getId()), attachment2.getSource().toString());
        }
        hashMap.put("atts", hashMap2);
        return hashMap;
    }

    public void convertAttachmentIDsToPaths(Map<String, Object> map, Map<Integer, String> map2) {
        Map<String, Object> map3;
        int n = 0;
        Map<String, Map<String, Object>> map32 = this.getAttachmentPropertyMaps(map);
        for (Map.Entry<String, Map<String, Object>> object2 : map32.entrySet()) {
            Map<String, Object> map4 = object2.getValue();
            n += this.convertAttachmentMapIDsToPaths(map4, map2);
        }
        map.put("hasAttachments", n != 0);
        Object object3 = map.get("imagePropName");
        if (object3 instanceof String && (map3 = map32.get((String)object3)) != null) {
            map.put("imageValue", map3);
        }
    }

    private int convertAttachmentMapIDsToPaths(Map<String, Object> map, Map<Integer, String> map2) {
        Object object;
        int n = 0;
        Object object2 = map.get("atts");
        if (object2 instanceof Map) {
            object = (Map)object2;
            HashMap<String, String> hashMap = new HashMap<String, String>();
            for (Map.Entry entry : object.entrySet()) {
                String string = (String)entry.getKey();
                String string2 = (String)entry.getValue();
                String string3 = map2.get(Integer.valueOf(string));
                if (string3 == null) {
                    LOG.log(Level.WARNING, "Path not found for attachment ID {0}, trying to match by source: {0}", new Object[]{string, string2});
                    string3 = this.findPathForSource(map2, string2);
                    if (string3 != null) {
                        LOG.log(Level.WARNING, "Path found: {0}", string3);
                    }
                }
                if (string3 != null) {
                    LOG.log(Level.FINE, "Replacing attachment id with path: {0}->{1}", new Object[]{string, string3});
                    hashMap.put(this.escapeDots(string3), string2);
                    continue;
                }
                LOG.log(Level.WARNING, "Path not found for attachment ID: {0}", string);
            }
            map.put("atts", hashMap);
            n = hashMap.size();
        }
        if ((object = map.get("primary")) instanceof Integer) {
            Integer n2 = (Integer)object;
            String string = map2.get(n2);
            if (string != null) {
                LOG.log(Level.FINE, "Replacing primary id with path: {0}->{1}", new Object[]{n2, string});
            } else {
                LOG.log(Level.WARNING, "Path not found for attachment ID: {0}", n2);
            }
            map.put("primary", string);
        }
        return n;
    }

    public void convertAttachmentPathsToIDs(Map<String, Object> map, Map<Integer, String> map2) {
        Map<String, Object> map3;
        int n = 0;
        Map<String, Map<String, Object>> map32 = this.getAttachmentPropertyMaps(map);
        for (Map.Entry<String, Map<String, Object>> object2 : map32.entrySet()) {
            Map<String, Object> map4 = object2.getValue();
            n += this.convertAttachmentMapPathsToIDs(map4, map2);
        }
        map.put("hasAttachments", n != 0);
        Object object3 = map.get("imagePropName");
        if (object3 instanceof String && (map3 = map32.get(this.escapeDots((String)object3))) != null) {
            map.put("imageValue", map3);
        }
    }

    public int convertAttachmentMapPathsToIDs(Map<String, Object> map, Map<Integer, String> map2) {
        Object object2;
        Object object;
        int n = 0;
        Object object3 = map.get("atts");
        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        if (object3 instanceof Map) {
            object2 = (Map)object3;
            object = new HashMap();
            for (Map.Entry entry : object2.entrySet()) {
                String string = this.unescapeDots((String)entry.getKey());
                String string2 = (String)entry.getValue();
                Integer n2 = this.getIdForPath(map2, string);
                if (n2 == null) {
                    LOG.log(Level.WARNING, "ID not found for attachment {0}, trying to match by source: {0}", new Object[]{string, string2});
                    n2 = this.findIDForSource(map2, string2);
                    if (n2 != null) {
                        LOG.log(Level.WARNING, "ID found: {0}", n2);
                    }
                }
                if (n2 != null) {
                    LOG.log(Level.FINE, "Replacing attachment path with id: {0}->{1}", new Object[]{string, n2});
                    hashMap.put(string, n2);
                    object.put(Integer.toString(n2), string2);
                    continue;
                }
                LOG.log(Level.WARNING, "ID not found for attachment: {0}", string);
            }
            map.put("atts", object);
            n = object.size();
        }
        if ((object2 = map.get("primary")) != null) {
            object = object2.toString();
            Object object4 = (Integer)hashMap.get(object);
            if (object4 == null) {
                object4 = this.getIdForPath(map2, (String)object);
            }
            if (object4 != null) {
                LOG.log(Level.FINE, "Replacing primary path with id: {0}->{1}", new Object[]{object, object4});
            } else {
                LOG.log(Level.WARNING, "ID not found for attachment: {0}", object);
            }
            map.put("primary", object4);
        }
        return n;
    }

    private Map<String, Map<String, Object>> getAttachmentPropertyMaps(Map<String, Object> map) {
        HashMap<String, Map<String, Object>> hashMap = new HashMap<String, Map<String, Object>>();
        Object object = map.get("properties");
        if (object instanceof Map) {
            Map map2 = (Map)object;
            for (Map.Entry entry : map2.entrySet()) {
                Map<String, Object> map3;
                Map map4;
                Object v = entry.getValue();
                if (!(v instanceof Map) || (map3 = this.getAttachmentsValueMap(map4 = (Map)v)) == null) continue;
                hashMap.put((String)entry.getKey(), map3);
            }
        }
        return hashMap;
    }

    private Map<String, Object> getAttachmentsValueMap(Map<String, Object> map) {
        Object object;
        if ("attachments".equals(map.get("type")) && (object = map.get("value")) instanceof Map) {
            Map map2 = (Map)object;
            return map2;
        }
        return null;
    }

    private Integer getIdForPath(Map<Integer, String> map, String string) {
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            Integer n = entry.getKey();
            String string2 = entry.getValue();
            if (!string.equals(string2)) continue;
            return n;
        }
        return null;
    }

    private String findPathForSource(Map<Integer, String> map, String string) {
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            String string2 = entry.getValue();
            string2 = new File(string2).getName();
            if (!string.toLowerCase().contains(string2.toLowerCase())) continue;
            return string2;
        }
        return null;
    }

    private Integer findIDForSource(Map<Integer, String> map, String string) {
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            Integer n = entry.getKey();
            String string2 = entry.getValue();
            string2 = new File(string2).getName();
            if (!string.toLowerCase().contains(string2.toLowerCase())) continue;
            return n;
        }
        return null;
    }
}

