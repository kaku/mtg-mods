/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.serializers;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;

public interface GraphConnectionSource {
    public EntityID getSource(LinkID var1);

    public EntityID getTarget(LinkID var1);
}

