/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.DisplayInfoStub;
import com.paterva.maltego.serializers.compact.stubs.DynamicPropertyStub;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import com.paterva.maltego.serializers.compact.stubs.PropertyStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.Collection;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

public abstract class PartStub
extends OrderedStub {
    @Attribute(name="id", required=0)
    @SerializedName(value="id")
    private String _id;
    @Attribute(name="type", required=0)
    @SerializedName(value="type")
    private String _type;
    @Attribute(name="val", required=0)
    @SerializedName(value="val")
    private String _valuePropertyName;
    @Attribute(name="disp", required=0)
    @SerializedName(value="disp")
    private String _displayValuePropertyName;
    @ElementList(inline=1, type=PropertyStub.class, required=0)
    @SerializedName(value="props")
    private List<PropertyStub> _properties;
    @ElementList(inline=1, type=DynamicPropertyStub.class, required=0)
    @SerializedName(value="dprops")
    private List<DynamicPropertyStub> _dynamicProperties;
    @ElementList(inline=1, type=DisplayInfoStub.class, required=0)
    @SerializedName(value="di")
    private List<DisplayInfoStub> _displayInfo;

    public void add(PropertyStub propertyStub) {
        this._properties = ListUtil.add(this._properties, propertyStub);
    }

    public void removeProperties(Collection<PropertyStub> collection) {
        this._properties = ListUtil.removeAll(this._properties, collection);
    }

    public List<PropertyStub> getProperties() {
        return ListUtil.get(this._properties);
    }

    public void add(DisplayInfoStub displayInfoStub) {
        this._displayInfo = ListUtil.add(this._displayInfo, displayInfoStub);
    }

    public List<DisplayInfoStub> getDisplayInfo() {
        return ListUtil.get(this._displayInfo);
    }

    public void add(DynamicPropertyStub dynamicPropertyStub) {
        this._dynamicProperties = ListUtil.add(this._dynamicProperties, dynamicPropertyStub);
    }

    public void removeDynamicProperties(Collection<DynamicPropertyStub> collection) {
        this._dynamicProperties = ListUtil.removeAll(this._dynamicProperties, collection);
    }

    public List<DynamicPropertyStub> getDynamicProperties() {
        return ListUtil.get(this._dynamicProperties);
    }

    public String getID() {
        return this._id;
    }

    public void setID(String string) {
        this._id = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public String getValuePropertyName() {
        return this._valuePropertyName;
    }

    public void setValuePropertyName(String string) {
        this._valuePropertyName = string;
    }

    public String getDisplayValuePropertyName() {
        return this._displayValuePropertyName;
    }

    public void setDisplayValuePropertyName(String string) {
        this._displayValuePropertyName = string;
    }
}

