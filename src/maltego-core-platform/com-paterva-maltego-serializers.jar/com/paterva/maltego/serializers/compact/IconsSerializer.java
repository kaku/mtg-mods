/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  org.apache.commons.io.output.ByteArrayOutputStream
 *  org.simpleframework.xml.core.Persister
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.compact.IconCollection;
import com.paterva.maltego.serializers.compact.stubs.IconCategoryStub;
import com.paterva.maltego.serializers.compact.stubs.IconImageStub;
import com.paterva.maltego.serializers.compact.stubs.IconStub;
import com.paterva.maltego.serializers.compact.stubs.IconsStub;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.simpleframework.xml.core.Persister;

public class IconsSerializer {
    public String toString(IconCollection iconCollection) throws GraphSerializationException {
        if (iconCollection == null || iconCollection.isEmpty()) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            try {
                this.write(iconCollection, (OutputStream)byteArrayOutputStream);
            }
            finally {
                byteArrayOutputStream.close();
            }
        }
        catch (IOException var3_4) {
            throw new GraphSerializationException(var3_4, true);
        }
        return byteArrayOutputStream.toString();
    }

    public void write(IconCollection iconCollection, OutputStream outputStream) throws GraphSerializationException {
        try {
            IconsStub iconsStub = this.translate(iconCollection);
            Persister persister = new Persister();
            persister.write((Object)iconsStub, outputStream);
        }
        catch (Exception var3_4) {
            throw new GraphSerializationException(var3_4, true);
        }
    }

    private IconsStub translate(IconCollection iconCollection) throws IOException {
        IconsStub iconsStub = new IconsStub();
        for (Map.Entry entry : iconCollection.entrySet()) {
            IconCategoryStub iconCategoryStub = new IconCategoryStub((String)entry.getKey());
            for (Map.Entry entry2 : ((Map)entry.getValue()).entrySet()) {
                IconStub iconStub = new IconStub((String)entry2.getKey());
                for (Map.Entry entry3 : ((Map)entry2.getValue()).entrySet()) {
                    IconSize iconSize = (IconSize)entry3.getKey();
                    Image image = (Image)entry3.getValue();
                    String string = ImageUtils.base64Encode((Image)image, (String)"png");
                    iconStub.add(new IconImageStub(iconSize.getSize(), string));
                }
                iconCategoryStub.add(iconStub);
            }
            iconsStub.add(iconCategoryStub);
        }
        return iconsStub;
    }

    public IconCollection read(String string) throws GraphSerializationException {
        Persister persister = new Persister();
        IconCollection iconCollection = new IconCollection();
        try {
            IconsStub iconsStub = (IconsStub)persister.read(IconsStub.class, string);
            for (IconCategoryStub iconCategoryStub : iconsStub.getCategories()) {
                HashMap hashMap = new HashMap();
                for (IconStub iconStub : iconCategoryStub.getIcons()) {
                    EnumMap<IconSize, BufferedImage> enumMap = new EnumMap<IconSize, BufferedImage>(IconSize.class);
                    for (IconImageStub iconImageStub : iconStub.getIconImages()) {
                        IconSize iconSize = IconSize.getSize((int)iconImageStub.getSize());
                        BufferedImage bufferedImage = ImageUtils.base64Decode((String)iconImageStub.getBase64Image());
                        enumMap.put(iconSize, bufferedImage);
                    }
                    hashMap.put(iconStub.getName(), enumMap);
                }
                iconCollection.put(iconCategoryStub.getName(), hashMap);
            }
        }
        catch (Exception var4_5) {
            throw new GraphSerializationException(var4_5, true);
        }
        return iconCollection;
    }
}

