/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="di", strict=0)
public class DisplayInfoStub {
    private static final String MIME_TEXT_HTML = "text/html";
    @Attribute(name="name", required=0)
    @SerializedName(value="name")
    private String _name;
    @Attribute(name="type", required=0)
    @SerializedName(value="type")
    private String _type;
    @Text(required=0)
    @SerializedName(value="value")
    private String _value;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public String getValue() {
        return this._value;
    }

    public void setValue(String string) {
        this._value = string;
    }

    public boolean isDefaultType() {
        return this.getDefaultType().equals(this._type);
    }

    public String getDefaultType() {
        return "text/html";
    }
}

