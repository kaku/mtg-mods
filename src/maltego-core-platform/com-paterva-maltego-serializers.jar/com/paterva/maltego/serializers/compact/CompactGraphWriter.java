/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  org.apache.commons.io.output.ByteArrayOutputStream
 *  org.openide.util.Exceptions
 *  org.simpleframework.xml.core.Persister
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.serializers.GraphConnectionSource;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.ViewPositionSource;
import com.paterva.maltego.serializers.compact.CompactGraphNameMappings;
import com.paterva.maltego.serializers.compact.SerializedGraph;
import com.paterva.maltego.serializers.compact.stubs.DisplayInfoStub;
import com.paterva.maltego.serializers.compact.stubs.DynamicPropertyStub;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import com.paterva.maltego.serializers.compact.stubs.PartStub;
import com.paterva.maltego.serializers.compact.stubs.PathStub;
import com.paterva.maltego.serializers.compact.stubs.PositionStub;
import com.paterva.maltego.serializers.compact.stubs.PropertyStub;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.awt.Point;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.openide.util.Exceptions;
import org.simpleframework.xml.core.Persister;

public class CompactGraphWriter {
    private CompactGraphNameMappings _mappings = null;

    public CompactGraphWriter(CompactGraphNameMappings compactGraphNameMappings) {
        this._mappings = compactGraphNameMappings;
    }

    public CompactGraphWriter() {
    }

    public void write(GraphID graphID, OutputStream outputStream, Collection<EntityID> collection, boolean bl) throws GraphSerializationException {
        if (graphID != null) {
            try {
                SerializedGraph serializedGraph = this.translate(graphID, new GraphSnippetStub(), collection, bl);
                Persister persister = new Persister();
                persister.write((Object)serializedGraph.getGraph(), outputStream, "UTF-8");
            }
            catch (Exception var5_6) {
                throw new GraphSerializationException(var5_6, true);
            }
        }
    }

    public String toString(GraphID graphID, boolean bl) throws GraphSerializationException {
        return this.toString(graphID, null, bl);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString(GraphID graphID, Collection<EntityID> collection, boolean bl) throws GraphSerializationException {
        String string;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.write(graphID, (OutputStream)byteArrayOutputStream, collection, bl);
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            catch (IOException var5_5) {
                throw new GraphSerializationException(var5_5, true);
            }
        }
        try {
            string = byteArrayOutputStream.toString("UTF-8");
        }
        catch (UnsupportedEncodingException var6_8) {
            throw new GraphSerializationException(var6_8, true);
        }
        return string;
    }

    public SerializedGraph translate(GraphID graphID, GraphSnippetStub graphSnippetStub, Collection<EntityID> collection, boolean bl) throws GraphSerializationException {
        SerializedGraph serializedGraph = new SerializedGraph();
        serializedGraph.setGraph(graphSnippetStub);
        this.copy(graphID, serializedGraph, collection, bl);
        return serializedGraph;
    }

    public void copy(GraphID graphID, SerializedGraph serializedGraph, boolean bl) throws GraphSerializationException {
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID);
        Set set2 = GraphStoreHelper.getMaltegoLinks((GraphID)graphID);
        this.copy(set, set2, null, new GraphConnectionWrapper(graphID), EntityRegistry.forGraphID((GraphID)graphID), LinkRegistry.forGraphID((GraphID)graphID), serializedGraph, bl);
    }

    public void copy(GraphID graphID, SerializedGraph serializedGraph, Collection<EntityID> collection, boolean bl) throws GraphSerializationException {
        Set set = collection == null ? GraphStoreHelper.getMaltegoEntities((GraphID)graphID) : GraphStoreHelper.getMaltegoEntities((GraphID)graphID, collection);
        Set set2 = GraphStoreHelper.getMaltegoLinks((GraphID)graphID);
        this.copy(set, set2, null, new GraphConnectionWrapper(graphID), EntityRegistry.forGraphID((GraphID)graphID), LinkRegistry.forGraphID((GraphID)graphID), serializedGraph, bl);
    }

    public void copy(Iterable<? extends MaltegoEntity> iterable, Iterable<? extends MaltegoLink> iterable2, ViewPositionSource viewPositionSource, GraphConnectionSource graphConnectionSource, EntityRegistry entityRegistry, LinkRegistry linkRegistry, SerializedGraph serializedGraph, boolean bl) throws GraphSerializationException {
        this.copyEntities(iterable, viewPositionSource, entityRegistry, serializedGraph, bl);
        this.copyLinks(iterable2, graphConnectionSource, viewPositionSource, linkRegistry, serializedGraph, bl);
    }

    protected void copyEntities(Iterable<? extends MaltegoEntity> iterable, ViewPositionSource viewPositionSource, EntityRegistry entityRegistry, SerializedGraph serializedGraph, boolean bl) throws GraphSerializationException {
        if (entityRegistry == null) {
            entityRegistry = EntityRegistry.getDefault();
        }
        for (MaltegoEntity maltegoEntity : iterable) {
            EntityStub entityStub = new EntityStub();
            entityStub.setID(((EntityID)maltegoEntity.getID()).toString());
            entityStub.setType(maltegoEntity.getTypeName());
            this.updateFixedFields(maltegoEntity, entityStub, bl);
            this.copyDisplayInformation((MaltegoPart)maltegoEntity, entityStub);
            if (viewPositionSource != null) {
                this.copyViews(viewPositionSource, maltegoEntity, entityStub);
            }
            this.copyPropertyMappings((MaltegoPart)maltegoEntity, entityStub);
            if (maltegoEntity.getTypeName() != null) {
                DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)entityRegistry, (String)maltegoEntity.getTypeName());
                this.copyStaticProperties(displayDescriptorCollection, (MaltegoPart)maltegoEntity, entityStub, bl);
                this.copyDynamicProperties(displayDescriptorCollection, (MaltegoPart)maltegoEntity, entityStub);
            } else if (!maltegoEntity.getProperties().isEmpty()) {
                throw new GraphSerializationException("Entity type must be specified if properties are to be serialized.", true);
            }
            serializedGraph.getGraph().add(entityStub);
            serializedGraph.add(maltegoEntity, entityStub);
        }
    }

    protected void copyLinks(Iterable<? extends MaltegoLink> iterable, GraphConnectionSource graphConnectionSource, ViewPositionSource viewPositionSource, LinkRegistry linkRegistry, SerializedGraph serializedGraph, boolean bl) throws GraphSerializationException {
        if (linkRegistry == null) {
            linkRegistry = LinkRegistry.getDefault();
        }
        for (MaltegoLink maltegoLink : iterable) {
            LinkStub linkStub = new LinkStub();
            linkStub.setID(((LinkID)maltegoLink.getID()).toString());
            linkStub.setType(maltegoLink.getTypeName());
            this.copyDisplayInformation((MaltegoPart)maltegoLink, linkStub);
            if (viewPositionSource != null) {
                this.copyPaths(viewPositionSource, maltegoLink, linkStub);
            }
            linkStub.setFrom(this.getID((Guid)graphConnectionSource.getSource((LinkID)maltegoLink.getID())));
            linkStub.setTo(this.getID((Guid)graphConnectionSource.getTarget((LinkID)maltegoLink.getID())));
            linkStub.setReversed(maltegoLink.isReversed());
            this.copyPropertyMappings((MaltegoPart)maltegoLink, linkStub);
            if (maltegoLink.getTypeName() != null) {
                DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)linkRegistry, (String)maltegoLink.getTypeName());
                this.copyStaticProperties(displayDescriptorCollection, (MaltegoPart)maltegoLink, linkStub, bl);
                this.copyDynamicProperties(displayDescriptorCollection, (MaltegoPart)maltegoLink, linkStub);
            } else if (!maltegoLink.getProperties().isEmpty()) {
                throw new GraphSerializationException("Link type must be specified if properties are to be serialized.", true);
            }
            this.convertLinkNames(linkStub);
            serializedGraph.getGraph().add(linkStub);
            serializedGraph.add(maltegoLink, linkStub);
        }
    }

    private void convertLinkNames(LinkStub linkStub) {
        if (this._mappings != null) {
            linkStub.setDisplayValuePropertyName(this._mappings.linkPropNameToShortName(linkStub.getDisplayValuePropertyName()));
            linkStub.setValuePropertyName(this._mappings.linkPropNameToShortName(linkStub.getValuePropertyName()));
            for (PropertyStub propertyStub2 : linkStub.getProperties()) {
                propertyStub2.setName(this._mappings.linkPropNameToShortName(propertyStub2.getName()));
            }
            for (PropertyStub propertyStub : linkStub.getDynamicProperties()) {
                propertyStub.setName(this._mappings.linkPropNameToShortName(propertyStub.getName()));
            }
            linkStub.setType(this._mappings.linkTypeNameToShortName(linkStub.getType()));
        }
    }

    private String getID(Guid guid) {
        if (guid == null) {
            return null;
        }
        return guid.toString();
    }

    private void copyPaths(ViewPositionSource viewPositionSource, MaltegoLink maltegoLink, LinkStub linkStub) {
        Collection<String> collection = viewPositionSource.getViews();
        for (String string : collection) {
            List<Point> list = viewPositionSource.getPath(string, (LinkID)maltegoLink.getID());
            if (list == null) continue;
            linkStub.add(new PathStub(string, this.pathToString(list)));
        }
    }

    private String pathToString(List<Point> list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Point point : list) {
            stringBuilder.append(this.getCenter(point));
            stringBuilder.append(";");
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    private void copyStaticProperties(DisplayDescriptorCollection displayDescriptorCollection, MaltegoPart maltegoPart, PartStub partStub, boolean bl) {
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            if (!maltegoPart.getProperties().contains((PropertyDescriptor)displayDescriptor)) continue;
            Object object = maltegoPart.getValue((PropertyDescriptor)displayDescriptor);
            if (bl && this.areEqual(object, displayDescriptor.getDefaultValue())) continue;
            String string = this.convertToString(object, displayDescriptor.getTypeDescriptor());
            string = XMLEscapeUtils.escapeUnicode((String)string);
            String string2 = XMLEscapeUtils.escapeUnicode((String)displayDescriptor.getName());
            partStub.add(new PropertyStub(string2, string));
        }
    }

    private void copyViews(ViewPositionSource viewPositionSource, MaltegoEntity maltegoEntity, EntityStub entityStub) {
        if (viewPositionSource != null) {
            Collection<String> collection = viewPositionSource.getViews();
            for (String string : collection) {
                Point point = viewPositionSource.getCenter(string, (EntityID)maltegoEntity.getID());
                if (point == null) continue;
                entityStub.addCenter(new PositionStub(string, this.getCenter(point)));
            }
        }
    }

    private String getCenter(Point point) {
        if (point == null) {
            return null;
        }
        return String.format("%d,%d", point.x, point.y);
    }

    private void copyDynamicProperties(DisplayDescriptorCollection displayDescriptorCollection, MaltegoPart maltegoPart, PartStub partStub) {
        for (PropertyDescriptor propertyDescriptor : maltegoPart.getProperties()) {
            Object object = maltegoPart.getValue(propertyDescriptor);
            if (displayDescriptorCollection.contains(propertyDescriptor.getName()) || !this.include(propertyDescriptor.getName())) continue;
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
            String string = this.convertToString(object, typeDescriptor);
            DynamicPropertyStub dynamicPropertyStub = this.translateDynamic(propertyDescriptor, string);
            partStub.add(dynamicPropertyStub);
        }
    }

    public DynamicPropertyStub translateDynamic(PropertyDescriptor propertyDescriptor, String string) {
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
        string = XMLEscapeUtils.escapeUnicode((String)string);
        String string2 = XMLEscapeUtils.escapeUnicode((String)propertyDescriptor.getName());
        DynamicPropertyStub dynamicPropertyStub = new DynamicPropertyStub(string2, string);
        dynamicPropertyStub.setType(typeDescriptor.getTypeName());
        dynamicPropertyStub.setHidden(this.convertOptional(propertyDescriptor.isHidden(), false));
        dynamicPropertyStub.setNullable(this.convertOptional(propertyDescriptor.isNullable(), true));
        dynamicPropertyStub.setReadOnly(this.convertOptional(propertyDescriptor.isReadonly(), false));
        String string3 = XMLEscapeUtils.escapeUnicode((String)this.getNonEmptyString(propertyDescriptor.getDisplayName(), propertyDescriptor.getName()));
        dynamicPropertyStub.setDisplayName(string3);
        return dynamicPropertyStub;
    }

    private void copyPropertyMappings(MaltegoPart maltegoPart, PartStub partStub) {
        partStub.setValuePropertyName(maltegoPart.getValuePropertyName());
        partStub.setDisplayValuePropertyName(maltegoPart.getDisplayValuePropertyName());
    }

    private void copyDisplayInformation(MaltegoPart maltegoPart, PartStub partStub) {
        if (maltegoPart.getDisplayInformation() != null) {
            for (DisplayInformation displayInformation : maltegoPart.getDisplayInformation()) {
                DisplayInfoStub displayInfoStub = new DisplayInfoStub();
                displayInfoStub.setName(XMLEscapeUtils.escapeUnicode((String)displayInformation.getName()));
                displayInfoStub.setValue(XMLEscapeUtils.escapeUnicode((String)displayInformation.getValue()));
                partStub.add(displayInfoStub);
            }
        }
    }

    private boolean include(String string) {
        return true;
    }

    private String convertToString(Object object, TypeDescriptor typeDescriptor) {
        if (typeDescriptor.getType() == String.class && object == null) {
            return "";
        }
        return typeDescriptor.convert(object);
    }

    private String getNonEmptyString(String string, String string2) {
        if (string == null || string.isEmpty()) {
            return null;
        }
        if (string.equals(string2)) {
            return null;
        }
        return string;
    }

    private String convertOptional(boolean bl, boolean bl2) {
        if (bl == bl2) {
            return null;
        }
        return Boolean.toString(bl);
    }

    private void updateFixedFields(MaltegoEntity maltegoEntity, EntityStub entityStub, boolean bl) {
        String string = XMLEscapeUtils.escapeUnicode((String)maltegoEntity.getNotes());
        if (bl) {
            if (maltegoEntity.getBookmark() >= 0) {
                entityStub.setBookmark(maltegoEntity.getBookmarkValue());
            }
            if (!StringUtilities.isNullOrEmpty((String)string) || maltegoEntity.isShowNotes()) {
                entityStub.setNote(string, maltegoEntity.isShowNotesValue());
            }
        } else {
            entityStub.setBookmark(maltegoEntity.getBookmarkValue());
            entityStub.setNote(string, maltegoEntity.isShowNotesValue());
        }
        entityStub.setWeight(maltegoEntity.getWeightValue());
        entityStub.setImagePropertyName(maltegoEntity.getImagePropertyName());
    }

    private boolean areEqual(Object object, Object object2) {
        if (object == object2) {
            return true;
        }
        if (object == null) {
            return object2.equals(object);
        }
        return object.equals(object2);
    }

    private static class GraphConnectionWrapper
    implements GraphConnectionSource {
        private GraphStructureReader _structureReader;

        public GraphConnectionWrapper(GraphID graphID) {
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                this._structureReader = graphStore.getGraphStructureStore().getStructureReader();
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }

        @Override
        public EntityID getSource(LinkID linkID) {
            EntityID entityID = null;
            try {
                entityID = this._structureReader.getSource(linkID);
            }
            catch (GraphStoreException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return entityID;
        }

        @Override
        public EntityID getTarget(LinkID linkID) {
            EntityID entityID = null;
            try {
                entityID = this._structureReader.getTarget(linkID);
            }
            catch (GraphStoreException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return entityID;
        }
    }

}

