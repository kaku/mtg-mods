/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.compact.CompactGraphNameMappings;
import com.paterva.maltego.serializers.compact.DeserializedGraph;
import com.paterva.maltego.serializers.compact.stubs.DisplayInfoStub;
import com.paterva.maltego.serializers.compact.stubs.DynamicPropertyStub;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import com.paterva.maltego.serializers.compact.stubs.PartStub;
import com.paterva.maltego.serializers.compact.stubs.PathStub;
import com.paterva.maltego.serializers.compact.stubs.PositionStub;
import com.paterva.maltego.serializers.compact.stubs.PropertyStub;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.util.Exceptions;

public class CompactGraphReader {
    private CompactGraphNameMappings _mappings = null;

    public CompactGraphReader(CompactGraphNameMappings compactGraphNameMappings) {
        this._mappings = compactGraphNameMappings;
    }

    public CompactGraphReader() {
    }

    public DeserializedGraph translate(GraphSnippetStub graphSnippetStub, GraphID graphID, boolean bl, boolean bl2) throws GraphSerializationException {
        EntityID entityID;
        DeserializedGraph deserializedGraph = new DeserializedGraph();
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        Collection<MaltegoEntity> collection = deserializedGraph.getEntities();
        HashMap<EntityID, MaltegoEntity> hashMap = new HashMap<EntityID, MaltegoEntity>();
        HashMap<Integer, MaltegoEntity> hashMap2 = new HashMap<Integer, MaltegoEntity>();
        Map<String, Map<EntityID, Point>> map = deserializedGraph.getCenters();
        for (EntityStub object2 : graphSnippetStub.getEntities()) {
            boolean collection2 = bl && object2.getID() != null;
            entityID = object2.getID() != null ? EntityID.parse((String)object2.getID()) : EntityID.create();
            MaltegoEntity maltegoEntity = this.createEntity(entityID, object2.getType(), graphID, collection2, bl2);
            this.translate(object2, maltegoEntity, entityRegistry, graphID, bl2);
            this.addCenters(map, maltegoEntity, object2.getCenters());
            collection.add(maltegoEntity);
            hashMap.put((EntityID)maltegoEntity.getID(), maltegoEntity);
            if (object2.getNum() != null) {
                hashMap2.put(object2.getNum(), maltegoEntity);
            }
            deserializedGraph.add(object2, maltegoEntity);
        }
        LinkRegistry linkRegistry = LinkRegistry.getDefault();
        Map<LinkID, LinkEntityIDs> map2 = deserializedGraph.getConnections();
        Collection<MaltegoLink> collection2 = deserializedGraph.getLinks();
        entityID = deserializedGraph.getPaths();
        for (LinkStub linkStub : graphSnippetStub.getLinks()) {
            MaltegoLink maltegoLink;
            LinkID linkID;
            if (linkStub.getID() != null && linkStub.getType() == null && (maltegoLink = GraphStoreHelper.getLink((GraphID)graphID, (LinkID)(linkID = LinkID.parse((String)linkStub.getID())))) != null) {
                linkStub.setType(maltegoLink.getTypeName());
            }
            if (linkStub.getType() == null) {
                linkStub.setType("maltego.link.manual-link");
            }
            boolean linkID2 = bl && linkStub.getID() != null;
            this.convertLinkNames(linkStub);
            maltegoLink = linkStub.getID() != null ? LinkID.parse((String)linkStub.getID()) : LinkID.create();
            MaltegoLink maltegoLink2 = this.createLink((LinkID)maltegoLink, linkStub.getType(), graphID, linkID2, bl2);
            this.translate(linkStub, maltegoLink2, map2, linkRegistry, (LinkID)maltegoLink, hashMap, hashMap2, graphID, bl2);
            this.addPaths((Map<String, Map<LinkID, List<Point>>>)entityID, maltegoLink2, linkStub.getPaths());
            collection2.add(maltegoLink2);
            deserializedGraph.add(linkStub, maltegoLink2);
        }
        return deserializedGraph;
    }

    public void translate(EntityStub entityStub, MaltegoEntity maltegoEntity, EntityRegistry entityRegistry, GraphID graphID, boolean bl) throws GraphSerializationException {
        DisplayDescriptorCollection displayDescriptorCollection;
        EntityID entityID;
        String string = entityStub.getType();
        if (string == null && entityStub.getID() != null && !bl) {
            entityID = EntityID.parse((String)entityStub.getID());
            if (!this.exists(graphID, entityID)) {
                throw new GraphSerializationException("Entity not found with id=" + entityStub.getID(), true);
            }
            try {
                displayDescriptorCollection = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphDataStoreReader graphDataStoreReader = displayDescriptorCollection.getGraphDataStore().getDataStoreReader();
                string = graphDataStoreReader.getEntityType(entityID);
            }
            catch (GraphStoreException var8_9) {
                throw new GraphSerializationException((Throwable)var8_9, true);
            }
        }
        entityID = entityStub.getProperties();
        if (string == null && !entityID.isEmpty()) {
            throw new GraphSerializationException("Type is required for entity", true);
        }
        if (string != null) {
            displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)entityRegistry, (String)string);
            this.copyStaticProperties(entityStub, (MaltegoPart)maltegoEntity, displayDescriptorCollection);
        }
        this.copyDynamicProperties(entityStub, (MaltegoPart)maltegoEntity);
        maltegoEntity.setWeight(entityStub.getWeight());
        maltegoEntity.setBookmark(entityStub.getBookmark());
        maltegoEntity.setNotes(XMLEscapeUtils.unescapeUnicode((String)entityStub.getNoteText()));
        maltegoEntity.setShowNotes(entityStub.getNoteVisible());
        this.copyDisplayInformation(entityStub, (MaltegoPart)maltegoEntity);
        this.copyPropertyMappings(entityStub, (MaltegoPart)maltegoEntity);
    }

    public void translate(LinkStub linkStub, MaltegoLink maltegoLink, Map<LinkID, LinkEntityIDs> map, LinkRegistry linkRegistry, LinkID linkID, Map<EntityID, MaltegoEntity> map2, Map<Integer, MaltegoEntity> map3, GraphID graphID, boolean bl) throws GraphSerializationException {
        DisplayDescriptorCollection displayDescriptorCollection;
        Object object;
        Object object2;
        LinkID linkID2;
        GraphDataStoreReader graphDataStoreReader;
        MaltegoEntity maltegoEntity;
        EntityID entityID;
        Object object3;
        String string = linkStub.getType();
        if (string == null && linkStub.getID() != null && !bl) {
            linkID2 = LinkID.parse((String)linkStub.getID());
            displayDescriptorCollection = GraphStoreHelper.getLink((GraphID)graphID, (LinkID)linkID2);
            if (displayDescriptorCollection == null) {
                throw new GraphSerializationException("Link not found with id=" + linkStub.getID(), true);
            }
            string = displayDescriptorCollection.getTypeName();
        }
        linkID2 = linkStub.getProperties();
        if (string == null && !linkID2.isEmpty()) {
            throw new GraphSerializationException("Type is required for link", true);
        }
        if (string != null) {
            displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)linkRegistry, (String)string);
            this.copyStaticProperties(linkStub, (MaltegoPart)maltegoLink, displayDescriptorCollection);
        }
        this.copyDynamicProperties(linkStub, (MaltegoPart)maltegoLink);
        this.copyDisplayInformation(linkStub, (MaltegoPart)maltegoLink);
        this.copyPropertyMappings(linkStub, (MaltegoPart)maltegoLink);
        maltegoLink.setReversed(linkStub.isReversed());
        boolean bl2 = linkStub.getID() == null;
        MaltegoEntity maltegoEntity2 = null;
        if (linkStub.getFrom() != null) {
            object3 = EntityID.parse((String)linkStub.getFrom());
            maltegoEntity2 = this.getEntity((EntityID)object3, map2);
        } else if (linkStub.getFromN() != null && (maltegoEntity2 = map3.get(linkStub.getFromN())) == null) {
            String string2 = this.getErrorLinkId(linkStub);
            String string3 = String.format("Source entity (%s) not found for link (%s)", linkStub.getFromN(), string2);
            throw new GraphSerializationException(string3, true);
        }
        if (maltegoEntity2 == null) {
            if (bl2) {
                object3 = this.getErrorLinkId(linkStub);
                String string4 = String.format("Source entity must be specified for link (%s)", object3);
                throw new GraphSerializationException(string4, true);
            }
            try {
                object3 = LinkID.parse((String)linkStub.getID());
                object2 = GraphStoreRegistry.getDefault().forGraphID(graphID);
                object = object2.getGraphStructureStore().getStructureReader();
                if (object.exists((LinkID)object3)) {
                    entityID = object.getSource((LinkID)object3);
                    graphDataStoreReader = object2.getGraphDataStore().getDataStoreReader();
                    maltegoEntity = graphDataStoreReader.getEntity(entityID);
                    maltegoEntity2 = new EntityUpdate(maltegoEntity);
                }
            }
            catch (GraphStoreException var14_17) {
                Exceptions.printStackTrace((Throwable)var14_17);
            }
        }
        object3 = null;
        if (linkStub.getTo() != null) {
            object2 = EntityID.parse((String)linkStub.getTo());
            object3 = this.getEntity((EntityID)object2, map2);
        } else if (linkStub.getToN() != null && (object3 = map3.get(linkStub.getToN())) == null) {
            object2 = this.getErrorLinkId(linkStub);
            object = String.format("Target entity (%s) not found for link (%s)", linkStub.getToN(), object2);
            throw new GraphSerializationException((String)object, true);
        }
        if (object3 == null) {
            if (bl2) {
                object2 = this.getErrorLinkId(linkStub);
                object = String.format("Target entity must be specified for link (%s)", object2);
                throw new GraphSerializationException((String)object, true);
            }
            try {
                object2 = LinkID.parse((String)linkStub.getID());
                object = GraphStoreRegistry.getDefault().forGraphID(graphID);
                entityID = object.getGraphStructureStore().getStructureReader();
                if (entityID.exists((LinkID)object2)) {
                    graphDataStoreReader = entityID.getTarget((LinkID)object2);
                    maltegoEntity = object.getGraphDataStore().getDataStoreReader();
                    MaltegoEntity maltegoEntity3 = maltegoEntity.getEntity((EntityID)graphDataStoreReader);
                    object3 = new EntityUpdate(maltegoEntity3);
                }
            }
            catch (GraphStoreException var15_21) {
                Exceptions.printStackTrace((Throwable)var15_21);
            }
        }
        if (maltegoEntity2 != null && object3 != null) {
            map.put(linkID, new LinkEntityIDs((EntityID)maltegoEntity2.getID(), (EntityID)object3.getID()));
        }
    }

    private void convertLinkNames(LinkStub linkStub) {
        if (this._mappings != null) {
            boolean bl = linkStub.getType().contains("manual");
            linkStub.setDisplayValuePropertyName(this._mappings.linkPropNameToLongName(linkStub.getDisplayValuePropertyName(), bl));
            linkStub.setValuePropertyName(this._mappings.linkPropNameToLongName(linkStub.getValuePropertyName(), bl));
            for (PropertyStub propertyStub2 : linkStub.getProperties()) {
                propertyStub2.setName(this._mappings.linkPropNameToLongName(propertyStub2.getName(), bl));
            }
            for (PropertyStub propertyStub : linkStub.getDynamicProperties()) {
                propertyStub.setName(this._mappings.linkPropNameToLongName(propertyStub.getName(), bl));
            }
            linkStub.setType(this._mappings.linkTypeNameToLongName(linkStub.getType()));
        }
    }

    private String getErrorLinkId(LinkStub linkStub) {
        String string = linkStub.getID();
        if (string == null) {
            string = linkStub.getNum() != null ? Integer.toString(linkStub.getNum()) : "new";
        }
        return string;
    }

    private MaltegoLink createLink(LinkID linkID, String string, GraphID graphID, boolean bl, boolean bl2) throws GraphSerializationException {
        if (!bl) {
            if (string == null) {
                throw new GraphSerializationException("Type required for link", true);
            }
            return LinkFactory.forGraphID((GraphID)graphID).createInstance(string, linkID, false);
        }
        if (!bl2 && !this.exists(graphID, linkID)) {
            throw new GraphSerializationException("Link not found with id=" + (Object)linkID, true);
        }
        return new LinkUpdate(linkID, string);
    }

    private MaltegoEntity createEntity(EntityID entityID, String string, GraphID graphID, boolean bl, boolean bl2) throws GraphSerializationException {
        if (!bl) {
            try {
                if (string == null) {
                    throw new GraphSerializationException("Type required for entity", true);
                }
                return EntityFactory.forGraphID((GraphID)graphID).createInstance(string, false, entityID, false);
            }
            catch (TypeInstantiationException var6_6) {
                throw new GraphSerializationException("Unable to create entity " + string, (Throwable)var6_6, true);
            }
        }
        if (!bl2 && !this.exists(graphID, entityID)) {
            throw new GraphSerializationException("Entity not found with id=" + (Object)entityID, true);
        }
        return new EntityUpdate(entityID, string);
    }

    private boolean exists(GraphID graphID, EntityID entityID) {
        boolean bl = false;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl = graphStructureReader.exists(entityID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return bl;
    }

    private boolean exists(GraphID graphID, LinkID linkID) {
        boolean bl = false;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl = graphStructureReader.exists(linkID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return bl;
    }

    private void addCenters(Map<String, Map<EntityID, Point>> map, MaltegoEntity maltegoEntity, Collection<PositionStub> collection) {
        for (PositionStub positionStub : collection) {
            Map<EntityID, Point> map2 = this.getOrCreateView(map, positionStub.getView());
            map2.put((EntityID)maltegoEntity.getID(), this.parsePoint(positionStub.getCenter()));
        }
    }

    private void addPaths(Map<String, Map<LinkID, List<Point>>> map, MaltegoLink maltegoLink, Collection<PathStub> collection) {
        for (PathStub pathStub : collection) {
            Map<LinkID, List<Point>> map2 = this.getOrCreatePathView(map, pathStub.getView());
            map2.put((LinkID)maltegoLink.getID(), this.parsePath(pathStub.getPath()));
        }
    }

    private Map<EntityID, Point> getOrCreateView(Map<String, Map<EntityID, Point>> map, String string) {
        Map<EntityID, Point> map2 = map.get(string);
        if (map2 == null) {
            map2 = new HashMap<EntityID, Point>();
            map.put(string, map2);
        }
        return map2;
    }

    private Map<LinkID, List<Point>> getOrCreatePathView(Map<String, Map<LinkID, List<Point>>> map, String string) {
        Map<LinkID, List<Point>> map2 = map.get(string);
        if (map2 == null) {
            map2 = new HashMap<LinkID, List<Point>>();
            map.put(string, map2);
        }
        return map2;
    }

    private List<Point> parsePath(String string) {
        String[] arrstring;
        ArrayList<Point> arrayList = new ArrayList<Point>();
        for (String string2 : arrstring = string.split(";")) {
            arrayList.add(this.parsePoint(string2));
        }
        return arrayList;
    }

    private Point parsePoint(String string) {
        int n = string.indexOf(44);
        if (n > 0) {
            int n2 = Integer.parseInt(string.substring(0, n));
            int n3 = Integer.parseInt(string.substring(n + 1, string.length()));
            return new Point(n2, n3);
        }
        return new Point();
    }

    public void copyDisplayInformation(PartStub partStub, MaltegoPart maltegoPart) {
        List<DisplayInfoStub> list = partStub.getDisplayInfo();
        if (list != null) {
            for (DisplayInfoStub displayInfoStub : list) {
                String string = XMLEscapeUtils.unescapeUnicode((String)displayInfoStub.getName());
                String string2 = XMLEscapeUtils.unescapeUnicode((String)displayInfoStub.getValue());
                maltegoPart.getOrCreateDisplayInformation().add(string, string2);
            }
        }
    }

    public void copyStaticProperties(PartStub partStub, MaltegoPart maltegoPart, DisplayDescriptorCollection displayDescriptorCollection) throws GraphSerializationException {
        for (PropertyStub propertyStub : partStub.getProperties()) {
            DisplayDescriptor displayDescriptor = displayDescriptorCollection.get(propertyStub.getName());
            if (displayDescriptor == null) {
                partStub.add(new DynamicPropertyStub(propertyStub));
                continue;
            }
            String string = XMLEscapeUtils.unescapeUnicode((String)propertyStub.getValue());
            if (string == null) {
                string = "";
            }
            Object object = displayDescriptor.getTypeDescriptor().convert(string);
            maltegoPart.addProperty((PropertyDescriptor)displayDescriptor);
            maltegoPart.setValue((PropertyDescriptor)displayDescriptor, object, false, false);
        }
    }

    public void copyDynamicProperties(PartStub partStub, MaltegoPart maltegoPart) throws GraphSerializationException {
        List<DynamicPropertyStub> list = partStub.getDynamicProperties();
        if (list == null) {
            return;
        }
        for (DynamicPropertyStub dynamicPropertyStub : list) {
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(dynamicPropertyStub.getType());
            if (typeDescriptor == null) {
                throw new GraphSerializationException(String.format("No type descriptor found for type=%s.", partStub.getType()), true);
            }
            String string = XMLEscapeUtils.unescapeUnicode((String)dynamicPropertyStub.getDisplayName());
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), dynamicPropertyStub.getName(), string);
            propertyDescriptor.setHidden(this.getBoolean(dynamicPropertyStub.getHidden(), false));
            propertyDescriptor.setNullable(this.getBoolean(dynamicPropertyStub.getNullable(), true));
            propertyDescriptor.setReadonly(this.getBoolean(dynamicPropertyStub.getReadOnly(), false));
            Object object = typeDescriptor.convert(XMLEscapeUtils.unescapeUnicode((String)dynamicPropertyStub.getValue()));
            maltegoPart.addProperty(propertyDescriptor);
            maltegoPart.setValue(propertyDescriptor, object, false, false);
        }
    }

    public void copyPropertyMappings(PartStub partStub, MaltegoPart maltegoPart) {
        if (partStub.getValuePropertyName() != null) {
            maltegoPart.setValueProperty(new PropertyDescriptor(String.class, partStub.getValuePropertyName()));
        }
        if (partStub.getDisplayValuePropertyName() != null) {
            maltegoPart.setDisplayValueProperty(new PropertyDescriptor(String.class, partStub.getDisplayValuePropertyName()));
        }
        if (partStub instanceof EntityStub && maltegoPart instanceof MaltegoEntity) {
            EntityStub entityStub = (EntityStub)partStub;
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            if (entityStub.getImagePropertyName() != null) {
                maltegoEntity.setImageProperty(new PropertyDescriptor(String.class, entityStub.getImagePropertyName()));
            }
        }
    }

    private MaltegoEntity getEntity(EntityID entityID, Map<EntityID, MaltegoEntity> map) {
        MaltegoEntity maltegoEntity = map.get((Object)entityID);
        if (maltegoEntity == null) {
            maltegoEntity = new EntityUpdate(entityID);
        }
        return maltegoEntity;
    }

    private boolean getBoolean(String string, boolean bl) {
        if (string == null) {
            return bl;
        }
        return Boolean.parseBoolean(string);
    }
}

