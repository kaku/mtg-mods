/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.IconStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="cat", strict=0)
public class IconCategoryStub {
    @Attribute(name="name", required=1)
    @SerializedName(value="name")
    private String _name;
    @ElementList(inline=1, type=IconStub.class, required=1)
    @SerializedName(value="icons")
    private List<IconStub> _icons;

    public IconCategoryStub() {
    }

    public IconCategoryStub(String string) {
        this._name = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public void add(IconStub iconStub) {
        this._icons = ListUtil.add(this._icons, iconStub);
    }

    public List<IconStub> getIcons() {
        return ListUtil.get(this._icons);
    }
}

