/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.IconSize
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.util.IconSize;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

public class IconCollection
extends HashMap<String, Map<String, Map<IconSize, Image>>> {
}

