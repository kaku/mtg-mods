/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.NoteStub;
import com.paterva.maltego.serializers.compact.stubs.PartStub;
import com.paterva.maltego.serializers.compact.stubs.PositionStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="entity", strict=0)
public class EntityStub
extends PartStub {
    @ElementList(inline=1, type=PositionStub.class, required=0)
    @SerializedName(value="positions")
    private List<PositionStub> _centers;
    @Attribute(name="bm", required=0)
    @SerializedName(value="bm")
    private String _bookmark;
    @Attribute(name="w8", required=0)
    @SerializedName(value="w8")
    private String _weight;
    @Element(name="note", required=0)
    @SerializedName(value="note")
    private NoteStub _note;
    @Attribute(name="img", required=0)
    @SerializedName(value="img")
    private String _imagePropertyName;

    public void addCenter(PositionStub positionStub) {
        this._centers = ListUtil.add(this._centers, positionStub);
    }

    public List<PositionStub> getCenters() {
        return ListUtil.get(this._centers);
    }

    public Integer getWeight() {
        if (this._weight == null) {
            return null;
        }
        return Integer.parseInt(this._weight);
    }

    public void setWeight(Integer n) {
        this._weight = n == null ? null : n.toString();
    }

    public Integer getBookmark() {
        if (this._bookmark == null) {
            return null;
        }
        return Integer.parseInt(this._bookmark);
    }

    public void setBookmark(Integer n) {
        this._bookmark = n == null ? null : n.toString();
    }

    public void setNote(String string, Boolean bl) {
        if (bl != null || string != null) {
            this._note = new NoteStub();
            this._note.setText(string);
            this._note.setVisible(bl == null ? null : bl.toString());
        } else {
            this._note = null;
        }
    }

    public Boolean getNoteVisible() {
        if (this._note != null && this._note.getVisible() != null) {
            return Boolean.parseBoolean(this._note.getVisible());
        }
        return null;
    }

    public String getNoteText() {
        if (this._note != null) {
            String string = this._note.getText();
            if (string == null) {
                return "";
            }
            return string;
        }
        return null;
    }

    public void setImagePropertyName(String string) {
        this._imagePropertyName = string;
    }

    public String getImagePropertyName() {
        return this._imagePropertyName;
    }
}

