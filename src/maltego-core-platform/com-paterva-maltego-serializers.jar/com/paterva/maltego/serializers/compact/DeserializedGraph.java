/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.MaltegoGraphSnippet
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.MaltegoGraphSnippet;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import java.awt.Point;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeserializedGraph {
    private final MaltegoGraphSnippet _graphSnippet = new MaltegoGraphSnippet();
    private final Map<EntityStub, MaltegoEntity> _entityMap = new HashMap<EntityStub, MaltegoEntity>();
    private final Map<LinkStub, MaltegoLink> _linkMap = new HashMap<LinkStub, MaltegoLink>();

    public MaltegoGraphSnippet getGraphSnippet() {
        return this._graphSnippet;
    }

    public void add(EntityStub entityStub, MaltegoEntity maltegoEntity) {
        this._entityMap.put(entityStub, maltegoEntity);
    }

    public void add(LinkStub linkStub, MaltegoLink maltegoLink) {
        this._linkMap.put(linkStub, maltegoLink);
    }

    public Map<EntityStub, MaltegoEntity> getEntityMap() {
        return Collections.unmodifiableMap(this._entityMap);
    }

    public Map<LinkStub, MaltegoLink> getLinkMap() {
        return Collections.unmodifiableMap(this._linkMap);
    }

    public Collection<MaltegoEntity> getEntities() {
        return this._graphSnippet.getEntities();
    }

    public Map<String, Map<EntityID, Point>> getCenters() {
        return this._graphSnippet.getCenters();
    }

    public Map<LinkID, LinkEntityIDs> getConnections() {
        return this._graphSnippet.getConnections();
    }

    public Collection<MaltegoLink> getLinks() {
        return this._graphSnippet.getLinks();
    }

    public Map<String, Map<LinkID, List<Point>>> getPaths() {
        return this._graphSnippet.getPaths();
    }
}

