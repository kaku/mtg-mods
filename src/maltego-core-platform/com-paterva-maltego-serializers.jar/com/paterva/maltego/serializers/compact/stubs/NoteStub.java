/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="note", strict=0)
public class NoteStub {
    @Attribute(name="show", required=0)
    @SerializedName(value="show")
    private String _visible;
    @Text(required=0)
    @SerializedName(value="text")
    private String _text;

    public String getVisible() {
        return this._visible;
    }

    public void setVisible(String string) {
        this._visible = string;
    }

    public String getText() {
        return this._text;
    }

    public void setText(String string) {
        this._text = string;
    }
}

