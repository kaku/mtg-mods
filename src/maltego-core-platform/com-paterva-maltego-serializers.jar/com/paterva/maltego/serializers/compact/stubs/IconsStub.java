/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.IconCategoryStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="icons", strict=0)
public class IconsStub {
    @ElementList(inline=1, type=IconCategoryStub.class, required=1)
    @SerializedName(value="cats")
    private List<IconCategoryStub> _categories;

    public void add(IconCategoryStub iconCategoryStub) {
        this._categories = ListUtil.add(this._categories, iconCategoryStub);
    }

    public List<IconCategoryStub> getCategories() {
        return ListUtil.get(this._categories);
    }
}

