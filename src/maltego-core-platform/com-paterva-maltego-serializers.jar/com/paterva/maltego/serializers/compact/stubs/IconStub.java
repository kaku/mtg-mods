/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.IconImageStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="icon", strict=0)
public class IconStub {
    @Attribute(name="name", required=1)
    @SerializedName(value="name")
    private String _name;
    @ElementList(inline=1, type=IconImageStub.class, required=1)
    @SerializedName(value="imgs")
    private List<IconImageStub> _iconImages;

    public IconStub() {
    }

    public IconStub(String string) {
        this._name = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public void add(IconImageStub iconImageStub) {
        this._iconImages = ListUtil.add(this._iconImages, iconImageStub);
    }

    public List<IconImageStub> getIconImages() {
        return ListUtil.get(this._iconImages);
    }
}

