/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="error", strict=0)
public class ErrorStub {
    @Attribute(name="msg", required=1)
    @SerializedName(value="msg")
    private String _message;

    public static ErrorStub create(Exception exception) {
        String string = exception.getMessage();
        if (string != null) {
            string = string.replaceFirst("^\\s*[^\\s]+:\\s+", "");
        }
        if (string == null || string.isEmpty()) {
            string = exception.getClass().getName();
        }
        ErrorStub errorStub = new ErrorStub();
        errorStub.setMessage(string);
        return errorStub;
    }

    public String getMessage() {
        return this._message;
    }

    public void setMessage(String string) {
        this._message = string;
    }
}

