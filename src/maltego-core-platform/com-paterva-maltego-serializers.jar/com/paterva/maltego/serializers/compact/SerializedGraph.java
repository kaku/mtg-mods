/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SerializedGraph {
    private GraphSnippetStub _graph;
    private final Map<MaltegoEntity, EntityStub> _entityMap = new HashMap<MaltegoEntity, EntityStub>();
    private final Map<MaltegoLink, LinkStub> _linkMap = new HashMap<MaltegoLink, LinkStub>();

    public GraphSnippetStub getGraph() {
        return this._graph;
    }

    public void setGraph(GraphSnippetStub graphSnippetStub) {
        this._graph = graphSnippetStub;
    }

    public void add(MaltegoEntity maltegoEntity, EntityStub entityStub) {
        this._entityMap.put(maltegoEntity, entityStub);
    }

    public void add(MaltegoLink maltegoLink, LinkStub linkStub) {
        this._linkMap.put(maltegoLink, linkStub);
    }

    public Map<MaltegoEntity, EntityStub> getEntityMap() {
        return Collections.unmodifiableMap(this._entityMap);
    }

    public Map<MaltegoLink, LinkStub> getLinkMap() {
        return Collections.unmodifiableMap(this._linkMap);
    }
}

