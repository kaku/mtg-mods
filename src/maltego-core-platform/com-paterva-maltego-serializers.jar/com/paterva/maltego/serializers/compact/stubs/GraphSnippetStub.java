/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.Collection;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(strict=0, name="graph")
public class GraphSnippetStub
extends OrderedStub {
    @ElementList(inline=1, type=EntityStub.class, required=0)
    @SerializedName(value="entities")
    private List<EntityStub> _entities;
    @ElementList(inline=1, type=LinkStub.class, required=0)
    @SerializedName(value="links")
    private List<LinkStub> _links;

    public void add(EntityStub entityStub) {
        this._entities = ListUtil.add(this._entities, entityStub);
    }

    public void addEntities(Collection<EntityStub> collection) {
        this._entities = ListUtil.addAll(this._entities, collection);
    }

    public void removeEntities(Collection<EntityStub> collection) {
        this._entities = ListUtil.removeAll(this._entities, collection);
    }

    public List<EntityStub> getEntities() {
        return ListUtil.get(this._entities);
    }

    public void add(LinkStub linkStub) {
        this._links = ListUtil.add(this._links, linkStub);
    }

    public void addLinks(Collection<LinkStub> collection) {
        this._links = ListUtil.addAll(this._links, collection);
    }

    public void removeLinks(Collection<LinkStub> collection) {
        this._links = ListUtil.removeAll(this._links, collection);
    }

    public List<LinkStub> getLinks() {
        return ListUtil.get(this._links);
    }
}

