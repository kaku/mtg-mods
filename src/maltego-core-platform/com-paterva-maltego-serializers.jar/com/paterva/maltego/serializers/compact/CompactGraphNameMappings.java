/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.serializers.compact;

public interface CompactGraphNameMappings {
    public String linkPropNameToShortName(String var1);

    public String linkPropNameToLongName(String var1, boolean var2);

    public String linkTypeNameToShortName(String var1);

    public String linkTypeNameToLongName(String var1);
}

