/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.serializers.compact;

import com.paterva.maltego.serializers.compact.CompactGraphNameMappings;

public class LinkNameMappings
implements CompactGraphNameMappings {
    private final String[] _linkPropMap = new String[]{"maltego.link.transform.name", "transform.name", "maltego.link.transform.display-name", "transform.display-name", "maltego.link.transform.version", "transform.version", "maltego.link.transform.run-date", "transform.run-date", "maltego.link.show-label", "show-label", "maltego.link.color", "color", "maltego.link.style", "style", "maltego.link.thickness", "thickness", "maltego.link.attachments", "attachments", "maltego.link.weight", "weight", "maltego.transform.type", "transform.type"};
    private final String[] _linkTypeMap = new String[]{"maltego.link.manual-link", "manual", "maltego.link.transform-link", "transform"};

    @Override
    public String linkPropNameToShortName(String string) {
        if ("maltego.link.manual.type".equals(string) || "maltego.link.label".equals(string)) {
            return "label";
        }
        if ("maltego.link.manual.description".equals(string) || "maltego.link.description".equals(string)) {
            return "description";
        }
        return this.getShortName(this._linkPropMap, string);
    }

    @Override
    public String linkPropNameToLongName(String string, boolean bl) {
        if ("label".equals(string)) {
            return bl ? "maltego.link.manual.type" : "maltego.link.label";
        }
        if ("description".equals(string)) {
            return bl ? "maltego.link.manual.description" : "maltego.link.description";
        }
        return this.getLongName(this._linkPropMap, string);
    }

    @Override
    public String linkTypeNameToShortName(String string) {
        return this.getShortName(this._linkTypeMap, string);
    }

    @Override
    public String linkTypeNameToLongName(String string) {
        return this.getLongName(this._linkTypeMap, string);
    }

    private String getShortName(String[] arrstring, String string) {
        if (string != null) {
            for (int i = 0; i < arrstring.length; i += 2) {
                if (!arrstring[i].equals(string)) continue;
                return arrstring[i + 1];
            }
        }
        return string;
    }

    private String getLongName(String[] arrstring, String string) {
        if (string != null) {
            for (int i = 1; i < arrstring.length; i += 2) {
                if (!arrstring[i].equals(string)) continue;
                return arrstring[i - 1];
            }
        }
        return string;
    }
}

