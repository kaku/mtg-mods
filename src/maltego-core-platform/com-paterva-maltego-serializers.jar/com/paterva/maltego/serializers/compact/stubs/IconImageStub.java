/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="img", strict=0)
public class IconImageStub {
    @Attribute(name="size", required=1)
    @SerializedName(value="size")
    private int _size;
    @Text(required=1)
    @SerializedName(value="base64")
    private String _base64Image;

    public IconImageStub() {
    }

    public IconImageStub(int n, String string) {
        this._size = n;
        this._base64Image = string;
    }

    public int getSize() {
        return this._size;
    }

    public void setSize(int n) {
        this._size = n;
    }

    public String getBase64Image() {
        return this._base64Image;
    }

    public void setBase64Image(String string) {
        this._base64Image = string;
    }
}

