/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.paterva.maltego.serializers.compact.stubs.PropertyStub;
import org.simpleframework.xml.Root;

@Root(name="dprop", strict=0)
public class DynamicPropertyStub
extends PropertyStub {
    public DynamicPropertyStub() {
    }

    public DynamicPropertyStub(String string, String string2) {
        super(string, string2);
    }

    public DynamicPropertyStub(PropertyStub propertyStub) {
        this.setHidden(propertyStub.getHidden());
        this.setName(propertyStub.getName());
        this.setNullable(propertyStub.getNullable());
        this.setReadOnly(propertyStub.getReadOnly());
        this.setValue(propertyStub.getValue());
        String string = propertyStub.getType();
        this.setType(string != null ? string : "string");
        String string2 = propertyStub.getDisplayName();
        this.setDisplayName(string2 != null ? string2 : propertyStub.getName());
    }
}

