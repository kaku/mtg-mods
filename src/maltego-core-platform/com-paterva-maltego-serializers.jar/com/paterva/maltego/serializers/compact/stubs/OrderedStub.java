/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.ErrorStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public abstract class OrderedStub {
    @Attribute(name="n", required=0)
    @SerializedName(value="n")
    private Integer _num;
    @Element(name="error", required=0)
    @SerializedName(value="error")
    private ErrorStub _error;

    public Integer getNum() {
        return this._num;
    }

    public void setNum(Integer n) {
        this._num = n;
    }

    public ErrorStub getError() {
        return this._error;
    }

    public void setError(ErrorStub errorStub) {
        this._error = errorStub;
    }
}

