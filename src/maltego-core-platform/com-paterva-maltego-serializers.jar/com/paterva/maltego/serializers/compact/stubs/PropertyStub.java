/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="prop", strict=0)
public class PropertyStub {
    @Attribute(name="name", required=1)
    @SerializedName(value="name")
    private String _name;
    @Text(required=0)
    @SerializedName(value="value")
    private String _value = "";
    @Attribute(name="hide", required=0)
    @SerializedName(value="hide")
    private String _hidden;
    @Attribute(name="ro", required=0)
    @SerializedName(value="ro")
    private String _readOnly;
    @Attribute(name="null", required=0)
    @SerializedName(value="null")
    private String _nullable;
    @Attribute(name="disp", required=0)
    @SerializedName(value="disp")
    private String _displayName;
    @Attribute(name="type", required=0)
    @SerializedName(value="type")
    private String _type;

    public PropertyStub() {
    }

    public PropertyStub(String string, String string2) {
        this._name = string;
        this.setValue(string2);
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getValue() {
        return this._value;
    }

    public final void setValue(String string) {
        this._value = string != null ? string : "";
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public String getHidden() {
        return this._hidden;
    }

    public void setHidden(String string) {
        this._hidden = string;
    }

    public String getReadOnly() {
        return this._readOnly;
    }

    public void setReadOnly(String string) {
        this._readOnly = string;
    }

    public String getNullable() {
        return this._nullable;
    }

    public void setNullable(String string) {
        this._nullable = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }
}

