/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.PartStub;
import com.paterva.maltego.serializers.compact.stubs.PathStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.Collection;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="link", strict=0)
public class LinkStub
extends PartStub {
    @ElementList(inline=1, type=PathStub.class, required=0)
    @SerializedName(value="paths")
    private List<PathStub> _paths;
    @Attribute(name="from", required=0)
    @SerializedName(value="from")
    private String _from;
    @Attribute(name="to", required=0)
    @SerializedName(value="to")
    private String _to;
    @Attribute(name="fromN", required=0)
    @SerializedName(value="fromN")
    private Integer _fromN;
    @Attribute(name="toN", required=0)
    @SerializedName(value="toN")
    private Integer _toN;
    @Attribute(name="rev", required=0)
    @SerializedName(value="rev")
    private Boolean _reversed;

    public void add(PathStub pathStub) {
        this._paths = ListUtil.add(this._paths, pathStub);
    }

    public Collection<PathStub> getPaths() {
        return ListUtil.get(this._paths);
    }

    public String getFrom() {
        return this._from;
    }

    public void setFrom(String string) {
        this._from = string;
    }

    public String getTo() {
        return this._to;
    }

    public void setTo(String string) {
        this._to = string;
    }

    public Integer getFromN() {
        return this._fromN;
    }

    public void setFromN(Integer n) {
        this._fromN = n;
    }

    public Integer getToN() {
        return this._toN;
    }

    public void setToN(Integer n) {
        this._toN = n;
    }

    public Boolean isReversed() {
        return this._reversed;
    }

    public void setReversed(Boolean bl) {
        this._reversed = bl;
    }
}

