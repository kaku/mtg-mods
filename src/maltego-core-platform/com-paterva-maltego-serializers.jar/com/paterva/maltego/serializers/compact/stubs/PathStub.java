/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.serializers.compact.stubs;

import com.google.gson.annotations.SerializedName;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="path", strict=0)
public class PathStub {
    @Attribute(name="view")
    @SerializedName(value="view")
    private String _view;
    @Text(required=0)
    @SerializedName(value="path")
    private String _path;

    public PathStub() {
    }

    public PathStub(String string, String string2) {
        this._view = string;
        this._path = string2;
    }

    public String getView() {
        return this._view;
    }

    public void setView(String string) {
        this._view = string;
    }

    public String getPath() {
        return this._path;
    }

    public void setPath(String string) {
        this._path = string;
    }
}

