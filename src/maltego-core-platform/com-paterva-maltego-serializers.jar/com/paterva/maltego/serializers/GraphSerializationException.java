/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.serializers;

import com.paterva.maltego.util.NormalException;

public class GraphSerializationException
extends NormalException {
    public GraphSerializationException(NormalException normalException) {
        super(normalException);
    }

    public GraphSerializationException(String string, boolean bl) {
        super(string, bl);
    }

    public GraphSerializationException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public GraphSerializationException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

