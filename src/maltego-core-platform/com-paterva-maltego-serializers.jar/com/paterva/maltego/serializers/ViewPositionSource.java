/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.serializers;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import java.awt.Point;
import java.util.Collection;
import java.util.List;

public interface ViewPositionSource {
    public Collection<String> getViews();

    public Point getCenter(String var1, EntityID var2);

    public List<Point> getPath(String var1, LinkID var2);
}

