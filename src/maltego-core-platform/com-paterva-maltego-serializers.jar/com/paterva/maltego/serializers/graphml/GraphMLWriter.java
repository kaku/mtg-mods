/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.serializers.graphml;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class GraphMLWriter {
    private static GraphMLWriter _default;

    public static synchronized GraphMLWriter getDefault() {
        if (_default == null && (GraphMLWriter._default = (GraphMLWriter)Lookup.getDefault().lookup(GraphMLWriter.class)) == null) {
            throw new IllegalStateException("GraphML Writer not found.");
        }
        return _default;
    }

    public abstract void write(GraphID var1, OutputStream var2) throws IOException;

    public abstract void write(GraphID var1, OutputStream var2, Collection<EntityID> var3) throws IOException;

    public abstract void write(GraphID var1, OutputStream var2, Collection<EntityID> var3, boolean var4) throws IOException;

    public abstract String toString(GraphID var1) throws IOException;

    public abstract String toString(GraphID var1, Collection<EntityID> var2) throws IOException;

    public abstract String toString(GraphID var1, Collection<EntityID> var2, boolean var3) throws IOException;
}

