/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptors
 *  com.paterva.maltego.typing.PropertyProvider
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptors;
import com.paterva.maltego.typing.PropertyProvider;
import org.openide.util.Lookup;

public abstract class GlobalInputProvider {
    public static GlobalInputProvider getDefault() {
        GlobalInputProvider globalInputProvider = (GlobalInputProvider)Lookup.getDefault().lookup(GlobalInputProvider.class);
        if (globalInputProvider == null) {
            globalInputProvider = new TrivialGlobalInputProvider();
        }
        return globalInputProvider;
    }

    public abstract PropertyProvider getInputs();

    private static class TrivialGlobalInputProvider
    extends GlobalInputProvider
    implements PropertyProvider {
        private TrivialGlobalInputProvider() {
        }

        @Override
        public PropertyProvider getInputs() {
            return this;
        }

        public PropertyDescriptorCollection getProperties() {
            return PropertyDescriptors.emptySet();
        }

        public Object getValue(PropertyDescriptor propertyDescriptor) {
            return null;
        }

        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        }
    }

}

