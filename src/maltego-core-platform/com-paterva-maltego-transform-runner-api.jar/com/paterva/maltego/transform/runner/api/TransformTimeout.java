/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.transform.runner.api;

import org.openide.util.NbPreferences;

public class TransformTimeout {
    private static final String PREF_TIMEOUT = "maltego.global.transform.timeout";

    public static int getTimeout() {
        return NbPreferences.forModule(TransformTimeout.class).getInt("maltego.global.transform.timeout", 120000);
    }

    public static void setTimeout(int n) {
        NbPreferences.forModule(TransformTimeout.class).putInt("maltego.global.transform.timeout", n);
    }
}

