/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformResultHandler;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class TransformResultHandler {
    public static TransformResultHandler getDefault() {
        TransformResultHandler transformResultHandler = (TransformResultHandler)Lookup.getDefault().lookup(TransformResultHandler.class);
        if (transformResultHandler == null) {
            transformResultHandler = new DefaultTransformResultHandler();
        }
        return transformResultHandler;
    }

    public abstract void updateGraph(TransformRunContext var1, TransformDefinition var2, TransformResult var3, Map<EntityID, MaltegoEntity> var4);
}

