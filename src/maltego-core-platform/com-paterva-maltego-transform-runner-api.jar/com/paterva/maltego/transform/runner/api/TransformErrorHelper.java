/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.api.TransformErrorHandler;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import java.util.Collection;
import org.openide.util.Lookup;

public class TransformErrorHelper {
    public static boolean handle(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformMessage transformMessage) {
        Collection collection = Lookup.getDefault().lookupAll(TransformErrorHandler.class);
        if (collection != null) {
            for (TransformErrorHandler transformErrorHandler : collection) {
                if (!transformErrorHandler.handle(transformRunContext, transformDefinition, transformMessage)) continue;
                return true;
            }
        }
        return false;
    }
}

