/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.typing.DataSource
 *  org.netbeans.api.progress.aggregate.ProgressContributor
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformRunner;
import com.paterva.maltego.typing.DataSource;
import java.beans.PropertyChangeListener;
import java.util.Map;
import org.netbeans.api.progress.aggregate.ProgressContributor;
import org.openide.util.Lookup;

public abstract class TransformRunner {
    public static final String PROP_TRANSFORM_STARTED = "transform.started";
    public static final String PROP_TRANSFORM_PROGRESS = "transform.progress";
    public static final String PROP_TRANSFORM_DONE = "transform.done";
    private static TransformRunner _default;

    public static synchronized TransformRunner getDefault() {
        if (_default == null && (TransformRunner._default = (TransformRunner)Lookup.getDefault().lookup(TransformRunner.class)) == null) {
            _default = new DefaultTransformRunner();
        }
        return _default;
    }

    public abstract Object run(TransformRunContext var1, TransformDefinition var2, TransformServerInfo var3, GraphID var4, Map<EntityID, MaltegoEntity> var5, DataSource var6, EntityRegistry var7, EntityFactory var8, LinkFactory var9, ProgressContributor var10);

    public abstract void cancelAll();

    public abstract boolean cancel(Object var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

