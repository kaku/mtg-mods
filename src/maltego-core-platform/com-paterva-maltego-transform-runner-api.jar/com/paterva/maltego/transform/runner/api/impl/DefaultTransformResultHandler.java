/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphFreezable
 *  com.paterva.maltego.graph.GraphFreezableRegistry
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.graph.GraphFreezableRegistry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.runner.api.TransformResultHandler;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.TransformBatchResult;
import com.paterva.maltego.transform.runner.api.impl.TransformResultBatcher;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class DefaultTransformResultHandler
extends TransformResultHandler {
    @Override
    public void updateGraph(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformResult transformResult, Map<EntityID, MaltegoEntity> map) {
        GraphID graphID = transformResult.getGraphID();
        boolean bl = transformResult.transformComplete();
        if (graphID != null) {
            this.handleGraphUpdate(transformRunContext, transformDefinition, transformResult, map);
        } else if (transformRunContext.getCallback() != null) {
            this.notifyContext(transformRunContext, bl);
        }
    }

    protected void notifyContext(TransformRunContext transformRunContext, boolean bl) {
        Set<MaltegoEntity> set = Collections.emptySet();
        transformRunContext.onTransformResult(set, set, bl);
    }

    protected void handleGraphUpdate(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformResult transformResult, Map<EntityID, MaltegoEntity> map) {
        GraphID graphID = transformRunContext.getTargetGraphID();
        TransformResultBatcher transformResultBatcher = (TransformResultBatcher)GraphFreezableRegistry.getDefault().forGraph(graphID);
        TransformBatchResult transformBatchResult = new TransformBatchResult(transformRunContext, transformDefinition, transformResult, map);
        transformResultBatcher.addTransformBatchResult(transformBatchResult);
    }
}

