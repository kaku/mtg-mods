/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class TransformServerMap {
    private final Map<TransformDefinition, TransformServerInfo> _map;

    TransformServerMap(Map<TransformDefinition, TransformServerInfo> map) {
        this._map = map;
    }

    public Map<TransformDefinition, TransformServerInfo> getMap() {
        return Collections.unmodifiableMap(this._map);
    }

    public void removeTransforms(Set<TransformDefinition> set) {
        for (TransformDefinition transformDefinition : set) {
            this._map.remove((Object)transformDefinition);
        }
    }
}

