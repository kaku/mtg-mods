/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.openide.util.Exceptions;

public class TransformRunContext {
    private GraphID _targetGraphID;
    private Callback _callback;
    private boolean _popupErrors = false;
    private Map<TransformDefinition, TransformServerInfo> _transforms;

    public TransformRunContext(GraphID graphID, Map<TransformDefinition, TransformServerInfo> map, Callback callback) {
        this._targetGraphID = graphID;
        this._callback = callback;
        this._transforms = map;
    }

    public TransformRunContext(GraphID graphID, Map<TransformDefinition, TransformServerInfo> map) {
        this(graphID, map, null);
    }

    public GraphID getTargetGraphID() {
        return this._targetGraphID;
    }

    public Callback getCallback() {
        return this._callback;
    }

    public boolean isPopupErrors() {
        return this._popupErrors;
    }

    public void setPopupErrors(boolean bl) {
        this._popupErrors = bl;
    }

    public Map<TransformDefinition, TransformServerInfo> getTransforms() {
        return Collections.unmodifiableMap(this._transforms);
    }

    public void onTransformResult(Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2, boolean bl) {
        Callback callback = this.getCallback();
        if (callback != null) {
            try {
                callback.onTransformResult(this, collection, collection2, bl);
            }
            catch (Exception var5_5) {
                Exceptions.printStackTrace((Throwable)var5_5);
            }
        }
    }

    public void onTransformError(List<TransformMessage> list) {
        Callback callback = this.getCallback();
        if (callback != null) {
            try {
                callback.onTransformError(this, list);
            }
            catch (Exception var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
    }

    public static interface Callback {
        public void onTransformResult(TransformRunContext var1, Collection<MaltegoEntity> var2, Collection<MaltegoEntity> var3, boolean var4);

        public void onTransformError(TransformRunContext var1, List<TransformMessage> var2);
    }

}

