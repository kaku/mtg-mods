/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptorSet
 *  com.paterva.maltego.typing.PropertyProvider
 *  org.openide.util.lookup.Lookups
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.transform.runner.api.GlobalInputProvider;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorSet;
import com.paterva.maltego.typing.PropertyProvider;
import java.util.Collection;
import org.openide.util.lookup.Lookups;

public class DefaultGlobalInputProvider
extends GlobalInputProvider {
    private PropertyProvider _inputs;

    @Override
    public synchronized PropertyProvider getInputs() {
        if (this._inputs == null) {
            this._inputs = this.createInputs();
        }
        return this._inputs;
    }

    private PropertyProvider createInputs() {
        Collection collection = Lookups.forPath((String)"Maltego/GlobalTransformInputs").lookupAll(PropertyProvider.class);
        return new CompoundPropertyProvider(collection.toArray((T[])new PropertyProvider[collection.size()]));
    }

    private static class CompoundPropertyProvider
    implements PropertyProvider {
        private PropertyProvider[] _providers;
        private PropertyDescriptorSet _set;

        public /* varargs */ CompoundPropertyProvider(PropertyProvider ... arrpropertyProvider) {
            this._providers = arrpropertyProvider;
            this._set = new PropertyDescriptorSet();
            for (PropertyProvider propertyProvider : arrpropertyProvider) {
                this._set.addAll(propertyProvider.getProperties());
            }
        }

        public PropertyDescriptorCollection getProperties() {
            return this._set;
        }

        public Object getValue(PropertyDescriptor propertyDescriptor) {
            for (PropertyProvider propertyProvider : this._providers) {
                if (!propertyProvider.getProperties().contains(propertyDescriptor)) continue;
                return propertyProvider.getValue(propertyDescriptor);
            }
            return null;
        }

        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            for (PropertyProvider propertyProvider : this._providers) {
                if (!propertyProvider.getProperties().contains(propertyDescriptor)) continue;
                propertyProvider.setValue(propertyDescriptor, object);
            }
        }
    }

}

