/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformMessageHandler;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class TransformMessageHandler {
    public static TransformMessageHandler getDefault() {
        TransformMessageHandler transformMessageHandler = (TransformMessageHandler)Lookup.getDefault().lookup(TransformMessageHandler.class);
        if (transformMessageHandler == null) {
            transformMessageHandler = new DefaultTransformMessageHandler();
        }
        return transformMessageHandler;
    }

    public void handleWithEntities(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformMessage transformMessage, Map<EntityID, MaltegoEntity> map) {
        this.handleWithEntities(transformRunContext, transformDefinition, Collections.singletonList(transformMessage), map);
    }

    public abstract void handleWithEntities(TransformRunContext var1, TransformDefinition var2, List<TransformMessage> var3, Map<EntityID, MaltegoEntity> var4);

    public void handle(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformMessage transformMessage) {
        this.handle(transformRunContext, transformDefinition, Collections.singletonList(transformMessage));
    }

    public abstract void handle(TransformRunContext var1, TransformDefinition var2, List<TransformMessage> var3);
}

