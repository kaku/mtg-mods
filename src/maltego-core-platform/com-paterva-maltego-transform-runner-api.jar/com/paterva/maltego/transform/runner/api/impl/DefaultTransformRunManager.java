/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.graph.GraphFreezable
 *  com.paterva.maltego.graph.GraphFreezableRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.InheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.RegistryInheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.PropertyProvider
 *  org.netbeans.api.progress.aggregate.AggregateProgressFactory
 *  org.netbeans.api.progress.aggregate.AggregateProgressHandle
 *  org.netbeans.api.progress.aggregate.ProgressContributor
 *  org.netbeans.api.progress.aggregate.ProgressMonitor
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.graph.GraphFreezableRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.RegistryInheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.GlobalInputProvider;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.TransformRunManager;
import com.paterva.maltego.transform.runner.api.TransformRunner;
import com.paterva.maltego.transform.runner.api.impl.TransformResultBatcher;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.PropertyProvider;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.aggregate.AggregateProgressFactory;
import org.netbeans.api.progress.aggregate.AggregateProgressHandle;
import org.netbeans.api.progress.aggregate.ProgressContributor;
import org.netbeans.api.progress.aggregate.ProgressMonitor;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;

public class DefaultTransformRunManager
extends TransformRunManager
implements Cancellable,
ProgressMonitor {
    private static final Logger LOG = Logger.getLogger(DefaultTransformRunManager.class.getName());
    private AggregateProgressHandle _progressHandle = null;
    private Map<Object, Set<TransformRunInfo>> _handles;

    @Override
    public Object runTransforms(TransformRunContext transformRunContext, Map<TransformDefinition, TransformServerInfo> map, Map<TransformDefinition, DataSource> map2, GraphID graphID, Set<EntityID> set, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory) {
        Object object;
        if (!SwingUtilities.isEventDispatchThread()) {
            LOG.severe("runTransforms(...) may not be called outside the EDT");
        }
        TransformRunner transformRunner = TransformRunner.getDefault();
        ProgressContributor[] arrprogressContributor = new ProgressContributor[map.size()];
        for (int i = 0; i < map.size(); ++i) {
            arrprogressContributor[i] = AggregateProgressFactory.createProgressContributor((String)("Transform " + i));
            if (this._progressHandle == null) continue;
            this._progressHandle.addContributor(arrprogressContributor[i]);
        }
        if (this._progressHandle == null) {
            this._progressHandle = AggregateProgressFactory.createHandle((String)"Running Transforms", (ProgressContributor[])arrprogressContributor, (Cancellable)this, (Action)null);
            this._progressHandle.setMonitor((ProgressMonitor)this);
            this._progressHandle.start();
        }
        if (this.getHandles().isEmpty() && !map.isEmpty()) {
            this.onTransformsStarted(map, graphID, set);
        }
        try {
            object = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = object.getGraphStructureStore().getStructureReader();
            set = graphStructureReader.getExistingEntities(set);
        }
        catch (GraphStoreException var11_13) {
            Exceptions.printStackTrace((Throwable)var11_13);
        }
        object = GraphStoreHelper.getEntities((GraphID)graphID, set);
        int n = 0;
        Object object2 = new Object();
        HashSet<TransformRunInfo> hashSet = new HashSet<TransformRunInfo>();
        for (Map.Entry<TransformDefinition, TransformServerInfo> entry : map.entrySet()) {
            DataSource dataSource;
            ProgressContributor progressContributor;
            Map<EntityID, MaltegoEntity> map3;
            Object object3;
            TransformDefinition transformDefinition = entry.getKey();
            TransformServerInfo transformServerInfo = entry.getValue();
            DataSource dataSource2 = map2.get((Object)transformDefinition);
            if (dataSource2 == null) {
                dataSource2 = DataSources.empty();
            }
            if ((object3 = transformRunner.run(transformRunContext, transformDefinition, transformServerInfo, graphID, map3 = DefaultTransformRunManager.getEntityIDs(graphID, object, (TransformDescriptor)transformDefinition, entityRegistry), dataSource = DataSources.readOnlyProxy((DataSource[])new DataSource[]{dataSource2, GlobalInputProvider.getDefault().getInputs(), transformDefinition}), entityRegistry, entityFactory, linkFactory, progressContributor = arrprogressContributor[n++])) == null) continue;
            hashSet.add(new TransformRunInfo(graphID, progressContributor, object2, object3));
        }
        this.getHandles().put(object2, hashSet);
        return object2;
    }

    private Map<Object, Set<TransformRunInfo>> getHandles() {
        if (this._handles == null) {
            this._handles = new HashMap<Object, Set<TransformRunInfo>>();
        }
        return this._handles;
    }

    @Override
    public void cancel(Object object) {
        Map<Object, Set<TransformRunInfo>> map = this.getHandles();
        Set<TransformRunInfo> set = map.get(object);
        if (set != null) {
            for (TransformRunInfo transformRunInfo : set) {
                TransformRunner.getDefault().cancel(transformRunInfo.getTransformHandle());
                this.remove(transformRunInfo);
            }
        }
    }

    @Override
    public boolean isRunning(Object object) {
        Map<Object, Set<TransformRunInfo>> map = this.getHandles();
        return map.get(object) != null;
    }

    public boolean cancel() {
        TransformRunner.getDefault().cancelAll();
        this.fireGraphTransformsDone();
        this.onTransformsDone();
        this.getHandles().clear();
        return true;
    }

    public void started(ProgressContributor progressContributor) {
    }

    public void finished(ProgressContributor progressContributor) {
        TransformRunInfo transformRunInfo = this.getTransformRunInfo(progressContributor);
        if (transformRunInfo != null) {
            this.remove(transformRunInfo);
        }
    }

    public void progressed(ProgressContributor progressContributor) {
    }

    private TransformRunInfo getTransformRunInfo(ProgressContributor progressContributor) {
        for (Map.Entry<Object, Set<TransformRunInfo>> entry : this.getHandles().entrySet()) {
            for (TransformRunInfo transformRunInfo : entry.getValue()) {
                if (!transformRunInfo.getProgressContributor().equals((Object)progressContributor)) continue;
                return transformRunInfo;
            }
        }
        return null;
    }

    protected void onTransformsStarted(Map<TransformDefinition, TransformServerInfo> map, final GraphID graphID, Set<EntityID> set) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DefaultTransformRunManager.getResultBatcher(graphID).onTransformsStarted();
            }
        });
    }

    protected void onTransformsDone(final GraphID graphID) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DefaultTransformRunManager.getResultBatcher(graphID).onTransformsDone();
            }
        });
    }

    protected void onTransformsDone() {
        if (this._progressHandle != null) {
            this._progressHandle.finish();
            this._progressHandle = null;
        }
    }

    private void remove(TransformRunInfo transformRunInfo) {
        Object object = transformRunInfo.getGroupHandle();
        Map<Object, Set<TransformRunInfo>> map = this.getHandles();
        Set<TransformRunInfo> set = map.get(object);
        if (set != null) {
            set.remove(transformRunInfo);
            GraphID graphID = transformRunInfo.getGraphID();
            if (this.isGraphTransformsDone(graphID)) {
                this.onTransformsDone(graphID);
            }
            if (set.isEmpty()) {
                map.remove(object);
                if (map.isEmpty()) {
                    TransformRunner.getDefault().cancelAll();
                    this.onTransformsDone();
                }
            }
        }
    }

    private boolean isGraphTransformsDone(GraphID graphID) {
        Map<Object, Set<TransformRunInfo>> map = this.getHandles();
        for (Map.Entry<Object, Set<TransformRunInfo>> entry : map.entrySet()) {
            Set<TransformRunInfo> set = entry.getValue();
            for (TransformRunInfo transformRunInfo : set) {
                if (!graphID.equals((Object)transformRunInfo.getGraphID())) continue;
                return false;
            }
        }
        return true;
    }

    private void fireGraphTransformsDone() {
        HashSet<GraphID> hashSet = new HashSet<GraphID>();
        Map<Object, Set<TransformRunInfo>> map = this.getHandles();
        for (Map.Entry<Object, Set<TransformRunInfo>> graphID2 : map.entrySet()) {
            Set<TransformRunInfo> set = graphID2.getValue();
            for (TransformRunInfo transformRunInfo : set) {
                hashSet.add(transformRunInfo.getGraphID());
            }
        }
        for (GraphID graphID : hashSet) {
            this.onTransformsDone(graphID);
        }
    }

    private static TransformResultBatcher getResultBatcher(GraphID graphID) {
        return (TransformResultBatcher)GraphFreezableRegistry.getDefault().forGraph(graphID);
    }

    public static Map<EntityID, MaltegoEntity> getEntityIDs(GraphID graphID, Map<EntityID, MaltegoEntity> map, TransformDescriptor transformDescriptor, EntityRegistry entityRegistry) {
        RegistryInheritedTypesProvider registryInheritedTypesProvider = new RegistryInheritedTypesProvider(entityRegistry);
        Map map2 = transformDescriptor.getInputConstraint().getSatisfiedBy(graphID, map, (InheritedTypesProvider)registryInheritedTypesProvider);
        return map2;
    }

    private static class TransformRunInfo {
        private final GraphID _graphID;
        private final ProgressContributor _progressContributor;
        private final Object _groupHandle;
        private final Object _transformHandle;

        public TransformRunInfo(GraphID graphID, ProgressContributor progressContributor, Object object, Object object2) {
            this._graphID = graphID;
            this._progressContributor = progressContributor;
            this._groupHandle = object;
            this._transformHandle = object2;
        }

        public GraphID getGraphID() {
            return this._graphID;
        }

        public ProgressContributor getProgressContributor() {
            return this._progressContributor;
        }

        public Object getGroupHandle() {
            return this._groupHandle;
        }

        public Object getTransformHandle() {
            return this._transformHandle;
        }
    }

}

