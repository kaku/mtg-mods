/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import java.util.Collection;
import org.openide.util.Lookup;

public abstract class TransformRunRequestDecorator {
    public static Collection<? extends TransformRunRequestDecorator> getAll() {
        return Lookup.getDefault().lookupAll(TransformRunRequestDecorator.class);
    }

    public abstract String getDecorations(String var1);

    public static String decorate(String string, String string2) {
        StringBuilder stringBuilder = new StringBuilder(string);
        for (TransformRunRequestDecorator transformRunRequestDecorator : TransformRunRequestDecorator.getAll()) {
            stringBuilder.append(transformRunRequestDecorator.getDecorations(string2));
        }
        return stringBuilder.toString();
    }
}

