/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import java.util.Map;

public class TransformBatchResult {
    private final TransformRunContext _ctx;
    private final TransformDefinition _transform;
    private final TransformResult _txResult;
    private final Map<EntityID, MaltegoEntity> _inputEntities;

    public TransformBatchResult(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformResult transformResult, Map<EntityID, MaltegoEntity> map) {
        this._ctx = transformRunContext;
        this._transform = transformDefinition;
        this._txResult = transformResult;
        this._inputEntities = map;
    }

    public TransformRunContext getTransformContext() {
        return this._ctx;
    }

    public TransformDefinition getTransform() {
        return this._transform;
    }

    public TransformResult getTransformResult() {
        return this._txResult;
    }

    public Map<EntityID, MaltegoEntity> getInputEntities() {
        return this._inputEntities;
    }
}

