/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.typing.DataSource
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformRunManager;
import com.paterva.maltego.typing.DataSource;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformRunManager {
    public static TransformRunManager getDefault() {
        TransformRunManager transformRunManager = (TransformRunManager)Lookup.getDefault().lookup(TransformRunManager.class);
        if (transformRunManager == null) {
            transformRunManager = new DefaultTransformRunManager();
        }
        return transformRunManager;
    }

    public abstract Object runTransforms(TransformRunContext var1, Map<TransformDefinition, TransformServerInfo> var2, Map<TransformDefinition, DataSource> var3, GraphID var4, Set<EntityID> var5, EntityRegistry var6, EntityFactory var7, LinkFactory var8);

    public Object runTransform(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, DataSource dataSource, GraphID graphID, Set<EntityID> set, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory) {
        Map<TransformDefinition, TransformServerInfo> map = Collections.singletonMap(transformDefinition, transformServerInfo);
        Map<TransformDefinition, DataSource> map2 = Collections.singletonMap(transformDefinition, dataSource);
        return this.runTransforms(transformRunContext, map, map2, graphID, set, entityRegistry, entityFactory, linkFactory);
    }

    public abstract void cancel(Object var1);

    public abstract boolean isRunning(Object var1);
}

