/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformServerMap;
import com.paterva.maltego.transform.runner.api.TransformServerMapProvider;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformInputProvider {
    public static TransformInputProvider getDefault() {
        TransformInputProvider transformInputProvider = (TransformInputProvider)Lookup.getDefault().lookup(TransformInputProvider.class);
        if (transformInputProvider == null) {
            transformInputProvider = new TrivialTransformInputProvider();
        }
        return transformInputProvider;
    }

    public abstract Map<TransformDefinition, DataSource> getInputs(TransformServerMap var1);

    public DataSource getInputs(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo) {
        TransformServerMap transformServerMap = TransformServerMapProvider.getDefault().get(Collections.singleton(transformDefinition), transformServerInfo);
        Map<TransformDefinition, DataSource> map = this.getInputs(transformServerMap);
        if (map == null) {
            return null;
        }
        DataSource dataSource = null;
        if (map.size() == 1) {
            dataSource = map.entrySet().iterator().next().getValue();
        }
        if (dataSource == null) {
            dataSource = DataSources.empty();
        }
        return dataSource;
    }

    private static class TrivialTransformInputProvider
    extends TransformInputProvider {
        private TrivialTransformInputProvider() {
        }

        @Override
        public Map<TransformDefinition, DataSource> getInputs(TransformServerMap transformServerMap) {
            throw new UnsupportedOperationException("No TransformInputProvider registered.");
        }
    }

}

