/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCallback
 *  com.paterva.maltego.typing.DataSource
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.typing.DataSource;

public interface TransformRunnable
extends Runnable,
TransformCallback {
    public TransformDefinition getTransform();

    public DataSource getTransformInputs();

    public TransformServerInfo getServer();

    public String getLastMessage();

    public int getProgress();
}

