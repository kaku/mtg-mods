/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.util.Lookup;

public abstract class NodePostProcessor {
    public static Collection<? extends NodePostProcessor> getAll() {
        Collection collection = Lookup.getDefault().lookupAll(NodePostProcessor.class);
        if (collection == null) {
            return Collections.EMPTY_LIST;
        }
        return collection;
    }

    public abstract void processNewNodes(TransformRunContext var1, Collection<MaltegoEntity> var2, Collection<MaltegoEntity> var3);
}

