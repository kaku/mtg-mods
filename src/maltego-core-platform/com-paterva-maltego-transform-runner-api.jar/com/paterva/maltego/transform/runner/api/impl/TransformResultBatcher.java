/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.GraphFreezable
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.transform.runner.api.impl.TransformBatchResult;

public interface TransformResultBatcher
extends GraphFreezable {
    public void onTransformsStarted();

    public void onTransformsDone();

    public void addTransformBatchResult(TransformBatchResult var1);

    public void onGraphClosed();
}

