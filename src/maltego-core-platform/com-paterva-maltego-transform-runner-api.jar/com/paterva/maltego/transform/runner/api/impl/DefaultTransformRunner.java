/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.adapter.TransformAdapter
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCallback
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.util.FastURL
 *  org.netbeans.api.progress.aggregate.ProgressContributor
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.adapter.TransformAdapter;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.runner.api.TransformMessageHandler;
import com.paterva.maltego.transform.runner.api.TransformResultHandler;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.TransformRunnable;
import com.paterva.maltego.transform.runner.api.TransformRunner;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.util.FastURL;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.netbeans.api.progress.aggregate.ProgressContributor;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class DefaultTransformRunner
extends TransformRunner {
    private static final int THROUGHPUT = 5;
    private RequestProcessor _processor = null;
    private final PropertyChangeSupport _changeSupport;

    public DefaultTransformRunner() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    @Override
    public Object run(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, GraphID graphID, Map<EntityID, MaltegoEntity> map, DataSource dataSource, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory, ProgressContributor progressContributor) {
        String string = this.getTarget(transformServerInfo);
        DefaultTransformRunnable defaultTransformRunnable = new DefaultTransformRunnable(transformRunContext, transformDefinition, transformServerInfo, graphID, map, dataSource, entityRegistry, entityFactory, linkFactory, progressContributor);
        RequestProcessor.Task task = this.getProcessor().create((Runnable)defaultTransformRunnable);
        this.getProcessor().post((Runnable)task);
        defaultTransformRunnable.setCancelDelegate((Cancellable)task);
        return defaultTransformRunnable;
    }

    private String getTarget(TransformServerInfo transformServerInfo) {
        FastURL fastURL;
        String string = null;
        if (transformServerInfo != null && (fastURL = transformServerInfo.getUrl()) != null) {
            string = fastURL.toString();
        }
        return string;
    }

    @Override
    public void cancelAll() {
        if (this._processor != null) {
            this._processor.stop();
            this._processor = null;
        }
    }

    @Override
    public boolean cancel(Object object) {
        if (object instanceof DefaultTransformRunnable) {
            DefaultTransformRunnable defaultTransformRunnable = (DefaultTransformRunnable)object;
            defaultTransformRunnable.cancelled();
            return true;
        }
        return false;
    }

    private RequestProcessor getProcessor() {
        if (this._processor == null) {
            this._processor = new RequestProcessor("Transform Runner", 5, true);
        }
        return this._processor;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void fireTransformStarted(TransformRunnable transformRunnable) {
        this._changeSupport.firePropertyChange("transform.started", null, transformRunnable);
    }

    private void fireTransformProgress(TransformRunnable transformRunnable) {
        this._changeSupport.firePropertyChange("transform.progress", null, transformRunnable);
    }

    private void fireTransformDone(TransformRunnable transformRunnable) {
        this._changeSupport.firePropertyChange("transform.done", null, transformRunnable);
    }

    private class DefaultTransformRunnable
    implements TransformRunnable {
        private final TransformDefinition _transform;
        private final GraphID _graphID;
        private final Map<EntityID, MaltegoEntity> _inputEntities;
        private DataSource _transformInputs;
        private Cancellable _cancelDelegate;
        private final TransformRunContext _ctx;
        private int _lastProgress;
        private String _lastMessage;
        private final TransformServerInfo _server;
        private TransformAdapter _adapter;
        private boolean _cancelled;
        private final ProgressContributor _progressContributor;
        private final EntityFactory _entityFactory;
        private final EntityRegistry _entityRegistry;
        private final LinkFactory _linkFactory;

        public DefaultTransformRunnable(TransformRunContext transformRunContext, TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, GraphID graphID, Map<EntityID, MaltegoEntity> map, DataSource dataSource, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory, ProgressContributor progressContributor) {
            this._lastProgress = -1;
            this._ctx = transformRunContext;
            this._server = transformServerInfo;
            this._transform = transformDefinition;
            this._graphID = graphID;
            this._inputEntities = map;
            this._transformInputs = dataSource;
            this._cancelled = false;
            this._progressContributor = progressContributor;
            this._entityRegistry = entityRegistry;
            this._entityFactory = entityFactory;
            this._linkFactory = linkFactory;
        }

        public synchronized void resultReceived(TransformResult transformResult, Map<EntityID, MaltegoEntity> map) {
            if (this._cancelled) {
                return;
            }
            String string = transformResult.getLastProgressMessage();
            if (transformResult.getLastProgress() > this._lastProgress || Utilities.compareObjects((Object)this._lastMessage, (Object)string) && string != null) {
                this._lastProgress = transformResult.getLastProgress();
                this._lastMessage = string;
                if (this._lastProgress < 0) {
                    this._progressContributor.progress(string);
                } else {
                    this._progressContributor.progress(string, this._lastProgress);
                }
                DefaultTransformRunner.this.fireTransformProgress(this);
            }
            if (transformResult.getMessages() != null) {
                TransformMessageHandler.getDefault().handleWithEntities(this._ctx, this._transform, transformResult.getMessages(), map);
            }
            if (transformResult.transformComplete() || transformResult.getGraphID() != null) {
                TransformResultHandler.getDefault().updateGraph(this._ctx, this._transform, transformResult, this._inputEntities);
            }
        }

        public synchronized void cancelled() {
            try {
                if (this._cancelDelegate != null) {
                    this._cancelDelegate.cancel();
                }
            }
            catch (Exception var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
            finally {
                TransformMessageHandler.getDefault().handle(this._ctx, this._transform, new TransformMessage(TransformMessage.Severity.Warning, "Transform " + this._transform.getDisplayName() + " stopped"));
                this._cancelled = true;
                this._progressContributor.finish();
                DefaultTransformRunner.this.fireTransformDone(this);
                if (this._adapter != null) {
                    this._adapter.cancel();
                }
                this._ctx.onTransformResult(null, null, true);
            }
        }

        @Override
        public void run() {
            this._progressContributor.start(100);
            DefaultTransformRunner.this.fireTransformStarted(this);
            try {
                this._adapter = this.createTransformAdapter();
                this._adapter.run(this._graphID, this._inputEntities, (TransformDescriptor)this._transform, DefaultTransformRunner.this.getTarget(this._server), this._transformInputs, this._entityRegistry, this._entityFactory, this._linkFactory, (TransformCallback)this);
            }
            catch (Exception var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
                this.resultReceived(TransformResult.error((Exception)var1_1), this._inputEntities);
            }
            finally {
                this._progressContributor.finish();
                DefaultTransformRunner.this.fireTransformDone(this);
            }
        }

        private TransformAdapter createTransformAdapter() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
            String string = this._transform.getTransformAdapterClass();
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            Class class_ = classLoader.loadClass(string);
            if (!TransformAdapter.class.isAssignableFrom(class_)) {
                throw new InstantiationException("Class " + string + " does not implement interface " + TransformAdapter.class.getName());
            }
            return (TransformAdapter)class_.newInstance();
        }

        @Override
        public TransformDefinition getTransform() {
            return this._transform;
        }

        @Override
        public DataSource getTransformInputs() {
            return this._transformInputs;
        }

        public void setTransformInputs(DataSource dataSource) {
            this._transformInputs = dataSource;
        }

        public Cancellable getCancelDelegate() {
            return this._cancelDelegate;
        }

        public void setCancelDelegate(Cancellable cancellable) {
            this._cancelDelegate = cancellable;
        }

        @Override
        public TransformServerInfo getServer() {
            return this._server;
        }

        @Override
        public int getProgress() {
            return this._lastProgress;
        }

        @Override
        public String getLastMessage() {
            return this._lastMessage;
        }
    }

}

