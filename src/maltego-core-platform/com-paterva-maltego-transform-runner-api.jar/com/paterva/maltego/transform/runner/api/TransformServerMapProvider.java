/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformServerMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class TransformServerMapProvider {
    private static TransformServerMapProvider _default;

    public static synchronized TransformServerMapProvider getDefault() {
        if (_default == null && (TransformServerMapProvider._default = (TransformServerMapProvider)Lookup.getDefault().lookup(TransformServerMapProvider.class)) == null) {
            _default = new TrivialTransformServerMapProvider();
        }
        return _default;
    }

    public abstract TransformServerMap get(Collection<? extends TransformDefinition> var1, TransformServerInfo var2);

    protected TransformServerMap createMap(Map<TransformDefinition, TransformServerInfo> map) {
        return new TransformServerMap(map);
    }

    private static class TrivialTransformServerMapProvider
    extends TransformServerMapProvider {
        private TrivialTransformServerMapProvider() {
        }

        @Override
        public TransformServerMap get(Collection<? extends TransformDefinition> collection, TransformServerInfo transformServerInfo) {
            HashMap<TransformDefinition, TransformServerInfo> hashMap = new HashMap<TransformDefinition, TransformServerInfo>();
            for (TransformDefinition transformDefinition : collection) {
                hashMap.put(transformDefinition, transformServerInfo);
            }
            return this.createMap(hashMap);
        }
    }

}

