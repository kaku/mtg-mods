/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import org.openide.util.Lookup;

public abstract class TransformServerSelector {
    public static TransformServerSelector getDefault() {
        TransformServerSelector transformServerSelector = (TransformServerSelector)Lookup.getDefault().lookup(TransformServerSelector.class);
        if (transformServerSelector == null) {
            transformServerSelector = new TrivialTransformServerSelector();
        }
        return transformServerSelector;
    }

    public abstract TransformServerInfo selectServer(TransformDefinition var1);

    private static class TrivialTransformServerSelector
    extends TransformServerSelector {
        private TrivialTransformServerSelector() {
        }

        @Override
        public TransformServerInfo selectServer(TransformDefinition transformDefinition) {
            throw new UnsupportedOperationException("No Server Selector registered");
        }
    }

}

