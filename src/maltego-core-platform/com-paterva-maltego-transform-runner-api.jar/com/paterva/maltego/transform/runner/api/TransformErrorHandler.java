/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 */
package com.paterva.maltego.transform.runner.api;

import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.api.TransformRunContext;

public interface TransformErrorHandler {
    public boolean handle(TransformRunContext var1, TransformDefinition var2, TransformMessage var3);
}

