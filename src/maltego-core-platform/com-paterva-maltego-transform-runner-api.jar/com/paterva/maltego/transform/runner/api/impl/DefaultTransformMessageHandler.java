/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.api.TransformErrorHelper;
import com.paterva.maltego.transform.runner.api.TransformMessageHandler;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class DefaultTransformMessageHandler
extends TransformMessageHandler {
    @Override
    public void handle(TransformRunContext transformRunContext, TransformDefinition transformDefinition, List<TransformMessage> list) {
        this.handleWithEntities(transformRunContext, transformDefinition, list, null);
    }

    protected void handleOther(TransformRunContext transformRunContext, TransformDefinition transformDefinition, List<TransformMessage> list) {
        this.handleOtherWithEntities(transformRunContext, transformDefinition, list, null);
    }

    @Override
    public void handleWithEntities(TransformRunContext transformRunContext, TransformDefinition transformDefinition, List<TransformMessage> list, Map<EntityID, MaltegoEntity> map) {
        list = this.handleSpecialErrors(transformRunContext, transformDefinition, list);
        if (map == null) {
            this.handleOther(transformRunContext, transformDefinition, list);
        } else {
            this.handleOtherWithEntities(transformRunContext, transformDefinition, list, map);
        }
    }

    protected void handleOtherWithEntities(TransformRunContext transformRunContext, TransformDefinition transformDefinition, List<TransformMessage> list, Map<EntityID, MaltegoEntity> map) {
        boolean bl = false;
        for (TransformMessage transformMessage : list) {
            if (map == null) {
                this.display(transformRunContext, transformMessage);
            } else {
                this.displayWithEntities(transformRunContext, transformMessage, map);
            }
            if (transformMessage.getSeverity() != TransformMessage.Severity.Error) continue;
            bl = true;
        }
        if (bl) {
            if (transformRunContext.isPopupErrors()) {
                this.popupErrors(transformDefinition, list);
            }
            transformRunContext.onTransformError(list);
        }
    }

    protected void display(TransformRunContext transformRunContext, TransformMessage transformMessage) {
        System.out.println("Transform message: " + transformMessage.getSeverity().toString().toUpperCase() + " " + transformMessage.getText());
    }

    protected void displayWithEntities(TransformRunContext transformRunContext, TransformMessage transformMessage, Map<EntityID, MaltegoEntity> map) {
        this.display(transformRunContext, transformMessage);
    }

    private List<TransformMessage> handleSpecialErrors(TransformRunContext transformRunContext, TransformDefinition transformDefinition, List<TransformMessage> list) {
        ArrayList<TransformMessage> arrayList = new ArrayList<TransformMessage>(list);
        for (TransformMessage transformMessage : list) {
            if (transformMessage.getSeverity() != TransformMessage.Severity.Error || transformMessage.getCode() == 0 || !TransformErrorHelper.handle(transformRunContext, transformDefinition, transformMessage)) continue;
            arrayList.remove((Object)transformMessage);
        }
        return arrayList;
    }

    protected void popupErrors(TransformDefinition transformDefinition, List<TransformMessage> list) {
    }
}

