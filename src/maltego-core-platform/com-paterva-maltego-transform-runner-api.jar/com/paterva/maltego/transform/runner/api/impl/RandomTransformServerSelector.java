/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 */
package com.paterva.maltego.transform.runner.api.impl;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.runner.api.TransformServerSelector;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class RandomTransformServerSelector
extends TransformServerSelector {
    private Random _random = new Random();

    @Override
    public TransformServerInfo selectServer(TransformDefinition transformDefinition) {
        Set set = TransformServerRegistry.getDefault().findServers(transformDefinition.getName(), true);
        if (set.isEmpty()) {
            return null;
        }
        if (set.size() == 1) {
            return (TransformServerInfo)set.iterator().next();
        }
        int n = this._random.nextInt(set.size());
        return RandomTransformServerSelector.getByIndex(set, n);
    }

    private static TransformServerInfo getByIndex(Collection<TransformServerInfo> collection, int n) {
        Iterator<TransformServerInfo> iterator = collection.iterator();
        TransformServerInfo transformServerInfo = null;
        for (int i = 0; i <= n; ++i) {
            transformServerInfo = iterator.next();
        }
        return transformServerInfo;
    }
}

