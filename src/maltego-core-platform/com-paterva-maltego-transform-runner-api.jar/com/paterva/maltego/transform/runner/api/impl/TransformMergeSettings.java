/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.transform.runner.api.impl;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class TransformMergeSettings {
    private static final String MERGE_LINKS = "mergeLinksFromSameTransform";

    public static boolean isMergeLinks() {
        Preferences preferences = NbPreferences.forModule(TransformMergeSettings.class);
        return preferences.getBoolean("mergeLinksFromSameTransform", true);
    }

    public static void setMergeLinks(boolean bl) {
        Preferences preferences = NbPreferences.forModule(TransformMergeSettings.class);
        preferences.putBoolean("mergeLinksFromSameTransform", bl);
    }
}

