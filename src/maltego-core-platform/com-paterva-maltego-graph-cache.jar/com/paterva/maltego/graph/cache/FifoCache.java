/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.cache;

import com.paterva.maltego.graph.cache.CachedItems;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FifoCache<K, V> {
    private static final Logger LOG = Logger.getLogger(FifoCache.class.getName());
    private final String _name;
    private final int _capacity;
    private final Map<K, V> _cachedItems = new HashMap();
    private final Queue<K> _keyQueue = new LinkedList<K>();
    private int _puts = 0;
    private int _new = 0;
    private int _hits = 0;
    private int _misses = 0;
    private int _trimmed = 0;
    private int _removes = 0;
    private int _clears = 0;
    private Timer _logTimer = null;

    public FifoCache(String string, int n) {
        this._name = string;
        this._capacity = n;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "{0} capacity: {1}", new Object[]{this._name, n});
        }
    }

    public int getCapacity() {
        return this._capacity;
    }

    public synchronized void put(K k, V v) {
        int n = this._cachedItems.size();
        this._cachedItems.put(k, v);
        ++this._puts;
        if (n != this._cachedItems.size()) {
            ++this._new;
            this._keyQueue.add(k);
            if (this._keyQueue.size() > this._capacity) {
                K k2 = this._keyQueue.remove();
                this._cachedItems.remove(k2);
                ++this._trimmed;
            }
            this.scheduleLogStats();
        }
    }

    public synchronized V get(K k) {
        V v = this._cachedItems.get(k);
        if (v != null || this._cachedItems.containsKey(k)) {
            ++this._hits;
        } else {
            ++this._misses;
        }
        this.scheduleLogStats();
        return v;
    }

    public synchronized V remove(K k) {
        V v = null;
        if (this._cachedItems.containsKey(k)) {
            v = this._cachedItems.remove(k);
            this._keyQueue.remove(k);
            ++this._removes;
        }
        return v;
    }

    public synchronized void remove(Collection<K> collection) {
        int n = this._keyQueue.size();
        this._cachedItems.keySet().removeAll(collection);
        this._keyQueue.removeAll(collection);
        this._removes += n - this._keyQueue.size();
    }

    public synchronized CachedItems<K, V> get(Collection<K> collection) {
        HashMap<K, V> hashMap = new HashMap<K, V>();
        HashSet<K> hashSet = new HashSet<K>();
        for (K k : collection) {
            if (this._cachedItems.containsKey(k)) {
                V v = this._cachedItems.get(k);
                hashMap.put(k, v);
                ++this._hits;
                continue;
            }
            hashSet.add(k);
            ++this._misses;
        }
        this.scheduleLogStats();
        return new CachedItems(hashMap, hashSet);
    }

    public synchronized void clear() {
        this._cachedItems.clear();
        this._keyQueue.clear();
        ++this._clears;
    }

    private synchronized void scheduleLogStats() {
        if (LOG.isLoggable(Level.FINE) && this._logTimer == null) {
            this._logTimer = new Timer("FiFo Log");
            this._logTimer.schedule(new TimerTask(){

                @Override
                public void run() {
                    FifoCache.this.logStats();
                }
            }, 1000);
        }
    }

    private synchronized void logStats() {
        LOG.log(Level.FINE, "{0} {1} puts:{2} new:{3} hits:{4} misses:{5} trimmed:{6} removes:{7} clears:{8} size:{9}", new Object[]{new Date(), this._name, this._puts, this._new, this._hits, this._misses, this._trimmed, this._removes, this._clears, this._keyQueue.size()});
        this._puts = 0;
        this._new = 0;
        this._hits = 0;
        this._misses = 0;
        this._trimmed = 0;
        this._removes = 0;
        this._clears = 0;
        this._logTimer = null;
    }

}

