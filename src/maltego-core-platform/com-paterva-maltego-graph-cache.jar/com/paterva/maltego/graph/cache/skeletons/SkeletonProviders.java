/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.cache.skeletons;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.LinkSkeletonProvider;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import org.openide.util.Exceptions;

public class SkeletonProviders {
    public static synchronized EntitySkeletonProvider entitiesForGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        EntitySkeletonProvider entitySkeletonProvider = (EntitySkeletonProvider)graphUserData.get((Object)(string = EntitySkeletonProvider.class.getName()));
        if (entitySkeletonProvider == null) {
            try {
                entitySkeletonProvider = new EntitySkeletonProvider(graphID);
                graphUserData.put((Object)string, (Object)entitySkeletonProvider);
            }
            catch (GraphStoreException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
        }
        return entitySkeletonProvider;
    }

    public static synchronized LinkSkeletonProvider linksForGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        LinkSkeletonProvider linkSkeletonProvider = (LinkSkeletonProvider)graphUserData.get((Object)(string = LinkSkeletonProvider.class.getName()));
        if (linkSkeletonProvider == null) {
            try {
                linkSkeletonProvider = new LinkSkeletonProvider(graphID);
                graphUserData.put((Object)string, (Object)linkSkeletonProvider);
            }
            catch (GraphStoreException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
        }
        return linkSkeletonProvider;
    }
}

