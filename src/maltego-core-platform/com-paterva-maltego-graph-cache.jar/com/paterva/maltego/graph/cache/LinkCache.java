/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.cache;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.cache.PartCache;

public class LinkCache
extends PartCache<LinkID, MaltegoLink> {
    public LinkCache(String string, int n) {
        super(string, n);
    }
}

