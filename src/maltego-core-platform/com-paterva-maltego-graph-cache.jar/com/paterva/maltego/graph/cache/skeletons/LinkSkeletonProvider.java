/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.LinkDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 */
package com.paterva.maltego.graph.cache.skeletons;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.cache.CachedItems;
import com.paterva.maltego.graph.cache.skeletons.LinkSkeletonCache;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class LinkSkeletonProvider {
    private final GraphID _graphID;
    private final GraphDataStoreReader _reader;
    private final LinkSkeletonCache _cache;
    private final GraphDataStore _graphDataStore;
    private LinkDataQuery _skeletonQuery;
    private PropertyChangeListener _dataStoreListener;

    public LinkSkeletonProvider(GraphID graphID) throws GraphStoreException {
        this._graphID = graphID;
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        this._graphDataStore = graphStore.getGraphDataStore();
        this._reader = this._graphDataStore.getDataStoreReader();
        this._cache = new LinkSkeletonCache();
        this._dataStoreListener = new DataStoreListener();
        this._graphDataStore.addPropertyChangeListener(this._dataStoreListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)new GraphCloseListener());
    }

    public Map<LinkID, MaltegoLink> getLinkSkeletons(Collection<LinkID> collection) throws GraphStoreException {
        CachedItems cachedItems = this._cache.get(collection);
        Set<LinkID> set = cachedItems.getMissedKeys();
        Map<LinkID, MaltegoLink> map = cachedItems.getItems();
        if (!set.isEmpty()) {
            LinkDataQuery linkDataQuery = this.getLinkSkeletonQuery();
            LinksDataQuery linksDataQuery = new LinksDataQuery();
            linksDataQuery.setIDs(set);
            linksDataQuery.setPartDataQuery((PartDataQuery)linkDataQuery);
            Map map2 = this._reader.getLinks(linksDataQuery);
            this._cache.put(map2.values());
            map.putAll(map2);
        }
        return map;
    }

    public MaltegoLink getLinkSkeleton(LinkID linkID) throws GraphStoreException {
        MaltegoLink maltegoLink = (MaltegoLink)this._cache.get(linkID);
        if (maltegoLink == null) {
            LinkDataQuery linkDataQuery = this.getLinkSkeletonQuery();
            maltegoLink = this._reader.getLink(linkID, linkDataQuery);
            this._cache.put(linkID, maltegoLink);
        }
        return maltegoLink;
    }

    public void putSkeleton(MaltegoLink maltegoLink) {
        MaltegoLink maltegoLink2 = maltegoLink.createClone();
        this._cache.put(maltegoLink2.getID(), maltegoLink2);
    }

    private synchronized LinkDataQuery getLinkSkeletonQuery() {
        if (this._skeletonQuery == null) {
            this._skeletonQuery = new LinkDataQuery();
            this._skeletonQuery.setAllSections(true);
            this._skeletonQuery.setAllProperties(true);
        }
        return this._skeletonQuery;
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private GraphCloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("graphClosed".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)LinkSkeletonProvider.this._graphID)) {
                GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                LinkSkeletonProvider.this._graphDataStore.removePropertyChangeListener(LinkSkeletonProvider.this._dataStoreListener);
                LinkSkeletonProvider.this._dataStoreListener = null;
            }
        }
    }

    private class DataStoreListener
    implements PropertyChangeListener {
        private DataStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("partsChanged".equals(propertyChangeEvent.getPropertyName())) {
                GraphDataMods graphDataMods = (GraphDataMods)propertyChangeEvent.getNewValue();
                LinkSkeletonProvider.this._cache.remove(graphDataMods.getLinksUpdated().keySet());
            }
        }
    }

}

