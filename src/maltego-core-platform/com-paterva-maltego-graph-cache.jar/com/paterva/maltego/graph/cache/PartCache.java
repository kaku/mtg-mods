/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 */
package com.paterva.maltego.graph.cache;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.cache.FifoCache;
import java.util.Collection;

public abstract class PartCache<ID extends Guid, Part extends MaltegoPart<ID>>
extends FifoCache<ID, Part> {
    public PartCache(String string, int n) {
        super(string, n);
    }

    public synchronized void put(Collection<Part> collection) {
        for (MaltegoPart maltegoPart : collection) {
            this.put(maltegoPart.getID(), maltegoPart);
        }
    }
}

