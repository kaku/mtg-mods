/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.graph.cache;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.PartCache;

public class EntityCache
extends PartCache<EntityID, MaltegoEntity> {
    public EntityCache(String string, int n) {
        super(string, n);
    }
}

