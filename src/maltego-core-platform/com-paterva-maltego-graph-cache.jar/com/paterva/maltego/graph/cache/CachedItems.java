/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.cache;

import java.util.Map;
import java.util.Set;

public class CachedItems<K, V> {
    private final Map<K, V> _items;
    private final Set<K> _missedKeys;

    public CachedItems(Map<K, V> map, Set<K> set) {
        this._items = map;
        this._missedKeys = set;
    }

    public Map<K, V> getItems() {
        return this._items;
    }

    public Set<K> getMissedKeys() {
        return this._missedKeys;
    }
}

