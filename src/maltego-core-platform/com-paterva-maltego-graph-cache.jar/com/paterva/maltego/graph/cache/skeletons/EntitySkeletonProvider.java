/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntitySectionsQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 */
package com.paterva.maltego.graph.cache.skeletons;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonCache;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EntitySkeletonProvider {
    private static final Logger LOG = Logger.getLogger(EntitySkeletonProvider.class.getName());
    private final GraphID _graphID;
    private final GraphDataStoreReader _reader;
    private final EntitySkeletonCache _cache;
    private final GraphDataStore _graphDataStore;
    private EntityDataQuery _skeletonQuery;
    private PropertyChangeListener _dataStoreListener;

    public EntitySkeletonProvider(GraphID graphID) throws GraphStoreException {
        this._graphID = graphID;
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        this._graphDataStore = graphStore.getGraphDataStore();
        this._reader = this._graphDataStore.getDataStoreReader();
        this._cache = new EntitySkeletonCache();
        this._dataStoreListener = new DataStoreListener();
        this._graphDataStore.addPropertyChangeListener(this._dataStoreListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)new GraphCloseListener());
    }

    public MaltegoEntity getEntitySkeleton(EntityID entityID) throws GraphStoreException {
        MaltegoEntity maltegoEntity = (MaltegoEntity)this._cache.get(entityID);
        if (maltegoEntity == null) {
            EntityDataQuery entityDataQuery = this.getEntitySkeletonQuery();
            maltegoEntity = this._reader.getEntity(entityID, entityDataQuery);
            this._cache.put(entityID, maltegoEntity);
        }
        return maltegoEntity;
    }

    public Map<EntityID, MaltegoEntity> getEntitySkeletons(Set<EntityID> set) throws GraphStoreException {
        Object object;
        LOG.log(Level.FINE, "IDs: {0}", set);
        HashMap<EntityID, MaltegoEntity> hashMap = new HashMap<EntityID, MaltegoEntity>(set.size());
        HashSet<EntityID> hashSet = new HashSet<EntityID>(set.size());
        hashSet.addAll(set);
        for (EntityID object22 : set) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this._cache.get(object22);
            if (maltegoEntity == null) continue;
            hashMap.put(object22, maltegoEntity);
            hashSet.remove((Object)object22);
        }
        if (!hashSet.isEmpty()) {
            LOG.log(Level.FINE, "IDs: {0}", hashSet);
            object = new EntitiesDataQuery();
            object.setIDs(hashSet);
            object.setPartDataQuery((PartDataQuery)this.getEntitySkeletonQuery());
            Map map = this._reader.getEntities((EntitiesDataQuery)object);
            LOG.log(Level.FINE, "Skeletons: {0}", map);
            hashMap.putAll(map);
            this._cache.put(map.values());
        }
        LOG.log(Level.FINE, "Skeletons: {0}\n\n", hashMap);
        if (set.size() != hashMap.size()) {
            object = new HashSet<EntityID>(set);
            object.removeAll(hashMap.keySet());
            throw new IllegalStateException("Skeletons not found for: " + object);
        }
        return hashMap;
    }

    public void putSkeleton(MaltegoEntity maltegoEntity) {
        MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
        HashSet<String> hashSet = new HashSet<String>();
        for (PropertyDescriptor object2 : maltegoEntity2.getProperties()) {
            hashSet.add(object2.getName());
        }
        for (String string : hashSet) {
            maltegoEntity2.removeProperty(string);
        }
        maltegoEntity2.setDisplayInformation(null);
        this._cache.put(maltegoEntity2.getID(), maltegoEntity2);
    }

    public void removeEntitySkeleton(EntityID entityID) {
        this._cache.remove(entityID);
    }

    private synchronized EntityDataQuery getEntitySkeletonQuery() {
        if (this._skeletonQuery == null) {
            this._skeletonQuery = new EntityDataQuery();
            EntitySectionsQuery entitySectionsQuery = new EntitySectionsQuery();
            entitySectionsQuery.setQueryType(true);
            entitySectionsQuery.setQueryCachedValueStr(true);
            entitySectionsQuery.setQueryCachedDisplayStr(true);
            entitySectionsQuery.setQueryCachedImageValue(true);
            entitySectionsQuery.setQueryCachedHasAttachments(true);
            entitySectionsQuery.setQueryBookmark(true);
            entitySectionsQuery.setQueryNotes(true);
            entitySectionsQuery.setQueryWeight(true);
            this._skeletonQuery.setSections((PartSectionsQuery)entitySectionsQuery);
        }
        return this._skeletonQuery;
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private GraphCloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)EntitySkeletonProvider.this._graphID)) {
                GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                EntitySkeletonProvider.this._graphDataStore.removePropertyChangeListener(EntitySkeletonProvider.this._dataStoreListener);
                EntitySkeletonProvider.this._dataStoreListener = null;
            }
        }
    }

    private class DataStoreListener
    implements PropertyChangeListener {
        private DataStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("partsChanged".equals(propertyChangeEvent.getPropertyName())) {
                GraphDataMods graphDataMods = (GraphDataMods)propertyChangeEvent.getNewValue();
                EntitySkeletonProvider.this._cache.remove(graphDataMods.getEntitiesUpdated().keySet());
            }
        }
    }

}

