/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.cache.skeletons;

import com.paterva.maltego.graph.cache.LinkCache;

public class LinkSkeletonCache
extends LinkCache {
    private static final int CAPACITY = 10000;

    public LinkSkeletonCache() {
        super("Link Skeletons", 10000);
    }
}

