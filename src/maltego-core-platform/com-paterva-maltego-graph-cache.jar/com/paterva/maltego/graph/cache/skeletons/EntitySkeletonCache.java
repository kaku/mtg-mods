/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.cache.skeletons;

import com.paterva.maltego.graph.cache.EntityCache;

public class EntitySkeletonCache
extends EntityCache {
    public static final int CAPACITY = 10000;

    public EntitySkeletonCache() {
        super("Entity Skeletons", 10000);
    }
}

