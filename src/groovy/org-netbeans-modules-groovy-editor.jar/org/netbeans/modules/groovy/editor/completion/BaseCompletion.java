/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.openide.filesystems.FileObject;

public abstract class BaseCompletion {
    protected static final Logger LOG = Logger.getLogger(BaseCompletion.class.getName());

    public abstract boolean complete(List<CompletionProposal> var1, CompletionContext var2, int var3);

    protected final ClasspathInfo getClasspathInfoFromRequest(CompletionContext request) {
        FileObject fileObject = request.getSourceFile();
        if (fileObject != null) {
            return ClasspathInfo.create((FileObject)fileObject);
        }
        return null;
    }

    protected final boolean isValidPackage(ClasspathInfo pathInfo, String pkg) {
        assert (pathInfo != null);
        Set pkgSet = pathInfo.getClassIndex().getPackageNames(pkg, true, EnumSet.allOf(ClassIndex.SearchScope.class));
        if (pkgSet.size() > 0) {
            LOG.log(Level.FINEST, "Packages with prefix : {0}", pkg);
            LOG.log(Level.FINEST, "               found : {0}", pkgSet);
            for (String singlePkg : pkgSet) {
                if (!singlePkg.equals(pkg)) continue;
                LOG.log(Level.FINEST, "Exact match found.");
                return true;
            }
            return false;
        }
        return false;
    }

    protected boolean isPrefixed(CompletionContext request, String name) {
        return name.toUpperCase(Locale.ENGLISH).startsWith(request.getPrefix().toUpperCase(Locale.ENGLISH));
    }

    protected boolean isPrefixedAndNotEqual(CompletionContext request, String name) {
        return this.isPrefixed(request, name) && !name.equals(request.getPrefix());
    }

    protected final PackageCompletionRequest getPackageRequest(CompletionContext request) {
        String pkgString;
        Token t;
        Token t2;
        int position = request.lexOffset;
        PackageCompletionRequest result = new PackageCompletionRequest();
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)request.doc, position);
        ts.move(position);
        Token token = null;
        boolean remainingTokens = true;
        while (ts.isValid() && (remainingTokens = ts.movePrevious()) && ts.offset() >= 0 && ((t2 = ts.token()).id() == GroovyTokenId.DOT || t2.id() == GroovyTokenId.IDENTIFIER || "keyword".equals(((GroovyTokenId)t2.id()).primaryCategory()))) {
            token = t2;
        }
        StringBuilder sb = new StringBuilder();
        Token lastToken = null;
        if (!remainingTokens && token != null && ts.isValid()) {
            sb.append(token.text().toString());
            lastToken = token;
        }
        while (ts.isValid() && ts.moveNext() && ts.offset() < position && ((t = ts.token()).id() == GroovyTokenId.DOT || t.id() == GroovyTokenId.IDENTIFIER)) {
            sb.append(t.text().toString());
            lastToken = t;
        }
        result.fullString = sb.toString();
        if (sb.length() == 0) {
            result.basePackage = "";
            result.prefix = "";
            if (token != null && "keyword".equals(((GroovyTokenId)token.id()).primaryCategory())) {
                result.prefix = request.getPrefix();
            }
        } else if (lastToken != null && lastToken.id() == GroovyTokenId.DOT) {
            pkgString = sb.toString();
            result.basePackage = pkgString.substring(0, pkgString.length() - 1);
            result.prefix = "";
        } else if (lastToken != null && lastToken.id() == GroovyTokenId.IDENTIFIER) {
            pkgString = sb.toString();
            result.prefix = lastToken.text().toString();
            result.basePackage = pkgString.substring(0, pkgString.length() - result.prefix.length());
            if (result.basePackage.endsWith(".")) {
                result.basePackage = result.basePackage.substring(0, result.basePackage.length() - 1);
            }
        }
        LOG.log(Level.FINEST, "-- fullString : >{0}<", result.fullString);
        LOG.log(Level.FINEST, "-- basePackage: >{0}<", result.basePackage);
        LOG.log(Level.FINEST, "-- prefix:      >{0}<", result.prefix);
        return result;
    }

    protected class PackageCompletionRequest {
        String fullString;
        String basePackage;
        String prefix;

        protected PackageCompletionRequest() {
            this.fullString = "";
            this.basePackage = "";
            this.prefix = "";
        }
    }

}

