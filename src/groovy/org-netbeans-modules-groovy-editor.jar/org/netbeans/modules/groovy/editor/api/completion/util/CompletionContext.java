/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.ListExpression
 *  org.codehaus.groovy.ast.expr.NamedArgumentListExpression
 *  org.codehaus.groovy.ast.expr.RangeExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.completion.util;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.ListExpression;
import org.codehaus.groovy.ast.expr.NamedArgumentListExpression;
import org.codehaus.groovy.ast.expr.RangeExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.ContextHelper;
import org.netbeans.modules.groovy.editor.api.completion.util.DotCompletionContext;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.completion.AccessLevel;
import org.netbeans.modules.groovy.editor.completion.inference.GroovyTypeAnalyzer;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;

public final class CompletionContext {
    private final ParserResult parserResult;
    private final FileObject sourceFile;
    private String typeName;
    private String prefix;
    private int anchor;
    private boolean nameOnly;
    public final int lexOffset;
    public final int astOffset;
    public final BaseDocument doc;
    public boolean scriptMode;
    public CaretLocation location;
    public CompletionSurrounding context;
    public AstPath path;
    public ClassNode declaringClass;
    public DotCompletionContext dotContext;
    public Set<AccessLevel> access;

    public CompletionContext(ParserResult parseResult, String prefix, int anchor, int lexOffset, int astOffset, BaseDocument doc) {
        this.parserResult = parseResult;
        this.sourceFile = parseResult.getSnapshot().getSource().getFileObject();
        this.prefix = prefix;
        this.anchor = anchor;
        this.lexOffset = lexOffset;
        this.astOffset = astOffset;
        this.doc = doc;
        this.path = this.getPathFromRequest();
        this.location = this.getCaretLocationFromRequest();
        this.context = this.getCompletionContext();
        this.dotContext = this.getDotCompletionContext();
        this.nameOnly = this.dotContext != null && this.dotContext.isMethodsOnly();
        this.declaringClass = this.getBeforeDotDeclaringClass();
    }

    public void init() {
        this.access = this.declaringClass != null ? AccessLevel.create(ContextHelper.getSurroundingClassNode(this), this.declaringClass) : null;
    }

    public ParserResult getParserResult() {
        return this.parserResult;
    }

    public FileObject getSourceFile() {
        return this.sourceFile;
    }

    public ClassNode getSurroundingClass() {
        return ContextHelper.getSurroundingClassNode(this);
    }

    public String getTypeName() {
        return this.typeName;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public int getAnchor() {
        return this.anchor;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setAnchor(int anchor) {
        this.anchor = anchor;
    }

    public boolean isBehindDot() {
        return this.dotContext != null;
    }

    public boolean isNameOnly() {
        return this.nameOnly;
    }

    private AstPath getPathFromRequest() {
        ModuleNode root = ASTUtils.getRoot(this.parserResult);
        if (root == null) {
            return null;
        }
        return new AstPath((ASTNode)root, this.astOffset, this.doc);
    }

    private AstPath getPath(ParserResult parseResult, BaseDocument doc, int astOffset) {
        ModuleNode root = ASTUtils.getRoot(parseResult);
        if (root == null) {
            return null;
        }
        return new AstPath((ASTNode)root, astOffset, doc);
    }

    private CaretLocation getCaretLocationFromRequest() {
        ASTNode node;
        Token t;
        int position = this.lexOffset;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)this.doc, position);
        ts.move(position);
        if (ts.isValid() && ts.moveNext() && ts.offset() < this.doc.getLength()) {
            Token tparent;
            t = ts.token();
            if (t.id() == GroovyTokenId.LINE_COMMENT || t.id() == GroovyTokenId.BLOCK_COMMENT) {
                return CaretLocation.INSIDE_COMMENT;
            }
            if (t.id() == GroovyTokenId.STRING_LITERAL) {
                return CaretLocation.INSIDE_STRING;
            }
            if (t.id() == GroovyTokenId.NLS && ts.isValid() && ts.movePrevious() && ts.offset() >= 0 && (tparent = ts.token()).id() == GroovyTokenId.LINE_COMMENT) {
                return CaretLocation.INSIDE_COMMENT;
            }
        }
        ts.move(position);
        while (ts.isValid() && ts.moveNext() && ts.offset() < this.doc.getLength()) {
            t = ts.token();
            if (t.id() != GroovyTokenId.LITERAL_package) continue;
            return CaretLocation.ABOVE_PACKAGE;
        }
        boolean classDefBeforePosition = false;
        ts.move(position);
        while (ts.isValid() && ts.movePrevious() && ts.offset() >= 0) {
            Token t2 = ts.token();
            if (t2.id() != GroovyTokenId.LITERAL_class && t2.id() != GroovyTokenId.LITERAL_interface) continue;
            classDefBeforePosition = true;
            break;
        }
        boolean classDefAfterPosition = false;
        ts.move(position);
        while (ts.isValid() && ts.moveNext() && ts.offset() < this.doc.getLength()) {
            Token t3 = ts.token();
            if (t3.id() != GroovyTokenId.LITERAL_class && t3.id() != GroovyTokenId.LITERAL_interface) continue;
            classDefAfterPosition = true;
            break;
        }
        if (this.path != null && (node = this.path.root()) instanceof ModuleNode) {
            ModuleNode module = (ModuleNode)node;
            String name = null;
            for (ClassNode clazz2 : module.getClasses()) {
                if (!clazz2.isScript()) continue;
                name = clazz2.getName();
                this.scriptMode = true;
                break;
            }
            if (name != null) {
                for (ClassNode clazz2 : module.getClasses()) {
                    if (clazz2.isScript() || !name.equals(clazz2.getName())) continue;
                    this.scriptMode = false;
                    break;
                }
            }
        }
        if (!this.scriptMode && !classDefBeforePosition && classDefAfterPosition) {
            return CaretLocation.ABOVE_FIRST_CLASS;
        }
        if (!classDefBeforePosition && this.scriptMode) {
            return CaretLocation.INSIDE_METHOD;
        }
        if (this.path == null) {
            return null;
        }
        for (ASTNode current : this.path) {
            if (current instanceof ClosureExpression) {
                return CaretLocation.INSIDE_CLOSURE;
            }
            if (current instanceof FieldNode) {
                FieldNode fn = (FieldNode)current;
                if (!fn.isClosureSharedVariable()) continue;
                return CaretLocation.INSIDE_CLOSURE;
            }
            if (current instanceof MethodNode) {
                return CaretLocation.INSIDE_METHOD;
            }
            if (current instanceof ClassNode) {
                return CaretLocation.INSIDE_CLASS;
            }
            if (current instanceof ModuleNode) {
                return CaretLocation.OUTSIDE_CLASSES;
            }
            if (current instanceof Parameter) {
                return CaretLocation.INSIDE_PARAMETERS;
            }
            if (!(current instanceof ConstructorCallExpression) && !(current instanceof NamedArgumentListExpression)) continue;
            ts.move(position);
            boolean afterLeftParen = false;
            block14 : while (ts.isValid() && ts.movePrevious() && ts.offset() >= 0) {
                Token t4 = ts.token();
                switch ((GroovyTokenId)t4.id()) {
                    case LPAREN: {
                        afterLeftParen = true;
                        break block14;
                    }
                    case LITERAL_new: {
                        break block14;
                    }
                    default: {
                        continue block14;
                    }
                }
            }
            ts.move(position);
            boolean beforeRightParen = false;
            block15 : while (ts.isValid() && ts.moveNext() && ts.offset() >= 0) {
                Token t5 = ts.token();
                switch ((GroovyTokenId)t5.id()) {
                    case RPAREN: {
                        beforeRightParen = true;
                        break block15;
                    }
                    case SEMI: {
                        break block15;
                    }
                    default: {
                        continue block15;
                    }
                }
            }
            if (!afterLeftParen || !beforeRightParen) continue;
            return CaretLocation.INSIDE_CONSTRUCTOR_CALL;
        }
        return CaretLocation.UNDEFINED;
    }

    private CompletionSurrounding getCompletionContext() {
        Token t;
        int position = this.lexOffset;
        Token beforeLiteral = null;
        Token before2 = null;
        Token before1 = null;
        Token active = null;
        Token after1 = null;
        Token after2 = null;
        Token afterLiteral = null;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)this.doc, position);
        int difference = ts.move(position);
        if (ts.isValid() && ts.moveNext() && ts.offset() >= 0) {
            active = ts.token();
        }
        if (active != null) {
            if (active.id() == GroovyTokenId.WHITESPACE && difference == 0) {
                ts.movePrevious();
            } else if (active.id() == GroovyTokenId.NLS) {
                ts.movePrevious();
                if (ts.token().id() == GroovyTokenId.AT || ts.token().id() == GroovyTokenId.DOT || ts.token().id() == GroovyTokenId.SPREAD_DOT || ts.token().id() == GroovyTokenId.OPTIONAL_DOT || ts.token().id() == GroovyTokenId.MEMBER_POINTER || ts.token().id() == GroovyTokenId.ELVIS_OPERATOR) {
                    ts.moveNext();
                }
            }
        }
        int stopAt = 0;
        while (ts.isValid() && ts.movePrevious() && ts.offset() >= 0 && (t = ts.token()).id() != GroovyTokenId.NLS) {
            if (t.id() == GroovyTokenId.WHITESPACE) continue;
            if (stopAt == 0) {
                before1 = t;
            } else if (stopAt == 1) {
                before2 = t;
            } else if (stopAt == 2) break;
            ++stopAt;
        }
        ts.move(position);
        while (ts.isValid() && ts.movePrevious() && ts.offset() >= 0 && (t = ts.token()).id() != GroovyTokenId.NLS && t.id() != GroovyTokenId.LBRACE) {
            if (!((GroovyTokenId)t.id()).primaryCategory().equals("keyword")) continue;
            beforeLiteral = t;
            break;
        }
        ts.move(position);
        while (ts.isValid() && ts.moveNext() && ts.offset() < this.doc.getLength() && (t = ts.token()).id() != GroovyTokenId.NLS && t.id() != GroovyTokenId.RBRACE) {
            if (!((GroovyTokenId)t.id()).primaryCategory().equals("keyword")) continue;
            afterLiteral = t;
            break;
        }
        ts.move(position);
        stopAt = 0;
        while (ts.isValid() && ts.moveNext() && ts.offset() < this.doc.getLength() && (t = ts.token()).id() != GroovyTokenId.NLS) {
            if (t.id() == GroovyTokenId.WHITESPACE) continue;
            if (stopAt == 0) {
                after1 = t;
            } else if (stopAt == 1) {
                after2 = t;
            } else if (stopAt == 2) break;
            ++stopAt;
        }
        return new CompletionSurrounding(beforeLiteral, before2, before1, active, after1, after2, afterLiteral, ts);
    }

    private DotCompletionContext getDotCompletionContext() {
        GroovyTokenId tokenID;
        Token t;
        if (this.dotContext != null) {
            return this.dotContext;
        }
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)this.doc, this.lexOffset);
        ts.move(this.lexOffset);
        Token active = null;
        if (ts.isValid() && ts.moveNext() && ts.offset() >= 0) {
            active = ts.token();
        }
        if (ts.isValid() && ts.movePrevious() && ts.offset() >= 0 && (tokenID = (GroovyTokenId)ts.token().id()) != GroovyTokenId.AT && tokenID != GroovyTokenId.DOT && tokenID != GroovyTokenId.NLS && tokenID != GroovyTokenId.WHITESPACE && tokenID != GroovyTokenId.SPREAD_DOT && tokenID != GroovyTokenId.OPTIONAL_DOT && tokenID != GroovyTokenId.MEMBER_POINTER && tokenID != GroovyTokenId.ELVIS_OPERATOR) {
            if (tokenID != GroovyTokenId.IDENTIFIER && !tokenID.primaryCategory().equals("keyword")) {
                return null;
            }
            ts.movePrevious();
        }
        boolean fieldsOnly = false;
        if (ts.token().id() == GroovyTokenId.AT) {
            ts.movePrevious();
            fieldsOnly = true;
        }
        boolean remainingTokens = true;
        if (ts.token().id() != GroovyTokenId.DOT && ts.token().id() != GroovyTokenId.SPREAD_DOT && ts.token().id() != GroovyTokenId.OPTIONAL_DOT && ts.token().id() != GroovyTokenId.MEMBER_POINTER && ts.token().id() != GroovyTokenId.ELVIS_OPERATOR) {
            Token t2;
            while (ts.isValid() && (remainingTokens = ts.movePrevious()) && ts.offset() >= 0 && ((t2 = ts.token()).id() == GroovyTokenId.WHITESPACE || t2.id() == GroovyTokenId.NLS)) {
            }
        }
        if (ts.token().id() != GroovyTokenId.DOT && ts.token().id() != GroovyTokenId.SPREAD_DOT && ts.token().id() != GroovyTokenId.OPTIONAL_DOT && ts.token().id() != GroovyTokenId.MEMBER_POINTER && ts.token().id() != GroovyTokenId.ELVIS_OPERATOR || !remainingTokens) {
            return null;
        }
        boolean methodsOnly = false;
        if (ts.token().id() == GroovyTokenId.MEMBER_POINTER) {
            methodsOnly = true;
        }
        while (ts.isValid() && ts.movePrevious() && ts.offset() >= 0 && ((t = ts.token()).id() == GroovyTokenId.WHITESPACE || t.id() == GroovyTokenId.NLS)) {
        }
        int lexOffset = ts.offset();
        int astOffset = ASTUtils.getAstOffset((Parser.Result)this.parserResult, lexOffset);
        AstPath realPath = this.getPath(this.parserResult, this.doc, astOffset);
        return new DotCompletionContext(lexOffset, astOffset, realPath, fieldsOnly, methodsOnly);
    }

    private ClassNode getBeforeDotDeclaringClass() {
        Expression expression;
        ListExpression listExpression;
        Iterator i$;
        if (this.declaringClass != null && this.declaringClass instanceof ClassNode) {
            return this.declaringClass;
        }
        if ((this.isAfterSpreadOperator() || this.isAfterSpreadJavaFieldOperator()) && this.dotContext.getAstPath().leaf() instanceof Expression && (expression = (Expression)this.dotContext.getAstPath().leaf()) instanceof ListExpression && (i$ = (listExpression = (ListExpression)expression).getExpressions().iterator()).hasNext()) {
            Expression expr = (Expression)i$.next();
            return expr.getType();
        }
        DotCompletionContext dotCompletionContext = this.getDotCompletionContext();
        if (!(this.isBehindDot() || this.context.before1 != null || this.location != CaretLocation.INSIDE_CLOSURE && this.location != CaretLocation.INSIDE_METHOD)) {
            this.declaringClass = ContextHelper.getSurroundingClassNode(this);
            return this.declaringClass;
        }
        if (dotCompletionContext == null || dotCompletionContext.getAstPath() == null || dotCompletionContext.getAstPath().leaf() == null) {
            return null;
        }
        Object declClass = null;
        GroovyTypeAnalyzer typeAnalyzer = new GroovyTypeAnalyzer(this.doc);
        Set<ClassNode> infered = typeAnalyzer.getTypes(dotCompletionContext.getAstPath(), dotCompletionContext.getAstOffset());
        if (!infered.isEmpty()) {
            return infered.iterator().next();
        }
        if (declClass != null) {
            this.declaringClass = declClass;
            return this.declaringClass;
        }
        if (dotCompletionContext.getAstPath().leaf() instanceof VariableExpression) {
            VariableExpression variable = (VariableExpression)dotCompletionContext.getAstPath().leaf();
            if ("this".equals(variable.getName())) {
                this.declaringClass = ContextHelper.getSurroundingClassNode(this);
                return this.declaringClass;
            }
            if ("super".equals(variable.getName())) {
                ClassNode thisClass = ContextHelper.getSurroundingClassNode(this);
                this.declaringClass = thisClass.getSuperClass();
                if (this.declaringClass == null) {
                    return new ClassNode("java.lang.Object", 1, null);
                }
                return this.declaringClass;
            }
        }
        if (dotCompletionContext.getAstPath().leaf() instanceof Expression) {
            ConstantExpression constantExpression;
            Expression expression2 = (Expression)dotCompletionContext.getAstPath().leaf();
            if (expression2 instanceof RangeExpression && "java.lang.Object".equals(expression2.getType().getName())) {
                try {
                    expression2.setType(new ClassNode(Class.forName("groovy.lang.Range")));
                }
                catch (ClassNotFoundException ex) {
                    expression2.setType(new ClassNode("groovy.lang.Range", 513, null));
                }
            } else if (expression2 instanceof ConstantExpression && !(constantExpression = (ConstantExpression)expression2).isNullExpression()) {
                constantExpression.setType(new ClassNode(constantExpression.getValue().getClass()));
            }
            this.declaringClass = expression2.getType();
        }
        return this.declaringClass;
    }

    private boolean isAfterSpreadJavaFieldOperator() {
        if (this.context.before1 != null && this.context.before2 != null && ((GroovyTokenId)this.context.before1.id()).equals((Object)GroovyTokenId.AT) && ((GroovyTokenId)this.context.before2.id()).equals((Object)GroovyTokenId.SPREAD_DOT)) {
            return true;
        }
        return false;
    }

    private boolean isAfterSpreadOperator() {
        if (this.context.before1 != null && ((GroovyTokenId)this.context.before1.id()).equals((Object)GroovyTokenId.SPREAD_DOT)) {
            return true;
        }
        return false;
    }

    public boolean isBehindImportStatement() {
        int rowStart = 0;
        int nonWhite = 0;
        try {
            rowStart = Utilities.getRowStart((BaseDocument)this.doc, (int)this.lexOffset);
            nonWhite = Utilities.getFirstNonWhiteFwd((BaseDocument)this.doc, (int)rowStart);
        }
        catch (BadLocationException ex) {
            // empty catch block
        }
        Token<GroovyTokenId> importToken = LexUtilities.getToken(this.doc, nonWhite);
        if (importToken != null && importToken.id() == GroovyTokenId.LITERAL_import) {
            return true;
        }
        return false;
    }

}

