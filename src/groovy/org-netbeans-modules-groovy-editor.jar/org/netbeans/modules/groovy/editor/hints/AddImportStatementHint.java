/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintFix
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.hints;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;
import org.netbeans.modules.groovy.editor.hints.Bundle;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovyErrorRule;
import org.netbeans.modules.groovy.editor.hints.utils.HintUtils;
import org.netbeans.modules.groovy.editor.imports.ImportCandidate;
import org.netbeans.modules.groovy.editor.imports.ImportHelper;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class AddImportStatementHint
extends GroovyErrorRule {
    public static final Logger LOG = Logger.getLogger(AddImportStatementHint.class.getName());

    @Override
    public Set<CompilerErrorID> getCodes() {
        return EnumSet.of(CompilerErrorID.CLASS_NOT_FOUND);
    }

    @Override
    public void run(RuleContext context, GroovyError error, List<Hint> result) {
        LOG.log(Level.FINEST, "run()");
        String desc = error.getDescription();
        if (desc == null) {
            LOG.log(Level.FINEST, "desc == null");
            return;
        }
        LOG.log(Level.FINEST, "Processing : {0}", desc);
        String missingClassName = ImportHelper.getMissingClassName(desc);
        if (missingClassName == null) {
            return;
        }
        FileObject fo = context.parserResult.getSnapshot().getSource().getFileObject();
        Set<ImportCandidate> importCandidates = ImportHelper.getImportCandidate(fo, this.getPackageName(context), missingClassName);
        if (importCandidates.isEmpty()) {
            return;
        }
        int DEFAULT_PRIORITY = 292;
        OffsetRange range = HintUtils.getLineOffset(context, error);
        if (range != null) {
            for (ImportCandidate candidate : importCandidates) {
                ArrayList<AddImportFix> fixList = new ArrayList<AddImportFix>(1);
                String fqn = candidate.getFqnName();
                AddImportFix fixToApply = new AddImportFix(fo, fqn);
                fixList.add(fixToApply);
                Hint descriptor = new Hint((Rule)this, fixToApply.getDescription(), fo, range, fixList, DEFAULT_PRIORITY);
                result.add(descriptor);
            }
        }
    }

    private String getPackageName(RuleContext context) {
        ModuleNode module = ASTUtils.getRoot(context.parserResult);
        if (module != null) {
            return module.getPackageName();
        }
        return "";
    }

    public boolean appliesTo(RuleContext context) {
        return true;
    }

    public String getDisplayName() {
        return Bundle.FixImportsHintDescription();
    }

    public boolean showInTasklist() {
        return false;
    }

    public HintSeverity getDefaultSeverity() {
        return HintSeverity.ERROR;
    }

    private static class AddImportFix
    implements HintFix {
        private final FileObject fo;
        private final String fqn;

        public AddImportFix(FileObject fo, String fqn) {
            this.fo = fo;
            this.fqn = fqn;
        }

        public String getDescription() {
            return Bundle.ClassNotFoundRuleHintDescription(this.fqn);
        }

        public void implement() throws Exception {
            ImportHelper.addImportStatement(this.fo, this.fqn);
        }

        public boolean isSafe() {
            return true;
        }

        public boolean isInteractive() {
            return false;
        }
    }

}

