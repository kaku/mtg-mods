/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.control.ErrorCollector
 *  org.codehaus.groovy.control.SourceUnit
 *  org.codehaus.groovy.control.messages.SimpleMessage
 *  org.codehaus.groovy.control.messages.SyntaxErrorMessage
 *  org.codehaus.groovy.syntax.SyntaxException
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Severity
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.Task
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api.parser;

import java.util.Collection;
import java.util.EventObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.control.ErrorCollector;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.control.messages.SimpleMessage;
import org.codehaus.groovy.control.messages.SyntaxErrorMessage;
import org.codehaus.groovy.syntax.SyntaxException;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorResolver;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class GroovyParser
extends Parser {
    private static final Logger LOG = Logger.getLogger(GroovyParser.class.getName());
    private static final AtomicLong PARSING_TIME = new AtomicLong(0);
    private static final AtomicInteger PARSING_COUNT = new AtomicInteger(0);
    private static long maximumParsingTime;
    private GroovyParserResult lastResult;
    private AtomicBoolean cancelled = new AtomicBoolean();

    public void addChangeListener(ChangeListener changeListener) {
    }

    public void removeChangeListener(ChangeListener changeListener) {
    }

    public boolean isCancelled() {
        return this.cancelled.get();
    }

    public void cancel() {
        LOG.log(Level.FINEST, "Parser cancelled");
        this.cancelled.set(true);
    }

    public Parser.Result getResult(Task task) throws ParseException {
        assert (this.lastResult != null);
        return this.lastResult;
    }

    public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) throws ParseException {
        this.cancelled.set(false);
        Context context = new Context(snapshot, event);
        final HashSet errors = new HashSet();
        context.errorHandler = new ParseErrorHandler(){

            @Override
            public void error(Error error) {
                errors.add(error);
            }
        };
        this.lastResult = this.parseBuffer(context, Sanitize.NONE);
        if (this.lastResult != null) {
            this.lastResult.setErrors(errors);
        } else {
            this.lastResult = this.createParseResult(snapshot, null, null);
        }
    }

    protected GroovyParserResult createParseResult(Snapshot snapshot, ModuleNode rootNode, ErrorCollector errorCollector) {
        GroovyParserResult parserResult = new GroovyParserResult(this, snapshot, rootNode, errorCollector);
        return parserResult;
    }

    private boolean sanitizeSource(Context context, Sanitize sanitizing) {
        if (sanitizing == Sanitize.MISSING_END) {
            context.sanitizedSource = context.source + "}";
            int start = context.source.length();
            context.sanitizedRange = new OffsetRange(start, start + 1);
            context.sanitizedContents = "";
            return true;
        }
        int offset = context.caretOffset;
        if (sanitizing == Sanitize.ERROR_DOT || sanitizing == Sanitize.ERROR_LINE) {
            offset = context.errorOffset;
        }
        if (offset == -1) {
            return false;
        }
        String doc = context.source;
        if (offset > doc.length()) {
            return false;
        }
        try {
            if ((GroovyUtils.isRowEmpty(doc, offset) || GroovyUtils.isRowWhite(doc, offset)) && (offset = GroovyUtils.getRowStart(doc, offset) - 1) < 0) {
                offset = 0;
            }
            if (!GroovyUtils.isRowEmpty(doc, offset) && !GroovyUtils.isRowWhite(doc, offset)) {
                if (sanitizing == Sanitize.EDITED_LINE || sanitizing == Sanitize.ERROR_LINE) {
                    int lineEnd;
                    if (sanitizing == Sanitize.ERROR_LINE) {
                        TokenSequence<GroovyTokenId> ts;
                        Token<GroovyTokenId> token;
                        TokenSequence<GroovyTokenId> tokenSequence = ts = context.document != null ? LexUtilities.getPositionedSequence(context.document, offset) : null;
                        if (ts != null && (token = LexUtilities.findPreviousNonWsNonComment(ts)).id() == GroovyTokenId.DOT) {
                            int removeStart = ts.offset();
                            int removeEnd = removeStart + 1;
                            StringBuilder sb = new StringBuilder(doc.length());
                            sb.append(doc.substring(0, removeStart));
                            sb.append(' ');
                            if (removeEnd < doc.length()) {
                                sb.append(doc.substring(removeEnd, doc.length()));
                            }
                            assert (sb.length() == doc.length());
                            context.sanitizedRange = new OffsetRange(removeStart, removeEnd);
                            context.sanitizedSource = sb.toString();
                            context.sanitizedContents = doc.substring(removeStart, removeEnd);
                            return true;
                        }
                    }
                    if ((lineEnd = GroovyUtils.getRowLastNonWhite(doc, offset)) != -1) {
                        StringBuilder sb = new StringBuilder(doc.length());
                        int lineStart = GroovyUtils.getRowStart(doc, offset);
                        if (++lineEnd >= lineStart + 2) {
                            sb.append(doc.substring(0, lineStart));
                            sb.append("//");
                            int rest = lineStart + 2;
                            if (rest < doc.length()) {
                                sb.append(doc.substring(rest, doc.length()));
                            }
                        } else {
                            sb.append(doc.substring(0, lineStart));
                            sb.append(" ");
                            int rest = lineStart + 1;
                            if (rest < doc.length()) {
                                sb.append(doc.substring(rest, doc.length()));
                            }
                        }
                        assert (sb.length() == doc.length());
                        context.sanitizedRange = new OffsetRange(lineStart, lineEnd);
                        context.sanitizedSource = sb.toString();
                        context.sanitizedContents = doc.substring(lineStart, lineEnd);
                        return true;
                    }
                } else {
                    int lineEnd;
                    assert (sanitizing == Sanitize.ERROR_DOT || sanitizing == Sanitize.EDITED_DOT);
                    int lineStart = GroovyUtils.getRowStart(doc, offset);
                    for (lineEnd = offset - 1; lineEnd >= lineStart && lineEnd < doc.length() && Character.isWhitespace(doc.charAt(lineEnd)); --lineEnd) {
                    }
                    if (lineEnd > lineStart) {
                        StringBuilder sb = new StringBuilder(doc.length());
                        String line = doc.substring(lineStart, lineEnd + 1);
                        int removeChars = 0;
                        int removeEnd = lineEnd + 1;
                        if (line.endsWith(GroovyTokenId.SPREAD_DOT.fixedText() + GroovyTokenId.AT.fixedText())) {
                            removeChars = 3;
                        } else if (line.endsWith(GroovyTokenId.OPTIONAL_DOT.fixedText()) || line.endsWith(GroovyTokenId.ELVIS_OPERATOR.fixedText()) || line.endsWith(GroovyTokenId.MEMBER_POINTER.fixedText()) || line.endsWith(GroovyTokenId.SPREAD_DOT.fixedText()) || line.endsWith(GroovyTokenId.DOT.fixedText() + GroovyTokenId.AT.fixedText())) {
                            removeChars = 2;
                        } else if (line.endsWith(".") || line.endsWith("(")) {
                            removeChars = 1;
                        } else if (line.endsWith(",")) {
                            removeChars = 1;
                        } else if (line.endsWith(", ")) {
                            removeChars = 2;
                        } else if (line.endsWith(",)")) {
                            removeChars = 1;
                            --removeEnd;
                        } else if (line.endsWith(", )")) {
                            removeChars = 1;
                            removeEnd -= 2;
                        }
                        if (removeChars == 0) {
                            return false;
                        }
                        int removeStart = removeEnd - removeChars;
                        sb.append(doc.substring(0, removeStart));
                        for (int i = 0; i < removeChars; ++i) {
                            sb.append(' ');
                        }
                        if (removeEnd < doc.length()) {
                            sb.append(doc.substring(removeEnd, doc.length()));
                        }
                        assert (sb.length() == doc.length());
                        context.sanitizedRange = new OffsetRange(removeStart, removeEnd);
                        context.sanitizedSource = sb.toString();
                        context.sanitizedContents = doc.substring(removeStart, removeEnd);
                        return true;
                    }
                }
            }
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
        return false;
    }

    private GroovyParserResult sanitize(Context context, Sanitize sanitizing) {
        switch (sanitizing) {
            case NEVER: {
                return this.createParseResult(context.snapshot, null, null);
            }
            case NONE: {
                if (context.caretOffset != -1) {
                    return this.parseBuffer(context, Sanitize.EDITED_DOT);
                }
            }
            case EDITED_DOT: {
                if (context.errorOffset != -1 && context.errorOffset != context.caretOffset) {
                    return this.parseBuffer(context, Sanitize.ERROR_DOT);
                }
            }
            case ERROR_DOT: {
                if (context.errorOffset != -1) {
                    return this.parseBuffer(context, Sanitize.ERROR_LINE);
                }
            }
            case ERROR_LINE: {
                if (context.caretOffset != -1) {
                    return this.parseBuffer(context, Sanitize.EDITED_LINE);
                }
            }
            case EDITED_LINE: {
                return this.parseBuffer(context, Sanitize.MISSING_END);
            }
        }
        return this.createParseResult(context.snapshot, null, null);
    }

    /*
     * Exception decompiling
     */
    GroovyParserResult parseBuffer(Context context, Sanitize sanitizing) {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [0[TRYBLOCK]], but top level block is 4[CATCHBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
        // org.benf.cfr.reader.Main.doJar(Main.java:134)
        // org.benf.cfr.reader.Main.main(Main.java:189)
        throw new IllegalStateException("Decompilation failed");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void logParsingTime(Context context, long start, boolean cancelled) {
        long diff = System.currentTimeMillis() - start;
        long full = PARSING_TIME.addAndGet(diff);
        if (cancelled) {
            LOG.log(Level.FINEST, "Compilation cancelled in {0} for file {3}; total time spent {1}; total count {2}", new Object[]{diff, full, PARSING_COUNT.intValue(), context.snapshot.getSource().getFileObject()});
        } else {
            LOG.log(Level.FINEST, "Compilation finished in {0} for file {3}; total time spent {1}; total count {2}", new Object[]{diff, full, PARSING_COUNT.intValue(), context.snapshot.getSource().getFileObject()});
        }
        Class<GroovyParser> class_ = GroovyParser.class;
        synchronized (GroovyParser.class) {
            if (diff > maximumParsingTime) {
                maximumParsingTime = diff;
                LOG.log(Level.FINEST, "Maximum parsing time has been updated to {0}; file {1}", new Object[]{diff, context.snapshot.getSource().getFileObject()});
            }
            // ** MonitorExit[var8_5] (shouldn't be in output)
            return;
        }
    }

    private static String asString(CharSequence sequence) {
        if (sequence instanceof String) {
            return (String)sequence;
        }
        return sequence.toString();
    }

    private static void notifyError(Context context, String key, Severity severity, String description, String details, int offset, Sanitize sanitizing) {
        GroovyParser.notifyError(context, key, severity, description, details, offset, offset, sanitizing);
    }

    private static void notifyError(Context context, String key, Severity severity, String description, String displayName, int startOffset, int endOffset, Sanitize sanitizing) {
        LOG.log(Level.FINEST, "---------------------------------------------------");
        LOG.log(Level.FINEST, "key         : {0}\n", key);
        LOG.log(Level.FINEST, "description : {0}\n", description);
        LOG.log(Level.FINEST, "displayName : {0}\n", displayName);
        LOG.log(Level.FINEST, "startOffset : {0}\n", startOffset);
        LOG.log(Level.FINEST, "endOffset   : {0}\n", endOffset);
        if (description == null) {
            LOG.log(Level.FINEST, "dropping error");
            return;
        }
        if (key == null) {
            key = description;
        }
        if (displayName == null) {
            displayName = description;
        }
        GroovyError error = new GroovyError(key, displayName, description, context.snapshot.getSource().getFileObject(), startOffset, endOffset, severity, CompilerErrorResolver.getId(description));
        context.errorHandler.error((Error)error);
        if (sanitizing == Sanitize.NONE) {
            context.errorOffset = startOffset;
        }
    }

    private void handleErrorCollector(ErrorCollector errorCollector, Context context, ModuleNode moduleNode, boolean ignoreErrors, Sanitize sanitizing) {
        List errors;
        if (!ignoreErrors && errorCollector != null && (errors = errorCollector.getErrors()) != null) {
            for (Object object : errors) {
                LOG.log(Level.FINEST, "Error found in collector: {0}", object);
                if (object instanceof SyntaxErrorMessage) {
                    SyntaxException ex = ((SyntaxErrorMessage)object).getCause();
                    String sourceLocator = ex.getSourceLocator();
                    String name = null;
                    if (moduleNode != null) {
                        name = moduleNode.getContext().getName();
                    } else if (context.snapshot.getSource().getFileObject() != null) {
                        name = context.snapshot.getSource().getFileObject().getNameExt();
                    }
                    if (sourceLocator == null || name == null || !sourceLocator.equals(name)) continue;
                    int startLine = ex.getStartLine();
                    int startColumn = ex.getStartColumn();
                    int line = ex.getLine();
                    int endColumn = ex.getEndColumn();
                    int startOffset = 0;
                    int endOffset = 0;
                    if (context.document != null) {
                        startOffset = ASTUtils.getOffset(context.document, startLine > 0 ? startLine : 1, startColumn > 0 ? startColumn : 1);
                        endOffset = ASTUtils.getOffset(context.document, line > 0 ? line : 1, endColumn > 0 ? endColumn : 1);
                    }
                    GroovyParser.notifyError(context, null, Severity.ERROR, ex.getMessage(), null, startOffset, endOffset, sanitizing);
                    continue;
                }
                if (object instanceof SimpleMessage) {
                    String message = ((SimpleMessage)object).getMessage();
                    GroovyParser.notifyError(context, null, Severity.ERROR, message, null, -1, sanitizing);
                    continue;
                }
                GroovyParser.notifyError(context, null, Severity.ERROR, "Error", null, -1, sanitizing);
            }
        }
    }

    private static interface ParseErrorHandler {
        public void error(Error var1);
    }

    public static final class Context {
        private final Snapshot snapshot;
        private final SourceModificationEvent event;
        private final BaseDocument document;
        private ParseErrorHandler errorHandler;
        private int errorOffset;
        private String source;
        private String sanitizedSource;
        private OffsetRange sanitizedRange = OffsetRange.NONE;
        private String sanitizedContents;
        private int caretOffset;
        private Sanitize sanitized = Sanitize.NONE;

        public Context(Snapshot snapshot, SourceModificationEvent event) {
            this.snapshot = snapshot;
            this.event = event;
            this.source = GroovyParser.asString(snapshot.getText());
            this.caretOffset = GsfUtilities.getLastKnownCaretOffset((Snapshot)snapshot, (EventObject)event);
            this.document = LexUtilities.getDocument(snapshot.getSource(), true);
        }

        public String toString() {
            return "GroovyParser.Context(" + (Object)this.snapshot.getSource().getFileObject() + ")";
        }

        public OffsetRange getSanitizedRange() {
            return this.sanitizedRange;
        }

        Sanitize getSanitized() {
            return this.sanitized;
        }

        public String getSanitizedSource() {
            return this.sanitizedSource;
        }

        public int getErrorOffset() {
            return this.errorOffset;
        }

        static /* synthetic */ String access$100(Context x0) {
            return x0.sanitizedSource;
        }

        static /* synthetic */ Sanitize access$902(Context x0, Sanitize x1) {
            x0.sanitized = x1;
            return x0.sanitized;
        }

        static /* synthetic */ Sanitize access$900(Context x0) {
            return x0.sanitized;
        }

        static /* synthetic */ OffsetRange access$300(Context x0) {
            return x0.sanitizedRange;
        }

        static /* synthetic */ String access$400(Context x0) {
            return x0.sanitizedContents;
        }
    }

    public static enum Sanitize {
        NEVER,
        NONE,
        EDITED_DOT,
        ERROR_DOT,
        ERROR_LINE,
        EDITED_LINE,
        MISSING_END;
        

        private Sanitize() {
        }
    }

}

