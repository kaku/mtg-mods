/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.GroovyClassLoader
 *  groovy.lang.GroovyResourceLoader
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.control.CompilationFailedException
 *  org.codehaus.groovy.control.CompilerConfiguration
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.JavaSource
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 */
package org.netbeans.modules.groovy.editor.compiler;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyResourceLoader;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.modules.groovy.editor.compiler.CompilationUnit;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;

public final class ClassNodeCache {
    private static final Logger LOG = Logger.getLogger(ClassNodeCache.class.getName());
    private static final ThreadLocal<ClassNodeCache> instance = new ThreadLocal();
    private static final int DEFAULT_NON_EXISTENT_CACHE_SIZE = 10000;
    private static final int NON_EXISTENT_CACHE_SIZE = Integer.getInteger("groovy.editor.ClassNodeCache.nonExistent.size", 10000);
    private static final char INNER_SEPARATOR = '$';
    private static final char PKG_SEPARATOR = '.';
    private final Map<CharSequence, ClassNode> cache = new HashMap<CharSequence, ClassNode>();
    private final Map<CharSequence, Void> nonExistent;
    private Reference<JavaSource> resolver;
    private Reference<GroovyClassLoader> transformationLoaderRef;
    private Reference<GroovyClassLoader> resolveLoaderRef;
    private long invocationCount;
    private long hitCount;

    private ClassNodeCache() {
        this.nonExistent = new LinkedHashMap<CharSequence, Void>(16, 0.75f, true){

            @Override
            protected boolean removeEldestEntry(Map.Entry<CharSequence, Void> eldest) {
                if (this.size() > NON_EXISTENT_CACHE_SIZE) {
                    LOG.log(Level.FINE, "Non existent cache full, removing : {0}", eldest.getKey());
                    return true;
                }
                return false;
            }
        };
        LOG.fine("ClassNodeCache created");
    }

    @CheckForNull
    public ClassNode get(@NonNull CharSequence name) {
        ClassNode result = this.cache.get(name);
        if (LOG.isLoggable(Level.FINER)) {
            ++this.invocationCount;
            if (result != null) {
                ++this.hitCount;
            } else {
                LOG.log(Level.FINEST, "No binding for: {0}", name);
            }
            LOG.log(Level.FINER, "Hit ratio: {0}%", (double)this.hitCount / (double)this.invocationCount * 100.0);
        }
        return result;
    }

    public boolean isNonExistent(@NonNull CharSequence name) {
        boolean res;
        if (!ClassNodeCache.isValidClassName(name)) {
            return true;
        }
        boolean bl = res = this.getNonExistent(name) != null;
        if (LOG.isLoggable(Level.FINER)) {
            ++this.invocationCount;
            if (res) {
                ++this.hitCount;
            } else {
                LOG.log(Level.FINEST, "No binding for: {0}", name);
            }
            LOG.log(Level.FINER, "Hit ratio: {0}%", (double)this.hitCount / (double)this.invocationCount * 100.0);
        }
        return res;
    }

    public void put(@NonNull CharSequence name, @NullAllowed ClassNode node) {
        if (node != null) {
            LOG.log(Level.FINE, "Added binding for: {0}", name);
            this.cache.put(name, node);
        } else {
            CharSequence parentName = this.getNonExistent(name);
            LOG.log(Level.FINE, "Added nonexistent class: {0}", name);
            this.nonExistent.put(parentName != null ? parentName : name, null);
        }
    }

    public boolean containsKey(@NonNull CharSequence name) {
        boolean result = this.cache.containsKey(name);
        if (LOG.isLoggable(Level.FINER)) {
            ++this.invocationCount;
            if (result) {
                ++this.hitCount;
            } else {
                LOG.log(Level.FINEST, "No binding for: {0}", name);
            }
            LOG.log(Level.FINER, "Hit ratio: {0}%", (double)this.hitCount / (double)this.invocationCount * 100.0);
        }
        return result;
    }

    @NonNull
    public JavaSource createResolver(@NonNull ClasspathInfo info) {
        JavaSource src;
        JavaSource javaSource = src = this.resolver == null ? null : this.resolver.get();
        if (src == null) {
            LOG.log(Level.FINE, "Javac resolver created.");
            src = JavaSource.create((ClasspathInfo)info, (FileObject[])new FileObject[0]);
            this.resolver = new SoftReference<JavaSource>(src);
        }
        return src;
    }

    public GroovyClassLoader createTransformationLoader(@NonNull ClassPath allResources, @NonNull CompilerConfiguration configuration) {
        GroovyClassLoader transformationLoader;
        GroovyClassLoader groovyClassLoader = transformationLoader = this.transformationLoaderRef == null ? null : this.transformationLoaderRef.get();
        if (transformationLoader == null) {
            LOG.log(Level.FINE, "Transformation ClassLoader created.");
            transformationLoader = new TransformationClassLoader(CompilationUnit.class.getClassLoader(), allResources, configuration);
            this.transformationLoaderRef = new SoftReference<GroovyClassLoader>(transformationLoader);
        }
        return transformationLoader;
    }

    public GroovyClassLoader createResolveLoader(@NonNull ClassPath allResources, @NonNull CompilerConfiguration configuration) {
        GroovyClassLoader resolveLoader;
        GroovyClassLoader groovyClassLoader = resolveLoader = this.resolveLoaderRef == null ? null : this.resolveLoaderRef.get();
        if (resolveLoader == null) {
            LOG.log(Level.FINE, "Resolver ClassLoader created.");
            resolveLoader = new ParsingClassLoader(allResources, configuration, this);
            this.resolveLoaderRef = new SoftReference<GroovyClassLoader>(resolveLoader);
        }
        return resolveLoader;
    }

    @CheckForNull
    private CharSequence getNonExistent(@NonNull CharSequence name) {
        int index = name.length();
        while (index > 0) {
            CharSequence subName = name.subSequence(0, index);
            if (this.nonExistent.containsKey(subName)) {
                return subName;
            }
            index = ClassNodeCache.getNextPoint(name, index);
        }
        return null;
    }

    @NonNull
    public static ClassNodeCache get() {
        ClassNodeCache c = instance.get();
        if (c == null) {
            c = new ClassNodeCache();
        }
        return c;
    }

    public static ClassNodeCache createThreadLocalInstance() {
        ClassNodeCache c = new ClassNodeCache();
        instance.set(c);
        LOG.log(Level.FINE, "ClassNodeCache attached to thread: {0}", Thread.currentThread().getId());
        return c;
    }

    public static void clearThreadLocalInstance() {
        instance.remove();
        LOG.log(Level.FINE, "ClassNodeCache removed from thread: {0}", Thread.currentThread().getId());
    }

    private static int getNextPoint(@NonNull CharSequence name, int currentPoint) {
        for (int i = currentPoint - 1; i > 0; --i) {
            if (name.charAt(i) != '$') continue;
            return i;
        }
        return -1;
    }

    private static boolean isValidClassName(@NonNull CharSequence name) {
        char lastDot = '\uffffffff';
        for (int i = name.length() - 1; i >= 0; --i) {
            char c = name.charAt(i);
            if (c == '.') {
                lastDot = c;
                continue;
            }
            if (c != '$' || lastDot <= c) continue;
            return false;
        }
        return true;
    }

    private static class ParsingClassLoader
    extends GroovyClassLoader {
        private static final ClassNotFoundException CNF = new ClassNotFoundException();
        private final CompilerConfiguration config;
        private final ClassPath path;
        private final ClassNodeCache cache;
        private final GroovyResourceLoader resourceLoader;

        public ParsingClassLoader(@NonNull ClassPath path, @NonNull CompilerConfiguration config, @NonNull ClassNodeCache cache) {
            super(path.getClassLoader(true), config);
            this.resourceLoader = new GroovyResourceLoader(){

                public URL loadGroovySource(final String filename) throws MalformedURLException {
                    URL file = (URL)AccessController.doPrivileged(new PrivilegedAction(){

                        public Object run() {
                            return ParsingClassLoader.this.getSourceFile(filename);
                        }
                    });
                    return file;
                }

            };
            this.config = config;
            this.path = path;
            this.cache = cache;
        }

        public Class loadClass(String name, boolean lookupScriptFiles, boolean preferClassOverScript, boolean resolve) throws ClassNotFoundException, CompilationFailedException {
            if (preferClassOverScript && !lookupScriptFiles && this.cache.isNonExistent(name)) {
                throw CNF;
            }
            return super.loadClass(name, lookupScriptFiles, preferClassOverScript, resolve);
        }

        public GroovyResourceLoader getResourceLoader() {
            return this.resourceLoader;
        }

        private URL getSourceFile(String name) {
            FileObject fo = this.path.findResource(name.replace('.', '/') + this.config.getDefaultScriptExtension());
            if (fo == null || fo.isFolder()) {
                return null;
            }
            return URLMapper.findURL((FileObject)fo, (int)1);
        }

    }

    private static class TransformationClassLoader
    extends GroovyClassLoader {
        public TransformationClassLoader(ClassLoader parent, ClassPath cp, CompilerConfiguration config) {
            super(parent, config);
            for (ClassPath.Entry entry : cp.entries()) {
                this.addURL(entry.getURL());
            }
        }
    }

}

