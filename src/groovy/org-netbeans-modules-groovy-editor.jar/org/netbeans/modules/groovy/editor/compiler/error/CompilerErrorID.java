/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.compiler.error;

public enum CompilerErrorID {
    CLASS_DOES_NOT_IMPLEMENT_ALL_METHODS,
    CLASS_NOT_FOUND,
    UNDEFINED;
    

    private CompilerErrorID() {
    }
}

