/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.api.elements.common;

import java.util.List;
import java.util.Objects;

public interface MethodElement {
    public List<MethodParameter> getParameters();

    public List<String> getParameterTypes();

    public String getReturnType();

    public static final class MethodParameter {
        private final String fqnType;
        private final String type;
        private final String name;

        public MethodParameter(String fqnType, String type) {
            this(fqnType, type, null);
        }

        public MethodParameter(String fqnType, String type, String name) {
            this.fqnType = fqnType;
            this.type = type;
            this.name = name;
        }

        public String getFqnType() {
            return this.fqnType;
        }

        public String getName() {
            return this.name;
        }

        public String getType() {
            return this.type;
        }

        public String toString() {
            return this.type + " " + this.name;
        }

        public int hashCode() {
            int hash = 5;
            hash = 13 * hash + Objects.hashCode(this.fqnType);
            hash = 13 * hash + Objects.hashCode(this.type);
            hash = 13 * hash + Objects.hashCode(this.name);
            return hash;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            MethodParameter other = (MethodParameter)obj;
            if (!Objects.equals(this.fqnType, other.fqnType)) {
                return false;
            }
            if (!Objects.equals(this.type, other.type)) {
                return false;
            }
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return true;
        }
    }

}

