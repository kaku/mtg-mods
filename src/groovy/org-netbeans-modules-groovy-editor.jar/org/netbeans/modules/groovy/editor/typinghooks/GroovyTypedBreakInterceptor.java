/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$MutableContext
 */
package org.netbeans.modules.groovy.editor.typinghooks;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.typinghooks.TypingHooksUtil;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;

public class GroovyTypedBreakInterceptor
implements TypedBreakInterceptor {
    static final boolean CONTINUE_COMMENTS = Boolean.getBoolean("groovy.cont.comment");

    public void insert(TypedBreakInterceptor.MutableContext context) throws BadLocationException {
        int indentSize;
        String str;
        int begin;
        String text;
        boolean isComment;
        StringBuilder sb;
        Token<GroovyTokenId> prevToken;
        BaseDocument doc = (BaseDocument)context.getDocument();
        TokenHierarchy tokenHierarchy = TokenHierarchy.get((Document)context.getDocument());
        int offset = context.getCaretOffset();
        boolean insertMatching = TypingHooksUtil.isMatchingBracketsEnabled(doc);
        int lineBegin = Utilities.getRowStart((BaseDocument)doc, (int)offset);
        int lineEnd = Utilities.getRowEnd((BaseDocument)doc, (int)offset);
        if (lineBegin == offset && lineEnd == offset) {
            return;
        }
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence(tokenHierarchy, offset);
        if (ts == null) {
            return;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return;
        }
        Token token = ts.token();
        GroovyTokenId id = (GroovyTokenId)token.id();
        boolean insertRightBrace = this.isAddRightBrace(doc, offset);
        if (id != GroovyTokenId.ERROR && id != GroovyTokenId.BLOCK_COMMENT && insertMatching && insertRightBrace) {
            int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
            int afterLastNonWhite = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)offset);
            sb = new StringBuilder();
            int carretOffset = 0;
            int curlyOffset = this.getUnbalancedCurlyOffset(doc, offset);
            if (offset > afterLastNonWhite) {
                sb.append("\n");
                sb.append(IndentUtils.createIndentString((Document)doc, (int)(indent + IndentUtils.indentLevelSize((Document)doc))));
                carretOffset = sb.length();
                sb.append("\n");
                if (curlyOffset >= 0) {
                    sb.append(IndentUtils.createIndentString((Document)doc, (int)GsfUtilities.getLineIndent((BaseDocument)doc, (int)curlyOffset)));
                } else {
                    sb.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                }
                sb.append("}");
            } else {
                boolean[] insert = new boolean[]{true};
                int end = this.getRowOrBlockEnd(doc, offset, insert);
                if (insert[0]) {
                    String restOfLine = doc.getText(offset, Math.min(end, Utilities.getRowEnd((BaseDocument)doc, (int)afterLastNonWhite)) - offset);
                    sb.append("\n");
                    sb.append(IndentUtils.createIndentString((Document)doc, (int)(indent + IndentUtils.indentLevelSize((Document)doc))));
                    carretOffset = sb.length();
                    sb.append(restOfLine);
                    sb.append("\n");
                    if (curlyOffset >= 0) {
                        sb.append(IndentUtils.createIndentString((Document)doc, (int)GsfUtilities.getLineIndent((BaseDocument)doc, (int)curlyOffset)));
                    } else {
                        sb.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                    }
                    sb.append("}");
                    doc.remove(offset, restOfLine.length());
                }
            }
            if (sb.length() > 0) {
                context.setText(sb.toString(), 0, carretOffset, new int[0]);
            }
            return;
        }
        if (id == GroovyTokenId.ERROR && (text = token.text().toString()).startsWith("/*") && ts.offset() == Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset)) {
            int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("\n");
            sb2.append(IndentUtils.createIndentString((Document)doc, (int)indent));
            sb2.append(" * ");
            int carretOffset = sb2.length();
            sb2.append("\n");
            sb2.append(IndentUtils.createIndentString((Document)doc, (int)indent));
            sb2.append(" */");
            context.setText(sb2.toString(), 0, carretOffset, new int[0]);
            return;
        }
        if (id == GroovyTokenId.STRING_LITERAL || id == GroovyTokenId.STRING_CH && offset < ts.offset() + ts.token().length()) {
            str = id != GroovyTokenId.STRING_LITERAL || offset > ts.offset() ? "\\n\\\n" : "\\\n";
            context.setText(str, -1, str.length(), new int[0]);
            return;
        }
        if (id == GroovyTokenId.REGEXP_LITERAL && offset < ts.offset() + ts.token().length()) {
            str = id != GroovyTokenId.REGEXP_LITERAL || offset > ts.offset() ? "\\n\\\n" : "\\\n";
            context.setText(str, -1, str.length(), new int[0]);
            return;
        }
        if ((id == GroovyTokenId.RBRACE || id == GroovyTokenId.RBRACKET) && offset > 0 && (prevToken = LexUtilities.getToken(doc, offset - 1)) != null) {
            GroovyTokenId prevTokenId = (GroovyTokenId)prevToken.id();
            if (id == GroovyTokenId.RBRACE && prevTokenId == GroovyTokenId.LBRACE || id == GroovyTokenId.RBRACKET && prevTokenId == GroovyTokenId.LBRACKET) {
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb3 = new StringBuilder();
                sb3.append("\n");
                sb3.append(IndentUtils.createIndentString((Document)doc, (int)(indent + IndentUtils.indentLevelSize((Document)doc))));
                int carretOffset = sb3.length();
                sb3.append("\n");
                sb3.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                context.setText(sb3.toString(), 0, carretOffset, new int[0]);
                return;
            }
        }
        if ((id == GroovyTokenId.WHITESPACE || id == GroovyTokenId.NLS) && (begin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset)) != -1 && offset < begin) {
            ts.move(begin);
            if (ts.moveNext() && (id = (GroovyTokenId)ts.token().id()) == GroovyTokenId.LINE_COMMENT) {
                offset = begin;
            }
        }
        if (id == GroovyTokenId.BLOCK_COMMENT && offset > ts.offset() && offset < ts.offset() + ts.token().length()) {
            String line;
            boolean isBlockStart;
            int begin2 = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            int end = Utilities.getRowEnd((BaseDocument)doc, (int)offset) + 1;
            if (begin2 == -1) {
                begin2 = end;
            }
            boolean bl = isBlockStart = (line = doc.getText(begin2, end - begin2)).startsWith("/*") || begin2 != -1 && begin2 < ts.offset();
            if (isBlockStart || line.startsWith("*")) {
                int carretPosition;
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb4 = new StringBuilder("\n");
                if (isBlockStart) {
                    ++indent;
                }
                sb4.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                if (isBlockStart) {
                    sb4.append("* ");
                    carretPosition = sb4.length();
                } else {
                    char c;
                    sb4.append("*");
                    int afterStar = isBlockStart ? begin2 + 2 : begin2 + 1;
                    line = doc.getText(afterStar, Utilities.getRowEnd((BaseDocument)doc, (int)afterStar) - afterStar);
                    for (int i = 0; i < line.length() && ((c = line.charAt(i)) == ' ' || c == '\t'); ++i) {
                        sb4.append(c);
                    }
                    carretPosition = sb4.length();
                }
                if (offset == begin2 && offset > 0) {
                    context.setText(sb4.toString(), -1, sb4.length(), new int[0]);
                    return;
                }
                context.setText(sb4.toString(), -1, carretPosition, new int[0]);
                return;
            }
        }
        boolean bl = isComment = id == GroovyTokenId.LINE_COMMENT;
        if (id == GroovyTokenId.EOL && ts.movePrevious() && ts.token().id() == GroovyTokenId.LINE_COMMENT) {
            isComment = true;
        }
        if (isComment) {
            int nextBegin;
            int rowEnd;
            Token<GroovyTokenId> firstToken;
            int prevBegin;
            Token<GroovyTokenId> firstToken2;
            boolean continueComment = false;
            int begin3 = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            boolean previousLineWasComment = false;
            boolean nextLineIsComment = false;
            int rowStart = Utilities.getRowStart((BaseDocument)doc, (int)offset);
            if (rowStart > 0 && (prevBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)(rowStart - 1))) != -1 && (firstToken2 = LexUtilities.getToken(doc, prevBegin)) != null && firstToken2.id() == GroovyTokenId.LINE_COMMENT) {
                previousLineWasComment = true;
            }
            if ((rowEnd = Utilities.getRowEnd((BaseDocument)doc, (int)offset)) < doc.getLength() && (nextBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)(rowEnd + 1))) != -1 && (firstToken = LexUtilities.getToken(doc, nextBegin)) != null && firstToken.id() == GroovyTokenId.LINE_COMMENT) {
                nextLineIsComment = true;
            }
            if (previousLineWasComment || nextLineIsComment || offset > ts.offset() && offset < ts.offset() + ts.token().length()) {
                Token<GroovyTokenId> firstToken3;
                int nextLineFirst;
                Token<GroovyTokenId> firstToken4;
                int nextLine;
                if (ts.offset() + token.length() > offset + 1) {
                    String trailing = doc.getText(offset, Utilities.getRowEnd((BaseDocument)doc, (int)offset) - offset);
                    if (trailing.trim().length() != 0) {
                        continueComment = true;
                    }
                } else if (CONTINUE_COMMENTS && (firstToken3 = LexUtilities.getToken(doc, begin3)).id() == GroovyTokenId.LINE_COMMENT) {
                    continueComment = true;
                }
                if (!continueComment && (nextLine = Utilities.getRowEnd((BaseDocument)doc, (int)offset) + 1) < doc.getLength() && (nextLineFirst = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)nextLine)) != -1 && (firstToken4 = LexUtilities.getToken(doc, nextLineFirst)) != null && firstToken4.id() == GroovyTokenId.LINE_COMMENT) {
                    continueComment = true;
                }
            }
            if (continueComment) {
                char c;
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb5 = new StringBuilder();
                if (offset != begin3 || offset <= 0) {
                    sb5.append("\n");
                }
                sb5.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                sb5.append("//");
                int afterSlash = begin3 + 2;
                String line = doc.getText(afterSlash, Utilities.getRowEnd((BaseDocument)doc, (int)afterSlash) - afterSlash);
                for (int i = 0; i < line.length() && ((c = line.charAt(i)) == ' ' || c == '\t'); ++i) {
                    sb5.append(c);
                }
                if (offset == begin3 && offset > 0) {
                    int caretPosition = sb5.length();
                    sb5.append("\n");
                    context.setText(sb5.toString(), -1, caretPosition, new int[0]);
                    return;
                }
                context.setText(sb5.toString(), -1, sb5.length(), new int[0]);
                return;
            }
        }
        if ((indentSize = this.getNextLineIndentation(doc, offset)) > 0) {
            sb = new StringBuilder("\n");
            sb.append(IndentUtils.createIndentString((Document)doc, (int)indentSize));
            context.setText(sb.toString(), -1, sb.length(), new int[0]);
        }
    }

    public void afterInsert(TypedBreakInterceptor.Context context) throws BadLocationException {
    }

    public boolean beforeInsert(TypedBreakInterceptor.Context context) throws BadLocationException {
        return false;
    }

    public void cancelled(TypedBreakInterceptor.Context context) {
    }

    private int getNextLineIndentation(BaseDocument doc, int offset) throws BadLocationException {
        int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
        int currentOffset = offset;
        while (currentOffset > 0) {
            if (!(Utilities.isRowEmpty((BaseDocument)doc, (int)currentOffset) || Utilities.isRowWhite((BaseDocument)doc, (int)currentOffset) || LexUtilities.isCommentOnlyLine(doc, currentOffset))) {
                indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)currentOffset);
                int parenBalance = LexUtilities.getLineBalance(doc, currentOffset, GroovyTokenId.LPAREN, GroovyTokenId.RPAREN);
                if (parenBalance < 0) break;
                int curlyBalance = LexUtilities.getLineBalance(doc, currentOffset, GroovyTokenId.LBRACE, GroovyTokenId.RBRACE);
                if (curlyBalance > 0) {
                    indent += IndentUtils.indentLevelSize((Document)doc);
                }
                return indent;
            }
            currentOffset = Utilities.getRowStart((BaseDocument)doc, (int)currentOffset) - 1;
        }
        return indent;
    }

    private boolean isAddRightBrace(BaseDocument doc, int caretOffset) throws BadLocationException {
        if (LexUtilities.getTokenBalance(doc, GroovyTokenId.LBRACE, GroovyTokenId.RBRACE, caretOffset) <= 0) {
            return false;
        }
        int caretRowStartOffset = Utilities.getRowStart((BaseDocument)doc, (int)caretOffset);
        TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(doc, caretOffset);
        if (ts == null) {
            return false;
        }
        boolean first = true;
        do {
            if (ts.offset() < caretRowStartOffset) {
                return false;
            }
            GroovyTokenId id = (GroovyTokenId)ts.token().id();
            switch (id) {
                case WHITESPACE: 
                case LINE_COMMENT: {
                    break;
                }
                case BLOCK_COMMENT: {
                    if (!first || caretOffset <= ts.offset() || caretOffset >= ts.offset() + ts.token().length()) break;
                    return false;
                }
                case LBRACE: {
                    return true;
                }
            }
            first = false;
        } while (ts.movePrevious());
        return false;
    }

    private int getRowOrBlockEnd(BaseDocument doc, int caretOffset, boolean[] insert) throws BadLocationException {
        int rowEnd = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)caretOffset);
        if (rowEnd == -1 || caretOffset >= rowEnd) {
            return caretOffset;
        }
        ++rowEnd;
        int parenBalance = 0;
        int braceBalance = 0;
        int bracketBalance = 0;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(doc, caretOffset);
        if (ts == null) {
            return caretOffset;
        }
        while (ts.offset() < rowEnd) {
            GroovyTokenId id = (GroovyTokenId)ts.token().id();
            switch (id) {
                case SEMI: {
                    return ts.offset() + 1;
                }
                case COMMA: {
                    return ts.offset();
                }
                case LPAREN: {
                    ++parenBalance;
                    break;
                }
                case RPAREN: {
                    if (parenBalance-- != 0) break;
                    return ts.offset();
                }
                case LBRACE: {
                    ++braceBalance;
                    break;
                }
                case RBRACE: {
                    if (braceBalance-- != 0) break;
                    return ts.offset();
                }
                case LBRACKET: {
                    ++bracketBalance;
                    break;
                }
                case RBRACKET: {
                    if (bracketBalance-- != 0) break;
                    return ts.offset();
                }
            }
            if (ts.moveNext()) continue;
            if (caretOffset - ts.offset() != 1 || bracketBalance != 1 && parenBalance != 1 && braceBalance != 1) break;
            return caretOffset;
        }
        insert[0] = false;
        return rowEnd;
    }

    private int getUnbalancedCurlyOffset(BaseDocument doc, int offset) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(doc, offset);
        if (ts == null) {
            return -1;
        }
        int balance = 0;
        while (ts.movePrevious()) {
            Token t = ts.token();
            if (t.id() == GroovyTokenId.RBRACE) {
                ++balance;
                continue;
            }
            if (t.id() != GroovyTokenId.LBRACE || --balance >= 0) continue;
            return ts.offset();
        }
        return -1;
    }

    public static class GroovyTypedBreakInterceptorFactory
    implements TypedBreakInterceptor.Factory {
        public TypedBreakInterceptor createTypedBreakInterceptor(MimePath mimePath) {
            return new GroovyTypedBreakInterceptor();
        }
    }

}

