/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.MetaMethod
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.expr.ArgumentListExpression
 *  org.codehaus.groovy.control.SourceUnit
 *  org.codehaus.groovy.reflection.CachedClass
 *  org.netbeans.api.java.platform.JavaPlatform
 *  org.netbeans.api.java.platform.JavaPlatformManager
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.modules.csl.api.CodeCompletionContext
 *  org.netbeans.modules.csl.api.CodeCompletionHandler
 *  org.netbeans.modules.csl.api.CodeCompletionHandler$QueryType
 *  org.netbeans.modules.csl.api.CodeCompletionResult
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.ParameterInfo
 *  org.netbeans.modules.csl.spi.DefaultCompletionResult
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.groovy.editor.api.completion;

import groovy.lang.MetaMethod;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.reflection.CachedClass;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.csl.api.CodeCompletionContext;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.CodeCompletionResult;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.ParameterInfo;
import org.netbeans.modules.csl.spi.DefaultCompletionResult;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.GroovyCompletionResult;
import org.netbeans.modules.groovy.editor.api.completion.HTMLJavadocParser;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.ContextHelper;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTMethod;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.completion.ProposalsCollector;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class CompletionHandler
implements CodeCompletionHandler {
    private static final Logger LOG = Logger.getLogger(CompletionHandler.class.getName());
    private final PropertyChangeListener docListener;
    private String jdkJavaDocBase = null;
    private String groovyJavaDocBase = null;
    private String groovyApiDocBase = null;

    public CompletionHandler() {
        JavaPlatformManager platformMan = JavaPlatformManager.getDefault();
        JavaPlatform platform = platformMan.getDefaultPlatform();
        List docfolder = platform.getJavadocFolders();
        for (URL url : docfolder) {
            LOG.log(Level.FINEST, "JDK Doc path: {0}", url.toString());
            this.jdkJavaDocBase = url.toString();
        }
        this.docListener = new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                CompletionHandler completionHandler = CompletionHandler.this;
                synchronized (completionHandler) {
                    CompletionHandler.this.groovyJavaDocBase = null;
                    CompletionHandler.this.groovyApiDocBase = null;
                }
            }
        };
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public CodeCompletionResult complete(CodeCompletionContext completionContext) {
        ParserResult parserResult = completionContext.getParserResult();
        String prefix = completionContext.getPrefix();
        if (prefix == null) {
            prefix = "";
        }
        int lexOffset = completionContext.getCaretOffset();
        int astOffset = ASTUtils.getAstOffset((Parser.Result)parserResult, lexOffset);
        int anchor = lexOffset - prefix.length();
        LOG.log(Level.FINEST, "complete(...), prefix      : {0}", prefix);
        LOG.log(Level.FINEST, "complete(...), lexOffset   : {0}", lexOffset);
        LOG.log(Level.FINEST, "complete(...), astOffset   : {0}", astOffset);
        Document document = parserResult.getSnapshot().getSource().getDocument(false);
        if (document == null) {
            return CodeCompletionResult.NONE;
        }
        BaseDocument doc = (BaseDocument)document;
        doc.readLock();
        try {
            CompletionContext context = new CompletionContext(parserResult, prefix, anchor, lexOffset, astOffset, doc);
            context.init();
            if (context.location == CaretLocation.ABOVE_PACKAGE || context.location == CaretLocation.INSIDE_COMMENT) {
                DefaultCompletionResult defaultCompletionResult = new DefaultCompletionResult(Collections.EMPTY_LIST, false);
                return defaultCompletionResult;
            }
            ProposalsCollector proposalsCollector = new ProposalsCollector(context);
            if (ContextHelper.isVariableNameDefinition(context) || ContextHelper.isFieldNameDefinition(context)) {
                proposalsCollector.completeNewVars(context);
            } else {
                if (context.location != CaretLocation.OUTSIDE_CLASSES && context.location != CaretLocation.INSIDE_STRING) {
                    proposalsCollector.completePackages(context);
                    proposalsCollector.completeTypes(context);
                }
                if (!context.isBehindImportStatement()) {
                    if (context.location != CaretLocation.INSIDE_STRING) {
                        proposalsCollector.completeKeywords(context);
                        proposalsCollector.completeMethods(context);
                    }
                    proposalsCollector.completeFields(context);
                    proposalsCollector.completeLocalVars(context);
                }
                if (context.location == CaretLocation.INSIDE_CONSTRUCTOR_CALL && (ContextHelper.isAfterComma(context) || ContextHelper.isAfterLeftParenthesis(context))) {
                    proposalsCollector.completeNamedParams(context);
                }
            }
            proposalsCollector.completeCamelCase(context);
            GroovyCompletionResult groovyCompletionResult = new GroovyCompletionResult(proposalsCollector.getCollectedProposals(), context);
            return groovyCompletionResult;
        }
        finally {
            doc.readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String getGroovyJavadocBase() {
        CompletionHandler completionHandler = this;
        synchronized (completionHandler) {
            if (this.groovyJavaDocBase == null) {
                this.groovyJavaDocBase = "";
            }
            return this.groovyJavaDocBase;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String getGroovyApiDocBase() {
        CompletionHandler completionHandler = this;
        synchronized (completionHandler) {
            if (this.groovyApiDocBase == null) {
                this.groovyApiDocBase = "";
            }
            return this.groovyApiDocBase;
        }
    }

    private static String directoryNameToUrl(String dirname) {
        if (dirname == null) {
            return "";
        }
        File dirFile = new File(dirname);
        if (dirFile != null && dirFile.exists() && dirFile.isDirectory()) {
            String fileURL = "";
            if (Utilities.isWindows()) {
                dirname = dirname.replace("\\", "/");
                fileURL = "file:/";
            } else {
                fileURL = "file://";
            }
            return fileURL + dirname;
        }
        return "";
    }

    private static void printASTNodeInformation(String description, ASTNode node) {
        LOG.log(Level.FINEST, "--------------------------------------------------------");
        LOG.log(Level.FINEST, "{0}", description);
        if (node == null) {
            LOG.log(Level.FINEST, "node == null");
        } else {
            LOG.log(Level.FINEST, "Node.getText()  : {0}", node.getText());
            LOG.log(Level.FINEST, "Node.toString() : {0}", node.toString());
            LOG.log(Level.FINEST, "Node.getClass() : {0}", node.getClass());
            LOG.log(Level.FINEST, "Node.hashCode() : {0}", node.hashCode());
            if (node instanceof ModuleNode) {
                LOG.log(Level.FINEST, "ModuleNode.getClasses() : {0}", ((ModuleNode)node).getClasses());
                LOG.log(Level.FINEST, "SourceUnit.getName() : {0}", ((ModuleNode)node).getContext().getName());
            }
        }
        LOG.log(Level.FINEST, "--------------------------------------------------------");
    }

    private static void printMethod(MetaMethod mm) {
        LOG.log(Level.FINEST, "--------------------------------------------------");
        LOG.log(Level.FINEST, "getName()           : {0}", mm.getName());
        LOG.log(Level.FINEST, "toString()          : {0}", mm.toString());
        LOG.log(Level.FINEST, "getDescriptor()     : {0}", mm.getDescriptor());
        LOG.log(Level.FINEST, "getSignature()      : {0}", mm.getSignature());
        LOG.log(Level.FINEST, "getDeclaringClass() : {0}", (Object)mm.getDeclaringClass());
    }

    boolean checkForPackageStatement(CompletionContext request) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)request.doc, 1);
        if (ts != null) {
            ts.move(1);
            while (ts.isValid() && ts.moveNext() && ts.offset() < request.doc.getLength()) {
                Token t = ts.token();
                if (t.id() != GroovyTokenId.LITERAL_package) continue;
                return true;
            }
        }
        return false;
    }

    private ArgumentListExpression getSurroundingArgumentList(AstPath path) {
        if (path == null) {
            LOG.log(Level.FINEST, "path == null");
            return null;
        }
        LOG.log(Level.FINEST, "AEL, Path : {0}", path);
        for (ASTNode current : path) {
            if (!(current instanceof ArgumentListExpression)) continue;
            return (ArgumentListExpression)current;
        }
        return null;
    }

    private AstPath getPathFromInfo(int caretOffset, ParserResult info) {
        assert (info != null);
        ModuleNode root = ASTUtils.getRoot(info);
        if (root == null) {
            return null;
        }
        BaseDocument doc = (BaseDocument)info.getSnapshot().getSource().getDocument(true);
        return new AstPath((ASTNode)root, caretOffset, doc);
    }

    boolean checkBehindDot(CompletionContext request) {
        boolean behindDot = false;
        if (request == null || request.context == null || request.context.before1 == null) {
            behindDot = false;
        } else if (CharSequenceUtilities.textEquals((CharSequence)request.context.before1.text(), (CharSequence)".") || request.context.before1.text().toString().equals(request.getPrefix()) && request.context.before2 != null && CharSequenceUtilities.textEquals((CharSequence)request.context.before2.text(), (CharSequence)".")) {
            behindDot = true;
        }
        return behindDot;
    }

    public static String getMethodSignature(MetaMethod method, boolean forURL, boolean isGDK) {
        String methodSignature = method.getSignature();
        methodSignature = methodSignature.trim();
        if (isGDK) {
            int firstSpace = methodSignature.indexOf(" ");
            if (firstSpace != -1) {
                methodSignature = methodSignature.substring(firstSpace + 1);
            }
            if (forURL) {
                methodSignature = methodSignature.replaceAll(", ", ",%20");
            }
            return methodSignature;
        }
        String[] parts = methodSignature.split("[()]");
        if (parts.length < 2) {
            return "";
        }
        String paramsBody = CompletionHandler.decodeTypes(parts[1], forURL);
        return parts[0] + "(" + paramsBody + ")";
    }

    static String decodeTypes(String encodedType, boolean forURL) {
        String DELIMITER = ",";
        DELIMITER = forURL ? DELIMITER + "%20" : DELIMITER + " ";
        StringBuilder sb = new StringBuilder("");
        boolean nextIsAnArray = false;
        for (int i = 0; i < encodedType.length(); ++i) {
            char c = encodedType.charAt(i);
            if (c == '[') {
                nextIsAnArray = true;
                continue;
            }
            if (c == 'Z') {
                sb.append("boolean");
            } else if (c == 'B') {
                sb.append("byte");
            } else if (c == 'C') {
                sb.append("char");
            } else if (c == 'D') {
                sb.append("double");
            } else if (c == 'F') {
                sb.append("float");
            } else if (c == 'I') {
                sb.append("int");
            } else if (c == 'J') {
                sb.append("long");
            } else if (c == 'S') {
                sb.append("short");
            } else if (c == 'L') {
                int semicolon = encodedType.indexOf(";", ++i);
                String typeName = encodedType.substring(i, semicolon);
                typeName = typeName.replace('/', '.');
                if (forURL) {
                    sb.append(typeName);
                } else {
                    sb.append(GroovyUtils.stripPackage(typeName));
                }
                i = semicolon;
            }
            if (nextIsAnArray) {
                sb.append("[]");
                nextIsAnArray = false;
            }
            if (i >= encodedType.length() - 1) continue;
            sb.append(DELIMITER);
        }
        return sb.toString();
    }

    public String document(ParserResult info, ElementHandle element) {
        LOG.log(Level.FINEST, "document(), ElementHandle : {0}", (Object)element);
        String error = NbBundle.getMessage(CompletionHandler.class, (String)"GroovyCompletion_NoJavaDocFound");
        String doctext = null;
        if (element instanceof ASTMethod) {
            String className;
            ASTMethod ame = (ASTMethod)element;
            String base = "";
            String javadoc = this.getGroovyJavadocBase();
            if (this.jdkJavaDocBase != null && !ame.isGDK()) {
                base = this.jdkJavaDocBase;
            } else if (javadoc != null && ame.isGDK()) {
                base = javadoc;
            } else {
                LOG.log(Level.FINEST, "Neither JDK nor GDK or error locating: {0}", ame.isGDK());
                return error;
            }
            MetaMethod mm = ame.getMethod();
            CompletionHandler.printMethod(mm);
            if (ame.isGDK()) {
                className = mm.getDeclaringClass().getName();
            } else {
                CachedClass cc;
                String declName = null;
                if (mm != null && (cc = mm.getDeclaringClass()) != null) {
                    declName = cc.getName();
                }
                className = declName != null ? declName : ame.getClz().getName();
            }
            String classNamePath = className.replace(".", "/");
            classNamePath = classNamePath + ".html";
            if (!ame.isGDK()) {
                File testFile;
                String apiDoc = this.getGroovyApiDocBase();
                try {
                    URL url = new URL(apiDoc + classNamePath);
                    testFile = new File(url.toURI());
                }
                catch (MalformedURLException ex) {
                    LOG.log(Level.FINEST, "MalformedURLException: {0}", ex);
                    return error;
                }
                catch (URISyntaxException uriEx) {
                    LOG.log(Level.FINEST, "URISyntaxException: {0}", uriEx);
                    return error;
                }
                if (testFile != null && testFile.exists()) {
                    base = apiDoc;
                }
            }
            String sig = CompletionHandler.getMethodSignature(ame.getMethod(), true, ame.isGDK());
            String printSig = CompletionHandler.getMethodSignature(ame.getMethod(), false, ame.isGDK());
            String urlName = base + classNamePath + "#" + sig;
            try {
                LOG.log(Level.FINEST, "Trying to load URL = {0}", urlName);
                doctext = HTMLJavadocParser.getJavadocText(new URL(urlName), false, ame.isGDK());
            }
            catch (MalformedURLException ex) {
                LOG.log(Level.FINEST, "document(), URL trouble: {0}", ex);
                return error;
            }
            if (doctext == null) {
                return error;
            }
            doctext = "<h3>" + className + "." + printSig + "</h3><BR>" + doctext;
        }
        return doctext;
    }

    public ElementHandle resolveLink(String link, ElementHandle originalHandle) {
        return originalHandle;
    }

    public String getPrefix(ParserResult info, int caretOffset, boolean upToOffset) {
        return null;
    }

    public CodeCompletionHandler.QueryType getAutoQuery(JTextComponent component, String typedText) {
        char c = typedText.charAt(0);
        if (c == '.') {
            return CodeCompletionHandler.QueryType.COMPLETION;
        }
        return CodeCompletionHandler.QueryType.NONE;
    }

    public String resolveTemplateVariable(String variable, ParserResult info, int caretOffset, String name, Map parameters) {
        return null;
    }

    public Set<String> getApplicableTemplates(Document d, int selectionBegin, int selectionEnd) {
        return Collections.emptySet();
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public ParameterInfo parameters(ParserResult info, int caretOffset, CompletionProposal proposal) {
        LOG.log(Level.FINEST, "parameters(), caretOffset = {0}", caretOffset);
        ArrayList<String> paramList = new ArrayList<String>();
        AstPath path = this.getPathFromInfo(caretOffset, info);
        BaseDocument doc = (BaseDocument)info.getSnapshot().getSource().getDocument(true);
        if (path != null) {
            ArgumentListExpression ael = this.getSurroundingArgumentList(path);
            if (ael != null) {
                List<ASTNode> children = ASTUtils.children((ASTNode)ael);
                int idx = 1;
                int index = -1;
                int offset = -1;
                for (ASTNode node : children) {
                    OffsetRange range = ASTUtils.getRange(node, doc);
                    paramList.add(node.getText());
                    if (range.containsInclusive(caretOffset)) {
                        offset = range.getStart();
                        index = idx;
                    }
                    ++idx;
                }
                if (paramList == null || paramList.isEmpty()) return ParameterInfo.NONE;
                return new ParameterInfo(paramList, index, offset);
            }
            LOG.log(Level.FINEST, "ArgumentListExpression ==  null");
            return ParameterInfo.NONE;
        }
        LOG.log(Level.FINEST, "path ==  null");
        return ParameterInfo.NONE;
    }

}

