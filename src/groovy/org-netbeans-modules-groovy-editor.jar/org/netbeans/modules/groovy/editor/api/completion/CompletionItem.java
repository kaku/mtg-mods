/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.MetaMethod
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.Variable
 *  org.netbeans.api.java.source.ui.ElementIcons
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.spi.DefaultCompletionProposal
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.groovy.editor.api.completion;

import groovy.lang.MetaMethod;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ElementKind;
import javax.lang.model.type.TypeMirror;
import javax.swing.ImageIcon;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.Variable;
import org.netbeans.api.java.source.ui.ElementIcons;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.spi.DefaultCompletionProposal;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.completion.CompletionHandler;
import org.netbeans.modules.groovy.editor.api.elements.ElementHandleSupport;
import org.netbeans.modules.groovy.editor.api.elements.GroovyElement;
import org.netbeans.modules.groovy.editor.api.elements.KeywordElement;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTMethod;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.java.Utilities;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.openide.util.ImageUtilities;

public abstract class CompletionItem
extends DefaultCompletionProposal {
    protected final GroovyElement element;
    private static final Logger LOG = Logger.getLogger(CompletionItem.class.getName());
    private static volatile ImageIcon groovyIcon;
    private static volatile ImageIcon javaIcon;
    private static volatile ImageIcon newConstructorIcon;

    private CompletionItem(GroovyElement element, int anchorOffset) {
        this.element = element;
        this.anchorOffset = anchorOffset;
        LOG.setLevel(Level.OFF);
    }

    public String getName() {
        return this.element.getName();
    }

    public ElementHandle getElement() {
        LOG.log(Level.FINEST, "getElement() element : {0}", this.element);
        return null;
    }

    public org.netbeans.modules.csl.api.ElementKind getKind() {
        return this.element.getKind();
    }

    public Set<Modifier> getModifiers() {
        return this.element.getModifiers();
    }

    public String toString() {
        String cls = this.getClass().getName();
        cls = cls.substring(cls.lastIndexOf(46) + 1);
        return cls + "(" + (Object)this.getKind() + "): " + this.getName();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        CompletionItem other = (CompletionItem)((Object)obj);
        if (this.getName() == null ? other.getName() != null : !this.getName().equals(other.getName())) {
            return false;
        }
        if (this.getKind() != other.getKind()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.getName() != null ? this.getName().hashCode() : 0);
        hash = 47 * hash + (this.getKind() != null ? this.getKind().hashCode() : 0);
        return hash;
    }

    public static CompletionItem forJavaMethod(String className, String simpleName, List<String> parameters, TypeMirror returnType, Set<javax.lang.model.element.Modifier> modifiers, int anchorOffset, boolean emphasise, boolean nameOnly) {
        return new JavaMethodItem(className, simpleName, parameters, returnType, modifiers, anchorOffset, emphasise, nameOnly);
    }

    public static CompletionItem forJavaMethod(String className, String simpleName, List<String> parameters, String returnType, Set<javax.lang.model.element.Modifier> modifiers, int anchorOffset, boolean emphasise, boolean nameOnly) {
        return new JavaMethodItem(className, simpleName, parameters, returnType, modifiers, anchorOffset, emphasise, nameOnly);
    }

    public static CompletionItem forDynamicMethod(int anchorOffset, String name, String[] parameters, String returnType, boolean prefix) {
        return new DynamicMethodItem(anchorOffset, name, parameters, returnType, prefix);
    }

    public static CompletionItem forDynamicField(int anchorOffset, String name, String type) {
        return new DynamicFieldItem(anchorOffset, name, type);
    }

    public static class NewFieldItem
    extends CompletionItem {
        private final String fieldName;

        public NewFieldItem(String var, int anchorOffset) {
            super(null, anchorOffset);
            this.fieldName = var;
        }

        @Override
        public String getName() {
            return this.fieldName;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.FIELD;
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.FIELD, (Collection)null);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }
    }

    public static class NewVarItem
    extends CompletionItem {
        private final String var;

        public NewVarItem(String var, int anchorOffset) {
            super(null, anchorOffset);
            this.var = var;
        }

        @Override
        public String getName() {
            return this.var;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.VARIABLE;
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.LOCAL_VARIABLE, (Collection)null);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }
    }

    public static class LocalVarItem
    extends CompletionItem {
        private final Variable var;

        public LocalVarItem(Variable var, int anchorOffset) {
            super(null, anchorOffset);
            this.var = var;
        }

        @Override
        public String getName() {
            return this.var.getName();
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.VARIABLE;
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            return GroovyUtils.stripPackage(Utilities.translateClassLoaderTypeName(this.var.getType().getName()));
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.LOCAL_VARIABLE, (Collection)null);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }
    }

    public static class FieldItem
    extends CompletionItem {
        private final String typeName;
        private final String fieldName;
        private final Set<Modifier> modifiers;

        public FieldItem(String typeName, String fieldName, int modifiers, int anchorOffset) {
            this(typeName, fieldName, Utilities.modelModifiersToGsf(Utilities.reflectionModifiersToModel(modifiers)), anchorOffset);
        }

        public FieldItem(String typeName, String fieldName, Set<Modifier> modifiers, int anchorOffset) {
            super(null, anchorOffset);
            this.typeName = typeName;
            this.fieldName = fieldName;
            this.modifiers = modifiers;
        }

        @Override
        public String getName() {
            return this.fieldName;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.FIELD;
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            return this.typeName;
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.FIELD, Utilities.gsfModifiersToModel(this.modifiers, null));
        }

        @Override
        public Set<Modifier> getModifiers() {
            return this.modifiers;
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(this.typeName, this.fieldName, org.netbeans.modules.csl.api.ElementKind.FIELD, this.getModifiers());
        }
    }

    public static class JavaFieldItem
    extends CompletionItem {
        private final String className;
        private final String name;
        private final TypeMirror type;
        private final Set<javax.lang.model.element.Modifier> modifiers;
        private final boolean emphasise;

        public JavaFieldItem(String className, String name, TypeMirror type, Set<javax.lang.model.element.Modifier> modifiers, int anchorOffset, boolean emphasise) {
            super(null, anchorOffset);
            this.className = className;
            this.name = name;
            this.type = type;
            this.modifiers = modifiers;
            this.emphasise = emphasise;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.FIELD;
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            String retType = "";
            if (this.type != null) {
                retType = Utilities.getTypeName(this.type, false).toString();
            }
            formatter.appendText(retType);
            return formatter.getText();
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.FIELD, this.modifiers);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Utilities.modelModifiersToGsf(this.modifiers);
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(this.className, this.name, org.netbeans.modules.csl.api.ElementKind.FIELD, this.getModifiers());
        }
    }

    public static class NamedParameter
    extends CompletionItem {
        private final String typeName;
        private final String name;

        public NamedParameter(String typeName, String name, int anchorOffset) {
            super(null, anchorOffset);
            this.typeName = typeName;
            this.name = name;
        }

        public String getLhsHtml(HtmlFormatter formatter) {
            return this.name + ": " + this.typeName;
        }

        @Override
        public String getName() {
            return this.name;
        }

        public String getCustomInsertTemplate() {
            return this.name + ": " + this.typeName;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.PARAMETER;
        }

        public boolean isSmart() {
            return true;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + Objects.hashCode(this.typeName);
            hash = 89 * hash + Objects.hashCode(this.name);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            NamedParameter other = (NamedParameter)((Object)obj);
            if (!Objects.equals(this.typeName, other.typeName)) {
                return false;
            }
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return true;
        }
    }

    public static class ConstructorItem
    extends CompletionItem {
        private static final String NEW_CSTR = "org/netbeans/modules/groovy/editor/resources/new_constructor_16.png";
        private final boolean expand;
        private final String name;
        private final String paramListString;
        private final List<MethodElement.MethodParameter> parameters;

        public ConstructorItem(String name, List<MethodElement.MethodParameter> parameters, int anchorOffset, boolean expand) {
            super(null, anchorOffset);
            this.name = name;
            this.expand = expand;
            this.parameters = parameters;
            this.paramListString = this.parseParams();
        }

        private String parseParams() {
            StringBuilder sb = new StringBuilder();
            if (!this.parameters.isEmpty()) {
                for (MethodElement.MethodParameter parameter : this.parameters) {
                    sb.append(parameter.getType());
                    sb.append(", ");
                }
                sb.delete(sb.length() - 2, sb.length());
            }
            return sb.toString();
        }

        public String getLhsHtml(HtmlFormatter formatter) {
            if (this.expand) {
                return this.name + "(" + this.paramListString + ") - generate";
            }
            return this.name + "(" + this.paramListString + ")";
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.CONSTRUCTOR;
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            return null;
        }

        public ImageIcon getIcon() {
            if (newConstructorIcon == null) {
                newConstructorIcon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/groovy/editor/resources/new_constructor_16.png", (boolean)false);
            }
            return newConstructorIcon;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public ElementHandle getElement() {
            return null;
        }

        public boolean isSmart() {
            return true;
        }

        public String getCustomInsertTemplate() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.getInsertPrefix());
            sb.append("(");
            int id = 1;
            if (this.parameters != null) {
                for (MethodElement.MethodParameter paramDesc : this.parameters) {
                    LOG.log(Level.FINEST, "-------------------------------------------------------------------");
                    LOG.log(Level.FINEST, "paramDesc.fullTypeName : {0}", paramDesc.getFqnType());
                    LOG.log(Level.FINEST, "paramDesc.typeName     : {0}", paramDesc.getType());
                    LOG.log(Level.FINEST, "paramDesc.name         : {0}", paramDesc.getName());
                    sb.append("${");
                    sb.append("groovy-cc-");
                    sb.append(Integer.toString(id));
                    sb.append(" default=\"");
                    sb.append(paramDesc.getName());
                    sb.append("\"");
                    sb.append("}");
                    if (id < this.parameters.size()) {
                        sb.append(", ");
                    }
                    ++id;
                }
            }
            sb.append(")");
            if (this.expand) {
                sb.append(" {\n}");
            }
            LOG.log(Level.FINEST, "Template returned : {0}", sb.toString());
            return sb.toString();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            ConstructorItem other = (ConstructorItem)((Object)obj);
            if (this.expand != other.expand) {
                return false;
            }
            if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
                return false;
            }
            if (this.paramListString == null ? other.paramListString != null : !this.paramListString.equals(other.paramListString)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 61 * hash + (this.expand ? 1 : 0);
            hash = 61 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 61 * hash + (this.paramListString != null ? this.paramListString.hashCode() : 0);
            return hash;
        }
    }

    public static class TypeItem
    extends CompletionItem {
        private final String fqn;
        private final String name;
        private final ElementKind ek;

        public TypeItem(String fqn, String name, int anchorOffset, ElementKind ek) {
            super(null, anchorOffset);
            this.fqn = fqn;
            this.name = name;
            this.ek = ek;
        }

        public String getFqn() {
            return this.fqn;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.CLASS;
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)this.ek, (Collection)null);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public ElementHandle getElement() {
            return null;
        }
    }

    public static class PackageItem
    extends CompletionItem {
        private final String packageName;
        private final ParserResult info;

        public PackageItem(String packageName, int anchorOffset, ParserResult info) {
            super(null, anchorOffset);
            this.packageName = packageName;
            this.info = info;
        }

        @Override
        public String getName() {
            return this.packageName;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.PACKAGE;
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.PACKAGE, (Collection)null);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(this.info, new KeywordElement(this.packageName));
        }

        public String getCustomInsertTemplate() {
            return this.packageName + ".";
        }
    }

    public static class KeywordItem
    extends CompletionItem {
        private static final String JAVA_KEYWORD = "org/netbeans/modules/groovy/editor/resources/duke.png";
        private final String keyword;
        private final String description;
        private final boolean isGroovy;
        private final ParserResult info;

        public KeywordItem(String keyword, String description, int anchorOffset, ParserResult info, boolean isGroovy) {
            super(null, anchorOffset);
            this.keyword = keyword;
            this.description = description;
            this.info = info;
            this.isGroovy = isGroovy;
        }

        @Override
        public String getName() {
            return this.keyword;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.KEYWORD;
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            if (this.description != null) {
                formatter.appendHtml(this.description);
                return formatter.getText();
            }
            return null;
        }

        public ImageIcon getIcon() {
            if (this.isGroovy) {
                if (groovyIcon == null) {
                    // empty if block
                }
                return groovyIcon;
            }
            if (javaIcon == null) {
                javaIcon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/groovy/editor/resources/duke.png", (boolean)false);
            }
            return javaIcon;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(this.info, new KeywordElement(this.keyword));
        }
    }

    public static class MetaMethodItem
    extends CompletionItem {
        private final MetaMethod method;
        private final boolean isGDK;
        private final ASTMethod methodElement;
        private final boolean nameOnly;

        public MetaMethodItem(Class clz, MetaMethod method, int anchorOffset, boolean isGDK, boolean nameOnly) {
            super(null, anchorOffset);
            this.method = method;
            this.isGDK = isGDK;
            this.nameOnly = nameOnly;
            this.methodElement = new ASTMethod(new ASTNode(), clz, method, isGDK);
        }

        public MetaMethod getMethod() {
            return this.method;
        }

        @Override
        public String getName() {
            return this.method.getName() + "()";
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.METHOD;
        }

        public String getLhsHtml(HtmlFormatter formatter) {
            org.netbeans.modules.csl.api.ElementKind kind = this.getKind();
            formatter.name(kind, true);
            if (this.isGDK) {
                formatter.appendText(this.method.getName());
                String signature = this.method.getSignature();
                int start = signature.indexOf("(");
                int end = signature.indexOf(")");
                String sig = signature.substring(start + 1, end);
                StringBuilder buf = new StringBuilder();
                for (String param : sig.split(",")) {
                    if (buf.length() > 0) {
                        buf.append(", ");
                    }
                    buf.append(GroovyUtils.stripPackage(Utilities.translateClassLoaderTypeName(param)));
                }
                String simpleSig = buf.toString();
                formatter.appendText("(" + simpleSig + ")");
            } else {
                formatter.appendText(CompletionHandler.getMethodSignature(this.method, false, this.isGDK));
            }
            formatter.name(kind, false);
            return formatter.getText();
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            String retType = this.method.getReturnType().getSimpleName();
            retType = GroovyUtils.stripPackage(retType);
            formatter.appendText(retType);
            return formatter.getText();
        }

        public ImageIcon getIcon() {
            if (!this.isGDK) {
                return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.METHOD, Utilities.reflectionModifiersToModel(this.method.getModifiers()));
            }
            if (groovyIcon == null) {
                // empty if block
            }
            return groovyIcon;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public ElementHandle getElement() {
            return this.methodElement;
        }

        public String getCustomInsertTemplate() {
            if (this.nameOnly) {
                return this.method.getName();
            }
            return super.getCustomInsertTemplate();
        }
    }

    private static class DynamicMethodItem
    extends CompletionItem {
        private final String name;
        private final String[] parameters;
        private final String returnType;
        private final boolean prefix;

        public DynamicMethodItem(int anchorOffset, String name, String[] parameters, String returnType, boolean prefix) {
            super(null, anchorOffset);
            this.name = name;
            this.parameters = parameters;
            this.returnType = returnType;
            this.prefix = prefix;
        }

        @Override
        public String getName() {
            return this.name + "()";
        }

        public String getSortText() {
            return this.name + (this.prefix ? 1 : 0) + this.parameters.length;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.METHOD;
        }

        public String getLhsHtml(HtmlFormatter formatter) {
            org.netbeans.modules.csl.api.ElementKind kind = this.getKind();
            formatter.name(kind, true);
            formatter.appendText(this.name);
            if (!this.prefix) {
                StringBuilder buf = new StringBuilder();
                for (String param : this.parameters) {
                    if (buf.length() > 0) {
                        buf.append(", ");
                    }
                    buf.append(GroovyUtils.stripPackage(Utilities.translateClassLoaderTypeName(param)));
                }
                String simpleSig = buf.toString();
                formatter.appendText("(" + simpleSig + ")");
            } else {
                formatter.appendText("...");
            }
            formatter.name(kind, false);
            return formatter.getText();
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            String retType = this.returnType;
            retType = GroovyUtils.stripPackage(retType);
            formatter.appendText(retType);
            return formatter.getText();
        }

        public ImageIcon getIcon() {
            if (groovyIcon == null) {
                // empty if block
            }
            return groovyIcon;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.singleton(Modifier.PROTECTED);
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(null, this.name, org.netbeans.modules.csl.api.ElementKind.METHOD, this.getModifiers());
        }

        public String getCustomInsertTemplate() {
            return this.name;
        }
    }

    public static class DynamicFieldItem
    extends CompletionItem {
        private final String name;
        private final String type;

        public DynamicFieldItem(int anchorOffset, String name, String type) {
            super(null, anchorOffset);
            this.name = name;
            this.type = type;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.FIELD;
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            String retType = this.type;
            retType = GroovyUtils.stripPackage(retType);
            formatter.appendText(retType);
            return formatter.getText();
        }

        public ImageIcon getIcon() {
            if (groovyIcon == null) {
                // empty if block
            }
            return groovyIcon;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.singleton(Modifier.PROTECTED);
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(null, this.name, org.netbeans.modules.csl.api.ElementKind.FIELD, this.getModifiers());
        }
    }

    private static class JavaMethodItem
    extends CompletionItem {
        private final String className;
        private final String simpleName;
        private final List<String> parameters;
        private final String returnType;
        private final Set<javax.lang.model.element.Modifier> modifiers;
        private final boolean emphasise;
        private final boolean nameOnly;

        public JavaMethodItem(String className, String simpleName, List<String> parameters, TypeMirror returnType, Set<javax.lang.model.element.Modifier> modifiers, int anchorOffset, boolean emphasise, boolean nameOnly) {
            this(className, simpleName, parameters, Utilities.getTypeName(returnType, false).toString(), modifiers, anchorOffset, emphasise, nameOnly);
        }

        public JavaMethodItem(String className, String simpleName, List<String> parameters, String returnType, Set<javax.lang.model.element.Modifier> modifiers, int anchorOffset, boolean emphasise, boolean nameOnly) {
            super(null, anchorOffset);
            this.className = className;
            this.simpleName = simpleName;
            this.parameters = parameters;
            this.returnType = GroovyUtils.stripPackage(returnType);
            this.modifiers = modifiers;
            this.emphasise = emphasise;
            this.nameOnly = nameOnly;
        }

        @Override
        public String getName() {
            return this.simpleName + "()";
        }

        @Override
        public org.netbeans.modules.csl.api.ElementKind getKind() {
            return org.netbeans.modules.csl.api.ElementKind.METHOD;
        }

        public String getLhsHtml(HtmlFormatter formatter) {
            if (this.emphasise) {
                formatter.emphasis(true);
            }
            formatter.appendText(this.simpleName + "(" + this.getParameters() + ")");
            if (this.emphasise) {
                formatter.emphasis(false);
            }
            return formatter.getText();
        }

        private String getParameters() {
            StringBuilder sb = new StringBuilder();
            for (String string : this.parameters) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(GroovyUtils.stripPackage(string));
            }
            return sb.toString();
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            String retType = "";
            if (this.returnType != null) {
                retType = this.returnType;
            }
            formatter.appendText(retType);
            return formatter.getText();
        }

        public ImageIcon getIcon() {
            return (ImageIcon)ElementIcons.getElementIcon((ElementKind)ElementKind.METHOD, this.modifiers);
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Utilities.modelModifiersToGsf(this.modifiers);
        }

        @Override
        public ElementHandle getElement() {
            return ElementHandleSupport.createHandle(this.className, this.simpleName, org.netbeans.modules.csl.api.ElementKind.METHOD, this.getModifiers());
        }

        public String getCustomInsertTemplate() {
            if (this.nameOnly) {
                return this.simpleName;
            }
            return super.getCustomInsertTemplate();
        }
    }

}

