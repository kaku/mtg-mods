/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.api.completion.util;

import org.netbeans.modules.groovy.editor.api.AstPath;

public class DotCompletionContext {
    private final int lexOffset;
    private final int astOffset;
    private final AstPath astPath;
    private final boolean fieldsOnly;
    private final boolean methodsOnly;

    public DotCompletionContext(int lexOffset, int astOffset, AstPath astPath, boolean fieldsOnly, boolean methodsOnly) {
        this.lexOffset = lexOffset;
        this.astOffset = astOffset;
        this.astPath = astPath;
        this.fieldsOnly = fieldsOnly;
        this.methodsOnly = methodsOnly;
    }

    public int getLexOffset() {
        return this.lexOffset;
    }

    public int getAstOffset() {
        return this.astOffset;
    }

    public AstPath getAstPath() {
        return this.astPath;
    }

    public boolean isMethodsOnly() {
        return this.methodsOnly;
    }

    public boolean isFieldsOnly() {
        return this.fieldsOnly;
    }
}

