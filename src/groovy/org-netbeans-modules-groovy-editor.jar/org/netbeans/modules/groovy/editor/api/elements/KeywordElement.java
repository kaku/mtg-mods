/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementKind
 */
package org.netbeans.modules.groovy.editor.api.elements;

import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.GroovyElement;

public class KeywordElement
extends GroovyElement {
    private final String name;

    public KeywordElement(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ElementKind getKind() {
        return ElementKind.KEYWORD;
    }
}

