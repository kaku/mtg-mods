/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.MetaMethod
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.netbeans.modules.csl.api.ElementKind
 */
package org.netbeans.modules.groovy.editor.api.elements.ast;

import groovy.lang.MetaMethod;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.Parameter;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;

public class ASTMethod
extends ASTElement
implements MethodElement {
    private List<MethodElement.MethodParameter> parameters;
    private String returnType;
    private Class clz;
    private MetaMethod method;
    private boolean isGDK;

    public ASTMethod(ASTNode node) {
        this(node, null);
    }

    public ASTMethod(ASTNode node, String in) {
        super(node, in);
    }

    public ASTMethod(ASTNode node, Class clz, MetaMethod method, boolean isGDK) {
        super(node);
        this.clz = clz;
        this.method = method;
        this.isGDK = isGDK;
    }

    public boolean isGDK() {
        return this.isGDK;
    }

    public MetaMethod getMethod() {
        return this.method;
    }

    public Class getClz() {
        return this.clz;
    }

    @Override
    public List<MethodElement.MethodParameter> getParameters() {
        if (this.parameters == null) {
            this.parameters = new ArrayList<MethodElement.MethodParameter>();
            for (Parameter parameter : ((MethodNode)this.node).getParameters()) {
                String paramName = parameter.getName();
                String fqnType = parameter.getType().getName();
                String type = parameter.getType().getNameWithoutPackage();
                this.parameters.add(new MethodElement.MethodParameter(fqnType, type, paramName));
            }
        }
        return this.parameters;
    }

    @Override
    public List<String> getParameterTypes() {
        ArrayList<String> paramTypes = new ArrayList<String>();
        for (MethodElement.MethodParameter parameter : this.getParameters()) {
            paramTypes.add(parameter.getType());
        }
        return paramTypes;
    }

    @Override
    public String getSignature() {
        if (this.signature == null) {
            StringBuilder builder = new StringBuilder(super.getSignature());
            List<MethodElement.MethodParameter> params = this.getParameters();
            if (params.size() > 0) {
                builder.append("(");
                for (MethodElement.MethodParameter parameter : params) {
                    builder.append(parameter.getFqnType());
                    builder.append(",");
                }
                builder.setLength(builder.length() - 1);
                builder.append(")");
            }
            this.signature = builder.toString();
        }
        return this.signature;
    }

    @Override
    public String getName() {
        if (this.name == null) {
            if (this.node instanceof ConstructorNode) {
                this.name = ((ConstructorNode)this.node).getDeclaringClass().getNameWithoutPackage();
            } else if (this.node instanceof MethodNode) {
                this.name = ((MethodNode)this.node).getName();
            }
            if (this.name == null) {
                this.name = this.node.toString();
            }
        }
        return this.name;
    }

    @Override
    public String getReturnType() {
        if (this.returnType == null) {
            this.returnType = ((MethodNode)this.node).getReturnType().getNameWithoutPackage();
        }
        return this.returnType;
    }

    @Override
    public ElementKind getKind() {
        if (this.node instanceof ConstructorNode) {
            return ElementKind.CONSTRUCTOR;
        }
        if (this.node instanceof MethodNode) {
            return ElementKind.METHOD;
        }
        return ElementKind.OTHER;
    }
}

