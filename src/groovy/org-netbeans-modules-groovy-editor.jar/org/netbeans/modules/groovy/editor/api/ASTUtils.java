/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.GroovyCodeVisitor
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.VariableScope
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ClosureListExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.ExpressionStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.codehaus.groovy.ast.stmt.Statement
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.lexer.TokenUtilities
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Finder
 *  org.netbeans.editor.FinderFactory
 *  org.netbeans.editor.FinderFactory$StringFwdFinder
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.GroovyCodeVisitor;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.VariableScope;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ClosureListExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.lexer.TokenUtilities;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Finder;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.ElementUtils;
import org.netbeans.modules.groovy.editor.api.StructureAnalyzer;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTRoot;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.occurrences.VariableScopeVisitor;
import org.netbeans.modules.groovy.editor.utils.ASTChildrenVisitor;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class ASTUtils {
    private static final Logger LOGGER = Logger.getLogger(ASTUtils.class.getName());

    public static int getAstOffset(Parser.Result info, int lexOffset) {
        GroovyParserResult result = ASTUtils.getParseResult(info);
        if (result != null) {
            return result.getSnapshot().getEmbeddedOffset(lexOffset);
        }
        return lexOffset;
    }

    public static GroovyParserResult getParseResult(Parser.Result info) {
        assert (info instanceof GroovyParserResult);
        return (GroovyParserResult)info;
    }

    public static ModuleNode getRoot(ParserResult r) {
        assert (r instanceof GroovyParserResult);
        GroovyParserResult result = (GroovyParserResult)r;
        if (result.getRootElement() == null) {
            return null;
        }
        return result.getRootElement().getModuleNode();
    }

    public static OffsetRange getRangeFull(ASTNode node, BaseDocument doc) {
        int end;
        if (node.getLineNumber() < 0 || node.getColumnNumber() < 0 || node.getLastLineNumber() < 0 || node.getLastColumnNumber() < 0) {
            return OffsetRange.NONE;
        }
        int start = ASTUtils.getOffset(doc, node.getLineNumber(), node.getColumnNumber());
        if (start < 0) {
            start = 0;
        }
        if ((end = ASTUtils.getOffset(doc, node.getLastLineNumber(), node.getLastColumnNumber())) < 0) {
            end = 0;
        }
        if (start > end) {
            return OffsetRange.NONE;
        }
        return new OffsetRange(start, end);
    }

    @NonNull
    public static OffsetRange getRange(ASTNode node, BaseDocument doc) {
        int lineNumber = node.getLineNumber();
        int columnNumber = node.getColumnNumber();
        if (lineNumber < 1 || columnNumber < 1) {
            return OffsetRange.NONE;
        }
        if (doc == null) {
            LOGGER.log(Level.INFO, "Null document in getRange()");
            return OffsetRange.NONE;
        }
        if (node instanceof FieldNode) {
            int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
            FieldNode fieldNode = (FieldNode)node;
            return ASTUtils.getNextIdentifierByName(doc, fieldNode.getName(), start);
        }
        if (node instanceof ClassNode) {
            ClassNode classNode = (ClassNode)node;
            int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
            if (classNode.isScript()) {
                return ASTUtils.getNextIdentifierByName(doc, classNode.getNameWithoutPackage(), start);
            }
            if (doc != null) {
                int end;
                int docLength = doc.getLength();
                int limit = ASTUtils.getLimit(node, doc, docLength);
                try {
                    start = doc.find((Finder)new FinderFactory.StringFwdFinder("class", true), start, limit) + "class".length();
                }
                catch (BadLocationException ex) {
                    LOGGER.log(Level.WARNING, null, ex);
                }
                if (start > docLength) {
                    start = docLength;
                }
                try {
                    start = Utilities.getFirstNonWhiteFwd((BaseDocument)doc, (int)start);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                if (start < 0) {
                    start = 0;
                }
                if ((end = start + classNode.getNameWithoutPackage().length()) > docLength) {
                    end = docLength;
                }
                if (start == end) {
                    return OffsetRange.NONE;
                }
                return new OffsetRange(start, end);
            }
        } else {
            if (node instanceof ConstructorNode) {
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                ConstructorNode constructorNode = (ConstructorNode)node;
                return ASTUtils.getNextIdentifierByName(doc, constructorNode.getDeclaringClass().getNameWithoutPackage(), start);
            }
            if (node instanceof MethodNode) {
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                MethodNode methodNode = (MethodNode)node;
                return ASTUtils.getNextIdentifierByName(doc, methodNode.getName(), start);
            }
            if (node instanceof VariableExpression) {
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                VariableExpression variableExpression = (VariableExpression)node;
                return ASTUtils.getNextIdentifierByName(doc, variableExpression.getName(), start);
            }
            if (node instanceof Parameter) {
                int docLength = doc.getLength();
                int start = ASTUtils.getOffset(doc, node.getLineNumber(), node.getColumnNumber());
                int limit = ASTUtils.getLimit(node, doc, docLength);
                Parameter parameter = (Parameter)node;
                String name = parameter.getName();
                try {
                    start = doc.find((Finder)new FinderFactory.StringFwdFinder(name, true), start, limit);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                int end = start + name.length();
                if (end > docLength) {
                    return OffsetRange.NONE;
                }
                return ASTUtils.getNextIdentifierByName(doc, name, start);
            }
            if (node instanceof MethodCallExpression) {
                MethodCallExpression methodCall = (MethodCallExpression)node;
                Expression method = methodCall.getMethod();
                lineNumber = method.getLineNumber();
                columnNumber = method.getColumnNumber();
                if (lineNumber < 1 || columnNumber < 1) {
                    lineNumber = 1;
                    columnNumber = 1;
                }
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                return new OffsetRange(start, start + methodCall.getMethodAsString().length());
            }
            if (node instanceof ConstructorCallExpression) {
                ConstructorCallExpression methodCall = (ConstructorCallExpression)node;
                String name = methodCall.getType().getNameWithoutPackage();
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber + 4);
                return ASTUtils.getNextIdentifierByName(doc, name, start);
            }
            if (node instanceof ClassExpression) {
                ClassExpression clazz = (ClassExpression)node;
                String name = clazz.getType().getNameWithoutPackage();
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                return ASTUtils.getNextIdentifierByName(doc, name, start);
            }
            if (node instanceof ConstantExpression) {
                ConstantExpression constantExpression = (ConstantExpression)node;
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                return new OffsetRange(start, start + constantExpression.getText().length());
            }
            if (node instanceof FakeASTNode) {
                String typeName = ElementUtils.getTypeNameWithoutPackage(((FakeASTNode)node).getOriginalNode());
                int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
                return ASTUtils.getNextIdentifierByName(doc, typeName, start);
            }
        }
        return OffsetRange.NONE;
    }

    public static List<ASTNode> children(ASTNode root) {
        List children = new ArrayList<ASTNode>();
        if (root instanceof ModuleNode) {
            ModuleNode moduleNode = (ModuleNode)root;
            children.addAll(moduleNode.getClasses());
            children.add((BlockStatement)moduleNode.getStatementBlock());
        } else if (root instanceof ClassNode) {
            ClassNode classNode = (ClassNode)root;
            HashSet<String> possibleMethods = new HashSet<String>();
            for (Object object : classNode.getProperties()) {
                PropertyNode property = (PropertyNode)object;
                if (property.getLineNumber() < 0) continue;
                children.add((PropertyNode)property);
                FieldNode field = property.getField();
                String fieldName = field.getName();
                String fieldTypeName = field.getType().getNameWithoutPackage();
                if (fieldName.length() <= 0 || field.isStatic() || (field.getModifiers() & 2) == 0) continue;
                fieldName = "" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1, fieldName.length());
                if (!field.isFinal()) {
                    possibleMethods.add("set" + fieldName);
                }
                possibleMethods.add("get" + fieldName);
                if (!"Boolean".equals(fieldTypeName) && !"boolean".equals(fieldTypeName)) continue;
                possibleMethods.add("is" + fieldName);
            }
            for (FieldNode field : classNode.getFields()) {
                if (field.getLineNumber() < 0) continue;
                children.add(field);
            }
            for (MethodNode method : classNode.getMethods()) {
                if ((method.isSynthetic() || method.getCode() == null) && (!method.isSynthetic() || !possibleMethods.contains(method.getName()))) continue;
                children.add(method);
            }
            for (ConstructorNode constructor : classNode.getDeclaredConstructors()) {
                if (constructor.getLineNumber() < 0) continue;
                children.add(constructor);
            }
        } else if (root instanceof MethodNode) {
            MethodNode methodNode = (MethodNode)root;
            children.add((Statement)methodNode.getCode());
            children.addAll(Arrays.asList(methodNode.getParameters()));
        } else if (!(root instanceof Parameter)) {
            if (root instanceof FieldNode) {
                FieldNode fieldNode = (FieldNode)root;
                Expression expression = fieldNode.getInitialExpression();
                if (expression != null) {
                    children.add((Expression)expression);
                }
            } else if (!(root instanceof PropertyNode) && root != null) {
                ASTChildrenVisitor astChildrenSupport = new ASTChildrenVisitor();
                root.visit((GroovyCodeVisitor)astChildrenSupport);
                children = astChildrenSupport.children();
            }
        }
        return children;
    }

    public static int getOffset(BaseDocument doc, int lineNumber, int columnNumber) {
        assert (lineNumber > 0);
        assert (columnNumber > 0);
        int offset = Utilities.getRowStartFromLineOffset((BaseDocument)doc, (int)(lineNumber - 1));
        if ((offset += columnNumber - 1) < 0) {
            offset = 0;
        }
        return offset;
    }

    public static ASTNode getForeignNode(final IndexedElement o) {
        final ASTNode[] nodes = new ASTNode[1];
        FileObject fileObject = o.getFileObject();
        assert (fileObject != null);
        try {
            Source source = Source.create((FileObject)fileObject);
            ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    GroovyParserResult result = ASTUtils.getParseResult(resultIterator.getParserResult());
                    String signature = o.getSignature();
                    if (signature == null) {
                        return;
                    }
                    int index = signature.indexOf(35);
                    if (index != -1) {
                        signature = signature.substring(index + 1);
                    }
                    for (ASTElement element : result.getStructure().getElements()) {
                        ASTNode node = ASTUtils.findBySignature(element, signature);
                        if (node == null) continue;
                        nodes[0] = node;
                        return;
                    }
                }
            });
        }
        catch (ParseException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return nodes[0];
    }

    private static ASTNode findBySignature(ASTElement root, String signature) {
        if (signature.equals(root.getSignature())) {
            return root.getNode();
        }
        for (ASTElement element : root.getChildren()) {
            ASTNode node = ASTUtils.findBySignature(element, signature);
            if (node == null) continue;
            return node;
        }
        return null;
    }

    public static String getDefSignature(MethodNode node) {
        StringBuilder sb = new StringBuilder();
        sb.append(node.getName());
        Parameter[] parameters = node.getParameters();
        if (parameters.length > 0) {
            sb.append('(');
            Iterator<Parameter> it = Arrays.asList(parameters).iterator();
            sb.append(org.netbeans.modules.groovy.editor.java.Utilities.translateClassLoaderTypeName(it.next().getType().getName()));
            while (it.hasNext()) {
                sb.append(',');
                sb.append(org.netbeans.modules.groovy.editor.java.Utilities.translateClassLoaderTypeName(it.next().getType().getName()));
            }
            sb.append(')');
        }
        return sb.toString();
    }

    public static OffsetRange getNextIdentifierByName(final BaseDocument doc, String fieldName, final int startOffset) {
        final String identifier = fieldName.endsWith("[]") ? fieldName.substring(0, fieldName.length() - 2) : fieldName;
        final OffsetRange[] result = new OffsetRange[]{OffsetRange.NONE};
        doc.render(new Runnable(){

            @Override
            public void run() {
                TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(doc, startOffset);
                if (ts != null) {
                    Token token = ts.token();
                    if (token != null && token.id() == GroovyTokenId.IDENTIFIER && TokenUtilities.endsWith((CharSequence)identifier, (CharSequence)token.text())) {
                        result[0] = this.computeRange(ts, token);
                        return;
                    }
                    while (ts.moveNext()) {
                        token = ts.token();
                        if (token == null || token.id() != GroovyTokenId.IDENTIFIER || !TokenUtilities.endsWith((CharSequence)identifier, (CharSequence)token.text())) continue;
                        result[0] = this.computeRange(ts, token);
                        return;
                    }
                }
            }

            private OffsetRange computeRange(TokenSequence<GroovyTokenId> ts, Token<GroovyTokenId> token) {
                int start = ts.offset() + token.text().length() - identifier.length();
                int end = ts.offset() + token.text().length();
                if (start < 0) {
                    start = 0;
                }
                return new OffsetRange(start, end);
            }
        });
        return result[0];
    }

    public static String getFqnName(AstPath path) {
        ClassNode classNode = ASTUtils.getOwningClass(path);
        return classNode == null ? "" : classNode.getName();
    }

    public static ClassNode getOwningClass(AstPath path) {
        ListIterator<ASTNode> it = path.rootToLeaf();
        while (it.hasNext()) {
            ASTNode node = it.next();
            if (!(node instanceof ClassNode)) continue;
            return (ClassNode)node;
        }
        return null;
    }

    public static ASTNode getScope(AstPath path, Variable variable) {
        for (ASTNode scope : path) {
            Statement statement;
            VariableScope variableScope;
            if (scope instanceof ClosureExpression) {
                variableScope = ((ClosureExpression)scope).getVariableScope();
                if (variableScope.getDeclaredVariable(variable.getName()) != null) {
                    return scope;
                }
                statement = ((ClosureExpression)scope).getCode();
                if (!(statement instanceof BlockStatement) || (variableScope = ((BlockStatement)statement).getVariableScope()).getDeclaredVariable(variable.getName()) == null) continue;
                return scope;
            }
            if (scope instanceof MethodNode || scope instanceof ConstructorNode) {
                variableScope = ((MethodNode)scope).getVariableScope();
                if (variableScope.getDeclaredVariable(variable.getName()) != null) {
                    return scope;
                }
                statement = ((MethodNode)scope).getCode();
                if (!(statement instanceof BlockStatement) || (variableScope = ((BlockStatement)statement).getVariableScope()).getDeclaredVariable(variable.getName()) == null) continue;
                return scope;
            }
            if (scope instanceof ForStatement) {
                variableScope = ((ForStatement)scope).getVariableScope();
                if (variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                return scope;
            }
            if (scope instanceof BlockStatement) {
                variableScope = ((BlockStatement)scope).getVariableScope();
                if (variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                return scope;
            }
            if (scope instanceof ClosureListExpression) {
                variableScope = ((ClosureListExpression)scope).getVariableScope();
                if (variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                return scope;
            }
            if (scope instanceof ClassNode) {
                ClassNode classNode = (ClassNode)scope;
                if (classNode.getField(variable.getName()) == null) continue;
                return scope;
            }
            if (!(scope instanceof ModuleNode)) continue;
            ModuleNode moduleNode = (ModuleNode)scope;
            BlockStatement blockStatement = moduleNode.getStatementBlock();
            VariableScope variableScope2 = blockStatement.getVariableScope();
            if (variableScope2.getDeclaredVariable(variable.getName()) != null) {
                return blockStatement;
            }
            Variable classVariable = variableScope2.getReferencedClassVariable(variable.getName());
            if (classVariable == null) continue;
            return moduleNode;
        }
        return null;
    }

    public static ASTNode getVariable(ASTNode scope, String variable, AstPath path, BaseDocument doc, int cursorOffset) {
        if (scope instanceof ClosureExpression) {
            ClosureExpression closure = (ClosureExpression)scope;
            for (Parameter parameter : closure.getParameters()) {
                if (!variable.equals(parameter.getName())) continue;
                return parameter;
            }
            Statement code = closure.getCode();
            if (code instanceof BlockStatement) {
                return ASTUtils.getVariableInBlockStatement((BlockStatement)code, variable);
            }
        } else if (scope instanceof MethodNode) {
            MethodNode method = (MethodNode)scope;
            for (Parameter parameter : method.getParameters()) {
                if (!variable.equals(parameter.getName())) continue;
                return parameter;
            }
            Statement code = method.getCode();
            if (code instanceof BlockStatement) {
                return ASTUtils.getVariableInBlockStatement((BlockStatement)code, variable);
            }
        } else if (scope instanceof ConstructorNode) {
            ConstructorNode constructor = (ConstructorNode)scope;
            for (Parameter parameter : constructor.getParameters()) {
                if (!variable.equals(parameter.getName())) continue;
                return parameter;
            }
            Statement code = constructor.getCode();
            if (code instanceof BlockStatement) {
                return ASTUtils.getVariableInBlockStatement((BlockStatement)code, variable);
            }
        } else if (scope instanceof ForStatement) {
            ASTNode result;
            ASTNode result2;
            ForStatement forStatement = (ForStatement)scope;
            Parameter parameter = forStatement.getVariable();
            if (variable.equals(parameter.getName())) {
                return parameter;
            }
            Expression collectionExpression = forStatement.getCollectionExpression();
            if (collectionExpression instanceof ClosureListExpression && (result = ASTUtils.getVariableInClosureList((ClosureListExpression)collectionExpression, variable)) != null) {
                return result;
            }
            Statement code = forStatement.getLoopBlock();
            if (code instanceof BlockStatement && (result2 = ASTUtils.getVariableInBlockStatement((BlockStatement)code, variable)) != null) {
                return result2;
            }
        } else {
            if (scope instanceof BlockStatement) {
                return ASTUtils.getVariableInBlockStatement((BlockStatement)scope, variable);
            }
            if (scope instanceof ClosureListExpression) {
                return ASTUtils.getVariableInClosureList((ClosureListExpression)scope, variable);
            }
            if (scope instanceof ClassNode) {
                return ((ClassNode)scope).getField(variable);
            }
            if (scope instanceof ModuleNode) {
                VariableScope variableScope;
                ModuleNode moduleNode = (ModuleNode)scope;
                BlockStatement blockStatement = moduleNode.getStatementBlock();
                ASTNode result = ASTUtils.getVariableInBlockStatement(blockStatement, variable);
                if (result == null && (variableScope = blockStatement.getVariableScope()).getReferencedClassVariable(variable) != null) {
                    VariableScopeVisitor scopeVisitor = new VariableScopeVisitor(moduleNode.getContext(), path, doc, cursorOffset);
                    scopeVisitor.collect();
                    Set<ASTNode> occurrences = scopeVisitor.getOccurrences();
                    if (!occurrences.isEmpty()) {
                        result = occurrences.iterator().next();
                    }
                }
                return result;
            }
        }
        return null;
    }

    private static ASTNode getVariableInBlockStatement(BlockStatement block, String variable) {
        for (Object object : block.getStatements()) {
            DeclarationExpression declaration;
            ExpressionStatement expressionStatement;
            Expression expression;
            if (!(object instanceof ExpressionStatement) || !((expression = (expressionStatement = (ExpressionStatement)object).getExpression()) instanceof DeclarationExpression) || !variable.equals((declaration = (DeclarationExpression)expression).getVariableExpression().getName())) continue;
            return declaration.getVariableExpression();
        }
        return null;
    }

    private static ASTNode getVariableInClosureList(ClosureListExpression closureList, String variable) {
        for (Object object : closureList.getExpressions()) {
            DeclarationExpression declaration;
            if (!(object instanceof DeclarationExpression) || !variable.equals((declaration = (DeclarationExpression)object).getVariableExpression().getName())) continue;
            return declaration.getVariableExpression();
        }
        return null;
    }

    private static int getLimit(ASTNode node, BaseDocument doc, int docLength) {
        int limit;
        int n = limit = node.getLastLineNumber() > 0 && node.getLastColumnNumber() > 0 ? ASTUtils.getOffset(doc, node.getLastLineNumber(), node.getLastColumnNumber()) : docLength;
        if (limit > docLength) {
            limit = docLength;
        }
        return limit;
    }

    public static final class FakeASTNode
    extends ASTNode {
        private final String name;
        private final ASTNode node;

        public FakeASTNode(ASTNode node) {
            this(node, node.getText());
        }

        public FakeASTNode(ASTNode node, String name) {
            this.node = node;
            this.name = name;
            this.setLineNumber(node.getLineNumber());
            this.setColumnNumber(node.getColumnNumber());
            this.setLastLineNumber(node.getLastLineNumber());
            this.setLastColumnNumber(node.getLastColumnNumber());
        }

        public ASTNode getOriginalNode() {
            return this.node;
        }

        public String getText() {
            return this.name;
        }

        public void visit(GroovyCodeVisitor visitor) {
        }

        public int hashCode() {
            int hash = 7;
            hash = 71 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 71 * hash + this.getLineNumber();
            hash = 71 * hash + this.getColumnNumber();
            hash = 71 * hash + this.getLastLineNumber();
            hash = 71 * hash + this.getLastColumnNumber();
            return hash;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            FakeASTNode other = (FakeASTNode)((Object)obj);
            if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
                return false;
            }
            if (this.getLineNumber() != other.getLineNumber()) {
                return false;
            }
            if (this.getColumnNumber() != other.getColumnNumber()) {
                return false;
            }
            if (this.getLastLineNumber() != other.getLastLineNumber()) {
                return false;
            }
            if (this.getLastColumnNumber() != other.getLastColumnNumber()) {
                return false;
            }
            return true;
        }
    }

}

