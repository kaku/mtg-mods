/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.KeystrokeHandler
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.language;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Exceptions;

public class GroovyBracketCompleter
implements KeystrokeHandler {
    public boolean beforeCharInserted(Document doc, int caretOffset, JTextComponent target, char ch) throws BadLocationException {
        return false;
    }

    public boolean afterCharInserted(Document doc, int caretOffset, JTextComponent target, char ch) throws BadLocationException {
        return false;
    }

    public boolean charBackspaced(Document doc, int caretOffset, JTextComponent target, char ch) throws BadLocationException {
        return false;
    }

    public int beforeBreak(Document doc, int caretOffset, JTextComponent target) throws BadLocationException {
        return -1;
    }

    public OffsetRange findMatching(Document doc, int caretOffset) {
        return OffsetRange.NONE;
    }

    public List<OffsetRange> findLogicalRanges(ParserResult info, int caretOffset) {
        ArrayList<OffsetRange> ranges;
        ModuleNode root = ASTUtils.getRoot(info);
        if (root == null) {
            return Collections.emptyList();
        }
        GroovyParserResult gpr = ASTUtils.getParseResult((Parser.Result)info);
        int astOffset = ASTUtils.getAstOffset((Parser.Result)info, caretOffset);
        if (astOffset == -1) {
            return Collections.emptyList();
        }
        ranges = new ArrayList<OffsetRange>();
        int min = 0;
        int max = Integer.MAX_VALUE;
        try {
            BaseDocument doc = LexUtilities.getDocument(gpr, false);
            if (doc == null) {
                return ranges;
            }
            AstPath path = new AstPath((ASTNode)root, astOffset, doc);
            int length = doc.getLength();
            TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(doc, caretOffset);
            if (ts != null) {
                int end;
                int begin;
                Token token = ts.token();
                if (token != null && token.id() == GroovyTokenId.BLOCK_COMMENT) {
                    begin = ts.offset();
                    end = begin + token.length();
                    ranges.add(new OffsetRange(begin, end));
                } else if (token != null && token.id() == GroovyTokenId.LINE_COMMENT) {
                    begin = Utilities.getRowStart((BaseDocument)doc, (int)caretOffset);
                    end = Utilities.getRowEnd((BaseDocument)doc, (int)caretOffset);
                    if (LexUtilities.isCommentOnlyLine(doc, caretOffset)) {
                        ranges.add(new OffsetRange(Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)begin), Utilities.getRowLastNonWhite((BaseDocument)doc, (int)end) + 1));
                        int lineBegin = begin;
                        int lineEnd = end;
                        while (begin > 0) {
                            int newBegin = Utilities.getRowStart((BaseDocument)doc, (int)(begin - 1));
                            if (newBegin < 0 || !LexUtilities.isCommentOnlyLine(doc, newBegin)) {
                                begin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)begin);
                                break;
                            }
                            begin = newBegin;
                        }
                        do {
                            int newEnd;
                            if ((newEnd = Utilities.getRowEnd((BaseDocument)doc, (int)(end + 1))) >= length || !LexUtilities.isCommentOnlyLine(doc, newEnd)) break;
                            end = newEnd;
                        } while (true);
                        end = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)end) + 1;
                        if (lineBegin > begin || lineEnd < end) {
                            ranges.add(new OffsetRange(begin, end));
                        }
                    } else {
                        TokenHierarchy th = TokenHierarchy.get((Document)doc);
                        int offset = token.offset(th);
                        ranges.add(new OffsetRange(offset, offset + token.length()));
                    }
                }
            }
            ListIterator<ASTNode> it = path.leafToRoot();
            OffsetRange previous = OffsetRange.NONE;
            while (it.hasNext()) {
                ASTNode node = it.next();
                OffsetRange range = ASTUtils.getRange(node, doc);
                if (!range.containsInclusive(astOffset) || range.equals((Object)previous) || (range = LexUtilities.getLexerOffsets(gpr, range)) == OffsetRange.NONE) continue;
                if (range.getStart() < min) {
                    ranges.add(new OffsetRange(min, max));
                    ranges.add(new OffsetRange(0, length));
                    break;
                }
                ranges.add(range);
                previous = range;
            }
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
            return ranges;
        }
        return ranges;
    }

    public int getNextWordOffset(Document document, int offset, boolean reverse) {
        BaseDocument doc = (BaseDocument)document;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts == null) {
            return -1;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return -1;
        }
        if (reverse && ts.offset() == offset && !ts.movePrevious()) {
            return -1;
        }
        Token token = ts.token();
        TokenId id = token.id();
        if (id == GroovyTokenId.WHITESPACE) {
            int start;
            if (reverse && ts.offset() < offset || !reverse && ts.offset() > offset) {
                return ts.offset();
            }
            while (id == GroovyTokenId.WHITESPACE) {
                if (reverse && !ts.movePrevious()) {
                    return -1;
                }
                if (!reverse && !ts.moveNext()) {
                    return -1;
                }
                token = ts.token();
                id = token.id();
            }
            if (reverse ? (start = ts.offset() + token.length()) < offset : (start = ts.offset()) > offset) {
                return start;
            }
        }
        if (id == GroovyTokenId.IDENTIFIER) {
            char charAtI;
            int i;
            String s = token.text().toString();
            int length = s.length();
            int wordOffset = offset - ts.offset();
            if (reverse) {
                int offsetInImage = offset - 1 - ts.offset();
                if (offsetInImage < 0) {
                    return -1;
                }
                if (offsetInImage < length && Character.isUpperCase(s.charAt(offsetInImage))) {
                    for (int i2 = offsetInImage - 1; i2 >= 0; --i2) {
                        char charAtI2 = s.charAt(i2);
                        if (charAtI2 == '_') {
                            return ts.offset() + i2 + 1;
                        }
                        if (Character.isUpperCase(charAtI2)) continue;
                        return ts.offset() + i2 + 1;
                    }
                    return ts.offset();
                }
                for (int i3 = offsetInImage - 1; i3 >= 0; --i3) {
                    char charAtI3 = s.charAt(i3);
                    if (charAtI3 == '_') {
                        return ts.offset() + i3 + 1;
                    }
                    if (!Character.isUpperCase(charAtI3)) continue;
                    for (int j = i3; j >= 0; --j) {
                        char charAtJ = s.charAt(j);
                        if (charAtJ == '_') {
                            return ts.offset() + j + 1;
                        }
                        if (Character.isUpperCase(charAtJ)) continue;
                        return ts.offset() + j + 1;
                    }
                    return ts.offset();
                }
                return ts.offset();
            }
            int start = wordOffset + 1;
            if (wordOffset < 0 || wordOffset >= s.length()) {
                return -1;
            }
            if (Character.isUpperCase(s.charAt(wordOffset))) {
                for (i = start; i < length && Character.isUpperCase(charAtI = s.charAt(i)); ++i) {
                    if (s.charAt(i) == '_') {
                        return ts.offset() + i;
                    }
                    ++start;
                }
            }
            for (i = start; i < length; ++i) {
                charAtI = s.charAt(i);
                if (charAtI != '_' && !Character.isUpperCase(charAtI)) continue;
                return ts.offset() + i;
            }
        }
        return -1;
    }
}

