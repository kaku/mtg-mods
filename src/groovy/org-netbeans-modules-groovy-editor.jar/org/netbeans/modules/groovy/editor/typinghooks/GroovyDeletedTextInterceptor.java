/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Factory
 */
package org.netbeans.modules.groovy.editor.typinghooks;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;

public class GroovyDeletedTextInterceptor
implements DeletedTextInterceptor {
    public boolean beforeRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
        return false;
    }

    public void remove(DeletedTextInterceptor.Context context) throws BadLocationException {
        BaseDocument doc = (BaseDocument)context.getDocument();
        char ch = context.getText().charAt(0);
        int dotPos = context.getOffset() - 1;
        switch (ch) {
            case '(': 
            case '[': 
            case '{': {
                char tokenAtDot = LexUtilities.getTokenChar(doc, dotPos);
                if (!(tokenAtDot == ']' && LexUtilities.getTokenBalance(doc, GroovyTokenId.LBRACKET, GroovyTokenId.RBRACKET, dotPos) != 0 || tokenAtDot == ')' && LexUtilities.getTokenBalance(doc, GroovyTokenId.LPAREN, GroovyTokenId.RPAREN, dotPos) != 0) && (tokenAtDot != '}' || LexUtilities.getTokenBalance(doc, GroovyTokenId.LBRACE, GroovyTokenId.RBRACE, dotPos) == 0)) break;
                doc.remove(dotPos, 1);
                break;
            }
            case '\"': 
            case '\'': 
            case '|': {
                char[] match = doc.getChars(dotPos, 1);
                if (match == null || match[0] != ch) break;
                doc.remove(dotPos, 1);
            }
        }
    }

    public void afterRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
    }

    public void cancelled(DeletedTextInterceptor.Context context) {
    }

    public static class GroovyDeletedTextInterceptorFactory
    implements DeletedTextInterceptor.Factory {
        public DeletedTextInterceptor createDeletedTextInterceptor(MimePath mimePath) {
            return new GroovyDeletedTextInterceptor();
        }
    }

}

