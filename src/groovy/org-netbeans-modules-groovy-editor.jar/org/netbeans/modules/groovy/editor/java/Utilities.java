/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.modules.csl.api.Modifier
 */
package org.netbeans.modules.groovy.editor.java;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.SimpleTypeVisitor6;
import org.netbeans.api.java.source.SourceUtils;

public final class Utilities {
    private static final String CAPTURED_WILDCARD = "<captured wildcard>";
    private static final String UNKNOWN = "<unknown>";
    private static final Map<Character, String> NATIVE_TYPES = new HashMap<Character, String>();

    public static String translateClassLoaderTypeName(String type) {
        if (type.length() < 1) {
            return type;
        }
        char start = type.charAt(0);
        if ('L' == start && type.charAt(type.length() - 1) == ';') {
            return type.substring(1, type.length() - 1);
        }
        if ('[' == start) {
            if (type.length() < 2) {
                throw new IllegalArgumentException("Not a correct type: " + type);
            }
            return Utilities.translateClassLoaderTypeName(type.substring(1)) + "[]";
        }
        if (type.length() == 1) {
            String result = NATIVE_TYPES.get(Character.valueOf(start));
            if (result != null) {
                return result;
            }
            return type;
        }
        return type;
    }

    public static Set<javax.lang.model.element.Modifier> reflectionModifiersToModel(int modifiers) {
        HashSet<javax.lang.model.element.Modifier> ret = new HashSet<javax.lang.model.element.Modifier>();
        if (Modifier.isAbstract(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.ABSTRACT);
        }
        if (Modifier.isFinal(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.FINAL);
        }
        if (Modifier.isNative(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.NATIVE);
        }
        if (Modifier.isStatic(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.STATIC);
        }
        if (Modifier.isStrict(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.STRICTFP);
        }
        if (Modifier.isSynchronized(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.SYNCHRONIZED);
        }
        if (Modifier.isTransient(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.TRANSIENT);
        }
        if (Modifier.isVolatile(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.VOLATILE);
        }
        if (Modifier.isPrivate(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.PRIVATE);
        } else if (Modifier.isProtected(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.PROTECTED);
        } else if (Modifier.isPublic(modifiers)) {
            ret.add(javax.lang.model.element.Modifier.PUBLIC);
        }
        return ret;
    }

    public static Set<javax.lang.model.element.Modifier> gsfModifiersToModel(Set<org.netbeans.modules.csl.api.Modifier> modifiers, javax.lang.model.element.Modifier defaultModifier) {
        HashSet<javax.lang.model.element.Modifier> ret = new HashSet<javax.lang.model.element.Modifier>();
        if (modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.STATIC)) {
            ret.add(javax.lang.model.element.Modifier.STATIC);
        }
        if (modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PRIVATE)) {
            ret.add(javax.lang.model.element.Modifier.PRIVATE);
        } else if (modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PROTECTED)) {
            ret.add(javax.lang.model.element.Modifier.PROTECTED);
        } else if (modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PUBLIC)) {
            ret.add(javax.lang.model.element.Modifier.PUBLIC);
        } else if (defaultModifier != null) {
            ret.add(defaultModifier);
        }
        return ret;
    }

    public static Set<org.netbeans.modules.csl.api.Modifier> modelModifiersToGsf(Set<javax.lang.model.element.Modifier> modifiers) {
        LinkedHashSet<org.netbeans.modules.csl.api.Modifier> ret = new LinkedHashSet<org.netbeans.modules.csl.api.Modifier>();
        if (modifiers.contains((Object)javax.lang.model.element.Modifier.STATIC)) {
            ret.add(org.netbeans.modules.csl.api.Modifier.STATIC);
        }
        if (modifiers.contains((Object)javax.lang.model.element.Modifier.PRIVATE)) {
            ret.add(org.netbeans.modules.csl.api.Modifier.PRIVATE);
        } else if (modifiers.contains((Object)javax.lang.model.element.Modifier.PROTECTED)) {
            ret.add(org.netbeans.modules.csl.api.Modifier.PROTECTED);
        } else if (modifiers.contains((Object)javax.lang.model.element.Modifier.PUBLIC)) {
            ret.add(org.netbeans.modules.csl.api.Modifier.PUBLIC);
        }
        return ret;
    }

    public static CharSequence getTypeName(TypeMirror type, boolean fqn) {
        return Utilities.getTypeName(type, fqn, false);
    }

    public static CharSequence getClassName(TypeMirror type) {
        assert (type != null);
        return (CharSequence)new ClassNameVisitor().visit(type, null);
    }

    public static CharSequence getTypeName(TypeMirror type, boolean fqn, boolean varArg) {
        if (type == null) {
            return "";
        }
        return (CharSequence)new TypeNameVisitor(varArg).visit(type, fqn);
    }

    static {
        NATIVE_TYPES.put(Character.valueOf('B'), "byte");
        NATIVE_TYPES.put(Character.valueOf('C'), "char");
        NATIVE_TYPES.put(Character.valueOf('D'), "double");
        NATIVE_TYPES.put(Character.valueOf('F'), "float");
        NATIVE_TYPES.put(Character.valueOf('I'), "int");
        NATIVE_TYPES.put(Character.valueOf('J'), "long");
        NATIVE_TYPES.put(Character.valueOf('S'), "short");
        NATIVE_TYPES.put(Character.valueOf('Z'), "boolean");
    }

    private static class ClassNameVisitor
    extends SimpleTypeVisitor6<StringBuilder, Void> {
        private ClassNameVisitor() {
            super(new StringBuilder());
        }

        @Override
        public StringBuilder defaultAction(TypeMirror t, Void p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append(t);
        }

        @Override
        public StringBuilder visitDeclared(DeclaredType t, Void p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                return ((StringBuilder)this.DEFAULT_VALUE).append(te.getQualifiedName().toString());
            }
            return ((StringBuilder)this.DEFAULT_VALUE).append("<unknown>");
        }

        @Override
        public StringBuilder visitError(ErrorType t, Void p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                return ((StringBuilder)this.DEFAULT_VALUE).append(te.getQualifiedName().toString());
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }
    }

    private static class TypeNameVisitor
    extends SimpleTypeVisitor6<StringBuilder, Boolean> {
        private boolean varArg;
        private boolean insideCapturedWildcard = false;

        private TypeNameVisitor(boolean varArg) {
            super(new StringBuilder());
            this.varArg = varArg;
        }

        @Override
        public StringBuilder defaultAction(TypeMirror t, Boolean p) {
            return ((StringBuilder)this.DEFAULT_VALUE).append(t);
        }

        @Override
        public StringBuilder visitDeclared(DeclaredType t, Boolean p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? te.getQualifiedName() : te.getSimpleName()).toString());
                Iterator<? extends TypeMirror> it = t.getTypeArguments().iterator();
                if (it.hasNext()) {
                    ((StringBuilder)this.DEFAULT_VALUE).append("<");
                    while (it.hasNext()) {
                        this.visit(it.next(), p);
                        if (!it.hasNext()) continue;
                        ((StringBuilder)this.DEFAULT_VALUE).append(", ");
                    }
                    ((StringBuilder)this.DEFAULT_VALUE).append(">");
                }
                return (StringBuilder)this.DEFAULT_VALUE;
            }
            return ((StringBuilder)this.DEFAULT_VALUE).append("<unknown>");
        }

        @Override
        public StringBuilder visitArray(ArrayType t, Boolean p) {
            boolean isVarArg = this.varArg;
            this.varArg = false;
            this.visit(t.getComponentType(), p);
            return ((StringBuilder)this.DEFAULT_VALUE).append(isVarArg ? "..." : "[]");
        }

        @Override
        public StringBuilder visitTypeVariable(TypeVariable t, Boolean p) {
            String name;
            Element e = t.asElement();
            if (e != null && !"<captured wildcard>".equals(name = e.getSimpleName().toString())) {
                return ((StringBuilder)this.DEFAULT_VALUE).append(name);
            }
            ((StringBuilder)this.DEFAULT_VALUE).append("?");
            if (!this.insideCapturedWildcard) {
                this.insideCapturedWildcard = true;
                TypeMirror bound = t.getLowerBound();
                if (bound != null && bound.getKind() != TypeKind.NULL) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" super ");
                    this.visit(bound, p);
                } else {
                    bound = t.getUpperBound();
                    if (bound != null && bound.getKind() != TypeKind.NULL) {
                        ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                        if (bound.getKind() == TypeKind.TYPEVAR) {
                            bound = ((TypeVariable)bound).getLowerBound();
                        }
                        this.visit(bound, p);
                    }
                }
                this.insideCapturedWildcard = false;
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitWildcard(WildcardType t, Boolean p) {
            int len = ((StringBuilder)this.DEFAULT_VALUE).length();
            ((StringBuilder)this.DEFAULT_VALUE).append("?");
            TypeMirror bound = t.getSuperBound();
            if (bound == null) {
                bound = t.getExtendsBound();
                if (bound != null) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                    if (bound.getKind() == TypeKind.WILDCARD) {
                        bound = ((WildcardType)bound).getSuperBound();
                    }
                    this.visit(bound, p);
                } else if (!(len != 0 || (bound = SourceUtils.getBound((WildcardType)t)) == null || bound.getKind() == TypeKind.DECLARED && ((TypeElement)((DeclaredType)bound).asElement()).getQualifiedName().contentEquals("java.lang.Object"))) {
                    ((StringBuilder)this.DEFAULT_VALUE).append(" extends ");
                    this.visit(bound, p);
                }
            } else {
                ((StringBuilder)this.DEFAULT_VALUE).append(" super ");
                this.visit(bound, p);
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }

        @Override
        public StringBuilder visitError(ErrorType t, Boolean p) {
            Element e = t.asElement();
            if (e instanceof TypeElement) {
                TypeElement te = (TypeElement)e;
                return ((StringBuilder)this.DEFAULT_VALUE).append((p != false ? te.getQualifiedName() : te.getSimpleName()).toString());
            }
            return (StringBuilder)this.DEFAULT_VALUE;
        }
    }

}

