/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.modules.csl.api.CompletionProposal
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;

public class NewVarCompletion
extends BaseCompletion {
    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext request, int anchor) {
        LOG.log(Level.FINEST, "-> completeNewVars");
        List<String> newVars = this.getNewVarNameSuggestion(request.context);
        if (!this.isValid(request, newVars)) {
            return false;
        }
        boolean stuffAdded = false;
        for (String var : newVars) {
            LOG.log(Level.FINEST, "Variable candidate: {0}", var);
            if (!var.startsWith(request.getPrefix()) || var.equals(request.getPrefix())) continue;
            proposals.add((CompletionProposal)new CompletionItem.NewVarItem(var, anchor));
            stuffAdded = true;
        }
        return stuffAdded;
    }

    private boolean isValid(CompletionContext request, List<String> newVars) {
        if (request.location == CaretLocation.OUTSIDE_CLASSES) {
            LOG.log(Level.FINEST, "outside of any class, bail out.");
            return false;
        }
        if (request.isBehindDot()) {
            LOG.log(Level.FINEST, "We are invoked right behind a dot.");
            return false;
        }
        if (newVars == null) {
            LOG.log(Level.FINEST, "Can not propose with newVars == null");
            return false;
        }
        return true;
    }

    private List<String> getNewVarNameSuggestion(CompletionSurrounding ctx) {
        String typeName;
        LOG.log(Level.FINEST, "getNewVarNameSuggestion()");
        ArrayList<String> result = new ArrayList<String>();
        if (ctx == null || ctx.before1 == null) {
            return result;
        }
        GroovyTokenId tokenBefore = (GroovyTokenId)ctx.before1.id();
        switch (tokenBefore) {
            case LITERAL_boolean: {
                result.add("b");
                break;
            }
            case LITERAL_byte: {
                result.add("b");
                break;
            }
            case LITERAL_char: {
                result.add("c");
                break;
            }
            case LITERAL_double: {
                result.add("d");
                break;
            }
            case LITERAL_float: {
                result.add("f");
                break;
            }
            case LITERAL_int: {
                result.add("i");
                break;
            }
            case LITERAL_long: {
                result.add("l");
                break;
            }
            case LITERAL_short: {
                result.add("s");
            }
        }
        if (ctx.before1.id() == GroovyTokenId.IDENTIFIER && (typeName = ctx.before1.text().toString()) != null) {
            this.addIfNotIn(result, typeName.substring(0, 1).toLowerCase(Locale.ENGLISH));
            this.addIfNotIn(result, typeName.toLowerCase(Locale.ENGLISH));
            this.addIfNotIn(result, NewVarCompletion.camelCaseHunch(typeName));
            this.addIfNotIn(result, typeName.substring(0, 1).toLowerCase(Locale.ENGLISH) + typeName.substring(1));
        }
        return result;
    }

    void addIfNotIn(List<String> result, String name) {
        if (name.length() > 0 && !result.contains(name)) {
            LOG.log(Level.FINEST, "Adding new-var suggestion : {0}", name);
            result.add(name);
        }
    }

    private static String camelCaseHunch(CharSequence name) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.length(); ++i) {
            char c = name.charAt(i);
            if (!Character.isUpperCase(c)) continue;
            char lc = Character.toLowerCase(c);
            sb.append(lc);
        }
        return sb.toString();
    }

}

