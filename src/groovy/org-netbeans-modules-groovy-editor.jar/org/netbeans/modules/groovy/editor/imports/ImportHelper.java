/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$NameKind
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.ui.ElementIcons
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.EditList
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.groovy.editor.imports;

import java.awt.Dialog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.ElementKind;
import javax.swing.Icon;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.ui.ElementIcons;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.EditList;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.imports.ImportCandidate;
import org.netbeans.modules.groovy.editor.imports.ImportChooserInnerPanel;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class ImportHelper {
    private static final Logger LOG = Logger.getLogger(ImportHelper.class.getName());

    private ImportHelper() {
    }

    public static void resolveImport(FileObject fo, String packageName, String importName) {
        ImportHelper.resolveImports(fo, packageName, Collections.singletonList(importName));
    }

    public static void resolveImports(final FileObject fo, String packageName, List<String> missingNames) {
        AtomicBoolean cancel = new AtomicBoolean();
        final ArrayList<String> singleCandidates = new ArrayList<String>();
        HashMap<String, Set<ImportCandidate>> multipleCandidates = new HashMap<String, Set<ImportCandidate>>();
        block4 : for (String name : missingNames) {
            Set<ImportCandidate> importCandidates = ImportHelper.getImportCandidate(fo, packageName, name);
            switch (importCandidates.size()) {
                case 0: {
                    continue block4;
                }
                case 1: {
                    singleCandidates.add(importCandidates.iterator().next().getFqnName());
                    continue block4;
                }
            }
            multipleCandidates.put(name, importCandidates);
        }
        if (!multipleCandidates.isEmpty()) {
            List<String> choosenCandidates = ImportHelper.showFixImportChooser(multipleCandidates);
            singleCandidates.addAll(choosenCandidates);
        }
        if (!singleCandidates.isEmpty()) {
            Collections.sort(singleCandidates);
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    ImportHelper.addImportStatements(fo, singleCandidates);
                }
            }, (String)"Adding imports", (AtomicBoolean)cancel, (boolean)false);
        }
    }

    public static Set<ImportCandidate> getImportCandidate(FileObject fo, String packageName, String missingClass) {
        LOG.log(Level.FINEST, "Looking for class: {0}", missingClass);
        HashSet<ImportCandidate> candidates = new HashSet<ImportCandidate>();
        candidates.addAll(ImportHelper.findGroovyImportCandidates(fo, packageName, missingClass));
        candidates.addAll(ImportHelper.findJavaImportCandidates(fo, packageName, missingClass));
        return candidates;
    }

    private static Set<ImportCandidate> findGroovyImportCandidates(FileObject fo, String packageName, String missingClass) {
        HashSet<ImportCandidate> candidates = new HashSet<ImportCandidate>();
        GroovyIndex index = GroovyIndex.get(QuerySupport.findRoots((FileObject)fo, Collections.singleton("classpath/source"), (Collection)null, (Collection)null));
        Set<IndexedClass> classes = index.getClasses(missingClass, QuerySupport.Kind.PREFIX);
        for (IndexedClass indexedClass : classes) {
            String pkgName;
            if (!indexedClass.getName().equals(missingClass) || (pkgName = GroovyUtils.stripClassName(indexedClass.getFqn())) == null || "".equals(pkgName.trim()) || packageName != null && packageName.equals(pkgName)) continue;
            if (indexedClass.getKind() == org.netbeans.modules.csl.api.ElementKind.CLASS) {
                candidates.add(ImportHelper.createImportCandidate(missingClass, indexedClass.getFqn(), ElementKind.CLASS));
            }
            if (indexedClass.getKind() != org.netbeans.modules.csl.api.ElementKind.INTERFACE) continue;
            candidates.add(ImportHelper.createImportCandidate(missingClass, indexedClass.getFqn(), ElementKind.INTERFACE));
        }
        return candidates;
    }

    private static Set<ImportCandidate> findJavaImportCandidates(FileObject fo, String packageName, String missingClass) {
        HashSet<ImportCandidate> candidates = new HashSet<ImportCandidate>();
        ClasspathInfo pathInfo = ImportHelper.createClasspathInfo(fo);
        Set typeNames = pathInfo.getClassIndex().getDeclaredTypes(missingClass, ClassIndex.NameKind.SIMPLE_NAME, EnumSet.allOf(ClassIndex.SearchScope.class));
        for (ElementHandle typeName : typeNames) {
            ElementKind kind = typeName.getKind();
            String pkgName = GroovyUtils.stripClassName(typeName.getQualifiedName());
            if (packageName == null && pkgName == null || packageName != null && packageName.equals(pkgName) || kind != ElementKind.CLASS && kind != ElementKind.INTERFACE && kind != ElementKind.ANNOTATION_TYPE) continue;
            candidates.add(ImportHelper.createImportCandidate(missingClass, typeName.getQualifiedName(), kind));
        }
        return candidates;
    }

    @NonNull
    private static ClasspathInfo createClasspathInfo(FileObject fo) {
        ClassPath bootPath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/boot");
        ClassPath compilePath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/compile");
        ClassPath srcPath = ClassPath.getClassPath((FileObject)fo, (String)"classpath/source");
        if (bootPath == null) {
            bootPath = ClassPath.EMPTY;
        }
        if (compilePath == null) {
            compilePath = ClassPath.EMPTY;
        }
        if (srcPath == null) {
            srcPath = ClassPath.EMPTY;
        }
        return ClasspathInfo.create((ClassPath)bootPath, (ClassPath)compilePath, (ClassPath)srcPath);
    }

    private static ImportCandidate createImportCandidate(String missingClass, String fqnName, ElementKind kind) {
        int level = ImportHelper.getImportanceLevel(fqnName);
        Icon icon = ElementIcons.getElementIcon((ElementKind)kind, (Collection)null);
        return new ImportCandidate(missingClass, fqnName, icon, level);
    }

    private static int getImportanceLevel(String fqn) {
        int weight = 50;
        if (fqn.startsWith("java.lang") || fqn.startsWith("java.util")) {
            weight -= 10;
        } else if (fqn.startsWith("org.omg") || fqn.startsWith("org.apache")) {
            weight += 10;
        } else if (fqn.startsWith("com.sun") || fqn.startsWith("com.ibm") || fqn.startsWith("com.apple")) {
            weight += 20;
        } else if (fqn.startsWith("sun") || fqn.startsWith("sunw") || fqn.startsWith("netscape")) {
            weight += 30;
        }
        return weight;
    }

    public static String getMissingClassName(String errorMessage) {
        int idx;
        String errorPrefix = "unable to resolve class ";
        String missingClass = null;
        if (errorMessage.startsWith(errorPrefix) && (idx = (missingClass = errorMessage.substring(errorPrefix.length())).indexOf(" ")) != -1) {
            return missingClass.substring(0, idx);
        }
        return missingClass;
    }

    private static List<String> showFixImportChooser(Map<String, Set<ImportCandidate>> multipleCandidates) {
        List result = new ArrayList<String>();
        ImportChooserInnerPanel panel = new ImportChooserInnerPanel();
        panel.initPanel(multipleCandidates);
        DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(ImportHelper.class, (String)"FixImportsDialogTitle"));
        Dialog d = DialogDisplayer.getDefault().createDialog(dd);
        d.setVisible(true);
        d.setVisible(false);
        d.dispose();
        if (dd.getValue() == DialogDescriptor.OK_OPTION) {
            result = panel.getSelections();
        }
        return result;
    }

    public static void addImportStatement(FileObject fo, String fqName) {
        ImportHelper.addImportStatements(fo, Collections.singletonList(fqName));
    }

    private static void addImportStatements(FileObject fo, List<String> fqNames) {
        BaseDocument doc = LexUtilities.getDocument(fo, true);
        if (doc == null) {
            return;
        }
        for (String fqName : fqNames) {
            EditList edits = new EditList(doc);
            try {
                int packageLine = ImportHelper.getPackageLineIndex(doc);
                int afterPackageLine = packageLine + 1;
                int afterPackageOffset = Utilities.getRowStartFromLineOffset((BaseDocument)doc, (int)afterPackageLine);
                int importLine = ImportHelper.getAppropriateLine(doc, fqName);
                if (!Utilities.isRowWhite((BaseDocument)doc, (int)afterPackageOffset)) {
                    edits.replace(afterPackageOffset, 0, "\n", false, 0);
                } else if (ImportHelper.collectImports(doc).isEmpty()) {
                    ++importLine;
                }
                int importOffset = Utilities.getRowStartFromLineOffset((BaseDocument)doc, (int)importLine);
                edits.replace(importOffset, 0, "import " + fqName + "\n", false, 0);
                int afterImportsOffset = Utilities.getRowStartFromLineOffset((BaseDocument)doc, (int)importLine);
                if (!Utilities.isRowWhite((BaseDocument)doc, (int)afterImportsOffset) && ImportHelper.isLastImport(doc, fqName)) {
                    edits.replace(afterImportsOffset, 0, "\n", false, 0);
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            edits.apply();
        }
    }

    private static int getAppropriateLine(BaseDocument doc, String fqName) throws BadLocationException {
        Map<String, Integer> imports = ImportHelper.collectImports(doc);
        if (imports.isEmpty()) {
            return ImportHelper.getPackageLineIndex(doc) + 1;
        }
        imports.put(fqName, -1);
        String lastImportName = null;
        for (String importName : imports.keySet()) {
            if (fqName.equals(importName)) break;
            lastImportName = importName;
        }
        if (lastImportName == null) {
            for (Integer importLine : imports.values()) {
                if (importLine <= 0) continue;
                return importLine;
            }
        }
        return imports.get(lastImportName) + 1;
    }

    private static boolean isLastImport(BaseDocument doc, String fqName) throws BadLocationException {
        Map<String, Integer> imports = ImportHelper.collectImports(doc);
        if (imports.isEmpty()) {
            return true;
        }
        String lastImportName = null;
        Iterator<String> i$ = imports.keySet().iterator();
        while (i$.hasNext()) {
            String importName;
            lastImportName = importName = i$.next();
        }
        if (lastImportName != null && fqName.compareTo(lastImportName) > 0) {
            return true;
        }
        return false;
    }

    private static Map<String, Integer> collectImports(BaseDocument doc) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 1);
        TreeMap<String, Integer> result = new TreeMap<String, Integer>();
        while (ts.moveNext()) {
            if (ts.token().id() != GroovyTokenId.LITERAL_import) continue;
            StringBuilder sb = new StringBuilder();
            block5 : while (ts.moveNext()) {
                GroovyTokenId tokenID = (GroovyTokenId)ts.token().id();
                switch (tokenID) {
                    case IDENTIFIER: 
                    case DOT: {
                        sb.append(ts.token().text());
                        continue block5;
                    }
                    case WHITESPACE: {
                        continue block5;
                    }
                }
                break;
            }
            result.put(sb.toString(), Utilities.getLineOffset((BaseDocument)doc, (int)ts.offset()));
        }
        return result;
    }

    private static int getPackageLineIndex(BaseDocument doc) {
        try {
            int lastPackageOffset = ImportHelper.getLastPackageStatementOffset(doc);
            if (lastPackageOffset != -1) {
                return Utilities.getLineOffset((BaseDocument)doc, (int)lastPackageOffset);
            }
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return -1;
    }

    private static int getLastPackageStatementOffset(BaseDocument doc) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 1);
        int packageOffset = -1;
        while (ts.moveNext()) {
            if (ts.token().id() != GroovyTokenId.LITERAL_package) continue;
            packageOffset = ts.offset();
        }
        return packageOffset;
    }

    private static int getLastImportLineIndex(BaseDocument doc) {
        try {
            int lastImportOffset = ImportHelper.getLastImportStatementOffset(doc);
            if (lastImportOffset != -1) {
                return Utilities.getLineOffset((BaseDocument)doc, (int)lastImportOffset);
            }
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return -1;
    }

    private static int getLastImportStatementOffset(BaseDocument doc) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 1);
        int importEnd = -1;
        while (ts.moveNext()) {
            if (ts.token().id() != GroovyTokenId.LITERAL_import) continue;
            importEnd = ts.offset();
        }
        return importEnd;
    }

}

