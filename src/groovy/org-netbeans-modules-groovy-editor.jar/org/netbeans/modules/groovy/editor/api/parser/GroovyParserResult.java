/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.control.ErrorCollector
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.control.ErrorCollector;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.StructureAnalyzer;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTRoot;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParser;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class GroovyParserResult
extends ParserResult {
    private final GroovyParser parser;
    private List<Error> errors = new ArrayList<Error>();
    private ASTRoot rootElement;
    private OffsetRange sanitizedRange = OffsetRange.NONE;
    private String sanitizedContents;
    private StructureAnalyzer.AnalysisResult analysisResult;
    private GroovyParser.Sanitize sanitized;
    private ErrorCollector errorCollector;

    GroovyParserResult(GroovyParser parser, Snapshot snapshot, ModuleNode rootNode, ErrorCollector errorCollector) {
        super(snapshot);
        this.parser = parser;
        this.rootElement = new ASTRoot(snapshot.getSource().getFileObject(), rootNode);
        this.errorCollector = errorCollector;
    }

    public ErrorCollector getErrorCollector() {
        return this.errorCollector;
    }

    public ASTRoot getRootElement() {
        return this.rootElement;
    }

    public void setErrors(Collection<? extends Error> errors) {
        this.errors = new ArrayList<Error>(errors);
    }

    public List<? extends Error> getDiagnostics() {
        return this.errors;
    }

    protected void invalidate() {
    }

    public OffsetRange getSanitizedRange() {
        return this.sanitizedRange;
    }

    public String getSanitizedContents() {
        return this.sanitizedContents;
    }

    void setSanitized(GroovyParser.Sanitize sanitized, OffsetRange sanitizedRange, String sanitizedContents) {
        this.sanitized = sanitized;
        this.sanitizedRange = sanitizedRange;
        this.sanitizedContents = sanitizedContents;
    }

    GroovyParser.Sanitize getSanitized() {
        return this.sanitized;
    }

    public void setStructure(@NonNull StructureAnalyzer.AnalysisResult result) {
        this.analysisResult = result;
    }

    @NonNull
    public StructureAnalyzer.AnalysisResult getStructure() {
        if (this.analysisResult == null) {
            this.analysisResult = new StructureAnalyzer().analyze(this);
        }
        return this.analysisResult;
    }
}

