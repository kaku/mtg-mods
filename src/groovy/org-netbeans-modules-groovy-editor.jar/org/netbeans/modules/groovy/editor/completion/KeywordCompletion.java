/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.spi.ParserResult
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.EnumSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.GroovyKeyword;
import org.netbeans.modules.groovy.editor.api.completion.KeywordCategory;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.DotCompletionContext;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;

class KeywordCompletion
extends BaseCompletion {
    private EnumSet<GroovyKeyword> keywords;
    private CompletionContext request;

    KeywordCompletion() {
    }

    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext request, int anchor) {
        this.request = request;
        LOG.log(Level.FINEST, "-> completeKeywords");
        String prefix = request.getPrefix();
        if (request.location == CaretLocation.INSIDE_PARAMETERS) {
            return false;
        }
        if (request.dotContext != null && (request.dotContext.isFieldsOnly() || request.dotContext.isMethodsOnly())) {
            return false;
        }
        if (request.context.beforeLiteral != null && request.context.beforeLiteral.id() == GroovyTokenId.LITERAL_implements || request.context.beforeLiteral != null && request.context.beforeLiteral.id() == GroovyTokenId.LITERAL_extends) {
            return false;
        }
        if (request.isBehindDot()) {
            LOG.log(Level.FINEST, "We are invoked right behind a dot.");
            return false;
        }
        boolean havePackage = this.checkForPackageStatement(request);
        this.keywords = EnumSet.allOf(GroovyKeyword.class);
        this.filterPackageStatement(havePackage);
        this.filterPrefix(prefix);
        this.filterLocation(request.location);
        this.filterClassInterfaceOrdering(request.context);
        this.filterMethodDefinitions(request.context);
        this.filterKeywordsNextToEachOther(request.context);
        for (GroovyKeyword groovyKeyword : this.keywords) {
            LOG.log(Level.FINEST, "Adding keyword proposal : {0}", groovyKeyword.getName());
            proposals.add((CompletionProposal)new CompletionItem.KeywordItem(groovyKeyword.getName(), null, anchor, request.getParserResult(), groovyKeyword.isGroovyKeyword()));
        }
        return true;
    }

    boolean checkForPackageStatement(CompletionContext request) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)request.doc, 1);
        if (ts != null) {
            ts.move(1);
            while (ts.isValid() && ts.moveNext() && ts.offset() < request.doc.getLength()) {
                Token t = ts.token();
                if (t.id() != GroovyTokenId.LITERAL_package) continue;
                return true;
            }
        }
        return false;
    }

    void filterPackageStatement(boolean havePackage) {
        for (GroovyKeyword groovyKeyword : this.keywords) {
            if (!groovyKeyword.getName().equals("package") || !havePackage) continue;
            this.keywords.remove((Object)groovyKeyword);
        }
    }

    void filterPrefix(String prefix) {
        for (GroovyKeyword groovyKeyword : this.keywords) {
            if (groovyKeyword.getName().startsWith(prefix)) continue;
            this.keywords.remove((Object)groovyKeyword);
        }
    }

    void filterLocation(CaretLocation location) {
        for (GroovyKeyword groovyKeyword : this.keywords) {
            if (this.checkKeywordAllowance(groovyKeyword, location)) continue;
            this.keywords.remove((Object)groovyKeyword);
        }
    }

    void filterClassInterfaceOrdering(CompletionSurrounding ctx) {
        if (ctx == null || ctx.beforeLiteral == null) {
            return;
        }
        if (ctx.beforeLiteral.id() == GroovyTokenId.LITERAL_interface) {
            this.keywords.clear();
            this.addIfPrefixed(GroovyKeyword.KEYWORD_extends);
        } else if (ctx.beforeLiteral.id() == GroovyTokenId.LITERAL_class) {
            this.keywords.clear();
            this.addIfPrefixed(GroovyKeyword.KEYWORD_extends);
            this.addIfPrefixed(GroovyKeyword.KEYWORD_implements);
        }
    }

    private void addIfPrefixed(GroovyKeyword keyword) {
        if (this.isPrefixed(this.request, keyword.getName())) {
            this.keywords.add(keyword);
        }
    }

    void filterMethodDefinitions(CompletionSurrounding ctx) {
        if (ctx == null || ctx.afterLiteral == null) {
            return;
        }
        if (ctx.afterLiteral.id() == GroovyTokenId.LITERAL_void || ctx.afterLiteral.id() == GroovyTokenId.IDENTIFIER || ((GroovyTokenId)ctx.afterLiteral.id()).primaryCategory().equals("number")) {
            for (GroovyKeyword groovyKeyword : this.keywords) {
                if (groovyKeyword.getCategory() != KeywordCategory.PRIMITIVE) continue;
                LOG.log(Level.FINEST, "filterMethodDefinitions - removing : {0}", groovyKeyword.getName());
                this.keywords.remove((Object)groovyKeyword);
            }
        }
    }

    void filterKeywordsNextToEachOther(CompletionSurrounding ctx) {
        if (ctx == null) {
            return;
        }
        boolean filter = false;
        if (ctx.after1 != null && ((GroovyTokenId)ctx.after1.id()).primaryCategory().equals("keyword")) {
            filter = true;
        }
        if (ctx.before1 != null && ((GroovyTokenId)ctx.before1.id()).primaryCategory().equals("keyword")) {
            filter = true;
        }
        if (filter) {
            for (GroovyKeyword groovyKeyword : this.keywords) {
                if (groovyKeyword.getCategory() != KeywordCategory.KEYWORD) continue;
                LOG.log(Level.FINEST, "filterMethodDefinitions - removing : {0}", groovyKeyword.getName());
                this.keywords.remove((Object)groovyKeyword);
            }
        }
    }

    boolean checkKeywordAllowance(GroovyKeyword groovyKeyword, CaretLocation location) {
        if (location == null) {
            return false;
        }
        switch (location) {
            case ABOVE_FIRST_CLASS: {
                if (!groovyKeyword.isAboveFistClass()) break;
                return true;
            }
            case OUTSIDE_CLASSES: {
                if (!groovyKeyword.isOutsideClasses()) break;
                return true;
            }
            case INSIDE_CLASS: {
                if (!groovyKeyword.isInsideClass()) break;
                return true;
            }
            case INSIDE_METHOD: 
            case INSIDE_CLOSURE: {
                if (!groovyKeyword.isInsideCode()) break;
                return true;
            }
        }
        return false;
    }

}

