/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.utils;

import javax.swing.text.BadLocationException;

public final class GroovyUtils {
    private GroovyUtils() {
    }

    public static String stripPackage(String fqn) {
        if (fqn.contains(".")) {
            int idx = fqn.lastIndexOf(".");
            fqn = fqn.substring(idx + 1);
        }
        return fqn.replace(";", "");
    }

    public static String stripClassName(String fqn) {
        if (!fqn.contains(".")) {
            return "";
        }
        return fqn.substring(0, fqn.lastIndexOf(".") + 1);
    }

    public static String getPackageName(String fqn) {
        if (!fqn.contains(".")) {
            return "";
        }
        return fqn.substring(0, fqn.lastIndexOf("."));
    }

    public static boolean isRowWhite(String text, int offset) throws BadLocationException {
        try {
            int i;
            char c;
            for (i = offset; i < text.length() && (c = text.charAt(i)) != '\n'; ++i) {
                if (Character.isWhitespace(c)) continue;
                return false;
            }
            for (i = offset - 1; i >= 0 && (c = text.charAt(i)) != '\n'; --i) {
                if (Character.isWhitespace(c)) continue;
                return false;
            }
            return true;
        }
        catch (IndexOutOfBoundsException ex) {
            throw GroovyUtils.getBadLocationException(ex, text, offset);
        }
    }

    public static boolean isRowEmpty(String text, int offset) throws BadLocationException {
        try {
            char c;
            if (offset < text.length() && (c = text.charAt(offset)) != '\n' && (c != '\r' || offset != text.length() - 1 && text.charAt(offset + 1) != '\n')) {
                return false;
            }
            if (offset != 0 && text.charAt(offset - 1) != '\n') {
                return false;
            }
            return true;
        }
        catch (IndexOutOfBoundsException ex) {
            throw GroovyUtils.getBadLocationException(ex, text, offset);
        }
    }

    public static int getRowLastNonWhite(String text, int offset) throws BadLocationException {
        try {
            int i;
            char c;
            for (i = offset; i < text.length() && (c = text.charAt(i)) != '\n' && (c != '\r' || i != text.length() - 1 && text.charAt(i + 1) != '\n'); ++i) {
            }
            --i;
            while (i >= 0) {
                c = text.charAt(i);
                if (c == '\n') {
                    return -1;
                }
                if (!Character.isWhitespace(c)) {
                    return i;
                }
                --i;
            }
            return -1;
        }
        catch (IndexOutOfBoundsException ex) {
            throw GroovyUtils.getBadLocationException(ex, text, offset);
        }
    }

    public static int getRowFirstNonWhite(String text, int offset) throws BadLocationException {
        try {
            int i;
            char c;
            if (i < text.length()) {
                for (i = offset - 1; i >= 0 && (c = text.charAt(i)) != '\n'; --i) {
                }
                ++i;
            }
            while (i < text.length()) {
                c = text.charAt(i);
                if (c == '\n') {
                    return -1;
                }
                if (!Character.isWhitespace(c)) {
                    return i;
                }
                ++i;
            }
            return -1;
        }
        catch (IndexOutOfBoundsException ex) {
            throw GroovyUtils.getBadLocationException(ex, text, offset);
        }
    }

    public static int getRowStart(String text, int offset) throws BadLocationException {
        try {
            for (int i = offset - 1; i >= 0; --i) {
                char c = text.charAt(i);
                if (c != '\n') continue;
                return i + 1;
            }
            return 0;
        }
        catch (IndexOutOfBoundsException ex) {
            throw GroovyUtils.getBadLocationException(ex, text, offset);
        }
    }

    private static BadLocationException getBadLocationException(IndexOutOfBoundsException ex, String text, int offset) {
        BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
        ble.initCause(ex);
        return ble;
    }
}

