/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 */
package org.netbeans.modules.groovy.editor.api.elements.ast;

import java.util.Collections;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;

public final class ASTField
extends ASTElement {
    private final String fieldType;
    private final boolean isProperty;

    public ASTField(FieldNode node, String in, boolean isProperty) {
        super((ASTNode)node, in, node.getName());
        this.isProperty = isProperty;
        this.fieldType = node.getType().getNameWithoutPackage();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public ElementKind getKind() {
        return ElementKind.FIELD;
    }

    public String getType() {
        return this.fieldType;
    }

    public boolean isProperty() {
        return this.isProperty;
    }

    @Override
    public Set<Modifier> getModifiers() {
        Set<Modifier> mods = super.getModifiers();
        if (this.isProperty()) {
            if (mods.isEmpty()) {
                return Collections.singleton(Modifier.PRIVATE);
            }
            mods.add(Modifier.PRIVATE);
        }
        return mods;
    }
}

