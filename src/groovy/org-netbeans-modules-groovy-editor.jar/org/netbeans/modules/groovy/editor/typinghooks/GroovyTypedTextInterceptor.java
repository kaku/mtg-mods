/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$MutableContext
 */
package org.netbeans.modules.groovy.editor.typinghooks;

import java.util.EnumSet;
import java.util.Set;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.typinghooks.TypingHooksUtil;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;

public class GroovyTypedTextInterceptor
implements TypedTextInterceptor {
    private static final Set<GroovyTokenId> STRING_TOKENS = EnumSet.of(GroovyTokenId.STRING_LITERAL);
    private static final Set<GroovyTokenId> COMMENT_TOKENS = EnumSet.of(GroovyTokenId.BLOCK_COMMENT, GroovyTokenId.LINE_COMMENT);
    private static final Set<GroovyTokenId> REGEXP_TOKENS = EnumSet.of(GroovyTokenId.REGEXP_LITERAL, GroovyTokenId.REGEXP_SYMBOL);
    private boolean isAfter;
    private int previousAdjustmentIndent;
    private int previousAdjustmentOffset = -1;

    public void afterInsert(TypedTextInterceptor.Context context) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts;
        this.isAfter = true;
        BaseDocument doc = (BaseDocument)context.getDocument();
        doc.runAtomicAsUser(new Runnable(){

            @Override
            public void run() {
            }
        });
        int dotPos = context.getOffset();
        Caret caret = context.getComponent().getCaret();
        char ch = context.getText().charAt(0);
        if (this.previousAdjustmentOffset != -1) {
            if (dotPos == this.previousAdjustmentOffset && (ts = LexUtilities.getGroovyTokenSequence((Document)doc, dotPos)) != null) {
                ts.move(dotPos);
                if (ts.moveNext() && ts.offset() < dotPos) {
                    GsfUtilities.setLineIndentation((BaseDocument)doc, (int)dotPos, (int)this.previousAdjustmentIndent);
                }
            }
            this.previousAdjustmentOffset = -1;
        }
        switch (ch) {
            case '(': 
            case ')': 
            case '[': 
            case ']': 
            case '{': 
            case '}': {
                Token<GroovyTokenId> token = LexUtilities.getToken(doc, dotPos);
                if (token == null) {
                    return;
                }
                TokenId id = token.id();
                if (id == GroovyTokenId.IDENTIFIER && token.length() == 1 || id == GroovyTokenId.LBRACKET || id == GroovyTokenId.RBRACKET || id == GroovyTokenId.LBRACE || id == GroovyTokenId.RBRACE || id == GroovyTokenId.LPAREN || id == GroovyTokenId.RPAREN) {
                    if (ch == ']') {
                        this.skipClosingBracket(doc, caret, ch, GroovyTokenId.RBRACKET);
                    } else if (ch == ')') {
                        this.skipClosingBracket(doc, caret, ch, GroovyTokenId.RPAREN);
                    } else if (ch == '}') {
                        this.skipClosingBracket(doc, caret, ch, GroovyTokenId.RBRACE);
                    } else if (ch == '[' || ch == '(') {
                        this.completeOpeningBracket(doc, dotPos, caret, ch);
                    }
                }
                if (ch == '}') {
                    this.reindent(doc, dotPos, GroovyTokenId.RBRACE, caret);
                    break;
                }
                if (ch != ']') break;
                this.reindent(doc, dotPos, GroovyTokenId.RBRACKET, caret);
                break;
            }
            case ';': {
                GroovyTypedTextInterceptor.moveSemicolon(doc, dotPos, caret);
                break;
            }
            case '/': {
                if (!TypingHooksUtil.isMatchingBracketsEnabled(doc) || (ts = LexUtilities.getPositionedSequence(doc, dotPos)) == null) break;
                Token token = ts.token();
                TokenId id = token.id();
                if (id == GroovyTokenId.LINE_COMMENT && dotPos == ts.offset() + 1 && dotPos + 1 < doc.getLength() && doc.getText(dotPos + 1, 1).charAt(0) == '/') {
                    doc.remove(dotPos, 1);
                    caret.setDot(dotPos + 1);
                    break;
                } else {
                    break;
                }
            }
        }
    }

    public boolean beforeInsert(TypedTextInterceptor.Context context) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts;
        this.isAfter = false;
        JTextComponent target = context.getComponent();
        Caret caret = target.getCaret();
        int caretOffset = context.getOffset();
        char ch = context.getText().charAt(0);
        BaseDocument doc = (BaseDocument)context.getDocument();
        if (target.getSelectionStart() != -1) {
            String selection;
            char firstChar;
            if (GsfUtilities.isCodeTemplateEditing((Document)doc)) {
                int end;
                int start = target.getSelectionStart();
                if (start < (end = target.getSelectionEnd())) {
                    target.setSelectionStart(start);
                    target.setSelectionEnd(start);
                    caretOffset = start;
                    caret.setDot(caretOffset);
                    doc.remove(start, end - start);
                }
            } else if ((ch == '\"' || ch == '\'' || ch == '(' || ch == '{' || ch == '[') && (selection = target.getSelectedText()) != null && selection.length() > 0 && (firstChar = selection.charAt(0)) != ch) {
                int start = target.getSelectionStart();
                int end = target.getSelectionEnd();
                TokenSequence<GroovyTokenId> ts2 = LexUtilities.getPositionedSequence(doc, start);
                if (ts2 != null && ts2.token().id() != GroovyTokenId.LINE_COMMENT && ts2.token().id() != GroovyTokenId.BLOCK_COMMENT && ts2.token().id() != GroovyTokenId.STRING_LITERAL) {
                    char lastChar = selection.charAt(selection.length() - 1);
                    if (selection.length() > 1 && (firstChar == '\"' || firstChar == '\'' || firstChar == '(' || firstChar == '{' || firstChar == '[' || firstChar == '/') && lastChar == GroovyTypedTextInterceptor.matching(firstChar)) {
                        doc.remove(end - 1, 1);
                        doc.insertString(end - 1, "" + GroovyTypedTextInterceptor.matching(ch), null);
                        doc.remove(start, 1);
                        doc.insertString(start, "" + ch, null);
                        target.getCaret().setDot(end);
                    } else {
                        doc.remove(start, end - start);
                        doc.insertString(start, "" + ch + selection + GroovyTypedTextInterceptor.matching(ch), null);
                        target.getCaret().setDot(start + selection.length() + 2);
                    }
                    return true;
                }
            }
        }
        if ((ts = LexUtilities.getGroovyTokenSequence((Document)doc, caretOffset)) == null) {
            return false;
        }
        ts.move(caretOffset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return false;
        }
        Token token = ts.token();
        GroovyTokenId id = (GroovyTokenId)token.id();
        if (ch == '*' && id == GroovyTokenId.LINE_COMMENT && caretOffset == ts.offset() + 1) {
            doc.remove(caretOffset, 1);
            return false;
        }
        if ((ch == '\"' || ch == '\'') && this.completeQuote(doc, caretOffset, caret, ch, STRING_TOKENS, GroovyTokenId.STRING_LITERAL)) {
            caret.setDot(caretOffset + 1);
            return true;
        }
        return false;
    }

    public void insert(TypedTextInterceptor.MutableContext context) throws BadLocationException {
    }

    public void cancelled(TypedTextInterceptor.Context context) {
    }

    private void reindent(BaseDocument doc, int offset, TokenId id, Caret caret) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts != null) {
            ts.move(offset);
            if (!ts.moveNext() && !ts.movePrevious()) {
                return;
            }
            Token token = ts.token();
            if (token.id() == id) {
                int rowFirstNonWhite = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
                if (ts.offset() > rowFirstNonWhite) {
                    return;
                }
                OffsetRange begin = OffsetRange.NONE;
                if (id == GroovyTokenId.RBRACE) {
                    begin = LexUtilities.findBwd(doc, ts, GroovyTokenId.LBRACE, GroovyTokenId.RBRACE);
                } else if (id == GroovyTokenId.RBRACKET) {
                    begin = LexUtilities.findBwd(doc, ts, GroovyTokenId.LBRACKET, GroovyTokenId.RBRACKET);
                }
                if (begin != OffsetRange.NONE) {
                    int beginOffset = begin.getStart();
                    int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)beginOffset);
                    this.previousAdjustmentIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                    GsfUtilities.setLineIndentation((BaseDocument)doc, (int)offset, (int)indent);
                    this.previousAdjustmentOffset = caret.getDot();
                }
            }
        }
    }

    private void completeOpeningBracket(BaseDocument doc, int dotPos, Caret caret, char bracket) throws BadLocationException {
        if (this.isCompletablePosition(doc, dotPos + 1)) {
            String matchingBracket = "" + GroovyTypedTextInterceptor.matching(bracket);
            doc.insertString(dotPos + 1, matchingBracket, null);
            caret.setDot(dotPos + 1);
        }
    }

    private boolean completeQuote(BaseDocument doc, int dotPos, Caret caret, char bracket, Set<GroovyTokenId> stringTokens, TokenId beginToken) throws BadLocationException {
        String text;
        boolean eol;
        int lastNonWhite;
        if (GroovyTypedTextInterceptor.isEscapeSequence(doc, dotPos)) {
            return false;
        }
        if (doc.getLength() < dotPos) {
            return false;
        }
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, dotPos);
        if (ts == null) {
            return false;
        }
        ts.move(dotPos);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return false;
        }
        Token<GroovyTokenId> token = ts.token();
        Token previousToken = null;
        if (ts.movePrevious()) {
            previousToken = ts.token();
        }
        boolean bl = eol = (lastNonWhite = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)dotPos)) < dotPos;
        if (token.id() == GroovyTokenId.BLOCK_COMMENT || token.id() == GroovyTokenId.LINE_COMMENT || previousToken != null && previousToken.id() == GroovyTokenId.LINE_COMMENT && token.id() == GroovyTokenId.EOL) {
            return false;
        }
        if ((token.id() == GroovyTokenId.WHITESPACE || token.id() == GroovyTokenId.NLS) && eol && dotPos - 1 > 0 && (token = LexUtilities.getToken(doc, dotPos - 1)).id() == GroovyTokenId.LINE_COMMENT) {
            return false;
        }
        boolean completablePosition = this.isQuoteCompletablePosition(doc, dotPos);
        boolean insideString = false;
        GroovyTokenId id = (GroovyTokenId)token.id();
        for (TokenId currId : stringTokens) {
            if (id != currId) continue;
            insideString = true;
            break;
        }
        if (id == GroovyTokenId.IDENTIFIER && ((text = token.text().toString()).startsWith("\"") || text.startsWith("'"))) {
            insideString = true;
        }
        if (id == GroovyTokenId.ERROR && previousToken != null && previousToken.id() == beginToken) {
            insideString = true;
        }
        if (id == GroovyTokenId.EOL && previousToken != null) {
            if (previousToken.id() == beginToken) {
                insideString = true;
            } else if (previousToken.id() == GroovyTokenId.ERROR && ts.movePrevious() && ts.token().id() == beginToken) {
                insideString = true;
            }
        }
        if (!insideString && token.id() == GroovyTokenId.WHITESPACE && eol && dotPos - 1 > 0) {
            token = LexUtilities.getToken(doc, dotPos - 1);
            boolean bl2 = insideString = token.id() == GroovyTokenId.STRING_LITERAL;
        }
        if (insideString) {
            if (eol) {
                return false;
            }
            char chr = doc.getChars(dotPos, 1)[0];
            if (chr == bracket) {
                if (!this.isAfter) {
                    doc.insertString(dotPos, "" + bracket, null);
                } else if (dotPos >= doc.getLength() - 1 || doc.getText(dotPos + 1, 1).charAt(0) != bracket) {
                    return true;
                }
                doc.remove(dotPos, 1);
                return true;
            }
        }
        if (completablePosition && !insideString || eol) {
            doc.insertString(dotPos, "" + bracket + (this.isAfter ? "" : Character.valueOf(GroovyTypedTextInterceptor.matching(bracket))), null);
            return true;
        }
        return false;
    }

    private boolean isCompletablePosition(BaseDocument doc, int dotPos) throws BadLocationException {
        if (dotPos == doc.getLength()) {
            return true;
        }
        char chr = doc.getChars(dotPos, 1)[0];
        return chr == ')' || chr == ',' || chr == '\"' || chr == '\'' || chr == ' ' || chr == ']' || chr == '}' || chr == '\n' || chr == '\t' || chr == ';';
    }

    private boolean isQuoteCompletablePosition(BaseDocument doc, int dotPos) throws BadLocationException {
        if (dotPos == doc.getLength()) {
            return true;
        }
        int eol = Utilities.getRowEnd((BaseDocument)doc, (int)dotPos);
        if (dotPos == eol || eol == -1) {
            return false;
        }
        int firstNonWhiteFwd = Utilities.getFirstNonWhiteFwd((BaseDocument)doc, (int)dotPos, (int)eol);
        if (firstNonWhiteFwd == -1) {
            return false;
        }
        char chr = doc.getChars(firstNonWhiteFwd, 1)[0];
        return chr == ')' || chr == ',' || chr == '+' || chr == '}' || chr == ';' || chr == ']';
    }

    private void skipClosingBracket(BaseDocument doc, Caret caret, char bracket, TokenId bracketId) throws BadLocationException {
        int caretOffset = caret.getDot();
        if (this.isSkipClosingBracket(doc, caretOffset, bracketId)) {
            doc.remove(caretOffset - 1, 1);
            caret.setDot(caretOffset);
        }
    }

    private boolean isSkipClosingBracket(BaseDocument doc, int caretOffset, TokenId bracketId) throws BadLocationException {
        if (caretOffset == doc.getLength()) {
            return false;
        }
        boolean skipClosingBracket = false;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, caretOffset);
        if (ts == null) {
            return false;
        }
        ts.move(caretOffset);
        if (!ts.moveNext()) {
            return false;
        }
        Token token = ts.token();
        if (token != null && token.id() == bracketId) {
            int bracketIntId = bracketId.ordinal();
            int leftBracketIntId = bracketIntId == GroovyTokenId.RPAREN.ordinal() ? GroovyTokenId.LPAREN.ordinal() : GroovyTokenId.LBRACKET.ordinal();
            ts.moveNext();
            Token nextToken = ts.token();
            boolean endOfJs = false;
            while (nextToken != null && nextToken.id() == bracketId) {
                token = nextToken;
                if (!ts.moveNext()) {
                    endOfJs = true;
                    break;
                }
                nextToken = ts.token();
            }
            int braceBalance = 0;
            int bracketBalance = 0;
            Token lastRBracket = token;
            if (!endOfJs) {
                ts.movePrevious();
            }
            token = ts.token();
            boolean finished = false;
            while (!finished && token != null) {
                int tokenIntId = ((GroovyTokenId)token.id()).ordinal();
                if (token.id() == GroovyTokenId.LPAREN || token.id() == GroovyTokenId.LBRACKET) {
                    if (tokenIntId == leftBracketIntId && ++bracketBalance == 0) {
                        if (braceBalance != 0) {
                            bracketBalance = 1;
                        }
                        finished = true;
                    }
                } else if (token.id() == GroovyTokenId.RPAREN || token.id() == GroovyTokenId.RBRACKET) {
                    if (tokenIntId == bracketIntId) {
                        --bracketBalance;
                    }
                } else if (token.id() == GroovyTokenId.LBRACE) {
                    if (++braceBalance > 0) {
                        finished = true;
                    }
                } else if (token.id() == GroovyTokenId.RBRACE) {
                    --braceBalance;
                }
                if (!ts.movePrevious()) break;
                token = ts.token();
            }
            if (bracketBalance != 0 || bracketId == GroovyTokenId.RBRACE && braceBalance < 0) {
                skipClosingBracket = true;
            } else {
                braceBalance = 0;
                bracketBalance = 0;
                TokenHierarchy th = TokenHierarchy.get((Document)doc);
                int ofs = lastRBracket.offset(th);
                ts.move(ofs);
                ts.moveNext();
                token = ts.token();
                finished = false;
                while (!finished && token != null) {
                    if (token.id() == GroovyTokenId.LPAREN || token.id() == GroovyTokenId.LBRACKET) {
                        if (((GroovyTokenId)token.id()).ordinal() == leftBracketIntId) {
                            ++bracketBalance;
                        }
                    } else if (token.id() == GroovyTokenId.RPAREN || token.id() == GroovyTokenId.RBRACKET) {
                        if (((GroovyTokenId)token.id()).ordinal() == bracketIntId && --bracketBalance == 0) {
                            if (braceBalance != 0) {
                                bracketBalance = -1;
                            }
                            finished = true;
                        }
                    } else if (token.id() == GroovyTokenId.LBRACE) {
                        ++braceBalance;
                    } else if (token.id() == GroovyTokenId.RBRACE) {
                        --braceBalance;
                    }
                    if (!ts.movePrevious()) break;
                    token = ts.token();
                }
                skipClosingBracket = braceBalance == 0 && bracketId == GroovyTokenId.RBRACE || bracketBalance > 0 && (bracketId == GroovyTokenId.RBRACKET || bracketId == GroovyTokenId.RPAREN);
            }
        }
        return skipClosingBracket;
    }

    private static boolean isEscapeSequence(BaseDocument doc, int dotPos) throws BadLocationException {
        if (dotPos <= 0) {
            return false;
        }
        char previousChar = doc.getChars(dotPos - 1, 1)[0];
        return previousChar == '\\';
    }

    private static char matching(char bracket) {
        switch (bracket) {
            case '(': {
                return ')';
            }
            case '[': {
                return ']';
            }
            case '\"': {
                return '\"';
            }
            case '\'': {
                return '\'';
            }
            case '/': {
                return '/';
            }
            case '{': {
                return '}';
            }
            case '}': {
                return '{';
            }
        }
        return bracket;
    }

    private static boolean moveSemicolon(BaseDocument doc, int dotPos, Caret caret) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(doc, dotPos);
        if (ts == null || GroovyTypedTextInterceptor.isStringOrComment((GroovyTokenId)ts.token().id())) {
            return false;
        }
        int lastParenPos = dotPos;
        int index = ts.index();
        block4 : while (ts.moveNext() && ts.token().id() != GroovyTokenId.NLS) {
            switch ((GroovyTokenId)ts.token().id()) {
                case RPAREN: {
                    lastParenPos = ts.offset();
                    continue block4;
                }
                case WHITESPACE: {
                    continue block4;
                }
            }
            return false;
        }
        ts.moveIndex(index);
        ts.moveNext();
        if (GroovyTypedTextInterceptor.isForLoopOrTryWithResourcesSemicolon(ts) || GroovyTypedTextInterceptor.posWithinAnyQuote(doc, dotPos, ts) || lastParenPos == dotPos && !((GroovyTokenId)ts.token().id()).equals((Object)GroovyTokenId.RPAREN)) {
            return false;
        }
        doc.remove(dotPos, 1);
        doc.insertString(lastParenPos, ";", null);
        caret.setDot(lastParenPos + 1);
        return true;
    }

    private static boolean isStringOrComment(GroovyTokenId tokenID) {
        return COMMENT_TOKENS.contains((Object)tokenID) || STRING_TOKENS.contains((Object)tokenID);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean isForLoopOrTryWithResourcesSemicolon(TokenSequence<GroovyTokenId> ts) {
        block25 : {
            int parenDepth = 0;
            int braceDepth = 0;
            boolean semicolonFound = false;
            int tsOrigIndex = ts.index();
            do {
                if (ts.movePrevious()) {
                    switch ((GroovyTokenId)ts.token().id()) {
                        case LPAREN: {
                            if (parenDepth == 0) {
                                block20 : while (ts.movePrevious()) {
                                    switch ((GroovyTokenId)ts.token().id()) {
                                        case WHITESPACE: 
                                        case BLOCK_COMMENT: 
                                        case LINE_COMMENT: {
                                            continue block20;
                                        }
                                        case LITERAL_for: 
                                        case LITERAL_try: {
                                            boolean bl = true;
                                            return bl;
                                        }
                                    }
                                    boolean bl = false;
                                    return bl;
                                }
                                boolean bl = false;
                                return bl;
                            }
                            --parenDepth;
                            break;
                        }
                        case RPAREN: {
                            ++parenDepth;
                            break;
                        }
                        case LBRACE: {
                            if (braceDepth == 0) {
                                boolean bl = false;
                                return bl;
                            }
                            --braceDepth;
                            break;
                        }
                        case RBRACE: {
                            ++braceDepth;
                            break;
                        }
                        case SEMI: {
                            if (semicolonFound) {
                                boolean bl = false;
                                return bl;
                            }
                            semicolonFound = true;
                        }
                    }
                    continue;
                }
                break block25;
                break;
            } while (true);
            finally {
                ts.moveIndex(tsOrigIndex);
                ts.moveNext();
            }
        }
        return false;
    }

    private static boolean posWithinAnyQuote(BaseDocument doc, int offset, TokenSequence<GroovyTokenId> ts) throws BadLocationException {
        if (ts.token().id() == GroovyTokenId.STRING_LITERAL || ts.token().id() == GroovyTokenId.STRING_CH) {
            char chr = doc.getText(offset, 1).charAt(0);
            return offset - ts.offset() == 1 || chr != '\"' && chr != '\'';
        }
        return false;
    }

    public static class GroovyTypedTextInterceptorFactory
    implements TypedTextInterceptor.Factory {
        public TypedTextInterceptor createTypedTextInterceptor(MimePath mimePath) {
            return new GroovyTypedTextInterceptor();
        }
    }

}

