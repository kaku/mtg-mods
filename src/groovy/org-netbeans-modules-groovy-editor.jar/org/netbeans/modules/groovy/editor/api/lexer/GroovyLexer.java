/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovyjarjarantlr.CharBuffer
 *  groovyjarjarantlr.CharQueue
 *  groovyjarjarantlr.CharStreamException
 *  groovyjarjarantlr.InputBuffer
 *  groovyjarjarantlr.LexerSharedInputState
 *  groovyjarjarantlr.Token
 *  groovyjarjarantlr.TokenStreamException
 *  org.codehaus.groovy.antlr.parser.GroovyLexer
 *  org.codehaus.groovy.antlr.parser.GroovyRecognizer
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 */
package org.netbeans.modules.groovy.editor.api.lexer;

import groovyjarjarantlr.CharBuffer;
import groovyjarjarantlr.CharQueue;
import groovyjarjarantlr.CharStreamException;
import groovyjarjarantlr.InputBuffer;
import groovyjarjarantlr.LexerSharedInputState;
import groovyjarjarantlr.TokenStreamException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.antlr.parser.GroovyRecognizer;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

public final class GroovyLexer
implements Lexer<GroovyTokenId> {
    private static final Logger LOG = Logger.getLogger(GroovyLexer.class.getName());
    private DelegateLexer scanner = new DelegateLexer(null);
    private LexerInput lexerInput;
    private MyCharBuffer myCharBuffer;
    private TokenFactory<GroovyTokenId> tokenFactory;
    private final GroovyRecognizer parser;
    private int index = 1;

    public GroovyLexer(LexerRestartInfo<GroovyTokenId> info) {
        this.scanner.setWhitespaceIncluded(true);
        this.parser = GroovyRecognizer.make((org.codehaus.groovy.antlr.parser.GroovyLexer)this.scanner);
        this.restart(info);
    }

    private void restart(LexerRestartInfo<GroovyTokenId> info) {
        this.tokenFactory = info.tokenFactory();
        this.lexerInput = info.input();
        LexerSharedInputState inputState = null;
        if (this.lexerInput != null) {
            this.myCharBuffer = new MyCharBuffer(new LexerInputReader(this.lexerInput));
            inputState = new LexerSharedInputState((InputBuffer)this.myCharBuffer);
        }
        this.scanner.setInputState(inputState);
        if (inputState != null) {
            this.scanner.resetText();
        }
        this.scanner.setState((State)info.state());
    }

    private void scannerConsumeChar() {
        try {
            this.scanner.consume();
        }
        catch (CharStreamException e) {
            throw new IllegalStateException();
        }
    }

    private Token<GroovyTokenId> createToken(int tokenIntId, int tokenLength) {
        GroovyTokenId id = this.getTokenId(tokenIntId);
        LOG.log(Level.FINEST, "Creating token: {0}, length: {1}", new Object[]{id.name(), tokenLength});
        String fixedText = id.fixedText();
        return fixedText != null ? this.tokenFactory.getFlyweightToken((TokenId)id, fixedText) : this.tokenFactory.createToken((TokenId)id, tokenLength);
    }

    public Token<GroovyTokenId> nextToken() {
        LOG.finest("");
        try {
            groovyjarjarantlr.Token antlrToken = this.parser.LT(this.index++);
            LOG.log(Level.FINEST, "Received token from antlr: {0}", (Object)antlrToken);
            if (antlrToken != null) {
                int intId = antlrToken.getType();
                int len = this.lexerInput.readLengthEOF() - this.myCharBuffer.getExtraCharCount();
                if (antlrToken.getText() != null) {
                    len = Math.max(len, antlrToken.getText().length());
                    LOG.log(Level.FINEST, "Counting length from {0} and {1}", new Object[]{this.lexerInput.readLengthEOF(), this.myCharBuffer.getExtraCharCount()});
                }
                LOG.log(Level.FINEST, "Length of token to create: {0}", len);
                switch (intId) {
                    case 48: 
                    case 195: 
                    case 196: {
                        intId = 87;
                        break;
                    }
                    case 1: {
                        if (this.lexerInput.readLength() > 0) {
                            return this.recovery();
                        }
                        return null;
                    }
                }
                return this.createToken(intId, len);
            }
            LOG.finest("Antlr token was null");
            int scannerTextTokenLength = this.scanner.getText().length();
            if (scannerTextTokenLength > 0) {
                return this.createToken(205, scannerTextTokenLength);
            }
            return null;
        }
        catch (TokenStreamException e) {
            LOG.log(Level.FINEST, "Caught exception: {0}", (Throwable)e);
            return this.recovery();
        }
    }

    public Object state() {
        return this.scanner.getState();
    }

    public void release() {
    }

    private Token<GroovyTokenId> recovery() {
        int tokenLength = this.lexerInput.readLength();
        this.scanner.resetText();
        for (int len = this.lexerInput.readLength() - this.myCharBuffer.getExtraCharCount(); len < tokenLength; ++len) {
            LOG.finest("Consuming character");
            this.scannerConsumeChar();
        }
        return tokenLength > 0 ? this.createToken(GroovyTokenId.ERROR.ordinal(), tokenLength) : null;
    }

    private GroovyTokenId getTokenId(int token) {
        switch (token) {
            case 38: {
                return GroovyTokenId.LITERAL_abstract;
            }
            case 68: {
                return GroovyTokenId.ANNOTATION_ARRAY_INIT;
            }
            case 63: {
                return GroovyTokenId.ANNOTATION_DEF;
            }
            case 67: {
                return GroovyTokenId.ANNOTATION_FIELD_DEF;
            }
            case 66: {
                return GroovyTokenId.ANNOTATION_MEMBER_VALUE_PAIR;
            }
            case 65: {
                return GroovyTokenId.ANNOTATION;
            }
            case 64: {
                return GroovyTokenId.ANNOTATIONS;
            }
            case 16: {
                return GroovyTokenId.ARRAY_DECLARATOR;
            }
            case 122: {
                return GroovyTokenId.ASSIGN;
            }
            case 94: {
                return GroovyTokenId.AT;
            }
            case 168: {
                return GroovyTokenId.BAND_ASSIGN;
            }
            case 123: {
                return GroovyTokenId.BAND;
            }
            case 228: {
                return GroovyTokenId.BIG_SUFFIX;
            }
            case 4: {
                return GroovyTokenId.BLOCK;
            }
            case 193: {
                return GroovyTokenId.BNOT;
            }
            case 170: {
                return GroovyTokenId.BOR_ASSIGN;
            }
            case 132: {
                return GroovyTokenId.BOR;
            }
            case 166: {
                return GroovyTokenId.BSR_ASSIGN;
            }
            case 101: {
                return GroovyTokenId.BSR;
            }
            case 169: {
                return GroovyTokenId.BXOR_ASSIGN;
            }
            case 175: {
                return GroovyTokenId.BXOR;
            }
            case 31: {
                return GroovyTokenId.CASE_GROUP;
            }
            case 13: {
                return GroovyTokenId.CLASS_DEF;
            }
            case 49: {
                return GroovyTokenId.CLOSED_BLOCK;
            }
            case 133: {
                return GroovyTokenId.CLOSED_BLOCK_OP;
            }
            case 76: {
                return GroovyTokenId.CLOSURE_OP;
            }
            case 134: {
                return GroovyTokenId.COLON;
            }
            case 99: {
                return GroovyTokenId.COMMA;
            }
            case 182: {
                return GroovyTokenId.COMPARE_TO;
            }
            case 44: {
                return GroovyTokenId.CTOR_CALL;
            }
            case 45: {
                return GroovyTokenId.CTOR_IDENT;
            }
            case 191: {
                return GroovyTokenId.DEC;
            }
            case 223: {
                return GroovyTokenId.DIGIT;
            }
            case 224: {
                return GroovyTokenId.DIGITS_WITH_UNDERSCORE;
            }
            case 225: {
                return GroovyTokenId.DIGITS_WITH_UNDERSCORE_OPT;
            }
            case 163: {
                return GroovyTokenId.DIV_ASSIGN;
            }
            case 189: {
                return GroovyTokenId.DIV;
            }
            case 204: {
                return GroovyTokenId.DOLLAR;
            }
            case 213: {
                return GroovyTokenId.DOLLAR_REGEXP_CTOR_END;
            }
            case 211: {
                return GroovyTokenId.DOLLAR_REGEXP_LITERAL;
            }
            case 217: {
                return GroovyTokenId.DOLLAR_REGEXP_SYMBOL;
            }
            case 89: {
                return GroovyTokenId.DOT;
            }
            case 52: {
                return GroovyTokenId.DYNAMIC_MEMBER;
            }
            case 32: {
                return GroovyTokenId.ELIST;
            }
            case 172: {
                return GroovyTokenId.ELVIS_OPERATOR;
            }
            case 36: {
                return GroovyTokenId.EMPTY_STAT;
            }
            case 61: {
                return GroovyTokenId.ENUM_CONSTANT_DEF;
            }
            case 60: {
                return GroovyTokenId.ENUM_DEF;
            }
            case 1: {
                return GroovyTokenId.EOF;
            }
            case 179: {
                return GroovyTokenId.EQUAL;
            }
            case 218: {
                return GroovyTokenId.ESC;
            }
            case 215: {
                return GroovyTokenId.ESCAPED_DOLLAR;
            }
            case 214: {
                return GroovyTokenId.ESCAPED_SLASH;
            }
            case 226: {
                return GroovyTokenId.EXPONENT;
            }
            case 27: {
                return GroovyTokenId.EXPR;
            }
            case 17: {
                return GroovyTokenId.EXTENDS_CLAUSE;
            }
            case 37: {
                return GroovyTokenId.LITERAL_final;
            }
            case 227: {
                return GroovyTokenId.FLOAT_SUFFIX;
            }
            case 34: {
                return GroovyTokenId.FOR_CONDITION;
            }
            case 62: {
                return GroovyTokenId.FOR_EACH_CLAUSE;
            }
            case 58: {
                return GroovyTokenId.FOR_IN_ITERABLE;
            }
            case 33: {
                return GroovyTokenId.FOR_INIT;
            }
            case 35: {
                return GroovyTokenId.FOR_ITERATOR;
            }
            case 184: {
                return GroovyTokenId.GE;
            }
            case 98: {
                return GroovyTokenId.GT;
            }
            case 220: {
                return GroovyTokenId.HEX_DIGIT;
            }
            case 86: {
                return GroovyTokenId.IDENTIFIER;
            }
            case 180: {
                return GroovyTokenId.IDENTICAL;
            }
            case 18: {
                return GroovyTokenId.IMPLEMENTS_CLAUSE;
            }
            case 50: {
                return GroovyTokenId.IMPLICIT_PARAMETERS;
            }
            case 28: {
                return GroovyTokenId.IMPORT;
            }
            case 188: {
                return GroovyTokenId.INC;
            }
            case 23: {
                return GroovyTokenId.INDEX_OP;
            }
            case 10: {
                return GroovyTokenId.INSTANCE_INIT;
            }
            case 14: {
                return GroovyTokenId.INTERFACE_DEF;
            }
            case 53: {
                return GroovyTokenId.LABELED_ARG;
            }
            case 21: {
                return GroovyTokenId.LABELED_STAT;
            }
            case 174: {
                return GroovyTokenId.LAND;
            }
            case 84: {
                return GroovyTokenId.LBRACKET;
            }
            case 124: {
                return GroovyTokenId.LBRACE;
            }
            case 183: {
                return GroovyTokenId.LE;
            }
            case 222: {
                return GroovyTokenId.LETTER;
            }
            case 56: {
                return GroovyTokenId.LIST_CONSTRUCTOR;
            }
            case 112: {
                return GroovyTokenId.LITERAL_as;
            }
            case 145: {
                return GroovyTokenId.LITERAL_assert;
            }
            case 103: {
                return GroovyTokenId.LITERAL_boolean;
            }
            case 142: {
                return GroovyTokenId.LITERAL_break;
            }
            case 104: {
                return GroovyTokenId.LITERAL_byte;
            }
            case 148: {
                return GroovyTokenId.LITERAL_case;
            }
            case 151: {
                return GroovyTokenId.LITERAL_catch;
            }
            case 105: {
                return GroovyTokenId.LITERAL_char;
            }
            case 91: {
                return GroovyTokenId.LITERAL_class;
            }
            case 143: {
                return GroovyTokenId.LITERAL_continue;
            }
            case 83: {
                return GroovyTokenId.LITERAL_def;
            }
            case 127: {
                return GroovyTokenId.LITERAL_default;
            }
            case 110: {
                return GroovyTokenId.LITERAL_double;
            }
            case 136: {
                return GroovyTokenId.LITERAL_else;
            }
            case 93: {
                return GroovyTokenId.LITERAL_enum;
            }
            case 96: {
                return GroovyTokenId.LITERAL_extends;
            }
            case 155: {
                return GroovyTokenId.LITERAL_false;
            }
            case 150: {
                return GroovyTokenId.LITERAL_finally;
            }
            case 108: {
                return GroovyTokenId.LITERAL_float;
            }
            case 139: {
                return GroovyTokenId.LITERAL_for;
            }
            case 135: {
                return GroovyTokenId.LITERAL_if;
            }
            case 129: {
                return GroovyTokenId.LITERAL_implements;
            }
            case 81: {
                return GroovyTokenId.LITERAL_import;
            }
            case 140: {
                return GroovyTokenId.LITERAL_in;
            }
            case 156: {
                return GroovyTokenId.LITERAL_instanceof;
            }
            case 107: {
                return GroovyTokenId.LITERAL_int;
            }
            case 92: {
                return GroovyTokenId.LITERAL_interface;
            }
            case 109: {
                return GroovyTokenId.LITERAL_long;
            }
            case 117: {
                return GroovyTokenId.LITERAL_native;
            }
            case 157: {
                return GroovyTokenId.LITERAL_new;
            }
            case 158: {
                return GroovyTokenId.LITERAL_null;
            }
            case 80: {
                return GroovyTokenId.LITERAL_package;
            }
            case 113: {
                return GroovyTokenId.LITERAL_private;
            }
            case 115: {
                return GroovyTokenId.LITERAL_protected;
            }
            case 114: {
                return GroovyTokenId.LITERAL_public;
            }
            case 141: {
                return GroovyTokenId.LITERAL_return;
            }
            case 106: {
                return GroovyTokenId.LITERAL_short;
            }
            case 82: {
                return GroovyTokenId.LITERAL_static;
            }
            case 97: {
                return GroovyTokenId.LITERAL_super;
            }
            case 138: {
                return GroovyTokenId.LITERAL_switch;
            }
            case 119: {
                return GroovyTokenId.LITERAL_synchronized;
            }
            case 130: {
                return GroovyTokenId.LITERAL_this;
            }
            case 118: {
                return GroovyTokenId.LITERAL_threadsafe;
            }
            case 144: {
                return GroovyTokenId.LITERAL_throw;
            }
            case 128: {
                return GroovyTokenId.LITERAL_throws;
            }
            case 116: {
                return GroovyTokenId.LITERAL_transient;
            }
            case 159: {
                return GroovyTokenId.LITERAL_true;
            }
            case 149: {
                return GroovyTokenId.LITERAL_try;
            }
            case 102: {
                return GroovyTokenId.LITERAL_void;
            }
            case 120: {
                return GroovyTokenId.LITERAL_volatile;
            }
            case 137: {
                return GroovyTokenId.LITERAL_while;
            }
            case 194: {
                return GroovyTokenId.LNOT;
            }
            case 173: {
                return GroovyTokenId.LOR;
            }
            case 90: {
                return GroovyTokenId.LPAREN;
            }
            case 88: {
                return GroovyTokenId.LT;
            }
            case 57: {
                return GroovyTokenId.MAP_CONSTRUCTOR;
            }
            case 154: {
                return GroovyTokenId.MEMBER_POINTER;
            }
            case 26: {
                return GroovyTokenId.METHOD_CALL;
            }
            case 8: {
                return GroovyTokenId.METHOD_DEF;
            }
            case 161: {
                return GroovyTokenId.MINUS_ASSIGN;
            }
            case 147: {
                return GroovyTokenId.MINUS;
            }
            case 208: {
                return GroovyTokenId.BLOCK_COMMENT;
            }
            case 164: {
                return GroovyTokenId.MOD_ASSIGN;
            }
            case 190: {
                return GroovyTokenId.MOD;
            }
            case 5: {
                return GroovyTokenId.MODIFIERS;
            }
            case 77: {
                return GroovyTokenId.MULTICATCH;
            }
            case 78: {
                return GroovyTokenId.MULTICATCH_TYPES;
            }
            case 203: {
                return GroovyTokenId.NLS;
            }
            case 178: {
                return GroovyTokenId.NOT_EQUAL;
            }
            case 181: {
                return GroovyTokenId.NOT_IDENTICAL;
            }
            case 3: {
                return GroovyTokenId.NULL_TREE_LOOKAHEAD;
            }
            case 202: {
                return GroovyTokenId.NUM_BIG_DECIMAL;
            }
            case 201: {
                return GroovyTokenId.NUM_BIG_INT;
            }
            case 200: {
                return GroovyTokenId.NUM_DOUBLE;
            }
            case 198: {
                return GroovyTokenId.NUM_FLOAT;
            }
            case 197: {
                return GroovyTokenId.NUM_INT;
            }
            case 199: {
                return GroovyTokenId.NUM_LONG;
            }
            case 6: {
                return GroovyTokenId.OBJBLOCK;
            }
            case 206: {
                return GroovyTokenId.ONE_NL;
            }
            case 153: {
                return GroovyTokenId.OPTIONAL_DOT;
            }
            case 15: {
                return GroovyTokenId.PACKAGE_DEF;
            }
            case 20: {
                return GroovyTokenId.PARAMETER_DEF;
            }
            case 19: {
                return GroovyTokenId.PARAMETERS;
            }
            case 160: {
                return GroovyTokenId.PLUS_ASSIGN;
            }
            case 146: {
                return GroovyTokenId.PLUS;
            }
            case 25: {
                return GroovyTokenId.POST_DEC;
            }
            case 24: {
                return GroovyTokenId.POST_INC;
            }
            case 95: {
                return GroovyTokenId.QUESTION;
            }
            case 187: {
                return GroovyTokenId.RANGE_EXCLUSIVE;
            }
            case 186: {
                return GroovyTokenId.RANGE_INCLUSIVE;
            }
            case 125: {
                return GroovyTokenId.RBRACE;
            }
            case 85: {
                return GroovyTokenId.RBRACKET;
            }
            case 212: {
                return GroovyTokenId.REGEXP_CTOR_END;
            }
            case 210: {
                return GroovyTokenId.REGEXP_LITERAL;
            }
            case 216: {
                return GroovyTokenId.REGEXP_SYMBOL;
            }
            case 176: {
                return GroovyTokenId.REGEX_FIND;
            }
            case 177: {
                return GroovyTokenId.REGEX_MATCH;
            }
            case 121: {
                return GroovyTokenId.RPAREN;
            }
            case 51: {
                return GroovyTokenId.SELECT_SLOT;
            }
            case 126: {
                return GroovyTokenId.SEMI;
            }
            case 79: {
                return GroovyTokenId.LINE_COMMENT;
            }
            case 167: {
                return GroovyTokenId.SL_ASSIGN;
            }
            case 207: {
                return GroovyTokenId.LINE_COMMENT;
            }
            case 185: {
                return GroovyTokenId.SL;
            }
            case 7: {
                return GroovyTokenId.SLIST;
            }
            case 54: {
                return GroovyTokenId.SPREAD_ARG;
            }
            case 152: {
                return GroovyTokenId.SPREAD_DOT;
            }
            case 55: {
                return GroovyTokenId.SPREAD_MAP_ARG;
            }
            case 165: {
                return GroovyTokenId.SR_ASSIGN;
            }
            case 100: {
                return GroovyTokenId.SR;
            }
            case 162: {
                return GroovyTokenId.STAR_ASSIGN;
            }
            case 171: {
                return GroovyTokenId.STAR_STAR_ASSIGN;
            }
            case 192: {
                return GroovyTokenId.STAR_STAR;
            }
            case 111: {
                return GroovyTokenId.STAR;
            }
            case 59: {
                return GroovyTokenId.STATIC_IMPORT;
            }
            case 11: {
                return GroovyTokenId.STATIC_INIT;
            }
            case 42: {
                return GroovyTokenId.STRICTFP;
            }
            case 47: {
                return GroovyTokenId.STRING_CONSTRUCTOR;
            }
            case 196: {
                return GroovyTokenId.STRING_CTOR_END;
            }
            case 48: {
                return GroovyTokenId.STRING_CTOR_MIDDLE;
            }
            case 195: {
                return GroovyTokenId.STRING_CTOR_START;
            }
            case 209: {
                return GroovyTokenId.STRING_CH;
            }
            case 87: {
                return GroovyTokenId.STRING_LITERAL;
            }
            case 219: {
                return GroovyTokenId.STRING_NL;
            }
            case 43: {
                return GroovyTokenId.SUPER_CTOR_CALL;
            }
            case 131: {
                return GroovyTokenId.TRIPLE_DOT;
            }
            case 70: {
                return GroovyTokenId.TYPE_ARGUMENT;
            }
            case 69: {
                return GroovyTokenId.TYPE_ARGUMENTS;
            }
            case 75: {
                return GroovyTokenId.TYPE_LOWER_BOUNDS;
            }
            case 72: {
                return GroovyTokenId.TYPE_PARAMETER;
            }
            case 71: {
                return GroovyTokenId.TYPE_PARAMETERS;
            }
            case 74: {
                return GroovyTokenId.TYPE_UPPER_BOUNDS;
            }
            case 12: {
                return GroovyTokenId.TYPE;
            }
            case 22: {
                return GroovyTokenId.TYPECAST;
            }
            case 29: {
                return GroovyTokenId.UNARY_MINUS;
            }
            case 30: {
                return GroovyTokenId.UNARY_PLUS;
            }
            case 40: {
                return GroovyTokenId.UNUSED_CONST;
            }
            case 41: {
                return GroovyTokenId.UNUSED_DO;
            }
            case 39: {
                return GroovyTokenId.UNUSED_GOTO;
            }
            case 9: {
                return GroovyTokenId.VARIABLE_DEF;
            }
            case 46: {
                return GroovyTokenId.VARIABLE_PARAMETER_DEF;
            }
            case 221: {
                return GroovyTokenId.VOCAB;
            }
            case 73: {
                return GroovyTokenId.WILDCARD_TYPE;
            }
            case 205: {
                return GroovyTokenId.WHITESPACE;
            }
        }
        return GroovyTokenId.IDENTIFIER;
    }

    private static class State {
        private final int stringCtorState;
        private final ArrayList parenLevelStack;

        public State(int stringCtorState, ArrayList parenLevelStack) {
            this.stringCtorState = stringCtorState;
            this.parenLevelStack = new ArrayList(parenLevelStack);
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            State other = (State)obj;
            if (this.stringCtorState != other.stringCtorState) {
                return false;
            }
            if (this.parenLevelStack == null || !this.parenLevelStack.equals(other.parenLevelStack)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + this.stringCtorState;
            hash = 67 * hash + (this.parenLevelStack != null ? this.parenLevelStack.hashCode() : 0);
            return hash;
        }
    }

    private static class DelegateLexer
    extends org.codehaus.groovy.antlr.parser.GroovyLexer {
        public DelegateLexer(LexerSharedInputState state) {
            super(state);
        }

        public State getState() {
            if (this.stringCtorState > 0 || !this.parenLevelStack.isEmpty()) {
                return new State(this.stringCtorState, this.parenLevelStack);
            }
            return null;
        }

        public void setState(State d) {
            if (d != null) {
                this.stringCtorState = d.stringCtorState;
                this.parenLevelStack = new ArrayList(d.parenLevelStack);
            }
        }
    }

    private static class LexerInputReader
    extends Reader {
        private LexerInput input;

        LexerInputReader(LexerInput input) {
            this.input = input;
        }

        @Override
        public int read(char[] buf, int off, int len) throws IOException {
            for (int i = 0; i < len; ++i) {
                int c = this.input.read();
                if (c == -1) {
                    return -1;
                }
                buf[i + off] = (char)c;
            }
            return len;
        }

        @Override
        public void close() throws IOException {
        }
    }

    private static class MyCharQueue
    extends CharQueue {
        public MyCharQueue(int minSize) {
            super(minSize);
        }

        public int getNbrEntries() {
            return this.nbrEntries;
        }
    }

    private static class MyCharBuffer
    extends CharBuffer {
        public MyCharBuffer(Reader reader) {
            super(reader);
            this.queue = new MyCharQueue(1);
        }

        public int getExtraCharCount() {
            this.syncConsume();
            return ((MyCharQueue)this.queue).getNbrEntries();
        }
    }

}

