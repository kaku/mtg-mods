/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.stmt.Statement
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.editor.BaseDocument
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import javax.swing.text.BadLocationException;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.PathFinderVisitor;
import org.openide.util.Exceptions;

public class AstPath
implements Iterable<ASTNode> {
    private ArrayList<ASTNode> path = new ArrayList(30);
    private int lineNumber = -1;
    private int columnNumber = -1;

    public AstPath() {
    }

    public AstPath(ASTNode root, int caretOffset, BaseDocument document) {
        try {
            int offset;
            int length = document.getLength();
            int n = offset = length == 0 ? 0 : caretOffset + 1;
            if (length > 0 && offset >= length) {
                offset = length - 1;
            }
            Scanner scanner = new Scanner(document.getText(0, offset));
            int line = 0;
            String lineText = "";
            while (scanner.hasNextLine()) {
                lineText = scanner.nextLine();
                ++line;
            }
            int column = lineText.length();
            this.lineNumber = line;
            this.columnNumber = column;
            this.findPathTo(root, line, column);
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
    }

    public AstPath(ASTNode root, int line, int column) {
        this.lineNumber = line;
        this.columnNumber = column;
        this.findPathTo(root, line, column);
    }

    public AstPath(ASTNode node, ASTNode target) {
        if (!this.find(node, target)) {
            this.path.clear();
        } else {
            Collections.reverse(this.path);
        }
    }

    public int getLineNumber() {
        return this.lineNumber;
    }

    public int getColumnNumber() {
        return this.columnNumber;
    }

    public void descend(ASTNode node) {
        this.path.add(node);
    }

    public void ascend() {
        this.path.remove(this.path.size() - 1);
    }

    private ASTNode findPathTo(ASTNode node, int line, int column) {
        ClassNode clazz;
        MethodNode method;
        assert (node != null);
        assert (node instanceof ModuleNode);
        assert (line >= 0);
        assert (column >= 0);
        this.path.addAll(this.find(node, line, column));
        if (this.path.isEmpty() || !(this.path.get(0) instanceof ClassNode)) {
            ModuleNode moduleNode = (ModuleNode)node;
            String name = moduleNode.getContext().getName();
            int index = name.lastIndexOf(".groovy");
            if (index != -1) {
                name = name.substring(0, index);
            }
            if ((index = name.lastIndexOf(46)) != -1) {
                name = name.substring(index + 1);
            }
            for (Object object : moduleNode.getClasses()) {
                ClassNode classNode = (ClassNode)object;
                if (!name.equals(classNode.getNameWithoutPackage())) continue;
                this.path.add(0, (ASTNode)classNode);
                break;
            }
        }
        if (!this.path.isEmpty() && this.path.get(0) instanceof ClassNode && (clazz = (ClassNode)this.path.get(0)).isScript() && (this.path.size() == 1 || this.path.get(1) instanceof Expression || this.path.get(1) instanceof Statement) && (method = clazz.getMethod("run", new Parameter[0])) != null) {
            if (method.getCode() != null && (this.path.size() <= 1 || method.getCode() != this.path.get(1))) {
                this.path.add(1, (ASTNode)method.getCode());
            }
            this.path.add(1, (ASTNode)method);
        }
        this.path.add(0, node);
        ASTNode result = this.path.get(this.path.size() - 1);
        return result;
    }

    private List<ASTNode> find(ASTNode node, int line, int column) {
        assert (line >= 0);
        assert (column >= 0);
        assert (node != null);
        assert (node instanceof ModuleNode);
        ModuleNode moduleNode = (ModuleNode)node;
        PathFinderVisitor pathFinder = new PathFinderVisitor(moduleNode.getContext(), line, column);
        for (ClassNode classNode : moduleNode.getClasses()) {
            pathFinder.visitClass(classNode);
        }
        for (MethodNode methodNode : moduleNode.getMethods()) {
            pathFinder.visitMethod(methodNode);
        }
        return pathFinder.getPath();
    }

    public boolean find(ASTNode node, ASTNode target) {
        if (node == target) {
            return true;
        }
        List<ASTNode> children = ASTUtils.children(node);
        for (ASTNode child : children) {
            boolean found = this.find(child, target);
            if (!found) continue;
            this.path.add(child);
            return found;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Path(");
        sb.append(this.path.size());
        sb.append(")=[");
        for (ASTNode n : this.path) {
            String name = n.getClass().getName();
            name = name.substring(name.lastIndexOf(46) + 1);
            sb.append(name);
            sb.append("\n");
        }
        sb.append("]");
        return sb.toString();
    }

    public ASTNode leaf() {
        if (this.path.isEmpty()) {
            return null;
        }
        return this.path.get(this.path.size() - 1);
    }

    public ASTNode leafParent() {
        if (this.path.size() < 2) {
            return null;
        }
        return this.path.get(this.path.size() - 2);
    }

    public ASTNode leafGrandParent() {
        if (this.path.size() < 3) {
            return null;
        }
        return this.path.get(this.path.size() - 3);
    }

    public ASTNode root() {
        if (this.path.isEmpty()) {
            return null;
        }
        return this.path.get(0);
    }

    @Override
    public Iterator<ASTNode> iterator() {
        return new LeafToRootIterator(this.path);
    }

    public ListIterator<ASTNode> rootToLeaf() {
        return this.path.listIterator();
    }

    public ListIterator<ASTNode> leafToRoot() {
        return new LeafToRootIterator(this.path);
    }

    private static class LeafToRootIterator
    implements ListIterator<ASTNode> {
        private final ListIterator<ASTNode> it;

        private LeafToRootIterator(ArrayList<ASTNode> path) {
            this.it = path.listIterator(path.size());
        }

        @Override
        public boolean hasNext() {
            return this.it.hasPrevious();
        }

        @Override
        public ASTNode next() {
            return this.it.previous();
        }

        @Override
        public boolean hasPrevious() {
            return this.it.hasNext();
        }

        @Override
        public ASTNode previous() {
            return this.it.next();
        }

        @Override
        public int nextIndex() {
            return this.it.previousIndex();
        }

        @Override
        public int previousIndex() {
            return this.it.nextIndex();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void set(ASTNode arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void add(ASTNode arg0) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

