/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.groovy.editor.hints.utils;

import javax.swing.text.BadLocationException;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;
import org.openide.util.Parameters;

public final class HintUtils {
    private HintUtils() {
    }

    @CheckForNull
    public static OffsetRange getLineOffset(@NonNull RuleContext context, @NonNull GroovyError error) {
        Parameters.notNull((CharSequence)"context", (Object)context);
        Parameters.notNull((CharSequence)"error", (Object)error);
        try {
            int lineStart = Utilities.getRowStart((BaseDocument)context.doc, (int)error.getStartPosition());
            int lineEnd = Utilities.getRowEnd((BaseDocument)context.doc, (int)error.getEndPosition());
            return new OffsetRange(lineStart, lineEnd);
        }
        catch (BadLocationException ex) {
            return null;
        }
    }
}

