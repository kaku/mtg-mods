/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 */
package org.netbeans.modules.groovy.editor.api.elements.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.swing.text.Document;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.elements.GroovyElement;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTMethod;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;

public abstract class ASTElement
extends GroovyElement {
    protected final ASTNode node;
    protected final List<ASTElement> children;
    protected Set<Modifier> modifiers;

    public ASTElement(ASTNode node) {
        this(node, null);
    }

    public ASTElement(ASTNode node, String in) {
        this(node, in, null);
    }

    public ASTElement(ASTNode node, String in, String name) {
        super(in, name);
        this.node = node;
        this.children = new ArrayList<ASTElement>();
    }

    public ASTNode getNode() {
        return this.node;
    }

    public List<ASTElement> getChildren() {
        return this.children;
    }

    public void addChild(ASTElement child) {
        this.children.add(child);
    }

    @Override
    public Set<Modifier> getModifiers() {
        if (this.modifiers == null) {
            int flags = -1;
            if (this.node instanceof FieldNode) {
                flags = ((FieldNode)this.node).getModifiers();
            } else if (this.node instanceof MethodNode) {
                flags = ((MethodNode)this.node).getModifiers();
            }
            if (flags != -1) {
                EnumSet<Modifier> result = EnumSet.noneOf(Modifier.class);
                if ((flags & 1) != 0) {
                    result.add(Modifier.PUBLIC);
                }
                if ((flags & 4) != 0) {
                    result.add(Modifier.PROTECTED);
                }
                if ((flags & 2) != 0) {
                    result.add(Modifier.PRIVATE);
                }
                if ((flags & 8) != 0) {
                    result.add(Modifier.STATIC);
                }
                this.modifiers = result;
            } else {
                this.modifiers = Collections.emptySet();
            }
        }
        return this.modifiers;
    }

    @Override
    public boolean signatureEquals(ElementHandle handle) {
        if (handle instanceof ASTElement) {
            return this.equals((Object)handle);
        }
        return false;
    }

    @Override
    public OffsetRange getOffsetRange(ParserResult result) {
        BaseDocument doc = (BaseDocument)result.getSnapshot().getSource().getDocument(false);
        int lineNumber = this.node.getLineNumber();
        int columnNumber = this.node.getColumnNumber();
        int start = ASTUtils.getOffset(doc, lineNumber, columnNumber);
        return new OffsetRange(start, start);
    }

    public static ASTElement create(ASTNode node) {
        if (node instanceof MethodNode) {
            return new ASTMethod(node);
        }
        return null;
    }

    public String toString() {
        return (Object)this.getKind() + "<" + this.getName() + ">";
    }
}

