/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.control.ErrorCollector
 *  org.codehaus.groovy.control.messages.SyntaxErrorMessage
 *  org.codehaus.groovy.syntax.SyntaxException
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.control.ErrorCollector;
import org.codehaus.groovy.control.messages.SyntaxErrorMessage;
import org.codehaus.groovy.syntax.SyntaxException;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.imports.ImportHelper;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class FixImportsAction
extends BaseAction {
    protected static final String ACTION_NAME = "fix-groovy-imports";

    public FixImportsAction() {
        super(10);
    }

    public boolean isEnabled() {
        return true;
    }

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        FileObject fo = NbEditorUtilities.getDataObject((Document)target.getDocument()).getPrimaryFile();
        Source source = Source.create((FileObject)fo);
        CollectMissingImportsTask task = new CollectMissingImportsTask();
        try {
            ParserManager.parse(Collections.singleton(source), (UserTask)task);
        }
        catch (ParseException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        ImportHelper.resolveImports(fo, task.getPackageName(), task.getMissingNames());
    }

    private static final class CollectMissingImportsTask
    extends UserTask {
        private final List<String> missingNames = new ArrayList<String>();
        private String packageName;

        public void run(ResultIterator resultIterator) throws Exception {
            GroovyParserResult result = ASTUtils.getParseResult(resultIterator.getParserResult());
            if (result != null) {
                ErrorCollector errorCollector;
                ModuleNode rootModule = ASTUtils.getRoot(result);
                if (rootModule != null) {
                    this.packageName = rootModule.getPackageName();
                }
                if ((errorCollector = result.getErrorCollector()) == null) {
                    return;
                }
                List errors = errorCollector.getErrors();
                if (errors == null) {
                    return;
                }
                this.collectMissingImports(errors);
            }
        }

        private void collectMissingImports(List errors) {
            for (Object error : errors) {
                SyntaxException se;
                String missingClassName;
                if (!(error instanceof SyntaxErrorMessage) || (se = ((SyntaxErrorMessage)error).getCause()) == null || (missingClassName = ImportHelper.getMissingClassName(se.getMessage())) == null || this.missingNames.contains(missingClassName)) continue;
                this.missingNames.add(missingClassName);
            }
        }

        public List<String> getMissingNames() {
            return this.missingNames;
        }

        public String getPackageName() {
            return this.packageName;
        }
    }

}

