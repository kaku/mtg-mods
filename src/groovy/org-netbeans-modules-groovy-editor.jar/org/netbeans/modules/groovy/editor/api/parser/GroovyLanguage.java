/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.core.spi.multiview.text.MultiViewEditorElement
 *  org.netbeans.modules.csl.api.CodeCompletionHandler
 *  org.netbeans.modules.csl.api.DeclarationFinder
 *  org.netbeans.modules.csl.api.Formatter
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.IndexSearcher
 *  org.netbeans.modules.csl.api.InstantRenamer
 *  org.netbeans.modules.csl.api.KeystrokeHandler
 *  org.netbeans.modules.csl.api.OccurrencesFinder
 *  org.netbeans.modules.csl.api.SemanticAnalyzer
 *  org.netbeans.modules.csl.api.StructureScanner
 *  org.netbeans.modules.csl.spi.DefaultLanguageConfig
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.groovy.editor.api.parser;

import java.util.Collections;
import java.util.Set;
import org.netbeans.api.lexer.Language;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.groovy.editor.api.GroovyIndexer;
import org.netbeans.modules.groovy.editor.api.StructureAnalyzer;
import org.netbeans.modules.groovy.editor.api.completion.CompletionHandler;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.parser.GroovyOccurrencesFinder;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParser;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovyHintsProvider;
import org.netbeans.modules.groovy.editor.language.GroovyBracketCompleter;
import org.netbeans.modules.groovy.editor.language.GroovyDeclarationFinder;
import org.netbeans.modules.groovy.editor.language.GroovyFormatter;
import org.netbeans.modules.groovy.editor.language.GroovyInstantRenamer;
import org.netbeans.modules.groovy.editor.language.GroovySemanticAnalyzer;
import org.netbeans.modules.groovy.editor.language.GroovyTypeSearcher;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.openide.util.Lookup;

public class GroovyLanguage
extends DefaultLanguageConfig {
    public static final String GROOVY_MIME_TYPE = "text/x-groovy";
    public static final String ACTIONS = "Loaders/text/x-groovy/Actions";
    private static final String GROOVY_FILE_ICON_16x16 = "org/netbeans/modules/groovy/editor/resources/GroovyFile16x16.png";

    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }

    public String getLineCommentPrefix() {
        return "//";
    }

    public boolean isIdentifierChar(char c) {
        return Character.isJavaIdentifierPart(c) || c == '$';
    }

    public Language getLexerLanguage() {
        return GroovyTokenId.language();
    }

    public String getDisplayName() {
        return "Groovy";
    }

    public String getPreferredExtension() {
        return "groovy";
    }

    public Parser getParser() {
        return new GroovyParser();
    }

    public boolean hasFormatter() {
        return true;
    }

    public Formatter getFormatter() {
        return new GroovyFormatter();
    }

    public KeystrokeHandler getKeystrokeHandler() {
        return new GroovyBracketCompleter();
    }

    public CodeCompletionHandler getCompletionHandler() {
        return new CompletionHandler();
    }

    public SemanticAnalyzer getSemanticAnalyzer() {
        return new GroovySemanticAnalyzer();
    }

    public boolean hasOccurrencesFinder() {
        return true;
    }

    public OccurrencesFinder getOccurrencesFinder() {
        return new GroovyOccurrencesFinder();
    }

    public boolean hasStructureScanner() {
        return true;
    }

    public StructureScanner getStructureScanner() {
        return new StructureAnalyzer();
    }

    public boolean hasHintsProvider() {
        return true;
    }

    public HintsProvider getHintsProvider() {
        return new GroovyHintsProvider();
    }

    public DeclarationFinder getDeclarationFinder() {
        return new GroovyDeclarationFinder();
    }

    public InstantRenamer getInstantRenamer() {
        return new GroovyInstantRenamer();
    }

    public IndexSearcher getIndexSearcher() {
        return new GroovyTypeSearcher();
    }

    public EmbeddingIndexerFactory getIndexerFactory() {
        return new GroovyIndexer.Factory();
    }

    public Set<String> getSourcePathIds() {
        return Collections.singleton("classpath/source");
    }
}

