/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.groovy.editor.completion.provider;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.spi.completion.CompletionProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public final class CompletionProviderHandler
implements CompletionProvider {
    @Override
    public Map<MethodSignature, CompletionItem> getMethods(CompletionContext context) {
        HashMap<MethodSignature, CompletionItem> result = new HashMap<MethodSignature, CompletionItem>();
        if (context.getSourceFile() != null) {
            for (CompletionProvider provider : Lookup.getDefault().lookupAll(CompletionProvider.class)) {
                for (Map.Entry<MethodSignature, CompletionItem> entry : provider.getMethods(context).entrySet()) {
                    if (!entry.getKey().getName().startsWith(context.getPrefix())) continue;
                    result.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return result;
    }

    @Override
    public Map<FieldSignature, CompletionItem> getFields(CompletionContext context) {
        HashMap<FieldSignature, CompletionItem> result = new HashMap<FieldSignature, CompletionItem>();
        if (context.getSourceFile() != null) {
            for (CompletionProvider provider : Lookup.getDefault().lookupAll(CompletionProvider.class)) {
                for (Map.Entry<FieldSignature, CompletionItem> entry : provider.getFields(context).entrySet()) {
                    if (!entry.getKey().getName().startsWith(context.getPrefix())) continue;
                    result.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return result;
    }

    @Override
    public Map<MethodSignature, CompletionItem> getStaticMethods(CompletionContext context) {
        HashMap<MethodSignature, CompletionItem> result = new HashMap<MethodSignature, CompletionItem>();
        if (context.getSourceFile() != null) {
            for (CompletionProvider provider : Lookup.getDefault().lookupAll(CompletionProvider.class)) {
                for (Map.Entry<MethodSignature, CompletionItem> entry : provider.getStaticMethods(context).entrySet()) {
                    if (!entry.getKey().getName().startsWith(context.getPrefix())) continue;
                    result.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return result;
    }

    @Override
    public Map<FieldSignature, CompletionItem> getStaticFields(CompletionContext context) {
        HashMap<FieldSignature, CompletionItem> result = new HashMap<FieldSignature, CompletionItem>();
        if (context.getSourceFile() != null) {
            for (CompletionProvider provider : Lookup.getDefault().lookupAll(CompletionProvider.class)) {
                for (Map.Entry<FieldSignature, CompletionItem> entry : provider.getStaticFields(context).entrySet()) {
                    if (!entry.getKey().getName().startsWith(context.getPrefix())) continue;
                    result.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return result;
    }
}

