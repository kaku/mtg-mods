/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.ColoringAttributes
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.SemanticAnalyzer
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 */
package org.netbeans.modules.groovy.editor.language;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.language.SemanticAnalysisVisitor;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;

public class GroovySemanticAnalyzer
extends SemanticAnalyzer<GroovyParserResult> {
    private boolean cancelled;
    private Map<OffsetRange, Set<ColoringAttributes>> semanticHighlights;

    public Map<OffsetRange, Set<ColoringAttributes>> getHighlights() {
        return this.semanticHighlights;
    }

    protected final synchronized boolean isCancelled() {
        return this.cancelled;
    }

    protected final synchronized void resume() {
        this.cancelled = false;
    }

    public final synchronized void cancel() {
        this.cancelled = true;
    }

    public int getPriority() {
        return 0;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void run(GroovyParserResult result, SchedulerEvent event) {
        this.resume();
        if (this.isCancelled()) {
            return;
        }
        ModuleNode root = ASTUtils.getRoot(result);
        if (root == null) {
            return;
        }
        HashMap<OffsetRange, Set<ColoringAttributes>> highlights = new HashMap<OffsetRange, Set<ColoringAttributes>>(100);
        AstPath path = new AstPath();
        path.descend((ASTNode)root);
        BaseDocument doc = LexUtilities.getDocument(result.getSnapshot().getSource(), false);
        if (doc == null) {
            return;
        }
        SemanticAnalysisVisitor visitor = new SemanticAnalysisVisitor(root, doc);
        highlights.putAll(visitor.annotate());
        path.ascend();
        if (this.isCancelled()) {
            return;
        }
        if (highlights.size() > 0) {
            HashMap translated = new HashMap(2 * highlights.size());
            for (Map.Entry entry : highlights.entrySet()) {
                OffsetRange range = LexUtilities.getLexerOffsets(result, (OffsetRange)entry.getKey());
                if (range == OffsetRange.NONE) continue;
                translated.put(range, entry.getValue());
            }
            highlights = translated;
            this.semanticHighlights = highlights;
        } else {
            this.semanticHighlights = null;
        }
    }
}

