/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 */
package org.netbeans.modules.groovy.editor.hints.infrastructure;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovyErrorRule;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovySelectionRule;
import org.netbeans.modules.parsing.spi.Parser;

public class GroovyHintsProvider
implements HintsProvider {
    public static final Logger LOG = Logger.getLogger(GroovyHintsProvider.class.getName());
    private boolean cancelled;

    public RuleContext createRuleContext() {
        return new RuleContext();
    }

    public void computeHints(HintsProvider.HintsManager manager, RuleContext context, List<Hint> hints) {
    }

    public void computeSuggestions(HintsProvider.HintsManager manager, RuleContext context, List<Hint> suggestions, int caretOffset) {
    }

    public void computeSelectionHints(HintsProvider.HintsManager manager, RuleContext context, List<Hint> result, int start, int end) {
        this.cancelled = false;
        ParserResult parserResult = context.parserResult;
        if (parserResult == null) {
            return;
        }
        ModuleNode root = ASTUtils.getRoot(parserResult);
        if (root == null) {
            return;
        }
        List hints = manager.getSelectionHints();
        if (hints.isEmpty()) {
            return;
        }
        if (this.isCancelled()) {
            return;
        }
        this.applyRules(context, hints, start, end, result);
    }

    public void computeErrors(HintsProvider.HintsManager manager, RuleContext context, List<Hint> result, List<Error> unhandled) {
        LOG.log(Level.FINEST, "@@@ computeErrors()");
        ParserResult info = context.parserResult;
        GroovyParserResult rpr = ASTUtils.getParseResult((Parser.Result)info);
        if (rpr == null) {
            return;
        }
        List<? extends Error> errors = rpr.getDiagnostics();
        LOG.log(Level.FINEST, "@@@ errors.size() : {0}", errors.size());
        if (errors.isEmpty()) {
            return;
        }
        this.cancelled = false;
        Map hints = manager.getErrors();
        if (hints.isEmpty() || this.isCancelled()) {
            unhandled.addAll(errors);
            return;
        }
        LOG.log(Level.FINEST, "@@@ hints.size() : {0}", hints.size());
        for (Error error : errors) {
            if (!(error instanceof GroovyError)) continue;
            LOG.log(Level.FINEST, "@@@ ----------------------------------------------------\n");
            LOG.log(Level.FINEST, "@@@ thread name   : {0}\n", Thread.currentThread().getName());
            LOG.log(Level.FINEST, "@@@ error.getDescription()   : {0}\n", error.getDescription());
            LOG.log(Level.FINEST, "@@@ error.getKey()           : {0}\n", error.getKey());
            LOG.log(Level.FINEST, "@@@ error.getDisplayName()   : {0}\n", error.getDisplayName());
            LOG.log(Level.FINEST, "@@@ error.getStartPosition() : {0}\n", error.getStartPosition());
            LOG.log(Level.FINEST, "@@@ error.getEndPosition()   : {0}\n", error.getEndPosition());
            boolean applyRet = this.applyRules((GroovyError)error, context, hints, result);
            LOG.log(Level.FINEST, "@@@ apply   : {0}\n", applyRet);
            if (applyRet) continue;
            LOG.log(Level.FINEST, "@@@ Adding error to unhandled");
            unhandled.add(error);
        }
        LOG.log(Level.FINEST, "@@@ result.size() =  {0}", result.size());
    }

    public void cancel() {
    }

    public List<Rule> getBuiltinRules() {
        return null;
    }

    private boolean isCancelled() {
        return this.cancelled;
    }

    private void applyRules(RuleContext context, List<GroovySelectionRule> rules, int start, int end, List<Hint> result) {
        for (GroovySelectionRule rule : rules) {
            if (!rule.appliesTo(context)) continue;
            rule.run(context, result);
        }
    }

    private boolean applyRules(GroovyError error, RuleContext context, Map<CompilerErrorID, List<GroovyErrorRule>> hints, List<Hint> result) {
        List<GroovyErrorRule> rules;
        LOG.log(Level.FINEST, "applyRules(...)");
        CompilerErrorID code = error.getId();
        if (code != null && (rules = hints.get((Object)code)) != null) {
            int countBefore = result.size();
            for (GroovyErrorRule rule : rules) {
                if (!rule.appliesTo(context)) continue;
                rule.run(context, error, result);
            }
            return countBefore < result.size();
        }
        return false;
    }
}

