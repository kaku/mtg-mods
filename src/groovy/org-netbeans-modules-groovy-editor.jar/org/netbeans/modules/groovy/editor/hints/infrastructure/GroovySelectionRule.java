/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$SelectionRule
 *  org.netbeans.modules.csl.api.RuleContext
 */
package org.netbeans.modules.groovy.editor.hints.infrastructure;

import java.util.List;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;

public abstract class GroovySelectionRule
implements Rule.SelectionRule {
    public abstract void run(RuleContext var1, List<Hint> var2);
}

