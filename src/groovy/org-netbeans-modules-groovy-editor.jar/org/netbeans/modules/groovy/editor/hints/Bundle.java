/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.groovy.editor.hints;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String AddIfAroundBlockHintDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"AddIfAroundBlockHintDescription");
    }

    static String AddMissingMethodsStub() {
        return NbBundle.getMessage(Bundle.class, (String)"AddMissingMethodsStub");
    }

    static String ClassNotFoundRuleHintDescription(Object fully_qualified_name_of_the_class_we_want_to_import) {
        return NbBundle.getMessage(Bundle.class, (String)"ClassNotFoundRuleHintDescription", (Object)fully_qualified_name_of_the_class_we_want_to_import);
    }

    static String CommentOutRuleDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CommentOutRuleDescription");
    }

    static String CommentOutRuleHintDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CommentOutRuleHintDescription");
    }

    static String FixImportsHintDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"FixImportsHintDescription");
    }

    static String ImplementInterfaceHintDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"ImplementInterfaceHintDescription");
    }

    static String MakeClassAbstract(Object class_name) {
        return NbBundle.getMessage(Bundle.class, (String)"MakeClassAbstract", (Object)class_name);
    }

    static String Test() {
        return NbBundle.getMessage(Bundle.class, (String)"Test");
    }

    private void Bundle() {
    }
}

