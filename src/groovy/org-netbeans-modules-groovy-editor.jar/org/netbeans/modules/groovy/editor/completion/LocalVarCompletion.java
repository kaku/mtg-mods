/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.CompletionProposal
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;
import org.netbeans.modules.groovy.editor.completion.inference.VariableFinderVisitor;

public class LocalVarCompletion
extends BaseCompletion {
    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext request, int anchor) {
        LOG.log(Level.FINEST, "-> completeLocalVars");
        if (!(request.location == CaretLocation.INSIDE_CLOSURE || request.location == CaretLocation.INSIDE_METHOD || request.location == CaretLocation.INSIDE_STRING && request.getPrefix().matches("\\$[^\\{].*"))) {
            LOG.log(Level.FINEST, "Not inside method, closure or in-string variable, bail out.");
            return false;
        }
        if (request.isBehindDot()) {
            LOG.log(Level.FINEST, "We are invoked right behind a dot.");
            return false;
        }
        VariableFinderVisitor vis = new VariableFinderVisitor(((ModuleNode)request.path.root()).getContext(), request.path, request.doc, request.astOffset);
        vis.collect();
        boolean updated = false;
        int anchorShift = 0;
        String varPrefix = request.getPrefix();
        if (request.getPrefix().startsWith("$")) {
            varPrefix = request.getPrefix().substring(1);
            anchorShift = 1;
        }
        for (Variable node : vis.getVariables()) {
            String varName = node.getName();
            LOG.log(Level.FINEST, "Node found: {0}", varName);
            if (varPrefix.length() < 1) {
                proposals.add((CompletionProposal)new CompletionItem.LocalVarItem(node, anchor + anchorShift));
                updated = true;
                continue;
            }
            if (varName.equals(varPrefix) || !varName.startsWith(varPrefix)) continue;
            proposals.add((CompletionProposal)new CompletionItem.LocalVarItem(node, anchor + anchorShift));
            updated = true;
        }
        return updated;
    }
}

