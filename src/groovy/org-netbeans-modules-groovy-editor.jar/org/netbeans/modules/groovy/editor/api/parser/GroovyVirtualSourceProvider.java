/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassHelper
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.GenericsType
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.expr.ArgumentListExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.ExpressionStatement
 *  org.codehaus.groovy.ast.stmt.Statement
 *  org.codehaus.groovy.control.ResolveVisitor
 *  org.netbeans.modules.java.preprocessorbridge.spi.VirtualSourceProvider
 *  org.netbeans.modules.java.preprocessorbridge.spi.VirtualSourceProvider$Result
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.codehaus.groovy.ast.ClassHelper;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.GenericsType;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.control.ResolveVisitor;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTRoot;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.java.preprocessorbridge.spi.VirtualSourceProvider;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class GroovyVirtualSourceProvider
implements VirtualSourceProvider {
    public Set<String> getSupportedExtensions() {
        return Collections.singleton("groovy");
    }

    public void translate(Iterable<File> files, File sourceRoot, VirtualSourceProvider.Result result) {
        JavaStubGenerator generator = new JavaStubGenerator();
        FileObject rootFO = FileUtil.toFileObject((File)sourceRoot);
        Iterator<File> it = files.iterator();
        while (it.hasNext()) {
            File file = FileUtil.normalizeFile((File)it.next());
            List<ClassNode> classNodes = GroovyVirtualSourceProvider.getClassNodes(file);
            if (classNodes.isEmpty()) {
                String pkg;
                FileObject fo = FileUtil.toFileObject((File)file);
                if (fo == null || (pkg = FileUtil.getRelativePath((FileObject)rootFO, (FileObject)fo.getParent())) == null) continue;
                pkg = pkg.replace('/', '.');
                StringBuilder sb = new StringBuilder();
                if (!pkg.equals("")) {
                    sb.append("package ").append(pkg).append(";");
                }
                String name = fo.getName();
                sb.append("public class ").append(name).append("{}");
                result.add(file, pkg, name, (CharSequence)sb.toString());
                continue;
            }
            for (ClassNode classNode : classNodes) {
                try {
                    CharSequence javaStub = generator.generateClass(classNode);
                    String pkgName = classNode.getPackageName();
                    if (pkgName == null) {
                        pkgName = "";
                    }
                    result.add(file, pkgName, classNode.getNameWithoutPackage(), javaStub);
                }
                catch (FileNotFoundException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
    }

    static List<ClassNode> getClassNodes(File file) {
        Source source;
        final ArrayList<ClassNode> resultList = new ArrayList<ClassNode>();
        FileObject fo = FileUtil.toFileObject((File)file);
        if (fo != null && "text/x-groovy".equals((source = Source.create((FileObject)fo)).getMimeType())) {
            try {
                ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        GroovyParserResult result;
                        ASTRoot astRootElement;
                        ModuleNode moduleNode;
                        Parser.Result parseResult = resultIterator.getParserResult();
                        if (parseResult != null && (astRootElement = (result = ASTUtils.getParseResult(parseResult)).getRootElement()) != null && (moduleNode = astRootElement.getModuleNode()) != null) {
                            resultList.addAll(moduleNode.getClasses());
                        }
                    }
                });
            }
            catch (ParseException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return resultList;
    }

    public boolean index() {
        return false;
    }

    static final class JavaStubGenerator {
        private boolean java5 = false;
        private boolean requireSuperResolved = false;
        private List toCompile = new ArrayList();

        private JavaStubGenerator(boolean requireSuperResolved, boolean java5) {
            this.requireSuperResolved = requireSuperResolved;
            this.java5 = java5;
        }

        public JavaStubGenerator() {
            this(false, true);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public CharSequence generateClass(ClassNode classNode) throws FileNotFoundException {
            if (this.requireSuperResolved && !classNode.getSuperClass().isResolved()) {
                return null;
            }
            String fileName = classNode.getName().replace('.', '/');
            this.toCompile.add(fileName);
            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);
            try {
                ClassNode[] interfaces;
                String packageName = classNode.getPackageName();
                if (packageName != null) {
                    out.println("package " + packageName + ";\n");
                }
                this.genImports(classNode, out);
                boolean isInterface = classNode.isInterface();
                boolean isEnum = (classNode.getModifiers() & 16384) != 0;
                this.printModifiers(out, classNode.getModifiers() & ~ (isInterface ? 1024 : 0));
                if (isInterface) {
                    out.print("interface ");
                } else if (isEnum) {
                    out.print("enum ");
                } else {
                    out.print("class ");
                }
                out.println(classNode.getNameWithoutPackage());
                this.writeGenericsBounds(out, classNode, true);
                ClassNode superClass = classNode.getUnresolvedSuperClass(false);
                if (!isInterface && !isEnum) {
                    out.print("  extends ");
                    this.printType(superClass, out);
                }
                if ((interfaces = classNode.getInterfaces()) != null && interfaces.length > 0) {
                    if (isInterface) {
                        out.println("  extends");
                    } else {
                        out.println("  implements");
                    }
                    for (int i = 0; i < interfaces.length - 1; ++i) {
                        out.print("    ");
                        this.printType(interfaces[i], out);
                        out.print(",");
                    }
                    out.print("    ");
                    this.printType(interfaces[interfaces.length - 1], out);
                }
                out.println(" {");
                this.genFields(classNode, out, isEnum);
                this.genMethods(classNode, out, isEnum);
                this.genProps(classNode, out);
                out.println("}");
            }
            finally {
                try {
                    out.close();
                }
                catch (Exception e) {}
                try {
                    sw.close();
                }
                catch (IOException e) {}
            }
            return sw.toString();
        }

        private void genMethods(ClassNode classNode, PrintWriter out, boolean isEnum) {
            List methods;
            if (!isEnum) {
                this.getConstructors(classNode, out);
            }
            if ((methods = classNode.getMethods()) != null) {
                for (MethodNode methodNode : methods) {
                    if (isEnum && methodNode.isSynthetic()) {
                        String name = methodNode.getName();
                        Parameter[] params = methodNode.getParameters();
                        if (name.equals("values") && params.length == 0 || name.equals("valueOf") && params.length == 1 && params[0].getType().equals((Object)ClassHelper.STRING_TYPE)) continue;
                    }
                    this.genMethod(classNode, methodNode, out);
                }
            }
            List properties = classNode.getProperties();
            for (Object object : properties) {
                MethodNode isMethod;
                MethodNode setter;
                PropertyNode propertyNode = (PropertyNode)object;
                if (propertyNode.isSynthetic()) continue;
                String name = propertyNode.getName();
                name = "" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
                MethodNode getter = classNode.getGetterMethod("get" + name);
                if (getter != null) {
                    this.genMethod(classNode, getter, out, false);
                }
                if ((setter = classNode.getSetterMethod("set" + name)) != null) {
                    this.genMethod(classNode, setter, out, false);
                }
                if ((isMethod = classNode.getDeclaredMethod("is" + name, new Parameter[0])) == null) continue;
                this.genMethod(classNode, isMethod, out, false);
            }
        }

        private void getConstructors(ClassNode classNode, PrintWriter out) {
            List constrs = classNode.getDeclaredConstructors();
            if (constrs != null) {
                for (ConstructorNode constrNode : constrs) {
                    this.genConstructor(classNode, constrNode, out);
                }
            }
        }

        private void genFields(ClassNode classNode, PrintWriter out, boolean isEnum) {
            List fields = classNode.getFields();
            if (fields == null) {
                return;
            }
            ArrayList<FieldNode> enumFields = new ArrayList<FieldNode>(fields.size());
            ArrayList<FieldNode> normalFields = new ArrayList<FieldNode>(fields.size());
            for (FieldNode fieldNode2 : fields) {
                boolean isSynthetic;
                boolean isEnumField = (fieldNode2.getModifiers() & 16384) != 0;
                boolean bl = isSynthetic = (fieldNode2.getModifiers() & 4096) != 0;
                if (isEnumField) {
                    enumFields.add(fieldNode2);
                    continue;
                }
                if (isSynthetic) continue;
                normalFields.add(fieldNode2);
            }
            this.genEnumFields(enumFields, out);
            for (FieldNode fieldNode2 : normalFields) {
                this.genField(fieldNode2, out);
            }
        }

        private void genProps(ClassNode classNode, PrintWriter out) {
            List props = classNode.getProperties();
            if (props != null) {
                for (PropertyNode propNode : props) {
                    this.genProp(propNode, out);
                }
            }
        }

        private void genProp(PropertyNode propNode, PrintWriter out) {
            String name = propNode.getName().substring(0, 1).toUpperCase() + propNode.getName().substring(1);
            String getterName = "get" + name;
            boolean skipGetter = false;
            List getterCandidates = propNode.getField().getOwner().getMethods(getterName);
            if (getterCandidates != null) {
                for (MethodNode method : getterCandidates) {
                    if (method.getParameters().length != 0) continue;
                    skipGetter = true;
                }
            }
            if (!skipGetter) {
                this.printModifiers(out, propNode.getModifiers());
                this.printType(propNode.getType(), out);
                out.print(" ");
                out.print(getterName);
                out.print("() { ");
                this.printReturn(out, propNode.getType());
                out.println(" }");
            }
            String setterName = "set" + name;
            boolean skipSetter = false;
            List setterCandidates = propNode.getField().getOwner().getMethods(setterName);
            if (setterCandidates != null) {
                for (MethodNode method : setterCandidates) {
                    if (method.getParameters().length != 1) continue;
                    skipSetter = true;
                }
            }
            if (!skipSetter) {
                this.printModifiers(out, propNode.getModifiers());
                out.print("void ");
                out.print(setterName);
                out.print("(");
                this.printType(propNode.getType(), out);
                out.println(" value) {}");
            }
        }

        private void genEnumFields(List fields, PrintWriter out) {
            if (fields.isEmpty()) {
                return;
            }
            boolean first = true;
            for (FieldNode fieldNode : fields) {
                if (!first) {
                    out.print(", ");
                } else {
                    first = false;
                }
                out.print(fieldNode.getName());
            }
            out.println(";");
        }

        private void genField(FieldNode fieldNode, PrintWriter out) {
            if (fieldNode.isSynthetic() || "metaClass".equals(fieldNode.getName())) {
                return;
            }
            if ((fieldNode.getModifiers() & 2) != 0) {
                return;
            }
            this.printModifiers(out, fieldNode.getModifiers());
            this.printType(fieldNode.getType(), out);
            out.print(" ");
            out.print(fieldNode.getName());
            out.println(";");
        }

        private ConstructorCallExpression getConstructorCallExpression(ConstructorNode constructorNode) {
            Statement code = constructorNode.getCode();
            if (!(code instanceof BlockStatement)) {
                return null;
            }
            BlockStatement block = (BlockStatement)code;
            List stats = block.getStatements();
            if (stats == null || stats.isEmpty()) {
                return null;
            }
            Statement stat = (Statement)stats.get(0);
            if (!(stat instanceof ExpressionStatement)) {
                return null;
            }
            Expression expr = ((ExpressionStatement)stat).getExpression();
            if (!(expr instanceof ConstructorCallExpression)) {
                return null;
            }
            return (ConstructorCallExpression)expr;
        }

        private void genConstructor(ClassNode clazz, ConstructorNode constructorNode, PrintWriter out) {
            if (constructorNode.isSynthetic()) {
                return;
            }
            out.print("public ");
            out.print(clazz.getNameWithoutPackage());
            this.printParams((MethodNode)constructorNode, out);
            ConstructorCallExpression constrCall = this.getConstructorCallExpression(constructorNode);
            if (constrCall == null || !constrCall.isSpecialCall()) {
                out.println(" {}");
            } else {
                out.println(" {");
                this.genSpecialConstructorArgs(out, constructorNode, constrCall);
                out.println("}");
            }
        }

        private Parameter[] selectAccessibleConstructorFromSuper(ConstructorNode node) {
            ClassNode type = node.getDeclaringClass();
            ClassNode superType = type.getSuperClass();
            boolean hadPrivateConstructor = false;
            for (ConstructorNode c : superType.getDeclaredConstructors()) {
                if (!c.isPublic() && !c.isProtected()) continue;
                return c.getParameters();
            }
            if (superType.isPrimaryClassNode()) {
                return Parameter.EMPTY_ARRAY;
            }
            return null;
        }

        private void genSpecialConstructorArgs(PrintWriter out, ConstructorNode node, ConstructorCallExpression constrCall) {
            Parameter[] params = this.selectAccessibleConstructorFromSuper(node);
            if (params != null) {
                out.print("super (");
                for (int i = 0; i < params.length; ++i) {
                    this.printDefaultValue(out, params[i].getType());
                    if (i + 1 >= params.length) continue;
                    out.print(", ");
                }
                out.println(");");
                return;
            }
            Expression arguments = constrCall.getArguments();
            if (constrCall.isSuperCall()) {
                out.print("super(");
            } else {
                out.print("this(");
            }
            if (arguments instanceof ArgumentListExpression) {
                ArgumentListExpression argumentListExpression = (ArgumentListExpression)arguments;
                List args = argumentListExpression.getExpressions();
                for (Expression arg : args) {
                    if (arg instanceof ConstantExpression) {
                        ConstantExpression expression = (ConstantExpression)arg;
                        Object o = expression.getValue();
                        if (o instanceof String) {
                            out.print("(String)null");
                        } else {
                            out.print(expression.getText());
                        }
                    } else {
                        this.printDefaultValue(out, arg.getType());
                    }
                    if (arg == args.get(args.size() - 1)) continue;
                    out.print(", ");
                }
            }
            out.println(");");
        }

        private void genMethod(ClassNode clazz, MethodNode methodNode, PrintWriter out) {
            this.genMethod(clazz, methodNode, out, true);
        }

        private void genMethod(ClassNode clazz, MethodNode methodNode, PrintWriter out, boolean ignoreSynthetic) {
            String name = methodNode.getName();
            if (ignoreSynthetic && methodNode.isSynthetic() || name.startsWith("super$")) {
                return;
            }
            if (methodNode.getName().equals("<clinit>")) {
                return;
            }
            if (!clazz.isInterface()) {
                this.printModifiers(out, methodNode.getModifiers());
            }
            this.printType(methodNode.getReturnType(), out);
            out.print(" ");
            out.print(methodNode.getName());
            this.printParams(methodNode, out);
            ClassNode[] exceptions = methodNode.getExceptions();
            if (exceptions != null && exceptions.length > 0) {
                out.print(" throws ");
                for (int i = 0; i < exceptions.length; ++i) {
                    if (i > 0) {
                        out.print(", ");
                    }
                    this.printType(exceptions[i], out);
                }
            }
            if ((methodNode.getModifiers() & 1024) != 0) {
                out.println(";");
            } else {
                out.print(" { ");
                ClassNode retType = methodNode.getReturnType();
                this.printReturn(out, retType);
                out.println("}");
            }
        }

        private void printReturn(PrintWriter out, ClassNode retType) {
            String retName = retType.getName();
            if (!retName.equals("void")) {
                out.print("return ");
                this.printDefaultValue(out, retType);
                out.print(";");
            }
        }

        private void printDefaultValue(PrintWriter out, ClassNode type) {
            if (type.redirect() != ClassHelper.OBJECT_TYPE) {
                out.print("(");
                this.printType(type, out);
                out.print(")");
            }
            if (ClassHelper.isPrimitiveType((ClassNode)type)) {
                if (type == ClassHelper.boolean_TYPE) {
                    out.print("false");
                } else {
                    out.print("0");
                }
            } else {
                out.print("null");
            }
        }

        private void printType(ClassNode type, PrintWriter out) {
            if (type.isArray()) {
                this.printType(type.getComponentType(), out);
                out.print("[]");
            } else {
                this.writeGenericsBounds(out, type, false);
            }
        }

        private void printTypeName(ClassNode type, PrintWriter out) {
            if (ClassHelper.isPrimitiveType((ClassNode)type)) {
                if (type == ClassHelper.boolean_TYPE) {
                    out.print("boolean");
                } else if (type == ClassHelper.char_TYPE) {
                    out.print("char");
                } else if (type == ClassHelper.int_TYPE) {
                    out.print("int");
                } else if (type == ClassHelper.short_TYPE) {
                    out.print("short");
                } else if (type == ClassHelper.long_TYPE) {
                    out.print("long");
                } else if (type == ClassHelper.float_TYPE) {
                    out.print("float");
                } else if (type == ClassHelper.double_TYPE) {
                    out.print("double");
                } else if (type == ClassHelper.byte_TYPE) {
                    out.print("byte");
                } else {
                    out.print("void");
                }
            } else {
                out.print(type.redirect().getName().replace('$', '.'));
            }
        }

        private void writeGenericsBounds(PrintWriter out, ClassNode type, boolean skipName) {
            if (!skipName) {
                this.printTypeName(type, out);
            }
            if (this.java5 && !type.isGenericsPlaceHolder()) {
                this.writeGenericsBounds(out, type.getGenericsTypes());
            }
        }

        private void writeGenericsBounds(PrintWriter out, GenericsType[] genericsTypes) {
            if (genericsTypes == null || genericsTypes.length == 0) {
                return;
            }
            out.print('<');
            for (int i = 0; i < genericsTypes.length; ++i) {
                if (i != 0) {
                    out.print(", ");
                }
                this.writeGenericsBounds(out, genericsTypes[i]);
            }
            out.print('>');
        }

        private void writeGenericsBounds(PrintWriter out, GenericsType genericsType) {
            if (genericsType.isPlaceholder()) {
                out.print(genericsType.getName());
            } else {
                this.printTypeName(genericsType.getType(), out);
                ClassNode[] upperBounds = genericsType.getUpperBounds();
                ClassNode lowerBound = genericsType.getLowerBound();
                if (upperBounds != null) {
                    out.print(" extends ");
                    for (int i = 0; i < upperBounds.length; ++i) {
                        this.printType(upperBounds[i], out);
                        if (i + 1 >= upperBounds.length) continue;
                        out.print(" & ");
                    }
                } else if (lowerBound != null) {
                    out.print(" super ");
                    this.printType(lowerBound, out);
                }
            }
        }

        private void printParams(MethodNode methodNode, PrintWriter out) {
            out.print("(");
            Parameter[] parameters = methodNode.getParameters();
            if (parameters != null && parameters.length != 0) {
                for (int i = 0; i != parameters.length; ++i) {
                    this.printType(parameters[i].getType(), out);
                    out.print(" ");
                    out.print(parameters[i].getName());
                    if (i + 1 >= parameters.length) continue;
                    out.print(", ");
                }
            }
            out.print(")");
        }

        private void printModifiers(PrintWriter out, int modifiers) {
            if ((modifiers & 1) != 0) {
                out.print("public ");
            }
            if ((modifiers & 4) != 0) {
                out.print("protected ");
            }
            if ((modifiers & 2) != 0) {
                out.print("private ");
            }
            if ((modifiers & 8) != 0) {
                out.print("static ");
            }
            if ((modifiers & 32) != 0) {
                out.print("synchronized ");
            }
            if ((modifiers & 1024) != 0) {
                out.print("abstract ");
            }
        }

        private void genImports(ClassNode classNode, PrintWriter out) {
            HashSet<String> imports = new HashSet<String>();
            imports.addAll(Arrays.asList(ResolveVisitor.DEFAULT_IMPORTS));
            ModuleNode moduleNode = classNode.getModule();
            for (ImportNode importNode : moduleNode.getImports()) {
                if (!importNode.isStar()) continue;
                imports.add(importNode.getPackageName() + ".");
            }
            for (ImportNode imp2 : moduleNode.getImports()) {
                String name = imp2.getType().getName();
                int lastDot = name.lastIndexOf(46);
                if (lastDot == -1) continue;
                imports.add(name.substring(0, lastDot + 1));
            }
            for (String imp : imports) {
                out.print("import ");
                out.print(imp);
                out.println("*;");
            }
            out.println();
        }
    }

}

