/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementKind
 */
package org.netbeans.modules.groovy.editor.api.elements;

import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.GroovyElement;

public class CommentElement
extends GroovyElement {
    private final String text;

    public CommentElement(String text) {
        this.text = text;
    }

    @Override
    public String getName() {
        return this.text;
    }

    @Override
    public ElementKind getKind() {
        return ElementKind.KEYWORD;
    }
}

