/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.Formatter
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.language;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.openide.util.Exceptions;

public class GroovyFormatter
implements Formatter {
    public boolean needsParserResult() {
        return false;
    }

    public void reindent(Context context) {
        this.reindent(context, null, true);
    }

    public void reformat(Context context, ParserResult compilationInfo) {
        this.reindent(context, compilationInfo, false);
    }

    public int indentSize() {
        return IndentUtils.indentLevelSize((Document)null);
    }

    public int hangingIndentSize() {
        return CodeStylePreferences.get((Document)null).getPreferences().getInt("continuationIndentSize", 4);
    }

    private int getFormatStableStart(BaseDocument doc, int offset) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts == null) {
            return 0;
        }
        ts.move(offset);
        if (!ts.movePrevious()) {
            return 0;
        }
        do {
            TokenId id;
            Token token;
            if ((id = (token = ts.token()).id()) != GroovyTokenId.LITERAL_class) continue;
            return ts.offset();
        } while (ts.movePrevious());
        return ts.offset();
    }

    private int getTokenBalanceDelta(TokenId id, Token<GroovyTokenId> token, BaseDocument doc, TokenSequence<GroovyTokenId> ts, boolean includeKeywords) {
        if (id == GroovyTokenId.IDENTIFIER) {
            if (token.length() == 1) {
                char c = token.text().charAt(0);
                if (c == '[') {
                    return 1;
                }
                if (c == ']') {
                    return -1;
                }
            }
        } else {
            if (id == GroovyTokenId.LPAREN || id == GroovyTokenId.LBRACKET || id == GroovyTokenId.LBRACE) {
                return 1;
            }
            if (id == GroovyTokenId.RPAREN || id == GroovyTokenId.RBRACKET || id == GroovyTokenId.RBRACE) {
                return -1;
            }
            if (includeKeywords) {
                if (LexUtilities.isBeginToken(id, doc, ts)) {
                    return 1;
                }
                if (id == GroovyTokenId.RBRACE) {
                    return -1;
                }
            }
        }
        return 0;
    }

    private int getTokenBalance(BaseDocument doc, int begin, int end, boolean includeKeywords) {
        int balance = 0;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, begin);
        if (ts == null) {
            return 0;
        }
        ts.move(begin);
        if (!ts.moveNext()) {
            return 0;
        }
        do {
            Token token = ts.token();
            TokenId id = token.id();
            balance += this.getTokenBalanceDelta(id, token, doc, ts, includeKeywords);
        } while (ts.moveNext() && ts.offset() < end);
        return balance;
    }

    private boolean isJavaDocComment(BaseDocument doc, int offset, int endOfLine) throws BadLocationException {
        Token<GroovyTokenId> token;
        String text;
        TokenId id;
        int pos = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (pos != -1 && (token = LexUtilities.getToken(doc, pos)) != null && (id = token.id()) == GroovyTokenId.BLOCK_COMMENT && (text = doc.getText(offset, endOfLine - offset)).trim().startsWith("*")) {
            return true;
        }
        return false;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private boolean isInLiteral(BaseDocument doc, int offset) throws BadLocationException {
        TokenId id;
        int pos = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (pos != -1) {
            Token<GroovyTokenId> token = LexUtilities.getToken(doc, pos);
            if (token == null) return true;
            TokenId id2 = token.id();
            if (id2 != GroovyTokenId.STRING_LITERAL && id2 != GroovyTokenId.REGEXP_LITERAL) return false;
            return true;
        }
        Token<GroovyTokenId> token = LexUtilities.getToken(doc, offset);
        if (token == null || (id = token.id()) != GroovyTokenId.STRING_LITERAL && id != GroovyTokenId.REGEXP_LITERAL) return false;
        return true;
    }

    private boolean isEndIndent(BaseDocument doc, int offset) throws BadLocationException {
        int lineBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (lineBegin != -1) {
            Token<GroovyTokenId> token = LexUtilities.getToken(doc, lineBegin);
            if (token == null) {
                return false;
            }
            TokenId id = token.id();
            return LexUtilities.isIndentToken(id) && !LexUtilities.isBeginToken(id, doc, offset) || id == GroovyTokenId.RBRACE || id == GroovyTokenId.RBRACKET || id == GroovyTokenId.RPAREN;
        }
        return false;
    }

    private boolean isLineContinued(BaseDocument doc, int offset, int bracketBalance) throws BadLocationException {
        if ((offset = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)offset)) == -1) {
            return false;
        }
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts == null) {
            return false;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return false;
        }
        Token<GroovyTokenId> token = ts.token();
        if (token != null) {
            boolean isContinuationOperator;
            TokenId id = token.id();
            boolean bl = isContinuationOperator = id == GroovyTokenId.DOT;
            if (ts.offset() == offset && token.length() > 1 && token.text().toString().startsWith("\\")) {
                isContinuationOperator = true;
            }
            if (token.length() == 1 && id == GroovyTokenId.IDENTIFIER && token.text().toString().equals(",") && bracketBalance == 0) {
                isContinuationOperator = true;
            }
            if (isContinuationOperator) {
                token = LexUtilities.getToken(doc, Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset));
                if (token != null && (id = token.id()) == GroovyTokenId.LBRACE) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    private void reindent(final Context context, ParserResult info, boolean indentOnly) {
        Document document = context.document();
        int endOffset = Math.min(context.endOffset(), document.getLength());
        try {
            int startOffset;
            final BaseDocument doc = (BaseDocument)document;
            final int lineStart = startOffset = Utilities.getRowStart((BaseDocument)doc, (int)context.startOffset());
            int initialOffset = 0;
            int initialIndent = 0;
            if (startOffset > 0) {
                int prevOffset = Utilities.getRowStart((BaseDocument)doc, (int)(startOffset - 1));
                initialOffset = this.getFormatStableStart(doc, prevOffset);
                initialIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)initialOffset);
            }
            final ArrayList<Integer> offsets = new ArrayList<Integer>();
            final ArrayList<Integer> indents = new ArrayList<Integer>();
            boolean indentEmptyLines = startOffset != 0 || endOffset != doc.getLength();
            boolean includeEnd = endOffset == doc.getLength() || indentOnly;
            this.computeIndents(doc, initialIndent, initialOffset, endOffset, info, offsets, indents, indentEmptyLines, includeEnd, indentOnly);
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        assert (indents.size() == offsets.size());
                        for (int i = indents.size() - 1; i >= 0; --i) {
                            int indent = (Integer)indents.get(i);
                            int lineBegin = (Integer)offsets.get(i);
                            if (lineBegin >= lineStart) {
                                int currentIndent;
                                if (lineBegin == lineStart && i > 0) {
                                    int prevOffset = (Integer)offsets.get(i - 1);
                                    int prevIndent = (Integer)indents.get(i - 1);
                                    int actualPrevIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)prevOffset);
                                    if (actualPrevIndent != prevIndent && !Utilities.isRowEmpty((BaseDocument)doc, (int)prevOffset) && !Utilities.isRowWhite((BaseDocument)doc, (int)prevOffset) && (indent = actualPrevIndent + (indent - prevIndent)) < 0) {
                                        indent = 0;
                                    }
                                }
                                if ((currentIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)lineBegin)) == indent) continue;
                                context.modifyIndent(lineBegin, indent);
                                continue;
                            }
                            break;
                        }
                    }
                    catch (BadLocationException ble) {
                        Exceptions.printStackTrace((Throwable)ble);
                    }
                }
            });
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
    }

    private void computeIndents(BaseDocument doc, int initialIndent, int startOffset, int endOffset, ParserResult info, List<Integer> offsets, List<Integer> indents, boolean indentEmptyLines, boolean includeEnd, boolean indentOnly) {
        try {
            int offset = Utilities.getRowStart((BaseDocument)doc, (int)startOffset);
            int end = endOffset;
            int indentSize = IndentUtils.indentLevelSize((Document)doc);
            int hangingIndentSize = this.hangingIndentSize();
            int balance = 0;
            int bracketBalance = 0;
            boolean continued = false;
            while (!includeEnd && offset < end || includeEnd && offset <= end) {
                int lineBegin;
                int hangingIndent;
                int n = hangingIndent = continued ? hangingIndentSize : 0;
                int indent = this.isInLiteral(doc, offset) ? GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset) : (this.isEndIndent(doc, offset) ? (balance - 1) * indentSize + hangingIndent + initialIndent : balance * indentSize + hangingIndent + initialIndent);
                int endOfLine = Utilities.getRowEnd((BaseDocument)doc, (int)offset) + 1;
                if (this.isJavaDocComment(doc, offset, endOfLine)) {
                    ++indent;
                }
                if (indent < 0) {
                    indent = 0;
                }
                if ((lineBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset)) != -1 || indentEmptyLines) {
                    indents.add(indent);
                    offsets.add(offset);
                }
                if (lineBegin != -1) {
                    balance += this.getTokenBalance(doc, lineBegin, endOfLine, true);
                    continued = this.isLineContinued(doc, offset, bracketBalance += this.getTokenBalance(doc, lineBegin, endOfLine, false));
                }
                offset = endOfLine;
            }
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
    }

}

