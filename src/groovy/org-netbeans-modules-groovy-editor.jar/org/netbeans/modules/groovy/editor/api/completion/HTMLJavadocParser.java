/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.api.completion;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.StringTokenizer;
import javax.swing.text.ChangedCharSetException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

final class HTMLJavadocParser {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String getJavadocText(URL url, boolean pkg, boolean isGDK) {
        if (url == null) {
            return null;
        }
        InputStream is = null;
        String charset = null;
        do {
            try {
                String memberName;
                InputStreamReader reader;
                is = url.openStream();
                ParserDelegator parser = new ParserDelegator();
                String urlStr = URLDecoder.decode(url.toString(), "UTF-8");
                int[] offsets = new int[2];
                InputStreamReader inputStreamReader = reader = charset == null ? new InputStreamReader(is) : new InputStreamReader(is, charset);
                if (pkg) {
                    offsets = HTMLJavadocParser.parsePackage(reader, parser, charset != null);
                } else if (urlStr.indexOf(35) > 0) {
                    memberName = urlStr.substring(urlStr.indexOf(35) + 1);
                    if (memberName.length() > 0) {
                        offsets = HTMLJavadocParser.parseMember(reader, memberName, parser, charset != null, isGDK);
                    }
                } else {
                    offsets = HTMLJavadocParser.parseClass(reader, parser, charset != null);
                }
                if (offsets == null || offsets[0] == -1 || offsets[1] <= offsets[0]) break;
                memberName = HTMLJavadocParser.getTextFromURLStream(url, offsets[0], offsets[1], charset);
                return memberName;
            }
            catch (ChangedCharSetException e) {
                if (charset == null) {
                    charset = HTMLJavadocParser.getCharSet(e);
                    continue;
                }
                e.printStackTrace();
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            finally {
                if (is == null) continue;
                try {
                    is.close();
                    continue;
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                continue;
            }
            break;
        } while (true);
        return null;
    }

    private static String getCharSet(ChangedCharSetException e) {
        String spec = e.getCharSetSpec();
        if (e.keyEqualsCharSet()) {
            return spec;
        }
        int index = spec.indexOf(";");
        if (index != -1) {
            spec = spec.substring(index + 1);
        }
        spec = spec.toLowerCase(Locale.ENGLISH);
        StringTokenizer st = new StringTokenizer(spec, " \t=", true);
        boolean foundCharSet = false;
        boolean foundEquals = false;
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (token.equals(" ") || token.equals("\t")) continue;
            if (!foundCharSet && !foundEquals && token.equals("charset")) {
                foundCharSet = true;
                continue;
            }
            if (!foundEquals && token.equals("=")) {
                foundEquals = true;
                continue;
            }
            if (foundEquals && foundCharSet) {
                return token;
            }
            foundCharSet = false;
            foundEquals = false;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String getTextFromURLStream(URL url, int startOffset, int endOffset, String charset) throws IOException {
        char[] buffer;
        if (url == null) {
            return null;
        }
        if (startOffset > endOffset) {
            throw new IOException();
        }
        InputStream fis = url.openStream();
        InputStreamReader fisreader = charset == null ? new InputStreamReader(fis) : new InputStreamReader(fis, charset);
        try {
            int count;
            int len = endOffset - startOffset;
            int bytesAlreadyRead = 0;
            buffer = new char[len];
            int bytesToSkip = startOffset;
            long bytesSkipped = 0;
            do {
                bytesSkipped = fisreader.skip(bytesToSkip);
            } while ((bytesToSkip = (int)((long)bytesToSkip - bytesSkipped)) > 0 && bytesSkipped > 0);
            while ((count = fisreader.read(buffer, bytesAlreadyRead, len - bytesAlreadyRead)) >= 0 && (bytesAlreadyRead += count) < len) {
            }
        }
        finally {
            fisreader.close();
        }
        return new String(buffer);
    }

    private static int[] parseClass(Reader reader, HTMLEditorKit.Parser parser, boolean ignoreCharset) throws IOException {
        boolean INIT = false;
        boolean CLASS_DATA_START = true;
        int TEXT_START = 2;
        final int[] state = new int[1];
        final int[] offset = new int[]{-1, -1};
        state[0] = 0;
        HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback(){
            int nextHRPos;
            int lastHRPos;

            @Override
            public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.HR) {
                    if (state[0] == 2) {
                        this.nextHRPos = pos;
                    }
                    this.lastHRPos = pos;
                }
            }

            @Override
            public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                String attrName;
                if (t == HTML.Tag.P && state[0] == 1) {
                    state[0] = 2;
                }
                if (t == HTML.Tag.A && state[0] == 2 && (attrName = (String)a.getAttribute(HTML.Attribute.NAME)) != null && attrName.length() > 0) {
                    offset[1] = this.nextHRPos != -1 ? this.nextHRPos : pos;
                    state[0] = 0;
                }
            }

            @Override
            public void handleComment(char[] data, int pos) {
                String comment = String.valueOf(data);
                if (comment != null) {
                    if (comment.indexOf("START OF CLASS DATA") > 0) {
                        state[0] = 1;
                    } else if (comment.indexOf("NESTED CLASS SUMMARY") > 0) {
                        offset[1] = this.lastHRPos != -1 ? this.lastHRPos : pos;
                    }
                }
            }

            @Override
            public void handleText(char[] data, int pos) {
                if (state[0] == 2 && offset[0] < 0) {
                    offset[0] = pos;
                }
            }
        };
        parser.parse(reader, callback, ignoreCharset);
        return offset;
    }

    private static int[] parseMember(Reader reader, final String name, HTMLEditorKit.Parser parser, boolean ignoreCharset, final boolean isGDK) throws IOException {
        boolean INIT = false;
        boolean A_OPEN = true;
        int A_CLOSE = 2;
        int PRE_CLOSE = 3;
        final int[] state = new int[1];
        final int[] offset = new int[]{-1, -1};
        state[0] = 0;
        HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback(){
            int hrPos;

            String methodName(String signature) {
                if (signature == null) {
                    return "<NULL>";
                }
                int idx = signature.indexOf("(");
                if (idx != -1) {
                    return signature.substring(0, idx);
                }
                return signature;
            }

            int countParameters(String signature) {
                int openSign = signature.indexOf("(");
                int closeSign = signature.indexOf(")");
                if (openSign == -1 || closeSign == -1 || closeSign <= openSign) {
                    return -1;
                }
                if (closeSign - openSign == 1) {
                    return 0;
                }
                String paramList = signature.substring(openSign + 1, closeSign);
                int num = 0;
                int idx = 0;
                while ((idx = paramList.indexOf(",", idx)) != -1) {
                    ++idx;
                    ++num;
                }
                return num + 1;
            }

            boolean checkSignatureLink(String signature, String attrName, boolean isGDK2) {
                if (signature == null && attrName == null) {
                    return false;
                }
                if (isGDK2 ? this.methodName(signature).equals(this.methodName(attrName)) && this.countParameters(signature) == this.countParameters(attrName) : signature.equals(attrName)) {
                    return true;
                }
                return false;
            }

            @Override
            public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.HR && state[0] != 0 && state[0] == 3) {
                    this.hrPos = pos;
                }
            }

            @Override
            public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.A) {
                    String attrName = (String)a.getAttribute(HTML.Attribute.NAME);
                    if (this.checkSignatureLink(name, attrName, isGDK)) {
                        state[0] = 1;
                    } else if (state[0] == 3 && attrName != null) {
                        state[0] = 0;
                        offset[1] = this.hrPos != -1 ? this.hrPos : pos;
                    }
                } else if (t == HTML.Tag.DD && state[0] == 3 && offset[0] < 0) {
                    offset[0] = pos;
                }
            }

            @Override
            public void handleEndTag(HTML.Tag t, int pos) {
                if (t == HTML.Tag.A && state[0] == 1) {
                    state[0] = 2;
                } else if (t == HTML.Tag.PRE && state[0] == 2) {
                    state[0] = 3;
                }
            }
        };
        parser.parse(reader, callback, ignoreCharset);
        return offset;
    }

    private static int[] parsePackage(Reader reader, HTMLEditorKit.Parser parser, boolean ignoreCharset) throws IOException {
        String name = "package_description";
        boolean INIT = false;
        boolean A_OPEN = true;
        final int[] state = new int[1];
        final int[] offset = new int[]{-1, -1};
        state[0] = 0;
        HTMLEditorKit.ParserCallback callback = new HTMLEditorKit.ParserCallback(){
            int hrPos;

            @Override
            public void handleSimpleTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.HR && state[0] != 0 && state[0] == 1) {
                    this.hrPos = pos;
                    offset[1] = pos;
                }
            }

            @Override
            public void handleStartTag(HTML.Tag t, MutableAttributeSet a, int pos) {
                if (t == HTML.Tag.A) {
                    String attrName = (String)a.getAttribute(HTML.Attribute.NAME);
                    if ("package_description".equals(attrName)) {
                        state[0] = 1;
                        offset[0] = pos;
                    } else if (state[0] == 1 && attrName != null) {
                        state[0] = 0;
                        offset[1] = this.hrPos != -1 ? this.hrPos : pos;
                    }
                }
            }
        };
        parser.parse(reader, callback, ignoreCharset);
        return offset;
    }

}

