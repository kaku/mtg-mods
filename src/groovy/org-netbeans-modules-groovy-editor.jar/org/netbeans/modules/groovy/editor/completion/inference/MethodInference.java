/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.groovy.editor.completion.inference;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;

public final class MethodInference {
    private MethodInference() {
    }

    @CheckForNull
    public static ClassNode findCallerType(@NonNull ASTNode expression) {
        ClassNode callerType;
        MethodCallExpression methodCall;
        Variable variable;
        if (expression instanceof MethodCallExpression && (callerType = MethodInference.findCallerType((ASTNode)(methodCall = (MethodCallExpression)expression).getObjectExpression())) != null) {
            return MethodInference.findReturnTypeFor(callerType, methodCall.getMethodAsString(), methodCall.getArguments());
        }
        if (expression instanceof VariableExpression && (variable = ((VariableExpression)expression).getAccessedVariable()) != null) {
            return variable.getType();
        }
        return null;
    }

    @CheckForNull
    private static ClassNode findReturnTypeFor(@NonNull ClassNode callerType, @NonNull String methodName, @NonNull Expression arguments) {
        MethodNode possibleMethod = callerType.tryFindPossibleMethod(methodName, arguments);
        if (possibleMethod != null) {
            return possibleMethod.getReturnType();
        }
        return null;
    }
}

