/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.completion.provider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.ElementUtilities;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.completion.AccessLevel;
import org.netbeans.modules.groovy.editor.completion.provider.GroovyElementsProvider;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public final class JavaElementHandler {
    private static final Logger LOG = Logger.getLogger(GroovyElementsProvider.class.getName());
    private final ParserResult info;

    private JavaElementHandler(ParserResult info) {
        this.info = info;
    }

    public static JavaElementHandler forCompilationInfo(ParserResult info) {
        return new JavaElementHandler(info);
    }

    public Map<MethodSignature, CompletionItem> getMethods(String className, String prefix, int anchor, String[] typeParameters, boolean emphasise, Set<AccessLevel> levels, boolean nameOnly) {
        JavaSource javaSource = this.createJavaSource();
        if (javaSource == null) {
            return Collections.emptyMap();
        }
        CountDownLatch cnt = new CountDownLatch(1);
        Map<MethodSignature, CompletionItem> result = Collections.synchronizedMap(new HashMap());
        try {
            javaSource.runUserActionTask((Task)new MethodCompletionHelper(cnt, javaSource, className, typeParameters, levels, prefix, anchor, result, emphasise, nameOnly), true);
        }
        catch (IOException ex) {
            LOG.log(Level.FINEST, "Problem in runUserActionTask :  {0}", ex.getMessage());
            return Collections.emptyMap();
        }
        try {
            cnt.await();
        }
        catch (InterruptedException ex) {
            LOG.log(Level.FINEST, "InterruptedException while waiting on latch :  {0}", ex.getMessage());
            return Collections.emptyMap();
        }
        return result;
    }

    public Map<FieldSignature, CompletionItem> getFields(String className, String prefix, int anchor, boolean emphasise) {
        JavaSource javaSource = this.createJavaSource();
        if (javaSource == null) {
            return Collections.emptyMap();
        }
        CountDownLatch cnt = new CountDownLatch(1);
        Map<FieldSignature, CompletionItem> result = Collections.synchronizedMap(new HashMap());
        try {
            javaSource.runUserActionTask((Task)new FieldCompletionHelper(cnt, javaSource, className, Collections.singleton(AccessLevel.PUBLIC), prefix, anchor, result, emphasise), true);
        }
        catch (IOException ex) {
            LOG.log(Level.FINEST, "Problem in runUserActionTask :  {0}", ex.getMessage());
            return Collections.emptyMap();
        }
        try {
            cnt.await();
        }
        catch (InterruptedException ex) {
            LOG.log(Level.FINEST, "InterruptedException while waiting on latch :  {0}", ex.getMessage());
            return Collections.emptyMap();
        }
        return result;
    }

    private JavaSource createJavaSource() {
        FileObject fileObject = this.info.getSnapshot().getSource().getFileObject();
        if (fileObject == null) {
            return null;
        }
        JavaSource javaSource = JavaSource.create((ClasspathInfo)ClasspathInfo.create((FileObject)fileObject), (FileObject[])new FileObject[0]);
        if (javaSource == null) {
            LOG.log(Level.FINEST, "Problem retrieving JavaSource from ClassPathInfo, exiting.");
            return null;
        }
        return javaSource;
    }

    private static class FieldCompletionHelper
    implements Task<CompilationController> {
        private final CountDownLatch cnt;
        private final JavaSource javaSource;
        private final String className;
        private final Set<AccessLevel> levels;
        private final String prefix;
        private final int anchor;
        private final boolean emphasise;
        private final Map<FieldSignature, CompletionItem> proposals;

        public FieldCompletionHelper(CountDownLatch cnt, JavaSource javaSource, String className, Set<AccessLevel> levels, String prefix, int anchor, Map<FieldSignature, CompletionItem> proposals, boolean emphasise) {
            this.cnt = cnt;
            this.javaSource = javaSource;
            this.className = className;
            this.levels = levels;
            this.prefix = prefix;
            this.anchor = anchor;
            this.proposals = proposals;
            this.emphasise = emphasise;
        }

        public void run(CompilationController info) throws Exception {
            Elements elements = info.getElements();
            if (elements != null) {
                ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                    public boolean accept(Element e, TypeMirror type) {
                        if (e.getKind() != ElementKind.FIELD) {
                            return false;
                        }
                        for (AccessLevel level : FieldCompletionHelper.this.levels) {
                            if (!level.getJavaAcceptor().accept(e, type)) continue;
                            return true;
                        }
                        return false;
                    }
                };
                TypeElement te = elements.getTypeElement(this.className);
                if (te != null) {
                    for (VariableElement element : ElementFilter.fieldsIn(te.getEnclosedElements())) {
                        if (!acceptor.accept((Element)element, te.asType())) continue;
                        String simpleName = element.getSimpleName().toString();
                        TypeMirror type = element.asType();
                        if (!simpleName.toUpperCase(Locale.ENGLISH).startsWith(this.prefix.toUpperCase(Locale.ENGLISH))) continue;
                        if (LOG.isLoggable(Level.FINEST)) {
                            LOG.log(Level.FINEST, simpleName + " " + type.toString());
                        }
                        this.proposals.put(this.getSignature(te, element), new CompletionItem.JavaFieldItem(this.className, simpleName, type, element.getModifiers(), this.anchor, this.emphasise));
                    }
                }
            }
            this.cnt.countDown();
        }

        private FieldSignature getSignature(TypeElement classElement, VariableElement element) {
            String name = element.getSimpleName().toString();
            return new FieldSignature(name);
        }

    }

    private static class MethodCompletionHelper
    implements Task<CompilationController> {
        private final CountDownLatch cnt;
        private final JavaSource javaSource;
        private final String className;
        private final String[] typeParameters;
        private final Set<AccessLevel> levels;
        private final String prefix;
        private final int anchor;
        private final boolean emphasise;
        private final Map<MethodSignature, CompletionItem> proposals;
        private final boolean nameOnly;

        public MethodCompletionHelper(CountDownLatch cnt, JavaSource javaSource, String className, String[] typeParameters, Set<AccessLevel> levels, String prefix, int anchor, Map<MethodSignature, CompletionItem> proposals, boolean emphasise, boolean nameOnly) {
            this.cnt = cnt;
            this.javaSource = javaSource;
            this.className = className;
            this.typeParameters = typeParameters;
            this.levels = levels;
            this.prefix = prefix;
            this.anchor = anchor;
            this.proposals = proposals;
            this.emphasise = emphasise;
            this.nameOnly = nameOnly;
        }

        public void run(CompilationController info) throws Exception {
            Elements elements = info.getElements();
            ElementUtilities.ElementAcceptor acceptor = new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror type) {
                    if (e.getKind() != ElementKind.METHOD) {
                        return false;
                    }
                    for (AccessLevel level : MethodCompletionHelper.this.levels) {
                        if (!level.getJavaAcceptor().accept(e, type)) continue;
                        return true;
                    }
                    return false;
                }
            };
            TypeElement te = elements.getTypeElement(this.className);
            if (te != null) {
                for (ExecutableElement element : ElementFilter.methodsIn(te.getEnclosedElements())) {
                    if (!acceptor.accept((Element)element, te.asType())) continue;
                    String simpleName = element.getSimpleName().toString();
                    List<String> params = this.getParameterListForMethod(element);
                    TypeMirror returnType = element.getReturnType();
                    if (!simpleName.toUpperCase(Locale.ENGLISH).startsWith(this.prefix.toUpperCase(Locale.ENGLISH)) || simpleName.contains("$")) continue;
                    this.proposals.put(this.getSignature(te, element, this.typeParameters, info.getTypes()), CompletionItem.forJavaMethod(this.className, simpleName, params, returnType, element.getModifiers(), this.anchor, this.emphasise, this.nameOnly));
                }
            }
            this.cnt.countDown();
        }

        private List<String> getParameterListForMethod(ExecutableElement exe) {
            ArrayList<String> parameters = new ArrayList<String>();
            if (exe != null) {
                try {
                    List<? extends VariableElement> params = exe.getParameters();
                    for (VariableElement variableElement : params) {
                        TypeMirror tm = variableElement.asType();
                        if (tm.getKind() == TypeKind.DECLARED || tm.getKind() == TypeKind.ARRAY) {
                            parameters.add(GroovyUtils.stripPackage(tm.toString()));
                            continue;
                        }
                        parameters.add(tm.toString());
                    }
                }
                catch (NullPointerException e) {
                    // empty catch block
                }
            }
            return parameters;
        }

        private MethodSignature getSignature(TypeElement classElement, ExecutableElement element, String[] typeParameters, Types types) {
            String name = element.getSimpleName().toString();
            String[] parameters = new String[element.getParameters().size()];
            for (int i = 0; i < parameters.length; ++i) {
                VariableElement var = element.getParameters().get(i);
                TypeMirror type = var.asType();
                String typeString = null;
                if (type.getKind() == TypeKind.TYPEVAR) {
                    List<? extends TypeParameterElement> declaredTypeParameters = element.getTypeParameters();
                    if (declaredTypeParameters.isEmpty()) {
                        declaredTypeParameters = classElement.getTypeParameters();
                    }
                    int j = -1;
                    for (TypeParameterElement typeParam : declaredTypeParameters) {
                        ++j;
                        if (!typeParam.getSimpleName().toString().equals(type.toString())) continue;
                        break;
                    }
                    typeString = types.erasure(type).toString();
                } else {
                    typeString = type.toString();
                }
                int index = typeString.indexOf(60);
                if (index >= 0) {
                    typeString = typeString.substring(0, index);
                }
                parameters[i] = typeString;
            }
            return new MethodSignature(name, parameters);
        }

    }

}

