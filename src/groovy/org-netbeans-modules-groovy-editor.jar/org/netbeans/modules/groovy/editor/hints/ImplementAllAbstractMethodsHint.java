/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.EditList
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintFix
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.hints;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.EditList;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;
import org.netbeans.modules.groovy.editor.hints.Bundle;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovyErrorRule;
import org.netbeans.modules.groovy.editor.hints.utils.HintUtils;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public final class ImplementAllAbstractMethodsHint
extends GroovyErrorRule {
    @Override
    public Set<CompilerErrorID> getCodes() {
        return EnumSet.of(CompilerErrorID.CLASS_DOES_NOT_IMPLEMENT_ALL_METHODS);
    }

    @Override
    public void run(RuleContext context, GroovyError error, List<Hint> result) {
        FileObject fo = context.parserResult.getSnapshot().getSource().getFileObject();
        AddMethodStubsFix fix = this.findExistingFix(result);
        if (fix == null) {
            fix = new AddMethodStubsFix(error);
            OffsetRange range = HintUtils.getLineOffset(context, error);
            if (range != null) {
                result.add(new Hint((Rule)this, fix.getDescription(), fo, range, Collections.singletonList(fix), 100));
            }
        }
        fix.addMethodSignature(this.getMethodName(error));
    }

    private AddMethodStubsFix findExistingFix(List<Hint> result) {
        for (Hint existingHint : result) {
            List fixes;
            HintFix fix;
            if (!(existingHint.getRule() instanceof ImplementAllAbstractMethodsHint) || (fixes = existingHint.getFixes()) == null || fixes.isEmpty() || !((fix = (HintFix)fixes.get(0)) instanceof AddMethodStubsFix)) continue;
            return (AddMethodStubsFix)fix;
        }
        return null;
    }

    @NonNull
    public String getMethodName(@NonNull GroovyError error) {
        String errorMessage = error.getDescription();
        String classNameSuffix = "' must be declared abstract or the method '";
        String methodSuffix = "' must be implemented.";
        int startOffset = errorMessage.indexOf(classNameSuffix) + classNameSuffix.length();
        String methodName = errorMessage.substring(startOffset);
        int suffixOffset = methodName.indexOf(methodSuffix);
        String suffix = methodName.substring(suffixOffset);
        return methodName.replace(suffix, "");
    }

    public boolean appliesTo(RuleContext context) {
        return true;
    }

    public String getDisplayName() {
        return Bundle.ImplementInterfaceHintDescription();
    }

    public boolean showInTasklist() {
        return false;
    }

    public HintSeverity getDefaultSeverity() {
        return HintSeverity.ERROR;
    }

    private static class AddMethodStubsFix
    implements HintFix {
        private final GroovyError error;
        private final List<String> missingMethods;

        public AddMethodStubsFix(GroovyError error) {
            this.error = error;
            this.missingMethods = new ArrayList<String>();
        }

        public void addMethodSignature(String methodSignature) {
            this.missingMethods.add(methodSignature);
        }

        public void implement() throws Exception {
            BaseDocument baseDoc = LexUtilities.getDocument(this.error.getFile(), true);
            if (baseDoc == null) {
                return;
            }
            EditList edits = new EditList(baseDoc);
            for (int i = 0; i < this.missingMethods.size(); ++i) {
                StringBuilder sb = new StringBuilder();
                if (i == 0) {
                    sb.append("\n");
                }
                sb.append("public ");
                sb.append(this.missingMethods.get(i));
                sb.append(" {\n");
                for (int space = 0; space < IndentUtils.indentLevelSize((Document)baseDoc); ++space) {
                    sb.append(" ");
                }
                sb.append("throw new UnsupportedOperationException(\"Not supported yet.\");\n");
                sb.append("}");
                if (i == this.missingMethods.size() - 1) {
                    sb.append("\n");
                } else {
                    sb.append("\n\n");
                }
                edits.replace(this.getInsertPosition(baseDoc), 0, sb.toString(), true, 0);
            }
            edits.apply();
        }

        private int getInsertPosition(BaseDocument doc) {
            TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 1);
            while (ts.moveNext()) {
                Token t = ts.token();
                if (t.id() != GroovyTokenId.LITERAL_class || !this.isCorrectClassDefinition(ts)) continue;
                return this.findClosingCurlyOffset(ts);
            }
            return 0;
        }

        private boolean isCorrectClassDefinition(TokenSequence<GroovyTokenId> ts) {
            while (ts.moveNext()) {
                Token token = ts.token();
                TokenId id = token.id();
                if (id != GroovyTokenId.IDENTIFIER) continue;
                String identifierName = token.text().toString();
                if (identifierName.equals(this.getClassName())) {
                    return true;
                }
                return false;
            }
            return false;
        }

        public int findClosingCurlyOffset(TokenSequence<GroovyTokenId> ts) {
            int balance = 0;
            while (ts.moveNext()) {
                Token token = ts.token();
                TokenId id = token.id();
                if (id == GroovyTokenId.LBRACE) {
                    ++balance;
                    continue;
                }
                if (id != GroovyTokenId.RBRACE || --balance != 0) continue;
                return ts.offset();
            }
            return 0;
        }

        @NonNull
        private String getClassName() {
            String errorMessage = this.error.getDescription();
            String classNamePrefix = "Can't have an abstract method in a non-abstract class. The class '";
            String classNameSuffix = "' must be declared abstract or the method '";
            int endOffset = errorMessage.indexOf(classNameSuffix);
            String fqName = errorMessage.substring(0, endOffset);
            fqName = fqName.replace(classNamePrefix, "");
            return GroovyUtils.stripPackage(fqName);
        }

        public String getDescription() {
            return Bundle.AddMissingMethodsStub();
        }

        public boolean isSafe() {
            return true;
        }

        public boolean isInteractive() {
            return false;
        }
    }

}

