/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.AnnotationNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.Parameter
 */
package org.netbeans.modules.groovy.editor.completion.provider;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.groovy.ast.AnnotationNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.Parameter;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.java.Utilities;

public final class TransformationHandler {
    private static final String SINGLETON_ANNOTATION = "Singleton";
    private static final String DELEGATE_ANNOTATION = "Delegate";
    private static final String SINGLETON_FIELD_NAME = "instance";
    private static final String SINGLETON_METHOD_NAME = "getInstance";

    public static Map<FieldSignature, CompletionItem> getFields(GroovyIndex index, ClassNode typeNode, String prefix, int anchorOffset) {
        HashMap<FieldSignature, CompletionItem> result = new HashMap<FieldSignature, CompletionItem>();
        for (AnnotationNode annotation : typeNode.getAnnotations()) {
            String annotationName = annotation.getClassNode().getNameWithoutPackage();
            if (!"Singleton".equals(annotationName)) continue;
            FieldSignature signature = new FieldSignature("instance");
            CompletionItem.FieldItem proposal = new CompletionItem.FieldItem(typeNode.getNameWithoutPackage(), "instance", 8, anchorOffset);
            if (!signature.getName().startsWith(prefix)) continue;
            result.put(signature, proposal);
        }
        for (FieldNode field : typeNode.getFields()) {
            for (AnnotationNode fieldAnnotation : field.getAnnotations()) {
                String fieldAnnotationName = fieldAnnotation.getClassNode().getNameWithoutPackage();
                if (!"Delegate".equals(fieldAnnotationName)) continue;
                for (FieldNode annotatedField : field.getType().getFields()) {
                    FieldSignature signature = new FieldSignature(annotatedField.getName());
                    CompletionItem.FieldItem fieldProposal = new CompletionItem.FieldItem(annotatedField.getType().getNameWithoutPackage(), annotatedField.getName(), annotatedField.getModifiers(), anchorOffset);
                    if (!signature.getName().startsWith(prefix)) continue;
                    result.put(signature, fieldProposal);
                }
            }
        }
        return result;
    }

    public static Map<MethodSignature, CompletionItem> getMethods(GroovyIndex index, ClassNode typeNode, String prefix, int anchorOffset) {
        HashMap<MethodSignature, CompletionItem> result = new HashMap<MethodSignature, CompletionItem>();
        boolean prefixed = !"".equals(prefix);
        for (AnnotationNode annotation : typeNode.getAnnotations()) {
            String annotationName = annotation.getClassNode().getNameWithoutPackage();
            if (!"Singleton".equals(annotationName)) continue;
            MethodSignature signature = new MethodSignature("getInstance", new String[0]);
            CompletionItem proposal = CompletionItem.forJavaMethod(typeNode.getNameWithoutPackage(), "getInstance", Collections.emptyList(), typeNode.getNameWithoutPackage(), Utilities.reflectionModifiersToModel(8), anchorOffset, true, false);
            if (!signature.getName().startsWith(prefix)) continue;
            result.put(signature, proposal);
        }
        for (FieldNode field : typeNode.getFields()) {
            for (AnnotationNode fieldAnnotation : field.getAnnotations()) {
                String fieldAnnotationName = fieldAnnotation.getClassNode().getNameWithoutPackage();
                if (!"Delegate".equals(fieldAnnotationName)) continue;
                for (MethodNode method : field.getType().getMethods()) {
                    MethodSignature signature = TransformationHandler.getSignature(method);
                    CompletionItem proposal = TransformationHandler.createMethodProposal(method, prefixed, anchorOffset);
                    if (!signature.getName().startsWith(prefix)) continue;
                    result.put(signature, proposal);
                }
            }
        }
        return result;
    }

    private static MethodSignature getSignature(MethodNode method) {
        String[] parameters = new String[method.getParameters().length];
        for (int i = 0; i < parameters.length; ++i) {
            parameters[i] = Utilities.translateClassLoaderTypeName(method.getParameters()[i].getName());
        }
        return new MethodSignature(method.getName(), parameters);
    }

    private static CompletionItem createMethodProposal(MethodNode method, boolean prefixed, int anchorOffset) {
        String methodName = method.getName();
        String[] methodParams = TransformationHandler.getMethodParams(method);
        String returnType = method.getReturnType().getName();
        return CompletionItem.forDynamicMethod(anchorOffset, methodName, methodParams, returnType, prefixed);
    }

    private static String[] getMethodParams(MethodNode method) {
        String[] parameters = new String[method.getParameters().length];
        for (int i = 0; i < parameters.length; ++i) {
            parameters[i] = method.getParameters()[i].getName();
        }
        return parameters;
    }
}

