/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.groovy.editor.imports;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.modules.groovy.editor.spi.completion.DefaultImportsProvider;
import org.openide.util.Lookup;

public final class ImportUtils {
    private ImportUtils() {
    }

    public static boolean isDefaultlyImported(String fqn) {
        for (String defaultImport : ImportUtils.getDefaultImportClasses()) {
            if (!defaultImport.equals(fqn)) continue;
            return true;
        }
        String packageName = ImportUtils.getPackageName(fqn);
        for (String defaultImport2 : ImportUtils.getDefaultImportPackages()) {
            if (!defaultImport2.equals(packageName)) continue;
            return true;
        }
        return false;
    }

    private static String getPackageName(String fqn) {
        if (fqn.contains(".")) {
            fqn = fqn.substring(0, fqn.lastIndexOf("."));
        }
        return fqn;
    }

    public static Set<String> getDefaultImportPackages() {
        HashSet<String> defaultPackages = new HashSet<String>();
        defaultPackages.add("java.io");
        defaultPackages.add("java.lang");
        defaultPackages.add("java.net");
        defaultPackages.add("java.util");
        defaultPackages.add("groovy.util");
        defaultPackages.add("groovy.lang");
        for (DefaultImportsProvider importsProvider : Lookup.getDefault().lookupAll(DefaultImportsProvider.class)) {
            defaultPackages.addAll(importsProvider.getDefaultImportPackages());
        }
        return defaultPackages;
    }

    public static Set<String> getDefaultImportClasses() {
        HashSet<String> defaultClasses = new HashSet<String>();
        defaultClasses.add("java.math.BigDecimal");
        defaultClasses.add("java.math.BigInteger");
        for (DefaultImportsProvider importsProvider : Lookup.getDefault().lookupAll(DefaultImportsProvider.class)) {
            defaultClasses.addAll(importsProvider.getDefaultImportClasses());
        }
        return defaultClasses;
    }
}

