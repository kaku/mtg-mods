/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexDocument
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.StructureAnalyzer;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTClass;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTField;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTMethod;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.compiler.ClassNodeCache;
import org.netbeans.modules.groovy.editor.java.Utilities;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class GroovyIndexer
extends EmbeddingIndexer {
    static final String FQN_NAME = "fqn";
    static final String CLASS_NAME = "class";
    static final String CASE_INSENSITIVE_CLASS_NAME = "class-ig";
    static final String IN = "in";
    static final String CLASS_ATTRS = "attrs";
    static final String METHOD_NAME = "method";
    static final String CONSTRUCTOR = "ctor";
    static final String FIELD_NAME = "field";
    private static FileObject preindexedDb;
    private static long indexerRunTime;
    private static long indexerFirstRun;
    private static long filesIndexed;
    private static final Logger LOG;

    protected void index(Indexable indexable, Parser.Result parserResult, Context context) {
        ModuleNode root;
        IndexingSupport support;
        GroovyParserResult r;
        long indexerThisStartTime = System.currentTimeMillis();
        if (indexerFirstRun == 0) {
            indexerFirstRun = indexerThisStartTime;
        }
        if ((root = ASTUtils.getRoot(r = ASTUtils.getParseResult(parserResult))) == null) {
            return;
        }
        try {
            support = IndexingSupport.getInstance((Context)context);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return;
        }
        TreeAnalyzer analyzer = new TreeAnalyzer(r, support, indexable);
        analyzer.analyze();
        for (IndexDocument doc : analyzer.getDocuments()) {
            support.addDocument(doc);
        }
        long indexerThisStopTime = System.currentTimeMillis();
        long indexerThisRunTime = indexerThisStopTime - indexerThisStartTime;
        LOG.log(Level.FINEST, "Indexed File                : {0}", (Object)r.getSnapshot().getSource().getFileObject());
        LOG.log(Level.FINEST, "Indexing time (ms)          : {0}", indexerThisRunTime);
        LOG.log(Level.FINEST, "Number of files indexed     : {0}", ++filesIndexed);
        LOG.log(Level.FINEST, "Time spend indexing (ms)    : {0}", indexerRunTime += indexerThisRunTime);
        LOG.log(Level.FINEST, "Avg indexing time/file (ms) : {0}", indexerRunTime / filesIndexed);
        LOG.log(Level.FINEST, "Time betw. 1st and Last idx : {0}", indexerThisStopTime - indexerFirstRun);
        LOG.log(Level.FINEST, "---------------------------------------------------------------------------------");
    }

    public FileObject getPreindexedDb() {
        return preindexedDb;
    }

    private static int getFieldModifiersFlag(Set<Modifier> modifiers) {
        int flags;
        int n = flags = modifiers.contains((Object)Modifier.STATIC) ? 8 : 0;
        if (modifiers.contains((Object)Modifier.PUBLIC)) {
            flags |= 1;
        } else if (modifiers.contains((Object)Modifier.PROTECTED)) {
            flags |= 4;
        }
        return flags;
    }

    private static int getMethodModifiersFlag(Set<Modifier> modifiers) {
        int flags;
        int n = flags = modifiers.contains((Object)Modifier.STATIC) ? 8 : 0;
        if (modifiers.contains((Object)Modifier.PRIVATE)) {
            flags |= 2;
        } else if (modifiers.contains((Object)Modifier.PROTECTED)) {
            flags |= 4;
        }
        return flags;
    }

    static {
        indexerRunTime = 0;
        indexerFirstRun = 0;
        filesIndexed = 0;
        LOG = Logger.getLogger(GroovyIndexer.class.getName());
    }

    private static class TreeAnalyzer {
        private final FileObject file;
        private final IndexingSupport support;
        private final Indexable indexable;
        private final GroovyParserResult result;
        private final List<IndexDocument> documents = new ArrayList<IndexDocument>();
        private String url;
        private BaseDocument doc;

        private TreeAnalyzer(GroovyParserResult result, IndexingSupport support, Indexable indexable) {
            this.result = result;
            this.file = result.getSnapshot().getSource().getFileObject();
            this.support = support;
            this.indexable = indexable;
        }

        List<IndexDocument> getDocuments() {
            return this.documents;
        }

        public void analyze() {
            this.doc = LexUtilities.getDocument(this.result, true);
            try {
                this.url = this.file.getURL().toExternalForm();
                this.url = GroovyIndex.getPreindexUrl(this.url);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
            StructureAnalyzer.AnalysisResult ar = this.result.getStructure();
            List<ASTElement> children = ar.getElements();
            if (children == null || children.size() == 0) {
                return;
            }
            for (ASTElement child : children) {
                switch (child.getKind()) {
                    case CLASS: {
                        this.analyzeClass((ASTClass)child);
                    }
                }
            }
        }

        private void analyzeClass(ASTClass element) {
            IndexDocument document = this.support.createDocument(this.indexable);
            this.documents.add(document);
            this.indexClass(element, document);
            for (ASTElement child : element.getChildren()) {
                switch (child.getKind()) {
                    case METHOD: {
                        this.indexMethod((ASTMethod)child, document);
                        break;
                    }
                    case CONSTRUCTOR: {
                        this.indexConstructor((ASTMethod)child, document);
                        break;
                    }
                    case FIELD: {
                        this.indexField((ASTField)child, document);
                    }
                }
            }
        }

        private void indexClass(ASTClass element, IndexDocument document) {
            String name = element.getName();
            document.addPair("fqn", element.getFqn(), true, true);
            document.addPair("class", name, true, true);
            document.addPair("class-ig", name.toLowerCase(), true, true);
        }

        private void indexField(ASTField child, IndexDocument document) {
            StringBuilder sb = new StringBuilder(child.getName());
            FieldNode node = (FieldNode)child.getNode();
            sb.append(';').append(Utilities.translateClassLoaderTypeName(node.getType().getName()));
            int flags = GroovyIndexer.getFieldModifiersFlag(child.getModifiers());
            if (flags != 0 || child.isProperty()) {
                sb.append(';');
                sb.append(IndexedElement.flagToFirstChar(flags));
                sb.append(IndexedElement.flagToSecondChar(flags));
            }
            if (child.isProperty()) {
                sb.append(';');
                sb.append(child.isProperty());
            }
            document.addPair("field", sb.toString(), true, true);
        }

        private void indexConstructor(ASTMethod constructor, IndexDocument document) {
            Set<Modifier> modifiers;
            int flags;
            StringBuilder sb = new StringBuilder();
            sb.append(constructor.getName());
            sb.append(';');
            List<String> params = constructor.getParameterTypes();
            if (!params.isEmpty()) {
                for (String paramName : params) {
                    sb.append(paramName);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            if ((flags = GroovyIndexer.getMethodModifiersFlag(modifiers = constructor.getModifiers())) != 0) {
                sb.append(';');
                sb.append(IndexedElement.flagToFirstChar(flags));
                sb.append(IndexedElement.flagToSecondChar(flags));
            }
            document.addPair("ctor", sb.toString(), true, true);
        }

        private void indexMethod(ASTMethod child, IndexDocument document) {
            MethodNode childNode = (MethodNode)child.getNode();
            StringBuilder sb = new StringBuilder(ASTUtils.getDefSignature(childNode));
            sb.append(';').append(Utilities.translateClassLoaderTypeName(childNode.getReturnType().getName()));
            Set<Modifier> modifiers = child.getModifiers();
            int flags = GroovyIndexer.getMethodModifiersFlag(modifiers);
            if (flags != 0) {
                sb.append(';');
                sb.append(IndexedElement.flagToFirstChar(flags));
                sb.append(IndexedElement.flagToSecondChar(flags));
            }
            document.addPair("method", sb.toString(), true, true);
        }
    }

    public static final class Factory
    extends EmbeddingIndexerFactory {
        public static final String NAME = "groovy";
        public static final int VERSION = 8;

        public EmbeddingIndexer createIndexer(Indexable indexable, Snapshot snapshot) {
            if (this.isIndexable(indexable, snapshot)) {
                return new GroovyIndexer();
            }
            return null;
        }

        public int getIndexVersion() {
            return 8;
        }

        public String getIndexerName() {
            return "groovy";
        }

        private boolean isIndexable(Indexable indexable, Snapshot snapshot) {
            String extension = snapshot.getSource().getFileObject().getExt();
            if (extension.equals("groovy")) {
                return true;
            }
            return false;
        }

        public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
            try {
                IndexingSupport support = IndexingSupport.getInstance((Context)context);
                for (Indexable indexable : deleted) {
                    support.removeDocuments(indexable);
                }
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        public void rootsRemoved(Iterable<? extends URL> removedRoots) {
        }

        public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
            try {
                IndexingSupport is = IndexingSupport.getInstance((Context)context);
                for (Indexable i : dirty) {
                    is.markDirtyDocuments(i);
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
        }

        public boolean scanStarted(Context context) {
            ClassNodeCache.createThreadLocalInstance();
            return super.scanStarted(context);
        }

        public void scanFinished(Context context) {
            ClassNodeCache.clearThreadLocalInstance();
            super.scanFinished(context);
        }
    }

}

