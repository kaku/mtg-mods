/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.GroovySystem
 *  groovy.lang.MetaClass
 *  groovy.lang.MetaMethod
 *  groovy.lang.MetaProperty
 *  org.codehaus.groovy.reflection.CachedClass
 */
package org.netbeans.modules.groovy.editor.completion.provider;

import groovy.lang.GroovySystem;
import groovy.lang.MetaClass;
import groovy.lang.MetaMethod;
import groovy.lang.MetaProperty;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.groovy.reflection.CachedClass;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.java.Utilities;
import org.netbeans.modules.groovy.editor.spi.completion.CompletionProvider;

public final class MetaElementsProvider
implements CompletionProvider {
    @Override
    public Map<MethodSignature, CompletionItem> getMethods(CompletionContext context) {
        MetaClass metaClz;
        HashMap<MethodSignature, CompletionItem> result = new HashMap<MethodSignature, CompletionItem>();
        Class clz = this.loadClass(context.getTypeName());
        if (clz != null && (metaClz = GroovySystem.getMetaClassRegistry().getMetaClass(clz)) != null) {
            for (MetaMethod method : metaClz.getMetaMethods()) {
                this.populateProposal(clz, method, context.getPrefix(), context.getAnchor(), result, context.isNameOnly());
            }
        }
        return result;
    }

    @Override
    public Map<MethodSignature, CompletionItem> getStaticMethods(CompletionContext context) {
        return Collections.emptyMap();
    }

    @Override
    public Map<FieldSignature, CompletionItem> getFields(CompletionContext context) {
        MetaClass metaClass;
        HashMap<FieldSignature, CompletionItem> result = new HashMap<FieldSignature, CompletionItem>();
        Class clazz = this.loadClass(context.getTypeName());
        if (clazz != null && (metaClass = GroovySystem.getMetaClassRegistry().getMetaClass(clazz)) != null) {
            for (Object field : metaClass.getProperties()) {
                MetaProperty prop = (MetaProperty)field;
                if (!prop.getName().startsWith(context.getPrefix())) continue;
                result.put(new FieldSignature(prop.getName()), new CompletionItem.FieldItem(prop.getType().getSimpleName(), prop.getName(), prop.getModifiers(), context.getAnchor()));
            }
        }
        return result;
    }

    @Override
    public Map<FieldSignature, CompletionItem> getStaticFields(CompletionContext context) {
        return Collections.emptyMap();
    }

    private Class loadClass(String className) {
        try {
            return Class.forName(className);
        }
        catch (ClassNotFoundException e) {
            return null;
        }
        catch (NoClassDefFoundError err) {
            return null;
        }
    }

    private void populateProposal(Class clz, MetaMethod method, String prefix, int anchor, Map<MethodSignature, CompletionItem> methodList, boolean nameOnly) {
        if (method.getName().startsWith(prefix)) {
            this.addOrReplaceItem(methodList, new CompletionItem.MetaMethodItem(clz, method, anchor, true, nameOnly));
        }
    }

    private void addOrReplaceItem(Map<MethodSignature, CompletionItem> methodItemList, CompletionItem.MetaMethodItem itemToStore) {
        MetaMethod methodToStore = itemToStore.getMethod();
        for (CompletionItem methodItem : methodItemList.values()) {
            MetaMethod currentMethod;
            if (!(methodItem instanceof CompletionItem.MetaMethodItem) || !this.isSameMethod(currentMethod = ((CompletionItem.MetaMethodItem)methodItem).getMethod(), methodToStore)) continue;
            if (this.isBetterDistance(currentMethod, methodToStore)) {
                methodItemList.remove(this.getSignature(currentMethod));
                methodItemList.put(this.getSignature(methodToStore), itemToStore);
            }
            return;
        }
        methodItemList.put(this.getSignature(methodToStore), itemToStore);
    }

    private boolean isSameMethod(MetaMethod currentMethod, MetaMethod methodToStore) {
        if (!currentMethod.getName().equals(methodToStore.getName())) {
            return false;
        }
        int mask = 15;
        if ((currentMethod.getModifiers() & mask) != (methodToStore.getModifiers() & mask)) {
            return false;
        }
        if (!this.isSameParams(currentMethod.getParameterTypes(), methodToStore.getParameterTypes())) {
            return false;
        }
        return true;
    }

    private boolean isSameParams(CachedClass[] parameters1, CachedClass[] parameters2) {
        if (parameters1.length == parameters2.length) {
            int size = parameters1.length;
            for (int i = 0; i < size; ++i) {
                if (parameters1[i] == parameters2[i]) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    private MethodSignature getSignature(MetaMethod method) {
        String[] parameters = new String[method.getParameterTypes().length];
        for (int i = 0; i < parameters.length; ++i) {
            parameters[i] = Utilities.translateClassLoaderTypeName(method.getParameterTypes()[i].getName());
        }
        return new MethodSignature(method.getName(), parameters);
    }

    private boolean isBetterDistance(MetaMethod currentMethod, MetaMethod methodToStore) {
        int toStoreSuperClassDistance;
        String currentClassName = currentMethod.getDeclaringClass().getName();
        String toStoreClassName = methodToStore.getDeclaringClass().getName();
        if ("java.lang.Object".equals(currentClassName)) {
            return true;
        }
        if ("java.lang.Object".equals(toStoreClassName)) {
            return false;
        }
        int currentSuperClassDistance = currentMethod.getDeclaringClass().getSuperClassDistance();
        if (currentSuperClassDistance < (toStoreSuperClassDistance = methodToStore.getDeclaringClass().getSuperClassDistance())) {
            return true;
        }
        if (currentSuperClassDistance == toStoreSuperClassDistance) {
            if ("java.util.Collection".equals(currentClassName) && "java.util.Set".equals(toStoreClassName)) {
                return true;
            }
            if ("java.util.Collection".equals(toStoreClassName) && "java.util.Set".equals(currentClassName)) {
                return false;
            }
            if ("java.util.Collection".equals(currentClassName) && "java.util.List".equals(toStoreClassName)) {
                return true;
            }
            if ("java.util.Collection".equals(toStoreClassName) && "java.util.List".equals(currentClassName)) {
                return false;
            }
        }
        return false;
    }
}

