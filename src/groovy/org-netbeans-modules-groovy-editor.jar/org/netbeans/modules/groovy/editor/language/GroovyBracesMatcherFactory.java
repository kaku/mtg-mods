/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcher
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory
 *  org.netbeans.spi.editor.bracesmatching.MatcherContext
 */
package org.netbeans.modules.groovy.editor.language;

import org.netbeans.modules.groovy.editor.language.GroovyBracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;

public final class GroovyBracesMatcherFactory
implements BracesMatcherFactory {
    public BracesMatcher createMatcher(MatcherContext context) {
        return new GroovyBracesMatcher(context);
    }
}

