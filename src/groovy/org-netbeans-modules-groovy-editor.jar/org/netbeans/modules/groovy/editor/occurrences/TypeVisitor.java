/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassCodeVisitorSupport
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.VariableScope
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ClosureListExpression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 */
package org.netbeans.modules.groovy.editor.occurrences;

import java.util.List;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassCodeVisitorSupport;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.VariableScope;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ClosureListExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.FindTypeUtils;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;

public class TypeVisitor
extends ClassCodeVisitorSupport {
    protected final SourceUnit sourceUnit;
    protected final AstPath path;
    protected final ASTNode leaf;
    protected final BaseDocument doc;
    protected final int cursorOffset;
    private final boolean visitOtherClasses;

    public TypeVisitor(SourceUnit sourceUnit, AstPath path, BaseDocument doc, int cursorOffset, boolean visitOtherClasses) {
        this.sourceUnit = sourceUnit;
        this.path = path;
        this.leaf = path.leaf();
        this.doc = doc;
        this.cursorOffset = cursorOffset;
        this.visitOtherClasses = visitOtherClasses;
    }

    protected SourceUnit getSourceUnit() {
        return this.sourceUnit;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void collect() {
        this.doc.readLock();
        try {
            TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(this.doc, this.cursorOffset);
            if (ts == null) {
                return;
            }
            Token token = ts.token();
            if (token == null) {
                return;
            }
            ts.movePrevious();
            if (!this.isValidToken(token, ts.token())) {
                return;
            }
        }
        finally {
            this.doc.readUnlock();
        }
        if (this.leaf instanceof Variable) {
            Variable variable = (Variable)this.leaf;
            for (ASTNode scope : this.path) {
                VariableScope variableScope;
                VariableScope variableScope2;
                if (scope instanceof ClosureExpression) {
                    variableScope = ((ClosureExpression)scope).getVariableScope();
                    if (variableScope == null || variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                    this.visitClosureExpression((ClosureExpression)scope);
                    return;
                }
                if (scope instanceof MethodNode) {
                    MethodNode method = (MethodNode)scope;
                    variableScope2 = method.getVariableScope();
                    if (variableScope2 != null && variableScope2.getDeclaredVariable(variable.getName()) != null) {
                        this.visitParameters(method.getParameters(), variable);
                        boolean isParamType = false;
                        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
                            isParamType = true;
                        }
                        if (!isParamType) {
                            super.visitMethod(method);
                            return;
                        }
                    }
                    super.visitMethod(method);
                    continue;
                }
                if (scope instanceof ConstructorNode) {
                    ConstructorNode constructor = (ConstructorNode)scope;
                    variableScope2 = constructor.getVariableScope();
                    if (variableScope2 != null && variableScope2.getDeclaredVariable(variable.getName()) != null) {
                        this.visitParameters(constructor.getParameters(), variable);
                    }
                    super.visitConstructor(constructor);
                    continue;
                }
                if (scope instanceof ForStatement) {
                    variableScope = ((ForStatement)scope).getVariableScope();
                    if (variableScope == null || variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                    this.visitForLoop((ForStatement)scope);
                    return;
                }
                if (scope instanceof BlockStatement) {
                    variableScope = ((BlockStatement)scope).getVariableScope();
                    if (variableScope == null || variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                    this.visitBlockStatement((BlockStatement)scope);
                    return;
                }
                if (!(scope instanceof ClosureListExpression) || (variableScope = ((ClosureListExpression)scope).getVariableScope()) == null || variableScope.getDeclaredVariable(variable.getName()) == null) continue;
                this.visitClosureListExpression((ClosureListExpression)scope);
                return;
            }
        }
        if (this.visitOtherClasses) {
            ModuleNode moduleNode = (ModuleNode)this.path.root();
            for (Object object : moduleNode.getClasses()) {
                this.visitClass((ClassNode)object);
            }
        }
    }

    protected boolean isValidToken(Token<GroovyTokenId> currentToken, Token<GroovyTokenId> previousToken) {
        return true;
    }

    protected void visitParameters(Parameter[] parameters, Variable variable) {
    }
}

