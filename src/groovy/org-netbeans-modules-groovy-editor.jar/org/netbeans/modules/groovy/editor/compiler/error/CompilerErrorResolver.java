/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.groovy.editor.compiler.error;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.openide.util.Parameters;

public final class CompilerErrorResolver {
    private static final String UNABLE_TO_RESOLVE_CLASS = "unable to resolve class ";
    private static final String CLASS_DOES_NOT_IMPLEMENT_ALL_METHODS = "Can't have an abstract method in a non-abstract class.";

    private CompilerErrorResolver() {
    }

    public static CompilerErrorID getId(@NonNull String errorMessage) {
        Parameters.notNull((CharSequence)"errorMessage", (Object)errorMessage);
        if (errorMessage.startsWith("unable to resolve class ")) {
            return CompilerErrorID.CLASS_NOT_FOUND;
        }
        if (errorMessage.startsWith("Can't have an abstract method in a non-abstract class.")) {
            return CompilerErrorID.CLASS_DOES_NOT_IMPLEMENT_ALL_METHODS;
        }
        return CompilerErrorID.UNDEFINED;
    }
}

