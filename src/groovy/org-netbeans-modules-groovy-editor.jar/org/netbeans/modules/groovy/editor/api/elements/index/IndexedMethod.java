/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 */
package org.netbeans.modules.groovy.editor.api.elements.index;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;

public final class IndexedMethod
extends IndexedElement
implements MethodElement {
    private final List<MethodElement.MethodParameter> parameters;
    private final String returnType;

    public IndexedMethod(IndexResult result, String clz, String name, String returnType, List<MethodElement.MethodParameter> parameters, String attributes, int flags) {
        super(result, clz, name, attributes, flags);
        this.returnType = returnType;
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return this.getSignature();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getReturnType() {
        return this.returnType;
    }

    @Override
    public String getSignature() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.in);
        sb.append("#");
        sb.append(this.name);
        if (!this.parameters.isEmpty()) {
            sb.append("(");
            for (MethodElement.MethodParameter param : this.parameters) {
                sb.append(param.getFqnType());
                sb.append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(")");
        }
        return sb.toString();
    }

    @Override
    public List<MethodElement.MethodParameter> getParameters() {
        return this.parameters;
    }

    @Override
    public List<String> getParameterTypes() {
        ArrayList<String> paramTypes = new ArrayList<String>();
        for (MethodElement.MethodParameter parameter : this.getParameters()) {
            paramTypes.add(parameter.getType());
        }
        return paramTypes;
    }

    @Override
    public ElementKind getKind() {
        if (this.name == null && this.signature.startsWith("initialize(") || this.name != null && this.name.equals("initialize")) {
            return ElementKind.CONSTRUCTOR;
        }
        return ElementKind.METHOD;
    }

    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.in);
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.signature);
        hash = 47 * hash + Objects.hashCode(this.modifiers);
        hash = 47 * hash + Objects.hashCode(this.parameters);
        hash = 47 * hash + Objects.hashCode(this.returnType);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        IndexedMethod other = (IndexedMethod)obj;
        if (!Objects.equals(this.in, other.in)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.signature, other.signature)) {
            return false;
        }
        if (!Objects.equals(this.modifiers, other.modifiers)) {
            return false;
        }
        if (!Objects.equals(this.parameters, other.parameters)) {
            return false;
        }
        if (!Objects.equals(this.returnType, other.returnType)) {
            return false;
        }
        return true;
    }
}

