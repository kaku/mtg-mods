/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.CompletionProposal
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;
import org.netbeans.modules.groovy.editor.completion.ConstructorGenerationCompletion;
import org.netbeans.modules.groovy.editor.completion.FieldCompletion;
import org.netbeans.modules.groovy.editor.completion.KeywordCompletion;
import org.netbeans.modules.groovy.editor.completion.LocalVarCompletion;
import org.netbeans.modules.groovy.editor.completion.MethodCompletion;
import org.netbeans.modules.groovy.editor.completion.NamedParamsCompletion;
import org.netbeans.modules.groovy.editor.completion.NewVarCompletion;
import org.netbeans.modules.groovy.editor.completion.PackageCompletion;
import org.netbeans.modules.groovy.editor.completion.TypesCompletion;

public class ProposalsCollector {
    private List<CompletionProposal> proposals;
    private CompletionContext context;
    private BaseCompletion typesCompletion;
    private BaseCompletion fieldCompletion;
    private BaseCompletion methodCompletion;
    private BaseCompletion newVarCompletion;
    private BaseCompletion keywordCompletion;
    private BaseCompletion packageCompletion;
    private BaseCompletion localVarCompletion;
    private BaseCompletion camelCaseCompletion;
    private BaseCompletion namedParamCompletion;

    public ProposalsCollector(CompletionContext context) {
        this.context = context;
        this.proposals = new ArrayList<CompletionProposal>();
        this.typesCompletion = new TypesCompletion();
        this.fieldCompletion = new FieldCompletion();
        this.methodCompletion = new MethodCompletion();
        this.newVarCompletion = new NewVarCompletion();
        this.keywordCompletion = new KeywordCompletion();
        this.packageCompletion = new PackageCompletion();
        this.localVarCompletion = new LocalVarCompletion();
        this.camelCaseCompletion = new ConstructorGenerationCompletion();
        this.namedParamCompletion = new NamedParamsCompletion();
    }

    public void completeKeywords(CompletionContext completionRequest) {
        this.keywordCompletion.complete(this.proposals, completionRequest, this.context.getAnchor());
    }

    public void completeMethods(CompletionContext completionRequest) {
        this.methodCompletion.complete(this.proposals, completionRequest, this.context.getAnchor());
    }

    public void completeFields(CompletionContext completionRequest) {
        this.fieldCompletion.complete(this.proposals, completionRequest, this.context.getAnchor());
    }

    public void completeCamelCase(CompletionContext request) {
        this.camelCaseCompletion.complete(this.proposals, request, this.context.getAnchor());
    }

    public void completeTypes(CompletionContext request) {
        this.typesCompletion.complete(this.proposals, request, this.context.getAnchor());
    }

    public void completePackages(CompletionContext request) {
        this.packageCompletion.complete(this.proposals, request, this.context.getAnchor());
    }

    public void completeLocalVars(CompletionContext request) {
        this.localVarCompletion.complete(this.proposals, request, this.context.getAnchor());
    }

    public void completeNewVars(CompletionContext request) {
        this.newVarCompletion.complete(this.proposals, request, this.context.getAnchor());
    }

    public void completeNamedParams(CompletionContext request) {
        this.namedParamCompletion.complete(this.proposals, request, this.context.getAnchor());
    }

    public List<CompletionProposal> getCollectedProposals() {
        return this.proposals;
    }

    public void clearProposals() {
        this.proposals.clear();
    }
}

