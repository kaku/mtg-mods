/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.completion.provider;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Modifier;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedField;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedMethod;
import org.netbeans.modules.groovy.editor.completion.AccessLevel;
import org.netbeans.modules.groovy.editor.java.Utilities;
import org.netbeans.modules.groovy.editor.spi.completion.CompletionProvider;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public final class GroovyElementsProvider
implements CompletionProvider {
    @Override
    public Map<MethodSignature, CompletionItem> getMethods(CompletionContext context) {
        GroovyIndex index = this.getIndex(context);
        HashMap<MethodSignature, CompletionItem> result = new HashMap<MethodSignature, CompletionItem>();
        if (index != null) {
            Set<IndexedMethod> methods = "".equals(context.getPrefix()) ? index.getMethods(".*", context.getTypeName(), QuerySupport.Kind.REGEXP) : index.getMethods(context.getPrefix(), context.getTypeName(), QuerySupport.Kind.PREFIX);
            for (IndexedMethod indexedMethod : methods) {
                if (!this.accept(context.access, indexedMethod)) continue;
                result.put(this.getMethodSignature(indexedMethod), CompletionItem.forJavaMethod(context.getTypeName(), indexedMethod.getName(), indexedMethod.getParameterTypes(), indexedMethod.getReturnType(), Utilities.gsfModifiersToModel(indexedMethod.getModifiers(), Modifier.PUBLIC), context.getAnchor(), false, context.isNameOnly()));
            }
        }
        return result;
    }

    @Override
    public Map<MethodSignature, CompletionItem> getStaticMethods(CompletionContext context) {
        return Collections.emptyMap();
    }

    @Override
    public Map<FieldSignature, CompletionItem> getFields(CompletionContext context) {
        GroovyIndex index = this.getIndex(context);
        HashMap<FieldSignature, CompletionItem> result = new HashMap<FieldSignature, CompletionItem>();
        if (index != null) {
            Set<IndexedField> fields = "".equals(context.getPrefix()) ? index.getAllFields(context.getTypeName()) : index.getFields(context.getPrefix(), context.getTypeName(), QuerySupport.Kind.PREFIX);
            for (IndexedField indexedField : fields) {
                result.put(this.getFieldSignature(indexedField), new CompletionItem.FieldItem(indexedField.getTypeName(), indexedField.getName(), indexedField.getModifiers(), context.getAnchor()));
            }
        }
        return result;
    }

    @Override
    public Map<FieldSignature, CompletionItem> getStaticFields(CompletionContext context) {
        return Collections.emptyMap();
    }

    private GroovyIndex getIndex(CompletionContext context) {
        FileObject fo = context.getSourceFile();
        if (fo != null) {
            return GroovyIndex.get(QuerySupport.findRoots((FileObject)fo, Collections.singleton("classpath/source"), (Collection)null, (Collection)null));
        }
        return null;
    }

    private MethodSignature getMethodSignature(IndexedMethod method) {
        String[] parameters = method.getParameterTypes().toArray(new String[method.getParameterTypes().size()]);
        return new MethodSignature(method.getName(), parameters);
    }

    private FieldSignature getFieldSignature(IndexedField field) {
        return new FieldSignature(field.getName());
    }

    private boolean accept(Set<AccessLevel> levels, IndexedElement element) {
        for (AccessLevel accessLevel : levels) {
            if (!accessLevel.accept(element.getModifiers())) continue;
            return true;
        }
        return false;
    }
}

