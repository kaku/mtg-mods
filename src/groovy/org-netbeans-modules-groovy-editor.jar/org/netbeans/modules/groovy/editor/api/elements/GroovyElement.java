/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.elements;

import java.util.Collections;
import java.util.Set;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;

public abstract class GroovyElement
implements ElementHandle {
    protected String in;
    protected String name;
    protected String signature;

    public GroovyElement() {
    }

    public GroovyElement(String in) {
        this(in, null);
    }

    public GroovyElement(String in, String name) {
        this.in = in;
        this.name = name;
    }

    public abstract ElementKind getKind();

    public boolean signatureEquals(ElementHandle handle) {
        if (this.getIn().equals(handle.getIn()) && this.getName().equals(handle.getName()) && this.getKind().equals((Object)handle.getKind())) {
            return true;
        }
        return false;
    }

    public String getIn() {
        return this.in;
    }

    public String getName() {
        return this.name;
    }

    public String getSignature() {
        if (this.signature == null) {
            StringBuilder sb = new StringBuilder();
            String clz = this.getIn();
            if (clz != null && clz.length() > 0) {
                sb.append(clz);
                sb.append(".");
            }
            sb.append(this.getName());
            this.signature = sb.toString();
        }
        return this.signature;
    }

    public String getMimeType() {
        return "text/x-groovy";
    }

    public FileObject getFileObject() {
        return null;
    }

    public Set<Modifier> getModifiers() {
        return Collections.emptySet();
    }

    public OffsetRange getOffsetRange(ParserResult result) {
        return OffsetRange.NONE;
    }
}

