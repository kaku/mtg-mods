/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.netbeans.modules.csl.api.ElementKind
 */
package org.netbeans.modules.groovy.editor.api.elements.ast;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.netbeans.modules.groovy.editor.api.elements.common.ClassElement;

public class ASTClass
extends ASTElement
implements ClassElement {
    private final String fqn;

    public ASTClass(ClassNode node, String fqn) {
        super((ASTNode)node, node.getPackageName(), node.getNameWithoutPackage());
        this.fqn = fqn != null ? fqn : this.getName();
    }

    @Override
    public String getFqn() {
        return this.fqn;
    }

    @Override
    public ElementKind getKind() {
        return ElementKind.CLASS;
    }
}

