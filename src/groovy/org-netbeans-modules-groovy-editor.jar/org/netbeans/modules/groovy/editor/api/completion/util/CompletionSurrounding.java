/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.groovy.editor.api.completion.util;

import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;

public class CompletionSurrounding {
    public Token<GroovyTokenId> beforeLiteral;
    public Token<GroovyTokenId> before2;
    public Token<GroovyTokenId> before1;
    public Token<GroovyTokenId> active;
    public Token<GroovyTokenId> after1;
    public Token<GroovyTokenId> after2;
    public Token<GroovyTokenId> afterLiteral;
    public TokenSequence<GroovyTokenId> ts;

    public CompletionSurrounding(Token<GroovyTokenId> beforeLiteral, Token<GroovyTokenId> before2, Token<GroovyTokenId> before1, Token<GroovyTokenId> active, Token<GroovyTokenId> after1, Token<GroovyTokenId> after2, Token<GroovyTokenId> afterLiteral, TokenSequence<GroovyTokenId> ts) {
        this.beforeLiteral = beforeLiteral;
        this.before2 = before2;
        this.before1 = before1;
        this.active = active;
        this.after1 = after1;
        this.after2 = after2;
        this.afterLiteral = afterLiteral;
        this.ts = ts;
    }
}

