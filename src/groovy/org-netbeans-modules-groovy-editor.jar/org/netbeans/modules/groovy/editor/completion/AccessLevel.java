/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassNode
 *  org.netbeans.api.java.source.ElementUtilities
 *  org.netbeans.api.java.source.ElementUtilities$ElementAcceptor
 *  org.netbeans.modules.csl.api.Modifier
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;
import org.codehaus.groovy.ast.ClassNode;
import org.netbeans.api.java.source.ElementUtilities;

public enum AccessLevel {
    PUBLIC{

        @Override
        public ElementUtilities.ElementAcceptor getJavaAcceptor() {
            return new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror type) {
                    return e.getModifiers().contains((Object)Modifier.PUBLIC);
                }
            };
        }

        @Override
        public boolean accept(Set<org.netbeans.modules.csl.api.Modifier> modifiers) {
            return modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PUBLIC);
        }

    }
    ,
    PACKAGE{

        @Override
        public ElementUtilities.ElementAcceptor getJavaAcceptor() {
            return new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror type) {
                    Set<Modifier> modifiers = e.getModifiers();
                    return !modifiers.contains((Object)Modifier.PUBLIC) && !modifiers.contains((Object)Modifier.PROTECTED) && !modifiers.contains((Object)Modifier.PRIVATE);
                }
            };
        }

        @Override
        public boolean accept(Set<org.netbeans.modules.csl.api.Modifier> modifiers) {
            return !modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PRIVATE) && !modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PROTECTED) && !modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PUBLIC);
        }

    }
    ,
    PROTECTED{

        @Override
        public ElementUtilities.ElementAcceptor getJavaAcceptor() {
            return new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror type) {
                    return e.getModifiers().contains((Object)Modifier.PROTECTED);
                }
            };
        }

        @Override
        public boolean accept(Set<org.netbeans.modules.csl.api.Modifier> modifiers) {
            return modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PROTECTED);
        }

    }
    ,
    PRIVATE{

        @Override
        public ElementUtilities.ElementAcceptor getJavaAcceptor() {
            return new ElementUtilities.ElementAcceptor(){

                public boolean accept(Element e, TypeMirror type) {
                    return e.getModifiers().contains((Object)Modifier.PRIVATE);
                }
            };
        }

        @Override
        public boolean accept(Set<org.netbeans.modules.csl.api.Modifier> modifiers) {
            return modifiers.contains((Object)org.netbeans.modules.csl.api.Modifier.PRIVATE);
        }

    };
    

    private AccessLevel() {
    }

    public abstract ElementUtilities.ElementAcceptor getJavaAcceptor();

    public abstract boolean accept(Set<org.netbeans.modules.csl.api.Modifier> var1);

    public static Set<AccessLevel> create(ClassNode source, ClassNode type) {
        EnumSet<AccessLevel> levels = source == null ? EnumSet.of(PUBLIC) : (type.equals((Object)source) ? EnumSet.allOf(AccessLevel.class) : (AccessLevel.getPackageName(source).equals(AccessLevel.getPackageName(type)) ? EnumSet.of(PUBLIC, PACKAGE) : (source.getSuperClass() == null && type.getName().equals("java.lang.Object") || source.getSuperClass() != null && source.getSuperClass().getName().equals(type.getName()) ? EnumSet.complementOf(EnumSet.of(PRIVATE)) : EnumSet.of(PUBLIC))));
        return levels;
    }

    public static Set<AccessLevel> update(Set<AccessLevel> levels, ClassNode source, ClassNode type) {
        HashSet<AccessLevel> modifiedAccess = new HashSet<AccessLevel>(levels);
        if (source == null || !type.equals((Object)source)) {
            modifiedAccess.remove((Object)((Object)PRIVATE));
        }
        if (source == null || !AccessLevel.getPackageName(source).equals(AccessLevel.getPackageName(type))) {
            modifiedAccess.remove((Object)((Object)PACKAGE));
        } else {
            modifiedAccess.add(PACKAGE);
        }
        return modifiedAccess;
    }

    private static String getPackageName(ClassNode node) {
        if (node.getPackageName() != null) {
            return node.getPackageName();
        }
        return "";
    }

}

