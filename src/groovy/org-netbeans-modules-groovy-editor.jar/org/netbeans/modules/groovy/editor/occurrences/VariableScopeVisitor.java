/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.AnnotationNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.GenericsType
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.VariableScope
 *  org.codehaus.groovy.ast.expr.ArrayExpression
 *  org.codehaus.groovy.ast.expr.AttributeExpression
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.PropertyExpression
 *  org.codehaus.groovy.ast.expr.TupleExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.CatchStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.groovy.editor.occurrences;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotationNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.GenericsType;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.VariableScope;
import org.codehaus.groovy.ast.expr.ArrayExpression;
import org.codehaus.groovy.ast.expr.AttributeExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.ElementUtils;
import org.netbeans.modules.groovy.editor.api.FindTypeUtils;
import org.netbeans.modules.groovy.editor.api.Methods;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.occurrences.TypeVisitor;

public final class VariableScopeVisitor
extends TypeVisitor {
    private final Set<ASTNode> occurrences = new HashSet<ASTNode>();
    private final ASTNode leafParent;

    public VariableScopeVisitor(SourceUnit sourceUnit, AstPath path, BaseDocument doc, int cursorOffset) {
        super(sourceUnit, path, doc, cursorOffset, true);
        this.leafParent = path.leafParent();
    }

    public Set<ASTNode> getOccurrences() {
        return this.occurrences;
    }

    public void visitArrayExpression(ArrayExpression visitedArray) {
        ClassNode visitedType = visitedArray.getElementType();
        String visitedName = ElementUtils.getTypeName((ASTNode)visitedType);
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            ASTNode currentNode = FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset);
            this.addOccurrences(visitedType, (ClassNode)currentNode);
        } else if (this.leaf instanceof Variable) {
            String varName = this.removeParentheses(((Variable)this.leaf).getName());
            if (varName.equals(visitedName)) {
                this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)visitedType, visitedName));
            }
        } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof PropertyExpression && visitedName.equals(((PropertyExpression)this.leafParent).getPropertyAsString())) {
            this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)visitedType, visitedName));
        }
        super.visitArrayExpression(visitedArray);
    }

    @Override
    protected void visitParameters(Parameter[] parameters, Variable variable) {
        for (Parameter parameter : parameters) {
            ClassNode paramType = parameter.getType();
            if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
                this.addOccurrences(paramType, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
                continue;
            }
            if (!parameter.getName().equals(variable.getName())) continue;
            this.occurrences.add((ASTNode)parameter);
            break;
        }
        super.visitParameters(parameters, variable);
    }

    public void visitClosureExpression(ClosureExpression expression) {
        if (expression.isParameterSpecified() && this.leaf instanceof Variable) {
            this.visitParameters(expression.getParameters(), (Variable)this.leaf);
        }
        super.visitClosureExpression(expression);
    }

    @Override
    protected boolean isValidToken(Token<GroovyTokenId> currentToken, Token<GroovyTokenId> previousToken) {
        return currentToken.id() == GroovyTokenId.IDENTIFIER || previousToken.id() == GroovyTokenId.IDENTIFIER;
    }

    public void visitVariableExpression(VariableExpression variableExpression) {
        ASTNode root;
        ClassNode visitedType = variableExpression.getType();
        String visitedName = variableExpression.getName();
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addOccurrences(visitedType, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        } else if (this.leaf instanceof FieldNode) {
            if (visitedName.equals(((FieldNode)this.leaf).getName())) {
                this.occurrences.add((ASTNode)variableExpression);
            }
        } else if (this.leaf instanceof PropertyNode) {
            if (visitedName.equals(((PropertyNode)this.leaf).getField().getName())) {
                this.occurrences.add((ASTNode)variableExpression);
            }
        } else if (this.leaf instanceof Variable) {
            if (visitedName.equals(((Variable)this.leaf).getName())) {
                this.occurrences.add((ASTNode)variableExpression);
            }
        } else if (this.leaf instanceof ForStatement) {
            if (visitedName.equals(((ForStatement)this.leaf).getVariable().getName())) {
                this.occurrences.add((ASTNode)variableExpression);
            }
        } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof PropertyExpression) {
            PropertyExpression property = (PropertyExpression)this.leafParent;
            if (variableExpression.getName().equals(property.getPropertyAsString())) {
                this.occurrences.add((ASTNode)variableExpression);
            }
        } else if (this.leaf instanceof BlockStatement && (root = this.path.root()) instanceof ModuleNode) {
            for (Map.Entry entry : ((ModuleNode)root).getStaticImports().entrySet()) {
                OffsetRange range;
                String alias = (String)entry.getKey();
                if (!alias.equals(variableExpression.getName()) || !(range = ASTUtils.getNextIdentifierByName(this.doc, alias, this.cursorOffset)).containsInclusive(this.cursorOffset)) continue;
                this.occurrences.add((ASTNode)variableExpression);
            }
        }
        super.visitVariableExpression(variableExpression);
    }

    public void visitDeclarationExpression(DeclarationExpression expression) {
        ClassNode visitedType = !expression.isMultipleAssignmentDeclaration() ? expression.getVariableExpression().getType() : expression.getTupleExpression().getType();
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addOccurrences(visitedType, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        }
        super.visitDeclarationExpression(expression);
    }

    public void visitField(FieldNode visitedField) {
        PropertyExpression property;
        String visitedName = visitedField.getName();
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addFieldOccurrences(visitedField, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        } else if (this.leaf instanceof FieldNode) {
            if (visitedName.equals(((FieldNode)this.leaf).getName())) {
                this.occurrences.add((ASTNode)visitedField);
            }
        } else if (this.leaf instanceof PropertyNode) {
            if (visitedName.equals(((PropertyNode)this.leaf).getField().getName())) {
                this.occurrences.add((ASTNode)visitedField);
            }
        } else if (this.leaf instanceof Variable) {
            if (visitedName.equals(((Variable)this.leaf).getName())) {
                this.occurrences.add((ASTNode)visitedField);
            }
        } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof PropertyExpression && visitedName.equals((property = (PropertyExpression)this.leafParent).getPropertyAsString())) {
            this.occurrences.add((ASTNode)visitedField);
        }
        super.visitField(visitedField);
    }

    private void addFieldOccurrences(FieldNode visitedField, ClassNode findingNode) {
        this.addOccurrences(visitedField.getType(), findingNode);
        for (AnnotationNode annotation : visitedField.getAnnotations(findingNode)) {
            this.addAnnotationOccurrences(annotation, findingNode);
        }
    }

    public void visitMethod(MethodNode methodNode) {
        MethodCallExpression methodCallExpression;
        VariableScope variableScope = methodNode.getVariableScope();
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addMethodOccurrences(methodNode, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        } else if (this.leaf instanceof Variable) {
            String name = ((Variable)this.leaf).getName();
            if (variableScope != null && variableScope.getDeclaredVariable(name) != null) {
                return;
            }
        } else if (this.leaf instanceof MethodNode) {
            if (Methods.isSameMethod(methodNode, (MethodNode)this.leaf)) {
                this.occurrences.add((ASTNode)methodNode);
            }
        } else if (this.leaf instanceof DeclarationExpression) {
            VariableExpression variable = ((DeclarationExpression)this.leaf).getVariableExpression();
            if (!variable.isDynamicTyped() && !methodNode.isDynamicReturnType()) {
                this.addMethodOccurrences(methodNode, variable.getType());
            }
        } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof MethodCallExpression && Methods.isSameMethod(methodNode, methodCallExpression = (MethodCallExpression)this.leafParent)) {
            this.occurrences.add((ASTNode)methodNode);
        }
        super.visitMethod(methodNode);
    }

    private void addMethodOccurrences(MethodNode visitedMethod, ClassNode findingNode) {
        this.addOccurrences(visitedMethod.getReturnType(), findingNode);
        for (Parameter parameter : visitedMethod.getParameters()) {
            this.addOccurrences(parameter.getType(), findingNode);
        }
        for (AnnotationNode annotation : visitedMethod.getAnnotations(findingNode)) {
            this.addAnnotationOccurrences(annotation, findingNode);
        }
    }

    public void visitConstructor(ConstructorNode constructor) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addConstructorOccurrences(constructor, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        } else {
            VariableScope variableScope = constructor.getVariableScope();
            if (this.leaf instanceof Variable) {
                String name = ((Variable)this.leaf).getName();
                if (variableScope != null && variableScope.getDeclaredVariable(name) != null) {
                    return;
                }
            } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof PropertyExpression) {
                String name = ((ConstantExpression)this.leaf).getText();
                if (variableScope != null && variableScope.getDeclaredVariable(name) != null) {
                    return;
                }
            }
            if (this.leaf instanceof ConstructorNode) {
                if (Methods.isSameConstructor(constructor, (ConstructorNode)this.leaf)) {
                    this.occurrences.add((ASTNode)constructor);
                }
            } else if (this.leaf instanceof ConstructorCallExpression && Methods.isSameConstructor(constructor, (ConstructorCallExpression)this.leaf) && !constructor.hasNoRealSourcePosition()) {
                this.occurrences.add((ASTNode)constructor);
            }
        }
        super.visitConstructor(constructor);
    }

    private void addConstructorOccurrences(ConstructorNode constructor, ClassNode findingNode) {
        for (Parameter parameter : constructor.getParameters()) {
            this.addOccurrences(parameter.getType(), findingNode);
        }
        for (AnnotationNode annotation : constructor.getAnnotations(findingNode)) {
            this.addAnnotationOccurrences(annotation, findingNode);
        }
    }

    public void visitMethodCallExpression(MethodCallExpression methodCall) {
        if (!FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            ASTNode root;
            if (this.leaf instanceof MethodNode) {
                MethodNode method = (MethodNode)this.leaf;
                if (Methods.isSameMethod(method, methodCall)) {
                    this.occurrences.add((ASTNode)methodCall);
                }
            } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof MethodCallExpression && Methods.isSameMethod(methodCall, (MethodCallExpression)this.leafParent)) {
                this.occurrences.add((ASTNode)methodCall);
            }
            if (this.leaf instanceof BlockStatement && (root = this.path.root()) instanceof ModuleNode) {
                for (Map.Entry entry : ((ModuleNode)root).getStaticImports().entrySet()) {
                    OffsetRange range;
                    String alias = (String)entry.getKey();
                    if (!alias.equals(methodCall.getMethodAsString()) || !(range = ASTUtils.getNextIdentifierByName(this.doc, alias, this.cursorOffset)).containsInclusive(this.cursorOffset)) continue;
                    this.occurrences.add((ASTNode)methodCall);
                }
            }
        }
        super.visitMethodCallExpression(methodCall);
    }

    public void visitConstructorCallExpression(ConstructorCallExpression call) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            String findingNodeName;
            ClassNode findingNode = (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset);
            String callName = ElementUtils.getNameWithoutPackage((ASTNode)call);
            if (!callName.equals(findingNodeName = ElementUtils.getNameWithoutPackage((ASTNode)findingNode))) {
                this.addOccurrences(call.getType(), findingNode);
            }
        } else if (this.leaf instanceof ConstructorNode) {
            ConstructorNode constructor = (ConstructorNode)this.leaf;
            if (Methods.isSameConstructor(constructor, call)) {
                this.occurrences.add((ASTNode)call);
            }
        } else if (this.leaf instanceof ConstructorCallExpression && Methods.isSameConstuctor(call, (ConstructorCallExpression)this.leaf)) {
            this.occurrences.add((ASTNode)call);
        }
        super.visitConstructorCallExpression(call);
    }

    public void visitClassExpression(ClassExpression clazz) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addClassExpressionOccurrences(clazz, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        }
        super.visitClassExpression(clazz);
    }

    private void addClassExpressionOccurrences(ClassExpression clazz, ClassNode findingNode) {
        String findingName;
        String visitedName = ElementUtils.getTypeName((ASTNode)clazz);
        if (visitedName.equals(findingName = ElementUtils.getTypeName((ASTNode)findingNode))) {
            this.occurrences.add((ASTNode)clazz);
        }
    }

    public void visitClass(ClassNode classNode) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addClassNodeOccurrences(classNode, (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        }
        super.visitClass(classNode);
    }

    private void addClassNodeOccurrences(ClassNode visitedNode, ClassNode findingNode) {
        String findingName = ElementUtils.getTypeName((ASTNode)findingNode);
        ClassNode superClass = visitedNode.getUnresolvedSuperClass(false);
        ClassNode[] interfaces = visitedNode.getInterfaces();
        if (findingName.equals(visitedNode.getName())) {
            this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)visitedNode, visitedNode.getNameWithoutPackage()));
        }
        if (superClass.getLineNumber() > 0 && superClass.getColumnNumber() > 0 && findingName.equals(superClass.getName())) {
            this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)superClass, superClass.getNameWithoutPackage()));
        }
        for (ClassNode interfaceNode : interfaces) {
            if (interfaceNode.getLineNumber() <= 0 || interfaceNode.getColumnNumber() <= 0 || !findingName.equals(interfaceNode.getName())) continue;
            this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)interfaceNode, interfaceNode.getNameWithoutPackage()));
        }
        for (AnnotationNode annotation : visitedNode.getAnnotations(findingNode)) {
            this.addAnnotationOccurrences(annotation, findingNode);
        }
    }

    public void visitAttributeExpression(AttributeExpression expression) {
        this.addExpressionOccurrences((PropertyExpression)expression);
        super.visitAttributeExpression(expression);
    }

    public void visitPropertyExpression(PropertyExpression node) {
        this.addExpressionOccurrences(node);
        super.visitPropertyExpression(node);
    }

    private void addExpressionOccurrences(PropertyExpression expression) {
        Expression property = expression.getProperty();
        String nodeAsString = expression.getPropertyAsString();
        if (nodeAsString != null) {
            PropertyExpression propertyUnderCursor;
            if (this.leaf instanceof Variable && nodeAsString.equals(((Variable)this.leaf).getName())) {
                this.occurrences.add((ASTNode)property);
            } else if (this.leaf instanceof ConstantExpression && this.leafParent instanceof PropertyExpression && nodeAsString.equals((propertyUnderCursor = (PropertyExpression)this.leafParent).getPropertyAsString())) {
                this.occurrences.add((ASTNode)property);
            }
        }
    }

    public void visitForLoop(ForStatement forLoop) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addOccurrences(forLoop.getVariableType(), (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        } else {
            Parameter forLoopVar = forLoop.getVariable();
            String varName = forLoopVar.getName();
            if (this.leaf instanceof Variable) {
                if (varName.equals(((Variable)this.leaf).getName())) {
                    this.occurrences.add((ASTNode)forLoopVar);
                }
            } else if (this.leaf instanceof ForStatement && varName.equals(((ForStatement)this.leaf).getVariable().getName())) {
                this.occurrences.add((ASTNode)forLoopVar);
            }
        }
        super.visitForLoop(forLoop);
    }

    public void visitCatchStatement(CatchStatement statement) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            this.addOccurrences(statement.getExceptionType(), (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
        }
        super.visitCatchStatement(statement);
    }

    public void visitImports(ModuleNode node) {
        if (FindTypeUtils.isCaretOnClassNode(this.path, this.doc, this.cursorOffset)) {
            for (ImportNode importNode : node.getImports()) {
                this.addOccurrences(importNode.getType(), (ClassNode)FindTypeUtils.findCurrentNode(this.path, this.doc, this.cursorOffset));
            }
        } else {
            if (this.leaf instanceof ConstantExpression && this.leafParent instanceof MethodCallExpression) {
                this.addAliasOccurrences(node, ((MethodCallExpression)this.leafParent).getMethodAsString());
            }
            if (this.leaf instanceof VariableExpression) {
                this.addAliasOccurrences(node, ((VariableExpression)this.leaf).getName());
            }
            if (this.leaf instanceof BlockStatement) {
                for (Map.Entry entry : node.getStaticImports().entrySet()) {
                    OffsetRange range = ASTUtils.getNextIdentifierByName(this.doc, (String)entry.getKey(), this.cursorOffset);
                    if (!range.containsInclusive(this.cursorOffset)) continue;
                    this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)((ImportNode)entry.getValue()).getType(), (String)entry.getKey()));
                }
            }
        }
        super.visitImports(node);
    }

    private void addAliasOccurrences(ModuleNode moduleNode, String findingName) {
        for (Map.Entry entry : moduleNode.getStaticImports().entrySet()) {
            if (!findingName.equals(entry.getKey())) continue;
            this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)((ImportNode)entry.getValue()).getType(), (String)entry.getKey()));
        }
    }

    private void addAnnotationOccurrences(AnnotationNode annotation, ClassNode findingNode) {
        ClassNode classNode = annotation.getClassNode();
        classNode.setLineNumber(annotation.getLineNumber());
        classNode.setColumnNumber(annotation.getColumnNumber());
        classNode.setLastLineNumber(annotation.getLastLineNumber());
        classNode.setLastColumnNumber(annotation.getLastColumnNumber());
        this.addOccurrences(classNode, findingNode);
    }

    private void addOccurrences(ClassNode visitedType, ClassNode findingType) {
        String visitedTypeName = ElementUtils.getTypeName((ASTNode)visitedType);
        String findingName = ElementUtils.getTypeName((ASTNode)findingType);
        String findingNameWithoutPkg = ElementUtils.getTypeNameWithoutPackage((ASTNode)findingType);
        if (visitedTypeName.equals(findingName)) {
            this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)visitedType, findingNameWithoutPkg));
        }
        this.addGenericsOccurrences(visitedType, findingType);
    }

    private void addGenericsOccurrences(ClassNode visitedType, ClassNode findingNode) {
        String findingTypeName = ElementUtils.getTypeName((ASTNode)findingNode);
        GenericsType[] genericsTypes = visitedType.getGenericsTypes();
        if (genericsTypes != null && genericsTypes.length > 0) {
            for (GenericsType genericsType : genericsTypes) {
                String genericTypeName = genericsType.getType().getName();
                String genericTypeNameWithoutPkg = genericsType.getName();
                if (!genericTypeName.equals(findingTypeName)) continue;
                this.occurrences.add(new ASTUtils.FakeASTNode((ASTNode)genericsType, genericTypeNameWithoutPkg));
            }
        }
    }

    private String removeParentheses(String name) {
        if (name.endsWith("[]")) {
            name = name.substring(0, name.length() - 2);
        }
        return name;
    }
}

