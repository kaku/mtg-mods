/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.BinaryExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.control.SourceUnit
 *  org.codehaus.groovy.syntax.Token
 *  org.netbeans.editor.BaseDocument
 */
package org.netbeans.modules.groovy.editor.completion.inference;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.syntax.Token;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.occurrences.TypeVisitor;

public class TypeInferenceVisitor
extends TypeVisitor {
    private ClassNode guessedType;
    private boolean leafReached = false;

    public TypeInferenceVisitor(SourceUnit sourceUnit, AstPath path, BaseDocument doc, int cursorOffset) {
        super(sourceUnit, path, doc, cursorOffset, false);
    }

    public ClassNode getGuessedType() {
        return this.guessedType;
    }

    @Override
    public void collect() {
        this.guessedType = null;
        this.leafReached = false;
        super.collect();
    }

    @Override
    protected void visitParameters(Parameter[] parameters, Variable variable) {
        if (!this.leafReached) {
            for (Parameter param : parameters) {
                if (!TypeInferenceVisitor.sameVariableName(param, variable)) continue;
                this.guessedType = param.getType();
                break;
            }
        }
    }

    public void visitVariableExpression(VariableExpression expression) {
        if (expression == this.leaf) {
            this.leafReached = true;
        }
        super.visitVariableExpression(expression);
    }

    public void visitBinaryExpression(BinaryExpression expression) {
        Expression leftExpression;
        if (!this.leafReached && (leftExpression = expression.getLeftExpression()) instanceof VariableExpression && expression.getOperation().isA(100) && TypeInferenceVisitor.sameVariableName(this.leaf, (ASTNode)leftExpression)) {
            Expression rightExpression = expression.getRightExpression();
            if (rightExpression instanceof ConstantExpression && !rightExpression.getText().equals("null")) {
                this.guessedType = ((ConstantExpression)rightExpression).getType();
            } else if (rightExpression instanceof ConstructorCallExpression) {
                this.guessedType = ((ConstructorCallExpression)rightExpression).getType();
            }
        }
        super.visitBinaryExpression(expression);
    }

    private static boolean sameVariableName(Parameter param, Variable variable) {
        return param.getName().equals(variable.getName());
    }

    private static boolean sameVariableName(ASTNode node1, ASTNode node2) {
        return node1 instanceof VariableExpression && node2 instanceof VariableExpression && ((VariableExpression)node1).getName().equals(((VariableExpression)node2).getName());
    }
}

