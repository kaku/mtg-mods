/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.GroovyCodeVisitor
 *  org.codehaus.groovy.ast.expr.ArgumentListExpression
 *  org.codehaus.groovy.ast.expr.ArrayExpression
 *  org.codehaus.groovy.ast.expr.AttributeExpression
 *  org.codehaus.groovy.ast.expr.BinaryExpression
 *  org.codehaus.groovy.ast.expr.BitwiseNegationExpression
 *  org.codehaus.groovy.ast.expr.BooleanExpression
 *  org.codehaus.groovy.ast.expr.CastExpression
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ClosureListExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.ElvisOperatorExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.FieldExpression
 *  org.codehaus.groovy.ast.expr.GStringExpression
 *  org.codehaus.groovy.ast.expr.ListExpression
 *  org.codehaus.groovy.ast.expr.MapEntryExpression
 *  org.codehaus.groovy.ast.expr.MapExpression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.MethodPointerExpression
 *  org.codehaus.groovy.ast.expr.NotExpression
 *  org.codehaus.groovy.ast.expr.PostfixExpression
 *  org.codehaus.groovy.ast.expr.PrefixExpression
 *  org.codehaus.groovy.ast.expr.PropertyExpression
 *  org.codehaus.groovy.ast.expr.RangeExpression
 *  org.codehaus.groovy.ast.expr.SpreadExpression
 *  org.codehaus.groovy.ast.expr.SpreadMapExpression
 *  org.codehaus.groovy.ast.expr.StaticMethodCallExpression
 *  org.codehaus.groovy.ast.expr.TernaryExpression
 *  org.codehaus.groovy.ast.expr.TupleExpression
 *  org.codehaus.groovy.ast.expr.UnaryMinusExpression
 *  org.codehaus.groovy.ast.expr.UnaryPlusExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.AssertStatement
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.BreakStatement
 *  org.codehaus.groovy.ast.stmt.CaseStatement
 *  org.codehaus.groovy.ast.stmt.CatchStatement
 *  org.codehaus.groovy.ast.stmt.ContinueStatement
 *  org.codehaus.groovy.ast.stmt.DoWhileStatement
 *  org.codehaus.groovy.ast.stmt.ExpressionStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.codehaus.groovy.ast.stmt.IfStatement
 *  org.codehaus.groovy.ast.stmt.ReturnStatement
 *  org.codehaus.groovy.ast.stmt.Statement
 *  org.codehaus.groovy.ast.stmt.SwitchStatement
 *  org.codehaus.groovy.ast.stmt.SynchronizedStatement
 *  org.codehaus.groovy.ast.stmt.ThrowStatement
 *  org.codehaus.groovy.ast.stmt.TryCatchStatement
 *  org.codehaus.groovy.ast.stmt.WhileStatement
 *  org.codehaus.groovy.classgen.BytecodeExpression
 */
package org.netbeans.modules.groovy.editor.utils;

import java.util.ArrayList;
import java.util.List;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.GroovyCodeVisitor;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ArrayExpression;
import org.codehaus.groovy.ast.expr.AttributeExpression;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.BitwiseNegationExpression;
import org.codehaus.groovy.ast.expr.BooleanExpression;
import org.codehaus.groovy.ast.expr.CastExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ClosureListExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.ElvisOperatorExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.FieldExpression;
import org.codehaus.groovy.ast.expr.GStringExpression;
import org.codehaus.groovy.ast.expr.ListExpression;
import org.codehaus.groovy.ast.expr.MapEntryExpression;
import org.codehaus.groovy.ast.expr.MapExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.MethodPointerExpression;
import org.codehaus.groovy.ast.expr.NotExpression;
import org.codehaus.groovy.ast.expr.PostfixExpression;
import org.codehaus.groovy.ast.expr.PrefixExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.RangeExpression;
import org.codehaus.groovy.ast.expr.SpreadExpression;
import org.codehaus.groovy.ast.expr.SpreadMapExpression;
import org.codehaus.groovy.ast.expr.StaticMethodCallExpression;
import org.codehaus.groovy.ast.expr.TernaryExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.expr.UnaryMinusExpression;
import org.codehaus.groovy.ast.expr.UnaryPlusExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.AssertStatement;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.BreakStatement;
import org.codehaus.groovy.ast.stmt.CaseStatement;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ContinueStatement;
import org.codehaus.groovy.ast.stmt.DoWhileStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.ast.stmt.IfStatement;
import org.codehaus.groovy.ast.stmt.ReturnStatement;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.ast.stmt.SwitchStatement;
import org.codehaus.groovy.ast.stmt.SynchronizedStatement;
import org.codehaus.groovy.ast.stmt.ThrowStatement;
import org.codehaus.groovy.ast.stmt.TryCatchStatement;
import org.codehaus.groovy.ast.stmt.WhileStatement;
import org.codehaus.groovy.classgen.BytecodeExpression;

public class ASTChildrenVisitor
implements GroovyCodeVisitor {
    private List<ASTNode> children = new ArrayList<ASTNode>();

    public List<ASTNode> children() {
        return this.children;
    }

    public void visitBlockStatement(BlockStatement block) {
        List statements = block.getStatements();
        for (Statement statement : statements) {
            this.children.add((ASTNode)statement);
        }
    }

    public void visitForLoop(ForStatement forLoop) {
        this.children.add((ASTNode)forLoop.getCollectionExpression());
        this.children.add((ASTNode)forLoop.getLoopBlock());
    }

    public void visitWhileLoop(WhileStatement loop) {
        this.children.add((ASTNode)loop.getBooleanExpression());
        this.children.add((ASTNode)loop.getLoopBlock());
    }

    public void visitDoWhileLoop(DoWhileStatement loop) {
        this.children.add((ASTNode)loop.getLoopBlock());
        this.children.add((ASTNode)loop.getBooleanExpression());
    }

    public void visitIfElse(IfStatement ifElse) {
        this.children.add((ASTNode)ifElse.getBooleanExpression());
        this.children.add((ASTNode)ifElse.getIfBlock());
        this.children.add((ASTNode)ifElse.getElseBlock());
    }

    public void visitExpressionStatement(ExpressionStatement statement) {
        this.children.add((ASTNode)statement.getExpression());
    }

    public void visitReturnStatement(ReturnStatement statement) {
        this.children.add((ASTNode)statement.getExpression());
    }

    public void visitAssertStatement(AssertStatement statement) {
        this.children.add((ASTNode)statement.getBooleanExpression());
        this.children.add((ASTNode)statement.getMessageExpression());
    }

    public void visitTryCatchFinally(TryCatchStatement statement) {
        this.children.add((ASTNode)statement.getTryStatement());
        List list = statement.getCatchStatements();
        for (CatchStatement catchStatement : list) {
            this.children.add((ASTNode)catchStatement);
        }
        this.children.add((ASTNode)statement.getFinallyStatement());
    }

    public void visitSwitch(SwitchStatement statement) {
        this.children.add((ASTNode)statement.getExpression());
        List list = statement.getCaseStatements();
        for (CaseStatement caseStatement : list) {
            this.children.add((ASTNode)caseStatement);
        }
        this.children.add((ASTNode)statement.getDefaultStatement());
    }

    public void visitCaseStatement(CaseStatement statement) {
        this.children.add((ASTNode)statement.getExpression());
        this.children.add((ASTNode)statement.getCode());
    }

    public void visitBreakStatement(BreakStatement statement) {
    }

    public void visitContinueStatement(ContinueStatement statement) {
    }

    public void visitSynchronizedStatement(SynchronizedStatement statement) {
        this.children.add((ASTNode)statement.getExpression());
        this.children.add((ASTNode)statement.getCode());
    }

    public void visitThrowStatement(ThrowStatement statement) {
        this.children.add((ASTNode)statement.getExpression());
    }

    public void visitMethodCallExpression(MethodCallExpression call) {
        this.children.add((ASTNode)call.getObjectExpression());
        this.children.add((ASTNode)call.getMethod());
        this.children.add((ASTNode)call.getArguments());
    }

    public void visitStaticMethodCallExpression(StaticMethodCallExpression call) {
        this.children.add((ASTNode)call.getArguments());
    }

    public void visitConstructorCallExpression(ConstructorCallExpression call) {
        this.children.add((ASTNode)call.getArguments());
    }

    public void visitBinaryExpression(BinaryExpression expression) {
        this.children.add((ASTNode)expression.getLeftExpression());
        this.children.add((ASTNode)expression.getRightExpression());
    }

    public void visitTernaryExpression(TernaryExpression expression) {
        this.children.add((ASTNode)expression.getBooleanExpression());
        this.children.add((ASTNode)expression.getTrueExpression());
        this.children.add((ASTNode)expression.getFalseExpression());
    }

    public void visitShortTernaryExpression(ElvisOperatorExpression expression) {
        this.visitTernaryExpression((TernaryExpression)expression);
    }

    public void visitPostfixExpression(PostfixExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitPrefixExpression(PrefixExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitBooleanExpression(BooleanExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitNotExpression(NotExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitClosureExpression(ClosureExpression expression) {
        this.children.add((ASTNode)expression.getCode());
    }

    public void visitTupleExpression(TupleExpression expression) {
        this.visitListOfExpressions(expression.getExpressions());
    }

    public void visitListExpression(ListExpression expression) {
        this.visitListOfExpressions(expression.getExpressions());
    }

    public void visitArrayExpression(ArrayExpression expression) {
        this.visitListOfExpressions(expression.getExpressions());
        this.visitListOfExpressions(expression.getSizeExpression());
    }

    public void visitMapExpression(MapExpression expression) {
        this.visitListOfExpressions(expression.getMapEntryExpressions());
    }

    public void visitMapEntryExpression(MapEntryExpression expression) {
        this.children.add((ASTNode)expression.getKeyExpression());
        this.children.add((ASTNode)expression.getValueExpression());
    }

    public void visitRangeExpression(RangeExpression expression) {
        this.children.add((ASTNode)expression.getFrom());
        this.children.add((ASTNode)expression.getTo());
    }

    public void visitSpreadExpression(SpreadExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitSpreadMapExpression(SpreadMapExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitMethodPointerExpression(MethodPointerExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
        this.children.add((ASTNode)expression.getMethodName());
    }

    public void visitUnaryMinusExpression(UnaryMinusExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitUnaryPlusExpression(UnaryPlusExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitBitwiseNegationExpression(BitwiseNegationExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitCastExpression(CastExpression expression) {
        this.children.add((ASTNode)expression.getExpression());
    }

    public void visitConstantExpression(ConstantExpression expression) {
    }

    public void visitClassExpression(ClassExpression expression) {
    }

    public void visitVariableExpression(VariableExpression expression) {
    }

    public void visitDeclarationExpression(DeclarationExpression expression) {
        this.visitBinaryExpression((BinaryExpression)expression);
    }

    public void visitPropertyExpression(PropertyExpression expression) {
        this.children.add((ASTNode)expression.getObjectExpression());
        this.children.add((ASTNode)expression.getProperty());
    }

    public void visitAttributeExpression(AttributeExpression expression) {
        this.children.add((ASTNode)expression.getObjectExpression());
        this.children.add((ASTNode)expression.getProperty());
    }

    public void visitFieldExpression(FieldExpression expression) {
    }

    public void visitGStringExpression(GStringExpression expression) {
        this.visitListOfExpressions(expression.getStrings());
        this.visitListOfExpressions(expression.getValues());
    }

    private void visitListOfExpressions(List list) {
        if (list == null) {
            return;
        }
        for (Expression expression : list) {
            if (expression instanceof SpreadExpression) {
                Expression spread = ((SpreadExpression)expression).getExpression();
                this.children.add((ASTNode)spread);
                continue;
            }
            this.children.add((ASTNode)expression);
        }
    }

    public void visitCatchStatement(CatchStatement statement) {
        this.children.add((ASTNode)statement.getCode());
    }

    public void visitArgumentlistExpression(ArgumentListExpression ale) {
        this.visitTupleExpression((TupleExpression)ale);
    }

    public void visitClosureListExpression(ClosureListExpression cle) {
        this.visitListOfExpressions(cle.getExpressions());
    }

    public void visitBytecodeExpression(BytecodeExpression bce) {
    }
}

