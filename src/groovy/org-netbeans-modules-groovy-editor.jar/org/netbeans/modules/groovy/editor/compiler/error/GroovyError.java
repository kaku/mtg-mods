/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.Error$Badging
 *  org.netbeans.modules.csl.api.Severity
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.compiler.error;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.openide.filesystems.FileObject;

public class GroovyError
implements Error.Badging {
    private final String displayName;
    private final String description;
    private final FileObject file;
    private final int start;
    private final int end;
    private final String key;
    private final Severity severity;
    private final CompilerErrorID id;

    public GroovyError(@NullAllowed String key, @NonNull String displayName, @NullAllowed String description, @NullAllowed FileObject file, @NonNull int start, @NonNull int end, @NonNull Severity severity, @NonNull CompilerErrorID id) {
        this.key = key;
        this.displayName = displayName;
        this.description = description;
        this.file = file;
        this.start = start;
        this.end = end;
        this.severity = severity;
        this.id = id;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getDescription() {
        return this.description;
    }

    public int getStartPosition() {
        return this.start;
    }

    public int getEndPosition() {
        return this.end;
    }

    public String toString() {
        return "GroovyError[" + this.displayName + ", " + this.description + ", " + (Object)this.severity + "]";
    }

    public String getKey() {
        return this.key;
    }

    public Object[] getParameters() {
        return null;
    }

    public Severity getSeverity() {
        return this.severity;
    }

    public FileObject getFile() {
        return this.file;
    }

    public CompilerErrorID getId() {
        return this.id;
    }

    public boolean isLineError() {
        return true;
    }

    public boolean showExplorerBadge() {
        return false;
    }
}

