/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.source.ClassIndex
 *  org.netbeans.api.java.source.ClassIndex$SearchScope
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.spi.ParserResult
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.source.ClassIndex;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.lexer.Token;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;

public class PackageCompletion
extends BaseCompletion {
    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext request, int anchor) {
        LOG.log(Level.FINEST, "-> completePackages");
        BaseCompletion.PackageCompletionRequest packageRequest = this.getPackageRequest(request);
        if (request.isBehindDot() && packageRequest.basePackage.length() <= 0) {
            return false;
        }
        LOG.log(Level.FINEST, "Token fullString = >{0}<", packageRequest.fullString);
        ClasspathInfo pathInfo = this.getClasspathInfoFromRequest(request);
        assert (pathInfo != null);
        if (request.context.before1 != null && CharSequenceUtilities.textEquals((CharSequence)request.context.before1.text(), (CharSequence)"*") && request.isBehindImportStatement()) {
            return false;
        }
        Set pkgSet = pathInfo.getClassIndex().getPackageNames(packageRequest.fullString, true, EnumSet.allOf(ClassIndex.SearchScope.class));
        Iterator i$ = pkgSet.iterator();
        while (i$.hasNext()) {
            String singlePackage = (String)i$.next();
            LOG.log(Level.FINEST, "PKG set item: {0}", singlePackage);
            if (packageRequest.prefix.equals("")) {
                singlePackage = singlePackage.substring(packageRequest.fullString.length());
            } else if (!packageRequest.basePackage.equals("")) {
                singlePackage = singlePackage.substring(packageRequest.basePackage.length() + 1);
            }
            if (!singlePackage.startsWith(packageRequest.prefix) || singlePackage.length() <= 0) continue;
            CompletionItem.PackageItem item = new CompletionItem.PackageItem(singlePackage, anchor, request.getParserResult());
            if (request.isBehindImportStatement()) {
                item.setSmart(true);
            }
            proposals.add((CompletionProposal)item);
        }
        return false;
    }
}

