/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.elements.ast;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.openide.filesystems.FileObject;

public class ASTRoot
extends ASTElement {
    private final FileObject fileObject;
    private final ModuleNode moduleNode;

    public ASTRoot(FileObject fo, ModuleNode moduleNode) {
        super((ASTNode)moduleNode);
        this.fileObject = fo;
        this.moduleNode = moduleNode;
    }

    @Override
    public String getName() {
        return this.fileObject.getNameExt();
    }

    public ModuleNode getModuleNode() {
        return this.moduleNode;
    }

    @Override
    public ElementKind getKind() {
        return ElementKind.MODULE;
    }
}

