/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.api.completion;

public enum CaretLocation {
    ABOVE_PACKAGE("ABOVE_PACKAGE"),
    ABOVE_FIRST_CLASS("ABOVE_FIRST_CLASS"),
    OUTSIDE_CLASSES("OUTSIDE_CLASSES"),
    INSIDE_CLASS("INSIDE_CLASS"),
    INSIDE_METHOD("INSIDE_METHOD"),
    INSIDE_CLOSURE("INSIDE_CLOSURE"),
    INSIDE_CONSTRUCTOR_CALL(""),
    INSIDE_PARAMETERS("INSIDE_PARAMETERS"),
    INSIDE_COMMENT("INSIDE_COMMENT"),
    INSIDE_STRING("INSIDE_STRING"),
    UNDEFINED("UNDEFINED");
    
    private String id;

    private CaretLocation(String id) {
        this.id = id;
    }
}

