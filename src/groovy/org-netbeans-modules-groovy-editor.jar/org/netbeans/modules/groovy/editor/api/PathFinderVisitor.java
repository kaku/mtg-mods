/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.AnnotatedNode
 *  org.codehaus.groovy.ast.ClassCodeVisitorSupport
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.GroovyCodeVisitor
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.expr.ArgumentListExpression
 *  org.codehaus.groovy.ast.expr.ArrayExpression
 *  org.codehaus.groovy.ast.expr.AttributeExpression
 *  org.codehaus.groovy.ast.expr.BinaryExpression
 *  org.codehaus.groovy.ast.expr.BitwiseNegationExpression
 *  org.codehaus.groovy.ast.expr.BooleanExpression
 *  org.codehaus.groovy.ast.expr.CastExpression
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ClosureListExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.ElvisOperatorExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.FieldExpression
 *  org.codehaus.groovy.ast.expr.GStringExpression
 *  org.codehaus.groovy.ast.expr.ListExpression
 *  org.codehaus.groovy.ast.expr.MapEntryExpression
 *  org.codehaus.groovy.ast.expr.MapExpression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.MethodPointerExpression
 *  org.codehaus.groovy.ast.expr.NotExpression
 *  org.codehaus.groovy.ast.expr.PostfixExpression
 *  org.codehaus.groovy.ast.expr.PrefixExpression
 *  org.codehaus.groovy.ast.expr.PropertyExpression
 *  org.codehaus.groovy.ast.expr.RangeExpression
 *  org.codehaus.groovy.ast.expr.SpreadExpression
 *  org.codehaus.groovy.ast.expr.SpreadMapExpression
 *  org.codehaus.groovy.ast.expr.StaticMethodCallExpression
 *  org.codehaus.groovy.ast.expr.TernaryExpression
 *  org.codehaus.groovy.ast.expr.TupleExpression
 *  org.codehaus.groovy.ast.expr.UnaryMinusExpression
 *  org.codehaus.groovy.ast.expr.UnaryPlusExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.AssertStatement
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.BreakStatement
 *  org.codehaus.groovy.ast.stmt.CaseStatement
 *  org.codehaus.groovy.ast.stmt.CatchStatement
 *  org.codehaus.groovy.ast.stmt.ContinueStatement
 *  org.codehaus.groovy.ast.stmt.DoWhileStatement
 *  org.codehaus.groovy.ast.stmt.ExpressionStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.codehaus.groovy.ast.stmt.IfStatement
 *  org.codehaus.groovy.ast.stmt.ReturnStatement
 *  org.codehaus.groovy.ast.stmt.Statement
 *  org.codehaus.groovy.ast.stmt.SwitchStatement
 *  org.codehaus.groovy.ast.stmt.SynchronizedStatement
 *  org.codehaus.groovy.ast.stmt.ThrowStatement
 *  org.codehaus.groovy.ast.stmt.TryCatchStatement
 *  org.codehaus.groovy.ast.stmt.WhileStatement
 *  org.codehaus.groovy.control.SourceUnit
 */
package org.netbeans.modules.groovy.editor.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotatedNode;
import org.codehaus.groovy.ast.ClassCodeVisitorSupport;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.GroovyCodeVisitor;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ArrayExpression;
import org.codehaus.groovy.ast.expr.AttributeExpression;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.BitwiseNegationExpression;
import org.codehaus.groovy.ast.expr.BooleanExpression;
import org.codehaus.groovy.ast.expr.CastExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ClosureListExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.ElvisOperatorExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.FieldExpression;
import org.codehaus.groovy.ast.expr.GStringExpression;
import org.codehaus.groovy.ast.expr.ListExpression;
import org.codehaus.groovy.ast.expr.MapEntryExpression;
import org.codehaus.groovy.ast.expr.MapExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.MethodPointerExpression;
import org.codehaus.groovy.ast.expr.NotExpression;
import org.codehaus.groovy.ast.expr.PostfixExpression;
import org.codehaus.groovy.ast.expr.PrefixExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.RangeExpression;
import org.codehaus.groovy.ast.expr.SpreadExpression;
import org.codehaus.groovy.ast.expr.SpreadMapExpression;
import org.codehaus.groovy.ast.expr.StaticMethodCallExpression;
import org.codehaus.groovy.ast.expr.TernaryExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.expr.UnaryMinusExpression;
import org.codehaus.groovy.ast.expr.UnaryPlusExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.AssertStatement;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.BreakStatement;
import org.codehaus.groovy.ast.stmt.CaseStatement;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ContinueStatement;
import org.codehaus.groovy.ast.stmt.DoWhileStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.ast.stmt.IfStatement;
import org.codehaus.groovy.ast.stmt.ReturnStatement;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.ast.stmt.SwitchStatement;
import org.codehaus.groovy.ast.stmt.SynchronizedStatement;
import org.codehaus.groovy.ast.stmt.ThrowStatement;
import org.codehaus.groovy.ast.stmt.TryCatchStatement;
import org.codehaus.groovy.ast.stmt.WhileStatement;
import org.codehaus.groovy.control.SourceUnit;

public class PathFinderVisitor
extends ClassCodeVisitorSupport {
    private static final Logger LOG = Logger.getLogger(PathFinderVisitor.class.getName());
    private final SourceUnit sourceUnit;
    private final int line;
    private final int column;
    private final List<ASTNode> path = new ArrayList<ASTNode>();

    public PathFinderVisitor(SourceUnit sourceUnit, int line, int column) {
        this.sourceUnit = sourceUnit;
        this.line = line;
        this.column = column;
    }

    public List<ASTNode> getPath() {
        return new ArrayList<ASTNode>(this.path);
    }

    protected SourceUnit getSourceUnit() {
        return this.sourceUnit;
    }

    protected void visitConstructorOrMethod(MethodNode node, boolean isConstructor) {
        super.visitConstructorOrMethod(node, isConstructor);
        for (Parameter parameter : node.getParameters()) {
            this.isInside((ASTNode)parameter, this.line, this.column);
        }
    }

    protected void visitStatement(Statement statement) {
    }

    public void visitBlockStatement(BlockStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column, false)) {
            this.path.add((ASTNode)node);
        } else {
            for (Object object : node.getStatements()) {
                if (!this.isInside((ASTNode)object, this.line, this.column, false)) continue;
                this.path.add((ASTNode)node);
                break;
            }
        }
        for (Object object : node.getStatements()) {
            Statement statement = (Statement)object;
            statement.visit((GroovyCodeVisitor)this);
        }
    }

    public void visitForLoop(ForStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitForLoop(node);
        }
    }

    public void visitWhileLoop(WhileStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitWhileLoop(node);
        }
    }

    public void visitDoWhileLoop(DoWhileStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitDoWhileLoop(node);
        }
    }

    public void visitIfElse(IfStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitIfElse(node);
        }
    }

    public void visitExpressionStatement(ExpressionStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitExpressionStatement(node);
        }
    }

    public void visitReturnStatement(ReturnStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitReturnStatement(node);
        }
    }

    public void visitAssertStatement(AssertStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitAssertStatement(node);
        }
    }

    public void visitTryCatchFinally(TryCatchStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitTryCatchFinally(node);
        }
    }

    public void visitSwitch(SwitchStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitSwitch(node);
        }
    }

    public void visitCaseStatement(CaseStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitCaseStatement(node);
        }
    }

    public void visitBreakStatement(BreakStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitBreakStatement(node);
        }
    }

    public void visitContinueStatement(ContinueStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitContinueStatement(node);
        }
    }

    public void visitThrowStatement(ThrowStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitThrowStatement(node);
        }
    }

    public void visitSynchronizedStatement(SynchronizedStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitSynchronizedStatement(node);
        }
    }

    public void visitCatchStatement(CatchStatement node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitCatchStatement(node);
        }
    }

    public void visitMethodCallExpression(MethodCallExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            if (node.isImplicitThis()) {
                node.getMethod().visit((GroovyCodeVisitor)this);
                node.getArguments().visit((GroovyCodeVisitor)this);
            } else {
                super.visitMethodCallExpression(node);
            }
        }
    }

    public void visitStaticMethodCallExpression(StaticMethodCallExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitStaticMethodCallExpression(node);
        }
    }

    public void visitConstructorCallExpression(ConstructorCallExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitConstructorCallExpression(node);
        }
    }

    public void visitTernaryExpression(TernaryExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitTernaryExpression(node);
        }
    }

    public void visitShortTernaryExpression(ElvisOperatorExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitShortTernaryExpression(node);
        }
    }

    public void visitBinaryExpression(BinaryExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitBinaryExpression(node);
        }
    }

    public void visitPrefixExpression(PrefixExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitPrefixExpression(node);
        }
    }

    public void visitPostfixExpression(PostfixExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitPostfixExpression(node);
        }
    }

    public void visitBooleanExpression(BooleanExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitBooleanExpression(node);
        }
    }

    public void visitClosureExpression(ClosureExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitClosureExpression(node);
            if (node.isParameterSpecified()) {
                for (Parameter parameter : node.getParameters()) {
                    this.isInside((ASTNode)parameter, this.line, this.column);
                }
            }
        }
    }

    public void visitTupleExpression(TupleExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitTupleExpression(node);
        }
    }

    public void visitMapExpression(MapExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitMapExpression(node);
        }
    }

    public void visitMapEntryExpression(MapEntryExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitMapEntryExpression(node);
        }
    }

    public void visitListExpression(ListExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitListExpression(node);
        }
    }

    public void visitRangeExpression(RangeExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitRangeExpression(node);
        }
    }

    public void visitPropertyExpression(PropertyExpression node) {
        Expression objectExpression = node.getObjectExpression();
        Expression property = node.getProperty();
        if (this.isInside((ASTNode)node, this.line, this.column, false)) {
            this.path.add((ASTNode)node);
        } else {
            boolean nodeAdded = false;
            if (this.isInside((ASTNode)objectExpression, this.line, this.column, false)) {
                this.path.add((ASTNode)node);
                nodeAdded = true;
            }
            if (this.isInside((ASTNode)property, this.line, this.column, false) && !nodeAdded) {
                this.path.add((ASTNode)node);
            }
        }
        objectExpression.visit((GroovyCodeVisitor)this);
        property.visit((GroovyCodeVisitor)this);
    }

    public void visitAttributeExpression(AttributeExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitAttributeExpression(node);
        }
    }

    public void visitFieldExpression(FieldExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitFieldExpression(node);
        }
    }

    public void visitMethodPointerExpression(MethodPointerExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitMethodPointerExpression(node);
        }
    }

    public void visitConstantExpression(ConstantExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitConstantExpression(node);
        }
    }

    public void visitClassExpression(ClassExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitClassExpression(node);
        }
    }

    public void visitVariableExpression(VariableExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitVariableExpression(node);
        }
    }

    public void visitDeclarationExpression(DeclarationExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitDeclarationExpression(node);
        }
    }

    public void visitGStringExpression(GStringExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitGStringExpression(node);
        }
    }

    public void visitArrayExpression(ArrayExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitArrayExpression(node);
        }
    }

    public void visitSpreadExpression(SpreadExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitSpreadExpression(node);
        }
    }

    public void visitSpreadMapExpression(SpreadMapExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitSpreadMapExpression(node);
        }
    }

    public void visitNotExpression(NotExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitNotExpression(node);
        }
    }

    public void visitUnaryMinusExpression(UnaryMinusExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitUnaryMinusExpression(node);
        }
    }

    public void visitUnaryPlusExpression(UnaryPlusExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitUnaryPlusExpression(node);
        }
    }

    public void visitBitwiseNegationExpression(BitwiseNegationExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitBitwiseNegationExpression(node);
        }
    }

    public void visitCastExpression(CastExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitCastExpression(node);
        }
    }

    public void visitArgumentlistExpression(ArgumentListExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitArgumentlistExpression(node);
        }
    }

    public void visitClosureListExpression(ClosureListExpression node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitClosureListExpression(node);
        }
    }

    public void visitClass(ClassNode node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitClass(node);
        }
    }

    public void visitConstructor(ConstructorNode node) {
        if (!node.isSynthetic() && this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitConstructor(node);
        }
    }

    public void visitMethod(MethodNode node) {
        if (this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitMethod(node);
        }
    }

    public void visitField(FieldNode node) {
        if (!node.isSynthetic() && this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitField(node);
        }
    }

    public void visitProperty(PropertyNode node) {
        if (!node.isSynthetic() && this.isInside((ASTNode)node, this.line, this.column)) {
            super.visitProperty(node);
        }
    }

    private boolean isInside(ASTNode node, int line, int column) {
        return this.isInside(node, line, column, true);
    }

    private boolean isInside(ASTNode node, int line, int column, boolean addToPath) {
        if (node == null || !this.isInSource(node)) {
            return false;
        }
        this.fixNode(node);
        int beginLine = node.getLineNumber();
        int beginColumn = node.getColumnNumber();
        int endLine = node.getLastLineNumber();
        int endColumn = node.getLastColumnNumber();
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "isInside: " + (Object)node + " - " + beginLine + ", " + beginColumn + ", " + endLine + ", " + endColumn);
        }
        if (beginLine == -1 || beginColumn == -1 || endLine == -1 || endColumn == -1) {
            return addToPath;
        }
        boolean result = false;
        if (beginLine == endLine) {
            if (line == beginLine && column >= beginColumn && column < endColumn) {
                result = true;
            }
        } else if (line == beginLine) {
            if (column >= beginColumn) {
                result = true;
            }
        } else if (line == endLine) {
            if (column < endColumn) {
                result = true;
            }
        } else {
            result = beginLine < line && line < endLine;
        }
        if (result && addToPath) {
            this.path.add(node);
            LOG.log(Level.FINEST, "Path: {0}", this.path);
        }
        return addToPath ? true : result;
    }

    private void fixNode(ASTNode node) {
        if (node instanceof MethodCallExpression && !((MethodCallExpression)node).isImplicitThis()) {
            MethodCallExpression call = (MethodCallExpression)node;
            if (call.getObjectExpression() == VariableExpression.THIS_EXPRESSION || call.getObjectExpression() == VariableExpression.SUPER_EXPRESSION) {
                VariableExpression var = new VariableExpression(call.getObjectExpression() == VariableExpression.THIS_EXPRESSION ? "this" : "super", call.getObjectExpression().getType());
                var.setLineNumber(call.getLineNumber());
                var.setColumnNumber(call.getColumnNumber());
                var.setLastLineNumber(call.getMethod().getLineNumber());
                var.setLastColumnNumber(call.getMethod().getColumnNumber());
                call.setObjectExpression((Expression)var);
            }
        } else if (node instanceof MethodNode || node instanceof ClosureExpression) {
            List statements;
            BlockStatement block;
            Statement code = null;
            code = node instanceof MethodNode ? ((MethodNode)node).getCode() : ((ClosureExpression)node).getCode();
            if (code != null && code instanceof BlockStatement && (code.getLineNumber() < 0 && code.getColumnNumber() < 0 || code.getLastLineNumber() < 0 && code.getLastColumnNumber() < 0) && (statements = (block = (BlockStatement)code).getStatements()) != null && !statements.isEmpty()) {
                if (code.getLineNumber() < 0 && code.getColumnNumber() < 0) {
                    Statement first = (Statement)statements.get(0);
                    code.setLineNumber(first.getLineNumber());
                    code.setColumnNumber(first.getColumnNumber());
                }
                if (code.getLastLineNumber() < 0 && code.getLastColumnNumber() < 0) {
                    code.setLastLineNumber(node.getLastLineNumber());
                    int lastColumn = node.getLastColumnNumber();
                    if (lastColumn > 0) {
                        --lastColumn;
                    }
                    code.setLastColumnNumber(lastColumn);
                }
            }
        }
    }

    private boolean isInSource(ASTNode node) {
        StaticMethodCallExpression methodCall;
        VariableExpression var;
        Expression args;
        if (node instanceof AnnotatedNode && ((AnnotatedNode)node).hasNoRealSourcePosition()) {
            return false;
        }
        if (node instanceof StaticMethodCallExpression && node.getLineNumber() == -1 && node.getLastLineNumber() == -1 && node.getColumnNumber() == -1 && node.getLastColumnNumber() == -1 && "initMetaClass".equals((methodCall = (StaticMethodCallExpression)node).getMethod()) && (args = methodCall.getArguments()) instanceof VariableExpression && "this".equals((var = (VariableExpression)args).getName())) {
            return false;
        }
        return true;
    }
}

