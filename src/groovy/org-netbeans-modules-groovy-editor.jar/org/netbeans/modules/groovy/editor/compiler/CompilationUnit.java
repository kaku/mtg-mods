/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.GroovyClassLoader
 *  org.codehaus.groovy.ast.ClassHelper
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.CompileUnit
 *  org.codehaus.groovy.ast.GenericsType
 *  org.codehaus.groovy.ast.MixinNode
 *  org.codehaus.groovy.control.CompilationUnit
 *  org.codehaus.groovy.control.CompilerConfiguration
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.Task
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.compiler;

import groovy.lang.GroovyClassLoader;
import java.io.IOException;
import java.security.CodeSource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.CancellationException;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import org.codehaus.groovy.ast.ClassHelper;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.GenericsType;
import org.codehaus.groovy.ast.MixinNode;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParser;
import org.netbeans.modules.groovy.editor.compiler.ClassNodeCache;
import org.netbeans.modules.groovy.editor.java.ElementSearch;
import org.netbeans.modules.groovy.editor.java.Utilities;
import org.openide.util.Exceptions;

public final class CompilationUnit
extends org.codehaus.groovy.control.CompilationUnit {
    public CompilationUnit(GroovyParser parser, CompilerConfiguration configuration, CodeSource security, @NonNull GroovyClassLoader loader, @NonNull GroovyClassLoader transformationLoader, @NonNull ClasspathInfo cpInfo, @NonNull ClassNodeCache classNodeCache) {
        super(configuration, security, loader, transformationLoader);
        this.ast = new CompileUnit(parser, this.classLoader, security, this.configuration, cpInfo, classNodeCache);
    }

    private static class CompileUnit
    extends org.codehaus.groovy.ast.CompileUnit {
        private final ClassNodeCache cache;
        private final GroovyParser parser;
        private final JavaSource javaSource;

        public CompileUnit(GroovyParser parser, GroovyClassLoader classLoader, CodeSource codeSource, CompilerConfiguration config, ClasspathInfo cpInfo, ClassNodeCache classNodeCache) {
            super(classLoader, codeSource, config);
            this.parser = parser;
            this.cache = classNodeCache;
            this.javaSource = this.cache.createResolver(cpInfo);
        }

        public ClassNode getClass(final String name) {
            if (this.parser.isCancelled()) {
                throw new CancellationException();
            }
            ClassNode classNode = this.cache.get(name);
            if (classNode != null) {
                return classNode;
            }
            classNode = super.getClass(name);
            if (classNode != null) {
                return classNode;
            }
            if (this.cache.isNonExistent(name)) {
                return null;
            }
            try {
                final ClassNode[] holder = new ClassNode[1];
                Task<CompilationController> task = new Task<CompilationController>(){

                    public void run(CompilationController controller) throws Exception {
                        Elements elements = controller.getElements();
                        TypeElement typeElement = ElementSearch.getClass(elements, name);
                        if (typeElement != null) {
                            ClassNode node = CompileUnit.this.createClassNode(name, typeElement);
                            if (node != null) {
                                CompileUnit.this.cache.put(name, node);
                            }
                            holder[0] = node;
                        } else {
                            CompileUnit.this.cache.put(name, null);
                        }
                    }
                };
                this.javaSource.runUserActionTask((Task)task, true);
                return holder[0];
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return null;
            }
        }

        private ClassNode createClassNode(String name, TypeElement typeElement) {
            ElementKind kind = typeElement.getKind();
            if (kind == ElementKind.ANNOTATION_TYPE) {
                return this.createAnnotationType(name, typeElement);
            }
            if (kind == ElementKind.INTERFACE) {
                return this.createInterfaceKind(name, typeElement);
            }
            return this.createClassType(name, typeElement);
        }

        private ClassNode createAnnotationType(String name, TypeElement typeElement) {
            return new ClassNode(name, 8192, ClassHelper.Annotation_TYPE, null, MixinNode.EMPTY_ARRAY);
        }

        private ClassNode createInterfaceKind(String name, TypeElement typeElement) {
            int modifiers = 0;
            HashSet<ClassNode> interfaces = new HashSet<ClassNode>();
            HashSet<GenericsType> generics = new HashSet<GenericsType>();
            for (TypeParameterElement typeParameter : typeElement.getTypeParameters()) {
                List<? extends TypeMirror> bounds = typeParameter.getBounds();
                for (TypeMirror bound : bounds) {
                    ClassNode typeParam = this.getClass(bound.toString());
                    generics.add(new GenericsType(typeParam));
                }
            }
            modifiers |= 512;
            for (TypeMirror interfaceType : typeElement.getInterfaces()) {
                interfaces.add(new ClassNode(Utilities.getClassName(interfaceType).toString(), 512, null));
            }
            return this.createClassNode(name, modifiers, null, interfaces.toArray((T[])new ClassNode[interfaces.size()]), generics);
        }

        private ClassNode createClassType(String name, TypeElement typeElement) {
            Stack<DeclaredType> supers = new Stack<DeclaredType>();
            HashSet<GenericsType> generics = new HashSet<GenericsType>();
            while (typeElement != null && typeElement.asType().getKind() != TypeKind.NONE) {
                for (TypeParameterElement typeParameter : typeElement.getTypeParameters()) {
                    List<? extends TypeMirror> bounds = typeParameter.getBounds();
                    block2 : for (TypeMirror bound : bounds) {
                        ClassNode typeParam = this.getClass(bound.toString());
                        for (GenericsType generic : generics) {
                            if (!generic.getType().equals((Object)typeParam)) continue;
                            continue block2;
                        }
                        generics.add(new GenericsType(typeParam));
                    }
                }
                TypeMirror type = typeElement.getSuperclass();
                if (type.getKind() != TypeKind.DECLARED) break;
                DeclaredType superType = (DeclaredType)typeElement.getSuperclass();
                supers.push(superType);
                Element element = superType.asElement();
                if ((element.getKind() == ElementKind.CLASS || element.getKind() == ElementKind.ENUM) && element instanceof TypeElement) {
                    typeElement = (TypeElement)element;
                    continue;
                }
                typeElement = null;
            }
            ClassNode superClass = null;
            while (!supers.empty()) {
                superClass = this.createClassNode(Utilities.getClassName((TypeMirror)supers.pop()).toString(), 0, superClass, new ClassNode[0], generics);
            }
            return this.createClassNode(name, 0, superClass, new ClassNode[0], generics);
        }

        private ClassNode createClassNode(String name, int modifiers, ClassNode superClass, ClassNode[] interfaces, Set<GenericsType> generics) {
            if ("java.lang.Object".equals(name) && superClass == null) {
                return ClassHelper.OBJECT_TYPE;
            }
            ClassNode classNode = new ClassNode(name, modifiers, superClass, interfaces, MixinNode.EMPTY_ARRAY);
            if (generics != null) {
                classNode.setGenericsTypes(generics.toArray((T[])new GenericsType[generics.size()]));
            }
            return classNode;
        }

    }

}

