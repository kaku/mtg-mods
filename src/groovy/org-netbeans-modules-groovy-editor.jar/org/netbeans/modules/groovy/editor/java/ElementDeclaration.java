/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.ClassTree
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.MethodTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.tree.VariableTree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.TreePath
 *  com.sun.source.util.TreePathScanner
 *  com.sun.source.util.Trees
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.modules.csl.api.DeclarationFinder
 *  org.netbeans.modules.csl.api.DeclarationFinder$DeclarationLocation
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.java;

import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreePath;
import com.sun.source.util.TreePathScanner;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public final class ElementDeclaration {
    private static Logger log = Logger.getLogger(ElementDeclaration.class.getName());

    private ElementDeclaration() {
    }

    public static DeclarationFinder.DeclarationLocation getDeclarationLocation(ClasspathInfo cpInfo, Element el) {
        ElementHandle handle = ElementHandle.create((Element)el);
        FileObject fo = SourceUtils.getFile((ElementHandle)handle, (ClasspathInfo)cpInfo);
        if (fo != null) {
            return ElementDeclaration.getDeclarationLocation(fo, handle);
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private static DeclarationFinder.DeclarationLocation getDeclarationLocation(FileObject fo, ElementHandle<? extends Element> handle) {
        assert (fo != null);
        try {
            int offset = ElementDeclaration.getOffset(fo, handle);
            return new DeclarationFinder.DeclarationLocation(fo, offset);
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
            return DeclarationFinder.DeclarationLocation.NONE;
        }
    }

    private static int getOffset(FileObject fo, final ElementHandle<? extends Element> handle) throws IOException {
        if (IndexingManager.getDefault().isIndexing()) {
            log.info("Skipping location of element offset within file, Scannig in progress");
            return 0;
        }
        final int[] result = new int[]{-1};
        JavaSource js = JavaSource.forFileObject((FileObject)fo);
        if (js != null) {
            js.runUserActionTask((Task)new Task<CompilationController>(){

                public void run(CompilationController info) {
                    try {
                        info.toPhase(JavaSource.Phase.RESOLVED);
                    }
                    catch (IOException ioe) {
                        Exceptions.printStackTrace((Throwable)ioe);
                    }
                    Element el = handle.resolve((CompilationInfo)info);
                    if (el == null) {
                        log.severe("Cannot resolve " + (Object)handle + ". " + (Object)info.getClasspathInfo());
                        return;
                    }
                    FindDeclarationVisitor v = new FindDeclarationVisitor(el, (CompilationInfo)info);
                    CompilationUnitTree cu = info.getCompilationUnit();
                    v.scan((Tree)cu, (Object)null);
                    Tree elTree = v.declTree;
                    if (elTree != null) {
                        result[0] = (int)info.getTrees().getSourcePositions().getStartPosition(cu, elTree);
                    }
                }
            }, true);
        }
        return result[0];
    }

    private static class FindDeclarationVisitor
    extends TreePathScanner<Void, Void> {
        private Element element;
        private Tree declTree;
        private CompilationInfo info;

        public FindDeclarationVisitor(Element element, CompilationInfo info) {
            this.element = element;
            this.info = info;
        }

        public Void visitClass(ClassTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitClass(tree, (Object)d);
            return null;
        }

        public Void visitMethod(MethodTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitMethod(tree, (Object)d);
            return null;
        }

        public Void visitVariable(VariableTree tree, Void d) {
            this.handleDeclaration();
            TreePathScanner.super.visitVariable(tree, (Object)d);
            return null;
        }

        public void handleDeclaration() {
            Element found = this.info.getTrees().getElement(this.getCurrentPath());
            if (this.element.equals(found)) {
                this.declTree = this.getCurrentPath().getLeaf();
            }
        }
    }

}

