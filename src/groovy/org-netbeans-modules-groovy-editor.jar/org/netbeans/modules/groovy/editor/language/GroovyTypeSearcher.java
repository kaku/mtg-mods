/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.IndexSearcher
 *  org.netbeans.modules.csl.api.IndexSearcher$Descriptor
 *  org.netbeans.modules.csl.api.IndexSearcher$Helper
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.groovy.editor.language;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

public class GroovyTypeSearcher
implements IndexSearcher {
    private static final Logger LOGGER = Logger.getLogger(GroovyTypeSearcher.class.getName());
    private static Pattern camelCasePattern = Pattern.compile("(?:\\p{javaUpperCase}(?:\\p{javaLowerCase}|\\p{Digit}|\\:|\\.|\\$)*){2,}");
    private QuerySupport.Kind cachedKind;
    private String cachedString = "/";

    public Set<? extends IndexSearcher.Descriptor> getSymbols(Project project, String textForQuery, QuerySupport.Kind kind, IndexSearcher.Helper helper) {
        return this.getTypes(project, textForQuery, kind, helper);
    }

    public Set<? extends IndexSearcher.Descriptor> getTypes(Project project, String textForQuery, QuerySupport.Kind kind, IndexSearcher.Helper helper) {
        GroovyIndex index = GroovyIndex.get(QuerySupport.findRoots((Project)project, Collections.singleton("classpath/source"), Collections.emptySet(), Collections.emptySet()));
        if ((kind = this.adjustKind(kind, textForQuery)) == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX) {
            textForQuery = textForQuery.toLowerCase(Locale.ENGLISH);
        }
        Set<IndexedClass> classes = null;
        if (textForQuery.length() <= 0) {
            return Collections.emptySet();
        }
        classes = index.getClasses(textForQuery, kind);
        HashSet<GroovyTypeDescriptor> result = new HashSet<GroovyTypeDescriptor>();
        for (IndexedClass cls : classes) {
            result.add(new GroovyTypeDescriptor(cls, helper));
        }
        return result;
    }

    private static boolean isAllUpper(String text) {
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (Character.isUpperCase(c) || c == ':') continue;
            return false;
        }
        return true;
    }

    private static boolean isCamelCase(String text) {
        return camelCasePattern.matcher(text).matches();
    }

    private QuerySupport.Kind adjustKind(QuerySupport.Kind kind, String text) {
        if (text.equals(this.cachedString)) {
            return this.cachedKind;
        }
        if (kind == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX && (GroovyTypeSearcher.isAllUpper(text) && text.length() > 1 || GroovyTypeSearcher.isCamelCase(text))) {
            kind = QuerySupport.Kind.CAMEL_CASE;
        }
        this.cachedString = text;
        this.cachedKind = kind;
        return kind;
    }

    private class GroovyTypeDescriptor
    extends IndexSearcher.Descriptor {
        private final IndexedElement element;
        private String projectName;
        private Icon projectIcon;
        private final IndexSearcher.Helper helper;
        private boolean isLibrary;

        public GroovyTypeDescriptor(IndexedElement element, IndexSearcher.Helper helper) {
            this.element = element;
            this.helper = helper;
        }

        public Icon getIcon() {
            if (this.projectName == null) {
                this.initProjectInfo();
            }
            return this.helper.getIcon((ElementHandle)this.element);
        }

        public String getTypeName() {
            return this.element.getName();
        }

        public String getProjectName() {
            if (this.projectName == null) {
                this.initProjectInfo();
            }
            return this.projectName;
        }

        private void initProjectInfo() {
            FileObject fo = this.element.getFileObject();
            if (fo != null) {
                Project p = FileOwnerQuery.getOwner((FileObject)fo);
                if (p != null) {
                    ProjectInformation pi = ProjectUtils.getInformation((Project)p);
                    this.projectName = pi.getDisplayName();
                    this.projectIcon = pi.getIcon();
                }
            } else {
                this.isLibrary = true;
                LOGGER.log(Level.FINE, "No fileobject for {0}", this.element.toString());
            }
            if (this.projectName == null) {
                this.projectName = "";
            }
        }

        public Icon getProjectIcon() {
            if (this.projectName == null) {
                this.initProjectInfo();
            }
            if (this.isLibrary) {
                // empty if block
            }
            return this.projectIcon;
        }

        public FileObject getFileObject() {
            return this.element.getFileObject();
        }

        public void open() {
            FileObject fileObject = this.element.getFileObject();
            if (fileObject == null) {
                NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)NbBundle.getMessage(GroovyTypeSearcher.class, (String)"FileDeleted"), 0);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
            } else {
                GsfUtilities.open((FileObject)fileObject, (int)0, (String)this.element.getName());
            }
        }

        public String getContextName() {
            String fqn;
            StringBuilder sb = new StringBuilder();
            String string = fqn = this.element.getIn() != null ? this.element.getIn() + "." + this.element.getName() : this.element.getName();
            if (this.element.getName().equals(fqn)) {
                fqn = null;
            }
            if (fqn != null) {
                if (fqn != null) {
                    sb.append(fqn);
                }
                return sb.toString();
            }
            return null;
        }

        public ElementHandle getElement() {
            return this.element;
        }

        public int getOffset() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getSimpleName() {
            return this.element.getName();
        }

        public String getOuterName() {
            return null;
        }
    }

}

