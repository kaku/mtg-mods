/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.EditList
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintFix
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.hints;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.text.JTextComponent;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.EditList;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.hints.Bundle;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovySelectionRule;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class SurroundWithHint
extends GroovySelectionRule {
    public static final Logger LOG = Logger.getLogger(SurroundWithHint.class.getName());

    @Override
    public void run(RuleContext context, List<Hint> result) {
        ParserResult info = context.parserResult;
        int start = context.selectionStart;
        int end = context.selectionEnd;
        assert (start < end);
        BaseDocument baseDoc = context.doc;
        if (end > baseDoc.getLength()) {
            return;
        }
        if (end - start > 1000) {
            return;
        }
        ModuleNode root = ASTUtils.getRoot(info);
        if (root == null) {
            return;
        }
        OffsetRange range = new OffsetRange(start, end);
        result.add(this.getDescriptor(Operation.COMMENT_OUT, Bundle.CommentOutRuleHintDescription(), context, baseDoc, range));
        result.add(this.getDescriptor(Operation.ADD_IF, Bundle.AddIfAroundBlockHintDescription(), context, baseDoc, range));
    }

    private Hint getDescriptor(Operation operation, String description, RuleContext context, BaseDocument baseDoc, OffsetRange range) {
        int DEFAULT_PRIORITY = 292;
        SimpleFix fixToApply = new SimpleFix(operation, description, baseDoc, context);
        ArrayList<SimpleFix> fixList = new ArrayList<SimpleFix>(1);
        fixList.add(fixToApply);
        Hint descriptor = new Hint((Rule)this, fixToApply.getDescription(), context.parserResult.getSnapshot().getSource().getFileObject(), range, fixList, DEFAULT_PRIORITY);
        return descriptor;
    }

    public boolean appliesTo(RuleContext context) {
        return true;
    }

    public String getDisplayName() {
        return Bundle.CommentOutRuleDescription();
    }

    public boolean showInTasklist() {
        return false;
    }

    public HintSeverity getDefaultSeverity() {
        return HintSeverity.CURRENT_LINE_WARNING;
    }

    private static class SimpleFix
    implements HintFix {
        final BaseDocument baseDoc;
        final String desc;
        final RuleContext context;
        final Operation operation;

        public SimpleFix(Operation operation, String desc, BaseDocument baseDoc, RuleContext context) {
            this.desc = desc;
            this.baseDoc = baseDoc;
            this.context = context;
            this.operation = operation;
        }

        public String getDescription() {
            return this.desc;
        }

        public void implement() throws Exception {
            EditList edits = new EditList(this.baseDoc);
            int start = this.context.selectionStart;
            int end = this.context.selectionEnd;
            JTextComponent component = Utilities.getFocusedComponent();
            switch (this.operation) {
                case COMMENT_OUT: {
                    edits.replace(end, 0, "*/", false, 0);
                    edits.replace(start, 0, "/*", false, 1);
                    edits.apply();
                    component.setCaretPosition(start);
                    break;
                }
                case ADD_IF: {
                    String START_INSERT = "if (true) {\n";
                    String END_INSERT = "\n}";
                    edits.replace(end, 0, END_INSERT, false, 0);
                    int startOfRow = Utilities.getRowStart((BaseDocument)this.baseDoc, (int)start);
                    edits.replace(startOfRow, 0, START_INSERT, false, 1);
                    edits.setFormatAll(true);
                    edits.apply();
                    component.setCaretPosition(start + 4);
                    component.moveCaretPosition(start + 8);
                }
            }
        }

        public boolean isSafe() {
            return false;
        }

        public boolean isInteractive() {
            return false;
        }
    }

    private static enum Operation {
        COMMENT_OUT,
        ADD_IF;
        

        private Operation() {
        }
    }

}

