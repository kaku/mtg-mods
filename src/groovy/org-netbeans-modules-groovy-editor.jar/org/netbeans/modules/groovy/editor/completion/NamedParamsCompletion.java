/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.MapEntryExpression
 *  org.codehaus.groovy.ast.expr.NamedArgumentListExpression
 *  org.codehaus.groovy.ast.expr.TupleExpression
 *  org.netbeans.modules.csl.api.CompletionProposal
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.List;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MapEntryExpression;
import org.codehaus.groovy.ast.expr.NamedArgumentListExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;

public class NamedParamsCompletion
extends BaseCompletion {
    private CompletionContext context;

    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext context, int anchor) {
        ConstructorCallExpression constructorCall;
        Expression constructorArgs;
        this.context = context;
        ASTNode leaf = context.path.leaf();
        if (leaf instanceof ConstructorCallExpression && (constructorArgs = (constructorCall = (ConstructorCallExpression)leaf).getArguments()) instanceof TupleExpression) {
            List arguments = ((TupleExpression)constructorArgs).getExpressions();
            if (arguments.isEmpty()) {
                this.completeNamedParams(proposals, anchor, constructorCall, null);
            } else {
                for (Expression argExpression : arguments) {
                    if (!(argExpression instanceof NamedArgumentListExpression)) continue;
                    this.completeNamedParams(proposals, anchor, constructorCall, (NamedArgumentListExpression)argExpression);
                }
            }
        }
        ASTNode leafParent = context.path.leafParent();
        ASTNode leafGrandparent = context.path.leafGrandParent();
        if (leafParent instanceof NamedArgumentListExpression && leafGrandparent instanceof ConstructorCallExpression) {
            this.completeNamedParams(proposals, anchor, (ConstructorCallExpression)leafGrandparent, (NamedArgumentListExpression)leafParent);
        }
        return false;
    }

    private void completeNamedParams(List<CompletionProposal> proposals, int anchor, ConstructorCallExpression constructorCall, NamedArgumentListExpression namedArguments) {
        ClassNode type = constructorCall.getType();
        String prefix = this.context.getPrefix();
        for (FieldNode fieldNode : type.getFields()) {
            if (fieldNode.getLineNumber() < 0 || fieldNode.getColumnNumber() < 0) continue;
            String typeName = fieldNode.getType().getNameWithoutPackage();
            String name = fieldNode.getName();
            if ("".equals(prefix) ? this.isAlreadyPresent(namedArguments, name) : name.equals(prefix) || !name.startsWith(prefix)) continue;
            proposals.add((CompletionProposal)new CompletionItem.NamedParameter(typeName, name, anchor));
        }
    }

    private boolean isAlreadyPresent(NamedArgumentListExpression namedArgsExpression, String name) {
        if (namedArgsExpression == null) {
            return false;
        }
        List namedArgs = namedArgsExpression.getMapEntryExpressions();
        for (MapEntryExpression namedEntry : namedArgs) {
            String namedArgument = namedEntry.getKeyExpression().getText();
            if (namedArgument == null || !namedArgument.equals(name)) continue;
            return true;
        }
        return false;
    }

    private boolean isParameterPrefix(NamedArgumentListExpression namedArgsExpression, String name) {
        List namedArgs = namedArgsExpression.getMapEntryExpressions();
        for (MapEntryExpression namedEntry : namedArgs) {
            String namedArgument = namedEntry.getKeyExpression().getText();
            if (namedArgument == null || !name.startsWith(namedArgument)) continue;
            return true;
        }
        return false;
    }
}

