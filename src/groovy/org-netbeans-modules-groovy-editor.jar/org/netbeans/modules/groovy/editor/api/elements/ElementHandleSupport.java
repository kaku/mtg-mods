/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.elements;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.elements.CommentElement;
import org.netbeans.modules.groovy.editor.api.elements.GroovyElement;
import org.netbeans.modules.groovy.editor.api.elements.KeywordElement;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;

public class ElementHandleSupport {
    public static ElementHandle createHandle(ParserResult info, GroovyElement object) {
        if (object instanceof KeywordElement || object instanceof CommentElement) {
            return new GroovyElementHandle(null, object, info.getSnapshot().getSource().getFileObject());
        }
        if (object instanceof IndexedElement) {
            return new GroovyElementHandle(null, object, ((IndexedElement)object).getFileObject());
        }
        if (!(object instanceof ASTElement)) {
            return null;
        }
        if (info == null) {
            return null;
        }
        GroovyParserResult result = ASTUtils.getParseResult((Parser.Result)info);
        if (result == null) {
            return null;
        }
        ModuleNode root = ASTUtils.getRoot(info);
        return new GroovyElementHandle((ASTNode)root, object, info.getSnapshot().getSource().getFileObject());
    }

    public static ElementHandle createHandle(ParserResult result, ASTElement object) {
        ModuleNode root = ASTUtils.getRoot(result);
        return new GroovyElementHandle((ASTNode)root, object, result.getSnapshot().getSource().getFileObject());
    }

    public static GroovyElement resolveHandle(ParserResult info, ElementHandle handle) {
        GroovyElementHandle h = (GroovyElementHandle)handle;
        ASTNode oldRoot = h.root;
        if (h.object instanceof KeywordElement || h.object instanceof IndexedElement || h.object instanceof CommentElement) {
            return h.object;
        }
        if (!(h.object instanceof ASTElement)) {
            return null;
        }
        ASTNode oldNode = ((ASTElement)h.object).getNode();
        ModuleNode newRoot = ASTUtils.getRoot(info);
        if (newRoot == null) {
            return null;
        }
        ASTNode newNode = ElementHandleSupport.find(oldRoot, oldNode, (ASTNode)newRoot);
        if (newNode != null) {
            ASTElement co = ASTElement.create(newNode);
            return co;
        }
        return null;
    }

    public static ElementHandle createHandle(String className, String elementName, ElementKind kind, Set<Modifier> modifiers) {
        return new SimpleElementHandle(className, elementName, kind, modifiers);
    }

    private static ASTNode find(ASTNode oldRoot, ASTNode oldObject, ASTNode newRoot) {
        List<ASTNode> oldChildren = ASTUtils.children(oldRoot);
        List<ASTNode> newChildren = ASTUtils.children(newRoot);
        Iterator<ASTNode> itOld = oldChildren.iterator();
        Iterator<ASTNode> itNew = newChildren.iterator();
        while (itOld.hasNext()) {
            if (!itNew.hasNext()) {
                return null;
            }
            ASTNode o = itOld.next();
            ASTNode n = itNew.next();
            if (o == oldObject) {
                return n;
            }
            ASTNode match = ElementHandleSupport.find(o, oldObject, n);
            if (match == null) continue;
            return match;
        }
        if (itNew.hasNext()) {
            return null;
        }
        return null;
    }

    private static class SimpleElementHandle
    implements ElementHandle {
        private final String className;
        private final String elementName;
        private final ElementKind kind;
        private final Set<Modifier> modifiers;

        public SimpleElementHandle(String className, String elementName, ElementKind kind, Set<Modifier> modifiers) {
            this.className = className;
            this.elementName = elementName;
            this.kind = kind;
            this.modifiers = modifiers;
        }

        public FileObject getFileObject() {
            return null;
        }

        public String getIn() {
            return this.className;
        }

        public ElementKind getKind() {
            return this.kind;
        }

        public String getMimeType() {
            return "text/x-groovy";
        }

        public Set<Modifier> getModifiers() {
            return this.modifiers;
        }

        public String getName() {
            return this.elementName;
        }

        public boolean signatureEquals(ElementHandle handle) {
            return false;
        }

        public OffsetRange getOffsetRange(ParserResult result) {
            return OffsetRange.NONE;
        }
    }

    private static class GroovyElementHandle
    implements ElementHandle {
        private final ASTNode root;
        private final GroovyElement object;
        private final FileObject fileObject;

        private GroovyElementHandle(ASTNode root, GroovyElement object, FileObject fileObject) {
            this.root = root;
            this.object = object;
            this.fileObject = fileObject;
        }

        public boolean signatureEquals(ElementHandle handle) {
            return false;
        }

        public FileObject getFileObject() {
            if (this.object instanceof IndexedElement) {
                return ((IndexedElement)this.object).getFileObject();
            }
            return this.fileObject;
        }

        public String getMimeType() {
            return "text/x-groovy";
        }

        public String getName() {
            return this.object.getName();
        }

        public String getIn() {
            return this.object.getIn();
        }

        public ElementKind getKind() {
            return this.object.getKind();
        }

        public Set<Modifier> getModifiers() {
            return this.object.getModifiers();
        }

        public OffsetRange getOffsetRange(ParserResult result) {
            return OffsetRange.NONE;
        }
    }

}

