/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.completion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.ContextHelper;
import org.netbeans.modules.groovy.editor.api.completion.util.DotCompletionContext;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;
import org.netbeans.modules.groovy.editor.completion.util.CamelCaseUtil;
import org.netbeans.modules.groovy.editor.imports.ImportUtils;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public class TypesCompletion
extends BaseCompletion {
    private List<CompletionProposal> proposals;
    private CompletionContext request;
    private int anchor;
    private boolean constructorCompletion;

    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext request, int anchor) {
        LOG.log(Level.FINEST, "-> completeTypes");
        this.proposals = proposals;
        this.request = request;
        this.anchor = anchor;
        if (request.dotContext != null && (request.dotContext.isFieldsOnly() || request.dotContext.isMethodsOnly())) {
            return false;
        }
        BaseCompletion.PackageCompletionRequest packageRequest = this.getPackageRequest(request);
        if (packageRequest.basePackage.length() == 0 && packageRequest.prefix.length() == 0 && packageRequest.fullString.equals(".")) {
            return false;
        }
        this.constructorCompletion = ContextHelper.isConstructorCall(request);
        boolean onlyInterfaces = false;
        Token<GroovyTokenId> literal = request.context.beforeLiteral;
        if (literal != null) {
            if (literal.id() == GroovyTokenId.LITERAL_class) {
                return false;
            }
            if (literal.id() == GroovyTokenId.LITERAL_implements) {
                LOG.log(Level.FINEST, "Completing only interfaces after implements keyword.");
                onlyInterfaces = true;
            }
        }
        HashSet<TypeHolder> addedTypes = new HashSet<TypeHolder>();
        ModuleNode moduleNode = ContextHelper.getSurroundingModuleNode(request);
        String currentPackage = this.getCurrentPackageName(moduleNode);
        JavaSource javaSource = this.getJavaSourceFromRequest();
        GroovyIndex index = null;
        FileObject fo = request.getSourceFile();
        if (fo != null) {
            index = GroovyIndex.get(QuerySupport.findRoots((FileObject)fo, Collections.singleton("classpath/source"), Collections.emptyList(), Collections.emptyList()));
        }
        if (packageRequest.basePackage.length() > 0 || request.isBehindImportStatement()) {
            List<TypeHolder> typeList = this.getTypeHoldersForPackage(javaSource, packageRequest.basePackage, currentPackage);
            LOG.log(Level.FINEST, "Number of types found:  {0}", typeList.size());
            for (TypeHolder singleType : typeList) {
                this.addToProposalUsingFilter(addedTypes, singleType, onlyInterfaces);
            }
            if (index != null) {
                Set<IndexedClass> classes = index.getClassesFromPackage(packageRequest.basePackage);
                for (IndexedClass indexedClass : classes) {
                    this.addToProposalUsingFilter(addedTypes, new TypeHolder(indexedClass), onlyInterfaces);
                }
            }
            return true;
        }
        if (request.isBehindDot()) {
            return false;
        }
        if (moduleNode != null) {
            String camelCaseFirstWord;
            Set<IndexedClass> classes;
            LOG.log(Level.FINEST, "We are living in package : {0} ", currentPackage);
            if (index != null && !(classes = index.getClasses(camelCaseFirstWord = CamelCaseUtil.getCamelCaseFirstWord(request.getPrefix()), QuerySupport.Kind.PREFIX)).isEmpty()) {
                for (IndexedClass indexedClass : classes) {
                    this.addToProposalUsingFilter(addedTypes, new TypeHolder(indexedClass), onlyInterfaces);
                }
            }
        }
        ArrayList<String> localDefaultImports = new ArrayList<String>();
        if (moduleNode != null) {
            List imports = moduleNode.getImports();
            if (imports != null) {
                for (ImportNode importNode : imports) {
                    ElementKind ek = importNode.getType().isInterface() ? ElementKind.INTERFACE : ElementKind.CLASS;
                    this.addToProposalUsingFilter(addedTypes, new TypeHolder(importNode.getClassName(), ek), onlyInterfaces);
                }
            }
            List importNodes = moduleNode.getStarImports();
            for (ImportNode wildcardImport : importNodes) {
                String packageName = wildcardImport.getPackageName();
                if (packageName.endsWith(".")) {
                    packageName = packageName.substring(0, packageName.length() - 1);
                }
                localDefaultImports.add(packageName);
            }
        }
        localDefaultImports.addAll(ImportUtils.getDefaultImportPackages());
        for (String singlePackage : localDefaultImports) {
            List<TypeHolder> typeList = this.getTypeHoldersForPackage(javaSource, singlePackage, currentPackage);
            LOG.log(Level.FINEST, "Number of types found:  {0}", typeList.size());
            for (TypeHolder element : typeList) {
                this.addToProposalUsingFilter(addedTypes, element, onlyInterfaces);
            }
        }
        for (String className : ImportUtils.getDefaultImportClasses()) {
            this.addToProposalUsingFilter(addedTypes, new TypeHolder(className, ElementKind.CLASS), onlyInterfaces);
        }
        for (ClassNode declaredClass : ContextHelper.getDeclaredClasses(request)) {
            this.addToProposalUsingFilter(addedTypes, new TypeHolder(declaredClass.getName(), ElementKind.CLASS), onlyInterfaces);
        }
        return true;
    }

    private String getCurrentPackageName(ModuleNode moduleNode) {
        if (moduleNode != null) {
            return moduleNode.getPackageName();
        }
        ClassNode node = ContextHelper.getSurroundingClassNode(this.request);
        if (node != null) {
            return node.getPackageName();
        }
        return "";
    }

    private JavaSource getJavaSourceFromRequest() {
        ClasspathInfo pathInfo = this.getClasspathInfoFromRequest(this.request);
        assert (pathInfo != null);
        JavaSource javaSource = JavaSource.create((ClasspathInfo)pathInfo, (FileObject[])new FileObject[0]);
        if (javaSource == null) {
            LOG.log(Level.FINEST, "Problem retrieving JavaSource from ClassPathInfo, exiting.");
            return null;
        }
        return javaSource;
    }

    private void addToProposalUsingFilter(Set<TypeHolder> alreadyPresent, TypeHolder type, boolean onlyInterfaces) {
        CompletionItem.TypeItem camelCaseProposal;
        if (onlyInterfaces && type.getKind() != ElementKind.INTERFACE || alreadyPresent.contains(type)) {
            return;
        }
        String fqnTypeName = type.getName();
        String typeName = GroovyUtils.stripPackage(fqnTypeName);
        if (this.constructorCompletion && typeName.toUpperCase().equals(this.request.getPrefix().toUpperCase())) {
            return;
        }
        if (this.isPrefixed(this.request, typeName)) {
            alreadyPresent.add(type);
            this.proposals.add((CompletionProposal)new CompletionItem.TypeItem(fqnTypeName, typeName, this.anchor, type.getKind()));
        }
        if (CamelCaseUtil.compareCamelCase(typeName, this.request.getPrefix()) && !this.proposals.contains((Object)(camelCaseProposal = new CompletionItem.TypeItem(fqnTypeName, typeName, this.anchor, ElementKind.CLASS)))) {
            this.proposals.add((CompletionProposal)camelCaseProposal);
        }
    }

    @NonNull
    private List<TypeHolder> getTypeHoldersForPackage(JavaSource javaSource, final String pkg, final String currentPackage) {
        LOG.log(Level.FINEST, "getElementListForPackageAsString(), Package :  {0}", pkg);
        final ArrayList<TypeHolder> result = new ArrayList<TypeHolder>();
        if (javaSource != null) {
            try {
                javaSource.runUserActionTask((Task)new Task<CompilationController>(){

                    public void run(CompilationController info) {
                        Elements elements = info.getElements();
                        this.addPackageElements(elements.getPackageElement(pkg));
                        this.addTypeElements(elements.getTypeElement(pkg));
                    }

                    private void addPackageElements(PackageElement packageElement) {
                        if (packageElement != null) {
                            List<? extends Element> typelist = packageElement.getEnclosedElements();
                            boolean samePackage = pkg.equals(currentPackage);
                            for (Element element : typelist) {
                                Set<Modifier> modifiers = element.getModifiers();
                                if (!modifiers.contains((Object)Modifier.PUBLIC) && (!samePackage || !modifiers.contains((Object)Modifier.PROTECTED) && (modifiers.contains((Object)Modifier.PUBLIC) || modifiers.contains((Object)Modifier.PRIVATE)))) continue;
                                result.add(new TypeHolder(element.toString(), element.getKind()));
                            }
                        }
                    }

                    private void addTypeElements(TypeElement typeElement) {
                        if (typeElement != null) {
                            List<? extends Element> typelist = typeElement.getEnclosedElements();
                            boolean samePackage = pkg.equals(currentPackage);
                            for (Element element : typelist) {
                                Set<Modifier> modifiers = element.getModifiers();
                                if (!modifiers.contains((Object)Modifier.PUBLIC) && (!samePackage || !modifiers.contains((Object)Modifier.PROTECTED) && (modifiers.contains((Object)Modifier.PUBLIC) || modifiers.contains((Object)Modifier.PRIVATE)))) continue;
                                result.add(new TypeHolder(element.toString(), element.getKind()));
                            }
                        }
                    }
                }, true);
            }
            catch (IOException ex) {
                LOG.log(Level.FINEST, "IOException : {0}", ex.getMessage());
            }
        }
        return result;
    }

    private static class TypeHolder {
        private final String name;
        private final ElementKind kind;

        public TypeHolder(IndexedClass indexedClass) {
            this.name = indexedClass.getFqn();
            this.kind = indexedClass.getKind() == org.netbeans.modules.csl.api.ElementKind.CLASS ? ElementKind.CLASS : ElementKind.INTERFACE;
        }

        public TypeHolder(String name, ElementKind kind) {
            this.name = name;
            this.kind = kind;
        }

        public ElementKind getKind() {
            return this.kind;
        }

        public String getName() {
            return this.name;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            TypeHolder other = (TypeHolder)obj;
            if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
                return false;
            }
            if (this.kind != other.kind) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 3;
            hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 59 * hash + (this.kind != null ? this.kind.hashCode() : 0);
            return hash;
        }
    }

}

