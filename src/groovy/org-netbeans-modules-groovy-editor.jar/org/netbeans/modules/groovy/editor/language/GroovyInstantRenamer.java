/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.InstantRenamer
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.groovy.editor.language;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.FindTypeUtils;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.occurrences.VariableScopeVisitor;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.NbBundle;

public class GroovyInstantRenamer
implements InstantRenamer {
    private static final Logger LOG = Logger.getLogger(GroovyInstantRenamer.class.getName());

    public boolean isRenameAllowed(ParserResult info, int caretOffset, String[] explanationRetValue) {
        LOG.log(Level.FINEST, "isRenameAllowed()");
        AstPath path = this.getPathUnderCaret(ASTUtils.getParseResult((Parser.Result)info), caretOffset);
        if (path != null) {
            ASTNode closest = path.leaf();
            if (closest instanceof Variable) {
                BaseDocument doc = LexUtilities.getDocument(ASTUtils.getParseResult((Parser.Result)info), false);
                if (!FindTypeUtils.isCaretOnClassNode(path, doc, caretOffset)) {
                    return true;
                }
                explanationRetValue[0] = NbBundle.getMessage(GroovyInstantRenamer.class, (String)"NoInstantRenameOnClassNode");
                return false;
            }
            explanationRetValue[0] = NbBundle.getMessage(GroovyInstantRenamer.class, (String)"OnlyRenameLocalVars");
            return false;
        }
        return false;
    }

    public Set<OffsetRange> getRenameRegions(ParserResult info, int caretOffset) {
        LOG.log(Level.FINEST, "getRenameRegions()");
        GroovyParserResult gpr = ASTUtils.getParseResult((Parser.Result)info);
        BaseDocument doc = LexUtilities.getDocument(gpr, false);
        if (doc == null) {
            return Collections.emptySet();
        }
        AstPath path = this.getPathUnderCaret(gpr, caretOffset);
        return GroovyInstantRenamer.markOccurences(path, doc, caretOffset);
    }

    private AstPath getPathUnderCaret(GroovyParserResult info, int caretOffset) {
        ModuleNode rootNode = ASTUtils.getRoot(info);
        if (rootNode == null) {
            return null;
        }
        int astOffset = ASTUtils.getAstOffset((Parser.Result)info, caretOffset);
        if (astOffset == -1) {
            return null;
        }
        BaseDocument document = LexUtilities.getDocument(info, false);
        if (document == null) {
            LOG.log(Level.FINEST, "Could not get BaseDocument. It's null");
            return null;
        }
        return new AstPath((ASTNode)rootNode, astOffset, document);
    }

    private static Set<OffsetRange> markOccurences(AstPath path, BaseDocument document, int cursorOffset) {
        HashSet<OffsetRange> regions = new HashSet<OffsetRange>();
        ASTNode root = path.root();
        assert (root instanceof ModuleNode);
        ModuleNode moduleNode = (ModuleNode)root;
        VariableScopeVisitor scopeVisitor = new VariableScopeVisitor(moduleNode.getContext(), path, document, cursorOffset);
        scopeVisitor.collect();
        for (ASTNode astNode : scopeVisitor.getOccurrences()) {
            regions.add(ASTUtils.getRange(astNode, document));
        }
        return regions;
    }
}

