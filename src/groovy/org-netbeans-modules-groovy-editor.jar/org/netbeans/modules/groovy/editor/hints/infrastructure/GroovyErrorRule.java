/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$ErrorRule
 *  org.netbeans.modules.csl.api.RuleContext
 */
package org.netbeans.modules.groovy.editor.hints.infrastructure;

import java.util.List;
import java.util.Set;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;

public abstract class GroovyErrorRule
implements Rule.ErrorRule {
    public abstract Set<CompilerErrorID> getCodes();

    public abstract void run(RuleContext var1, GroovyError var2, List<Hint> var3);
}

