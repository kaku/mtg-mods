/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcher
 *  org.netbeans.spi.editor.bracesmatching.MatcherContext
 */
package org.netbeans.modules.groovy.editor.language;

import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;

public final class GroovyBracesMatcher
implements BracesMatcher {
    MatcherContext context;

    public GroovyBracesMatcher(MatcherContext context) {
        this.context = context;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] findOrigin() throws InterruptedException, BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            Token token;
            BaseDocument doc = (BaseDocument)this.context.getDocument();
            int offset = this.context.getSearchOffset();
            TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
            if (ts != null) {
                ts.move(offset);
                if (!ts.moveNext()) {
                    int[] arrn = null;
                    return arrn;
                }
                token = ts.token();
                if (token == null) {
                    int[] arrn = null;
                    return arrn;
                }
                TokenId id = token.id();
                if (id == GroovyTokenId.LPAREN) {
                    int[] arrn = new int[]{ts.offset(), ts.offset() + token.length()};
                    return arrn;
                }
                if (id == GroovyTokenId.RPAREN) {
                    int[] arrn = new int[]{ts.offset(), ts.offset() + token.length()};
                    return arrn;
                }
                if (id == GroovyTokenId.LBRACE) {
                    int[] arrn = new int[]{ts.offset(), ts.offset() + token.length()};
                    return arrn;
                }
                if (id == GroovyTokenId.RBRACE) {
                    int[] arrn = new int[]{ts.offset(), ts.offset() + token.length()};
                    return arrn;
                }
                if (id == GroovyTokenId.LBRACKET) {
                    int[] arrn = new int[]{ts.offset(), ts.offset() + token.length()};
                    return arrn;
                }
                if (id == GroovyTokenId.RBRACKET) {
                    int[] arrn = new int[]{ts.offset(), ts.offset() + token.length()};
                    return arrn;
                }
            }
            token = null;
            return token;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] findMatches() throws InterruptedException, BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            Token token;
            BaseDocument doc = (BaseDocument)this.context.getDocument();
            int offset = this.context.getSearchOffset();
            TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
            if (ts != null) {
                ts.move(offset);
                if (!ts.moveNext()) {
                    int[] arrn = null;
                    return arrn;
                }
                token = ts.token();
                if (token == null) {
                    int[] arrn = null;
                    return arrn;
                }
                TokenId id = token.id();
                if (id == GroovyTokenId.LPAREN) {
                    OffsetRange r = LexUtilities.findFwd(doc, ts, GroovyTokenId.LPAREN, GroovyTokenId.RPAREN);
                    int[] arrn = new int[]{r.getStart(), r.getEnd()};
                    return arrn;
                }
                if (id == GroovyTokenId.RPAREN) {
                    OffsetRange r = LexUtilities.findBwd(doc, ts, GroovyTokenId.LPAREN, GroovyTokenId.RPAREN);
                    int[] arrn = new int[]{r.getStart(), r.getEnd()};
                    return arrn;
                }
                if (id == GroovyTokenId.LBRACE) {
                    OffsetRange r = LexUtilities.findFwd(doc, ts, GroovyTokenId.LBRACE, GroovyTokenId.RBRACE);
                    int[] arrn = new int[]{r.getStart(), r.getEnd()};
                    return arrn;
                }
                if (id == GroovyTokenId.RBRACE) {
                    OffsetRange r = LexUtilities.findBwd(doc, ts, GroovyTokenId.LBRACE, GroovyTokenId.RBRACE);
                    int[] arrn = new int[]{r.getStart(), r.getEnd()};
                    return arrn;
                }
                if (id == GroovyTokenId.LBRACKET) {
                    OffsetRange r = LexUtilities.findFwd(doc, ts, GroovyTokenId.LBRACKET, GroovyTokenId.RBRACKET);
                    int[] arrn = new int[]{r.getStart(), r.getEnd()};
                    return arrn;
                }
                if (id == GroovyTokenId.RBRACKET) {
                    OffsetRange r = LexUtilities.findBwd(doc, ts, GroovyTokenId.LBRACKET, GroovyTokenId.RBRACKET);
                    int[] arrn = new int[]{r.getStart(), r.getEnd()};
                    return arrn;
                }
            }
            token = null;
            return token;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }
}

