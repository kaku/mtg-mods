/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.elements.index;

import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.groovy.editor.api.elements.GroovyElement;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.openide.filesystems.FileObject;

public abstract class IndexedElement
extends GroovyElement {
    protected final IndexResult result;
    protected final String attributes;
    protected final int flags;
    protected Set<Modifier> modifiers;
    private Document document;

    protected IndexedElement(IndexResult result, String in, String attributes, int flags) {
        this(result, in, null, attributes, flags);
    }

    protected IndexedElement(IndexResult result, String in, String name, String attributes, int flags) {
        super(in, name);
        this.result = result;
        this.attributes = attributes;
        this.flags = flags;
    }

    @Override
    public abstract String getSignature();

    public String toString() {
        return this.getSignature();
    }

    public Document getDocument() throws IOException {
        if (this.document == null) {
            FileObject fo = this.getFileObject();
            if (fo == null) {
                return null;
            }
            this.document = LexUtilities.getDocument(fo, true);
        }
        return this.document;
    }

    @Override
    public FileObject getFileObject() {
        return this.result.getFile();
    }

    @Override
    public Set<Modifier> getModifiers() {
        if (this.modifiers == null) {
            Modifier access = null;
            if (this.isPublic()) {
                access = Modifier.PUBLIC;
            } else if (this.isProtected()) {
                access = Modifier.PROTECTED;
            } else if (this.isPrivate()) {
                access = Modifier.PRIVATE;
            }
            boolean isStatic = this.isStatic();
            this.modifiers = access != null ? (isStatic ? EnumSet.of(access, Modifier.STATIC) : EnumSet.of(access)) : (isStatic ? EnumSet.of(Modifier.STATIC) : Collections.emptySet());
        }
        return this.modifiers;
    }

    public static char flagToFirstChar(int flags) {
        char first = (char)(flags >>= 4);
        if (first >= '\n') {
            return (char)(first - 10 + 97);
        }
        return (char)(first + 48);
    }

    public static char flagToSecondChar(int flags) {
        char second = (char)(flags & 15);
        if (second >= '\n') {
            return (char)(second - 10 + 97);
        }
        return (char)(second + 48);
    }

    public static String flagToString(int flags) {
        return "" + IndexedElement.flagToFirstChar(flags) + IndexedElement.flagToSecondChar(flags);
    }

    public static int stringToFlag(String s, int startIndex) {
        return IndexedElement.stringToFlag(s.charAt(startIndex), s.charAt(startIndex + 1));
    }

    public static int stringToFlag(char first, char second) {
        int high = first > '9' ? first - 97 + 10 : first - 48;
        int low = second > '9' ? second - 97 + 10 : second - 48;
        return (high << 4) + low;
    }

    public boolean isPublic() {
        return (this.flags & 1) != 0;
    }

    public boolean isPrivate() {
        return (this.flags & 2) != 0;
    }

    public boolean isProtected() {
        return (this.flags & 4) != 0;
    }

    public boolean isStatic() {
        return (this.flags & 8) != 0;
    }

    public static String decodeFlags(int flags) {
        StringBuilder sb = new StringBuilder();
        if ((flags & 1) != 0) {
            sb.append("|PUBLIC");
        }
        if ((flags & 4) != 0) {
            sb.append("|PROTECTED");
        }
        if ((flags & 8) != 0) {
            sb.append("|STATIC");
        }
        return sb.toString();
    }
}

