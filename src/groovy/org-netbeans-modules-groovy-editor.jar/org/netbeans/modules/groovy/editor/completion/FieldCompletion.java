/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassNode
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.modules.csl.api.CompletionProposal
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ClassNode;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.ContextHelper;
import org.netbeans.modules.groovy.editor.api.completion.util.DotCompletionContext;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;
import org.netbeans.modules.groovy.editor.completion.provider.CompleteElementHandler;

public class FieldCompletion
extends BaseCompletion {
    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext context, int anchor) {
        FieldSignature prefixFieldSignature;
        Map<FieldSignature, CompletionItem> result;
        LOG.log(Level.FINEST, "-> completeFields");
        if (context.location == CaretLocation.INSIDE_PARAMETERS && !context.isBehindDot()) {
            LOG.log(Level.FINEST, "no fields completion inside of parameters-list");
            return false;
        }
        if (context.dotContext != null && context.dotContext.isMethodsOnly()) {
            return false;
        }
        if (context.context.beforeLiteral != null && context.context.beforeLiteral.id() == GroovyTokenId.LITERAL_implements || context.context.beforeLiteral != null && context.context.beforeLiteral.id() == GroovyTokenId.LITERAL_extends) {
            return false;
        }
        if (context.context.beforeLiteral != null && context.context.beforeLiteral.id() == GroovyTokenId.LITERAL_class) {
            return false;
        }
        if (context.isBehindDot()) {
            ClasspathInfo pathInfo;
            LOG.log(Level.FINEST, "We are invoked right behind a dot.");
            BaseCompletion.PackageCompletionRequest packageRequest = this.getPackageRequest(context);
            if (packageRequest.basePackage.length() > 0 && this.isValidPackage(pathInfo = this.getClasspathInfoFromRequest(context), packageRequest.basePackage)) {
                LOG.log(Level.FINEST, "The string before the dot seems to be a valid package");
                return false;
            }
        } else {
            context.declaringClass = ContextHelper.getSurroundingClassNode(context);
        }
        if (context.getPrefix().startsWith("$")) {
            context.setPrefix(context.getPrefix().substring(1));
            context.setAnchor(context.getAnchor() + 1);
        }
        if ((result = new CompleteElementHandler(context).getFields()).containsKey(prefixFieldSignature = new FieldSignature(context.getPrefix()))) {
            result.remove(prefixFieldSignature);
        }
        proposals.addAll(result.values());
        return true;
    }
}

