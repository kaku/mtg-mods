/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.spi.completion;

import java.util.Set;

public interface DefaultImportsProvider {
    public Set<String> getDefaultImportPackages();

    public Set<String> getDefaultImportClasses();
}

