/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.GenericsType
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.completion.provider;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.GenericsType;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.DotCompletionContext;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.completion.AccessLevel;
import org.netbeans.modules.groovy.editor.completion.provider.CompletionProviderHandler;
import org.netbeans.modules.groovy.editor.completion.provider.GroovyElementsProvider;
import org.netbeans.modules.groovy.editor.completion.provider.JavaElementHandler;
import org.netbeans.modules.groovy.editor.completion.provider.TransformationHandler;
import org.netbeans.modules.groovy.editor.java.Utilities;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public final class CompleteElementHandler {
    private final CompletionContext context;
    private final ParserResult info;
    private final GroovyIndex index;

    public CompleteElementHandler(CompletionContext context) {
        this.context = context;
        this.info = context.getParserResult();
        FileObject fo = this.info.getSnapshot().getSource().getFileObject();
        this.index = fo != null ? GroovyIndex.get(QuerySupport.findRoots((FileObject)fo, Collections.singleton("classpath/source"), (Collection)null, (Collection)null)) : null;
    }

    public Map<MethodSignature, CompletionItem> getMethods() {
        ClassNode source = this.context.getSurroundingClass();
        ClassNode node = this.context.declaringClass;
        if (node == null) {
            return Collections.emptyMap();
        }
        Map<MethodSignature, CompletionItem> result = this.getMethodsInner(source, node, this.context.getPrefix(), this.context.getAnchor(), 0, AccessLevel.create(source, node), this.context.dotContext != null && this.context.dotContext.isMethodsOnly());
        return result;
    }

    public Map<FieldSignature, CompletionItem> getFields() {
        ClassNode source = this.context.getSurroundingClass();
        ClassNode node = this.context.declaringClass;
        if (node == null) {
            return Collections.emptyMap();
        }
        Map<FieldSignature, CompletionItem> result = this.getFieldsInner(source, node, this.context.getPrefix(), this.context.getAnchor(), 0);
        return result;
    }

    private Map<MethodSignature, CompletionItem> getMethodsInner(ClassNode source, ClassNode node, String prefix, int anchor, int level, Set<AccessLevel> access, boolean nameOnly) {
        boolean leaf = level == 0;
        Set<AccessLevel> modifiedAccess = AccessLevel.update(access, source, node);
        TreeMap<MethodSignature, CompletionItem> result = new TreeMap<MethodSignature, CompletionItem>(new Comparator<MethodSignature>(){

            @Override
            public int compare(MethodSignature method1, MethodSignature method2) {
                if (!method1.getName().equals(method2.getName())) {
                    return method1.getName().compareTo(method2.getName());
                }
                if (method1.getParameters().length < method2.getParameters().length) {
                    return -1;
                }
                if (method1.getParameters().length > method2.getParameters().length) {
                    return 1;
                }
                for (int i = 0; i < method1.getParameters().length; ++i) {
                    String param2;
                    String param1 = method1.getParameters()[i];
                    int comparedValue = param1.compareTo(param2 = method2.getParameters()[i]);
                    if (comparedValue == 0) continue;
                    return comparedValue;
                }
                return 0;
            }
        });
        ClassDefinition definition = this.loadDefinition(node);
        ClassNode typeNode = definition.getNode();
        String typeName = typeNode.getName();
        if ("int".equals(typeName)) {
            typeName = "java.lang.Integer";
        }
        this.context.setTypeName(typeName);
        GroovyElementsProvider groovyProvider = new GroovyElementsProvider();
        CompleteElementHandler.fillSuggestions(groovyProvider.getMethods(this.context), result);
        if (result.isEmpty()) {
            String[] typeParameters = new String[typeNode.isUsingGenerics() && typeNode.getGenericsTypes() != null ? typeNode.getGenericsTypes().length : 0];
            for (int i = 0; i < typeParameters.length; ++i) {
                GenericsType genType = typeNode.getGenericsTypes()[i];
                typeParameters[i] = genType.getUpperBounds() != null ? Utilities.translateClassLoaderTypeName(genType.getUpperBounds()[0].getName()) : Utilities.translateClassLoaderTypeName(genType.getName());
            }
            CompleteElementHandler.fillSuggestions(JavaElementHandler.forCompilationInfo(this.info).getMethods(typeName, prefix, anchor, typeParameters, leaf, modifiedAccess, nameOnly), result);
        }
        CompletionProviderHandler providerHandler = new CompletionProviderHandler();
        CompleteElementHandler.fillSuggestions(providerHandler.getMethods(this.context), result);
        CompleteElementHandler.fillSuggestions(providerHandler.getStaticMethods(this.context), result);
        if (typeNode.getSuperClass() != null) {
            CompleteElementHandler.fillSuggestions(this.getMethodsInner(source, typeNode.getSuperClass(), prefix, anchor, level + 1, modifiedAccess, nameOnly), result);
        } else if (leaf) {
            CompleteElementHandler.fillSuggestions(JavaElementHandler.forCompilationInfo(this.info).getMethods("java.lang.Object", prefix, anchor, new String[0], false, modifiedAccess, nameOnly), result);
        }
        for (ClassNode inter : typeNode.getInterfaces()) {
            CompleteElementHandler.fillSuggestions(this.getMethodsInner(source, inter, prefix, anchor, level + 1, modifiedAccess, nameOnly), result);
        }
        CompleteElementHandler.fillSuggestions(TransformationHandler.getMethods(this.index, typeNode, prefix, anchor), result);
        return result;
    }

    private Map<FieldSignature, CompletionItem> getFieldsInner(ClassNode source, ClassNode node, String prefix, int anchor, int level) {
        boolean leaf = level == 0;
        ClassDefinition definition = this.loadDefinition(node);
        ClassNode typeNode = definition.getNode();
        String typeName = typeNode.getName();
        if ("int".equals(typeName)) {
            typeName = "java.lang.Integer";
        }
        this.context.setTypeName(typeName);
        HashMap<FieldSignature, CompletionItem> result = new HashMap<FieldSignature, CompletionItem>();
        GroovyElementsProvider groovyProvider = new GroovyElementsProvider();
        CompleteElementHandler.fillSuggestions(groovyProvider.getFields(this.context), result);
        CompleteElementHandler.fillSuggestions(groovyProvider.getStaticFields(this.context), result);
        CompleteElementHandler.fillSuggestions(JavaElementHandler.forCompilationInfo(this.info).getFields(typeNode.getName(), prefix, anchor, leaf), result);
        CompletionProviderHandler providerHandler = new CompletionProviderHandler();
        CompleteElementHandler.fillSuggestions(providerHandler.getFields(this.context), result);
        CompleteElementHandler.fillSuggestions(providerHandler.getStaticFields(this.context), result);
        if (typeNode.getSuperClass() != null) {
            CompleteElementHandler.fillSuggestions(this.getFieldsInner(source, typeNode.getSuperClass(), prefix, anchor, level + 1), result);
        } else if (leaf) {
            CompleteElementHandler.fillSuggestions(JavaElementHandler.forCompilationInfo(this.info).getFields("java.lang.Object", prefix, anchor, false), result);
        }
        for (ClassNode inter : typeNode.getInterfaces()) {
            CompleteElementHandler.fillSuggestions(this.getFieldsInner(source, inter, prefix, anchor, level + 1), result);
        }
        CompleteElementHandler.fillSuggestions(TransformationHandler.getFields(this.index, typeNode, prefix, anchor), result);
        return result;
    }

    private ClassDefinition loadDefinition(ClassNode node) {
        IndexedClass indexed;
        ASTNode astNode;
        if (this.index == null) {
            return new ClassDefinition(node, null);
        }
        Set<IndexedClass> classes = this.index.getClasses(node.getName(), QuerySupport.Kind.EXACT);
        if (!classes.isEmpty() && (astNode = ASTUtils.getForeignNode(indexed = classes.iterator().next())) instanceof ClassNode) {
            return new ClassDefinition((ClassNode)astNode, indexed);
        }
        return new ClassDefinition(node, null);
    }

    private static <T> void fillSuggestions(Map<T, ? extends CompletionItem> input, Map<T, ? super CompletionItem> result) {
        for (Map.Entry<T, CompletionItem> entry : input.entrySet()) {
            if (result.containsKey(entry.getKey())) continue;
            result.put(entry.getKey(), entry.getValue());
        }
    }

    private static class ClassDefinition {
        private final ClassNode node;
        private final IndexedClass indexed;

        public ClassDefinition(ClassNode node, IndexedClass indexed) {
            this.node = node;
            this.indexed = indexed;
        }

        public ClassNode getNode() {
            return this.node;
        }

        public FileObject getFileObject() {
            return this.indexed != null ? this.indexed.getFileObject() : null;
        }
    }

}

