/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 */
package org.netbeans.modules.groovy.editor.java;

import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import org.netbeans.api.annotations.common.CheckForNull;

public final class ElementSearch {
    private ElementSearch() {
    }

    @CheckForNull
    public static TypeElement getClass(Elements elements, String name) {
        TypeElement typeElement = elements.getTypeElement(name);
        if (typeElement == null) {
            typeElement = ElementSearch.getInnerClass(elements, name);
        }
        return typeElement;
    }

    private static TypeElement getInnerClass(Elements elements, String name) {
        int index = name.indexOf("$");
        TypeElement typeElement = null;
        if (index > 0 && name.length() > index + 1) {
            TypeElement enclosingElement = elements.getTypeElement(name.substring(0, index));
            int nextIndex = index;
            while (enclosingElement != null && nextIndex >= 0) {
                String subName = name.substring(nextIndex + 1);
                int subIndex = subName.indexOf("$");
                if (subIndex >= 0) {
                    subName = subName.substring(0, subIndex);
                    nextIndex = nextIndex + 1 + subIndex;
                } else {
                    nextIndex = -1;
                }
                boolean found = false;
                for (TypeElement elem : ElementFilter.typesIn(enclosingElement.getEnclosedElements())) {
                    Name elemName = elem.getSimpleName();
                    if (!elemName.toString().equals(subName)) continue;
                    enclosingElement = elem;
                    found = true;
                    break;
                }
                if (found) continue;
                enclosingElement = null;
            }
            typeElement = enclosingElement;
        }
        return typeElement;
    }
}

