/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.api.completion;

public enum KeywordCategory {
    KEYWORD("keyword"),
    PRIMITIVE("primitive"),
    MODIFIER("modifier"),
    ANY("any"),
    NONE("none");
    
    private final String category;

    private KeywordCategory(String category) {
        this.category = category;
    }
}

