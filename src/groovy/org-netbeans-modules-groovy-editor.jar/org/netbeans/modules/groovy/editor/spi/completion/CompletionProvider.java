/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.spi.completion;

import java.util.Map;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.FieldSignature;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;

public interface CompletionProvider {
    public Map<MethodSignature, CompletionItem> getMethods(CompletionContext var1);

    public Map<MethodSignature, CompletionItem> getStaticMethods(CompletionContext var1);

    public Map<FieldSignature, CompletionItem> getFields(CompletionContext var1);

    public Map<FieldSignature, CompletionItem> getStaticFields(CompletionContext var1);
}

