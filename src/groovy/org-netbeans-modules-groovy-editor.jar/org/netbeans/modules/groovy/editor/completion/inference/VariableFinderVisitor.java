/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassCodeVisitorSupport
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.BinaryExpression
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.ClosureListExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.CatchStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.codehaus.groovy.control.SourceUnit
 *  org.codehaus.groovy.syntax.Token
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 */
package org.netbeans.modules.groovy.editor.completion.inference;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassCodeVisitorSupport;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.ClosureListExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;

public class VariableFinderVisitor
extends ClassCodeVisitorSupport {
    private final SourceUnit sourceUnit;
    private final AstPath path;
    private final BaseDocument doc;
    private final int cursorOffset;
    private Set<ASTNode> blocks = new HashSet<ASTNode>();
    private Map<String, Variable> variables = new HashMap<String, Variable>();

    public VariableFinderVisitor(SourceUnit sourceUnit, AstPath path, BaseDocument doc, int cursorOffset) {
        this.sourceUnit = sourceUnit;
        this.path = path;
        this.doc = doc;
        this.cursorOffset = cursorOffset;
    }

    protected SourceUnit getSourceUnit() {
        return this.sourceUnit;
    }

    public Collection<Variable> getVariables() {
        return this.variables.values();
    }

    public void collect() {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getPositionedSequence(this.doc, this.cursorOffset);
        if (ts == null) {
            return;
        }
        Token token = ts.token();
        if (token == null) {
            return;
        }
        ASTNode last = null;
        this.blocks.clear();
        this.variables.clear();
        for (ASTNode scope : this.path) {
            if (!(scope instanceof ClosureExpression) && !(scope instanceof MethodNode) && !(scope instanceof ConstructorNode) && !(scope instanceof ForStatement) && !(scope instanceof BlockStatement) && !(scope instanceof ClosureListExpression) && !(scope instanceof CatchStatement)) continue;
            last = scope;
            this.blocks.add(scope);
            if (!(scope instanceof ForStatement) || !(((ForStatement)scope).getCollectionExpression() instanceof ClosureListExpression)) continue;
            this.blocks.add((ASTNode)((ForStatement)scope).getCollectionExpression());
        }
        if (last instanceof ClosureExpression) {
            this.visitClosureExpression((ClosureExpression)last);
        } else if (last instanceof MethodNode) {
            this.visitMethod((MethodNode)last);
        } else if (last instanceof ConstructorNode) {
            this.visitConstructor((ConstructorNode)last);
        } else if (last instanceof ForStatement) {
            this.visitForLoop((ForStatement)last);
        } else if (last instanceof BlockStatement) {
            this.visitBlockStatement((BlockStatement)last);
        } else if (last instanceof ClosureListExpression) {
            this.visitClosureListExpression((ClosureListExpression)last);
        } else if (last instanceof CatchStatement) {
            this.visitCatchStatement((CatchStatement)last);
        }
    }

    public void visitDeclarationExpression(DeclarationExpression expression) {
        VariableExpression variableExpression;
        if (this.blocks.isEmpty() && expression.getLineNumber() >= 0 && expression.getColumnNumber() >= 0 && this.path.getLineNumber() >= 0 && this.path.getColumnNumber() >= 0 && (expression.getLineNumber() > this.path.getLineNumber() || expression.getLineNumber() == this.path.getLineNumber() && expression.getColumnNumber() >= this.path.getColumnNumber())) {
            return;
        }
        if (!expression.isMultipleAssignmentDeclaration() && (variableExpression = expression.getVariableExpression()).getAccessedVariable() != null) {
            String name = variableExpression.getAccessedVariable().getName();
            this.variables.put(name, variableExpression.getAccessedVariable());
        }
        super.visitDeclarationExpression(expression);
    }

    public void visitBinaryExpression(BinaryExpression expression) {
        VariableExpression variableExpression;
        String name;
        if (this.blocks.isEmpty() && expression.getLineNumber() >= 0 && expression.getColumnNumber() >= 0 && this.path.getLineNumber() >= 0 && this.path.getColumnNumber() >= 0 && (expression.getLineNumber() > this.path.getLineNumber() || expression.getLineNumber() == this.path.getLineNumber() && expression.getColumnNumber() >= this.path.getColumnNumber())) {
            return;
        }
        Expression leftExpression = expression.getLeftExpression();
        if (leftExpression instanceof VariableExpression && expression.getOperation().isA(100) && (variableExpression = (VariableExpression)leftExpression).getAccessedVariable() != null && !this.variables.containsKey(name = variableExpression.getAccessedVariable().getName())) {
            this.variables.put(name, variableExpression.getAccessedVariable());
        }
        super.visitBinaryExpression(expression);
    }

    public void visitMethod(MethodNode node) {
        if (!this.blocks.remove((Object)node)) {
            return;
        }
        for (Parameter param : node.getParameters()) {
            this.variables.put(param.getName(), (Variable)param);
        }
        super.visitMethod(node);
    }

    public void visitCatchStatement(CatchStatement statement) {
        if (!this.blocks.remove((Object)statement)) {
            return;
        }
        if (statement.getVariable() != null) {
            String name = statement.getVariable().getName();
            this.variables.put(name, (Variable)statement.getVariable());
        }
        super.visitCatchStatement(statement);
    }

    public void visitClosureExpression(ClosureExpression expression) {
        if (!this.blocks.remove((Object)expression)) {
            return;
        }
        if (expression.isParameterSpecified()) {
            for (Parameter param : expression.getParameters()) {
                this.variables.put(param.getName(), (Variable)param);
            }
        } else {
            this.variables.put("it", (Variable)new VariableExpression("it"));
        }
        super.visitClosureExpression(expression);
    }

    public void visitConstructor(ConstructorNode node) {
        if (!this.blocks.remove((Object)node)) {
            return;
        }
        for (Parameter param : node.getParameters()) {
            this.variables.put(param.getName(), (Variable)param);
        }
        super.visitConstructor(node);
    }

    public void visitForLoop(ForStatement forLoop) {
        if (!this.blocks.remove((Object)forLoop)) {
            return;
        }
        Parameter param = forLoop.getVariable();
        if (param != ForStatement.FOR_LOOP_DUMMY) {
            this.variables.put(param.getName(), (Variable)param);
        }
        super.visitForLoop(forLoop);
    }

    public void visitBlockStatement(BlockStatement block) {
        if (!this.blocks.remove((Object)block)) {
            return;
        }
        super.visitBlockStatement(block);
    }

    public void visitClosureListExpression(ClosureListExpression cle) {
        if (!this.blocks.remove((Object)cle)) {
            return;
        }
        super.visitClosureListExpression(cle);
    }
}

