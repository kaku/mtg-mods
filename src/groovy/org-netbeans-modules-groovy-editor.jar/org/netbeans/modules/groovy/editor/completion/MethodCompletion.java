/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.completion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.lexer.Token;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.MethodSignature;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.ContextHelper;
import org.netbeans.modules.groovy.editor.api.completion.util.DotCompletionContext;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedMethod;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;
import org.netbeans.modules.groovy.editor.completion.provider.CompleteElementHandler;
import org.netbeans.modules.groovy.editor.imports.ImportUtils;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public class MethodCompletion
extends BaseCompletion {
    private List<CompletionProposal> proposals;
    private CompletionContext context;
    private int anchor;

    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext context, int anchor) {
        ClasspathInfo pathInfo;
        LOG.log(Level.FINEST, "-> completeMethods");
        this.proposals = proposals;
        this.context = context;
        this.anchor = anchor;
        if (context == null || context.context == null || context.location == CaretLocation.INSIDE_PARAMETERS) {
            return false;
        }
        if (context.dotContext != null && context.dotContext.isFieldsOnly()) {
            return false;
        }
        if (ContextHelper.isConstructorCall(context)) {
            return this.completeConstructor();
        }
        if (!context.isBehindDot() && context.context.before1 != null) {
            return false;
        }
        if (context.declaringClass == null) {
            LOG.log(Level.FINEST, "No declaring class found");
            return false;
        }
        BaseCompletion.PackageCompletionRequest packageRequest = this.getPackageRequest(context);
        if (packageRequest.basePackage.length() > 0 && this.isValidPackage(pathInfo = this.getClasspathInfoFromRequest(context), packageRequest.basePackage)) {
            LOG.log(Level.FINEST, "The string before the dot seems to be a valid package");
            return false;
        }
        Map<MethodSignature, CompletionItem> result = new CompleteElementHandler(context).getMethods();
        proposals.addAll(result.values());
        return true;
    }

    private boolean completeConstructor() {
        GroovyIndex index;
        LOG.log(Level.FINEST, "This looks like a constructor ...");
        JavaSource javaSource = this.getJavaSourceFromRequest();
        if (javaSource != null) {
            try {
                javaSource.runUserActionTask((Task)new Task<CompilationController>(){

                    public void run(CompilationController info) {
                        ArrayList typelist = new ArrayList();
                        for (String importName : MethodCompletion.this.getAllImports()) {
                            typelist.addAll(MethodCompletion.this.getElementListFor(info.getElements(), importName));
                        }
                        BaseCompletion.LOG.log(Level.FINEST, "Number of types found:  {0}", typelist.size());
                        if (MethodCompletion.this.exactConstructorExists(typelist, MethodCompletion.this.context.getPrefix())) {
                            MethodCompletion.this.addExactProposals(typelist);
                        }
                        MethodCompletion.this.addConstructorProposalsForDeclaredClasses();
                    }
                }, true);
            }
            catch (IOException ex) {
                LOG.log(Level.FINEST, "IOException : {0}", ex.getMessage());
            }
        }
        if (this.exactClassExists(index = GroovyIndex.get(QuerySupport.findRoots((FileObject)this.context.getSourceFile(), Collections.singleton("classpath/source"), (Collection)null, (Collection)null)))) {
            String name = this.context.getPrefix();
            Set<IndexedMethod> constructors = index.getConstructors(name);
            for (IndexedMethod indexedMethod : constructors) {
                List<MethodElement.MethodParameter> parameters = indexedMethod.getParameters();
                CompletionItem.ConstructorItem constructor = new CompletionItem.ConstructorItem(name, parameters, this.anchor, false);
                if (this.proposals.contains((Object)constructor)) continue;
                this.proposals.add((CompletionProposal)constructor);
            }
            if (this.proposals.isEmpty()) {
                this.proposals.add((CompletionProposal)new CompletionItem.ConstructorItem(name, Collections.emptyList(), this.anchor, false));
            }
        }
        return !this.proposals.isEmpty();
    }

    private boolean exactClassExists(GroovyIndex index) {
        Set<IndexedClass> classes = index.getClasses(this.context.getPrefix(), QuerySupport.Kind.PREFIX);
        for (IndexedClass indexedClass : classes) {
            if (!indexedClass.getName().equals(this.context.getPrefix())) continue;
            return true;
        }
        return false;
    }

    private List<String> getAllImports() {
        ArrayList<String> imports = new ArrayList<String>();
        imports.addAll(ImportUtils.getDefaultImportPackages());
        imports.addAll(this.getImportedTypes());
        imports.addAll(this.getCurrentPackage());
        imports.addAll(this.getTypesInSameFile());
        return imports;
    }

    private List<String> getImportedTypes() {
        ArrayList<String> importedTypes = new ArrayList<String>();
        ModuleNode moduleNode = ContextHelper.getSurroundingModuleNode(this.context);
        if (moduleNode != null) {
            for (ImportNode importNode : moduleNode.getImports()) {
                importedTypes.add(importNode.getClassName());
            }
            for (ImportNode wildcardImport : moduleNode.getStarImports()) {
                importedTypes.add(wildcardImport.getPackageName());
            }
        }
        return importedTypes;
    }

    private List<String> getCurrentPackage() {
        String packageName;
        ModuleNode moduleNode = ContextHelper.getSurroundingModuleNode(this.context);
        if (moduleNode != null && (packageName = moduleNode.getPackageName()) != null) {
            packageName = packageName.substring(0, packageName.length() - 1);
            return Collections.singletonList(packageName);
        }
        return Collections.emptyList();
    }

    private List<String> getTypesInSameFile() {
        ArrayList<String> declaredClassNames = new ArrayList<String>();
        List<ClassNode> declaredClasses = ContextHelper.getDeclaredClasses(this.context);
        for (ClassNode declaredClass : declaredClasses) {
            declaredClassNames.add(declaredClass.getName());
        }
        return declaredClassNames;
    }

    private boolean exactConstructorExists(List<? extends Element> typelist, String prefix) {
        for (Element element : typelist) {
            if (!prefix.toUpperCase().equals(element.getSimpleName().toString().toUpperCase())) continue;
            return true;
        }
        return false;
    }

    private void addExactProposals(List<? extends Element> typelist) {
        for (Element element : typelist) {
            if (element.getKind() != ElementKind.CLASS) continue;
            for (Element encl : element.getEnclosedElements()) {
                String constructorName;
                if (encl.getKind() != ElementKind.CONSTRUCTOR || !(constructorName = element.getSimpleName().toString()).toUpperCase().equals(this.context.getPrefix().toUpperCase())) continue;
                this.addConstructorProposal(constructorName, (ExecutableElement)encl);
            }
        }
    }

    private void addConstructorProposal(String constructorName, ExecutableElement encl) {
        List<MethodElement.MethodParameter> paramList = this.getParameterList(encl);
        CompletionItem.ConstructorItem constructor = new CompletionItem.ConstructorItem(constructorName, paramList, this.anchor, false);
        if (!this.proposals.contains((Object)constructor)) {
            this.proposals.add((CompletionProposal)constructor);
        }
    }

    private void addConstructorProposalsForDeclaredClasses() {
        for (ClassNode declaredClass : ContextHelper.getDeclaredClasses(this.context)) {
            this.addConstructorProposal(declaredClass);
        }
    }

    private void addConstructorProposal(ClassNode classNode) {
        String constructorName = classNode.getNameWithoutPackage();
        if (this.isPrefixed(this.context, constructorName)) {
            for (ConstructorNode constructor : classNode.getDeclaredConstructors()) {
                Parameter[] parameters = constructor.getParameters();
                List<MethodElement.MethodParameter> paramList = this.getParameterListForMethod(parameters);
                this.proposals.add((CompletionProposal)new CompletionItem.ConstructorItem(constructorName, paramList, this.anchor, false));
            }
        }
    }

    private JavaSource getJavaSourceFromRequest() {
        ClasspathInfo pathInfo = this.getClasspathInfoFromRequest(this.context);
        assert (pathInfo != null);
        JavaSource javaSource = JavaSource.create((ClasspathInfo)pathInfo, (FileObject[])new FileObject[0]);
        if (javaSource == null) {
            LOG.log(Level.FINEST, "Problem retrieving JavaSource from ClassPathInfo, exiting.");
            return null;
        }
        return javaSource;
    }

    @NonNull
    private List<? extends Element> getElementListFor(Elements elements, String importName) {
        if (elements != null && importName != null) {
            PackageElement packageElement = elements.getPackageElement(importName);
            if (packageElement != null) {
                return packageElement.getEnclosedElements();
            }
            TypeElement typeElement = elements.getTypeElement(importName);
            if (typeElement != null) {
                return Collections.singletonList(typeElement);
            }
        }
        return Collections.emptyList();
    }

    private List<MethodElement.MethodParameter> getParameterList(ExecutableElement exe) {
        ArrayList<MethodElement.MethodParameter> paramList = new ArrayList<MethodElement.MethodParameter>();
        if (exe != null) {
            try {
                List<? extends VariableElement> params = exe.getParameters();
                int i = 1;
                for (VariableElement variableElement : params) {
                    String fullName;
                    TypeMirror tm = variableElement.asType();
                    String name = fullName = tm.toString();
                    if (tm.getKind() == TypeKind.DECLARED) {
                        name = GroovyUtils.stripPackage(fullName);
                    }
                    String varName = "param" + String.valueOf(i);
                    paramList.add(new MethodElement.MethodParameter(fullName, name, varName));
                    ++i;
                }
            }
            catch (NullPointerException e) {
                // empty catch block
            }
        }
        return paramList;
    }

    private String getParameterListForMethod(ExecutableElement exe) {
        StringBuilder sb = new StringBuilder();
        if (exe != null) {
            try {
                List<? extends VariableElement> params = exe.getParameters();
                for (VariableElement variableElement : params) {
                    TypeMirror tm = variableElement.asType();
                    if (sb.length() > 0) {
                        sb.append(", ");
                    }
                    if (tm.getKind() == TypeKind.DECLARED || tm.getKind() == TypeKind.ARRAY) {
                        sb.append(GroovyUtils.stripPackage(tm.toString()));
                        continue;
                    }
                    sb.append(tm.toString());
                }
            }
            catch (NullPointerException e) {
                // empty catch block
            }
        }
        return sb.toString();
    }

    private List<MethodElement.MethodParameter> getParameterListForMethod(Parameter[] parameters) {
        if (parameters.length == 0) {
            return Collections.EMPTY_LIST;
        }
        ArrayList<MethodElement.MethodParameter> paramDescriptors = new ArrayList<MethodElement.MethodParameter>();
        for (Parameter param : parameters) {
            String fullTypeName = param.getType().getName();
            String typeName = param.getType().getNameWithoutPackage();
            String name = param.getName();
            paramDescriptors.add(new MethodElement.MethodParameter(fullTypeName, typeName, name));
        }
        return paramDescriptors;
    }

}

