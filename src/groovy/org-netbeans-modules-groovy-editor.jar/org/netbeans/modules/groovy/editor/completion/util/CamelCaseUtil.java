/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.groovy.editor.completion.util;

import java.util.ArrayList;
import java.util.List;

public class CamelCaseUtil {
    public static boolean compareCamelCase(String longName, String camelCasePrefix) {
        List<String> splittedClassName = CamelCaseUtil.splitByUpperCases(longName);
        List<String> splittedPrefixes = CamelCaseUtil.splitByUpperCases(camelCasePrefix);
        if (splittedPrefixes.size() > splittedClassName.size()) {
            return false;
        }
        for (int i = 0; i < splittedPrefixes.size(); ++i) {
            if (splittedClassName.get(i).startsWith(splittedPrefixes.get(i))) continue;
            return false;
        }
        return true;
    }

    public static String getCamelCaseFirstWord(String longName) {
        List<String> splittedPrefix = CamelCaseUtil.splitByUpperCases(longName);
        if (splittedPrefix.isEmpty()) {
            return "";
        }
        return splittedPrefix.get(0);
    }

    private static List<String> splitByUpperCases(String prefix) {
        ArrayList<String> splitedPrefixes = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < prefix.length(); ++i) {
            char actualChar = prefix.charAt(i);
            if (Character.isUpperCase(actualChar)) {
                if (builder.length() != 0) {
                    splitedPrefixes.add(builder.toString());
                }
                builder.delete(0, builder.length());
            }
            builder.append(actualChar);
        }
        splitedPrefixes.add(builder.toString());
        return splitedPrefixes;
    }
}

