/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.groovy.editor.api.lexer;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public final class LexUtilities {
    private static final Set<TokenId> END_PAIRS = new HashSet<TokenId>();
    private static final Set<TokenId> INDENT_WORDS = new HashSet<TokenId>();
    private static final Set<TokenId> WHITESPACES_AND_COMMENTS = new HashSet<TokenId>();

    @CheckForNull
    public static BaseDocument getDocument(GroovyParserResult info, boolean forceOpen) {
        if (info != null) {
            Source source = info.getSnapshot().getSource();
            return LexUtilities.getDocument(source, forceOpen);
        }
        return null;
    }

    @CheckForNull
    public static BaseDocument getDocument(Source source, boolean forceOpen) {
        BaseDocument bdoc = null;
        Document doc = source.getDocument(true);
        if (doc instanceof BaseDocument) {
            bdoc = (BaseDocument)doc;
        }
        return bdoc;
    }

    public static BaseDocument getDocument(FileObject fileObject, boolean forceOpen) {
        try {
            DataObject dobj = DataObject.find((FileObject)fileObject);
            EditorCookie ec = (EditorCookie)dobj.getLookup().lookup(EditorCookie.class);
            if (ec == null) {
                throw new IOException("Can't open " + fileObject.getNameExt());
            }
            StyledDocument document = forceOpen ? ec.openDocument() : ec.getDocument();
            if (document instanceof BaseDocument) {
                return (BaseDocument)document;
            }
            try {
                Class c = Class.forName("org.netbeans.modules.groovy.editor.test.GroovyTestBase");
                if (c != null) {
                    Method m = c.getMethod("getDocumentFor", FileObject.class);
                    return (BaseDocument)m.invoke(null, (Object[])new FileObject[]{fileObject});
                }
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
        return null;
    }

    private LexUtilities() {
    }

    public static OffsetRange getLexerOffsets(GroovyParserResult info, OffsetRange astRange) {
        int rangeStart = astRange.getStart();
        int start = info.getSnapshot().getOriginalOffset(rangeStart);
        if (start == rangeStart) {
            return astRange;
        }
        if (start == -1) {
            return OffsetRange.NONE;
        }
        return new OffsetRange(start, start + astRange.getLength());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static TokenSequence<GroovyTokenId> getGroovyTokenSequence(Document doc, int offset) {
        BaseDocument baseDocument = (BaseDocument)doc;
        try {
            baseDocument.readLock();
            TokenSequence<GroovyTokenId> tokenSequence = LexUtilities.getGroovyTokenSequence(TokenHierarchy.get((Document)doc), offset);
            return tokenSequence;
        }
        finally {
            baseDocument.readUnlock();
        }
    }

    private static TokenSequence<GroovyTokenId> findRhtmlDelimited(TokenSequence t, int offset) {
        if (t.language().mimeType().equals("text/x-gsp")) {
            TokenSequence ets;
            t.move(offset);
            if (t.moveNext() && t.token() != null && "groovy-delimiter".equals(t.token().id().primaryCategory()) && t.moveNext() && t.token() != null && "groovy".equals(t.token().id().primaryCategory()) && (ets = t.embedded()) != null) {
                return ets;
            }
        }
        return null;
    }

    public static TokenSequence<GroovyTokenId> getGroovyTokenSequence(TokenHierarchy<Document> th, int offset) {
        TokenSequence ts = th.tokenSequence(GroovyTokenId.language());
        if (ts == null) {
            TokenSequence<GroovyTokenId> ets;
            List list = th.embeddedTokenSequences(offset, true);
            for (TokenSequence t2 : list) {
                if (t2.language() == GroovyTokenId.language()) {
                    ts = t2;
                    break;
                }
                ets = LexUtilities.findRhtmlDelimited(t2, offset);
                if (ets == null) continue;
                return ets;
            }
            if (ts == null) {
                list = th.embeddedTokenSequences(offset, false);
                for (TokenSequence t2 : list) {
                    if (t2.language() == GroovyTokenId.language()) {
                        ts = t2;
                        break;
                    }
                    ets = LexUtilities.findRhtmlDelimited(t2, offset);
                    if (ets == null) continue;
                    return ets;
                }
            }
        }
        return ts;
    }

    public static TokenSequence<GroovyTokenId> getPositionedSequence(BaseDocument doc, int offset) {
        return LexUtilities.getPositionedSequence(doc, offset, true);
    }

    public static TokenSequence<GroovyTokenId> getPositionedSequence(BaseDocument doc, int offset, boolean lookBack) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts != null) {
            try {
                ts.move(offset);
            }
            catch (AssertionError e) {
                DataObject dobj = (DataObject)doc.getProperty((Object)"stream");
                if (dobj != null) {
                    Exceptions.attachMessage((Throwable)((Object)e), (String)FileUtil.getFileDisplayName((FileObject)dobj.getPrimaryFile()));
                }
                throw e;
            }
            if (!lookBack && !ts.moveNext()) {
                return null;
            }
            if (lookBack && !ts.moveNext() && !ts.movePrevious()) {
                return null;
            }
            return ts;
        }
        return null;
    }

    public static Token<GroovyTokenId> getToken(BaseDocument doc, int offset) {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts != null) {
            try {
                ts.move(offset);
            }
            catch (AssertionError e) {
                DataObject dobj = (DataObject)doc.getProperty((Object)"stream");
                if (dobj != null) {
                    Exceptions.attachMessage((Throwable)((Object)e), (String)FileUtil.getFileDisplayName((FileObject)dobj.getPrimaryFile()));
                }
                throw e;
            }
            if (!ts.moveNext() && !ts.movePrevious()) {
                return null;
            }
            Token token = ts.token();
            return token;
        }
        return null;
    }

    public static char getTokenChar(BaseDocument doc, int offset) {
        String text;
        Token<GroovyTokenId> token = LexUtilities.getToken(doc, offset);
        if (token != null && (text = token.text().toString()).length() > 0) {
            return text.charAt(0);
        }
        return '\u0000';
    }

    public static OffsetRange findFwd(BaseDocument doc, TokenSequence<GroovyTokenId> ts, TokenId up, TokenId down) {
        int balance = 0;
        while (ts.moveNext()) {
            Token token = ts.token();
            TokenId id = token.id();
            if (id == up) {
                ++balance;
                continue;
            }
            if (id != down) continue;
            if (balance == 0) {
                return new OffsetRange(ts.offset(), ts.offset() + token.length());
            }
            --balance;
        }
        return OffsetRange.NONE;
    }

    public static OffsetRange findBwd(BaseDocument doc, TokenSequence<GroovyTokenId> ts, TokenId up, TokenId down) {
        int balance = 0;
        while (ts.movePrevious()) {
            Token token = ts.token();
            TokenId id = token.id();
            if (id == up) {
                if (balance == 0) {
                    return new OffsetRange(ts.offset(), ts.offset() + token.length());
                }
                ++balance;
                continue;
            }
            if (id != down) continue;
            --balance;
        }
        return OffsetRange.NONE;
    }

    public static OffsetRange findBegin(BaseDocument doc, TokenSequence<GroovyTokenId> ts) {
        int balance = 0;
        while (ts.movePrevious()) {
            Token token = ts.token();
            TokenId id = token.id();
            if (LexUtilities.isBeginToken(id, doc, ts)) {
                if (balance == 0) {
                    return new OffsetRange(ts.offset(), ts.offset() + token.length());
                }
                --balance;
                continue;
            }
            if (id != GroovyTokenId.RBRACE) continue;
            ++balance;
        }
        return OffsetRange.NONE;
    }

    public static OffsetRange findEnd(BaseDocument doc, TokenSequence<GroovyTokenId> ts) {
        int balance = 0;
        while (ts.moveNext()) {
            Token token = ts.token();
            TokenId id = token.id();
            if (LexUtilities.isBeginToken(id, doc, ts)) {
                --balance;
                continue;
            }
            if (id != GroovyTokenId.RBRACE) continue;
            if (balance == 0) {
                return new OffsetRange(ts.offset(), ts.offset() + token.length());
            }
            ++balance;
        }
        return OffsetRange.NONE;
    }

    public static boolean isEndmatchingDo(BaseDocument doc, int offset) {
        try {
            TokenId id;
            Token<GroovyTokenId> token;
            int first = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            if (first != -1 && (token = LexUtilities.getToken(doc, first)) != null && ((id = token.id()) == GroovyTokenId.LITERAL_while || id == GroovyTokenId.LITERAL_for)) {
                return false;
            }
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
        return true;
    }

    public static boolean isBeginToken(TokenId id, BaseDocument doc, int offset) {
        return END_PAIRS.contains((Object)id);
    }

    public static boolean isBeginToken(TokenId id, BaseDocument doc, TokenSequence<GroovyTokenId> ts) {
        return END_PAIRS.contains((Object)id);
    }

    public static boolean isIndentToken(TokenId id) {
        return INDENT_WORDS.contains((Object)id);
    }

    public static int getBeginEndLineBalance(BaseDocument doc, int offset, boolean upToOffset) {
        try {
            int begin = Utilities.getRowStart((BaseDocument)doc, (int)offset);
            int end = upToOffset ? offset : Utilities.getRowEnd((BaseDocument)doc, (int)offset);
            TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, begin);
            if (ts == null) {
                return 0;
            }
            ts.move(begin);
            if (!ts.moveNext()) {
                return 0;
            }
            int balance = 0;
            do {
                Token token;
                TokenId id;
                if (LexUtilities.isBeginToken(id = (token = ts.token()).id(), doc, ts)) {
                    ++balance;
                    continue;
                }
                if (id != GroovyTokenId.RBRACE) continue;
                --balance;
            } while (ts.moveNext() && ts.offset() <= end);
            return balance;
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
            return 0;
        }
    }

    public static int getLineBalance(BaseDocument doc, int offset, TokenId up, TokenId down) {
        try {
            int begin = Utilities.getRowStart((BaseDocument)doc, (int)offset);
            int end = Utilities.getRowEnd((BaseDocument)doc, (int)offset);
            TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, begin);
            if (ts == null) {
                return 0;
            }
            ts.move(begin);
            if (!ts.moveNext()) {
                return 0;
            }
            int balance = 0;
            do {
                Token token;
                TokenId id;
                if ((id = (token = ts.token()).id()) == up) {
                    ++balance;
                    continue;
                }
                if (id != down) continue;
                --balance;
            } while (ts.moveNext() && ts.offset() <= end);
            return balance;
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
            return 0;
        }
    }

    public static int getTokenBalance(BaseDocument doc, TokenId open, TokenId close, int offset) throws BadLocationException {
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 0);
        if (ts == null) {
            return 0;
        }
        ts.moveIndex(0);
        if (!ts.moveNext()) {
            return 0;
        }
        int balance = 0;
        do {
            Token t;
            if ((t = ts.token()).id() == open) {
                ++balance;
                continue;
            }
            if (t.id() != close) continue;
            --balance;
        } while (ts.moveNext());
        return balance;
    }

    public static boolean isCommentOnlyLine(BaseDocument doc, int offset) throws BadLocationException {
        int begin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (begin == -1) {
            return false;
        }
        if (begin == doc.getLength()) {
            return false;
        }
        return false;
    }

    private static int getLiteralStringOffset(int caretOffset, TokenHierarchy<Document> th, GroovyTokenId begin) {
        Token token;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence(th, caretOffset);
        if (ts == null) {
            return -1;
        }
        ts.move(caretOffset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return -1;
        }
        if (ts.offset() == caretOffset) {
            ts.movePrevious();
        }
        if ((token = ts.token()) != null) {
            TokenId id = token.id();
            while (id == GroovyTokenId.ERROR || id == GroovyTokenId.STRING_LITERAL || id == GroovyTokenId.REGEXP_LITERAL) {
                ts.movePrevious();
                token = ts.token();
                id = token.id();
            }
            if (id == begin) {
                if (!ts.moveNext()) {
                    return -1;
                }
                return ts.offset();
            }
        }
        return -1;
    }

    public static OffsetRange getCommentBlock(BaseDocument doc, int caretOffset) {
        block7 : {
            try {
                Token<GroovyTokenId> token = LexUtilities.getToken(doc, caretOffset);
                if (token == null || token.id() != GroovyTokenId.LINE_COMMENT) break block7;
                int begin = Utilities.getRowStart((BaseDocument)doc, (int)caretOffset);
                int end = Utilities.getRowEnd((BaseDocument)doc, (int)caretOffset);
                if (LexUtilities.isCommentOnlyLine(doc, caretOffset)) {
                    while (begin > 0) {
                        int newBegin = Utilities.getRowStart((BaseDocument)doc, (int)(begin - 1));
                        if (newBegin < 0 || !LexUtilities.isCommentOnlyLine(doc, newBegin)) {
                            begin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)begin);
                            break;
                        }
                        begin = newBegin;
                    }
                    int length = doc.getLength();
                    do {
                        int newEnd;
                        if ((newEnd = Utilities.getRowEnd((BaseDocument)doc, (int)(end + 1))) >= length || !LexUtilities.isCommentOnlyLine(doc, newEnd)) break;
                        end = newEnd;
                    } while (true);
                    end = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)end) + 1;
                    if (begin < end) {
                        return new OffsetRange(begin, end);
                    }
                    break block7;
                }
                TokenHierarchy th = TokenHierarchy.get((Document)doc);
                int offset = token.offset(th);
                return new OffsetRange(offset, offset + token.length());
            }
            catch (BadLocationException ble) {
                Exceptions.printStackTrace((Throwable)ble);
            }
        }
        return OffsetRange.NONE;
    }

    public static int findSpaceBegin(BaseDocument doc, int lexOffset) {
        int lineStart;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, lexOffset);
        if (ts == null) {
            return lexOffset;
        }
        boolean allowPrevLine = false;
        try {
            char c;
            int firstNonWhite;
            lineStart = Utilities.getRowStart((BaseDocument)doc, (int)Math.min(lexOffset, doc.getLength()));
            int prevLast = lineStart - 1;
            if (lineStart > 0 && (prevLast = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)(lineStart - 1))) != -1 && (c = doc.getText(prevLast, 1).charAt(0)) == ',') {
                allowPrevLine = true;
            }
            if (!allowPrevLine) {
                firstNonWhite = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)lineStart);
                if (lexOffset <= firstNonWhite || firstNonWhite == -1) {
                    return lexOffset;
                }
            } else {
                firstNonWhite = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)lineStart);
                if (prevLast >= 0 && (lexOffset <= firstNonWhite || firstNonWhite == -1)) {
                    return prevLast + 1;
                }
                lineStart = 0;
            }
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
            return lexOffset;
        }
        ts.move(lexOffset);
        if (ts.moveNext()) {
            if (lexOffset > ts.offset()) {
                return Math.max(ts.token().id() == GroovyTokenId.WHITESPACE ? ts.offset() : lexOffset, lineStart);
            }
            while (ts.movePrevious()) {
                Token token = ts.token();
                if (token.id() == GroovyTokenId.WHITESPACE) continue;
                return Math.max(ts.offset() + token.length(), lineStart);
            }
        }
        return lexOffset;
    }

    public static Token<GroovyTokenId> findPreviousNonWsNonComment(TokenSequence<GroovyTokenId> ts) {
        return LexUtilities.findPrevious(ts, WHITESPACES_AND_COMMENTS);
    }

    private static Token<GroovyTokenId> findPrevious(TokenSequence<GroovyTokenId> ts, Set<TokenId> ignores) {
        ts.movePrevious();
        if (ignores.contains((Object)ts.token().id())) {
            while (ts.movePrevious() && ignores.contains((Object)ts.token().id())) {
            }
        }
        return ts.token();
    }

    static {
        WHITESPACES_AND_COMMENTS.add(GroovyTokenId.WHITESPACE);
        WHITESPACES_AND_COMMENTS.add(GroovyTokenId.NLS);
        WHITESPACES_AND_COMMENTS.add(GroovyTokenId.EOL);
        WHITESPACES_AND_COMMENTS.add(GroovyTokenId.LINE_COMMENT);
        WHITESPACES_AND_COMMENTS.add(GroovyTokenId.BLOCK_COMMENT);
        END_PAIRS.add(GroovyTokenId.LBRACE);
        INDENT_WORDS.addAll(END_PAIRS);
        INDENT_WORDS.add(GroovyTokenId.COLON);
        INDENT_WORDS.add(GroovyTokenId.LITERAL_case);
        INDENT_WORDS.add(GroovyTokenId.LITERAL_default);
    }
}

