/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.expr.ArgumentListExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.NamedArgumentListExpression
 */
package org.netbeans.modules.groovy.editor.api;

import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.VariableElement;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.NamedArgumentListExpression;
import org.netbeans.modules.groovy.editor.api.ElementUtils;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedMethod;

public class Methods {
    public static boolean isSameMethod(ExecutableElement javaMethod, MethodCallExpression methodCall) {
        ConstantExpression methodName = (ConstantExpression)methodCall.getMethod();
        if (javaMethod.getSimpleName().contentEquals(methodName.getText()) && Methods.getParameterCount(methodCall) == javaMethod.getParameters().size()) {
            return true;
        }
        return false;
    }

    public static boolean isSameMethod(MethodNode methodNode, MethodCallExpression methodCall) {
        if (methodNode.getName().equals(methodCall.getMethodAsString()) && Methods.getParameterCount(methodCall) == methodNode.getParameters().length) {
            return true;
        }
        return false;
    }

    public static boolean isSameMethod(MethodNode methodNode1, MethodNode methodNode2) {
        Parameter[] params2;
        Parameter[] params1;
        if (methodNode1.getName().equals(methodNode2.getName()) && (params1 = methodNode1.getParameters()).length == (params2 = methodNode2.getParameters()).length) {
            for (int i = 0; i < params1.length; ++i) {
                ClassNode type2;
                ClassNode type1 = params1[i].getType();
                if (type1.equals((Object)(type2 = params2[i].getType()))) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isSameMethod(MethodCallExpression methodCall1, MethodCallExpression methodCall2) {
        String method1 = methodCall1.getMethodAsString();
        if (method1 != null && method1.equals(methodCall2.getMethodAsString())) {
            int size1 = Methods.getParameterCount(methodCall1);
            int size2 = Methods.getParameterCount(methodCall2);
            if (size1 >= 0 && size1 == size2) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSameConstructor(ConstructorNode constructor, ConstructorCallExpression call) {
        if (constructor.getDeclaringClass().getNameWithoutPackage().equals(call.getType().getNameWithoutPackage()) && Methods.getParameterCount(call) == constructor.getParameters().length) {
            return true;
        }
        return false;
    }

    public static boolean isSameConstuctor(ConstructorCallExpression call1, ConstructorCallExpression call2) {
        String constructor1 = call1.getType().getNameWithoutPackage();
        if (constructor1 != null && constructor1.equals(call2.getType().getNameWithoutPackage())) {
            int size1 = Methods.getParameterCount(call1);
            int size2 = Methods.getParameterCount(call2);
            if (size1 >= 0 && size1 == size2) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSameConstructor(ConstructorNode constructor1, ConstructorNode constructor2) {
        return Methods.isSameMethod((MethodNode)constructor1, (MethodNode)constructor2);
    }

    private static int getParameterCount(MethodCallExpression methodCall) {
        Expression expression = methodCall.getArguments();
        if (expression instanceof ArgumentListExpression) {
            return ((ArgumentListExpression)expression).getExpressions().size();
        }
        if (expression instanceof NamedArgumentListExpression) {
            return 1;
        }
        return -1;
    }

    private static int getParameterCount(ConstructorCallExpression constructorCall) {
        Expression expression = constructorCall.getArguments();
        if (expression instanceof ArgumentListExpression) {
            return ((ArgumentListExpression)expression).getExpressions().size();
        }
        if (expression instanceof NamedArgumentListExpression) {
            return 1;
        }
        return -1;
    }

    public static boolean hasSameParameters(IndexedMethod indexedMethod, MethodNode method) {
        return Methods.isSameList(Methods.getMethodParams(indexedMethod), Methods.getMethodParams(method));
    }

    public static boolean hasSameParameters(IndexedMethod indexedMethod, MethodCallExpression methodCall) {
        return Methods.isSameList(Methods.getMethodParams(indexedMethod), Methods.getMethodParams(methodCall));
    }

    public static boolean hasSameParameters(MethodNode methodNode, MethodCallExpression methodCall) {
        return Methods.isSameList(Methods.getMethodParams(methodNode), Methods.getMethodParams(methodCall));
    }

    public static boolean isSameList(List<String> list1, List<String> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list1.size(); ++i) {
            if (list1.get(i).equals(list2.get(i))) continue;
            return false;
        }
        return true;
    }

    private static List<String> getMethodParams(IndexedMethod indexedMethod) {
        ArrayList<String> paramTypes = new ArrayList<String>();
        if (indexedMethod != null) {
            List<MethodElement.MethodParameter> parameters = indexedMethod.getParameters();
            for (MethodElement.MethodParameter param : parameters) {
                paramTypes.add(param.getType());
            }
        }
        return paramTypes;
    }

    private static List<String> getMethodParams(MethodNode methodNode) {
        ArrayList<String> params = new ArrayList<String>();
        for (Parameter param : methodNode.getParameters()) {
            params.add(ElementUtils.getTypeName((ASTNode)param));
        }
        return params;
    }

    private static List<String> getMethodParams(MethodCallExpression methodCall) {
        ArgumentListExpression argumentList;
        ArrayList<String> params = new ArrayList<String>();
        Expression arguments = methodCall.getArguments();
        if (arguments instanceof ArgumentListExpression && (argumentList = (ArgumentListExpression)arguments).getExpressions().size() > 0) {
            for (Expression argument : argumentList.getExpressions()) {
                params.add(ElementUtils.getTypeName((ASTNode)argument.getType()));
            }
        }
        return params;
    }
}

