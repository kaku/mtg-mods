/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassCodeVisitorSupport
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.spi.DefaultCompletionResult
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.completion;

import java.util.ArrayList;
import java.util.List;
import org.codehaus.groovy.ast.ClassCodeVisitorSupport;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.spi.DefaultCompletionResult;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.imports.ImportHelper;
import org.netbeans.modules.groovy.editor.imports.ImportUtils;
import org.openide.filesystems.FileObject;

public class GroovyCompletionResult
extends DefaultCompletionResult {
    private final CompletionContext context;
    private final ModuleNode root;
    private final FileObject fo;

    public GroovyCompletionResult(List<CompletionProposal> list, CompletionContext context) {
        super(list, false);
        this.context = context;
        this.root = ASTUtils.getRoot(context.getParserResult());
        this.fo = context.getSourceFile();
    }

    public void afterInsert(@NonNull CompletionProposal item) {
        CompletionItem.TypeItem typeItem;
        List<String> imports;
        if (this.root == null) {
            return;
        }
        if (this.context.isBehindImportStatement()) {
            return;
        }
        if (item instanceof CompletionItem.TypeItem && ImportUtils.isDefaultlyImported((typeItem = (CompletionItem.TypeItem)item).getFqn())) {
            return;
        }
        ElementKind kind = item.getKind();
        String name = item.getName();
        if (!(kind != ElementKind.CLASS && kind != ElementKind.INTERFACE && kind != ElementKind.CONSTRUCTOR || (imports = ImportCollector.collect(this.root)).contains(name))) {
            ImportHelper.resolveImport(this.fo, this.root.getPackageName(), name);
        }
    }

    private static final class ImportCollector
    extends ClassCodeVisitorSupport {
        private final ModuleNode moduleNode;
        private final List<String> imports;

        private ImportCollector(ModuleNode moduleNode) {
            this.moduleNode = moduleNode;
            this.imports = new ArrayList<String>();
        }

        public static List<String> collect(ModuleNode root) {
            ImportCollector collector = new ImportCollector(root);
            collector.visitImports(root);
            return collector.imports;
        }

        protected SourceUnit getSourceUnit() {
            return this.moduleNode.getContext();
        }

        public void visitImports(ModuleNode node) {
            for (ImportNode importNode : node.getImports()) {
                this.imports.add(importNode.getType().getNameWithoutPackage());
            }
            super.visitImports(node);
        }
    }

}

