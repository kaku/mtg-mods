/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.ColoringAttributes
 *  org.netbeans.modules.csl.api.OccurrencesFinder
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.parser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.occurrences.VariableScopeVisitor;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.openide.filesystems.FileObject;

public class GroovyOccurrencesFinder
extends OccurrencesFinder<GroovyParserResult> {
    private boolean cancelled;
    private int caretPosition;
    private Map<OffsetRange, ColoringAttributes> occurrences;
    private FileObject file;
    private static final Logger LOG = Logger.getLogger(GroovyOccurrencesFinder.class.getName());

    public Map<OffsetRange, ColoringAttributes> getOccurrences() {
        LOG.log(Level.FINEST, "getOccurrences()\n");
        return this.occurrences;
    }

    protected final synchronized boolean isCancelled() {
        return this.cancelled;
    }

    protected final synchronized void resume() {
        this.cancelled = false;
    }

    public final synchronized void cancel() {
        this.cancelled = true;
    }

    public int getPriority() {
        return 200;
    }

    public final Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void run(GroovyParserResult result, SchedulerEvent event) {
        ModuleNode rootNode;
        LOG.log(Level.FINEST, "run()");
        this.resume();
        if (this.isCancelled()) {
            return;
        }
        FileObject currentFile = result.getSnapshot().getSource().getFileObject();
        if (currentFile != this.file) {
            this.occurrences = null;
            this.file = currentFile;
        }
        if ((rootNode = ASTUtils.getRoot(result)) == null) {
            return;
        }
        int astOffset = ASTUtils.getAstOffset((Parser.Result)result, this.caretPosition);
        if (astOffset == -1) {
            return;
        }
        BaseDocument document = LexUtilities.getDocument(result, false);
        if (document == null) {
            LOG.log(Level.FINEST, "Could not get BaseDocument. It's null");
            return;
        }
        AstPath path = new AstPath((ASTNode)rootNode, astOffset, document);
        ASTNode closest = path.leaf();
        LOG.log(Level.FINEST, "path = {0}", path);
        LOG.log(Level.FINEST, "closest: {0}", (Object)closest);
        if (closest == null) {
            return;
        }
        HashMap highlights = new HashMap<OffsetRange, ColoringAttributes>(100);
        GroovyOccurrencesFinder.highlight(path, highlights, document, this.caretPosition);
        if (this.isCancelled()) {
            return;
        }
        if (highlights.size() > 0) {
            HashMap<OffsetRange, ColoringAttributes> translated = new HashMap<OffsetRange, ColoringAttributes>(2 * highlights.size());
            for (Map.Entry<OffsetRange, ColoringAttributes> entry : highlights.entrySet()) {
                OffsetRange range = LexUtilities.getLexerOffsets(result, entry.getKey());
                if (range == OffsetRange.NONE) continue;
                translated.put(range, entry.getValue());
            }
            highlights = translated;
            this.occurrences = highlights;
        } else {
            this.occurrences = null;
        }
    }

    public void setCaretPosition(int position) {
        this.caretPosition = position;
        LOG.log(Level.FINEST, "\n\nsetCaretPosition() = {0}\n", position);
    }

    /*
     * Enabled aggressive block sorting
     */
    private static void highlight(AstPath path, Map<OffsetRange, ColoringAttributes> highlights, BaseDocument document, int cursorOffset) {
        ASTNode root = path.root();
        assert (root instanceof ModuleNode);
        ModuleNode moduleNode = (ModuleNode)root;
        VariableScopeVisitor scopeVisitor = new VariableScopeVisitor(moduleNode.getContext(), path, document, cursorOffset);
        scopeVisitor.collect();
        Iterator<ASTNode> i$ = scopeVisitor.getOccurrences().iterator();
        while (i$.hasNext()) {
            OffsetRange range;
            ASTNode astNode = i$.next();
            if (astNode instanceof ASTUtils.FakeASTNode) {
                String text = astNode.getText();
                ASTNode orig = ((ASTUtils.FakeASTNode)astNode).getOriginalNode();
                int line = orig.getLineNumber();
                int column = orig.getColumnNumber();
                if (line > 0 && column > 0) {
                    int start = ASTUtils.getOffset(document, line, column);
                    range = ASTUtils.getNextIdentifierByName(document, text, start);
                } else {
                    range = OffsetRange.NONE;
                }
            } else if (astNode instanceof ClassNode && path.leaf() instanceof ClassNode) {
                ClassNode found = (ClassNode)astNode;
                ClassNode leaf = (ClassNode)path.leaf();
                if (found == leaf) {
                    OffsetRange rangeClassName = ASTUtils.getRange(astNode, document);
                    if (!rangeClassName.containsInclusive(cursorOffset)) {
                        highlights.clear();
                        return;
                    }
                    range = rangeClassName;
                } else {
                    range = ASTUtils.getRange(astNode, document);
                }
            } else {
                range = ASTUtils.getRange(astNode, document);
            }
            if (range == OffsetRange.NONE) continue;
            highlights.put(range, ColoringAttributes.MARK_OCCURRENCES);
        }
    }
}

