/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.AnnotatedNode
 *  org.codehaus.groovy.ast.ClassCodeVisitorSupport
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.PropertyExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.ColoringAttributes
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.groovy.editor.language;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotatedNode;
import org.codehaus.groovy.ast.ClassCodeVisitorSupport;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.groovy.editor.api.ASTUtils;

public class SemanticAnalysisVisitor
extends ClassCodeVisitorSupport {
    private final ModuleNode root;
    private final BaseDocument doc;
    private final Map<OffsetRange, Set<ColoringAttributes>> highlights;

    public SemanticAnalysisVisitor(ModuleNode root, BaseDocument document) {
        this.root = root;
        this.doc = document;
        this.highlights = new HashMap<OffsetRange, Set<ColoringAttributes>>();
    }

    public Map<OffsetRange, Set<ColoringAttributes>> annotate() {
        this.highlights.clear();
        for (Object object2 : this.root.getClasses()) {
            this.visitClass((ClassNode)object2);
        }
        for (Object object2 : this.root.getMethods()) {
            this.visitMethod((MethodNode)object2);
        }
        this.visitBlockStatement(this.root.getStatementBlock());
        return this.highlights;
    }

    protected SourceUnit getSourceUnit() {
        return this.root.getContext();
    }

    public void visitField(FieldNode node) {
        if (this.isInSource((AnnotatedNode)node)) {
            OffsetRange range = ASTUtils.getRange((ASTNode)node, this.doc);
            EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.FIELD);
            if (node.isStatic()) {
                attributes.add(ColoringAttributes.STATIC);
            }
            this.highlights.put(range, attributes);
        }
        super.visitField(node);
    }

    public void visitConstructor(ConstructorNode node) {
        if (this.isInSource((AnnotatedNode)node)) {
            OffsetRange range = ASTUtils.getRange((ASTNode)node, this.doc);
            this.highlights.put(range, ColoringAttributes.CONSTRUCTOR_SET);
        }
        super.visitConstructor(node);
    }

    public void visitMethod(MethodNode node) {
        if (this.isInSource((AnnotatedNode)node)) {
            OffsetRange range = ASTUtils.getRange((ASTNode)node, this.doc);
            EnumSet<ColoringAttributes> attributes = EnumSet.of(ColoringAttributes.METHOD);
            if (node.isStatic()) {
                attributes.add(ColoringAttributes.STATIC);
            }
            this.highlights.put(range, attributes);
        }
        super.visitMethod(node);
    }

    public void visitPropertyExpression(PropertyExpression node) {
        super.visitPropertyExpression(node);
    }

    public void visitClass(ClassNode node) {
        if (this.isInSource((AnnotatedNode)node)) {
            OffsetRange range = ASTUtils.getRange((ASTNode)node, this.doc);
            this.highlights.put(range, ColoringAttributes.CLASS_SET);
        }
        super.visitClass(node);
    }

    public void visitVariableExpression(VariableExpression node) {
        Variable var = node.getAccessedVariable();
        if (var instanceof FieldNode && this.isInSource((AnnotatedNode)node)) {
            OffsetRange range = ASTUtils.getRange((ASTNode)node, this.doc);
            this.highlights.put(range, ColoringAttributes.FIELD_SET);
        }
        super.visitVariableExpression(node);
    }

    private boolean isInSource(AnnotatedNode node) {
        return node.getLineNumber() > 0 && !node.hasNoRealSourcePosition();
    }
}

