/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 *  org.openide.modules.InstalledFileLocator
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedField;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedMethod;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.Exceptions;

public final class GroovyIndex {
    private static final Logger LOG = Logger.getLogger(GroovyIndex.class.getName());
    private static final GroovyIndex EMPTY = new GroovyIndex(null);
    private static final String CLUSTER_URL = "cluster:";
    private static String clusterUrl = null;
    private final QuerySupport querySupport;

    private GroovyIndex(QuerySupport querySupport) {
        this.querySupport = querySupport;
    }

    public static GroovyIndex get(Collection<FileObject> roots) {
        try {
            return new GroovyIndex(QuerySupport.forRoots((String)"groovy", (int)8, (FileObject[])roots.toArray((T[])new FileObject[roots.size()])));
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return EMPTY;
        }
    }

    public Set<IndexedClass> getClassesFromPackage(String packageName) {
        HashSet<IndexedClass> result = new HashSet<IndexedClass>();
        for (IndexedClass indexedClass : this.getAllClasses()) {
            String pkgName = GroovyUtils.getPackageName(indexedClass.getFqn());
            if (!packageName.equals(pkgName)) continue;
            result.add(indexedClass);
        }
        return result;
    }

    public Set<IndexedClass> getAllClasses() {
        return this.getClasses(".*", QuerySupport.Kind.REGEXP);
    }

    public Set<IndexedClass> getClasses(String name, QuerySupport.Kind kind) {
        String field;
        String classFqn = null;
        if (name != null && name.endsWith(".") && QuerySupport.Kind.CAMEL_CASE != kind) {
            classFqn = name.substring(0, name.length() - 1);
            name = "";
        }
        HashSet<IndexResult> result = new HashSet<IndexResult>();
        switch (kind) {
            case EXACT: {
                field = "fqn";
                break;
            }
            case PREFIX: 
            case CAMEL_CASE: 
            case REGEXP: {
                field = "class";
                break;
            }
            case CASE_INSENSITIVE_PREFIX: 
            case CASE_INSENSITIVE_REGEXP: {
                field = "class-ig";
                break;
            }
            default: {
                throw new UnsupportedOperationException(kind.toString());
            }
        }
        this.search(field, name, kind, result);
        HashSet<IndexedClass> classes = new HashSet<IndexedClass>();
        for (IndexResult map : result) {
            String simpleName = map.getValue("class");
            if (simpleName == null || kind == QuerySupport.Kind.PREFIX && !simpleName.startsWith(name) || kind == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX && !simpleName.regionMatches(true, 0, name, 0, name.length())) continue;
            if (classFqn != null) {
                if (kind == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX || kind == QuerySupport.Kind.CASE_INSENSITIVE_REGEXP) {
                    if (!classFqn.equalsIgnoreCase(map.getValue("in"))) {
                        continue;
                    }
                } else if (kind == QuerySupport.Kind.CAMEL_CASE) {
                    int idx;
                    String in = map.getValue("in");
                    if (in == null) continue;
                    StringBuilder sb = new StringBuilder();
                    int lastIndex = 0;
                    do {
                        int nextUpper = -1;
                        for (int i = lastIndex + 1; i < classFqn.length(); ++i) {
                            if (!Character.isUpperCase(classFqn.charAt(i))) continue;
                            nextUpper = i;
                            break;
                        }
                        String token = classFqn.substring(lastIndex, (idx = nextUpper) == -1 ? classFqn.length() : idx);
                        sb.append(token);
                        sb.append(idx != -1 ? "[\\p{javaLowerCase}\\p{Digit}_\\$]*" : ".*");
                        lastIndex = idx;
                    } while (idx != -1);
                    Pattern pattern = Pattern.compile(sb.toString());
                    if (!pattern.matcher(in).matches()) {
                        continue;
                    }
                } else if (!classFqn.equals(map.getValue("in"))) continue;
            }
            String attrs = map.getValue("attrs");
            boolean isClass = true;
            if (attrs != null) {
                int flags = IndexedElement.stringToFlag(attrs, 0);
                isClass = (flags & 64) == 0;
            }
            String fqn = map.getValue("fqn");
            classes.add(this.createClass(fqn, simpleName, map));
        }
        return classes;
    }

    public Set<IndexedMethod> getConstructors(String className) {
        HashSet<IndexResult> indexResult = new HashSet<IndexResult>();
        HashSet<IndexedMethod> result = new HashSet<IndexedMethod>();
        this.search("ctor", className, QuerySupport.Kind.PREFIX, indexResult);
        for (IndexResult map : indexResult) {
            String[] constructors;
            for (String constructor : constructors = map.getValues("ctor")) {
                String paramList = constructor.substring(constructor.indexOf(";") + 1, constructor.length());
                String[] params = paramList.split(",");
                ArrayList<MethodElement.MethodParameter> methodParams = new ArrayList<MethodElement.MethodParameter>();
                for (String param : params) {
                    if ("".equals(param.trim())) continue;
                    methodParams.add(new MethodElement.MethodParameter(param, GroovyUtils.stripPackage(param)));
                }
                result.add(new IndexedMethod(map, className, className, "void", methodParams, "", 0));
            }
        }
        return result;
    }

    public Set<IndexedMethod> getMethods(String name, String clz, QuerySupport.Kind kind) {
        HashSet<IndexResult> result = new HashSet<IndexResult>();
        String field = "method";
        QuerySupport.Kind originalKind = kind;
        if (kind == QuerySupport.Kind.EXACT) {
            kind = QuerySupport.Kind.PREFIX;
        }
        this.search(field, name, kind, result);
        HashSet<IndexedMethod> methods = new HashSet<IndexedMethod>();
        for (IndexResult map : result) {
            String fqn;
            String[] signatures;
            if (clz != null && !clz.equals(fqn = map.getValue("fqn")) || (signatures = map.getValues("method")) == null) continue;
            for (String signature : signatures) {
                block9 : {
                    if ((name == null || name.length() == 0) && !Character.isLowerCase(signature.charAt(0)) || kind == QuerySupport.Kind.PREFIX && !signature.startsWith(name) || kind == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX && !signature.regionMatches(true, 0, name, 0, name.length())) continue;
                    if (kind == QuerySupport.Kind.CASE_INSENSITIVE_REGEXP) {
                        int len = signature.length();
                        int end = signature.indexOf(40);
                        if (end == -1 && (end = signature.indexOf(59)) == -1) {
                            end = len;
                        }
                        String n = end != len ? signature.substring(0, end) : signature;
                        try {
                            if (!n.matches(name)) {
                                continue;
                            }
                            break block9;
                        }
                        catch (PatternSyntaxException e) {
                            break block9;
                        }
                    }
                    if (originalKind == QuerySupport.Kind.EXACT && signature.length() > name.length() && signature.charAt(name.length()) != '(' && signature.charAt(name.length()) != ';') continue;
                }
                assert (map != null);
                methods.add(this.createMethod(signature, map));
            }
        }
        return methods;
    }

    public Set<IndexedField> getAllFields(String fqName) {
        return this.getFields(".*", fqName, QuerySupport.Kind.REGEXP);
    }

    public Set<IndexedField> getStaticFields(String fqName) {
        Set<IndexedField> fields = this.getFields(".*", fqName, QuerySupport.Kind.REGEXP);
        HashSet<IndexedField> staticFields = new HashSet<IndexedField>();
        for (IndexedField field : fields) {
            if (!field.getModifiers().contains((Object)Modifier.STATIC)) continue;
            staticFields.add(field);
        }
        return staticFields;
    }

    public Set<IndexedField> getFields(String name, String clz, QuerySupport.Kind kind) {
        boolean inherited = clz == null;
        HashSet<IndexResult> result = new HashSet<IndexResult>();
        String field = "field";
        QuerySupport.Kind originalKind = kind;
        if (kind == QuerySupport.Kind.EXACT) {
            kind = QuerySupport.Kind.PREFIX;
        }
        this.search(field, name, kind, result);
        HashSet<IndexedField> fields = new HashSet<IndexedField>();
        for (IndexResult map : result) {
            String fqn;
            String[] signatures;
            if (clz != null && !clz.equals(fqn = map.getValue("fqn")) || (signatures = map.getValues("field")) == null) continue;
            for (String signature : signatures) {
                block9 : {
                    if ((name == null || name.length() == 0) && !Character.isLowerCase(signature.charAt(0)) || kind == QuerySupport.Kind.PREFIX && !signature.startsWith(name) || kind == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX && !signature.regionMatches(true, 0, name, 0, name.length())) continue;
                    if (kind == QuerySupport.Kind.CASE_INSENSITIVE_REGEXP) {
                        int len = signature.length();
                        int end = signature.indexOf(59);
                        if (end == -1) {
                            end = len;
                        }
                        String n = end != len ? signature.substring(0, end) : signature;
                        try {
                            if (!n.matches(name)) {
                                continue;
                            }
                            break block9;
                        }
                        catch (PatternSyntaxException e) {
                            break block9;
                        }
                    }
                    if (originalKind == QuerySupport.Kind.EXACT && signature.length() > name.length() && signature.charAt(name.length()) != ';') continue;
                }
                assert (map != null);
                fields.add(this.createField(signature, map, inherited));
            }
        }
        return fields;
    }

    public Set<IndexedMethod> getInheritedMethods(String classFqn, String prefix, QuerySupport.Kind kind) {
        HashSet<IndexedMethod> methods = new HashSet<IndexedMethod>();
        HashSet<String> scannedClasses = new HashSet<String>();
        HashSet<String> seenSignatures = new HashSet<String>();
        if (prefix == null) {
            prefix = "";
        }
        this.addMethodsFromClass(prefix, kind, classFqn, methods, seenSignatures, scannedClasses);
        return methods;
    }

    private boolean addMethodsFromClass(String prefix, QuerySupport.Kind kind, String classFqn, Set<IndexedMethod> methods, Set<String> seenSignatures, Set<String> scannedClasses) {
        boolean foundIt;
        if (scannedClasses.contains(classFqn)) {
            return false;
        }
        scannedClasses.add(classFqn);
        String searchField = "fqn";
        HashSet<IndexResult> result = new HashSet<IndexResult>();
        this.search(searchField, classFqn, QuerySupport.Kind.EXACT, result);
        boolean bl = foundIt = result.size() > 0;
        if (!foundIt) {
            return foundIt;
        }
        for (IndexResult map : result) {
            assert (map != null);
            String[] signatures = map.getValues("method");
            if (signatures == null) continue;
            for (String signature : signatures) {
                if (prefix.length() == 0 && !Character.isLowerCase(signature.charAt(0)) || seenSignatures.contains(signature) || !signature.startsWith(prefix)) continue;
                if (kind == QuerySupport.Kind.EXACT) {
                    if (signature.length() > prefix.length() && signature.charAt(prefix.length()) != '(' && signature.charAt(prefix.length()) != ';') {
                        continue;
                    }
                } else assert (kind == QuerySupport.Kind.PREFIX || kind == QuerySupport.Kind.CASE_INSENSITIVE_PREFIX);
                seenSignatures.add(signature);
                IndexedMethod method = this.createMethod(signature, map);
                methods.add(method);
            }
        }
        this.addMethodsFromClass(prefix, kind, "java.lang.Object", methods, seenSignatures, scannedClasses);
        return foundIt;
    }

    private IndexedClass createClass(String fqn, String simpleName, IndexResult map) {
        if (simpleName == null) {
            simpleName = map.getValue("class");
        }
        String attrs = map.getValue("attrs");
        int flags = 0;
        if (attrs != null) {
            flags = IndexedElement.stringToFlag(attrs, 0);
        }
        IndexedClass c = IndexedClass.create(simpleName, fqn, map, attrs, flags);
        return c;
    }

    private IndexedMethod createMethod(String signature, IndexResult map) {
        String clz = map.getValue("class");
        String module = map.getValue("in");
        if (clz == null) {
            clz = module;
        } else if (module != null && module.length() > 0) {
            clz = module + "." + clz;
        }
        int typeIndex = signature.indexOf(59);
        String methodSignature = signature;
        String type = "void";
        if (typeIndex != -1) {
            int endIndex = signature.indexOf(59, typeIndex + 1);
            if (endIndex == -1) {
                endIndex = signature.length();
            }
            type = signature.substring(typeIndex + 1, endIndex);
            methodSignature = signature.substring(0, typeIndex);
        }
        int attributeIndex = signature.indexOf(59, typeIndex + 1);
        String attributes = null;
        int flags = 0;
        if (attributeIndex != -1) {
            flags = IndexedElement.stringToFlag(signature, attributeIndex + 1);
            if (signature.length() > attributeIndex + 1) {
                attributes = signature.substring(attributeIndex + 1, signature.length());
            }
        }
        return new IndexedMethod(map, clz, this.getMethodName(methodSignature), type, this.getMethodParameter(methodSignature), attributes, flags);
    }

    private String getMethodName(String methodSignature) {
        int parenIndex = methodSignature.indexOf(40);
        if (parenIndex == -1) {
            return methodSignature;
        }
        return methodSignature.substring(0, parenIndex);
    }

    private List<MethodElement.MethodParameter> getMethodParameter(String methodSignature) {
        int parenIndex = methodSignature.indexOf(40);
        if (parenIndex == -1) {
            return Collections.emptyList();
        }
        String argsPortion = methodSignature.substring(parenIndex + 1, methodSignature.length() - 1);
        String[] args = argsPortion.split(",");
        if (args == null || args.length <= 0) {
            return Collections.emptyList();
        }
        ArrayList<MethodElement.MethodParameter> parameters = new ArrayList<MethodElement.MethodParameter>();
        for (String paramType : args) {
            parameters.add(new MethodElement.MethodParameter(paramType, GroovyUtils.stripPackage(paramType)));
        }
        return parameters;
    }

    private IndexedField createField(String signature, IndexResult map, boolean inherited) {
        String clz = map.getValue("class");
        String module = map.getValue("in");
        if (clz == null) {
            clz = module;
        } else if (module != null && module.length() > 0) {
            clz = module + "." + clz;
        }
        int typeIndex = signature.indexOf(59);
        String name = signature;
        String type = "java.lang.Object";
        if (typeIndex != -1) {
            int endIndex = signature.indexOf(59, typeIndex + 1);
            if (endIndex == -1) {
                endIndex = signature.length();
            }
            type = signature.substring(typeIndex + 1, endIndex);
            name = signature.substring(0, typeIndex);
        }
        int attributeIndex = signature.indexOf(59, typeIndex + 1);
        String attributes = null;
        int flags = 0;
        if (attributeIndex != -1) {
            flags = IndexedElement.stringToFlag(signature, attributeIndex + 1);
            if (signature.length() > attributeIndex + 1) {
                attributes = signature.substring(attributeIndex + 1, signature.length());
            }
        }
        IndexedField m = IndexedField.create(type, name, clz, map, attributes, flags);
        m.setInherited(inherited);
        return m;
    }

    private boolean search(String key, String name, QuerySupport.Kind kind, Set<IndexResult> result) {
        try {
            result.addAll(this.querySupport.query(key, name, kind, new String[0]));
            return true;
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
            return false;
        }
    }

    public static void setClusterUrl(String url) {
        clusterUrl = url;
    }

    static String getPreindexUrl(String url) {
        String s = GroovyIndex.getClusterUrl();
        if (url.startsWith(s)) {
            return "cluster:" + url.substring(s.length());
        }
        return url;
    }

    static String getClusterUrl() {
        if (clusterUrl == null) {
            File f = InstalledFileLocator.getDefault().locate("modules/org-netbeans-modules-groovy-editor.jar", null, false);
            if (f == null) {
                throw new RuntimeException("Can't find cluster");
            }
            f = new File(f.getParentFile().getParentFile().getAbsolutePath());
            try {
                f = f.getCanonicalFile();
                clusterUrl = f.toURI().toURL().toExternalForm();
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
        return clusterUrl;
    }

}

