/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.AnnotatedNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ConstructorNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.StructureItem
 *  org.netbeans.modules.csl.api.StructureScanner
 *  org.netbeans.modules.csl.api.StructureScanner$Configuration
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotatedNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ConstructorNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTClass;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTElement;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTField;
import org.netbeans.modules.groovy.editor.api.elements.ast.ASTMethod;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Exceptions;

public class StructureAnalyzer
implements StructureScanner {
    private List<ASTElement> structure;
    private Map<ASTClass, Set<FieldNode>> fields;
    private Map<ASTClass, Set<PropertyNode>> properties;
    private List<ASTMethod> methods;
    private static final Logger LOG = Logger.getLogger(StructureAnalyzer.class.getName());

    public AnalysisResult analyze(GroovyParserResult result) {
        return this.scan(result);
    }

    public List<? extends StructureItem> scan(ParserResult info) {
        GroovyParserResult result = ASTUtils.getParseResult((Parser.Result)info);
        AnalysisResult ar = result.getStructure();
        List<ASTElement> elements = ar.getElements();
        ArrayList<GroovyStructureItem> itemList = new ArrayList<GroovyStructureItem>(elements.size());
        for (ASTElement e : elements) {
            if (!StructureAnalyzer.isVisible(e)) continue;
            itemList.add(new GroovyStructureItem(e, info));
        }
        return itemList;
    }

    private AnalysisResult scan(GroovyParserResult result) {
        AnalysisResult analysisResult = new AnalysisResult();
        ModuleNode root = ASTUtils.getRoot(result);
        if (root == null) {
            return analysisResult;
        }
        this.structure = new ArrayList<ASTElement>();
        this.fields = new HashMap<ASTClass, Set<FieldNode>>();
        this.methods = new ArrayList<ASTMethod>();
        this.properties = new HashMap<ASTClass, Set<PropertyNode>>();
        AstPath path = new AstPath();
        path.descend((ASTNode)root);
        this.scan(result, (ASTNode)root, path, null, null, null);
        path.ascend();
        HashMap<String, FieldNode> names = new HashMap<String, FieldNode>();
        for (ASTClass clz : this.fields.keySet()) {
            Set<FieldNode> assignments = this.fields.get(clz);
            if (assignments == null) continue;
            for (FieldNode assignment : assignments) {
                names.put(assignment.getName(), assignment);
            }
            for (FieldNode field : names.values()) {
                String fieldName = field.getName();
                boolean found = false;
                Set<PropertyNode> nodes = this.properties.get(clz);
                if (nodes != null) {
                    for (PropertyNode node : nodes) {
                        if (!fieldName.equals(node.getName())) continue;
                        found = true;
                        break;
                    }
                }
                boolean isProperty = false;
                if (found) {
                    isProperty = true;
                }
                clz.addChild(new ASTField(field, clz.getFqn(), isProperty));
            }
            names.clear();
        }
        analysisResult.setElements(this.structure);
        return analysisResult;
    }

    private void scan(GroovyParserResult result, ASTNode node, AstPath path, String in, Set<String> includes, ASTElement parent) {
        if (node instanceof AnnotatedNode && !((AnnotatedNode)node).hasNoRealSourcePosition()) {
            if (node instanceof ClassNode) {
                ClassNode classNode = (ClassNode)node;
                ASTClass co = new ASTClass(classNode, classNode.getName());
                if (parent != null) {
                    parent.addChild(co);
                } else {
                    this.structure.add(co);
                }
                parent = co;
            } else if (node instanceof FieldNode) {
                if (parent instanceof ASTClass) {
                    Set<FieldNode> assignments = this.fields.get(parent);
                    if (assignments == null) {
                        assignments = new HashSet<FieldNode>();
                        this.fields.put((ASTClass)parent, assignments);
                    }
                    assignments.add((FieldNode)node);
                }
            } else if (node instanceof MethodNode) {
                ASTMethod co = new ASTMethod(node, in);
                this.methods.add(co);
                if (parent != null) {
                    parent.addChild(co);
                } else {
                    this.structure.add(co);
                }
            } else if (node instanceof PropertyNode) {
                Set<PropertyNode> declarations = this.properties.get(parent);
                if (declarations == null) {
                    declarations = new HashSet<PropertyNode>();
                    this.properties.put((ASTClass)parent, declarations);
                }
                declarations.add((PropertyNode)node);
            }
        }
        List<ASTNode> list = ASTUtils.children(node);
        for (ASTNode child : list) {
            path.descend(child);
            this.scan(result, child, path, in, includes, parent);
            path.ascend();
        }
    }

    public Map<String, List<OffsetRange>> folds(ParserResult info) {
        ModuleNode root = ASTUtils.getRoot(info);
        if (root == null) {
            return Collections.emptyMap();
        }
        GroovyParserResult rpr = ASTUtils.getParseResult((Parser.Result)info);
        AnalysisResult analysisResult = rpr.getStructure();
        HashMap<String, List<OffsetRange>> folds = new HashMap<String, List<OffsetRange>>();
        ArrayList<OffsetRange> codefolds = new ArrayList<OffsetRange>();
        folds.put("codeblocks", codefolds);
        final BaseDocument doc = LexUtilities.getDocument(rpr, false);
        if (doc == null) {
            return Collections.emptyMap();
        }
        final OffsetRange[] importsRange = new OffsetRange[1];
        final ArrayList commentsRanges = new ArrayList();
        doc.render(new Runnable(){

            @Override
            public void run() {
                TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 1);
                int importStart = 0;
                int importEnd = 0;
                boolean startSet = false;
                while (ts != null && ts.isValid() && ts.moveNext()) {
                    StringBuffer sb;
                    Token t = ts.token();
                    if (t.id() == GroovyTokenId.LITERAL_import) {
                        int offset = ts.offset();
                        if (!startSet) {
                            importStart = offset;
                            startSet = true;
                        }
                        importEnd = offset;
                        continue;
                    }
                    if (t.id() != GroovyTokenId.BLOCK_COMMENT || (sb = new StringBuffer(t.text())).indexOf("\n") == -1) continue;
                    int offset = ts.offset();
                    commentsRanges.add(new OffsetRange(offset, offset + t.length()));
                }
                try {
                    importEnd = Utilities.getRowEnd((BaseDocument)doc, (int)importEnd);
                    importsRange[0] = new OffsetRange(importStart, importEnd);
                }
                catch (BadLocationException ble) {
                    Exceptions.printStackTrace((Throwable)ble);
                }
            }
        });
        if (!commentsRanges.isEmpty()) {
            folds.put("comments", commentsRanges);
        }
        try {
            if (importsRange[0] != null && Utilities.getRowCount((BaseDocument)doc, (int)importsRange[0].getStart(), (int)importsRange[0].getEnd()) > 1) {
                folds.put("imports", Collections.singletonList(importsRange[0]));
            }
            this.addFolds(doc, analysisResult.getElements(), folds, codefolds);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return folds;
    }

    private void addFolds(BaseDocument doc, List<? extends ASTElement> elements, Map<String, List<OffsetRange>> folds, List<OffsetRange> codeblocks) throws BadLocationException {
        for (ASTElement element : elements) {
            List<ASTElement> children;
            ElementKind kind = element.getKind();
            switch (kind) {
                case FIELD: 
                case METHOD: 
                case CONSTRUCTOR: 
                case CLASS: 
                case MODULE: {
                    ASTNode node = element.getNode();
                    OffsetRange range = ASTUtils.getRangeFull(node, doc);
                    if (!(kind == ElementKind.METHOD && !((MethodNode)node).isSynthetic() || kind == ElementKind.CONSTRUCTOR && !((ConstructorNode)node).isSynthetic() || kind == ElementKind.FIELD && ((FieldNode)node).getInitialExpression() instanceof ClosureExpression) && (range.getStart() <= Utilities.getRowStart((BaseDocument)doc, (int)range.getStart()) || kind == ElementKind.FIELD)) break;
                    int start = range.getStart();
                    if ((start = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)start)) >= 0 && doc.getChars(start, 1)[0] != '{') {
                        ++start;
                    }
                    int end = range.getEnd();
                    if (start == -1 || end == -1 || start >= end || end > doc.getLength()) break;
                    range = new OffsetRange(start, end);
                    codeblocks.add(range);
                }
            }
            if ((children = element.getChildren()) == null || children.size() <= 0) continue;
            this.addFolds(doc, children, folds, codeblocks);
        }
    }

    public StructureScanner.Configuration getConfiguration() {
        return null;
    }

    private static boolean isVisible(ASTElement element) {
        if (element.getKind() == ElementKind.METHOD) {
            ASTMethod method = (ASTMethod)element;
            ASTNode node = method.getNode();
            return !(node instanceof MethodNode) || !((MethodNode)node).isSynthetic() && ((MethodNode)node).getLineNumber() >= 0;
        }
        return true;
    }

    private static class GroovyStructureItem
    implements StructureItem {
        private final ASTElement node;
        private final ElementKind kind;
        private final ParserResult info;
        @NullAllowed
        private final BaseDocument doc;

        private GroovyStructureItem(ASTElement node, ParserResult info) {
            this.node = node;
            this.kind = node.getKind();
            this.info = info;
            this.doc = (BaseDocument)info.getSnapshot().getSource().getDocument(false);
        }

        public String getName() {
            return this.node.getName();
        }

        public String getHtml(HtmlFormatter formatter) {
            if (this.kind == ElementKind.METHOD || this.kind == ElementKind.CONSTRUCTOR) {
                return this.getMethodHTML(formatter, (ASTMethod)this.node);
            }
            if (this.kind == ElementKind.FIELD) {
                return this.getFieldHTML(formatter, (ASTField)this.node);
            }
            formatter.appendText(this.node.getName());
            return formatter.getText();
        }

        private String getMethodHTML(HtmlFormatter formatter, ASTMethod method) {
            this.appendMethodName(formatter, method);
            this.appendParameters(formatter, method.getParameters());
            this.appendReturnType(formatter, method.getReturnType());
            return formatter.getText();
        }

        private void appendMethodName(HtmlFormatter formatter, ASTMethod method) {
            formatter.appendHtml(method.getName());
        }

        private void appendParameters(HtmlFormatter formatter, Collection<MethodElement.MethodParameter> params) {
            if (!params.isEmpty()) {
                formatter.appendHtml("(");
                formatter.parameters(true);
                Iterator<MethodElement.MethodParameter> it = params.iterator();
                while (it.hasNext()) {
                    MethodElement.MethodParameter param = it.next();
                    formatter.appendText(param.toString());
                    if (!it.hasNext()) continue;
                    formatter.appendHtml(", ");
                }
                formatter.parameters(false);
                formatter.appendHtml(")");
            } else {
                formatter.appendHtml("()");
            }
        }

        private void appendReturnType(HtmlFormatter formatter, String returnType) {
            if (returnType != null) {
                formatter.appendHtml(" : ");
                formatter.parameters(true);
                formatter.appendHtml(returnType);
                formatter.parameters(false);
            }
        }

        private String getFieldHTML(HtmlFormatter formatter, ASTField field) {
            formatter.appendText(field.getName());
            formatter.appendText(" : ");
            formatter.parameters(true);
            formatter.appendText(field.getType());
            formatter.parameters(false);
            return formatter.getText();
        }

        public ElementHandle getElementHandle() {
            return this.node;
        }

        public ElementKind getKind() {
            return this.kind;
        }

        public Set<Modifier> getModifiers() {
            return this.node.getModifiers();
        }

        public boolean isLeaf() {
            switch (this.kind) {
                case FIELD: 
                case METHOD: 
                case CONSTRUCTOR: 
                case ATTRIBUTE: 
                case PROPERTY: 
                case CONSTANT: 
                case KEYWORD: 
                case VARIABLE: 
                case OTHER: {
                    return true;
                }
                case CLASS: 
                case MODULE: {
                    return false;
                }
            }
            throw new RuntimeException("Unhandled kind: " + (Object)this.kind);
        }

        public List<? extends StructureItem> getNestedItems() {
            List<ASTElement> nested = this.node.getChildren();
            if (nested != null && nested.size() > 0) {
                ArrayList<GroovyStructureItem> children = new ArrayList<GroovyStructureItem>(nested.size());
                for (ASTElement co : nested) {
                    if (!StructureAnalyzer.isVisible(co)) continue;
                    children.add(new GroovyStructureItem(co, this.info));
                }
                return children;
            }
            return Collections.emptyList();
        }

        public long getPosition() {
            if (this.doc != null) {
                OffsetRange range = ASTUtils.getRangeFull(this.node.getNode(), this.doc);
                LOG.log(Level.FINEST, "getPosition(), start: {0}", range.getStart());
                return range.getStart();
            }
            return 0;
        }

        public long getEndPosition() {
            if (this.doc != null) {
                OffsetRange range = ASTUtils.getRangeFull(this.node.getNode(), this.doc);
                LOG.log(Level.FINEST, "getEndPosition(), end: {0}", range.getEnd());
                return range.getEnd();
            }
            return 0;
        }

        public boolean equals(Object o) {
            if (!(o instanceof GroovyStructureItem)) {
                return false;
            }
            GroovyStructureItem d = (GroovyStructureItem)o;
            if (this.kind != d.kind) {
                return false;
            }
            if (!this.getName().equals(d.getName())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 7;
            hash = 29 * hash + (this.getName() != null ? this.getName().hashCode() : 0);
            hash = 29 * hash + (this.kind != null ? this.kind.hashCode() : 0);
            return hash;
        }

        public String toString() {
            return this.getName();
        }

        public String getSortText() {
            return this.getName();
        }

        public ImageIcon getCustomIcon() {
            return null;
        }
    }

    public static final class AnalysisResult {
        private List<ASTElement> elements;

        Set<String> getRequires() {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        List<ASTElement> getElements() {
            if (this.elements == null) {
                return Collections.emptyList();
            }
            return this.elements;
        }

        private void setElements(List<ASTElement> elements) {
            this.elements = elements;
        }
    }

}

