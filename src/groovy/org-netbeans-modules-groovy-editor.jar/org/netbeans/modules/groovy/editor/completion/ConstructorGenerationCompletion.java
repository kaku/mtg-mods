/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ClassNode
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.CompletionProposal
 */
package org.netbeans.modules.groovy.editor.completion;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.groovy.ast.ClassNode;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.groovy.editor.api.completion.CaretLocation;
import org.netbeans.modules.groovy.editor.api.completion.CompletionItem;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.completion.util.ContextHelper;
import org.netbeans.modules.groovy.editor.api.elements.common.MethodElement;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.completion.BaseCompletion;
import org.netbeans.modules.groovy.editor.completion.util.CamelCaseUtil;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;

public class ConstructorGenerationCompletion
extends BaseCompletion {
    @Override
    public boolean complete(List<CompletionProposal> proposals, CompletionContext request, int anchor) {
        LOG.log(Level.FINEST, "-> constructor generation completion");
        if (!this.isValidLocation(request)) {
            return false;
        }
        ClassNode requestedClass = ContextHelper.getSurroundingClassNode(request);
        if (requestedClass == null) {
            LOG.log(Level.FINEST, "No surrounding class found, bail out ...");
            return false;
        }
        String className = GroovyUtils.stripPackage(requestedClass.getName());
        boolean camelCaseMatch = CamelCaseUtil.compareCamelCase(className, request.getPrefix());
        if (camelCaseMatch) {
            LOG.log(Level.FINEST, "Prefix matches Class's CamelCase signature. Adding.");
            proposals.add((CompletionProposal)new CompletionItem.ConstructorItem(className, Collections.EMPTY_LIST, anchor, true));
        }
        return camelCaseMatch;
    }

    private boolean isValidLocation(CompletionContext request) {
        if (request.location != CaretLocation.INSIDE_CLASS) {
            LOG.log(Level.FINEST, "Not inside a class");
            return false;
        }
        if (request.context.before1 != null && request.context.before1.text().toString().equals("new") && request.getPrefix().length() > 0) {
            return false;
        }
        if (request.context.beforeLiteral != null && request.context.beforeLiteral.id() == GroovyTokenId.LITERAL_implements || request.context.beforeLiteral != null && request.context.beforeLiteral.id() == GroovyTokenId.LITERAL_extends) {
            return false;
        }
        if (request.getPrefix() == null || request.getPrefix().length() < 0) {
            return false;
        }
        if (request.context.before1 != null && request.context.before1.id() == GroovyTokenId.IDENTIFIER) {
            request.context.ts.movePrevious();
            Token caretToken = request.context.ts.token();
            if (" ".equals(caretToken.text().toString())) {
                return false;
            }
        }
        if (request.context.beforeLiteral != null && request.context.beforeLiteral.id() == GroovyTokenId.LITERAL_class) {
            return false;
        }
        return true;
    }
}

