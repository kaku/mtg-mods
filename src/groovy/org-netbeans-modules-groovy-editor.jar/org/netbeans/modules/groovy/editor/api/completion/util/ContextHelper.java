/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ClosureExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.api.completion.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionContext;
import org.netbeans.modules.groovy.editor.api.completion.util.CompletionSurrounding;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedField;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public final class ContextHelper {
    protected static final Logger LOG = Logger.getLogger(ContextHelper.class.getName());

    private ContextHelper() {
    }

    public static List<ClassNode> getDeclaredClasses(CompletionContext request) {
        if (request.path == null) {
            LOG.log(Level.FINEST, "path == null");
            return Collections.EMPTY_LIST;
        }
        for (ASTNode current : request.path) {
            if (!(current instanceof ModuleNode)) continue;
            return ((ModuleNode)current).getClasses();
        }
        return Collections.EMPTY_LIST;
    }

    public static ClassNode getSurroundingClassNode(CompletionContext request) {
        if (request.path == null) {
            LOG.log(Level.FINEST, "path == null");
            return null;
        }
        for (ASTNode current : request.path) {
            if (!(current instanceof ClassNode)) continue;
            ClassNode classNode = (ClassNode)current;
            LOG.log(Level.FINEST, "Found surrounding Class: {0}", classNode.getName());
            return classNode;
        }
        return null;
    }

    public static ModuleNode getSurroundingModuleNode(CompletionContext request) {
        AstPath path = request.path;
        if (path != null) {
            for (ASTNode current : path) {
                if (!(current instanceof ModuleNode)) continue;
                LOG.log(Level.FINEST, "Found ModuleNode");
                return (ModuleNode)current;
            }
        }
        return null;
    }

    public static ASTNode getSurroundingMethodOrClosure(CompletionContext request) {
        if (request.path == null) {
            LOG.log(Level.FINEST, "path == null");
            return null;
        }
        LOG.log(Level.FINEST, "getSurroundingMethodOrClosure() ----------------------------------------");
        LOG.log(Level.FINEST, "Path : {0}", request.path);
        for (ASTNode current : request.path) {
            if (current instanceof MethodNode) {
                MethodNode mn = (MethodNode)current;
                LOG.log(Level.FINEST, "Found Method: {0}", mn.getName());
                return mn;
            }
            if (current instanceof FieldNode) {
                FieldNode fn = (FieldNode)current;
                if (!fn.isClosureSharedVariable()) continue;
                LOG.log(Level.FINEST, "Found Closure(Field): {0}", fn.getName());
                return fn;
            }
            if (!(current instanceof ClosureExpression)) continue;
            LOG.log(Level.FINEST, "Found Closure(Expr.): {0}", ((ClosureExpression)current).getText());
            return current;
        }
        return null;
    }

    public static boolean isConstructorCall(CompletionContext request) {
        if (request.getPrefix().length() > 0) {
            if (ContextHelper.isEqualsNew(request.context.before1)) {
                return true;
            }
            if (ContextHelper.isEqualsNew(request.context.before2)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isEqualsNew(Token<GroovyTokenId> token) {
        if (token != null && token.text().toString().equals("new")) {
            return true;
        }
        return false;
    }

    public static boolean isAfterComma(CompletionContext request) {
        if (request.context.before2.id() == GroovyTokenId.COMMA || request.context.before1.id() == GroovyTokenId.COMMA || request.context.active.id() == GroovyTokenId.COMMA) {
            return true;
        }
        return false;
    }

    public static boolean isAfterLeftParenthesis(CompletionContext request) {
        if (request.context.before2.id() == GroovyTokenId.LPAREN || request.context.before1.id() == GroovyTokenId.LPAREN || request.context.active.id() == GroovyTokenId.LPAREN) {
            return true;
        }
        return false;
    }

    public static boolean isVariableNameDefinition(CompletionContext request) {
        LOG.log(Level.FINEST, "checkForVariableDefinition()");
        CompletionSurrounding ctx = request.context;
        if (ctx == null || ctx.before1 == null) {
            return false;
        }
        GroovyTokenId id = (GroovyTokenId)ctx.before1.id();
        switch (id) {
            case LITERAL_boolean: 
            case LITERAL_byte: 
            case LITERAL_char: 
            case LITERAL_double: 
            case LITERAL_float: 
            case LITERAL_int: 
            case LITERAL_long: 
            case LITERAL_short: 
            case LITERAL_def: {
                LOG.log(Level.FINEST, "LITERAL_* discovered");
                return true;
            }
            case IDENTIFIER: {
                ASTNode node = ContextHelper.getASTNodeForToken(ctx.before1, request);
                LOG.log(Level.FINEST, "getASTNodeForToken(ASTNode) : {0}", (Object)node);
                if (node != null && (node instanceof ClassExpression || node instanceof DeclarationExpression)) {
                    LOG.log(Level.FINEST, "ClassExpression or DeclarationExpression discovered");
                    return true;
                }
                return false;
            }
        }
        LOG.log(Level.FINEST, "default:");
        return false;
    }

    public static boolean isFieldNameDefinition(CompletionContext request) {
        LOG.log(Level.FINEST, "isFieldDefinitionLine()");
        CompletionSurrounding ctx = request.context;
        if (ctx == null || ctx.before1 == null) {
            return false;
        }
        ASTNode node = ContextHelper.getASTNodeForToken(ctx.before1, request);
        if (node != null) {
            if (node instanceof PropertyNode) {
                return true;
            }
            if (node instanceof ClassNode && ctx.before2 == null && ctx.after1 == null && ctx.after2 == null && ctx.afterLiteral == null) {
                if ("keyword".equals(((GroovyTokenId)ctx.before1.id()).primaryCategory())) {
                    return false;
                }
                return true;
            }
            if (node instanceof FieldNode && ctx.before2 != null) {
                if (ctx.before1.id() == GroovyTokenId.IDENTIFIER || ctx.before2.id() == GroovyTokenId.IDENTIFIER) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    private static ASTNode getASTNodeForToken(Token<GroovyTokenId> tokenId, CompletionContext request) {
        LOG.log(Level.FINEST, "getASTNodeForToken()");
        TokenHierarchy th = TokenHierarchy.get((Document)request.doc);
        int position = tokenId.offset(th);
        ModuleNode rootNode = ASTUtils.getRoot(request.getParserResult());
        if (rootNode == null) {
            return null;
        }
        int astOffset = ASTUtils.getAstOffset((Parser.Result)request.getParserResult(), position);
        if (astOffset == -1) {
            return null;
        }
        BaseDocument document = (BaseDocument)request.getParserResult().getSnapshot().getSource().getDocument(false);
        if (document == null) {
            LOG.log(Level.FINEST, "Could not get BaseDocument. It's null");
            return null;
        }
        AstPath path = new AstPath((ASTNode)rootNode, astOffset, document);
        ASTNode node = path.leaf();
        LOG.log(Level.FINEST, "path = {0}", path);
        LOG.log(Level.FINEST, "node: {0}", (Object)node);
        return node;
    }

    public static List<String> getProperties(CompletionContext context) {
        FileObject f = context.getParserResult().getSnapshot().getSource().getFileObject();
        if (f == null) {
            return Collections.emptyList();
        }
        GroovyIndex index = GroovyIndex.get(QuerySupport.findRoots((FileObject)f, Collections.singleton("classpath/source"), Collections.emptySet(), Collections.emptySet()));
        ArrayList<String> result = new ArrayList<String>();
        for (IndexedField indexedField : index.getAllFields(context.getTypeName())) {
            if (indexedField.isStatic() || !indexedField.isProperty()) continue;
            result.add(indexedField.getName());
        }
        return result;
    }

}

