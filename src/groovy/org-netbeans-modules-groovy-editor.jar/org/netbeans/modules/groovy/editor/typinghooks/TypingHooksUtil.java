/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.EditorOptions
 */
package org.netbeans.modules.groovy.editor.typinghooks;

import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.EditorOptions;

public final class TypingHooksUtil {
    private TypingHooksUtil() {
    }

    public static boolean isMatchingBracketsEnabled(BaseDocument doc) {
        EditorOptions options = EditorOptions.get((String)"text/x-groovy");
        if (options != null) {
            return options.getMatchBrackets();
        }
        return true;
    }
}

