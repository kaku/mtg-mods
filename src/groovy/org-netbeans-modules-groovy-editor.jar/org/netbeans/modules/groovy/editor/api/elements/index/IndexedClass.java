/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 */
package org.netbeans.modules.groovy.editor.api.elements.index;

import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.elements.common.ClassElement;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedElement;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;

public final class IndexedClass
extends IndexedElement
implements ClassElement {
    public static final int MODULE = 64;
    private final String simpleName;

    protected IndexedClass(IndexResult result, String fqn, String simpleName, String attributes, int flags) {
        super(result, fqn, attributes, flags);
        this.simpleName = simpleName;
    }

    public static IndexedClass create(String simpleName, String fqn, IndexResult result, String attributes, int flags) {
        IndexedClass c = new IndexedClass(result, fqn, simpleName, attributes, flags);
        return c;
    }

    @Override
    public String getSignature() {
        return this.in;
    }

    @Override
    public String getName() {
        return this.simpleName;
    }

    @Override
    public ElementKind getKind() {
        return (this.flags & 64) != 0 ? ElementKind.MODULE : ElementKind.CLASS;
    }

    public boolean equals(Object o) {
        if (o instanceof IndexedClass && this.in != null) {
            return this.in.equals(((IndexedClass)o).in);
        }
        return Object.super.equals(o);
    }

    public int hashCode() {
        return this.in == null ? Object.super.hashCode() : this.in.hashCode();
    }

    @Override
    public String getFqn() {
        return this.in;
    }
}

