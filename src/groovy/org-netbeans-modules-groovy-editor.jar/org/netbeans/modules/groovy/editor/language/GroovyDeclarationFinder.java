/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.sun.source.tree.CompilationUnitTree
 *  com.sun.source.tree.Tree
 *  com.sun.source.util.SourcePositions
 *  com.sun.source.util.Trees
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.BinaryExpression
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.Expression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.PropertyExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.api.java.source.ClasspathInfo
 *  org.netbeans.api.java.source.CompilationController
 *  org.netbeans.api.java.source.CompilationInfo
 *  org.netbeans.api.java.source.ElementHandle
 *  org.netbeans.api.java.source.JavaSource
 *  org.netbeans.api.java.source.JavaSource$Phase
 *  org.netbeans.api.java.source.SourceUtils
 *  org.netbeans.api.java.source.Task
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.DeclarationFinder
 *  org.netbeans.modules.csl.api.DeclarationFinder$DeclarationLocation
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.groovy.editor.language;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.Trees;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.CompilationController;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.JavaSource;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.java.source.Task;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.GroovyIndex;
import org.netbeans.modules.groovy.editor.api.Methods;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedClass;
import org.netbeans.modules.groovy.editor.api.elements.index.IndexedMethod;
import org.netbeans.modules.groovy.editor.api.lexer.Call;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.api.parser.GroovyParserResult;
import org.netbeans.modules.groovy.editor.java.ElementDeclaration;
import org.netbeans.modules.groovy.editor.java.ElementSearch;
import org.netbeans.modules.groovy.editor.occurrences.VariableScopeVisitor;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class GroovyDeclarationFinder
implements DeclarationFinder {
    private static final Logger LOG = Logger.getLogger(GroovyDeclarationFinder.class.getName());

    public GroovyDeclarationFinder() {
        LOG.setLevel(Level.OFF);
    }

    public OffsetRange getReferenceSpan(Document document, int lexOffset) {
        TokenHierarchy th = TokenHierarchy.get((Document)document);
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence(document, lexOffset);
        if (ts == null) {
            return OffsetRange.NONE;
        }
        ts.move(lexOffset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return OffsetRange.NONE;
        }
        boolean isBetween = lexOffset == ts.offset();
        OffsetRange range = this.getReferenceSpan(ts, th, lexOffset);
        if (range == OffsetRange.NONE && isBetween && ts.movePrevious()) {
            range = this.getReferenceSpan(ts, th, lexOffset);
        }
        return range;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public DeclarationFinder.DeclarationLocation findDeclaration(ParserResult info, int lexOffset) {
        parserResult = ASTUtils.getParseResult((Parser.Result)info);
        document = LexUtilities.getDocument(parserResult, false);
        if (document == null) {
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        th = TokenHierarchy.get((Document)document);
        doc = document;
        astOffset = ASTUtils.getAstOffset((Parser.Result)info, lexOffset);
        if (astOffset == -1) {
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        range = this.getReferenceSpan((Document)doc, lexOffset);
        if (range == OffsetRange.NONE) {
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        root = ASTUtils.getRoot(info);
        try {
            index = GroovyIndex.get(QuerySupport.findRoots((FileObject)info.getSnapshot().getSource().getFileObject(), Collections.singleton("classpath/source"), (Collection)null, (Collection)null));
            if (root == null) {
                text = doc.getText(range.getStart(), range.getLength());
                if (index == null) return DeclarationFinder.DeclarationLocation.NONE;
                if (text.length() == 0) {
                    return DeclarationFinder.DeclarationLocation.NONE;
                }
                if (!Character.isUpperCase(text.charAt(0))) {
                    methods = index.getMethods(text, null, QuerySupport.Kind.EXACT);
                    l = this.getMethodDeclaration(parserResult, text, methods, null, null, index, astOffset, lexOffset);
                    if (l == null) return DeclarationFinder.DeclarationLocation.NONE;
                    return l;
                }
                classes = index.getClasses(text, QuerySupport.Kind.EXACT);
                if (classes.isEmpty()) {
                    return DeclarationFinder.DeclarationLocation.NONE;
                }
                l = this.getClassDeclaration(parserResult, classes, null, null, index, doc);
                if (l == null) return DeclarationFinder.DeclarationLocation.NONE;
                return l;
            }
            tokenOffset = lexOffset;
            v0 = leftSide = range.getEnd() <= lexOffset;
            if (leftSide && tokenOffset > 0) {
                --tokenOffset;
            }
            path = new AstPath((ASTNode)root, astOffset, doc);
            closest = path.leaf();
            parent = path.leafParent();
            if (closest instanceof ConstantExpression && parent instanceof MethodCallExpression) {
                name = ((ConstantExpression)closest).getText();
                call = Call.getCallType(doc, th, lexOffset);
                type = call.getType();
                lhs = call.getLhs();
                methodCall = (MethodCallExpression)parent;
                objectExpression = methodCall.getObjectExpression();
                if (objectExpression instanceof ClassExpression || objectExpression instanceof VariableExpression) {
                    typeName = objectExpression.getType().getName();
                    fo = parserResult.getSnapshot().getSource().getFileObject();
                    if (fo != null && (location = GroovyDeclarationFinder.findJavaMethod(cpInfo = ClasspathInfo.create((FileObject)fo), typeName, methodCall)) != DeclarationFinder.DeclarationLocation.NONE) {
                        return location;
                    }
                }
                if (type == null && lhs != null && closest != null && call.isSimpleIdentifier()) {
                    if (!GroovyDeclarationFinder.$assertionsDisabled && !(root instanceof ModuleNode)) {
                        throw new AssertionError();
                    }
                    moduleNode = root;
                    scopeVisitor = new VariableScopeVisitor(moduleNode.getContext(), path, doc, lexOffset);
                    scopeVisitor.collect();
                }
                if (type != null) {
                    if (call.isStatic() == false) return DeclarationFinder.DeclarationLocation.NONE;
                }
                fqn = ASTUtils.getFqnName(path);
                if (call != Call.LOCAL) return this.findMethod(name, fqn, type, call, parserResult, astOffset, lexOffset, path, closest, index);
                if (fqn == null) return this.findMethod(name, fqn, type, call, parserResult, astOffset, lexOffset, path, closest, index);
                if (fqn.length() != 0) return this.findMethod(name, fqn, type, call, parserResult, astOffset, lexOffset, path, closest, index);
                fqn = "java.lang.Object";
                return this.findMethod(name, fqn, type, call, parserResult, astOffset, lexOffset, path, closest, index);
            }
            if (closest instanceof VariableExpression) {
                variableExpression = (VariableExpression)closest;
                scope = ASTUtils.getScope(path, (Variable)variableExpression);
                if (scope == null) return DeclarationFinder.DeclarationLocation.NONE;
                variable = ASTUtils.getVariable(scope, variableExpression.getName(), path, doc, lexOffset);
                if (variable == null) return DeclarationFinder.DeclarationLocation.NONE;
                offset = ASTUtils.getRange(variable, doc).getStart();
                return new DeclarationFinder.DeclarationLocation(info.getSnapshot().getSource().getFileObject(), offset);
            }
            if (closest instanceof ConstantExpression && parent instanceof PropertyExpression) {
                propertyExpression = (PropertyExpression)parent;
                objectExpression = propertyExpression.getObjectExpression();
                property = propertyExpression.getProperty();
                if (!(objectExpression instanceof ClassExpression)) {
                    if (objectExpression instanceof VariableExpression == false) return DeclarationFinder.DeclarationLocation.NONE;
                }
                if (property instanceof ConstantExpression == false) return DeclarationFinder.DeclarationLocation.NONE;
                if (objectExpression instanceof VariableExpression && "this".equals(((VariableExpression)objectExpression).getName())) {
                    scope = ASTUtils.getOwningClass(path);
                    if (scope == null) {
                        scope = (ModuleNode)path.root();
                    }
                    if ((variable = ASTUtils.getVariable((ASTNode)scope, ((ConstantExpression)property).getText(), path, doc, lexOffset)) == null) return DeclarationFinder.DeclarationLocation.NONE;
                    offset = ASTUtils.getOffset(doc, variable.getLineNumber(), variable.getColumnNumber());
                    return new DeclarationFinder.DeclarationLocation(info.getSnapshot().getSource().getFileObject(), offset);
                }
                typeName = objectExpression.getType().getName();
                fieldName = ((ConstantExpression)closest).getText();
                fo = parserResult.getSnapshot().getSource().getFileObject();
                if (fo == null) return DeclarationFinder.DeclarationLocation.NONE;
                cpInfo = ClasspathInfo.create((FileObject)fo);
                location = GroovyDeclarationFinder.findJavaField(cpInfo, typeName, fieldName);
                if (location == DeclarationFinder.DeclarationLocation.NONE) return DeclarationFinder.DeclarationLocation.NONE;
                return location;
            }
            if (closest instanceof DeclarationExpression || closest instanceof ConstructorCallExpression || closest instanceof ClassExpression || closest instanceof PropertyNode || closest instanceof FieldNode || closest instanceof Parameter) {
                fqName = this.getFqNameForNode(closest);
                return this.findType(fqName, range, doc, info, index);
            }
            if (closest instanceof ClassNode == false) return DeclarationFinder.DeclarationLocation.NONE;
            node = (ClassNode)closest;
            ts = LexUtilities.getGroovyTokenSequence((Document)doc, lexOffset);
            if (ts == null) {
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            ts.move(lexOffset);
            while (ts.isValid() && ts.movePrevious() && ts.token().id() != GroovyTokenId.LITERAL_class && ts.token().id() != GroovyTokenId.LITERAL_extends && ts.token().id() != GroovyTokenId.LITERAL_implements) {
            }
            if (ts.isValid() == false) return DeclarationFinder.DeclarationLocation.NONE;
            if (ts.token().id() == GroovyTokenId.LITERAL_class) {
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            fqName = null;
            text = doc.getText(range.getStart(), range.getLength());
            if (ts.token().id() != GroovyTokenId.LITERAL_extends) ** GOTO lbl116
            superClass = node.getSuperClass();
            if (!superClass.getName().endsWith(text)) ** GOTO lbl121
            fqName = superClass.getName();
            ** GOTO lbl121
lbl116: // 1 sources:
            if (ts.token().id() == GroovyTokenId.LITERAL_implements) {
                for (ClassNode ifNode : node.getInterfaces()) {
                    if (!ifNode.getName().endsWith(text)) continue;
                    fqName = ifNode.getName();
                    break;
                }
            }
lbl121: // 6 sources:
            if (fqName == null) return DeclarationFinder.DeclarationLocation.NONE;
            return this.findType(fqName, range, doc, info, index);
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private DeclarationFinder.DeclarationLocation findType(String fqName, OffsetRange range, BaseDocument doc, ParserResult info, GroovyIndex index) throws BadLocationException {
        LOG.log(Level.FINEST, "Looking for type: {0}", fqName);
        if (doc != null && range != null) {
            String text = doc.getText(range.getStart(), range.getLength());
            if (!GroovyUtils.stripPackage(fqName).equals(text)) {
                String[] parts = fqName.split(Pattern.quote("$"));
                if (parts.length < 2) {
                    return DeclarationFinder.DeclarationLocation.NONE;
                }
                boolean found = false;
                StringBuilder builder = new StringBuilder();
                for (String part : parts) {
                    builder.append(part).append("$");
                    if (!GroovyUtils.stripPackage(part).equals(text)) continue;
                    found = true;
                    break;
                }
                if (!found) {
                    return DeclarationFinder.DeclarationLocation.NONE;
                }
                builder.setLength(builder.length() - 1);
                fqName = builder.toString();
            }
            Set<IndexedClass> classes = index.getClasses(fqName, QuerySupport.Kind.EXACT);
            for (IndexedClass indexedClass : classes) {
                ASTNode node = ASTUtils.getForeignNode(indexedClass);
                if (node == null) continue;
                OffsetRange defRange = null;
                try {
                    defRange = ASTUtils.getRange(node, (BaseDocument)indexedClass.getDocument());
                }
                catch (IOException ex) {
                    LOG.log(Level.FINEST, "IOException while getting destination range : {0}", ex.getMessage());
                }
                if (defRange == null) continue;
                LOG.log(Level.FINEST, "Found decl. for : {0}", text);
                LOG.log(Level.FINEST, "Foreign Node    : {0}", (Object)node);
                LOG.log(Level.FINEST, "Range start     : {0}", defRange.getStart());
                return new DeclarationFinder.DeclarationLocation(indexedClass.getFileObject(), defRange.getStart());
            }
            FileObject fileObject = info.getSnapshot().getSource().getFileObject();
            if (fileObject != null) {
                ClasspathInfo cpi = ClasspathInfo.create((FileObject)fileObject);
                if (cpi != null) {
                    JavaSource javaSource = JavaSource.create((ClasspathInfo)cpi, (FileObject[])new FileObject[0]);
                    if (javaSource != null) {
                        CountDownLatch latch = new CountDownLatch(1);
                        SourceLocator locator = new SourceLocator(fqName, cpi, latch);
                        try {
                            javaSource.runUserActionTask((Task)locator, false);
                        }
                        catch (IOException ex) {
                            LOG.log(Level.FINEST, "Problem in runUserActionTask :  {0}", ex.getMessage());
                            return DeclarationFinder.DeclarationLocation.NONE;
                        }
                        try {
                            latch.await();
                        }
                        catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                            return DeclarationFinder.DeclarationLocation.NONE;
                        }
                        return locator.getLocation();
                    }
                    LOG.log(Level.FINEST, "javaSource == null");
                } else {
                    LOG.log(Level.FINEST, "classpathinfo == null");
                }
            } else {
                LOG.log(Level.FINEST, "fileObject == null");
            }
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private String getFqNameForNode(ASTNode node) {
        if (node instanceof DeclarationExpression) {
            return ((BinaryExpression)node).getLeftExpression().getType().getName();
        }
        if (node instanceof Expression) {
            return ((Expression)node).getType().getName();
        }
        if (node instanceof PropertyNode) {
            return ((PropertyNode)node).getField().getType().getName();
        }
        if (node instanceof FieldNode) {
            return ((FieldNode)node).getType().getName();
        }
        if (node instanceof Parameter) {
            return ((Parameter)node).getType().getName();
        }
        return "";
    }

    private OffsetRange getReferenceSpan(TokenSequence<?> ts, TokenHierarchy<Document> th, int lexOffset) {
        Token token = ts.token();
        TokenId id = token.id();
        if (id == GroovyTokenId.IDENTIFIER && token.length() == 1 && id == GroovyTokenId.IDENTIFIER && token.text().toString().equals(",")) {
            assert (false);
            return OffsetRange.NONE;
        }
        if (id == GroovyTokenId.IDENTIFIER) {
            return new OffsetRange(ts.offset(), ts.offset() + token.length());
        }
        return OffsetRange.NONE;
    }

    private DeclarationFinder.DeclarationLocation getClassDeclaration(GroovyParserResult info, Set<IndexedClass> classes, AstPath path, ASTNode closest, GroovyIndex index, BaseDocument doc) {
        IndexedClass candidate = this.findBestClassMatch(classes, path, closest, index);
        if (candidate != null) {
            IndexedClass com = candidate;
            ASTNode node = ASTUtils.getForeignNode(com);
            DeclarationFinder.DeclarationLocation loc = new DeclarationFinder.DeclarationLocation(com.getFileObject(), ASTUtils.getOffset(doc, node.getLineNumber(), node.getColumnNumber()), (org.netbeans.modules.csl.api.ElementHandle)com);
            return loc;
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private DeclarationFinder.DeclarationLocation findMethod(String name, String possibleFqn, String type, Call call, GroovyParserResult info, int caretOffset, int lexOffset, AstPath path, ASTNode closest, GroovyIndex index) {
        Set<IndexedMethod> methods = this.getApplicableMethods(name, possibleFqn, type, call, index);
        int astOffset = caretOffset;
        DeclarationFinder.DeclarationLocation l = this.getMethodDeclaration(info, name, methods, path, closest, index, astOffset, lexOffset);
        return l;
    }

    private Set<IndexedMethod> getApplicableMethods(String name, String possibleFqn, String type, Call call, GroovyIndex index) {
        Set methods = new HashSet<IndexedMethod>();
        String fqn = possibleFqn;
        if (type == null && possibleFqn != null && call.getLhs() == null && call != Call.UNKNOWN) {
            fqn = possibleFqn;
            if (methods.isEmpty()) {
                methods = index.getMethods(name, fqn, QuerySupport.Kind.EXACT);
            }
            methods = index.getInheritedMethods(fqn, name, QuerySupport.Kind.EXACT);
        }
        if (type != null && methods.isEmpty()) {
            fqn = possibleFqn;
            if (methods.isEmpty()) {
                methods = index.getInheritedMethods(fqn + "." + type, name, QuerySupport.Kind.EXACT);
            }
            if (methods.isEmpty() && (methods = index.getInheritedMethods(type, name, QuerySupport.Kind.EXACT)).isEmpty() && type.indexOf(".") == -1) {
                Set<IndexedClass> classes = index.getClasses(type, QuerySupport.Kind.EXACT);
                HashSet<String> fqns = new HashSet<String>();
                for (IndexedClass cls : classes) {
                    String f = cls.getFqn();
                    if (f == null) continue;
                    fqns.add(f);
                }
                for (String f : fqns) {
                    if (f.equals(type)) continue;
                    methods.addAll(index.getInheritedMethods(f, name, QuerySupport.Kind.EXACT));
                }
            }
            if (methods.isEmpty()) {
                int f;
                for (fqn = possibleFqn; methods.isEmpty() && fqn != null && fqn.length() > 0; fqn = fqn.substring((int)0, (int)f)) {
                    methods = index.getMethods(name, fqn + "." + type, QuerySupport.Kind.EXACT);
                    f = fqn.lastIndexOf(".");
                    if (f == -1) break;
                }
            }
        }
        if (methods.isEmpty() && (methods = index.getMethods(name, type, QuerySupport.Kind.EXACT)).isEmpty() && type != null) {
            methods = index.getMethods(name, null, QuerySupport.Kind.EXACT);
        }
        return methods;
    }

    private DeclarationFinder.DeclarationLocation getMethodDeclaration(GroovyParserResult info, String name, Set<IndexedMethod> methods, AstPath path, ASTNode closest, GroovyIndex index, int astOffset, int lexOffset) {
        BaseDocument doc = LexUtilities.getDocument(info, false);
        if (doc == null) {
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        IndexedMethod candidate = this.findBestMethodMatch(name, methods, doc, astOffset, lexOffset, path, closest, index);
        if (candidate != null) {
            FileObject fileObject = candidate.getFileObject();
            if (fileObject == null) {
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            ASTNode node = ASTUtils.getForeignNode(candidate);
            int nodeOffset = node != null && node.getLineNumber() > 0 && node.getColumnNumber() > 0 ? ASTUtils.getOffset(doc, node.getLineNumber(), node.getColumnNumber()) : 0;
            DeclarationFinder.DeclarationLocation loc = new DeclarationFinder.DeclarationLocation(fileObject, nodeOffset, (org.netbeans.modules.csl.api.ElementHandle)candidate);
            return loc;
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    IndexedClass findBestClassMatch(Set<IndexedClass> classSet, AstPath path, ASTNode reference, GroovyIndex index) {
        HashSet<IndexedClass> classes = new HashSet<IndexedClass>(classSet);
        while (!classes.isEmpty()) {
            IndexedClass clz = this.findBestClassMatchHelper(classes, path, reference, index);
            if (clz == null) {
                return null;
            }
            ASTNode node = ASTUtils.getForeignNode(clz);
            if (node != null) {
                return clz;
            }
            if (!classes.contains(clz)) {
                classes.remove(classes.iterator().next());
                continue;
            }
            classes.remove(clz);
        }
        return null;
    }

    private IndexedClass findBestClassMatchHelper(Set<IndexedClass> classes, AstPath path, ASTNode reference, GroovyIndex index) {
        return null;
    }

    IndexedMethod findBestMethodMatch(String name, Set<IndexedMethod> methodSet, BaseDocument doc, int astOffset, int lexOffset, AstPath path, ASTNode call, GroovyIndex index) {
        HashSet<IndexedMethod> methods = new HashSet<IndexedMethod>(methodSet);
        while (!methods.isEmpty()) {
            ASTNode node;
            IndexedMethod method = this.findBestMethodMatchHelper(name, methods, doc, astOffset, lexOffset, path, call, index);
            ASTNode aSTNode = node = method == null ? null : ASTUtils.getForeignNode(method);
            if (node != null) {
                return method;
            }
            if (!methods.contains(method)) {
                methods.remove(methods.iterator().next());
                continue;
            }
            methods.remove(method);
        }
        if (methodSet.size() > 0) {
            return methodSet.iterator().next();
        }
        return null;
    }

    private IndexedMethod findBestMethodMatchHelper(String name, Set<IndexedMethod> methods, BaseDocument doc, int astOffset, int lexOffset, AstPath path, ASTNode callNode, GroovyIndex index) {
        HashSet<IndexedMethod> candidates = new HashSet<IndexedMethod>();
        if (path == null) {
            return null;
        }
        ASTNode parent = path.leafParent();
        if (callNode instanceof ConstantExpression && parent instanceof MethodCallExpression) {
            String fqn = null;
            MethodCallExpression methodCall = (MethodCallExpression)parent;
            Expression objectExpression = methodCall.getObjectExpression();
            if (objectExpression instanceof VariableExpression) {
                VariableExpression variable = (VariableExpression)objectExpression;
                fqn = "this".equals(variable.getName()) ? ASTUtils.getFqnName(path) : variable.getType().getName();
            }
            if (fqn != null) {
                for (IndexedMethod method : methods) {
                    if (!fqn.equals(method.getIn())) continue;
                    candidates.add(method);
                }
            }
        }
        if (candidates.size() == 1) {
            return (IndexedMethod)candidates.iterator().next();
        }
        if (!candidates.isEmpty()) {
            methods = candidates;
        }
        return null;
    }

    private static DeclarationFinder.DeclarationLocation findJavaField(ClasspathInfo cpInfo, final String fqn, final String fieldName) {
        final ElementHandle[] handles = new ElementHandle[1];
        final int[] offset = new int[1];
        JavaSource javaSource = JavaSource.create((ClasspathInfo)cpInfo, (FileObject[])new FileObject[0]);
        try {
            FileObject fileObject;
            javaSource.runUserActionTask((Task)new Task<CompilationController>(){

                public void run(CompilationController controller) throws Exception {
                    controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    TypeElement typeElement = ElementSearch.getClass(controller.getElements(), fqn);
                    if (typeElement != null) {
                        for (VariableElement variable : ElementFilter.fieldsIn(typeElement.getEnclosedElements())) {
                            if (!variable.getSimpleName().contentEquals(fieldName)) continue;
                            handles[0] = ElementHandle.create((Element)variable);
                        }
                    }
                }
            }, true);
            if (handles[0] != null && (fileObject = SourceUtils.getFile((ElementHandle)handles[0], (ClasspathInfo)cpInfo)) != null) {
                javaSource = JavaSource.forFileObject((FileObject)fileObject);
                javaSource.runUserActionTask((Task)new Task<CompilationController>(){

                    public void run(CompilationController controller) throws Exception {
                        controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                        Element element = handles[0].resolve((CompilationInfo)controller);
                        Trees trees = controller.getTrees();
                        Tree tree = trees.getTree(element);
                        SourcePositions sourcePositions = trees.getSourcePositions();
                        offset[0] = (int)sourcePositions.getStartPosition(controller.getCompilationUnit(), tree);
                    }
                }, true);
                return new DeclarationFinder.DeclarationLocation(fileObject, offset[0]);
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private static DeclarationFinder.DeclarationLocation findJavaMethod(ClasspathInfo cpInfo, final String fqn, final MethodCallExpression methodCall) {
        final ElementHandle[] handles = new ElementHandle[1];
        final int[] offset = new int[1];
        JavaSource javaSource = JavaSource.create((ClasspathInfo)cpInfo, (FileObject[])new FileObject[0]);
        try {
            FileObject fileObject;
            javaSource.runUserActionTask((Task)new Task<CompilationController>(){

                public void run(CompilationController controller) throws Exception {
                    controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                    TypeElement typeElement = ElementSearch.getClass(controller.getElements(), fqn);
                    if (typeElement != null) {
                        for (ExecutableElement javaMethod : ElementFilter.methodsIn(typeElement.getEnclosedElements())) {
                            if (!Methods.isSameMethod(javaMethod, methodCall)) continue;
                            handles[0] = ElementHandle.create((Element)javaMethod);
                        }
                    }
                }
            }, true);
            if (handles[0] != null && (fileObject = SourceUtils.getFile((ElementHandle)handles[0], (ClasspathInfo)cpInfo)) != null) {
                javaSource = JavaSource.forFileObject((FileObject)fileObject);
                if (javaSource != null) {
                    javaSource.runUserActionTask((Task)new Task<CompilationController>(){

                        public void run(CompilationController controller) throws Exception {
                            controller.toPhase(JavaSource.Phase.ELEMENTS_RESOLVED);
                            Element element = handles[0].resolve((CompilationInfo)controller);
                            Trees trees = controller.getTrees();
                            Tree tree = trees.getTree(element);
                            SourcePositions sourcePositions = trees.getSourcePositions();
                            offset[0] = (int)sourcePositions.getStartPosition(controller.getCompilationUnit(), tree);
                        }
                    }, true);
                }
                return new DeclarationFinder.DeclarationLocation(fileObject, offset[0]);
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private class SourceLocator
    implements Task<CompilationController> {
        private final String fqName;
        private final ClasspathInfo cpi;
        private final CountDownLatch latch;
        private DeclarationFinder.DeclarationLocation location;

        public SourceLocator(String fqName, ClasspathInfo cpi, CountDownLatch latch) {
            this.location = DeclarationFinder.DeclarationLocation.NONE;
            this.fqName = fqName;
            this.cpi = cpi;
            this.latch = latch;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void run(CompilationController info) throws Exception {
            Elements elements = info.getElements();
            if (elements != null) {
                TypeElement typeElement = ElementSearch.getClass(elements, this.fqName);
                if (typeElement != null) {
                    DeclarationFinder.DeclarationLocation found = ElementDeclaration.getDeclarationLocation(this.cpi, typeElement);
                    SourceLocator sourceLocator = this;
                    synchronized (sourceLocator) {
                        this.location = found;
                    }
                } else {
                    LOG.log(Level.FINEST, "typeElement == null");
                }
            } else {
                LOG.log(Level.FINEST, "elements == null");
            }
            this.latch.countDown();
        }

        public synchronized DeclarationFinder.DeclarationLocation getLocation() {
            return this.location;
        }
    }

}

