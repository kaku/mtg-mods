/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.AnnotationNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.GenericsType
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.PackageNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.ArrayExpression
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.TupleExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.CatchStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.groovy.editor.api;

import java.util.List;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotationNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.GenericsType;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.PackageNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.ArrayExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.ElementUtils;

public final class FindTypeUtils {
    private FindTypeUtils() {
    }

    public static boolean isCaretOnClassNode(AstPath path, BaseDocument doc, int caret) {
        if (FindTypeUtils.findCurrentNode(path, doc, caret) instanceof ClassNode) {
            return true;
        }
        return false;
    }

    public static ASTNode findCurrentNode(AstPath path, BaseDocument doc, int caret) {
        ASTNode leaf = path.leaf();
        if (FindTypeUtils.isOnImportNode(path, doc, caret)) {
            return FindTypeUtils.getCurrentImportNode(FindTypeUtils.getCurrentModuleNode(path), doc, caret);
        }
        if (FindTypeUtils.isOnPackageNode(path, doc, caret)) {
            return FindTypeUtils.getCurrentModuleNode(path).getPackage();
        }
        if (leaf instanceof ClassNode) {
            ClassNode classNode = (ClassNode)leaf;
            if (FindTypeUtils.isCaretOnClassNode(classNode, doc, caret)) {
                return classNode;
            }
            ClassNode superClass = classNode.getUnresolvedSuperClass(false);
            if (FindTypeUtils.isCaretOnClassNode(superClass, doc, caret)) {
                return superClass;
            }
            for (ClassNode interfaceNode : classNode.getInterfaces()) {
                if (!FindTypeUtils.isCaretOnClassNode(interfaceNode, doc, caret)) continue;
                return interfaceNode;
            }
            for (AnnotationNode annotation : classNode.getAnnotations()) {
                if (!FindTypeUtils.isCaretOnAnnotation(annotation, doc, caret)) continue;
                return annotation.getClassNode();
            }
        } else if (leaf instanceof FieldNode) {
            FieldNode field = (FieldNode)leaf;
            for (AnnotationNode annotation : field.getAnnotations()) {
                if (!FindTypeUtils.isCaretOnAnnotation(annotation, doc, caret)) continue;
                return annotation.getClassNode();
            }
            if (FindTypeUtils.isCaretOnFieldType(field, doc, caret)) {
                return ElementUtils.getType(leaf);
            }
            if (FindTypeUtils.isCaretOnGenericType(field.getType(), doc, caret)) {
                return FindTypeUtils.getGenericType(field.getType(), doc, caret);
            }
        } else if (leaf instanceof PropertyNode) {
            PropertyNode property = (PropertyNode)leaf;
            FieldNode field = property.getField();
            for (AnnotationNode annotation : field.getAnnotations()) {
                if (!FindTypeUtils.isCaretOnAnnotation(annotation, doc, caret)) continue;
                return annotation.getClassNode();
            }
            if (FindTypeUtils.isCaretOnFieldType(field, doc, caret)) {
                return ElementUtils.getType(leaf);
            }
            if (FindTypeUtils.isCaretOnGenericType(field.getType(), doc, caret)) {
                return FindTypeUtils.getGenericType(field.getType(), doc, caret);
            }
        } else {
            if (leaf instanceof MethodNode) {
                MethodNode method = (MethodNode)leaf;
                for (AnnotationNode annotation : method.getAnnotations()) {
                    if (!FindTypeUtils.isCaretOnAnnotation(annotation, doc, caret)) continue;
                    return annotation.getClassNode();
                }
                if (FindTypeUtils.isCaretOnReturnType(method, doc, caret)) {
                    return ElementUtils.getType(leaf);
                }
                if (FindTypeUtils.isCaretOnGenericType(method.getReturnType(), doc, caret)) {
                    return FindTypeUtils.getGenericType(method.getReturnType(), doc, caret);
                }
                return method;
            }
            if (leaf instanceof Parameter) {
                Parameter param = (Parameter)leaf;
                if (FindTypeUtils.isCaretOnParamType(param, doc, caret)) {
                    return ElementUtils.getType(leaf);
                }
                if (FindTypeUtils.isCaretOnGenericType(param.getType(), doc, caret)) {
                    return FindTypeUtils.getGenericType(param.getType(), doc, caret);
                }
            } else {
                if (leaf instanceof ForStatement) {
                    if (FindTypeUtils.isCaretOnForStatementType((ForStatement)leaf, doc, caret)) {
                        return ((ForStatement)leaf).getVariableType();
                    }
                    return ((ForStatement)leaf).getVariable();
                }
                if (leaf instanceof CatchStatement) {
                    CatchStatement catchStatement = (CatchStatement)leaf;
                    if (FindTypeUtils.isCaretOnCatchStatement(catchStatement, doc, caret)) {
                        return catchStatement.getVariable().getType();
                    }
                } else if (leaf instanceof ClassExpression) {
                    if (FindTypeUtils.isCaretOnClassExpressionType((ClassExpression)leaf, doc, caret)) {
                        return ElementUtils.getType(leaf);
                    }
                } else if (leaf instanceof VariableExpression) {
                    if (FindTypeUtils.isCaretOnVariableType((VariableExpression)leaf, doc, caret)) {
                        return ElementUtils.getType(leaf);
                    }
                } else if (leaf instanceof DeclarationExpression) {
                    DeclarationExpression declaration = (DeclarationExpression)leaf;
                    if (FindTypeUtils.isCaretOnDeclarationType(declaration, doc, caret)) {
                        return ElementUtils.getType(leaf);
                    }
                    ClassNode declarationType = !declaration.isMultipleAssignmentDeclaration() ? declaration.getVariableExpression().getType() : declaration.getTupleExpression().getType();
                    if (FindTypeUtils.isCaretOnGenericType(declarationType, doc, caret)) {
                        return FindTypeUtils.getGenericType(declarationType, doc, caret);
                    }
                } else if (leaf instanceof ArrayExpression) {
                    if (FindTypeUtils.isCaretOnArrayExpressionType((ArrayExpression)leaf, doc, caret)) {
                        return ElementUtils.getType(leaf);
                    }
                } else if (leaf instanceof ConstructorCallExpression) {
                    ClassNode constructorType = ((ConstructorCallExpression)leaf).getType();
                    if (FindTypeUtils.isCaretOnGenericType(constructorType, doc, caret)) {
                        return FindTypeUtils.getGenericType(constructorType, doc, caret);
                    }
                    return leaf;
                }
            }
        }
        return leaf;
    }

    private static boolean isOnImportNode(AstPath path, BaseDocument doc, int caret) {
        ModuleNode moduleNode = FindTypeUtils.getCurrentModuleNode(path);
        if (moduleNode == null) {
            return false;
        }
        if (FindTypeUtils.getCurrentImportNode(moduleNode, doc, caret) == null) {
            return false;
        }
        return true;
    }

    private static ModuleNode getCurrentModuleNode(AstPath path) {
        ASTNode leaf = path.leaf();
        ASTNode leafParent = path.leafParent();
        ModuleNode moduleNode = null;
        if (leaf instanceof ModuleNode) {
            moduleNode = (ModuleNode)leaf;
        } else if (leaf instanceof ClassNode) {
            moduleNode = ((ClassNode)leaf).getModule();
        } else if (leaf instanceof BlockStatement && leafParent instanceof MethodNode && path.root() instanceof ModuleNode) {
            moduleNode = (ModuleNode)path.root();
        }
        return moduleNode;
    }

    private static ClassNode getCurrentImportNode(ModuleNode moduleNode, BaseDocument doc, int caret) {
        for (ImportNode importNode : moduleNode.getImports()) {
            if (!FindTypeUtils.isCaretOnImportStatement(importNode, doc, caret) || importNode.isStar()) continue;
            return ElementUtils.getType((ASTNode)importNode);
        }
        return null;
    }

    private static boolean isOnPackageNode(AstPath path, BaseDocument doc, int caret) {
        ModuleNode moduleNode = FindTypeUtils.getCurrentModuleNode(path);
        if (moduleNode == null || moduleNode.getPackage() == null) {
            return false;
        }
        if (FindTypeUtils.isCaretOnPackageStatement(moduleNode.getPackage(), doc, caret)) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnClassNode(ClassNode classNode, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getClassNodeRange(classNode, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnReturnType(MethodNode method, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getMethodRange(method, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnFieldType(FieldNode field, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getFieldRange(field, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnParamType(Parameter param, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getParameterRange(param, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnForStatementType(ForStatement forLoop, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getForLoopRange(forLoop, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnCatchStatement(CatchStatement catchStatement, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getCatchStatementRange(catchStatement, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnImportStatement(ImportNode importNode, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getImportRange(importNode, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnPackageStatement(PackageNode packageNode, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getPackageRange(packageNode, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnDeclarationType(DeclarationExpression expression, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getDeclarationExpressionRange(expression, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnClassExpressionType(ClassExpression expression, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getClassExpressionRange(expression, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnArrayExpressionType(ArrayExpression expression, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getArrayExpressionRange(expression, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnVariableType(VariableExpression expression, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getVariableRange(expression, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnGenericType(ClassNode classNode, BaseDocument doc, int cursorOffset) {
        GenericsType[] genericsTypes = classNode.getGenericsTypes();
        if (genericsTypes != null && genericsTypes.length > 0) {
            for (GenericsType genericsType : genericsTypes) {
                if (FindTypeUtils.getGenericTypeRange(genericsType, doc, cursorOffset) == OffsetRange.NONE) continue;
                return true;
            }
        }
        return false;
    }

    private static boolean isCaretOnAnnotation(AnnotationNode annotation, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getAnnotationRange(annotation, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static boolean isCaretOnGenericType(GenericsType genericsType, BaseDocument doc, int cursorOffset) {
        if (FindTypeUtils.getGenericTypeRange(genericsType, doc, cursorOffset) != OffsetRange.NONE) {
            return true;
        }
        return false;
    }

    private static ClassNode getGenericType(ClassNode classNode, BaseDocument doc, int cursorOffset) {
        GenericsType[] genericsTypes = classNode.getGenericsTypes();
        if (genericsTypes != null && genericsTypes.length > 0) {
            for (GenericsType genericsType : genericsTypes) {
                if (!FindTypeUtils.isCaretOnGenericType(genericsType, doc, cursorOffset)) continue;
                return genericsType.getType();
            }
        }
        return null;
    }

    private static OffsetRange getGenericTypeRange(GenericsType genericType, BaseDocument doc, int cursorOffset) {
        int offset = ASTUtils.getOffset(doc, genericType.getLineNumber(), genericType.getColumnNumber());
        OffsetRange range = ASTUtils.getNextIdentifierByName(doc, genericType.getType().getNameWithoutPackage(), offset);
        if (range.containsInclusive(cursorOffset)) {
            return range;
        }
        return OffsetRange.NONE;
    }

    private static OffsetRange getAnnotationRange(AnnotationNode annotation, BaseDocument doc, int cursorOffset) {
        int offset = ASTUtils.getOffset(doc, annotation.getLineNumber(), annotation.getColumnNumber());
        OffsetRange range = ASTUtils.getNextIdentifierByName(doc, annotation.getClassNode().getNameWithoutPackage(), offset);
        if (range.containsInclusive(cursorOffset)) {
            return range;
        }
        return OffsetRange.NONE;
    }

    private static OffsetRange getDeclarationExpressionRange(DeclarationExpression expression, BaseDocument doc, int cursorOffset) {
        OffsetRange range = !expression.isMultipleAssignmentDeclaration() ? FindTypeUtils.getVariableRange(expression.getVariableExpression(), doc, cursorOffset) : FindTypeUtils.getRange((ASTNode)expression.getTupleExpression(), doc, cursorOffset);
        return range;
    }

    private static OffsetRange getClassExpressionRange(ClassExpression expression, BaseDocument doc, int cursorOffset) {
        return FindTypeUtils.getRange((ASTNode)expression, doc, cursorOffset);
    }

    private static OffsetRange getArrayExpressionRange(ArrayExpression expression, BaseDocument doc, int cursorOffset) {
        return FindTypeUtils.getRange((ASTNode)expression.getElementType(), doc, cursorOffset);
    }

    private static OffsetRange getMethodRange(MethodNode method, BaseDocument doc, int cursorOffset) {
        if (method.isDynamicReturnType()) {
            return OffsetRange.NONE;
        }
        return FindTypeUtils.getRange((ASTNode)method, doc, cursorOffset);
    }

    private static OffsetRange getClassNodeRange(ClassNode classNode, BaseDocument doc, int cursorOffset) {
        return FindTypeUtils.getRange((ASTNode)classNode, doc, cursorOffset);
    }

    private static OffsetRange getFieldRange(FieldNode field, BaseDocument doc, int cursorOffset) {
        if (field.isDynamicTyped()) {
            return OffsetRange.NONE;
        }
        return FindTypeUtils.getRange((ASTNode)field, doc, cursorOffset);
    }

    private static OffsetRange getParameterRange(Parameter param, BaseDocument doc, int cursorOffset) {
        if (param.isDynamicTyped()) {
            return OffsetRange.NONE;
        }
        return FindTypeUtils.getRange((ASTNode)param, doc, cursorOffset);
    }

    private static OffsetRange getForLoopRange(ForStatement forLoop, BaseDocument doc, int cursorOffset) {
        return FindTypeUtils.getRange((ASTNode)forLoop.getVariableType(), doc, cursorOffset);
    }

    private static OffsetRange getCatchStatementRange(CatchStatement catchStatement, BaseDocument doc, int cursorOffset) {
        return FindTypeUtils.getRange((ASTNode)catchStatement.getVariable().getOriginType(), doc, cursorOffset);
    }

    private static OffsetRange getImportRange(ImportNode importNode, BaseDocument doc, int cursorOffset) {
        return FindTypeUtils.getRange((ASTNode)importNode.getType(), doc, cursorOffset);
    }

    private static OffsetRange getPackageRange(PackageNode packageNode, BaseDocument doc, int cursorOffset) {
        OffsetRange range = ASTUtils.getNextIdentifierByName(doc, packageNode.getName().substring(0, packageNode.getName().length() - 1), FindTypeUtils.getOffset((ASTNode)packageNode, doc));
        if (range.containsInclusive(cursorOffset)) {
            return range;
        }
        return OffsetRange.NONE;
    }

    private static OffsetRange getVariableRange(VariableExpression variable, BaseDocument doc, int cursorOffset) {
        if (variable == null || variable.getAccessedVariable() == null || variable.isDynamicTyped()) {
            return OffsetRange.NONE;
        }
        return FindTypeUtils.getRange((ASTNode)variable.getAccessedVariable().getOriginType(), doc, cursorOffset);
    }

    private static OffsetRange getRange(ASTNode node, BaseDocument doc, int cursorOffset) {
        if (node.getLineNumber() < 0 || node.getColumnNumber() < 0) {
            return OffsetRange.NONE;
        }
        OffsetRange range = ASTUtils.getNextIdentifierByName(doc, ElementUtils.getTypeName(node), FindTypeUtils.getOffset(node, doc));
        if (range.containsInclusive(cursorOffset)) {
            return range;
        }
        return OffsetRange.NONE;
    }

    private static int getOffset(ASTNode node, BaseDocument doc) {
        return ASTUtils.getOffset(doc, node.getLineNumber(), node.getColumnNumber());
    }
}

