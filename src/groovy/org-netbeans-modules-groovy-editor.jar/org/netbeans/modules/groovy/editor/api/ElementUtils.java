/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.AnnotationNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.DynamicVariable
 *  org.codehaus.groovy.ast.FieldNode
 *  org.codehaus.groovy.ast.ImportNode
 *  org.codehaus.groovy.ast.MethodNode
 *  org.codehaus.groovy.ast.PackageNode
 *  org.codehaus.groovy.ast.Parameter
 *  org.codehaus.groovy.ast.PropertyNode
 *  org.codehaus.groovy.ast.Variable
 *  org.codehaus.groovy.ast.expr.ArrayExpression
 *  org.codehaus.groovy.ast.expr.ClassExpression
 *  org.codehaus.groovy.ast.expr.ConstantExpression
 *  org.codehaus.groovy.ast.expr.ConstructorCallExpression
 *  org.codehaus.groovy.ast.expr.DeclarationExpression
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.PropertyExpression
 *  org.codehaus.groovy.ast.expr.TupleExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.ast.stmt.CatchStatement
 *  org.codehaus.groovy.ast.stmt.ForStatement
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.ElementKind
 */
package org.netbeans.modules.groovy.editor.api;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.AnnotationNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.DynamicVariable;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.ImportNode;
import org.codehaus.groovy.ast.MethodNode;
import org.codehaus.groovy.ast.PackageNode;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.PropertyNode;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.ArrayExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.groovy.editor.api.ASTUtils;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.api.FindTypeUtils;

public final class ElementUtils {
    private ElementUtils() {
    }

    public static ElementKind getKind(AstPath path, BaseDocument doc, int caret) {
        ASTNode node = path.leaf();
        ASTNode leafParent = path.leafParent();
        if (node instanceof ClassNode || node instanceof ClassExpression || node instanceof CatchStatement || node instanceof AnnotationNode || node instanceof ConstructorCallExpression || FindTypeUtils.isCaretOnClassNode(path, doc, caret)) {
            return ElementKind.CLASS;
        }
        if (node instanceof MethodNode) {
            if ("<init>".equals(((MethodNode)node).getName())) {
                return ElementKind.CONSTRUCTOR;
            }
            return ElementKind.METHOD;
        }
        if (node instanceof ConstantExpression && leafParent instanceof MethodCallExpression) {
            return ElementKind.METHOD;
        }
        if (node instanceof FieldNode) {
            return ElementKind.FIELD;
        }
        if (node instanceof PropertyNode) {
            return ElementKind.PROPERTY;
        }
        if (node instanceof VariableExpression) {
            Variable variable = ((VariableExpression)node).getAccessedVariable();
            if (variable instanceof DynamicVariable) {
                return ElementKind.CLASS;
            }
            return ElementKind.VARIABLE;
        }
        if (node instanceof Parameter) {
            return ElementKind.VARIABLE;
        }
        if (node instanceof DeclarationExpression) {
            return ElementKind.VARIABLE;
        }
        if (node instanceof ConstantExpression && leafParent instanceof PropertyExpression) {
            return ElementKind.VARIABLE;
        }
        if (node instanceof PackageNode) {
            return ElementKind.PACKAGE;
        }
        return ElementKind.OTHER;
    }

    public static String getTypeName(ASTNode node) {
        ClassNode type = ElementUtils.getType(node);
        return ElementUtils.normalizeTypeName(type.getName(), type);
    }

    public static String getTypeNameWithoutPackage(ASTNode node) {
        ClassNode type = ElementUtils.getType(node);
        return ElementUtils.normalizeTypeName(type.getNameWithoutPackage(), type);
    }

    public static ClassNode getType(ASTNode node) {
        if (node instanceof ASTUtils.FakeASTNode) {
            node = ((ASTUtils.FakeASTNode)node).getOriginalNode();
        }
        if (node instanceof ClassNode) {
            ClassNode clazz = (ClassNode)node;
            if (clazz.getComponentType() != null) {
                return clazz.getComponentType();
            }
            return clazz;
        }
        if (node instanceof AnnotationNode) {
            return ((AnnotationNode)node).getClassNode();
        }
        if (node instanceof FieldNode) {
            return ((FieldNode)node).getType();
        }
        if (node instanceof PropertyNode) {
            return ((PropertyNode)node).getType();
        }
        if (node instanceof MethodNode) {
            return ((MethodNode)node).getReturnType();
        }
        if (node instanceof Parameter) {
            return ((Parameter)node).getType();
        }
        if (node instanceof ForStatement) {
            return ((ForStatement)node).getVariableType();
        }
        if (node instanceof CatchStatement) {
            return ((CatchStatement)node).getVariable().getOriginType();
        }
        if (node instanceof ImportNode) {
            return ((ImportNode)node).getType();
        }
        if (node instanceof ClassExpression) {
            return ((ClassExpression)node).getType();
        }
        if (node instanceof VariableExpression) {
            return ((VariableExpression)node).getType();
        }
        if (node instanceof DeclarationExpression) {
            DeclarationExpression declaration = (DeclarationExpression)node;
            if (declaration.isMultipleAssignmentDeclaration()) {
                return declaration.getTupleExpression().getType();
            }
            return declaration.getVariableExpression().getType();
        }
        if (node instanceof ConstructorCallExpression) {
            return ((ConstructorCallExpression)node).getType();
        }
        if (node instanceof ArrayExpression) {
            return ((ArrayExpression)node).getElementType();
        }
        throw new IllegalStateException("Not implemented yet - GroovyRefactoringElement.getType() needs to be improve!");
    }

    public static String getNameWithoutPackage(ASTNode node) {
        if (node instanceof ASTUtils.FakeASTNode) {
            node = ((ASTUtils.FakeASTNode)node).getOriginalNode();
        }
        String name = null;
        if (node instanceof ClassNode) {
            name = ((ClassNode)node).getNameWithoutPackage();
        } else {
            if (node instanceof AnnotationNode) {
                return ((AnnotationNode)node).getText();
            }
            if (node instanceof MethodNode) {
                name = ((MethodNode)node).getName();
                if ("<init>".equals(name)) {
                    name = ElementUtils.getDeclaringClassNameWithoutPackage(node);
                }
            } else if (node instanceof FieldNode) {
                name = ((FieldNode)node).getName();
            } else if (node instanceof PropertyNode) {
                name = ((PropertyNode)node).getName();
            } else if (node instanceof Parameter) {
                name = ((Parameter)node).getName();
            } else if (node instanceof ForStatement) {
                name = ((ForStatement)node).getVariableType().getNameWithoutPackage();
            } else if (node instanceof CatchStatement) {
                name = ((CatchStatement)node).getVariable().getName();
            } else if (node instanceof ImportNode) {
                name = ((ImportNode)node).getType().getNameWithoutPackage();
            } else if (node instanceof ClassExpression) {
                name = ((ClassExpression)node).getType().getNameWithoutPackage();
            } else if (node instanceof VariableExpression) {
                name = ((VariableExpression)node).getName();
            } else if (node instanceof DeclarationExpression) {
                DeclarationExpression declaration = (DeclarationExpression)node;
                name = declaration.isMultipleAssignmentDeclaration() ? declaration.getTupleExpression().getType().getNameWithoutPackage() : declaration.getVariableExpression().getType().getNameWithoutPackage();
            } else if (node instanceof ConstantExpression) {
                name = ((ConstantExpression)node).getText();
            } else if (node instanceof MethodCallExpression) {
                name = ((MethodCallExpression)node).getMethodAsString();
            } else if (node instanceof ConstructorCallExpression) {
                name = ((ConstructorCallExpression)node).getType().getNameWithoutPackage();
            } else if (node instanceof ArrayExpression) {
                name = ((ArrayExpression)node).getElementType().getNameWithoutPackage();
            }
        }
        if (name != null) {
            return ElementUtils.normalizeTypeName(name, null);
        }
        throw new IllegalStateException("Not implemented yet - GroovyRefactoringElement.getName() needs to be improve for type: " + node.getClass().getSimpleName());
    }

    public static ClassNode getDeclaringClass(ASTNode node) {
        if (node instanceof ClassNode) {
            return (ClassNode)node;
        }
        if (node instanceof AnnotationNode) {
            return ((AnnotationNode)node).getClassNode();
        }
        if (node instanceof MethodNode) {
            return ((MethodNode)node).getDeclaringClass();
        }
        if (node instanceof FieldNode) {
            return ((FieldNode)node).getDeclaringClass();
        }
        if (node instanceof PropertyNode) {
            return ((PropertyNode)node).getDeclaringClass();
        }
        if (node instanceof Parameter) {
            return ((Parameter)node).getDeclaringClass();
        }
        if (node instanceof ForStatement) {
            return ((ForStatement)node).getVariableType().getDeclaringClass();
        }
        if (node instanceof CatchStatement) {
            return ((CatchStatement)node).getVariable().getDeclaringClass();
        }
        if (node instanceof ImportNode) {
            return ((ImportNode)node).getDeclaringClass();
        }
        if (node instanceof ClassExpression) {
            return ((ClassExpression)node).getType().getDeclaringClass();
        }
        if (node instanceof VariableExpression) {
            return ((VariableExpression)node).getDeclaringClass();
        }
        if (node instanceof DeclarationExpression) {
            DeclarationExpression declaration = (DeclarationExpression)node;
            if (declaration.isMultipleAssignmentDeclaration()) {
                return declaration.getTupleExpression().getDeclaringClass();
            }
            return declaration.getVariableExpression().getDeclaringClass();
        }
        if (node instanceof ConstantExpression) {
            return ((ConstantExpression)node).getDeclaringClass();
        }
        if (node instanceof MethodCallExpression) {
            return ((MethodCallExpression)node).getType();
        }
        if (node instanceof ConstructorCallExpression) {
            return ((ConstructorCallExpression)node).getType();
        }
        if (node instanceof ArrayExpression) {
            return ((ArrayExpression)node).getDeclaringClass();
        }
        throw new IllegalStateException("Not implemented yet - GroovyRefactoringElement.getDeclaringClass() ..looks like the type: " + node.getClass().getName() + " isn't handled at the moment!");
    }

    public static String getDeclaringClassName(ASTNode node) {
        ClassNode declaringClass = ElementUtils.getDeclaringClass(node);
        if (declaringClass != null) {
            return declaringClass.getName();
        }
        return null;
    }

    public static String getDeclaringClassNameWithoutPackage(ASTNode node) {
        ClassNode declaringClass = ElementUtils.getDeclaringClass(node);
        if (declaringClass != null) {
            return declaringClass.getNameWithoutPackage();
        }
        return null;
    }

    public static String normalizeTypeName(String typeName, ClassNode type) {
        if (typeName.startsWith("[") && type != null) {
            typeName = type.getComponentType().getNameWithoutPackage();
        }
        if (typeName.endsWith("[]")) {
            typeName = typeName.substring(0, typeName.length() - 2);
        }
        if (typeName.endsWith(";")) {
            typeName = typeName.substring(0, typeName.length() - 1);
        }
        return typeName;
    }
}

