/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.groovy.editor.api.lexer;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class Call {
    public static final Call LOCAL = new Call(null, null, false, false);
    public static final Call NONE = new Call(null, null, false, false);
    public static final Call UNKNOWN = new Call(null, null, false, false);
    private final String type;
    private final String lhs;
    private final boolean isStatic;
    private final boolean methodExpected;

    public Call(String type, String lhs, boolean isStatic, boolean methodExpected) {
        this.type = type;
        this.lhs = lhs;
        this.methodExpected = methodExpected;
        if (lhs == null) {
            lhs = type;
        }
        this.isStatic = isStatic;
    }

    public String getType() {
        return this.type;
    }

    public String getLhs() {
        return this.lhs;
    }

    public boolean isStatic() {
        return this.isStatic;
    }

    public boolean isSimpleIdentifier() {
        if (this.lhs == null) {
            return false;
        }
        int n = this.lhs.length();
        for (int i = 0; i < n; ++i) {
            char c = this.lhs.charAt(i);
            if (Character.isJavaIdentifierPart(c) || c == '@' || c == '$') continue;
            return false;
        }
        return true;
    }

    public String toString() {
        if (this == LOCAL) {
            return "LOCAL";
        }
        if (this == NONE) {
            return "NONE";
        }
        if (this == UNKNOWN) {
            return "UNKNOWN";
        }
        return "Call(" + this.type + "," + this.lhs + "," + this.isStatic + ")";
    }

    public boolean isMethodExpected() {
        return this.methodExpected;
    }

    @NonNull
    public static Call getCallType(BaseDocument doc, TokenHierarchy<Document> th, int offset) {
        Token token;
        TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, offset);
        if (ts == null) {
            return NONE;
        }
        ts.move(offset);
        boolean methodExpected = false;
        if (!ts.moveNext() && !ts.movePrevious()) {
            return NONE;
        }
        if (ts.offset() == offset) {
            ts.movePrevious();
        }
        if ((token = ts.token()) != null) {
            int lastSeparatorOffset;
            TokenId id = token.id();
            if (id == GroovyTokenId.WHITESPACE) {
                return LOCAL;
            }
            if (id == GroovyTokenId.IDENTIFIER || id.primaryCategory().equals("keyword")) {
                String tokenText = token.text().toString();
                if (".".equals(tokenText)) {
                    methodExpected = true;
                } else {
                    methodExpected = true;
                    if (Character.isUpperCase(tokenText.charAt(0))) {
                        methodExpected = false;
                    }
                    if (!ts.movePrevious()) {
                        return LOCAL;
                    }
                }
                token = ts.token();
                id = token.id();
            }
            if (id == GroovyTokenId.DOT) {
                methodExpected = true;
            } else if (id != GroovyTokenId.COLON) {
                if (id == GroovyTokenId.IDENTIFIER) {
                    String t = token.text().toString();
                    if (t.equals(".")) {
                        methodExpected = true;
                    }
                } else {
                    return LOCAL;
                }
            }
            int beginOffset = lastSeparatorOffset = ts.offset();
            int lineStart = 0;
            try {
                if (offset > doc.getLength()) {
                    offset = doc.getLength();
                }
                lineStart = org.netbeans.editor.Utilities.getRowStart((BaseDocument)doc, (int)offset);
            }
            catch (BadLocationException ble) {
                Exceptions.printStackTrace((Throwable)ble);
            }
            while (ts.movePrevious() && ts.offset() >= lineStart && (id = (token = ts.token()).id()) != GroovyTokenId.WHITESPACE) {
                if (id == GroovyTokenId.STRING_LITERAL) {
                    return new Call("java.lang.String", null, false, methodExpected);
                }
                if (id == GroovyTokenId.RBRACKET) {
                    return new Call("java.util.ArrayList", null, false, methodExpected);
                }
                if (id == GroovyTokenId.NUM_INT) {
                    return new Call("java.lang.Integer", null, false, methodExpected);
                }
                if (id == GroovyTokenId.NUM_DOUBLE) {
                    return new Call("java.math.BigDecimal", null, false, methodExpected);
                }
                if (id == GroovyTokenId.IDENTIFIER || id.primaryCategory().equals("keyword") || id == GroovyTokenId.DOT || id == GroovyTokenId.COLON || id == GroovyTokenId.SUPER_CTOR_CALL || id == GroovyTokenId.CTOR_CALL) {
                    beginOffset = ts.offset();
                    continue;
                }
                if (id == GroovyTokenId.LPAREN || id == GroovyTokenId.LBRACE || id == GroovyTokenId.LBRACKET) break;
                return UNKNOWN;
            }
            if (beginOffset < lastSeparatorOffset) {
                try {
                    String lhs = doc.getText(beginOffset, lastSeparatorOffset - beginOffset);
                    if (lhs.equals("super") || lhs.equals("this")) {
                        return new Call(lhs, lhs, false, true);
                    }
                    if (Character.isUpperCase(lhs.charAt(0))) {
                        String type = null;
                        if (Utilities.isJavaIdentifier((String)lhs)) {
                            type = lhs;
                        }
                        return new Call(type, lhs, true, methodExpected);
                    }
                    return new Call(null, lhs, false, methodExpected);
                }
                catch (BadLocationException ble) {
                    Exceptions.printStackTrace((Throwable)ble);
                }
            } else {
                return UNKNOWN;
            }
        }
        return LOCAL;
    }
}

