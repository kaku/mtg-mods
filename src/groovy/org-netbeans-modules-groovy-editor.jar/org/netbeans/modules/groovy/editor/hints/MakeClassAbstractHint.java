/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.EditList
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintFix
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.groovy.editor.hints;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.EditList;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.groovy.editor.api.lexer.GroovyTokenId;
import org.netbeans.modules.groovy.editor.api.lexer.LexUtilities;
import org.netbeans.modules.groovy.editor.compiler.error.CompilerErrorID;
import org.netbeans.modules.groovy.editor.compiler.error.GroovyError;
import org.netbeans.modules.groovy.editor.hints.Bundle;
import org.netbeans.modules.groovy.editor.hints.infrastructure.GroovyErrorRule;
import org.netbeans.modules.groovy.editor.hints.utils.HintUtils;
import org.netbeans.modules.groovy.editor.utils.GroovyUtils;
import org.openide.filesystems.FileObject;

public final class MakeClassAbstractHint
extends GroovyErrorRule {
    @Override
    public Set<CompilerErrorID> getCodes() {
        return EnumSet.of(CompilerErrorID.CLASS_DOES_NOT_IMPLEMENT_ALL_METHODS);
    }

    @Override
    public void run(RuleContext context, GroovyError error, List<Hint> result) {
        for (Hint existingHint : result) {
            if (!(existingHint.getRule() instanceof MakeClassAbstractHint)) continue;
            return;
        }
        OffsetRange range = HintUtils.getLineOffset(context, error);
        if (range != null) {
            MakeClassAbstractFix fix = new MakeClassAbstractFix(error);
            result.add(new Hint((Rule)this, fix.getDescription(), error.getFile(), range, Collections.singletonList(fix), 200));
        }
    }

    public boolean appliesTo(RuleContext context) {
        return true;
    }

    public String getDisplayName() {
        return Bundle.Test();
    }

    public boolean showInTasklist() {
        return false;
    }

    public HintSeverity getDefaultSeverity() {
        return HintSeverity.ERROR;
    }

    private static class MakeClassAbstractFix
    implements HintFix {
        private final GroovyError error;

        public MakeClassAbstractFix(GroovyError error) {
            this.error = error;
        }

        public void implement() throws Exception {
            BaseDocument baseDoc = LexUtilities.getDocument(this.error.getFile(), true);
            if (baseDoc == null) {
                return;
            }
            EditList edits = new EditList(baseDoc);
            int classPosition = this.getInsertPosition(baseDoc);
            if (classPosition != 0) {
                edits.replace(classPosition, 0, "abstract ", false, 0);
            }
            edits.apply();
        }

        private int getInsertPosition(BaseDocument doc) {
            TokenSequence<GroovyTokenId> ts = LexUtilities.getGroovyTokenSequence((Document)doc, 1);
            while (ts.moveNext()) {
                Token t = ts.token();
                if (t.id() != GroovyTokenId.LITERAL_class) continue;
                int offset = ts.offset();
                if (!this.isCorrectClassDefinition(ts)) continue;
                return offset;
            }
            return 0;
        }

        private boolean isCorrectClassDefinition(TokenSequence<GroovyTokenId> ts) {
            while (ts.moveNext()) {
                Token token = ts.token();
                TokenId id = token.id();
                if (id != GroovyTokenId.IDENTIFIER) continue;
                String identifierName = token.text().toString();
                if (identifierName.equals(this.getClassName())) {
                    return true;
                }
                return false;
            }
            return false;
        }

        public String getDescription() {
            return Bundle.MakeClassAbstract(this.getClassName());
        }

        @NonNull
        public String getClassName() {
            String errorMessage = this.error.getDescription();
            String classNamePrefix = "Can't have an abstract method in a non-abstract class. The class '";
            String classNameSuffix = "' must be declared abstract or the method '";
            int endOffset = errorMessage.indexOf(classNameSuffix);
            String fqName = errorMessage.substring(0, endOffset);
            fqName = fqName.replace(classNamePrefix, "");
            return GroovyUtils.stripPackage(fqName);
        }

        public boolean isSafe() {
            return true;
        }

        public boolean isInteractive() {
            return false;
        }
    }

}

