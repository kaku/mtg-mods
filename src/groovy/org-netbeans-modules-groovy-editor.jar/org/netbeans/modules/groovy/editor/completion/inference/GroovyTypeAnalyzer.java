/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.codehaus.groovy.ast.ASTNode
 *  org.codehaus.groovy.ast.ClassNode
 *  org.codehaus.groovy.ast.ModuleNode
 *  org.codehaus.groovy.ast.expr.MethodCallExpression
 *  org.codehaus.groovy.ast.expr.VariableExpression
 *  org.codehaus.groovy.control.SourceUnit
 *  org.netbeans.editor.BaseDocument
 */
package org.netbeans.modules.groovy.editor.completion.inference;

import java.util.Collections;
import java.util.Set;
import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.ModuleNode;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.control.SourceUnit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.groovy.editor.api.AstPath;
import org.netbeans.modules.groovy.editor.completion.inference.MethodInference;
import org.netbeans.modules.groovy.editor.completion.inference.TypeInferenceVisitor;

public class GroovyTypeAnalyzer {
    private final BaseDocument document;

    public GroovyTypeAnalyzer(BaseDocument document) {
        this.document = document;
    }

    public Set<ClassNode> getTypes(AstPath path, int astOffset) {
        ASTNode caller = path.leaf();
        if (caller instanceof VariableExpression) {
            ModuleNode moduleNode = (ModuleNode)path.root();
            TypeInferenceVisitor typeVisitor = new TypeInferenceVisitor(moduleNode.getContext(), path, this.document, astOffset);
            typeVisitor.collect();
            ClassNode guessedType = typeVisitor.getGuessedType();
            if (guessedType != null) {
                return Collections.singleton(guessedType);
            }
        }
        if (caller instanceof MethodCallExpression) {
            return Collections.singleton(MethodInference.findCallerType(caller));
        }
        return Collections.emptySet();
    }
}

