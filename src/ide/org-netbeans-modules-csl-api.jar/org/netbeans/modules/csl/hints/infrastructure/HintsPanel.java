/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.OptionsFilter
 *  org.netbeans.modules.options.editor.spi.OptionsFilter$Acceptor
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.hints.infrastructure.HintsPanelLogic;
import org.netbeans.modules.csl.hints.infrastructure.HintsSettings;
import org.netbeans.modules.options.editor.spi.OptionsFilter;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

public final class HintsPanel
extends JPanel
implements TreeCellRenderer {
    private DefaultTreeCellRenderer dr = new DefaultTreeCellRenderer();
    private JCheckBox renderer = new JCheckBox();
    private HintsPanelLogic logic;
    private GsfHintsManager manager;
    private OptionsFilter filter;
    private DefaultTreeModel baseModel;
    private JPanel customizerPanel;
    private JLabel descriptionLabel;
    private JPanel descriptionPanel;
    private JEditorPane descriptionTextArea;
    private JPanel detailsPanel;
    private JTree errorTree;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JSplitPane jSplitPane1;
    private JPanel optionsPanel;
    private JComboBox severityComboBox;
    private JLabel severityLabel;
    private JCheckBox toProblemCheckBox;
    private JPanel treePanel;

    public HintsPanel(OptionsFilter filter, TreeModel treeModel, GsfHintsManager manager) {
        this.manager = manager;
        this.initComponents();
        this.descriptionTextArea.setContentType("text/html");
        this.descriptionTextArea.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
        if ("Windows".equals(UIManager.getLookAndFeel().getID())) {
            this.setOpaque(false);
        }
        this.errorTree.setCellRenderer(this);
        this.errorTree.setRootVisible(false);
        this.errorTree.setShowsRootHandles(true);
        this.errorTree.getSelectionModel().setSelectionMode(1);
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
        model.addElement(NbBundle.getMessage(HintsPanel.class, (String)"CTL_AsError"));
        model.addElement(NbBundle.getMessage(HintsPanel.class, (String)"CTL_AsWarning"));
        model.addElement(NbBundle.getMessage(HintsPanel.class, (String)"CTL_WarningOnCurrentLine"));
        model.addElement(NbBundle.getMessage(HintsPanel.class, (String)"CTL_Info"));
        this.severityComboBox.setModel(model);
        this.toProblemCheckBox.setVisible(false);
        this.update();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
        treeModel = new DefaultTreeModel(this.sort(root));
        this.errorTree.setModel(treeModel);
        this.baseModel = (DefaultTreeModel)treeModel;
        if (filter != null) {
            filter.installFilteringModel(this.errorTree, treeModel, (OptionsFilter.Acceptor)new AcceptorImpl());
        }
        for (int lastRow = this.errorTree.getRowCount(); lastRow >= 0; --lastRow) {
            this.errorTree.expandRow(lastRow);
        }
    }

    private DefaultMutableTreeNode sort(DefaultMutableTreeNode parent) {
        ArrayList<DefaultMutableTreeNode> nodes = new ArrayList<DefaultMutableTreeNode>();
        for (int i = 0; i < parent.getChildCount(); ++i) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode)parent.getChildAt(i);
            nodes.add(this.sort(node));
        }
        Collections.sort(nodes, new Comparator<DefaultMutableTreeNode>(){

            @Override
            public int compare(DefaultMutableTreeNode p1, DefaultMutableTreeNode p2) {
                Object o1 = p1.getUserObject();
                String s1 = "";
                if (o1 instanceof Rule) {
                    s1 = ((Rule)o1).getDisplayName();
                }
                if (o1 instanceof FileObject) {
                    s1 = HintsPanel.this.getFileObjectLocalizedName((FileObject)o1);
                }
                Object o2 = p2.getUserObject();
                String s2 = "";
                if (o2 instanceof Rule) {
                    s2 = ((Rule)o2).getDisplayName();
                }
                if (o2 instanceof FileObject) {
                    s2 = HintsPanel.this.getFileObjectLocalizedName((FileObject)o2);
                }
                return s1.compareTo(s2);
            }
        });
        parent.removeAllChildren();
        for (DefaultMutableTreeNode node : nodes) {
            parent.add(node);
        }
        return parent;
    }

    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this.treePanel = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.errorTree = new JTree();
        this.detailsPanel = new JPanel();
        this.optionsPanel = new JPanel();
        this.severityLabel = new JLabel();
        this.severityComboBox = new JComboBox();
        this.toProblemCheckBox = new JCheckBox();
        this.customizerPanel = new JPanel();
        this.descriptionPanel = new JPanel();
        this.jScrollPane2 = new JScrollPane();
        this.descriptionTextArea = new JEditorPane();
        this.descriptionLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        this.setLayout(new GridBagLayout());
        this.jSplitPane1.setBorder(null);
        this.jSplitPane1.setDividerLocation(320);
        this.treePanel.setOpaque(false);
        this.treePanel.setLayout(new BorderLayout());
        this.jScrollPane1.setViewportView(this.errorTree);
        this.treePanel.add((Component)this.jScrollPane1, "Center");
        this.jSplitPane1.setLeftComponent(this.treePanel);
        this.detailsPanel.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 0));
        this.detailsPanel.setOpaque(false);
        this.detailsPanel.setLayout(new GridBagLayout());
        this.optionsPanel.setOpaque(false);
        this.optionsPanel.setLayout(new GridBagLayout());
        this.severityLabel.setLabelFor(this.severityComboBox);
        Mnemonics.setLocalizedText((JLabel)this.severityLabel, (String)NbBundle.getMessage(HintsPanel.class, (String)"CTL_ShowAs_Label"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 4);
        this.optionsPanel.add((Component)this.severityLabel, gridBagConstraints);
        this.severityComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        this.optionsPanel.add((Component)this.severityComboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.toProblemCheckBox, (String)NbBundle.getMessage(HintsPanel.class, (String)"CTL_InTasklist_CheckBox"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(8, 0, 0, 0);
        this.optionsPanel.add((Component)this.toProblemCheckBox, gridBagConstraints);
        this.customizerPanel.setOpaque(false);
        this.customizerPanel.setLayout(new BorderLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(8, 0, 0, 0);
        this.optionsPanel.add((Component)this.customizerPanel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.7;
        gridBagConstraints.insets = new Insets(0, 0, 12, 0);
        this.detailsPanel.add((Component)this.optionsPanel, gridBagConstraints);
        this.descriptionPanel.setOpaque(false);
        this.descriptionPanel.setLayout(new GridBagLayout());
        this.descriptionTextArea.setEditable(false);
        this.descriptionTextArea.setPreferredSize(new Dimension(100, 50));
        this.jScrollPane2.setViewportView(this.descriptionTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(4, 0, 0, 0);
        this.descriptionPanel.add((Component)this.jScrollPane2, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.descriptionLabel, (String)NbBundle.getMessage(HintsPanel.class, (String)"CTL_Description_Border"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        this.descriptionPanel.add((Component)this.descriptionLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.3;
        this.detailsPanel.add((Component)this.descriptionPanel, gridBagConstraints);
        this.jSplitPane1.setRightComponent(this.detailsPanel);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jSplitPane1, gridBagConstraints);
    }

    public synchronized void update() {
        if (this.logic != null) {
            this.logic.disconnect();
        }
        this.logic = new HintsPanelLogic(this.manager);
        this.logic.connect(this.errorTree, this.baseModel, this.severityComboBox, this.toProblemCheckBox, this.customizerPanel, this.descriptionTextArea);
    }

    public void cancel() {
        if (this.logic != null) {
            this.logic.disconnect();
            this.logic = null;
        }
    }

    public boolean isChanged() {
        return this.logic != null ? this.logic.isChanged() : false;
    }

    public void applyChanges() {
        if (this.logic != null) {
            this.logic.applyChanges();
            this.logic.disconnect();
            this.logic = null;
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        this.renderer.setBackground(selected ? this.dr.getBackgroundSelectionColor() : this.dr.getBackgroundNonSelectionColor());
        this.renderer.setForeground(selected ? this.dr.getTextSelectionColor() : this.dr.getTextNonSelectionColor());
        this.renderer.setEnabled(true);
        Object data = ((DefaultMutableTreeNode)value).getUserObject();
        if (data instanceof FileObject) {
            FileObject fo = (FileObject)data;
            this.renderer.setText(this.getFileObjectLocalizedName(fo));
            if (this.logic != null) {
                this.renderer.setSelected(this.logic.isSelected((DefaultMutableTreeNode)value));
            }
        } else if (data instanceof Rule.UserConfigurableRule) {
            Rule.UserConfigurableRule treeRule = (Rule.UserConfigurableRule)data;
            this.renderer.setText(treeRule.getDisplayName());
            if (this.logic != null) {
                Preferences node = this.logic.getCurrentPrefernces(treeRule);
                this.renderer.setSelected(HintsSettings.isEnabled(this.manager, treeRule, node));
            }
        } else {
            this.renderer.setText(value.toString());
        }
        return this.renderer;
    }

    private String getFileObjectLocalizedName(FileObject fo) {
        Object o = fo.getAttribute("SystemFileSystem.localizingBundle");
        if (o instanceof String) {
            String bundleName = (String)o;
            try {
                ResourceBundle rb = NbBundle.getBundle((String)bundleName);
                String localizedName = rb.getString(fo.getPath());
                return localizedName;
            }
            catch (MissingResourceException ex) {
                // empty catch block
            }
        }
        return fo.getPath();
    }

    private class AcceptorImpl
    implements OptionsFilter.Acceptor {
        private AcceptorImpl() {
        }

        public boolean accept(Object originalTreeNode, String filterText) {
            DefaultMutableTreeNode tn = (DefaultMutableTreeNode)originalTreeNode;
            Object o = tn.getUserObject();
            if (!(o instanceof Rule)) {
                return false;
            }
            Rule r = (Rule)o;
            if (filterText == null) {
                return true;
            }
            filterText = filterText.toLowerCase();
            if (r.getDisplayName().toLowerCase().contains(filterText)) {
                return true;
            }
            if (r instanceof Rule.UserConfigurableRule) {
                String htmlDesc = ((Rule.UserConfigurableRule)r).getDescription().toLowerCase();
                String untagged = htmlDesc.replaceAll("</?[a-z0-9]+.*?>", "");
                return untagged.toLowerCase().contains(filterText);
            }
            return false;
        }
    }

}

