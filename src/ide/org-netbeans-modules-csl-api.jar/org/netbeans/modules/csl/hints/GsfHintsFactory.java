/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.hints;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.text.Document;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.hints.GsfHintsProvider;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.openide.filesystems.FileObject;

public class GsfHintsFactory
extends AbstractTaskFactory {
    public static final String LAYER_NAME = "csl-hints";

    public GsfHintsFactory() {
        super(true);
    }

    @Override
    public Collection<? extends SchedulerTask> createTasks(Language l, Snapshot snapshot) {
        FileObject fo = snapshot.getSource().getFileObject();
        if (fo != null) {
            return Collections.singleton(new GsfHintsProvider(fo));
        }
        return null;
    }

    public static List<ErrorDescription> getErrors(Snapshot s, ParserResult res, Snapshot tls) throws ParseException {
        FileObject fo = s.getSource().getFileObject();
        if (fo == null) {
            return new ArrayList<ErrorDescription>();
        }
        GsfHintsProvider hp = new GsfHintsProvider(fo);
        ArrayList<ErrorDescription> descs = new ArrayList<ErrorDescription>();
        hp.processErrors(s, res, null, descs, tls);
        return descs;
    }
}

