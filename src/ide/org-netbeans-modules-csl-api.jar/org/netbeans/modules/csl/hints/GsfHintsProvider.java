/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.ErrorDescriptionFactory
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.netbeans.spi.editor.hints.LazyFixList
 *  org.netbeans.spi.editor.hints.Severity
 *  org.openide.filesystems.FileObject
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.csl.hints;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.LazyFixList;
import org.openide.filesystems.FileObject;
import org.openide.text.NbDocument;

public final class GsfHintsProvider
extends ParserResultTask<ParserResult> {
    public static final Logger LOG = Logger.getLogger(GsfHintsProvider.class.getName());
    private FileObject file;
    private volatile HintsProvider pendingProvider;
    private static final Map<Severity, org.netbeans.spi.editor.hints.Severity> errorKind2Severity = new EnumMap<Severity, org.netbeans.spi.editor.hints.Severity>(Severity.class);
    private boolean cancel;

    GsfHintsProvider(FileObject file) {
        this.file = file;
    }

    List<ErrorDescription> computeErrors(Document doc, ParserResult result, List<? extends Error> errors, List<ErrorDescription> descs) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "errors = " + errors);
        }
        for (Error d : errors) {
            if (this.isCanceled()) {
                return null;
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "d = " + d);
            }
            int astOffset = d.getStartPosition();
            int astEndOffset = d.getEndPosition();
            int position = result.getSnapshot().getOriginalOffset(astOffset);
            if (position == -1) continue;
            int endPosition = position + (astEndOffset - astOffset);
            LazyFixList ehm = ErrorDescriptionFactory.lazyListForFixes(Collections.emptyList());
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "ehm=" + (Object)ehm);
            }
            String desc = d.getDisplayName();
            Position[] range = this.getLine(d, doc, position, endPosition);
            if (this.isCanceled()) {
                return null;
            }
            if (range[0] == null || range[1] == null) continue;
            descs.add(ErrorDescriptionFactory.createErrorDescription((org.netbeans.spi.editor.hints.Severity)errorKind2Severity.get((Object)d.getSeverity()), (String)desc, (LazyFixList)ehm, (Document)doc, (Position)range[0], (Position)range[1]));
        }
        if (this.isCanceled()) {
            return null;
        }
        return descs;
    }

    public Document getDocument() {
        return DataLoadersBridge.getDefault().getDocument(this.file);
    }

    private Position[] getLine(Error d, final Document doc, int startOffset, int endOffset) {
        StyledDocument sdoc = (StyledDocument)doc;
        int lineNumber = NbDocument.findLineNumber((StyledDocument)sdoc, (int)startOffset);
        int lineOffset = NbDocument.findLineOffset((StyledDocument)sdoc, (int)lineNumber);
        String text = DataLoadersBridge.getDefault().getLine(doc, lineNumber);
        if (text == null) {
            return new Position[2];
        }
        if (d.isLineError()) {
            int column;
            int length = text.length();
            for (column = 0; column < text.length() && Character.isWhitespace(text.charAt(column)); ++column) {
            }
            while (length > 0 && Character.isWhitespace(text.charAt(length - 1))) {
                --length;
            }
            startOffset = lineOffset + column;
            endOffset = lineOffset + length;
            if (startOffset > endOffset) {
                startOffset = lineOffset;
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "startOffset = " + startOffset);
            LOG.log(Level.FINE, "endOffset = " + endOffset);
        }
        final int startOffsetFinal = startOffset;
        final int endOffsetFinal = endOffset;
        final Position[] result = new Position[2];
        doc.render(new Runnable(){

            @Override
            public void run() {
                if (GsfHintsProvider.this.isCanceled()) {
                    return;
                }
                int len = doc.getLength();
                if (startOffsetFinal > len || endOffsetFinal > len) {
                    if (!GsfHintsProvider.this.isCanceled() && GsfHintsProvider.LOG.isLoggable(Level.WARNING)) {
                        GsfHintsProvider.LOG.log(Level.WARNING, "document changed, but not canceled?");
                        GsfHintsProvider.LOG.log(Level.WARNING, "len = " + len);
                        GsfHintsProvider.LOG.log(Level.WARNING, "startOffset = " + startOffsetFinal);
                        GsfHintsProvider.LOG.log(Level.WARNING, "endOffset = " + endOffsetFinal);
                    }
                    GsfHintsProvider.this.cancel();
                    return;
                }
                try {
                    result[0] = NbDocument.createPosition((Document)doc, (int)startOffsetFinal, (Position.Bias)Position.Bias.Forward);
                    result[1] = NbDocument.createPosition((Document)doc, (int)endOffsetFinal, (Position.Bias)Position.Bias.Backward);
                }
                catch (BadLocationException e) {
                    GsfHintsProvider.LOG.log(Level.WARNING, null, e);
                }
            }
        });
        return result;
    }

    synchronized boolean isCanceled() {
        return this.cancel;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        HintsProvider curProvider;
        GsfHintsProvider gsfHintsProvider = this;
        synchronized (gsfHintsProvider) {
            curProvider = this.pendingProvider;
            this.cancel = true;
        }
        if (curProvider != null) {
            curProvider.cancel();
        }
    }

    synchronized void resume() {
        this.cancel = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private List<Error> processProviderErrors(List<ErrorDescription> descriptions, Snapshot topLevelSnapshot, ParserResult r, Language language) throws ParseException {
        ArrayList<Hint> hints;
        ArrayList<Error> errors;
        RuleContext ruleContext;
        HintsProvider provider;
        GsfHintsManager manager;
        provider = language.getHintsProvider();
        if (provider == null) {
            return null;
        }
        manager = language.getHintsManager();
        if (manager == null) {
            return null;
        }
        ruleContext = manager.createRuleContext(r, language, -1, -1, -1);
        if (ruleContext == null) {
            return null;
        }
        hints = new ArrayList<Hint>();
        errors = new ArrayList<Error>();
        try {
            GsfHintsProvider gsfHintsProvider = this;
            synchronized (gsfHintsProvider) {
                this.pendingProvider = provider;
                if (this.isCanceled()) {
                    ArrayList<Error> arrayList = errors;
                    return arrayList;
                }
            }
            provider.computeErrors(manager, ruleContext, hints, errors);
        }
        finally {
            this.pendingProvider = null;
        }
        boolean allowDisableEmpty = true;
        int i = 0;
        while (i < hints.size()) {
            Hint hint = hints.get(i);
            OffsetRange range = hint.getRange();
            if (range != null && range.getStart() >= 0 && range.getStart() <= topLevelSnapshot.getText().length() && range.getEnd() >= 0 && range.getEnd() <= topLevelSnapshot.getText().length() && range.getStart() <= range.getEnd()) {
                ErrorDescription errorDesc = manager.createDescription(hint, ruleContext, allowDisableEmpty, i == hints.size() - 1);
                descriptions.add(errorDesc);
            } else {
                String msg = provider + " supplied hint " + hint + " with invalid range " + range + ", topLevelSnapshot.length=" + topLevelSnapshot.getText().length() + ", file=" + (Object)topLevelSnapshot.getSource().getFileObject();
                LOG.log(Level.FINE, msg);
            }
            ++i;
        }
        return errors;
    }

    private void refreshErrors(ResultIterator resultIterator) throws ParseException {
        ArrayList<ErrorDescription> descs = new ArrayList<ErrorDescription>();
        Document doc = this.getDocument();
        this.processErrorsRecursive(resultIterator, doc, descs, resultIterator.getSnapshot());
        HintsController.setErrors((Document)doc, (String)"csl-hints", descs);
    }

    private void processErrorsRecursive(ResultIterator resultIterator, Document doc, List<ErrorDescription> descriptions, Snapshot topLevelSnapshot) throws ParseException {
        if (resultIterator == null) {
            return;
        }
        if (doc == null) {
            doc = this.getDocument();
        }
        for (Embedding e : resultIterator.getEmbeddings()) {
            try {
                if (this.isCanceled()) {
                    return;
                }
            }
            catch (Exception ex) {
                LOG.log(Level.WARNING, "Unexpected error", ex);
            }
            this.processErrorsRecursive(resultIterator.getResultIterator(e), doc, descriptions, topLevelSnapshot);
        }
        if (!(resultIterator.getParserResult() instanceof ParserResult)) {
            return;
        }
        this.processErrors(resultIterator.getSnapshot(), (ParserResult)resultIterator.getParserResult(), doc, descriptions, topLevelSnapshot);
    }

    void processErrors(Snapshot snapshot, ParserResult result, Document doc, List<ErrorDescription> descriptions, Snapshot topLevelSnapshot) throws ParseException {
        Language language;
        if (doc == null) {
            doc = this.getDocument();
        }
        if ((language = LanguageRegistry.getInstance().getLanguageByMimeType(snapshot.getMimeType())) == null) {
            return;
        }
        if (!(result instanceof ParserResult)) {
            return;
        }
        ParserResult r = result;
        List<? extends Error> errors = r.getDiagnostics();
        List desc = new ArrayList<ErrorDescription>();
        List<Error> unhandled = this.processProviderErrors(descriptions, topLevelSnapshot, r, language);
        if (unhandled != null) {
            errors = unhandled;
        }
        if ((desc = this.computeErrors(doc, r, errors, desc)) == null) {
            return;
        }
        descriptions.addAll(desc);
    }

    public void run(ParserResult result, SchedulerEvent event) {
        this.resume();
        Document doc = this.getDocument();
        if (doc == null) {
            LOG.log(Level.INFO, "SemanticHighlighter: Cannot get document!");
            return;
        }
        try {
            ParserManager.parse(Collections.singleton(result.getSnapshot().getSource()), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws ParseException {
                    GsfHintsProvider.this.refreshErrors(resultIterator);
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
    }

    public static void refreshErrors() {
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    static {
        errorKind2Severity.put(Severity.ERROR, org.netbeans.spi.editor.hints.Severity.ERROR);
        errorKind2Severity.put(Severity.WARNING, org.netbeans.spi.editor.hints.Severity.WARNING);
        errorKind2Severity.put(Severity.INFO, org.netbeans.spi.editor.hints.Severity.HINT);
    }

}

