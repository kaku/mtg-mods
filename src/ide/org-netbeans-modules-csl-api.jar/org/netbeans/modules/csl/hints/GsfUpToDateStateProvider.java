/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatus
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider
 */
package org.netbeans.modules.csl.hints;

import org.netbeans.spi.editor.errorstripe.UpToDateStatus;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider;

public class GsfUpToDateStateProvider
extends UpToDateStatusProvider {
    public UpToDateStatus getUpToDate() {
        return UpToDateStatus.UP_TO_DATE_OK;
    }
}

