/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.HintsController;
import org.openide.filesystems.FileObject;

public class HintsTask
extends ParserResultTask<ParserResult> {
    private static final Logger LOG = Logger.getLogger(HintsTask.class.getName());
    private volatile boolean cancelled = false;
    private volatile HintsProvider pendingProvider;

    public void run(ParserResult result, SchedulerEvent event) {
        this.resume();
        final ArrayList descriptions = new ArrayList();
        try {
            ParserManager.parse(Collections.singleton(result.getSnapshot().getSource()), (UserTask)new UserTask(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 * Enabled aggressive block sorting
                 * Enabled unnecessary exception pruning
                 * Enabled aggressive exception aggregation
                 */
                public void run(ResultIterator resultIterator) throws ParseException {
                    RuleContext ruleContext;
                    ArrayList<Hint> hints;
                    GsfHintsManager manager;
                    if (resultIterator == null) {
                        return;
                    }
                    for (Embedding e : resultIterator.getEmbeddings()) {
                        this.run(resultIterator.getResultIterator(e));
                    }
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(resultIterator.getSnapshot().getMimeType());
                    if (language == null) {
                        return;
                    }
                    HintsProvider provider = language.getHintsProvider();
                    if (provider == null) {
                        return;
                    }
                    manager = language.getHintsManager();
                    if (manager == null) {
                        return;
                    }
                    Parser.Result r = resultIterator.getParserResult();
                    if (!(r instanceof ParserResult)) {
                        return;
                    }
                    ParserResult parserResult = (ParserResult)r;
                    ruleContext = manager.createRuleContext(parserResult, language, -1, -1, -1);
                    if (ruleContext == null) {
                        return;
                    }
                    hints = new ArrayList<Hint>();
                    try {
                        HintsTask hintsTask = HintsTask.this;
                        synchronized (hintsTask) {
                            HintsTask.this.pendingProvider = provider;
                            if (HintsTask.this.isCancelled()) {
                                return;
                            }
                        }
                        provider.computeHints(manager, ruleContext, hints);
                    }
                    finally {
                        HintsTask.this.pendingProvider = null;
                    }
                    if (HintsTask.this.isCancelled()) {
                        return;
                    }
                    int i = 0;
                    while (i < hints.size()) {
                        Hint hint = hints.get(i);
                        ErrorDescription desc = manager.createDescription(hint, ruleContext, false, i == hints.size() - 1);
                        descriptions.add(desc);
                        ++i;
                    }
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
        HintsController.setErrors((FileObject)result.getSnapshot().getSource().getFileObject(), (String)HintsTask.class.getName(), descriptions);
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        HintsProvider proc;
        HintsTask hintsTask = this;
        synchronized (hintsTask) {
            proc = this.pendingProvider;
            this.cancelled = true;
        }
        if (proc != null) {
            proc.cancel();
        }
    }

    private synchronized void resume() {
        this.cancelled = false;
    }

    private synchronized boolean isCancelled() {
        return this.cancelled;
    }

}

