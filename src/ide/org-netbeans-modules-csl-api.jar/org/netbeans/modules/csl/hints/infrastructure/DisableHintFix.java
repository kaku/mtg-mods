/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.netbeans.spi.editor.hints.ChangeInfo
 *  org.netbeans.spi.editor.hints.EnhancedFix
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.hints.infrastructure;

import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.spi.editor.hints.ChangeInfo;
import org.netbeans.spi.editor.hints.EnhancedFix;
import org.openide.util.NbBundle;

final class DisableHintFix
implements EnhancedFix {
    private final GsfHintsManager manager;
    private final RuleContext context;

    DisableHintFix(GsfHintsManager manager, RuleContext context) {
        this.manager = manager;
        this.context = context;
    }

    public String getText() {
        return NbBundle.getMessage(DisableHintFix.class, (String)"DisableHint");
    }

    public ChangeInfo implement() throws Exception {
        OptionsDisplayer.getDefault().open("Editor/Hints");
        this.manager.refreshHints(this.context);
        return null;
    }

    public CharSequence getSortText() {
        return Integer.toString(Integer.MAX_VALUE);
    }
}

