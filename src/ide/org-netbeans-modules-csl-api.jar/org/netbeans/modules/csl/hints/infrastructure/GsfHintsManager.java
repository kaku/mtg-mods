/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.ErrorDescriptionFactory
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.netbeans.spi.editor.hints.Severity
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.PreviewableFix;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.hints.GsfHintsFactory;
import org.netbeans.modules.csl.hints.infrastructure.DisableHintFix;
import org.netbeans.modules.csl.hints.infrastructure.FixWrapper;
import org.netbeans.modules.csl.hints.infrastructure.HintsOptionsPanelController;
import org.netbeans.modules.csl.hints.infrastructure.HintsSettings;
import org.netbeans.modules.csl.hints.infrastructure.HintsTask;
import org.netbeans.modules.csl.hints.infrastructure.PreviewHintFix;
import org.netbeans.modules.csl.hints.infrastructure.SuggestionsTask;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Pair;

public class GsfHintsManager
extends HintsProvider.HintsManager {
    private static final Logger LOG = Logger.getLogger(GsfHintsManager.class.getName());
    private static final String INSTANCE_EXT = ".instance";
    private static final String NON_GUI = "nonGUI";
    private static final String RULES_FOLDER = "csl-hints/";
    private static final String ERRORS = "/errors";
    private static final String HINTS = "/hints";
    private static final String SUGGESTIONS = "/suggestions";
    private static final String SELECTION = "/selection";
    private Map<?, List<? extends Rule.ErrorRule>> errors = new HashMap();
    private Map<?, List<? extends Rule.AstRule>> hints = new HashMap();
    private Map<?, List<? extends Rule.AstRule>> suggestions = new HashMap();
    private List<Rule.SelectionRule> selectionHints = new ArrayList<Rule.SelectionRule>();
    private TreeModel errorsTreeModel;
    private TreeModel hintsTreeModel;
    private TreeModel suggestionsTreeModel;
    private String mimeType;
    private HintsProvider provider;
    private String id;
    boolean isTest = false;
    private OptionsPanelController panelController;

    public GsfHintsManager(String mimeType, HintsProvider provider, Language language) {
        this.mimeType = mimeType;
        this.provider = provider;
        this.id = language.getMimeType().replace('/', '_') + '_';
        this.initErrors();
        this.initHints();
        this.initSuggestions();
        this.initSelectionHints();
        this.initBuiltins();
    }

    @Override
    public boolean isEnabled(Rule.UserConfigurableRule rule) {
        return HintsSettings.isEnabled(this, rule);
    }

    @Override
    public Map<?, List<? extends Rule.ErrorRule>> getErrors() {
        return this.errors;
    }

    @Override
    public Map<?, List<? extends Rule.AstRule>> getHints() {
        return this.hints;
    }

    @Override
    public List<? extends Rule.SelectionRule> getSelectionHints() {
        return this.selectionHints;
    }

    @Override
    public Map<?, List<? extends Rule.AstRule>> getHints(boolean onLine, RuleContext context) {
        HashMap result = new HashMap();
        for (Map.Entry e : this.getHints().entrySet()) {
            LinkedList<Rule.AstRule> nueRules = new LinkedList<Rule.AstRule>();
            for (Rule.AstRule r : e.getValue()) {
                Preferences p = HintsSettings.getPreferences(this, r, null);
                if (p == null) {
                    if (onLine || !r.appliesTo(context)) continue;
                    nueRules.add(r);
                    continue;
                }
                if (HintsSettings.getSeverity(this, r) == HintSeverity.CURRENT_LINE_WARNING) {
                    if (!onLine || !r.appliesTo(context)) continue;
                    nueRules.add(r);
                    continue;
                }
                if (onLine || !r.appliesTo(context)) continue;
                nueRules.add(r);
            }
            if (nueRules.isEmpty()) continue;
            result.put(e.getKey(), nueRules);
        }
        return result;
    }

    @Override
    public Map<?, List<? extends Rule.AstRule>> getSuggestions() {
        return this.suggestions;
    }

    TreeModel getErrorsTreeModel() {
        return this.errorsTreeModel;
    }

    public TreeModel getHintsTreeModel() {
        return this.hintsTreeModel;
    }

    public String getId() {
        return this.id;
    }

    TreeModel getSuggestionsTreeModel() {
        return this.suggestionsTreeModel;
    }

    private void initErrors() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
        this.errorsTreeModel = new DefaultTreeModel(rootNode);
        FileObject folder = FileUtil.getConfigFile((String)("csl-hints/" + this.mimeType + "/errors"));
        List<Pair<Rule, FileObject>> rules = GsfHintsManager.readRules(folder);
        GsfHintsManager.categorizeErrorRules(rules, this.errors, folder, rootNode);
    }

    private void initHints() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
        this.hintsTreeModel = new DefaultTreeModel(rootNode);
        FileObject folder = FileUtil.getConfigFile((String)("csl-hints/" + this.mimeType + "/hints"));
        List<Pair<Rule, FileObject>> rules = GsfHintsManager.readRules(folder);
        GsfHintsManager.categorizeAstRules(rules, this.hints, folder, rootNode);
    }

    private void initSuggestions() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
        this.suggestionsTreeModel = new DefaultTreeModel(rootNode);
        FileObject folder = FileUtil.getConfigFile((String)("csl-hints/" + this.mimeType + "/suggestions"));
        List<Pair<Rule, FileObject>> rules = GsfHintsManager.readRules(folder);
        GsfHintsManager.categorizeAstRules(rules, this.suggestions, folder, rootNode);
    }

    private void initSelectionHints() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
        this.suggestionsTreeModel = new DefaultTreeModel(rootNode);
        FileObject folder = FileUtil.getConfigFile((String)("csl-hints/" + this.mimeType + "/selection"));
        List<Pair<Rule, FileObject>> rules = GsfHintsManager.readRules(folder);
        GsfHintsManager.categorizeSelectionRules(rules, this.selectionHints, folder, rootNode);
    }

    private void initBuiltins() {
        List<Rule> extraRules = this.provider.getBuiltinRules();
        if (extraRules != null) {
            Map errorMap = this.errors;
            List<Rule.SelectionRule> selectionList = this.selectionHints;
            Map hintsMap = this.hints;
            for (Rule rule : extraRules) {
                List list;
                if (rule instanceof Rule.ErrorRule) {
                    Rule.ErrorRule errorRule = (Rule.ErrorRule)rule;
                    for (Object key : errorRule.getCodes()) {
                        list = this.errors.get(key);
                        if (list == null) {
                            list = new ArrayList<Rule.ErrorRule>(2);
                            errorMap.put(key, list);
                        }
                        list.add((Rule)rule);
                    }
                    continue;
                }
                if (rule instanceof Rule.SelectionRule) {
                    selectionList.add((Rule.SelectionRule)rule);
                    continue;
                }
                if (rule instanceof Rule.AstRule) {
                    Rule.AstRule astRule = (Rule.AstRule)rule;
                    for (Object key : astRule.getKinds()) {
                        list = this.hints.get(key);
                        if (list == null) {
                            list = new ArrayList(2);
                            hintsMap.put(key, list);
                        }
                        list.add(rule);
                    }
                    continue;
                }
                assert (false);
            }
        }
    }

    private static List<Pair<Rule, FileObject>> readRules(FileObject folder) {
        LinkedList<Pair<Rule, FileObject>> rules = new LinkedList<Pair<Rule, FileObject>>();
        if (folder == null) {
            return rules;
        }
        Enumeration<FileObject> e = Collections.enumeration(GsfHintsManager.getSortedDataRecursively(folder));
        while (e.hasMoreElements()) {
            FileObject o = e.nextElement();
            String name = o.getNameExt().toLowerCase();
            if (!o.canRead()) continue;
            Rule r = null;
            if (name.endsWith(".instance")) {
                r = GsfHintsManager.instantiateRule(o);
            }
            if (r == null) continue;
            rules.add((Pair)Pair.of((Object)r, (Object)o));
        }
        Collections.sort(rules, new Comparator<Pair<Rule, FileObject>>(){

            @Override
            public int compare(Pair<Rule, FileObject> p1, Pair<Rule, FileObject> p2) {
                return ((Rule)p1.first()).getDisplayName().compareTo(((Rule)p2.first()).getDisplayName());
            }
        });
        return rules;
    }

    private static List<FileObject> getSortedDataRecursively(FileObject folder) {
        LinkedList<FileObject> files = new LinkedList<FileObject>();
        GsfHintsManager.addChildren(files, folder);
        return files;
    }

    private static void addChildren(List<FileObject> items, FileObject folder) {
        FileObject[] children;
        for (FileObject fo : children = folder.getChildren()) {
            if (fo.isFolder()) {
                GsfHintsManager.addChildren(items, fo);
                continue;
            }
            items.add(fo);
        }
    }

    private static void categorizeErrorRules(List<Pair<Rule, FileObject>> rules, Map<?, List<? extends Rule.ErrorRule>> dest, FileObject rootFolder, DefaultMutableTreeNode rootNode) {
        HashMap<FileObject, DefaultMutableTreeNode> dir2node = new HashMap<FileObject, DefaultMutableTreeNode>();
        dir2node.put(rootFolder, rootNode);
        for (Pair<Rule, FileObject> pair : rules) {
            Rule rule = (Rule)pair.first();
            FileObject fo = (FileObject)pair.second();
            if (rule instanceof Rule.ErrorRule) {
                GsfHintsManager.addRule((Rule.ErrorRule)rule, dest);
                FileObject parent = fo.getParent();
                DefaultMutableTreeNode category = (DefaultMutableTreeNode)dir2node.get((Object)parent);
                if (category == null) {
                    category = new DefaultMutableTreeNode((Object)parent);
                    rootNode.add(category);
                    dir2node.put(parent, category);
                }
                category.add(new DefaultMutableTreeNode(rule, false));
                continue;
            }
            LOG.log(Level.WARNING, "The rule defined in " + fo.getPath() + "is not instance of ErrorRule");
        }
    }

    private static void categorizeAstRules(List<Pair<Rule, FileObject>> rules, Map<?, List<? extends Rule.AstRule>> dest, FileObject rootFolder, DefaultMutableTreeNode rootNode) {
        HashMap<FileObject, DefaultMutableTreeNode> dir2node = new HashMap<FileObject, DefaultMutableTreeNode>();
        dir2node.put(rootFolder, rootNode);
        for (Pair<Rule, FileObject> pair : rules) {
            Rule rule = (Rule)pair.first();
            FileObject fo = (FileObject)pair.second();
            if (rule instanceof Rule.AstRule) {
                Object nonGuiObject = fo.getAttribute("nonGUI");
                boolean toGui = true;
                if (nonGuiObject != null && nonGuiObject instanceof Boolean && ((Boolean)nonGuiObject).booleanValue()) {
                    toGui = false;
                }
                GsfHintsManager.addRule((Rule.AstRule)rule, dest);
                FileObject parent = fo.getParent();
                DefaultMutableTreeNode category = (DefaultMutableTreeNode)dir2node.get((Object)parent);
                if (category == null) {
                    category = new DefaultMutableTreeNode((Object)parent);
                    rootNode.add(category);
                    dir2node.put(parent, category);
                }
                if (!toGui) continue;
                category.add(new DefaultMutableTreeNode(rule, false));
                continue;
            }
            LOG.log(Level.WARNING, "The rule defined in " + fo.getPath() + "is not instance of AstRule");
        }
    }

    private static void categorizeSelectionRules(List<Pair<Rule, FileObject>> rules, List<? extends Rule.SelectionRule> dest, FileObject rootFolder, DefaultMutableTreeNode rootNode) {
        HashMap<FileObject, DefaultMutableTreeNode> dir2node = new HashMap<FileObject, DefaultMutableTreeNode>();
        dir2node.put(rootFolder, rootNode);
        for (Pair<Rule, FileObject> pair : rules) {
            Rule rule = (Rule)pair.first();
            FileObject fo = (FileObject)pair.second();
            if (rule instanceof Rule.SelectionRule) {
                GsfHintsManager.addRule((Rule.SelectionRule)rule, dest);
                FileObject parent = fo.getParent();
                DefaultMutableTreeNode category = (DefaultMutableTreeNode)dir2node.get((Object)parent);
                if (category == null) {
                    category = new DefaultMutableTreeNode((Object)parent);
                    rootNode.add(category);
                    dir2node.put(parent, category);
                }
                category.add(new DefaultMutableTreeNode(rule, false));
                continue;
            }
            LOG.log(Level.WARNING, "The rule defined in " + fo.getPath() + "is not instance of SelectionRule");
        }
    }

    private static void addRule(Rule.AstRule rule, Map<? super Object, List<Rule.AstRule>> dest) {
        for (Object kind : rule.getKinds()) {
            List<Rule.AstRule> l = dest.get(kind);
            if (l == null) {
                l = new LinkedList<Rule.AstRule>();
                dest.put(kind, l);
            }
            l.add(rule);
        }
    }

    private static void addRule(Rule.ErrorRule rule, Map<? super Object, List<Rule.ErrorRule>> dest) {
        for (Object code : rule.getCodes()) {
            List<Rule.ErrorRule> l = dest.get(code);
            if (l == null) {
                l = new LinkedList<Rule.ErrorRule>();
                dest.put(code, l);
            }
            l.add(rule);
        }
    }

    private static void addRule(Rule.SelectionRule rule, List<? super Rule.SelectionRule> dest) {
        dest.add(rule);
    }

    private static Rule instantiateRule(FileObject fileObject) {
        try {
            DataObject dobj = DataObject.find((FileObject)fileObject);
            InstanceCookie ic = (InstanceCookie)dobj.getCookie(InstanceCookie.class);
            Object instance = ic.instanceCreate();
            if (instance instanceof Rule) {
                return (Rule)instance;
            }
            return null;
        }
        catch (IOException e) {
            LOG.log(Level.INFO, null, e);
        }
        catch (ClassNotFoundException e) {
            LOG.log(Level.INFO, null, e);
        }
        return null;
    }

    public final ErrorDescription createDescription(Hint desc, RuleContext context, boolean allowDisableEmpty, boolean last) {
        List fixList;
        Rule rule = desc.getRule();
        HintSeverity severity = rule instanceof Rule.UserConfigurableRule ? HintsSettings.getSeverity(this, (Rule.UserConfigurableRule)rule) : rule.getDefaultSeverity();
        OffsetRange range = desc.getRange();
        ParserResult info = context.parserResult;
        if (desc.getFixes() != null && desc.getFixes().size() > 0) {
            fixList = new ArrayList(desc.getFixes().size());
            String sortText = Integer.toString(10000 + desc.getPriority());
            for (HintFix fix : desc.getFixes()) {
                PreviewableFix previewFix;
                fixList.add(new FixWrapper(fix, sortText));
                if (!(fix instanceof PreviewableFix) || !(previewFix = (PreviewableFix)fix).canPreview() || this.isTest) continue;
                fixList.add(new PreviewHintFix(info, previewFix, sortText));
            }
            if (last && rule instanceof Rule.UserConfigurableRule && !this.isTest) {
                fixList.add(new DisableHintFix(this, context));
            }
        } else {
            fixList = last && allowDisableEmpty && rule instanceof Rule.UserConfigurableRule && !this.isTest ? Collections.singletonList(new DisableHintFix(this, context)) : Collections.emptyList();
        }
        return ErrorDescriptionFactory.createErrorDescription((Severity)severity.toEditorSeverity(), (String)desc.getDescription(), fixList, (FileObject)desc.getFile(), (int)range.getStart(), (int)range.getEnd());
    }

    @Override
    public final void refreshHints(RuleContext context) {
        List[] result = new List[3];
        GsfHintsManager.getHints(this, context, result, context.parserResult.getSnapshot());
        FileObject f = context.parserResult.getSnapshot().getSource().getFileObject();
        if (result[0] != null) {
            HintsController.setErrors((FileObject)f, (String)HintsTask.class.getName(), (Collection)result[0]);
        }
        if (result[1] != null) {
            HintsController.setErrors((FileObject)f, (String)SuggestionsTask.class.getName(), (Collection)result[1]);
        }
        if (result[2] != null) {
            HintsController.setErrors((FileObject)f, (String)"csl-hints", (Collection)result[2]);
        }
    }

    private static final void getHints(GsfHintsManager hintsManager, RuleContext context, List<ErrorDescription>[] ret, Snapshot tls) {
        if (hintsManager != null && context != null) {
            int caretPos = context.caretOffset;
            HintsProvider provider = hintsManager.provider;
            if (provider != null) {
                ArrayList<Hint> descriptions = new ArrayList<Hint>();
                if (caretPos == -1) {
                    provider.computeHints(hintsManager, context, descriptions);
                    List result = ret[0] == null ? new ArrayList<ErrorDescription>(descriptions.size()) : ret[0];
                    for (int i = 0; i < descriptions.size(); ++i) {
                        Hint desc = descriptions.get(i);
                        boolean allowDisable = true;
                        ErrorDescription errorDesc = hintsManager.createDescription(desc, context, allowDisable, i == descriptions.size() - 1);
                        result.add(errorDesc);
                    }
                    ret[0] = result;
                } else {
                    provider.computeSuggestions(hintsManager, context, descriptions, caretPos);
                    List result = ret[1] == null ? new ArrayList<ErrorDescription>(descriptions.size()) : ret[1];
                    for (int i = 0; i < descriptions.size(); ++i) {
                        Hint desc = descriptions.get(i);
                        boolean allowDisable = true;
                        ErrorDescription errorDesc = hintsManager.createDescription(desc, context, allowDisable, i == descriptions.size() - 1);
                        result.add(errorDesc);
                    }
                    ret[1] = result;
                }
            }
        }
        try {
            ret[2] = GsfHintsFactory.getErrors(context.parserResult.getSnapshot(), context.parserResult, tls);
        }
        catch (ParseException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    static final void refreshHints(ResultIterator controller) {
        ArrayList[] allHints = new ArrayList[3];
        GsfHintsManager.collectHints(controller, allHints, controller.getSnapshot());
        FileObject f = controller.getSnapshot().getSource().getFileObject();
        if (f != null) {
            if (allHints[0] != null) {
                HintsController.setErrors((FileObject)f, (String)HintsTask.class.getName(), (Collection)allHints[0]);
            }
            if (allHints[1] != null) {
                HintsController.setErrors((FileObject)f, (String)SuggestionsTask.class.getName(), (Collection)allHints[1]);
            }
            if (allHints[2] != null) {
                HintsController.setErrors((FileObject)f, (String)"csl-hints", (Collection)allHints[2]);
            }
        } else {
            LOG.warning("Source " + (Object)controller.getSnapshot().getSource() + " returns null from getFileObject()");
        }
    }

    private static void collectHints(ResultIterator controller, List<ErrorDescription>[] allHints, Snapshot tls) {
        String mimeType = controller.getSnapshot().getMimeType();
        Language language = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
        if (language == null) {
            return;
        }
        GsfHintsManager hintsManager = language.getHintsManager();
        if (hintsManager == null) {
            return;
        }
        ParserResult parserResult = null;
        try {
            Parser.Result pr = controller.getParserResult();
            if (pr instanceof ParserResult) {
                parserResult = (ParserResult)pr;
            }
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
        if (parserResult == null) {
            return;
        }
        RuleContext context = hintsManager.createRuleContext(parserResult, language, -1, -1, -1);
        List[] hints = new List[3];
        GsfHintsManager.getHints(hintsManager, context, hints, tls);
        for (int i = 0; i < 3; ++i) {
            allHints[i] = GsfHintsManager.merge(allHints[i], hints[i]);
        }
        for (Embedding e : controller.getEmbeddings()) {
            GsfHintsManager.collectHints(controller.getResultIterator(e), allHints, tls);
        }
    }

    private static List<ErrorDescription> merge(List<ErrorDescription> a, List<ErrorDescription> b) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        a.addAll(b);
        return a;
    }

    public RuleContext createRuleContext(ParserResult parserResult, Language language, int caretOffset, int selectionStart, int selectionEnd) {
        RuleContext context = this.provider.createRuleContext();
        context.manager = this;
        context.parserResult = parserResult;
        context.caretOffset = caretOffset;
        context.selectionStart = selectionStart;
        context.selectionEnd = selectionEnd;
        context.doc = (BaseDocument)parserResult.getSnapshot().getSource().getDocument(false);
        if (context.doc == null) {
            return null;
        }
        return context;
    }

    @Override
    public synchronized OptionsPanelController getOptionsController() {
        if (this.panelController == null) {
            this.panelController = new HintsOptionsPanelController(this);
        }
        return this.panelController;
    }

    public void setTestingRules(Map<?, List<? extends Rule.ErrorRule>> errors, Map<?, List<? extends Rule.AstRule>> hints, Map<?, List<? extends Rule.AstRule>> suggestions, List<Rule.SelectionRule> selectionHints) {
        this.errors = errors;
        this.hints = hints;
        this.suggestions = suggestions;
        this.selectionHints = selectionHints;
        this.isTest = true;
    }

    @Override
    public Preferences getPreferences(Rule.UserConfigurableRule rule) {
        return HintsSettings.getPreferences(this, rule, null);
    }

}

