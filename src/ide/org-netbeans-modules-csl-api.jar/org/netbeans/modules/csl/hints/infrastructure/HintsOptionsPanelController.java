/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.OptionsFilter
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import javax.swing.tree.TreeModel;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.hints.infrastructure.HintsPanel;
import org.netbeans.modules.options.editor.spi.OptionsFilter;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

final class HintsOptionsPanelController
extends OptionsPanelController {
    private GsfHintsManager manager;
    private HintsPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;

    HintsOptionsPanelController(GsfHintsManager manager) {
        this.pcs = new PropertyChangeSupport((Object)this);
        this.manager = manager;
    }

    public void update() {
        if (this.panel != null) {
            this.panel.update();
        }
    }

    public void applyChanges() {
        if (this.isChanged()) {
            this.panel.applyChanges();
        }
    }

    public void cancel() {
        if (this.panel != null) {
            this.panel.cancel();
        }
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.panel == null ? false : this.panel.isChanged();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public synchronized JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            OptionsFilter filter = (OptionsFilter)masterLookup.lookup(OptionsFilter.class);
            this.panel = new HintsPanel(filter, this.manager.getHintsTreeModel(), this.manager);
        }
        return this.panel;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    void changed() {
        if (!this.changed) {
            this.changed = true;
            this.pcs.firePropertyChange("changed", false, true);
        }
        this.pcs.firePropertyChange("valid", null, null);
    }
}

