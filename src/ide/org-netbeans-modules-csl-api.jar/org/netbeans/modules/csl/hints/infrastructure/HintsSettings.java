/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.util.prefs.Preferences;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.openide.util.NbPreferences;

public class HintsSettings {
    static final HintSeverity SEVERITY_DEFAUT = HintSeverity.WARNING;
    static final boolean IN_TASK_LIST_DEFAULT = true;
    static final String ENABLED_KEY = "enabled";
    static final String SEVERITY_KEY = "severity";
    static final String IN_TASK_LIST_KEY = "inTaskList";
    private static final String DEFAULT_PROFILE = "default";

    private HintsSettings() {
    }

    public static String getCurrentProfileId() {
        return "default";
    }

    public static Preferences getPreferences(GsfHintsManager manager, Rule.UserConfigurableRule rule, String profile) {
        profile = profile == null ? HintsSettings.getCurrentProfileId() : profile;
        return NbPreferences.forModule(HintsSettings.class).node(profile).node(manager.getId() + rule.getId());
    }

    public static HintSeverity getSeverity(GsfHintsManager manager, Rule.UserConfigurableRule rule) {
        return HintsSettings.getSeverity(rule, HintsSettings.getPreferences(manager, rule, HintsSettings.getCurrentProfileId()));
    }

    public static boolean isEnabled(GsfHintsManager manager, Rule.UserConfigurableRule hint) {
        Preferences p = HintsSettings.getPreferences(manager, hint, HintsSettings.getCurrentProfileId());
        return HintsSettings.isEnabled(manager, hint, p);
    }

    public static boolean isShowInTaskList(GsfHintsManager manager, Rule.UserConfigurableRule hint) {
        Preferences p = HintsSettings.getPreferences(manager, hint, HintsSettings.getCurrentProfileId());
        return HintsSettings.isShowInTaskList(hint, p);
    }

    public static boolean isEnabled(HintsProvider.HintsManager manager, Rule.UserConfigurableRule hint, Preferences preferences) {
        return preferences.getBoolean("enabled", hint.getDefaultEnabled());
    }

    public static void setEnabled(Preferences p, boolean value) {
        p.putBoolean("enabled", value);
    }

    public static boolean isShowInTaskList(Rule.UserConfigurableRule hint, Preferences preferences) {
        return preferences.getBoolean("inTaskList", hint.showInTasklist());
    }

    public static void setShowInTaskList(Preferences p, boolean value) {
        p.putBoolean("inTaskList", value);
    }

    public static HintSeverity getSeverity(Rule.UserConfigurableRule hint, Preferences preferences) {
        String s = preferences.get("severity", null);
        return s == null ? hint.getDefaultSeverity() : HintSeverity.valueOf(s);
    }

    public static void setSeverity(Preferences p, HintSeverity severity) {
        p.put("severity", severity.name());
    }
}

