/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.hints.infrastructure.SelectionHintsTask;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public class SelectionHintsTaskFactory
extends AbstractTaskFactory {
    public SelectionHintsTaskFactory() {
        super(true);
    }

    @Override
    public Collection<? extends SchedulerTask> createTasks(Language l, Snapshot snapshot) {
        return Collections.singleton(new SelectionHintsTask());
    }
}

