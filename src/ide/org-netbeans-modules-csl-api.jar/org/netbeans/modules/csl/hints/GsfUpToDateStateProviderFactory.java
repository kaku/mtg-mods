/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatusProviderFactory
 */
package org.netbeans.modules.csl.hints;

import javax.swing.text.Document;
import org.netbeans.modules.csl.hints.GsfUpToDateStateProvider;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProviderFactory;

public class GsfUpToDateStateProviderFactory
implements UpToDateStatusProviderFactory {
    public UpToDateStatusProvider createUpToDateStatusProvider(Document document) {
        return new GsfUpToDateStateProvider();
    }
}

