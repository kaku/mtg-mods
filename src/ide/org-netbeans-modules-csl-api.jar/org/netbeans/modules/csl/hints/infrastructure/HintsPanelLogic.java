/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.hints.infrastructure.HintsSettings;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

class HintsPanelLogic
implements MouseListener,
KeyListener,
TreeSelectionListener,
ChangeListener,
ActionListener {
    private static final Logger LOG = Logger.getLogger(HintsPanelLogic.class.getName());
    private Map<Rule.UserConfigurableRule, ModifiedPreferences> changes;
    private static final Map<HintSeverity, Integer> severity2index = new HashMap<HintSeverity, Integer>();
    private static final String DESCRIPTION_HEADER = "<html><head></head><body>";
    private static final String DESCRIPTION_FOOTER = "</body></html>";
    private JTree errorTree;
    private JComboBox severityComboBox;
    private JCheckBox tasklistCheckBox;
    private JPanel customizerPanel;
    private JEditorPane descriptionTextArea;
    private GsfHintsManager manager;
    private DefaultTreeModel errModel;
    private boolean ignoreControlChanges;

    HintsPanelLogic(GsfHintsManager manager) {
        this.manager = manager;
        this.changes = new HashMap<Rule.UserConfigurableRule, ModifiedPreferences>();
    }

    void connect(JTree errorTree, DefaultTreeModel errModel, JComboBox severityComboBox, JCheckBox tasklistCheckBox, JPanel customizerPanel, JEditorPane descriptionTextArea) {
        this.errorTree = errorTree;
        this.errModel = errModel;
        this.severityComboBox = severityComboBox;
        this.tasklistCheckBox = tasklistCheckBox;
        this.customizerPanel = customizerPanel;
        this.descriptionTextArea = descriptionTextArea;
        this.valueChanged(null);
        errorTree.addKeyListener(this);
        errorTree.addMouseListener(this);
        errorTree.getSelectionModel().addTreeSelectionListener(this);
        severityComboBox.addActionListener(this);
        tasklistCheckBox.addChangeListener(this);
    }

    void disconnect() {
        this.errorTree.removeKeyListener(this);
        this.errorTree.removeMouseListener(this);
        this.errorTree.getSelectionModel().removeTreeSelectionListener(this);
        this.severityComboBox.removeActionListener(this);
        this.tasklistCheckBox.removeChangeListener(this);
        this.componentsSetEnabled(false);
    }

    synchronized void applyChanges() {
        for (Rule.UserConfigurableRule hint : this.changes.keySet()) {
            ModifiedPreferences mn = this.changes.get(hint);
            mn.store(HintsSettings.getPreferences(this.manager, hint, HintsSettings.getCurrentProfileId()));
        }
        this.updateHints();
    }

    private void updateHints() {
        Document doc;
        Source source;
        JTextComponent pane = EditorRegistry.lastFocusedComponent();
        if (pane != null && (source = Source.create((Document)(doc = pane.getDocument()))) != null && source.getFileObject() != null) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    try {
                        ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                            public void run(ResultIterator resultIterator) throws Exception {
                                GsfHintsManager.refreshHints(resultIterator);
                            }
                        });
                    }
                    catch (ParseException ex) {
                        LOG.log(Level.WARNING, null, (Throwable)ex);
                    }
                }

            });
        }
    }

    boolean isChanged() {
        boolean isChanged = false;
        for (Rule.UserConfigurableRule hint : this.changes.keySet()) {
            String savedSeverity;
            Boolean savedEnabled;
            ModifiedPreferences mn = this.changes.get(hint);
            Boolean currentEnabled = mn.getBoolean("enabled", hint.getDefaultEnabled());
            if (isChanged |= currentEnabled != (savedEnabled = Boolean.valueOf(HintsSettings.isEnabled(this.manager, hint)))) {
                return true;
            }
            String currentSeverity = mn.get("severity", hint.getDefaultSeverity().toString());
            if (isChanged |= !currentSeverity.equals(savedSeverity = HintsSettings.getSeverity(this.manager, hint).toString())) {
                return true;
            }
            try {
                for (String key : mn.keys()) {
                    if (key.equals("enabled") || key.equals("severity")) continue;
                    String current = mn.get(key, null);
                    String saved = mn.getSavedValue(key);
                    boolean bl = current == null ? saved != null : !current.equals(saved);
                    if (!(isChanged |= bl)) continue;
                    return true;
                }
                continue;
            }
            catch (BackingStoreException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                continue;
            }
        }
        return false;
    }

    synchronized Preferences getCurrentPrefernces(Rule.UserConfigurableRule hint) {
        Preferences node = this.changes.get(hint);
        return node == null ? HintsSettings.getPreferences(this.manager, hint, HintsSettings.getCurrentProfileId()) : node;
    }

    synchronized Preferences getPreferences4Modification(Rule.UserConfigurableRule hint) {
        Preferences node = this.changes.get(hint);
        if (node == null) {
            node = new ModifiedPreferences(HintsSettings.getPreferences(this.manager, hint, HintsSettings.getCurrentProfileId()));
            this.changes.put(hint, (ModifiedPreferences)node);
        }
        return node;
    }

    static Object getUserObject(TreePath path) {
        if (path == null) {
            return null;
        }
        DefaultMutableTreeNode tn = (DefaultMutableTreeNode)path.getLastPathComponent();
        return tn.getUserObject();
    }

    static Object getUserObject(DefaultMutableTreeNode node) {
        return node.getUserObject();
    }

    boolean isSelected(DefaultMutableTreeNode node) {
        for (int i = 0; i < node.getChildCount(); ++i) {
            Rule.UserConfigurableRule hint;
            DefaultMutableTreeNode ch = (DefaultMutableTreeNode)node.getChildAt(i);
            Object o = ch.getUserObject();
            if (!(o instanceof Rule) || !HintsSettings.isEnabled(this.manager, hint = (Rule.UserConfigurableRule)o, this.getCurrentPrefernces(hint))) continue;
            return true;
        }
        return false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Rectangle r;
        Point p = e.getPoint();
        TreePath path = this.errorTree.getPathForLocation(e.getPoint().x, e.getPoint().y);
        if (path != null && (r = this.errorTree.getPathBounds(path)) != null) {
            r.width = r.height;
            if (r.contains(p)) {
                this.toggle(path);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        TreePath path;
        JTree tree;
        if ((e.getKeyCode() == 32 || e.getKeyCode() == 10) && e.getSource() instanceof JTree && this.toggle(path = (tree = (JTree)e.getSource()).getSelectionPath())) {
            e.consume();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void valueChanged(TreeSelectionEvent ex) {
        Object o = HintsPanelLogic.getUserObject(this.errorTree.getSelectionPath());
        if (o instanceof Rule.UserConfigurableRule) {
            Rule.UserConfigurableRule hint = (Rule.UserConfigurableRule)o;
            this.componentsSetEnabled(true);
            Preferences p = this.getCurrentPrefernces(hint);
            this.ignoreControlChanges = true;
            try {
                HintSeverity severity = HintsSettings.getSeverity(hint, p);
                this.severityComboBox.setSelectedIndex(severity2index.get((Object)severity));
                boolean toTasklist = HintsSettings.isShowInTaskList(hint, p);
                this.tasklistCheckBox.setSelected(toTasklist);
                String description = hint.getDescription();
                this.descriptionTextArea.setText(description == null ? "" : this.wrapDescription(description));
            }
            finally {
                this.ignoreControlChanges = false;
            }
            this.customizerPanel.removeAll();
            JComponent c = hint.getCustomizer(this.getPreferences4Modification(hint));
            if (c != null) {
                this.customizerPanel.add((Component)c, "Center");
            }
            this.customizerPanel.getParent().invalidate();
            ((JComponent)this.customizerPanel.getParent()).revalidate();
            this.customizerPanel.getParent().repaint();
        } else {
            this.componentsSetEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.errorTree.getSelectionPath() == null || this.ignoreControlChanges) {
            return;
        }
        Object o = HintsPanelLogic.getUserObject(this.errorTree.getSelectionPath());
        if (o instanceof Rule.UserConfigurableRule) {
            Rule.UserConfigurableRule hint = (Rule.UserConfigurableRule)o;
            Preferences p = this.getPreferences4Modification(hint);
            if (this.severityComboBox.equals(e.getSource())) {
                HintsSettings.setSeverity(p, this.index2severity(this.severityComboBox.getSelectedIndex()));
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
    }

    private String wrapDescription(String description) {
        return new StringBuffer("<html><head></head><body>").append(description).append("</body></html>").toString();
    }

    private HintSeverity index2severity(int index) {
        for (Map.Entry<HintSeverity, Integer> e : severity2index.entrySet()) {
            if (e.getValue() != index) continue;
            return e.getKey();
        }
        throw new IllegalStateException("Unknown severity");
    }

    private boolean toggle(TreePath treePath) {
        if (treePath == null) {
            return false;
        }
        Object o = HintsPanelLogic.getUserObject(treePath);
        DefaultTreeModel model = this.errModel;
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)treePath.getLastPathComponent();
        if (o instanceof Rule.UserConfigurableRule) {
            Rule.UserConfigurableRule hint = (Rule.UserConfigurableRule)o;
            boolean value = HintsSettings.isEnabled(this.manager, hint, this.getCurrentPrefernces(hint));
            Preferences mn = this.getPreferences4Modification(hint);
            HintsSettings.setEnabled(mn, !value);
            model.nodeChanged(node);
            model.nodeChanged(node.getParent());
        } else if (o instanceof FileObject) {
            boolean value = !this.isSelected(node);
            for (int i = 0; i < node.getChildCount(); ++i) {
                boolean cv;
                Rule.UserConfigurableRule hint;
                DefaultMutableTreeNode ch = (DefaultMutableTreeNode)node.getChildAt(i);
                Object cho = ch.getUserObject();
                if (!(cho instanceof Rule.UserConfigurableRule) || (cv = HintsSettings.isEnabled(this.manager, hint = (Rule.UserConfigurableRule)cho, this.getCurrentPrefernces(hint))) == value) continue;
                Preferences mn = this.getPreferences4Modification(hint);
                HintsSettings.setEnabled(mn, value);
                model.nodeChanged(ch);
            }
            model.nodeChanged(node);
        }
        return false;
    }

    private void componentsSetEnabled(boolean enabled) {
        if (!enabled) {
            this.customizerPanel.removeAll();
            this.customizerPanel.getParent().invalidate();
            ((JComponent)this.customizerPanel.getParent()).revalidate();
            this.customizerPanel.getParent().repaint();
            this.severityComboBox.setSelectedIndex(severity2index.get((Object)HintsSettings.SEVERITY_DEFAUT));
            this.tasklistCheckBox.setSelected(true);
            this.descriptionTextArea.setText("");
        }
        this.severityComboBox.setEnabled(enabled);
        this.tasklistCheckBox.setEnabled(enabled);
        this.descriptionTextArea.setEnabled(enabled);
    }

    static {
        severity2index.put(HintSeverity.ERROR, 0);
        severity2index.put(HintSeverity.WARNING, 1);
        severity2index.put(HintSeverity.CURRENT_LINE_WARNING, 2);
        severity2index.put(HintSeverity.INFO, 3);
    }

    private static class ModifiedPreferences
    extends AbstractPreferences {
        private Map<String, Object> map = new HashMap<String, Object>();
        private final Map<String, String> mapSaved = new HashMap<String, String>();
        private boolean modified;

        public ModifiedPreferences(Preferences node) {
            super(null, "");
            try {
                for (String key : node.keys()) {
                    this.put(key, node.get(key, null));
                }
                this.modified = false;
            }
            catch (BackingStoreException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        public boolean isModified() {
            return this.modified;
        }

        public void store(Preferences target) {
            try {
                for (String key : this.keys()) {
                    target.put(key, this.get(key, null));
                }
                this.modified = false;
            }
            catch (BackingStoreException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        public String getSavedValue(String key) {
            return this.mapSaved.get(key);
        }

        @Override
        protected void putSpi(String key, String value) {
            this.modified = true;
            this.map.put(key, value);
            if (!this.mapSaved.containsKey(key)) {
                this.mapSaved.put(key, value);
            }
        }

        @Override
        protected String getSpi(String key) {
            return (String)this.map.get(key);
        }

        @Override
        protected void removeSpi(String key) {
            this.modified = true;
            this.map.remove(key);
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            String[] array = new String[this.map.keySet().size()];
            return this.map.keySet().toArray(array);
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

