/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.diff.DiffController
 *  org.netbeans.api.diff.DiffController$DiffPane
 *  org.netbeans.api.diff.DiffController$LocationType
 *  org.netbeans.api.diff.Difference
 *  org.netbeans.api.diff.StreamSource
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.spi.editor.hints.ChangeInfo
 *  org.netbeans.spi.editor.hints.EnhancedFix
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.text.NbDocument
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.diff.DiffController;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.api.lexer.Language;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.EditList;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.PreviewableFix;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.spi.editor.hints.ChangeInfo;
import org.netbeans.spi.editor.hints.EnhancedFix;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.text.NbDocument;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

final class PreviewHintFix
implements EnhancedFix {
    private ParserResult info;
    private PreviewableFix fix;
    private final String sortText;

    PreviewHintFix(ParserResult info, PreviewableFix fix, String sortText) {
        this.info = info;
        this.fix = fix;
        this.sortText = sortText;
    }

    public String getText() {
        return "    " + NbBundle.getMessage(PreviewHintFix.class, (String)"PreviewHint");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ChangeInfo implement() throws Exception {
        EditList edits = this.fix.getEditList();
        Document oldDoc = this.info.getSnapshot().getSource().getDocument(true);
        OffsetRange range = new OffsetRange(0, oldDoc.getLength());
        String oldSource = oldDoc.getText(range.getStart(), range.getEnd());
        String mimeType = (String)oldDoc.getProperty("mimeType");
        BaseDocument newDoc = new BaseDocument(false, mimeType);
        Language language = (Language)oldDoc.getProperty(Language.class);
        newDoc.putProperty(Language.class, (Object)language);
        newDoc.insertString(0, oldSource, null);
        edits.applyToDocument(newDoc);
        String newSource = newDoc.getText(0, newDoc.getLength());
        String oldTitle = NbBundle.getMessage(PreviewHintFix.class, (String)"CurrentSource");
        String newTitle = NbBundle.getMessage(PreviewHintFix.class, (String)"FixedSource");
        final DiffController diffView = DiffController.create((StreamSource)new DiffSource(oldSource, oldTitle), (StreamSource)new DiffSource(newSource, newTitle));
        JComponent jc = diffView.getJComponent();
        jc.setPreferredSize(new Dimension(800, 600));
        boolean index = false;
        final int firstLine = diffView.getDifferenceCount() == 0 ? NbDocument.findLineNumber((StyledDocument)((StyledDocument)oldDoc), (int)edits.getRange().getStart()) : -1;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (firstLine != -1) {
                    diffView.setLocation(DiffController.DiffPane.Base, DiffController.LocationType.LineNumber, firstLine);
                } else if (diffView.getDifferenceCount() > 0) {
                    diffView.setLocation(DiffController.DiffPane.Base, DiffController.LocationType.DifferenceIndex, 0);
                }
            }
        });
        JButton apply = new JButton(NbBundle.getMessage(PreviewHintFix.class, (String)"Apply"));
        JButton ok = new JButton(NbBundle.getMessage(PreviewHintFix.class, (String)"Ok"));
        JButton cancel = new JButton(NbBundle.getMessage(PreviewHintFix.class, (String)"Cancel"));
        String dialogTitle = NbBundle.getMessage(PreviewHintFix.class, (String)"PreviewTitle", (Object)this.fix.getDescription());
        DialogDescriptor descriptor = new DialogDescriptor((Object)jc, dialogTitle, true, new Object[]{apply, ok, cancel}, (Object)ok, 0, null, null, true);
        Dialog dlg = null;
        try {
            dlg = DialogDisplayer.getDefault().createDialog(descriptor);
            dlg.setVisible(true);
            if (descriptor.getValue() == apply) {
                this.fix.implement();
            }
        }
        finally {
            if (dlg != null) {
                dlg.dispose();
            }
        }
        return null;
    }

    public CharSequence getSortText() {
        return this.sortText;
    }

    private class DiffSource
    extends StreamSource {
        private String source;
        private String title;

        private DiffSource(String source, String title) {
            this.source = source;
            this.title = title;
        }

        public String getName() {
            return "?";
        }

        public String getTitle() {
            return this.title;
        }

        public String getMIMEType() {
            return PreviewHintFix.this.info.getSnapshot().getMimeType();
        }

        public Reader createReader() throws IOException {
            return new StringReader(this.source);
        }

        public Writer createWriter(Difference[] conflicts) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean isEditable() {
            return false;
        }
    }

}

