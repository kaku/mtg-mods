/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.hints.ChangeInfo
 *  org.netbeans.spi.editor.hints.EnhancedFix
 */
package org.netbeans.modules.csl.hints.infrastructure;

import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.spi.editor.hints.ChangeInfo;
import org.netbeans.spi.editor.hints.EnhancedFix;

final class FixWrapper
implements EnhancedFix {
    private HintFix fix;
    private String sortText;

    FixWrapper(HintFix fix, String sortText) {
        this.fix = fix;
        this.sortText = sortText;
    }

    public String getText() {
        return this.fix.getDescription();
    }

    public ChangeInfo implement() throws Exception {
        this.fix.implement();
        return null;
    }

    public CharSequence getSortText() {
        return this.sortText;
    }
}

