/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.HintsController;
import org.openide.filesystems.FileObject;

public class SelectionHintsTask
extends ParserResultTask<ParserResult> {
    private static final Logger LOG = Logger.getLogger(SelectionHintsTask.class.getName());
    private volatile boolean cancelled = false;
    private volatile HintsProvider pendingProvider;

    public void run(ParserResult result, SchedulerEvent event) {
        this.resume();
        final FileObject fileObject = result.getSnapshot().getSource().getFileObject();
        if (fileObject == null || this.isCancelled()) {
            return;
        }
        if (!(event instanceof CursorMovedSchedulerEvent) || this.isCancelled()) {
            return;
        }
        CursorMovedSchedulerEvent evt = (CursorMovedSchedulerEvent)event;
        final int[] range = new int[]{Math.min(evt.getMarkOffset(), evt.getCaretOffset()), Math.max(evt.getMarkOffset(), evt.getCaretOffset())};
        if (range == null || range.length != 2 || range[0] == -1 || range[1] == -1 || range[0] == range[1]) {
            HintsController.setErrors((FileObject)fileObject, (String)SelectionHintsTask.class.getName(), Collections.emptyList());
            return;
        }
        try {
            ParserManager.parse(Collections.singleton(result.getSnapshot().getSource()), (UserTask)new UserTask(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 * Enabled aggressive block sorting
                 * Enabled unnecessary exception pruning
                 * Enabled aggressive exception aggregation
                 */
                public void run(ResultIterator resultIterator) throws Exception {
                    Parser.Result r = resultIterator.getParserResult(range[0]);
                    if (!(r instanceof ParserResult)) {
                        return;
                    }
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(r.getSnapshot().getMimeType());
                    if (language == null || SelectionHintsTask.this.isCancelled()) {
                        return;
                    }
                    HintsProvider provider = language.getHintsProvider();
                    if (provider == null || SelectionHintsTask.this.isCancelled()) {
                        return;
                    }
                    GsfHintsManager manager = language.getHintsManager();
                    if (manager == null || SelectionHintsTask.this.isCancelled()) {
                        return;
                    }
                    ArrayList<ErrorDescription> description = new ArrayList<ErrorDescription>();
                    ArrayList<Hint> hints = new ArrayList<Hint>();
                    RuleContext ruleContext = manager.createRuleContext((ParserResult)r, language, -1, range[0], range[1]);
                    if (ruleContext != null) {
                        try {
                             var9_9 = this;
                            synchronized (var9_9) {
                                SelectionHintsTask.this.pendingProvider = provider;
                                if (SelectionHintsTask.this.isCancelled()) {
                                    return;
                                }
                            }
                            provider.computeSelectionHints(manager, ruleContext, hints, range[0], range[1]);
                        }
                        finally {
                            SelectionHintsTask.this.pendingProvider = null;
                        }
                        for (int i = 0; i < hints.size(); ++i) {
                            Hint hint = hints.get(i);
                            if (SelectionHintsTask.this.isCancelled()) {
                                return;
                            }
                            ErrorDescription desc = manager.createDescription(hint, ruleContext, false, i == hints.size() - 1);
                            description.add(desc);
                        }
                    }
                    if (SelectionHintsTask.this.isCancelled()) {
                        return;
                    }
                    HintsController.setErrors((FileObject)fileObject, (String)SelectionHintsTask.class.getName(), description);
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        SelectionHintsTask selectionHintsTask = this;
        synchronized (selectionHintsTask) {
            this.cancelled = true;
        }
        HintsProvider p = this.pendingProvider;
        if (p != null) {
            p.cancel();
        }
    }

    private synchronized void resume() {
        this.cancelled = false;
    }

    private synchronized boolean isCancelled() {
        return this.cancelled;
    }

}

