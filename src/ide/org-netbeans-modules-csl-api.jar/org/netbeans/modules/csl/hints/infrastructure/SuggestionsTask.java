/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.hints.infrastructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.HintsController;
import org.openide.filesystems.FileObject;

public class SuggestionsTask
extends ParserResultTask<ParserResult> {
    private static final Logger LOG = Logger.getLogger(SuggestionsTask.class.getName());
    private volatile boolean cancelled = false;
    private volatile HintsProvider pendingProvider;

    public void run(ParserResult result, SchedulerEvent event) {
        this.resume();
        FileObject fileObject = result.getSnapshot().getSource().getFileObject();
        if (fileObject == null || this.isCancelled()) {
            return;
        }
        if (!(event instanceof CursorMovedSchedulerEvent) || this.isCancelled()) {
            return;
        }
        CursorMovedSchedulerEvent evt = (CursorMovedSchedulerEvent)event;
        int[] range = new int[]{Math.min(evt.getMarkOffset(), evt.getCaretOffset()), Math.max(evt.getMarkOffset(), evt.getCaretOffset())};
        if (range != null && range.length == 2 && range[0] != -1 && range[1] != -1 && range[0] != range[1]) {
            HintsController.setErrors((FileObject)fileObject, (String)SuggestionsTask.class.getName(), Collections.emptyList());
            return;
        }
        final int pos = evt.getCaretOffset();
        if (pos == -1 || this.isCancelled()) {
            return;
        }
        try {
            ParserManager.parse(Collections.singleton(result.getSnapshot().getSource()), (UserTask)new UserTask(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 * Enabled aggressive block sorting
                 * Enabled unnecessary exception pruning
                 * Enabled aggressive exception aggregation
                 */
                public void run(ResultIterator resultIterator) throws Exception {
                    RuleContext ruleContext;
                    ArrayList<ErrorDescription> descriptions;
                    ArrayList<Hint> hints;
                    GsfHintsManager manager;
                    Parser.Result r;
                    OffsetRange linerange;
                    r = resultIterator.getParserResult(pos);
                    if (!(r instanceof ParserResult)) {
                        return;
                    }
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(r.getSnapshot().getMimeType());
                    if (language == null || SuggestionsTask.this.isCancelled()) {
                        return;
                    }
                    HintsProvider provider = language.getHintsProvider();
                    if (provider == null || SuggestionsTask.this.isCancelled()) {
                        return;
                    }
                    manager = language.getHintsManager();
                    if (manager == null || SuggestionsTask.this.isCancelled()) {
                        return;
                    }
                    ruleContext = manager.createRuleContext((ParserResult)r, language, pos, -1, -1);
                    if (ruleContext == null || SuggestionsTask.this.isCancelled()) {
                        return;
                    }
                    descriptions = new ArrayList<ErrorDescription>();
                    hints = new ArrayList<Hint>();
                    linerange = SuggestionsTask.findLineBoundaries(resultIterator.getSnapshot().getText(), pos);
                    try {
                         var10_10 = this;
                        synchronized (var10_10) {
                            SuggestionsTask.this.pendingProvider = provider;
                            if (SuggestionsTask.this.isCancelled()) {
                                return;
                            }
                        }
                        provider.computeSuggestions(manager, ruleContext, hints, pos);
                    }
                    finally {
                        SuggestionsTask.this.pendingProvider = null;
                    }
                    for (int i = 0; i < hints.size(); ++i) {
                        Hint hint = hints.get(i);
                        if (SuggestionsTask.this.isCancelled()) {
                            return;
                        }
                        if (linerange != OffsetRange.NONE && !SuggestionsTask.overlaps(linerange, hint.getRange())) continue;
                        ErrorDescription desc = manager.createDescription(hint, ruleContext, false, i == hints.size() - 1);
                        descriptions.add(desc);
                    }
                    if (SuggestionsTask.this.isCancelled()) {
                        return;
                    }
                    HintsController.setErrors((FileObject)r.getSnapshot().getSource().getFileObject(), (String)SuggestionsTask.class.getName(), descriptions);
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
    }

    private static boolean overlaps(OffsetRange line, OffsetRange hint) {
        return hint.overlaps(line) || hint.isEmpty() && line.containsInclusive(hint.getStart());
    }

    private static OffsetRange findLineBoundaries(CharSequence s, int position) {
        int max;
        int min;
        int l = s.length();
        if (position == -1 || position > l) {
            return OffsetRange.NONE;
        }
        if (position == l && l >= 1 && s.charAt(l - 1) == '\n') {
            return new OffsetRange(l - 1, l);
        }
        for (min = position; min > 1 && s.charAt(min - 1) != '\n'; --min) {
        }
        for (max = position; max < l && s.charAt(max) != '\n'; ++max) {
        }
        return new OffsetRange(min, max);
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        SuggestionsTask suggestionsTask = this;
        synchronized (suggestionsTask) {
            this.cancelled = true;
        }
        HintsProvider p = this.pendingProvider;
        if (p != null) {
            p.cancel();
        }
    }

    private synchronized void resume() {
        this.cancelled = false;
    }

    private synchronized boolean isCancelled() {
        return this.cancelled;
    }

}

