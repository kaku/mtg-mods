/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.ErrorManager
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.api;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;
import java.util.MissingResourceException;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.ErrorManager;
import org.openide.util.NbBundle;

public final class SelectCodeElementAction
extends BaseAction {
    public static final String selectNextElementAction = "select-element-next";
    public static final String selectPreviousElementAction = "select-element-previous";
    private boolean selectNext;

    public SelectCodeElementAction(String name, boolean selectNext) {
        super(name);
        this.selectNext = selectNext;
        String desc = this.getShortDescription();
        if (desc != null) {
            this.putValue("ShortDescription", (Object)desc);
        }
    }

    public String getShortDescription() {
        String shortDesc;
        String name = (String)this.getValue("Name");
        if (name == null) {
            return null;
        }
        try {
            shortDesc = NbBundle.getBundle(SelectCodeElementAction.class).getString(name);
        }
        catch (MissingResourceException mre) {
            shortDesc = name;
        }
        return shortDesc;
    }

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        if (target != null) {
            int selectionStartOffset = target.getSelectionStart();
            int selectionEndOffset = target.getSelectionEnd();
            if (selectionEndOffset > selectionStartOffset || this.selectNext) {
                SelectionHandler handler = (SelectionHandler)target.getClientProperty(SelectionHandler.class);
                if (handler == null) {
                    handler = new SelectionHandler(target);
                    target.addCaretListener(handler);
                    target.putClientProperty(SelectionHandler.class, handler);
                }
                if (this.selectNext) {
                    handler.selectNext();
                } else {
                    handler.selectPrevious();
                }
            }
        }
    }

    private static final class SelectionInfo {
        private int startOffset;
        private int endOffset;

        SelectionInfo(int startOffset, int endOffset) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        public int getStartOffset() {
            return this.startOffset;
        }

        public int getEndOffset() {
            return this.endOffset;
        }
    }

    private static final class SelectionHandler
    extends UserTask
    implements CaretListener,
    Runnable {
        private JTextComponent target;
        private SelectionInfo[] selectionInfos;
        private int selIndex = -1;
        private boolean ignoreNextCaretUpdate;

        SelectionHandler(JTextComponent target) {
            this.target = target;
        }

        public void selectNext() {
            if (this.selectionInfos == null) {
                Source source = Source.create((Document)this.target.getDocument());
                try {
                    ParserManager.parse(Collections.singleton(source), (UserTask)this);
                }
                catch (ParseException ex) {
                    ErrorManager.getDefault().notify((Throwable)ex);
                }
            }
            if (this.selectionInfos != null) {
                this.run();
            }
        }

        public synchronized void selectPrevious() {
            if (this.selIndex == -1) {
                this.selIndex = this.computeSelIndex(false);
            }
            if (this.selIndex > 0) {
                this.select(this.selectionInfos[--this.selIndex]);
            }
        }

        private void select(SelectionInfo selectionInfo) {
            Caret caret = this.target.getCaret();
            this.markIgnoreNextCaretUpdate();
            caret.setDot(selectionInfo.getStartOffset());
            this.markIgnoreNextCaretUpdate();
            caret.moveDot(selectionInfo.getEndOffset());
        }

        private void markIgnoreNextCaretUpdate() {
            this.ignoreNextCaretUpdate = true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void caretUpdate(CaretEvent e) {
            if (!this.ignoreNextCaretUpdate) {
                SelectionHandler selectionHandler = this;
                synchronized (selectionHandler) {
                    this.selectionInfos = null;
                    this.selIndex = -1;
                }
            }
            this.ignoreNextCaretUpdate = false;
        }

        public void cancel() {
        }

        public void run(ResultIterator resultIterator) throws ParseException {
            Parser.Result parserResult = resultIterator.getParserResult(this.target.getCaretPosition());
            if (!(parserResult instanceof ParserResult)) {
                return;
            }
            this.selectionInfos = this.initSelectionPath(this.target, (ParserResult)parserResult);
        }

        private KeystrokeHandler getBracketCompletion(Document doc, int offset) {
            BaseDocument baseDoc = (BaseDocument)doc;
            List<Language> list = LanguageRegistry.getInstance().getEmbeddedLanguages(baseDoc, offset);
            for (Language l : list) {
                if (l.getBracketCompletion() == null) continue;
                return l.getBracketCompletion();
            }
            return null;
        }

        private SelectionInfo[] initSelectionPath(JTextComponent target, ParserResult parserResult) {
            KeystrokeHandler bc = this.getBracketCompletion(target.getDocument(), target.getCaretPosition());
            if (bc != null) {
                List<OffsetRange> ranges = bc.findLogicalRanges(parserResult, target.getCaretPosition());
                SelectionInfo[] result = new SelectionInfo[ranges.size()];
                for (int i = 0; i < ranges.size(); ++i) {
                    OffsetRange range = ranges.get(i);
                    result[i] = new SelectionInfo(range.getStart(), range.getEnd());
                }
                return result;
            }
            return new SelectionInfo[0];
        }

        private int computeSelIndex(boolean inner) {
            Caret caret = this.target.getCaret();
            if (this.selectionInfos != null && caret != null && caret.getDot() != caret.getMark()) {
                int i;
                int dot = caret.getDot();
                int mark = caret.getMark();
                int start = Math.min(dot, mark);
                for (i = 0; i < this.selectionInfos.length; ++i) {
                    if (this.selectionInfos[i].getStartOffset() != start) continue;
                    return i;
                }
                for (i = this.selectionInfos.length - 2; i >= 0; --i) {
                    if (this.selectionInfos[i].getStartOffset() <= start || this.selectionInfos[i + 1].getStartOffset() >= start) continue;
                    return inner ? i : i - 1;
                }
            }
            return this.selIndex;
        }

        @Override
        public void run() {
            if (this.selIndex == -1) {
                this.selIndex = this.computeSelIndex(true);
            }
            if (this.selIndex < this.selectionInfos.length - 1) {
                this.select(this.selectionInfos[++this.selIndex]);
            }
        }
    }

}

