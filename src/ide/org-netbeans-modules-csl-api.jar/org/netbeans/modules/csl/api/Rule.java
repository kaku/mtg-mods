/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.RuleContext;

public interface Rule {
    public boolean appliesTo(RuleContext var1);

    public String getDisplayName();

    public boolean showInTasklist();

    public HintSeverity getDefaultSeverity();

    public static interface SelectionRule
    extends Rule {
    }

    public static interface ErrorRule
    extends Rule {
        public Set<?> getCodes();
    }

    public static interface AstRule
    extends UserConfigurableRule {
        public Set<?> getKinds();
    }

    public static interface UserConfigurableRule
    extends Rule {
        public String getId();

        public String getDescription();

        public boolean getDefaultEnabled();

        public JComponent getCustomizer(Preferences var1);
    }

}

