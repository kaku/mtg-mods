/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.api;

import java.util.Collections;
import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;

public interface ElementHandle {
    @CheckForNull
    public FileObject getFileObject();

    @CheckForNull
    public String getMimeType();

    @NonNull
    public String getName();

    @CheckForNull
    public String getIn();

    @NonNull
    public ElementKind getKind();

    @NonNull
    public Set<Modifier> getModifiers();

    public boolean signatureEquals(@NonNull ElementHandle var1);

    public OffsetRange getOffsetRange(@NonNull ParserResult var1);

    public static class UrlHandle
    implements ElementHandle {
        private String url;

        public UrlHandle(@NonNull String url) {
            this.url = url;
        }

        @Override
        public FileObject getFileObject() {
            return null;
        }

        @Override
        public String getMimeType() {
            return null;
        }

        @Override
        public boolean signatureEquals(ElementHandle handle) {
            if (handle instanceof UrlHandle) {
                return this.url.equals(((UrlHandle)handle).url);
            }
            return false;
        }

        @Override
        public OffsetRange getOffsetRange(@NonNull ParserResult result) {
            return null;
        }

        @NonNull
        public String getUrl() {
            return this.url;
        }

        @Override
        public String getName() {
            return this.url;
        }

        @Override
        public String getIn() {
            return null;
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.OTHER;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }
    }

}

