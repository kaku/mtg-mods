/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.api.PreviousCamelCasePosition;

public class SelectPreviousCamelCasePosition
extends PreviousCamelCasePosition {
    public static final String selectPreviousCamelCasePosition = "select-previous-camel-case-position";

    public SelectPreviousCamelCasePosition(Action originalAction) {
        this("select-previous-camel-case-position", originalAction);
    }

    protected SelectPreviousCamelCasePosition(String name, Action originalAction) {
        super(name, originalAction);
    }

    @Override
    protected void moveToNewOffset(JTextComponent textComponent, int offset) throws BadLocationException {
        textComponent.getCaret().moveDot(offset);
    }
}

