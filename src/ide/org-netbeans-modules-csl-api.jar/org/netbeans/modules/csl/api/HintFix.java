/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

public interface HintFix {
    public String getDescription();

    public void implement() throws Exception;

    public boolean isSafe();

    public boolean isInteractive();
}

