/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.spi.ParserResult;

public abstract class CodeCompletionContext {
    public abstract int getCaretOffset();

    public abstract ParserResult getParserResult();

    @NonNull
    public abstract String getPrefix();

    @NonNull
    public abstract CodeCompletionHandler.QueryType getQueryType();

    public abstract boolean isPrefixMatch();

    public abstract boolean isCaseSensitive();
}

