/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.api;

import java.util.Set;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.Project;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public interface IndexSearcher {
    @NonNull
    public Set<? extends Descriptor> getTypes(@NullAllowed Project var1, @NonNull String var2, @NonNull QuerySupport.Kind var3, @NonNull Helper var4);

    @NonNull
    public Set<? extends Descriptor> getSymbols(@NullAllowed Project var1, @NonNull String var2, @NonNull QuerySupport.Kind var3, @NonNull Helper var4);

    public static interface Helper {
        @CheckForNull
        public Icon getIcon(@NonNull ElementHandle var1);

        public void open(@NonNull FileObject var1, @NonNull ElementHandle var2);
    }

    public static abstract class Descriptor {
        @NonNull
        public abstract ElementHandle getElement();

        public abstract String getSimpleName();

        public abstract String getOuterName();

        public abstract String getTypeName();

        public abstract String getContextName();

        public abstract Icon getIcon();

        public abstract String getProjectName();

        public abstract Icon getProjectIcon();

        public abstract FileObject getFileObject();

        public abstract int getOffset();

        public abstract void open();

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Descriptor)) {
                return false;
            }
            Descriptor other = (Descriptor)obj;
            FileObject thisFo = this.getFileObject();
            FileObject otherFo = other.getFileObject();
            return thisFo == null ? otherFo == null : thisFo.equals((Object)otherFo);
        }

        public int hashCode() {
            FileObject fo = this.getFileObject();
            return fo == null ? 0 : fo.hashCode();
        }
    }

}

