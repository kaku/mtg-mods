/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;

public interface StructureItem {
    @NonNull
    public String getName();

    @NonNull
    public String getSortText();

    @NonNull
    public String getHtml(@NonNull HtmlFormatter var1);

    @NonNull
    public ElementHandle getElementHandle();

    @NonNull
    public ElementKind getKind();

    @NonNull
    public Set<Modifier> getModifiers();

    public boolean isLeaf();

    @NonNull
    public List<? extends StructureItem> getNestedItems();

    public long getPosition();

    public long getEndPosition();

    @CheckForNull
    public ImageIcon getCustomIcon();

    public boolean equals(Object var1);

    public int hashCode();

    public static interface CollapsedDefault
    extends StructureItem {
        public boolean isCollapsedByDefault();
    }

}

