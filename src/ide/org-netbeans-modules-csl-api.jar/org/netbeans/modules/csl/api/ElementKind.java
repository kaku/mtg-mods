/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

public enum ElementKind {
    CONSTRUCTOR,
    MODULE,
    PACKAGE,
    CLASS,
    METHOD,
    FIELD,
    VARIABLE,
    ATTRIBUTE,
    CONSTANT,
    KEYWORD,
    OTHER,
    PARAMETER,
    GLOBAL,
    PROPERTY,
    ERROR,
    DB,
    CALL,
    TAG,
    RULE,
    FILE,
    TEST,
    INTERFACE;
    

    private ElementKind() {
    }
}

