/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.csl.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;

public class EditList {
    private static final Logger LOG = Logger.getLogger(EditList.class.getName());
    private BaseDocument doc;
    private List<Edit> edits;
    private boolean formatAll;
    private List<DelegatedPosition> positions = new ArrayList<DelegatedPosition>();

    public EditList(BaseDocument doc) {
        this.doc = doc;
        this.edits = new ArrayList<Edit>();
    }

    public String toString() {
        return "EditList(" + this.edits + ")";
    }

    public Position createPosition(int offset) {
        return this.createPosition(offset, Position.Bias.Forward);
    }

    public Position createPosition(int offset, Position.Bias bias) {
        DelegatedPosition pos = new DelegatedPosition(offset, bias);
        this.positions.add(pos);
        return pos;
    }

    public EditList replace(int offset, int removeLen, String insertText, boolean format, int offsetOrdinal) {
        this.edits.add(new Edit(offset, removeLen, insertText, format, offsetOrdinal));
        return this;
    }

    public void applyToDocument(BaseDocument otherDoc) {
        EditList newList = new EditList(otherDoc);
        newList.formatAll = this.formatAll;
        newList.edits = this.edits;
        newList.apply();
    }

    public void setFormatAll(boolean formatAll) {
        this.formatAll = formatAll;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void apply() {
        int lastOffset;
        int firstOffset;
        if (this.edits.size() == 0) {
            return;
        }
        Collections.sort(this.edits);
        Collections.reverse(this.edits);
        final Position[] lastPos = new Position[]{null};
        Iterator<Edit> i$ = this.edits.iterator();
        while (i$.hasNext()) {
            Edit edit;
            final Edit fEdit = edit = i$.next();
            final int[] fEnd = new int[]{-1};
            this.doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        if (lastPos[0] == null) {
                            lastPos[0] = EditList.this.doc.createPosition(((Edit)EditList.access$200((EditList)EditList.this).get((int)0)).offset, Position.Bias.Forward);
                        }
                        if (fEdit.removeLen > 0) {
                            EditList.this.doc.remove(fEdit.offset, fEdit.removeLen);
                            LOG.fine("Remove text: <" + fEdit.offset + ", " + (fEdit.offset + fEdit.removeLen) + ">");
                        }
                        if (fEdit.getInsertText() != null) {
                            EditList.this.doc.insertString(fEdit.offset, fEdit.insertText, null);
                            LOG.fine("Insert text: offset=" + fEdit.offset + ", text='" + fEdit.insertText + "'\n");
                            fEnd[0] = fEdit.offset + fEdit.insertText.length();
                            for (int i = 0; i < EditList.this.positions.size(); ++i) {
                                DelegatedPosition pos = (DelegatedPosition)EditList.this.positions.get(i);
                                int positionOffset = pos.originalOffset;
                                if (fEdit.getOffset() > positionOffset || fEnd[0] < positionOffset) continue;
                                pos.delegate = EditList.this.doc.createPosition(positionOffset, pos.bias);
                            }
                        }
                    }
                    catch (BadLocationException ble) {
                        Exceptions.printStackTrace((Throwable)ble);
                    }
                }
            });
            if (!edit.format || edit.offset > fEnd[0]) continue;
            final Reformat r = Reformat.get((Document)this.doc);
            r.lock();
            try {
                this.doc.runAtomic(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            r.reformat(fEdit.offset, fEnd[0]);
                            LOG.fine("Formatting: <" + fEdit.offset + ", " + fEnd[0] + ">");
                        }
                        catch (BadLocationException ble) {
                            Exceptions.printStackTrace((Throwable)ble);
                        }
                    }
                });
                continue;
            }
            finally {
                r.unlock();
                continue;
            }
        }
        if (this.formatAll && (firstOffset = this.edits.get((int)(this.edits.size() - 1)).offset) <= (lastOffset = lastPos[0].getOffset())) {
            final Reformat r = Reformat.get((Document)this.doc);
            r.lock();
            try {
                this.doc.runAtomic(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            r.reformat(firstOffset, lastOffset);
                            LOG.fine("Formatting all: <" + firstOffset + ", " + lastOffset + ">");
                        }
                        catch (BadLocationException ble) {
                            Exceptions.printStackTrace((Throwable)ble);
                        }
                    }
                });
            }
            finally {
                r.unlock();
            }
        }
    }

    public OffsetRange getRange() {
        int minOffset;
        int maxOffset = minOffset = this.edits.get((int)0).offset;
        for (Edit edit : this.edits) {
            if (edit.offset < minOffset) {
                minOffset = edit.offset;
            }
            if (edit.offset <= maxOffset) continue;
            maxOffset = edit.offset;
        }
        return new OffsetRange(minOffset, maxOffset);
    }

    public int firstLine(BaseDocument doc) {
        OffsetRange range = this.getRange();
        return NbDocument.findLineNumber((StyledDocument)((StyledDocument)doc), (int)range.getStart());
    }

    static /* synthetic */ List access$200(EditList x0) {
        return x0.edits;
    }

    private class DelegatedPosition
    implements Position {
        private int originalOffset;
        private Position delegate;
        private Position.Bias bias;

        private DelegatedPosition(int offset, Position.Bias bias) {
            this.originalOffset = offset;
            this.bias = bias;
        }

        @Override
        public int getOffset() {
            if (this.delegate != null) {
                return this.delegate.getOffset();
            }
            return -1;
        }
    }

    private static class Edit
    implements Comparable<Edit> {
        int offset;
        int removeLen;
        String insertText;
        boolean format;
        int offsetOrdinal;

        private Edit(int offset, int removeLen, String insertText, boolean format) {
            this.offset = offset;
            this.removeLen = removeLen;
            this.insertText = insertText;
            this.format = format;
        }

        private Edit(int offset, int removeLen, String insertText, boolean format, int offsetOrdinal) {
            this(offset, removeLen, insertText, format);
            this.offsetOrdinal = offsetOrdinal;
        }

        @Override
        public int compareTo(Edit other) {
            if (this.offset == other.offset) {
                return other.offsetOrdinal - this.offsetOrdinal;
            }
            return this.offset - other.offset;
        }

        public int getOffset() {
            return this.offset;
        }

        public int getRemoveLen() {
            return this.removeLen;
        }

        public String getInsertText() {
            return this.insertText;
        }

        public String toString() {
            return "Edit(pos=" + this.offset + ",delete=" + this.removeLen + ",insert=" + this.insertText + ")";
        }
    }

}

