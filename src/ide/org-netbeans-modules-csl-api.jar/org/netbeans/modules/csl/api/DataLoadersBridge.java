/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.csl.api;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.csl.spi.DefaultDataLoadersBridge;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public abstract class DataLoadersBridge {
    private static DataLoadersBridge instance = null;

    public abstract Object createInstance(FileObject var1);

    public abstract <T> T getCookie(FileObject var1, Class<T> var2) throws IOException;

    public <T> T getCookie(JTextComponent comp, Class<T> aClass) throws IOException {
        return this.getCookie(this.getFileObject(comp), aClass);
    }

    public abstract Node getNodeDelegate(JTextComponent var1);

    public abstract <T> T getSafeCookie(FileObject var1, Class<T> var2);

    public abstract StyledDocument getDocument(FileObject var1);

    public abstract String getLine(Document var1, int var2);

    public abstract JEditorPane[] getOpenedPanes(FileObject var1);

    public abstract FileObject getFileObject(Document var1);

    public abstract FileObject getPrimaryFile(FileObject var1);

    public abstract EditorCookie isModified(FileObject var1);

    public FileObject getFileObject(JTextComponent text) {
        return this.getFileObject(text.getDocument());
    }

    public abstract PropertyChangeListener getDataObjectListener(FileObject var1, FileChangeListener var2) throws IOException;

    public static synchronized DataLoadersBridge getDefault() {
        if (instance == null && (DataLoadersBridge.instance = (DataLoadersBridge)Lookup.getDefault().lookup(DataLoadersBridge.class)) == null) {
            instance = new DefaultDataLoadersBridge();
        }
        return instance;
    }
}

