/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

public enum Modifier {
    PUBLIC,
    STATIC,
    PROTECTED,
    PRIVATE,
    DEPRECATED,
    ABSTRACT;
    

    private Modifier() {
    }
}

