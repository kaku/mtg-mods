/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.Set;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;

public interface CompletionProposal {
    public int getAnchorOffset();

    @CheckForNull
    public ElementHandle getElement();

    @NonNull
    public String getName();

    @NonNull
    public String getInsertPrefix();

    @CheckForNull
    public String getSortText();

    @NonNull
    public String getLhsHtml(@NonNull HtmlFormatter var1);

    @CheckForNull
    public String getRhsHtml(@NonNull HtmlFormatter var1);

    @NonNull
    public ElementKind getKind();

    @CheckForNull
    public ImageIcon getIcon();

    @NonNull
    public Set<Modifier> getModifiers();

    public boolean isSmart();

    public int getSortPrioOverride();

    @CheckForNull
    public String getCustomInsertTemplate();
}

