/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.api;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.Severity;
import org.openide.filesystems.FileObject;

public interface Error {
    @NonNull
    public String getDisplayName();

    @CheckForNull
    public String getDescription();

    @CheckForNull
    public String getKey();

    @CheckForNull
    public FileObject getFile();

    public int getStartPosition();

    public int getEndPosition();

    public boolean isLineError();

    @NonNull
    public Severity getSeverity();

    @CheckForNull
    public Object[] getParameters();

    public static interface Badging
    extends Error {
        public boolean showExplorerBadge();
    }

}

