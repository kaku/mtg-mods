/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.api.SelectPreviousCamelCasePosition;

public final class DeleteToPreviousCamelCasePosition
extends SelectPreviousCamelCasePosition {
    public static final String deletePreviousCamelCasePosition = "delete-previous-camel-case-position";

    public DeleteToPreviousCamelCasePosition(Action originalAction) {
        super("delete-previous-camel-case-position", originalAction);
    }

    @Override
    protected void moveToNewOffset(JTextComponent textComponent, int offset) throws BadLocationException {
        textComponent.getDocument().remove(offset, textComponent.getCaretPosition() - offset);
    }
}

