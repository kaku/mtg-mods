/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.api.AbstractCamelCasePosition;
import org.netbeans.modules.csl.api.CamelCaseOperations;

public class PreviousCamelCasePosition
extends AbstractCamelCasePosition {
    public static final String previousCamelCasePosition = "previous-camel-case-position";

    public PreviousCamelCasePosition(Action originalAction) {
        this("previous-camel-case-position", originalAction);
    }

    protected PreviousCamelCasePosition(String name, Action originalAction) {
        super(name, originalAction);
    }

    @Override
    protected int newOffset(JTextComponent textComponent) throws BadLocationException {
        return CamelCaseOperations.previousCamelCasePosition(textComponent);
    }

    @Override
    protected void moveToNewOffset(JTextComponent textComponent, int offset) throws BadLocationException {
        textComponent.setCaretPosition(offset);
    }
}

