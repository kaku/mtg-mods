/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.awt.Component;
import java.util.List;
import java.util.Map;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.spi.ParserResult;

public interface StructureScanner {
    @NonNull
    public List<? extends StructureItem> scan(@NonNull ParserResult var1);

    @NonNull
    public Map<String, List<OffsetRange>> folds(@NonNull ParserResult var1);

    @CheckForNull
    public Configuration getConfiguration();

    public static final class Configuration {
        private final boolean sortable;
        private final boolean filterable;
        private Component customFilter;
        private int expandDepth = -1;

        public Configuration(boolean sortable, boolean filterable) {
            this.sortable = sortable;
            this.filterable = filterable;
        }

        public Configuration(boolean sortable, boolean filterable, int expandDepth) {
            this(sortable, filterable);
            this.expandDepth = expandDepth;
        }

        public boolean isSortable() {
            return this.sortable;
        }

        public boolean isFilterable() {
            return this.filterable;
        }

        public Component getCustomFilter() {
            return this.customFilter;
        }

        public int getExpandDepth() {
            return this.expandDepth;
        }

        public void setExpandDepth(int depth) {
            this.expandDepth = depth;
        }

        public void setCustomFilter(Component customFilter) {
            this.customFilter = customFilter;
        }
    }

}

