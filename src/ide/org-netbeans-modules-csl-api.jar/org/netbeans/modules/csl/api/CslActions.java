/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import org.netbeans.modules.csl.api.DeleteToNextCamelCasePosition;
import org.netbeans.modules.csl.api.DeleteToPreviousCamelCasePosition;
import org.netbeans.modules.csl.api.GoToDeclarationAction;
import org.netbeans.modules.csl.api.GoToMarkOccurrencesAction;
import org.netbeans.modules.csl.api.InstantRenameAction;
import org.netbeans.modules.csl.api.NextCamelCasePosition;
import org.netbeans.modules.csl.api.PreviousCamelCasePosition;
import org.netbeans.modules.csl.api.SelectCodeElementAction;
import org.netbeans.modules.csl.api.SelectNextCamelCasePosition;
import org.netbeans.modules.csl.api.SelectPreviousCamelCasePosition;
import org.netbeans.modules.csl.api.ToggleBlockCommentAction;

public final class CslActions {
    private CslActions() {
    }

    public static Action createGoToDeclarationAction() {
        return new GoToDeclarationAction();
    }

    public static Action createGoToMarkOccurrencesAction(boolean nextOccurrence) {
        return new GoToMarkOccurrencesAction(nextOccurrence);
    }

    public static Action createInstantRenameAction() {
        return new InstantRenameAction();
    }

    public static Action createSelectCodeElementAction(boolean selectNext) {
        String name = selectNext ? "select-element-next" : "select-element-previous";
        return new SelectCodeElementAction(name, selectNext);
    }

    public static Action createToggleBlockCommentAction() {
        return new ToggleBlockCommentAction();
    }

    public static Action createCamelCasePositionAction(Action originalAction, boolean next) {
        return next ? new NextCamelCasePosition(originalAction) : new PreviousCamelCasePosition(originalAction);
    }

    public static Action createDeleteToCamelCasePositionAction(Action originalAction, boolean next) {
        return next ? new DeleteToNextCamelCasePosition(originalAction) : new DeleteToPreviousCamelCasePosition(originalAction);
    }

    public static Action createSelectCamelCasePositionAction(Action originalAction, boolean next) {
        return next ? new SelectNextCamelCasePosition(originalAction) : new SelectPreviousCamelCasePosition(originalAction);
    }
}

