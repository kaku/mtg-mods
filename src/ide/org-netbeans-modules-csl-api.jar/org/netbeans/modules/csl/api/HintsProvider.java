/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.spi.options.OptionsPanelController
 */
package org.netbeans.modules.csl.api;

import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.spi.options.OptionsPanelController;

public interface HintsProvider {
    public void computeHints(@NonNull HintsManager var1, @NonNull RuleContext var2, @NonNull List<Hint> var3);

    public void computeSuggestions(@NonNull HintsManager var1, @NonNull RuleContext var2, @NonNull List<Hint> var3, int var4);

    public void computeSelectionHints(@NonNull HintsManager var1, @NonNull RuleContext var2, @NonNull List<Hint> var3, int var4, int var5);

    public void computeErrors(@NonNull HintsManager var1, @NonNull RuleContext var2, @NonNull List<Hint> var3, @NonNull List<Error> var4);

    public void cancel();

    @CheckForNull
    public List<Rule> getBuiltinRules();

    @NonNull
    public RuleContext createRuleContext();

    public static abstract class HintsManager {
        public static final HintsManager getManagerForMimeType(String mimeType) {
            Language language = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
            if (language != null && language.getHintsProvider() != null) {
                return language.getHintsManager();
            }
            return null;
        }

        public abstract boolean isEnabled(Rule.UserConfigurableRule var1);

        @NonNull
        public abstract Map<?, List<? extends Rule.ErrorRule>> getErrors();

        @NonNull
        public abstract Map<?, List<? extends Rule.AstRule>> getHints();

        @NonNull
        public abstract List<? extends Rule.SelectionRule> getSelectionHints();

        @NonNull
        public abstract Map<?, List<? extends Rule.AstRule>> getHints(boolean var1, RuleContext var2);

        @NonNull
        public abstract Map<?, List<? extends Rule.AstRule>> getSuggestions();

        public abstract void refreshHints(@NonNull RuleContext var1);

        public abstract OptionsPanelController getOptionsController();

        public abstract Preferences getPreferences(Rule.UserConfigurableRule var1);
    }

}

