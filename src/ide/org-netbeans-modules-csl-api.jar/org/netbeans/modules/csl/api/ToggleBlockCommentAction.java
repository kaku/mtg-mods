/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.csl.api;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.spi.CommentHandler;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;

public class ToggleBlockCommentAction
extends BaseAction {
    private static final Logger LOG = Logger.getLogger(ToggleBlockCommentAction.class.getName());
    static final long serialVersionUID = -1;
    private final CommentHandler commentHandler;

    public ToggleBlockCommentAction(CommentHandler commentHandler) {
        super("toggle-comment");
        this.commentHandler = commentHandler;
    }

    public ToggleBlockCommentAction() {
        this(null);
    }

    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        if (target != null) {
            if (!(target.isEditable() && target.isEnabled() && target.getDocument() instanceof BaseDocument)) {
                target.getToolkit().beep();
                return;
            }
            BaseDocument doc = (BaseDocument)target.getDocument();
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        int offset = target.getCaretPosition();
                        if (Utilities.isSelectionShowing((JTextComponent)target)) {
                            offset = target.getSelectionStart();
                        } else {
                            offset = Utilities.getRowFirstNonWhite((BaseDocument)((BaseDocument)target.getDocument()), (int)offset);
                            if (offset == -1) {
                                offset = target.getCaretPosition();
                            }
                        }
                        TokenHierarchy th = TokenHierarchy.get((Document)target.getDocument());
                        if (ToggleBlockCommentAction.this.commentHandler == null) {
                            List seqs = th.embeddedTokenSequences(offset, false);
                            if (seqs.isEmpty()) {
                                return;
                            }
                            org.netbeans.modules.csl.core.Language lang = null;
                            LanguagePath langPath = null;
                            for (int i = seqs.size() - 1; i >= 0; --i) {
                                TokenSequence ts = (TokenSequence)seqs.get(i);
                                String mimeType = ts.language().mimeType();
                                lang = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
                                if (lang == null) continue;
                                langPath = ts.languagePath();
                                int n = offset = i < seqs.size() - 1 ? ts.offset() : offset;
                                if (!LOG.isLoggable(Level.FINE)) break;
                                LOG.log(Level.FINE, "offset={0}, lang={1}, mimeType={2}, ts={3}", new Object[]{offset, lang, mimeType, ts});
                                break;
                            }
                            if (lang == null) {
                                return;
                            }
                            CommentHandler commentHandler = null;
                            if (lang.getGsfLanguage() instanceof DefaultLanguageConfig) {
                                commentHandler = ((DefaultLanguageConfig)lang.getGsfLanguage()).getCommentHandler();
                            }
                            if (commentHandler != null) {
                                ToggleBlockCommentAction.this.commentUncommentBlock(target, th, commentHandler, true);
                            } else if (lang.getGsfLanguage().getLineCommentPrefix() != null) {
                                ToggleBlockCommentAction.this.commentUncommentLines(target, offset, langPath, th, lang.getGsfLanguage().getLineCommentPrefix());
                            }
                        } else {
                            ToggleBlockCommentAction.this.commentUncommentBlock(target, th, ToggleBlockCommentAction.this.commentHandler, false);
                        }
                    }
                    catch (BadLocationException e) {
                        target.getToolkit().beep();
                    }
                }
            });
        }
    }

    private int findCommentStart(BaseDocument doc, CommentHandler handler, int offsetFrom, int offsetTo) throws BadLocationException {
        int from = Utilities.getFirstNonWhiteFwd((BaseDocument)doc, (int)offsetFrom, (int)offsetTo);
        if (from == -1) {
            return offsetFrom;
        }
        String startDelim = handler.getCommentStartDelimiter();
        if (CharSequenceUtilities.equals((CharSequence)DocumentUtilities.getText((Document)doc).subSequence(from, Math.min(offsetTo, from + startDelim.length())), (Object)startDelim)) {
            return from;
        }
        return offsetFrom;
    }

    private int findCommentEnd(BaseDocument doc, CommentHandler handler, int offsetFrom, int offsetTo) throws BadLocationException {
        int to = Utilities.getFirstNonWhiteBwd((BaseDocument)doc, (int)offsetTo, (int)offsetFrom);
        if (to == -1) {
            return offsetTo;
        }
        String endDelim = handler.getCommentEndDelimiter();
        if (DocumentUtilities.getText((Document)doc).subSequence(Math.max(offsetFrom, to - endDelim.length() + 1), to + 1).equals(endDelim)) {
            return to + 1;
        }
        return offsetFrom;
    }

    private void commentUncommentBlock(JTextComponent target, TokenHierarchy<?> th, CommentHandler commentHandler, boolean dynamicCH) throws BadLocationException {
        int to;
        int from;
        Caret caret = target.getCaret();
        BaseDocument doc = (BaseDocument)target.getDocument();
        if (Utilities.isSelectionShowing((Caret)caret)) {
            from = Utilities.getRowStart((BaseDocument)doc, (int)target.getSelectionStart());
            to = target.getSelectionEnd();
            if (to > 0 && Utilities.getRowStart((BaseDocument)doc, (int)to) == to) {
                --to;
            }
            to = Utilities.getRowEnd((BaseDocument)doc, (int)to);
        } else {
            from = Utilities.getRowStart((BaseDocument)doc, (int)caret.getDot());
            to = Utilities.getRowEnd((BaseDocument)doc, (int)caret.getDot());
        }
        boolean lineSelection = false;
        boolean inComment = this.isInComment((Document)doc, commentHandler, caret.getDot());
        if (!Utilities.isSelectionShowing((Caret)caret)) {
            if (Utilities.isRowEmpty((BaseDocument)doc, (int)from) || Utilities.isRowWhite((BaseDocument)doc, (int)from)) {
                return;
            }
            if (!inComment) {
                from = Utilities.getFirstNonWhiteFwd((BaseDocument)doc, (int)Utilities.getRowStart((BaseDocument)doc, (int)from));
                to = Utilities.getFirstNonWhiteBwd((BaseDocument)doc, (int)Utilities.getRowEnd((BaseDocument)doc, (int)to)) + 1;
                lineSelection = true;
            } else {
                from = this.findCommentStart(doc, commentHandler, from, to);
                to = this.findCommentEnd(doc, commentHandler, from, to);
            }
        }
        if (!inComment && from == to) {
            return;
        }
        if (dynamicCH && !this.sameCommentHandler(th, from, false, commentHandler)) {
            return;
        }
        int[] adjustedRange = commentHandler.getAdjustedBlocks((Document)doc, from, to);
        if (adjustedRange.length == 0) {
            return;
        }
        from = adjustedRange[0];
        to = adjustedRange[1];
        if (!(!dynamicCH || this.sameCommentHandler(th, from, false, commentHandler) && this.sameCommentHandler(th, to, true, commentHandler))) {
            return;
        }
        if (!inComment && from == to) {
            return;
        }
        int[] comments = commentHandler.getCommentBlocks((Document)doc, from, to);
        assert (comments != null);
        this.check(comments, from, to);
        int _from = from;
        int _to = to;
        boolean _lineSelection = lineSelection;
        int[] commentRange = this.getCommentRange(comments, _from, commentHandler);
        if (commentRange == null) {
            if (!this.forceDirection(false)) {
                this.comment(target, doc, commentHandler, comments, _from, _to, _lineSelection);
            } else {
                target.getToolkit().beep();
            }
        } else if (comments.length > 0) {
            if (!this.forceDirection(true)) {
                this.uncomment(target, doc, commentHandler, comments, _from, _to, _lineSelection);
            } else {
                target.getToolkit().beep();
            }
        }
    }

    private boolean forceDirection(boolean comment) {
        Object force;
        Object object = force = comment ? this.getValue("force-comment") : this.getValue("force-uncomment");
        if (force instanceof Boolean) {
            return (Boolean)force;
        }
        return false;
    }

    private void comment(JTextComponent target, BaseDocument doc, CommentHandler commentHandler, int[] comments, int from, int to, boolean lineSelection) throws BadLocationException {
        int diff = 0;
        String startDelim = commentHandler.getCommentStartDelimiter();
        String endDelim = commentHandler.getCommentEndDelimiter();
        boolean startInComment = false;
        if (comments.length > 0) {
            int cStart = comments[0];
            int cEnd = comments[1];
            if (cStart <= from && cEnd > from) {
                startInComment = true;
            }
        }
        if (!startInComment) {
            diff += this.insert((Document)doc, from, startDelim);
        }
        for (int i = 0; i < comments.length; i += 2) {
            int commentStart = comments[i];
            int commentEnd = comments[i + 1];
            if (commentStart >= from) {
                diff += this.remove((Document)doc, commentStart + diff, startDelim.length());
            }
            if (commentEnd > to) continue;
            diff += this.remove((Document)doc, commentEnd + diff - endDelim.length(), endDelim.length());
        }
        if (comments.length == 0 || comments[comments.length - 1] <= to) {
            diff += this.insert((Document)doc, to + diff, endDelim);
        }
        if (!lineSelection) {
            target.setSelectionStart(from);
            target.setSelectionEnd(to + diff);
        }
    }

    private void uncomment(JTextComponent target, BaseDocument doc, CommentHandler commentHandler, int[] comments, int from, int to, boolean lineSelection) throws BadLocationException {
        int diff = 0;
        String startDelim = commentHandler.getCommentStartDelimiter();
        String endDelim = commentHandler.getCommentEndDelimiter();
        if (from == to) {
            assert (comments.length == 2);
            from = comments[0];
            to = comments[1];
            lineSelection = true;
        }
        if (comments[0] < from) {
            diff += this.insert((Document)doc, from, endDelim);
        }
        int selectionStart = from + diff;
        for (int i = 0; i < comments.length; i += 2) {
            int commentStart = comments[i];
            int commentEnd = comments[i + 1];
            if (commentStart >= from) {
                diff += this.remove((Document)doc, commentStart + diff, startDelim.length());
            }
            if (commentEnd > to) continue;
            diff += this.remove((Document)doc, commentEnd + diff - endDelim.length(), endDelim.length());
        }
        int selectionEnd = to + diff;
        if (comments[comments.length - 1] > to) {
            diff += this.insert((Document)doc, to + diff, commentHandler.getCommentStartDelimiter());
        }
        if (!lineSelection) {
            target.setSelectionStart(selectionStart);
            target.setSelectionEnd(selectionEnd);
        }
    }

    private int insert(Document doc, int offset, String text) throws BadLocationException {
        doc.insertString(offset, text, null);
        return text.length();
    }

    private int remove(Document doc, int offset, int length) throws BadLocationException {
        doc.remove(offset, length);
        return - length;
    }

    private int[] getCommentRange(int[] comments, int offset, CommentHandler handler) {
        String commentEnd = handler.getCommentEndDelimiter();
        for (int i = 0; i < comments.length; ++i) {
            int from = comments[i];
            int to = comments[++i];
            if (from > offset || to <= offset || commentEnd != null && offset > to - commentEnd.length()) continue;
            return new int[]{from, to};
        }
        return null;
    }

    private boolean isInComment(Document doc, CommentHandler commentHandler, int offset) {
        CharSequence text = DocumentUtilities.getText((Document)doc);
        int lastCommentStartIndex = CharSequenceUtilities.lastIndexOf((CharSequence)text, (CharSequence)commentHandler.getCommentStartDelimiter(), (int)offset);
        int lastCommentEndIndex = CharSequenceUtilities.lastIndexOf((CharSequence)text, (CharSequence)commentHandler.getCommentEndDelimiter(), (int)offset);
        return lastCommentStartIndex > -1 && (lastCommentStartIndex > lastCommentEndIndex || lastCommentEndIndex == -1 || lastCommentEndIndex == offset);
    }

    private boolean sameCommentHandler(TokenHierarchy<?> th, int offset, boolean backwardBias, CommentHandler cH) {
        Object cH2 = null;
        List seqs = th.embeddedTokenSequences(offset, backwardBias);
        if (!seqs.isEmpty()) {
            for (int i = seqs.size() - 1; i >= 0; --i) {
                TokenSequence ts = (TokenSequence)seqs.get(i);
                org.netbeans.modules.csl.core.Language lang = LanguageRegistry.getInstance().getLanguageByMimeType(ts.language().mimeType());
                if (lang == null) continue;
                if (!(lang.getGsfLanguage() instanceof DefaultLanguageConfig)) break;
                cH2 = ((DefaultLanguageConfig)lang.getGsfLanguage()).getCommentHandler();
                break;
            }
        }
        return cH2 != null && cH.getClass() == cH2.getClass();
    }

    private void debug(Document doc, List<int[]> blocks, Level level) {
        LOG.log(level, "TOGGLE_COMENT");
        for (int[] block : blocks) {
            try {
                int from = block[0];
                int to = block[1];
                if (from != -1 && to != -1) {
                    LOG.log(level, "[{0}, {1}]: ''{2}''", new Object[]{from, to, doc.getText(from, to - from)});
                    continue;
                }
                LOG.log(level, "[{0}, {1}]", new Object[]{from, to});
            }
            catch (BadLocationException ex) {
                LOG.log(level, null, ex);
            }
        }
        LOG.log(level, "----------------");
    }

    private void check(int[] comments, int from, int to) {
        if (comments.length % 2 != 0) {
            throw new IllegalArgumentException("Comments array size must be even, e.g. contain just pairs.");
        }
        for (int i = 0; i < comments.length; ++i) {
            int cfrom = comments[i];
            int cto = comments[++i];
            if ((cfrom >= from || cto >= from) && (cto <= to || cfrom <= to)) continue;
            throw new IllegalArgumentException("Comment [" + cfrom + " - " + cto + " is out of the range [" + from + " - " + to + "]!");
        }
    }

    private void commentUncommentLines(JTextComponent target, int offset, LanguagePath lp, TokenHierarchy<?> th, String lineCommentString) throws BadLocationException {
        int to;
        BaseDocument doc = (BaseDocument)target.getDocument();
        int from = offset;
        if (Utilities.isSelectionShowing((JTextComponent)target)) {
            to = target.getSelectionEnd();
            if (to > 0 && Utilities.getRowStart((BaseDocument)doc, (int)to) == to) {
                --to;
            }
        } else {
            to = Utilities.getRowEnd((BaseDocument)doc, (int)target.getCaretPosition());
        }
        int fromLineStartOffset = Utilities.getRowStart((BaseDocument)doc, (int)from);
        List seqs = th.tokenSequenceList(lp, fromLineStartOffset, to);
        LinkedList<int[]> blocks = new LinkedList<int[]>();
        for (int i = 0; i < seqs.size(); ++i) {
            int startPos = -1;
            int endPos = -1;
            TokenSequence ts = (TokenSequence)seqs.get(i);
            ts.move(fromLineStartOffset);
            while (ts.moveNext()) {
                int tokenEnd;
                TokenSequence embeddedSeq = ts.embedded();
                if (embeddedSeq != null && !ts.token().id().primaryCategory().contains("comment")) {
                    if (startPos != -1 && endPos != -1) {
                        blocks.add(new int[]{startPos, endPos});
                    }
                    endPos = -1;
                    startPos = -1;
                    continue;
                }
                if (startPos == -1) {
                    startPos = Math.max(ts.offset(), fromLineStartOffset);
                }
                if (endPos < (tokenEnd = ts.offset() + ts.token().length())) {
                    endPos = Math.min(tokenEnd, to);
                }
                if (tokenEnd <= to) continue;
                break;
            }
            if (startPos == -1 || endPos == -1) continue;
            blocks.add(new int[]{startPos, endPos});
        }
        if (blocks.isEmpty() && from == to) {
            this.comment(doc, from, 1, lineCommentString);
        }
        if (LOG.isLoggable(Level.FINE)) {
            this.debug((Document)doc, blocks, Level.FINE);
        }
        int lastLineOffset = -1;
        int lastLineEndOffset = -1;
        for (int[] block2 : blocks) {
            if (lastLineOffset != -1 && block2[0] <= lastLineEndOffset) {
                if (block2[1] <= lastLineEndOffset) {
                    block2[1] = -1;
                    block2[0] = -1;
                    continue;
                }
                block2[0] = lastLineEndOffset + 1;
            }
            lastLineOffset = Math.max(Utilities.getRowStart((BaseDocument)doc, (int)block2[1]), block2[0]);
            lastLineEndOffset = Utilities.getRowEnd((BaseDocument)doc, (int)block2[1]);
        }
        if (LOG.isLoggable(Level.FINE)) {
            this.debug((Document)doc, blocks, Level.FINE);
        }
        ListIterator<int[]> i2 = blocks.listIterator(blocks.size());
        while (i2.hasPrevious()) {
            boolean comment;
            int[] block2;
            block2 = i2.previous();
            int startPos = block2[0];
            int endPos = block2[1];
            if (startPos == -1 || endPos == -1) continue;
            int lineCount = Utilities.getRowCount((BaseDocument)doc, (int)startPos, (int)endPos);
            boolean bl = comment = !this.allComments(doc, startPos, lineCount, lineCommentString);
            if (comment) {
                if (!this.forceDirection(false)) {
                    this.comment(doc, startPos, lineCount, lineCommentString);
                    continue;
                }
                target.getToolkit().beep();
                continue;
            }
            if (!this.forceDirection(true)) {
                this.uncomment(doc, startPos, lineCount, lineCommentString);
                continue;
            }
            target.getToolkit().beep();
        }
    }

    private boolean allComments(BaseDocument doc, int startOffset, int lineCount, String lineCommentString) throws BadLocationException {
        int lineCommentStringLen = lineCommentString.length();
        int offset = startOffset;
        while (lineCount > 0) {
            int firstNonWhitePos = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            if (firstNonWhitePos == -1) {
                return false;
            }
            if (Utilities.getRowEnd((BaseDocument)doc, (int)firstNonWhitePos) - firstNonWhitePos < lineCommentStringLen) {
                return false;
            }
            CharSequence maybeLineComment = DocumentUtilities.getText((Document)doc, (int)firstNonWhitePos, (int)lineCommentStringLen);
            if (!CharSequenceUtilities.textEquals((CharSequence)maybeLineComment, (CharSequence)lineCommentString)) {
                return false;
            }
            offset = Utilities.getRowStart((BaseDocument)doc, (int)offset, (int)1);
            --lineCount;
        }
        return true;
    }

    private void comment(BaseDocument doc, int startOffset, int lineCount, String lineCommentString) throws BadLocationException {
        int offset = startOffset;
        while (lineCount > 0) {
            doc.insertString(offset, lineCommentString, null);
            offset = Utilities.getRowStart((BaseDocument)doc, (int)offset, (int)1);
            --lineCount;
        }
    }

    private void uncomment(BaseDocument doc, int startOffset, int lineCount, String lineCommentString) throws BadLocationException {
        int lineCommentStringLen = lineCommentString.length();
        int offset = startOffset;
        while (lineCount > 0) {
            CharSequence maybeLineComment;
            int firstNonWhitePos = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            if (firstNonWhitePos != -1 && Utilities.getRowEnd((BaseDocument)doc, (int)firstNonWhitePos) - firstNonWhitePos >= lineCommentStringLen && CharSequenceUtilities.textEquals((CharSequence)(maybeLineComment = DocumentUtilities.getText((Document)doc, (int)firstNonWhitePos, (int)lineCommentStringLen)), (CharSequence)lineCommentString)) {
                doc.remove(firstNonWhitePos, lineCommentStringLen);
            }
            offset = Utilities.getRowStart((BaseDocument)doc, (int)offset, (int)1);
            --lineCount;
        }
    }

}

