/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.api;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;

public interface DeclarationFinder {
    @NonNull
    public DeclarationLocation findDeclaration(@NonNull ParserResult var1, int var2);

    @NonNull
    public OffsetRange getReferenceSpan(@NonNull Document var1, int var2);

    public static interface AlternativeLocation
    extends Comparable<AlternativeLocation> {
        public ElementHandle getElement();

        public String getDisplayHtml(HtmlFormatter var1);

        public DeclarationLocation getLocation();
    }

    public static final class DeclarationLocation {
        public static final DeclarationLocation NONE = new DeclarationLocation(null, -1);
        private final FileObject fileObject;
        private final int offset;
        private final URL url;
        private List<AlternativeLocation> alternatives;
        private ElementHandle element;
        private String invalidMessage;

        public DeclarationLocation(@NonNull FileObject fileObject, int offset) {
            this.fileObject = fileObject;
            this.offset = offset;
            this.url = null;
        }

        public DeclarationLocation(@NonNull FileObject fileObject, int offset, @NonNull ElementHandle element) {
            this(fileObject, offset);
            this.element = element;
        }

        public DeclarationLocation(@NonNull URL url) {
            this.url = url;
            this.fileObject = null;
            this.offset = -1;
        }

        public void setInvalidMessage(String invalidMessage) {
            this.invalidMessage = invalidMessage;
        }

        public void addAlternative(@NonNull AlternativeLocation location) {
            if (this.alternatives == null) {
                this.alternatives = new ArrayList<AlternativeLocation>();
            }
            this.alternatives.add(location);
        }

        @NonNull
        public List<AlternativeLocation> getAlternativeLocations() {
            if (this.alternatives != null) {
                return this.alternatives;
            }
            return Collections.emptyList();
        }

        @CheckForNull
        public URL getUrl() {
            return this.url;
        }

        @CheckForNull
        public FileObject getFileObject() {
            return this.fileObject;
        }

        public int getOffset() {
            return this.offset;
        }

        @CheckForNull
        public ElementHandle getElement() {
            return this.element;
        }

        @CheckForNull
        public String getInvalidMessage() {
            return this.invalidMessage;
        }

        public String toString() {
            if (this == NONE) {
                return "NONE";
            }
            if (this.url != null) {
                return this.url.toExternalForm();
            }
            return this.fileObject.getNameExt() + ":" + this.offset;
        }
    }

}

