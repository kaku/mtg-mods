/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;

public interface KeystrokeHandler {
    public boolean beforeCharInserted(@NonNull Document var1, int var2, @NonNull JTextComponent var3, char var4) throws BadLocationException;

    public boolean afterCharInserted(@NonNull Document var1, int var2, @NonNull JTextComponent var3, char var4) throws BadLocationException;

    public boolean charBackspaced(@NonNull Document var1, int var2, @NonNull JTextComponent var3, char var4) throws BadLocationException;

    public int beforeBreak(@NonNull Document var1, int var2, @NonNull JTextComponent var3) throws BadLocationException;

    @NonNull
    public OffsetRange findMatching(@NonNull Document var1, int var2);

    @NonNull
    public List<OffsetRange> findLogicalRanges(@NonNull ParserResult var1, int var2);

    @CheckForNull
    public int getNextWordOffset(Document var1, int var2, boolean var3);
}

