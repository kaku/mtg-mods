/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.csl.api;

import java.util.List;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public class Hint {
    private final String description;
    private final List<HintFix> fixes;
    private final FileObject file;
    private final OffsetRange range;
    private final Rule rule;
    private int priority;

    public Hint(@NonNull Rule rule, @NonNull String description, @NonNull FileObject file, OffsetRange range, @NullAllowed List<HintFix> fixes, int priority) {
        Parameters.notNull((CharSequence)"rule", (Object)rule);
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"file", (Object)file);
        this.rule = rule;
        this.description = description;
        this.file = file;
        this.range = range;
        this.fixes = fixes;
        this.priority = priority;
    }

    @NonNull
    public Rule getRule() {
        return this.rule;
    }

    @NonNull
    public String getDescription() {
        return this.description;
    }

    @NonNull
    public FileObject getFile() {
        return this.file;
    }

    @CheckForNull
    public List<HintFix> getFixes() {
        return this.fixes;
    }

    public OffsetRange getRange() {
        return this.range;
    }

    public int getPriority() {
        return this.priority;
    }

    public String toString() {
        return "Description(desc=" + this.description + ",fixes=" + this.fixes + ")";
    }
}

