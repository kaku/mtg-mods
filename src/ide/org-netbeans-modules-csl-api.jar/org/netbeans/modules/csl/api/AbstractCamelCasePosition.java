/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.csl.api;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.MissingResourceException;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public abstract class AbstractCamelCasePosition
extends BaseAction {
    private Action originalAction;

    public AbstractCamelCasePosition(String name, Action originalAction) {
        String desc;
        Object nameObj;
        super(name, 2);
        if (originalAction != null && (nameObj = originalAction.getValue("Name")) instanceof String) {
            this.putValue("Name", nameObj);
            this.originalAction = originalAction;
        }
        if ((desc = this.getShortDescription()) != null) {
            this.putValue("ShortDescription", (Object)desc);
        }
    }

    public final void actionPerformed(ActionEvent evt, final JTextComponent target) {
        if (target != null) {
            if (this.originalAction != null && !this.isUsingCamelCase()) {
                if (this.originalAction instanceof BaseAction) {
                    ((BaseAction)this.originalAction).actionPerformed(evt, target);
                } else {
                    this.originalAction.actionPerformed(evt);
                }
            } else {
                final BaseDocument bdoc = Utilities.getDocument((JTextComponent)target);
                if (bdoc != null) {
                    bdoc.runAtomic(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            DocumentUtilities.setTypingModification((Document)bdoc, (boolean)true);
                            try {
                                int offset = AbstractCamelCasePosition.this.newOffset(target);
                                if (offset != -1) {
                                    AbstractCamelCasePosition.this.moveToNewOffset(target, offset);
                                }
                            }
                            catch (BadLocationException ble) {
                                target.getToolkit().beep();
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)bdoc, (boolean)false);
                            }
                        }
                    });
                } else {
                    target.getToolkit().beep();
                }
            }
        }
    }

    protected abstract int newOffset(JTextComponent var1) throws BadLocationException;

    protected abstract void moveToNewOffset(JTextComponent var1, int var2) throws BadLocationException;

    public String getShortDescription() {
        String shortDesc;
        String name = (String)this.getValue("Name");
        if (name == null) {
            return null;
        }
        try {
            shortDesc = NbBundle.getBundle(AbstractCamelCasePosition.class).getString(name);
        }
        catch (MissingResourceException mre) {
            shortDesc = name;
        }
        return shortDesc;
    }

    private boolean isUsingCamelCase() {
        Preferences p = NbPreferences.root();
        if (p == null) {
            return false;
        }
        return p.getBoolean("useCamelCaseStyleNavigation", true);
    }

}

