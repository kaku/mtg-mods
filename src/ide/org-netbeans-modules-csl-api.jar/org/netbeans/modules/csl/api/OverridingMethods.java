/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.Collection;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.spi.ParserResult;

public interface OverridingMethods {
    @CheckForNull
    public Collection<? extends DeclarationFinder.AlternativeLocation> overrides(@NonNull ParserResult var1, @NonNull ElementHandle var2);

    public boolean isOverriddenBySupported(@NonNull ParserResult var1, @NonNull ElementHandle var2);

    @CheckForNull
    public Collection<? extends DeclarationFinder.AlternativeLocation> overriddenBy(@NonNull ParserResult var1, @NonNull ElementHandle var2);
}

