/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;

public class ParameterInfo {
    public static final ParameterInfo NONE = new ParameterInfo(null, -1, -1);
    private List<String> names;
    private int index;
    private int anchorOffset;

    public ParameterInfo(@NonNull List<String> names, int index, int anchorOffset) {
        this.names = names;
        this.index = index;
        this.anchorOffset = anchorOffset;
    }

    @NonNull
    public List<String> getNames() {
        return this.names;
    }

    public int getCurrentIndex() {
        return this.index;
    }

    public int getAnchorOffset() {
        return this.anchorOffset;
    }
}

