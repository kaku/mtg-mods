/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.api.NextCamelCasePosition;

public class SelectNextCamelCasePosition
extends NextCamelCasePosition {
    public static final String selectNextCamelCasePosition = "select-next-camel-case-position";

    public SelectNextCamelCasePosition(Action originalAction) {
        this("select-next-camel-case-position", originalAction);
    }

    protected SelectNextCamelCasePosition(String name, Action originalAction) {
        super(name, originalAction);
    }

    @Override
    protected void moveToNewOffset(JTextComponent textComponent, int offset) throws BadLocationException {
        textComponent.getCaret().moveDot(offset);
    }
}

