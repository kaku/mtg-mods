/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.hints.Severity
 */
package org.netbeans.modules.csl.api;

import org.netbeans.spi.editor.hints.Severity;

public enum HintSeverity {
    INFO,
    ERROR,
    WARNING,
    CURRENT_LINE_WARNING;
    

    private HintSeverity() {
    }

    public Severity toEditorSeverity() {
        switch (this) {
            case INFO: {
                return Severity.HINT;
            }
            case ERROR: {
                return Severity.ERROR;
            }
            case WARNING: {
                return Severity.VERIFIER;
            }
            case CURRENT_LINE_WARNING: {
                return Severity.HINT;
            }
        }
        return null;
    }

}

