/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.lexer.Language
 */
package org.netbeans.modules.csl.api;

import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.lexer.Language;

public interface GsfLanguage {
    @CheckForNull
    public String getLineCommentPrefix();

    public boolean isIdentifierChar(char var1);

    @NonNull
    public Language getLexerLanguage();

    @NonNull
    public String getDisplayName();

    @CheckForNull
    public String getPreferredExtension();

    public Set<String> getSourcePathIds();

    public Set<String> getLibraryPathIds();

    public Set<String> getBinaryLibraryPathIds();
}

