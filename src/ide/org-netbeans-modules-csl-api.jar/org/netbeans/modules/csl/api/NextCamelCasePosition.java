/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.api.AbstractCamelCasePosition;
import org.netbeans.modules.csl.api.CamelCaseOperations;

public class NextCamelCasePosition
extends AbstractCamelCasePosition {
    public static final String nextCamelCasePosition = "next-camel-case-position";

    public NextCamelCasePosition(Action originalAction) {
        this("next-camel-case-position", originalAction);
    }

    protected NextCamelCasePosition(String name, Action originalAction) {
        super(name, originalAction);
    }

    @Override
    protected int newOffset(JTextComponent textComponent) throws BadLocationException {
        return CamelCaseOperations.nextCamelCasePosition(textComponent);
    }

    @Override
    protected void moveToNewOffset(JTextComponent textComponent, int offset) throws BadLocationException {
        textComponent.setCaretPosition(offset);
    }
}

