/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

public final class OffsetRange
implements Comparable<OffsetRange> {
    public static final OffsetRange NONE = new OffsetRange(0, 0);
    private final int start;
    private final int end;

    public OffsetRange(int start, int end) {
        assert (start >= 0);
        assert (end >= start);
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public int getLength() {
        return this.getEnd() - this.getStart();
    }

    public boolean overlaps(OffsetRange range) {
        if (range == NONE) {
            return false;
        }
        if (this == NONE) {
            return false;
        }
        return this.end > range.start && this.start < range.end;
    }

    public OffsetRange boundTo(int minimumStart, int maximumEnd) {
        assert (minimumStart <= maximumEnd);
        assert (this != NONE);
        int newStart = this.start;
        int newEnd = this.end;
        if (newEnd > maximumEnd) {
            newEnd = maximumEnd;
            if (newStart > maximumEnd) {
                newStart = maximumEnd;
            }
        }
        if (newStart < minimumStart) {
            newStart = minimumStart;
            if (newEnd < minimumStart) {
                newEnd = minimumStart;
            }
        }
        return new OffsetRange(newStart, newEnd);
    }

    public String toString() {
        if (this == NONE) {
            return "OffsetRange[NONE]";
        }
        return "OffsetRange[" + this.start + "," + this.end + ">";
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o.getClass() != OffsetRange.class) {
            return false;
        }
        OffsetRange test = (OffsetRange)o;
        if (this.start != test.start) {
            return false;
        }
        if (this.end != test.end) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.start;
    }

    public boolean containsInclusive(int offset) {
        if (this == NONE) {
            return false;
        }
        return offset >= this.getStart() && offset <= this.getEnd();
    }

    @Override
    public int compareTo(OffsetRange o) {
        if (this.start != o.start) {
            return this.start - o.start;
        }
        return this.end - o.end;
    }

    public boolean isEmpty() {
        return this.start == this.end;
    }
}

