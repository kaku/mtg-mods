/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import javax.swing.Action;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.api.SelectNextCamelCasePosition;

public final class DeleteToNextCamelCasePosition
extends SelectNextCamelCasePosition {
    public static final String deleteNextCamelCasePosition = "delete-next-camel-case-position";

    public DeleteToNextCamelCasePosition(Action originalAction) {
        super("delete-next-camel-case-position", originalAction);
    }

    @Override
    protected void moveToNewOffset(JTextComponent textComponent, int offset) throws BadLocationException {
        textComponent.getDocument().remove(textComponent.getCaretPosition(), offset - textComponent.getCaretPosition());
    }
}

