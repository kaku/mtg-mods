/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

public enum Severity {
    INFO,
    WARNING,
    ERROR,
    FATAL;
    

    private Severity() {
    }
}

