/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.csl.api;

import java.net.URL;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Parameters;

public final class Documentation {
    private final String content;
    private final URL url;

    private Documentation(String content, URL url) {
        assert (content != null);
        this.content = content;
        this.url = url;
    }

    @NonNull
    public static Documentation create(@NonNull String content) {
        Parameters.notNull((CharSequence)"content", (Object)content);
        return new Documentation(content, null);
    }

    @NonNull
    public static Documentation create(@NonNull String content, URL url) {
        Parameters.notNull((CharSequence)"content", (Object)content);
        return new Documentation(content, url);
    }

    @NonNull
    public String getContent() {
        return this.content;
    }

    @CheckForNull
    public URL getUrl() {
        return this.url;
    }
}

