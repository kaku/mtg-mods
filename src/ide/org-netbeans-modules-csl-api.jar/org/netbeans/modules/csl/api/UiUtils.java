/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.Line
 *  org.openide.text.Line$ShowOpenType
 *  org.openide.text.Line$ShowVisibilityType
 *  org.openide.text.NbDocument
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.api;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.navigation.Icons;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class UiUtils {
    private static final int AWT_TIMEOUT = 1000;
    private static final int NON_AWT_TIMEOUT = 2000;
    private static final Logger LOG = Logger.getLogger(UiUtils.class.getName());

    public static boolean open(Source source, ElementHandle handle) {
        assert (source != null);
        assert (handle != null);
        DeclarationFinder.DeclarationLocation location = UiUtils.getElementLocation(source, handle);
        if (location != DeclarationFinder.DeclarationLocation.NONE) {
            return UiUtils.doOpen(location.getFileObject(), location.getOffset());
        }
        return false;
    }

    public static boolean open(final FileObject fo, final int offset) {
        assert (fo != null);
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    UiUtils.doOpen(fo, offset);
                }
            });
            return true;
        }
        return UiUtils.doOpen(fo, offset);
    }

    public static ImageIcon getElementIcon(ElementKind elementKind, Collection<Modifier> modifiers) {
        return Icons.getElementIcon(elementKind, modifiers);
    }

    public static KeystrokeHandler getBracketCompletion(final Document doc, final int offset) {
        final AtomicReference ref = new AtomicReference();
        doc.render(new Runnable(){

            @Override
            public void run() {
                KeystrokeHandler defaultt;
                TokenHierarchy hi = TokenHierarchy.get((Document)doc);
                List forward = hi.embeddedTokenSequences(offset, false);
                List backward = hi.embeddedTokenSequences(offset, true);
                final KeystrokeHandler bwHandler = UiUtils.getFirstHandler(backward);
                final KeystrokeHandler fwHandler = UiUtils.getFirstHandler(forward);
                if (fwHandler == null && bwHandler == null) {
                    return;
                }
                KeystrokeHandler keystrokeHandler = defaultt = fwHandler == null ? bwHandler : fwHandler;
                if (fwHandler != null && bwHandler != null && fwHandler != bwHandler) {
                    ref.set(new KeystrokeHandler(){

                        @Override
                        public boolean beforeCharInserted(Document doc, int caretOffset, JTextComponent target, char ch) throws BadLocationException {
                            if (!fwHandler.beforeCharInserted(doc, caretOffset, target, ch)) {
                                return bwHandler.beforeCharInserted(doc, caretOffset, target, ch);
                            }
                            return true;
                        }

                        @Override
                        public boolean afterCharInserted(Document doc, int caretOffset, JTextComponent target, char ch) throws BadLocationException {
                            if (!fwHandler.afterCharInserted(doc, caretOffset, target, ch)) {
                                return bwHandler.afterCharInserted(doc, caretOffset, target, ch);
                            }
                            return true;
                        }

                        @Override
                        public boolean charBackspaced(Document doc, int caretOffset, JTextComponent target, char ch) throws BadLocationException {
                            if (!fwHandler.charBackspaced(doc, caretOffset, target, ch)) {
                                return bwHandler.charBackspaced(doc, caretOffset, target, ch);
                            }
                            return true;
                        }

                        @Override
                        public int beforeBreak(Document doc, int caretOffset, JTextComponent target) throws BadLocationException {
                            return defaultt.beforeBreak(doc, caretOffset, target);
                        }

                        @Override
                        public OffsetRange findMatching(Document doc, int caretOffset) {
                            return defaultt.findMatching(doc, caretOffset);
                        }

                        @Override
                        public List<OffsetRange> findLogicalRanges(ParserResult info, int caretOffset) {
                            return defaultt.findLogicalRanges(info, caretOffset);
                        }

                        @Override
                        public int getNextWordOffset(Document doc, int caretOffset, boolean reverse) {
                            return defaultt.getNextWordOffset(doc, caretOffset, reverse);
                        }
                    });
                } else {
                    ref.set(defaultt);
                }
            }

        });
        return (KeystrokeHandler)ref.get();
    }

    private static KeystrokeHandler getFirstHandler(List<TokenSequence<?>> embeddedTS) {
        for (int i = embeddedTS.size() - 1; i >= 0; --i) {
            KeystrokeHandler handler;
            TokenSequence ts = embeddedTS.get(i);
            org.netbeans.modules.csl.core.Language lang = LanguageRegistry.getInstance().getLanguageByMimeType(ts.language().mimeType());
            KeystrokeHandler keystrokeHandler = handler = lang != null ? lang.getBracketCompletion() : null;
            if (handler == null) continue;
            return handler;
        }
        return null;
    }

    private UiUtils() {
    }

    private static boolean doOpen(FileObject fo, int offset) {
        try {
            DataObject od = DataObject.find((FileObject)fo);
            return NbDocument.openDocument((Lookup.Provider)od, (int)offset, (Line.ShowOpenType)Line.ShowOpenType.OPEN, (Line.ShowVisibilityType)Line.ShowVisibilityType.FOCUS);
        }
        catch (DataObjectNotFoundException e) {
            LOG.log(Level.FINE, null, (Throwable)e);
            return false;
        }
    }

    private static DeclarationFinder.DeclarationLocation getElementLocation(final Source source, final ElementHandle handle) {
        if (source.getFileObject() == null) {
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        FileObject fileObject = handle.getFileObject();
        if (fileObject != null && fileObject != source.getFileObject()) {
            return new DeclarationFinder.DeclarationLocation(fileObject, -1);
        }
        final DeclarationFinder.DeclarationLocation[] result = new DeclarationFinder.DeclarationLocation[]{null};
        final AtomicBoolean cancel = new AtomicBoolean();
        final UserTask t = new UserTask(){

            public void run(ResultIterator resultIterator) throws ParseException {
                Parser.Result r;
                ParserResult info;
                OffsetRange range;
                if (cancel.get()) {
                    return;
                }
                if (resultIterator.getSnapshot().getMimeType().equals(handle.getMimeType()) && (r = resultIterator.getParserResult()) instanceof ParserResult && (range = handle.getOffsetRange(info = (ParserResult)r)) != OffsetRange.NONE && range != null) {
                    result[0] = new DeclarationFinder.DeclarationLocation(info.getSnapshot().getSource().getFileObject(), range.getStart());
                    return;
                }
                for (Embedding e : resultIterator.getEmbeddings()) {
                    this.run(resultIterator.getResultIterator(e));
                    if (result[0] == null) continue;
                    break;
                }
            }
        };
        if (IndexingManager.getDefault().isIndexing()) {
            Future f;
            int timeout = SwingUtilities.isEventDispatchThread() ? 1000 : 2000;
            try {
                f = ParserManager.parseWhenScanFinished(Collections.singleton(source), (UserTask)t);
            }
            catch (ParseException ex) {
                LOG.log(Level.WARNING, null, (Throwable)ex);
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            try {
                f.get(timeout, TimeUnit.MILLISECONDS);
            }
            catch (InterruptedException ex) {
                LOG.log(Level.INFO, null, ex);
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            catch (ExecutionException ex) {
                LOG.log(Level.INFO, null, ex);
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            catch (TimeoutException ex) {
                f.cancel(true);
                LOG.info("Skipping location of element offset within file, Scannig in progress");
                return DeclarationFinder.DeclarationLocation.NONE;
            }
            if (!f.isDone()) {
                f.cancel(true);
                LOG.info("Skipping location of element offset within file, Scannig in progress");
                return DeclarationFinder.DeclarationLocation.NONE;
            }
        } else if (SwingUtilities.isEventDispatchThread()) {
            ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ParserManager.parse(Collections.singleton(source), (UserTask)t);
                    }
                    catch (ParseException ex) {
                        LOG.log(Level.WARNING, null, (Throwable)ex);
                    }
                }
            }, (String)NbBundle.getMessage(UiUtils.class, (String)"TXT_CalculatingDeclPos"), (AtomicBoolean)cancel, (boolean)false);
        } else {
            try {
                ParserManager.parse(Collections.singleton(source), (UserTask)t);
            }
            catch (ParseException ex) {
                LOG.log(Level.WARNING, null, (Throwable)ex);
                return DeclarationFinder.DeclarationLocation.NONE;
            }
        }
        return result[0] != null ? result[0] : DeclarationFinder.DeclarationLocation.NONE;
    }

}

