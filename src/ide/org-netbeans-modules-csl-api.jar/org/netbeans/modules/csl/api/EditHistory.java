/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenChange
 *  org.netbeans.api.lexer.TokenHierarchyEvent
 *  org.netbeans.api.lexer.TokenHierarchyEventType
 *  org.netbeans.api.lexer.TokenHierarchyListener
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.csl.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.OffsetRange;

public final class EditHistory
implements DocumentListener,
TokenHierarchyListener {
    private static final Object ADDED = new Object();
    private static final Object REMOVED = new Object();
    private int start = -1;
    private int originalEnd = -1;
    private int editedEnd = -1;
    private List<Edit> edits = new ArrayList<Edit>(4);
    private Map<TokenId, Object> tokenIds = new IdentityHashMap<TokenId, Object>();
    private int delta = 0;
    private boolean valid = true;
    EditHistory previous;
    private int version = -1;
    private static final int MAX_KEEP = 15;

    public int getStart() {
        return this.start;
    }

    public boolean isInDamagedRegion(int pos) {
        if (this.start == -1) {
            return false;
        }
        return pos >= this.start && pos <= this.editedEnd;
    }

    public boolean isInDamagedRegion(OffsetRange range) {
        if (this.start == -1) {
            return false;
        }
        return range.getStart() < this.editedEnd && range.getEnd() > this.start;
    }

    public boolean isValid() {
        return this.valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getEditedSize() {
        return this.editedEnd - this.start;
    }

    public int getOriginalEnd() {
        return this.originalEnd;
    }

    public int getEditedEnd() {
        return this.editedEnd;
    }

    public int getSizeDelta() {
        return this.delta;
    }

    public int getOriginalSize() {
        return this.originalEnd - this.start;
    }

    public int getVersion() {
        return this.version;
    }

    public boolean wasModified(@NullAllowed TokenId id) {
        if (id == null) {
            return false;
        }
        return this.tokenIds.containsKey((Object)id);
    }

    public int convertOriginalToEdited(int oldPos) {
        if (this.start == -1 || oldPos <= this.start) {
            return oldPos;
        }
        if (oldPos >= this.originalEnd) {
            return oldPos + this.delta;
        }
        List<Edit> list = this.edits;
        int len = list.size();
        if (len == 0) {
            return oldPos;
        }
        for (int i = 0; i < len; ++i) {
            Edit edit = list.get(i);
            if (oldPos <= edit.offset) continue;
            if (edit.insert) {
                oldPos += edit.len;
                continue;
            }
            if (oldPos < edit.offset + edit.len) {
                oldPos = edit.offset;
                continue;
            }
            oldPos -= edit.len;
        }
        if (oldPos < 0) {
            oldPos = 0;
        }
        return oldPos;
    }

    public int convertEditedToOriginal(int newPos) {
        List<Edit> list = this.edits;
        int len = list.size();
        if (len == 0) {
            return newPos;
        }
        for (int i = len - 1; i >= 0; --i) {
            Edit edit = list.get(i);
            if (edit.insert) {
                if (newPos <= edit.offset) continue;
                if (newPos < edit.offset + edit.len) {
                    newPos = edit.offset;
                    continue;
                }
                newPos -= edit.len;
                continue;
            }
            if (newPos < edit.offset) continue;
            newPos += edit.len;
        }
        if (newPos < 0) {
            newPos = 0;
        }
        return newPos;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        int pos = e.getOffset();
        int length = e.getLength();
        this.insertUpdate(pos, length);
    }

    private void insertUpdate(int pos, int length) {
        this.edits.add(new Edit(pos, length, true));
        if (this.start == -1) {
            this.start = pos;
            this.originalEnd = pos;
            this.editedEnd = pos + length;
            this.delta = length;
        } else {
            int original = this.convertEditedToOriginal(pos);
            if (original > this.originalEnd) {
                this.originalEnd = original;
            }
            if (pos < this.start) {
                this.start = pos;
            }
            this.editedEnd = pos > this.editedEnd ? pos + length : (this.editedEnd += length);
            this.delta = this.getEditedSize() - this.getOriginalSize();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        int pos = e.getOffset();
        int length = e.getLength();
        this.removeUpdate(pos, length);
    }

    private void removeUpdate(int pos, int length) {
        this.edits.add(new Edit(pos, length, false));
        if (this.start == -1) {
            this.start = pos;
            this.originalEnd = pos + length;
            this.editedEnd = pos;
            this.delta = - length;
        } else {
            int original = this.convertEditedToOriginal(pos);
            if (original > this.originalEnd) {
                this.originalEnd = original;
            } else if (pos + length > this.editedEnd) {
                this.originalEnd += pos + length - this.editedEnd;
            }
            if (pos > this.editedEnd) {
                this.editedEnd = pos;
            } else {
                this.editedEnd -= length;
                if (this.editedEnd < pos) {
                    this.editedEnd = pos;
                }
            }
            if (pos < this.start) {
                this.start = pos;
            }
            this.delta = this.getEditedSize() - this.getOriginalSize();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    public void tokenHierarchyChanged(TokenHierarchyEvent evt) {
        TokenHierarchyEventType type = evt.type();
        if (type == TokenHierarchyEventType.MODIFICATION) {
            this.changed(evt.tokenChange());
        } else if (type == TokenHierarchyEventType.REBUILD) {
            this.valid = false;
        }
    }

    public void changed(TokenChange change) {
        TokenSequence removed;
        TokenSequence current;
        int embeddedCount = change.embeddedChangeCount();
        for (int i = 0; i < embeddedCount; ++i) {
            this.changed(change.embeddedChange(i));
        }
        if (change.removedTokenCount() > 0 && (removed = change.removedTokenSequence()) != null) {
            removed.moveStart();
            while (removed.moveNext()) {
                Token token = removed.token();
                if (token == null) continue;
                TokenId id = token.id();
                this.tokenIds.put(id, REMOVED);
            }
        }
        if (change.addedTokenCount() > 0 && (current = change.currentTokenSequence()) != null) {
            current.moveIndex(change.index());
            int n = change.addedTokenCount();
            for (int i2 = 0; current.moveNext() && i2 < n; ++i2) {
                Token token = current.token();
                if (token == null) continue;
                TokenId id = token.id();
                this.tokenIds.put(id, ADDED);
            }
        }
    }

    public String toString() {
        return "EditHistory(version=" + this.version + ", offset=" + this.start + ", originalSize=" + this.getOriginalSize() + ", editedSize=" + this.getEditedSize() + ", delta=" + this.delta + ")";
    }

    public void add(@NonNull EditHistory history) {
        history.previous = this;
        history.version = this.version + 1;
        if (history.version % 15 == 0) {
            EditHistory curr = history;
            for (int i = 0; i < 15; ++i) {
                curr = curr.previous;
                if (curr != null) continue;
                return;
            }
            curr.previous = null;
        }
    }

    @CheckForNull
    public static EditHistory getCombinedEdits(int lastVersion, @NonNull EditHistory mostRecent) {
        if (!mostRecent.isValid()) {
            return mostRecent;
        }
        if (mostRecent.previous == null || mostRecent.version == lastVersion) {
            return null;
        }
        if (mostRecent.previous.version == lastVersion) {
            return mostRecent;
        }
        EditHistory current = mostRecent;
        ArrayList<EditHistory> histories = new ArrayList<EditHistory>();
        while (current.version != lastVersion) {
            histories.add(current);
            if (current.version == lastVersion) break;
            current = current.previous;
            if (current != null) continue;
            if (lastVersion == -1) break;
            return null;
        }
        EditHistory result = new EditHistory();
        Collections.reverse(histories);
        for (EditHistory history : histories) {
            for (Edit edit : history.edits) {
                if (edit.insert) {
                    result.insertUpdate(edit.offset, edit.len);
                    continue;
                }
                result.removeUpdate(edit.offset, edit.len);
            }
            result.tokenIds.putAll(history.tokenIds);
        }
        return result;
    }

    public void testHelperNotifyToken(boolean add, TokenId id) {
        if (add) {
            this.tokenIds.put(id, ADDED);
        } else {
            this.tokenIds.put(id, REMOVED);
        }
    }

    private class Edit {
        private final int offset;
        private final int len;
        private final boolean insert;

        private Edit(int offset, int len, boolean insert) {
            this.offset = offset;
            this.len = len;
            this.insert = insert;
        }
    }

}

