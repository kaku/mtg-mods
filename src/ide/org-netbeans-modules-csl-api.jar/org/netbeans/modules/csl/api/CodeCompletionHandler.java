/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.csl.api;

import java.util.Map;
import java.util.Set;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.CodeCompletionContext;
import org.netbeans.modules.csl.api.CodeCompletionResult;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ParameterInfo;
import org.netbeans.modules.csl.spi.ParserResult;

public interface CodeCompletionHandler {
    @NonNull
    public CodeCompletionResult complete(@NonNull CodeCompletionContext var1);

    @CheckForNull
    public String document(@NonNull ParserResult var1, @NonNull ElementHandle var2);

    @CheckForNull
    public ElementHandle resolveLink(@NonNull String var1, ElementHandle var2);

    @CheckForNull
    public String getPrefix(@NonNull ParserResult var1, int var2, boolean var3);

    @NonNull
    public QueryType getAutoQuery(@NonNull JTextComponent var1, @NonNull String var2);

    @CheckForNull
    public String resolveTemplateVariable(String var1, @NonNull ParserResult var2, int var3, @NonNull String var4, @NullAllowed Map var5);

    @CheckForNull
    public Set<String> getApplicableTemplates(@NonNull Document var1, int var2, int var3);

    @NonNull
    public ParameterInfo parameters(@NonNull ParserResult var1, int var2, @NullAllowed CompletionProposal var3);

    public static enum QueryType {
        COMPLETION,
        DOCUMENTATION,
        TOOLTIP,
        ALL_COMPLETION,
        NONE,
        STOP;
        

        private QueryType() {
        }
    }

}

