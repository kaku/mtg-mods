/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.ext.ExtKit
 *  org.netbeans.editor.ext.ExtKit$GotoDeclarationAction
 */
package org.netbeans.modules.csl.api;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.ext.ExtKit;
import org.netbeans.modules.csl.editor.hyperlink.GoToSupport;

public final class GoToDeclarationAction
extends ExtKit.GotoDeclarationAction {
    public boolean gotoDeclaration(JTextComponent target) {
        GoToSupport.performGoTo((Document)((BaseDocument)target.getDocument()), target.getCaretPosition());
        return true;
    }
}

