/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.csl.api;

import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;

public interface InstantRenamer {
    public boolean isRenameAllowed(@NonNull ParserResult var1, int var2, @NullAllowed String[] var3);

    @CheckForNull
    public Set<OffsetRange> getRenameRegions(@NonNull ParserResult var1, int var2);
}

