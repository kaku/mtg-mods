/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ElementKind;

public abstract class HtmlFormatter {
    protected int textLength;
    protected int maxLength = Integer.MAX_VALUE;

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public abstract void reset();

    public abstract void appendHtml(String var1);

    public void appendText(@NonNull String text) {
        this.appendText(text, 0, text.length());
    }

    public abstract void appendText(@NonNull String var1, int var2, int var3);

    public abstract void emphasis(boolean var1);

    public abstract void name(@NonNull ElementKind var1, boolean var2);

    public abstract void parameters(boolean var1);

    public abstract void active(boolean var1);

    public abstract void type(boolean var1);

    public abstract void deprecated(boolean var1);

    @NonNull
    public abstract String getText();
}

