/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 */
package org.netbeans.modules.csl.api;

import java.util.prefs.Preferences;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;

public final class EditorOptions {
    private final String mimeType;
    private final Preferences preferences;

    @CheckForNull
    public static EditorOptions get(String mimeType) {
        return new EditorOptions(mimeType);
    }

    private EditorOptions(String mimeType) {
        this.mimeType = mimeType;
        this.preferences = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
    }

    public int getTabSize() {
        return this.preferences.getInt("tab-size", 8);
    }

    public boolean getExpandTabs() {
        return this.preferences.getBoolean("expand-tabs", true);
    }

    public int getSpacesPerTab() {
        return this.preferences.getInt("spaces-per-tab", 2);
    }

    public boolean getMatchBrackets() {
        return this.preferences.getBoolean("pair-characters-completion", true);
    }

    public int getRightMargin() {
        return this.preferences.getInt("text-limit-width", 80);
    }
}

