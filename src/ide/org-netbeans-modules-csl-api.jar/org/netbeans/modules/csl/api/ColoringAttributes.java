/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;

public enum ColoringAttributes {
    ABSTRACT,
    ANNOTATION_TYPE,
    CLASS,
    CUSTOM1,
    CUSTOM2,
    CUSTOM3,
    CONSTRUCTOR,
    DECLARATION,
    DEPRECATED,
    ENUM,
    FIELD,
    GLOBAL,
    INTERFACE,
    LOCAL_VARIABLE,
    MARK_OCCURRENCES,
    METHOD,
    PACKAGE_PRIVATE,
    PARAMETER,
    PRIVATE,
    PROTECTED,
    PUBLIC,
    REGEXP,
    STATIC,
    TYPE_PARAMETER_DECLARATION,
    TYPE_PARAMETER_USE,
    UNDEFINED,
    UNUSED;
    
    public static final EnumSet<ColoringAttributes> UNUSED_SET;
    public static final EnumSet<ColoringAttributes> FIELD_SET;
    public static final EnumSet<ColoringAttributes> STATIC_FIELD_SET;
    public static final EnumSet<ColoringAttributes> PARAMETER_SET;
    public static final EnumSet<ColoringAttributes> CUSTOM1_SET;
    public static final EnumSet<ColoringAttributes> CUSTOM2_SET;
    public static final EnumSet<ColoringAttributes> CUSTOM3_SET;
    public static final EnumSet<ColoringAttributes> CONSTRUCTOR_SET;
    public static final EnumSet<ColoringAttributes> METHOD_SET;
    public static final EnumSet<ColoringAttributes> CLASS_SET;
    public static final EnumSet<ColoringAttributes> GLOBAL_SET;
    public static final EnumSet<ColoringAttributes> REGEXP_SET;
    public static final EnumSet<ColoringAttributes> STATIC_SET;

    private ColoringAttributes() {
    }

    public static Coloring empty() {
        return new Coloring();
    }

    public static Coloring add(Coloring c, ColoringAttributes a) {
        Coloring ci = new Coloring();
        ci.value = c.value | 1 << a.ordinal();
        return ci;
    }

    static {
        UNUSED_SET = EnumSet.of(UNUSED);
        FIELD_SET = EnumSet.of(FIELD);
        STATIC_FIELD_SET = EnumSet.of(FIELD, STATIC);
        PARAMETER_SET = EnumSet.of(PARAMETER);
        CUSTOM1_SET = EnumSet.of(CUSTOM1);
        CUSTOM2_SET = EnumSet.of(CUSTOM2);
        CUSTOM3_SET = EnumSet.of(CUSTOM3);
        CONSTRUCTOR_SET = EnumSet.of(CONSTRUCTOR);
        METHOD_SET = EnumSet.of(METHOD);
        CLASS_SET = EnumSet.of(CLASS);
        GLOBAL_SET = EnumSet.of(GLOBAL);
        REGEXP_SET = EnumSet.of(REGEXP);
        STATIC_SET = EnumSet.of(STATIC);
    }

    public static final class Coloring
    implements Collection<ColoringAttributes> {
        private int value;
        static final /* synthetic */ boolean $assertionsDisabled;

        private Coloring() {
        }

        @Override
        public int size() {
            return Integer.bitCount(this.value);
        }

        @Override
        public boolean isEmpty() {
            return this.value == 0;
        }

        @Override
        public boolean contains(Object o) {
            if (o instanceof ColoringAttributes) {
                return (this.value & 1 << ((ColoringAttributes)((Object)o)).ordinal()) != 0;
            }
            return false;
        }

        @Override
        public Iterator<ColoringAttributes> iterator() {
            EnumSet<ColoringAttributes> s = EnumSet.noneOf(ColoringAttributes.class);
            for (ColoringAttributes c : ColoringAttributes.values()) {
                if (!this.contains((Object)c)) continue;
                s.add(c);
            }
            return s.iterator();
        }

        @Override
        public Object[] toArray() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public <T> T[] toArray(T[] a) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean add(ColoringAttributes o) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            for (Object o : c) {
                if (this.contains(o)) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean addAll(Collection<? extends ColoringAttributes> c) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int hashCode() {
            return this.value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Coloring) {
                return ((Coloring)obj).value == this.value;
            }
            return false;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (ColoringAttributes a : this) {
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(a.name());
            }
            return sb.toString();
        }

        static {
            boolean bl = Coloring.$assertionsDisabled = !ColoringAttributes.class.desiredAssertionStatus();
            if (!$assertionsDisabled && ColoringAttributes.values().length >= 32) {
                throw new AssertionError();
            }
        }
    }

}

