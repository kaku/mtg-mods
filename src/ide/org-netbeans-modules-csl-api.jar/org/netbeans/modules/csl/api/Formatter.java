/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.editor.indent.spi.Context
 */
package org.netbeans.modules.csl.api;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.indent.spi.Context;

public interface Formatter {
    public void reformat(@NonNull Context var1, @NullAllowed ParserResult var2);

    public void reindent(@NonNull Context var1);

    public boolean needsParserResult();

    public int indentSize();

    public int hangingIndentSize();
}

