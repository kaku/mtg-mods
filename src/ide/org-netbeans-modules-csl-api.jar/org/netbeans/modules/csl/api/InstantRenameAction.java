/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory
 *  org.openide.ErrorManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.csl.api;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.Action;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.InstantRenamePerformer;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory;
import org.openide.ErrorManager;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class InstantRenameAction
extends BaseAction {
    public InstantRenameAction() {
        super("in-place-refactoring", 10);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        try {
            final int caret = target.getCaretPosition();
            final String ident = Utilities.getIdentifier((BaseDocument)Utilities.getDocument((JTextComponent)target), (int)caret);
            if (ident == null) {
                Utilities.setStatusBoldText((JTextComponent)target, (String)NbBundle.getMessage(InstantRenameAction.class, (String)"InstantRenameDenied"));
                return;
            }
            if (IndexingManager.getDefault().isIndexing()) {
                Utilities.setStatusBoldText((JTextComponent)target, (String)NbBundle.getMessage(InstantRenameAction.class, (String)"scanning-in-progress"));
                return;
            }
            Source js = Source.create((Document)target.getDocument());
            if (js == null) {
                return;
            }
            final Set[] changePoints = new Set[1];
            final AtomicInteger changed = new AtomicInteger(0);
            DocumentListener dl = new DocumentListener(){

                @Override
                public void insertUpdate(DocumentEvent e) {
                    changed.compareAndSet(0, 1);
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    changed.compareAndSet(0, 1);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            };
            target.getDocument().addDocumentListener(dl);
            final InstantRenamer[] renamer = new InstantRenamer[1];
            try {
                do {
                    changed.set(0);
                    ParserManager.parse(Collections.singleton(js), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws Exception {
                            HashMap<String, Parser.Result> embeddedResults = new HashMap<String, Parser.Result>();
                            block0 : do {
                                Parser.Result parserResult;
                                if ((parserResult = resultIterator.getParserResult()) == null) {
                                    return;
                                }
                                embeddedResults.put(parserResult.getSnapshot().getMimeType(), resultIterator.getParserResult());
                                Iterable embeddings = resultIterator.getEmbeddings();
                                for (Embedding e : embeddings) {
                                    if (!e.containsOriginalOffset(caret)) continue;
                                    resultIterator = resultIterator.getResultIterator(e);
                                    continue block0;
                                    continue block0;
                                }
                                break;
                            } while (true);
                            BaseDocument baseDoc = (BaseDocument)target.getDocument();
                            List<Language> list = LanguageRegistry.getInstance().getEmbeddedLanguages(baseDoc, caret);
                            for (Language language : list) {
                                if (language.getInstantRenamer() == null) continue;
                                Parser.Result result = (Parser.Result)embeddedResults.get(language.getMimeType());
                                if (!(result instanceof ParserResult)) {
                                    return;
                                }
                                ParserResult parserResult = (ParserResult)result;
                                renamer[0] = language.getInstantRenamer();
                                assert (renamer[0] != null);
                                String[] descRetValue = new String[1];
                                if (!renamer[0].isRenameAllowed(parserResult, caret, descRetValue)) {
                                    return;
                                }
                                Set<OffsetRange> regions = renamer[0].getRenameRegions(parserResult, caret);
                                if (regions == null || regions.size() <= 0) break;
                                changePoints[0] = regions;
                                break;
                            }
                        }
                    });
                    if (changePoints[0] != null) {
                        final BadLocationException[] exc = new BadLocationException[1];
                        final BaseDocument baseDoc = (BaseDocument)target.getDocument();
                        baseDoc.render(new Runnable(){

                            @Override
                            public void run() {
                                try {
                                    if (changed.get() == 0) {
                                        int maxLen = baseDoc.getLength();
                                        for (OffsetRange r : changePoints[0]) {
                                            if (r.getStart() < maxLen && r.getEnd() < maxLen) continue;
                                            throw new IllegalArgumentException("Bad OffsetRange provided by " + renamer[0] + ": " + r + ", docLen=" + maxLen);
                                        }
                                        InstantRenameAction.this.doInstantRename(changePoints[0], target, caret, ident);
                                        changed.set(2);
                                    }
                                }
                                catch (BadLocationException ex) {
                                    exc[0] = ex;
                                }
                            }
                        });
                        if (exc[0] == null) continue;
                        throw exc[0];
                    }
                    this.doFullRename(DataLoadersBridge.getDefault().getCookie(target, EditorCookie.class), DataLoadersBridge.getDefault().getNodeDelegate(target));
                    break;
                } while (changed.get() == 1);
            }
            finally {
                target.getDocument().removeDocumentListener(dl);
            }
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        catch (IOException ioe) {
            ErrorManager.getDefault().notify((Throwable)ioe);
        }
        catch (ParseException ioe) {
            ErrorManager.getDefault().notify((Throwable)ioe);
        }
    }

    protected Class getShortDescriptionBundleClass() {
        return InstantRenameAction.class;
    }

    private void doInstantRename(Set<OffsetRange> changePoints, JTextComponent target, int caret, String ident) throws BadLocationException {
        InstantRenamePerformer.performInstantRename(target, changePoints, caret);
    }

    private void doFullRename(EditorCookie ec, Node n) {
        InstanceContent ic = new InstanceContent();
        ic.add((Object)ec);
        ic.add((Object)n);
        AbstractLookup actionContext = new AbstractLookup((AbstractLookup.Content)ic);
        Action a = RefactoringActionsFactory.renameAction().createContextAwareInstance((Lookup)actionContext);
        a.actionPerformed(RefactoringActionsFactory.DEFAULT_EVENT);
    }

}

