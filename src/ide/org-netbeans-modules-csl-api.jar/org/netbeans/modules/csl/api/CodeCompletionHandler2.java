/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.concurrent.Callable;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.spi.ParserResult;

public interface CodeCompletionHandler2
extends CodeCompletionHandler {
    @CheckForNull
    public Documentation documentElement(@NonNull ParserResult var1, @NonNull ElementHandle var2, @NonNull Callable<Boolean> var3);
}

