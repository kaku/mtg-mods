/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.api;

import java.util.Set;
import javax.swing.text.BadLocationException;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.editor.EditRegionsImpl;
import org.openide.filesystems.FileObject;

public abstract class EditRegions {
    private static EditRegions editRegions;

    public abstract void edit(@NonNull FileObject var1, @NonNull Set<OffsetRange> var2, int var3) throws BadLocationException;

    @NonNull
    public static synchronized EditRegions getInstance() {
        if (editRegions == null) {
            editRegions = new EditRegionsImpl();
        }
        return editRegions;
    }
}

