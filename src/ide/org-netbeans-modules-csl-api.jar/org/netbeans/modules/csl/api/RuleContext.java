/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.editor.BaseDocument
 */
package org.netbeans.modules.csl.api;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.spi.ParserResult;

public class RuleContext {
    @NonNull
    public HintsProvider.HintsManager manager;
    public int caretOffset = -1;
    public int selectionStart = -1;
    public int selectionEnd;
    @NonNull
    public ParserResult parserResult;
    @NonNull
    public BaseDocument doc;
    public int lexOffset;
    public int astOffset;
}

