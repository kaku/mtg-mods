/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.csl.api;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.UiUtils;
import org.openide.ErrorManager;

class CamelCaseOperations {
    CamelCaseOperations() {
    }

    static int nextCamelCasePosition(JTextComponent textComponent) {
        Document doc;
        int nextOffset;
        int offset = textComponent.getCaretPosition();
        if (offset == (doc = textComponent.getDocument()).getLength()) {
            return -1;
        }
        KeystrokeHandler bc = UiUtils.getBracketCompletion(doc, offset);
        if (bc != null && (nextOffset = bc.getNextWordOffset(doc, offset, false)) != -1) {
            return nextOffset;
        }
        try {
            return Utilities.getNextWord((JTextComponent)textComponent, (int)offset);
        }
        catch (BadLocationException ble) {
            ErrorManager.getDefault().notify((Throwable)ble);
            return -1;
        }
    }

    static int previousCamelCasePosition(JTextComponent textComponent) {
        int nextOffset;
        int offset = textComponent.getCaretPosition();
        if (offset == 0) {
            return -1;
        }
        Document doc = textComponent.getDocument();
        KeystrokeHandler bc = UiUtils.getBracketCompletion(doc, offset);
        if (bc != null && (nextOffset = bc.getNextWordOffset(doc, offset, true)) != -1) {
            return nextOffset;
        }
        try {
            return Utilities.getPreviousWord((JTextComponent)textComponent, (int)offset);
        }
        catch (BadLocationException ble) {
            ErrorManager.getDefault().notify((Throwable)ble);
            return -1;
        }
    }

    static void replaceChar(JTextComponent textComponent, int offset, char c) {
        if (!textComponent.isEditable()) {
            return;
        }
        CamelCaseOperations.replaceText(textComponent, offset, 1, String.valueOf(c));
    }

    static void replaceText(JTextComponent textComponent, final int offset, final int length, final String text) {
        if (!textComponent.isEditable()) {
            return;
        }
        final Document document = textComponent.getDocument();
        Runnable r = new Runnable(){

            @Override
            public void run() {
                try {
                    if (length > 0) {
                        document.remove(offset, length);
                    }
                    document.insertString(offset, text, null);
                }
                catch (BadLocationException ble) {
                    ErrorManager.getDefault().notify((Throwable)ble);
                }
            }
        };
        if (document instanceof BaseDocument) {
            ((BaseDocument)document).runAtomic(r);
        } else {
            r.run();
        }
    }

}

