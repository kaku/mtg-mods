/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 */
package org.netbeans.modules.csl.api;

import java.util.Map;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;

public abstract class OccurrencesFinder<T extends Parser.Result>
extends ParserResultTask<T> {
    public abstract void setCaretPosition(int var1);

    @NonNull
    public abstract Map<OffsetRange, ColoringAttributes> getOccurrences();
}

