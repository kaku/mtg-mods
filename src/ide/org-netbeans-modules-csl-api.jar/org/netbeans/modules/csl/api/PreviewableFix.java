/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.api;

import org.netbeans.modules.csl.api.EditList;
import org.netbeans.modules.csl.api.HintFix;

public interface PreviewableFix
extends HintFix {
    public boolean canPreview();

    public EditList getEditList() throws Exception;
}

