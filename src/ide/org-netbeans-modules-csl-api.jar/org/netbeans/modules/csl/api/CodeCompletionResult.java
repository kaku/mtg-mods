/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.csl.api;

import java.util.Collections;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.CompletionProposal;

public abstract class CodeCompletionResult {
    public static final CodeCompletionResult NONE = new CodeCompletionResult(){

        @Override
        public List<CompletionProposal> getItems() {
            return Collections.emptyList();
        }

        @Override
        public boolean isTruncated() {
            return false;
        }

        @Override
        public boolean isFilterable() {
            return false;
        }
    };

    @NonNull
    public abstract List<CompletionProposal> getItems();

    public void beforeInsert(@NonNull CompletionProposal item) {
    }

    public boolean insert(@NonNull CompletionProposal item) {
        return false;
    }

    public void afterInsert(@NonNull CompletionProposal item) {
    }

    public abstract boolean isTruncated();

    public abstract boolean isFilterable();

}

