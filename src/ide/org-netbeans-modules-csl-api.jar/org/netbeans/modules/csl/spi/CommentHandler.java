/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.csl.spi;

import java.util.ArrayList;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;

public interface CommentHandler {
    @NonNull
    public int[] getCommentBlocks(@NonNull Document var1, int var2, int var3);

    @NonNull
    public int[] getAdjustedBlocks(@NonNull Document var1, int var2, int var3);

    @NonNull
    public String getCommentStartDelimiter();

    @NonNull
    public String getCommentEndDelimiter();

    public static abstract class DefaultCommentHandler
    implements CommentHandler {
        @Override
        public int[] getAdjustedBlocks(@NonNull Document doc, int from, int to) {
            return new int[]{from, to};
        }

        @Override
        public int[] getCommentBlocks(final Document doc, final int from, final int to) {
            final ArrayList comments = new ArrayList();
            Runnable task = new Runnable(){

                @Override
                public void run() {
                    int nextCommentEnd;
                    int nextCommentStart;
                    int commentEndOffset;
                    CharSequence text = DocumentUtilities.getText((Document)doc);
                    int lastCommentStartIndex = CharSequenceUtilities.lastIndexOf((CharSequence)text, (CharSequence)DefaultCommentHandler.this.getCommentStartDelimiter(), (int)from);
                    int lastCommentEndIndex = from > 0 ? CharSequenceUtilities.lastIndexOf((CharSequence)text, (CharSequence)DefaultCommentHandler.this.getCommentEndDelimiter(), (int)(from - 1)) : -1;
                    int searchFrom = from;
                    if (lastCommentStartIndex > -1 && (lastCommentStartIndex > lastCommentEndIndex || lastCommentEndIndex == -1) && (commentEndOffset = DefaultCommentHandler.this.getCommentEnd(text, lastCommentStartIndex)) > 0) {
                        comments.add(lastCommentStartIndex);
                        comments.add(commentEndOffset);
                        searchFrom = commentEndOffset;
                    }
                    while ((nextCommentStart = CharSequenceUtilities.indexOf((CharSequence)text, (CharSequence)DefaultCommentHandler.this.getCommentStartDelimiter(), (int)searchFrom)) != -1 && nextCommentStart < to && (nextCommentEnd = DefaultCommentHandler.this.getCommentEnd(text, nextCommentStart)) > 0) {
                        comments.add(nextCommentStart);
                        comments.add(nextCommentEnd);
                        searchFrom = nextCommentEnd;
                    }
                }
            };
            if (doc instanceof BaseDocument) {
                ((BaseDocument)doc).runAtomic(task);
            } else {
                task.run();
            }
            int[] arr = new int[comments.size()];
            for (int i = 0; i < arr.length; ++i) {
                arr[i] = (Integer)comments.get(i);
            }
            return arr;
        }

        private int getCommentEnd(CharSequence text, int commentStartOffset) {
            int offset = CharSequenceUtilities.indexOf((CharSequence)text, (CharSequence)this.getCommentEndDelimiter(), (int)commentStartOffset);
            return offset == -1 ? -1 : offset + this.getCommentEndDelimiter().length();
        }

    }

}

