/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.LineCookie
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObject$Registry
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.csl.spi;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.openide.ErrorManager;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.InstanceCookie;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public class DefaultDataLoadersBridge
extends DataLoadersBridge {
    private static final Logger LOG = Logger.getLogger(DataLoadersBridge.class.getName());

    private DataObject getDataObject(Document doc) {
        Object o = doc.getProperty("stream");
        if (o instanceof DataObject) {
            return (DataObject)o;
        }
        if (o != null) {
            LOG.warning("Unable to return DataObject for Document " + doc + ". StreamDescriptionProperty points to non-DataLoader instace: " + o);
        }
        return null;
    }

    @Override
    public FileObject getFileObject(Document doc) {
        Object o = doc.getProperty("stream");
        if (o instanceof DataObject) {
            return ((DataObject)o).getPrimaryFile();
        }
        if (o instanceof FileObject) {
            return (FileObject)o;
        }
        if (o != null) {
            LOG.warning("Unable to return FileObject for Document " + doc + ". StreamDescriptionProperty points to non-DataLoader, non-FileObject instace: " + o);
        }
        return null;
    }

    @Override
    public StyledDocument getDocument(FileObject file) {
        try {
            DataObject d = DataObject.find((FileObject)file);
            EditorCookie ec = (EditorCookie)d.getCookie(EditorCookie.class);
            if (ec == null) {
                return null;
            }
            return ec.getDocument();
        }
        catch (IOException e) {
            LOG.log(Level.INFO, "SemanticHighlighter: Cannot find DataObject for file: " + FileUtil.getFileDisplayName((FileObject)file), e);
            return null;
        }
    }

    @Override
    public JEditorPane[] getOpenedPanes(FileObject fo) {
        DataObject dobj;
        try {
            dobj = DataObject.find((FileObject)fo);
        }
        catch (DataObjectNotFoundException ex) {
            return new JEditorPane[0];
        }
        EditorCookie editorCookie = (EditorCookie)dobj.getCookie(EditorCookie.class);
        if (editorCookie == null) {
            return new JEditorPane[0];
        }
        return editorCookie.getOpenedPanes();
    }

    @Override
    public Object createInstance(FileObject file) {
        assert (file.getExt().equals("instance"));
        try {
            DataObject dobj = DataObject.find((FileObject)file);
            InstanceCookie ic = (InstanceCookie)dobj.getCookie(InstanceCookie.class);
            return ic.instanceCreate();
        }
        catch (ClassNotFoundException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        catch (DataObjectNotFoundException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        catch (IOException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        return null;
    }

    @Override
    public FileObject getPrimaryFile(FileObject fileObject) {
        try {
            DataObject dobj = DataObject.find((FileObject)fileObject);
            if (dobj != null) {
                return dobj.getPrimaryFile();
            }
            return null;
        }
        catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

    @Override
    public String getLine(Document doc, int lineNumber) {
        DataObject dObj = (DataObject)doc.getProperty("stream");
        LineCookie lc = (LineCookie)dObj.getCookie(LineCookie.class);
        Line line = lc.getLineSet().getCurrent(lineNumber);
        return line.getText();
    }

    public Object getCookie(FileObject fo, Class aClass) throws IOException {
        DataObject od = DataObject.find((FileObject)fo);
        return od.getCookie(aClass);
    }

    public Object getSafeCookie(FileObject fo, Class aClass) {
        try {
            return this.getCookie(fo, aClass);
        }
        catch (IOException ioe) {
            return null;
        }
    }

    @Override
    public EditorCookie isModified(FileObject file) {
        DataObject.Registry regs = DataObject.getRegistry();
        Set modified = regs.getModifiedSet();
        for (DataObject dobj : modified) {
            if (!file.equals((Object)dobj.getPrimaryFile())) continue;
            EditorCookie ec = (EditorCookie)dobj.getCookie(EditorCookie.class);
            return ec;
        }
        return null;
    }

    @Override
    public PropertyChangeListener getDataObjectListener(FileObject fo, FileChangeListener fcl) throws IOException {
        return new DataObjectListener(fo, fcl);
    }

    @Override
    public Node getNodeDelegate(JTextComponent target) {
        DataObject dobj = this.getDataObject(target.getDocument());
        return dobj != null ? dobj.getNodeDelegate() : null;
    }

    private static final class DataObjectListener
    implements PropertyChangeListener {
        private DataObject dobj;
        private final FileObject fobj;
        private PropertyChangeListener wlistener;
        private final FileChangeListener flisten;

        public DataObjectListener(FileObject fo, FileChangeListener fcl) throws DataObjectNotFoundException {
            this.fobj = fo;
            this.flisten = fcl;
            this.dobj = DataObject.find((FileObject)fo);
            this.wlistener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.dobj);
            this.dobj.addPropertyChangeListener(this.wlistener);
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            DataObject invalidDO = (DataObject)pce.getSource();
            if (invalidDO != this.dobj) {
                return;
            }
            if ("valid".equals(pce.getPropertyName())) {
                this.handleInvalidDataObject(invalidDO);
            } else if (pce.getPropertyName() == null && !this.dobj.isValid()) {
                this.handleInvalidDataObject(invalidDO);
            }
        }

        private void handleInvalidDataObject(DataObject invalidDO) {
            invalidDO.removePropertyChangeListener(this.wlistener);
            if (this.fobj.isValid()) {
                try {
                    this.dobj = DataObject.find((FileObject)this.fobj);
                    this.dobj.addPropertyChangeListener(this.wlistener);
                    this.flisten.fileChanged(new FileEvent(this.fobj));
                }
                catch (IOException ex) {
                    LOG.log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        }
    }

}

