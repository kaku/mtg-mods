/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.spi;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Severity;
import org.openide.filesystems.FileObject;

public class DefaultError
implements Error {
    private String displayName;
    private String description;
    private FileObject file;
    private int start;
    private int end;
    private boolean lineError;
    private String key;
    private Severity severity;
    private Object[] parameters;

    public static Error createDefaultError(@NullAllowed String key, @NonNull String displayName, @NullAllowed String description, @NonNull FileObject file, @NonNull int start, @NonNull int end, boolean lineError, @NonNull Severity severity) {
        return new DefaultError(key, displayName, description, file, start, end, lineError, severity);
    }

    public DefaultError(@NullAllowed String key, @NonNull String displayName, @NullAllowed String description, @NonNull FileObject file, @NonNull int start, @NonNull int end, @NonNull Severity severity) {
        this(key, displayName, description, file, start, end, true, severity);
    }

    public DefaultError(@NullAllowed String key, @NonNull String displayName, @NullAllowed String description, @NonNull FileObject file, @NonNull int start, @NonNull int end, boolean lineError, @NonNull Severity severity) {
        this.key = key;
        this.displayName = displayName;
        this.description = description;
        this.file = file;
        this.start = start;
        this.end = end;
        this.lineError = lineError;
        this.severity = severity;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public int getStartPosition() {
        return this.start;
    }

    @Override
    public int getEndPosition() {
        return this.end;
    }

    @Override
    public boolean isLineError() {
        return this.lineError;
    }

    public String toString() {
        return "DefaultError[" + this.displayName + ", " + this.description + ", " + (Object)((Object)this.severity) + "]";
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public Object[] getParameters() {
        return this.parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }

    @Override
    public Severity getSeverity() {
        return this.severity;
    }

    @Override
    public FileObject getFile() {
        return this.file;
    }

    public void setOffsets(int start, int end) {
        this.start = start;
        this.end = end;
    }
}

