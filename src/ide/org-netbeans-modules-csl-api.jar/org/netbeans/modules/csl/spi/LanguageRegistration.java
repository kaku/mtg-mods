/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.spi;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE})
public @interface LanguageRegistration {
    public String[] mimeType();

    public boolean useCustomEditorKit() default 0;

    public boolean useMultiview() default 0;
}

