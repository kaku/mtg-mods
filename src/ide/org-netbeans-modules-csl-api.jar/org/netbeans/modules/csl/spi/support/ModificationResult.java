/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.modules.refactoring.spi.ModificationResult
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.NbDocument
 *  org.openide.text.PositionRef
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.csl.spi.support;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.text.PositionRef;
import org.openide.util.Parameters;

public final class ModificationResult
implements org.netbeans.modules.refactoring.spi.ModificationResult {
    private boolean committed;
    private final Map<FileObject, List<Difference>> diffs = new HashMap<FileObject, List<Difference>>();
    private static final Comparator<Difference> COMPARATOR = new Comparator<Difference>(){

        @Override
        public int compare(Difference d1, Difference d2) {
            return d1.getStartPosition().getOffset() - d2.getStartPosition().getOffset();
        }
    };

    public void addDifferences(FileObject fo, List<Difference> differences) {
        List<Difference> fileDiffs = this.diffs.get((Object)fo);
        if (fileDiffs == null) {
            fileDiffs = new ArrayList<Difference>();
            this.diffs.put(fo, fileDiffs);
        }
        fileDiffs.addAll(differences);
        if (fileDiffs.size() > 0) {
            Collections.sort(fileDiffs, COMPARATOR);
        }
    }

    public Set<? extends FileObject> getModifiedFileObjects() {
        return this.diffs.keySet();
    }

    public List<? extends Difference> getDifferences(FileObject fo) {
        return this.diffs.get((Object)fo);
    }

    public Set<File> getNewFiles() {
        HashSet<File> newFiles = new HashSet<File>();
        for (List<Difference> ds : this.diffs.values()) {
            for (Difference d : ds) {
                if (d.getKind() != Difference.Kind.CREATE) continue;
                newFiles.add(((CreateFileDifference)d).getFile());
            }
        }
        return newFiles;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void commit() throws IOException {
        if (this.committed) {
            throw new IllegalStateException("Calling commit on already committed Modificationesult.");
        }
        try {
            for (Map.Entry<FileObject, List<Difference>> me : this.diffs.entrySet()) {
                this.commit(me.getKey(), me.getValue(), null);
            }
        }
        finally {
            this.committed = true;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void commit(FileObject fo, final List<Difference> differences, final Writer out) throws IOException {
        EditorCookie ec;
        StyledDocument doc;
        DataObject dObj = DataObject.find((FileObject)fo);
        EditorCookie editorCookie = ec = dObj != null ? (EditorCookie)dObj.getCookie(EditorCookie.class) : null;
        if (ec != null && out == null && (doc = ec.getDocument()) != null) {
            final IOException[] exceptions = new IOException[1];
            NbDocument.runAtomic((StyledDocument)doc, (Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        ModificationResult.this.commit2(doc, differences, out);
                    }
                    catch (IOException ex) {
                        exceptions[0] = ex;
                    }
                }
            });
            if (exceptions[0] != null) {
                throw exceptions[0];
            }
            return;
        }
        InputStream ins = null;
        ByteArrayOutputStream baos = null;
        Reader in = null;
        Writer out2 = out;
        try {
            boolean ownOutput;
            int n;
            Charset encoding = FileEncodingQuery.getEncoding((FileObject)fo);
            ins = fo.getInputStream();
            baos = new ByteArrayOutputStream();
            FileUtil.copy((InputStream)ins, (OutputStream)baos);
            ins.close();
            ins = null;
            byte[] arr = baos.toByteArray();
            int arrLength = this.convertToLF(arr);
            baos.close();
            baos = null;
            in = new InputStreamReader((InputStream)new ByteArrayInputStream(arr, 0, arrLength), encoding);
            boolean bl = ownOutput = out != null;
            if (out2 == null) {
                out2 = new OutputStreamWriter(fo.getOutputStream(), encoding);
            }
            int offset = 0;
            for (Difference diff : differences) {
                int n2;
                if (diff.isExcluded()) continue;
                if (Difference.Kind.CREATE == diff.getKind()) {
                    if (ownOutput) continue;
                    this.createUnit((CreateFileDifference)diff, null);
                    continue;
                }
                int pos = diff.getStartPosition().getOffset();
                int toread = pos - offset;
                char[] buff = new char[toread];
                int rc = 0;
                while ((n2 = in.read(buff, 0, toread - rc)) > 0 && rc < toread) {
                    out2.write(buff, 0, n2);
                    rc += n2;
                    offset += n2;
                }
                switch (diff.getKind()) {
                    int len;
                    case INSERT: {
                        out2.write(diff.getNewText());
                        break;
                    }
                    case REMOVE: {
                        len = diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset();
                        in.skip(len);
                        offset += len;
                        break;
                    }
                    case CHANGE: {
                        len = diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset();
                        in.skip(len);
                        offset += len;
                        out2.write(diff.getNewText());
                    }
                }
            }
            char[] buff = new char[1024];
            while ((n = in.read(buff)) > 0) {
                out2.write(buff, 0, n);
            }
        }
        finally {
            if (ins != null) {
                ins.close();
            }
            if (baos != null) {
                baos.close();
            }
            if (in != null) {
                in.close();
            }
            if (out2 != null) {
                out2.close();
            }
        }
    }

    private void commit2(StyledDocument doc, List<Difference> differences, Writer out) throws IOException {
        for (Difference diff : differences) {
            if (diff.isExcluded()) continue;
            switch (diff.getKind()) {
                case INSERT: 
                case REMOVE: 
                case CHANGE: {
                    this.processDocument(doc, diff);
                    break;
                }
                case CREATE: {
                    this.createUnit((CreateFileDifference)diff, out);
                }
            }
        }
    }

    private void processDocument(final StyledDocument doc, final Difference diff) throws IOException {
        final BadLocationException[] blex = new BadLocationException[1];
        Runnable task = new Runnable(){

            @Override
            public void run() {
                try {
                    ModificationResult.this.processDocumentLocked(doc, diff);
                }
                catch (BadLocationException ex) {
                    blex[0] = ex;
                }
            }
        };
        if (diff.isCommitToGuards()) {
            NbDocument.runAtomic((StyledDocument)doc, (Runnable)task);
        } else {
            try {
                NbDocument.runAtomicAsUser((StyledDocument)doc, (Runnable)task);
            }
            catch (BadLocationException ex) {
                blex[0] = ex;
            }
        }
        if (blex[0] != null) {
            IOException ioe = new IOException();
            ioe.initCause(blex[0]);
            throw ioe;
        }
    }

    private void processDocumentLocked(Document doc, Difference diff) throws BadLocationException {
        switch (diff.getKind()) {
            case INSERT: {
                doc.insertString(diff.getStartPosition().getOffset(), diff.getNewText(), null);
                break;
            }
            case REMOVE: {
                doc.remove(diff.getStartPosition().getOffset(), diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset());
                break;
            }
            case CHANGE: {
                doc.remove(diff.getStartPosition().getOffset(), diff.getEndPosition().getOffset() - diff.getStartPosition().getOffset());
                doc.insertString(diff.getStartPosition().getOffset(), diff.getNewText(), null);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void createUnit(CreateFileDifference diff, Writer out) {
        Writer w = out;
        try {
            if (w == null) {
                w = new FileWriter(diff.getFile());
            }
            w.append(diff.getNewText());
        }
        catch (IOException e) {
            Logger.getLogger(ModificationResult.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        finally {
            if (w != null) {
                try {
                    w.close();
                }
                catch (IOException e) {
                    Logger.getLogger(ModificationResult.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }

    private int convertToLF(byte[] buff) {
        int j = 0;
        for (int i = 0; i < buff.length; ++i) {
            if (buff[i] == 13) continue;
            buff[j++] = buff[i];
        }
        return j;
    }

    public String getResultingSource(FileObject fileObject) throws IOException, IllegalArgumentException {
        Parameters.notNull((CharSequence)"fileObject", (Object)fileObject);
        if (!this.getModifiedFileObjects().contains((Object)fileObject)) {
            throw new IllegalArgumentException("File: " + FileUtil.getFileDisplayName((FileObject)fileObject) + " is not modified in this ModificationResult");
        }
        StringWriter writer = new StringWriter();
        this.commit(fileObject, this.diffs.get((Object)fileObject), writer);
        return writer.toString();
    }

    public static class CreateFileDifference
    extends Difference {
        private final File file;

        public CreateFileDifference(File file, String text) {
            super(Difference.Kind.CREATE, null, null, null, text, "Create file " + file.getPath());
            this.file = file;
        }

        public final File getFile() {
            return this.file;
        }

        @Override
        public String toString() {
            return (Object)((Object)this.kind) + "Create File: " + this.file.getName() + "; contents = \"\n" + this.newText + "\"";
        }
    }

    public static class Difference {
        Kind kind;
        PositionRef startPos;
        PositionRef endPos;
        String oldText;
        String newText;
        String description;
        private boolean excluded;
        private boolean ignoreGuards = false;

        public Difference(Kind kind, PositionRef startPos, PositionRef endPos, String oldText, String newText, String description) {
            this.kind = kind;
            this.startPos = startPos;
            this.endPos = endPos;
            this.oldText = oldText;
            this.newText = newText;
            this.description = description;
            this.excluded = false;
        }

        public Difference(Kind kind, PositionRef startPos, PositionRef endPos, String oldText, String newText) {
            this(kind, startPos, endPos, oldText, newText, null);
        }

        public Kind getKind() {
            return this.kind;
        }

        public PositionRef getStartPosition() {
            return this.startPos;
        }

        public PositionRef getEndPosition() {
            return this.endPos;
        }

        public String getOldText() {
            return this.oldText;
        }

        public String getNewText() {
            return this.newText;
        }

        public boolean isExcluded() {
            return this.excluded;
        }

        public void exclude(boolean b) {
            this.excluded = b;
        }

        public boolean isCommitToGuards() {
            return this.ignoreGuards;
        }

        public void setCommitToGuards(boolean b) {
            this.ignoreGuards = b;
        }

        public String toString() {
            return (Object)((Object)this.kind) + "<" + this.startPos.getOffset() + ", " + this.endPos.getOffset() + ">: " + this.oldText + " -> " + this.newText;
        }

        public String getDescription() {
            return this.description;
        }

        public static enum Kind {
            INSERT,
            REMOVE,
            CHANGE,
            CREATE;
            

            private Kind() {
            }
        }

    }

}

