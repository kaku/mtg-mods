/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 */
package org.netbeans.modules.csl.spi;

import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.OverridingMethods;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.CommentHandler;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;

public abstract class DefaultLanguageConfig
implements GsfLanguage {
    public CommentHandler getCommentHandler() {
        return null;
    }

    @Override
    public String getLineCommentPrefix() {
        return null;
    }

    @Override
    public boolean isIdentifierChar(char c) {
        return Character.isJavaIdentifierPart(c);
    }

    @Override
    public abstract Language getLexerLanguage();

    @Override
    public abstract String getDisplayName();

    @Override
    public String getPreferredExtension() {
        return null;
    }

    @Override
    public Set<String> getBinaryLibraryPathIds() {
        return null;
    }

    @Override
    public Set<String> getLibraryPathIds() {
        return null;
    }

    @Override
    public Set<String> getSourcePathIds() {
        return null;
    }

    public Parser getParser() {
        return null;
    }

    public boolean isUsingCustomEditorKit() {
        return false;
    }

    @CheckForNull
    public CodeCompletionHandler getCompletionHandler() {
        return null;
    }

    @CheckForNull
    public InstantRenamer getInstantRenamer() {
        return null;
    }

    @CheckForNull
    public DeclarationFinder getDeclarationFinder() {
        return null;
    }

    public boolean hasFormatter() {
        return false;
    }

    @CheckForNull
    public Formatter getFormatter() {
        return null;
    }

    @CheckForNull
    public KeystrokeHandler getKeystrokeHandler() {
        return null;
    }

    @CheckForNull
    public EmbeddingIndexerFactory getIndexerFactory() {
        return null;
    }

    public boolean hasStructureScanner() {
        return false;
    }

    @CheckForNull
    public StructureScanner getStructureScanner() {
        return null;
    }

    public boolean hasHintsProvider() {
        return false;
    }

    @CheckForNull
    public HintsProvider getHintsProvider() {
        return null;
    }

    public boolean hasOccurrencesFinder() {
        return false;
    }

    @CheckForNull
    public OccurrencesFinder getOccurrencesFinder() {
        return null;
    }

    @CheckForNull
    public SemanticAnalyzer getSemanticAnalyzer() {
        return null;
    }

    public IndexSearcher getIndexSearcher() {
        return null;
    }

    public OverridingMethods getOverridingMethods() {
        return null;
    }
}

