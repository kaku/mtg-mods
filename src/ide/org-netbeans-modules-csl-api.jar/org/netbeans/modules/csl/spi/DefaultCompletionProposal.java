/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.spi;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;

public abstract class DefaultCompletionProposal
implements CompletionProposal {
    protected int anchorOffset;
    protected boolean smart;
    protected ElementKind elementKind;

    public boolean beforeDefaultAction() {
        return false;
    }

    @Override
    public int getSortPrioOverride() {
        return 0;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public Set<Modifier> getModifiers() {
        return Collections.emptySet();
    }

    @Override
    public boolean isSmart() {
        return this.smart;
    }

    @Override
    public int getAnchorOffset() {
        return this.anchorOffset;
    }

    @Override
    public String getInsertPrefix() {
        return this.getName();
    }

    @Override
    public String getSortText() {
        return this.getName();
    }

    @Override
    public ElementKind getKind() {
        return this.elementKind;
    }

    @Override
    public String getLhsHtml(HtmlFormatter formatter) {
        ElementKind kind = this.getKind();
        formatter.name(kind, true);
        formatter.appendText(this.getName());
        formatter.name(kind, false);
        return formatter.getText();
    }

    @Override
    public String getRhsHtml(HtmlFormatter formatter) {
        return null;
    }

    public void setKind(ElementKind kind) {
        this.elementKind = kind;
    }

    public void setSmart(boolean smart) {
        this.smart = smart;
    }

    public void setAnchorOffset(int anchorOffset) {
        this.anchorOffset = anchorOffset;
    }

    public List<String> getInsertParams() {
        return null;
    }

    public String[] getParamListDelimiters() {
        return null;
    }

    @Override
    public String getCustomInsertTemplate() {
        List<String> params = this.getInsertParams();
        if (params == null || params.size() == 0) {
            return this.getInsertPrefix();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.getInsertPrefix());
        String[] delimiters = this.getParamListDelimiters();
        assert (delimiters.length == 2);
        sb.append(delimiters[0]);
        int id = 1;
        Iterator<String> it = params.iterator();
        while (it.hasNext()) {
            String paramDesc = it.next();
            sb.append("${");
            sb.append("gsf-cc-");
            sb.append(Integer.toString(id++));
            sb.append(" default=\"");
            sb.append(paramDesc);
            sb.append("\"");
            sb.append("}");
            if (!it.hasNext()) continue;
            sb.append(", ");
        }
        sb.append(delimiters[1]);
        sb.append("${cursor}");
        return sb.toString();
    }
}

