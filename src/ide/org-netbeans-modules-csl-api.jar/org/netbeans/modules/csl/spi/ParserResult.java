/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 */
package org.netbeans.modules.csl.spi;

import java.util.List;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;

public abstract class ParserResult
extends Parser.Result {
    protected ParserResult(Snapshot snapshot) {
        super(snapshot);
    }

    public abstract List<? extends Error> getDiagnostics();
}

