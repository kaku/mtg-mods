/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.openide.ErrorManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.LineCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.Line
 *  org.openide.text.Line$ShowOpenType
 *  org.openide.text.Line$ShowVisibilityType
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.UserQuestionException
 */
package org.netbeans.modules.csl.spi;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.EventObject;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.openide.ErrorManager;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.UserQuestionException;

public final class GsfUtilities {
    private static final Logger LOG = Logger.getLogger(GsfUtilities.class.getName());
    private static final Map<Source, Integer> enforcedCaretOffsets = new WeakHashMap<Source, Integer>();

    private GsfUtilities() {
    }

    public static int getLineIndent(BaseDocument doc, int offset) {
        try {
            return IndentUtils.lineIndent((Document)doc, (int)Utilities.getRowStart((BaseDocument)doc, (int)offset));
        }
        catch (BadLocationException ble) {
            LOG.log(Level.WARNING, null, ble);
            return 0;
        }
    }

    public static int setLineIndentation(BaseDocument doc, int lineOffset, int newIndent) throws BadLocationException {
        int i;
        char ch;
        int oldIndentEndOffset;
        int lineStartOffset = Utilities.getRowStart((BaseDocument)doc, (int)lineOffset);
        int indent = 0;
        int tabSize = -1;
        CharSequence docText = DocumentUtilities.getText((Document)doc);
        for (oldIndentEndOffset = lineStartOffset; oldIndentEndOffset < docText.length() && (ch = docText.charAt(oldIndentEndOffset)) != '\n'; ++oldIndentEndOffset) {
            if (ch == '\t') {
                if (tabSize == -1) {
                    tabSize = IndentUtils.tabSize((Document)doc);
                }
                indent = (indent + tabSize) / tabSize * tabSize;
                continue;
            }
            if (!Character.isWhitespace(ch)) break;
            ++indent;
        }
        String newIndentString = IndentUtils.createIndentString((Document)doc, (int)newIndent);
        int offset = lineStartOffset;
        boolean different = false;
        for (i = 0; i < newIndentString.length() && lineStartOffset + i < oldIndentEndOffset; ++i) {
            if (newIndentString.charAt(i) == docText.charAt(lineStartOffset + i)) continue;
            offset = lineStartOffset + i;
            newIndentString = newIndentString.substring(i);
            different = true;
            break;
        }
        if (!different) {
            offset = lineStartOffset + i;
            newIndentString = newIndentString.substring(i);
        }
        if (offset < oldIndentEndOffset) {
            doc.remove(offset, oldIndentEndOffset - offset);
        }
        if (newIndentString.length() > 0) {
            doc.insertString(offset, newIndentString, null);
        }
        return newIndentString.length() - (oldIndentEndOffset - offset);
    }

    public static JTextComponent getOpenPane() {
        JTextComponent pane = EditorRegistry.lastFocusedComponent();
        return pane;
    }

    public static JTextComponent getPaneFor(FileObject fo) {
        JTextComponent pane = GsfUtilities.getOpenPane();
        if (pane != null && GsfUtilities.findFileObject(pane) == fo) {
            return pane;
        }
        for (JTextComponent c : EditorRegistry.componentList()) {
            if (GsfUtilities.findFileObject(c) != fo) continue;
            return c;
        }
        return null;
    }

    public static BaseDocument getDocument(FileObject fileObject, boolean openIfNecessary) {
        return GsfUtilities.getDocument(fileObject, openIfNecessary, false);
    }

    public static BaseDocument getDocument(FileObject fileObject, boolean openIfNecessary, boolean skipLarge) {
        if (skipLarge && fileObject.getSize() > 0x100000) {
            return null;
        }
        try {
            EditorCookie ec;
            EditorCookie editorCookie = ec = fileObject.isValid() ? DataLoadersBridge.getDefault().getCookie(fileObject, EditorCookie.class) : null;
            if (ec != null) {
                if (openIfNecessary) {
                    try {
                        return (BaseDocument)ec.openDocument();
                    }
                    catch (UserQuestionException uqe) {
                        uqe.confirmed();
                        return (BaseDocument)ec.openDocument();
                    }
                }
                return (BaseDocument)ec.getDocument();
            }
        }
        catch (IOException ex) {
            LOG.log(Level.WARNING, null, ex);
        }
        return null;
    }

    @Deprecated
    public static BaseDocument getBaseDocument(FileObject fileObject, boolean forceOpen) {
        return GsfUtilities.getDocument(fileObject, forceOpen);
    }

    public static FileObject findFileObject(Document doc) {
        DataObject dobj = (DataObject)doc.getProperty("stream");
        if (dobj == null) {
            return null;
        }
        return dobj.getPrimaryFile();
    }

    public static FileObject findFileObject(JTextComponent target) {
        Document doc = target.getDocument();
        return GsfUtilities.findFileObject(doc);
    }

    public static boolean open(final FileObject fo, final int offset, final String search) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GsfUtilities.doOpen(fo, offset, search);
                }
            });
            return true;
        }
        return GsfUtilities.doOpen(fo, offset, search);
    }

    private static boolean doOpen(FileObject fo, int offset, String search) {
        try {
            DataObject od = DataObject.find((FileObject)fo);
            EditorCookie ec = (EditorCookie)od.getCookie(EditorCookie.class);
            LineCookie lc = (LineCookie)od.getCookie(LineCookie.class);
            if (ec != null && offset == -1 && ec.getDocument() != null && search == null) {
                ec.open();
                return true;
            }
            if (search != null && offset == -1) {
                StyledDocument doc = NbDocument.getDocument((Lookup.Provider)od);
                try {
                    String text = doc.getText(0, doc.getLength());
                    int caretDelta = search.indexOf(94);
                    if (caretDelta != -1) {
                        search = search.substring(0, caretDelta) + search.substring(caretDelta + 1);
                    } else {
                        caretDelta = 0;
                    }
                    offset = text.indexOf(search);
                    if (offset != -1) {
                        offset += caretDelta;
                    }
                }
                catch (BadLocationException ble) {
                    LOG.log(Level.WARNING, null, ble);
                }
            }
            return NbDocument.openDocument((Lookup.Provider)od, (int)offset, (Line.ShowOpenType)Line.ShowOpenType.OPEN, (Line.ShowVisibilityType)Line.ShowVisibilityType.FOCUS);
        }
        catch (IOException e) {
            ErrorManager.getDefault().notify(1, (Throwable)e);
            return false;
        }
    }

    public static void extractZip(FileObject extract, FileObject dest) throws IOException {
        File extractFile = FileUtil.toFile((FileObject)extract);
        GsfUtilities.extractZip(dest, new BufferedInputStream(new FileInputStream(extractFile)));
    }

    private static void extractZip(final FileObject fo, final InputStream is) throws IOException {
        FileSystem fs = fo.getFileSystem();
        fs.runAtomicAction(new FileSystem.AtomicAction(){

            public void run() throws IOException {
                GsfUtilities.extractZipImpl(fo, is);
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void extractZipImpl(FileObject fo, InputStream is) throws IOException {
        ZipEntry je;
        ZipInputStream jis = new ZipInputStream(is);
        while ((je = jis.getNextEntry()) != null) {
            String name = je.getName();
            if (name.toLowerCase().startsWith("meta-inf/")) continue;
            if (je.isDirectory()) {
                FileUtil.createFolder((FileObject)fo, (String)name);
                continue;
            }
            FileObject fd = FileUtil.createData((FileObject)fo, (String)name);
            FileLock lock = fd.lock();
            try {
                OutputStream os = fd.getOutputStream(lock);
                try {
                    FileUtil.copy((InputStream)jis, (OutputStream)os);
                    continue;
                }
                finally {
                    os.close();
                    continue;
                }
            }
            finally {
                lock.releaseLock();
                continue;
            }
        }
    }

    public static boolean isCodeTemplateEditing(Document doc) {
        String EDITING_TEMPLATE_DOC_PROPERTY = "processing-code-template";
        String CT_HANDLER_DOC_PROPERTY = "code-template-insert-handler";
        return doc.getProperty(EDITING_TEMPLATE_DOC_PROPERTY) == Boolean.TRUE || doc.getProperty(CT_HANDLER_DOC_PROPERTY) != null;
    }

    public static boolean isRowWhite(CharSequence text, int offset) throws BadLocationException {
        try {
            int i;
            char c;
            for (i = offset; i < text.length() && (c = text.charAt(i)) != '\n'; ++i) {
                if (Character.isWhitespace(c)) continue;
                return false;
            }
            for (i = offset - 1; i >= 0 && (c = text.charAt(i)) != '\n'; --i) {
                if (Character.isWhitespace(c)) continue;
                return false;
            }
            return true;
        }
        catch (Exception ex) {
            BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
            ble.initCause(ex);
            throw ble;
        }
    }

    public static boolean isRowEmpty(CharSequence text, int offset) throws BadLocationException {
        try {
            char c;
            if (offset < text.length() && (c = text.charAt(offset)) != '\n' && (c != '\r' || offset != text.length() - 1 && text.charAt(offset + 1) != '\n')) {
                return false;
            }
            if (offset != 0 && text.charAt(offset - 1) != '\n') {
                return false;
            }
            return true;
        }
        catch (Exception ex) {
            BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
            ble.initCause(ex);
            throw ble;
        }
    }

    public static int getRowLastNonWhite(CharSequence text, int offset) throws BadLocationException {
        try {
            int i;
            char c;
            for (i = offset; i < text.length() && (c = text.charAt(i)) != '\n' && (c != '\r' || i != text.length() - 1 && text.charAt(i + 1) != '\n'); ++i) {
            }
            --i;
            while (i >= 0) {
                c = text.charAt(i);
                if (c == '\n') {
                    return -1;
                }
                if (!Character.isWhitespace(c)) {
                    return i;
                }
                --i;
            }
            return -1;
        }
        catch (Exception ex) {
            BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
            ble.initCause(ex);
            throw ble;
        }
    }

    public static int getRowFirstNonWhite(CharSequence text, int offset) throws BadLocationException {
        try {
            int i;
            char c;
            if (i < text.length()) {
                for (i = offset - 1; i >= 0 && (c = text.charAt(i)) != '\n'; --i) {
                }
                ++i;
            }
            while (i < text.length()) {
                c = text.charAt(i);
                if (c == '\n') {
                    return -1;
                }
                if (!Character.isWhitespace(c)) {
                    return i;
                }
                ++i;
            }
            return -1;
        }
        catch (Exception ex) {
            BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
            ble.initCause(ex);
            throw ble;
        }
    }

    public static int getRowStart(CharSequence text, int offset) throws BadLocationException {
        try {
            for (int i = offset - 1; i >= 0; --i) {
                char c = text.charAt(i);
                if (c != '\n') continue;
                return i + 1;
            }
            return 0;
        }
        catch (Exception ex) {
            BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
            ble.initCause(ex);
            throw ble;
        }
    }

    public static int getRowEnd(CharSequence text, int offset) throws BadLocationException {
        try {
            for (int i = offset; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (c != '\n') continue;
                return i;
            }
            return text.length();
        }
        catch (Exception ex) {
            BadLocationException ble = new BadLocationException("" + offset + " out of " + text.length(), offset);
            ble.initCause(ex);
            throw ble;
        }
    }

    public static boolean endsWith(StringBuilder sb, String s) {
        int len = s.length();
        if (sb.length() < len) {
            return false;
        }
        int i = sb.length() - len;
        for (int j = 0; j < len; ++j) {
            if (sb.charAt(i) != s.charAt(j)) {
                return false;
            }
            ++i;
        }
        return true;
    }

    public static String truncate(String s, int length) {
        assert (length > 3);
        if (s.length() <= length) {
            return s;
        }
        return s.substring(0, length - 3) + "...";
    }

    public static int getLastKnownCaretOffset(Snapshot snapshot, EventObject event) {
        Integer enforcedCaretOffset;
        Caret c;
        if (event instanceof CursorMovedSchedulerEvent) {
            return ((CursorMovedSchedulerEvent)event).getCaretOffset();
        }
        FileObject snapshotFile = snapshot.getSource().getFileObject();
        Document snapshotDoc = null;
        if (snapshotFile != null) {
            for (JTextComponent jtc : EditorRegistry.componentList()) {
                if (snapshotFile != NbEditorUtilities.getFileObject((Document)jtc.getDocument())) continue;
                snapshotDoc = snapshot.getSource().getDocument(false);
                if (snapshotDoc != null && snapshotDoc != jtc.getDocument() || (c = jtc.getCaret()) == null) break;
                return c.getDot();
            }
        }
        if (snapshotDoc == null && snapshotFile == null) {
            snapshotDoc = snapshot.getSource().getDocument(false);
        }
        if (snapshotDoc != null) {
            for (JTextComponent jtc : EditorRegistry.componentList()) {
                if (snapshotDoc != jtc.getDocument() || (c = jtc.getCaret()) == null) continue;
                return c.getDot();
            }
        }
        if ((enforcedCaretOffset = enforcedCaretOffsets.get((Object)snapshot.getSource())) != null) {
            return enforcedCaretOffset;
        }
        return -1;
    }

    public static CloneableEditorSupport findCloneableEditorSupport(FileObject fo) {
        try {
            DataObject dob = DataObject.find((FileObject)fo);
            Node.Cookie obj = dob.getCookie(OpenCookie.class);
            if (obj instanceof CloneableEditorSupport) {
                return (CloneableEditorSupport)obj;
            }
            obj = dob.getCookie(EditorCookie.class);
            if (obj instanceof CloneableEditorSupport) {
                return (CloneableEditorSupport)obj;
            }
        }
        catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    static void setLastKnowCaretOffset(Source source, int offset) {
        enforcedCaretOffsets.put(source, offset);
    }

}

