/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.spi;

import java.util.List;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.ParserResult;

public interface ErrorFilter {
    public static final String FEATURE_TASKLIST = "tasklist";

    public List<? extends Error> filter(ParserResult var1);

    public static interface Factory {
        public ErrorFilter createErrorFilter(String var1);
    }

}

