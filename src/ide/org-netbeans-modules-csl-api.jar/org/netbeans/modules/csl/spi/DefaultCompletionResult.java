/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.spi;

import java.util.List;
import org.netbeans.modules.csl.api.CodeCompletionResult;
import org.netbeans.modules.csl.api.CompletionProposal;

public class DefaultCompletionResult
extends CodeCompletionResult {
    protected boolean truncated;
    protected List<CompletionProposal> list;
    protected boolean filterable = true;

    public DefaultCompletionResult(List<CompletionProposal> list, boolean truncated) {
        this.list = list;
        this.truncated = truncated;
    }

    @Override
    public List<CompletionProposal> getItems() {
        return this.list;
    }

    @Override
    public boolean isTruncated() {
        return this.truncated;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
        if (truncated) {
            this.filterable = false;
        }
    }

    @Override
    public boolean isFilterable() {
        return this.filterable;
    }

    public void setFilterable(boolean filterable) {
        this.filterable = filterable;
    }
}

