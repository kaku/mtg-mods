/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.netbeans.spi.tasklist.PushTaskScanner
 *  org.netbeans.spi.tasklist.PushTaskScanner$Callback
 *  org.netbeans.spi.tasklist.Task
 *  org.netbeans.spi.tasklist.TaskScanningScope
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 */
package org.netbeans.modules.csl.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.TaskListener;

public final class GsfTaskProvider
extends PushTaskScanner {
    private static final Logger LOG = Logger.getLogger(GsfTaskProvider.class.getName());
    private static GsfTaskProvider INSTANCE;
    private PushTaskScanner.Callback callback;
    private TaskScanningScope scope;
    private static final Map<RequestProcessor.Task, Work> TASKS;
    private static boolean clearing;
    private static final RequestProcessor WORKER;

    public GsfTaskProvider() {
        this(null);
        INSTANCE = this;
    }

    private GsfTaskProvider(String languageList) {
        super(NbBundle.getMessage(GsfTaskProvider.class, (String)"GsfTasks"), NbBundle.getMessage(GsfTaskProvider.class, (String)"GsfTasksDesc"), null);
    }

    public synchronized void setScope(TaskScanningScope scope, PushTaskScanner.Callback callback) {
        GsfTaskProvider.cancelAllCurrent();
        this.scope = scope;
        this.callback = callback;
        if (scope == null || callback == null) {
            return;
        }
        for (FileObject file : scope.getLookup().lookupAll(FileObject.class)) {
            GsfTaskProvider.enqueue(new Work(file, callback));
        }
        for (Project p : scope.getLookup().lookupAll(Project.class)) {
            GsfTaskProvider.enqueue(new Work(p, callback));
        }
    }

    public static void refresh(FileObject file) {
        if (INSTANCE != null) {
            INSTANCE.refreshImpl(file);
        }
    }

    private synchronized void refreshImpl(FileObject file) {
        LOG.log(Level.FINE, "refresh: {0}", (Object)file);
        if (this.scope == null || this.callback == null) {
            return;
        }
        if (!this.scope.isInScope(file)) {
            if (!file.isFolder()) {
                return;
            }
            for (FileObject inScope : this.scope.getLookup().lookupAll(FileObject.class)) {
                if (!FileUtil.isParentOf((FileObject)file, (FileObject)inScope)) continue;
                GsfTaskProvider.enqueue(new Work(inScope, this.callback));
            }
            return;
        }
        LOG.log(Level.FINE, "enqueing work for: {0}", (Object)file);
        GsfTaskProvider.enqueue(new Work(file, this.callback));
    }

    static String getAllLanguageNames() {
        StringBuilder sb = new StringBuilder();
        for (Language language : LanguageRegistry.getInstance()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(language.getDisplayName());
        }
        return sb.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void enqueue(Work w) {
        Map<RequestProcessor.Task, Work> map = TASKS;
        synchronized (map) {
            RequestProcessor.Task task = WORKER.post((Runnable)w);
            TASKS.put(task, w);
            task.addTaskListener(new TaskListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public void taskFinished(org.openide.util.Task task) {
                    Map map = TASKS;
                    synchronized (map) {
                        if (!clearing) {
                            TASKS.remove((Object)((RequestProcessor.Task)task));
                        }
                    }
                }
            });
            if (task.isFinished()) {
                TASKS.remove((Object)task);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void cancelAllCurrent() {
        Map<RequestProcessor.Task, Work> map = TASKS;
        synchronized (map) {
            clearing = true;
            try {
                for (Map.Entry<RequestProcessor.Task, Work> t : TASKS.entrySet()) {
                    t.getKey().cancel();
                    t.getValue().cancel();
                }
                TASKS.clear();
            }
            finally {
                clearing = false;
            }
        }
    }

    static {
        TASKS = new HashMap<RequestProcessor.Task, Work>();
        WORKER = new RequestProcessor("CSL Task Provider");
    }

    private static final class Work
    implements Runnable {
        private final FileObject file;
        private final Project project;
        private final PushTaskScanner.Callback callback;
        private final AtomicBoolean canceled = new AtomicBoolean();

        public Work(FileObject file, PushTaskScanner.Callback callback) {
            Parameters.notNull((CharSequence)"file", (Object)file);
            Parameters.notNull((CharSequence)"callback", (Object)callback);
            this.file = file;
            this.project = null;
            this.callback = callback;
        }

        public Work(Project project, PushTaskScanner.Callback callback) {
            Parameters.notNull((CharSequence)"project", (Object)project);
            Parameters.notNull((CharSequence)"callback", (Object)callback);
            this.file = null;
            this.project = project;
            this.callback = callback;
        }

        @Override
        public void run() {
            Collection results = null;
            if (this.isCanceled()) {
                return;
            }
            if (this.file != null) {
                FileObject root;
                Collection roots = QuerySupport.findRoots((FileObject)this.file, (Collection)null, Collections.emptyList(), Collections.emptyList());
                String relativePath = null;
                Iterator i$ = roots.iterator();
                while (i$.hasNext() && null == (relativePath = FileUtil.getRelativePath((FileObject)(root = (FileObject)i$.next()), (FileObject)this.file))) {
                }
                LOG.log(Level.FINE, "Querying TL index for {0}", relativePath);
                if (relativePath != null) {
                    try {
                        QuerySupport querySupport = QuerySupport.forRoots((String)"TLIndexer", (int)5, (FileObject[])roots.toArray((T[])new FileObject[roots.size()]));
                        if (this.isCanceled()) {
                            return;
                        }
                        results = querySupport.query("_sn", relativePath, QuerySupport.Kind.EXACT, new String[0]);
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.WARNING, null, ioe);
                    }
                }
            } else {
                Collection roots = QuerySupport.findRoots((Project)this.project, (Collection)null, Collections.emptyList(), Collections.emptyList());
                try {
                    QuerySupport querySupport = QuerySupport.forRoots((String)"TLIndexer", (int)5, (FileObject[])roots.toArray((T[])new FileObject[roots.size()]));
                    if (this.isCanceled()) {
                        return;
                    }
                    results = querySupport.query("_sn", "", QuerySupport.Kind.PREFIX, new String[0]);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            if (results != null && !this.isCanceled()) {
                Work.pushTasks(results, this.callback);
            }
        }

        public void cancel() {
            this.canceled.set(true);
        }

        private boolean isCanceled() {
            return this.canceled.get();
        }

        private static void pushTasks(Collection<? extends IndexResult> results, PushTaskScanner.Callback callback) {
            HashMap tasks = new HashMap();
            for (IndexResult result : results) {
                String description;
                FileObject f = result.getFile();
                if (f == null || !f.isValid()) continue;
                ArrayList<Task> l = (ArrayList<Task>)tasks.get((Object)f);
                if (l == null) {
                    l = new ArrayList<Task>();
                    tasks.put(f, l);
                }
                if ((description = result.getValue("description")) == null) continue;
                int lineNumber = 1;
                try {
                    lineNumber = Integer.parseInt(result.getValue("lineNumber"));
                }
                catch (NumberFormatException ex) {
                    // empty catch block
                }
                Task task = Task.create((FileObject)f, (String)result.getValue("groupName"), (String)description, (int)lineNumber);
                l.add(task);
            }
            for (FileObject f : tasks.keySet()) {
                List l = (List)tasks.get((Object)f);
                LOG.log(Level.FINE, "Refreshing TL for {0} with {1}", new Object[]{f, l});
                callback.setTasks(f, l);
            }
        }
    }

}

