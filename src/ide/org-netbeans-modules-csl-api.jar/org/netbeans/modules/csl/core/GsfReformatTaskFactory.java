/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.ReformatTask
 *  org.netbeans.modules.editor.indent.spi.ReformatTask$Factory
 *  org.netbeans.modules.parsing.api.Source
 */
package org.netbeans.modules.csl.core;

import javax.swing.text.Document;
import org.netbeans.modules.csl.core.GsfReformatTask;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.netbeans.modules.parsing.api.Source;

public class GsfReformatTaskFactory
implements ReformatTask.Factory {
    public ReformatTask createTask(Context context) {
        Source source = Source.create((Document)context.document());
        return source != null ? new GsfReformatTask(source, context) : null;
    }
}

