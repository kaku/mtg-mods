/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.lib.editor.codetemplates.CodeTemplateCompletionProvider
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator
 *  org.netbeans.modules.editor.indent.spi.IndentTask
 *  org.netbeans.modules.editor.indent.spi.IndentTask$Factory
 *  org.netbeans.modules.editor.indent.spi.ReformatTask
 *  org.netbeans.modules.editor.indent.spi.ReformatTask$Factory
 *  org.netbeans.modules.parsing.spi.ParserFactory
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.PathRecognizer
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatusProviderFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.csl.core;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.swing.JSeparator;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.lib.editor.codetemplates.CodeTemplateCompletionProvider;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.modules.csl.core.CslEditorKit;
import org.netbeans.modules.csl.core.EmbeddingIndexerFactoryImpl;
import org.netbeans.modules.csl.core.GsfDataLoader;
import org.netbeans.modules.csl.core.GsfIndentTaskFactory;
import org.netbeans.modules.csl.core.GsfParserFactory;
import org.netbeans.modules.csl.core.GsfReformatTaskFactory;
import org.netbeans.modules.csl.core.PathRecognizerImpl;
import org.netbeans.modules.csl.core.TLIndexerFactory;
import org.netbeans.modules.csl.editor.codetemplates.GsfCodeTemplateFilter;
import org.netbeans.modules.csl.editor.codetemplates.GsfCodeTemplateProcessor;
import org.netbeans.modules.csl.editor.completion.GsfCompletionProvider;
import org.netbeans.modules.csl.editor.fold.GsfFoldManagerFactory;
import org.netbeans.modules.csl.editor.hyperlink.GsfHyperlinkProvider;
import org.netbeans.modules.csl.editor.semantic.HighlightsLayerFactoryImpl;
import org.netbeans.modules.csl.editor.semantic.OccurrencesMarkProviderCreator;
import org.netbeans.modules.csl.hints.GsfUpToDateStateProviderFactory;
import org.netbeans.modules.csl.navigation.ClassMemberPanel;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator;
import org.netbeans.modules.editor.indent.spi.IndentTask;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProviderFactory;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedAnnotationTypes(value={"org.netbeans.modules.csl.spi.LanguageRegistration"})
@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class LanguageRegistrationProcessor
extends LayerGeneratingProcessor {
    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        for (Element e : roundEnv.getElementsAnnotatedWith(LanguageRegistration.class)) {
            TypeElement cls = (TypeElement)e;
            LanguageRegistration languageRegistration = cls.getAnnotation(LanguageRegistration.class);
            String[] mimeTypes = languageRegistration.mimeType();
            if (mimeTypes == null || mimeTypes.length == 0) {
                // empty if block
            }
            for (String mimeType : mimeTypes) {
                if (MimePath.validate((CharSequence)mimeType)) continue;
                throw new LayerGenerationException("Invalid mime type: '" + mimeType + "'", (Element)cls);
            }
            TypeElement dlc = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.csl.spi.DefaultLanguageConfig");
            if (!this.processingEnv.getTypeUtils().isSubtype(cls.asType(), dlc.asType())) {
                throw new LayerGenerationException("Class " + cls + " is not subclass of " + dlc, e);
            }
            boolean isAnnotatedByPathRecognizerRegistration = false;
            TypeElement prr = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.parsing.spi.indexing.PathRecognizerRegistration");
            for (AnnotationMirror am : cls.getAnnotationMirrors()) {
                if (!am.getAnnotationType().asElement().equals(prr)) continue;
                isAnnotatedByPathRecognizerRegistration = true;
                break;
            }
            List<ExecutableElement> methodsList = ElementFilter.methodsIn(cls.getEnclosedElements());
            HashMap<String, ExecutableElement> methods = new HashMap<String, ExecutableElement>(methodsList.size());
            for (ExecutableElement m : methodsList) {
                methods.put(m.getSimpleName().toString(), m);
            }
            LayerBuilder lb = this.layer(new Element[]{cls});
            for (String mimeType2 : mimeTypes) {
                LanguageRegistrationProcessor.registerCslPlugin(lb, mimeType2, cls, languageRegistration.useMultiview());
                LanguageRegistrationProcessor.registerTLIndexer(lb, mimeType2);
                if (methods.containsKey("getStructureScanner")) {
                    this.registerStructureScanner(lb, mimeType2);
                }
                if (languageRegistration.useCustomEditorKit()) continue;
                LanguageRegistrationProcessor.registerEditorKit(lb, mimeType2);
                LanguageRegistrationProcessor.registerLoader(lb, mimeType2);
                if (methods.containsKey("getLexerLanguage")) {
                    LanguageRegistrationProcessor.registerLexer(lb, mimeType2);
                }
                if (methods.containsKey("getParser")) {
                    LanguageRegistrationProcessor.registerParser(lb, mimeType2);
                }
                if (methods.containsKey("getIndexerFactory")) {
                    LanguageRegistrationProcessor.registerIndexer(lb, mimeType2);
                    if (!isAnnotatedByPathRecognizerRegistration) {
                        LanguageRegistrationProcessor.registerPathRecognizer(lb, mimeType2);
                    }
                }
                LanguageRegistrationProcessor.registerCodeCompletion(lb, mimeType2);
                LanguageRegistrationProcessor.registerCodeFolding(lb, mimeType2);
                LanguageRegistrationProcessor.registerCodeTemplates(lb, mimeType2);
                if (methods.containsKey("getDeclarationFinder")) {
                    LanguageRegistrationProcessor.registerHyperlinks(lb, mimeType2);
                }
                LanguageRegistrationProcessor.registerSemanticHighlighting(lb, mimeType2);
                LanguageRegistrationProcessor.registerUpToDateStatus(lb, mimeType2);
                LanguageRegistrationProcessor.registerContextMenu(lb, mimeType2, methods);
                LanguageRegistrationProcessor.registerCommentUncommentToolbarButtons(lb, mimeType2);
                if (!methods.containsKey("getFormatter")) continue;
                LanguageRegistrationProcessor.registerFormatterIndenter(lb, mimeType2);
            }
        }
        return true;
    }

    private static /* varargs */ LayerBuilder.File instanceFile(LayerBuilder b, String folder, String name, Class implClass, String factoryMethod, Class ... instanceOf) {
        return LanguageRegistrationProcessor.instanceFile(b, folder, name, implClass == null ? null : implClass.getName(), factoryMethod, instanceOf);
    }

    private static /* varargs */ LayerBuilder.File instanceFile(LayerBuilder b, String folder, String name, String implClass, String factoryMethod, Class ... instanceOf) {
        String basename;
        if (name == null) {
            basename = implClass.replace('.', '-');
            if (factoryMethod != null) {
                basename = basename + "-" + factoryMethod;
            }
        } else {
            basename = name;
        }
        LayerBuilder.File f = b.file(folder + "/" + basename + ".instance");
        if (implClass != null) {
            if (factoryMethod != null) {
                f.methodvalue("instanceCreate", implClass, factoryMethod);
            } else {
                f.stringvalue("instanceClass", implClass);
            }
        }
        for (Class c : instanceOf) {
            f.stringvalue("instanceOf", c.getName());
        }
        return f;
    }

    private static void registerCslPlugin(LayerBuilder b, String mimeType, TypeElement language, boolean useMultiview) throws LayerGenerationException {
        LayerBuilder.File f = b.folder("CslPlugins/" + mimeType);
        f.intvalue("genver", 2);
        f.write();
        f.boolvalue("useMultiview", useMultiview);
        f.write();
        f = LanguageRegistrationProcessor.instanceFile(b, "CslPlugins/" + mimeType, "language", (String)null, null, new Class[0]);
        f.stringvalue("instanceClass", language.getQualifiedName().toString());
        f.write();
    }

    private static void registerLoader(LayerBuilder b, String mimeType) throws LayerGenerationException {
        LayerBuilder.File f = LanguageRegistrationProcessor.instanceFile(b, "Loaders/" + mimeType + "/Factories", null, GsfDataLoader.class, null, new Class[0]);
        f.position(89998);
        f.write();
    }

    private static void registerPathRecognizer(LayerBuilder b, String mimeType) throws LayerGenerationException {
        LayerBuilder.File f = LanguageRegistrationProcessor.instanceFile(b, "Services/Hidden/PathRecognizers", "org-netbeans-modules-csl-core-PathRecognizerImpl-" + LanguageRegistrationProcessor.makeFilesystemName(mimeType), PathRecognizerImpl.class, "createInstance", PathRecognizer.class);
        f.stringvalue("mimeType", mimeType);
        f.write();
    }

    private static void registerParser(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, GsfParserFactory.class, "create", ParserFactory.class).write();
    }

    private static void registerIndexer(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, EmbeddingIndexerFactoryImpl.class, "create", EmbeddingIndexerFactory.class).write();
    }

    private static void registerTLIndexer(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, TLIndexerFactory.class, null, EmbeddingIndexerFactory.class).write();
    }

    private static void registerCodeCompletion(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/CompletionProviders", null, CodeTemplateCompletionProvider.class, null, new Class[0]).write();
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/CompletionProviders", null, GsfCompletionProvider.class, null, new Class[0]).write();
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/CompletionProviders", null, "org.netbeans.modules.parsing.ui.WaitScanFinishedCompletionProvider", null, new Class[0]).write();
    }

    private static void registerCodeFolding(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/FoldManager", null, GsfFoldManagerFactory.class, null, new Class[0]).intvalue("position", 900).write();
    }

    private static void registerCodeTemplates(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/CodeTemplateProcessorFactories", null, GsfCodeTemplateProcessor.Factory.class, null, new Class[0]).write();
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/CodeTemplateFilterFactories", null, GsfCodeTemplateFilter.Factory.class, null, new Class[0]).write();
    }

    private static void registerHyperlinks(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/HyperlinkProviders", null, GsfHyperlinkProvider.class, null, HyperlinkProviderExt.class).write();
    }

    private static void registerSemanticHighlighting(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, HighlightsLayerFactoryImpl.class, null, HighlightsLayerFactory.class).write();
    }

    private void registerStructureScanner(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Navigator/Panels/" + mimeType, null, ClassMemberPanel.class, null, new Class[0]).intvalue("position", 1000).write();
        LayerBuilder.File sideBar = LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/SideBar", null, "org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsController", "createSideBarFactory", new Class[0]);
        sideBar.stringvalue("location", "South").intvalue("position", 5238).boolvalue("scrollable", false).write();
    }

    private static void registerUpToDateStatus(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/UpToDateStatusProvider", null, GsfUpToDateStateProviderFactory.class, null, UpToDateStatusProviderFactory.class).write();
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/UpToDateStatusProvider", null, OccurrencesMarkProviderCreator.class, null, MarkProviderCreator.class).write();
    }

    private static void registerContextMenu(LayerBuilder b, String mimeType, Map<String, ExecutableElement> methods) {
        LayerBuilder.File f = b.folder("Editors/" + mimeType + "/Popup/goto");
        f.position(500);
        f.bundlevalue("displayName", "org.netbeans.modules.csl.core.Bundle", "generate-goto-popup");
        f.write();
        if (methods.containsKey("getDeclarationFinder")) {
            f = b.file("Editors/" + mimeType + "/Popup/goto/goto-declaration");
            f.position(500);
            f.write();
        }
        f = b.file("Editors/" + mimeType + "/Popup/goto/goto");
        f.position(600);
        f.write();
        f = LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/Popup", "SeparatorBeforeCut", JSeparator.class, null, new Class[0]);
        f.position(1200);
        f.write();
        f = b.file("Editors/" + mimeType + "/Popup/format");
        f.position(750);
        f.write();
        f = LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/Popup", "SeparatorAfterFormat", JSeparator.class, null, new Class[0]);
        f.position(780);
        f.write();
    }

    private static void registerCommentUncommentToolbarButtons(LayerBuilder b, String mimeType) {
        LayerBuilder.File f = LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType + "/Toolbars/Default", "Separator-before-comment", JSeparator.class, null, new Class[0]);
        f.position(30000);
        f.write();
        f = b.file("Editors/" + mimeType + "/Toolbars/Default/comment");
        f.position(30100);
        f.write();
        f = b.file("Editors/" + mimeType + "/Toolbars/Default/uncomment");
        f.position(30200);
        f.write();
    }

    private static void registerEditorKit(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, CslEditorKit.class, "createEditorKitInstance", EditorKit.class).write();
    }

    private static void registerLexer(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, CslEditorKit.class, "createLexerLanguageInstance", Language.class).write();
    }

    private static void registerFormatterIndenter(LayerBuilder b, String mimeType) {
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, GsfReformatTaskFactory.class, null, ReformatTask.Factory.class).write();
        LanguageRegistrationProcessor.instanceFile(b, "Editors/" + mimeType, null, GsfIndentTaskFactory.class, null, IndentTask.Factory.class).write();
    }

    private static String makeFilesystemName(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
                continue;
            }
            sb.append("-");
        }
        return sb.toString();
    }
}

