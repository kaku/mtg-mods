/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.ParserFactory
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.core;

import java.util.Collection;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.openide.filesystems.FileObject;

public final class GsfParserFactory
extends ParserFactory {
    private final String mimeType;

    public static ParserFactory create(FileObject f) {
        String mimeType = f.getParent().getPath().substring("Editors/".length());
        return new GsfParserFactory(mimeType);
    }

    public Parser createParser(Collection<Snapshot> snapshots) {
        for (Snapshot s : snapshots) {
            MimePath p = MimePath.get((String)s.getMimeType());
            String inhType = p.getInheritedType();
            if (p.getMimeType(0).equals(this.mimeType) || this.mimeType.equals(inhType)) continue;
            return null;
        }
        Language l = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
        assert (l != null);
        return l == null ? null : l.getParser(snapshots);
    }

    private GsfParserFactory(String mimeType) {
        this.mimeType = mimeType;
    }
}

