/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.ExtraLock
 *  org.netbeans.modules.editor.indent.spi.IndentTask
 */
package org.netbeans.modules.csl.core;

import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.IndentTask;

public class GsfIndentTask
implements IndentTask {
    private Context context;
    private Formatter formatter;

    GsfIndentTask(Context context) {
        this.context = context;
    }

    public void reindent() throws BadLocationException {
        Formatter f = null;
        BaseDocument baseDoc = (BaseDocument)this.context.document();
        List<Language> list = LanguageRegistry.getInstance().getEmbeddedLanguages(baseDoc, this.context.startOffset());
        if (this.context.endOffset() - this.context.startOffset() < 4) {
            for (Language l : list) {
                if (l.getFormatter() == null) continue;
                f = l.getFormatter();
                break;
            }
        }
        if (f == null) {
            f = this.getFormatter();
        }
        if (f != null) {
            f.reindent(this.context);
        }
    }

    public ExtraLock indentLock() {
        return null;
    }

    private synchronized Formatter getFormatter() {
        if (this.formatter == null) {
            MimePath mimePath = MimePath.parse((String)this.context.mimePath());
            String mimeType = mimePath.size() > 1 ? mimePath.getMimeType(mimePath.size() - 1) : mimePath.getPath();
            Language language = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
            this.formatter = language.getFormatter();
        }
        return this.formatter;
    }
}

