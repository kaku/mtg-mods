/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.ExtraLock
 *  org.netbeans.modules.editor.indent.spi.ReformatTask
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.impl.Utilities
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 */
package org.netbeans.modules.csl.core;

import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;

public class GsfReformatTask
implements ReformatTask {
    private static final Logger LOG = Logger.getLogger(GsfReformatTask.class.getName());
    private final Context context;
    private final Source source;
    private Formatter formatter;
    private String mimeType;

    GsfReformatTask(Source source, Context context) {
        this.context = context;
        this.source = source;
    }

    private synchronized Formatter getFormatter() {
        if (this.formatter == null) {
            MimePath mimePath = MimePath.parse((String)this.context.mimePath());
            this.mimeType = mimePath.size() > 1 ? mimePath.getMimeType(mimePath.size() - 1) : mimePath.getPath();
            Language language = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
            this.formatter = language.getFormatter();
        }
        return this.formatter;
    }

    public void reformat() throws BadLocationException {
        final Formatter f = this.getFormatter();
        if (f != null) {
            if (f.needsParserResult()) {
                try {
                    ParserManager.parse(Collections.singleton(this.source), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws ParseException {
                            MimePath mp = MimePath.get((String)resultIterator.getSnapshot().getMimeType());
                            String inh = mp.getInheritedType();
                            if (mp.getMimeType(0).equals(GsfReformatTask.this.mimeType) || GsfReformatTask.this.mimeType.equals(inh)) {
                                ParserResult parserResult = (ParserResult)resultIterator.getParserResult();
                                if (!(parserResult instanceof ParserResult)) {
                                    return;
                                }
                                f.reformat(GsfReformatTask.this.context, parserResult);
                            }
                            for (Embedding e : resultIterator.getEmbeddings()) {
                                this.run(resultIterator.getResultIterator(e));
                            }
                        }
                    });
                }
                catch (ParseException e) {
                    LOG.log(Level.WARNING, null, (Throwable)e);
                }
            } else {
                f.reformat(this.context, null);
            }
        }
    }

    public ExtraLock reformatLock() {
        Formatter f = this.getFormatter();
        if (f != null && f.needsParserResult()) {
            return new Lock();
        }
        return null;
    }

    private class Lock
    implements ExtraLock {
        private Lock() {
        }

        public void lock() {
            Utilities.acquireParserLock();
        }

        public void unlock() {
            Utilities.releaseParserLock();
        }
    }

}

