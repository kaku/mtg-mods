/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.ErrorManager
 *  org.openide.actions.OpenAction
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.csl.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JSeparator;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.core.GsfDataObject;
import org.netbeans.modules.csl.core.Language;
import org.openide.ErrorManager;
import org.openide.actions.OpenAction;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.actions.SystemAction;

public class GsfDataNode
extends DataNode {
    private static final Logger LOG = Logger.getLogger(GsfDataNode.class.getName());
    private static Map<String, Action[]> mimeTypeToActions = new HashMap<String, Action[]>();

    public GsfDataNode(GsfDataObject basDataObject, Language language) {
        super((DataObject)basDataObject, Children.LEAF);
        if (language != null && language.getIconBase() != null) {
            this.setIconBaseWithExtension(language.getIconBase());
        }
    }

    public Action getPreferredAction() {
        return SystemAction.get(OpenAction.class);
    }

    private void loadActions(List<Action> actions, DataFolder df) throws IOException, ClassNotFoundException {
        DataObject[] dob = df.getChildren();
        int k = dob.length;
        for (int i = 0; i < k; ++i) {
            InstanceCookie ic = (InstanceCookie)dob[i].getCookie(InstanceCookie.class);
            if (ic == null) {
                LOG.log(Level.WARNING, "Not an action instance, or broken action: {0}", (Object)dob[i].getPrimaryFile());
                continue;
            }
            Class clazz = ic.instanceClass();
            if (JSeparator.class.isAssignableFrom(clazz)) {
                actions.add(null);
                continue;
            }
            actions.add((Action)ic.instanceCreate());
        }
    }

    public Action[] getActions(boolean context) {
        String mimeType = this.getDataObject().getPrimaryFile().getMIMEType();
        if (!mimeTypeToActions.containsKey(mimeType)) {
            ArrayList<Action> actions = new ArrayList<Action>();
            try {
                String s;
                MimePath mp;
                FileObject fo = FileUtil.getConfigFile((String)("Loaders/" + mimeType + "/Actions"));
                if (fo != null) {
                    DataFolder df = DataFolder.findFolder((FileObject)fo);
                    this.loadActions(actions, df);
                }
                if ((s = (mp = MimePath.get((String)mimeType)).getInheritedType()) != null && !s.isEmpty() && (fo = FileUtil.getConfigFile((String)("Loaders/" + s + "/Actions"))) != null) {
                    DataFolder df = DataFolder.findFolder((FileObject)fo);
                    this.loadActions(actions, df);
                }
            }
            catch (ClassNotFoundException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
            }
            catch (IOException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
            }
            if (!actions.isEmpty()) {
                mimeTypeToActions.put(mimeType, actions.toArray(new Action[actions.size()]));
            } else {
                mimeTypeToActions.put(mimeType, super.getActions(context));
            }
        }
        return mimeTypeToActions.get(mimeType);
    }
}

