/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.Acceptor
 *  org.netbeans.modules.editor.settings.storage.spi.StorageFilter
 *  org.netbeans.modules.editor.settings.storage.spi.TypedValue
 */
package org.netbeans.modules.csl.core;

import java.util.Map;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.Acceptor;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.editor.settings.storage.spi.StorageFilter;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;

public class GsfEditorSettings
extends StorageFilter<String, TypedValue> {
    public static final Acceptor defaultAbbrevResetAcceptor = new Acceptor(){

        public final boolean accept(char ch) {
            return !Character.isJavaIdentifierPart(ch) && ch != ':' && ch != '-' && ch != '=' && ch != '#';
        }
    };

    public GsfEditorSettings() {
        super("Preferences");
    }

    public void afterLoad(Map<String, TypedValue> map, MimePath mimePath, String profile, boolean defaults) {
        if (mimePath.size() == 1 && null != LanguageRegistry.getInstance().getLanguageByMimeType(mimePath.getPath())) {
            if (!map.containsKey("word-match-match-case")) {
                map.put("word-match-match-case", new TypedValue("true", Boolean.class.getName()));
            }
            if (!map.containsKey("reindent-with-text-before")) {
                map.put("reindent-with-text-before", new TypedValue("false", Boolean.class.getName()));
            }
            if (!map.containsKey("abbrev-reset-acceptor")) {
                map.put("abbrev-reset-acceptor", new TypedValue(this.getClass().getName() + ".getAbbrevResetAcceptor", "methodvalue"));
            }
        }
    }

    public void beforeSave(Map<String, TypedValue> map, MimePath mimePath, String profile, boolean defaults) {
    }

    public static Acceptor getAbbrevResetAcceptor(MimePath mimePath, String settingName) {
        return defaultAbbrevResetAcceptor;
    }

}

