/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache$Convertor
 *  org.netbeans.modules.parsing.spi.indexing.ErrorsCache$ErrorKind
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.spi.tasklist.TaskScanningScope
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.core;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.csl.core.ErrorFilterQuery;
import org.netbeans.modules.csl.core.TasklistStateBackdoor;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.ErrorsCache;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;

public final class TLIndexerFactory
extends EmbeddingIndexerFactory {
    private static final Logger LOG = Logger.getLogger(TLIndexerFactory.class.getName());
    public static final String INDEXER_NAME = "TLIndexer";
    public static final int INDEXER_VERSION = 5;
    public static final String FIELD_GROUP_NAME = "groupName";
    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_LINE_NUMBER = "lineNumber";
    public static final String FEATURE_ERROR_BADGES = "errorBadges";
    private static final Map<Indexable, Collection<SimpleError>> errors = new IdentityHashMap<Indexable, Collection<SimpleError>>();
    private static final Map<Indexable, List<Integer>> lineStartOffsetsCache = new IdentityHashMap<Indexable, List<Integer>>();
    private static final ErrorConvertorImpl DUMMY = new ErrorConvertorImpl(Collections.<Integer>emptyList());

    public boolean scanStarted(Context context) {
        return true;
    }

    public void scanFinished(Context context) {
        if (!context.checkForEditorModifications()) {
            TLIndexerFactory.commitErrors(context.getRootURI(), errors, lineStartOffsetsCache);
        }
    }

    private static void commitErrors(URL root, Map<Indexable, Collection<SimpleError>> errors, Map<Indexable, List<Integer>> lineStartOffsetsCache) {
        for (Map.Entry<Indexable, Collection<SimpleError>> e : errors.entrySet()) {
            ErrorsCache.setErrors((URL)root, (Indexable)e.getKey(), (Iterable)e.getValue(), (ErrorsCache.Convertor)new ErrorConvertorImpl(lineStartOffsetsCache.get((Object)e.getKey())));
        }
        errors.clear();
        lineStartOffsetsCache.clear();
    }

    public synchronized EmbeddingIndexer createIndexer(Indexable indexable, Snapshot snapshot) {
        assert (errors != null);
        assert (lineStartOffsetsCache != null);
        return new TLIndexer(errors, lineStartOffsetsCache);
    }

    public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
        for (Indexable indexable : deleted) {
            ErrorsCache.setErrors((URL)context.getRootURI(), (Indexable)indexable, Collections.emptyList(), (ErrorsCache.Convertor)DUMMY);
        }
    }

    public void rootsRemoved(Iterable<? extends URL> removedRoots) {
    }

    public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
    }

    public String getIndexerName() {
        return "TLIndexer";
    }

    public int getIndexVersion() {
        return 5;
    }

    private static SimpleError simplify(Error error, int pos) {
        return new SimpleError(error.getDisplayName(), error.getDescription(), pos == -1 ? error.getStartPosition() : pos, error.getSeverity(), error instanceof Error.Badging && ((Error.Badging)error).showExplorerBadge());
    }

    private static class SimpleError {
        private String displayName;
        private String description;
        private int startPosition;
        private Severity severity;
        private boolean isBadging;

        public SimpleError(String displayName, String description, int startPosition, Severity severity, boolean isBadging) {
            this.displayName = displayName;
            this.description = description;
            this.startPosition = startPosition;
            this.severity = severity;
            this.isBadging = isBadging;
        }

        public String getDescription() {
            return this.description;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public Severity getSeverity() {
            return this.severity;
        }

        public int getStartPosition() {
            return this.startPosition;
        }

        public boolean isBadging() {
            return this.isBadging;
        }
    }

    private static final class TLIndexer
    extends EmbeddingIndexer {
        private final Map<Indexable, Collection<SimpleError>> errors;
        private final Map<Indexable, List<Integer>> lineStartOffsetsCache;

        public TLIndexer(Map<Indexable, Collection<SimpleError>> errors, Map<Indexable, List<Integer>> lineStartOffsetsCache) {
            this.errors = errors;
            this.lineStartOffsetsCache = lineStartOffsetsCache;
        }

        protected void index(Indexable indexable, Parser.Result parserResult, Context context) {
            Collection<SimpleError> storedErrors;
            List<? extends Error> e;
            boolean process = true;
            if (context.checkForEditorModifications()) {
                return;
            }
            FileObject root = context.getRoot();
            boolean allTasks = TasklistStateBackdoor.getInstance().isObserved();
            if (allTasks && root != null) {
                FileObject indexedFile = root.getFileObject(indexable.getRelativePath());
                process = TasklistStateBackdoor.getInstance().isCurrentEditorScope() ? TasklistStateBackdoor.getInstance().getScope().isInScope(indexedFile) : true;
            }
            ParserResult gsfParserResult = (ParserResult)parserResult;
            if (!this.errors.containsKey((Object)indexable)) {
                TLIndexerFactory.commitErrors(context.getRootURI(), this.errors, this.lineStartOffsetsCache);
            }
            if ((storedErrors = this.errors.get((Object)indexable)) == null) {
                storedErrors = new ArrayList<SimpleError>();
                this.errors.put(indexable, storedErrors);
            }
            if (this.errors == null) {
                return;
            }
            List<Integer> lineStartOffsets = this.lineStartOffsetsCache.get((Object)indexable);
            if (lineStartOffsets == null) {
                this.lineStartOffsetsCache.put(indexable, TLIndexer.getLineStartOffsets(gsfParserResult.getSnapshot().getSource()));
            }
            List<? extends Error> filteredErrors = null;
            if (allTasks && process && (e = ErrorFilterQuery.getFilteredErrors(gsfParserResult, "tasklist")) != null) {
                filteredErrors = e;
            }
            List<? extends Error> lst = ErrorFilterQuery.getFilteredErrors(gsfParserResult, "errorBadges");
            ArrayList<SimpleError> simplifiedErrors = new ArrayList<SimpleError>();
            HashSet<String> seenErrorKeys = new HashSet<String>();
            if (lst != null) {
                if (filteredErrors == null) {
                    filteredErrors = new ArrayList<Error>(lst.size());
                }
                filteredErrors.addAll(lst);
            } else if (filteredErrors == null) {
                filteredErrors = new ArrayList<Error>(gsfParserResult.getDiagnostics());
            }
            for (Error err : filteredErrors) {
                int startPos = err.getStartPosition();
                startPos = gsfParserResult.getSnapshot().getOriginalOffset(startPos);
                String ek = Integer.toString(startPos) + ":" + err.getKey();
                if (!seenErrorKeys.add(ek)) continue;
                simplifiedErrors.add(TLIndexerFactory.simplify(err, startPos));
            }
            storedErrors.addAll(simplifiedErrors);
        }

        private static List<Integer> getLineStartOffsets(Source source) {
            ArrayList<Integer> lineStartOffsets = new ArrayList<Integer>();
            lineStartOffsets.add(0);
            CharSequence text = source.createSnapshot().getText();
            for (int i = 0; i < text.length(); ++i) {
                if (text.charAt(i) != '\n') continue;
                lineStartOffsets.add(i + 1);
            }
            return lineStartOffsets;
        }
    }

    private static final class ErrorConvertorImpl
    implements ErrorsCache.Convertor<SimpleError> {
        private final List<Integer> lineStartOffsets;

        public ErrorConvertorImpl(List<Integer> lineStartOffsets) {
            this.lineStartOffsets = lineStartOffsets;
        }

        public ErrorsCache.ErrorKind getKind(SimpleError error) {
            if (error.getSeverity() == Severity.WARNING) {
                return ErrorsCache.ErrorKind.WARNING;
            }
            if (error.isBadging()) {
                return ErrorsCache.ErrorKind.ERROR;
            }
            return ErrorsCache.ErrorKind.ERROR_NO_BADGE;
        }

        public int getLineNumber(SimpleError error) {
            int originalOffset = error.getStartPosition();
            int lineNumber = 1;
            if (originalOffset >= 0) {
                int idx = Collections.binarySearch(this.lineStartOffsets, originalOffset);
                if (idx < 0) {
                    int ln = - idx - 1;
                    assert (ln >= 1 && ln <= this.lineStartOffsets.size());
                    if (ln >= 1 && ln <= this.lineStartOffsets.size()) {
                        lineNumber = ln;
                    }
                } else {
                    lineNumber = idx + 1;
                }
            }
            return lineNumber;
        }

        public String getMessage(SimpleError error) {
            return error.getDisplayName();
        }
    }

}

