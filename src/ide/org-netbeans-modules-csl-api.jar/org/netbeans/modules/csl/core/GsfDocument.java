/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.NbEditorDocument
 */
package org.netbeans.modules.csl.core;

import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.editor.NbEditorDocument;

public class GsfDocument
extends NbEditorDocument {
    private GsfLanguage language = null;

    public GsfDocument(String mimeType) {
        super(mimeType);
    }

    public boolean isIdentifierPart(char ch) {
        Language l;
        if (this.language == null && (l = LanguageRegistry.getInstance().getLanguageByMimeType((String)this.getProperty((Object)"mimeType"))) != null) {
            this.language = l.getGsfLanguage();
        }
        return this.language != null ? this.language.isIdentifierChar(ch) : super.isIdentifierPart(ch);
    }
}

