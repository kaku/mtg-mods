/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 */
package org.netbeans.modules.csl.core;

import java.util.Collection;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;

public abstract class AbstractTaskFactory
extends TaskFactory {
    private final boolean topLevelLanguageOnly;

    public final Collection<? extends SchedulerTask> create(Snapshot snapshot) {
        String mimeType = snapshot.getMimeType();
        Language l = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
        if (l == null) {
            return null;
        }
        if (!this.topLevelLanguageOnly || AbstractTaskFactory.isTopLevel(snapshot)) {
            return this.createTasks(l, snapshot);
        }
        return null;
    }

    protected abstract Collection<? extends SchedulerTask> createTasks(Language var1, Snapshot var2);

    protected AbstractTaskFactory(boolean topLevelLanguageOnly) {
        this.topLevelLanguageOnly = topLevelLanguageOnly;
    }

    private static boolean isTopLevel(Snapshot snapshot) {
        return snapshot.getSource().getMimeType().equals(snapshot.getMimeType());
    }
}

