/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.csl.core;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public final class EmbeddingIndexerFactoryImpl
extends EmbeddingIndexerFactory {
    private static final Logger LOG = Logger.getLogger(EmbeddingIndexerFactoryImpl.class.getName());
    private static final EmbeddingIndexerFactory VOID_INDEXER_FACTORY = new EmbeddingIndexerFactory(){
        private final EmbeddingIndexer voidIndexer;

        public EmbeddingIndexer createIndexer(Indexable indexable, Snapshot snapshot) {
            return this.voidIndexer;
        }

        public String getIndexerName() {
            return "void-indexer";
        }

        public int getIndexVersion() {
            return 0;
        }

        public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
        }

        public void rootsRemoved(Iterable<? extends URL> removedRoots) {
        }

        public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
        }

    };
    private final String mimeType;
    private EmbeddingIndexerFactory realFactory;

    public boolean scanStarted(Context context) {
        if (!this.getFactory().scanStarted(context)) {
            return false;
        }
        return EmbeddingIndexerFactoryImpl.verifyIndex(context);
    }

    public void scanFinished(Context context) {
        this.getFactory().scanFinished(context);
    }

    public EmbeddingIndexer createIndexer(Indexable indexable, Snapshot snapshot) {
        return this.getFactory().createIndexer(indexable, snapshot);
    }

    public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
        this.getFactory().filesDeleted(deleted, context);
    }

    public void rootsRemoved(Iterable<? extends URL> removedRoots) {
        assert (removedRoots != null);
        this.getFactory().rootsRemoved(removedRoots);
    }

    public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
        this.getFactory().filesDirty(dirty, context);
    }

    public String getIndexerName() {
        return this.getFactory().getIndexerName();
    }

    public int getIndexVersion() {
        return this.getFactory().getIndexVersion();
    }

    public String toString() {
        return String.format("%s[%s]", new Object[]{this.getClass().getName(), this.getFactory()});
    }

    public static EmbeddingIndexerFactory create(FileObject fileObject) {
        String mimeType = fileObject.getParent().getPath().substring("Editors/".length());
        return new EmbeddingIndexerFactoryImpl(mimeType);
    }

    private static boolean verifyIndex(Context context) {
        try {
            return IndexingSupport.getInstance((Context)context).isValid();
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
            return false;
        }
    }

    private EmbeddingIndexerFactoryImpl(String mimeType) {
        this.mimeType = mimeType;
    }

    private EmbeddingIndexerFactory getFactory() {
        if (this.realFactory == null) {
            EmbeddingIndexerFactory factory;
            Language language = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
            if (language != null && (factory = language.getIndexerFactory()) != null) {
                this.realFactory = factory;
            }
            if (this.realFactory == null) {
                this.realFactory = VOID_INDEXER_FACTORY;
            }
            LOG.fine("EmbeddingIndexerFactory for '" + this.mimeType + "': " + (Object)this.realFactory);
        }
        return this.realFactory;
    }

}

