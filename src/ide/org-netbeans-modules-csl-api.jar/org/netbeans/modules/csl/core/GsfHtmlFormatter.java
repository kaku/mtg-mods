/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.core;

import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;

public class GsfHtmlFormatter
extends HtmlFormatter {
    protected boolean isDeprecated;
    protected boolean isParameter;
    protected boolean isType;
    protected boolean isName;
    protected boolean isEmphasis;
    protected StringBuilder sb = new StringBuilder();

    @Override
    public void reset() {
        this.textLength = 0;
        this.sb.setLength(0);
    }

    @Override
    public void appendHtml(String html) {
        this.sb.append(html);
    }

    @Override
    public void appendText(String text, int fromInclusive, int toExclusive) {
        for (int i = fromInclusive; i < toExclusive; ++i) {
            if (this.textLength >= this.maxLength) {
                if (this.textLength != this.maxLength) break;
                this.sb.append("...");
                this.textLength += 3;
                break;
            }
            char c = text.charAt(i);
            switch (c) {
                case '<': {
                    this.sb.append("&lt;");
                    break;
                }
                case '>': {
                    if (i > 1 && text.charAt(i - 2) == ']' && text.charAt(i - 1) == ']') {
                        this.sb.append("&gt;");
                        break;
                    }
                    this.sb.append(c);
                    break;
                }
                case '&': {
                    this.sb.append("&amp;");
                    break;
                }
                default: {
                    this.sb.append(c);
                }
            }
            ++this.textLength;
        }
    }

    @Override
    public void name(ElementKind kind, boolean start) {
        assert (start != this.isName);
        this.isName = start;
        if (this.isName) {
            this.sb.append("<b>");
        } else {
            this.sb.append("</b>");
        }
    }

    @Override
    public void parameters(boolean start) {
        assert (start != this.isParameter);
        this.isParameter = start;
        if (this.isParameter) {
            this.sb.append("<font color=\"#808080\">");
        } else {
            this.sb.append("</font>");
        }
    }

    @Override
    public void active(boolean start) {
        this.emphasis(start);
    }

    @Override
    public void type(boolean start) {
        assert (start != this.isType);
        this.isType = start;
        if (this.isType) {
            this.sb.append("<font color=\"#808080\">");
        } else {
            this.sb.append("</font>");
        }
    }

    @Override
    public void deprecated(boolean start) {
        assert (start != this.isDeprecated);
        this.isDeprecated = start;
        if (this.isDeprecated) {
            this.sb.append("<s>");
        } else {
            this.sb.append("</s>");
        }
    }

    @Override
    public String getText() {
        assert (!(this.isParameter || this.isDeprecated || this.isName || this.isType));
        return this.sb.toString();
    }

    @Override
    public void emphasis(boolean start) {
        assert (start != this.isEmphasis);
        this.isEmphasis = start;
        if (this.isEmphasis) {
            this.sb.append("<b>");
        } else {
            this.sb.append("</b>");
        }
    }
}

