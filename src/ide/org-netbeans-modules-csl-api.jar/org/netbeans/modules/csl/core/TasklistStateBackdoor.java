/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.spi.tasklist.PushTaskScanner
 *  org.netbeans.spi.tasklist.PushTaskScanner$Callback
 *  org.netbeans.spi.tasklist.TaskScanningScope
 */
package org.netbeans.modules.csl.core;

import org.netbeans.modules.csl.core.Bundle;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.TaskScanningScope;

class TasklistStateBackdoor
extends PushTaskScanner {
    private static final TasklistStateBackdoor INSTANCE = new TasklistStateBackdoor();
    private volatile TaskScanningScope scope;
    private volatile PushTaskScanner.Callback callback;
    private volatile boolean seenByTlIndexer = true;
    private boolean wasScanning;

    TasklistStateBackdoor() {
        super(Bundle.DN_tlIndexerName(), Bundle.DESC_tlIndexerName(), null);
    }

    static TasklistStateBackdoor getInstance() {
        return INSTANCE;
    }

    boolean isCurrentEditorScope() {
        PushTaskScanner.Callback c = this.callback;
        this.seenByTlIndexer = true;
        return c != null && c.isCurrentEditorScope();
    }

    boolean isObserved() {
        PushTaskScanner.Callback c = this.callback;
        this.seenByTlIndexer = true;
        return c != null && c.isObserved();
    }

    TaskScanningScope getScope() {
        return this.scope;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setScope(TaskScanningScope scope, PushTaskScanner.Callback callback) {
        this.callback = callback;
        if (scope == null) {
            return;
        }
        TasklistStateBackdoor tasklistStateBackdoor = this;
        synchronized (tasklistStateBackdoor) {
            boolean newScanning;
            boolean bl = newScanning = !callback.isCurrentEditorScope();
            if (!callback.isObserved()) {
                scope = null;
                newScanning = false;
            }
            this.scope = scope;
            if (!callback.isObserved() || callback.isCurrentEditorScope() || !newScanning || this.wasScanning == newScanning || !this.seenByTlIndexer) {
                this.wasScanning = newScanning;
                this.seenByTlIndexer = false;
                return;
            }
            this.wasScanning = newScanning;
            this.seenByTlIndexer = false;
            IndexingManager.getDefault().refreshAllIndices("TLIndexer");
        }
    }
}

