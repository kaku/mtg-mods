/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.ExtensionList
 *  org.openide.loaders.FileEntry
 *  org.openide.loaders.FileEntry$Format
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.loaders.UniFileLoader
 *  org.openide.util.MapFormat
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.core;

import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.csl.core.GsfDataObject;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.ExtensionList;
import org.openide.loaders.FileEntry;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.UniFileLoader;
import org.openide.util.MapFormat;
import org.openide.util.NbBundle;

public class GsfDataLoader
extends UniFileLoader {
    boolean initialized;
    volatile Set<String> registeredMimes = Collections.emptySet();

    public GsfDataLoader() {
        super("org.netbeans.modules.csl.core.GsfDataObject");
        this.initExtensions();
    }

    final void initExtensions() {
        ExtensionList list = new ExtensionList();
        HashSet<String> mimes = new HashSet<String>();
        for (Language language : LanguageRegistry.getInstance()) {
            if (language.useCustomEditorKit()) continue;
            mimes.add(language.getMimeType());
            list.addMimeType(language.getMimeType());
        }
        this.setExtensions(list);
        this.registeredMimes = mimes;
        this.initialized = true;
    }

    protected FileObject findPrimaryFile(FileObject fo) {
        FileObject pf = super.findPrimaryFile(fo);
        if (pf != null) {
            return pf;
        }
        String mime = fo.getMIMEType();
        int slash = -1;
        int l = mime.length();
        for (int i = 0; i < l; ++i) {
            char c = mime.charAt(i);
            if (c == '/') {
                slash = i;
                continue;
            }
            if (c != '+') continue;
            if (slash == -1) {
                return null;
            }
            String baseMime = mime.substring(0, slash + 1) + mime.substring(i + 1);
            if (!this.registeredMimes.contains(baseMime)) continue;
            return fo;
        }
        return null;
    }

    protected MultiDataObject createMultiObject(FileObject primaryFile) throws DataObjectExistsException, IOException {
        Language language = LanguageRegistry.getInstance().getLanguageByMimeType(primaryFile.getMIMEType());
        return new GsfDataObject(primaryFile, (MultiFileLoader)this, language);
    }

    protected String defaultDisplayName() {
        StringBuilder sb = new StringBuilder();
        for (Language language : LanguageRegistry.getInstance()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(language.getDisplayName());
        }
        return NbBundle.getMessage(GsfDataLoader.class, (String)"GenericLoaderName", (Object)sb.toString());
    }

    protected MultiDataObject.Entry createPrimaryEntry(MultiDataObject obj, FileObject primaryFile) {
        FileEntry.Format entry = new FileEntry.Format(obj, primaryFile){

            protected Format createFormat(FileObject target, String n, String e) {
                String capitalizedPkgName;
                ClassPath cp = ClassPath.getClassPath((FileObject)target, (String)"classpath/source");
                String resourcePath = "";
                if (cp != null) {
                    resourcePath = cp.getResourceName(target);
                    if (resourcePath == null) {
                        resourcePath = "";
                    }
                } else {
                    ErrorManager.getDefault().log(16, "No classpath was found for folder: " + (Object)target);
                }
                HashMap<String, String> m = new HashMap<String, String>();
                m.put("NAME", n);
                String capitalizedName = n.length() > 1 ? "" + Character.toUpperCase(n.charAt(0)) + n.substring(1) : (n.length() == 1 ? "" + Character.toUpperCase(n.charAt(0)) : "");
                m.put("CAPITALIZEDNAME", capitalizedName);
                m.put("LOWERNAME", n.toLowerCase());
                m.put("UPPERNAME", n.toUpperCase());
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < n.length(); ++i) {
                    char c = n.charAt(i);
                    if (!Character.isJavaIdentifierPart(c)) continue;
                    sb.append(c);
                }
                String identifier = sb.toString();
                m.put("IDENTIFIER", identifier);
                sb.setCharAt(0, Character.toUpperCase(identifier.charAt(0)));
                m.put("CAPITALIZEDIDENTIFIER", sb.toString());
                m.put("LOWERIDENTIFIER", identifier.toLowerCase());
                String packageName = resourcePath.replace('/', '.');
                m.put("PACKAGE", packageName);
                if (packageName == null || packageName.length() == 0) {
                    packageName = "";
                    capitalizedPkgName = "";
                } else {
                    capitalizedPkgName = packageName.length() > 1 ? "" + Character.toUpperCase(packageName.charAt(0)) + packageName.substring(1) : "" + Character.toUpperCase(packageName.charAt(0));
                }
                m.put("CAPITALIZEDPACKAGE", capitalizedPkgName);
                m.put("PACKAGE_SLASHES", resourcePath);
                if (target.isRoot()) {
                    m.put("PACKAGE_AND_NAME", n);
                    m.put("PACKAGE_AND_NAME_SLASHES", n);
                } else {
                    m.put("PACKAGE_AND_NAME", resourcePath.replace('/', '.') + '.' + n);
                    m.put("PACKAGE_AND_NAME_SLASHES", resourcePath + '/' + n);
                }
                m.put("DATE", DateFormat.getDateInstance(1).format(new Date()));
                m.put("TIME", DateFormat.getTimeInstance(3).format(new Date()));
                MapFormat f = new MapFormat(m);
                f.setLeftBrace("__");
                f.setRightBrace("__");
                f.setExactMatch(false);
                return f;
            }
        };
        return entry;
    }

}

