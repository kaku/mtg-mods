/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.spi.java.classpath.ClassPathProvider
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.csl.core;

import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.spi.java.classpath.ClassPathProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class ProjectClassPathProvider
implements ClassPathProvider {
    public ClassPath findClassPath(FileObject file, String type) {
        Project p = FileOwnerQuery.getOwner((FileObject)file);
        if (p != null) {
            ClassPathProvider cpp = (ClassPathProvider)p.getLookup().lookup(ClassPathProvider.class);
            if (cpp != null) {
                return cpp.findClassPath(file, type);
            }
            return null;
        }
        return null;
    }
}

