/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor$MutableContext
 */
package org.netbeans.modules.csl.core;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor;

public class CslCamelCaseInterceptor
implements CamelCaseInterceptor {
    public boolean beforeChange(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
        return false;
    }

    public void change(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
        int offset = context.getOffset();
        Document doc = context.getDocument();
        KeystrokeHandler bc = UiUtils.getBracketCompletion(doc, offset);
        if (bc != null) {
            int nextOffset = bc.getNextWordOffset(doc, offset, context.isBackward());
            context.setNextWordOffset(nextOffset);
        }
    }

    public void afterChange(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
    }

    public void cancelled(CamelCaseInterceptor.MutableContext context) {
    }

    public static class Factory
    implements CamelCaseInterceptor.Factory {
        public CamelCaseInterceptor createCamelCaseInterceptor(MimePath mimePath) {
            return new CslCamelCaseInterceptor();
        }
    }

}

