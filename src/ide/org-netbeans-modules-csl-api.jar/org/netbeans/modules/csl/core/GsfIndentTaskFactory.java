/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.IndentTask
 *  org.netbeans.modules.editor.indent.spi.IndentTask$Factory
 */
package org.netbeans.modules.csl.core;

import org.netbeans.modules.csl.core.GsfIndentTask;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.IndentTask;

public class GsfIndentTaskFactory
implements IndentTask.Factory {
    public IndentTask createTask(Context context) {
        return new GsfIndentTask(context);
    }
}

