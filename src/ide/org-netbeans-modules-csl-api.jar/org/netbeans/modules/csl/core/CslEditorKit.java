/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.BaseKit$InsertBreakAction
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtKit
 *  org.netbeans.editor.ext.ExtKit$ExtDefaultKeyTypedAction
 *  org.netbeans.editor.ext.ExtKit$ExtDeleteCharAction
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager
 *  org.netbeans.modules.editor.NbEditorKit
 *  org.netbeans.modules.editor.NbEditorKit$GenerateFoldPopupAction
 *  org.netbeans.modules.editor.NbEditorKit$NbGenerateGoToPopupAction
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtKit;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager;
import org.netbeans.modules.csl.api.CslActions;
import org.netbeans.modules.csl.api.GoToMarkOccurrencesAction;
import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.api.InstantRenameAction;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.SelectCodeElementAction;
import org.netbeans.modules.csl.api.ToggleBlockCommentAction;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.csl.core.GsfDocument;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.editor.NbEditorKit;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public final class CslEditorKit
extends NbEditorKit {
    private static final Logger LOG = Logger.getLogger(CslEditorKit.class.getName());
    private boolean cloned;
    private volatile String mimeType;

    public static EditorKit createEditorKitInstance(FileObject f) {
        String mimeType = CslEditorKit.detectMimeType(f);
        return mimeType != null ? new CslEditorKit(mimeType) : null;
    }

    public static org.netbeans.api.lexer.Language createLexerLanguageInstance(FileObject f) {
        Language l;
        String mimeType = CslEditorKit.detectMimeType(f);
        if (mimeType != null && (l = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType)) != null) {
            return l.getGsfLanguage().getLexerLanguage();
        }
        return null;
    }

    public CslEditorKit(String mimeType) {
        this.mimeType = mimeType;
    }

    public Object clone() {
        Object o = super.clone();
        ((CslEditorKit)o).cloned = true;
        return o;
    }

    public void applyContentType(String mimeType) {
        if (!this.cloned) {
            return;
        }
        EditorKit ek = (EditorKit)MimeLookup.getLookup((String)mimeType).lookup(EditorKit.class);
        if (ek == null || ek.getClass() != this.getClass()) {
            return;
        }
        this.mimeType = mimeType;
    }

    public String getContentType() {
        return this.mimeType;
    }

    public Document createDefaultDocument() {
        return new GsfDocument(this.mimeType);
    }

    public SyntaxSupport createSyntaxSupport(final BaseDocument doc) {
        return new ExtSyntaxSupport(doc){

            public int[] findMatchingBlock(int offset, boolean simpleSearch) throws BadLocationException {
                KeystrokeHandler bracketCompletion = UiUtils.getBracketCompletion((Document)doc, offset);
                if (bracketCompletion != null) {
                    OffsetRange range = bracketCompletion.findMatching((Document)this.getDocument(), offset);
                    if (range == OffsetRange.NONE) {
                        return null;
                    }
                    return new int[]{range.getStart(), range.getEnd()};
                }
                return null;
            }
        };
    }

    protected void initDocument(BaseDocument doc) {
        CodeTemplateManager.get((Document)doc);
    }

    protected Action[] createActions() {
        Action[] superActions = super.createActions();
        Language language = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
        ArrayList<Object> actions = new ArrayList<Object>(30);
        actions.add((Object)new GsfDefaultKeyTypedAction());
        actions.add((Object)new GsfInsertBreakAction());
        actions.add((Object)new GsfDeleteCharAction("delete-previous", false));
        if (!this.mimeType.equals("text/x-php5")) {
            actions.add((Object)new ToggleBlockCommentAction());
        }
        actions.add((Object)new NbEditorKit.GenerateFoldPopupAction());
        actions.add((Object)new InstantRenameAction());
        actions.add(CslActions.createGoToDeclarationAction());
        actions.add((Object)new GenericGenerateGoToPopupAction());
        actions.add((Object)new SelectCodeElementAction("select-element-next", true));
        actions.add((Object)new SelectCodeElementAction("select-element-previous", false));
        if (language == null) {
            LOG.log(Level.WARNING, "Language missing for MIME type {0}", this.mimeType);
        } else if (language.hasOccurrencesFinder()) {
            actions.add((Object)new GoToMarkOccurrencesAction(false));
            actions.add((Object)new GoToMarkOccurrencesAction(true));
        }
        return TextAction.augmentList(superActions, actions.toArray(new Action[actions.size()]));
    }

    private static String detectMimeType(FileObject f) {
        String mimeType = f.getParent().getPath().substring("Editors/".length());
        return MimePath.validate((CharSequence)mimeType) && mimeType.length() > 0 ? mimeType : null;
    }

    private static Action findAction(Action[] actions, String name) {
        for (Action a : actions) {
            Object nameObj = a.getValue("Name");
            if (!(nameObj instanceof String) || !name.equals(nameObj)) continue;
            return a;
        }
        return null;
    }

    private static boolean completionSettingEnabled() {
        return true;
    }

    private final class GenericGenerateGoToPopupAction
    extends NbEditorKit.NbGenerateGoToPopupAction {
        private GenericGenerateGoToPopupAction() {
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }

        private void addAcceleretors(Action a, JMenuItem item, JTextComponent target) {
            Keymap km = target.getKeymap();
            if (km != null) {
                KeyStroke ks;
                KeyStroke[] keys = km.getKeyStrokesForAction(a);
                if (keys != null && keys.length > 0) {
                    item.setAccelerator(keys[0]);
                } else if (a != null && (ks = (KeyStroke)a.getValue("AcceleratorKey")) != null) {
                    item.setAccelerator(ks);
                }
            }
        }

        private void addAction(JTextComponent target, JMenu menu, Action a) {
            if (a != null) {
                String actionName = (String)a.getValue("Name");
                JMenuItem item = null;
                if (a instanceof BaseAction) {
                    item = ((BaseAction)a).getPopupMenuItem(target);
                }
                if (item == null) {
                    String itemText = (String)a.getValue("trimmed-text");
                    if (itemText == null) {
                        itemText = this.getItemText(target, actionName, a);
                    }
                    if (itemText != null) {
                        item = new JMenuItem(itemText);
                        Mnemonics.setLocalizedText((AbstractButton)item, (String)itemText);
                        item.addActionListener(a);
                        this.addAcceleretors(a, item, target);
                        item.setEnabled(a.isEnabled());
                        Object helpID = a.getValue("helpID");
                        if (helpID != null && helpID instanceof String) {
                            item.putClientProperty("HelpID", helpID);
                        }
                    } else if ("goto-source".equals(actionName)) {
                        item = new JMenuItem(NbBundle.getBundle(CslEditorKit.class).getString("goto_source_open_source_not_formatted"));
                        this.addAcceleretors(a, item, target);
                        item.setEnabled(false);
                    }
                }
                if (item != null) {
                    menu.add(item);
                }
            }
        }

        private void addAction(JTextComponent target, JMenu menu, String actionName) {
            BaseKit kit = Utilities.getKit((JTextComponent)target);
            if (kit == null) {
                return;
            }
            Action a = kit.getActionByName(actionName);
            if (a != null) {
                this.addAction(target, menu, a);
            } else {
                menu.addSeparator();
            }
        }

        private String getItemText(JTextComponent target, String actionName, Action a) {
            String itemText = a instanceof BaseAction ? ((BaseAction)a).getPopupMenuText(target) : actionName;
            return itemText;
        }

        public JMenuItem getPopupMenuItem(JTextComponent target) {
            String menuText = NbBundle.getBundle(CslEditorKit.class).getString("generate-goto-popup");
            JMenu jm = new JMenu(menuText);
            this.addAction(target, jm, "goto-declaration");
            return jm;
        }
    }

    private final class GsfDeleteCharAction
    extends ExtKit.ExtDeleteCharAction {
        private JTextComponent currentTarget;

        public GsfDeleteCharAction(String nm, boolean nextChar) {
            super(nm, nextChar);
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            this.currentTarget = target;
            super.actionPerformed(evt, target);
            this.currentTarget = null;
        }

        protected void charBackspaced(BaseDocument doc, int dotPos, Caret caret, char ch) throws BadLocationException {
            KeystrokeHandler bracketCompletion;
            if (CslEditorKit.completionSettingEnabled() && (bracketCompletion = UiUtils.getBracketCompletion((Document)doc, dotPos)) != null) {
                boolean success = bracketCompletion.charBackspaced((Document)doc, dotPos, this.currentTarget, ch);
                return;
            }
            super.charBackspaced(doc, dotPos, caret, ch);
        }
    }

    private final class GsfInsertBreakAction
    extends BaseKit.InsertBreakAction {
        static final long serialVersionUID = -1506173310438326380L;

        private GsfInsertBreakAction() {
        }

        protected Object beforeBreak(JTextComponent target, BaseDocument doc, Caret caret) {
            KeystrokeHandler bracketCompletion;
            if (CslEditorKit.completionSettingEnabled() && (bracketCompletion = UiUtils.getBracketCompletion((Document)doc, caret.getDot())) != null) {
                try {
                    int newOffset = bracketCompletion.beforeBreak((Document)doc, caret.getDot(), target);
                    if (newOffset >= 0) {
                        return new Integer(newOffset);
                    }
                }
                catch (BadLocationException ble) {
                    Exceptions.printStackTrace((Throwable)ble);
                }
            }
            return null;
        }

        protected void afterBreak(JTextComponent target, BaseDocument doc, Caret caret, Object cookie) {
            if (CslEditorKit.completionSettingEnabled() && cookie != null && cookie instanceof Integer) {
                int dotPos = (Integer)cookie;
                if (dotPos != -1) {
                    caret.setDot(dotPos);
                } else {
                    int nowDotPos = caret.getDot();
                    caret.setDot(nowDotPos + 1);
                }
            }
        }
    }

    private final class GsfDefaultKeyTypedAction
    extends ExtKit.ExtDefaultKeyTypedAction {
        private JTextComponent currentTarget;
        private String replacedText;

        private GsfDefaultKeyTypedAction() {
            this.replacedText = null;
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            this.currentTarget = target;
            super.actionPerformed(evt, target);
            this.currentTarget = null;
        }

        protected void insertString(BaseDocument doc, int dotPos, Caret caret, String str, boolean overwrite) throws BadLocationException {
            KeystrokeHandler bracketCompletion;
            if (CslEditorKit.completionSettingEnabled() && (bracketCompletion = UiUtils.getBracketCompletion((Document)doc, dotPos)) != null) {
                boolean handled = bracketCompletion.beforeCharInserted((Document)doc, dotPos, this.currentTarget, str.charAt(0));
                if (!handled) {
                    super.insertString(doc, dotPos, caret, str, overwrite);
                    handled = bracketCompletion.afterCharInserted((Document)doc, dotPos, this.currentTarget, str.charAt(0));
                }
                return;
            }
            super.insertString(doc, dotPos, caret, str, overwrite);
        }

        protected void replaceSelection(JTextComponent target, int dotPos, Caret caret, String str, boolean overwrite) throws BadLocationException {
            if (str.equals("")) {
                return;
            }
            char insertedChar = str.charAt(0);
            Document document = target.getDocument();
            if (document instanceof BaseDocument) {
                KeystrokeHandler bracketCompletion;
                BaseDocument doc = (BaseDocument)document;
                if (CslEditorKit.completionSettingEnabled() && (bracketCompletion = UiUtils.getBracketCompletion((Document)doc, dotPos)) != null) {
                    try {
                        int caretPosition = caret.getDot();
                        boolean handled = bracketCompletion.beforeCharInserted((Document)doc, caretPosition, target, insertedChar);
                        int p0 = Math.min(caret.getDot(), caret.getMark());
                        int p1 = Math.max(caret.getDot(), caret.getMark());
                        if (p0 != p1) {
                            doc.remove(p0, p1 - p0);
                        }
                        if (!handled) {
                            if (str != null && str.length() > 0) {
                                doc.insertString(p0, str, null);
                            }
                            bracketCompletion.afterCharInserted((Document)doc, caret.getDot() - 1, target, insertedChar);
                        }
                    }
                    catch (BadLocationException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
            super.replaceSelection(target, dotPos, caret, str, overwrite);
        }
    }

}

