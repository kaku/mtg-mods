/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Editable
 *  org.netbeans.api.actions.Openable
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.core.api.multiview.MultiViews
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.LineCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.loaders.SaveAsCapable
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.CookieSet$Factory
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.text.DataEditorSupport
 *  org.openide.text.DataEditorSupport$Env
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 */
package org.netbeans.modules.csl.core;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.netbeans.api.actions.Editable;
import org.netbeans.api.actions.Openable;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.core.api.multiview.MultiViews;
import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.core.CslEditorKit;
import org.netbeans.modules.csl.core.GsfDataNode;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.loaders.SaveAsCapable;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.DataEditorSupport;
import org.openide.util.Lookup;
import org.openide.windows.CloneableOpenSupport;

public class GsfDataObject
extends MultiDataObject {
    private static Language templateLanguage;
    private GenericEditorSupport jes;
    private final Language language;

    public GsfDataObject(FileObject pf, MultiFileLoader loader, Language language) throws DataObjectExistsException {
        super(pf, loader);
        if (language == null) {
            language = templateLanguage;
        }
        this.language = language;
        this.getCookieSet().add(new Class[]{GenericEditorSupport.class, SaveAsCapable.class, Openable.class, EditorCookie.Observable.class, PrintCookie.class, CloseCookie.class, Editable.class, LineCookie.class, DataEditorSupport.class, CloneableEditorSupport.class, CloneableOpenSupport.class}, (CookieSet.Factory)new EditorSupportFactory());
    }

    public Node createNodeDelegate() {
        return new GsfDataNode(this, this.language);
    }

    protected int associateLookup() {
        return 1;
    }

    public void setModified(boolean modif) {
        GenericEditorSupport ges;
        super.setModified(modif);
        if (!modif && (ges = (GenericEditorSupport)((Object)this.getLookup().lookup(GenericEditorSupport.class))) != null) {
            ges.removeSaveCookie();
        }
    }

    public <T extends Node.Cookie> T getCookie(Class<T> type) {
        return (T)this.getCookieSet().getCookie(type);
    }

    protected DataObject handleCopyRename(DataFolder df, String name, String ext) throws IOException {
        FileObject fo = this.getPrimaryEntry().copyRename(df.getPrimaryFile(), name, ext);
        DataObject dob = DataObject.find((FileObject)fo);
        return dob;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected DataObject handleCreateFromTemplate(DataFolder df, String name) throws IOException {
        if (name == null && this.language != null && this.language.getGsfLanguage().getPreferredExtension() != null) {
            name = FileUtil.findFreeFileName((FileObject)df.getPrimaryFile(), (String)this.getPrimaryFile().getName(), (String)this.language.getGsfLanguage().getPreferredExtension());
        }
        try {
            templateLanguage = this.language;
            DataObject retValue = super.handleCreateFromTemplate(df, name);
            FileObject fo = retValue.getPrimaryFile();
            assert (fo != null);
            DataObject dataObject = retValue;
            return dataObject;
        }
        finally {
            templateLanguage = null;
        }
    }

    private synchronized GenericEditorSupport createEditorSupport() {
        if (this.jes == null) {
            this.jes = new GenericEditorSupport(this, this.language);
        }
        return this.jes;
    }

    public static final class GenericEditorSupport
    extends DataEditorSupport
    implements OpenCookie,
    EditCookie,
    EditorCookie,
    PrintCookie,
    EditorCookie.Observable,
    SaveAsCapable,
    LineCookie,
    CloseCookie {
        private Language language;

        protected boolean asynchronousOpen() {
            return true;
        }

        public GenericEditorSupport(GsfDataObject dataObject, Language language) {
            super((DataObject)dataObject, null, (CloneableEditorSupport.Env)new Environment(dataObject));
            String mime = dataObject.getPrimaryFile().getMIMEType();
            List<Language> lngs = LanguageRegistry.getInstance().getApplicableLanguages(mime);
            if (lngs.contains(language)) {
                this.setMIMEType(mime);
            } else {
                this.setMIMEType(language.getMimeType());
            }
            this.language = language;
        }

        protected CloneableEditorSupport.Pane createPane() {
            if (this.language.useMultiview()) {
                return (CloneableEditorSupport.Pane)MultiViews.createCloneableMultiView((String)this.language.getMimeType(), (Serializable)this.getDataObject());
            }
            return super.createPane();
        }

        protected boolean notifyModified() {
            if (!super.notifyModified()) {
                return false;
            }
            ((Environment)this.env).addSaveCookie();
            return true;
        }

        protected void notifyUnmodified() {
            super.notifyUnmodified();
            this.removeSaveCookie();
        }

        final void removeSaveCookie() {
            ((Environment)this.env).removeSaveCookie();
        }

        public boolean close(boolean ask) {
            return super.close(ask);
        }

        protected EditorKit createEditorKit() {
            EditorKit kit = super.createEditorKit();
            if (kit instanceof CslEditorKit) {
                CslEditorKit csKit = (CslEditorKit)((Object)kit);
                csKit.applyContentType(this.getDataObject().getPrimaryFile().getMIMEType());
            }
            return kit;
        }

        protected StyledDocument createStyledDocument(EditorKit kit) {
            StyledDocument doc = super.createStyledDocument(kit);
            InputAttributes attributes = new InputAttributes();
            FileObject fileObject = NbEditorUtilities.getFileObject((Document)doc);
            GsfLanguage lng = this.language.getGsfLanguage();
            if (lng != null) {
                attributes.setValue(lng.getLexerLanguage(), FileObject.class, (Object)fileObject, false);
            }
            doc.putProperty(InputAttributes.class, (Object)attributes);
            return doc;
        }

        private static class Environment
        extends DataEditorSupport.Env {
            private static final long serialVersionUID = -1;
            private transient SaveSupport saveCookie = null;

            public Environment(GsfDataObject obj) {
                super((DataObject)obj);
            }

            protected FileObject getFile() {
                return this.getDataObject().getPrimaryFile();
            }

            protected FileLock takeLock() throws IOException {
                return ((MultiDataObject)this.getDataObject()).getPrimaryEntry().takeLock();
            }

            public CloneableOpenSupport findCloneableOpenSupport() {
                return (CloneableEditorSupport)this.getDataObject().getLookup().lookup(CloneableEditorSupport.class);
            }

            public void addSaveCookie() {
                GsfDataObject javaData = (GsfDataObject)this.getDataObject();
                if (javaData.getCookie(SaveCookie.class) == null) {
                    if (this.saveCookie == null) {
                        this.saveCookie = new SaveSupport();
                    }
                    javaData.getCookieSet().add((Node.Cookie)this.saveCookie);
                    javaData.setModified(true);
                }
            }

            public void removeSaveCookie() {
                GsfDataObject javaData = (GsfDataObject)this.getDataObject();
                if (javaData.getCookie(SaveCookie.class) != null) {
                    javaData.getCookieSet().remove((Node.Cookie)this.saveCookie);
                    javaData.setModified(false);
                }
            }

            private class SaveSupport
            implements SaveCookie {
                private SaveSupport() {
                }

                public void save() throws IOException {
                    ((GenericEditorSupport)Environment.this.findCloneableOpenSupport()).saveDocument();
                }
            }

        }

    }

    public final class EditorSupportFactory
    implements CookieSet.Factory {
        public <T extends Node.Cookie> T createCookie(Class<T> klass) {
            if (klass.isAssignableFrom(DataEditorSupport.class) || DataEditorSupport.class.isAssignableFrom(klass) || klass.isAssignableFrom(Openable.class) || klass.isAssignableFrom(Editable.class) || klass.isAssignableFrom(EditorCookie.Observable.class) || klass.isAssignableFrom(PrintCookie.class) || klass.isAssignableFrom(CloseCookie.class) || klass.isAssignableFrom(LineCookie.class)) {
                return (T)((Node.Cookie)klass.cast((Object)GsfDataObject.this.createEditorSupport()));
            }
            return null;
        }
    }

}

