/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.core;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String DESC_tlIndexerName() {
        return NbBundle.getMessage(Bundle.class, (String)"DESC_tlIndexerName");
    }

    static String DN_tlIndexerName() {
        return NbBundle.getMessage(Bundle.class, (String)"DN_tlIndexerName");
    }

    private void Bundle() {
    }
}

