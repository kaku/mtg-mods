/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.ParserFactory
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.swing.Action;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.KeystrokeHandler;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.OverridingMethods;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.editor.semantic.ColoringManager;
import org.netbeans.modules.csl.hints.infrastructure.GsfHintsManager;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.openide.filesystems.FileObject;

public final class Language {
    private ColoringManager coloringManager;
    private String iconBase;
    private String mime;
    private Boolean useCustomEditorKit;
    private Boolean useMultiview;
    private List<Action> actions;
    private GsfLanguage language;
    private DefaultLanguageConfig languageConfig;
    private CodeCompletionHandler completionProvider;
    private InstantRenamer renamer;
    private DeclarationFinder declarationFinder;
    private Formatter formatter;
    private KeystrokeHandler keystrokeHandler;
    private EmbeddingIndexerFactory indexerFactory;
    private StructureScanner structure;
    private OverridingMethods overridingMethods;
    private HintsProvider hintsProvider;
    private GsfHintsManager hintsManager;
    private IndexSearcher indexSearcher;
    private OccurrencesFinder occurrences;
    private SemanticAnalyzer semantic;
    private FileObject parserFile;
    private FileObject languageFile;
    private FileObject completionProviderFile;
    private FileObject renamerFile;
    private FileObject declarationFinderFile;
    private FileObject formatterFile;
    private FileObject keystrokeHandlerFile;
    private FileObject indexerFile;
    private FileObject structureFile;
    private FileObject overridingMethodsFile;
    private FileObject hintsProviderFile;
    private FileObject semanticFile;
    private FileObject occurrencesFile;
    private FileObject indexSearcherFile;
    private Set<String> sourcePathIds;
    private Set<String> libraryPathIds;
    private Set<String> binaryLibraryPathIds;

    public Language(String mime) {
        this.mime = mime;
    }

    public Language(String iconBase, String mime, List<Action> actions, GsfLanguage gsfLanguage, CodeCompletionHandler completionProvider, InstantRenamer renamer, DeclarationFinder declarationFinder, Formatter formatter, KeystrokeHandler bracketCompletion, EmbeddingIndexerFactory indexerFactory, StructureScanner structure, Object palette, boolean useCustomEditorKit) {
        this.iconBase = iconBase;
        this.mime = mime;
        this.actions = actions;
        this.language = gsfLanguage;
        this.completionProvider = completionProvider;
        this.renamer = renamer;
        this.declarationFinder = declarationFinder;
        this.formatter = formatter;
        this.keystrokeHandler = bracketCompletion;
        this.indexerFactory = indexerFactory;
        this.structure = structure;
        this.useCustomEditorKit = useCustomEditorKit;
    }

    public boolean useCustomEditorKit() {
        if (this.useCustomEditorKit == null) {
            this.useCustomEditorKit = Boolean.FALSE;
        }
        return this.useCustomEditorKit;
    }

    void setUseCustomEditorKit(boolean useCustomEditorKit) {
        this.useCustomEditorKit = useCustomEditorKit;
    }

    public boolean useMultiview() {
        if (this.useMultiview == null) {
            this.useMultiview = Boolean.FALSE;
        }
        return this.useMultiview;
    }

    void setUseMultiview(boolean use) {
        this.useMultiview = use;
    }

    @NonNull
    public String getDisplayName() {
        GsfLanguage l = this.getGsfLanguage();
        return l == null ? this.mime : l.getDisplayName();
    }

    public String getIconBase() {
        return this.iconBase;
    }

    void setIconBase(String iconBase) {
        this.iconBase = iconBase;
    }

    @NonNull
    public String getMimeType() {
        return this.mime;
    }

    void setMimeType(String mime) {
        this.mime = mime;
    }

    public Action[] getEditorActions() {
        if (this.actions != null) {
            return this.actions.toArray(new Action[this.actions.size()]);
        }
        return new Action[0];
    }

    @NonNull
    public GsfLanguage getGsfLanguage() {
        if (this.language == null && this.languageFile != null) {
            this.language = (GsfLanguage)this.createInstance(this.languageFile);
            if (this.language == null) {
                this.languageFile = null;
            } else if (this.language instanceof DefaultLanguageConfig) {
                this.languageConfig = (DefaultLanguageConfig)this.language;
            }
        }
        return this.language;
    }

    void setGsfLanguageFile(FileObject languageFile) {
        this.languageFile = languageFile;
    }

    @CheckForNull
    public Parser getParser(Collection<Snapshot> snapshots) {
        Parser parser = null;
        if (this.parserFile != null) {
            ParserFactory factory = (ParserFactory)this.createInstance(this.parserFile);
            if (factory == null) {
                this.parserFile = null;
            } else {
                parser = factory.createParser(snapshots);
            }
        } else {
            this.getGsfLanguage();
            if (this.languageConfig != null) {
                parser = this.languageConfig.getParser();
            }
        }
        return parser;
    }

    void setParserFile(FileObject parserFile) {
        this.parserFile = parserFile;
    }

    public void addAction(Action action) {
        if (this.actions == null) {
            this.actions = new ArrayList<Action>();
        }
        this.actions.add(action);
    }

    private Object createInstance(FileObject file) {
        return DataLoadersBridge.getDefault().createInstance(file);
    }

    public String toString() {
        return super.toString() + "[" + this.mime;
    }

    @CheckForNull
    public CodeCompletionHandler getCompletionProvider() {
        if (this.completionProvider == null) {
            if (this.completionProviderFile != null) {
                this.completionProvider = (CodeCompletionHandler)this.createInstance(this.completionProviderFile);
                if (this.completionProvider == null) {
                    this.completionProviderFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.completionProvider = this.languageConfig.getCompletionHandler();
                }
            }
        }
        return this.completionProvider;
    }

    void setCompletionProvider(CodeCompletionHandler completionProvider) {
        this.completionProvider = completionProvider;
    }

    void setCompletionProviderFile(FileObject completionProviderFile) {
        this.completionProviderFile = completionProviderFile;
    }

    @CheckForNull
    public InstantRenamer getInstantRenamer() {
        if (this.renamer == null) {
            if (this.renamerFile != null) {
                this.renamer = (InstantRenamer)this.createInstance(this.renamerFile);
                if (this.renamer == null) {
                    this.renamerFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.renamer = this.languageConfig.getInstantRenamer();
                }
            }
        }
        return this.renamer;
    }

    void setInstantRenamerFile(FileObject renamerFile) {
        this.renamerFile = renamerFile;
    }

    @CheckForNull
    public DeclarationFinder getDeclarationFinder() {
        if (this.declarationFinder == null) {
            if (this.declarationFinderFile != null) {
                this.declarationFinder = (DeclarationFinder)this.createInstance(this.declarationFinderFile);
                if (this.declarationFinder == null) {
                    this.declarationFinderFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.declarationFinder = this.languageConfig.getDeclarationFinder();
                }
            }
        }
        return this.declarationFinder;
    }

    void setDeclarationFinderFile(FileObject declarationFinderFile) {
        this.declarationFinderFile = declarationFinderFile;
    }

    @CheckForNull
    public Formatter getFormatter() {
        if (this.formatter == null) {
            if (this.formatterFile != null) {
                this.formatter = (Formatter)this.createInstance(this.formatterFile);
                if (this.formatter == null) {
                    this.formatterFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.formatter = this.languageConfig.getFormatter();
                    if (this.formatter != null && !$assertionsDisabled && !this.languageConfig.hasFormatter()) {
                        throw new AssertionError();
                    }
                }
            }
        }
        return this.formatter;
    }

    void setFormatterFile(FileObject formatterFile) {
        this.formatterFile = formatterFile;
    }

    @CheckForNull
    public KeystrokeHandler getBracketCompletion() {
        if (this.keystrokeHandler == null) {
            if (this.keystrokeHandlerFile != null) {
                this.keystrokeHandler = (KeystrokeHandler)this.createInstance(this.keystrokeHandlerFile);
                if (this.keystrokeHandler == null) {
                    this.keystrokeHandlerFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.keystrokeHandler = this.languageConfig.getKeystrokeHandler();
                }
            }
        }
        return this.keystrokeHandler;
    }

    void setBracketCompletionFile(FileObject bracketCompletionFile) {
        this.keystrokeHandlerFile = bracketCompletionFile;
    }

    @CheckForNull
    public EmbeddingIndexerFactory getIndexerFactory() {
        if (this.indexerFactory == null) {
            if (this.indexerFile != null) {
                this.indexerFactory = (EmbeddingIndexerFactory)this.createInstance(this.indexerFile);
                if (this.indexerFactory == null) {
                    this.indexerFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.indexerFactory = this.languageConfig.getIndexerFactory();
                }
            }
        }
        return this.indexerFactory;
    }

    void setIndexerFile(FileObject indexerFile) {
        this.indexerFile = indexerFile;
    }

    @CheckForNull
    public StructureScanner getStructure() {
        if (this.structure == null) {
            if (this.structureFile != null) {
                this.structure = (StructureScanner)this.createInstance(this.structureFile);
                if (this.structure == null) {
                    this.structureFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.structure = this.languageConfig.getStructureScanner();
                    if (this.structure != null && !$assertionsDisabled && !this.languageConfig.hasStructureScanner()) {
                        throw new AssertionError();
                    }
                }
            }
        }
        return this.structure;
    }

    void setStructureFile(FileObject structureFile) {
        this.structureFile = structureFile;
    }

    @CheckForNull
    public HintsProvider getHintsProvider() {
        if (this.hintsProvider == null) {
            if (this.hintsProviderFile != null) {
                this.hintsProvider = (HintsProvider)this.createInstance(this.hintsProviderFile);
                if (this.hintsProvider == null) {
                    this.hintsProviderFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.hintsProvider = this.languageConfig.getHintsProvider();
                    if (this.hintsProvider != null && !$assertionsDisabled && !this.languageConfig.hasHintsProvider()) {
                        throw new AssertionError();
                    }
                }
            }
            if (this.hintsProvider != null) {
                this.hintsManager = new GsfHintsManager(this.getMimeType(), this.hintsProvider, this);
            }
        }
        return this.hintsProvider;
    }

    @NonNull
    public GsfHintsManager getHintsManager() {
        if (this.hintsManager == null) {
            if (this.hintsProvider == null) {
                this.hintsProvider = this.getHintsProvider();
            }
            if (this.hintsProvider != null) {
                this.hintsManager = new GsfHintsManager(this.getMimeType(), this.hintsProvider, this);
            }
        }
        return this.hintsManager;
    }

    void setHintsProviderFile(FileObject hintsProviderFile) {
        this.hintsProviderFile = hintsProviderFile;
    }

    @NonNull
    public ColoringManager getColoringManager() {
        if (this.coloringManager == null) {
            this.coloringManager = new ColoringManager(this.mime);
        }
        return this.coloringManager;
    }

    public boolean hasStructureScanner() {
        if (this.structureFile != null) {
            return true;
        }
        return false;
    }

    public boolean hasFormatter() {
        if (this.formatterFile != null) {
            return true;
        }
        this.getGsfLanguage();
        if (this.languageConfig != null) {
            return this.languageConfig.hasFormatter();
        }
        return false;
    }

    public boolean hasHints() {
        if (this.hintsProviderFile != null) {
            return true;
        }
        this.getGsfLanguage();
        if (this.languageConfig != null) {
            return this.languageConfig.hasHintsProvider();
        }
        return false;
    }

    @NonNull
    public OccurrencesFinder getOccurrencesFinder() {
        if (this.occurrences == null) {
            if (this.occurrencesFile != null) {
                this.occurrences = (OccurrencesFinder)((Object)this.createInstance(this.occurrencesFile));
                if (this.occurrences == null) {
                    this.occurrencesFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.occurrences = this.languageConfig.getOccurrencesFinder();
                    if (this.occurrences != null && !$assertionsDisabled && !this.languageConfig.hasOccurrencesFinder()) {
                        throw new AssertionError();
                    }
                }
            }
        }
        return this.occurrences;
    }

    void setOccurrencesFinderFile(FileObject occurrencesFile) {
        this.occurrencesFile = occurrencesFile;
    }

    public boolean hasOccurrencesFinder() {
        if (this.occurrencesFile != null) {
            return true;
        }
        this.getGsfLanguage();
        if (this.languageConfig != null) {
            return this.languageConfig.hasOccurrencesFinder();
        }
        return false;
    }

    @NonNull
    public SemanticAnalyzer getSemanticAnalyzer() {
        if (this.semantic == null) {
            if (this.semanticFile != null) {
                this.semantic = (SemanticAnalyzer)((Object)this.createInstance(this.semanticFile));
                if (this.semantic == null) {
                    this.semanticFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.semantic = this.languageConfig.getSemanticAnalyzer();
                }
            }
        }
        return this.semantic;
    }

    void setSemanticAnalyzer(FileObject semanticFile) {
        this.semanticFile = semanticFile;
    }

    @NonNull
    public IndexSearcher getIndexSearcher() {
        if (this.indexSearcher == null) {
            if (this.indexSearcherFile != null) {
                this.indexSearcher = (IndexSearcher)this.createInstance(this.indexSearcherFile);
                if (this.indexSearcher == null) {
                    this.indexSearcherFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.indexSearcher = this.languageConfig.getIndexSearcher();
                }
            }
        }
        return this.indexSearcher;
    }

    void setIndexSearcher(FileObject indexSearcherFile) {
        this.indexSearcherFile = indexSearcherFile;
    }

    public Set<String> getSourcePathIds() {
        if (this.sourcePathIds == null) {
            this.getGsfLanguage();
            if (this.languageConfig != null) {
                this.sourcePathIds = this.languageConfig.getSourcePathIds();
            }
        }
        return this.sourcePathIds;
    }

    public Set<String> getLibraryPathIds() {
        if (this.libraryPathIds == null) {
            this.getGsfLanguage();
            if (this.languageConfig != null) {
                this.libraryPathIds = this.languageConfig.getLibraryPathIds();
            }
        }
        return this.libraryPathIds;
    }

    public Set<String> getBinaryLibraryPathIds() {
        if (this.binaryLibraryPathIds == null) {
            this.getGsfLanguage();
            if (this.languageConfig != null) {
                this.binaryLibraryPathIds = this.languageConfig.getBinaryLibraryPathIds();
            }
        }
        return this.binaryLibraryPathIds;
    }

    @NonNull
    public OverridingMethods getOverridingMethods() {
        if (this.overridingMethods == null) {
            if (this.overridingMethodsFile != null) {
                this.overridingMethods = (OverridingMethods)this.createInstance(this.overridingMethodsFile);
                if (this.overridingMethods == null) {
                    this.overridingMethodsFile = null;
                }
            } else {
                this.getGsfLanguage();
                if (this.languageConfig != null) {
                    this.overridingMethods = this.languageConfig.getOverridingMethods();
                }
            }
        }
        return this.overridingMethods;
    }

    void setOverridingMethodsFile(FileObject fo) {
        this.overridingMethodsFile = fo;
    }
}

