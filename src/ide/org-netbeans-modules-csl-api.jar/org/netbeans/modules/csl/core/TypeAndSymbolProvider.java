/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.netbeans.spi.jumpto.support.NameMatcherFactory
 *  org.netbeans.spi.jumpto.symbol.SymbolDescriptor
 *  org.netbeans.spi.jumpto.symbol.SymbolProvider
 *  org.netbeans.spi.jumpto.symbol.SymbolProvider$Context
 *  org.netbeans.spi.jumpto.symbol.SymbolProvider$Result
 *  org.netbeans.spi.jumpto.type.SearchType
 *  org.netbeans.spi.jumpto.type.TypeDescriptor
 *  org.netbeans.spi.jumpto.type.TypeProvider
 *  org.netbeans.spi.jumpto.type.TypeProvider$Context
 *  org.netbeans.spi.jumpto.type.TypeProvider$Result
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.core;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import org.netbeans.api.project.Project;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.csl.core.GsfTaskProvider;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.navigation.Icons;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.jumpto.support.NameMatcherFactory;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.symbol.SymbolProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;
import org.openide.filesystems.FileObject;

public class TypeAndSymbolProvider {
    private static final Logger LOG = Logger.getLogger(TypeAndSymbolProvider.class.getName());
    private static final IndexSearcher.Helper HELPER = new IndexSearcher.Helper(){

        @Override
        public Icon getIcon(ElementHandle element) {
            return Icons.getElementIcon(element.getKind(), element.getModifiers());
        }

        @Override
        public void open(FileObject fileObject, ElementHandle element) {
            Source js = Source.create((FileObject)fileObject);
            if (js != null) {
                UiUtils.open(js, element);
            }
        }
    };
    private final boolean typeProvider;
    private boolean cancelled;

    public String name() {
        return this.typeProvider ? "CSL-TypeProvider" : "CSL-SymbolProvider";
    }

    public String getDisplayName() {
        return GsfTaskProvider.getAllLanguageNames();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        TypeAndSymbolProvider typeAndSymbolProvider = this;
        synchronized (typeAndSymbolProvider) {
            this.cancelled = true;
        }
    }

    public void cleanup() {
    }

    private TypeAndSymbolProvider(boolean typeProvider) {
        this.typeProvider = typeProvider;
    }

    protected final Set<? extends IndexSearcher.Descriptor> compute(String text, SearchType searchType, Project project) {
        this.resume();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Looking for '" + text + "', searchType=" + (Object)searchType + ", project=" + (Object)project);
        }
        HashSet<? extends IndexSearcher.Descriptor> results = new HashSet<IndexSearcher.Descriptor>();
        for (Language language : LanguageRegistry.getInstance()) {
            if (this.isCancelled()) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Search '" + text + "', searchType=" + (Object)searchType + ", project=" + (Object)project + " cancelled");
                }
                return null;
            }
            IndexSearcher searcher = language.getIndexSearcher();
            if (searcher == null) {
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine("No IndexSearcher for " + language);
                continue;
            }
            Object[] o = TypeAndSymbolProvider.t2t(searchType, text);
            Set<? extends IndexSearcher.Descriptor> languageResults = this.typeProvider ? searcher.getTypes(project, (String)o[1], (QuerySupport.Kind)o[0], HELPER) : searcher.getSymbols(project, (String)o[1], (QuerySupport.Kind)o[0], HELPER);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(searcher + " found " + languageResults);
            }
            if (languageResults == null) continue;
            results.addAll(languageResults);
        }
        return results;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void resume() {
        TypeAndSymbolProvider typeAndSymbolProvider = this;
        synchronized (typeAndSymbolProvider) {
            this.cancelled = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean isCancelled() {
        TypeAndSymbolProvider typeAndSymbolProvider = this;
        synchronized (typeAndSymbolProvider) {
            return this.cancelled;
        }
    }

    private static Object[] t2t(SearchType searchType, String text) {
        switch (searchType) {
            case EXACT_NAME: {
                return new Object[]{QuerySupport.Kind.EXACT, text};
            }
            case CASE_INSENSITIVE_EXACT_NAME: {
                return new Object[]{QuerySupport.Kind.CASE_INSENSITIVE_REGEXP, NameMatcherFactory.wildcardsToRegexp((String)text, (boolean)false)};
            }
            case PREFIX: {
                return new Object[]{QuerySupport.Kind.PREFIX, text};
            }
            case CASE_INSENSITIVE_PREFIX: {
                return new Object[]{QuerySupport.Kind.CASE_INSENSITIVE_PREFIX, text};
            }
            case REGEXP: {
                return new Object[]{QuerySupport.Kind.REGEXP, NameMatcherFactory.wildcardsToRegexp((String)text, (boolean)true)};
            }
            case CASE_INSENSITIVE_REGEXP: {
                return new Object[]{QuerySupport.Kind.CASE_INSENSITIVE_REGEXP, NameMatcherFactory.wildcardsToRegexp((String)text, (boolean)true)};
            }
            case CAMEL_CASE: {
                return new Object[]{QuerySupport.Kind.CAMEL_CASE, text};
            }
        }
        throw new IllegalStateException("Can't translate " + (Object)searchType + " to QuerySupport.Kind");
    }

    private static final class SymbolWrapper
    extends SymbolDescriptor {
        private final IndexSearcher.Descriptor delegated;

        private SymbolWrapper(IndexSearcher.Descriptor delegated) {
            this.delegated = delegated;
        }

        public Icon getIcon() {
            return this.delegated.getIcon();
        }

        public String getProjectName() {
            return this.delegated.getProjectName();
        }

        public Icon getProjectIcon() {
            return this.delegated.getProjectIcon();
        }

        public FileObject getFileObject() {
            return this.delegated.getFileObject();
        }

        public int getOffset() {
            return this.delegated.getOffset();
        }

        public void open() {
            this.delegated.open();
        }

        public String getSymbolName() {
            return this.delegated.getSimpleName();
        }

        public String getOwnerName() {
            String owner = this.delegated.getContextName();
            if (owner == null) {
                owner = "";
            }
            return owner;
        }
    }

    private static final class TypeWrapper
    extends TypeDescriptor {
        private final IndexSearcher.Descriptor delegated;

        public TypeWrapper(IndexSearcher.Descriptor delegated) {
            this.delegated = delegated;
        }

        public String getSimpleName() {
            return this.delegated.getSimpleName();
        }

        public String getOuterName() {
            return this.delegated.getOuterName();
        }

        public String getTypeName() {
            return this.delegated.getTypeName();
        }

        public String getContextName() {
            String s = this.delegated.getContextName();
            if (s != null) {
                return " (" + s + ")";
            }
            return null;
        }

        public Icon getIcon() {
            return this.delegated.getIcon();
        }

        public String getProjectName() {
            return this.delegated.getProjectName();
        }

        public Icon getProjectIcon() {
            return this.delegated.getProjectIcon();
        }

        public FileObject getFileObject() {
            return this.delegated.getFileObject();
        }

        public int getOffset() {
            return this.delegated.getOffset();
        }

        public void open() {
            this.delegated.open();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TypeWrapper)) {
                return false;
            }
            TypeWrapper other = (TypeWrapper)((Object)obj);
            return this.delegated == null ? other.delegated == null : this.delegated.equals(other.delegated);
        }

        public int hashCode() {
            return this.delegated == null ? 0 : this.delegated.hashCode();
        }
    }

    public static final class SymbolProviderImpl
    extends TypeAndSymbolProvider
    implements SymbolProvider {
        public SymbolProviderImpl() {
            super(false);
        }

        public void computeSymbolNames(SymbolProvider.Context context, SymbolProvider.Result result) {
            Set<? extends IndexSearcher.Descriptor> descriptors = this.compute(context.getText(), context.getSearchType(), context.getProject());
            if (descriptors != null) {
                for (IndexSearcher.Descriptor d : descriptors) {
                    result.addResult((SymbolDescriptor)new SymbolWrapper(d));
                }
            }
        }
    }

    public static final class TypeProviderImpl
    extends TypeAndSymbolProvider
    implements TypeProvider {
        public TypeProviderImpl() {
            super(true);
        }

        public void computeTypeNames(TypeProvider.Context context, TypeProvider.Result result) {
            Set<? extends IndexSearcher.Descriptor> descriptors = this.compute(context.getText(), context.getSearchType(), context.getProject());
            if (descriptors != null) {
                for (IndexSearcher.Descriptor d : descriptors) {
                    result.addResult((TypeDescriptor)new TypeWrapper(d));
                }
            }
        }
    }

}

