/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.csl.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.ErrorFilter;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.util.Lookup;

public class ErrorFilterQuery {
    private static final Logger UPDATER_BACKDOOR = Logger.getLogger("org.netbeans.modules.parsing.impl.indexing.LogContext.backdoor");

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static List<? extends Error> getFilteredErrors(ParserResult parserResult, String featureName) {
        Collection factories = Lookup.getDefault().lookupAll(ErrorFilter.Factory.class);
        ArrayList<? extends Error> filtered = null;
        for (ErrorFilter.Factory factory : factories) {
            LogRecord lr;
            ErrorFilter filter = factory.createErrorFilter(featureName);
            String fn = "TLIndexer:" + ErrorFilterQuery.filterName(factory, filter);
            if (filter == null) continue;
            try {
                lr = new LogRecord(Level.INFO, "INDEXER_START");
                lr.setParameters(new Object[]{fn});
                UPDATER_BACKDOOR.log(lr);
                List<? extends Error> result = filter.filter(parserResult);
                if (result != null) {
                    if (filtered == null) {
                        filtered = new ArrayList<Error>(result);
                    } else {
                        filtered.addAll(result);
                    }
                }
                lr = new LogRecord(Level.INFO, "INDEXER_END");
            }
            catch (Throwable var10_10) {
                LogRecord lr2 = new LogRecord(Level.INFO, "INDEXER_END");
                lr2.setParameters(new Object[]{fn});
                UPDATER_BACKDOOR.log(lr2);
                throw var10_10;
            }
            lr.setParameters(new Object[]{fn});
            UPDATER_BACKDOOR.log(lr);
        }
        return filtered;
    }

    static String filterName(ErrorFilter.Factory fact, ErrorFilter f) {
        Class c = f == null ? fact.getClass() : f.getClass();
        String n = c.getName();
        int idx = n.indexOf(".modules.");
        int last = n.lastIndexOf(46);
        if (idx > -1) {
            int n2 = n.indexOf(46, idx + 9);
            return n.substring(idx + 9, n2 + 1) + n.substring(last + 1);
        }
        return n.substring(last + 1);
    }
}

