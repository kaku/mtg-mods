/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.indexing.PathRecognizer
 */
package org.netbeans.modules.csl.core;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;

public final class PathRecognizerImpl
extends PathRecognizer {
    private static final Logger LOG = Logger.getLogger(PathRecognizerImpl.class.getName());
    private final String mimeType;

    public Set<String> getSourcePathIds() {
        Language l = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
        return l != null ? l.getSourcePathIds() : null;
    }

    public Set<String> getBinaryLibraryPathIds() {
        Language l = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
        return l != null ? l.getBinaryLibraryPathIds() : null;
    }

    public Set<String> getLibraryPathIds() {
        Language l = LanguageRegistry.getInstance().getLanguageByMimeType(this.mimeType);
        return l != null ? l.getLibraryPathIds() : null;
    }

    public Set<String> getMimeTypes() {
        return Collections.singleton(this.mimeType);
    }

    public static PathRecognizer createInstance(Map fileAttributes) {
        Object v = fileAttributes.get("mimeType");
        if (v instanceof String) {
            return new PathRecognizerImpl((String)v);
        }
        return null;
    }

    public PathRecognizerImpl(String mimeType) {
        this.mimeType = mimeType;
    }

    public String toString() {
        return Object.super.toString() + "[mimeType=" + this.mimeType;
    }
}

