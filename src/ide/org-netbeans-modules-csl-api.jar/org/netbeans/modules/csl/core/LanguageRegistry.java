/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.csl.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.core.GsfDataLoader;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.editor.codetemplates.CslCorePackageAccessor;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public final class LanguageRegistry
implements Iterable<Language> {
    private static final Logger LOG = Logger.getLogger(LanguageRegistry.class.getName());
    private static LanguageRegistry instance;
    private static final String STRUCTURE = "structure.instance";
    private static final String LANGUAGE = "language.instance";
    private static final String ICON_BASE = "iconBase";
    private static final String PARSER = "parser.instance";
    private static final String COMPLETION = "completion.instance";
    private static final String RENAMER = "renamer.instance";
    private static final String FORMATTER = "formatter.instance";
    private static final String BRACKET_COMPLETION = "bracket.instance";
    private static final String DECLARATION_FINDER = "declarationfinder.instance";
    private static final String INDEXER = "indexer.instance";
    private static final String HINTS = "hints.instance";
    private static final String SEMANTIC = "semantic.instance";
    private static final String OCCURRENCES = "occurrences.instance";
    private static final String OVERRIDING_METHODS = "overridingmethods.instance";
    private static final String INDEX_SEARCHER = "index_searcher.instance";
    private static final String FOLDER = "CslPlugins";
    private boolean cacheDirty = true;
    private Map<String, Language> languagesCache;
    private FileChangeListener sfsTracker;

    public static synchronized LanguageRegistry getInstance() {
        if (instance == null) {
            instance = new LanguageRegistry();
        }
        return instance;
    }

    public Language getLanguageByMimeType(@NonNull String mimeType) {
        Map<String, Language> map = this.getLanguages();
        MimePath mp = MimePath.get((String)mimeType);
        Language lng = map.get(mimeType);
        if (lng != null) {
            return lng;
        }
        String s = mp.getInheritedType();
        if (s != null && !s.isEmpty()) {
            return map.get(s);
        }
        return null;
    }

    @NonNull
    public List<Language> getApplicableLanguages(String mimeType) {
        ArrayList<Language> result = new ArrayList<Language>(5);
        Language origLanguage = this.getLanguageByMimeType(mimeType);
        if (origLanguage != null) {
            result.add(origLanguage);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<Language> getEmbeddedLanguages(BaseDocument doc, int offset) {
        Language language;
        String mimeType;
        ArrayList<Language> result;
        result = new ArrayList<Language>();
        doc.readLock();
        try {
            TokenSequence ts = TokenHierarchy.get((Document)doc).tokenSequence();
            if (ts != null) {
                this.addLanguages(result, ts, offset);
            }
        }
        finally {
            doc.readUnlock();
        }
        if ((mimeType = (String)doc.getProperty((Object)"mimeType")) != null && (language = this.getLanguageByMimeType(mimeType)) != null && (result.size() == 0 || result.get(result.size() - 1) != language)) {
            result.add(language);
        }
        return result;
    }

    public boolean isSupported(@NonNull String mimeType) {
        Parameters.notNull((CharSequence)"mimeType", (Object)mimeType);
        return this.getLanguageByMimeType(mimeType) != null;
    }

    @Override
    public Iterator<Language> iterator() {
        Map<String, Language> map = this.getLanguages();
        return map.values().iterator();
    }

    private LanguageRegistry() {
    }

    private synchronized void addLanguages(Collection<? extends Language> newLanguages) {
        if (this.languagesCache != null) {
            throw new RuntimeException("This is for testing purposes only!!!");
        }
        this.cacheDirty = false;
        this.languagesCache = new HashMap<String, Language>(2 * newLanguages.size());
        for (Language language : newLanguages) {
            String mimeType = language.getMimeType();
            this.languagesCache.put(mimeType, language);
        }
    }

    private void addLanguages(List<Language> result, TokenSequence ts, int offset) {
        ts.move(offset);
        if (ts.moveNext() || ts.movePrevious()) {
            String mimeType;
            Language language;
            TokenSequence ets = ts.embedded();
            if (ets != null) {
                this.addLanguages(result, ets, offset);
            }
            if ((language = this.getLanguageByMimeType(mimeType = ts.language().mimeType())) != null) {
                result.add(language);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Map<String, Language> getLanguages() {
        boolean[] refreshLoader = new boolean[]{false};
        LanguageRegistry languageRegistry = this;
        synchronized (languageRegistry) {
            if (this.cacheDirty) {
                FileSystem sfs;
                this.cacheDirty = false;
                try {
                    sfs = FileUtil.getConfigRoot().getFileSystem();
                }
                catch (FileStateInvalidException fse) {
                    throw new IllegalStateException((Throwable)fse);
                }
                this.languagesCache = LanguageRegistry.readSfs(sfs, this.languagesCache, refreshLoader);
                if (this.sfsTracker == null) {
                    LanguageRegistry.userdirCleanup();
                    this.sfsTracker = new FsTracker(sfs);
                }
            }
        }
        if (refreshLoader[0]) {
            ((GsfDataLoader)GsfDataLoader.getLoader(GsfDataLoader.class)).initExtensions();
        }
        return this.languagesCache;
    }

    private static boolean isValidType(FileObject typeFile) {
        if (!typeFile.isFolder()) {
            return false;
        }
        String typeName = typeFile.getNameExt();
        return MimePath.validate((CharSequence)typeName, (CharSequence)null);
    }

    private static boolean isValidSubtype(FileObject subtypeFile) {
        if (!subtypeFile.isFolder()) {
            return false;
        }
        String typeName = subtypeFile.getNameExt();
        return MimePath.validate((CharSequence)null, (CharSequence)typeName) && !typeName.equals("base");
    }

    private static Map<String, Language> readSfs(FileSystem sfs, Map<String, Language> existingMap, boolean[] changesDetected) {
        FileObject registryFolder = sfs.findResource("CslPlugins");
        if (registryFolder == null) {
            LOG.fine("No CslPlugins folder");
            return Collections.emptyMap();
        }
        LOG.fine("Reading CslPlugins registry...");
        HashMap<String, Language> newMap = new HashMap<String, Language>();
        changesDetected[0] = false;
        FileObject[] types = registryFolder.getChildren();
        for (int i = 0; i < types.length; ++i) {
            if (!LanguageRegistry.isValidType(types[i])) continue;
            FileObject[] subtypes = types[i].getChildren();
            for (int j = 0; j < subtypes.length; ++j) {
                String iconBase;
                Boolean useMultiview;
                FileObject loaderMimeFile;
                Language existingLanguage;
                if (!LanguageRegistry.isValidSubtype(subtypes[j])) continue;
                String mimeType = types[i].getNameExt() + "/" + subtypes[j].getNameExt();
                Language language = existingLanguage = existingMap != null ? existingMap.get(mimeType) : null;
                if (existingLanguage != null) {
                    LOG.fine("Reusing existing Language for '" + mimeType + "': " + existingLanguage);
                    newMap.put(mimeType, existingLanguage);
                    continue;
                }
                Integer attr = (Integer)subtypes[j].getAttribute("genver");
                if (attr == null) {
                    LOG.log(Level.SEVERE, "Language " + mimeType + " has not been preprocessed during jar module creation");
                } else if (attr == 1) {
                    LOG.log(Level.WARNING, "Language " + mimeType + " has been preprocessed using the deprecated CslJar task. " + "Please use @LanguageRegistration annotation, see https://netbeans.org/bugzilla/show_bug.cgi?id=169991 for details.");
                }
                Language language2 = new Language(mimeType);
                newMap.put(mimeType, language2);
                LOG.fine("Adding new Language for '" + mimeType + "': " + language2);
                changesDetected[0] = true;
                Boolean useCustomEditorKit = (Boolean)subtypes[j].getAttribute("useCustomEditorKit");
                if (useCustomEditorKit != null && useCustomEditorKit.booleanValue()) {
                    language2.setUseCustomEditorKit(true);
                    LOG.fine("Language for '" + mimeType + "' is using custom editor kit.");
                }
                if ((useMultiview = (Boolean)subtypes[j].getAttribute("useMultiview")) != null && useMultiview.booleanValue()) {
                    language2.setUseMultiview(true);
                    LOG.fine("Language for '" + mimeType + "' is using multview.");
                }
                if ((loaderMimeFile = sfs.findResource("Loaders/" + mimeType)) != null && (iconBase = (String)loaderMimeFile.getAttribute("iconBase")) != null && iconBase.length() > 0) {
                    language2.setIconBase(iconBase);
                }
                boolean foundConfig = false;
                for (FileObject fo : subtypes[j].getChildren()) {
                    String name = fo.getNameExt();
                    LOG.fine("Language for '" + mimeType + "' registers: " + name);
                    if ("language.instance".equals(name)) {
                        foundConfig = true;
                        language2.setGsfLanguageFile(fo);
                        continue;
                    }
                    if ("hints.instance".equals(name)) {
                        language2.setHintsProviderFile(fo);
                        continue;
                    }
                    if ("structure.instance".equals(name)) {
                        language2.setStructureFile(fo);
                        continue;
                    }
                    if ("parser.instance".equals(name)) {
                        language2.setParserFile(fo);
                        continue;
                    }
                    if ("completion.instance".equals(name)) {
                        language2.setCompletionProviderFile(fo);
                        continue;
                    }
                    if ("renamer.instance".equals(name)) {
                        language2.setInstantRenamerFile(fo);
                        continue;
                    }
                    if ("formatter.instance".equals(name)) {
                        language2.setFormatterFile(fo);
                        continue;
                    }
                    if ("declarationfinder.instance".equals(name)) {
                        language2.setDeclarationFinderFile(fo);
                        continue;
                    }
                    if ("bracket.instance".equals(name)) {
                        language2.setBracketCompletionFile(fo);
                        continue;
                    }
                    if ("indexer.instance".equals(name)) {
                        language2.setIndexerFile(fo);
                        continue;
                    }
                    if ("semantic.instance".equals(name)) {
                        language2.setSemanticAnalyzer(fo);
                        continue;
                    }
                    if ("occurrences.instance".equals(name)) {
                        language2.setOccurrencesFinderFile(fo);
                        continue;
                    }
                    if ("overridingmethods.instance".equals(name)) {
                        language2.setOverridingMethodsFile(fo);
                        continue;
                    }
                    if (!"index_searcher.instance".equals(name)) continue;
                    language2.setIndexSearcher(fo);
                }
                if (foundConfig) continue;
                LOG.warning("No GsfLanguage instance registered in " + subtypes[j].getPath());
            }
        }
        if (existingMap != null && newMap.size() != existingMap.size()) {
            changesDetected[0] = true;
        }
        LOG.fine("-- Finished reading CslPlugins registry!");
        return newMap;
    }

    private static void userdirCleanup() {
        boolean assertionsEnabled = false;
        if (!$assertionsDisabled) {
            assertionsEnabled = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (!assertionsEnabled) {
            return;
        }
        FileObject navFo = FileUtil.getConfigFile((String)"Navigator/Panels/text/javascript/org-netbeans-modules-gsfret-navigation-ClassMemberPanel.instance");
        if (navFo == null || !navFo.canRevert()) {
            return;
        }
        try {
            FileObject editors;
            FileObject panels;
            FileObject navigator = FileUtil.getConfigFile((String)"Navigator");
            if (navigator != null && (panels = navigator.getFileObject("Panels")) != null) {
                for (FileObject outerMime : panels.getChildren()) {
                    for (FileObject innerMime : outerMime.getChildren()) {
                        FileObject panel = innerMime.getFileObject("org-netbeans-modules-gsfret-navigation-ClassMemberPanel.instance");
                        if (panel != null) {
                            panel.revert();
                            if (innerMime.getChildren().length == 0) {
                                innerMime.revert();
                            }
                        }
                        if (outerMime.getChildren().length != 0) continue;
                        outerMime.revert();
                    }
                }
                if (panels.getChildren().length == 0) {
                    panels.revert();
                    if (navigator.getChildren().length == 0) {
                        navigator.revert();
                    }
                }
            }
            if ((editors = FileUtil.getConfigFile((String)"Editors")) != null) {
                for (FileObject outerMime : editors.getChildren()) {
                    for (FileObject innerMime : outerMime.getChildren()) {
                        FileObject popup;
                        FileObject completion;
                        FileObject fo;
                        String completionProviders;
                        FileObject sep;
                        FileObject f;
                        FileObject hyperlinkProvider;
                        FileObject oldSidebar;
                        FileObject old;
                        String mimeType = outerMime.getName() + "/" + innerMime.getName();
                        FileObject root = innerMime;
                        FileObject settings = root.getFileObject("Settings.settings");
                        if (settings != null) {
                            settings.revert();
                        }
                        if ((fo = root.getFileObject("SideBar/org-netbeans-modules-editor-gsfret-GsfCodeFoldingSideBarFactory.instance")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("SideBar")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("FoldManager/org-netbeans-modules-gsfret-editor-fold-GsfFoldManagerFactory.instance")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("FoldManager")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                        }
                        if ((mimeType.equals("text/x-yaml") || mimeType.equals("text/x-json")) && (f = root.getFileObject("Popup/generate-fold-popup")) != null) {
                            f.revert();
                            f = root.getFileObject("ToolTips/org-netbeans-modules-languages-features-ToolTipAnnotation.instance");
                            if (f != null) {
                                f.revert();
                            }
                            if ((f = root.getFileObject("Popup/org-netbeans-modules-languages-features-GoToDeclarationAction.instance")) != null) {
                                f.revert();
                            }
                            if ((f = root.getFileObject("UpToDateStatusProvider/org-netbeans-modules-languages-features-UpToDateStatusProviderFactoryImpl.instance")) != null) {
                                f.revert();
                            }
                            if ((f = root.getFileObject("run_script.instance")) != null) {
                                f.revert();
                            }
                        }
                        if ((oldSidebar = root.getFileObject("SideBar/org-netbeans-modules-editor-retouche-GsfCodeFoldingSideBarFactory.instance")) != null) {
                            oldSidebar.revert();
                            oldSidebar = root.getFileObject("FoldManager/org-netbeans-modules-retouche-editor-fold-GsfFoldManagerFactory.instance");
                            if (oldSidebar != null) {
                                oldSidebar.revert();
                            }
                        }
                        if ((hyperlinkProvider = root.getFileObject("HyperlinkProviders/GsfHyperlinkProvider.instance")) != null) {
                            hyperlinkProvider.revert();
                        }
                        if ((fo = root.getFileObject("HyperlinkProviders")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                        }
                        if ((popup = root.getFileObject("Popup")) != null) {
                            FileObject gotoF;
                            FileObject ref = popup.getFileObject("in-place-refactoring");
                            if (ref != null) {
                                ref.revert();
                            }
                            if ((gotoF = popup.getFileObject("goto")) != null) {
                                fo = gotoF.getFileObject("goto-declaration");
                                if (fo != null) {
                                    fo.revert();
                                }
                                if ((fo = gotoF.getFileObject("goto")) != null) {
                                    fo.revert();
                                }
                                if (gotoF.getChildren().length == 0) {
                                    gotoF.revert();
                                }
                            }
                            if ((fo = popup.getFileObject("SeparatorBeforeCut.instance")) != null) {
                                fo.revert();
                            }
                            if ((fo = popup.getFileObject("format")) != null) {
                                fo.revert();
                            }
                            if ((fo = popup.getFileObject("SeparatorAfterFormat.instance")) != null) {
                                fo.revert();
                            }
                            if ((fo = popup.getFileObject("pretty-print")) != null) {
                                fo.revert();
                            }
                            if ((fo = popup.getFileObject("generate-goto-popup")) != null) {
                                fo.revert();
                            }
                            if (popup.getChildren().length == 0) {
                                popup.revert();
                            }
                        }
                        if ((fo = root.getFileObject("UpToDateStatusProvider/org-netbeans-modules-gsfret-hints-GsfUpToDateStateProviderFactory.instance")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("UpToDateStatusProvider/org-netbeans-modules-retouche-hints-GsfUpToDateStateProviderFactory.instance")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("UpToDateStatusProvider/org-netbeans-modules-gsfret-editor-semantic-OccurrencesMarkProviderCreator.instance")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("UpToDateStatusProvider/org-netbeans-modules-retouche-editor-semantic-OccurrencesMarkProviderCreator.instance")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("UpToDateStatusProvider")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("org-netbeans-modules-gsfret-editor-semantic-HighlightsLayerFactoryImpl.instance")) != null) {
                            fo.revert();
                        }
                        if ((completion = root.getFileObject(completionProviders = "CompletionProviders")) != null) {
                            FileObject old2;
                            String templates = "org-netbeans-lib-editor-codetemplates-CodeTemplateCompletionProvider.instance";
                            FileObject templeteProvider = root.getFileObject(completionProviders + "/" + templates);
                            if (templeteProvider != null) {
                                templeteProvider.revert();
                            }
                            String provider = "org-netbeans-modules-gsfret-editor-completion-GsfCompletionProvider.instance";
                            FileObject completionProvider = root.getFileObject(completionProviders + "/" + provider);
                            if (completionProvider != null) {
                                completionProvider.revert();
                            }
                            if ((old2 = completion.getFileObject("org-netbeans-modules-retouche-editor-completion-GsfCompletionProvider.instance")) != null) {
                                old2.revert();
                            }
                            if (completion.getChildren().length == 0) {
                                completion.revert();
                            }
                        }
                        if ((fo = root.getFileObject("Toolbars/Default/comment")) != null) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("Toolbars/Default/uncomment")) != null) {
                            fo.revert();
                        }
                        if ((sep = root.getFileObject("Toolbars/Default/Separator-before-comment.instance")) != null) {
                            sep.revert();
                        }
                        if ((fo = root.getFileObject("Toolbars/Default")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                            fo = root.getFileObject("Toolbars");
                            if (fo != null && fo.getChildren().length == 0) {
                                fo.revert();
                            }
                        }
                        if ((fo = root.getFileObject("CodeTemplateProcessorFactories/org-netbeans-modules-gsfret-editor-codetemplates-GsfCodeTemplateProcessor$Factory.instance")) != null) {
                            fo.revert();
                        }
                        if ((old = root.getFileObject("CodeTemplateProcessorFactories/org-netbeans-modules-retouche-editor-codetemplates-GsfCodeTemplateProcessor$Factory.instance")) != null) {
                            old.revert();
                        }
                        if ((fo = root.getFileObject("CodeTemplateProcessorFactories")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                        }
                        if ((fo = root.getFileObject("CodeTemplateFilterFactories/org-netbeans-modules-gsfret-editor-codetemplates-GsfCodeTemplateFilter$Factory.instance")) != null) {
                            fo.revert();
                        }
                        if ((old = root.getFileObject("CodeTemplateFilterFactories/org-netbeans-modules-retouche-editor-codetemplates-GsfCodeTemplateFilter$Factory.instance")) != null) {
                            old.revert();
                        }
                        if ((fo = root.getFileObject("CodeTemplateFilterFactories")) != null && fo.getChildren().length == 0) {
                            fo.revert();
                        }
                        if (innerMime.getChildren().length != 0) continue;
                        innerMime.revert();
                    }
                    if (outerMime.getChildren().length != 0) continue;
                    outerMime.revert();
                }
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
        }
    }

    static {
        CslCorePackageAccessor.register(new CslCorePackageAccessorImpl());
    }

    private static final class CslCorePackageAccessorImpl
    extends CslCorePackageAccessor {
        @Override
        public void languageRegistryAddLanguages(Collection<? extends Language> languages) {
            LanguageRegistry.getInstance().addLanguages(languages);
        }
    }

    private final class FsTracker
    implements FileChangeListener,
    Runnable {
        private final FileSystem fs;
        private RequestProcessor.Task slidingTask;

        public FsTracker(FileSystem fs) {
            this.slidingTask = RequestProcessor.getDefault().create((Runnable)this);
            this.fs = fs;
            this.fs.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)this.fs));
        }

        public void fileFolderCreated(FileEvent fe) {
            this.process(fe);
        }

        public void fileDataCreated(FileEvent fe) {
            this.process(fe);
        }

        public void fileChanged(FileEvent fe) {
            this.process(fe);
        }

        public void fileDeleted(FileEvent fe) {
            this.process(fe);
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.process((FileEvent)fe);
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
            this.process((FileEvent)fe);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void process(FileEvent fe) {
            if (fe.getFile().getPath().startsWith("CslPlugins")) {
                LanguageRegistry languageRegistry = LanguageRegistry.this;
                synchronized (languageRegistry) {
                    LanguageRegistry.this.cacheDirty = true;
                    this.slidingTask.schedule(100);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            boolean[] refreshLoader = new boolean[]{false};
            LanguageRegistry languageRegistry = LanguageRegistry.this;
            synchronized (languageRegistry) {
                if (LanguageRegistry.this.cacheDirty) {
                    LanguageRegistry.this.cacheDirty = false;
                    LanguageRegistry.this.languagesCache = LanguageRegistry.readSfs(this.fs, LanguageRegistry.this.languagesCache, refreshLoader);
                }
            }
            if (refreshLoader[0]) {
                ((GsfDataLoader)GsfDataLoader.getLoader(GsfDataLoader.class)).initExtensions();
            }
        }
    }

}

