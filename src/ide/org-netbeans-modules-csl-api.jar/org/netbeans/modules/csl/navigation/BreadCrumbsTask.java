/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsController
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.csl.navigation;

import java.awt.Image;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.navigation.ElementScanningTask;
import org.netbeans.modules.csl.navigation.Icons;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsController;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.Lookups;

public class BreadCrumbsTask
extends ElementScanningTask {
    private static final RequestProcessor WORKER = new RequestProcessor(BreadCrumbsTask.class.getName(), 1, false, false);
    private final AtomicLong requestId = new AtomicLong();

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return BreadcrumbsController.BREADCRUMBS_SCHEDULER;
    }

    public void run(ParserResult result, SchedulerEvent event) {
        JTextComponent c;
        final long id = this.requestId.incrementAndGet();
        final Document doc = result.getSnapshot().getSource().getDocument(false);
        if (doc == null || !BreadcrumbsController.areBreadCrumsEnabled((Document)doc)) {
            return;
        }
        final int caret = event instanceof CursorMovedSchedulerEvent ? ((CursorMovedSchedulerEvent)event).getCaretOffset() : ((c = EditorRegistry.focusedComponent()) != null && c.getDocument() == doc ? c.getCaretPosition() : -1);
        if (caret == -1) {
            return;
        }
        final StructureItem structureRoot = this.computeStructureRoot(result.getSnapshot().getSource());
        if (structureRoot == null) {
            return;
        }
        WORKER.post(new Runnable(){

            @Override
            public void run() {
                BreadCrumbsTask.this.selectNode(doc, structureRoot, id, caret);
            }
        });
    }

    private void selectNode(Document doc, StructureItem structureRoot, long id, int caret) {
        StructureItemNode root;
        StructureItemNode toSelect = root = new StructureItemNode(structureRoot);
        block0 : while (this.requestId.get() == id) {
            for (Node n : toSelect.getChildren().getNodes(true)) {
                StructureItemNode sin = (StructureItemNode)n;
                if (sin.item.getPosition() > (long)caret || (long)caret > sin.item.getEndPosition()) continue;
                toSelect = sin;
                if (!(toSelect.item instanceof ElementScanningTask.MimetypeRootNode)) continue block0;
                root = sin;
                continue block0;
            }
        }
        if (this.requestId.get() == id) {
            BreadcrumbsController.setBreadcrumbs((Document)doc, (Node)root, (Node)toSelect);
        }
    }

    @Override
    public synchronized void cancel() {
        super.cancel();
        this.requestId.incrementAndGet();
    }

    public static final class TaskFactoryImpl
    extends AbstractTaskFactory {
        public TaskFactoryImpl() {
            super(true);
        }

        @Override
        protected Collection<? extends SchedulerTask> createTasks(Language language, Snapshot snapshot) {
            return Collections.singletonList(new BreadCrumbsTask());
        }
    }

    private static final class StructureItemNode
    extends AbstractNode {
        private final StructureItem item;

        public StructureItemNode(StructureItem item) {
            super(Children.create((ChildFactory)new ChildFactory<StructureItem>(){

                protected boolean createKeys(List<StructureItem> toPopulate) {
                    toPopulate.addAll(StructureItem.this.getNestedItems());
                    return true;
                }

                protected Node createNodeForKey(StructureItem key) {
                    return new StructureItemNode(key);
                }
            }, (boolean)false), Lookups.fixed((Object[])new Object[]{new OpenCookie(){

                public void open() {
                    FileObject file;
                    ElementHandle elementHandle = StructureItem.this.getElementHandle();
                    FileObject fileObject = file = elementHandle != null ? elementHandle.getFileObject() : null;
                    if (file != null) {
                        UiUtils.open(file, (int)StructureItem.this.getPosition());
                    }
                }
            }}));
            this.item = item;
            this.setDisplayName(item.getName());
        }

        public Image getIcon(int type) {
            if (this.item.getCustomIcon() != null) {
                return ImageUtilities.icon2Image((Icon)this.item.getCustomIcon());
            }
            ImageIcon icon = Icons.getElementIcon(this.item.getKind(), this.item.getModifiers());
            if (icon != null) {
                return ImageUtilities.icon2Image((Icon)icon);
            }
            return super.getIcon(type);
        }

        public Image getOpenedIcon(int type) {
            return this.getIcon(type);
        }

    }

}

