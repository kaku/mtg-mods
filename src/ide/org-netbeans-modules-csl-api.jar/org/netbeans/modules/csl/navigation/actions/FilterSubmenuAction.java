/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.csl.navigation.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.netbeans.modules.csl.navigation.ClassMemberFilters;
import org.netbeans.modules.csl.navigation.base.FiltersDescription;
import org.netbeans.modules.csl.navigation.base.FiltersManager;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public final class FilterSubmenuAction
extends AbstractAction
implements Presenter.Popup {
    private static final String PROP_FILTER_NAME = "nbFilterName";
    private ClassMemberFilters filters;

    public FilterSubmenuAction(ClassMemberFilters filters) {
        this.filters = filters;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Object source = ev.getSource();
        if (source instanceof JCheckBoxMenuItem) {
            JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem)source;
            String filterName = (String)menuItem.getClientProperty("nbFilterName");
            this.filters.getInstance().setSelected(filterName, menuItem.isSelected());
        }
    }

    public final JMenuItem getPopupPresenter() {
        return this.createSubmenu();
    }

    private JMenuItem createSubmenu() {
        if (this.filters.disableFiltering) {
            return null;
        }
        FiltersDescription filtersDesc = this.filters.getInstance().getDescription();
        JMenu menu = new JMenu(NbBundle.getMessage(FilterSubmenuAction.class, (String)"LBL_FilterSubmenu"));
        JCheckBoxMenuItem menuItem = null;
        String filterName = null;
        for (int i = 0; i < filtersDesc.getFilterCount(); ++i) {
            filterName = filtersDesc.getName(i);
            menuItem = new JCheckBoxMenuItem(filtersDesc.getDisplayName(i), this.filters.getInstance().isSelected(filterName));
            menuItem.addActionListener(this);
            menuItem.putClientProperty("nbFilterName", filterName);
            menu.add(menuItem);
        }
        return menu;
    }
}

