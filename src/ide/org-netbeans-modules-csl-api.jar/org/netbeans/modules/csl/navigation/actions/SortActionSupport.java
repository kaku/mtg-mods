/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.csl.navigation.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import org.netbeans.modules.csl.navigation.ClassMemberFilters;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public abstract class SortActionSupport
extends AbstractAction
implements Presenter.Popup {
    private JRadioButtonMenuItem menuItem;
    protected ClassMemberFilters filters;

    public SortActionSupport(ClassMemberFilters filters) {
        this.filters = filters;
    }

    public final JMenuItem getPopupPresenter() {
        JRadioButtonMenuItem result = this.obtainMenuItem();
        this.updateMenuItem();
        return result;
    }

    protected final JRadioButtonMenuItem obtainMenuItem() {
        if (this.menuItem == null) {
            this.menuItem = new JRadioButtonMenuItem((String)this.getValue("Name"));
            this.menuItem.setAction(this);
        }
        return this.menuItem;
    }

    protected abstract void updateMenuItem();

    public static final class SortBySourceAction
    extends SortActionSupport {
        public SortBySourceAction(ClassMemberFilters filters) {
            super(filters);
            this.putValue("Name", NbBundle.getMessage(SortBySourceAction.class, (String)"LBL_SortBySource"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.filters.setNaturalSort(true);
            this.updateMenuItem();
        }

        @Override
        protected void updateMenuItem() {
            JRadioButtonMenuItem mi = this.obtainMenuItem();
            mi.setSelected(this.filters.isNaturalSort());
        }
    }

    public static final class SortByNameAction
    extends SortActionSupport {
        public SortByNameAction(ClassMemberFilters filters) {
            super(filters);
            this.putValue("Name", NbBundle.getMessage(SortByNameAction.class, (String)"LBL_SortByName"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.filters.setNaturalSort(false);
            this.updateMenuItem();
        }

        @Override
        protected void updateMenuItem() {
            JRadioButtonMenuItem mi = this.obtainMenuItem();
            mi.setSelected(!this.filters.isNaturalSort());
        }
    }

}

