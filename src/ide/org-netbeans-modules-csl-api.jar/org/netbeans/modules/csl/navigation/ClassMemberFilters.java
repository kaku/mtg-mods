/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.navigation;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.navigation.ClassMemberPanelUI;
import org.netbeans.modules.csl.navigation.ElementNode;
import org.netbeans.modules.csl.navigation.base.FiltersDescription;
import org.netbeans.modules.csl.navigation.base.FiltersManager;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class ClassMemberFilters {
    private ClassMemberPanelUI ui;
    private static final String SHOW_NON_PUBLIC = "show_non_public";
    private static final String SHOW_STATIC = "show_static";
    private static final String SHOW_FIELDS = "show_fields";
    private static final String SHOW_INHERITED = "show_inherited";
    private FiltersManager filters;
    private boolean naturalSort = false;
    public boolean disableFiltering = false;

    ClassMemberFilters(ClassMemberPanelUI ui) {
        this.ui = ui;
    }

    public FiltersManager getInstance() {
        if (this.filters == null) {
            this.filters = ClassMemberFilters.createFilters();
        }
        return this.filters;
    }

    public JComponent getComponent() {
        FiltersManager f = this.getInstance();
        return f.getComponent();
    }

    public Collection<StructureItem> filter(List<StructureItem> original) {
        boolean non_public = this.filters.isSelected("show_non_public");
        boolean statik = this.filters.isSelected("show_static");
        boolean fields = this.filters.isSelected("show_fields");
        if (original == null || original.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        ArrayList<StructureItem> result = new ArrayList<StructureItem>(original.size());
        if (!this.disableFiltering) {
            for (StructureItem description : original) {
                if (!non_public && (description.getModifiers().contains((Object)Modifier.PROTECTED) || description.getModifiers().contains((Object)Modifier.PRIVATE)) || !statik && description.getModifiers().contains((Object)Modifier.STATIC) || !fields && (description.getKind() == ElementKind.FIELD || description.getKind() == ElementKind.ATTRIBUTE)) continue;
                result.add(description);
            }
        } else {
            result.addAll(original);
        }
        Collections.sort(result, this.isNaturalSort() ? ElementNode.Description.POSITION_COMPARATOR : ElementNode.Description.ALPHA_COMPARATOR);
        return result;
    }

    public boolean isNaturalSort() {
        return this.naturalSort;
    }

    public void setNaturalSort(boolean naturalSort) {
        this.naturalSort = naturalSort;
        this.ui.sort();
    }

    private static FiltersManager createFilters() {
        FiltersDescription desc = new FiltersDescription();
        desc.addFilter("show_fields", NbBundle.getMessage(ClassMemberFilters.class, (String)"LBL_ShowFields"), NbBundle.getMessage(ClassMemberFilters.class, (String)"LBL_ShowFieldsTip"), true, new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/csl/navigation/resources/filterHideFields.gif")), null);
        desc.addFilter("show_static", NbBundle.getMessage(ClassMemberFilters.class, (String)"LBL_ShowStatic"), NbBundle.getMessage(ClassMemberFilters.class, (String)"LBL_ShowStaticTip"), true, new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/csl/navigation/resources/filterHideStatic.png")), null);
        desc.addFilter("show_non_public", NbBundle.getMessage(ClassMemberFilters.class, (String)"LBL_ShowNonPublic"), NbBundle.getMessage(ClassMemberFilters.class, (String)"LBL_ShowNonPublicTip"), true, new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/csl/navigation/resources/filterHideNonPublic.png")), null);
        return FiltersDescription.createManager(desc);
    }
}

