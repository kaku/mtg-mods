/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.navigation.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

public final class OpenAction
extends AbstractAction {
    private ElementHandle elementHandle;
    private FileObject fileObject;
    private long start;

    public OpenAction(ElementHandle elementHandle, FileObject fileObject, long start) {
        this.elementHandle = elementHandle;
        this.fileObject = fileObject;
        this.start = start;
        this.putValue("Name", NbBundle.getMessage(OpenAction.class, (String)"LBL_Goto"));
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Source js;
        if (this.fileObject != null && this.elementHandle == null) {
            UiUtils.open(this.fileObject, (int)this.start);
            return;
        }
        ElementHandle handle = this.elementHandle;
        FileObject primaryFile = DataLoadersBridge.getDefault().getPrimaryFile(this.fileObject);
        if (primaryFile != null && handle != null && (js = Source.create((FileObject)primaryFile)) != null) {
            UiUtils.open(js, handle);
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

