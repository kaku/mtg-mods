/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 */
package org.netbeans.modules.csl.navigation;

import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.navigation.CaretListeningTask;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public class CaretListeningFactory
extends AbstractTaskFactory {
    public CaretListeningFactory() {
        super(false);
    }

    @Override
    public Collection<? extends SchedulerTask> createTasks(Language l, Snapshot snapshot) {
        if (snapshot.getSource().getMimeType().equals(snapshot.getMimeType())) {
            return Collections.singleton(new CaretListeningTask());
        }
        return null;
    }
}

