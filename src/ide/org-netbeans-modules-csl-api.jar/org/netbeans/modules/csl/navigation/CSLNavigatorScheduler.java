/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.impl.indexing.Util
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.csl.navigation;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.netbeans.modules.csl.navigation.ClassMemberNavigatorSourceFactory;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public class CSLNavigatorScheduler
extends Scheduler {
    private RequestProcessor requestProcessor;

    public CSLNavigatorScheduler() {
        ClassMemberNavigatorSourceFactory.getInstance().addPropertyChangeListener(new AListener());
        this.refresh();
    }

    private void refresh() {
        if (this.requestProcessor == null) {
            this.requestProcessor = new RequestProcessor("CSLNavigatorScheduler");
        }
        this.requestProcessor.post(new Runnable(){

            @Override
            public void run() {
                FileObject fileObject;
                Source source;
                ClassMemberNavigatorSourceFactory f = ClassMemberNavigatorSourceFactory.getInstance();
                if (f != null && f.getContext() != null && (fileObject = (FileObject)f.getContext().lookup(FileObject.class)) != null && fileObject.isValid() && Util.canBeParsed((String)fileObject.getMIMEType()) && (source = Source.create((FileObject)fileObject)) != null) {
                    CSLNavigatorScheduler.this.schedule(source, new SchedulerEvent((Object)CSLNavigatorScheduler.this){});
                    return;
                }
                CSLNavigatorScheduler.this.schedule(null, null);
            }

        });
    }

    public String toString() {
        return "CSLNavigatorScheduler";
    }

    protected SchedulerEvent createSchedulerEvent(SourceModificationEvent event) {
        if (event.getModifiedSource() == this.getSource()) {
            return new SchedulerEvent((Object)this){};
        }
        return null;
    }

    private class AListener
    implements PropertyChangeListener {
        private AListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            CSLNavigatorScheduler.this.refresh();
        }
    }

}

