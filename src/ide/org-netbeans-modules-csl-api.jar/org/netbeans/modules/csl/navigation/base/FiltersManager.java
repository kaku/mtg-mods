/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.navigation.base;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import org.netbeans.modules.csl.navigation.base.FiltersDescription;

public final class FiltersManager {
    private FiltersComponent comp;

    static FiltersManager create(FiltersDescription descr) {
        return new FiltersManager(descr);
    }

    public boolean isSelected(String filterName) {
        return this.comp.isSelected(filterName);
    }

    public void setSelected(String filterName, boolean value) {
        this.comp.setFilterSelected(filterName, value);
    }

    public JComponent getComponent() {
        return this.comp;
    }

    public FiltersDescription getDescription() {
        return this.comp.getDescription();
    }

    public void hookChangeListener(FilterChangeListener l) {
        this.comp.hookFilterChangeListener(l);
    }

    private FiltersManager(FiltersDescription descr) {
        this.comp = new FiltersComponent(descr);
    }

    private class FiltersComponent
    extends Box
    implements ActionListener {
        private List<JToggleButton> toggles;
        private final FiltersDescription filtersDesc;
        private Object L_LOCK;
        private FilterChangeListener clientL;
        private Object STATES_LOCK;
        private Map<String, Boolean> filterStates;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isSelected(String filterName) {
            Boolean result;
            Object object = this.STATES_LOCK;
            synchronized (object) {
                if (this.filterStates == null) {
                    int index = this.filterIndexForName(filterName);
                    if (index < 0) {
                        return false;
                    }
                    return this.filtersDesc.isSelected(index);
                }
                result = this.filterStates.get(filterName);
            }
            if (result == null) {
                throw new IllegalArgumentException("Filter " + filterName + " not found.");
            }
            return result;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setFilterSelected(String filterName, boolean value) {
            assert (SwingUtilities.isEventDispatchThread());
            int index = this.filterIndexForName(filterName);
            if (index < 0) {
                throw new IllegalArgumentException("Filter " + filterName + " not found.");
            }
            this.toggles.get(index).setSelected(value);
            Object object = this.STATES_LOCK;
            synchronized (object) {
                this.filterStates.put(filterName, value);
            }
            this.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void hookFilterChangeListener(FilterChangeListener l) {
            Object object = this.L_LOCK;
            synchronized (object) {
                this.clientL = l;
            }
        }

        public FiltersDescription getDescription() {
            return this.filtersDesc;
        }

        FiltersComponent(FiltersDescription descr) {
            super(0);
            this.L_LOCK = new Object();
            this.STATES_LOCK = new Object();
            this.filtersDesc = descr;
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(new Runnable(FiltersManager.this){
                    final /* synthetic */ FiltersManager val$this$0;

                    @Override
                    public void run() {
                        FiltersComponent.this.initPanel();
                    }
                });
            } else {
                this.initPanel();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void initPanel() {
            this.setBorder(new EmptyBorder(1, 2, 3, 5));
            JToolBar toolbar = new JToolBar(0);
            toolbar.setFloatable(false);
            toolbar.setRollover(true);
            toolbar.setBorderPainted(false);
            int filterCount = this.filtersDesc.getFilterCount();
            this.toggles = new ArrayList<JToggleButton>(filterCount);
            JToggleButton toggleButton = null;
            HashMap<String, Boolean> fStates = new HashMap<String, Boolean>(filterCount * 2);
            for (int i = 0; i < filterCount; ++i) {
                toggleButton = this.createToggle(fStates, i);
                this.toggles.add(toggleButton);
            }
            Dimension space = new Dimension(3, 0);
            for (int i22 = 0; i22 < this.toggles.size(); ++i22) {
                JToggleButton curToggle = this.toggles.get(i22);
                curToggle.addActionListener(this);
                toolbar.add(curToggle);
                if (i22 == this.toggles.size() - 1) continue;
                toolbar.addSeparator(space);
            }
            this.add(toolbar);
            Object i22 = this.STATES_LOCK;
            synchronized (i22) {
                this.filterStates = fStates;
            }
        }

        private JToggleButton createToggle(Map<String, Boolean> fStates, int index) {
            boolean isSelected = this.filtersDesc.isSelected(index);
            Icon icon = this.filtersDesc.getSelectedIcon(index);
            JToggleButton result = new JToggleButton(icon, isSelected);
            Dimension size = new Dimension(icon.getIconWidth() + 6, icon.getIconHeight() + 4);
            result.setPreferredSize(size);
            result.setMargin(new Insets(2, 3, 2, 3));
            result.setToolTipText(this.filtersDesc.getTooltip(index));
            fStates.put(this.filtersDesc.getName(index), isSelected);
            return result;
        }

        private int filterIndexForName(String filterName) {
            int filterCount = this.filtersDesc.getFilterCount();
            for (int i = 0; i < filterCount; ++i) {
                String curName = this.filtersDesc.getName(i);
                if (!filterName.equals(curName)) continue;
                return i;
            }
            return -1;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            JToggleButton toggle = (JToggleButton)e.getSource();
            int index = this.toggles.indexOf(e.getSource());
            Object object = this.STATES_LOCK;
            synchronized (object) {
                this.filterStates.put(this.filtersDesc.getName(index), toggle.isSelected());
            }
            this.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void fireChange() {
            FilterChangeListener lCopy;
            Object object = this.L_LOCK;
            synchronized (object) {
                if (this.clientL == null) {
                    return;
                }
                lCopy = this.clientL;
            }
            lCopy.filterStateChanged(new ChangeEvent(FiltersManager.this));
        }

    }

    public static interface FilterChangeListener {
        public void filterStateChanged(ChangeEvent var1);
    }

}

