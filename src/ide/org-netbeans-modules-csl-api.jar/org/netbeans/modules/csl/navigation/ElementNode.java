/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.csl.navigation;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.core.GsfHtmlFormatter;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.navigation.ClassMemberFilters;
import org.netbeans.modules.csl.navigation.ClassMemberPanelUI;
import org.netbeans.modules.csl.navigation.ElementScanningTask;
import org.netbeans.modules.csl.navigation.Icons;
import org.netbeans.modules.csl.navigation.actions.OpenAction;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public class ElementNode
extends AbstractNode {
    private static Node WAIT_NODE;
    private OpenAction openAction;
    private StructureItem description;
    private ClassMemberPanelUI ui;
    private final FileObject fileObject;

    public ElementNode(StructureItem description, ClassMemberPanelUI ui, FileObject fileObject) {
        super(description.isLeaf() ? Children.LEAF : new ElementChildren(description, ui, fileObject), Lookups.fixed((Object[])new Object[]{fileObject}));
        this.description = description;
        this.setDisplayName(description.getName());
        this.ui = ui;
        this.fileObject = fileObject;
    }

    public Image getIcon(int type) {
        if (this.description.getCustomIcon() != null) {
            return ImageUtilities.icon2Image((Icon)this.description.getCustomIcon());
        }
        ImageIcon icon = Icons.getElementIcon(this.description.getKind(), this.description.getModifiers());
        if (icon != null) {
            return ImageUtilities.icon2Image((Icon)icon);
        }
        return super.getIcon(type);
    }

    public Image getOpenedIcon(int type) {
        return this.getIcon(type);
    }

    public String getDisplayName() {
        if (this.description.getName() == null) {
            return this.fileObject.getNameExt();
        }
        return this.description.getName();
    }

    public String getHtmlDisplayName() {
        return this.description.getHtml(new NavigatorFormatter());
    }

    public Action[] getActions(boolean context) {
        if (context || this.description.getName() == null) {
            return this.ui.getActions();
        }
        Action[] panelActions = this.ui.getActions();
        Action[] actions = new Action[2 + panelActions.length];
        actions[0] = this.getOpenAction();
        actions[1] = null;
        for (int i = 0; i < panelActions.length; ++i) {
            actions[2 + i] = panelActions[i];
        }
        return actions;
    }

    public Action getPreferredAction() {
        return this.getOpenAction();
    }

    private synchronized Action getOpenAction() {
        if (this.openAction == null) {
            FileObject fo = this.ui.getFileObject();
            try {
                this.openAction = new OpenAction(this.description.getElementHandle(), fo, this.description.getPosition());
            }
            catch (UnsupportedOperationException uo) {
                return null;
            }
        }
        return this.openAction;
    }

    static synchronized Node getWaitNode() {
        if (WAIT_NODE == null) {
            WAIT_NODE = new WaitNode();
        }
        return WAIT_NODE;
    }

    public void refreshRecursively() {
        ArrayList<Node> toExpand = new ArrayList<Node>();
        this.refreshRecursively(Collections.singleton(this), toExpand);
        this.ui.performExpansion(toExpand, Collections.<Node>emptyList());
    }

    private void refreshRecursively(Collection<ElementNode> toDo, Collection<Node> toExpand) {
        for (ElementNode elnod : toDo) {
            Children ch = elnod.getChildren();
            if (!(ch instanceof ElementChildren)) continue;
            ((ElementChildren)ch).resetKeys(elnod.description.getNestedItems(), elnod.ui.getFilters());
            List<Node> children = Arrays.asList(ch.getNodes());
            toExpand.addAll(children);
            this.refreshRecursively(children, toExpand);
        }
    }

    public ElementNode getMimeRootNodeForOffset(ParserResult info, int offset) {
        if (this.getDescription().getPosition() > (long)offset) {
            return null;
        }
        Document document = info.getSnapshot().getSource().getDocument(false);
        if (document == null) {
            return null;
        }
        BaseDocument doc = (BaseDocument)document;
        return this.getMimeRootNodeForOffset(doc, offset);
    }

    ElementNode getMimeRootNodeForOffset(BaseDocument doc, int offset) {
        Children ch;
        List<Language> languages = LanguageRegistry.getInstance().getEmbeddedLanguages(doc, offset);
        if (languages.size() > 0 && (ch = this.getChildren()) instanceof ElementChildren) {
            Node[] children = ch.getNodes();
            for (Language language : languages) {
                for (int i = 0; i < children.length; ++i) {
                    ElementNode c = (ElementNode)children[i];
                    if (!(c.getDescription() instanceof ElementScanningTask.MimetypeRootNode)) continue;
                    ElementScanningTask.MimetypeRootNode mr = (ElementScanningTask.MimetypeRootNode)c.getDescription();
                    if (mr.language != language) continue;
                    return c.getNodeForOffset(offset);
                }
            }
        }
        return this.getNodeForOffset(offset);
    }

    public ElementNode getNodeForOffset(int offset) {
        if (this.getDescription().getPosition() > (long)offset) {
            return null;
        }
        Children ch = this.getChildren();
        if (ch instanceof ElementChildren) {
            Node[] children = ch.getNodes();
            for (int i = 0; i < children.length; ++i) {
                long end;
                ElementNode c = (ElementNode)children[i];
                long start = c.getDescription().getPosition();
                if (start > (long)offset || (end = c.getDescription().getEndPosition()) < (long)offset) continue;
                return c.getNodeForOffset(offset);
            }
        }
        return this;
    }

    public void updateRecursively(StructureItem newDescription) {
        LinkedList<Node> nodesToExpand = new LinkedList<Node>();
        LinkedList<Node> nodesToExpandRec = new LinkedList<Node>();
        this.updateRecursively(newDescription, nodesToExpand, nodesToExpandRec);
        this.ui.performExpansion(nodesToExpand, nodesToExpandRec);
    }

    private void updateRecursively(StructureItem newDescription, List<Node> nodesToExpand, List<Node> nodesToExpandRec) {
        Object ch = this.getChildren();
        if (!(ch instanceof ElementChildren) && newDescription.getNestedItems() != null && newDescription.getNestedItems().size() > 0) {
            ch = new ElementChildren(this.ui, this.fileObject);
            this.setChildren((Children)ch);
        }
        if (ch instanceof ElementChildren) {
            HashSet<? extends StructureItem> oldSubs = new HashSet<StructureItem>(this.description.getNestedItems());
            Node[] nodes = ch.getNodes(true);
            HashMap<StructureItem, ElementNode> oldD2node = new HashMap<StructureItem, ElementNode>();
            for (Node node2 : nodes) {
                oldD2node.put(((ElementNode)node2).description, (ElementNode)node2);
            }
            ((ElementChildren)((Object)ch)).resetKeys(newDescription.getNestedItems(), this.ui.getFilters());
            nodes = ch.getNodes(true);
            boolean alreadyExpanded = false;
            block1 : for (StructureItem newSub : newDescription.getNestedItems()) {
                ElementNode node = (ElementNode)((Object)oldD2node.get(newSub));
                if (node != null) {
                    if (!oldSubs.contains(newSub)) {
                        nodesToExpand.add((Node)node);
                    }
                    node.updateRecursively(newSub, nodesToExpand, nodesToExpandRec);
                    continue;
                }
                if (!alreadyExpanded) {
                    alreadyExpanded = true;
                    if (this.ui.isExpandedByDefault((Node)this)) {
                        nodesToExpand.add((Node)this);
                    }
                }
                for (Node newNode : nodes) {
                    if (!(newNode instanceof ElementNode) || ((ElementNode)newNode).getDescription() != newSub) continue;
                    nodesToExpandRec.add(newNode);
                    continue block1;
                }
            }
        }
        StructureItem oldDescription = this.description;
        this.description = newDescription;
        String oldHtml = oldDescription.getHtml(new NavigatorFormatter());
        String descHtml = this.description.getHtml(new NavigatorFormatter());
        if (oldHtml != null && !oldHtml.equals(descHtml)) {
            this.fireDisplayNameChange(oldHtml, descHtml);
        }
        if (oldDescription.getModifiers() != null && !oldDescription.getModifiers().equals(newDescription.getModifiers())) {
            this.fireIconChange();
            this.fireOpenedIconChange();
        }
    }

    public StructureItem getDescription() {
        return this.description;
    }

    public FileObject getFileObject() {
        return this.fileObject;
    }

    private static class NavigatorFormatter
    extends GsfHtmlFormatter {
        private NavigatorFormatter() {
        }

        @Override
        public void name(ElementKind kind, boolean start) {
        }
    }

    private static class WaitNode
    extends AbstractNode {
        private Image waitIcon = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/navigation/resources/wait.gif");
        private String displayName = NbBundle.getMessage(ElementNode.class, (String)"LBL_WaitNode");

        WaitNode() {
            super(Children.LEAF);
        }

        public Image getIcon(int type) {
            return this.waitIcon;
        }

        public Image getOpenedIcon(int type) {
            return this.getIcon(type);
        }

        public String getDisplayName() {
            return this.displayName;
        }
    }

    static class Description {
        public static final Comparator<StructureItem> ALPHA_COMPARATOR = new DescriptionComparator(true);
        public static final Comparator<StructureItem> POSITION_COMPARATOR = new DescriptionComparator(false);
        ClassMemberPanelUI ui;
        String name;
        ElementHandle elementHandle;
        ElementKind kind;
        Set<Modifier> modifiers;
        List<Description> subs;
        String htmlHeader;
        long pos;

        Description(ClassMemberPanelUI ui) {
            this.ui = ui;
        }

        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }
            if (!(o instanceof Description)) {
                return false;
            }
            Description d = (Description)o;
            if (this.kind != d.kind) {
                return false;
            }
            if (!this.name.equals(d.name)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 7;
            hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
            hash = 29 * hash + (this.kind != null ? this.kind.hashCode() : 0);
            return hash;
        }

        private static class DescriptionComparator
        implements Comparator<StructureItem> {
            boolean alpha;

            DescriptionComparator(boolean alpha) {
                this.alpha = alpha;
            }

            @Override
            public int compare(StructureItem d1, StructureItem d2) {
                if (this.alpha) {
                    if (this.k2i(d1.getKind()) != this.k2i(d2.getKind())) {
                        return this.k2i(d1.getKind()) - this.k2i(d2.getKind());
                    }
                    return d1.getSortText().compareTo(d2.getSortText());
                }
                return d1.getPosition() == d2.getPosition() ? 0 : (d1.getPosition() < d2.getPosition() ? -1 : 1);
            }

            int k2i(ElementKind kind) {
                switch (kind) {
                    case CONSTRUCTOR: {
                        return 1;
                    }
                    case METHOD: 
                    case DB: {
                        return 2;
                    }
                    case FIELD: {
                        return 3;
                    }
                }
                return 100;
            }
        }

    }

    private static final class ElementChildren
    extends Children.Keys<StructureItem> {
        private ClassMemberPanelUI ui;
        private FileObject fileObject;
        private StructureItem parent;

        protected void addNotify() {
            Children.Keys.super.addNotify();
            if (this.parent != null) {
                this.resetKeys(this.parent.getNestedItems(), this.ui.getFilters());
            }
        }

        public ElementChildren(ClassMemberPanelUI ui, FileObject fileObject) {
            this.ui = ui;
            this.fileObject = fileObject;
        }

        public ElementChildren(StructureItem parent, ClassMemberPanelUI ui, FileObject fileObject) {
            this.parent = parent;
            this.ui = ui;
            this.fileObject = fileObject;
        }

        protected Node[] createNodes(StructureItem key) {
            return new Node[]{new ElementNode(key, this.ui, this.fileObject)};
        }

        void resetKeys(List<StructureItem> descriptions, ClassMemberFilters filters) {
            this.setKeys(filters.filter(descriptions));
        }
    }

}

