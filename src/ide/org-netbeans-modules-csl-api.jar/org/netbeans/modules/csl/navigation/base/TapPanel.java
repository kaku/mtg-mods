/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.navigation.base;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.ref.WeakReference;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.modules.csl.navigation.base.TrivialLayout;

public final class TapPanel
extends JPanel {
    public static final int UP = 0;
    public static final int DOWN = 2;
    public static final String PROP_ORIENTATION = "orientation";
    private int orientation = 0;
    private boolean armed = false;
    private boolean expanded = true;
    private int minimumHeight = 8;
    private static WeakReference<Adap> adapRef = null;
    private Icon up;
    private Icon down;
    private int ICON_SIZE;

    public TapPanel() {
        this.up = new UpIcon();
        this.down = new DownIcon();
        this.ICON_SIZE = 8;
        this.setLayout(new TrivialLayout());
    }

    private static Adap getAdapter() {
        Adap result = null;
        if (adapRef != null) {
            result = adapRef.get();
        }
        if (result == null) {
            result = new Adap();
            adapRef = new WeakReference<Adap>(result);
        }
        return result;
    }

    void setSecondaryMouseHandler(MouseListener lis) {
        TapPanel.getAdapter().other = lis;
    }

    @Override
    public void addNotify() {
        this.addMouseMotionListener(TapPanel.getAdapter());
        this.addMouseListener(TapPanel.getAdapter());
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.removeMouseMotionListener(TapPanel.getAdapter());
        this.removeMouseListener(TapPanel.getAdapter());
    }

    public int getOrientation() {
        return this.orientation;
    }

    public void setOrientation(int i) {
        if (i != this.orientation) {
            int oldOr = i;
            this.orientation = i;
            this.firePropertyChange("orientation", oldOr, i);
        }
    }

    private void setArmed(boolean val) {
        if (val != this.armed) {
            this.armed = val;
            this.repaint();
        }
    }

    public boolean isExpanded() {
        return this.expanded;
    }

    @Override
    public Dimension getPreferredSize() {
        return this.getLayout().preferredLayoutSize(this);
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension d = this.getPreferredSize();
        d.width = 20;
        return d;
    }

    @Override
    public Dimension getMaximumSize() {
        return this.getPreferredSize();
    }

    public void setExpanded(boolean b) {
        if (this.expanded != b) {
            Dimension d = this.getPreferredSize();
            this.expanded = b;
            Dimension d1 = this.getPreferredSize();
            if (this.isDisplayable()) {
                this.revalidate();
            }
        }
    }

    private boolean isArmPoint(Point p) {
        if (!this.expanded) {
            return p.y > 0 && p.y < this.getHeight();
        }
        if (this.orientation == 0) {
            return p.y > this.getHeight() - this.minimumHeight;
        }
        return p.y < this.minimumHeight;
    }

    public void updateBorder() {
        if (this.orientation == 0) {
            super.setBorder(BorderFactory.createEmptyBorder(0, 0, this.minimumHeight, 0));
        } else {
            super.setBorder(BorderFactory.createEmptyBorder(this.minimumHeight, 0, 0, 0));
        }
    }

    public int getMinimumHeight() {
        return this.minimumHeight;
    }

    public void setBorder() {
    }

    @Override
    public void paintBorder(Graphics g) {
        Color c;
        Color color = c = this.armed ? UIManager.getColor("List.selectionBackground") : this.getBackground();
        if (c == null) {
            c = this.getBackground();
        }
        int x = 0;
        int y = this.orientation == 0 ? 1 + (this.getHeight() - this.minimumHeight) : 0;
        int w = this.getWidth();
        int h = this.minimumHeight - 1;
        g.setColor(c);
        g.fillRect(x, y, w, h);
        int pos = this.orientation == 0 ? this.getHeight() - 1 : 0;
        int dir = this.orientation == 0 ? -1 : 1;
        g.setColor(this.armed ? c.darker() : UIManager.getColor("controlShadow"));
        g.drawLine(0, pos, w, pos);
        pos += dir;
        if (this.orientation == 0 == this.expanded) {
            this.up.paintIcon(this, g, this.getWidth() / 2 - this.up.getIconWidth() / 2, this.getHeight() - (this.minimumHeight + (this.expanded ? 0 : -1)));
        } else {
            this.down.paintIcon(this, g, this.getWidth() / 2 - this.up.getIconWidth() / 2, this.expanded ? 2 : 1);
        }
    }

    @Override
    public void paintChildren(Graphics g) {
        if (!this.expanded) {
            return;
        }
        super.paintChildren(g);
    }

    private class DownIcon
    implements Icon {
        private DownIcon() {
        }

        @Override
        public int getIconHeight() {
            return TapPanel.this.ICON_SIZE - 3;
        }

        @Override
        public int getIconWidth() {
            return TapPanel.this.ICON_SIZE + 2;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(TapPanel.this.armed ? UIManager.getColor("List.selectionForeground") : UIManager.getColor("controlShadow"));
            int[] xPoints = new int[]{x, x + 8, ++x + 4};
            int[] yPoints = new int[]{y, y, y + 4};
            g.fillPolygon(xPoints, yPoints, 3);
        }
    }

    private class UpIcon
    implements Icon {
        private UpIcon() {
        }

        @Override
        public int getIconHeight() {
            return TapPanel.this.ICON_SIZE - 2;
        }

        @Override
        public int getIconWidth() {
            return TapPanel.this.ICON_SIZE + 2;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            g.setColor(TapPanel.this.armed ? UIManager.getColor("List.selectionForeground") : UIManager.getColor("controlShadow"));
            int[] xPoints = new int[]{x, x + 8, x + 4};
            int[] yPoints = new int[]{y + 5, y + 5, y};
            g.fillPolygon(xPoints, yPoints, 3);
        }
    }

    static class Adap
    extends MouseAdapter
    implements MouseMotionListener {
        MouseListener other = null;

        Adap() {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            ((TapPanel)e.getSource()).setArmed(true);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            ((TapPanel)e.getSource()).setArmed(false);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            ((TapPanel)e.getSource()).setArmed(((TapPanel)e.getSource()).isArmPoint(e.getPoint()));
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (((TapPanel)e.getSource()).isArmPoint(e.getPoint())) {
                ((TapPanel)e.getSource()).setExpanded(!((TapPanel)e.getSource()).isExpanded());
                e.consume();
            } else if (this.other != null) {
                this.other.mousePressed(e);
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
        }
    }

}

