/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.navigation.base;

public interface ModelBusyListener {
    public void busyStart();

    public void busyEnd();

    public void newContentReady();
}

