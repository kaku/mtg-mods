/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.spi.navigator.NavigatorPanel
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.csl.navigation;

import java.util.Collection;
import javax.swing.JComponent;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.navigation.ClassMemberNavigatorSourceFactory;
import org.netbeans.modules.csl.navigation.ClassMemberPanelUI;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class ClassMemberPanel
implements NavigatorPanel {
    private ClassMemberPanelUI component;
    private static ClassMemberPanel INSTANCE;
    private static final RequestProcessor RP;
    private Lookup.Result selection;
    private final LookupListener selectionListener;

    public ClassMemberPanel() {
        this.selectionListener = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                if (ClassMemberPanel.this.selection == null) {
                    return;
                }
                ClassMemberNavigatorSourceFactory f = ClassMemberNavigatorSourceFactory.getInstance();
                if (f != null) {
                    f.firePropertyChangeEvent();
                }
            }
        };
    }

    public void panelActivated(final Lookup context) {
        assert (context != null);
        INSTANCE = this;
        this.getClassMemberPanelUI().showWaitNode();
        for (Scheduler s : Lookup.getDefault().lookupAll(Scheduler.class)) {
        }
        this.selection = context.lookup(new Lookup.Template(DataObject.class));
        this.selection.addLookupListener(this.selectionListener);
        RP.post(new Runnable(){

            @Override
            public void run() {
                ClassMemberNavigatorSourceFactory f;
                FileObject fileObject = (FileObject)context.lookup(FileObject.class);
                Language language = null;
                if (fileObject != null) {
                    language = LanguageRegistry.getInstance().getLanguageByMimeType(fileObject.getMIMEType());
                }
                if ((f = ClassMemberNavigatorSourceFactory.getInstance()) != null) {
                    f.setLookup(context, ClassMemberPanel.this.getClassMemberPanelUI(language));
                }
            }
        });
    }

    public void panelDeactivated() {
        this.getClassMemberPanelUI().showWaitNode();
        INSTANCE = null;
        if (this.selection != null) {
            this.selection.removeLookupListener(this.selectionListener);
            this.selection = null;
        }
        RP.post(new Runnable(){

            @Override
            public void run() {
                ClassMemberNavigatorSourceFactory f = ClassMemberNavigatorSourceFactory.getInstance();
                if (f != null) {
                    f.setLookup(Lookup.EMPTY, null);
                }
            }
        });
    }

    public Lookup getLookup() {
        return this.getClassMemberPanelUI().getLookup();
    }

    public String getDisplayName() {
        return NbBundle.getMessage(ClassMemberPanel.class, (String)"LBL_members");
    }

    public String getDisplayHint() {
        return NbBundle.getMessage(ClassMemberPanel.class, (String)"HINT_members");
    }

    public JComponent getComponent() {
        return this.getClassMemberPanelUI();
    }

    public void selectElement(ParserResult info, int offset) {
        this.getClassMemberPanelUI().selectElementNode(info, offset);
    }

    private synchronized ClassMemberPanelUI getClassMemberPanelUI(Language language) {
        if (this.component == null) {
            this.component = new ClassMemberPanelUI(language);
        }
        return this.component;
    }

    private ClassMemberPanelUI getClassMemberPanelUI() {
        return this.getClassMemberPanelUI(null);
    }

    public static ClassMemberPanel getInstance() {
        return INSTANCE;
    }

    static {
        RP = new RequestProcessor(ClassMemberPanel.class.getName(), 1);
    }

}

