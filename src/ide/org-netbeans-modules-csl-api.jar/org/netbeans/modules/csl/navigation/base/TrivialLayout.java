/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.navigation.base;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import org.netbeans.modules.csl.navigation.base.TapPanel;

final class TrivialLayout
implements LayoutManager {
    TrivialLayout() {
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void removeLayoutComponent(Component comp) {
    }

    @Override
    public void layoutContainer(Container parent) {
        if (parent instanceof TapPanel) {
            this.layoutTapPanel((TapPanel)parent);
        } else {
            this.layoutComp(parent);
        }
    }

    private void layoutComp(Container parent) {
        Component[] c = parent.getComponents();
        if (c.length > 1) {
            Dimension d1 = c[0].getPreferredSize();
            Dimension d2 = c[1].getPreferredSize();
            int labely = 0;
            d1.width += 10;
            if (d2.height > d1.height) {
                labely = d2.height / 2 - d1.height / 2;
            }
            if (parent.getWidth() - d1.width < d2.width) {
                c[0].setBounds(0, 0, 0, 0);
                c[1].setBounds(0, 0, parent.getWidth(), parent.getHeight());
            } else {
                c[0].setBounds(0, labely, d1.width, d1.height);
                c[1].setBounds(d1.width + 1, 0, parent.getWidth() - d1.width, Math.min(parent.getHeight(), d2.height));
            }
        }
    }

    private void layoutTapPanel(TapPanel tp) {
        Component[] c = tp.getComponents();
        if (c.length > 1) {
            Dimension d1 = c[0].getPreferredSize();
            Dimension d2 = c[1].getPreferredSize();
            int labely = 0;
            if (d2.height > d1.height) {
                labely = d2.height / 2 - d1.height / 2 + 2;
            }
            if (tp.isExpanded()) {
                int top = tp.getOrientation() == 0 ? 0 : tp.getMinimumHeight();
                int height = Math.min(tp.getHeight() - tp.getMinimumHeight(), d2.height);
                if (tp.getWidth() - d1.width < d2.width) {
                    c[0].setBounds(0, 0, 0, 0);
                    c[1].setBounds(0, top, tp.getWidth(), height);
                } else {
                    c[0].setBounds(0, top + labely, d1.width, d1.height);
                    c[1].setBounds(d1.width + 1, top, tp.getWidth() - d1.width, height);
                }
            } else {
                c[0].setBounds(0, 0, 0, 0);
                c[1].setBounds(0, 0, 0, 0);
            }
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        Dimension result = new Dimension(20, 10);
        Component[] c = parent.getComponents();
        TapPanel tp = (TapPanel)parent;
        if (c.length > 1) {
            Dimension d1 = c[0].getPreferredSize();
            Dimension d2 = c[1].getPreferredSize();
            result.width = d1.width + d2.width;
            result.height = tp.isExpanded() ? Math.max(d1.height, d2.height) + tp.getMinimumHeight() : tp.getMinimumHeight();
        }
        return result;
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return this.minimumLayoutSize(parent);
    }
}

