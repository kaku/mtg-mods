/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.csl.navigation;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.navigation.CSLNavigatorScheduler;
import org.netbeans.modules.csl.navigation.ClassMemberPanelUI;
import org.netbeans.modules.csl.navigation.ElementScanningTask;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.util.Lookup;

public final class ClassMemberNavigatorSourceFactory
extends AbstractTaskFactory {
    private static ClassMemberNavigatorSourceFactory instance = null;
    private ClassMemberPanelUI ui;
    private PropertyChangeListener listener;
    private Lookup context;

    public static synchronized ClassMemberNavigatorSourceFactory getInstance() {
        if (instance == null) {
            instance = new ClassMemberNavigatorSourceFactory();
        }
        return instance;
    }

    private ClassMemberNavigatorSourceFactory() {
        super(true);
    }

    @Override
    public Collection<? extends SchedulerTask> createTasks(Language l, Snapshot snapshot) {
        return Collections.singleton(new ProxyElementScanningTask(CSLNavigatorScheduler.class));
    }

    public synchronized void setLookup(Lookup l, ClassMemberPanelUI ui) {
        this.ui = ui;
        this.context = l;
        this.firePropertyChangeEvent();
    }

    public void firePropertyChangeEvent() {
        if (this.listener != null) {
            this.listener.propertyChange(null);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.listener = l;
    }

    Lookup getContext() {
        return this.context;
    }

    private final class ProxyElementScanningTask
    extends IndexingAwareParserResultTask<ParserResult> {
        private ElementScanningTask task;
        private Class<? extends Scheduler> clazz;

        public ProxyElementScanningTask(Class<? extends Scheduler> c) {
            super(TaskIndexingMode.ALLOWED_DURING_SCAN);
            this.task = null;
            this.clazz = c;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private ElementScanningTask getTask() {
            ClassMemberNavigatorSourceFactory classMemberNavigatorSourceFactory = ClassMemberNavigatorSourceFactory.this;
            synchronized (classMemberNavigatorSourceFactory) {
                if (this.task == null && ClassMemberNavigatorSourceFactory.this.ui != null) {
                    this.task = ClassMemberNavigatorSourceFactory.this.ui.getTask();
                }
                return this.task;
            }
        }

        public void cancel() {
            ElementScanningTask t = this.getTask();
            if (t != null) {
                t.cancel();
            }
        }

        public void run(ParserResult result, SchedulerEvent event) {
            ElementScanningTask t = this.getTask();
            if (t != null) {
                t.run((Parser.Result)result, event);
            }
        }

        public int getPriority() {
            return Integer.MAX_VALUE;
        }

        public Class<? extends Scheduler> getSchedulerClass() {
            return this.clazz;
        }
    }

}

