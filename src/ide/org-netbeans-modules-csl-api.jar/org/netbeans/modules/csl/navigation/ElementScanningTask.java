/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.csl.navigation;

import java.awt.Image;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.navigation.ElementNode;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;

public abstract class ElementScanningTask
extends IndexingAwareParserResultTask<ParserResult> {
    private static final Logger LOG = Logger.getLogger(ElementScanningTask.class.getName());
    private boolean canceled;
    private static final Map<Snapshot, Reference<ResultStructure>> lastResults = new WeakHashMap<Snapshot, Reference<ResultStructure>>();

    public ElementScanningTask() {
        super(TaskIndexingMode.ALLOWED_DURING_SCAN);
    }

    public static List<? extends StructureItem> findCachedStructure(Snapshot s, Parser.Result r) {
        if (!(r instanceof ParserResult)) {
            return null;
        }
        Reference<ResultStructure> previousRef = lastResults.get((Object)s);
        if (previousRef == null) {
            return null;
        }
        ResultStructure cached = previousRef.get();
        if (cached == null || cached.result != r) {
            lastResults.remove((Object)s);
            return null;
        }
        return cached.structure;
    }

    public static void markProcessed(Parser.Result r, List<? extends StructureItem> structure) {
        lastResults.put(r.getSnapshot(), new WeakReference<ResultStructure>(new ResultStructure(r, structure)));
    }

    protected final StructureItem computeStructureRoot(Source source) {
        final FileObject fileObject = source.getFileObject();
        if (fileObject == null) {
            return null;
        }
        final int[] mimetypesWithElements = new int[]{0};
        final ArrayList roots = new ArrayList();
        try {
            ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    StructureScanner scanner;
                    Parser.Result r;
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(resultIterator.getSnapshot().getMimeType());
                    if (language != null && (scanner = language.getStructure()) != null && (r = resultIterator.getParserResult()) instanceof ParserResult) {
                        List<? extends StructureItem> children = ElementScanningTask.findCachedStructure(resultIterator.getSnapshot(), r);
                        if (children == null) {
                            long startTime = System.currentTimeMillis();
                            children = scanner.scan((ParserResult)r);
                            long endTime = System.currentTimeMillis();
                            Logger.getLogger("TIMER").log(Level.FINE, "Structure (" + language.getMimeType() + ")", new Object[]{fileObject, endTime - startTime});
                        }
                        if (children.size() > 0) {
                            int[] arrn = mimetypesWithElements;
                            arrn[0] = arrn[0] + 1;
                        }
                        ElementScanningTask.markProcessed(r, children);
                        roots.add(new MimetypeRootNode(language, children, resultIterator.getSnapshot().getMimePath()));
                    }
                    for (Embedding e : resultIterator.getEmbeddings()) {
                        this.run(resultIterator.getResultIterator(e));
                    }
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
        HashMap<String, MimetypeRootNode> map = new HashMap<String, MimetypeRootNode>();
        for (MimetypeRootNode mtRootNode : roots) {
            MimePath path = mtRootNode.getMimePath();
            String mimeType = path.getMimeType(path.size() - 1);
            MimetypeRootNode node = (MimetypeRootNode)map.get(mimeType);
            if (node != null) {
                MimePath nodeMimePath = node.getMimePath();
                if (path.size() >= nodeMimePath.size()) continue;
                map.put(mimeType, mtRootNode);
                continue;
            }
            map.put(mimeType, mtRootNode);
        }
        roots.clear();
        roots.addAll(map.values());
        if (roots.size() > 1) {
            Collections.sort(roots, new Comparator<MimetypeRootNode>(){

                @Override
                public int compare(MimetypeRootNode o1, MimetypeRootNode o2) {
                    return o1.getSortText().compareTo(o2.getSortText());
                }
            });
        }
        ArrayList<? extends StructureItem> items = new ArrayList<StructureItem>();
        if (mimetypesWithElements[0] > 1) {
            for (MimetypeRootNode root : roots) {
                items.add(root);
            }
        } else {
            for (MimetypeRootNode root : roots) {
                items.addAll(root.getNestedItems());
            }
        }
        return new RootStructureItem(items);
    }

    public synchronized void cancel() {
        this.canceled = true;
    }

    public synchronized void resume() {
        this.canceled = false;
    }

    public synchronized boolean isCancelled() {
        return this.canceled;
    }

    static final class MimetypeRootNode
    implements StructureItem {
        private static final String CSS_MIMETYPE = "text/css";
        private static final String CSS_SORT_TEXT = "2";
        private static final String JAVASCRIPT_MIMETYPE = "text/javascript";
        private static final String RUBY_MIMETYPE = "text/x-ruby";
        private static final String YAML_MIMETYPE = "text/x-yaml";
        private static final String PHP_MIME_TYPE = "text/x-php5";
        private static final String PHP_SORT_TEXT = "0";
        private static final String JAVASCRIPT_SORT_TEXT = "1";
        private static final String HTML_MIMETYPE = "text/html";
        private static final String HTML_SORT_TEXT = "3";
        private static final String YAML_SORT_TEXT = "4";
        private static final String RUBY_SORT_TEXT = "5";
        private static final String OTHER_SORT_TEXT = "9";
        Language language;
        private List<? extends StructureItem> items;
        long from;
        long to;
        private MimePath mimePath;

        private MimetypeRootNode(Language lang, List<? extends StructureItem> items, MimePath mimePath) {
            this.language = lang;
            this.items = new ArrayList<StructureItem>(items);
            Collections.sort(items, ElementNode.Description.POSITION_COMPARATOR);
            this.from = items.size() > 0 ? items.get(0).getPosition() : 0;
            this.to = items.size() > 0 ? items.get(items.size() - 1).getEndPosition() : 0;
            this.mimePath = mimePath;
        }

        public MimePath getMimePath() {
            return this.mimePath;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof MimetypeRootNode)) {
                return false;
            }
            MimetypeRootNode compared = (MimetypeRootNode)o;
            return this.language.equals(compared.language);
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + (this.language != null ? this.language.hashCode() : 0);
            return hash;
        }

        @Override
        public String getName() {
            return this.language.getDisplayName();
        }

        @Override
        public String getSortText() {
            if (this.language.getMimeType().equals("text/css")) {
                return "2";
            }
            if (this.language.getMimeType().equals("text/javascript")) {
                return "1";
            }
            if (this.language.getMimeType().equals("text/html")) {
                return "3";
            }
            if (this.language.getMimeType().equals("text/x-yaml")) {
                return "4";
            }
            if (this.language.getMimeType().equals("text/x-ruby")) {
                return "5";
            }
            if (this.language.getMimeType().equals("text/x-php5")) {
                return "0";
            }
            return "9" + this.getName();
        }

        @Override
        public String getHtml(HtmlFormatter formatter) {
            return this.getName();
        }

        @Override
        public ElementHandle getElementHandle() {
            return null;
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.OTHER;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public boolean isLeaf() {
            return false;
        }

        @Override
        public List<? extends StructureItem> getNestedItems() {
            return this.items;
        }

        @Override
        public long getPosition() {
            return this.from;
        }

        @Override
        public long getEndPosition() {
            return this.to;
        }

        @Override
        public ImageIcon getCustomIcon() {
            String iconBase = this.language.getIconBase();
            return iconBase == null ? null : new ImageIcon(ImageUtilities.loadImage((String)iconBase));
        }
    }

    private static final class RootStructureItem
    implements StructureItem {
        private final List<? extends StructureItem> items;

        public RootStructureItem(List<? extends StructureItem> items) {
            this.items = items;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getHtml(HtmlFormatter formatter) {
            return null;
        }

        @Override
        public ElementHandle getElementHandle() {
            return null;
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.OTHER;
        }

        @Override
        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        @Override
        public boolean isLeaf() {
            return false;
        }

        @Override
        public List<? extends StructureItem> getNestedItems() {
            return this.items;
        }

        @Override
        public long getPosition() {
            return 0;
        }

        @Override
        public long getEndPosition() {
            return Long.MAX_VALUE;
        }

        @Override
        public ImageIcon getCustomIcon() {
            return null;
        }

        @Override
        public String getSortText() {
            return null;
        }
    }

    private static class ResultStructure {
        private Parser.Result result;
        private List<? extends StructureItem> structure;

        public ResultStructure(Parser.Result result, List<? extends StructureItem> structure) {
            this.result = result;
            this.structure = structure;
        }
    }

}

