/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.csl.navigation;

import java.awt.Image;
import java.util.Collection;
import java.util.Collections;
import javax.swing.ImageIcon;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.openide.util.ImageUtilities;

public final class Icons {
    private static final String ICON_BASE = "org/netbeans/modules/csl/source/resources/icons/";
    private static final String GIF_EXTENSION = ".gif";
    private static final String PNG_EXTENSION = ".png";
    private static final String WAIT = "org/netbeans/modules/csl/source/resources/icons/wait.png";

    private Icons() {
    }

    public static ImageIcon getElementIcon(ElementKind elementKind, Collection<Modifier> modifiers) {
        if (modifiers == null) {
            modifiers = Collections.emptyList();
        }
        Image img = null;
        switch (elementKind) {
            case FILE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/emptyfile-icon.png");
                break;
            }
            case ERROR: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/error-glyph.gif");
                break;
            }
            case PACKAGE: 
            case MODULE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/package.gif");
                break;
            }
            case TEST: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/test.png");
                break;
            }
            case CLASS: 
            case INTERFACE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/class.png");
                break;
            }
            case TAG: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/html_element.png");
                break;
            }
            case RULE: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/rule.png");
                break;
            }
            case VARIABLE: 
            case PROPERTY: 
            case GLOBAL: 
            case ATTRIBUTE: 
            case FIELD: {
                img = ImageUtilities.loadImage((String)Icons.getIconName("org/netbeans/modules/csl/source/resources/icons/field", ".png", modifiers));
                break;
            }
            case PARAMETER: 
            case CONSTANT: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/constant.png");
                break;
            }
            case CONSTRUCTOR: {
                img = ImageUtilities.loadImage((String)Icons.getIconName("org/netbeans/modules/csl/source/resources/icons/constructor", ".png", modifiers));
                break;
            }
            case METHOD: {
                img = ImageUtilities.loadImage((String)Icons.getIconName("org/netbeans/modules/csl/source/resources/icons/method", ".png", modifiers));
                break;
            }
            case DB: {
                img = ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/database.gif");
                break;
            }
            default: {
                img = null;
            }
        }
        return img == null ? null : new ImageIcon(img);
    }

    private static String getIconName(String typeName, String extension, Collection<Modifier> modifiers) {
        StringBuffer fileName = new StringBuffer(typeName);
        if (modifiers.contains((Object)Modifier.STATIC)) {
            fileName.append("Static");
        }
        if (modifiers.contains((Object)Modifier.PROTECTED)) {
            return fileName.append("Protected").append(extension).toString();
        }
        if (modifiers.contains((Object)Modifier.PRIVATE)) {
            return fileName.append("Private").append(extension).toString();
        }
        return fileName.append("Public").append(extension).toString();
    }

}

