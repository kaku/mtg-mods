/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.csl.navigation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.navigation.CSLNavigatorScheduler;
import org.netbeans.modules.csl.navigation.ClassMemberFilters;
import org.netbeans.modules.csl.navigation.ElementNode;
import org.netbeans.modules.csl.navigation.ElementScanningTask;
import org.netbeans.modules.csl.navigation.actions.FilterSubmenuAction;
import org.netbeans.modules.csl.navigation.actions.SortActionSupport;
import org.netbeans.modules.csl.navigation.base.FiltersManager;
import org.netbeans.modules.csl.navigation.base.TapPanel;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class ClassMemberPanelUI
extends JPanel
implements ExplorerManager.Provider,
FiltersManager.FilterChangeListener {
    private static RequestProcessor RP = new RequestProcessor(ClassMemberPanelUI.class);
    private ExplorerManager manager = new ExplorerManager();
    private MyBeanTreeView elementView;
    private TapPanel filtersPanel;
    private JLabel filtersLbl;
    private Lookup lookup;
    private ClassMemberFilters filters;
    private Action[] actions = new Action[0];
    private Map<FileObject, Integer> positionRequests = new WeakHashMap<FileObject, Integer>();

    public ClassMemberPanelUI(Language language) {
        this.initComponents();
        this.elementView = this.createBeanTreeView();
        this.add((Component)((Object)this.elementView), "Center");
        this.filters = new ClassMemberFilters(this);
        this.filters.getInstance().hookChangeListener(this);
        RP.post((Runnable)new UpdateFilterState(language));
        this.filtersPanel = new TapPanel();
        this.filtersLbl = new JLabel(NbBundle.getMessage(ClassMemberPanelUI.class, (String)"LBL_Filter"));
        this.filtersLbl.setBorder(new EmptyBorder(0, 5, 5, 0));
        this.filtersPanel.add(this.filtersLbl);
        this.filtersPanel.setOrientation(2);
        KeyStroke toggleKey = KeyStroke.getKeyStroke(84, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());
        String keyText = Utilities.keyToString((KeyStroke)toggleKey);
        this.filtersPanel.setToolTipText(NbBundle.getMessage(ClassMemberPanelUI.class, (String)"TIP_TapPanel", (Object)keyText));
        this.filtersPanel.add(this.filters.getComponent());
        this.add((Component)this.filtersPanel, "South");
        this.manager.setRootContext(ElementNode.getWaitNode());
        this.lookup = ExplorerUtils.createLookup((ExplorerManager)this.manager, (ActionMap)this.getActionMap());
    }

    @Override
    public boolean requestFocusInWindow() {
        boolean result = super.requestFocusInWindow();
        this.elementView.requestFocusInWindow();
        return result;
    }

    @Override
    public void requestFocus() {
        super.requestFocus();
        this.elementView.requestFocus();
    }

    public Lookup getLookup() {
        return this.lookup;
    }

    public ElementScanningTask getTask() {
        return new ElementScanningTask(){

            public int getPriority() {
                return 20000;
            }

            public Class<? extends Scheduler> getSchedulerClass() {
                return CSLNavigatorScheduler.class;
            }

            public void run(ParserResult result, SchedulerEvent event) {
                this.resume();
                StructureItem root = this.computeStructureRoot(result.getSnapshot().getSource());
                FileObject file = result.getSnapshot().getSource().getFileObject();
                if (root != null && file != null) {
                    Document doc = result.getSnapshot().getSource().getDocument(false);
                    BaseDocument bd = doc instanceof BaseDocument ? (BaseDocument)doc : null;
                    ClassMemberPanelUI.this.refresh(root, file, bd);
                }
            }
        };
    }

    public void showWaitNode() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ClassMemberPanelUI.this.elementView.setRootVisible(true);
                ClassMemberPanelUI.this.manager.setRootContext(ElementNode.getWaitNode());
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void selectElementNode(ParserResult info, int offset) {
        ElementNode root = this.getRootNode();
        if (root == null) {
            return;
        }
        FileObject rootFo = root.getFileObject();
        FileObject sourceFo = info.getSnapshot().getSource().getFileObject();
        if (sourceFo != null && !sourceFo.equals((Object)rootFo)) {
            ClassMemberPanelUI classMemberPanelUI = this;
            synchronized (classMemberPanelUI) {
                this.positionRequests.put(sourceFo, offset);
            }
        } else {
            this.doSelectNodes(info, null, offset);
        }
    }

    private void doSelectNodes(final ParserResult info, final BaseDocument bd, final int offset) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ClassMemberPanelUI.this.doSelectNodes0(info, bd, offset);
            }
        });
    }

    private void doSelectNodes0(ParserResult info, BaseDocument bd, int offset) {
        ElementNode node;
        ElementNode rootNode = this.getRootNode();
        if (info != null && rootNode != null) {
            node = rootNode.getMimeRootNodeForOffset(info, offset);
        } else if (bd != null && rootNode != null) {
            node = rootNode.getMimeRootNodeForOffset(bd, offset);
        } else {
            return;
        }
        Node[] selectedNodes = this.manager.getSelectedNodes();
        if (selectedNodes == null || selectedNodes.length != 1 || selectedNodes[0] != node) {
            try {
                Node[] arrnode = new Node[1];
                arrnode[0] = node == null ? this.getRootNode() : node;
                this.manager.setSelectedNodes(arrnode);
            }
            catch (PropertyVetoException propertyVetoException) {
                Exceptions.printStackTrace((Throwable)propertyVetoException);
            }
        }
    }

    public void refresh(final StructureItem description, final FileObject fileObject, final BaseDocument bd) {
        final ElementNode rootNode = this.getRootNode();
        if (rootNode != null && rootNode.getFileObject().equals((Object)fileObject)) {
            Runnable r = new Runnable(){

                @Override
                public void run() {
                    long startTime = System.currentTimeMillis();
                    rootNode.updateRecursively(description);
                    long endTime = System.currentTimeMillis();
                    Logger.getLogger("TIMER").log(Level.FINE, "Navigator Merge", new Object[]{fileObject, endTime - startTime});
                }
            };
            RP.post(r);
        } else {
            Runnable r = new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    StructureScanner.Configuration configuration;
                    Integer offset;
                    StructureScanner scanner;
                    long startTime = System.currentTimeMillis();
                    ClassMemberPanelUI.this.elementView.setRootVisible(false);
                    ClassMemberPanelUI.this.elementView.setAutoWaitCursor(false);
                    ClassMemberPanelUI.this.manager.setRootContext((Node)new ElementNode(description, ClassMemberPanelUI.this, fileObject));
                    int expandDepth = -1;
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(fileObject.getMIMEType());
                    if (language != null && language.getStructure() != null && (configuration = (scanner = language.getStructure()).getConfiguration()) != null) {
                        expandDepth = configuration.getExpandDepth();
                    }
                    new UpdateFilterState(language).run();
                    final boolean scrollOnExpand = ClassMemberPanelUI.this.elementView.getScrollOnExpand();
                    ClassMemberPanelUI.this.elementView.setScrollOnExpand(false);
                    ClassMemberPanelUI.this.expandNodeByDefaultRecursively(ClassMemberPanelUI.this.manager.getRootContext(), 0, expandDepth);
                    Mutex.EVENT.writeAccess(new Runnable(){

                        @Override
                        public void run() {
                            ClassMemberPanelUI.this.elementView.setScrollOnExpand(scrollOnExpand);
                        }
                    });
                    ClassMemberPanelUI.this.elementView.setAutoWaitCursor(true);
                    long endTime = System.currentTimeMillis();
                    Logger.getLogger("TIMER").log(Level.FINE, "Navigator Initialization", new Object[]{fileObject, endTime - startTime});
                    ClassMemberPanelUI classMemberPanelUI = ClassMemberPanelUI.this;
                    synchronized (classMemberPanelUI) {
                        offset = (Integer)ClassMemberPanelUI.this.positionRequests.remove((Object)fileObject);
                    }
                    if (offset != null) {
                        ClassMemberPanelUI.this.doSelectNodes(null, bd, offset);
                    }
                }

            };
            RP.post(r);
        }
    }

    public void sort() {
        this.refreshRootRecursively();
    }

    public ClassMemberFilters getFilters() {
        return this.filters;
    }

    public void expandNode(Node n) {
        this.elementView.expandNode(n);
    }

    private void expandNodeByDefaultRecursively(Node node) {
        this.expandNodeByDefaultRecursively(node, 0, -1);
    }

    private void expandNodeByDefaultRecursively(Node node, int currentDepth, int maxDepth) {
        if (maxDepth >= 0 && currentDepth >= maxDepth) {
            return;
        }
        if (!this.expandNodeByDefault(node)) {
            return;
        }
        this.expandNode(node);
        for (Node subNode : node.getChildren().getNodes()) {
            this.expandNodeByDefaultRecursively(subNode, currentDepth + 1, maxDepth);
        }
    }

    private boolean expandNodeByDefault(Node node) {
        if (this.isExpandedByDefault(node)) {
            this.expandNode(node);
            return true;
        }
        return false;
    }

    void performExpansion(final Collection<Node> expand, final Collection<Node> expandRec) {
        Runnable r = new Runnable(){

            @Override
            public void run() {
                for (Node n2 : expand) {
                    ClassMemberPanelUI.this.expandNode(n2);
                }
                for (Node n2 : expandRec) {
                    ClassMemberPanelUI.this.expandNodeByDefaultRecursively(n2);
                }
            }
        };
        RP.post(r);
    }

    boolean isExpandedByDefault(Node node) {
        StructureItem item;
        if (node instanceof ElementNode && (item = ((ElementNode)node).getDescription()) instanceof StructureItem.CollapsedDefault && ((StructureItem.CollapsedDefault)item).isCollapsedByDefault()) {
            return false;
        }
        return true;
    }

    public Action[] getActions() {
        return this.actions;
    }

    public FileObject getFileObject() {
        return this.getRootNode().getFileObject();
    }

    @Override
    public void filterStateChanged(ChangeEvent e) {
        this.refreshRootRecursively();
    }

    private void refreshRootRecursively() {
        final ElementNode root = this.getRootNode();
        if (root != null) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    root.refreshRecursively();
                }
            });
        }
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
    }

    private ElementNode getRootNode() {
        Node n = this.manager.getRootContext();
        if (n instanceof ElementNode) {
            return (ElementNode)n;
        }
        return null;
    }

    private MyBeanTreeView createBeanTreeView() {
        MyBeanTreeView btv = new MyBeanTreeView();
        return btv;
    }

    public ExplorerManager getExplorerManager() {
        return this.manager;
    }

    private static class MyBeanTreeView
    extends BeanTreeView {
        private MyBeanTreeView() {
        }

        public boolean getScrollOnExpand() {
            return this.tree.getScrollsOnExpand();
        }

        public void setScrollOnExpand(boolean scroll) {
            this.tree.setScrollsOnExpand(scroll);
        }
    }

    private class UpdateFilterState
    implements Runnable {
        private final Language language;

        public UpdateFilterState(Language language) {
            this.language = language;
        }

        @Override
        public void run() {
            StructureScanner.Configuration configuration;
            StructureScanner scanner;
            boolean includeFilters = true;
            if (this.language != null && this.language.getStructure() != null && (configuration = (scanner = this.language.getStructure()).getConfiguration()) != null) {
                includeFilters = configuration.isFilterable();
                ArrayList<AbstractAction> newActions = new ArrayList<AbstractAction>();
                if (configuration.isSortable()) {
                    newActions.add(new SortActionSupport.SortByNameAction(ClassMemberPanelUI.this.filters));
                    newActions.add(new SortActionSupport.SortBySourceAction(ClassMemberPanelUI.this.filters));
                }
                if (!includeFilters) {
                    ClassMemberPanelUI.access$000((ClassMemberPanelUI)ClassMemberPanelUI.this).disableFiltering = true;
                } else {
                    ClassMemberPanelUI.access$000((ClassMemberPanelUI)ClassMemberPanelUI.this).disableFiltering = false;
                    if (!newActions.isEmpty()) {
                        newActions.add(null);
                        newActions.add(new FilterSubmenuAction(ClassMemberPanelUI.this.filters));
                    }
                }
                ClassMemberPanelUI.this.actions = newActions.toArray(new Action[newActions.size()]);
            }
            final boolean finalIncludeFilters = includeFilters;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ClassMemberPanelUI.this.filtersPanel.setVisible(finalIncludeFilters);
                }
            });
        }

    }

}

