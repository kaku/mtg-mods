/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 */
package org.netbeans.modules.csl.navigation;

import org.netbeans.modules.csl.navigation.ClassMemberPanel;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;

public class CaretListeningTask
extends IndexingAwareParserResultTask<ParserResult> {
    private boolean canceled;

    CaretListeningTask() {
        super(TaskIndexingMode.ALLOWED_DURING_SCAN);
    }

    public void run(ParserResult result, SchedulerEvent event) {
        boolean navigatorShouldUpdate;
        this.resume();
        boolean bl = navigatorShouldUpdate = ClassMemberPanel.getInstance() != null;
        if (this.isCancelled() || !navigatorShouldUpdate || !(event instanceof CursorMovedSchedulerEvent)) {
            return;
        }
        int offset = ((CursorMovedSchedulerEvent)event).getCaretOffset();
        if (offset != -1) {
            ClassMemberPanel.getInstance().selectElement(result, offset);
        }
    }

    public final synchronized void cancel() {
        this.canceled = true;
    }

    protected final synchronized boolean isCancelled() {
        return this.canceled;
    }

    protected final synchronized void resume() {
        this.canceled = false;
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }
}

