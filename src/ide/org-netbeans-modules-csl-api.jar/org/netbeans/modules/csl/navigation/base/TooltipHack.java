/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.navigation.base;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.Timer;
import javax.swing.ToolTipManager;

public final class TooltipHack
implements ActionListener {
    private static TooltipHack instance;
    private static int prevDismiss;

    private TooltipHack() {
    }

    public static void invokeTip(JComponent comp, int x, int y, int dismissDelay) {
        ToolTipManager ttm = ToolTipManager.sharedInstance();
        int prevInit = ttm.getInitialDelay();
        prevDismiss = ttm.getDismissDelay();
        ttm.setInitialDelay(0);
        ttm.setDismissDelay(dismissDelay);
        MouseEvent fakeEvt = new MouseEvent(comp, 503, System.currentTimeMillis(), 0, x, y, 0, false);
        ttm.mouseMoved(fakeEvt);
        ttm.setInitialDelay(prevInit);
        Timer timer = new Timer(20, TooltipHack.instance());
        timer.setRepeats(false);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (prevDismiss > 0) {
            ToolTipManager.sharedInstance().setDismissDelay(prevDismiss);
            prevDismiss = -1;
        }
    }

    private static TooltipHack instance() {
        if (instance == null) {
            instance = new TooltipHack();
        }
        return instance;
    }

    static {
        prevDismiss = -1;
    }
}

