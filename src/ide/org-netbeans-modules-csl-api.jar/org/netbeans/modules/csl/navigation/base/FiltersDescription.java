/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.navigation.base;

import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import org.netbeans.modules.csl.navigation.base.FiltersManager;

public final class FiltersDescription {
    private List<FilterItem> filters = new ArrayList<FilterItem>();

    public static FiltersManager createManager(FiltersDescription descr) {
        return FiltersManager.create(descr);
    }

    public void addFilter(String name, String displayName, String tooltip, boolean isSelected, Icon selectedIcon, Icon unselectedIcon) {
        FilterItem newItem = new FilterItem(name, displayName, tooltip, isSelected, selectedIcon, unselectedIcon);
        this.filters.add(newItem);
    }

    public int getFilterCount() {
        return this.filters.size();
    }

    public String getName(int index) {
        return this.filters.get((int)index).name;
    }

    public String getDisplayName(int index) {
        return this.filters.get((int)index).displayName;
    }

    public String getTooltip(int index) {
        return this.filters.get((int)index).tooltip;
    }

    public Icon getSelectedIcon(int index) {
        return this.filters.get((int)index).selectedIcon;
    }

    public Icon getUnselectedIcon(int index) {
        return this.filters.get((int)index).unselectedIcon;
    }

    public boolean isSelected(int index) {
        return this.filters.get((int)index).isSelected;
    }

    static class FilterItem {
        String name;
        String displayName;
        String tooltip;
        Icon selectedIcon;
        Icon unselectedIcon;
        boolean isSelected;

        FilterItem(String name, String displayName, String tooltip, boolean isSelected, Icon selectedIcon, Icon unselectedIcon) {
            this.name = name;
            this.displayName = displayName;
            this.tooltip = tooltip;
            this.selectedIcon = selectedIcon;
            this.unselectedIcon = unselectedIcon;
            this.isSelected = isSelected;
        }
    }

}

