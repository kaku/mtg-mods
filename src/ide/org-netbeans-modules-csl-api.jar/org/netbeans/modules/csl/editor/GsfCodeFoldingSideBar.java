/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.CodeFoldingSideBar
 */
package org.netbeans.modules.csl.editor;

import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.CodeFoldingSideBar;

public class GsfCodeFoldingSideBar
extends CodeFoldingSideBar {
    public GsfCodeFoldingSideBar(JTextComponent target) {
        super(target);
    }

    public JComponent createSideBar(JTextComponent target) {
        return new GsfCodeFoldingSideBar(target);
    }
}

