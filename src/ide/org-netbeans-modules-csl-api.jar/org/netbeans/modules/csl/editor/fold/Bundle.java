/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.fold;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String FT_label_imports() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_label_imports");
    }

    static String FT_label_innerclass() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_label_innerclass");
    }

    static String FT_label_othercodeblocks() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_label_othercodeblocks");
    }

    private void Bundle() {
    }
}

