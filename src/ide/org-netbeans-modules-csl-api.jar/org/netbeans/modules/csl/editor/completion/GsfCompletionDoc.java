/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.csl.editor.completion;

import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.CodeCompletionHandler2;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;

public class GsfCompletionDoc
implements CompletionDocumentation {
    private String content = null;
    private URL docURL = null;
    private AbstractAction goToSource = null;
    private ElementHandle elementHandle;
    private Language language;
    private ParserResult controller;
    private Callable<Boolean> cancel;

    private GsfCompletionDoc(final ParserResult controller, final ElementHandle elementHandle, URL url, Callable<Boolean> cancel) {
        Language embeddedLanguage;
        this.controller = controller;
        this.language = LanguageRegistry.getInstance().getLanguageByMimeType(controller.getSnapshot().getMimeType());
        this.cancel = cancel;
        if (elementHandle != null && elementHandle.getMimeType() != null && (embeddedLanguage = LanguageRegistry.getInstance().getLanguageByMimeType(elementHandle.getMimeType())) != null && embeddedLanguage.getParser(Collections.singleton(controller.getSnapshot())) != null) {
            this.language = embeddedLanguage;
        }
        CodeCompletionHandler completer = this.language.getCompletionProvider();
        this.elementHandle = elementHandle;
        if (elementHandle != null) {
            this.goToSource = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    Completion.get().hideAll();
                    UiUtils.open(controller.getSnapshot().getSource(), elementHandle);
                }
            };
            this.docURL = url != null ? url : null;
        }
        if (completer != null) {
            if (completer instanceof CodeCompletionHandler2) {
                Documentation doc = ((CodeCompletionHandler2)completer).documentElement(controller, elementHandle, cancel);
                if (doc != null) {
                    this.content = doc.getContent();
                    if (this.docURL == null) {
                        this.docURL = doc.getUrl();
                    }
                } else {
                    this.content = completer.document(controller, elementHandle);
                }
            } else {
                this.content = completer.document(controller, elementHandle);
            }
        }
        if (this.content == null) {
            Completion.get().hideDocumentation();
        }
    }

    public static final GsfCompletionDoc create(ParserResult controller, ElementHandle elementHandle, Callable<Boolean> cancel) {
        return new GsfCompletionDoc(controller, elementHandle, null, cancel);
    }

    public String getText() {
        return this.content;
    }

    public URL getURL() {
        return this.docURL;
    }

    public CompletionDocumentation resolveLink(String link) {
        ElementHandle handle;
        if (link.startsWith("www.")) {
            link = "http://" + link;
        }
        if (link.matches("[a-z]+://.*")) {
            try {
                URL url = new URL(link);
                HtmlBrowser.URLDisplayer.getDefault().showURL(url);
                return null;
            }
            catch (MalformedURLException mue) {
                Exceptions.printStackTrace((Throwable)mue);
            }
        }
        if ((handle = this.language.getCompletionProvider().resolveLink(link, this.elementHandle)) != null) {
            URL url = null;
            if (handle instanceof ElementHandle.UrlHandle) {
                String url_text = ((ElementHandle.UrlHandle)handle).getUrl();
                try {
                    url = new URL(url_text);
                }
                catch (MalformedURLException mue) {
                    Logger.getLogger("global").log(Level.INFO, null, mue);
                }
            }
            return new GsfCompletionDoc(this.controller, handle, url, this.cancel);
        }
        return null;
    }

    public Action getGotoSourceAction() {
        return this.goToSource;
    }

}

