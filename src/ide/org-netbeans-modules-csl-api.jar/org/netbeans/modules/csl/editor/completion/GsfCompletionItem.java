/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplate
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.CompletionUtilities
 *  org.netbeans.swing.plaf.LFCustoms
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.completion;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.Set;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager;
import org.netbeans.modules.csl.api.CodeCompletionResult;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.core.GsfHtmlFormatter;
import org.netbeans.modules.csl.editor.completion.GsfCompletionProvider;
import org.netbeans.modules.csl.navigation.Icons;
import org.netbeans.modules.csl.spi.DefaultCompletionProposal;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.netbeans.swing.plaf.LFCustoms;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public abstract class GsfCompletionItem
implements CompletionItem {
    private static CompletionFormatter FORMATTER = new CompletionFormatter();
    static CompletionProposal tipProposal;
    protected ParserResult info;
    protected CodeCompletionResult completionResult;
    protected static int SMART_TYPE;
    public static final String COLOR_END = "</font>";
    public static final String STRIKE = "<s>";
    public static final String STRIKE_END = "</s>";
    public static final String BOLD = "<b>";
    public static final String BOLD_END = "</b>";
    protected int substitutionOffset;

    public static final GsfCompletionItem createItem(CompletionProposal proposal, CodeCompletionResult result, ParserResult info) {
        return new DelegatedItem(info, result, proposal);
    }

    public static final GsfCompletionItem createTruncationItem() {
        return new TruncationItem();
    }

    private GsfCompletionItem(int substitutionOffset) {
        this.substitutionOffset = substitutionOffset;
    }

    public void defaultAction(JTextComponent component) {
        if (component != null) {
            if (this.getInsertPrefix().length() == 0) {
                return;
            }
            Completion.get().hideAll();
            int caretOffset = component.getSelectionEnd();
            if (caretOffset >= this.substitutionOffset) {
                this.substituteText(component, this.substitutionOffset, caretOffset - this.substitutionOffset, null);
            }
        }
    }

    public void processKeyEvent(KeyEvent evt) {
        if (evt.getID() == 400) {
            switch (evt.getKeyChar()) {
                case '\n': 
                case '(': 
                case ',': 
                case '.': 
                case ';': {
                    Completion.get().hideAll();
                }
            }
        }
    }

    public boolean instantSubstitution(JTextComponent component) {
        this.defaultAction(component);
        return true;
    }

    public CompletionTask createDocumentationTask() {
        return null;
    }

    public CompletionTask createToolTipTask() {
        return null;
    }

    public int getPreferredWidth(Graphics g, Font defaultFont) {
        return CompletionUtilities.getPreferredWidth((String)this.getLeftHtmlText(), (String)this.getRightHtmlText(), (Graphics)g, (Font)defaultFont);
    }

    public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
        CompletionUtilities.renderHtml((ImageIcon)this.getIcon(), (String)this.getLeftHtmlText(), (String)this.getRightHtmlText(), (Graphics)g, (Font)defaultFont, (Color)defaultColor, (int)width, (int)height, (boolean)selected);
    }

    protected abstract ImageIcon getIcon();

    protected String getLeftHtmlText() {
        return null;
    }

    protected String getRightHtmlText() {
        return null;
    }

    protected void substituteText(JTextComponent c, final int offset, final int len, String toAdd) {
        final BaseDocument doc = (BaseDocument)c.getDocument();
        final String text = this.getInsertPrefix().toString();
        if (text != null) {
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        int semiPos = -2;
                        String textToReplace = doc.getText(offset, len);
                        if (text.equals(textToReplace)) {
                            if (semiPos > -1) {
                                doc.insertString(semiPos, ";", null);
                            }
                            return;
                        }
                        int common = 0;
                        while (text.regionMatches(0, textToReplace, 0, ++common)) {
                        }
                        Position position = doc.createPosition(offset + --common);
                        Position semiPosition = semiPos > -1 ? doc.createPosition(semiPos) : null;
                        doc.remove(offset + common, len - common);
                        doc.insertString(position.getOffset(), text.substring(common), null);
                        if (semiPosition != null) {
                            doc.insertString(semiPosition.getOffset(), ";", null);
                        }
                    }
                    catch (BadLocationException e) {
                        // empty catch block
                    }
                }
            });
        }
    }

    private static String truncateRhs(String rhs, int left) {
        if (rhs != null) {
            int MAX_SIZE = 80;
            int size = 80 - left;
            if (size < 10) {
                size = 10;
            }
            if (rhs != null && rhs.length() > size) {
                rhs = rhs.substring(0, size - 3) + "<b>&gt;</b>";
            }
        }
        return rhs;
    }

    private static String getHTMLColor(int r, int g, int b) {
        Color c = LFCustoms.shiftColor((Color)new Color(r, g, b));
        return "<font color=#" + LFCustoms.getHexString((int)c.getRed()) + LFCustoms.getHexString((int)c.getGreen()) + LFCustoms.getHexString((int)c.getBlue()) + ">";
    }

    static /* synthetic */ String access$600(int x0, int x1, int x2) {
        return GsfCompletionItem.getHTMLColor(x0, x1, x2);
    }

    static {
        SMART_TYPE = 1000;
    }

    private static class CompletionFormatter
    extends GsfHtmlFormatter {
        private static final String METHOD_COLOR = LFCustoms.getTextFgColorHTML();
        private static final String PARAMETER_NAME_COLOR = GsfCompletionItem.access$600(160, 96, 1);
        private static final String END_COLOR = "</font>";
        private static final String CLASS_COLOR = GsfCompletionItem.access$600(86, 0, 0);
        private static final String PKG_COLOR = GsfCompletionItem.access$600(128, 128, 128);
        private static final String KEYWORD_COLOR = GsfCompletionItem.access$600(0, 0, 153);
        private static final String FIELD_COLOR = GsfCompletionItem.access$600(0, 134, 24);
        private static final String VARIABLE_COLOR = GsfCompletionItem.access$600(0, 0, 124);
        private static final String CONSTRUCTOR_COLOR = GsfCompletionItem.access$600(178, 139, 0);
        private static final String INTERFACE_COLOR = GsfCompletionItem.access$600(64, 64, 64);
        private static final String PARAMETERS_COLOR = GsfCompletionItem.access$600(128, 128, 128);
        private static final String ACTIVE_PARAMETER_COLOR = LFCustoms.getTextFgColorHTML();

        private CompletionFormatter() {
        }

        @Override
        public void parameters(boolean start) {
            assert (start != this.isParameter);
            this.isParameter = start;
            if (this.isParameter) {
                this.sb.append(PARAMETER_NAME_COLOR);
            } else {
                this.sb.append("</font>");
            }
        }

        @Override
        public void active(boolean start) {
            if (start) {
                this.sb.append(ACTIVE_PARAMETER_COLOR);
                this.sb.append("<b>");
            } else {
                this.sb.append("</b>");
                this.sb.append("</font>");
            }
        }

        @Override
        public void name(ElementKind kind, boolean start) {
            assert (start != this.isName);
            this.isName = start;
            if (this.isName) {
                switch (kind) {
                    case CONSTRUCTOR: {
                        this.sb.append(CONSTRUCTOR_COLOR);
                        break;
                    }
                    case CALL: {
                        this.sb.append(PARAMETERS_COLOR);
                        break;
                    }
                    case DB: 
                    case METHOD: {
                        this.sb.append(METHOD_COLOR);
                        break;
                    }
                    case CLASS: 
                    case INTERFACE: {
                        this.sb.append(CLASS_COLOR);
                        break;
                    }
                    case FIELD: {
                        this.sb.append(FIELD_COLOR);
                        break;
                    }
                    case MODULE: {
                        this.sb.append(PKG_COLOR);
                        break;
                    }
                    case KEYWORD: {
                        this.sb.append(KEYWORD_COLOR);
                        this.sb.append("<b>");
                        break;
                    }
                    case VARIABLE: {
                        this.sb.append(VARIABLE_COLOR);
                        this.sb.append("<b>");
                        break;
                    }
                    default: {
                        this.sb.append(LFCustoms.getTextFgColorHTML());
                        break;
                    }
                }
            } else {
                switch (kind) {
                    case VARIABLE: 
                    case KEYWORD: {
                        this.sb.append("</b>");
                    }
                }
                this.sb.append("</font>");
            }
        }
    }

    private static class TruncationItem
    extends GsfCompletionItem
    implements CompletionTask,
    CompletionDocumentation {
        private TruncationItem() {
            super(0);
        }

        @Override
        protected ImageIcon getIcon() {
            return new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/csl/editor/completion/warning.png"));
        }

        public int getSortPriority() {
            return -20000;
        }

        public CharSequence getSortText() {
            return "";
        }

        public CharSequence getInsertPrefix() {
            return "";
        }

        @Override
        protected String getLeftHtmlText() {
            return "<b>" + NbBundle.getMessage(GsfCompletionItem.class, (String)"ListTruncated") + "</b>";
        }

        @Override
        public CompletionTask createDocumentationTask() {
            return this;
        }

        public void query(CompletionResultSet resultSet) {
            resultSet.setDocumentation((CompletionDocumentation)this);
            resultSet.finish();
        }

        public void refresh(CompletionResultSet resultSet) {
            resultSet.setDocumentation((CompletionDocumentation)this);
            resultSet.finish();
        }

        public void cancel() {
        }

        public String getText() {
            return NbBundle.getMessage(GsfCompletionItem.class, (String)"TruncatedHelpHtml");
        }

        public URL getURL() {
            return null;
        }

        public CompletionDocumentation resolveLink(String link) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Action getGotoSourceAction() {
            return null;
        }
    }

    private static class DelegatedItem
    extends GsfCompletionItem {
        private CompletionProposal item;
        private Integer spCache = null;
        private CharSequence stCache = null;

        private DelegatedItem(ParserResult info, CodeCompletionResult completionResult, CompletionProposal item) {
            super(item.getAnchorOffset());
            this.item = item;
            this.completionResult = completionResult;
            this.info = info;
        }

        @Override
        public void defaultAction(JTextComponent component) {
            boolean cancel;
            if (this.item instanceof DefaultCompletionProposal && (cancel = ((DefaultCompletionProposal)this.item).beforeDefaultAction())) {
                return;
            }
            super.defaultAction(component);
        }

        public int getSortPriority() {
            if (this.spCache == null) {
                if (this.item.getSortPrioOverride() != 0) {
                    this.spCache = this.item.getSortPrioOverride();
                } else {
                    switch (this.item.getKind()) {
                        case ERROR: {
                            this.spCache = -5000;
                            break;
                        }
                        case DB: {
                            this.spCache = this.item.isSmart() ? 155 - SMART_TYPE : 155;
                            break;
                        }
                        case PARAMETER: {
                            this.spCache = this.item.isSmart() ? 105 - SMART_TYPE : 105;
                            break;
                        }
                        case CALL: {
                            this.spCache = this.item.isSmart() ? 110 - SMART_TYPE : 110;
                            break;
                        }
                        case CONSTRUCTOR: {
                            this.spCache = this.item.isSmart() ? 400 - SMART_TYPE : 400;
                            break;
                        }
                        case PACKAGE: 
                        case MODULE: {
                            this.spCache = this.item.isSmart() ? 640 - SMART_TYPE : 640;
                            break;
                        }
                        case CLASS: 
                        case INTERFACE: {
                            this.spCache = this.item.isSmart() ? 620 - SMART_TYPE : 620;
                            break;
                        }
                        case ATTRIBUTE: 
                        case RULE: {
                            this.spCache = this.item.isSmart() ? 482 - SMART_TYPE : 482;
                            break;
                        }
                        case TAG: {
                            this.spCache = this.item.isSmart() ? 480 - SMART_TYPE : 480;
                            break;
                        }
                        case TEST: 
                        case PROPERTY: 
                        case METHOD: {
                            this.spCache = this.item.isSmart() ? 500 - SMART_TYPE : 500;
                            break;
                        }
                        case FIELD: {
                            this.spCache = this.item.isSmart() ? 300 - SMART_TYPE : 300;
                            break;
                        }
                        case CONSTANT: 
                        case GLOBAL: 
                        case VARIABLE: {
                            this.spCache = this.item.isSmart() ? 200 - SMART_TYPE : 200;
                            break;
                        }
                        case KEYWORD: {
                            this.spCache = this.item.isSmart() ? 600 - SMART_TYPE : 600;
                            break;
                        }
                        default: {
                            this.spCache = this.item.isSmart() ? 999 - SMART_TYPE : 999;
                        }
                    }
                }
            }
            return this.spCache;
        }

        @Override
        public boolean instantSubstitution(JTextComponent component) {
            ElementKind kind = this.item.getKind();
            if (kind == ElementKind.PARAMETER || kind == ElementKind.CLASS || kind == ElementKind.MODULE) {
                return false;
            }
            if (component != null) {
                try {
                    int caretOffset = component.getSelectionEnd();
                    if (caretOffset > this.substitutionOffset) {
                        String text = component.getDocument().getText(this.substitutionOffset, caretOffset - this.substitutionOffset);
                        if (!this.getInsertPrefix().toString().startsWith(text)) {
                            return false;
                        }
                    }
                }
                catch (BadLocationException ble) {
                    // empty catch block
                }
            }
            this.defaultAction(component);
            return true;
        }

        public CharSequence getSortText() {
            if (this.stCache == null) {
                this.stCache = this.item.getSortText();
                if (this.stCache == null) {
                    this.stCache = "";
                }
            }
            return this.stCache;
        }

        public CharSequence getInsertPrefix() {
            return this.item.getInsertPrefix();
        }

        @Override
        protected String getLeftHtmlText() {
            FORMATTER.reset();
            return this.item.getLhsHtml(FORMATTER);
        }

        public String toString() {
            return this.item.getName();
        }

        @Override
        protected String getRightHtmlText() {
            FORMATTER.reset();
            String rhs = this.item.getRhsHtml(FORMATTER);
            FORMATTER.reset();
            String lhs = this.item.getLhsHtml(FORMATTER);
            boolean inTag = false;
            int length = 0;
            int n = lhs.length();
            for (int i = 0; i < n; ++i) {
                char c = lhs.charAt(i);
                if (inTag) {
                    if (c != '>') continue;
                    inTag = false;
                    continue;
                }
                if (c == '<') {
                    inTag = true;
                    continue;
                }
                ++length;
            }
            return GsfCompletionItem.truncateRhs(rhs, length);
        }

        @Override
        public CompletionTask createDocumentationTask() {
            ElementHandle element = this.item.getElement();
            if (element != null) {
                return GsfCompletionProvider.createDocTask(element, this.info);
            }
            return null;
        }

        @Override
        protected ImageIcon getIcon() {
            ImageIcon ic = this.item.getIcon();
            if (ic != null) {
                return ic;
            }
            ImageIcon imageIcon = Icons.getElementIcon(this.item.getKind(), this.item.getModifiers());
            return imageIcon;
        }

        @Override
        protected void substituteText(JTextComponent c, int offset, int len, String toAdd) {
            if (this.completionResult != null) {
                this.completionResult.beforeInsert(this.item);
                if (!this.completionResult.insert(this.item)) {
                    this.defaultSubstituteText(c, offset, len, toAdd);
                }
                this.completionResult.afterInsert(this.item);
            } else {
                this.defaultSubstituteText(c, offset, len, toAdd);
            }
        }

        private void defaultSubstituteText(final JTextComponent c, final int offset, final int len, String toAdd) {
            String template = this.item.getCustomInsertTemplate();
            if (template != null) {
                final BaseDocument doc = (BaseDocument)c.getDocument();
                CodeTemplateManager ctm = CodeTemplateManager.get((Document)doc);
                if (ctm != null) {
                    doc.runAtomic(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                doc.remove(offset, len);
                                c.getCaret().setDot(offset);
                            }
                            catch (BadLocationException e) {
                                // empty catch block
                            }
                        }
                    });
                    ctm.createTemporary(template).insert(c);
                    Completion.get().showToolTip();
                }
                return;
            }
            super.substituteText(c, offset, len, toAdd);
        }

    }

}

