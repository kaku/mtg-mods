/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.editor.semantic;

import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.Language;

class SequenceElement
implements Comparable<SequenceElement> {
    public final Language language;
    public OffsetRange range;
    public final ColoringAttributes.Coloring coloring;

    private SequenceElement() {
        this(null, null, null);
    }

    public SequenceElement(Language language, OffsetRange range, ColoringAttributes.Coloring coloring) {
        this.language = language;
        this.range = range;
        this.coloring = coloring;
    }

    @Override
    public int compareTo(SequenceElement o) {
        if (o instanceof ComparisonItem) {
            return -1 * ((ComparisonItem)o).compareTo(this);
        }
        assert (o.range != null);
        return this.range.compareTo(o.range);
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SequenceElement)) {
            return false;
        }
        SequenceElement other = (SequenceElement)obj;
        return this.range.equals(other.range);
    }

    public int hashCode() {
        return this.range.hashCode();
    }

    public String toString() {
        return "SequenceElement(range=" + this.range + ", lang=" + this.language + ", color=" + this.coloring + ")";
    }

    static class ComparisonItem
    extends SequenceElement {
        private int offset;

        ComparisonItem(int offset) {
            super();
            this.offset = offset;
        }

        @Override
        public int compareTo(SequenceElement o) {
            if (o instanceof ComparisonItem) {
                return this.offset - ((ComparisonItem)o).offset;
            }
            if (this.offset < o.range.getStart()) {
                return -1;
            }
            if (this.offset >= o.range.getEnd()) {
                return 1;
            }
            return 0;
        }
    }

}

