/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 */
package org.netbeans.modules.csl.editor.semantic;

import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.editor.semantic.MarkOccurrencesHighlighter;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public class MarkOccurrencesHighlighterFactory
extends AbstractTaskFactory {
    public MarkOccurrencesHighlighterFactory() {
        super(false);
    }

    @Override
    public Collection<? extends SchedulerTask> createTasks(Language l, Snapshot snapshot) {
        OccurrencesFinder finder = l.getOccurrencesFinder();
        if (finder != null) {
            return Collections.singleton(new MarkOccurrencesHighlighter(l, snapshot));
        }
        return null;
    }
}

