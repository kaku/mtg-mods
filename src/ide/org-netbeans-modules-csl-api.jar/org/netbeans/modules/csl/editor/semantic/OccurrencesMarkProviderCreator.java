/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator
 */
package org.netbeans.modules.csl.editor.semantic;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.csl.editor.semantic.OccurrencesMarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator;

public class OccurrencesMarkProviderCreator
implements MarkProviderCreator {
    public MarkProvider createMarkProvider(JTextComponent pane) {
        return OccurrencesMarkProvider.get(pane.getDocument());
    }
}

