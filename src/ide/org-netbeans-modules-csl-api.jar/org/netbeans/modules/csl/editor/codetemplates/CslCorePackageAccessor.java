/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.editor.codetemplates;

import java.util.Collection;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;

public abstract class CslCorePackageAccessor {
    private static CslCorePackageAccessor ACCESSOR = null;

    public static synchronized void register(CslCorePackageAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized CslCorePackageAccessor get() {
        try {
            Class clazz = Class.forName(LanguageRegistry.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected CslCorePackageAccessor() {
    }

    public abstract void languageRegistryAddLanguages(Collection<? extends Language> var1);
}

