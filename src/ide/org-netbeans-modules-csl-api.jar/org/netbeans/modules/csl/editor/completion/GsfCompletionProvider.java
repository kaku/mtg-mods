/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionProvider
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionQuery
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionTask
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.csl.editor.completion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JToolTip;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.CodeCompletionContext;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.CodeCompletionResult;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.GsfLanguage;
import org.netbeans.modules.csl.api.ParameterInfo;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.completion.GsfCompletionDoc;
import org.netbeans.modules.csl.editor.completion.GsfCompletionItem;
import org.netbeans.modules.csl.editor.completion.MethodParamsTipPaintComponent;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public class GsfCompletionProvider
implements CompletionProvider {
    private static boolean caseSensitive = true;
    private static boolean autoPopup = true;
    private static boolean inited;
    private static PreferenceChangeListener settingsListener;

    private static org.netbeans.modules.csl.core.Language getCompletableLanguage(Document doc, int offset) {
        BaseDocument baseDoc = (BaseDocument)doc;
        List<org.netbeans.modules.csl.core.Language> list = LanguageRegistry.getInstance().getEmbeddedLanguages(baseDoc, offset);
        for (org.netbeans.modules.csl.core.Language l : list) {
            if (l.getCompletionProvider() == null) continue;
            return l;
        }
        return null;
    }

    public static CodeCompletionHandler getCompletable(Document doc, int offset) {
        org.netbeans.modules.csl.core.Language l = GsfCompletionProvider.getCompletableLanguage(doc, offset);
        if (l != null) {
            return l.getCompletionProvider();
        }
        return null;
    }

    public static CodeCompletionHandler getCompletable(ParserResult result) {
        org.netbeans.modules.csl.core.Language l = LanguageRegistry.getInstance().getLanguageByMimeType(result.getSnapshot().getMimeType());
        if (l != null) {
            return l.getCompletionProvider();
        }
        return null;
    }

    public int getAutoQueryTypes(JTextComponent component, String typedText) {
        CodeCompletionHandler provider;
        if (!autoPopup) {
            return 0;
        }
        if (typedText.length() > 0 && (provider = GsfCompletionProvider.getCompletable(component.getDocument(), component.getCaretPosition())) != null) {
            CodeCompletionHandler.QueryType autoQuery = provider.getAutoQuery(component, typedText);
            switch (autoQuery) {
                case NONE: {
                    return 0;
                }
                case STOP: {
                    Completion.get().hideAll();
                    return 0;
                }
                case COMPLETION: {
                    return 1;
                }
                case DOCUMENTATION: {
                    return 2;
                }
                case TOOLTIP: {
                    return 4;
                }
                case ALL_COMPLETION: {
                    return 9;
                }
            }
        }
        return 0;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isJavaContext(JTextComponent component, int offset) {
        Document doc = component.getDocument();
        Language language = (Language)doc.getProperty(Language.class);
        if (language == null) {
            return true;
        }
        if (doc instanceof AbstractDocument) {
            ((AbstractDocument)doc).readLock();
        }
        try {
            TokenSequence ts = TokenHierarchy.get((Document)component.getDocument()).tokenSequence();
            if (ts == null) {
                boolean bl = false;
                return bl;
            }
            if (!ts.moveNext() || ts.move(offset) == 0) {
                boolean bl = true;
                return bl;
            }
            if (!ts.moveNext()) {
                boolean bl = false;
                return bl;
            }
            boolean bl = true;
            return bl;
        }
        finally {
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument)doc).readUnlock();
            }
        }
    }

    public static boolean startsWith(String theString, String prefix) {
        if (theString == null || theString.length() == 0) {
            return false;
        }
        if (prefix == null || prefix.length() == 0) {
            return true;
        }
        return GsfCompletionProvider.isCaseSensitive() ? theString.startsWith(prefix) : theString.toLowerCase().startsWith(prefix.toLowerCase());
    }

    public CompletionTask createTask(int type, JTextComponent component) {
        if ((type & 1) != 0 || type == 4 || type == 2) {
            return new AsyncCompletionTask((AsyncCompletionQuery)new JavaCompletionQuery(type, component.getSelectionStart()), component);
        }
        return null;
    }

    static CompletionTask createDocTask(ElementHandle element, ParserResult info) {
        JavaCompletionQuery query = new JavaCompletionQuery(2, -1);
        query.element = element;
        return new AsyncCompletionTask((AsyncCompletionQuery)query, EditorRegistry.lastFocusedComponent());
    }

    private static boolean isCaseSensitive() {
        GsfCompletionProvider.lazyInit();
        return caseSensitive;
    }

    private static void setCaseSensitive(boolean b) {
        GsfCompletionProvider.lazyInit();
        caseSensitive = b;
    }

    private static void setAutoPopup(boolean b) {
        GsfCompletionProvider.lazyInit();
        autoPopup = b;
    }

    private static void lazyInit() {
        if (!inited) {
            inited = true;
            Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
            prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)settingsListener, (Object)prefs));
            GsfCompletionProvider.setCaseSensitive(prefs.getBoolean("completion-case-sensitive", false));
            GsfCompletionProvider.setAutoPopup(prefs.getBoolean("completion-auto-popup", false));
        }
    }

    static {
        settingsListener = new SettingsListener();
    }

    private static class SettingsListener
    implements PreferenceChangeListener {
        private SettingsListener() {
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            if (evt.getKey() == null || "completion-case-sensitive".equals(evt.getKey())) {
                GsfCompletionProvider.setCaseSensitive(Boolean.valueOf(evt.getNewValue()));
            } else if ("completion-auto-popup".equals(evt.getKey())) {
                GsfCompletionProvider.setAutoPopup(Boolean.valueOf(evt.getNewValue()));
            }
        }
    }

    static final class JavaCompletionQuery
    extends AsyncCompletionQuery {
        private Collection<CompletionItem> results;
        private JToolTip toolTip;
        private CompletionDocumentation documentation;
        private int anchorOffset;
        private JTextComponent component;
        private int queryType;
        private int caretOffset;
        private String filterPrefix;
        private ElementHandle element;
        private boolean isTruncated;
        private boolean isFilterable;

        private JavaCompletionQuery(int queryType, int caretOffset) {
            this.queryType = queryType;
            this.caretOffset = caretOffset;
        }

        protected void preQueryUpdate(JTextComponent component) {
            int newCaretOffset = component.getSelectionStart();
            if (newCaretOffset >= this.caretOffset) {
                try {
                    Document doc = component.getDocument();
                    org.netbeans.modules.csl.core.Language language = GsfCompletionProvider.getCompletableLanguage(doc, this.caretOffset);
                    if (this.isJavaIdentifierPart(language, doc.getText(this.caretOffset, newCaretOffset - this.caretOffset))) {
                        return;
                    }
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
            Completion.get().hideCompletion();
        }

        protected void prepareQuery(JTextComponent component) {
            this.component = component;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void query(CompletionResultSet resultSet, Document doc, final int caretOffset) {
            try {
                this.caretOffset = caretOffset;
                if (this.queryType == 4 || this.queryType == 2 || GsfCompletionProvider.isJavaContext(this.component, caretOffset)) {
                    this.results = null;
                    this.isTruncated = false;
                    this.isFilterable = true;
                    this.documentation = null;
                    this.toolTip = null;
                    this.anchorOffset = -1;
                    Source source = Source.create((Document)doc);
                    if (source == null) {
                        FileObject fo = null;
                        if (this.element != null && (fo = this.element.getFileObject()) != null) {
                            source = Source.create((FileObject)fo);
                        }
                    }
                    if (source != null) {
                        Set<Source> sources = Collections.singleton(source);
                        UserTask task = new UserTask(){

                            public void run(ResultIterator resultIterator) throws Exception {
                                Parser.Result result = resultIterator.getParserResult(caretOffset);
                                if (!(result instanceof ParserResult)) {
                                    return;
                                }
                                ParserResult parserResult = (ParserResult)result;
                                if (parserResult == null) {
                                    return;
                                }
                                if ((JavaCompletionQuery.this.queryType & 1) != 0) {
                                    JavaCompletionQuery.this.resolveCompletion(parserResult);
                                } else if (JavaCompletionQuery.this.queryType == 4) {
                                    JavaCompletionQuery.this.resolveToolTip(parserResult);
                                } else if (JavaCompletionQuery.this.queryType == 2) {
                                    JavaCompletionQuery.this.resolveDocumentation(parserResult);
                                }
                                GsfCompletionItem.tipProposal = null;
                            }
                        };
                        ParserManager.parse(sources, (UserTask)task);
                        if ((this.queryType & 1) != 0) {
                            if (this.results != null) {
                                resultSet.addAllItems(this.results);
                            }
                        } else if (this.queryType == 4) {
                            if (this.toolTip != null) {
                                resultSet.setToolTip(this.toolTip);
                            }
                        } else if (this.queryType == 2 && this.documentation != null) {
                            resultSet.setDocumentation(this.documentation);
                        }
                        if (this.results != null && this.results.size() == 0) {
                            this.isFilterable = false;
                        }
                        if (this.anchorOffset > -1) {
                            resultSet.setAnchorOffset(this.anchorOffset);
                        }
                    }
                }
            }
            catch (ParseException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
            finally {
                resultSet.finish();
            }
        }

        protected boolean canFilter(JTextComponent component) {
            this.filterPrefix = null;
            int newOffset = component.getSelectionStart();
            if ((this.queryType & 1) != 0) {
                if (this.isTruncated || !this.isFilterable) {
                    return false;
                }
                int offset = Math.min(this.anchorOffset, this.caretOffset);
                if (offset > -1) {
                    if (newOffset < offset) {
                        return true;
                    }
                    if (newOffset >= this.caretOffset) {
                        try {
                            Document doc = component.getDocument();
                            org.netbeans.modules.csl.core.Language language = GsfCompletionProvider.getCompletableLanguage(doc, this.caretOffset);
                            String prefix = doc.getText(offset, newOffset - offset);
                            String string = this.filterPrefix = this.isJavaIdentifierPart(language, prefix) ? prefix : null;
                            if (this.filterPrefix != null && this.filterPrefix.length() == 0) {
                                this.anchorOffset = newOffset;
                            }
                        }
                        catch (BadLocationException e) {
                            // empty catch block
                        }
                        return true;
                    }
                }
                return false;
            }
            if (this.queryType == 4) {
                try {
                    if (newOffset == this.caretOffset) {
                        this.filterPrefix = "";
                    } else if (newOffset - this.caretOffset > 0) {
                        this.filterPrefix = component.getDocument().getText(this.caretOffset, newOffset - this.caretOffset);
                    } else if (newOffset - this.caretOffset < 0) {
                        this.filterPrefix = component.getDocument().getText(newOffset, this.caretOffset - newOffset);
                    }
                }
                catch (BadLocationException ex) {
                    // empty catch block
                }
                return this.filterPrefix != null && this.filterPrefix.indexOf(44) == -1 && this.filterPrefix.indexOf(40) == -1 && this.filterPrefix.indexOf(41) == -1;
            }
            return false;
        }

        protected void filter(CompletionResultSet resultSet) {
            try {
                if ((this.queryType & 1) != 0) {
                    if (this.results != null) {
                        if (this.filterPrefix != null) {
                            resultSet.addAllItems(this.getFilteredData(this.results, this.filterPrefix));
                        } else {
                            Completion.get().hideDocumentation();
                            Completion.get().hideCompletion();
                        }
                    }
                } else if (this.queryType == 4) {
                    resultSet.setToolTip(this.toolTip);
                }
                resultSet.setAnchorOffset(this.anchorOffset);
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            resultSet.finish();
        }

        private void resolveToolTip(ParserResult controller) throws IOException {
            ParameterInfo info;
            int offset;
            CompletionProposal proposal = GsfCompletionItem.tipProposal;
            Env env = this.getCompletionEnvironment(controller, false);
            CodeCompletionHandler completer = env.getCompletable();
            if (completer != null && (info = completer.parameters(controller, offset = env.getOffset(), proposal)) != ParameterInfo.NONE) {
                List<String> params = info.getNames();
                int MAX_WIDTH = 50;
                int column = 0;
                ArrayList<List<String>> parameterList = new ArrayList<List<String>>();
                ArrayList<String> p = new ArrayList<String>();
                int length = params.size();
                for (int i = 0; i < length; ++i) {
                    String parameter = params.get(i);
                    if (i < length - 1) {
                        parameter = parameter + ", ";
                    }
                    p.add(parameter);
                    if ((column += parameter.length()) <= MAX_WIDTH) continue;
                    column = 0;
                    parameterList.add(p);
                    p = new ArrayList();
                }
                if (p.size() > 0) {
                    parameterList.add(p);
                }
                int index = info.getCurrentIndex();
                this.anchorOffset = info.getAnchorOffset();
                if (this.anchorOffset == -1) {
                    this.anchorOffset = offset;
                }
                this.toolTip = new MethodParamsTipPaintComponent(parameterList, index, this.component);
                return;
            }
        }

        private void resolveDocumentation(ParserResult controller) throws IOException {
            if (this.element != null) {
                this.documentation = GsfCompletionDoc.create(controller, this.element, new Callable<Boolean>(){

                    @Override
                    public Boolean call() throws Exception {
                        return JavaCompletionQuery.this.isTaskCancelled();
                    }
                });
            } else {
                Env env = this.getCompletionEnvironment(controller, false);
                int offset = env.getOffset();
                String prefix = env.getPrefix();
                this.results = new ArrayList<CompletionItem>();
                this.isTruncated = false;
                this.isFilterable = true;
                this.anchorOffset = env.getOffset() - (prefix != null ? prefix.length() : 0);
                CodeCompletionHandler completer = env.getCompletable();
                if (completer != null) {
                    CodeCompletionContextImpl context = new CodeCompletionContextImpl(offset, controller, prefix, false, CodeCompletionHandler.QueryType.DOCUMENTATION);
                    CodeCompletionResult result = completer.complete(context);
                    if (result == null) {
                        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, completer.getClass().getName() + " should return CodeCompletionResult.NONE rather than null");
                        result = CodeCompletionResult.NONE;
                    }
                    if (result != CodeCompletionResult.NONE) {
                        for (CompletionProposal proposal : result.getItems()) {
                            ElementHandle el = proposal.getElement();
                            if (el == null) continue;
                            this.documentation = GsfCompletionDoc.create(controller, el, new Callable<Boolean>(){

                                @Override
                                public Boolean call() throws Exception {
                                    return JavaCompletionQuery.this.isTaskCancelled();
                                }
                            });
                            if (this.documentation.getText() == null || this.documentation.getText().length() <= 0) continue;
                            break;
                        }
                    }
                }
            }
        }

        private void resolveCompletion(ParserResult controller) throws IOException {
            Env env = this.getCompletionEnvironment(controller, true);
            int offset = env.getOffset();
            String prefix = env.getPrefix();
            this.results = new ArrayList<CompletionItem>();
            this.isTruncated = false;
            this.isFilterable = true;
            this.anchorOffset = env.getOffset() - (prefix != null ? prefix.length() : 0);
            CodeCompletionHandler completer = env.getCompletable();
            if (completer != null) {
                this.addCodeCompletionItems(controller, completer, offset, prefix);
                if (this.isTruncated) {
                    GsfCompletionItem item = GsfCompletionItem.createTruncationItem();
                    this.results.add(item);
                }
            }
        }

        private void addCodeCompletionItems(ParserResult controller, CodeCompletionHandler completer, int offset, String prefix) {
            CodeCompletionHandler.QueryType qtype = this.queryType == 9 ? CodeCompletionHandler.QueryType.ALL_COMPLETION : CodeCompletionHandler.QueryType.COMPLETION;
            CodeCompletionContextImpl context = new CodeCompletionContextImpl(offset, controller, prefix, true, qtype);
            CodeCompletionResult result = completer.complete(context);
            if (result == null) {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING, completer.getClass().getName() + " should return CodeCompletionResult.NONE rather than null");
                result = CodeCompletionResult.NONE;
            }
            if (result != CodeCompletionResult.NONE) {
                if (result.isTruncated()) {
                    this.isTruncated = true;
                }
                if (!result.isFilterable()) {
                    this.isFilterable = false;
                }
                for (CompletionProposal proposal : result.getItems()) {
                    GsfCompletionItem item = GsfCompletionItem.createItem(proposal, result, controller);
                    if (item == null) continue;
                    this.results.add(item);
                }
            }
        }

        private boolean isJavaIdentifierPart(org.netbeans.modules.csl.core.Language language, String text) {
            GsfLanguage gsfLanguage = language != null ? language.getGsfLanguage() : null;
            for (int i = 0; i < text.length(); ++i) {
                char c = text.charAt(i);
                if (!(gsfLanguage == null ? !Character.isJavaIdentifierPart(c) : !gsfLanguage.isIdentifierChar(c))) continue;
                return false;
            }
            return true;
        }

        private Collection getFilteredData(Collection<CompletionItem> data, String prefix) {
            if (prefix.length() == 0) {
                return data;
            }
            ArrayList<CompletionItem> ret = new ArrayList<CompletionItem>();
            for (CompletionItem itm : data) {
                if (!GsfCompletionProvider.startsWith(itm.getInsertPrefix().toString(), prefix)) continue;
                ret.add(itm);
            }
            return ret;
        }

        private Env getCompletionEnvironment(ParserResult controller, boolean upToOffset) throws IOException {
            int length;
            Document doc = controller.getSnapshot().getSource().getDocument(false);
            int n = length = doc != null ? doc.getLength() : (int)controller.getSnapshot().getSource().getFileObject().getSize();
            if (this.caretOffset > length) {
                this.caretOffset = length;
            }
            int offset = this.caretOffset;
            String prefix = null;
            CodeCompletionHandler completer = GsfCompletionProvider.getCompletable(controller);
            try {
                int[] blk;
                int start;
                if (completer != null) {
                    prefix = completer.getPrefix(controller, offset, upToOffset);
                }
                if (prefix == null && doc != null && (blk = Utilities.getIdentifierBlock((BaseDocument)((BaseDocument)doc), (int)offset)) != null && (start = blk[0]) < offset) {
                    prefix = upToOffset ? doc.getText(start, offset - start) : doc.getText(start, blk[1] - start);
                }
            }
            catch (BadLocationException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
            }
            return new Env(offset, prefix, controller, completer);
        }

        private class Env {
            private int offset;
            private String prefix;
            private ParserResult controller;
            private CodeCompletionHandler completable;
            private boolean autoCompleting;

            private Env(int offset, String prefix, ParserResult controller, CodeCompletionHandler completable) {
                this.offset = offset;
                this.prefix = prefix;
                this.controller = controller;
                this.completable = completable;
            }

            public int getOffset() {
                return this.offset;
            }

            public String getPrefix() {
                return this.prefix;
            }

            public boolean isAutoCompleting() {
                return this.autoCompleting;
            }

            public void setAutoCompleting(boolean autoCompleting) {
                this.autoCompleting = autoCompleting;
            }

            public ParserResult getController() {
                return this.controller;
            }

            public CodeCompletionHandler getCompletable() {
                return this.completable;
            }
        }

        private static class CodeCompletionContextImpl
        extends CodeCompletionContext {
            private final int caretOffset;
            private final ParserResult info;
            private final String prefix;
            private final boolean prefixMatch;
            private final CodeCompletionHandler.QueryType queryType;

            public CodeCompletionContextImpl(int caretOffset, ParserResult info, String prefix, boolean prefixMatch, CodeCompletionHandler.QueryType queryType) {
                this.caretOffset = caretOffset;
                this.info = info;
                this.prefix = prefix;
                this.prefixMatch = prefixMatch;
                this.queryType = queryType;
            }

            @Override
            public int getCaretOffset() {
                return this.caretOffset;
            }

            @Override
            public String getPrefix() {
                return this.prefix;
            }

            @Override
            public CodeCompletionHandler.QueryType getQueryType() {
                return this.queryType;
            }

            @Override
            public boolean isPrefixMatch() {
                return this.prefixMatch;
            }

            @Override
            public boolean isCaseSensitive() {
                return GsfCompletionProvider.isCaseSensitive();
            }

            @Override
            public ParserResult getParserResult() {
                return this.info;
            }
        }

    }

}

