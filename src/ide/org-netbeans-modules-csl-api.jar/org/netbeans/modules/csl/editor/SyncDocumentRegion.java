/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.csl.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.openide.ErrorManager;

public final class SyncDocumentRegion {
    private Document doc;
    private List<? extends MutablePositionRegion> regions;
    private List<? extends MutablePositionRegion> sortedRegions;
    private boolean regionsSortPerformed;

    public SyncDocumentRegion(Document doc, List<? extends MutablePositionRegion> regions) {
        this.doc = doc;
        this.regions = regions;
        this.regionsSortPerformed = PositionRegion.isRegionsSorted(regions);
        if (this.regionsSortPerformed) {
            this.sortedRegions = regions;
        } else {
            this.sortedRegions = new ArrayList<MutablePositionRegion>(regions);
            Collections.sort(this.sortedRegions, PositionRegion.getComparator());
        }
    }

    public int getRegionCount() {
        return this.regions.size();
    }

    public MutablePositionRegion getRegion(int regionIndex) {
        return this.regions.get(regionIndex);
    }

    public int getFirstRegionStartOffset() {
        return this.getRegion(0).getStartOffset();
    }

    public int getFirstRegionEndOffset() {
        return this.getRegion(0).getEndOffset();
    }

    public int getFirstRegionLength() {
        return this.getFirstRegionEndOffset() - this.getFirstRegionStartOffset();
    }

    public MutablePositionRegion getSortedRegion(int regionIndex) {
        return this.sortedRegions.get(regionIndex);
    }

    public void sync(int moveStartDownLength) {
        String firstRegionText;
        if (moveStartDownLength != 0) {
            MutablePositionRegion firstRegion = this.getRegion(0);
            try {
                Position newStartPos = this.doc.createPosition(firstRegion.getStartOffset() - moveStartDownLength);
                firstRegion.setStartPosition(newStartPos);
            }
            catch (BadLocationException e) {
                ErrorManager.getDefault().notify((Throwable)e);
            }
        }
        if ((firstRegionText = this.getFirstRegionText()) != null) {
            int regionCount = this.getRegionCount();
            for (int i = 1; i < regionCount; ++i) {
                MutablePositionRegion region = this.getRegion(i);
                int offset = region.getStartOffset();
                int length = region.getEndOffset() - offset;
                try {
                    if (CharSequenceUtilities.textEquals((CharSequence)firstRegionText, (CharSequence)DocumentUtilities.getText((Document)this.doc, (int)offset, (int)length))) continue;
                    if (firstRegionText.length() > 0) {
                        this.doc.insertString(offset, firstRegionText, null);
                    }
                    this.doc.remove(offset + firstRegionText.length(), length);
                    continue;
                }
                catch (BadLocationException e) {
                    ErrorManager.getDefault().notify((Throwable)e);
                }
            }
        }
    }

    private String getFirstRegionText() {
        return this.getRegionText(0);
    }

    private String getRegionText(int regionIndex) {
        try {
            MutablePositionRegion region = this.getRegion(regionIndex);
            int offset = region.getStartOffset();
            int length = region.getEndOffset() - offset;
            return this.doc.getText(offset, length);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
            return null;
        }
    }
}

