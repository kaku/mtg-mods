/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.hyperlink;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.CodeCompletionHandler2;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.csl.core.GsfHtmlFormatter;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.hyperlink.DeclarationPopup;
import org.netbeans.modules.csl.editor.hyperlink.PopupUtil;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class GoToSupport {
    private static final Logger LOG = Logger.getLogger(GoToSupport.class.getName());
    static final boolean IM_FEELING_LUCKY = Boolean.getBoolean("gsf.im_feeling_lucky");

    private GoToSupport() {
    }

    public static String getGoToElementTooltip(Document doc, int offset) {
        return GoToSupport.perform(doc, offset, true, new AtomicBoolean());
    }

    public static void performGoTo(final Document doc, final int offset) {
        final AtomicBoolean cancel = new AtomicBoolean();
        String name = NbBundle.getMessage(GoToSupport.class, (String)"NM_GoToDeclaration");
        ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                GoToSupport.perform(doc, offset, false, cancel);
            }
        }, (String)name, (AtomicBoolean)cancel, (boolean)false);
    }

    private static String perform(final Document doc, final int offset, final boolean tooltip, final AtomicBoolean cancel) {
        if (tooltip && PopupUtil.isPopupShowing()) {
            return null;
        }
        FileObject fo = GoToSupport.getFileObject(doc);
        if (fo == null) {
            return null;
        }
        Source js = Source.create((FileObject)fo);
        if (js == null) {
            return null;
        }
        final Language language = GoToSupport.identifyActiveFindersLanguage(doc, offset);
        if (language == null) {
            return null;
        }
        final String[] result = new String[]{null};
        final DeclarationFinder.DeclarationLocation[] location = new DeclarationFinder.DeclarationLocation[]{null};
        try {
            ParserManager.parse(Collections.singleton(js), (UserTask)new UserTask(){

                public void run(ResultIterator controller) throws Exception {
                    if (cancel.get()) {
                        return;
                    }
                    ResultIterator ri = GoToSupport.getResultIterator(controller, language.getMimeType());
                    if (ri == null) {
                        return;
                    }
                    Parser.Result embeddedResult = ri.getParserResult();
                    if (!(embeddedResult instanceof ParserResult)) {
                        return;
                    }
                    ParserResult info = (ParserResult)embeddedResult;
                    Language language2 = LanguageRegistry.getInstance().getLanguageByMimeType(info.getSnapshot().getMimeType());
                    if (language2 == null) {
                        return;
                    }
                    DeclarationFinder finder = language2.getDeclarationFinder();
                    if (finder == null) {
                        return;
                    }
                    location[0] = finder.findDeclaration(info, offset);
                    if (cancel.get()) {
                        return;
                    }
                    if (tooltip) {
                        ElementHandle element;
                        CodeCompletionHandler completer = language2.getCompletionProvider();
                        if (location[0] != DeclarationFinder.DeclarationLocation.NONE && completer != null && (element = location[0].getElement()) != null) {
                            Documentation documentation;
                            String documentationContent = completer instanceof CodeCompletionHandler2 ? ((documentation = ((CodeCompletionHandler2)completer).documentElement(info, element, new Callable<Boolean>(){

                                @Override
                                public Boolean call() throws Exception {
                                    return cancel.get();
                                }
                            })) != null ? documentation.getContent() : completer.document(info, element)) : completer.document(info, element);
                            if (documentationContent != null) {
                                result[0] = "<html><body>" + documentationContent;
                            }
                        }
                    } else if (location[0] != DeclarationFinder.DeclarationLocation.NONE && location[0] != null) {
                        final URL url = location[0].getUrl();
                        final String invalid = location[0].getInvalidMessage();
                        if (url != null) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    HtmlBrowser.URLDisplayer.getDefault().showURL(url);
                                }
                            });
                        } else if (invalid != null) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    StatusDisplayer.getDefault().setStatusText(invalid);
                                    Toolkit.getDefaultToolkit().beep();
                                }
                            });
                        } else if (!GoToSupport.IM_FEELING_LUCKY && location[0].getAlternativeLocations().size() > 0 && !PopupUtil.isPopupShowing()) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    if (!GoToSupport.chooseAlternatives(doc, offset, location[0].getAlternativeLocations())) {
                                        GoToSupport.openLocation(location[0]);
                                    }
                                }
                            });
                        } else {
                            GoToSupport.openLocation(location[0]);
                        }
                    } else {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                Toolkit.getDefaultToolkit().beep();
                            }
                        });
                    }
                }

            });
        }
        catch (ParseException pe) {
            LOG.log(Level.WARNING, null, (Throwable)pe);
        }
        return result[0];
    }

    private static ResultIterator getResultIterator(ResultIterator ri, String mimetype) {
        if (ri.getSnapshot().getMimeType().equals(mimetype)) {
            return ri;
        }
        for (Embedding e : ri.getEmbeddings()) {
            ResultIterator eri = ri.getResultIterator(e);
            if (e.getMimeType().equals(mimetype)) {
                return eri;
            }
            ResultIterator eeri = GoToSupport.getResultIterator(eri, mimetype);
            if (eeri == null) continue;
            return eeri;
        }
        return null;
    }

    private static void openLocation(DeclarationFinder.DeclarationLocation location) {
        FileObject f = location.getFileObject();
        int offset = location.getOffset();
        if (f != null && f.isValid()) {
            UiUtils.open(f, offset);
        }
    }

    private static JTextComponent findEditor(Document doc) {
        JTextComponent comp = EditorRegistry.lastFocusedComponent();
        if (comp.getDocument() == doc) {
            return comp;
        }
        List componentList = EditorRegistry.componentList();
        for (JTextComponent component : componentList) {
            if (comp.getDocument() != doc) continue;
            return comp;
        }
        return null;
    }

    private static boolean chooseAlternatives(Document doc, int offset, List<DeclarationFinder.AlternativeLocation> alternatives) {
        String caption = NbBundle.getMessage(GoToSupport.class, (String)"ChooseDecl");
        return GoToSupport.chooseAlternatives(doc, offset, caption, alternatives);
    }

    public static boolean chooseAlternatives(Document doc, int offset, String caption, List<DeclarationFinder.AlternativeLocation> alternatives) {
        Collections.sort(alternatives);
        int MAX_COUNT = 30;
        String previous = "";
        GsfHtmlFormatter formatter = new GsfHtmlFormatter();
        int count = 0;
        ArrayList<DeclarationFinder.AlternativeLocation> pruned = new ArrayList<DeclarationFinder.AlternativeLocation>(alternatives.size());
        for (DeclarationFinder.AlternativeLocation alt : alternatives) {
            String s = alt.getDisplayHtml(formatter);
            if (s.equals(previous)) continue;
            pruned.add(alt);
            previous = s;
            if (++count != MAX_COUNT) continue;
            break;
        }
        if ((alternatives = pruned).size() <= 1) {
            return false;
        }
        JTextComponent target = GoToSupport.findEditor(doc);
        if (target != null) {
            try {
                Rectangle rectangle = target.modelToView(offset);
                Point point = new Point(rectangle.x, rectangle.y + rectangle.height);
                SwingUtilities.convertPointToScreen(point, target);
                PopupUtil.showPopup(new DeclarationPopup(caption, alternatives), caption, point.x, point.y, true, 0);
                return true;
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return false;
    }

    private static FileObject getFileObject(Document doc) {
        return DataLoadersBridge.getDefault().getFileObject(doc);
    }

    public static int[] getIdentifierSpan(Document doc, int offset) {
        Language lang = GoToSupport.identifyActiveFindersLanguage(doc, offset);
        if (lang == null) {
            return null;
        }
        DeclarationFinder finder = lang.getDeclarationFinder();
        assert (finder != null);
        OffsetRange range = finder.getReferenceSpan(doc, offset);
        if (range == null || range == OffsetRange.NONE) {
            LOG.log(Level.WARNING, "Inconsistent DeclarationFinder {0} for offset {1}", new Object[]{finder, offset});
            return null;
        }
        return new int[]{range.getStart(), range.getEnd()};
    }

    private static Language identifyActiveFindersLanguage(Document doc, int offset) {
        FileObject fo = GoToSupport.getFileObject(doc);
        if (fo == null) {
            return null;
        }
        List<Language> list = LanguageRegistry.getInstance().getEmbeddedLanguages((BaseDocument)doc, offset);
        for (Language l : list) {
            DeclarationFinder finder = l.getDeclarationFinder();
            if (finder == null) continue;
            OffsetRange range = finder.getReferenceSpan(doc, offset);
            if (range == null) {
                throw new NullPointerException(finder + " violates its contract; should not return null from getReferenceSpan.");
            }
            if (range == OffsetRange.NONE) continue;
            return l;
        }
        return null;
    }

}

