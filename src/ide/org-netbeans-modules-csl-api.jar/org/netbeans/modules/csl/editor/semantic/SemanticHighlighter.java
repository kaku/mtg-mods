/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.editor.semantic;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.semantic.ColoringManager;
import org.netbeans.modules.csl.editor.semantic.GsfSemanticLayer;
import org.netbeans.modules.csl.editor.semantic.SequenceElement;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.filesystems.FileObject;

public class SemanticHighlighter
extends IndexingAwareParserResultTask<ParserResult> {
    private static final Logger LOG = Logger.getLogger(SemanticHighlighter.class.getName());
    private boolean cancelled;

    SemanticHighlighter() {
        super(TaskIndexingMode.ALLOWED_DURING_SCAN);
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public final synchronized void cancel() {
        this.cancelled = true;
    }

    protected final synchronized boolean isCancelled() {
        return this.cancelled;
    }

    protected final synchronized void resume() {
        this.cancelled = false;
    }

    public void run(ParserResult info, SchedulerEvent event) {
        this.resume();
        Document doc = info.getSnapshot().getSource().getDocument(false);
        if (doc == null) {
            return;
        }
        long startTime = System.currentTimeMillis();
        Source source = info.getSnapshot().getSource();
        final TreeSet newColoring = new TreeSet();
        try {
            ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    String mimeType = resultIterator.getSnapshot().getMimeType();
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
                    if (language != null) {
                        Parser.Result r;
                        ColoringManager manager = language.getColoringManager();
                        SemanticAnalyzer task = language.getSemanticAnalyzer();
                        if (manager != null && task != null && (r = resultIterator.getParserResult()) instanceof ParserResult) {
                            SemanticHighlighter.this.process(language, (ParserResult)r, newColoring);
                        }
                    }
                    for (Embedding e : resultIterator.getEmbeddings()) {
                        if (SemanticHighlighter.this.isCancelled()) {
                            return;
                        }
                        this.run(resultIterator.getResultIterator(e));
                    }
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
            return;
        }
        long endTime = System.currentTimeMillis();
        Logger.getLogger("TIMER").log(Level.FINE, "Semantic (" + source.getMimeType() + ")", new Object[]{source.getFileObject(), endTime - startTime});
        if (this.isCancelled()) {
            return;
        }
        final GsfSemanticLayer layer = GsfSemanticLayer.getLayer(SemanticHighlighter.class, doc);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                layer.setColorings(newColoring, -1);
            }
        });
    }

    private void process(Language language, ParserResult result, Set<SequenceElement> newColoring) throws ParseException {
        if (this.isCancelled()) {
            return;
        }
        ColoringManager manager = language.getColoringManager();
        SemanticAnalyzer task = language.getSemanticAnalyzer();
        try {
            task.run((Parser.Result)result, null);
        }
        catch (Exception ex) {
            LOG.log(Level.WARNING, "SemanticAnalyzer = " + (Object)((Object)task) + "; Language = " + language + " (mimetype = " + language.getMimeType() + "; ParserResult = " + (Object)((Object)result) + "(mimepath = " + (Object)result.getSnapshot().getMimePath() + ")", ex);
        }
        if (this.isCancelled()) {
            task.cancel();
            return;
        }
        Map<OffsetRange, Set<ColoringAttributes>> highlights = task.getHighlights();
        if (highlights != null) {
            for (OffsetRange range : highlights.keySet()) {
                Set<ColoringAttributes> colors = highlights.get(range);
                if (colors == null) continue;
                ColoringAttributes.Coloring c = manager.getColoring(colors);
                newColoring.add(new SequenceElement(language, range, c));
                if (!this.isCancelled()) continue;
                return;
            }
        }
    }

}

