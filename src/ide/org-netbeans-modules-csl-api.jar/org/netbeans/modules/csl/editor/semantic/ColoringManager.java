/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.spi.editor.highlighting.HighlightAttributeValue
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.semantic;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.spi.editor.highlighting.HighlightAttributeValue;
import org.openide.util.NbBundle;

public final class ColoringManager {
    private String mimeType;
    private final Map<Set<ColoringAttributes>, String> type2Coloring;
    Map<Set<ColoringAttributes>, ColoringAttributes.Coloring> COLORING_MAP = new IdentityHashMap<Set<ColoringAttributes>, ColoringAttributes.Coloring>();

    public ColoringManager(String mimeType) {
        this.mimeType = mimeType;
        this.type2Coloring = new LinkedHashMap<Set<ColoringAttributes>, String>();
        this.put("mark-occurrences", ColoringAttributes.MARK_OCCURRENCES);
        this.put("mod-abstract", ColoringAttributes.ABSTRACT);
        this.put("mod-annotation-type", ColoringAttributes.ANNOTATION_TYPE);
        this.put("mod-class", ColoringAttributes.CLASS);
        this.put("mod-constructor", ColoringAttributes.CONSTRUCTOR);
        this.put("mod-custom1", ColoringAttributes.CUSTOM1);
        this.put("mod-custom2", ColoringAttributes.CUSTOM2);
        this.put("mod-custom3", ColoringAttributes.CUSTOM3);
        this.put("mod-declaration", ColoringAttributes.DECLARATION);
        this.put("mod-deprecated", ColoringAttributes.DEPRECATED);
        this.put("mod-enum", ColoringAttributes.ENUM);
        this.put("mod-field", ColoringAttributes.FIELD);
        this.put("mod-global", ColoringAttributes.GLOBAL);
        this.put("mod-interface", ColoringAttributes.INTERFACE);
        this.put("mod-local-variable", ColoringAttributes.LOCAL_VARIABLE);
        this.put("mod-method", ColoringAttributes.METHOD);
        this.put("mod-package-private", ColoringAttributes.PACKAGE_PRIVATE);
        this.put("mod-parameter", ColoringAttributes.PARAMETER);
        this.put("mod-private", ColoringAttributes.PRIVATE);
        this.put("mod-protected", ColoringAttributes.PROTECTED);
        this.put("mod-public", ColoringAttributes.PUBLIC);
        this.put("mod-regexp", ColoringAttributes.REGEXP);
        this.put("mod-static", ColoringAttributes.STATIC);
        this.put("mod-type-parameter-declaration", ColoringAttributes.TYPE_PARAMETER_DECLARATION);
        this.put("mod-type-parameter-use", ColoringAttributes.TYPE_PARAMETER_USE);
        this.put("mod-undefined", ColoringAttributes.UNDEFINED);
        this.put("mod-unused", ColoringAttributes.UNUSED);
        this.COLORING_MAP.put(ColoringAttributes.UNUSED_SET, this.getColoring(ColoringAttributes.UNUSED_SET));
        this.COLORING_MAP.put(ColoringAttributes.FIELD_SET, this.getColoring(ColoringAttributes.FIELD_SET));
        this.COLORING_MAP.put(ColoringAttributes.STATIC_FIELD_SET, this.getColoring(ColoringAttributes.STATIC_FIELD_SET));
        this.COLORING_MAP.put(ColoringAttributes.PARAMETER_SET, this.getColoring(ColoringAttributes.PARAMETER_SET));
        this.COLORING_MAP.put(ColoringAttributes.CUSTOM1_SET, this.getColoring(ColoringAttributes.CUSTOM1_SET));
        this.COLORING_MAP.put(ColoringAttributes.CUSTOM2_SET, this.getColoring(ColoringAttributes.CUSTOM2_SET));
        this.COLORING_MAP.put(ColoringAttributes.CONSTRUCTOR_SET, this.getColoring(ColoringAttributes.CONSTRUCTOR_SET));
        this.COLORING_MAP.put(ColoringAttributes.METHOD_SET, this.getColoring(ColoringAttributes.METHOD_SET));
        this.COLORING_MAP.put(ColoringAttributes.CLASS_SET, this.getColoring(ColoringAttributes.CLASS_SET));
        this.COLORING_MAP.put(ColoringAttributes.GLOBAL_SET, this.getColoring(ColoringAttributes.GLOBAL_SET));
        this.COLORING_MAP.put(ColoringAttributes.REGEXP_SET, this.getColoring(ColoringAttributes.REGEXP_SET));
        this.COLORING_MAP.put(ColoringAttributes.STATIC_SET, this.getColoring(ColoringAttributes.STATIC_SET));
    }

    private /* varargs */ void put(String coloring, ColoringAttributes ... attributes) {
        EnumSet<ColoringAttributes> attribs = EnumSet.copyOf(Arrays.asList(attributes));
        this.type2Coloring.put(attribs, coloring);
    }

    public ColoringAttributes.Coloring getColoring(Set<ColoringAttributes> attrs) {
        ColoringAttributes.Coloring c = this.COLORING_MAP.get(attrs);
        if (c != null) {
            return c;
        }
        c = ColoringAttributes.empty();
        for (ColoringAttributes color : attrs) {
            c = ColoringAttributes.add(c, color);
        }
        return c;
    }

    public AttributeSet getColoringImpl(ColoringAttributes.Coloring colorings) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.get((String)this.mimeType)).lookup(FontColorSettings.class);
        if (fcs == null) {
            return AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[0]);
        }
        assert (fcs != null);
        LinkedList<AttributeSet> attribs = new LinkedList<AttributeSet>();
        EnumSet<ColoringAttributes> es = EnumSet.noneOf(ColoringAttributes.class);
        es.addAll(colorings);
        if (colorings.contains((Object)ColoringAttributes.UNUSED)) {
            attribs.add(AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.Tooltip, new UnusedTooltipResolver()}));
            attribs.add(AttributesUtilities.createImmutable((Object[])new Object[]{"unused-browseable", Boolean.TRUE}));
        }
        for (Map.Entry<Set<ColoringAttributes>, String> attribs2Colorings : this.type2Coloring.entrySet()) {
            if (!es.containsAll((Collection)attribs2Colorings.getKey())) continue;
            String key = attribs2Colorings.getValue();
            es.removeAll((Collection)attribs2Colorings.getKey());
            if (key == null) continue;
            AttributeSet colors = fcs.getTokenFontColors(key);
            if (colors == null) {
                Logger.getLogger(ColoringManager.class.getName()).log(Level.SEVERE, "no colors for: {0} with mime" + this.mimeType, key);
                continue;
            }
            attribs.add(ColoringManager.adjustAttributes(colors));
        }
        Collections.reverse(attribs);
        AttributeSet result = AttributesUtilities.createComposite((AttributeSet[])attribs.toArray(new AttributeSet[0]));
        return result;
    }

    private static AttributeSet adjustAttributes(AttributeSet as) {
        LinkedList<Object> attrs = new LinkedList<Object>();
        Enumeration e = as.getAttributeNames();
        while (e.hasMoreElements()) {
            Object key = e.nextElement();
            Object value = as.getAttribute(key);
            if (value == Boolean.FALSE) continue;
            attrs.add(key);
            attrs.add(value);
        }
        return AttributesUtilities.createImmutable((Object[])attrs.toArray());
    }

    final class UnusedTooltipResolver
    implements HighlightAttributeValue<String> {
        UnusedTooltipResolver() {
        }

        public String getValue(JTextComponent component, Document document, Object attributeKey, int startOffset, int endOffset) {
            return NbBundle.getMessage(ColoringManager.class, (String)"LBL_UNUSED");
        }
    }

}

