/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.errorstripe.privatespi.Mark
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider
 *  org.netbeans.modules.editor.errorstripe.privatespi.Status
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.csl.editor.semantic;

import java.awt.Color;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;

public class OccurrencesMarkProvider
extends MarkProvider {
    private static Map<Document, Reference<OccurrencesMarkProvider>> providers = new WeakHashMap<Document, Reference<OccurrencesMarkProvider>>();
    private List<Mark> semantic = Collections.emptyList();
    private List<Mark> occurrences = Collections.emptyList();
    private List<Mark> joint = Collections.emptyList();

    public static synchronized OccurrencesMarkProvider get(Document doc) {
        OccurrencesMarkProvider p;
        Reference<OccurrencesMarkProvider> ref = providers.get(doc);
        OccurrencesMarkProvider occurrencesMarkProvider = p = ref != null ? ref.get() : null;
        if (p == null) {
            p = new OccurrencesMarkProvider();
            providers.put(doc, new WeakReference<OccurrencesMarkProvider>(p));
        }
        return p;
    }

    private OccurrencesMarkProvider() {
    }

    public synchronized List getMarks() {
        return this.joint;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setSematic(Collection<Mark> s) {
        ArrayList<Mark> nue;
        List<Mark> old;
        OccurrencesMarkProvider occurrencesMarkProvider = this;
        synchronized (occurrencesMarkProvider) {
            this.semantic = new ArrayList<Mark>(s);
            old = this.joint;
            this.joint = new ArrayList<Mark>();
            nue = this.joint;
            this.joint.addAll(this.semantic);
            this.joint.addAll(this.occurrences);
        }
        this.firePropertyChange("marks", old, nue);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setOccurrences(Collection<Mark> s) {
        ArrayList<Mark> nue;
        List<Mark> old;
        OccurrencesMarkProvider occurrencesMarkProvider = this;
        synchronized (occurrencesMarkProvider) {
            this.occurrences = new ArrayList<Mark>(s);
            old = this.joint;
            this.joint = new ArrayList<Mark>();
            nue = this.joint;
            this.joint.addAll(this.semantic);
            this.joint.addAll(this.occurrences);
        }
        this.firePropertyChange("marks", old, nue);
    }

    public static Collection<Mark> createMarks(final Document doc, final List<OffsetRange> bag, final Color color, final String tooltip) {
        final LinkedList<Mark> result = new LinkedList<Mark>();
        doc.render(new Runnable(){

            @Override
            public void run() {
                for (OffsetRange span : bag) {
                    try {
                        if (span.getStart() >= doc.getLength()) continue;
                        result.add(new MarkImpl(doc, doc.createPosition(span.getStart()), color, tooltip));
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
        });
        return result;
    }

    private static final class MarkImpl
    implements Mark {
        private Document doc;
        private Position startOffset;
        private Color color;
        private String tooltip;

        public MarkImpl(Document doc, Position startOffset, Color color, String tooltip) {
            this.doc = doc;
            this.startOffset = startOffset;
            this.color = color;
            this.tooltip = tooltip;
        }

        public int getType() {
            return 1;
        }

        public Status getStatus() {
            return Status.STATUS_OK;
        }

        public int getPriority() {
            return 1000;
        }

        public Color getEnhancedColor() {
            return this.color;
        }

        public int[] getAssignedLines() {
            int line = NbDocument.findLineNumber((StyledDocument)((StyledDocument)this.doc), (int)this.startOffset.getOffset());
            return new int[]{line, line};
        }

        public String getShortDescription() {
            return this.tooltip;
        }
    }

}

