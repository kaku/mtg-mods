/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.ext.DataAccessor
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.csl.editor;

import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.netbeans.editor.ext.DataAccessor;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class FileObjectAccessor
implements DataAccessor {
    FileObject fo;
    InputStream inputStream;
    FileOutputStream fos;
    int actOff;

    public FileObjectAccessor(FileObject fo) {
        this.fo = fo;
    }

    public void append(byte[] buffer, int off, int len) throws IOException {
        this.fos = new FileOutputStream(FileUtil.toFile((FileObject)this.fo).getPath(), true);
        this.fos.write(buffer, off, len);
        this.fos.flush();
        this.fos.close();
        this.fos = null;
    }

    public void read(byte[] buffer, int off, int len) throws IOException {
        int count;
        int n = 0;
        off = this.actOff + off;
        do {
            if ((count = this.readStream(buffer, off + n, len - n)) >= 0) continue;
            throw new EOFException();
        } while ((n += count) < len);
    }

    public void open(boolean requestWrite) throws IOException {
        if (!this.fo.existsExt(this.fo.getExt())) {
            this.resetFile();
        }
    }

    public void close() throws IOException {
        if (this.inputStream != null) {
            this.inputStream.close();
        }
        this.inputStream = null;
    }

    public long getFilePointer() throws IOException {
        return this.actOff;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resetFile() throws IOException {
        FileObject folder = this.fo.getParent();
        String name = this.fo.getName();
        String ext = this.fo.getExt();
        FileLock lock = this.fo.lock();
        try {
            this.fo.delete(lock);
        }
        finally {
            lock.releaseLock();
        }
        this.fo = folder.createData(name, ext);
        this.actOff = 0;
    }

    public void seek(long pos) throws IOException {
        this.actOff = (int)pos;
    }

    private int readStream(byte[] buffer, int off, int len) throws IOException {
        int read = this.getStream(off).read(buffer, 0, len);
        this.actOff += read;
        return read;
    }

    private InputStream getStream(int off) throws IOException {
        if (this.inputStream == null) {
            this.inputStream = this.fo.getInputStream();
            this.inputStream.skip(off);
        } else if (off >= this.actOff) {
            this.inputStream.skip(off - this.actOff);
        } else {
            this.inputStream.close();
            this.inputStream = this.fo.getInputStream();
            this.inputStream.skip(off);
        }
        this.actOff = off;
        return this.inputStream;
    }

    public int getFileLength() {
        return (int)this.fo.getSize();
    }

    public String toString() {
        return this.fo.toString();
    }
}

