/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.csl.editor.fold;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class GsfFoldScheduler
extends Scheduler {
    private JTextComponent currentEditor;
    private Document currentDocument;

    public GsfFoldScheduler() {
        this.setEditor(EditorRegistry.focusedComponent());
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new AListener());
    }

    protected void setEditor(JTextComponent editor) {
        if (editor != null) {
            Document document = editor.getDocument();
            if (this.currentDocument == document) {
                return;
            }
            this.currentDocument = document;
            Source source = Source.create((Document)this.currentDocument);
            this.schedule(source, new SchedulerEvent((Object)this){});
        } else {
            this.currentDocument = null;
            this.schedule(null, null);
        }
    }

    public String toString() {
        return this.getClass().getSimpleName();
    }

    protected SchedulerEvent createSchedulerEvent(SourceModificationEvent event) {
        if (event.getModifiedSource() == this.getSource()) {
            return new SchedulerEvent((Object)this){};
        }
        return null;
    }

    public static void reschedule() {
        for (Scheduler s : Lookup.getDefault().lookupAll(Scheduler.class)) {
            if (!(s instanceof GsfFoldScheduler)) continue;
            GsfFoldScheduler gsfScheduler = (GsfFoldScheduler)s;
            gsfScheduler.schedule(new SchedulerEvent((Object)gsfScheduler){});
        }
    }

    private class AListener
    implements PropertyChangeListener {
        private AListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName() == null || evt.getPropertyName().equals("focusedDocument") || evt.getPropertyName().equals("focusGained")) {
                FileObject fileObject;
                Document document;
                JTextComponent editor = EditorRegistry.focusedComponent();
                if (editor == GsfFoldScheduler.this.currentEditor) {
                    return;
                }
                GsfFoldScheduler.this.currentEditor = editor;
                if (GsfFoldScheduler.this.currentEditor != null && (fileObject = NbEditorUtilities.getFileObject((Document)(document = GsfFoldScheduler.this.currentEditor.getDocument()))) == null) {
                    return;
                }
                GsfFoldScheduler.this.setEditor(GsfFoldScheduler.this.currentEditor);
            } else if (evt.getPropertyName().equals("lastFocusedRemoved")) {
                GsfFoldScheduler.this.currentEditor = null;
                GsfFoldScheduler.this.setEditor(null);
            }
        }
    }

}

