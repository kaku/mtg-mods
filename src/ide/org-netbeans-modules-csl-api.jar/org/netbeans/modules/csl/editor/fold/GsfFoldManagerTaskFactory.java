/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.editor.fold;

import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.editor.fold.GsfFoldManager;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.openide.filesystems.FileObject;

public final class GsfFoldManagerTaskFactory
extends AbstractTaskFactory {
    public GsfFoldManagerTaskFactory() {
        super(true);
    }

    @Override
    protected Collection<? extends SchedulerTask> createTasks(Language l, Snapshot snapshot) {
        FileObject file = snapshot.getSource().getFileObject();
        if (file != null) {
            GsfFoldManager.JavaElementFoldTask t = GsfFoldManager.JavaElementFoldTask.getTask(file);
            if (GsfFoldManager.LOG.isLoggable(Level.FINER)) {
                GsfFoldManager.LOG.log(Level.FINER, "Scheduling task for file: {0} -> {1}, Thread: {2}", new Object[]{file, t, Thread.currentThread()});
            }
            return Collections.singleton(t);
        }
        if (GsfFoldManager.LOG.isLoggable(Level.FINE)) {
            GsfFoldManager.LOG.log(Level.FINE, "FileObject is null: {0}", (Object)snapshot.getSource());
        }
        return null;
    }
}

