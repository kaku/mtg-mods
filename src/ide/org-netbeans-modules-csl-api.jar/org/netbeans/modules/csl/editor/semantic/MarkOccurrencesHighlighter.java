/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.editor.errorstripe.privatespi.Mark
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.openide.ErrorManager
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.semantic;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.editor.semantic.GsfSemanticLayer;
import org.netbeans.modules.csl.editor.semantic.OccurrencesMarkProvider;
import org.netbeans.modules.csl.editor.semantic.SequenceElement;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.ErrorManager;
import org.openide.util.NbBundle;

public class MarkOccurrencesHighlighter
extends ParserResultTask<ParserResult> {
    private static final Logger LOG = Logger.getLogger(MarkOccurrencesHighlighter.class.getName());
    private final Language language;
    private final Snapshot snapshot;
    private int version;
    static ColoringAttributes.Coloring MO = ColoringAttributes.add(ColoringAttributes.empty(), ColoringAttributes.MARK_OCCURRENCES);
    public static final Color ES_COLOR = new Color(175, 172, 102);
    private boolean canceled;

    MarkOccurrencesHighlighter(Language language, Snapshot snapshot) {
        this.language = language;
        this.snapshot = snapshot;
    }

    public void run(ParserResult info, SchedulerEvent event) {
        this.resume();
        Document doc = this.snapshot.getSource().getDocument(false);
        if (doc == null) {
            LOG.log(Level.INFO, "MarkOccurencesHighlighter: Cannot get document!");
            return;
        }
        if (!(event instanceof CursorMovedSchedulerEvent)) {
            return;
        }
        int caretPosition = ((CursorMovedSchedulerEvent)event).getCaretOffset();
        if (this.isCancelled()) {
            return;
        }
        int snapshotOffset = info.getSnapshot().getEmbeddedOffset(caretPosition);
        if (snapshotOffset == -1) {
            return;
        }
        List<OffsetRange> bag = this.processImpl(info, doc, caretPosition);
        if (bag == null) {
            return;
        }
        if (this.isCancelled()) {
            return;
        }
        GsfSemanticLayer layer = GsfSemanticLayer.getLayer(MarkOccurrencesHighlighter.class, doc);
        TreeSet<SequenceElement> seqs = new TreeSet<SequenceElement>();
        if (bag.size() > 0) {
            for (OffsetRange range : bag) {
                if (range == OffsetRange.NONE) continue;
                SequenceElement s = new SequenceElement(this.language, range, MO);
                seqs.add(s);
            }
        }
        layer.setColorings(seqs, this.version++);
        OccurrencesMarkProvider.get(doc).setOccurrences(OccurrencesMarkProvider.createMarks(doc, bag, ES_COLOR, NbBundle.getMessage(MarkOccurrencesHighlighter.class, (String)"LBL_ES_TOOLTIP")));
    }

    @NonNull
    List<OffsetRange> processImpl(ParserResult info, Document doc, int caretPosition) {
        Map<OffsetRange, ColoringAttributes> highlights;
        OccurrencesFinder finder = this.language.getOccurrencesFinder();
        assert (finder != null);
        finder.setCaretPosition(caretPosition);
        try {
            finder.run((Parser.Result)info, null);
        }
        catch (Exception ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
        if (this.isCancelled()) {
            finder.cancel();
        }
        return (highlights = finder.getOccurrences()) == null ? null : new ArrayList<OffsetRange>(highlights.keySet());
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }

    public final synchronized void cancel() {
        this.canceled = true;
    }

    protected final synchronized boolean isCancelled() {
        return this.canceled;
    }

    protected final synchronized void resume() {
        this.canceled = false;
    }

    public static AbstractHighlightsContainer getHighlightsBag(Document doc) {
        GsfSemanticLayer highlight = GsfSemanticLayer.getLayer(MarkOccurrencesHighlighter.class, doc);
        return highlight;
    }
}

