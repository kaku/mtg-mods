/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 */
package org.netbeans.modules.csl.editor.semantic;

import javax.swing.text.Document;
import org.netbeans.modules.csl.editor.InstantRenamePerformer;
import org.netbeans.modules.csl.editor.semantic.GsfSemanticLayer;
import org.netbeans.modules.csl.editor.semantic.MarkOccurrencesHighlighter;
import org.netbeans.modules.csl.editor.semantic.SemanticHighlighter;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

public class HighlightsLayerFactoryImpl
implements HighlightsLayerFactory {
    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        GsfSemanticLayer semantic = GsfSemanticLayer.getLayer(SemanticHighlighter.class, context.getDocument());
        GsfSemanticLayer occurrences = (GsfSemanticLayer)MarkOccurrencesHighlighter.getHighlightsBag(context.getDocument());
        semantic.clearColoringCache();
        return new HighlightsLayer[]{HighlightsLayer.create((String)(SemanticHighlighter.class.getName() + "-1"), (ZOrder)ZOrder.SYNTAX_RACK.forPosition(1000), (boolean)false, (HighlightsContainer)semantic), HighlightsLayer.create((String)MarkOccurrencesHighlighter.class.getName(), (ZOrder)ZOrder.CARET_RACK.forPosition(50), (boolean)false, (HighlightsContainer)occurrences), HighlightsLayer.create((String)InstantRenamePerformer.class.getName(), (ZOrder)ZOrder.CARET_RACK.forPosition(75), (boolean)false, (HighlightsContainer)InstantRenamePerformer.getHighlightsBag(context.getDocument()))};
    }
}

