/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.hyperlink;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Set;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.UiUtils;
import org.netbeans.modules.csl.core.GsfHtmlFormatter;
import org.netbeans.modules.csl.editor.hyperlink.PopupUtil;
import org.netbeans.modules.csl.navigation.Icons;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

public class DeclarationPopup
extends JPanel
implements FocusListener {
    private String caption;
    private List<DeclarationFinder.AlternativeLocation> declarations;
    private JLabel jLabel1;
    private JList jList1;
    private JScrollPane jScrollPane1;

    public DeclarationPopup(String caption, List<DeclarationFinder.AlternativeLocation> declarations) {
        this.caption = caption;
        this.declarations = declarations;
        this.initComponents();
        this.jList1.setCursor(Cursor.getPredefinedCursor(12));
        this.addFocusListener(this);
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.setFocusCycleRoot(true);
        this.setLayout(new GridBagLayout());
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText(this.caption);
        this.jLabel1.setFocusable(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.jList1.setModel(this.createListModel());
        this.jList1.setCellRenderer(new RendererImpl());
        this.jList1.setSelectedIndex(0);
        this.jList1.setVisibleRowCount(this.declarations.size());
        this.jList1.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                DeclarationPopup.this.jList1KeyPressed(evt);
            }
        });
        this.jList1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                DeclarationPopup.this.jList1MouseClicked(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jList1);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jScrollPane1, gridBagConstraints);
    }

    private void jList1MouseClicked(MouseEvent evt) {
        if (evt.getButton() == 1 && evt.getClickCount() == 1) {
            this.openSelected();
        }
    }

    private void jList1KeyPressed(KeyEvent evt) {
        if (evt.getKeyCode() == 10 && evt.getModifiers() == 0) {
            this.openSelected();
        }
    }

    private void openSelected() {
        DeclarationFinder.AlternativeLocation desc = (DeclarationFinder.AlternativeLocation)this.jList1.getSelectedValue();
        if (desc != null) {
            DeclarationFinder.DeclarationLocation location = desc.getLocation();
            if (location == DeclarationFinder.DeclarationLocation.NONE) {
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(DeclarationPopup.class, (String)"InvalidLoc"));
                Toolkit.getDefaultToolkit().beep();
            } else {
                String invalid = location.getInvalidMessage();
                if (invalid != null) {
                    StatusDisplayer.getDefault().setStatusText(invalid);
                    Toolkit.getDefaultToolkit().beep();
                } else {
                    FileObject fileObject = location.getFileObject();
                    if (fileObject != null) {
                        UiUtils.open(fileObject, location.getOffset());
                    } else {
                        Toolkit.getDefaultToolkit().beep();
                    }
                }
            }
        }
        PopupUtil.hidePopup();
    }

    private ListModel createListModel() {
        DefaultListModel<DeclarationFinder.AlternativeLocation> dlm = new DefaultListModel<DeclarationFinder.AlternativeLocation>();
        for (DeclarationFinder.AlternativeLocation el : this.declarations) {
            dlm.addElement(el);
        }
        return dlm;
    }

    @Override
    public void focusGained(FocusEvent arg0) {
        this.jList1.requestFocus();
        this.jList1.requestFocusInWindow();
    }

    @Override
    public void focusLost(FocusEvent arg0) {
    }

    private static class RendererImpl
    extends DefaultListCellRenderer {
        private RendererImpl() {
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof DeclarationFinder.AlternativeLocation) {
                DeclarationFinder.AlternativeLocation desc = (DeclarationFinder.AlternativeLocation)value;
                ElementHandle e = desc.getElement();
                ImageIcon icon = Icons.getElementIcon(e.getKind(), e.getModifiers());
                this.setIcon(icon);
                GsfHtmlFormatter formatter = new GsfHtmlFormatter();
                String s = desc.getDisplayHtml(formatter);
                this.setText("<html>" + s);
            }
            return c;
        }
    }

}

