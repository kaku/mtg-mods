/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkType
 */
package org.netbeans.modules.csl.editor.hyperlink;

import java.util.EnumSet;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.modules.csl.editor.hyperlink.GoToSupport;

public final class GsfHyperlinkProvider
implements HyperlinkProviderExt {
    public Set<HyperlinkType> getSupportedHyperlinkTypes() {
        return EnumSet.of(HyperlinkType.GO_TO_DECLARATION);
    }

    public boolean isHyperlinkPoint(Document doc, int offset, HyperlinkType type) {
        return this.getHyperlinkSpan(doc, offset, type) != null;
    }

    public int[] getHyperlinkSpan(Document doc, int offset, HyperlinkType type) {
        return GoToSupport.getIdentifierSpan(doc, offset);
    }

    public void performClickAction(Document doc, int offset, HyperlinkType type) {
        GoToSupport.performGoTo(doc, offset);
    }

    public String getTooltipText(Document doc, int offset, HyperlinkType type) {
        return GoToSupport.getGoToElementTooltip(doc, offset);
    }
}

