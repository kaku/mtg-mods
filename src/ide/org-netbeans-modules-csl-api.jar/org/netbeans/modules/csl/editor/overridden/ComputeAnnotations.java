/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.ParserResultTask
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.openide.filesystems.FileObject
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.overridden;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.OverridingMethods;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.core.AbstractTaskFactory;
import org.netbeans.modules.csl.core.GsfHtmlFormatter;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.overridden.AnnotationType;
import org.netbeans.modules.csl.editor.overridden.AnnotationsHolder;
import org.netbeans.modules.csl.editor.overridden.IsOverriddenAnnotation;
import org.netbeans.modules.csl.editor.overridden.OverrideDescription;
import org.netbeans.modules.csl.navigation.ElementScanningTask;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.openide.filesystems.FileObject;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;

public class ComputeAnnotations
extends ParserResultTask<Parser.Result> {
    private static final Logger LOG = Logger.getLogger(ComputeAnnotations.class.getName());
    private final AtomicBoolean cancel = new AtomicBoolean();

    public void run(Parser.Result result, SchedulerEvent event) {
        if (!(result instanceof ParserResult)) {
            return;
        }
        this.cancel.set(false);
        final FileObject file = result.getSnapshot().getSource().getFileObject();
        if (file == null) {
            return;
        }
        final StyledDocument doc = (StyledDocument)result.getSnapshot().getSource().getDocument(false);
        if (doc == null) {
            return;
        }
        final LinkedList<IsOverriddenAnnotation> annotations = new LinkedList<IsOverriddenAnnotation>();
        try {
            ParserManager.parse(Collections.singleton(result.getSnapshot().getSource()), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    Language language = LanguageRegistry.getInstance().getLanguageByMimeType(resultIterator.getSnapshot().getMimeType());
                    if (language != null) {
                        Parser.Result r;
                        StructureScanner scanner = language.getStructure();
                        OverridingMethods om = language.getOverridingMethods();
                        if (scanner != null && om != null && (r = resultIterator.getParserResult()) instanceof ParserResult) {
                            HashMap<ElementHandle, Collection<? extends DeclarationFinder.AlternativeLocation>> overriding = new HashMap<ElementHandle, Collection<? extends DeclarationFinder.AlternativeLocation>>();
                            HashMap<ElementHandle, Collection<? extends DeclarationFinder.AlternativeLocation>> overridden = new HashMap<ElementHandle, Collection<? extends DeclarationFinder.AlternativeLocation>>();
                            HashSet<ElementHandle> seen = new HashSet<ElementHandle>();
                            HashMap<ElementHandle, ElementHandle> node2Parent = new HashMap<ElementHandle, ElementHandle>();
                            List<? extends StructureItem> children = ElementScanningTask.findCachedStructure(resultIterator.getSnapshot(), r);
                            if (children == null) {
                                long startTime = System.currentTimeMillis();
                                children = scanner.scan((ParserResult)r);
                                long endTime = System.currentTimeMillis();
                                Logger.getLogger("TIMER").log(Level.FINE, "Structure (" + language.getMimeType() + ")", new Object[]{file, endTime - startTime});
                                ElementScanningTask.markProcessed(r, children);
                            }
                            LinkedList<? extends StructureItem> todo = new LinkedList<StructureItem>(children);
                            while (!todo.isEmpty()) {
                                Collection<? extends DeclarationFinder.AlternativeLocation> on;
                                Collection<? extends DeclarationFinder.AlternativeLocation> ov;
                                StructureItem i = todo.remove(0);
                                todo.addAll(i.getNestedItems());
                                for (StructureItem nested : i.getNestedItems()) {
                                    if (node2Parent.containsKey(nested.getElementHandle())) continue;
                                    node2Parent.put(nested.getElementHandle(), i.getElementHandle());
                                }
                                if (!seen.add(i.getElementHandle())) continue;
                                if (i.getElementHandle().getKind() != ElementKind.CLASS && i.getElementHandle().getKind() != ElementKind.INTERFACE && (ov = om.overrides((ParserResult)r, i.getElementHandle())) != null && !ov.isEmpty()) {
                                    overriding.put(i.getElementHandle(), ov);
                                }
                                if (!om.isOverriddenBySupported((ParserResult)r, i.getElementHandle()) || (on = om.overriddenBy((ParserResult)r, i.getElementHandle())) == null || on.isEmpty()) continue;
                                overridden.put(i.getElementHandle(), on);
                            }
                            ComputeAnnotations.this.createAnnotations((ParserResult)r, doc, overriding, node2Parent, false, annotations);
                            ComputeAnnotations.this.createAnnotations((ParserResult)r, doc, overridden, node2Parent, true, annotations);
                        }
                    }
                    for (Embedding e : resultIterator.getEmbeddings()) {
                        this.run(resultIterator.getResultIterator(e));
                    }
                }
            });
        }
        catch (ParseException e) {
            LOG.log(Level.WARNING, null, (Throwable)e);
        }
        AnnotationsHolder holder = AnnotationsHolder.get(file);
        if (holder != null) {
            holder.setNewAnnotations(annotations);
        }
    }

    private void createAnnotations(ParserResult r, StyledDocument doc, Map<ElementHandle, Collection<? extends DeclarationFinder.AlternativeLocation>> descriptions, Map<ElementHandle, ElementHandle> node2Parent, boolean overridden, List<IsOverriddenAnnotation> annotations) {
        if (descriptions != null) {
            for (Map.Entry<ElementHandle, Collection<? extends DeclarationFinder.AlternativeLocation>> e : descriptions.entrySet()) {
                String dn;
                AnnotationType type;
                OffsetRange range = e.getKey().getOffsetRange(r);
                if (range == null) continue;
                if (overridden) {
                    ElementHandle enclosing = node2Parent.get(e.getKey());
                    if (enclosing != null && enclosing.getKind() == ElementKind.INTERFACE || e.getKey().getKind() == ElementKind.INTERFACE) {
                        type = AnnotationType.HAS_IMPLEMENTATION;
                        dn = NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_HasImplementations");
                    } else {
                        type = AnnotationType.IS_OVERRIDDEN;
                        dn = NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_IsOverridden");
                    }
                } else {
                    StringBuilder tooltip = new StringBuilder();
                    boolean wasOverrides = false;
                    boolean newline = false;
                    for (DeclarationFinder.AlternativeLocation loc : e.getValue()) {
                        if (newline) {
                            tooltip.append("\n");
                        }
                        newline = true;
                        if (loc.getElement().getModifiers().contains((Object)Modifier.ABSTRACT)) {
                            tooltip.append(NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_Implements", (Object)loc.getDisplayHtml(new GsfHtmlFormatter())));
                            continue;
                        }
                        tooltip.append(NbBundle.getMessage(ComputeAnnotations.class, (String)"TP_Overrides", (Object)loc.getDisplayHtml(new GsfHtmlFormatter())));
                        wasOverrides = true;
                    }
                    type = wasOverrides ? AnnotationType.OVERRIDES : AnnotationType.IMPLEMENTS;
                    dn = tooltip.toString();
                }
                Position pos = ComputeAnnotations.getPosition(doc, range.getStart());
                if (pos == null) continue;
                LinkedList<OverrideDescription> ods = new LinkedList<OverrideDescription>();
                for (DeclarationFinder.AlternativeLocation l : e.getValue()) {
                    ods.add(new OverrideDescription(l, overridden));
                }
                annotations.add(new IsOverriddenAnnotation(doc, pos, type, dn, ods));
            }
        }
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void cancel() {
        this.cancel.set(true);
    }

    private static Position getPosition(StyledDocument doc, int offset) {
        class Impl
        implements Runnable {
            private Position pos;
            final /* synthetic */ StyledDocument val$doc;

            Impl() {
                this.val$doc = var2_2;
            }

            @Override
            public void run() {
                if (val$offset < 0 || val$offset >= this.val$doc.getLength()) {
                    return;
                }
                try {
                    this.pos = this.val$doc.createPosition(val$offset - NbDocument.findLineColumn((StyledDocument)this.val$doc, (int)val$offset));
                }
                catch (BadLocationException ex) {
                    Logger.getLogger(ComputeAnnotations.class.getName()).log(Level.FINE, null, ex);
                }
            }
        }
        Impl i = new Impl(offset, doc);
        doc.render(i);
        return i.pos;
    }

    public static final class FactoryImpl
    extends AbstractTaskFactory {
        public FactoryImpl() {
            super(true);
        }

        @Override
        protected Collection<? extends SchedulerTask> createTasks(Language language, Snapshot snapshot) {
            return Collections.singleton(new ComputeAnnotations());
        }
    }

}

