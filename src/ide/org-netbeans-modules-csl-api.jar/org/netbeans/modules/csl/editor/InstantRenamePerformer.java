/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.filesystems.FileObject
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.csl.editor;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.CaretEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.editor.SyncDocumentRegion;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.filesystems.FileObject;
import org.openide.text.NbDocument;

public class InstantRenamePerformer
implements DocumentListener,
KeyListener {
    private SyncDocumentRegion region;
    private Document doc;
    private JTextComponent target;
    private AttributeSet attribs = null;
    private AttributeSet attribsLeft = null;
    private AttributeSet attribsRight = null;
    private AttributeSet attribsMiddle = null;
    private AttributeSet attribsAll = null;
    private AttributeSet attribsSlave = null;
    private AttributeSet attribsSlaveLeft = null;
    private AttributeSet attribsSlaveRight = null;
    private AttributeSet attribsSlaveMiddle = null;
    private AttributeSet attribsSlaveAll = null;
    private boolean inSync;
    private static final AttributeSet defaultSyncedTextBlocksHighlight = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Foreground, Color.red});

    private InstantRenamePerformer(JTextComponent target, Set<OffsetRange> highlights, int caretOffset) throws BadLocationException {
        this.target = target;
        this.doc = target.getDocument();
        MutablePositionRegion mainRegion = null;
        ArrayList<MutablePositionRegion> regions = new ArrayList<MutablePositionRegion>();
        for (OffsetRange h : highlights) {
            Position end;
            Position start = NbDocument.createPosition((Document)this.doc, (int)h.getStart(), (Position.Bias)Position.Bias.Backward);
            MutablePositionRegion current = new MutablePositionRegion(start, end = NbDocument.createPosition((Document)this.doc, (int)h.getEnd(), (Position.Bias)Position.Bias.Forward));
            if (this.isIn(current, caretOffset)) {
                mainRegion = current;
                continue;
            }
            regions.add(current);
        }
        if (mainRegion == null) {
            Logger.getLogger(InstantRenamePerformer.class.getName()).warning("No highlight contains the caret (" + caretOffset + "; highlights=" + highlights + ")");
            if (regions.size() > 0) {
                mainRegion = (MutablePositionRegion)regions.get(0);
                int mainDistance = Integer.MAX_VALUE;
                for (MutablePositionRegion r : regions) {
                    int distance = caretOffset < r.getStartOffset() ? r.getStartOffset() - caretOffset : caretOffset - r.getEndOffset();
                    if (distance >= mainDistance) continue;
                    mainRegion = r;
                    mainDistance = distance;
                }
            } else {
                return;
            }
        }
        regions.add(0, mainRegion);
        this.region = new SyncDocumentRegion(this.doc, regions);
        if (this.doc instanceof BaseDocument) {
            ((BaseDocument)this.doc).addPostModificationDocumentListener((DocumentListener)this);
        }
        target.addKeyListener(this);
        target.putClientProperty("NetBeansEditor.navigateBoundaries", (Object)mainRegion);
        target.putClientProperty(InstantRenamePerformer.class, this);
        this.requestRepaint();
        target.select(mainRegion.getStartOffset(), mainRegion.getEndOffset());
    }

    private static InstantRenamePerformer getPerformerFromComponent(JTextComponent target) {
        return (InstantRenamePerformer)target.getClientProperty(InstantRenamePerformer.class);
    }

    public static void performInstantRename(JTextComponent target, Set<OffsetRange> highlights, int caretOffset) throws BadLocationException {
        InstantRenamePerformer performer = InstantRenamePerformer.getPerformerFromComponent(target);
        if (performer != null) {
            performer.release();
        }
        new InstantRenamePerformer(target, highlights, caretOffset);
    }

    private boolean isIn(MutablePositionRegion region, int caretOffset) {
        return region.getStartOffset() <= caretOffset && caretOffset <= region.getEndOffset();
    }

    @Override
    public synchronized void insertUpdate(DocumentEvent e) {
        if (this.inSync) {
            return;
        }
        this.inSync = true;
        this.region.sync(0);
        this.inSync = false;
        this.requestRepaint();
    }

    @Override
    public synchronized void removeUpdate(DocumentEvent e) {
        if (this.inSync) {
            return;
        }
        if (this.doc.getProperty("doc-replace-selection-property") != null) {
            return;
        }
        this.inSync = true;
        this.region.sync(0);
        this.inSync = false;
        this.requestRepaint();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    public void caretUpdate(CaretEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 27 && e.getModifiers() == 0 || e.getKeyCode() == 10 && e.getModifiers() == 0) {
            this.release();
            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    void release() {
        this.target.putClientProperty("NetBeansEditor.navigateBoundaries", null);
        this.target.putClientProperty(InstantRenamePerformer.class, null);
        if (this.doc instanceof BaseDocument) {
            ((BaseDocument)this.doc).removePostModificationDocumentListener((DocumentListener)this);
        }
        this.target.removeKeyListener(this);
        this.target = null;
        this.region = null;
        this.requestRepaint();
        this.doc = null;
    }

    private void requestRepaint() {
        if (this.region == null) {
            OffsetsBag bag = InstantRenamePerformer.getHighlightsBag(this.doc);
            bag.clear();
        } else {
            if (this.attribs == null) {
                this.attribs = InstantRenamePerformer.getSyncedTextBlocksHighlight("synchronized-text-blocks-ext");
                Color foreground = (Color)this.attribs.getAttribute(StyleConstants.Foreground);
                Color background = (Color)this.attribs.getAttribute(StyleConstants.Background);
                this.attribsLeft = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.LeftBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsRight = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.RightBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsMiddle = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsAll = InstantRenamePerformer.createAttribs(StyleConstants.Background, background, EditorStyleConstants.LeftBorderLineColor, foreground, EditorStyleConstants.RightBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                this.attribsSlave = InstantRenamePerformer.getSyncedTextBlocksHighlight("synchronized-text-blocks-ext-slave");
                Color slaveForeground = (Color)this.attribsSlave.getAttribute(StyleConstants.Foreground);
                Color slaveBackground = (Color)this.attribsSlave.getAttribute(StyleConstants.Background);
                this.attribsSlaveLeft = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.LeftBorderLineColor, slaveForeground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
                this.attribsSlaveRight = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.RightBorderLineColor, slaveForeground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
                this.attribsSlaveMiddle = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
                this.attribsSlaveAll = InstantRenamePerformer.createAttribs(StyleConstants.Background, slaveBackground, EditorStyleConstants.LeftBorderLineColor, slaveForeground, EditorStyleConstants.RightBorderLineColor, slaveForeground, EditorStyleConstants.TopBorderLineColor, slaveForeground, EditorStyleConstants.BottomBorderLineColor, slaveForeground);
            }
            OffsetsBag nue = new OffsetsBag(this.doc);
            for (int i = 0; i < this.region.getRegionCount(); ++i) {
                int startOffset = this.region.getRegion(i).getStartOffset();
                int endOffset = this.region.getRegion(i).getEndOffset();
                int size = this.region.getRegion(i).getLength();
                if (size == 1) {
                    nue.addHighlight(startOffset, endOffset, i == 0 ? this.attribsAll : this.attribsSlaveAll);
                    continue;
                }
                if (size <= 1) continue;
                nue.addHighlight(startOffset, startOffset + 1, i == 0 ? this.attribsLeft : this.attribsSlaveLeft);
                nue.addHighlight(endOffset - 1, endOffset, i == 0 ? this.attribsRight : this.attribsSlaveRight);
                if (size <= 2) continue;
                nue.addHighlight(startOffset + 1, endOffset - 1, i == 0 ? this.attribsMiddle : this.attribsSlaveMiddle);
            }
            OffsetsBag bag = InstantRenamePerformer.getHighlightsBag(this.doc);
            bag.setHighlights(nue);
        }
    }

    private static AttributeSet getSyncedTextBlocksHighlight(String name) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FontColorSettings.class);
        AttributeSet as = fcs != null ? fcs.getFontColors(name) : null;
        return as == null ? defaultSyncedTextBlocksHighlight : as;
    }

    private static /* varargs */ AttributeSet createAttribs(Object ... keyValuePairs) {
        assert (keyValuePairs.length % 2 == 0);
        ArrayList<Object> list = new ArrayList<Object>();
        for (int i = keyValuePairs.length / 2 - 1; i >= 0; --i) {
            Object attrKey = keyValuePairs[2 * i];
            Object attrValue = keyValuePairs[2 * i + 1];
            if (attrKey == null || attrValue == null) continue;
            list.add(attrKey);
            list.add(attrValue);
        }
        return AttributesUtilities.createImmutable((Object[])list.toArray());
    }

    public static OffsetsBag getHighlightsBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(InstantRenamePerformer.class);
        if (bag == null) {
            bag = new OffsetsBag(doc);
            doc.putProperty(InstantRenamePerformer.class, (Object)bag);
            FileObject stream = DataLoadersBridge.getDefault().getFileObject(doc);
            if (stream instanceof FileObject) {
                Logger.getLogger("TIMER").log(Level.FINE, "Instant Rename Highlights Bag", new Object[]{stream, bag});
            }
        }
        return bag;
    }
}

