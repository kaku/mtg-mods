/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.csl.editor.semantic;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.openide.text.NbDocument;

public final class HighlightImpl {
    private Document doc;
    private int start;
    private int end;
    private Collection<ColoringAttributes> colorings;
    Collection<ColoringAttributes> coloringsAttributesOrder = Arrays.asList(new ColoringAttributes[]{ColoringAttributes.ABSTRACT, ColoringAttributes.ANNOTATION_TYPE, ColoringAttributes.CLASS, ColoringAttributes.CONSTRUCTOR, ColoringAttributes.CUSTOM1, ColoringAttributes.CUSTOM2, ColoringAttributes.CUSTOM3, ColoringAttributes.DECLARATION, ColoringAttributes.DEPRECATED, ColoringAttributes.ENUM, ColoringAttributes.FIELD, ColoringAttributes.GLOBAL, ColoringAttributes.INTERFACE, ColoringAttributes.LOCAL_VARIABLE, ColoringAttributes.MARK_OCCURRENCES, ColoringAttributes.METHOD, ColoringAttributes.PACKAGE_PRIVATE, ColoringAttributes.PARAMETER, ColoringAttributes.PRIVATE, ColoringAttributes.PROTECTED, ColoringAttributes.PUBLIC, ColoringAttributes.REGEXP, ColoringAttributes.STATIC, ColoringAttributes.TYPE_PARAMETER_DECLARATION, ColoringAttributes.TYPE_PARAMETER_USE, ColoringAttributes.UNDEFINED, ColoringAttributes.UNUSED});

    public HighlightImpl(Document doc, Token token, Collection<ColoringAttributes> colorings) {
        this.doc = doc;
        this.start = token.offset(null);
        this.end = token.offset(null) + token.text().length();
        this.colorings = colorings;
    }

    public HighlightImpl(Document doc, int start, int end, Collection<ColoringAttributes> colorings) {
        this.doc = doc;
        this.start = start;
        this.end = end;
        this.colorings = colorings;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public String getHighlightTestData() {
        int lineStart = NbDocument.findLineNumber((StyledDocument)((StyledDocument)this.doc), (int)this.start);
        int columnStart = NbDocument.findLineColumn((StyledDocument)((StyledDocument)this.doc), (int)this.start);
        int lineEnd = NbDocument.findLineNumber((StyledDocument)((StyledDocument)this.doc), (int)this.end);
        int columnEnd = NbDocument.findLineColumn((StyledDocument)((StyledDocument)this.doc), (int)this.end);
        return this.coloringsToString() + ", " + lineStart + ":" + columnStart + "-" + lineEnd + ":" + columnEnd;
    }

    private String coloringsToString() {
        StringBuffer result = new StringBuffer();
        boolean first = true;
        result.append("[");
        for (ColoringAttributes attribute : this.coloringsAttributesOrder) {
            if (!this.colorings.contains((Object)attribute)) continue;
            if (!first) {
                result.append(", ");
            }
            first = false;
            result.append(attribute.name());
        }
        result.append("]");
        return result.toString();
    }

    public static HighlightImpl parse(StyledDocument doc, String line) throws ParseException, BadLocationException {
        MessageFormat f = new MessageFormat("[{0}], {1,number,integer}:{2,number,integer}-{3,number,integer}:{4,number,integer}");
        Object[] args = f.parse(line);
        String attributesString = (String)args[0];
        int lineStart = ((Long)args[1]).intValue();
        int columnStart = ((Long)args[2]).intValue();
        int lineEnd = ((Long)args[3]).intValue();
        int columnEnd = ((Long)args[4]).intValue();
        String[] attrElements = attributesString.split(",");
        ArrayList<ColoringAttributes> attributes = new ArrayList<ColoringAttributes>();
        for (String a : attrElements) {
            a = a.trim();
            attributes.add(ColoringAttributes.valueOf(a));
        }
        if (attributes.contains(null)) {
            throw new NullPointerException();
        }
        int offsetStart = NbDocument.findLineOffset((StyledDocument)doc, (int)lineStart) + columnStart;
        int offsetEnd = NbDocument.findLineOffset((StyledDocument)doc, (int)lineEnd) + columnEnd;
        return new HighlightImpl(doc, offsetStart, offsetEnd, attributes);
    }
}

