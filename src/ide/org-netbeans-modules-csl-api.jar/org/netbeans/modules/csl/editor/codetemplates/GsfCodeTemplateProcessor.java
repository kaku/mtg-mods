/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessor
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessorFactory
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.csl.editor.codetemplates;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessor;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessorFactory;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.editor.codetemplates.GsfCodeTemplateFilter;
import org.netbeans.modules.csl.editor.completion.GsfCompletionProvider;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class GsfCodeTemplateProcessor
implements CodeTemplateProcessor {
    private CodeTemplateInsertRequest request;
    private volatile ParserResult cInfo = null;
    private volatile Snapshot snapshot = null;

    private GsfCodeTemplateProcessor(CodeTemplateInsertRequest request) {
        this.request = request;
    }

    public synchronized void updateDefaultValues() {
        boolean cont = true;
        while (cont) {
            cont = false;
            for (Object p : this.request.getMasterParameters()) {
                CodeTemplateParameter param = (CodeTemplateParameter)p;
                String value = this.getProposedValue(param);
                if (value == null || value.equals(param.getValue())) continue;
                param.setValue(value);
                cont = true;
            }
        }
    }

    public void parameterValueChanged(CodeTemplateParameter masterParameter, boolean typingChange) {
        if (typingChange) {
            for (Object p : this.request.getMasterParameters()) {
                String value;
                CodeTemplateParameter param = (CodeTemplateParameter)p;
                if (param.isUserModified() || (value = this.getProposedValue(param)) == null || value.equals(param.getValue())) continue;
                param.setValue(value);
            }
        }
    }

    public void release() {
    }

    private String getProposedValue(CodeTemplateParameter param) {
        String variable = param.getName();
        JTextComponent c = this.request.getComponent();
        int caretOffset = c.getCaret().getDot();
        String resolved = this.delegatedResolve(caretOffset, param.getName(), variable, param.getHints());
        return resolved;
    }

    private String delegatedResolve(int caretOffset, String name, String variable, Map params) {
        try {
            if (this.initParsing()) {
                CodeCompletionHandler completer = GsfCompletionProvider.getCompletable(this.snapshot.getSource().getDocument(true), caretOffset);
                if (completer == null) {
                    return null;
                }
                return completer.resolveTemplateVariable(variable, this.cInfo, caretOffset, name, params);
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    private boolean initParsing() {
        if (this.cInfo == null) {
            BaseDocument doc;
            final JTextComponent c = this.request.getComponent();
            Source js = Source.create((Document)c.getDocument());
            if (c.getDocument() instanceof BaseDocument && (doc = (BaseDocument)c.getDocument()).isAtomicLock()) {
                return false;
            }
            if (js != null) {
                try {
                    final AtomicBoolean done = new AtomicBoolean();
                    final Thread me = Thread.currentThread();
                    ParserManager.parseWhenScanFinished(Collections.singleton(js), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws IOException, ParseException {
                            if (!Thread.currentThread().equals(me)) {
                                return;
                            }
                            Parser.Result parserResult = resultIterator.getParserResult(c.getCaretPosition());
                            if (!(parserResult instanceof ParserResult)) {
                                return;
                            }
                            GsfCodeTemplateProcessor.this.cInfo = (ParserResult)parserResult;
                            GsfCodeTemplateProcessor.this.snapshot = parserResult.getSnapshot();
                            done.set(true);
                        }
                    });
                    if (!done.get()) {
                        StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(GsfCodeTemplateFilter.class, (String)"JCT-scanning-in-progress"));
                    }
                }
                catch (ParseException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        }
        return this.cInfo != null;
    }

    public static final class Factory
    implements CodeTemplateProcessorFactory {
        public CodeTemplateProcessor createProcessor(CodeTemplateInsertRequest request) {
            return new GsfCodeTemplateProcessor(request);
        }
    }

}

