/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.csl.editor.semantic;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.editor.semantic.ColoringManager;
import org.netbeans.modules.csl.editor.semantic.SequenceElement;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public class GsfSemanticLayer
extends AbstractHighlightsContainer
implements DocumentListener {
    private static final Logger LOG = Logger.getLogger(GsfSemanticLayer.class.getName());
    private SortedSet<SequenceElement> colorings;
    private int version;
    private List<Edit> edits;
    private Map<Language, Map<ColoringAttributes.Coloring, AttributeSet>> CACHE = new HashMap<Language, Map<ColoringAttributes.Coloring, AttributeSet>>();
    private Document doc;
    private List<Lookup.Result> coloringResults = new ArrayList<Lookup.Result>(3);
    private List<LookupListener> coloringListeners = new ArrayList<LookupListener>(3);
    private static final SortedSet<SequenceElement> EMPTY_TREE_SET = new TreeSet<SequenceElement>();

    public static GsfSemanticLayer getLayer(Class id, Document doc) {
        GsfSemanticLayer l = (GsfSemanticLayer)doc.getProperty(id);
        if (l == null) {
            l = new GsfSemanticLayer(doc);
            doc.putProperty(id, l);
        }
        return l;
    }

    private GsfSemanticLayer(Document doc) {
        this.doc = doc;
        this.colorings = EMPTY_TREE_SET;
        this.version = -1;
    }

    void setColorings(final SortedSet<SequenceElement> colorings, final int version) {
        this.doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                GsfSemanticLayer gsfSemanticLayer = GsfSemanticLayer.this;
                synchronized (gsfSemanticLayer) {
                    GsfSemanticLayer.this.colorings = colorings;
                    GsfSemanticLayer.this.edits = new ArrayList();
                    GsfSemanticLayer.this.version = version;
                    GsfSemanticLayer.this.fireHighlightsChange(0, GsfSemanticLayer.this.doc.getLength());
                    DocumentUtilities.removeDocumentListener((Document)GsfSemanticLayer.this.doc, (DocumentListener)GsfSemanticLayer.this, (DocumentListenerPriority)DocumentListenerPriority.LEXER);
                    DocumentUtilities.addDocumentListener((Document)GsfSemanticLayer.this.doc, (DocumentListener)GsfSemanticLayer.this, (DocumentListenerPriority)DocumentListenerPriority.LEXER);
                }
            }
        });
    }

    synchronized SortedSet<SequenceElement> getColorings() {
        return this.colorings;
    }

    synchronized int getVersion() {
        return this.version;
    }

    public synchronized HighlightsSequence getHighlights(int startOffset, int endOffset) {
        if (this.colorings.isEmpty()) {
            return HighlightsSequence.EMPTY;
        }
        return new GsfHighlightSequence(this, this.doc, startOffset, endOffset, this.colorings);
    }

    public synchronized void clearColoringCache() {
        this.CACHE.clear();
    }

    private synchronized void clearLanguageColoring(Language mime) {
        this.CACHE.remove(mime);
    }

    synchronized AttributeSet getColoring(ColoringAttributes.Coloring c, final Language language) {
        AttributeSet a = null;
        Map<ColoringAttributes.Coloring, AttributeSet> map = this.CACHE.get(language);
        if (map == null) {
            String mime = language.getMimeType();
            a = language.getColoringManager().getColoringImpl(c);
            map = new HashMap<ColoringAttributes.Coloring, AttributeSet>();
            map.put(c, a);
            this.CACHE.put(language, map);
            Lookup.Result res = MimeLookup.getLookup((MimePath)MimePath.get((String)mime)).lookupResult(FontColorSettings.class);
            this.coloringResults.add(res);
            LookupListener l = new LookupListener(){

                public void resultChanged(LookupEvent ev) {
                    GsfSemanticLayer.this.clearLanguageColoring(language);
                    GsfSemanticLayer.this.fireHighlightsChange(0, GsfSemanticLayer.this.doc.getLength());
                }
            };
            res.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)l, (Object)res));
            this.coloringListeners.add(()l);
        } else {
            a = map.get(c);
            if (a == null) {
                a = language.getColoringManager().getColoringImpl(c);
                map.put(c, a);
            }
        }
        if (a == null) {
            LOG.log(Level.FINE, "Null AttributeSet for coloring {0} in language {1}", new Object[]{c, language});
        }
        return a;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        GsfSemanticLayer gsfSemanticLayer = this;
        synchronized (gsfSemanticLayer) {
            this.edits.add(new Edit(e.getOffset(), e.getLength(), true));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeUpdate(DocumentEvent e) {
        GsfSemanticLayer gsfSemanticLayer = this;
        synchronized (gsfSemanticLayer) {
            this.edits.add(new Edit(e.getOffset(), e.getLength(), false));
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    public int getShiftedPos(int pos) {
        List<Edit> list = this.edits;
        int len = list.size();
        if (len == 0) {
            return pos;
        }
        for (int i = 0; i < len; ++i) {
            Edit edit = list.get(i);
            if (pos <= edit.offset) continue;
            if (edit.insert) {
                pos += edit.len;
                continue;
            }
            if (pos < edit.offset + edit.len) {
                pos = edit.offset;
                continue;
            }
            pos -= edit.len;
        }
        if (pos < 0) {
            pos = 0;
        }
        return pos;
    }

    private static final class GsfHighlightSequence
    implements HighlightsSequence {
        private Iterator<SequenceElement> iterator;
        private SequenceElement element;
        private final GsfSemanticLayer layer;
        private final int endOffset;
        private SequenceElement nextElement;
        private int nextElementStartOffset = Integer.MAX_VALUE;

        GsfHighlightSequence(GsfSemanticLayer layer, Document doc, int startOffset, int endOffset, SortedSet<SequenceElement> colorings) {
            this.layer = layer;
            this.endOffset = endOffset;
            SequenceElement.ComparisonItem fromInclusive = new SequenceElement.ComparisonItem(startOffset);
            SortedSet<SequenceElement.ComparisonItem> subMap = colorings.tailSet(fromInclusive);
            this.iterator = subMap.iterator();
        }

        private SequenceElement fetchElementFromIterator(boolean updateNextElementStartOffset) {
            SequenceElement se;
            int seStartOffset;
            if (this.iterator != null && this.iterator.hasNext()) {
                se = this.iterator.next();
                seStartOffset = se.range.getStart();
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Fetched highlight <{0},{1}>\n", new Object[]{seStartOffset, se.range.getEnd()});
                }
                if (seStartOffset >= this.endOffset) {
                    se = null;
                    seStartOffset = Integer.MAX_VALUE;
                    this.iterator = null;
                }
            } else {
                se = null;
                seStartOffset = Integer.MAX_VALUE;
                this.iterator = null;
            }
            if (updateNextElementStartOffset) {
                this.nextElementStartOffset = seStartOffset;
            }
            return se;
        }

        public boolean moveNext() {
            if (this.nextElement != null) {
                this.element = this.nextElement;
                this.nextElement = this.fetchElementFromIterator(true);
            } else {
                this.element = this.fetchElementFromIterator(false);
                if (this.element != null) {
                    this.nextElement = this.fetchElementFromIterator(true);
                }
            }
            return this.element != null;
        }

        public int getStartOffset() {
            return this.element != null ? this.layer.getShiftedPos(this.element.range.getStart()) : Integer.MAX_VALUE;
        }

        public int getEndOffset() {
            return this.element != null ? Math.min(this.layer.getShiftedPos(this.element.range.getEnd()), this.nextElementStartOffset) : Integer.MAX_VALUE;
        }

        public AttributeSet getAttributes() {
            return this.element != null ? this.layer.getColoring(this.element.coloring, this.element.language) : SimpleAttributeSet.EMPTY;
        }
    }

    private class Edit {
        int offset;
        int len;
        boolean insert;

        public Edit(int offset, int len, boolean insert) {
            this.offset = offset;
            this.len = len;
            this.insert = insert;
        }
    }

}

