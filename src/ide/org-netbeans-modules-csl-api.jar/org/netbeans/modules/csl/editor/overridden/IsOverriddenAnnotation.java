/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.Annotation
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.csl.editor.overridden;

import java.util.Collection;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.csl.editor.overridden.AnnotationType;
import org.netbeans.modules.csl.editor.overridden.OverrideDescription;
import org.openide.text.Annotation;
import org.openide.text.NbDocument;

public class IsOverriddenAnnotation
extends Annotation {
    private final StyledDocument document;
    private final Position pos;
    private final String shortDescription;
    private final AnnotationType type;
    private final Collection<? extends OverrideDescription> declarations;

    public IsOverriddenAnnotation(StyledDocument document, Position pos, AnnotationType type, String shortDescription, Collection<? extends OverrideDescription> declarations) {
        assert (pos != null);
        this.document = document;
        this.pos = pos;
        this.type = type;
        this.shortDescription = shortDescription;
        this.declarations = declarations;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public String getAnnotationType() {
        switch (this.type) {
            case IS_OVERRIDDEN: {
                return "org-netbeans-modules-editor-annotations-is_overridden";
            }
            case HAS_IMPLEMENTATION: {
                return "org-netbeans-modules-editor-annotations-has_implementations";
            }
            case IMPLEMENTS: {
                return "org-netbeans-modules-editor-annotations-implements";
            }
            case OVERRIDES: {
                return "org-netbeans-modules-editor-annotations-overrides";
            }
        }
        throw new IllegalStateException("Currently not implemented: " + (Object)((Object)this.type));
    }

    public void attach() {
        NbDocument.addAnnotation((StyledDocument)this.document, (Position)this.pos, (int)-1, (Annotation)this);
    }

    public void detachImpl() {
        NbDocument.removeAnnotation((StyledDocument)this.document, (Annotation)this);
    }

    public String toString() {
        return "[IsOverriddenAnnotation: " + this.shortDescription + "]";
    }

    public Position getPosition() {
        return this.pos;
    }

    public AnnotationType getType() {
        return this.type;
    }

    public Collection<? extends OverrideDescription> getDeclarations() {
        return this.declarations;
    }

}

