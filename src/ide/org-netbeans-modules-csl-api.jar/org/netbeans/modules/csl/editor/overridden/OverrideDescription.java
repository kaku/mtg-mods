/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.csl.editor.overridden;

import java.awt.Image;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.navigation.Icons;
import org.openide.util.ImageUtilities;

public class OverrideDescription {
    @NonNull
    public final DeclarationFinder.AlternativeLocation location;
    private final boolean overriddenFlag;

    public OverrideDescription(@NonNull DeclarationFinder.AlternativeLocation location, boolean overriddenFlag) {
        this.location = location;
        this.overriddenFlag = overriddenFlag;
    }

    public Icon getIcon() {
        Image badge = this.overriddenFlag ? ImageUtilities.loadImage((String)"org/netbeans/modules/csl/navigation/resources/is-overridden-badge.png") : ImageUtilities.loadImage((String)"org/netbeans/modules/csl/navigation/resources/overrides-badge.png");
        ImageIcon icon = Icons.getElementIcon(this.location.getElement().getKind(), this.location.getElement().getModifiers());
        return ImageUtilities.image2Icon((Image)ImageUtilities.mergeImages((Image)ImageUtilities.icon2Image((Icon)icon), (Image)badge, (int)16, (int)0));
    }

    public boolean isOverridden() {
        return this.overriddenFlag;
    }
}

