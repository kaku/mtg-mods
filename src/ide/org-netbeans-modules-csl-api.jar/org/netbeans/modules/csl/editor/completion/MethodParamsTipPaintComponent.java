/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.csl.editor.completion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;
import javax.swing.JToolTip;
import javax.swing.text.JTextComponent;

public class MethodParamsTipPaintComponent
extends JToolTip {
    private int drawX;
    private int drawY;
    private int drawHeight;
    private int drawWidth;
    private Font drawFont;
    private int fontHeight;
    private int descent;
    private FontMetrics fontMetrics;
    private List<List<String>> params;
    private int idx;
    private JTextComponent component;

    public MethodParamsTipPaintComponent(List<List<String>> params, int idx, JTextComponent component) {
        this.params = params;
        this.idx = idx;
        this.component = component;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        Map renderingHints;
        Map value = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
        Map map = renderingHints = value instanceof Map ? value : null;
        if (renderingHints != null && g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D)g;
            RenderingHints oldHints = g2d.getRenderingHints();
            g2d.setRenderingHints(renderingHints);
            try {
                super.paint(g2d);
            }
            finally {
                g2d.setRenderingHints(oldHints);
            }
        }
        super.paint(g);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(this.getBackground());
        Rectangle r = g.getClipBounds();
        g.fillRect(r.x, r.y, r.width, r.height);
        g.setColor(this.getForeground());
        this.draw(g);
    }

    protected void draw(Graphics g) {
        int screenWidth;
        Insets in = this.getInsets();
        GraphicsConfiguration gc = this.component.getGraphicsConfiguration();
        int n = screenWidth = gc != null ? gc.getBounds().width : Integer.MAX_VALUE;
        if (in != null) {
            this.drawX = in.left;
            this.drawY = in.top;
        } else {
            this.drawX = 0;
            this.drawY = 0;
        }
        this.drawY += this.fontHeight - this.descent;
        int startX = this.drawX;
        this.drawWidth = this.drawX;
        int i = 0;
        for (List<String> p : this.params) {
            for (String s : p) {
                if (this.getWidth(s, i == this.idx ? this.getDrawFont().deriveFont(1) : this.getDrawFont()) + this.drawX > screenWidth) {
                    this.drawY += this.fontHeight;
                    this.drawX = startX + this.getWidth("        ", this.drawFont);
                }
                this.drawString(g, s, i == this.idx ? this.getDrawFont().deriveFont(1) : this.getDrawFont());
                if (this.drawWidth < this.drawX) {
                    this.drawWidth = this.drawX;
                }
                ++i;
            }
            this.drawY += this.fontHeight;
            this.drawX = startX;
        }
        this.drawHeight = this.drawY - this.fontHeight + this.descent;
        if (in != null) {
            this.drawHeight += in.bottom;
            this.drawWidth += in.right;
        }
    }

    protected void drawString(Graphics g, String s, Font font) {
        if (g != null) {
            g.setFont(font);
            g.drawString(s, this.drawX, this.drawY);
            g.setFont(this.drawFont);
        }
        this.drawX += this.getWidth(s, font);
    }

    protected int getWidth(String s, Font font) {
        if (font == null) {
            return this.fontMetrics.stringWidth(s);
        }
        return this.getFontMetrics(font).stringWidth(s);
    }

    protected int getHeight(String s, Font font) {
        if (font == null) {
            return this.fontMetrics.stringWidth(s);
        }
        return this.getFontMetrics(font).stringWidth(s);
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        this.fontMetrics = this.getFontMetrics(font);
        this.fontHeight = this.fontMetrics.getHeight();
        this.descent = this.fontMetrics.getDescent();
        this.drawFont = font;
    }

    protected Font getDrawFont() {
        return this.drawFont;
    }

    @Override
    public Dimension getPreferredSize() {
        this.draw(null);
        Insets i = this.getInsets();
        if (i != null) {
            this.drawX += i.right;
        }
        return new Dimension(this.drawWidth, this.drawHeight);
    }
}

