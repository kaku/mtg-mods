/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.TaskIndexingMode
 *  org.netbeans.spi.editor.fold.FoldHierarchyTransaction
 *  org.netbeans.spi.editor.fold.FoldInfo
 *  org.netbeans.spi.editor.fold.FoldManager
 *  org.netbeans.spi.editor.fold.FoldOperation
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.csl.editor.fold;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.fold.Bundle;
import org.netbeans.modules.csl.editor.fold.GsfFoldScheduler;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldInfo;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.filesystems.FileObject;

public class GsfFoldManager
implements FoldManager {
    static final Logger LOG = Logger.getLogger(GsfFoldManager.class.getName());
    private static final FoldTemplate TEMPLATE_CODEBLOCK = new FoldTemplate(1, 1, "{...}");
    @Deprecated
    public static final FoldType CODE_BLOCK_FOLD_TYPE = FoldType.CODE_BLOCK;
    @Deprecated
    public static final FoldType INITIAL_COMMENT_FOLD_TYPE = FoldType.INITIAL_COMMENT;
    @Deprecated
    public static final FoldType IMPORTS_FOLD_TYPE = FoldType.IMPORT;
    @Deprecated
    public static final FoldType JAVADOC_FOLD_TYPE = FoldType.DOCUMENTATION;
    @Deprecated
    public static final FoldType TAG_FOLD_TYPE = FoldType.TAG;
    @Deprecated
    public static final FoldType INNER_CLASS_FOLD_TYPE = FoldType.create((String)"innerclass", (String)Bundle.FT_label_innerclass(), (FoldTemplate)TEMPLATE_CODEBLOCK);
    @Deprecated
    public static final FoldType OTHER_CODEBLOCKS_FOLD_TYPE = FoldType.TAG.derive("othercodeblocks", Bundle.FT_label_othercodeblocks(), TEMPLATE_CODEBLOCK);
    private static final Set<String> LEGACY_FOLD_TAGS = new HashSet<String>(11);
    private FoldOperation operation;
    private FileObject file;
    private volatile JavaElementFoldTask task;
    private volatile Preferences prefs;
    private Fold initialCommentFold;
    private Fold importsFold;

    public void init(FoldOperation operation) {
        this.operation = operation;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Created FM: {0}\n\t\t, doc: {1}\n\t\t, comp: {2}", new Object[]{this, operation.getHierarchy().getComponent().getDocument(), Integer.toHexString(System.identityHashCode(operation.getHierarchy().getComponent()))});
        }
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)operation.getHierarchy().getComponent());
        if (this.prefs == null) {
            this.prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        }
    }

    public synchronized void initFolds(FoldHierarchyTransaction transaction) {
        Document doc = this.operation.getHierarchy().getComponent().getDocument();
        this.file = DataLoadersBridge.getDefault().getFileObject(doc);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Initializing, document {0}\n\t\t, file {1}\n\t\t, component {2}\n\t\t, FM {3}", new Object[]{doc, this.file, Integer.toHexString(System.identityHashCode(this.operation.getHierarchy().getComponent())), this});
        }
        if (this.file != null) {
            this.task = JavaElementFoldTask.getTask(this.file);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "registering FM to task: {0}, {1}", new Object[]{this, this.task});
            }
            this.task.setGsfFoldManager(this, this.file);
        }
    }

    public void insertUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void removeUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void changedUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void removeEmptyNotify(Fold emptyFold) {
        this.removeDamagedNotify(emptyFold);
    }

    public void removeDamagedNotify(Fold damagedFold) {
        if (this.importsFold == damagedFold) {
            this.importsFold = null;
        }
        if (this.initialCommentFold == damagedFold) {
            this.initialCommentFold = null;
        }
    }

    public void expandNotify(Fold expandedFold) {
    }

    public synchronized void release() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Releasing FM {0}, task {1}", new Object[]{this, this.task});
        }
        if (this.task != null) {
            this.task.setGsfFoldManager(this, null);
        }
        this.task = null;
        this.file = null;
        this.importsFold = null;
        this.initialCommentFold = null;
    }

    private static boolean hasErrors(ParserResult r) {
        for (Error e : r.getDiagnostics()) {
            if (e.getSeverity() != Severity.FATAL) continue;
            return true;
        }
        return false;
    }

    static {
        LEGACY_FOLD_TAGS.add(OTHER_CODEBLOCKS_FOLD_TYPE.code());
        LEGACY_FOLD_TAGS.add(INNER_CLASS_FOLD_TYPE.code());
        LEGACY_FOLD_TAGS.add(TAG_FOLD_TYPE.code());
        LEGACY_FOLD_TAGS.add(JAVADOC_FOLD_TYPE.code());
        LEGACY_FOLD_TAGS.add(IMPORTS_FOLD_TYPE.code());
        LEGACY_FOLD_TAGS.add(INITIAL_COMMENT_FOLD_TYPE.code());
        LEGACY_FOLD_TAGS.add(CODE_BLOCK_FOLD_TYPE.code());
    }

    private class CommitFolds
    implements Runnable {
        private final Document scannedDocument;
        private Source scanSource;
        private boolean insideRender;
        private Collection<FoldInfo> infos;
        private long startTime;
        private FoldInfo initComment;
        private FoldInfo imports;
        private final AtomicBoolean cancel;

        public CommitFolds(Collection<FoldInfo> infos, FoldInfo initComment, FoldInfo imports, Document scannedDocument, Source s, AtomicBoolean cancel) {
            this.infos = infos;
            this.initComment = initComment;
            this.imports = imports;
            this.scannedDocument = scannedDocument;
            this.scanSource = s;
            this.cancel = cancel;
        }

        private void mergeSpecialFoldState(FoldInfo fi, Fold f) {
            if (fi != null && f != null) {
                fi.collapsed(f.isCollapsed());
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Document document = GsfFoldManager.this.operation.getHierarchy().getComponent().getDocument();
            if (!this.insideRender) {
                this.startTime = System.currentTimeMillis();
                this.insideRender = true;
                document.render(this);
                return;
            }
            if (this.cancel.get() || GsfFoldManager.this.task == null) {
                return;
            }
            GsfFoldManager.this.operation.getHierarchy().lock();
            try {
                Map newState;
                block14 : {
                    Document newDoc = GsfFoldManager.this.operation.getHierarchy().getComponent().getDocument();
                    if (newDoc != this.scannedDocument) {
                        return;
                    }
                    if (newDoc != document) {
                        GsfFoldManager.LOG.log(Level.WARNING, "Locked different document than the component: currentDoc: {0}, lockedDoc: {1}", new Object[]{newDoc, document});
                    }
                    try {
                        this.mergeSpecialFoldState(this.imports, GsfFoldManager.this.importsFold);
                        this.mergeSpecialFoldState(this.initComment, GsfFoldManager.this.initialCommentFold);
                        newState = GsfFoldManager.this.operation.update(this.infos, null, null);
                        if (newState != null) break block14;
                        return;
                    }
                    catch (BadLocationException e) {
                        GsfFoldManager.LOG.log(Level.WARNING, null, e);
                    }
                }
                if (this.imports != null) {
                    GsfFoldManager.this.importsFold = (Fold)newState.get((Object)this.imports);
                }
                if (this.initComment != null) {
                    GsfFoldManager.this.initialCommentFold = (Fold)newState.get((Object)this.initComment);
                }
            }
            finally {
                GsfFoldManager.this.operation.getHierarchy().unlock();
            }
            long endTime = System.currentTimeMillis();
            Logger.getLogger("TIMER").log(Level.FINE, "Folds - 2", new Object[]{GsfFoldManager.this.file, endTime - this.startTime});
        }
    }

    static final class JavaElementFoldTask
    extends IndexingAwareParserResultTask<ParserResult> {
        private final AtomicBoolean cancelled = new AtomicBoolean(false);
        private FoldInfo initComment;
        private FoldInfo imports;
        private static final Map<FileObject, JavaElementFoldTask> file2Task = new WeakHashMap<FileObject, JavaElementFoldTask>();
        private Collection<Reference<GsfFoldManager>> managers = new ArrayList<Reference<GsfFoldManager>>(2);

        public JavaElementFoldTask() {
            super(TaskIndexingMode.ALLOWED_DURING_SCAN);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        static JavaElementFoldTask getTask(FileObject file) {
            Map<FileObject, JavaElementFoldTask> map = file2Task;
            synchronized (map) {
                JavaElementFoldTask task = file2Task.get((Object)file);
                if (task == null) {
                    task = new JavaElementFoldTask();
                    file2Task.put(file, task);
                }
                if (GsfFoldManager.LOG.isLoggable(Level.FINER)) {
                    GsfFoldManager.LOG.log(Level.FINER, "Task for file {0} -> {1}", new Object[]{file, task});
                }
                return task;
            }
        }

        synchronized void setGsfFoldManager(GsfFoldManager manager, FileObject file) {
            if (file == null) {
                if (GsfFoldManager.LOG.isLoggable(Level.FINE)) {
                    GsfFoldManager.LOG.log(Level.FINE, "Got null file, unregistering {0}, task {1}", new Object[]{manager, this});
                }
                Iterator<Reference<GsfFoldManager>> it = this.managers.iterator();
                while (it.hasNext()) {
                    Reference<GsfFoldManager> ref = it.next();
                    GsfFoldManager fm = ref.get();
                    if (fm != null && fm != manager) continue;
                    it.remove();
                    break;
                }
            } else {
                if (GsfFoldManager.LOG.isLoggable(Level.FINE)) {
                    GsfFoldManager.LOG.log(Level.FINE, "Registering manager {0} for file {1}, task {2} ", new Object[]{manager, file, this});
                }
                this.managers.add(new WeakReference<GsfFoldManager>(manager));
                GsfFoldScheduler.reschedule();
            }
        }

        private synchronized Object findLiveManagers() {
            ArrayList<GsfFoldManager> oneMgr = null;
            ArrayList<GsfFoldManager> result = null;
            Iterator<Reference<GsfFoldManager>> it = this.managers.iterator();
            while (it.hasNext()) {
                Reference<GsfFoldManager> ref = it.next();
                GsfFoldManager fm = ref.get();
                if (fm == null) {
                    it.remove();
                    continue;
                }
                if (result != null) {
                    result.add(fm);
                    continue;
                }
                if (oneMgr != null) {
                    result = new ArrayList<GsfFoldManager>(2);
                    result.add((GsfFoldManager)((Object)oneMgr));
                    result.add(fm);
                    continue;
                }
                oneMgr = fm;
            }
            return result != null ? result : oneMgr;
        }

        public void run(ParserResult info, SchedulerEvent event) {
            Object mgrs;
            this.cancelled.set(false);
            if (GsfFoldManager.LOG.isLoggable(Level.FINER)) {
                GsfFoldManager.LOG.log(Level.FINER, "GSF fold task {0} called for: {1}", new Object[]{this, info.getSnapshot().getSource()});
            }
            if ((mgrs = this.findLiveManagers()) == null) {
                GsfFoldManager.LOG.log(Level.FINE, "No live FoldManagers found for {0}", (Object)this);
                return;
            }
            long startTime = System.currentTimeMillis();
            if (GsfFoldManager.hasErrors(info)) {
                GsfFoldManager.LOG.log(Level.FINE, "File has errors, not updating: {0}", (Object)this);
                return;
            }
            HashSet<FoldInfo> folds = new HashSet<FoldInfo>();
            Document doc = info.getSnapshot().getSource().getDocument(false);
            if (doc == null) {
                GsfFoldManager.LOG.log(Level.FINE, "Could not open document: {0}", (Object)this);
                return;
            }
            boolean success = this.gsfFoldScan(doc, info, folds);
            if (!success || this.cancelled.get()) {
                GsfFoldManager.LOG.log(Level.FINER, "Fold scan cancelled or unsuccessful: {0}, {1}", new Object[]{success, this.cancelled.get()});
                return;
            }
            if (mgrs instanceof GsfFoldManager) {
                GsfFoldManager gsfFoldManager = (GsfFoldManager)mgrs;
                gsfFoldManager.getClass();
                gsfFoldManager.new CommitFolds(folds, this.initComment, this.imports, doc, info.getSnapshot().getSource(), this.cancelled).run();
            } else {
                Collection jefms = (Collection)mgrs;
                Iterator i$ = jefms.iterator();
                while (i$.hasNext()) {
                    GsfFoldManager jefm;
                    GsfFoldManager gsfFoldManager = jefm = (GsfFoldManager)i$.next();
                    gsfFoldManager.getClass();
                    gsfFoldManager.new CommitFolds(folds, this.initComment, this.imports, doc, info.getSnapshot().getSource(), this.cancelled).run();
                }
            }
            long endTime = System.currentTimeMillis();
            Logger.getLogger("TIMER").log(Level.FINE, "Folds - 1", new Object[]{info.getSnapshot().getSource().getFileObject(), endTime - startTime});
        }

        private boolean gsfFoldScan(final Document doc, ParserResult info, final Collection<FoldInfo> folds) {
            final boolean[] success = new boolean[]{false};
            Source source = info.getSnapshot().getSource();
            try {
                ParserManager.parse(Collections.singleton(source), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        String mimeType = resultIterator.getSnapshot().getMimeType();
                        Language language = LanguageRegistry.getInstance().getLanguageByMimeType(mimeType);
                        if (language == null) {
                            return;
                        }
                        StructureScanner scanner = language.getStructure();
                        if (scanner == null) {
                            return;
                        }
                        Parser.Result r = resultIterator.getParserResult();
                        if (!(r instanceof ParserResult)) {
                            return;
                        }
                        JavaElementFoldTask.this.scan((ParserResult)r, folds, doc, scanner);
                        if (JavaElementFoldTask.this.cancelled.get()) {
                            return;
                        }
                        for (Embedding e : resultIterator.getEmbeddings()) {
                            this.run(resultIterator.getResultIterator(e));
                            if (!JavaElementFoldTask.this.cancelled.get()) continue;
                            return;
                        }
                        success[0] = true;
                    }
                });
            }
            catch (ParseException e) {
                GsfFoldManager.LOG.log(Level.WARNING, null, (Throwable)e);
            }
            if (success[0]) {
                success[0] = this.checkInitialFold(doc, folds);
            }
            return success[0];
        }

        private boolean checkInitialFold(final Document doc, final Collection<FoldInfo> folds) {
            boolean[] ret = new boolean[]{true};
            final TokenHierarchy th = TokenHierarchy.get((Document)doc);
            if (th == null) {
                return false;
            }
            doc.render(new Runnable(){

                @Override
                public void run() {
                    TokenSequence ts = th.tokenSequence();
                    if (ts == null) {
                        return;
                    }
                    while (ts.moveNext()) {
                        Token token = ts.token();
                        String category = token.id().primaryCategory();
                        if ("comment".equals(category)) {
                            int startOffset = ts.offset();
                            int endOffset = startOffset + token.length();
                            while (ts.moveNext()) {
                                token = ts.token();
                                category = token.id().primaryCategory();
                                if ("comment".equals(category)) {
                                    endOffset = ts.offset() + token.length();
                                    continue;
                                }
                                if ("whitespace".equals(category)) continue;
                            }
                            try {
                                startOffset = Utilities.getRowEnd((BaseDocument)((BaseDocument)doc), (int)startOffset);
                                if (startOffset >= endOffset) {
                                    return;
                                }
                            }
                            catch (BadLocationException ex) {
                                GsfFoldManager.LOG.log(Level.WARNING, null, ex);
                            }
                            folds.add(JavaElementFoldTask.this.initComment = FoldInfo.range((int)startOffset, (int)endOffset, (FoldType)GsfFoldManager.INITIAL_COMMENT_FOLD_TYPE));
                            return;
                        }
                        if ("whitespace".equals(category)) continue;
                        break;
                    }
                }
            });
            return ret[0];
        }

        private void scan(final ParserResult info, final Collection<FoldInfo> folds, Document doc, StructureScanner scanner) {
            String mime = info.getSnapshot().getMimeType();
            if (!FoldUtilities.isFoldingEnabled((String)mime)) {
                GsfFoldManager.LOG.log(Level.FINER, "Folding is not enabled for MIME: {0}", mime);
                return;
            }
            final Map<String, List<OffsetRange>> collectedFolds = scanner.folds(info);
            final Collection ftypes = FoldUtilities.getFoldTypes((String)mime).values();
            doc.render(new Runnable(){

                @Override
                public void run() {
                    JavaElementFoldTask.this.addTree(folds, info, collectedFolds, ftypes);
                }
            });
        }

        private boolean addFoldsOfType(String type, Map<String, List<OffsetRange>> folds, Collection<FoldInfo> result, FoldType foldType) {
            List<OffsetRange> ranges = folds.get(type);
            if (ranges != null) {
                if (GsfFoldManager.LOG.isLoggable(Level.FINEST)) {
                    GsfFoldManager.LOG.log(Level.FINEST, "Creating folds {0}", new Object[]{type});
                }
                for (OffsetRange range : ranges) {
                    if (GsfFoldManager.LOG.isLoggable(Level.FINEST)) {
                        GsfFoldManager.LOG.log(Level.FINEST, "Fold: {0}", range);
                    }
                    this.addFold(range, result, foldType);
                }
                folds.remove(type);
                return true;
            }
            GsfFoldManager.LOG.log(Level.FINEST, "No folds of type {0}", type);
            return false;
        }

        private void addTree(Collection<FoldInfo> result, ParserResult info, Map<String, List<OffsetRange>> folds, Collection<? extends FoldType> ftypes) {
            if (this.cancelled.get()) {
                return;
            }
            folds = new HashMap<String, List<OffsetRange>>(folds);
            for (FoldType ft : ftypes) {
                this.addFoldsOfType(ft.code(), folds, result, ft);
            }
            this.addFoldsOfType("codeblocks", folds, result, GsfFoldManager.CODE_BLOCK_FOLD_TYPE);
            this.addFoldsOfType("comments", folds, result, GsfFoldManager.JAVADOC_FOLD_TYPE);
            this.addFoldsOfType("initial-comment", folds, result, GsfFoldManager.INITIAL_COMMENT_FOLD_TYPE);
            this.addFoldsOfType("imports", folds, result, GsfFoldManager.IMPORTS_FOLD_TYPE);
            this.addFoldsOfType("tags", folds, result, GsfFoldManager.TAG_FOLD_TYPE);
            this.addFoldsOfType("othercodeblocks", folds, result, GsfFoldManager.CODE_BLOCK_FOLD_TYPE);
            this.addFoldsOfType("inner-classes", folds, result, GsfFoldManager.INNER_CLASS_FOLD_TYPE);
            if (folds.size() > 0) {
                GsfFoldManager.LOG.log(Level.WARNING, "Undefined fold types used in {0}: {1}", new Object[]{info, folds.keySet()});
            }
        }

        private void addFold(OffsetRange range, Collection<FoldInfo> folds, FoldType type) {
            if (range != OffsetRange.NONE) {
                int start = range.getStart();
                int end = range.getEnd();
                if (start != -1 && end != -1) {
                    FoldInfo fi = FoldInfo.range((int)start, (int)end, (FoldType)type);
                    if (fi.getType() == GsfFoldManager.IMPORTS_FOLD_TYPE && this.imports == null) {
                        this.imports = fi;
                    }
                    folds.add(fi);
                }
            }
        }

        public int getPriority() {
            return Integer.MAX_VALUE;
        }

        public Class<? extends Scheduler> getSchedulerClass() {
            return GsfFoldScheduler.class;
        }

        public void cancel() {
            this.cancelled.set(true);
        }

    }

}

