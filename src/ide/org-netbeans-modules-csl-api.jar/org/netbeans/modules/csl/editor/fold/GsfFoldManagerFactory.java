/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.fold.FoldManager
 *  org.netbeans.spi.editor.fold.FoldManagerFactory
 */
package org.netbeans.modules.csl.editor.fold;

import org.netbeans.modules.csl.editor.fold.GsfFoldManager;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;

public class GsfFoldManagerFactory
implements FoldManagerFactory {
    public FoldManager createFoldManager() {
        return new GsfFoldManager();
    }
}

