/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.codetemplates.api.CodeTemplate
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter
 *  org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter$Factory
 */
package org.netbeans.modules.csl.editor.codetemplates;

import java.util.Set;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.editor.completion.GsfCompletionProvider;

public class GsfCodeTemplateFilter
implements CodeTemplateFilter {
    private int startOffset;
    private int endOffset;
    private Set<String> templates;

    private GsfCodeTemplateFilter(JTextComponent component, int offset) {
        CodeCompletionHandler completer;
        this.startOffset = offset;
        this.endOffset = component.getSelectionStart() == offset ? component.getSelectionEnd() : -1;
        Document doc = component.getDocument();
        CodeCompletionHandler codeCompletionHandler = completer = doc == null ? null : GsfCompletionProvider.getCompletable(doc, this.startOffset);
        if (completer != null) {
            this.templates = completer.getApplicableTemplates(doc, this.startOffset, this.endOffset);
        }
    }

    public boolean accept(CodeTemplate template) {
        if (this.templates != null && template != null && template.getParametrizedText().indexOf("${selection") != -1) {
            return this.templates.contains(template.getAbbreviation()) || template.getParametrizedText().indexOf("allowSurround") != -1;
        }
        return true;
    }

    public static final class Factory
    implements CodeTemplateFilter.Factory {
        public CodeTemplateFilter createFilter(JTextComponent component, int offset) {
            return new GsfCodeTemplateFilter(component, offset);
        }
    }

}

