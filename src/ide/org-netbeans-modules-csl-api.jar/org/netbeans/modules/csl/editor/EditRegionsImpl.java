/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.csl.editor;

import java.util.Set;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.csl.api.DataLoadersBridge;
import org.netbeans.modules.csl.api.EditRegions;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.core.Language;
import org.netbeans.modules.csl.core.LanguageRegistry;
import org.netbeans.modules.csl.editor.InstantRenamePerformer;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class EditRegionsImpl
extends EditRegions {
    @Override
    public void edit(final FileObject fo, final Set<OffsetRange> regions, final int caretOffset) throws BadLocationException {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        EditRegionsImpl.this.edit(fo, regions, caretOffset);
                    }
                    catch (BadLocationException ble) {
                        Exceptions.printStackTrace((Throwable)ble);
                    }
                }
            });
            return;
        }
        JEditorPane[] panes = DataLoadersBridge.getDefault().getOpenedPanes(fo);
        if (panes == null || panes.length == 0) {
            return;
        }
        JEditorPane pane = panes[0];
        Document doc = pane.getDocument();
        Language language = LanguageRegistry.getInstance().getLanguageByMimeType(fo.getMIMEType());
        if (language == null) {
            return;
        }
        if (regions != null && regions.size() > 0) {
            InstantRenamePerformer.performInstantRename(pane, regions, caretOffset);
        }
    }

}

