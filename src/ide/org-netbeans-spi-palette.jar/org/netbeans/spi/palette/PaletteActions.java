/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.palette;

import javax.swing.Action;
import org.openide.util.Lookup;

public abstract class PaletteActions {
    public abstract Action[] getImportActions();

    public abstract Action[] getCustomPaletteActions();

    public abstract Action[] getCustomCategoryActions(Lookup var1);

    public abstract Action[] getCustomItemActions(Lookup var1);

    public abstract Action getPreferredAction(Lookup var1);

    public Action getRefreshAction() {
        return null;
    }

    public Action getResetAction() {
        return null;
    }
}

