/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.spi.palette;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.SwingUtilities;
import org.netbeans.modules.palette.DefaultModel;
import org.netbeans.modules.palette.DefaultSettings;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.RootNode;
import org.netbeans.modules.palette.Settings;
import org.netbeans.spi.palette.DragAndDropHandler;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.netbeans.spi.palette.PaletteFilter;
import org.netbeans.spi.palette.PaletteSwitch;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class PaletteFactory {
    private PaletteFactory() {
    }

    public static PaletteController createPalette(String rootFolderName, PaletteActions customActions) throws IOException {
        return PaletteFactory.createPalette(rootFolderName, customActions, null, DragAndDropHandler.getDefault());
    }

    public static PaletteController createPalette(String rootFolderName, PaletteActions customActions, PaletteFilter filter, DragAndDropHandler dndHandler) throws IOException {
        if (null == rootFolderName) {
            throw new IllegalArgumentException("Folder name cannot be null.");
        }
        DataFolder paletteFolder = DataFolder.findFolder((FileObject)PaletteFactory.getPaletteFolder(rootFolderName));
        return PaletteFactory.createPalette(paletteFolder.getNodeDelegate(), customActions, filter, dndHandler);
    }

    public static PaletteController createPalette(Node paletteRoot, PaletteActions customActions) {
        return PaletteFactory.createPalette(paletteRoot, customActions, null, DragAndDropHandler.getDefault());
    }

    public static PaletteController createPalette(Node paletteRoot, PaletteActions customActions, PaletteFilter filter, DragAndDropHandler dndHandler) {
        if (null == paletteRoot) {
            throw new IllegalArgumentException("Palette root Node cannot be null.");
        }
        if (null == customActions) {
            throw new IllegalArgumentException("Palette custom actions must be provided.");
        }
        ArrayList<Object> lookupObjects = new ArrayList<Object>(3);
        lookupObjects.add(customActions);
        if (null != filter) {
            lookupObjects.add(filter);
        }
        if (null == dndHandler) {
            dndHandler = DragAndDropHandler.getDefault();
        }
        lookupObjects.add(dndHandler);
        RootNode root = new RootNode(paletteRoot, Lookups.fixed((Object[])lookupObjects.toArray()));
        Model model = PaletteFactory.createModel(root);
        DefaultSettings settings = new DefaultSettings(model);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                PaletteSwitch.getDefault().startListening();
            }
        });
        return new PaletteController(model, settings);
    }

    private static Model createModel(RootNode root) {
        return new DefaultModel(root);
    }

    private static FileObject getPaletteFolder(String folderName) throws IOException {
        FileObject paletteFolder = FileUtil.getConfigFile((String)folderName);
        if (paletteFolder == null) {
            throw new FileNotFoundException(folderName);
        }
        return paletteFolder;
    }

}

