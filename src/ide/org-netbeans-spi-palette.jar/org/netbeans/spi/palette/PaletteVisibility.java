/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.spi.palette;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.Utils;
import org.netbeans.spi.palette.PaletteController;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

final class PaletteVisibility {
    private static final RequestProcessor RP = new RequestProcessor("PaletteVisibility", 1);

    PaletteVisibility() {
    }

    public static boolean isVisible(PaletteController pc) {
        if (null == pc) {
            return PaletteVisibility.isVisible(null, false);
        }
        return PaletteVisibility.isVisible(pc, true);
    }

    private static boolean isVisible(PaletteController pc, boolean defValue) {
        Object val;
        Node rootNode;
        String paletteId = PaletteVisibility.getPaletteId(pc);
        FileObject fo = PaletteVisibility.findPaletteTopComponentSettings();
        boolean res = defValue;
        Object object = val = null == fo ? null : fo.getAttribute("_palette_visible_" + paletteId);
        if (val instanceof Boolean) {
            res = (Boolean)val;
        } else if (null != pc && null != (rootNode = (Node)pc.getRoot().lookup(Node.class))) {
            res = Utils.getBoolean(rootNode, "paletteDefaultVisible", defValue);
        }
        return res;
    }

    public static void setVisible(PaletteController pc, boolean isVisible) {
        String paletteId = PaletteVisibility.getPaletteId(pc);
        PaletteVisibility._setVisible(paletteId, isVisible);
    }

    private static void _setVisible(final String paletteId, final boolean isVisible) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                FileObject fo = PaletteVisibility.findPaletteTopComponentSettings();
                try {
                    if (null != fo) {
                        fo.setAttribute("_palette_visible_" + paletteId, (Object)new Boolean(isVisible));
                    }
                }
                catch (IOException ex) {
                    Logger.getLogger(PaletteVisibility.class.getName()).log(Level.INFO, null, ex);
                }
            }
        });
    }

    private static FileObject findPaletteTopComponentSettings() {
        FileObject res;
        String role = WindowManager.getDefault().getRole();
        String root = "Windows2Local";
        if (null != role) {
            root = root + "/Roles/" + role;
        }
        if (null == (res = FileUtil.getConfigFile((String)(root + "/Modes/commonpalette")))) {
            try {
                res = FileUtil.getConfigFile((String)(root + "/Modes"));
                if (null == res) {
                    res = FileUtil.getConfigFile((String)root);
                    if (null == res) {
                        res = FileUtil.getConfigRoot().createFolder(root);
                    }
                    res = res.createFolder("Modes");
                }
                res = res.createFolder("commonpalette");
            }
            catch (IOException ex) {
                Logger.getLogger(PaletteVisibility.class.getName()).log(Level.INFO, null, ex);
            }
        }
        return res;
    }

    private static String getPaletteId(PaletteController pc) {
        FileObject fo;
        if (null == pc) {
            return "_empty_";
        }
        DataFolder dof = (DataFolder)pc.getModel().getRoot().lookup(DataFolder.class);
        if (null != dof && null != (fo = dof.getPrimaryFile())) {
            return fo.getPath();
        }
        return pc.getModel().getName();
    }

}

