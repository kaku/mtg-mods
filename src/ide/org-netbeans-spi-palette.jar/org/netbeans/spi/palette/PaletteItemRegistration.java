/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.palette;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE, ElementType.PACKAGE})
public @interface PaletteItemRegistration {
    public String paletteid();

    public String category();

    public String itemid();

    public String body() default "";

    public String icon16();

    public String icon32();

    public String name();

    public String tooltip();
}

