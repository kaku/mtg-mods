/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.PasteType
 */
package org.netbeans.spi.palette;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.modules.palette.DefaultModel;
import org.netbeans.modules.palette.ui.TextImporter;
import org.netbeans.spi.palette.PaletteController;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.PasteType;

public abstract class DragAndDropHandler {
    private boolean isTextDnDEnabled;
    private static DragAndDropHandler defaultHandler;

    public DragAndDropHandler() {
        this(false);
    }

    protected DragAndDropHandler(boolean textDnDEnabled) {
        this.isTextDnDEnabled = textDnDEnabled;
    }

    static DragAndDropHandler getDefault() {
        if (null == defaultHandler) {
            defaultHandler = new DefaultDragAndDropHandler();
        }
        return defaultHandler;
    }

    public abstract void customize(ExTransferable var1, Lookup var2);

    public boolean canDrop(Lookup targetCategory, DataFlavor[] flavors, int dndAction) {
        for (int i = 0; i < flavors.length; ++i) {
            if (!PaletteController.ITEM_DATA_FLAVOR.equals(flavors[i])) continue;
            return true;
        }
        return this.isTextDnDEnabled && DataFlavor.selectBestTextFlavor(flavors) != null;
    }

    public boolean doDrop(Lookup targetCategory, Transferable item, int dndAction, int dropIndex) {
        Node categoryNode = (Node)targetCategory.lookup(Node.class);
        try {
            Lookup itemLookup;
            Node itemNode;
            Index order;
            if (item.isDataFlavorSupported(PaletteController.ITEM_DATA_FLAVOR) && null != (itemLookup = (Lookup)item.getTransferData(PaletteController.ITEM_DATA_FLAVOR)) && null != (itemNode = (Node)itemLookup.lookup(Node.class)) && null != (order = (Index)categoryNode.getCookie(Index.class)) && order.indexOf(itemNode) >= 0) {
                return this.moveItem(targetCategory, itemLookup, dropIndex);
            }
            PasteType paste = categoryNode.getDropType(item, dndAction, dropIndex);
            if (null != paste) {
                Node[] itemsBefore = categoryNode.getChildren().getNodes(DefaultModel.canBlock());
                paste.paste();
                Node[] itemsAfter = categoryNode.getChildren().getNodes(DefaultModel.canBlock());
                if (itemsAfter.length == itemsBefore.length + 1) {
                    int currentIndex = -1;
                    Node newItem = null;
                    for (int i = itemsAfter.length - 1; i >= 0; --i) {
                        newItem = itemsAfter[i];
                        currentIndex = i;
                        for (int j = 0; j < itemsBefore.length; ++j) {
                            if (!newItem.equals((Object)itemsBefore[j])) continue;
                            newItem = null;
                            break;
                        }
                        if (null != newItem) break;
                    }
                    if (null != newItem && dropIndex >= 0) {
                        if (currentIndex < dropIndex) {
                            ++dropIndex;
                        }
                        this.moveItem(targetCategory, newItem.getLookup(), dropIndex);
                    }
                }
                return true;
            }
            if (this.isTextDnDEnabled && null != DataFlavor.selectBestTextFlavor(item.getTransferDataFlavors())) {
                this.importTextIntoPalette(targetCategory, item, dropIndex);
                return false;
            }
        }
        catch (IOException ioE) {
            Logger.getLogger(DragAndDropHandler.class.getName()).log(Level.INFO, null, ioE);
        }
        catch (UnsupportedFlavorException e) {
            Logger.getLogger(DragAndDropHandler.class.getName()).log(Level.INFO, null, e);
        }
        return false;
    }

    private boolean moveItem(Lookup category, Lookup itemToMove, int moveToIndex) {
        Node categoryNode = (Node)category.lookup(Node.class);
        if (null == categoryNode) {
            return false;
        }
        Node itemNode = (Node)itemToMove.lookup(Node.class);
        if (null == itemNode) {
            return false;
        }
        Index order = (Index)categoryNode.getCookie(Index.class);
        if (null == order) {
            return false;
        }
        int sourceIndex = order.indexOf(itemNode);
        if (sourceIndex < moveToIndex) {
            --moveToIndex;
        }
        order.move(sourceIndex, moveToIndex);
        return true;
    }

    public boolean canReorderCategories(Lookup paletteRoot) {
        Node rootNode = (Node)paletteRoot.lookup(Node.class);
        if (null != rootNode) {
            return null != rootNode.getCookie(Index.class);
        }
        return false;
    }

    public boolean moveCategory(Lookup category, int moveToIndex) {
        Node categoryNode = (Node)category.lookup(Node.class);
        if (null == categoryNode) {
            return false;
        }
        Node rootNode = categoryNode.getParentNode();
        if (null == rootNode) {
            return false;
        }
        Index order = (Index)rootNode.getCookie(Index.class);
        if (null == order) {
            return false;
        }
        int sourceIndex = order.indexOf(categoryNode);
        if (sourceIndex < moveToIndex) {
            --moveToIndex;
        }
        order.move(sourceIndex, moveToIndex);
        return true;
    }

    private boolean importTextIntoPalette(Lookup targetCategory, Transferable item, int dropIndex) throws IOException, UnsupportedFlavorException {
        DataFlavor flavor = DataFlavor.selectBestTextFlavor(item.getTransferDataFlavors());
        if (null == flavor) {
            return false;
        }
        String textToImport = this.extractText(item, flavor);
        SwingUtilities.invokeLater(new TextImporter(textToImport, targetCategory, dropIndex));
        return true;
    }

    private String extractText(Transferable t, DataFlavor flavor) throws IOException, UnsupportedFlavorException {
        int len;
        Reader reader = flavor.getReaderForText(t);
        if (null == reader) {
            return null;
        }
        StringBuffer res = new StringBuffer();
        char[] buffer = new char[4096];
        while ((len = reader.read(buffer)) > 0) {
            res.append(buffer, 0, len);
        }
        return res.toString();
    }

    private static final class DefaultDragAndDropHandler
    extends DragAndDropHandler {
        private DefaultDragAndDropHandler() {
        }

        @Override
        public void customize(ExTransferable t, Lookup item) {
        }
    }

}

