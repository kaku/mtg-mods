/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.palette;

import java.awt.datatransfer.DataFlavor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.ModelListener;
import org.netbeans.modules.palette.Settings;
import org.openide.util.Lookup;

public final class PaletteController {
    public static final DataFlavor ITEM_DATA_FLAVOR;
    public static final String ATTR_ITEM_WIDTH = "itemWidth";
    public static final String ATTR_SHOW_ITEM_NAMES = "showItemNames";
    public static final String ATTR_ICON_SIZE = "iconSize";
    public static final String ATTR_IS_EXPANDED = "isExpanded";
    public static final String ATTR_IS_VISIBLE = "isVisible";
    public static final String ATTR_IS_READONLY = "isReadonly";
    public static final String ATTR_HELP_ID = "helpId";
    public static final String ATTR_PALETTE_DEFAULT_VISIBILITY = "paletteDefaultVisible";
    public static final String PROP_SELECTED_ITEM = "selectedItem";
    private Model model;
    private Settings settings;
    private PropertyChangeSupport support;

    private PaletteController() {
    }

    PaletteController(Model model, Settings settings) {
        this.model = model;
        this.settings = settings;
        this.support = new PropertyChangeSupport(this);
        this.model.addModelListener(new ModelListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("selectedItem".equals(evt.getPropertyName())) {
                    Lookup oldValue = null == evt.getOldValue() ? Lookup.EMPTY : ((Item)evt.getOldValue()).getLookup();
                    Lookup newValue = null == evt.getNewValue() ? Lookup.EMPTY : ((Item)evt.getNewValue()).getLookup();
                    PaletteController.this.support.firePropertyChange("selectedItem", (Object)oldValue, (Object)newValue);
                }
            }

            @Override
            public void categoriesRemoved(Category[] removedCategories) {
            }

            @Override
            public void categoriesAdded(Category[] addedCategories) {
            }

            @Override
            public void categoriesReordered() {
            }
        });
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.support.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.support.removePropertyChangeListener(listener);
    }

    public Lookup getSelectedItem() {
        Item selItem = this.model.getSelectedItem();
        return null == selItem ? Lookup.EMPTY : selItem.getLookup();
    }

    public void setSelectedItem(Lookup category, Lookup item) {
        this.model.setSelectedItem(category, item);
    }

    public Lookup getSelectedCategory() {
        Category selCategory = this.model.getSelectedCategory();
        return null == selCategory ? Lookup.EMPTY : selCategory.getLookup();
    }

    public void clearSelection() {
        this.model.clearSelection();
    }

    public void refresh() {
        this.model.refresh();
    }

    public void showCustomizer() {
        this.model.showCustomizer(this, this.settings);
    }

    public Lookup getRoot() {
        return this.model.getRoot();
    }

    Model getModel() {
        return this.model;
    }

    void setModel(Model model) {
        this.model = model;
    }

    Settings getSettings() {
        return this.settings;
    }

    static {
        try {
            ITEM_DATA_FLAVOR = new DataFlavor("application/x-java-openide-paletteitem;class=org.openide.util.Lookup", "Paste Item", Lookup.class.getClassLoader());
        }
        catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
    }

}

