/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.palette;

import org.openide.util.Lookup;

public abstract class PaletteFilter {
    public abstract boolean isValidCategory(Lookup var1);

    public abstract boolean isValidItem(Lookup var1);
}

