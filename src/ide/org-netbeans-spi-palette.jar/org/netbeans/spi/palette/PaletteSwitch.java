/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package org.netbeans.spi.palette;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.SwingUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.spi.palette.PaletteController;
import org.netbeans.spi.palette.PaletteModule;
import org.netbeans.spi.palette.PaletteTopComponent;
import org.netbeans.spi.palette.PaletteVisibility;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.windows.TopComponent;

final class PaletteSwitch
implements Runnable,
LookupListener {
    static final String PROP_PALETTE_CONTENTS = "component_palette_contents";
    private static PaletteSwitch theInstance;
    private PropertyChangeListener registryListener;
    private PropertyChangeSupport propertySupport;
    private PaletteController currentPalette;
    private Lookup.Result lookupRes;
    private Object currentToken;
    private static final RequestProcessor RP;
    private Map<TopComponent, Lookup.Result> watchedLkpResults = new WeakHashMap<TopComponent, Lookup.Result>(3);

    private PaletteSwitch() {
        this.propertySupport = new PropertyChangeSupport(this);
    }

    public static synchronized PaletteSwitch getDefault() {
        if (null == theInstance) {
            theInstance = new PaletteSwitch();
        }
        return theInstance;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void startListening() {
        if (!PaletteSwitch.isPaletteWindowEnabled()) {
            return;
        }
        PaletteSwitch paletteSwitch = theInstance;
        synchronized (paletteSwitch) {
            if (null == this.registryListener) {
                this.registryListener = this.createRegistryListener();
                TopComponent.getRegistry().addPropertyChangeListener(this.registryListener);
                this.switchLookupListener();
                this.run();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void stopListening() {
        PaletteSwitch paletteSwitch = theInstance;
        synchronized (paletteSwitch) {
            if (null != this.registryListener) {
                TopComponent.getRegistry().removePropertyChangeListener(this.registryListener);
                this.registryListener = null;
                this.currentPalette = null;
            }
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.propertySupport.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.propertySupport.removePropertyChangeListener(l);
    }

    public PaletteController getCurrentPalette() {
        return this.currentPalette;
    }

    @Override
    public void run() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(this);
            return;
        }
        this.currentToken = new Object();
        TopComponent.Registry registry = TopComponent.getRegistry();
        final TopComponent activeTc = registry.getActivated();
        final HashSet opened = new HashSet(registry.getOpened());
        final PaletteController existingPalette = this.currentPalette;
        final boolean isMaximized = this.isPaletteMaximized();
        final Object token = this.currentToken;
        RP.post(new Runnable(){

            @Override
            public void run() {
                PaletteSwitch.this.findNewPalette(existingPalette, activeTc, opened, isMaximized, token);
            }
        });
    }

    private boolean isPaletteMaximized() {
        boolean isMaximized = true;
        TopComponent.Registry registry = TopComponent.getRegistry();
        Set openedTcs = registry.getOpened();
        for (TopComponent tc : openedTcs) {
            if (!tc.isShowing() || tc instanceof PaletteTopComponent) continue;
            isMaximized = false;
            break;
        }
        return isMaximized;
    }

    PaletteController getPaletteFromTopComponent(TopComponent tc, boolean mustBeShowing, boolean isOpened) {
        Node[] activeNodes;
        DataObject dob;
        if (null == tc || !tc.isShowing() && mustBeShowing) {
            return null;
        }
        PaletteController pc = (PaletteController)tc.getLookup().lookup(PaletteController.class);
        if (null == pc && isOpened && null != (activeNodes = tc.getActivatedNodes()) && activeNodes.length > 0 && null != (dob = (DataObject)activeNodes[0].getLookup().lookup(DataObject.class))) {
            while (dob instanceof DataShadow) {
                dob = ((DataShadow)dob).getOriginal();
            }
            FileObject fo = dob.getPrimaryFile();
            if (!fo.isVirtual()) {
                String mimeType = fo.getMIMEType();
                pc = this.getPaletteFromMimeType(mimeType);
            }
        }
        return pc;
    }

    PaletteController getPaletteFromMimeType(String mimeType) {
        MimePath path = MimePath.get((String)mimeType);
        Lookup lkp = MimeLookup.getLookup((MimePath)path);
        return (PaletteController)lkp.lookup(PaletteController.class);
    }

    private void showHidePaletteTopComponent(PaletteController newPalette, boolean isNewVisible) {
        PaletteController oldPalette = this.currentPalette;
        this.currentPalette = newPalette;
        if (isNewVisible) {
            PaletteTopComponent.showPalette();
            PaletteVisibility.setVisible(newPalette, true);
        } else {
            PaletteTopComponent.hidePalette();
        }
        this.propertySupport.firePropertyChange("component_palette_contents", oldPalette, this.currentPalette);
    }

    private void switchLookupListener() {
        TopComponent active = TopComponent.getRegistry().getActivated();
        if (null != this.lookupRes) {
            this.lookupRes.removeLookupListener((LookupListener)this);
            this.lookupRes = null;
        }
        if (null != active) {
            this.lookupRes = active.getLookup().lookup(new Lookup.Template(PaletteController.class));
            this.lookupRes.addLookupListener((LookupListener)this);
            this.lookupRes.allItems();
        }
    }

    private PropertyChangeListener createRegistryListener() {
        return new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("currentNodes".equals(evt.getPropertyName()) || "opened".equals(evt.getPropertyName()) || "activated".equals(evt.getPropertyName())) {
                    if ("activated".equals(evt.getPropertyName()) || "opened".equals(evt.getPropertyName())) {
                        PaletteSwitch.this.watchOpenedTCs();
                    }
                    if ("activated".equals(evt.getPropertyName())) {
                        PaletteSwitch.this.switchLookupListener();
                    }
                    PaletteSwitch.this.run();
                }
            }
        };
    }

    public void resultChanged(LookupEvent ev) {
        this.run();
    }

    private void watchOpenedTCs() {
        Lookup.Result res;
        ArrayList<TopComponent> windowsToWatch = this.findShowingTCs();
        ArrayList<TopComponent> toAddListeners = new ArrayList<TopComponent>(windowsToWatch);
        toAddListeners.removeAll(this.watchedLkpResults.keySet());
        ArrayList<TopComponent> toRemoveListeners = new ArrayList<TopComponent>(this.watchedLkpResults.keySet());
        toRemoveListeners.removeAll(windowsToWatch);
        for (TopComponent tc2 : toRemoveListeners) {
            res = this.watchedLkpResults.get((Object)tc2);
            if (null == res) continue;
            res.removeLookupListener((LookupListener)this);
            this.watchedLkpResults.remove((Object)tc2);
        }
        for (TopComponent tc2 : toAddListeners) {
            res = tc2.getLookup().lookup(new Lookup.Template(PaletteController.class));
            res.addLookupListener((LookupListener)this);
            res.allItems();
            this.watchedLkpResults.put(tc2, res);
        }
    }

    private ArrayList<TopComponent> findShowingTCs() {
        ArrayList<TopComponent> res = new ArrayList<TopComponent>(3);
        for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
            if (!tc.isShowing()) continue;
            res.add(tc);
        }
        return res;
    }

    private void findNewPalette(PaletteController existingPalette, TopComponent activeTc, Set<TopComponent> openedTcs, boolean paletteIsMaximized, final Object token) {
        PaletteController newPalette;
        PaletteController palette = this.getPaletteFromTopComponent(activeTc, true, true);
        paletteIsMaximized &= this.isCurrentPaletteAvailable(existingPalette, openedTcs);
        ArrayList<PaletteController> availablePalettes = new ArrayList<PaletteController>(3);
        if (null == palette) {
            for (TopComponent tc : openedTcs) {
                palette = this.getPaletteFromTopComponent(tc, true, true);
                if (null == palette) continue;
                availablePalettes.add(palette);
            }
            if (null != existingPalette && (availablePalettes.contains(existingPalette) || paletteIsMaximized)) {
                palette = existingPalette;
            } else if (availablePalettes.size() > 0) {
                palette = (PaletteController)availablePalettes.get(0);
            }
        }
        if (existingPalette == (newPalette = palette) && null != newPalette) {
            return;
        }
        final boolean isNewVisible = PaletteVisibility.isVisible(newPalette) || PaletteVisibility.isVisible(null);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (PaletteSwitch.this.currentToken == token) {
                    PaletteSwitch.this.showHidePaletteTopComponent(newPalette, isNewVisible);
                }
            }
        });
    }

    private boolean isCurrentPaletteAvailable(PaletteController existingPalette, Set<TopComponent> openedTcs) {
        for (TopComponent tc : openedTcs) {
            PaletteController palette = this.getPaletteFromTopComponent(tc, false, true);
            if (null == palette || palette != existingPalette) continue;
            return true;
        }
        return false;
    }

    private static boolean isPaletteWindowEnabled() {
        boolean result = true;
        try {
            String resValue = NbBundle.getMessage(PaletteModule.class, (String)"Palette.Window.Enabled");
            result = "true".equals(resValue.toLowerCase());
        }
        catch (MissingResourceException mrE) {
            // empty catch block
        }
        return result;
    }

    static {
        RP = new RequestProcessor("PaletteSwitch");
    }

}

