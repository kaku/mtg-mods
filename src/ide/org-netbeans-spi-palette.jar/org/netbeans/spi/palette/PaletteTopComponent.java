/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.spi.palette;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JScrollPane;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.netbeans.modules.palette.ui.PalettePanel;
import org.netbeans.spi.palette.PaletteController;
import org.netbeans.spi.palette.PaletteSwitch;
import org.netbeans.spi.palette.PaletteVisibility;
import org.openide.util.HelpCtx;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

final class PaletteTopComponent
extends TopComponent
implements PropertyChangeListener {
    static final long serialVersionUID = 4248268998485315735L;
    private static PaletteTopComponent instance;

    private PaletteTopComponent() {
        this.setName(Utils.getBundleString("CTL_Component_palette"));
        this.setToolTipText(Utils.getBundleString("HINT_PaletteComponent"));
        this.setLayout((LayoutManager)new BorderLayout());
        this.setPreferredSize(new Dimension(505, 88));
        this.add((Component)PalettePanel.getDefault().getScrollPane(), (Object)"Center");
        this.putClientProperty((Object)"keepPreferredSizeWhenSlideIn", (Object)Boolean.TRUE);
    }

    public void requestActive() {
        super.requestActive();
        PalettePanel.getDefault().requestFocusInWindow();
    }

    public static synchronized PaletteTopComponent getDefault() {
        if (instance == null) {
            instance = new PaletteTopComponent();
        }
        return instance;
    }

    public int getPersistenceType() {
        return 0;
    }

    public void componentOpened() {
        PaletteSwitch switcher = PaletteSwitch.getDefault();
        switcher.addPropertyChangeListener(this);
        PaletteController pc = switcher.getCurrentPalette();
        this.setPaletteController(pc);
        if (Utils.isOpenedByUser(this)) {
            PaletteVisibility.setVisible(pc, true);
        }
    }

    public void componentClosed() {
        PaletteSwitch switcher = PaletteSwitch.getDefault();
        switcher.removePropertyChangeListener(this);
        PaletteController pc = switcher.getCurrentPalette();
        PaletteVisibility.setVisible(pc, false);
    }

    public Object writeReplace() {
        return new ResolvableHelper();
    }

    protected String preferredID() {
        return "CommonPalette";
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if ("component_palette_contents".equals(e.getPropertyName())) {
            PaletteController pc = (PaletteController)e.getNewValue();
            this.setPaletteController(pc);
        }
    }

    private void setPaletteController(PaletteController pc) {
        if (null != pc) {
            PalettePanel.getDefault().setContent(pc, pc.getModel(), pc.getSettings());
        } else {
            PalettePanel.getDefault().setContent(null, null, null);
        }
    }

    public HelpCtx getHelpCtx() {
        return PalettePanel.getDefault().getHelpCtx();
    }

    static void showPalette() {
        WindowManager wm = WindowManager.getDefault();
        TopComponent palette = wm.findTopComponent("CommonPalette");
        if (null == palette) {
            Logger.getLogger(PaletteSwitch.class.getName()).log(Level.INFO, "Cannot find CommonPalette component.");
            palette = PaletteTopComponent.getDefault();
        }
        if (!palette.isOpened()) {
            palette.open();
        }
    }

    static void hidePalette() {
        PaletteTopComponent palette = instance;
        if (palette != null && palette.isOpened()) {
            palette.close();
        }
    }

    static final class ResolvableHelper
    implements Serializable {
        static final long serialVersionUID = 7424646018839457788L;

        ResolvableHelper() {
        }

        public Object readResolve() {
            return PaletteTopComponent.getDefault();
        }
    }

}

