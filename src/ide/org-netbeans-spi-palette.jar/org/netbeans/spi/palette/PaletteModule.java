/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 *  org.openide.windows.WindowManager
 */
package org.netbeans.spi.palette;

import java.awt.EventQueue;
import org.netbeans.spi.palette.PaletteSwitch;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

@Deprecated
public class PaletteModule
extends ModuleInstall
implements Runnable {
    public void restored() {
        super.restored();
        WindowManager.getDefault().invokeWhenUIReady((Runnable)this);
    }

    @Override
    public void run() {
        assert (EventQueue.isDispatchThread());
        PaletteSwitch.getDefault().startListening();
    }
}

