/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.text.ActiveEditorDrop
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.netbeans.modules.palette;

import org.openide.ErrorManager;
import org.openide.text.ActiveEditorDrop;
import org.openide.util.Lookup;
import org.openide.util.lookup.InstanceContent;

class ActiveEditorDropProvider
implements InstanceContent.Convertor<String, ActiveEditorDrop> {
    private static ActiveEditorDropProvider instance = new ActiveEditorDropProvider();

    private ActiveEditorDropProvider() {
    }

    static ActiveEditorDropProvider getInstance() {
        return instance;
    }

    public Class<? extends ActiveEditorDrop> type(String obj) {
        return ActiveEditorDrop.class;
    }

    public String id(String obj) {
        return obj;
    }

    public String displayName(String obj) {
        return obj;
    }

    public ActiveEditorDrop convert(String obj) {
        return this.getActiveEditorDrop(obj);
    }

    private ActiveEditorDrop getActiveEditorDrop(String instanceName) {
        ActiveEditorDrop drop = null;
        if (instanceName != null && instanceName.trim().length() > 0) {
            try {
                ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                if (loader == null) {
                    loader = this.getClass().getClassLoader();
                }
                Class instanceClass = loader.loadClass(instanceName);
                drop = (ActiveEditorDrop)instanceClass.newInstance();
            }
            catch (Exception ex) {
                ErrorManager.getDefault().notify(1, (Throwable)ex);
            }
        }
        return drop;
    }
}

