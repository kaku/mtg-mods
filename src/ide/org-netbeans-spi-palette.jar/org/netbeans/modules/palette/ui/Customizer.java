/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.netbeans.modules.palette.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreeCellRenderer;
import org.netbeans.modules.palette.DefaultSettings;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.netbeans.modules.palette.ui.CheckListener;
import org.netbeans.modules.palette.ui.CheckRenderer;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class Customizer
extends JPanel
implements ExplorerManager.Provider,
Lookup.Provider {
    private ExplorerManager explorerManager;
    private Lookup lookup;
    private Node root;
    private PaletteController controller;
    private Settings settings;
    private JButton[] customButtons;
    private JLabel captionLabel;
    private JPanel customActionsPanel;
    private JLabel infoLabel;
    private JButton moveDownButton;
    private JButton moveUpButton;
    private JButton newCategoryButton;
    private JButton removeButton;
    private JButton resetButton;
    private JPanel treePanel;

    public static void show(Node paletteRoot, PaletteController controller, Settings settings) {
        JButton closeButton = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)closeButton, (String)Utils.getBundleString("CTL_Close_Button"));
        closeButton.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_Close"));
        DialogDescriptor dd = new DialogDescriptor((Object)new Customizer(paletteRoot, controller, settings), Utils.getBundleString("CTL_Customizer_Title"), false, new Object[]{closeButton}, (Object)closeButton, 0, null, null);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dd);
        dialog.setVisible(true);
    }

    public Customizer(Node paletteRoot, PaletteController controller, Settings settings) {
        this.root = paletteRoot;
        this.controller = controller;
        this.settings = settings;
        this.explorerManager = new ExplorerManager();
        ActionMap map = this.getActionMap();
        map.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this.explorerManager));
        map.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this.explorerManager));
        map.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this.explorerManager));
        map.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this.explorerManager, (boolean)true));
        this.lookup = ExplorerUtils.createLookup((ExplorerManager)this.explorerManager, (ActionMap)map);
        this.explorerManager.setRootContext(paletteRoot);
        this.initComponents();
        this.createCustomButtons();
        CheckTreeView treeView = new CheckTreeView(settings);
        treeView.getAccessibleContext().setAccessibleName(Utils.getBundleString("ACSN_PaletteContentsTree"));
        treeView.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_PaletteContentsTree"));
        this.treePanel.add((Component)((Object)treeView), "Center");
        this.captionLabel.setLabelFor((Component)((Object)treeView));
        this.explorerManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent ev) {
                if ("selectedNodes".equals(ev.getPropertyName())) {
                    Customizer.this.updateInfoLabel(Customizer.this.explorerManager.getSelectedNodes());
                    Customizer.this.updateButtons();
                }
            }
        });
        this.updateButtons();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        ExplorerUtils.activateActions((ExplorerManager)this.explorerManager, (boolean)true);
    }

    @Override
    public void removeNotify() {
        ExplorerUtils.activateActions((ExplorerManager)this.explorerManager, (boolean)false);
        super.removeNotify();
    }

    public ExplorerManager getExplorerManager() {
        return this.explorerManager;
    }

    public Lookup getLookup() {
        return this.lookup;
    }

    private void updateButtons() {
        Node[] selNodes = this.explorerManager.getSelectedNodes();
        boolean canRemove = null != selNodes && selNodes.length > 0;
        boolean canMoveUp = null != selNodes && selNodes.length == 1;
        boolean canMoveDown = null != selNodes && selNodes.length == 1;
        for (int i = 0; null != selNodes && i < selNodes.length; ++i) {
            Node parent;
            Node node = selNodes[i];
            if (!node.canDestroy()) {
                canRemove = false;
            }
            if (null == (parent = node.getParentNode()) || Customizer.movePossible(node, parent, true) < 0) {
                canMoveUp = false;
            }
            if (null != parent && Customizer.movePossible(node, parent, false) >= 0) continue;
            canMoveDown = false;
        }
        this.removeButton.setEnabled(canRemove);
        this.moveUpButton.setEnabled(canMoveUp);
        this.moveDownButton.setEnabled(canMoveDown);
        this.newCategoryButton.setEnabled(new Utils.NewCategoryAction(this.root).isEnabled());
    }

    private void initComponents() {
        this.captionLabel = new JLabel();
        this.treePanel = new JPanel();
        this.infoLabel = new JLabel();
        this.moveUpButton = new JButton();
        this.moveDownButton = new JButton();
        this.removeButton = new JButton();
        this.newCategoryButton = new JButton();
        this.customActionsPanel = new JPanel();
        this.resetButton = new JButton();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.captionLabel, (String)Utils.getBundleString("CTL_Caption"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 10, 0, 10);
        this.add((Component)this.captionLabel, gridBagConstraints);
        this.treePanel.setBackground(UIManager.getLookAndFeelDefaults().getColor("7-white"));
        this.treePanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.treePanel.setPreferredSize(new Dimension(288, 336));
        this.treePanel.setLayout(new BorderLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 7;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this.add((Component)this.treePanel, gridBagConstraints);
        this.infoLabel.setText(" ");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 10);
        this.add((Component)this.infoLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.moveUpButton, (String)Utils.getBundleString("CTL_MoveUp_Button"));
        this.moveUpButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                Customizer.this.moveUpButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(28, 12, 0, 10);
        this.add((Component)this.moveUpButton, gridBagConstraints);
        this.moveUpButton.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_MoveUp"));
        Mnemonics.setLocalizedText((AbstractButton)this.moveDownButton, (String)Utils.getBundleString("CTL_MoveDown_Button"));
        this.moveDownButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                Customizer.this.moveDownButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(5, 12, 0, 10);
        this.add((Component)this.moveDownButton, gridBagConstraints);
        this.moveDownButton.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_MoveDown"));
        Mnemonics.setLocalizedText((AbstractButton)this.removeButton, (String)Utils.getBundleString("CTL_Remove_Button"));
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                Customizer.this.removeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(5, 12, 0, 10);
        this.add((Component)this.removeButton, gridBagConstraints);
        this.removeButton.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_Remove"));
        Mnemonics.setLocalizedText((AbstractButton)this.newCategoryButton, (String)Utils.getBundleString("CTL_NewCategory_Button"));
        this.newCategoryButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                Customizer.this.newCategoryButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(5, 12, 0, 10);
        this.add((Component)this.newCategoryButton, gridBagConstraints);
        this.newCategoryButton.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_NewCategory"));
        this.customActionsPanel.setLayout(new GridBagLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        this.add((Component)this.customActionsPanel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.resetButton, (String)Utils.getBundleString("CTL_ResetPalette"));
        this.resetButton.setActionCommand("Reset Palette");
        this.resetButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                Customizer.this.resetButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(28, 12, 0, 10);
        this.add((Component)this.resetButton, gridBagConstraints);
        this.resetButton.getAccessibleContext().setAccessibleName(Utils.getBundleString("ASCN_ResetPalette"));
        this.resetButton.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ASCD_ResetPalette"));
        this.getAccessibleContext().setAccessibleDescription(Utils.getBundleString("ACSD_PaletteCustomizer"));
    }

    private void resetButtonActionPerformed(ActionEvent evt) {
        Utils.resetPalette(this.controller, this.settings);
    }

    private void removeButtonActionPerformed(ActionEvent evt) {
        Node[] selected = this.explorerManager.getSelectedNodes();
        if (selected.length == 0) {
            return;
        }
        if (selected.length == 1 && !selected[0].canDestroy()) {
            return;
        }
        NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)Utils.getBundleString("MSG_ConfirmPaletteDelete"), Utils.getBundleString("CTL_ConfirmDeleteTitle"), 0);
        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc))) {
            try {
                for (int i = 0; i < selected.length; ++i) {
                    if (!selected[i].canDestroy()) continue;
                    selected[i].destroy();
                }
            }
            catch (IOException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, e);
            }
        }
    }

    private void moveDownButtonActionPerformed(ActionEvent evt) {
        this.moveNode(false);
    }

    private void moveUpButtonActionPerformed(ActionEvent evt) {
        this.moveNode(true);
    }

    private void newCategoryButtonActionPerformed(ActionEvent evt) {
        new Utils.NewCategoryAction(this.root).actionPerformed(evt);
    }

    private void moveNode(boolean up) {
        Node[] selected = this.explorerManager.getSelectedNodes();
        if (selected.length != 1) {
            return;
        }
        Node node = selected[0];
        Node parent = node.getParentNode();
        if (parent == null) {
            return;
        }
        Index indexCookie = (Index)parent.getCookie(Index.class);
        if (indexCookie == null) {
            return;
        }
        int index = Customizer.movePossible(node, parent, up);
        if (index != -1) {
            if (up) {
                indexCookie.moveUp(index);
            } else {
                indexCookie.moveDown(index);
            }
        }
    }

    private static int movePossible(Node node, Node parentNode, boolean up) {
        if (parentNode == null) {
            return -1;
        }
        Node[] nodes = parentNode.getChildren().getNodes();
        for (int i = 0; i < nodes.length; ++i) {
            if (!nodes[i].getName().equals(node.getName())) continue;
            return up && i > 0 || !up && i + 1 < nodes.length ? i : -1;
        }
        return -1;
    }

    private void updateInfoLabel(Node[] nodes) {
        Item item;
        String text = " ";
        if (nodes.length == 1 && (item = (Item)nodes[0].getCookie(Item.class)) != null) {
            text = item.getShortDescription();
        }
        this.infoLabel.setText(text);
    }

    private void createCustomButtons() {
        PaletteActions customActions = (PaletteActions)this.root.getLookup().lookup(PaletteActions.class);
        if (null == customActions) {
            return;
        }
        Action[] actions = customActions.getImportActions();
        if (null == actions || actions.length == 0) {
            return;
        }
        this.customButtons = new JButton[actions.length];
        for (int i = 0; i < actions.length; ++i) {
            this.customButtons[i] = new JButton(actions[i]);
            if (null != actions[i].getValue("Name")) {
                Mnemonics.setLocalizedText((AbstractButton)this.customButtons[i], (String)actions[i].getValue("Name").toString());
            }
            if (null != actions[i].getValue("LongDescription")) {
                this.customButtons[i].getAccessibleContext().setAccessibleDescription(actions[i].getValue("LongDescription").toString());
            }
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = i;
            gridBagConstraints.fill = 2;
            gridBagConstraints.insets = new Insets(5, 12, 0, 10);
            this.customActionsPanel.add((Component)this.customButtons[i], gridBagConstraints);
        }
    }

    private static class CheckTreeView
    extends BeanTreeView {
        public CheckTreeView(Settings settings) {
            if (settings instanceof DefaultSettings) {
                CheckListener l = new CheckListener((DefaultSettings)settings);
                this.tree.addMouseListener(l);
                this.tree.addKeyListener(l);
                CheckRenderer check = new CheckRenderer((DefaultSettings)settings);
                this.tree.setCellRenderer(check);
            }
            this.tree.setEditable(false);
            this.tree.setBorder(new EmptyBorder(0, 8, 0, 0));
            this.tree.setBackground(UIManager.getLookAndFeelDefaults().getColor("7-white"));
        }
    }

}

