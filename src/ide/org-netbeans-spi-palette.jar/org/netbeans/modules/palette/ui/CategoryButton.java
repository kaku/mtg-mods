/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.palette.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.Autoscroll;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.UIResource;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.netbeans.modules.palette.ui.AutoscrollSupport;
import org.netbeans.modules.palette.ui.CategoryDescriptor;
import org.netbeans.modules.palette.ui.CategoryList;
import org.netbeans.modules.palette.ui.PalettePanel;
import org.netbeans.spi.palette.PaletteController;
import org.openide.awt.Mnemonics;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

class CategoryButton
extends JCheckBox
implements Autoscroll {
    static final boolean isGTK = "GTK".equals(UIManager.getLookAndFeel().getID());
    static final boolean isNimbus = "Nimbus".equals(UIManager.getLookAndFeel().getID());
    static final boolean isAqua = "Aqua".equals(UIManager.getLookAndFeel().getID());
    private CategoryDescriptor descriptor;
    private Category category;
    private AutoscrollSupport support;
    private static Color aquaBackground;

    @Override
    public String getUIClassID() {
        String classID = super.getUIClassID();
        if (isGTK) {
            classID = "MetalCheckBoxUI_4_GTK";
        }
        return classID;
    }

    CategoryButton(CategoryDescriptor descriptor, Category category) {
        this.descriptor = descriptor;
        this.category = category;
        UIManager.get("nb.propertysheet");
        this.setFont(this.getFont().deriveFont(1));
        this.setFocusPainted(false);
        this.setSelected(false);
        this.setHorizontalAlignment(2);
        this.setHorizontalTextPosition(4);
        this.setVerticalTextPosition(0);
        this.updateProperties();
        this.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                boolean opened = !CategoryButton.this.descriptor.isOpened();
                CategoryButton.this.setExpanded(opened);
            }
        });
        this.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                CategoryButton.this.scrollRectToVisible(CategoryButton.this.getBounds());
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });
        this.initActions();
    }

    @Override
    public Insets getInsets() {
        return super.getInsets();
    }

    @Override
    public void setBorder(Border border) {
        super.setBorder(new CategoryButtonBorder());
    }

    private void initActions() {
        InputMap inputMap = this.getInputMap(0);
        inputMap.put(KeyStroke.getKeyStroke(40, 0, false), "moveFocusDown");
        inputMap.put(KeyStroke.getKeyStroke(38, 0, false), "moveFocusUp");
        inputMap.put(KeyStroke.getKeyStroke(37, 0, false), "collapse");
        inputMap.put(KeyStroke.getKeyStroke(39, 0, false), "expand");
        inputMap.put(KeyStroke.getKeyStroke(121, 64, false), "popup");
        inputMap.put(KeyStroke.getKeyStroke("ctrl V"), "paste");
        inputMap.put(KeyStroke.getKeyStroke("PASTE"), "paste");
        ActionMap actionMap = this.getActionMap();
        actionMap.put("moveFocusDown", new MoveFocusAction(true));
        actionMap.put("moveFocusUp", new MoveFocusAction(false));
        actionMap.put("collapse", new ExpandAction(false));
        actionMap.put("expand", new ExpandAction(true));
        actionMap.put("popup", new PopupAction());
        Node categoryNode = (Node)this.category.getLookup().lookup(Node.class);
        if (null != categoryNode) {
            actionMap.put("paste", new Utils.PasteItemAction(categoryNode));
        }
    }

    void updateProperties() {
        Icon iconExpanded;
        Icon iconCollapsed = UIManager.getIcon(isGTK ? "Tree.gtk_collapsedIcon" : "Tree.collapsedIcon2");
        if (iconCollapsed == null) {
            iconCollapsed = UIManager.getIcon("Tree.collapsedIcon");
        }
        if ((iconExpanded = UIManager.getIcon(isGTK ? "Tree.gtk_expandedIcon" : "Tree.expandedIcon2")) == null) {
            iconExpanded = UIManager.getIcon("Tree.expandedIcon");
        }
        this.setIcon(iconCollapsed);
        this.setSelectedIcon(iconExpanded);
        this.setRolloverIcon(iconCollapsed);
        this.setRolloverSelectedIcon(iconExpanded);
        Mnemonics.setLocalizedText((AbstractButton)this, (String)this.category.getDisplayName());
        this.setToolTipText(this.category.getShortDescription());
        this.getAccessibleContext().setAccessibleName(this.category.getDisplayName());
        this.getAccessibleContext().setAccessibleDescription(this.category.getShortDescription());
        this.setOpaque(true);
        if (isAqua) {
            this.setContentAreaFilled(true);
            this.setOpaque(true);
            this.setBackground(new Color(0, 0, 0));
            this.setForeground(new Color(255, 255, 255));
        }
        if (isNimbus) {
            this.setOpaque(true);
            this.setContentAreaFilled(true);
        }
    }

    Category getCategory() {
        return this.category;
    }

    @Override
    public void autoscroll(Point cursorLoc) {
        Container dest = this.getParent().getParent();
        if (null == dest || null == SwingUtilities.getWindowAncestor(dest)) {
            return;
        }
        Point p = SwingUtilities.convertPoint(this, cursorLoc, dest);
        this.getSupport().autoscroll(p);
    }

    @Override
    public Insets getAutoscrollInsets() {
        return this.getSupport().getAutoscrollInsets();
    }

    boolean isExpanded() {
        return this.isSelected();
    }

    void setExpanded(boolean expand) {
        this.setSelected(expand);
        if (this.descriptor.isOpened() == expand) {
            return;
        }
        this.descriptor.setOpened(expand);
        this.descriptor.getPalettePanel().computeHeights(expand ? this.category : null);
        this.requestFocus();
    }

    AutoscrollSupport getSupport() {
        if (null == this.support) {
            this.support = new AutoscrollSupport(PalettePanel.getDefault());
        }
        return this.support;
    }

    @Override
    public Color getBackground() {
        if (this.isFocusOwner()) {
            Color color = UIManager.getColor("Palette.selectedCategoryBackground");
            if (color != null) {
                return color;
            }
            if (isGTK || isNimbus) {
                return UIManager.getColor("Tree.selectionBackground");
            }
            return UIManager.getColor("PropSheet.selectedSetBackground");
        }
        Color color = UIManager.getColor("Palette.categoryBackground");
        if (color != null) {
            return color;
        }
        if (isAqua) {
            Color defBk = UIManager.getColor("NbExplorerView.background");
            if (null == defBk) {
                defBk = Color.gray;
            }
            return new Color(defBk.getRed() - 10, defBk.getGreen() - 10, defBk.getBlue() - 10);
        }
        if (isGTK || isNimbus) {
            if (this.getModel().isRollover()) {
                return new Color(UIManager.getColor("Menu.background").getRGB()).darker();
            }
            return new Color(UIManager.getColor("Menu.background").getRGB());
        }
        return UIManager.getColor("PropSheet.setBackground");
    }

    @Override
    public Color getForeground() {
        if (this.isFocusOwner()) {
            Color color = UIManager.getColor("Palette.selectedCategoryForeground");
            if (color != null) {
                return color;
            }
            if (isAqua) {
                return UIManager.getColor("Table.foreground");
            }
            if (isGTK || isNimbus) {
                return UIManager.getColor("Tree.selectionForeground");
            }
            return UIManager.getColor("PropSheet.selectedSetForeground");
        }
        Color color = UIManager.getColor("Palette.categoryForeground");
        if (color != null) {
            return color;
        }
        if (isAqua) {
            Color res = UIManager.getColor("PropSheet.setForeground");
            if (res == null && (res = UIManager.getColor("Table.foreground")) == null && (res = UIManager.getColor("textText")) == null) {
                res = Color.BLACK;
            }
            return res;
        }
        if (isGTK || isNimbus) {
            return new Color(UIManager.getColor("Menu.foreground").getRGB());
        }
        return super.getForeground();
    }

    static {
        if (isGTK) {
            UIManager.put("MetalCheckBoxUI_4_GTK", "javax.swing.plaf.metal.MetalCheckBoxUI");
        }
        if (isAqua) {
            Color defBk = UIManager.getColor("NbExplorerView.background");
            if (null == defBk) {
                defBk = new JPanel().getBackground();
            }
            aquaBackground = new Color(Math.max(0, defBk.getRed() - 15), Math.max(0, defBk.getGreen() - 15), Math.max(0, defBk.getBlue() - 15));
        }
    }

    private class PopupAction
    extends AbstractAction {
        private PopupAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Action[] actions = CategoryButton.this.category.getActions();
            JPopupMenu popup = Utilities.actionsToPopup((Action[])actions, (Component)CategoryButton.this);
            Utils.addCustomizationMenuItems(popup, CategoryButton.this.descriptor.getPalettePanel().getController(), CategoryButton.this.descriptor.getPalettePanel().getSettings());
            popup.show(CategoryButton.this.getParent(), 0, CategoryButton.this.getHeight());
        }
    }

    private class ExpandAction
    extends AbstractAction {
        private boolean expand;

        public ExpandAction(boolean expand) {
            this.expand = expand;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (this.expand == CategoryButton.this.isExpanded()) {
                return;
            }
            CategoryButton.this.setExpanded(this.expand);
        }
    }

    private class MoveFocusAction
    extends AbstractAction {
        private boolean moveDown;

        public MoveFocusAction(boolean moveDown) {
            this.moveDown = moveDown;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Component next;
            KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            Container container = kfm.getCurrentFocusCycleRoot();
            FocusTraversalPolicy policy = container.getFocusTraversalPolicy();
            if (null == policy) {
                policy = kfm.getDefaultFocusTraversalPolicy();
            }
            Component component = next = this.moveDown ? policy.getComponentAfter(container, CategoryButton.this) : policy.getComponentBefore(container, CategoryButton.this);
            if (null != next && next instanceof CategoryList) {
                if (((CategoryList)next).getModel().getSize() != 0) {
                    ((CategoryList)next).takeFocusFrom(CategoryButton.this);
                    return;
                }
                Component component2 = next = this.moveDown ? policy.getComponentAfter(container, next) : policy.getComponentBefore(container, next);
            }
            if (null != next && next instanceof CategoryButton) {
                next.requestFocus();
            }
        }
    }

    private class CategoryButtonBorder
    implements Border,
    UIResource {
        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        }

        @Override
        public Insets getBorderInsets(Component c) {
            Insets inset = UIManager.getLookAndFeelDefaults().getInsets("category-Insets");
            if (inset == null) {
                inset = new Insets(3, 3, 1, 3);
            }
            return inset;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }
    }

}

