/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.palette.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ScrollPaneLayout;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.text.Document;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.ModelListener;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.netbeans.modules.palette.ui.CategoryButton;
import org.netbeans.modules.palette.ui.CategoryDescriptor;
import org.netbeans.modules.palette.ui.CategoryList;
import org.netbeans.modules.palette.ui.DnDSupport;
import org.netbeans.spi.palette.PaletteController;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class PalettePanel
extends JPanel
implements Scrollable {
    private static PalettePanel theInstance;
    private PaletteController controller;
    private Model model;
    private Settings settings;
    private ModelListener modelListener;
    private PropertyChangeListener settingsListener;
    private CategoryDescriptor[] descriptors = new CategoryDescriptor[0];
    private Category selectedCategory;
    private final Object lock = new Object();
    private MouseListener mouseListener;
    private static final boolean isAquaLaF;
    private MyScrollPane scrollPane;
    private DnDSupport dndSupport;
    private final KeyListener kl;
    private PropertyChangeListener _lookAndFeelListener;
    private JPanel searchpanel = null;
    private JTextField searchTextField;
    private int originalScrollMode;

    private PalettePanel() {
        this.searchTextField = new JTextField(){

            @Override
            public boolean isManagingFocus() {
                return true;
            }

            @Override
            public void processKeyEvent(KeyEvent ke) {
                if (ke.getKeyCode() == 27) {
                    PalettePanel.this.removeSearchField();
                    ke.consume();
                } else {
                    super.processKeyEvent(ke);
                }
            }
        };
        this.setLayout(new PaletteLayoutManager());
        this.addMouseListener(this.mouseListener());
        this.kl = new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (keyCode == 27 && PalettePanel.this.removeSearchField()) {
                    e.consume();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
                if (!e.isConsumed() && e.getKeyChar() != '\u001b') {
                    PalettePanel.this.displaySearchField(e.getKeyChar());
                }
            }
        };
        if (!GraphicsEnvironment.isHeadless()) {
            this.dndSupport = new DnDSupport(this);
        }
        this.updateLAF();
        this.addKeyListener(this.kl);
        SearchFieldListener searchFieldListener = new SearchFieldListener();
        this.searchTextField.addKeyListener(searchFieldListener);
        this.searchTextField.addFocusListener(searchFieldListener);
        this.searchTextField.getDocument().addDocumentListener(searchFieldListener);
    }

    private void updateLAF() {
        Color background = UIManager.getColor("Palette.background");
        if (background != null) {
            this.setBackground(background);
        } else if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.setBackground(UIManager.getColor("NbExplorerView.background"));
        } else {
            this.setBackground(UIManager.getColor("Panel.background"));
        }
        if (this.scrollPane != null) {
            this.scrollPane.updateLAF();
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._lookAndFeelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                PalettePanel.this.updateLAF();
            }
        };
        UIManager.addPropertyChangeListener(this._lookAndFeelListener);
    }

    @Override
    public void removeNotify() {
        UIManager.removePropertyChangeListener(this._lookAndFeelListener);
        this._lookAndFeelListener = null;
        super.removeNotify();
    }

    public static synchronized PalettePanel getDefault() {
        if (null == theInstance) {
            theInstance = new PalettePanel();
        }
        return theInstance;
    }

    public JScrollPane getScrollPane() {
        if (null == this.scrollPane) {
            this.scrollPane = new MyScrollPane(this);
        }
        return this.scrollPane;
    }

    CategoryDescriptor getCategoryDescriptor(Category category) {
        for (int i = 0; i < this.descriptors.length; ++i) {
            CategoryDescriptor descriptor = this.descriptors[i];
            if (descriptor.getCategory() != category) continue;
            return descriptor;
        }
        return null;
    }

    private CategoryDescriptor[] computeDescriptors(Category[] categories) {
        if (null == categories) {
            return new CategoryDescriptor[0];
        }
        String searchString = this.getSearchString();
        categories = this.getVisibleCategories(categories);
        ArrayList<CategoryDescriptor> newDescriptors = new ArrayList<CategoryDescriptor>(categories.length);
        for (int i = 0; i < categories.length; ++i) {
            Category category = categories[i];
            CategoryDescriptor descriptor = this.getCategoryDescriptor(category);
            if (descriptor == null) {
                descriptor = new CategoryDescriptor(this, category);
                descriptor.getList().addKeyListener(this.kl);
                descriptor.getButton().addKeyListener(this.kl);
                descriptor.setShowNames(this.getSettings().getShowItemNames());
                descriptor.setIconSize(this.getSettings().getIconSize());
            } else {
                descriptor.refresh();
            }
            descriptor.setWidth(this.getWidth());
            if (!descriptor.match(searchString)) continue;
            newDescriptors.add(descriptor);
        }
        return newDescriptors.toArray(new CategoryDescriptor[newDescriptors.size()]);
    }

    private String getSearchString() {
        if (null == this.searchpanel) {
            return null;
        }
        return this.searchTextField.getText().trim().toLowerCase();
    }

    private Category[] getVisibleCategories(Category[] cats) {
        ArrayList<Category> tmp = new ArrayList<Category>(cats.length);
        for (int i = 0; i < cats.length; ++i) {
            if (!this.settings.isVisible(cats[i])) continue;
            tmp.add(cats[i]);
        }
        return tmp.toArray(new Category[tmp.size()]);
    }

    void computeHeights(Category openedCategory) {
        this.computeHeights(this.descriptors, openedCategory);
    }

    private void computeHeights(CategoryDescriptor[] paletteCategoryDescriptors, Category openedCategory) {
        if (paletteCategoryDescriptors == null || paletteCategoryDescriptors.length <= 0) {
            return;
        }
        this.revalidate();
    }

    private static boolean arrayContains(Object[] objects, Object object) {
        if (objects == null || object == null) {
            return false;
        }
        for (int i = 0; i < objects.length; ++i) {
            if (objects[i] != object) continue;
            return true;
        }
        return false;
    }

    private void setDescriptors(CategoryDescriptor[] paletteCategoryDescriptors) {
        int i;
        for (i = 0; i < this.descriptors.length; ++i) {
            CategoryDescriptor descriptor = this.descriptors[i];
            if (PalettePanel.arrayContains(paletteCategoryDescriptors, descriptor)) continue;
            this.remove(descriptor.getComponent());
            if (this.dndSupport == null) continue;
            this.dndSupport.remove(descriptor);
        }
        for (i = 0; i < paletteCategoryDescriptors.length; ++i) {
            CategoryDescriptor paletteCategoryDescriptor = paletteCategoryDescriptors[i];
            if (PalettePanel.arrayContains(this.descriptors, paletteCategoryDescriptor)) continue;
            this.add(paletteCategoryDescriptor.getComponent());
            if (this.dndSupport == null) continue;
            this.dndSupport.add(paletteCategoryDescriptor);
        }
        if (this.descriptors.length == 0 && paletteCategoryDescriptors.length > 0) {
            boolean isAnyCategoryOpened = false;
            for (int i2 = 0; i2 < paletteCategoryDescriptors.length; ++i2) {
                if (!paletteCategoryDescriptors[i2].isOpened()) continue;
                isAnyCategoryOpened = true;
                break;
            }
            if (!isAnyCategoryOpened) {
                paletteCategoryDescriptors[0].setOpened(true);
            }
        }
        this.descriptors = paletteCategoryDescriptors;
        this.revalidate();
    }

    public void doRefresh() {
        if (null != this.controller) {
            this.controller.refresh();
        }
    }

    public void refresh() {
        Runnable runnable = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = PalettePanel.this.lock;
                synchronized (object) {
                    PalettePanel.this.setCursor(Cursor.getPredefinedCursor(3));
                    CategoryDescriptor[] paletteCategoryDescriptors = PalettePanel.this.computeDescriptors(null != PalettePanel.this.model ? PalettePanel.this.model.getCategories() : null);
                    PalettePanel.this.setDescriptors(paletteCategoryDescriptors);
                    if (null != PalettePanel.this.settings) {
                        PalettePanel.this.setIconSize(PalettePanel.this.settings.getIconSize());
                        PalettePanel.this.setShowItemNames(PalettePanel.this.settings.getShowItemNames());
                        PalettePanel.this.setItemWidth(PalettePanel.this.settings.getShowItemNames() ? PalettePanel.this.settings.getItemWidth() : -1);
                    }
                    if (null != PalettePanel.this.model) {
                        Item item = PalettePanel.this.model.getSelectedItem();
                        Category category = PalettePanel.this.model.getSelectedCategory();
                        PalettePanel.this.setSelectedItemFromModel(category, item);
                    }
                    PalettePanel.this.setCursor(Cursor.getDefaultCursor());
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        this.refresh();
    }

    void select(Category category, Item item) {
        CategoryDescriptor selectedDescriptor;
        if (category != this.selectedCategory && (selectedDescriptor = this.findDescriptorFor(this.selectedCategory)) != null) {
            selectedDescriptor.setSelectedItem(null);
        }
        this.selectedCategory = category;
        if (null != this.model) {
            if (null == category || null == item) {
                this.model.clearSelection();
            } else {
                this.model.setSelectedItem(category.getLookup(), item.getLookup());
            }
        }
    }

    private void setSelectedItemFromModel(Category category, Item item) {
        CategoryDescriptor selectedDescriptor;
        CategoryDescriptor descriptor;
        if (null != this.selectedCategory && !this.selectedCategory.equals(category) && (selectedDescriptor = this.findDescriptorFor(this.selectedCategory)) != null) {
            selectedDescriptor.setSelectedItem(null);
        }
        if ((descriptor = this.findDescriptorFor(category)) == null) {
            return;
        }
        if (item != null) {
            this.selectedCategory = category;
        }
        descriptor.setSelectedItem(item);
    }

    private CategoryDescriptor findDescriptorFor(Category category) {
        if (null != this.descriptors) {
            for (int i = 0; i < this.descriptors.length; ++i) {
                CategoryDescriptor descriptor = this.descriptors[i];
                if (!descriptor.getCategory().equals(category)) continue;
                return descriptor;
            }
        }
        return null;
    }

    private void scrollToCategory(final Category category) {
        Runnable runnable = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = PalettePanel.this.lock;
                synchronized (object) {
                    CategoryDescriptor descriptor = PalettePanel.this.findDescriptorFor(category);
                    if (null != descriptor) {
                        PalettePanel.this.scrollPane.validate();
                        Point loc = descriptor.getComponent().getLocation();
                        PalettePanel.this.scrollPane.getViewport().setViewPosition(loc);
                    }
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setContent(PaletteController newController, Model newModel, Settings newSettings) {
        Object object = this.lock;
        synchronized (object) {
            if (newModel == this.model && newSettings == this.settings) {
                return;
            }
            Model old = this.model;
            if (this.model != null && null != this.modelListener) {
                this.model.removeModelListener(this.modelListener);
            }
            if (this.settings != null && null != this.settingsListener) {
                this.settings.removePropertyChangeListener(this.settingsListener);
            }
            this.model = newModel;
            this.settings = newSettings;
            this.controller = newController;
            this.selectedCategory = null;
            if (this.model != null) {
                this.model.addModelListener(this.getModelListener());
            }
            if (null != this.settings) {
                this.settings.addPropertyChangeListener(this.getSettingsListener());
            }
            this.refresh();
        }
    }

    private MouseListener mouseListener() {
        if (null == this.mouseListener) {
            this.mouseListener = new MouseAdapter(){

                @Override
                public void mouseClicked(MouseEvent event) {
                    if (SwingUtilities.isRightMouseButton(event) && null != PalettePanel.this.model) {
                        JPopupMenu popup = Utilities.actionsToPopup((Action[])PalettePanel.this.model.getActions(), (Component)PalettePanel.this);
                        Utils.addCustomizationMenuItems(popup, PalettePanel.this.getController(), PalettePanel.this.getSettings());
                        popup.show((Component)event.getSource(), event.getX(), event.getY());
                    }
                }
            };
        }
        return this.mouseListener;
    }

    private void setShowItemNames(boolean showNames) {
        for (int i = 0; i < this.descriptors.length; ++i) {
            this.descriptors[i].setShowNames(showNames);
        }
        this.repaint();
    }

    private void setIconSize(int iconSize) {
        for (int i = 0; i < this.descriptors.length; ++i) {
            this.descriptors[i].setIconSize(iconSize);
        }
        this.repaint();
    }

    private void setItemWidth(int itemWidth) {
        for (int i = 0; i < this.descriptors.length; ++i) {
            this.descriptors[i].setItemWidth(itemWidth);
        }
        this.repaint();
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return this.getPreferredSize();
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 100;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 20;
    }

    public HelpCtx getHelpCtx() {
        HelpCtx ctx = null;
        if (null != this.getModel()) {
            Node selNode;
            Item selItem = this.getModel().getSelectedItem();
            if (null != selItem && null != (selNode = (Node)selItem.getLookup().lookup(Node.class))) {
                ctx = selNode.getHelpCtx();
            }
            if (null == ctx || HelpCtx.DEFAULT_HELP.equals((Object)ctx)) {
                Node selNode2;
                CategoryDescriptor selCategory = null;
                for (int i = 0; i < this.descriptors.length; ++i) {
                    if (!this.descriptors[i].isSelected()) continue;
                    selCategory = this.descriptors[i];
                    break;
                }
                if (null != selCategory && null != (selNode2 = (Node)selCategory.getCategory().getLookup().lookup(Node.class))) {
                    ctx = selNode2.getHelpCtx();
                }
            }
            if ((null == ctx || HelpCtx.DEFAULT_HELP.equals((Object)ctx)) && null != (selNode = (Node)this.getModel().getRoot().lookup(Node.class))) {
                ctx = selNode.getHelpCtx();
            }
        }
        if (null == ctx || HelpCtx.DEFAULT_HELP.equals((Object)ctx)) {
            ctx = new HelpCtx("CommonPalette");
        }
        return ctx;
    }

    private ModelListener getModelListener() {
        if (null == this.modelListener) {
            this.modelListener = new ModelListener(){

                @Override
                public void categoriesAdded(Category[] addedCategories) {
                    PalettePanel.this.refresh();
                    if (null != addedCategories && addedCategories.length > 0) {
                        PalettePanel.this.scrollToCategory(addedCategories[0]);
                    }
                }

                @Override
                public void categoriesRemoved(Category[] removedCategories) {
                    PalettePanel.this.refresh();
                }

                @Override
                public void categoriesReordered() {
                    PalettePanel.this.refresh();
                }

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("selectedItem".equals(evt.getPropertyName())) {
                        Item selectedItem = PalettePanel.this.model.getSelectedItem();
                        Category selectedCategory = PalettePanel.this.model.getSelectedCategory();
                        PalettePanel.this.setSelectedItemFromModel(selectedCategory, selectedItem);
                    }
                }
            };
        }
        return this.modelListener;
    }

    private PropertyChangeListener getSettingsListener() {
        if (null == this.settingsListener) {
            this.settingsListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("isVisible".equals(evt.getPropertyName())) {
                        PalettePanel.this.refresh();
                        for (int i = 0; null != PalettePanel.this.descriptors && i < PalettePanel.this.descriptors.length; ++i) {
                            PalettePanel.this.descriptors[i].computeItems();
                        }
                    } else if ("iconSize".equals(evt.getPropertyName())) {
                        PalettePanel.this.setIconSize(PalettePanel.this.getSettings().getIconSize());
                    } else if ("showItemNames".equals(evt.getPropertyName())) {
                        PalettePanel.this.setShowItemNames(PalettePanel.this.getSettings().getShowItemNames());
                        PalettePanel.this.setItemWidth(PalettePanel.this.getSettings().getShowItemNames() ? PalettePanel.this.getSettings().getItemWidth() : -1);
                    }
                }
            };
        }
        return this.settingsListener;
    }

    Model getModel() {
        return this.model;
    }

    Settings getSettings() {
        return this.settings;
    }

    PaletteController getController() {
        return this.controller;
    }

    @Override
    public void updateUI() {
        super.updateUI();
        if (null != this.model) {
            this.model.refresh();
        }
    }

    private void prepareSearchPanel() {
        if (this.searchpanel == null) {
            this.searchpanel = new SearchPanel();
            JLabel lbl = new JLabel(NbBundle.getMessage(PalettePanel.class, (String)"LBL_QUICKSEARCH"));
            this.searchpanel.setLayout(new GridBagLayout());
            this.searchpanel.add((Component)lbl, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 17, 0, new Insets(0, 0, 0, 5), 0, 0));
            this.searchpanel.add((Component)this.searchTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 17, 0, new Insets(0, 0, 0, 5), 0, 0));
            this.searchpanel.add((Component)new JLabel(), new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, 17, 0, new Insets(0, 0, 0, 0), 0, 0));
            lbl.setLabelFor(this.searchTextField);
            this.searchTextField.setColumns(10);
            this.searchTextField.setMaximumSize(this.searchTextField.getPreferredSize());
            this.searchTextField.putClientProperty("JTextField.variant", "search");
            lbl.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
            JButton btnCancel = new JButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/palette/resources/cancel.png", (boolean)true)){

                @Override
                public void updateUI() {
                    this.setUI(new BasicButtonUI());
                }
            };
            btnCancel.setRolloverIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/palette/resources/cancel_rollover.png", (boolean)true));
            btnCancel.setPressedIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/palette/resources/cancel_pressed.png", (boolean)true));
            btnCancel.setBorder(BorderFactory.createEmptyBorder());
            btnCancel.setBorderPainted(false);
            btnCancel.setOpaque(false);
            btnCancel.setContentAreaFilled(false);
            this.searchpanel.add((Component)btnCancel, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, 13, 0, new Insets(0, 0, 0, 5), 0, 0));
            btnCancel.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    PalettePanel.this.removeSearchField();
                }
            });
        }
    }

    private void displaySearchField(char initialChar) {
        if (null != this.searchpanel) {
            return;
        }
        JViewport vp = this.scrollPane.getViewport();
        this.originalScrollMode = vp.getScrollMode();
        vp.setScrollMode(0);
        this.searchTextField.setFont(this.getFont());
        this.searchTextField.setText(String.valueOf(initialChar));
        this.prepareSearchPanel();
        this.scrollPane.add(this.searchpanel);
        this.invalidate();
        this.revalidate();
        this.repaint();
        this.searchTextField.requestFocus();
    }

    private boolean removeSearchField() {
        if (null == this.searchpanel) {
            return false;
        }
        this.scrollPane.remove(this.searchpanel);
        this.searchpanel = null;
        this.scrollPane.getViewport().setScrollMode(this.originalScrollMode);
        this.invalidate();
        this.revalidate();
        this.repaint();
        this.refresh();
        this.focusPalette();
        return true;
    }

    private void focusPalette() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (null == PalettePanel.this.model) {
                    return;
                }
                Item item = PalettePanel.this.model.getSelectedItem();
                Category category = PalettePanel.this.model.getSelectedCategory();
                PalettePanel.this.setSelectedItemFromModel(category, item);
                if (null != category) {
                    CategoryDescriptor cd = PalettePanel.this.findDescriptorFor(category);
                    cd.getList().requestFocus();
                } else {
                    PalettePanel.this.descriptors[0].getButton().requestFocus();
                }
            }
        });
    }

    static {
        isAquaLaF = "Aqua".equals(UIManager.getLookAndFeel().getID());
    }

    private class MyScrollPane
    extends JScrollPane {
        public MyScrollPane(JComponent view) {
            super(view);
            this.setLayout(new MyScrollPaneLayout());
            this.setBorder(BorderFactory.createEmptyBorder());
            this.addMouseListener(PalettePanel.this.mouseListener());
            this.updateLAF();
            this.setViewportBorder(null);
        }

        public void updateLAF() {
            Color background = UIManager.getColor("Palette.background");
            if (background != null) {
                this.getViewport().setBackground(background);
            } else if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                this.getViewport().setBackground(UIManager.getColor("NbExplorerView.background"));
            } else {
                this.getViewport().setBackground(UIManager.getColor("Panel.background"));
            }
        }

        @Override
        public Insets getInsets() {
            Insets res = this.getInnerInsets();
            res = new Insets(res.top, res.left, res.bottom, res.right);
            if (null != PalettePanel.this.searchpanel && PalettePanel.this.searchpanel.isVisible()) {
                res.bottom += PalettePanel.access$1800((PalettePanel)PalettePanel.this).getPreferredSize().height;
            }
            return res;
        }

        private Insets getInnerInsets() {
            Insets res = super.getInsets();
            if (null == res) {
                res = new Insets(0, 0, 0, 0);
            }
            return res;
        }
    }

    private class SearchFieldListener
    extends KeyAdapter
    implements DocumentListener,
    FocusListener {
        SearchFieldListener() {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            PalettePanel.this.refresh();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            PalettePanel.this.refresh();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            PalettePanel.this.refresh();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            if (keyCode == 27) {
                PalettePanel.this.removeSearchField();
            } else if (keyCode == 10) {
                PalettePanel.this.focusPalette();
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            PalettePanel.this.searchTextField.select(0, 0);
            PalettePanel.this.searchTextField.setCaretPosition(PalettePanel.this.searchTextField.getText().length());
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }

    private class PaletteLayoutManager
    implements LayoutManager {
        private PaletteLayoutManager() {
        }

        @Override
        public void addLayoutComponent(String name, Component comp) {
        }

        @Override
        public void layoutContainer(Container parent) {
            int width = PalettePanel.this.getWidth();
            int height = 0;
            for (int i = 0; i < PalettePanel.this.descriptors.length; ++i) {
                CategoryDescriptor paletteCategoryDescriptor = PalettePanel.this.descriptors[i];
                paletteCategoryDescriptor.setPositionY(height);
                JComponent comp = paletteCategoryDescriptor.getComponent();
                comp.setSize(width, comp.getPreferredSize().height);
                height += paletteCategoryDescriptor.getComponent().getHeight();
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container parent) {
            return new Dimension(0, 0);
        }

        @Override
        public Dimension preferredLayoutSize(Container parent) {
            int height = 0;
            int width = PalettePanel.this.getWidth();
            for (int i = 0; i < PalettePanel.this.descriptors.length; ++i) {
                CategoryDescriptor descriptor = PalettePanel.this.descriptors[i];
                height += descriptor.getPreferredHeight(width) + 1;
            }
            return new Dimension(10, height);
        }

        @Override
        public void removeLayoutComponent(Component comp) {
        }
    }

    private class MyScrollPaneLayout
    extends ScrollPaneLayout {
        private MyScrollPaneLayout() {
        }

        @Override
        public void layoutContainer(Container parent) {
            super.layoutContainer(parent);
            if (null != PalettePanel.this.searchpanel && PalettePanel.this.searchpanel.isVisible()) {
                Insets innerInsets = PalettePanel.this.scrollPane.getInnerInsets();
                Dimension prefSize = PalettePanel.this.searchpanel.getPreferredSize();
                PalettePanel.this.searchpanel.setBounds(innerInsets.left, parent.getHeight() - innerInsets.bottom - prefSize.height, parent.getWidth() - innerInsets.left - innerInsets.right, prefSize.height);
            }
        }
    }

    private static class SearchPanel
    extends JPanel {
        public SearchPanel() {
            if (isAquaLaF) {
                this.setBorder(BorderFactory.createEmptyBorder(9, 6, 8, 2));
            } else {
                this.setBorder(BorderFactory.createEmptyBorder(2, 6, 2, 2));
            }
            this.setOpaque(true);
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (isAquaLaF && g instanceof Graphics2D) {
                Graphics2D g2d = (Graphics2D)g;
                Color col1 = UIManager.getColor("NbExplorerView.quicksearch.background.top");
                Color col2 = UIManager.getColor("NbExplorerView.quicksearch.background.bottom");
                Color col3 = UIManager.getColor("NbExplorerView.quicksearch.border");
                if (col1 != null && col2 != null && col3 != null) {
                    g2d.setPaint(new GradientPaint(0.0f, 0.0f, col1, 0.0f, this.getHeight(), col2));
                    g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
                    g2d.setColor(col3);
                    g2d.drawLine(0, 0, this.getWidth(), 0);
                    return;
                }
            }
            super.paintComponent(g);
            g.setColor(UIManager.getColor("PropSheet.setBackground"));
            g.drawLine(0, 0, this.getWidth(), 0);
        }
    }

}

