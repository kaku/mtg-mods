/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.Node
 */
package org.netbeans.modules.palette.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import org.netbeans.modules.palette.DefaultSettings;
import org.netbeans.modules.palette.ui.CheckRenderer;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;

class CheckListener
implements MouseListener,
KeyListener {
    DefaultSettings settings;

    public CheckListener(DefaultSettings settings) {
        this.settings = settings;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int y;
        JTree tree = (JTree)e.getSource();
        Point p = e.getPoint();
        int x = e.getX();
        int row = tree.getRowForLocation(x, y = e.getY());
        TreePath path = tree.getPathForRow(row);
        if (null == path) {
            return;
        }
        Node node = Visualizer.findNode((Object)path.getLastPathComponent());
        if (null == node) {
            return;
        }
        Rectangle chRect = CheckRenderer.getCheckBoxRectangle();
        Rectangle rowRect = tree.getPathBounds(path);
        chRect.setLocation(chRect.x + rowRect.x, chRect.y + rowRect.y);
        if (e.getClickCount() == 1 && chRect.contains(p)) {
            boolean isSelected = this.settings.isNodeVisible(node);
            this.settings.setNodeVisible(node, !isSelected);
            tree.repaint();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == ' ') {
            JTree tree = (JTree)e.getSource();
            TreePath path = tree.getSelectionPath();
            if (null == path) {
                return;
            }
            Node node = Visualizer.findNode((Object)path.getLastPathComponent());
            if (null == node) {
                return;
            }
            boolean isSelected = this.settings.isNodeVisible(node);
            this.settings.setNodeVisible(node, !isSelected);
            tree.repaint();
            e.consume();
        }
    }
}

