/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.palette.ui;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceAdapter;
import java.awt.dnd.DragSourceContext;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.ui.CategoryButton;
import org.netbeans.modules.palette.ui.CategoryDescriptor;
import org.netbeans.modules.palette.ui.CategoryList;
import org.netbeans.modules.palette.ui.DropGlassPane;
import org.netbeans.modules.palette.ui.PalettePanel;

public class DnDSupport
implements DragGestureListener,
DropTargetListener {
    private static final int DELAY_TIME_FOR_EXPAND = 1000;
    private Set<DragGestureRecognizer> recognizers = new HashSet<DragGestureRecognizer>(5);
    private Set<DropTarget> dropTargets = new HashSet<DropTarget>(5);
    private Category draggingCategory;
    private Item draggingItem;
    private CategoryList dragSourceCategoryList;
    private Item targetItem;
    private boolean dropBefore;
    private DragSourceListener dragSourceListener;
    private DropGlassPane dropPane;
    private PalettePanel palette;
    private Timer timer;
    private static final Logger ERR = Logger.getLogger("org.netbeans.modules.palette");

    public DnDSupport(PalettePanel palette) {
        this.palette = palette;
        new DropTarget(palette.getScrollPane(), this);
    }

    void add(CategoryDescriptor descriptor) {
        CategoryList list = descriptor.getList();
        list.setTransferHandler(null);
        list.setDragEnabled(false);
        this.recognizers.add(DragSource.getDefaultDragSource().createDefaultDragGestureRecognizer(list, 2, this));
        this.dropTargets.add(new DropTarget(list, this));
        CategoryButton button = descriptor.getButton();
        button.setTransferHandler(null);
        this.recognizers.add(DragSource.getDefaultDragSource().createDefaultDragGestureRecognizer(button, 2, this));
        this.dropTargets.add(new DropTarget(button, this));
    }

    void remove(CategoryDescriptor descriptor) {
        ArrayList<DragGestureRecognizer> recognizersToRemove = new ArrayList<DragGestureRecognizer>(2);
        for (DragGestureRecognizer dgr : this.recognizers) {
            if (dgr.getComponent() != descriptor.getButton() && dgr.getComponent() != descriptor.getList()) continue;
            recognizersToRemove.add(dgr);
            dgr.removeDragGestureListener(this);
        }
        this.recognizers.removeAll(recognizersToRemove);
        ArrayList<DropTarget> dropTargetsToRemove = new ArrayList<DropTarget>(2);
        for (DropTarget dt : this.dropTargets) {
            if (dt.getComponent() != descriptor.getButton() && dt.getComponent() != descriptor.getList()) continue;
            dropTargetsToRemove.add(dt);
            dt.removeDropTargetListener(this);
        }
        this.dropTargets.removeAll(dropTargetsToRemove);
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent dge) {
        Transferable t = null;
        if (dge.getComponent() instanceof CategoryButton) {
            CategoryButton button = (CategoryButton)dge.getComponent();
            this.draggingCategory = button.getCategory();
            t = this.draggingCategory.getTransferable();
        } else if (dge.getComponent() instanceof CategoryList) {
            CategoryList list = (CategoryList)dge.getComponent();
            int selIndex = list.locationToIndex(dge.getDragOrigin());
            this.draggingItem = list.getItemAt(selIndex);
            if (null == this.draggingItem) {
                return;
            }
            t = this.draggingItem.drag();
            this.dragSourceCategoryList = list;
        }
        if (null != t) {
            dge.getDragSource().addDragSourceListener(this.getDragSourceListener());
            try {
                dge.startDrag(null, t);
            }
            catch (InvalidDnDOperationException idndE) {
                try {
                    dge.startDrag(null, t);
                }
                catch (InvalidDnDOperationException e) {
                    ERR.log(Level.INFO, idndE.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void drop(DropTargetDropEvent dtde) {
        Category[] cats;
        Component target = dtde.getDropTargetContext().getComponent();
        Category targetCategory = null;
        if (target instanceof CategoryList) {
            targetCategory = ((CategoryList)target).getCategory();
        } else if (target instanceof CategoryButton) {
            targetCategory = ((CategoryButton)target).getCategory();
        } else if (target instanceof JScrollPane && null != this.palette.getModel() && null != (cats = this.palette.getModel().getCategories()) && cats.length > 0) {
            targetCategory = cats[cats.length - 1];
        }
        if (null != this.draggingCategory) {
            boolean res = false;
            if (null != targetCategory && target instanceof CategoryButton) {
                res = this.palette.getModel().moveCategory(this.draggingCategory, targetCategory, this.dropBefore);
            }
            dtde.dropComplete(res);
        } else {
            dtde.acceptDrop(dtde.getDropAction());
            boolean res = false;
            if (null != targetCategory) {
                Transferable t = null != this.draggingItem ? this.draggingItem.cut() : dtde.getTransferable();
                res = targetCategory.dropItem(t, dtde.getDropAction(), this.targetItem, this.dropBefore);
            }
            dtde.dropComplete(res);
        }
        this.cleanupAfterDnD();
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
        this.removeDropLine();
        if (DropGlassPane.isOriginalPaneStored()) {
            DropGlassPane.putBackOriginal();
        }
        this.removeTimer();
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
        this.checkStoredGlassPane();
        this.doDragOver(dtde);
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
        CategoryButton button;
        this.checkStoredGlassPane();
        Component target = dtde.getDropTargetContext().getComponent();
        if (!(!(target instanceof CategoryButton) || null != this.draggingCategory || (button = (CategoryButton)target).isSelected() || null != this.timer && this.timer.isRunning())) {
            this.removeTimer();
            this.timer = new Timer(1000, new ActionListener(){

                @Override
                public final void actionPerformed(ActionEvent e) {
                    button.setExpanded(true);
                }
            });
            this.timer.setRepeats(false);
            this.timer.start();
        }
        this.doDragOver(dtde);
    }

    private void removeTimer() {
        if (this.timer != null) {
            ActionListener[] l = (ActionListener[])this.timer.getListeners(ActionListener.class);
            for (int i = 0; i < l.length; ++i) {
                this.timer.removeActionListener(l[i]);
            }
            this.timer.stop();
            this.timer = null;
        }
    }

    private void doDragOver(DropTargetDragEvent dtde) {
        Component target = dtde.getDropTargetContext().getComponent();
        if (null != this.draggingCategory) {
            Category targetCategory = null;
            if (target instanceof CategoryButton) {
                CategoryButton button = (CategoryButton)target;
                targetCategory = button.getCategory();
            }
            if (null == targetCategory || !this.palette.getModel().canReorderCategories()) {
                dtde.rejectDrag();
                this.removeDropLine();
                return;
            }
            this.dropBefore = dtde.getLocation().y < target.getHeight() / 2;
            Point p1 = target.getLocation();
            Point p2 = target.getLocation();
            p2.x += target.getWidth();
            if (!this.dropBefore) {
                p1.y += target.getHeight();
                p2.y += target.getHeight();
            }
            p1 = SwingUtilities.convertPoint(target, p1, this.palette.getRootPane());
            p2 = SwingUtilities.convertPoint(target, p2, this.palette.getRootPane());
            Line2D.Double line = new Line2D.Double(p1.x, p1.y, p2.x, p2.y);
            this.dropPane.setDropLine(line);
        } else {
            CategoryList list;
            Category[] cats;
            Category targetCategory = null;
            if (target instanceof CategoryList) {
                list = (CategoryList)target;
                targetCategory = list.getCategory();
            } else if (target instanceof CategoryButton) {
                CategoryButton button = (CategoryButton)target;
                targetCategory = button.getCategory();
            } else if (target instanceof JScrollPane && null != this.palette.getModel() && null != (cats = this.palette.getModel().getCategories()) && cats.length > 0) {
                targetCategory = cats[cats.length - 1];
            }
            if (null == targetCategory || !targetCategory.dragOver(dtde)) {
                dtde.rejectDrag();
                this.removeDropLine();
                this.targetItem = null;
                return;
            }
            dtde.acceptDrag(dtde.getDropAction());
            if (target instanceof CategoryList) {
                list = (CategoryList)target;
                int dropIndex = list.locationToIndex(dtde.getLocation());
                if (dropIndex < 0) {
                    this.dropPane.setDropLine(null);
                    this.targetItem = null;
                } else {
                    this.setupDropLine(dtde, list, dropIndex);
                }
            } else if (target instanceof JScrollPane) {
                CategoryDescriptor cd;
                if (null != this.palette.getModel() && null != (cats = this.palette.getModel().getCategories()) && cats.length > 0) {
                    targetCategory = cats[cats.length - 1];
                }
                if (null != (cd = this.palette.getCategoryDescriptor(targetCategory))) {
                    cd.getButton().setExpanded(true);
                    CategoryList list2 = cd.getList();
                    int dropIndex = targetCategory.getItems().length - 1;
                    this.setupDropLine(dtde, list2, dropIndex);
                }
            } else {
                this.targetItem = null;
                this.dropBefore = false;
            }
        }
    }

    private DragSourceListener getDragSourceListener() {
        if (null == this.dragSourceListener) {
            this.dragSourceListener = new DragSourceAdapter(){

                @Override
                public void dragDropEnd(DragSourceDropEvent dsde) {
                    dsde.getDragSourceContext().getDragSource().removeDragSourceListener(this);
                    DnDSupport.this.cleanupAfterDnD();
                }
            };
        }
        return this.dragSourceListener;
    }

    private void cleanupAfterDnD() {
        this.draggingItem = null;
        this.draggingCategory = null;
        this.targetItem = null;
        if (null != this.dragSourceCategoryList) {
            this.dragSourceCategoryList.resetRollover();
        }
        this.dragSourceCategoryList = null;
        this.removeDropLine();
        if (DropGlassPane.isOriginalPaneStored()) {
            DropGlassPane.putBackOriginal();
        }
        this.removeTimer();
    }

    private void checkStoredGlassPane() {
        if (!DropGlassPane.isOriginalPaneStored()) {
            Component comp = this.palette.getRootPane().getGlassPane();
            DropGlassPane.setOriginalPane(this.palette, comp, comp.isVisible());
            this.dropPane = DropGlassPane.getDefault(this.palette);
            this.palette.getRootPane().setGlassPane(this.dropPane);
            this.dropPane.revalidate();
            this.dropPane.validate();
            this.dropPane.setVisible(true);
        }
    }

    private void removeDropLine() {
        if (null != this.dropPane) {
            this.dropPane.setDropLine(null);
        }
    }

    private void setupDropLine(DropTargetDragEvent dtde, CategoryList list, int dropIndex) {
        boolean verticalDropBar = list.getColumnCount() > 1;
        Rectangle rect = list.getCellBounds(dropIndex, dropIndex);
        this.dropBefore = verticalDropBar ? dtde.getLocation().x < rect.x + rect.width / 2 : dtde.getLocation().y < rect.y + rect.height / 2;
        Point p1 = rect.getLocation();
        Point p2 = rect.getLocation();
        if (verticalDropBar) {
            p2.y += rect.height;
            if (!this.dropBefore) {
                p1.x += rect.width;
                p2.x += rect.width;
            }
        } else {
            p2.x += rect.width;
            if (!this.dropBefore) {
                p1.y += rect.height;
                p2.y += rect.height;
            }
        }
        p1 = SwingUtilities.convertPoint(list, p1, this.palette.getRootPane());
        p2 = SwingUtilities.convertPoint(list, p2, this.palette.getRootPane());
        Line2D.Double line = new Line2D.Double(p1.x, p1.y, p2.x, p2.y);
        this.dropPane.setDropLine(line);
        this.targetItem = (Item)list.getModel().getElementAt(dropIndex);
    }

}

