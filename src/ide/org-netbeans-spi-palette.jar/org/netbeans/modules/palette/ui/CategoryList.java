/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.palette.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Image;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.Autoscroll;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ListUI;
import javax.swing.plaf.basic.BasicListUI;
import javax.swing.text.Position;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.netbeans.modules.palette.ui.AutoscrollSupport;
import org.netbeans.modules.palette.ui.CategoryButton;
import org.netbeans.modules.palette.ui.PalettePanel;
import org.netbeans.spi.palette.PaletteController;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public class CategoryList
extends JList
implements Autoscroll {
    private int rolloverIndex = -1;
    private boolean showNames;
    static final int BASIC_ICONSIZE = 1;
    private int iconSize = 1;
    private Category category;
    private PalettePanel palettePanel;
    private static WeakReference<ListCellRenderer> rendererRef;
    static final boolean isGTK;
    static final boolean isNimbus;
    private AutoscrollSupport support;
    Integer tempWidth;

    CategoryList(Category category, PalettePanel palettePanel) {
        this.category = category;
        this.palettePanel = palettePanel;
        if (isGTK || isNimbus) {
            this.setBackground(new Color(UIManager.getColor("Tree.background").getRGB()));
            this.setOpaque(true);
        } else {
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                this.setBackground(UIManager.getColor("NbExplorerView.background"));
            } else {
                Color bg = UIManager.getLookAndFeelDefaults().getColor("palette-item-name-bg");
                if (bg == null) {
                    bg = UIManager.getColor("Panel.background");
                }
                this.setBackground(bg);
            }
            this.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.setVisibleRowCount(0);
            this.setSelectionMode(0);
            this.setCellRenderer(CategoryList.getItemRenderer());
            this.setLayoutOrientation(2);
            this.getAccessibleContext().setAccessibleName(category.getDisplayName());
            this.getAccessibleContext().setAccessibleDescription(category.getShortDescription());
            this.initActions();
        }
    }

    private void initActions() {
        InputMap inputMap = this.getInputMap(0);
        inputMap.put(KeyStroke.getKeyStroke(10, 0, false), "defaultAction");
        inputMap.put(KeyStroke.getKeyStroke(121, 64, false), "popup");
        ActionMap map = this.getActionMap();
        map.put("defaultAction", new DefaultAction(this));
        map.put("popup", new PopupAction());
        map.put("selectPreviousRow", new MoveFocusAction(map.get("selectPreviousRow"), false));
        map.put("selectNextRow", new MoveFocusAction(map.get("selectNextRow"), true));
        map.put("selectPreviousColumn", new MoveFocusAction(new ChangeColumnAction(map.get("selectPreviousColumn"), false), false));
        map.put("selectNextColumn", new MoveFocusAction(new ChangeColumnAction(map.get("selectNextColumn"), true), true));
        Node categoryNode = (Node)this.category.getLookup().lookup(Node.class);
        if (null != categoryNode) {
            map.put("paste", new Utils.PasteItemAction(categoryNode));
        } else {
            map.remove("paste");
        }
        map.put("copy", new CutCopyAction(true));
        map.put("cut", new CutCopyAction(false));
    }

    Item getItemAt(int index) {
        if (index < 0 || index >= this.getModel().getSize()) {
            return null;
        }
        return (Item)this.getModel().getElementAt(index);
    }

    Category getCategory() {
        return this.category;
    }

    @Override
    public void updateUI() {
        if (null != rendererRef) {
            rendererRef.clear();
        }
        this.setUI(new CategoryListUI());
        this.invalidate();
    }

    @Override
    public int getWidth() {
        return this.tempWidth == null ? super.getWidth() : this.tempWidth.intValue();
    }

    boolean getShowNames() {
        return this.showNames;
    }

    void setShowNames(boolean show) {
        if (show != this.showNames) {
            this.showNames = show;
            this.firePropertyChange("cellRenderer", null, null);
        }
    }

    int getIconSize() {
        return this.iconSize;
    }

    void setIconSize(int size) {
        if (size != this.iconSize) {
            this.iconSize = size;
            this.firePropertyChange("cellRenderer", null, null);
        }
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        int row;
        if (orientation == 1 && direction < 0 && (row = this.getFirstVisibleIndex()) != -1) {
            Rectangle r = this.getCellBounds(row, row);
            if (r.y == visibleRect.y && row != 0) {
                Point loc = r.getLocation();
                --loc.y;
                int prevIndex = this.locationToIndex(loc);
                Rectangle prevR = this.getCellBounds(prevIndex, prevIndex);
                if (prevR == null || prevR.y >= r.y) {
                    return 0;
                }
                return prevR.height;
            }
        }
        return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
    }

    public int getPreferredHeight(int width) {
        return ((CategoryListUI)this.getUI()).getPreferredHeight(width);
    }

    public void resetRollover() {
        this.rolloverIndex = -1;
        this.repaint();
    }

    int getColumnCount() {
        if (this.getModel().getSize() > 0) {
            int cellWidth;
            Insets insets = this.getInsets();
            int listWidth = this.getWidth() - (insets.left + insets.right);
            if (listWidth >= (cellWidth = this.getCellBounds((int)0, (int)0).width)) {
                return listWidth / cellWidth;
            }
        }
        return 1;
    }

    private static ListCellRenderer getItemRenderer() {
        ListCellRenderer renderer;
        ListCellRenderer listCellRenderer = renderer = rendererRef == null ? null : rendererRef.get();
        if (renderer == null) {
            renderer = new ItemRenderer();
            rendererRef = new WeakReference<ListCellRenderer>(renderer);
        }
        return renderer;
    }

    @Override
    public void autoscroll(Point cursorLoc) {
        if (null != this.getParent() && null != this.getParent().getParent()) {
            Point p = SwingUtilities.convertPoint(this, cursorLoc, this.getParent().getParent());
            this.getSupport().autoscroll(p);
        }
    }

    @Override
    public Insets getAutoscrollInsets() {
        return this.getSupport().getAutoscrollInsets();
    }

    AutoscrollSupport getSupport() {
        if (null == this.support) {
            this.support = new AutoscrollSupport(this.palettePanel);
        }
        return this.support;
    }

    void takeFocusFrom(Component c) {
        int indexToSelect = -1;
        if (c.getParent() != this.getParent()) {
            indexToSelect = this.getModel().getSize() - 1;
        } else if (this.getModel().getSize() > 0) {
            indexToSelect = 0;
        }
        this.requestFocus();
        this.setSelectedIndex(indexToSelect);
        if (indexToSelect >= 0) {
            this.ensureIndexIsVisible(indexToSelect);
        }
    }

    @Override
    public int getNextMatch(String prefix, int startIndex, Position.Bias bias) {
        return -1;
    }

    static {
        isGTK = "GTK".equals(UIManager.getLookAndFeel().getID());
        isNimbus = "Nimbus".equals(UIManager.getLookAndFeel().getID());
    }

    private class CutCopyAction
    extends AbstractAction {
        private boolean doCopy;

        public CutCopyAction(boolean doCopy) {
            this.doCopy = doCopy;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Item item = CategoryList.this.getItemAt(CategoryList.this.getSelectedIndex());
            if (null == item) {
                return;
            }
            Node itemNode = (Node)item.getLookup().lookup(Node.class);
            if (null == itemNode) {
                return;
            }
            Action performer = this.doCopy ? new Utils.CopyItemAction(itemNode) : new Utils.CutItemAction(itemNode);
            if (performer.isEnabled()) {
                performer.actionPerformed(e);
            }
        }

        @Override
        public boolean isEnabled() {
            return CategoryList.this.getSelectedIndex() >= 0;
        }
    }

    private class ChangeColumnAction
    extends AbstractAction {
        private Action defaultAction;
        private boolean selectNext;

        public ChangeColumnAction(Action defaultAction, boolean selectNext) {
            this.defaultAction = defaultAction;
            this.selectNext = selectNext;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int selIndexBefore = CategoryList.this.getSelectedIndex();
            this.defaultAction.actionPerformed(e);
            int selIndexCurrent = CategoryList.this.getSelectedIndex();
            if (this.selectNext && selIndexBefore < selIndexCurrent || !this.selectNext && selIndexBefore > selIndexCurrent) {
                return;
            }
            if (this.selectNext) {
                if (selIndexCurrent == selIndexBefore + 1) {
                    ++selIndexCurrent;
                }
                if (selIndexCurrent < CategoryList.this.getModel().getSize() - 1) {
                    CategoryList.this.setSelectedIndex(selIndexCurrent + 1);
                    CategoryList.this.scrollRectToVisible(CategoryList.this.getCellBounds(selIndexCurrent + 1, selIndexCurrent + 1));
                }
            } else if (selIndexCurrent > 0) {
                CategoryList.this.setSelectedIndex(selIndexCurrent - 1);
                CategoryList.this.scrollRectToVisible(CategoryList.this.getCellBounds(selIndexCurrent - 1, selIndexCurrent - 1));
            }
        }
    }

    private class MoveFocusAction
    extends AbstractAction {
        private Action defaultAction;
        private boolean focusNext;

        public MoveFocusAction(Action defaultAction, boolean focusNext) {
            this.defaultAction = defaultAction;
            this.focusNext = focusNext;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Component next;
            int selIndexBefore = CategoryList.this.getSelectedIndex();
            this.defaultAction.actionPerformed(e);
            int selIndexCurrent = CategoryList.this.getSelectedIndex();
            if (selIndexBefore != selIndexCurrent) {
                return;
            }
            if (this.focusNext && 0 == selIndexCurrent && CategoryList.this.getModel().getSize() > 1 && CategoryList.this.getModel().getSize() > CategoryList.this.getColumnCount()) {
                return;
            }
            KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            Container container = kfm.getCurrentFocusCycleRoot();
            FocusTraversalPolicy policy = container.getFocusTraversalPolicy();
            if (null == policy) {
                policy = kfm.getDefaultFocusTraversalPolicy();
            }
            Component component = next = this.focusNext ? policy.getComponentAfter(container, CategoryList.this) : policy.getComponentBefore(container, CategoryList.this);
            if (null != next && next instanceof CategoryButton) {
                CategoryList.this.clearSelection();
                next.requestFocus();
            }
        }
    }

    private class PopupAction
    extends AbstractAction {
        private PopupAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int posX = 0;
            int posY = 0;
            Item item = CategoryList.this.getItemAt(CategoryList.this.getSelectedIndex());
            if (null != item) {
                Rectangle rect = CategoryList.this.getCellBounds(CategoryList.this.getSelectedIndex(), CategoryList.this.getSelectedIndex());
                posX = rect.x;
                posY = rect.y + rect.height;
            }
            Action[] actions = null == item ? CategoryList.this.category.getActions() : item.getActions();
            JPopupMenu popup = Utilities.actionsToPopup((Action[])actions, (Component)CategoryList.this);
            Utils.addCustomizationMenuItems(popup, CategoryList.this.palettePanel.getController(), CategoryList.this.palettePanel.getSettings());
            popup.show(CategoryList.this.getParent(), posX, posY);
        }

        @Override
        public boolean isEnabled() {
            return CategoryList.this.isEnabled();
        }
    }

    private static class DefaultAction
    extends AbstractAction {
        private CategoryList list;

        public DefaultAction(CategoryList list) {
            this.list = list;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Item item = this.list.getItemAt(this.list.getSelectedIndex());
            item.invokePreferredAction(e);
        }

        @Override
        public boolean isEnabled() {
            return this.list.isEnabled() && this.list.getSelectedIndex() >= 0;
        }
    }

    static class CategoryListUI
    extends BasicListUI {
        CategoryListUI() {
        }

        @Override
        protected void updateLayoutState() {
            super.updateLayoutState();
            if (this.list.getLayoutOrientation() == 2) {
                Insets insets = this.list.getInsets();
                int listWidth = this.list.getWidth() - (insets.left + insets.right);
                if (listWidth >= this.cellWidth) {
                    int columnCount = listWidth / this.cellWidth;
                    this.cellWidth = columnCount == 0 ? 1 : listWidth / columnCount;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getPreferredHeight(int width) {
            int result;
            ((CategoryList)this.list).tempWidth = width;
            try {
                result = (int)this.getPreferredSize(this.list).getHeight();
            }
            finally {
                ((CategoryList)this.list).tempWidth = null;
            }
            return result;
        }

        @Override
        protected MouseInputListener createMouseInputListener() {
            return new ListMouseInputHandler();
        }

        private int getValidIndex(Point p) {
            int index = this.locationToIndex(this.list, p);
            return index >= 0 && this.getCellBounds(this.list, index, index).contains(p) ? index : -1;
        }

        private class ListMouseInputHandler
        extends BasicListUI.MouseInputHandler {
            int selIndex;

            private ListMouseInputHandler() {
                super(CategoryListUI.this);
                this.selIndex = -1;
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (!CategoryListUI.this.list.isEnabled()) {
                    return;
                }
                if (e.getClickCount() > 1) {
                    this.selIndex = CategoryListUI.this.getValidIndex(e.getPoint());
                    if (this.selIndex >= 0) {
                        CategoryListUI.this.list.setSelectedIndex(this.selIndex);
                        Item item = (Item)CategoryListUI.this.list.getModel().getElementAt(this.selIndex);
                        ActionEvent ae = new ActionEvent(e.getSource(), e.getID(), "doubleclick", e.getWhen(), e.getModifiers());
                        item.invokePreferredAction(ae);
                        e.consume();
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (CategoryListUI.this.getValidIndex(e.getPoint()) >= 0) {
                    this.selIndex = CategoryListUI.this.list.getSelectedIndex();
                    super.mousePressed(e);
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                this.mouseEntered(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (CategoryListUI.this.list.isEnabled()) {
                    this.setRolloverIndex(CategoryListUI.this.getValidIndex(e.getPoint()));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (CategoryListUI.this.list.isEnabled()) {
                    this.setRolloverIndex(-1);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (CategoryListUI.this.getValidIndex(e.getPoint()) >= 0) {
                    super.mouseReleased(e);
                    if (this.selIndex > -1 && CategoryListUI.this.list.getSelectedIndex() == this.selIndex) {
                        CategoryListUI.this.list.removeSelectionInterval(this.selIndex, this.selIndex);
                    }
                }
            }

            private void setRolloverIndex(int index) {
                int oldIndex = ((CategoryList)CategoryListUI.this.list).rolloverIndex;
                if (index != oldIndex) {
                    Rectangle r;
                    ((CategoryList)CategoryListUI.this.list).rolloverIndex = index;
                    if (oldIndex > -1 && (r = CategoryListUI.this.getCellBounds(CategoryListUI.this.list, oldIndex, oldIndex)) != null) {
                        CategoryListUI.this.list.repaint(r.x, r.y, r.width, r.height);
                    }
                    if (index > -1 && (r = CategoryListUI.this.getCellBounds(CategoryListUI.this.list, index, index)) != null) {
                        CategoryListUI.this.list.repaint(r.x, r.y, r.width, r.height);
                    }
                }
            }
        }

    }

    static class ItemRenderer
    implements ListCellRenderer {
        private JToolBar toolbar;
        private JToggleButton button;

        ItemRenderer() {
            if (this.button == null) {
                this.button = new JToggleButton();
                this.button.setMargin(new Insets(1, 1, 1, 0));
                if (!CategoryButton.isGTK) {
                    this.toolbar = new JToolBar(){

                        @Override
                        public String getUIClassID() {
                            return "PaletteToolBarUI";
                        }
                    };
                    this.toolbar.setRollover(true);
                    this.toolbar.setFloatable(false);
                    this.toolbar.setLayout(new BorderLayout(0, 0));
                    this.toolbar.setBorder(BorderFactory.createEmptyBorder());
                    this.toolbar.add(this.button);
                    if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                        this.toolbar.setBackground(UIManager.getColor("NbExplorerView.background"));
                    }
                }
            }
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            CategoryList categoryList = (CategoryList)list;
            boolean showNames = categoryList.getShowNames();
            int iconSize = categoryList.getIconSize();
            JComponent rendererComponent = this.toolbar != null ? this.toolbar : this.button;
            Item item = (Item)value;
            Image icon = item.getIcon(iconSize);
            if (icon != null) {
                this.button.setIcon(new ImageIcon(icon));
            }
            this.button.setText(showNames ? item.getDisplayName() : null);
            rendererComponent.setToolTipText(item.getShortDescription());
            this.button.setSelected(isSelected);
            this.button.getModel().setRollover(index == categoryList.rolloverIndex && !isSelected);
            this.button.setBorderPainted(index == categoryList.rolloverIndex || isSelected);
            this.button.setHorizontalAlignment(showNames ? 2 : 0);
            this.button.setHorizontalTextPosition(4);
            this.button.setVerticalTextPosition(0);
            Color c = new Color(UIManager.getColor("Tree.background").getRGB());
            if (CategoryList.isNimbus) {
                this.toolbar.setBackground(c);
            }
            if (CategoryList.isGTK) {
                this.button.setBackground(c);
            }
            return rendererComponent;
        }

    }

}

