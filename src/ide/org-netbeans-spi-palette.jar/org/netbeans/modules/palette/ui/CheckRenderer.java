/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.NodeRenderer
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.Node
 */
package org.netbeans.modules.palette.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import org.netbeans.modules.palette.DefaultSettings;
import org.openide.explorer.view.NodeRenderer;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;

class CheckRenderer
extends JPanel
implements TreeCellRenderer {
    protected JCheckBox check;
    private NodeRenderer nodeRenderer = new NodeRenderer();
    private DefaultSettings settings;
    private static Dimension checkDim;
    static Rectangle checkBounds;
    private Component stringDisplayer = new JLabel(" ");

    public CheckRenderer(DefaultSettings settings) {
        this.settings = settings;
        this.setLayout(null);
        this.check = new JCheckBox();
        this.add(this.check);
        Color c = UIManager.getColor("Tree.textBackground");
        if (c == null) {
            c = Color.WHITE;
        }
        this.check.setBackground(c);
        Dimension dim = this.check.getPreferredSize();
        this.check.setPreferredSize(checkDim);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        this.stringDisplayer = this.nodeRenderer.getTreeCellRendererComponent(tree, value, isSelected, expanded, leaf, row, hasFocus);
        TreePath path = tree.getPathForRow(row);
        if (null != path && 1 == path.getPathCount()) {
            return this.stringDisplayer;
        }
        if (this.stringDisplayer instanceof JComponent) {
            this.setToolTipText(((JComponent)this.stringDisplayer).getToolTipText());
        }
        if (this.stringDisplayer.getBackground() == null) {
            this.stringDisplayer.setBackground(tree.getBackground());
        }
        if (this.stringDisplayer.getForeground() == null) {
            this.stringDisplayer.setForeground(tree.getForeground());
        }
        if (this.check != null) {
            Node node = value instanceof Node ? (Node)value : Visualizer.findNode((Object)value);
            this.check.setSelected(null == node || this.settings.isNodeVisible(node));
            this.check.setEnabled(true);
        }
        return this;
    }

    @Override
    public void paintComponent(Graphics g) {
        Dimension d_check = this.check == null ? new Dimension(0, 0) : this.check.getSize();
        Dimension d_label = this.stringDisplayer == null ? new Dimension(0, 0) : this.stringDisplayer.getPreferredSize();
        boolean y_check = false;
        int y_label = 0;
        if (d_check.height >= d_label.height) {
            y_label = (d_check.height - d_label.height) / 2;
        }
        if (this.check != null) {
            this.check.setBounds(0, 0, d_check.width, d_check.height);
            this.check.paint(g);
        }
        if (this.stringDisplayer != null) {
            int y = y_label - 2;
            this.stringDisplayer.setBounds(d_check.width, y, d_label.width, this.getHeight() - 1);
            g.translate(d_check.width, y_label);
            this.stringDisplayer.paint(g);
            g.translate(- d_check.width, - y_label);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.stringDisplayer != null) {
            this.stringDisplayer.setFont(this.getFont());
        }
        Dimension d_check = this.check == null ? new Dimension(0, CheckRenderer.checkDim.height) : this.check.getPreferredSize();
        Dimension d_label = this.stringDisplayer != null ? this.stringDisplayer.getPreferredSize() : new Dimension(0, 0);
        return new Dimension(d_check.width + d_label.width, d_check.height < d_label.height ? d_label.height : d_check.height);
    }

    @Override
    public void doLayout() {
        Dimension d_check = this.check == null ? new Dimension(0, 0) : this.check.getPreferredSize();
        Dimension d_label = this.stringDisplayer == null ? new Dimension(0, 0) : this.stringDisplayer.getPreferredSize();
        int y_check = 0;
        int y_label = 0;
        if (d_check.height < d_label.height) {
            y_check = (d_label.height - d_check.height) / 2;
        } else {
            y_label = (d_check.height - d_label.height) / 2;
        }
        if (this.check != null) {
            this.check.setLocation(0, y_check);
            this.check.setBounds(0, y_check, d_check.width, d_check.height);
            if (checkBounds == null) {
                checkBounds = this.check.getBounds();
            }
        }
    }

    public static Rectangle getCheckBoxRectangle() {
        if (null == checkBounds) {
            return new Rectangle(0, 0, 0, 0);
        }
        return (Rectangle)checkBounds.clone();
    }

    static {
        Dimension old = new JCheckBox().getPreferredSize();
        checkDim = new Dimension(old.width, old.height - 5);
    }
}

