/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.palette.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRootPane;

final class DropGlassPane
extends JPanel {
    private static HashMap<Integer, DropGlassPane> map = new HashMap();
    private static final int MIN_X = 0;
    private static final int MIN_Y = 0;
    private static final int MIN_WIDTH = 0;
    private static final int MIN_HEIGTH = 0;
    private static Component oldPane;
    private static JComponent originalSource;
    private static boolean wasVisible;
    Line2D line = null;
    Rectangle prevLineRect = null;

    private DropGlassPane() {
    }

    public static synchronized DropGlassPane getDefault(JComponent comp) {
        Integer id = System.identityHashCode(comp);
        if (map.get(id) == null) {
            DropGlassPane dgp = new DropGlassPane();
            dgp.setOpaque(false);
            map.put(id, dgp);
        }
        return map.get(id);
    }

    static void setOriginalPane(JComponent source, Component pane, boolean visible) {
        oldPane = pane;
        originalSource = source;
        wasVisible = visible;
    }

    static boolean isOriginalPaneStored() {
        return oldPane != null;
    }

    static void putBackOriginal() {
        if (oldPane == null) {
            return;
        }
        originalSource.getRootPane().setGlassPane(oldPane);
        oldPane.setVisible(wasVisible);
        oldPane = null;
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (!aFlag) {
            this.setDropLine(null);
        }
    }

    public void setDropLine(Line2D line) {
        if (!this.isValid()) {
            return;
        }
        if (!(null == this.prevLineRect || (null == line || this.prevLineRect.contains(line.getP1()) && this.prevLineRect.contains(line.getP2())) && null != line)) {
            this.repaint(this.prevLineRect);
        }
        this.line = line;
        Rectangle newLineRect = null;
        if (null != this.line) {
            this.checkLineBounds(this.line);
            newLineRect = line.getBounds();
            newLineRect.grow(5, 5);
        }
        if (null != newLineRect && !newLineRect.equals(this.prevLineRect)) {
            this.repaint(newLineRect);
        }
        this.prevLineRect = newLineRect;
    }

    private Line2D checkLineBounds(Line2D line) {
        Rectangle bounds = this.getBounds();
        double startPointX = Math.max(line.getX1(), (double)(bounds.x + 0));
        double startPointY = Math.max(line.getY1(), (double)(bounds.y + 0));
        double endPointX = Math.min(line.getX2(), (double)(bounds.x + bounds.width));
        double endPointY = Math.min(line.getY2(), (double)(bounds.y + bounds.height - 0));
        line.setLine(startPointX, startPointY, endPointX, endPointY);
        return line;
    }

    @Override
    public void paint(Graphics g) {
        if (this.line != null) {
            int y2;
            int x1 = (int)this.line.getX1();
            int x2 = (int)this.line.getX2();
            int y1 = (int)this.line.getY1();
            if (y1 == (y2 = (int)this.line.getY2())) {
                g.drawLine(x1 + 2, y1, x2 - 2, y1);
                g.drawLine(x1 + 2, y1 + 1, x2 - 2, y1 + 1);
                g.drawLine(x1, y1 - 2, x1, y1 + 3);
                g.drawLine(x1 + 1, y2 - 1, x1 + 1, y1 + 2);
                g.drawLine(x2, y1 - 2, x2, y1 + 3);
                g.drawLine(x2 - 1, y1 - 1, x2 - 1, y1 + 2);
            } else {
                g.drawLine(x1, y1 + 2, x2, y2 - 2);
                g.drawLine(x1 + 1, y1 + 2, x2 + 1, y2 - 2);
                g.drawLine(x1 - 2, y1, x1 + 3, y1);
                g.drawLine(x1 - 1, y1 + 1, x1 + 2, y1 + 1);
                g.drawLine(x2 - 2, y2, x2 + 3, y2);
                g.drawLine(x2 - 1, y2 - 1, x2 + 2, y2 - 1);
            }
        }
    }
}

