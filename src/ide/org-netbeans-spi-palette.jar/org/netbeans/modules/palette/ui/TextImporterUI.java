/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.palette.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

class TextImporterUI
extends JPanel {
    private String smallIconPath;
    private String largeIconPath;
    private JButton btnSelectLargeIcon;
    private JButton btnSelectSmallIcon;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JScrollPane jScrollPane1;
    private JLabel lblLargeIcon;
    private JLabel lblSmallIcon;
    private JTextArea txtContent;
    private JTextField txtName;
    private JTextField txtTooltip;
    private static File defaultFolder;

    public TextImporterUI(String content, final JButton btnOk) {
        this.initComponents();
        this.txtContent.setText(content);
        this.txtContent.setCaretPosition(0);
        DocumentListener dl = new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                TextImporterUI.this.updateButton(btnOk);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                TextImporterUI.this.updateButton(btnOk);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                TextImporterUI.this.updateButton(btnOk);
            }
        };
        this.updateButton(btnOk);
        this.txtName.getDocument().addDocumentListener(dl);
        this.txtContent.getDocument().addDocumentListener(dl);
    }

    String getItemName() {
        return this.txtName.getText();
    }

    String getItemTooltip() {
        return this.txtTooltip.getText();
    }

    String getItemContent() {
        return this.txtContent.getText();
    }

    String getItemSmallIconPath() {
        return this.smallIconPath;
    }

    String getItemLargeIconPath() {
        return this.largeIconPath;
    }

    private void updateButton(final JButton btn) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                boolean enable = TextImporterUI.this.txtName.getText().length() > 0;
                btn.setEnabled(enable &= TextImporterUI.this.txtContent.getText().length() > 0);
            }
        });
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.txtName = new JTextField();
        this.jLabel2 = new JLabel();
        this.txtTooltip = new JTextField();
        this.jLabel3 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.txtContent = new JTextArea();
        this.lblLargeIcon = new JLabel();
        this.btnSelectLargeIcon = new JButton();
        this.lblSmallIcon = new JLabel();
        this.btnSelectSmallIcon = new JButton();
        this.jLabel1.setDisplayedMnemonic('N');
        this.jLabel1.setLabelFor(this.txtName);
        this.jLabel1.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.jLabel1.text"));
        this.txtName.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.txtName.text"));
        this.jLabel2.setDisplayedMnemonic('T');
        this.jLabel2.setLabelFor(this.txtTooltip);
        this.jLabel2.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.jLabel2.text"));
        this.txtTooltip.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.txtTooltip.text"));
        this.jLabel3.setLabelFor(this.lblSmallIcon);
        this.jLabel3.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.jLabel3.text"));
        this.jLabel4.setLabelFor(this.lblLargeIcon);
        this.jLabel4.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.jLabel4.text"));
        this.jLabel5.setDisplayedMnemonic('C');
        this.jLabel5.setLabelFor(this.txtContent);
        this.jLabel5.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.jLabel5.text"));
        this.txtContent.setColumns(20);
        this.txtContent.setRows(5);
        this.jScrollPane1.setViewportView(this.txtContent);
        this.txtContent.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.txtContent.AccessibleContext.accessibleDescription"));
        this.lblLargeIcon.setHorizontalAlignment(0);
        this.lblLargeIcon.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/palette/resources/unknown32.gif")));
        this.lblLargeIcon.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.lblLargeIcon.text"));
        this.lblLargeIcon.setBorder(BorderFactory.createEtchedBorder());
        this.lblLargeIcon.setHorizontalTextPosition(0);
        this.lblLargeIcon.setIconTextGap(0);
        this.lblLargeIcon.setPreferredSize(new Dimension(40, 40));
        this.btnSelectLargeIcon.setMnemonic('e');
        this.btnSelectLargeIcon.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.btnSelectLargeIcon.text"));
        this.btnSelectLargeIcon.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                TextImporterUI.this.btnSelectLargeIconActionPerformed(evt);
            }
        });
        this.lblSmallIcon.setHorizontalAlignment(0);
        this.lblSmallIcon.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/palette/resources/unknown16.gif")));
        this.lblSmallIcon.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.lblSmallIcon.text"));
        this.lblSmallIcon.setBorder(BorderFactory.createEtchedBorder());
        this.lblSmallIcon.setFocusable(false);
        this.lblSmallIcon.setHorizontalTextPosition(0);
        this.lblSmallIcon.setIconTextGap(0);
        this.lblSmallIcon.setPreferredSize(new Dimension(40, 40));
        this.btnSelectSmallIcon.setMnemonic('S');
        this.btnSelectSmallIcon.setText(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.btnSelectSmallIcon.text"));
        this.btnSelectSmallIcon.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                TextImporterUI.this.btnSelectSmallIconActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jScrollPane1, GroupLayout.Alignment.LEADING, -1, 577, 32767).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.jLabel2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.txtTooltip, -1, 511, 32767).addComponent(this.txtName, -1, 511, 32767))).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addComponent(this.jLabel3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblSmallIcon, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnSelectSmallIcon).addGap(14, 14, 14).addComponent(this.jLabel4).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblLargeIcon, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnSelectLargeIcon)).addComponent(this.jLabel5, GroupLayout.Alignment.LEADING)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.txtName, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.txtTooltip, -2, -1, -2)).addGap(18, 18, 18).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lblSmallIcon, -2, -1, -2).addComponent(this.jLabel3).addComponent(this.btnSelectSmallIcon).addComponent(this.jLabel4).addComponent(this.lblLargeIcon, -2, -1, -2).addComponent(this.btnSelectLargeIcon)).addGap(18, 18, 18).addComponent(this.jLabel5).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 138, 32767).addContainerGap()));
        this.txtName.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.txtName.AccessibleContext.accessibleName"));
        this.txtName.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.txtName.AccessibleContext.accessibleDescription"));
        this.txtTooltip.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.txtTooltip.AccessibleContext.accessibleDescription"));
        this.lblLargeIcon.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.lblLargeIcon.AccessibleContext.accessibleName"));
        this.lblLargeIcon.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.lblLargeIcon.AccessibleContext.accessibleDescription"));
        this.btnSelectLargeIcon.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.btnSelectLargeIcon.AccessibleContext.accessibleDescription"));
        this.lblSmallIcon.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.lblSmallIcon.AccessibleContext.accessibleName"));
        this.lblSmallIcon.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.lblSmallIcon.AccessibleContext.accessibleDescription"));
        this.btnSelectSmallIcon.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporterUI.class, (String)"TextImporterUI.btnSelectSmallIcon.AccessibleContext.accessibleDescription"));
    }

    private void btnSelectLargeIconActionPerformed(ActionEvent evt) {
        Icon icon;
        File iconFile = this.selectIconFile();
        if (null != iconFile && null != (icon = this.readIconFromFile(iconFile))) {
            this.lblLargeIcon.setIcon(icon);
            try {
                this.largeIconPath = iconFile.toURL().toExternalForm();
            }
            catch (MalformedURLException ex) {
                // empty catch block
            }
        }
    }

    private void btnSelectSmallIconActionPerformed(ActionEvent evt) {
        Icon icon;
        File iconFile = this.selectIconFile();
        if (null != iconFile && null != (icon = this.readIconFromFile(iconFile))) {
            this.lblSmallIcon.setIcon(icon);
            try {
                this.smallIconPath = iconFile.toURL().toExternalForm();
            }
            catch (MalformedURLException ex) {
                // empty catch block
            }
        }
    }

    private File selectIconFile() {
        JFileChooser dlg = new JFileChooser(defaultFolder);
        dlg.setAcceptAllFileFilterUsed(true);
        dlg.setMultiSelectionEnabled(false);
        if (dlg.showOpenDialog(this) != 0) {
            return null;
        }
        defaultFolder = dlg.getCurrentDirectory();
        return dlg.getSelectedFile();
    }

    private Icon readIconFromFile(File iconFile) {
        try {
            BufferedImage img = ImageIO.read(iconFile.toURL());
            if (null != img) {
                ImageIcon res = new ImageIcon(img);
                if (res.getIconWidth() > 32 || res.getIconHeight() > 32) {
                    JOptionPane.showMessageDialog(this, NbBundle.getMessage(TextImporterUI.class, (String)"Err_IconTooBig"), NbBundle.getMessage(TextImporterUI.class, (String)"Err_Title"), 0);
                    return null;
                }
                return res;
            }
        }
        catch (ThreadDeath td) {
            throw td;
        }
        catch (Throwable ioE) {
            // empty catch block
        }
        JOptionPane.showMessageDialog(this, NbBundle.getMessage(TextImporterUI.class, (String)"Err_CannotLoadIconFromFile", (Object)iconFile.getName()), NbBundle.getMessage(TextImporterUI.class, (String)"Err_Title"), 0);
        return null;
    }

}

