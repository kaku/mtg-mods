/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.palette.ui;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import org.netbeans.modules.palette.ui.TextImporterUI;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.xml.XMLUtil;

public class TextImporter
implements Runnable {
    private String text;
    private Lookup category;
    private int dropIndex;
    private FileObject categoryFolder;

    public TextImporter(String text, Lookup category, int dropIndex) {
        this.text = text;
        this.category = category;
        this.dropIndex = dropIndex;
    }

    @Override
    public void run() {
        this.categoryFolder = this.findFolder(this.category);
        if (null == this.categoryFolder) {
            NotifyDescriptor nd = new NotifyDescriptor((Object)NbBundle.getMessage(TextImporter.class, (String)"Err_NoTextDnDSupport"), null, -1, 1, null, (Object)null);
            DialogDisplayer.getDefault().notify(nd);
            return;
        }
        JButton btnOk = new JButton(NbBundle.getMessage(TextImporter.class, (String)"Btn_AddToPalette"));
        btnOk.getAccessibleContext().setAccessibleName(btnOk.getText());
        btnOk.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporter.class, (String)"ACD_Btn_AddToPalette"));
        JButton btnCancel = new JButton(NbBundle.getMessage(TextImporter.class, (String)"Btn_Cancel"));
        btnCancel.getAccessibleContext().setAccessibleName(btnCancel.getText());
        btnCancel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(TextImporter.class, (String)"ACD_Btn_Cancel"));
        final TextImporterUI panel = new TextImporterUI(this.text, btnOk);
        DialogDescriptor dd = new DialogDescriptor((Object)panel, NbBundle.getMessage(TextImporter.class, (String)"Btn_AddToPalette"), true, new Object[]{btnOk, btnCancel}, (Object)new HelpCtx(TextImporter.class), 0, null, null);
        final Dialog dlg = DialogDisplayer.getDefault().createDialog(dd);
        btnCancel.setDefaultCapable(false);
        btnCancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                dlg.dispose();
            }
        });
        btnOk.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                dlg.dispose();
                TextImporter.this.doAddToPalette(panel);
            }
        });
        dlg.setVisible(true);
    }

    private void doAddToPalette(final TextImporterUI panel) {
        final String fileName = FileUtil.findFreeFileName((FileObject)this.categoryFolder, (String)"ccc", (String)"xml");
        try {
            this.categoryFolder.getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    FileObject itemFile = TextImporter.this.categoryFolder.createData(fileName, "xml");
                    PrintWriter w = new PrintWriter(itemFile.getOutputStream());
                    TextImporter.this.storeItem(w, panel);
                    w.close();
                }
            });
        }
        catch (IOException ioE) {
            Logger.getLogger(TextImporter.class.getName()).log(Level.SEVERE, NbBundle.getMessage(TextImporter.class, (String)"Err_StoreItemToDisk"), ioE);
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    TextImporter.this.reorder(fileName, TextImporter.this.categoryFolder, TextImporter.this.dropIndex);
                }
                catch (IOException ioE) {
                    Logger.getLogger(TextImporter.class.getName()).log(Level.INFO, null, ioE);
                }
            }
        });
    }

    private FileObject findFolder(Lookup category) {
        DataFolder df;
        Node n = (Node)category.lookup(Node.class);
        if (null != n && null != (df = (DataFolder)n.getCookie(DataFolder.class))) {
            return df.getPrimaryFile();
        }
        return null;
    }

    private void storeItem(PrintWriter w, TextImporterUI panel) throws IOException {
        String largeIconPath;
        String name = panel.getItemName();
        String tooltip = panel.getItemTooltip();
        if (null == tooltip || tooltip.trim().length() == 0) {
            tooltip = name;
        }
        String content = panel.getItemContent();
        String smallIconPath = panel.getItemSmallIconPath();
        ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (null == smallIconPath) {
            smallIconPath = "org/netbeans/modules/palette/resources/unknown16.gif";
        }
        if (null == (largeIconPath = panel.getItemLargeIconPath())) {
            largeIconPath = "org/netbeans/modules/palette/resources/unknown32.gif";
        }
        w.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        w.println("<!DOCTYPE editor_palette_item PUBLIC \"-//NetBeans//Editor Palette Item 1.1//EN\" \"http://www.netbeans.org/dtds/editor-palette-item-1_1.dtd\">");
        w.println();
        w.println("<editor_palette_item version=\"1.0\">");
        w.println("    <body>");
        w.println("        <![CDATA[");
        w.println(this.toUTF8(content));
        w.println("        ]]>");
        w.println("    </body>");
        w.print("    <icon16 urlvalue=\"");
        w.print(smallIconPath);
        w.println("\" />");
        w.print("    <icon32 urlvalue=\"");
        w.print(largeIconPath);
        w.println("\" />");
        w.println("    <inline-description>");
        w.print("        <display-name>");
        w.print(XMLUtil.toElementContent((String)this.toUTF8(name)));
        w.println("</display-name>");
        w.print("        <tooltip>");
        w.print(XMLUtil.toElementContent((String)this.toUTF8(tooltip)));
        w.println("</tooltip>");
        w.println("    </inline-description>");
        w.println("</editor_palette_item>");
    }

    private String toUTF8(String s) {
        try {
            return new String(s.getBytes("UTF-8"));
        }
        catch (UnsupportedEncodingException e) {
            Logger.getLogger(TextImporter.class.getName()).log(Level.WARNING, null, e);
            return s;
        }
    }

    private void reorder(String fileName, FileObject categoryFolder, int dropIndex) throws IOException {
        if (dropIndex < 0) {
            return;
        }
        FileObject itemFile = categoryFolder.getFileObject(fileName, "xml");
        if (null == itemFile) {
            return;
        }
        DataFolder catDob = DataFolder.findFolder((FileObject)categoryFolder);
        DataObject[] children = catDob.getChildren();
        DataObject dob = DataObject.find((FileObject)itemFile);
        if (null == dob) {
            return;
        }
        int curIndex = -1;
        for (int i = 0; i < children.length; ++i) {
            if (!children[i].equals((Object)dob)) continue;
            curIndex = i;
            break;
        }
        if (curIndex < 0) {
            return;
        }
        DataObject[] sortedChildren = new DataObject[children.length];
        if (dropIndex >= sortedChildren.length) {
            dropIndex = sortedChildren.length - 1;
        }
        sortedChildren[dropIndex] = dob;
        int index = 0;
        for (int i2 = 0; i2 < sortedChildren.length; ++i2) {
            DataObject tmp;
            if (sortedChildren[i2] != null) continue;
            if (dob.equals((Object)(tmp = children[index++]))) {
                --i2;
                continue;
            }
            sortedChildren[i2] = tmp;
        }
        catDob.setOrder(sortedChildren);
    }

}

