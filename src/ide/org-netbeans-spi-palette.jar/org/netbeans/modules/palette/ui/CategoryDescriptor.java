/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.palette.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.CategoryListener;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.netbeans.modules.palette.ui.CategoryButton;
import org.netbeans.modules.palette.ui.CategoryList;
import org.netbeans.modules.palette.ui.PalettePanel;
import org.netbeans.spi.palette.PaletteController;
import org.openide.util.Utilities;

class CategoryDescriptor
implements CategoryListener {
    private PalettePanel palettePanel;
    private Category category;
    private JPanel wholePanel;
    private CategoryButton categoryButton;
    private CategoryList itemsList;
    private DefaultListModel itemsListModel;
    private boolean opened;
    private boolean resetItems = true;
    private Settings settings;
    private String searchString;
    private boolean wasOpened;

    CategoryDescriptor(PalettePanel palettePanel, Category category) {
        assert (palettePanel != null);
        assert (category != null);
        this.palettePanel = palettePanel;
        this.category = category;
        this.settings = palettePanel.getSettings();
        this.wholePanel = new JPanel(){

            @Override
            public void addNotify() {
                super.addNotify();
                CategoryDescriptor.this.category.addCategoryListener(CategoryDescriptor.this);
            }

            @Override
            public void removeNotify() {
                super.removeNotify();
                CategoryDescriptor.this.category.removeCategoryListener(CategoryDescriptor.this);
            }
        };
        this.wholePanel.setLayout(new GridBagLayout());
        this.wholePanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        this.wholePanel.setOpaque(false);
        MouseListener listener = this.createMouseListener();
        this.categoryButton = new CategoryButton(this, category);
        this.categoryButton.addMouseListener(listener);
        Insets insets = UIManager.getLookAndFeelDefaults().getInsets("category-descriptor-Insets");
        if (insets == null) {
            insets = new Insets(0, 0, 0, 0);
        }
        GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, 11, 2, insets, 0, 0);
        this.wholePanel.add((Component)this.categoryButton, gbc);
        this.itemsList = new CategoryList(category, palettePanel);
        this.itemsListModel = new DefaultListModel();
        this.itemsList.setModel(this.itemsListModel);
        this.itemsList.setShowNames(palettePanel.getSettings().getShowItemNames());
        this.itemsList.setIconSize(palettePanel.getSettings().getIconSize());
        this.itemsList.addMouseListener(listener);
        this.itemsList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                CategoryDescriptor.this.palettePanel.select(CategoryDescriptor.this.category, (Item)CategoryDescriptor.this.itemsList.getSelectedValue());
            }
        });
        gbc = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, 10, 1, new Insets(0, 0, 0, 0), 0, 0);
        this.wholePanel.add((Component)this.itemsList, gbc);
        this.wholePanel.setFocusTraversalPolicy(new MyFocusTraversal(this));
        this.doSetOpened(this.settings.isExpanded(category));
    }

    private MouseListener createMouseListener() {
        return new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent event) {
                if (SwingUtilities.isRightMouseButton(event)) {
                    JComponent comp = (JComponent)event.getSource();
                    Item item = null;
                    if (comp instanceof JList) {
                        JList list = (JList)comp;
                        Point p = event.getPoint();
                        int index = list.locationToIndex(p);
                        if (index >= 0 && !list.getCellBounds(index, index).contains(p)) {
                            index = -1;
                        }
                        if (index >= 0) {
                            item = (Item)list.getModel().getElementAt(index);
                        }
                    }
                    Action[] actions = null == item ? CategoryDescriptor.this.category.getActions() : item.getActions();
                    JPopupMenu popup = Utilities.actionsToPopup((Action[])actions, (Component)CategoryDescriptor.this.getComponent());
                    Utils.addCustomizationMenuItems(popup, CategoryDescriptor.this.getPalettePanel().getController(), CategoryDescriptor.this.getPalettePanel().getSettings());
                    popup.show(comp, event.getX(), event.getY());
                }
            }
        };
    }

    void refresh() {
        this.categoryButton.updateProperties();
        this.categoryButton.repaint();
        if (this.isOpened() && this.resetItems) {
            this.computeItems();
        }
    }

    void computeItems() {
        DefaultListModel<Item> newModel = new DefaultListModel<Item>();
        Item[] items = this.category.getItems();
        if (items != null) {
            for (int i = 0; i < items.length; ++i) {
                if (!this.settings.isVisible(items[i]) || null != this.searchString && !items[i].getDisplayName().toLowerCase().contains(this.searchString)) continue;
                newModel.addElement(items[i]);
            }
        }
        this.itemsListModel = newModel;
        this.itemsList.setModel(newModel);
        this.resetItems = false;
    }

    void resetItems() {
        this.resetItems = true;
    }

    Category getCategory() {
        return this.category;
    }

    boolean isOpened() {
        return this.opened;
    }

    boolean isSelected() {
        return this.categoryButton.isFocusOwner() || this.itemsList.getSelectedIndex() >= 0;
    }

    void setSelectedItem(Item item) {
        if (this.itemsList.getSelectedValue() == item) {
            return;
        }
        if (item == null) {
            int selectedIndex = this.itemsList.getSelectedIndex();
            this.itemsList.removeSelectionInterval(selectedIndex, selectedIndex);
        } else {
            this.itemsList.setSelectedValue(item, true);
        }
    }

    void setOpened(boolean b) {
        if (this.opened == b) {
            return;
        }
        this.doSetOpened(b);
        this.settings.setExpanded(this.category, b);
    }

    private void doSetOpened(boolean b) {
        this.opened = b;
        if (this.opened) {
            if (this.resetItems) {
                this.computeItems();
            }
        } else {
            this.palettePanel.select(this.category, null);
        }
        this.itemsList.setVisible(this.opened);
        this.categoryButton.setSelected(this.opened);
        if (this.opened) {
            this.itemsList.ensureIndexIsVisible(0);
        }
    }

    void setPositionY(int yPosition) {
        this.wholePanel.setLocation(0, yPosition);
    }

    JComponent getComponent() {
        return this.wholePanel;
    }

    int getPreferredHeight(int width) {
        return this.isOpened() ? this.itemsList.getPreferredHeight(width) + this.categoryButton.getPreferredSize().height : this.categoryButton.getPreferredSize().height;
    }

    void setWidth(int width) {
        this.wholePanel.setSize(width, this.wholePanel.getHeight());
    }

    void setShowNames(boolean showNames) {
        this.itemsList.setShowNames(showNames);
    }

    void setIconSize(int iconSize) {
        this.itemsList.setIconSize(iconSize);
    }

    void setItemWidth(int itemWidth) {
        this.itemsList.setFixedCellWidth(itemWidth);
    }

    PalettePanel getPalettePanel() {
        return this.palettePanel;
    }

    @Override
    public void categoryModified(Category category) {
        this.resetItems();
        this.palettePanel.refresh();
    }

    CategoryList getList() {
        return this.itemsList;
    }

    CategoryButton getButton() {
        return this.categoryButton;
    }

    boolean match(String searchString) {
        if (null == searchString && this.searchString == null) {
            return true;
        }
        if (null != searchString && searchString.equals(this.searchString)) {
            return this.itemsList.getModel().getSize() > 0;
        }
        if (null == this.searchString && null != searchString) {
            this.wasOpened = this.opened;
        }
        this.searchString = searchString;
        this.computeItems();
        if (null != searchString) {
            this.opened = true;
        } else if (null == searchString && this.searchString != null) {
            this.opened = this.wasOpened;
        }
        this.itemsList.setVisible(this.opened);
        this.categoryButton.setSelected(this.opened);
        return this.itemsList.getModel().getSize() > 0 || null == searchString;
    }

    private static class MyFocusTraversal
    extends FocusTraversalPolicy {
        private CategoryDescriptor descriptor;

        public MyFocusTraversal(CategoryDescriptor descriptor) {
            this.descriptor = descriptor;
        }

        @Override
        public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
            if (focusCycleRoot == this.descriptor.wholePanel && aComponent == this.descriptor.categoryButton) {
                return this.descriptor.itemsList;
            }
            return null;
        }

        @Override
        public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
            if (focusCycleRoot == this.descriptor.wholePanel && aComponent == this.descriptor.itemsList) {
                return this.descriptor.categoryButton;
            }
            return null;
        }

        @Override
        public Component getLastComponent(Container focusCycleRoot) {
            if (focusCycleRoot == this.descriptor.wholePanel) {
                return this.descriptor.itemsList;
            }
            return null;
        }

        @Override
        public Component getFirstComponent(Container focusCycleRoot) {
            if (focusCycleRoot == this.descriptor.wholePanel) {
                return this.descriptor.categoryButton;
            }
            return null;
        }

        @Override
        public Component getDefaultComponent(Container focusCycleRoot) {
            return this.getFirstComponent(focusCycleRoot);
        }
    }

}

