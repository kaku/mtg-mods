/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.util.datatransfer.NewType
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.palette;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.ui.PalettePanel;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;
import org.openide.windows.TopComponent;

public final class Utils {
    private static final Logger ERR = Logger.getLogger("org.netbeans.modules.palette");

    private Utils() {
    }

    public static ResourceBundle getBundle() {
        return NbBundle.getBundle(Utils.class);
    }

    public static String getBundleString(String key) {
        return Utils.getBundle().getString(key);
    }

    public static Action[] mergeActions(Action[] first, Action[] second) {
        if (null == first) {
            return second;
        }
        if (null == second) {
            return first;
        }
        Action[] res = new Action[first.length + second.length + 1];
        System.arraycopy(first, 0, res, 0, first.length);
        res[first.length] = null;
        System.arraycopy(second, 0, res, first.length + 1, second.length);
        return res;
    }

    public static boolean isReadonly(Node node) {
        return Utils.getBoolean(node, "isReadonly", !node.canDestroy());
    }

    public static boolean getBoolean(Node node, String attrName, boolean defaultValue) {
        DataObject dobj;
        Object val = node.getValue(attrName);
        if (null == val && null != (dobj = (DataObject)node.getCookie(DataObject.class))) {
            val = dobj.getPrimaryFile().getAttribute(attrName);
        }
        if (null != val) {
            return Boolean.valueOf(val.toString());
        }
        return defaultValue;
    }

    public static HelpCtx getHelpCtx(Node node, HelpCtx defaultHelp) {
        HelpCtx retValue = defaultHelp;
        if (null == retValue || HelpCtx.DEFAULT_HELP.equals((Object)retValue)) {
            DataObject dobj;
            Object val = node.getValue("helpId");
            if (null == val && null != (dobj = (DataObject)node.getCookie(DataObject.class))) {
                val = dobj.getPrimaryFile().getAttribute("helpId");
            }
            if (null != val) {
                retValue = new HelpCtx(val.toString());
            }
        }
        return retValue;
    }

    public static void addCustomizationMenuItems(JPopupMenu popup, PaletteController controller, Settings settings) {
        popup.addSeparator();
        popup.add(new ShowNamesAction(settings));
        popup.add(new ChangeIconSizeAction(settings));
        Utils.addResetMenuItem(popup, controller, settings);
        popup.addSeparator();
        popup.add(new ShowCustomizerAction(controller));
    }

    static void addResetMenuItem(JPopupMenu popup, final PaletteController controller, final Settings settings) {
        JMenuItem item = new JMenuItem(Utils.getBundleString("CTL_ResetPalettePopup"));
        item.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Utils.resetPalette(controller, settings);
            }
        });
        popup.add(item);
    }

    public static Node findCategoryNode(Node root, String categoryName) {
        return root.getChildren().findChild(categoryName);
    }

    public static void resetPalette(PaletteController controller, Settings settings) {
        Node rootNode = (Node)controller.getRoot().lookup(Node.class);
        if (null != rootNode) {
            PaletteActions customActions = (PaletteActions)rootNode.getLookup().lookup(PaletteActions.class);
            Action resetAction = customActions.getResetAction();
            if (null != resetAction) {
                settings.reset();
                resetAction.actionPerformed(new ActionEvent(controller, 0, "reset"));
                controller.refresh();
            } else {
                Utils.resetPalette(rootNode, controller, settings);
            }
        }
    }

    public static void resetPalette(Node rootNode, PaletteController controller, Settings settings) {
        NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)Utils.getBundleString("MSG_ConfirmPaletteReset"), Utils.getBundleString("CTL_ConfirmResetTitle"), 0);
        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc))) {
            FileObject primaryFile;
            settings.reset();
            DataObject dob = (DataObject)rootNode.getLookup().lookup(DataObject.class);
            if (null != dob && null != (primaryFile = dob.getPrimaryFile()) && primaryFile.isFolder()) {
                try {
                    primaryFile.revert();
                    for (FileObject fo : primaryFile.getChildren()) {
                        fo.setAttribute("categoryName", (Object)null);
                        fo.setAttribute("position", (Object)null);
                    }
                }
                catch (IOException ex) {
                    ERR.log(Level.INFO, null, ex);
                }
            }
            controller.refresh();
        }
    }

    public static void setOpenedByUser(TopComponent tc, boolean userOpened) {
        tc.putClientProperty((Object)"userOpened", (Object)userOpened);
    }

    public static boolean isOpenedByUser(TopComponent tc) {
        Object val = tc.getClientProperty((Object)"userOpened");
        tc.putClientProperty((Object)"userOpened", (Object)null);
        return null != val && val instanceof Boolean && (Boolean)val != false;
    }

    private static class ShowCustomizerAction
    extends AbstractAction {
        private PaletteController palette;

        public ShowCustomizerAction(PaletteController palette) {
            this.palette = palette;
            this.putValue("Name", Utils.getBundleString("CTL_ShowCustomizer"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            this.palette.showCustomizer();
        }
    }

    static class RemoveItemAction
    extends AbstractAction {
        private Node itemNode;

        public RemoveItemAction(Node itemNode) {
            this.itemNode = itemNode;
            this.putValue("Name", Utils.getBundleString("CTL_Delete"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            String message = MessageFormat.format(Utils.getBundleString("FMT_ConfirmBeanDelete"), this.itemNode.getDisplayName());
            NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)message, Utils.getBundleString("CTL_ConfirmBeanTitle"), 0);
            if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc))) {
                try {
                    this.itemNode.destroy();
                }
                catch (IOException e) {
                    ERR.log(Level.INFO, e.getLocalizedMessage(), e);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return this.itemNode.canDestroy();
        }
    }

    public static class CopyItemAction
    extends AbstractAction {
        private Node itemNode;

        public CopyItemAction(Node itemNode) {
            this.itemNode = itemNode;
            this.putValue("Name", Utils.getBundleString("CTL_Copy"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                Transferable trans = this.itemNode.clipboardCopy();
                if (trans != null) {
                    Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(ExClipboard.class);
                    clipboard.setContents(trans, new StringSelection(""));
                }
            }
            catch (IOException e) {
                ERR.log(Level.INFO, e.getLocalizedMessage(), e);
            }
        }

        @Override
        public boolean isEnabled() {
            return this.itemNode.canCopy();
        }
    }

    public static class CutItemAction
    extends AbstractAction {
        private Node itemNode;

        public CutItemAction(Node itemNode) {
            this.itemNode = itemNode;
            this.putValue("Name", Utils.getBundleString("CTL_Cut"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                Transferable trans = this.itemNode.clipboardCut();
                if (trans != null) {
                    Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(ExClipboard.class);
                    clipboard.setContents(trans, new StringSelection(""));
                }
            }
            catch (IOException e) {
                ERR.log(Level.INFO, e.getLocalizedMessage(), e);
            }
        }

        @Override
        public boolean isEnabled() {
            return this.itemNode.canCut();
        }
    }

    public static class PasteItemAction
    extends AbstractAction {
        private Node categoryNode;

        public PasteItemAction(Node categoryNode) {
            this.categoryNode = categoryNode;
            this.putValue("Name", Utils.getBundleString("CTL_Paste"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            PasteType type = this.getPasteType();
            if (type != null) {
                try {
                    Transferable trans = type.paste();
                    if (trans != null) {
                        ClipboardOwner owner = trans instanceof ClipboardOwner ? (ClipboardOwner)((Object)trans) : new StringSelection("");
                        Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(ExClipboard.class);
                        clipboard.setContents(trans, owner);
                    }
                }
                catch (IOException e) {
                    ERR.log(Level.INFO, e.getLocalizedMessage(), e);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return this.getPasteType() != null;
        }

        private PasteType getPasteType() {
            PasteType[] pasteTypes;
            Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(ExClipboard.class);
            Transferable trans = clipboard.getContents(this);
            if (trans != null && (pasteTypes = this.categoryNode.getPasteTypes(trans)) != null && pasteTypes.length != 0) {
                return pasteTypes[0];
            }
            return null;
        }
    }

    static class SortItemsAction
    extends AbstractAction {
        private Node categoryNode;

        public SortItemsAction(Node categoryNode) {
            this.putValue("Name", Utils.getBundleString("CTL_SortItems"));
            this.categoryNode = categoryNode;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            Index order = (Index)this.categoryNode.getCookie(Index.class);
            if (order != null) {
                Node[] nodes = order.getNodes();
                Arrays.sort(nodes, new Comparator<Node>(){

                    @Override
                    public int compare(Node n1, Node n2) {
                        return n1.getDisplayName().compareTo(n2.getDisplayName());
                    }
                });
                int[] perm = new int[nodes.length];
                int i = 0;
                while (i < perm.length) {
                    perm[order.indexOf((Node)nodes[i])] = i++;
                }
                order.reorder(perm);
            }
        }

        @Override
        public boolean isEnabled() {
            return this.categoryNode.getCookie(Index.class) != null;
        }

    }

    static class RenameCategoryAction
    extends AbstractAction {
        private Node categoryNode;

        public RenameCategoryAction(Node categoryNode) {
            this.categoryNode = categoryNode;
            this.putValue("Name", Utils.getBundleString("CTL_RenameCategory"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            NotifyDescriptor.InputLine desc = new NotifyDescriptor.InputLine(Utils.getBundleString("CTL_NewName"), Utils.getBundleString("CTL_Rename"));
            desc.setInputText(this.categoryNode.getDisplayName());
            if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc))) {
                try {
                    String newName = desc.getInputText();
                    if (!"".equals(newName)) {
                        this.categoryNode.setDisplayName(newName);
                    }
                }
                catch (IllegalArgumentException e) {
                    ERR.log(Level.INFO, e.getLocalizedMessage(), e);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return this.categoryNode.canRename();
        }
    }

    static class DeleteCategoryAction
    extends AbstractAction {
        private Node categoryNode;

        public DeleteCategoryAction(Node categoryNode) {
            this.categoryNode = categoryNode;
            this.putValue("Name", Utils.getBundleString("CTL_DeleteCategory"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            String message = MessageFormat.format(Utils.getBundleString("FMT_ConfirmCategoryDelete"), this.categoryNode.getName());
            NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)message, Utils.getBundleString("CTL_ConfirmCategoryTitle"), 0);
            if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc))) {
                try {
                    this.categoryNode.destroy();
                }
                catch (IOException e) {
                    ERR.log(Level.INFO, e.getLocalizedMessage(), e);
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return this.categoryNode.canDestroy();
        }
    }

    static class RefreshPaletteAction
    extends AbstractAction {
        public RefreshPaletteAction() {
            this.putValue("Name", Utils.getBundleString("CTL_RefreshPalette"));
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            PalettePanel.getDefault().doRefresh();
        }
    }

    private static class ChangeIconSizeAction
    extends AbstractAction {
        private Settings settings;

        public ChangeIconSizeAction(Settings settings) {
            this.settings = settings;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            int oldSize = this.settings.getIconSize();
            int newSize = oldSize == 1 ? 2 : 1;
            this.settings.setIconSize(newSize);
        }

        @Override
        public Object getValue(String key) {
            if ("Name".equals(key)) {
                String namePattern = Utils.getBundleString("CTL_IconSize");
                return MessageFormat.format(namePattern, this.settings.getIconSize());
            }
            return super.getValue(key);
        }
    }

    private static class ShowNamesAction
    extends AbstractAction {
        private Settings settings;

        public ShowNamesAction(Settings settings) {
            this.settings = settings;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            this.settings.setShowItemNames(!this.settings.getShowItemNames());
        }

        @Override
        public Object getValue(String key) {
            if ("Name".equals(key)) {
                boolean showNames = this.settings.getShowItemNames();
                return Utils.getBundleString(showNames ? "CTL_HideNames" : "CTL_ShowNames");
            }
            return super.getValue(key);
        }
    }

    static class SortCategoriesAction
    extends AbstractAction {
        private Node paletteNode;

        public SortCategoriesAction(Node paletteNode) {
            this.putValue("Name", Utils.getBundleString("CTL_SortCategories"));
            this.paletteNode = paletteNode;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            Index order = (Index)this.paletteNode.getCookie(Index.class);
            if (order != null) {
                Node[] nodes = order.getNodes();
                Arrays.sort(nodes, new Comparator<Node>(){

                    @Override
                    public int compare(Node n1, Node n2) {
                        return n1.getDisplayName().compareTo(n2.getDisplayName());
                    }
                });
                int[] perm = new int[nodes.length];
                int i = 0;
                while (i < perm.length) {
                    perm[order.indexOf((Node)nodes[i])] = i++;
                }
                order.reorder(perm);
            }
        }

        @Override
        public boolean isEnabled() {
            return this.paletteNode.getCookie(Index.class) != null;
        }

    }

    public static class NewCategoryAction
    extends AbstractAction {
        private Node paletteNode;

        public NewCategoryAction(Node paletteRootNode) {
            this.putValue("Name", Utils.getBundleString("CTL_CreateCategory"));
            this.paletteNode = paletteRootNode;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            NewType[] newTypes = this.paletteNode.getNewTypes();
            try {
                if (null != newTypes && newTypes.length > 0) {
                    newTypes[0].create();
                }
            }
            catch (IOException ioE) {
                ERR.log(Level.INFO, ioE.getLocalizedMessage(), ioE);
            }
        }

        @Override
        public boolean isEnabled() {
            NewType[] newTypes = this.paletteNode.getNewTypes();
            return null != newTypes && newTypes.length > 0;
        }
    }

}

