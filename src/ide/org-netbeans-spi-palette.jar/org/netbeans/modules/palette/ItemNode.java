/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.datatransfer.PasteType
 */
package org.netbeans.modules.palette;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import javax.swing.Action;
import org.netbeans.modules.palette.Utils;
import org.netbeans.spi.palette.DragAndDropHandler;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.PasteType;

class ItemNode
extends FilterNode {
    private Action[] actions;

    public ItemNode(Node originalNode) {
        super(originalNode, FilterNode.Children.LEAF);
    }

    public Action[] getActions(boolean context) {
        PaletteActions customActions;
        if (this.actions == null) {
            Node n = this.getParentNode();
            if (null == n) {
                return new Action[0];
            }
            this.actions = new Action[]{new Utils.CutItemAction((Node)this), new Utils.CopyItemAction((Node)this), new Utils.PasteItemAction(n), null, new Utils.RemoveItemAction((Node)this), null, new Utils.SortItemsAction(n), null, new Utils.RefreshPaletteAction()};
        }
        if (null != (customActions = this.getCustomActions())) {
            return Utils.mergeActions(this.actions, customActions.getCustomItemActions(this.getLookup()));
        }
        return this.actions;
    }

    public Transferable clipboardCut() throws IOException {
        ExTransferable t = ExTransferable.create((Transferable)super.clipboardCut());
        this.customizeTransferable(t);
        t.put(this.createTransferable());
        return t;
    }

    public Transferable clipboardCopy() throws IOException {
        ExTransferable t = ExTransferable.create((Transferable)super.clipboardCopy());
        this.customizeTransferable(t);
        t.put(this.createTransferable());
        return t;
    }

    public PasteType getDropType(Transferable t, int action, int index) {
        return null;
    }

    public Transferable drag() throws IOException {
        ExTransferable t = ExTransferable.create((Transferable)super.drag());
        this.customizeTransferable(t);
        t.put(this.createTransferable());
        return t;
    }

    private ExTransferable.Single createTransferable() {
        final Lookup lkp = this.getLookup();
        return new ExTransferable.Single(PaletteController.ITEM_DATA_FLAVOR){

            public Object getData() {
                return lkp;
            }
        };
    }

    private void customizeTransferable(ExTransferable t) {
        DragAndDropHandler tp = this.getTransferableProvider();
        if (null != tp) {
            tp.customize(t, this.getLookup());
        }
    }

    private PaletteActions getCustomActions() {
        Node root;
        PaletteActions res = null;
        Node category = this.getParentNode();
        if (null != category && null != (root = category.getParentNode())) {
            res = (PaletteActions)root.getLookup().lookup(PaletteActions.class);
        }
        return res;
    }

    private DragAndDropHandler getTransferableProvider() {
        Node root;
        DragAndDropHandler res = null;
        Node category = this.getParentNode();
        if (null != category && null != (root = category.getParentNode())) {
            res = (DragAndDropHandler)root.getLookup().lookup(DragAndDropHandler.class);
        }
        return res;
    }

    public Action getPreferredAction() {
        PaletteActions customActions = this.getCustomActions();
        if (null == customActions) {
            return null;
        }
        return customActions.getPreferredAction(this.getLookup());
    }

    public boolean canDestroy() {
        return !Utils.isReadonly(this.getOriginal());
    }

    Node getOriginalNode() {
        return this.getOriginal();
    }

    public HelpCtx getHelpCtx() {
        return Utils.getHelpCtx((Node)this, super.getHelpCtx());
    }

}

