/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataFolder$Index
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.LookupEvent
 *  org.openide.util.Utilities
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.palette;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.modules.palette.ItemNode;
import org.netbeans.modules.palette.Utils;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.netbeans.spi.palette.PaletteFilter;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.Utilities;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

class CategoryNode
extends FilterNode {
    static final Node.PropertySet[] NO_PROPERTIES = new Node.PropertySet[0];
    static final String CAT_NAME = "categoryName";
    private Action[] actions;

    CategoryNode(Node originalNode, Lookup lkp) {
        this(originalNode, new InstanceContent(), lkp);
    }

    private CategoryNode(Node originalNode, InstanceContent content, Lookup lkp) {
        super(originalNode, (org.openide.nodes.Children)new Children(originalNode, lkp), (Lookup)new ProxyLookup(new Lookup[]{lkp, new AbstractLookup((AbstractLookup.Content)content), originalNode.getLookup()}));
        DataFolder folder = (DataFolder)originalNode.getCookie(DataFolder.class);
        if (null != folder) {
            content.add((Object)new DataFolder.Index(folder, (Node)this));
            FileObject fob = folder.getPrimaryFile();
            Object catName = fob.getAttribute("categoryName");
            if (catName instanceof String) {
                this.setDisplayName((String)catName);
            }
        }
        content.add((Object)this);
    }

    public String getDisplayName() {
        Node origNode;
        DataObject dobj;
        Object catName;
        DataShadow shadow;
        FileObject fob;
        String retValue = null;
        DataFolder folder = (DataFolder)this.getCookie(DataFolder.class);
        if (null != folder && (catName = (fob = folder.getPrimaryFile()).getAttribute("categoryName")) instanceof String) {
            retValue = catName.toString();
        }
        if (null == retValue) {
            retValue = super.getDisplayName();
        }
        if (null != retValue && retValue.indexOf("\u2192") > 0 && null != (shadow = (DataShadow)this.getCookie(DataShadow.class)) && null != (dobj = shadow.getOriginal()) && null != (origNode = dobj.getNodeDelegate()) && null != origNode.getDisplayName()) {
            retValue = origNode.getDisplayName();
        }
        return retValue;
    }

    public void setDisplayName(String displayName) {
        try {
            DataFolder folder = (DataFolder)this.getCookie(DataFolder.class);
            if (null != folder) {
                FileObject fo = folder.getPrimaryFile();
                fo.setAttribute("categoryName", (Object)displayName);
            }
        }
        catch (IOException ex) {
            IllegalArgumentException e = new IllegalArgumentException();
            ErrorManager.getDefault().annotate((Throwable)e, (Throwable)ex);
            throw e;
        }
        super.setDisplayName(displayName);
    }

    public String getShortDescription() {
        return this.getDisplayName();
    }

    public Action[] getActions(boolean context) {
        PaletteActions customActions;
        if (this.actions == null) {
            Node n = this.getParentNode();
            if (null == n) {
                return new Action[0];
            }
            ArrayList<AbstractAction> actionList = new ArrayList<AbstractAction>(12);
            actionList.add(new Utils.PasteItemAction((Node)this));
            actionList.add(null);
            Utils.NewCategoryAction a = new Utils.NewCategoryAction(n);
            if (a.isEnabled()) {
                actionList.add(a);
                actionList.add(null);
            }
            actionList.add(new Utils.DeleteCategoryAction((Node)this));
            actionList.add(new Utils.RenameCategoryAction((Node)this));
            actionList.add(null);
            actionList.add(new Utils.SortItemsAction((Node)this));
            actionList.add(null);
            actionList.add(new Utils.SortCategoriesAction(n));
            actionList.add(null);
            actionList.add(new Utils.RefreshPaletteAction());
            this.actions = actionList.toArray(new Action[actionList.size()]);
        }
        if (null != (customActions = (PaletteActions)this.getParentNode().getLookup().lookup(PaletteActions.class))) {
            return Utils.mergeActions(this.actions, customActions.getCustomCategoryActions(this.getLookup()));
        }
        return this.actions;
    }

    public Node.PropertySet[] getPropertySets() {
        return NO_PROPERTIES;
    }

    public boolean canDestroy() {
        return !Utils.isReadonly(this.getOriginal());
    }

    public HelpCtx getHelpCtx() {
        return Utils.getHelpCtx((Node)this, super.getHelpCtx());
    }

    static boolean checkCategoryName(Node parentNode, String name, Node namedNode) {
        boolean invalid = false;
        if (name == null || "".equals(name)) {
            invalid = true;
        } else {
            int n = name.length();
            for (int i = 0; i < n; ++i) {
                char ch = name.charAt(i);
                if (ch == '.' || ch == ' ' && i + 1 == n) {
                    invalid = true;
                    break;
                }
                if (ch != ' ') break;
            }
        }
        if (invalid) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)MessageFormat.format(Utils.getBundleString("ERR_InvalidName"), name), 1));
            return false;
        }
        Node[] nodes = parentNode.getChildren().getNodes();
        for (int i = 0; i < nodes.length; ++i) {
            if (!name.equals(nodes[i].getName()) || nodes[i] == namedNode) continue;
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)MessageFormat.format(Utils.getBundleString("FMT_CategoryExists"), name), 1));
            return false;
        }
        return true;
    }

    static String convertCategoryToFolderName(FileObject paletteFO, String name, String currentName) {
        int i;
        if (name == null || "".equals(name)) {
            return null;
        }
        int n = name.length();
        StringBuffer nameBuff = new StringBuffer(n);
        char ch = name.charAt(0);
        if (Character.isJavaIdentifierStart(ch)) {
            nameBuff.append(ch);
            i = 1;
        } else {
            nameBuff.append('_');
            i = 0;
        }
        while (i < n) {
            ch = name.charAt(i);
            if (Character.isJavaIdentifierPart(ch)) {
                nameBuff.append(ch);
            }
            ++i;
        }
        String fName = nameBuff.toString();
        if ("_".equals(fName)) {
            fName = "Category";
        }
        if (fName.equals(currentName)) {
            return fName;
        }
        String freeName = null;
        boolean nameOK = false;
        i = 0;
        while (!nameOK) {
            String string = freeName = i > 0 ? fName + "_" + i : fName;
            if (Utilities.isWindows()) {
                nameOK = true;
                Enumeration en = paletteFO.getChildren(false);
                while (en.hasMoreElements()) {
                    FileObject fo = (FileObject)en.nextElement();
                    String fn = fo.getName();
                    String fe = fo.getExt();
                    if (fe != null && !"".equals(fe) || !fn.equalsIgnoreCase(freeName)) continue;
                    nameOK = false;
                    break;
                }
            } else {
                nameOK = paletteFO.getFileObject(freeName) == null;
            }
            ++i;
        }
        return freeName;
    }

    public PasteType getDropType(Transferable t, int action, int index) {
        if (t.isDataFlavorSupported(PaletteController.ITEM_DATA_FLAVOR)) {
            return super.getDropType(t, action, index);
        }
        return null;
    }

    private static class Children
    extends FilterNode.Children {
        private Lookup lkp;
        private PaletteFilter filter;

        public Children(Node original, Lookup lkp) {
            super(original);
            this.lkp = lkp;
            this.filter = (PaletteFilter)lkp.lookup(PaletteFilter.class);
        }

        protected Node copyNode(Node node) {
            return new ItemNode(node);
        }

        protected Node[] createNodes(Node key) {
            if (null == this.filter || this.filter.isValidItem(key.getLookup())) {
                return new Node[]{this.copyNode(key)};
            }
            return null;
        }

        public void resultChanged(LookupEvent ev) {
            Object[] nodes = this.original.getChildren().getNodes();
            List empty = Collections.emptyList();
            this.setKeys(empty);
            this.setKeys(nodes);
        }
    }

}

