/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataFolder$Index
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.NewType
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.palette;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.modules.palette.CategoryNode;
import org.netbeans.modules.palette.Utils;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteFilter;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.NewType;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

public final class RootNode
extends FilterNode {
    static final Node.PropertySet[] NO_PROPERTIES = new Node.PropertySet[0];
    private Action[] actions;

    public RootNode(Node originalRoot, Lookup lkp) {
        this(originalRoot, new InstanceContent(), lkp);
    }

    private RootNode(Node originalRoot, InstanceContent content, Lookup lkp) {
        super(originalRoot, (org.openide.nodes.Children)new Children(originalRoot, lkp), (Lookup)new ProxyLookup(new Lookup[]{lkp, new AbstractLookup((AbstractLookup.Content)content), originalRoot.getLookup()}));
        DataFolder df = (DataFolder)this.getOriginal().getCookie(DataFolder.class);
        if (null != df) {
            content.add((Object)new DataFolder.Index(df, (Node)this));
        }
        content.add((Object)this);
        this.setDisplayName(Utils.getBundleString("CTL_Component_palette"));
    }

    public NewType[] getNewTypes() {
        DataFolder paletteFolder;
        NewType[] res = super.getNewTypes();
        if ((null == res || res.length == 0) && null != (paletteFolder = (DataFolder)this.getCookie(DataFolder.class))) {
            res = new NewType[]{new NewCategory()};
        }
        return res;
    }

    public Action[] getActions(boolean context) {
        PaletteActions customActions;
        if (this.actions == null) {
            ArrayList<AbstractAction> actionList = new ArrayList<AbstractAction>(5);
            Utils.NewCategoryAction a = new Utils.NewCategoryAction((Node)this);
            if (a.isEnabled()) {
                actionList.add(a);
                actionList.add(null);
            }
            actionList.add(new Utils.SortCategoriesAction((Node)this));
            actionList.add(null);
            actionList.add(new Utils.RefreshPaletteAction());
            this.actions = actionList.toArray(new Action[actionList.size()]);
        }
        if (null != (customActions = (PaletteActions)this.getLookup().lookup(PaletteActions.class))) {
            return Utils.mergeActions(this.actions, customActions.getCustomPaletteActions());
        }
        return this.actions;
    }

    public Node.PropertySet[] getPropertySets() {
        return NO_PROPERTIES;
    }

    public PasteType getDropType(Transferable t, int action, int index) {
        return null;
    }

    public void refreshChildren() {
        ((Children)this.getChildren()).refreshNodes();
    }

    void createNewCategory() throws IOException {
        ResourceBundle bundle = Utils.getBundle();
        NotifyDescriptor.InputLine input = new NotifyDescriptor.InputLine(bundle.getString("CTL_NewCategoryName"), bundle.getString("CTL_NewCategoryTitle"));
        input.setInputText(bundle.getString("CTL_NewCategoryValue"));
        while (DialogDisplayer.getDefault().notify((NotifyDescriptor)input) == NotifyDescriptor.OK_OPTION) {
            String categoryName = input.getInputText();
            if (!CategoryNode.checkCategoryName((Node)this, categoryName, null)) continue;
            DataFolder paletteFolder = (DataFolder)this.getCookie(DataFolder.class);
            FileObject parentFolder = paletteFolder.getPrimaryFile();
            String folderName = CategoryNode.convertCategoryToFolderName(parentFolder, categoryName, null);
            FileObject folder = parentFolder.createFolder(folderName);
            if (folderName.equals(categoryName)) break;
            folder.setAttribute("categoryName", (Object)categoryName);
            break;
        }
    }

    public boolean canCut() {
        return false;
    }

    public boolean canDestroy() {
        return false;
    }

    public HelpCtx getHelpCtx() {
        return Utils.getHelpCtx((Node)this, super.getHelpCtx());
    }

    final class NewCategory
    extends NewType {
        NewCategory() {
        }

        public String getName() {
            return Utils.getBundleString("CTL_NewCategory");
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx(NewCategory.class);
        }

        public void create() throws IOException {
            RootNode.this.createNewCategory();
        }
    }

    private static class Children
    extends FilterNode.Children {
        private PaletteFilter filter;
        private Lookup lkp;

        public Children(Node original, Lookup lkp) {
            super(original);
            this.lkp = lkp;
            this.filter = (PaletteFilter)lkp.lookup(PaletteFilter.class);
        }

        protected Node copyNode(Node node) {
            return new CategoryNode(node, this.lkp);
        }

        public int getNodesCount(boolean optimalResult) {
            return this.getNodes(optimalResult).length;
        }

        protected Node[] createNodes(Node key) {
            if (null == this.filter || this.filter.isValidCategory(key.getLookup())) {
                return new Node[]{this.copyNode(key)};
            }
            return null;
        }

        public void refreshNodes() {
            Object[] nodes = this.original.getChildren().getNodes();
            List empty = Collections.emptyList();
            this.setKeys(empty);
            this.setKeys(nodes);
        }
    }

}

