/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.palette;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public interface Item
extends Node.Cookie {
    public String getName();

    public String getDisplayName();

    public String getShortDescription();

    public Image getIcon(int var1);

    public Action[] getActions();

    public void invokePreferredAction(ActionEvent var1);

    public Lookup getLookup();

    public Transferable drag();

    public Transferable cut();
}

