/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.palette;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTargetDragEvent;
import javax.swing.Action;
import org.netbeans.modules.palette.CategoryListener;
import org.netbeans.modules.palette.Item;
import org.openide.util.Lookup;

public interface Category {
    public String getName();

    public String getDisplayName();

    public String getShortDescription();

    public Image getIcon(int var1);

    public Action[] getActions();

    public Item[] getItems();

    public void addCategoryListener(CategoryListener var1);

    public void removeCategoryListener(CategoryListener var1);

    public Transferable getTransferable();

    public Lookup getLookup();

    public boolean dropItem(Transferable var1, int var2, Item var3, boolean var4);

    public boolean dragOver(DropTargetDragEvent var1);
}

