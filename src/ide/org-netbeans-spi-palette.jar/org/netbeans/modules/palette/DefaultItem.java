/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.palette;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.modules.palette.Item;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class DefaultItem
implements Item {
    private Node itemNode;

    public DefaultItem(Node itemNode) {
        this.itemNode = itemNode;
    }

    @Override
    public String getName() {
        return this.itemNode.getName();
    }

    @Override
    public Image getIcon(int type) {
        return this.itemNode.getIcon(type);
    }

    @Override
    public Action[] getActions() {
        return this.itemNode.getActions(false);
    }

    @Override
    public String getShortDescription() {
        return this.itemNode.getShortDescription();
    }

    @Override
    public String getDisplayName() {
        return this.itemNode.getDisplayName();
    }

    @Override
    public void invokePreferredAction(ActionEvent e) {
        Action action = this.itemNode.getPreferredAction();
        if (null != action && action.isEnabled()) {
            action.actionPerformed(e);
        }
    }

    @Override
    public Lookup getLookup() {
        return this.itemNode.getLookup();
    }

    public boolean equals(Object obj) {
        if (null == obj || !(obj instanceof DefaultItem)) {
            return false;
        }
        return this.itemNode.equals((Object)((DefaultItem)obj).itemNode);
    }

    @Override
    public Transferable drag() {
        try {
            return this.itemNode.drag();
        }
        catch (IOException ioE) {
            Logger.getLogger(DefaultItem.class.getName()).log(Level.INFO, null, ioE);
            return null;
        }
    }

    @Override
    public Transferable cut() {
        try {
            return this.itemNode.clipboardCut();
        }
        catch (IOException ioE) {
            Logger.getLogger(DefaultItem.class.getName()).log(Level.INFO, null, ioE);
            return null;
        }
    }

    public String toString() {
        return this.itemNode.getDisplayName();
    }
}

