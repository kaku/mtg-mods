/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.ActiveEditorDrop
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.netbeans.modules.palette;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.openide.text.ActiveEditorDrop;
import org.openide.util.lookup.InstanceContent;

class ActiveEditorDropDefaultProvider
implements InstanceContent.Convertor<String, ActiveEditorDrop> {
    private static ActiveEditorDropDefaultProvider instance = new ActiveEditorDropDefaultProvider();

    private ActiveEditorDropDefaultProvider() {
    }

    static ActiveEditorDropDefaultProvider getInstance() {
        return instance;
    }

    public Class<? extends ActiveEditorDrop> type(String obj) {
        return ActiveEditorDrop.class;
    }

    public String id(String obj) {
        return obj;
    }

    public String displayName(String obj) {
        return obj;
    }

    public ActiveEditorDrop convert(String obj) {
        return this.getActiveEditorDrop(obj);
    }

    private ActiveEditorDrop getActiveEditorDrop(String body) {
        return new ActiveEditorDropDefault(body);
    }

    private static class ActiveEditorDropDefault
    implements ActiveEditorDrop {
        String body;

        public ActiveEditorDropDefault(String body) {
            this.body = body;
        }

        public boolean handleTransfer(JTextComponent targetComponent) {
            if (targetComponent == null) {
                return false;
            }
            try {
                Document doc = targetComponent.getDocument();
                Caret caret = targetComponent.getCaret();
                int p0 = Math.min(caret.getDot(), caret.getMark());
                int p1 = Math.max(caret.getDot(), caret.getMark());
                doc.remove(p0, p1 - p0);
                int start = caret.getDot();
                doc.insertString(start, this.body, null);
            }
            catch (BadLocationException ble) {
                return false;
            }
            return true;
        }
    }

}

