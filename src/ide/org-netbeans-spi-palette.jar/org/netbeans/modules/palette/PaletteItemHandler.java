/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.palette;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.NbBundle;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public final class PaletteItemHandler
extends DefaultHandler {
    private static final String XML_ROOT = "editor_palette_item";
    private static final String ATTR_VERSION = "version";
    private static final String TAG_BODY = "body";
    private static final String TAG_CLASS = "class";
    private static final String ATTR_CLASSNAME = "name";
    private static final String TAG_CUSTOMIZER = "customizer";
    private static final String ATTR_CUSTNAME = "name";
    private static final String TAG_ICON16 = "icon16";
    private static final String ATTR_URL = "urlvalue";
    private static final String TAG_ICON32 = "icon32";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_INLINE_DESCRIPTION = "inline-description";
    private static final String TAG_DISPLAY_NAME = "display-name";
    private static final String TAG_TOOLTIP = "tooltip";
    private static final String ATTR_BUNDLE = "localizing-bundle";
    private static final String ATTR_DISPLAY_NAME_KEY = "display-name-key";
    private static final String ATTR_TOOLTIP_KEY = "tooltip-key";
    private LinkedList<String> bodyLines;
    private boolean insideBody = false;
    private String body;
    private String className;
    private String icon16URL;
    private String icon32URL;
    private String bundleName;
    private String displayNameKey;
    private String tooltipKey;
    private String displayName;
    private String tooltip;
    private StringBuffer textBuffer;
    private boolean error = false;

    public String getBody() {
        return this.body;
    }

    public String getClassName() {
        return this.className;
    }

    public String getIcon16URL() {
        return this.icon16URL;
    }

    public String getIcon32URL() {
        return this.icon32URL;
    }

    public String getBundleName() {
        return this.bundleName;
    }

    public String getDisplayNameKey() {
        return this.displayNameKey;
    }

    public String getTooltipKey() {
        return this.tooltipKey;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getTooltip() {
        return this.tooltip;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ("editor_palette_item".equals(qName)) {
            String version = attributes.getValue("version");
            if (version == null) {
                String message = NbBundle.getBundle(PaletteItemHandler.class).getString("MSG_UnknownEditorPaletteItemVersion");
                throw new SAXException(message);
            }
            if (!"1.0".equals(version) && !"1.1".equals(version)) {
                String message = NbBundle.getBundle(PaletteItemHandler.class).getString("MSG_UnsupportedEditorPaletteItemVersion");
                throw new SAXException(message);
            }
        } else if ("body".equals(qName)) {
            this.bodyLines = new LinkedList();
            this.insideBody = true;
        } else if ("class".equals(qName)) {
            this.className = attributes.getValue("name");
        } else if ("icon16".equals(qName)) {
            this.icon16URL = attributes.getValue("urlvalue");
        } else if ("icon32".equals(qName)) {
            this.icon32URL = attributes.getValue("urlvalue");
        } else if ("description".equals(qName)) {
            this.bundleName = attributes.getValue("localizing-bundle");
            this.displayNameKey = attributes.getValue("display-name-key");
            this.tooltipKey = attributes.getValue("tooltip-key");
        } else if ("inline-description".equals(qName)) {
            this.bundleName = null;
            this.displayNameKey = null;
            this.tooltipKey = null;
        } else if ("display-name".equals(qName)) {
            this.textBuffer = new StringBuffer();
        } else if ("tooltip".equals(qName)) {
            this.textBuffer = new StringBuffer();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("body".equals(qName)) {
            this.insideBody = false;
            this.body = this.trimSurroundingLines(this.bodyLines);
        } else if ("display-name".equals(qName)) {
            this.displayName = this.textBuffer.toString();
            this.textBuffer = null;
        } else if ("tooltip".equals(qName)) {
            this.tooltip = this.textBuffer.toString();
            this.textBuffer = null;
        }
    }

    @Override
    public void characters(char[] buf, int offset, int len) throws SAXException {
        if (this.insideBody) {
            String chars = new String(buf, offset, len).trim();
            this.bodyLines.add(chars + "\n");
        } else if (null != this.textBuffer) {
            this.textBuffer.append(buf, offset, len);
        }
    }

    private String trimSurroundingLines(List<String> lines) {
        int nlines;
        int firstNonEmpty = nlines = lines.size();
        for (int i = 0; i < firstNonEmpty; ++i) {
            String line = lines.get(i);
            if (line.trim().length() == 0) continue;
            firstNonEmpty = i;
        }
        int lastNonEmpty = -1;
        for (int i2 = nlines - 1; i2 > lastNonEmpty; --i2) {
            String line = lines.get(i2);
            if (line.trim().length() == 0) continue;
            lastNonEmpty = i2;
        }
        StringBuffer sb = new StringBuffer();
        for (int i3 = firstNonEmpty; i3 <= lastNonEmpty; ++i3) {
            sb.append(lines.get(i3));
        }
        String theBody = sb.toString();
        if (theBody.length() > 0 && theBody.charAt(theBody.length() - 1) == '\n') {
            theBody = theBody.substring(0, theBody.length() - 1);
        }
        return theBody;
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        this.error = true;
        Logger.getLogger(PaletteItemHandler.class.getName()).log(Level.INFO, null, e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        this.error = true;
        Logger.getLogger(PaletteItemHandler.class.getName()).log(Level.INFO, null, e);
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        Logger.getLogger(PaletteItemHandler.class.getName()).log(Level.FINE, null, e);
    }

    boolean isError() {
        return this.error;
    }
}

