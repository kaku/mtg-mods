/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.palette;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.palette.Utils;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class ShowPaletteAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent evt) {
        TopComponent palette = WindowManager.getDefault().findTopComponent("CommonPalette");
        if (null == palette) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Cannot find CommonPalette component.");
            return;
        }
        Utils.setOpenedByUser(palette, true);
        palette.open();
        palette.requestActive();
    }
}

