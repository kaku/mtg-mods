/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.palette;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTargetDragEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.CategoryListener;
import org.netbeans.modules.palette.DefaultItem;
import org.netbeans.modules.palette.DefaultModel;
import org.netbeans.modules.palette.Item;
import org.netbeans.spi.palette.DragAndDropHandler;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Lookup;

public class DefaultCategory
implements Category,
NodeListener {
    private Node categoryNode;
    private ArrayList<CategoryListener> categoryListeners = new ArrayList(3);
    private Item[] items;

    public DefaultCategory(Node categoryNode) {
        this.categoryNode = categoryNode;
        this.categoryNode.addNodeListener((NodeListener)this);
        this.categoryNode.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DefaultCategory.this.notifyListeners();
            }
        });
    }

    @Override
    public Image getIcon(int type) {
        return this.categoryNode.getIcon(type);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addCategoryListener(CategoryListener listener) {
        ArrayList<CategoryListener> arrayList = this.categoryListeners;
        synchronized (arrayList) {
            this.categoryListeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeCategoryListener(CategoryListener listener) {
        ArrayList<CategoryListener> arrayList = this.categoryListeners;
        synchronized (arrayList) {
            this.categoryListeners.remove(listener);
        }
    }

    @Override
    public Action[] getActions() {
        return this.categoryNode.getActions(false);
    }

    @Override
    public String getShortDescription() {
        return this.categoryNode.getShortDescription();
    }

    @Override
    public Item[] getItems() {
        if (null == this.items) {
            Node[] children = this.categoryNode.getChildren().getNodes(DefaultModel.canBlock());
            Item[] newItems = new Item[children.length];
            for (int i = 0; i < children.length; ++i) {
                newItems[i] = new DefaultItem(children[i]);
            }
            this.items = newItems;
        }
        return this.items;
    }

    @Override
    public String getName() {
        return this.categoryNode.getName();
    }

    @Override
    public String getDisplayName() {
        return this.categoryNode.getDisplayName();
    }

    protected void notifyListeners() {
        SwingUtilities.invokeLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                CategoryListener[] listeners;
                ArrayList arrayList = DefaultCategory.this.categoryListeners;
                synchronized (arrayList) {
                    listeners = new CategoryListener[DefaultCategory.this.categoryListeners.size()];
                    listeners = DefaultCategory.this.categoryListeners.toArray(listeners);
                }
                for (int i = 0; i < listeners.length; ++i) {
                    listeners[i].categoryModified(DefaultCategory.this);
                }
            }
        });
    }

    public synchronized void childrenAdded(NodeMemberEvent ev) {
        this.items = null;
        this.notifyListeners();
    }

    public synchronized void childrenRemoved(NodeMemberEvent ev) {
        this.items = null;
        this.notifyListeners();
    }

    public synchronized void childrenReordered(NodeReorderEvent ev) {
        this.items = null;
        this.notifyListeners();
    }

    public synchronized void nodeDestroyed(NodeEvent ev) {
        this.categoryNode.removeNodeListener((NodeListener)this);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if ("displayName".equals(evt.getPropertyName())) {
            this.notifyListeners();
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DefaultCategory)) {
            return false;
        }
        return this.categoryNode.equals((Object)((DefaultCategory)obj).categoryNode);
    }

    @Override
    public Transferable getTransferable() {
        try {
            return this.categoryNode.drag();
        }
        catch (IOException ioE) {
            Logger.getLogger(DefaultCategory.class.getName()).log(Level.INFO, null, ioE);
            return null;
        }
    }

    @Override
    public Lookup getLookup() {
        return this.categoryNode.getLookup();
    }

    private int itemToIndex(Item item) {
        Index order;
        if (null == item) {
            return -1;
        }
        Node node = (Node)item.getLookup().lookup(Node.class);
        if (null != node && null != (order = (Index)this.categoryNode.getCookie(Index.class))) {
            return order.indexOf(node);
        }
        return -1;
    }

    @Override
    public boolean dragOver(DropTargetDragEvent e) {
        DragAndDropHandler handler = this.getDragAndDropHandler();
        return handler.canDrop(this.getLookup(), e.getCurrentDataFlavors(), e.getDropAction());
    }

    @Override
    public boolean dropItem(Transferable dropItem, int dndAction, Item target, boolean dropBefore) {
        int targetIndex = this.itemToIndex(target);
        if (!dropBefore) {
            ++targetIndex;
        }
        DragAndDropHandler handler = this.getDragAndDropHandler();
        boolean res = handler.doDrop(this.getLookup(), dropItem, dndAction, targetIndex);
        this.items = null;
        return res;
    }

    private DragAndDropHandler getDragAndDropHandler() {
        return (DragAndDropHandler)this.categoryNode.getLookup().lookup(DragAndDropHandler.class);
    }

    public String toString() {
        return this.categoryNode.getDisplayName();
    }

}

