/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.palette;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.CategoryListener;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.ModelListener;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.Utils;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public final class DefaultSettings
implements Settings,
ModelListener,
CategoryListener {
    private static final RequestProcessor RP = new RequestProcessor("PaletteSettings", 1);
    private static final String NODE_ATTR_PREFIX = "psa_";
    private static final String NULL_VALUE = "null";
    private static final String[] KNOWN_PROPERTIES = new String[]{"psa_iconSize", "psa_isExpanded", "psa_isVisible", "psa_showItemNames"};
    private Model model;
    private PropertyChangeSupport propertySupport;
    private String prefsName;
    private static final Logger ERR = Logger.getLogger("org.netbeans.modules.palette");
    private boolean isLoading;

    public DefaultSettings(Model model) {
        this.propertySupport = new PropertyChangeSupport(this);
        this.isLoading = false;
        this.model = model;
        this.prefsName = this.constructPrefsFileName(model);
        if (Utilities.isWindows()) {
            this.prefsName = this.prefsName.toLowerCase();
        }
        model.addModelListener(this);
        Category[] categories = model.getCategories();
        for (int i = 0; i < categories.length; ++i) {
            categories[i].addCategoryListener(this);
        }
        this.load();
    }

    private String constructPrefsFileName(Model model) {
        FileObject fo;
        DataFolder dof = (DataFolder)model.getRoot().lookup(DataFolder.class);
        if (null != dof && null != (fo = dof.getPrimaryFile())) {
            return fo.getPath();
        }
        return model.getName();
    }

    private Preferences getPreferences() {
        return NbPreferences.forModule(DefaultSettings.class).node("CommonPaletteSettings").node(this.prefsName);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.propertySupport.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.propertySupport.removePropertyChangeListener(l);
    }

    @Override
    public boolean isVisible(Item item) {
        return DefaultSettings._isVisible(item);
    }

    private static boolean _isVisible(Item item) {
        Node node = DefaultSettings.getNode(item.getLookup());
        return DefaultSettings.get(node, "isVisible", true);
    }

    @Override
    public void setVisible(Item item, boolean visible) {
        Node node = DefaultSettings.getNode(item.getLookup());
        this.set(node, "isVisible", visible, true);
    }

    @Override
    public boolean isVisible(Category category) {
        return DefaultSettings._isVisible(category);
    }

    private static boolean _isVisible(Category category) {
        Node node = DefaultSettings.getNode(category.getLookup());
        return DefaultSettings.get(node, "isVisible", true);
    }

    @Override
    public void setVisible(Category category, boolean visible) {
        Node node = DefaultSettings.getNode(category.getLookup());
        this.set(node, "isVisible", visible, true);
    }

    public boolean isNodeVisible(Node node) {
        return DefaultSettings.get(node, "isVisible", true);
    }

    public void setNodeVisible(Node node, boolean visible) {
        this.set(node, "isVisible", visible, true);
    }

    @Override
    public boolean isExpanded(Category category) {
        return DefaultSettings._isExpanded(category);
    }

    private static boolean _isExpanded(Category category) {
        Node node = DefaultSettings.getNode(category.getLookup());
        return DefaultSettings.get(node, "isExpanded", false);
    }

    @Override
    public void setExpanded(Category category, boolean expanded) {
        Node node = DefaultSettings.getNode(category.getLookup());
        this.set(node, "isExpanded", expanded, false);
    }

    @Override
    public int getIconSize() {
        return DefaultSettings._getIconSize(this.model);
    }

    private static int _getIconSize(Model model) {
        Node node = DefaultSettings.getNode(model.getRoot());
        return DefaultSettings.get(node, "iconSize", 1);
    }

    @Override
    public void setIconSize(int iconSize) {
        Node node = DefaultSettings.getNode(this.model.getRoot());
        this.set(node, "iconSize", iconSize, 1);
    }

    @Override
    public void setShowItemNames(boolean showNames) {
        Node node = DefaultSettings.getNode(this.model.getRoot());
        this.set(node, "showItemNames", showNames, true);
    }

    @Override
    public boolean getShowItemNames() {
        return DefaultSettings._getShowItemNames(this.model);
    }

    private static boolean _getShowItemNames(Model model) {
        Node node = DefaultSettings.getNode(model.getRoot());
        return DefaultSettings.get(node, "showItemNames", true);
    }

    private static Node getNode(Lookup lkp) {
        return (Node)lkp.lookup(Node.class);
    }

    private static boolean get(Node node, String attrName, boolean defaultValue) {
        Object value = DefaultSettings.get(node, attrName, (Object)defaultValue);
        return null == value ? defaultValue : Boolean.valueOf(value.toString());
    }

    private static int get(Node node, String attrName, int defaultValue) {
        Object value = DefaultSettings.get(node, attrName, (Object)defaultValue);
        try {
            if (null != value) {
                return Integer.parseInt(value.toString());
            }
        }
        catch (NumberFormatException nfE) {
            // empty catch block
        }
        return defaultValue;
    }

    private static Object get(Node node, String attrName, Object defaultValue) {
        Object res = null;
        if (null != node && (null == (res = node.getValue("psa_" + attrName)) || "null".equals(res))) {
            res = DefaultSettings.getNodeDefaultValue(node, attrName);
        }
        if (null == res) {
            res = defaultValue;
        }
        return res;
    }

    private static Object getNodeDefaultValue(Node node, String attrName) {
        DataObject dobj;
        Object res = node.getValue(attrName);
        if (null == res && null != (dobj = (DataObject)node.getCookie(DataObject.class))) {
            res = dobj.getPrimaryFile().getAttribute(attrName);
        }
        return res;
    }

    private void set(Node node, String attrName, boolean newValue, boolean defaultValue) {
        this.set(node, attrName, (Object)newValue, (Object)defaultValue);
    }

    private void set(Node node, String attrName, int newValue, int defaultValue) {
        this.set(node, attrName, (Object)newValue, (Object)defaultValue);
    }

    private void set(Node node, String attrName, Object newValue, Object defaultValue) {
        if (null == node) {
            return;
        }
        Object oldValue = DefaultSettings.get(node, attrName, defaultValue);
        if (oldValue.equals(newValue)) {
            return;
        }
        node.setValue("psa_" + attrName, newValue);
        this.store();
        this.propertySupport.firePropertyChange(attrName, oldValue, newValue);
    }

    @Override
    public void categoryModified(Category src) {
        this.store();
    }

    @Override
    public void categoriesRemoved(Category[] removedCategories) {
        for (int i = 0; i < removedCategories.length; ++i) {
            removedCategories[i].removeCategoryListener(this);
        }
        this.store();
    }

    @Override
    public void categoriesAdded(Category[] addedCategories) {
        for (int i = 0; i < addedCategories.length; ++i) {
            addedCategories[i].addCategoryListener(this);
        }
        this.store();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
    }

    @Override
    public void categoriesReordered() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void load() {
        try {
            this.isLoading = true;
            Preferences pref = this.getPreferences();
            this.setIconSize(pref.getInt("iconSize", this.getIconSize()));
            this.setShowItemNames(pref.getBoolean("showItemNames", this.getShowItemNames()));
            for (Category category : this.model.getCategories()) {
                this.setVisible(category, pref.getBoolean(category.getName() + '-' + "isVisible", this.isVisible(category)));
                this.setExpanded(category, pref.getBoolean(category.getName() + '-' + "isExpanded", this.isExpanded(category)));
                for (Item item : category.getItems()) {
                    this.setVisible(item, pref.getBoolean(category.getName() + '-' + item.getName() + '-' + "isVisible", this.isVisible(item)));
                }
            }
        }
        finally {
            this.isLoading = false;
        }
    }

    private void store() {
        if (this.isLoading) {
            return;
        }
        Preferences pref = this.getPreferences();
        DefaultSettings._store(pref, this.model);
    }

    private static void _store(final Preferences pref, final Model model) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                try {
                    pref.clear();
                }
                catch (BackingStoreException bsE) {
                    ERR.log(Level.INFO, Utils.getBundleString("Err_StoreSettings"), bsE);
                }
                pref.putInt("iconSize", DefaultSettings._getIconSize(model));
                pref.putBoolean("showItemNames", DefaultSettings._getShowItemNames(model));
                for (Category category : model.getCategories()) {
                    pref.putBoolean(category.getName() + '-' + "isVisible", DefaultSettings._isVisible(category));
                    pref.putBoolean(category.getName() + '-' + "isExpanded", DefaultSettings._isExpanded(category));
                    for (Item item : category.getItems()) {
                        pref.putBoolean(category.getName() + '-' + item.getName() + '-' + "isVisible", DefaultSettings._isVisible(item));
                    }
                }
            }
        });
    }

    @Override
    public int getItemWidth() {
        Node node = DefaultSettings.getNode(this.model.getRoot());
        return DefaultSettings.get(node, "itemWidth", -1);
    }

    @Override
    public void reset() {
        Node root = (Node)this.model.getRoot().lookup(Node.class);
        this.clearAttributes(root);
        Category[] categories = this.model.getCategories();
        for (int i = 0; i < categories.length; ++i) {
            Node cat = (Node)categories[i].getLookup().lookup(Node.class);
            this.clearAttributes(cat);
            Item[] items = categories[i].getItems();
            for (int j = 0; j < items.length; ++j) {
                Node it = (Node)items[j].getLookup().lookup(Node.class);
                this.clearAttributes(it);
            }
        }
        try {
            this.getPreferences().removeNode();
        }
        catch (BackingStoreException bsE) {
            ERR.log(Level.INFO, Utils.getBundleString("Err_StoreSettings"), bsE);
        }
    }

    private void clearAttributes(Node node) {
        for (int i = 0; i < KNOWN_PROPERTIES.length; ++i) {
            node.setValue(KNOWN_PROPERTIES[i], (Object)"null");
        }
    }

}

