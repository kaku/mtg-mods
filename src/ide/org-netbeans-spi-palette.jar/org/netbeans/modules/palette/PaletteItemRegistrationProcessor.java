/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 *  org.openide.text.ActiveEditorDrop
 */
package org.netbeans.modules.palette;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.FileObject;
import org.netbeans.spi.palette.PaletteItemRegistration;
import org.netbeans.spi.palette.PaletteItemRegistrations;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.text.ActiveEditorDrop;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
@SupportedAnnotationTypes(value={"org.netbeans.spi.palette.PaletteItemRegistration", "org.netbeans.spi.palette.PaletteItemRegistrations"})
public final class PaletteItemRegistrationProcessor
extends LayerGeneratingProcessor {
    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(PaletteItemRegistration.class)) {
            PaletteItemRegistration pir = e2.getAnnotation(PaletteItemRegistration.class);
            if (pir == null) continue;
            this.process(e2, pir);
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(PaletteItemRegistrations.class)) {
            PaletteItemRegistrations dfrr = e.getAnnotation(PaletteItemRegistrations.class);
            if (dfrr == null) continue;
            for (PaletteItemRegistration t : dfrr.value()) {
                this.process(e, t);
            }
        }
        return true;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void process(Element e, PaletteItemRegistration pir) throws LayerGenerationException {
        LayerBuilder builder = this.layer(new Element[]{e});
        TypeMirror activeEditorDrop = this.type(ActiveEditorDrop.class);
        LayerBuilder.File f = builder.file(pir.paletteid() + "/" + pir.category() + "/" + pir.itemid() + ".xml");
        StringBuilder paletteFile = new StringBuilder();
        paletteFile.append("<!DOCTYPE editor_palette_item PUBLIC '-//NetBeans//Editor Palette Item 1.1//EN' 'http://www.netbeans.org/dtds/editor-palette-item-1_1.dtd'>\n");
        paletteFile.append("<editor_palette_item version=\"1.1\">\n");
        if (pir.body().isEmpty()) {
            if (e.getKind() != ElementKind.CLASS || !this.isAssignable(e.asType(), activeEditorDrop)) throw new LayerGenerationException("Class annotated with @PaletteItemRegistration has to implements ActiveEditorDrop", e);
            String className = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString();
            paletteFile.append(" <class name=\"").append(className).append("\"/>\n");
        } else {
            paletteFile.append("<body>  <![CDATA[");
            paletteFile.append(pir.body());
            paletteFile.append("]]> </body>\n");
        }
        if (pir.icon16().isEmpty()) {
            throw new LayerGenerationException("Icon 16 must be set ", e);
        }
        builder.validateResource(pir.icon16(), e, (Annotation)pir, "icon16", true);
        paletteFile.append("<icon16 urlvalue=\"").append(pir.icon16()).append("\" />\n");
        if (pir.icon32().isEmpty()) {
            throw new LayerGenerationException("Icon 32 must be set ", e);
        }
        builder.validateResource(pir.icon32(), e, (Annotation)pir, "icon32", true);
        paletteFile.append("<icon32 urlvalue=\"").append(pir.icon32()).append("\" />\n");
        paletteFile.append("<inline-description>");
        paletteFile.append("<display-name>");
        paletteFile.append(pir.name());
        paletteFile.append("</display-name>");
        paletteFile.append("<tooltip> <![CDATA[ ");
        paletteFile.append(pir.tooltip());
        paletteFile.append("]]></tooltip>");
        paletteFile.append("</inline-description>");
        paletteFile.append("</editor_palette_item>");
        f.contents(paletteFile.toString());
        f.write();
    }

    private boolean isAssignable(TypeMirror first, TypeMirror snd) {
        if (snd == null) {
            return false;
        }
        return this.processingEnv.getTypeUtils().isAssignable(first, snd);
    }

    private TypeMirror type(Class<?> type) {
        TypeElement e = this.processingEnv.getElementUtils().getTypeElement(type.getCanonicalName());
        return e == null ? null : e.asType();
    }
}

