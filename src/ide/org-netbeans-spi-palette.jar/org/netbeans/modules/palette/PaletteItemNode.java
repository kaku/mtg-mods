/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.FilterNode$Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.text.ActiveEditorDrop
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.palette;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.text.ActiveEditorDrop;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

public final class PaletteItemNode
extends FilterNode
implements Node.Cookie {
    private static final Node.PropertySet[] NO_PROPERTIES = new Node.PropertySet[0];
    private String name;
    private String bundleName;
    private String displayNameKey;
    private String className;
    private String tooltipKey;
    private String icon16URL;
    private String icon32URL;
    private String displayName;
    private String description;
    private Image icon16;
    private Image icon32;
    private DataObject originalDO;

    PaletteItemNode(DataNode original, String name, String bundleName, String displayNameKey, String className, String tooltipKey, String icon16URL, String icon32URL, InstanceContent content) {
        super((Node)original, FilterNode.Children.LEAF, (Lookup)new ProxyLookup(new Lookup[]{new AbstractLookup((AbstractLookup.Content)content), original.getLookup()}));
        content.add((Object)this);
        this.name = name;
        this.bundleName = bundleName;
        this.displayNameKey = displayNameKey;
        this.className = className;
        this.tooltipKey = tooltipKey;
        this.icon16URL = icon16URL;
        this.icon32URL = icon32URL;
        this.originalDO = (DataObject)original.getLookup().lookup(DataObject.class);
    }

    PaletteItemNode(DataNode original, String name, String displayName, String tooltip, String icon16URL, String icon32URL, InstanceContent content) {
        super((Node)original, FilterNode.Children.LEAF, (Lookup)new ProxyLookup(new Lookup[]{new AbstractLookup((AbstractLookup.Content)content), original.getLookup()}));
        content.add((Object)this);
        this.name = name;
        assert (null != displayName);
        this.displayName = displayName;
        this.description = tooltip;
        if (null == this.description) {
            this.description = displayName;
        }
        this.icon16URL = icon16URL;
        this.icon32URL = icon32URL;
    }

    public String getName() {
        return this.name;
    }

    public String getDisplayName() {
        if (this.displayName == null) {
            this.displayName = this._getDisplayName(this.bundleName, this.displayNameKey, this.className);
        }
        return this.displayName;
    }

    public String getShortDescription() {
        if (this.description == null) {
            this.description = this._getShortDescription(this.bundleName, this.tooltipKey, this.className, this.displayNameKey);
        }
        return this.description;
    }

    public Image getIcon(int type) {
        Image icon = null;
        if (type == 1 || type == 3) {
            if (this.icon16 == null) {
                this.icon16 = this._getIcon(type, this.icon16URL);
                if (this.icon16 == null) {
                    this.icon16 = ImageUtilities.loadImage((String)"org/netbeans/modules/palette/resources/unknown16.gif");
                }
            }
            icon = this.icon16;
        } else if (type == 2 || type == 4) {
            if (this.icon32 == null) {
                this.icon32 = this._getIcon(type, this.icon32URL);
                if (this.icon32 == null) {
                    this.icon32 = ImageUtilities.loadImage((String)"org/netbeans/modules/palette/resources/unknown32.gif");
                }
            }
            icon = this.icon32;
        }
        return icon;
    }

    public boolean canRename() {
        return false;
    }

    public Node.PropertySet[] getPropertySets() {
        return NO_PROPERTIES;
    }

    public Transferable clipboardCopy() throws IOException {
        ExTransferable t = ExTransferable.create((Transferable)super.clipboardCopy());
        Lookup lookup = this.getLookup();
        ActiveEditorDrop drop = (ActiveEditorDrop)lookup.lookup(ActiveEditorDrop.class);
        ActiveEditorDropTransferable s = new ActiveEditorDropTransferable(drop);
        t.put((ExTransferable.Single)s);
        return new NoExternalDndTransferable((Transferable)t);
    }

    public Transferable drag() throws IOException {
        return this.clipboardCopy();
    }

    public String _getDisplayName(String bundleName, String displayNameKey, String instanceName) {
        String dName = null;
        try {
            dName = NbBundle.getBundle((String)bundleName).getString(displayNameKey);
            if (dName == null && displayNameKey != null) {
                dName = displayNameKey;
            }
            if (dName == null && instanceName != null && instanceName.trim().length() > 0) {
                int dotIndex = instanceName.lastIndexOf(46);
                dName = instanceName.substring(dotIndex);
            }
            if (dName == null) {
                dName = this.name;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, ex);
            dName = this.getOriginal().getDisplayName();
        }
        return dName == null ? "" : dName;
    }

    public String _getShortDescription(String bundleName, String tooltipKey, String instanceName, String displayNameKey) {
        String tooltip = null;
        try {
            tooltip = NbBundle.getBundle((String)bundleName).getString(tooltipKey);
            if (tooltip == null && tooltipKey != null) {
                tooltip = tooltipKey;
            }
            if (tooltip == null && instanceName != null && instanceName.trim().length() > 0) {
                int dotIndex = instanceName.indexOf(46);
                tooltip = instanceName.substring(0, dotIndex).replace('-', '.');
            }
            if (tooltip == null) {
                tooltip = this._getDisplayName(bundleName, displayNameKey, instanceName);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, ex);
            tooltip = this.getOriginal().getShortDescription();
        }
        return tooltip == null ? "" : tooltip;
    }

    public Image _getIcon(int iconType, String iconURL) {
        Image icon;
        block5 : {
            icon = null;
            try {
                icon = ImageUtilities.loadImage((String)iconURL);
            }
            catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, ex);
            }
            if (null == icon) {
                try {
                    icon = ImageIO.read(new URL(iconURL));
                }
                catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, ex);
                    if (null == this.originalDO || FileUtil.isParentOf((FileObject)FileUtil.getConfigRoot(), (FileObject)this.originalDO.getPrimaryFile())) break block5;
                    icon = this.getOriginal().getIcon(1);
                }
            }
        }
        return icon;
    }

    public HelpCtx getHelpCtx() {
        DataNode dn = (DataNode)this.getOriginal();
        Object helpId = dn.getDataObject().getPrimaryFile().getAttribute("helpId");
        return helpId == null ? super.getHelpCtx() : new HelpCtx(helpId.toString());
    }

    private static class NoExternalDndTransferable
    implements Transferable {
        private Transferable t;
        private DataFlavor uriListFlavor;

        public NoExternalDndTransferable(Transferable t) {
            this.t = t;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            DataFlavor[] flavors = this.t.getTransferDataFlavors();
            if (this.t.isDataFlavorSupported(DataFlavor.javaFileListFlavor) || this.t.isDataFlavorSupported(this.getUriListFlavor())) {
                ArrayList<DataFlavor> tmp = new ArrayList<DataFlavor>(flavors.length);
                for (int i = 0; i < flavors.length; ++i) {
                    if (!this.isDataFlavorSupported(flavors[i])) continue;
                    tmp.add(flavors[i]);
                }
                flavors = tmp.toArray(new DataFlavor[tmp.size()]);
            }
            return flavors;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            if (DataFlavor.javaFileListFlavor.equals(flavor) || this.getUriListFlavor().equals(flavor)) {
                return false;
            }
            return this.t.isDataFlavorSupported(flavor);
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (!this.isDataFlavorSupported(flavor)) {
                throw new UnsupportedFlavorException(flavor);
            }
            return this.t.getTransferData(flavor);
        }

        private DataFlavor getUriListFlavor() {
            if (null == this.uriListFlavor) {
                try {
                    this.uriListFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
                }
                catch (ClassNotFoundException ex) {
                    throw new AssertionError(ex);
                }
            }
            return this.uriListFlavor;
        }
    }

    private static class ActiveEditorDropTransferable
    extends ExTransferable.Single {
        private ActiveEditorDrop drop;

        ActiveEditorDropTransferable(ActiveEditorDrop drop) {
            super(ActiveEditorDrop.FLAVOR);
            this.drop = drop;
        }

        public Object getData() {
            return this.drop;
        }
    }

}

