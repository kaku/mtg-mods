/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.nodes.Index
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 */
package org.netbeans.modules.palette;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.DefaultCategory;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.Model;
import org.netbeans.modules.palette.ModelListener;
import org.netbeans.modules.palette.RootNode;
import org.netbeans.modules.palette.Settings;
import org.netbeans.modules.palette.ui.Customizer;
import org.netbeans.spi.palette.DragAndDropHandler;
import org.netbeans.spi.palette.PaletteActions;
import org.netbeans.spi.palette.PaletteController;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Lookup;
import org.openide.util.Mutex;

public class DefaultModel
implements Model,
NodeListener {
    private RootNode rootNode;
    private Item selectedItem;
    private Category selectedCategory;
    private PropertyChangeSupport propertySupport;
    private ArrayList<ModelListener> modelListeners = new ArrayList(3);
    private boolean categoriesNeedRefresh = true;
    private Category[] categories;
    private boolean isRefreshingChildren = false;

    public DefaultModel(RootNode rootNode) {
        this.rootNode = rootNode;
        this.propertySupport = new PropertyChangeSupport(this);
        this.rootNode.addNodeListener((NodeListener)this);
    }

    @Override
    public void setSelectedItem(Lookup category, Lookup item) {
        Node itNode;
        Node catNode;
        Category cat = null;
        Item it = null;
        if (null != category && null != (catNode = (Node)category.lookup(Node.class))) {
            cat = this.findCategory(catNode);
        }
        if (null != item && null != cat && null != (itNode = (Node)item.lookup(Node.class))) {
            it = this.findItem(cat, itNode);
        }
        Item oldValue = this.selectedItem;
        this.selectedItem = it;
        this.selectedCategory = cat;
        this.propertySupport.firePropertyChange("selectedItem", oldValue, this.selectedItem);
    }

    @Override
    public void clearSelection() {
        this.setSelectedItem(null, null);
    }

    @Override
    public Action[] getActions() {
        return this.rootNode.getActions(false);
    }

    @Override
    public Item getSelectedItem() {
        return this.selectedItem;
    }

    @Override
    public Category getSelectedCategory() {
        return this.selectedCategory;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addModelListener(ModelListener listener) {
        ArrayList<ModelListener> arrayList = this.modelListeners;
        synchronized (arrayList) {
            this.modelListeners.add(listener);
            this.propertySupport.addPropertyChangeListener(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeModelListener(ModelListener listener) {
        ArrayList<ModelListener> arrayList = this.modelListeners;
        synchronized (arrayList) {
            this.modelListeners.remove(listener);
            this.propertySupport.removePropertyChangeListener(listener);
        }
    }

    @Override
    public synchronized Category[] getCategories() {
        if (null == this.categories || this.categoriesNeedRefresh) {
            Node[] nodes = this.rootNode.getChildren().getNodes(DefaultModel.canBlock());
            this.categories = this.nodes2categories(nodes);
            this.categoriesNeedRefresh = false;
        }
        return this.categories;
    }

    public static boolean canBlock() {
        return !Children.MUTEX.isReadAccess() && !Children.MUTEX.isWriteAccess();
    }

    public void childrenAdded(NodeMemberEvent ev) {
        this.categoriesNeedRefresh = true;
        if (this.isRefreshingChildren) {
            return;
        }
        final Node[] nodes = ev.getDelta();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                Category[] addedCategories = DefaultModel.this.findCategories(nodes);
                DefaultModel.this.fireCategoriesChanged(addedCategories, true);
            }
        });
    }

    public void childrenRemoved(NodeMemberEvent ev) {
        this.categoriesNeedRefresh = true;
        final Node[] nodes = ev.getDelta();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                Category[] removedCategories = DefaultModel.this.findCategories(nodes);
                DefaultModel.this.fireCategoriesChanged(removedCategories, false);
            }
        });
    }

    public void childrenReordered(NodeReorderEvent ev) {
        this.categoriesNeedRefresh = true;
        this.fireCategoriesChanged(null, false);
    }

    public synchronized void nodeDestroyed(NodeEvent ev) {
        this.rootNode.removeNodeListener((NodeListener)this);
    }

    public void propertyChange(PropertyChangeEvent evt) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireCategoriesChanged(Category[] changedCategories, boolean added) {
        ModelListener[] listeners;
        ArrayList<ModelListener> arrayList = this.modelListeners;
        synchronized (arrayList) {
            listeners = new ModelListener[this.modelListeners.size()];
            listeners = this.modelListeners.toArray(listeners);
        }
        for (int i = 0; i < listeners.length; ++i) {
            if (null != changedCategories) {
                if (added) {
                    listeners[i].categoriesAdded(changedCategories);
                    continue;
                }
                listeners[i].categoriesRemoved(changedCategories);
                continue;
            }
            listeners[i].categoriesReordered();
        }
    }

    private Category[] nodes2categories(Node[] nodes) {
        Category[] res = new Category[nodes.length];
        for (int i = 0; i < res.length; ++i) {
            res[i] = new DefaultCategory(nodes[i]);
        }
        return res;
    }

    private Category[] findCategories(Node[] nodes) {
        Category[] res = new Category[nodes.length];
        Category[] current = this.getCategories();
        for (int i = 0; i < res.length; ++i) {
            boolean found = false;
            for (int j = 0; !found && null != current && j < current.length; ++j) {
                Node catNode = (Node)current[j].getLookup().lookup(Node.class);
                if (!nodes[i].equals((Object)catNode)) continue;
                res[i] = current[j];
                found = true;
            }
            if (found) continue;
            res[i] = new DefaultCategory(nodes[i]);
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void refresh() {
        RootNode rootNode = this.rootNode;
        synchronized (rootNode) {
            PaletteActions customActions = (PaletteActions)this.rootNode.getLookup().lookup(PaletteActions.class);
            Action customRefreshAction = customActions.getRefreshAction();
            if (null != customRefreshAction) {
                customRefreshAction.actionPerformed(new ActionEvent((Object)this.getRoot(), 0, "refresh"));
            }
            this.clearSelection();
            this.categoriesNeedRefresh = true;
            this.isRefreshingChildren = true;
            try {
                this.rootNode.refreshChildren();
            }
            finally {
                this.isRefreshingChildren = false;
            }
            this.fireCategoriesChanged(null, false);
        }
    }

    @Override
    public void showCustomizer(PaletteController controller, Settings settings) {
        Customizer.show((Node)this.rootNode, controller, settings);
    }

    @Override
    public Lookup getRoot() {
        return this.rootNode.getLookup();
    }

    @Override
    public boolean moveCategory(Category source, Category target, boolean moveBefore) {
        int targetIndex = this.categoryToIndex(target);
        if (!moveBefore) {
            ++targetIndex;
        }
        DragAndDropHandler handler = this.getDragAndDropHandler();
        return handler.moveCategory(source.getLookup(), targetIndex);
    }

    private int categoryToIndex(Category category) {
        Index order;
        Node node = (Node)category.getLookup().lookup(Node.class);
        if (null != node && null != (order = (Index)this.rootNode.getCookie(Index.class))) {
            return order.indexOf(node);
        }
        return -1;
    }

    @Override
    public String getName() {
        return this.rootNode.getName();
    }

    private Category findCategory(Node node) {
        Category[] cats = this.getCategories();
        for (int i = 0; i < cats.length; ++i) {
            Node catNode = (Node)cats[i].getLookup().lookup(Node.class);
            if (null == catNode || !catNode.equals((Object)node)) continue;
            return cats[i];
        }
        return null;
    }

    private Item findItem(Category category, Node node) {
        Item[] items = category.getItems();
        for (int i = 0; i < items.length; ++i) {
            Node itNode = (Node)items[i].getLookup().lookup(Node.class);
            if (null == itNode || !itNode.equals((Object)node)) continue;
            return items[i];
        }
        return null;
    }

    @Override
    public boolean canReorderCategories() {
        return this.getDragAndDropHandler().canReorderCategories(this.rootNode.getLookup());
    }

    private DragAndDropHandler getDragAndDropHandler() {
        return (DragAndDropHandler)this.rootNode.getLookup().lookup(DragAndDropHandler.class);
    }

}

