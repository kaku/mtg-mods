/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.palette;

import org.netbeans.modules.palette.Category;

public interface CategoryListener {
    public void categoryModified(Category var1);
}

