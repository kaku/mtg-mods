/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.palette;

import java.beans.PropertyChangeListener;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Item;

public interface Settings {
    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);

    public boolean isVisible(Item var1);

    public void setVisible(Item var1, boolean var2);

    public boolean isVisible(Category var1);

    public void setVisible(Category var1, boolean var2);

    public boolean isExpanded(Category var1);

    public void setExpanded(Category var1, boolean var2);

    public void setShowItemNames(boolean var1);

    public boolean getShowItemNames();

    public void setIconSize(int var1);

    public int getIconSize();

    public int getItemWidth();

    public void reset();
}

