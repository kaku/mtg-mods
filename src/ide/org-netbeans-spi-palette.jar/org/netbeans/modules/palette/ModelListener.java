/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.palette;

import java.beans.PropertyChangeListener;
import org.netbeans.modules.palette.Category;

public interface ModelListener
extends PropertyChangeListener {
    public static final String PROP_SELECTED_ITEM = "selectedItem";

    public void categoriesAdded(Category[] var1);

    public void categoriesRemoved(Category[] var1);

    public void categoriesReordered();
}

