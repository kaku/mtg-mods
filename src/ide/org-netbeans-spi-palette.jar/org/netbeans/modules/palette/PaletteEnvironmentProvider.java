/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.Environment
 *  org.openide.loaders.Environment$Provider
 *  org.openide.loaders.XMLDataObject
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.palette;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.palette.ActiveEditorDropDefaultProvider;
import org.netbeans.modules.palette.ActiveEditorDropProvider;
import org.netbeans.modules.palette.PaletteItemHandler;
import org.netbeans.modules.palette.PaletteItemNode;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.Environment;
import org.openide.loaders.XMLDataObject;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class PaletteEnvironmentProvider
implements Environment.Provider {
    private static PaletteEnvironmentProvider createProvider() {
        return new PaletteEnvironmentProvider();
    }

    private PaletteEnvironmentProvider() {
    }

    public Lookup getEnvironment(DataObject obj) {
        if (!FileUtil.isParentOf((FileObject)FileUtil.getConfigRoot(), (FileObject)obj.getPrimaryFile())) {
            return Lookup.EMPTY;
        }
        PaletteItemNodeFactory nodeFactory = new PaletteItemNodeFactory((XMLDataObject)obj);
        return nodeFactory.getLookup();
    }

    private static class PaletteItemNodeFactory
    implements InstanceContent.Convertor<Class, PaletteItemNode> {
        private XMLDataObject xmlDataObject = null;
        private Lookup lookup = null;
        Reference<PaletteItemNode> refNode = new WeakReference<Object>(null);
        private WeakReference<XMLReader> cachedReader;

        PaletteItemNodeFactory(XMLDataObject obj) {
            this.xmlDataObject = obj;
            InstanceContent content = new InstanceContent();
            content.add(Node.class, (InstanceContent.Convertor)this);
            this.lookup = new AbstractLookup((AbstractLookup.Content)content);
        }

        Lookup getLookup() {
            return this.lookup;
        }

        public Class<? extends PaletteItemNode> type(Class obj) {
            if (obj == Node.class) {
                return PaletteItemNode.class;
            }
            return null;
        }

        public String id(Class obj) {
            return obj.toString();
        }

        public String displayName(Class obj) {
            return obj.getName();
        }

        public PaletteItemNode convert(Class obj) {
            PaletteItemNode o = null;
            if (obj == Node.class) {
                try {
                    o = this.getInstance();
                }
                catch (Exception ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, ex);
                }
            }
            return o;
        }

        private XMLReader getXMLReader() throws SAXException {
            XMLReader res;
            XMLReader xMLReader = res = null == this.cachedReader ? null : this.cachedReader.get();
            if (null == res) {
                res = XMLUtil.createXMLReader((boolean)true);
                res.setEntityResolver((EntityResolver)EntityCatalog.getDefault());
                this.cachedReader = new WeakReference<XMLReader>(res);
            }
            return res;
        }

        public synchronized PaletteItemNode getInstance() {
            PaletteItemNode node = this.refNode.get();
            if (node != null) {
                return node;
            }
            FileObject file = this.xmlDataObject.getPrimaryFile();
            if (file.getSize() == 0) {
                return null;
            }
            PaletteItemHandler handler = new PaletteItemHandler();
            try {
                XMLReader reader = this.getXMLReader();
                FileObject fo = this.xmlDataObject.getPrimaryFile();
                String urlString = fo.getURL().toExternalForm();
                InputSource is = new InputSource(fo.getInputStream());
                is.setSystemId(urlString);
                reader.setContentHandler(handler);
                reader.setErrorHandler(handler);
                reader.parse(is);
            }
            catch (SAXException saxe) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, saxe);
                return null;
            }
            catch (IOException ioe) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, null, ioe);
                return null;
            }
            if (handler.isError()) {
                return null;
            }
            node = this.createPaletteItemNode(handler);
            this.refNode = new WeakReference<PaletteItemNode>(node);
            return node;
        }

        private PaletteItemNode createPaletteItemNode(PaletteItemHandler handler) {
            String name = this.xmlDataObject.getName();
            InstanceContent ic = new InstanceContent();
            String s = handler.getClassName();
            if (s != null) {
                ic.add((Object)s, (InstanceContent.Convertor)ActiveEditorDropProvider.getInstance());
            } else {
                s = handler.getBody();
                if (s != null) {
                    ic.add((Object)s, (InstanceContent.Convertor)ActiveEditorDropDefaultProvider.getInstance());
                }
            }
            return null == handler.getDisplayName() ? new PaletteItemNode(new DataNode((DataObject)this.xmlDataObject, Children.LEAF), name, handler.getBundleName(), handler.getDisplayNameKey(), handler.getClassName(), handler.getTooltipKey(), handler.getIcon16URL(), handler.getIcon32URL(), ic) : new PaletteItemNode(new DataNode((DataObject)this.xmlDataObject, Children.LEAF), name, handler.getDisplayName(), handler.getTooltip(), handler.getIcon16URL(), handler.getIcon32URL(), ic);
        }
    }

}

