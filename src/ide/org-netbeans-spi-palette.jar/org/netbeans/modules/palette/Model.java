/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.palette;

import javax.swing.Action;
import org.netbeans.modules.palette.Category;
import org.netbeans.modules.palette.Item;
import org.netbeans.modules.palette.ModelListener;
import org.netbeans.modules.palette.Settings;
import org.netbeans.spi.palette.PaletteController;
import org.openide.util.Lookup;

public interface Model {
    public static final String PROP_SELECTED_ITEM = "selectedItem";

    public String getName();

    public Category[] getCategories();

    public Action[] getActions();

    public void addModelListener(ModelListener var1);

    public void removeModelListener(ModelListener var1);

    public Item getSelectedItem();

    public Category getSelectedCategory();

    public void setSelectedItem(Lookup var1, Lookup var2);

    public void clearSelection();

    public void refresh();

    public void showCustomizer(PaletteController var1, Settings var2);

    public Lookup getRoot();

    public boolean moveCategory(Category var1, Category var2, boolean var3);

    public boolean canReorderCategories();
}

