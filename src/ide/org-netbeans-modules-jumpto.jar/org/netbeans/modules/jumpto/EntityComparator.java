/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.project.ui.OpenProjects
 */
package org.netbeans.modules.jumpto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.ui.OpenProjects;

public abstract class EntityComparator<E>
implements Comparator<E> {
    protected final String mainProjectName = EntityComparator.getMainProjectName();
    protected final Collection<String> namesOfOpenProjects = EntityComparator.getNamesOfOpenProjects();

    @Override
    protected int compare(String s1, String s2) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }
        return s1.compareTo(s2);
    }

    protected int compare(String s1, String s2, boolean caseSensitive) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }
        return caseSensitive ? s1.compareTo(s2) : s1.compareToIgnoreCase(s2);
    }

    protected int compareProjects(String p1Name, String p2Name) {
        boolean isP2Closed;
        if (p1Name == null) {
            p1Name = "";
        }
        if (p2Name == null) {
            p2Name = "";
        }
        if (p1Name.isEmpty()) {
            if (p2Name.isEmpty()) {
                return 0;
            }
            return 1;
        }
        if (p2Name.isEmpty()) {
            return -1;
        }
        boolean isP1Closed = !this.namesOfOpenProjects.contains(p1Name);
        boolean bl = isP2Closed = !this.namesOfOpenProjects.contains(p2Name);
        if (isP1Closed) {
            if (isP2Closed) {
                return 0;
            }
            return 1;
        }
        if (isP2Closed) {
            return -1;
        }
        if (p1Name.equals(this.mainProjectName)) {
            if (p2Name.equals(this.mainProjectName)) {
                return 0;
            }
            return -1;
        }
        if (p2Name.equals(this.mainProjectName)) {
            return 1;
        }
        return 0;
    }

    public static String getMainProjectName() {
        Project mainProject = OpenProjects.getDefault().getMainProject();
        return mainProject == null ? null : ProjectUtils.getInformation((Project)mainProject).getDisplayName();
    }

    private static Collection<String> getNamesOfOpenProjects() {
        ArrayList<String> names = new ArrayList<String>(10);
        for (Project p : OpenProjects.getDefault().getOpenProjects()) {
            String pName = ProjectUtils.getInformation((Project)p).getDisplayName();
            names.add(pName);
        }
        return names;
    }
}

