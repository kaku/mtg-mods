/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.NbDocument
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.jumpto.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.jumpto.EntitiesListCellRenderer;
import org.netbeans.modules.jumpto.common.Factory;
import org.netbeans.modules.jumpto.common.HighlightingNameFormatter;
import org.netbeans.modules.jumpto.common.Models;
import org.netbeans.modules.jumpto.common.Utils;
import org.netbeans.modules.jumpto.file.FileComarator;
import org.netbeans.modules.jumpto.file.FileProviderAccessor;
import org.netbeans.modules.jumpto.file.FileSearchOptions;
import org.netbeans.modules.jumpto.file.FileSearchPanel;
import org.netbeans.modules.jumpto.file.Worker;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.jumpto.file.FileDescriptor;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.HtmlRenderer;
import org.openide.awt.Mnemonics;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.windows.TopComponent;

public class FileSearchAction
extends AbstractAction
implements FileSearchPanel.ContentProvider {
    static final Logger LOGGER = Logger.getLogger(FileSearchAction.class.getName());
    private static final char LINE_NUMBER_SEPARATOR = ':';
    private static final Pattern PATTERN_WITH_LINE_NUMBER = Pattern.compile("(.*):(\\d*)");
    private static final ListModel EMPTY_LIST_MODEL = new DefaultListModel();
    private static final RequestProcessor rp = new RequestProcessor("FileSearchAction-RequestProcessor", 1);
    private Worker[] running;
    private RequestProcessor.Task[] scheduledTasks;
    private Dialog dialog;
    private JButton openBtn;
    private FileSearchPanel panel;
    private Dimension initialDimension;

    public FileSearchAction() {
        super(NbBundle.getMessage(FileSearchAction.class, (String)"CTL_FileSearchAction"));
        this.putValue("PopupMenuText", NbBundle.getBundle(FileSearchAction.class).getString("editor-popup-CTL_FileSearchAction"));
        this.putValue("noIconInMenu", Boolean.TRUE);
    }

    @Override
    public boolean isEnabled() {
        return OpenProjects.getDefault().getOpenProjects().length > 0;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        FileDescriptor[] typeDescriptors = this.getSelectedFiles();
        if (typeDescriptors != null) {
            JumpList.checkAddEntry();
            for (FileDescriptor td : typeDescriptors) {
                td.open();
            }
        }
    }

    @Override
    public ListCellRenderer getListCellRenderer(@NonNull JList list, @NonNull Document nameDocument, @NonNull ButtonModel caseSensitive) {
        Parameters.notNull((CharSequence)"list", (Object)list);
        Parameters.notNull((CharSequence)"nameDocument", (Object)nameDocument);
        Parameters.notNull((CharSequence)"caseSensitive", (Object)caseSensitive);
        return new Renderer(list, nameDocument, caseSensitive);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setListModel(final FileSearchPanel panel, String text) {
        int lineNr;
        QuerySupport.Kind nameKind;
        if (this.openBtn != null) {
            this.openBtn.setEnabled(false);
        }
        this.cancel();
        if (text == null) {
            panel.setModel(EMPTY_LIST_MODEL, true);
            return;
        }
        boolean exact = text.endsWith(" ");
        if ((text = text.trim()).length() == 0 || !Utils.isValidInput(text)) {
            panel.setModel(EMPTY_LIST_MODEL, true);
            return;
        }
        int wildcard = Utils.containsWildCard(text);
        if (exact) {
            nameKind = QuerySupport.Kind.EXACT;
        } else if (wildcard != -1) {
            nameKind = panel.isCaseSensitive() ? QuerySupport.Kind.REGEXP : QuerySupport.Kind.CASE_INSENSITIVE_REGEXP;
            text = Utils.removeNonNeededWildCards(text);
        } else {
            nameKind = Utils.isAllUpper(text) && text.length() > 1 || Utils.isCamelCase(text) ? QuerySupport.Kind.CAMEL_CASE : (panel.isCaseSensitive() ? QuerySupport.Kind.PREFIX : QuerySupport.Kind.CASE_INSENSITIVE_PREFIX);
        }
        Matcher matcher = PATTERN_WITH_LINE_NUMBER.matcher(text);
        if (matcher.matches()) {
            text = matcher.group(1);
            try {
                lineNr = Integer.parseInt(matcher.group(2));
            }
            catch (NumberFormatException numberFormatException) {
                lineNr = -1;
            }
        } else {
            lineNr = -1;
        }
        FileSearchAction numberFormatException = this;
        synchronized (numberFormatException) {
            final Models.MutableListModel<FileDescriptor> baseListModel = Models.mutable(new FileComarator(panel.isPreferedProject(), panel.isCaseSensitive()));
            panel.setModel(Models.refreshable(baseListModel, new Factory<FileDescriptor, Pair<FileDescriptor, Runnable>>(){

                @Override
                public FileDescriptor create(@NonNull Pair<FileDescriptor, Runnable> param) {
                    return new AsyncFileDescriptor((FileDescriptor)param.first(), (Runnable)param.second());
                }
            }), false);
            Worker.Request request = Worker.newRequest(text, nameKind, panel.getCurrentProject(), lineNr);
            Worker.Collector collector = Worker.newCollector(baseListModel, new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            panel.searchProgress();
                            if (FileSearchAction.this.openBtn != null && baseListModel.getSize() > 0) {
                                FileSearchAction.this.openBtn.setEnabled(true);
                            }
                        }
                    });
                }

            }, new Runnable(){

                @Override
                public void run() {
                    panel.searchCompleted();
                }
            }, panel.time);
            Worker.Type[] wts = Worker.Type.values();
            Worker[] workers = new Worker[wts.length];
            for (int i = 0; i < wts.length; ++i) {
                workers[i] = Worker.newWorker(request, collector, wts[i]);
            }
            this.running = workers;
            RequestProcessor.Task[] tasks = new RequestProcessor.Task[workers.length];
            for (int i2 = 0; i2 < workers.length; ++i2) {
                tasks[i2] = rp.post((Runnable)workers[i2], 220);
            }
            this.scheduledTasks = tasks;
            if (panel.time != -1) {
                LOGGER.log(Level.FINE, "Worker posted after {0} ms.", System.currentTimeMillis() - panel.time);
            }
        }
    }

    @Override
    public void closeDialog() {
        this.dialog.setVisible(false);
        this.cleanup();
    }

    @Override
    public boolean hasValidContent() {
        return this.openBtn != null && this.openBtn.isEnabled();
    }

    private FileDescriptor[] getSelectedFiles() {
        EditorCookie ec;
        JEditorPane recentPane;
        FileDescriptor[] result = null;
        this.panel = new FileSearchPanel(this, FileSearchAction.findCurrentProject());
        this.dialog = this.createDialog(this.panel);
        Node[] arr = TopComponent.getRegistry().getActivatedNodes();
        if (arr.length > 0 && (ec = (EditorCookie)arr[0].getCookie(EditorCookie.class)) != null && (recentPane = NbDocument.findRecentEditorPane((EditorCookie)ec)) != null) {
            String initSearchText = null;
            if (Utilities.isSelectionShowing((Caret)recentPane.getCaret())) {
                initSearchText = recentPane.getSelectedText();
            }
            if (initSearchText != null) {
                if (initSearchText.length() > 256) {
                    initSearchText = initSearchText.substring(0, 256);
                }
                this.panel.setInitialText(initSearchText);
            } else {
                FileObject fo = (FileObject)arr[0].getLookup().lookup(FileObject.class);
                if (fo != null) {
                    this.panel.setInitialText(fo.getNameExt());
                }
            }
        }
        this.dialog.setVisible(true);
        result = this.panel.getSelectedFiles();
        return result;
    }

    private Dialog createDialog(FileSearchPanel panel) {
        this.openBtn = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.openBtn, (String)NbBundle.getMessage(FileSearchAction.class, (String)"CTL_Open"));
        this.openBtn.getAccessibleContext().setAccessibleDescription(this.openBtn.getText());
        this.openBtn.setEnabled(false);
        Object[] buttons = new Object[]{this.openBtn, DialogDescriptor.CANCEL_OPTION};
        String title = NbBundle.getMessage(FileSearchAction.class, (String)"MSG_FileSearchDlgTitle");
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panel, title, true, buttons, (Object)this.openBtn, 0, HelpCtx.DEFAULT_HELP, (ActionListener)new DialogButtonListener(panel));
        dialogDescriptor.setClosingOptions(buttons);
        Dialog d = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        d.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileSearchAction.class, (String)"AN_FileSearchDialog"));
        d.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchAction.class, (String)"AD_FileSearchDialog"));
        d.setPreferredSize(new Dimension(FileSearchOptions.getWidth(), FileSearchOptions.getHeight()));
        Rectangle r = org.openide.util.Utilities.getUsableScreenBounds();
        int maxW = r.width * 9 / 10;
        int maxH = r.height * 9 / 10;
        Dimension dim = d.getPreferredSize();
        dim.width = Math.min(dim.width, maxW);
        dim.height = Math.min(dim.height, maxH);
        this.initialDimension = dim;
        d.setBounds(org.openide.util.Utilities.findCenterBounds((Dimension)dim));
        d.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosed(WindowEvent e) {
                FileSearchAction.this.cleanup();
            }
        });
        return d;
    }

    private static Project findCurrentProject() {
        Lookup lookup = org.openide.util.Utilities.actionsGlobalContext();
        Iterator i$ = lookup.lookupAll(Project.class).iterator();
        if (i$.hasNext()) {
            Project p = (Project)i$.next();
            return p;
        }
        for (DataObject dObj : lookup.lookupAll(DataObject.class)) {
            FileObject fObj = dObj.getPrimaryFile();
            Project p = FileOwnerQuery.getOwner((FileObject)fObj);
            if (p == null) continue;
            return p;
        }
        return OpenProjects.getDefault().getMainProject();
    }

    private void cleanup() {
        this.cancel();
        if (this.dialog != null) {
            int currentWidth = this.dialog.getWidth();
            int currentHeight = this.dialog.getHeight();
            if (this.initialDimension != null && (this.initialDimension.width != currentWidth || this.initialDimension.height != currentHeight)) {
                FileSearchOptions.setHeight(currentHeight);
                FileSearchOptions.setWidth(currentWidth);
            }
            this.initialDimension = null;
            this.dialog.dispose();
            this.dialog = null;
            FileSearchOptions.flush();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void cancel() {
        FileSearchAction fileSearchAction = this;
        synchronized (fileSearchAction) {
            if (this.running != null) {
                for (Worker w : this.running) {
                    w.cancel();
                }
                for (Worker t : this.scheduledTasks) {
                    t.cancel();
                }
                this.running = null;
                this.scheduledTasks = null;
            }
        }
    }

    public static class Renderer
    extends EntitiesListCellRenderer
    implements ActionListener,
    DocumentListener {
        public static Icon WAIT_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/resources/wait.gif", (boolean)false);
        private final HighlightingNameFormatter fileNameFormatter;
        private RendererComponent rendererComponent;
        private JLabel jlName = HtmlRenderer.createLabel();
        private JLabel jlPath = new JLabel();
        private JLabel jlPrj = new JLabel();
        private int DARKER_COLOR_COMPONENT = 15;
        private int LIGHTER_COLOR_COMPONENT = 80;
        private Color fgColor;
        private Color fgColorLighter;
        private Color bgColor;
        private Color bgColorDarker;
        private Color bgSelectionColor;
        private Color fgSelectionColor;
        private Color bgColorGreener;
        private Color bgColorDarkerGreener;
        private String textToFind = "";
        private boolean caseSensitive;
        private JList jList;
        private boolean colorPrefered;

        public Renderer(@NonNull JList list, @NonNull Document nameDocument, @NonNull ButtonModel caseSensitive) {
            this.jList = list;
            this.caseSensitive = caseSensitive.isSelected();
            this.resetName();
            Container container = list.getParent();
            if (container instanceof JViewport) {
                ((JViewport)container).addChangeListener(this);
                this.stateChanged(new ChangeEvent(container));
            }
            this.rendererComponent = new RendererComponent();
            this.rendererComponent.setLayout(new BorderLayout());
            this.rendererComponent.add((Component)this.jlName, "West");
            this.rendererComponent.add((Component)this.jlPath, "Center");
            this.rendererComponent.add((Component)this.jlPrj, "East");
            this.jlPath.setOpaque(false);
            this.jlPrj.setOpaque(false);
            this.jlPath.setFont(list.getFont());
            this.jlPrj.setFont(list.getFont());
            this.jlPrj.setHorizontalAlignment(4);
            this.jlPrj.setHorizontalTextPosition(2);
            this.fgColor = list.getForeground();
            this.fgColorLighter = new Color(Math.min(255, this.fgColor.getRed() + this.LIGHTER_COLOR_COMPONENT), Math.min(255, this.fgColor.getGreen() + this.LIGHTER_COLOR_COMPONENT), Math.min(255, this.fgColor.getBlue() + this.LIGHTER_COLOR_COMPONENT));
            this.bgColor = new Color(list.getBackground().getRGB());
            this.bgColorDarker = new Color(Math.abs(this.bgColor.getRed() - this.DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getGreen() - this.DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getBlue() - this.DARKER_COLOR_COMPONENT));
            this.bgSelectionColor = list.getSelectionBackground();
            this.fgSelectionColor = list.getSelectionForeground();
            this.bgColorGreener = new Color(Math.abs(this.bgColor.getRed() - 20), Math.min(255, this.bgColor.getGreen() + 10), Math.abs(this.bgColor.getBlue() - 20));
            this.bgColorDarkerGreener = new Color(Math.abs(this.bgColorDarker.getRed() - 35), Math.min(255, this.bgColorDarker.getGreen() + 5), Math.abs(this.bgColorDarker.getBlue() - 35));
            this.fileNameFormatter = HighlightingNameFormatter.createBoldFormatter();
            nameDocument.addDocumentListener(this);
            caseSensitive.addActionListener(this);
            this.jlName.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
            int height = list.getFixedCellHeight();
            int width = list.getFixedCellWidth() - 1;
            width = width < 200 ? 200 : width;
            Dimension size = new Dimension(width, height);
            this.rendererComponent.setMaximumSize(size);
            this.rendererComponent.setPreferredSize(size);
            this.resetName();
            if (isSelected) {
                this.jlName.setForeground(this.fgSelectionColor);
                this.jlName.setBackground(this.bgSelectionColor);
                this.jlPath.setForeground(this.fgSelectionColor);
                this.jlPrj.setForeground(this.fgSelectionColor);
                this.rendererComponent.setBackground(this.bgSelectionColor);
            } else {
                this.jlName.setForeground(this.fgColor);
                this.jlName.setBackground(this.bgColor);
                this.jlPath.setForeground(this.fgColorLighter);
                this.jlPrj.setForeground(this.fgColor);
                this.rendererComponent.setBackground(index % 2 == 0 ? this.bgColor : this.bgColorDarker);
            }
            if (value instanceof FileDescriptor) {
                FileDescriptor fd = (FileDescriptor)value;
                this.jlName.setIcon(fd.getIcon());
                String formattedFileName = this.fileNameFormatter.formatName(fd.getFileName(), this.textToFind, this.caseSensitive, isSelected ? this.fgSelectionColor : this.fgColor);
                this.jlName.setText(formattedFileName);
                this.jlPath.setIcon(null);
                this.jlPath.setHorizontalAlignment(2);
                this.jlPath.setText(fd.getOwnerPath().length() > 0 ? " (" + fd.getOwnerPath() + ")" : " ()");
                this.setProjectName(this.jlPrj, fd.getProjectName());
                this.jlPrj.setIcon(fd.getProjectIcon());
                if (!isSelected) {
                    boolean cprj;
                    boolean bl = cprj = FileProviderAccessor.getInstance().isFromCurrentProject(fd) && this.colorPrefered;
                    Color bgc = index % 2 == 0 ? (cprj ? this.bgColorGreener : this.bgColor) : (cprj ? this.bgColorDarkerGreener : this.bgColorDarker);
                    this.jlName.setBackground(bgc);
                    this.rendererComponent.setBackground(bgc);
                }
                this.rendererComponent.setDescription(fd);
            } else {
                this.jlName.setText("");
                this.jlName.setIcon(null);
                this.jlPath.setIcon(WAIT_ICON);
                this.jlPath.setHorizontalAlignment(0);
                this.jlPath.setText(value.toString());
                this.jlPrj.setIcon(null);
                this.jlPrj.setText("");
            }
            return this.rendererComponent;
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            JViewport jv = (JViewport)event.getSource();
            this.jlName.setText("Sample");
            this.jlName.setIcon(new ImageIcon());
            this.jList.setFixedCellHeight(this.jlName.getPreferredSize().height);
            this.jList.setFixedCellWidth(jv.getExtentSize().width);
        }

        public void setColorPrefered(boolean colorPrefered) {
            this.colorPrefered = colorPrefered;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.caseSensitive = ((ButtonModel)e.getSource()).isSelected();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.changedUpdate(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.changedUpdate(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            try {
                this.textToFind = e.getDocument().getText(0, e.getDocument().getLength());
            }
            catch (BadLocationException ex) {
                this.textToFind = "";
            }
        }

        private void resetName() {
            ((HtmlRenderer.Renderer)this.jlName).reset();
            this.jlName.setFont(this.jList.getFont());
            this.jlName.setOpaque(true);
            ((HtmlRenderer.Renderer)this.jlName).setHtml(true);
            ((HtmlRenderer.Renderer)this.jlName).setRenderStyle(1);
        }
    }

    private static final class AsyncFileDescriptor
    extends FileDescriptor
    implements Runnable {
        private static final String UNKNOWN_ICON_PATH = "org/netbeans/modules/jumpto/resources/unknown.gif";
        private static final Icon UNKNOWN_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/resources/unknown.gif", (boolean)false);
        private static final RequestProcessor LOAD_ICON_RP = new RequestProcessor(AsyncFileDescriptor.class.getName(), 1, false, false);
        private final FileDescriptor delegate;
        private final Runnable refreshCallback;
        private volatile Icon icon;

        AsyncFileDescriptor(@NonNull FileDescriptor delegate, @NonNull Runnable refreshCallback) {
            Parameters.notNull((CharSequence)"delegate", (Object)delegate);
            Parameters.notNull((CharSequence)"refreshCallback", (Object)refreshCallback);
            this.delegate = delegate;
            this.refreshCallback = refreshCallback;
            FileProviderAccessor.getInstance().setFromCurrentProject(this, FileProviderAccessor.getInstance().isFromCurrentProject(delegate));
        }

        @Override
        public String getFileName() {
            return this.delegate.getFileName();
        }

        @Override
        public String getOwnerPath() {
            return this.delegate.getOwnerPath();
        }

        @Override
        public Icon getIcon() {
            if (this.icon != null) {
                return this.icon;
            }
            LOAD_ICON_RP.execute((Runnable)this);
            return UNKNOWN_ICON;
        }

        @Override
        public String getProjectName() {
            return this.delegate.getProjectName();
        }

        @Override
        public Icon getProjectIcon() {
            return this.delegate.getProjectIcon();
        }

        @Override
        public void open() {
            this.delegate.open();
        }

        @Override
        public FileObject getFileObject() {
            FileObject res = this.delegate.getFileObject();
            if (res == null) {
                FileSearchAction.LOGGER.log(Level.FINE, "FileDescriptor: {0} : {1} returned null from getFile", new Object[]{this.delegate, this.delegate.getClass()});
            }
            return res;
        }

        @Override
        public String getFileDisplayPath() {
            return this.delegate.getFileDisplayPath();
        }

        @Override
        public void run() {
            this.icon = this.delegate.getIcon();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    AsyncFileDescriptor.this.refreshCallback.run();
                }
            });
        }

    }

    private static class RendererComponent
    extends JPanel {
        private FileDescriptor fd;

        private RendererComponent() {
        }

        void setDescription(FileDescriptor fd) {
            this.fd = fd;
            this.putClientProperty("ToolTipText", null);
        }

        @Override
        public String getToolTipText() {
            String text = (String)this.getClientProperty("ToolTipText");
            if (text == null) {
                if (this.fd != null) {
                    text = this.fd.getFileDisplayPath();
                }
                this.putClientProperty("ToolTipText", text);
            }
            return text;
        }
    }

    private class DialogButtonListener
    implements ActionListener {
        private FileSearchPanel panel;

        public DialogButtonListener(FileSearchPanel panel) {
            this.panel = panel;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == FileSearchAction.this.openBtn) {
                this.panel.setSelectedFile();
            }
        }
    }

}

