/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.Project
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.jumpto.file;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.Project;
import org.netbeans.spi.jumpto.file.FileDescriptor;
import org.netbeans.spi.jumpto.file.FileProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public abstract class FileProviderAccessor {
    private static volatile FileProviderAccessor instance;

    public static synchronized FileProviderAccessor getInstance() {
        if (instance == null) {
            try {
                Class.forName(FileProvider.Context.class.getName(), true, FileProviderAccessor.class.getClassLoader());
                assert (instance != null);
            }
            catch (ClassNotFoundException cnf) {
                Exceptions.printStackTrace((Throwable)cnf);
            }
        }
        assert (instance != null);
        return instance;
    }

    public static void setInstance(FileProviderAccessor theInstance) {
        assert (theInstance != null);
        instance = theInstance;
    }

    public abstract FileProvider.Context createContext(@NonNull String var1, @NonNull SearchType var2, int var3, @NullAllowed Project var4);

    public abstract void setRoot(FileProvider.Context var1, FileObject var2);

    public abstract FileProvider.Result createResult(List<? super FileDescriptor> var1, String[] var2, FileProvider.Context var3);

    public abstract int getRetry(FileProvider.Result var1);

    public abstract void setFromCurrentProject(FileDescriptor var1, boolean var2);

    public abstract boolean isFromCurrentProject(FileDescriptor var1);
}

