/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.CustomIndexer
 *  org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexDocument
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.jumpto.file;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.util.Exceptions;

public final class FileIndexer
extends CustomIndexer {
    public static final String ID = "org-netbeans-modules-jumpto-file-FileIndexer";
    public static final int VERSION = 1;
    public static final String FIELD_NAME = "file-name";
    public static final String FIELD_CASE_INSENSITIVE_NAME = "ci-file-name";
    public static final String FIELD_MIME_TYPE = "mime-type";
    private static final Logger LOG = Logger.getLogger(FileIndexer.class.getName());

    protected void index(Iterable<? extends Indexable> files, Context context) {
        try {
            long tm1 = System.currentTimeMillis();
            int cnt = 0;
            IndexingSupport is = IndexingSupport.getInstance((Context)context);
            for (Indexable i : files) {
                if (context.isCancelled()) {
                    LOG.fine("Indexer cancelled");
                    break;
                }
                ++cnt;
                String nameExt = FileIndexer.getNameExt(i);
                if (nameExt.length() <= 0) continue;
                IndexDocument d = is.createDocument(i);
                d.addPair("file-name", nameExt, true, true);
                d.addPair("ci-file-name", nameExt.toLowerCase(Locale.ENGLISH), true, true);
                String mimeType = FileIndexer.getMimeType(i);
                if (mimeType != null) {
                    d.addPair("mime-type", mimeType, false, true);
                }
                is.addDocument(d);
                if (!LOG.isLoggable(Level.FINEST)) continue;
                LOG.finest("added " + i.getURL() + "/" + i.getRelativePath());
            }
            long tm2 = System.currentTimeMillis();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Processed " + cnt + " files in " + (tm2 - tm1) + "ms.");
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
        }
    }

    private static String getNameExt(Indexable i) {
        String path = i.getRelativePath();
        int lastSlash = path.lastIndexOf(47);
        if (lastSlash != -1) {
            return path.substring(lastSlash + 1);
        }
        return i.getRelativePath();
    }

    private static String getMimeType(Indexable i) {
        String mimeType = i.getMimeType();
        if (!mimeType.equals("content/unknown")) {
            return mimeType;
        }
        return null;
    }

    public static final class Factory
    extends CustomIndexerFactory {
        public CustomIndexer createIndexer() {
            return new FileIndexer();
        }

        public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
            try {
                IndexingSupport is = IndexingSupport.getInstance((Context)context);
                for (Indexable i : deleted) {
                    is.removeDocuments(i);
                    if (!LOG.isLoggable(Level.FINEST)) continue;
                    LOG.finest("removed " + i.getURL() + "/" + i.getRelativePath());
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
        }

        public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
        }

        public boolean scanStarted(Context ctx) {
            try {
                IndexingSupport is = IndexingSupport.getInstance((Context)ctx);
                return is.isValid();
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
                return false;
            }
        }

        public void rootsRemoved(Iterable<? extends URL> removedRoots) {
        }

        public boolean supportsEmbeddedIndexers() {
            return true;
        }

        public String getIndexerName() {
            return "org-netbeans-modules-jumpto-file-FileIndexer";
        }

        public int getIndexVersion() {
            return 1;
        }
    }

}

