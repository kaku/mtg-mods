/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.jumpto.file;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class FileSearchOptions {
    private static final String CASE_SENSITIVE = "caseSensitive";
    private static final String SHOW_HIDDEN_FILES = "showHiddenFiles";
    private static final String PREFER_MAIN_PROJECT = "preferMainProject";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";
    private static Preferences node;

    public static boolean getCaseSensitive() {
        return FileSearchOptions.getNode().getBoolean("caseSensitive", false);
    }

    public static void setCaseSensitive(boolean caseSensitive) {
        FileSearchOptions.getNode().putBoolean("caseSensitive", caseSensitive);
    }

    public static boolean getShowHiddenFiles() {
        return FileSearchOptions.getNode().getBoolean("showHiddenFiles", false);
    }

    public static void setShowHiddenFiles(boolean showHiddenFiles) {
        FileSearchOptions.getNode().putBoolean("showHiddenFiles", showHiddenFiles);
    }

    public static boolean getPreferMainProject() {
        return FileSearchOptions.getNode().getBoolean("preferMainProject", true);
    }

    public static void setPreferMainProject(boolean preferMainProject) {
        FileSearchOptions.getNode().putBoolean("preferMainProject", preferMainProject);
    }

    public static int getHeight() {
        return FileSearchOptions.getNode().getInt("height", 460);
    }

    public static void setHeight(int height) {
        FileSearchOptions.getNode().putInt("height", height);
    }

    public static int getWidth() {
        return FileSearchOptions.getNode().getInt("width", 740);
    }

    public static void setWidth(int width) {
        FileSearchOptions.getNode().putInt("width", width);
    }

    static void flush() {
        try {
            FileSearchOptions.getNode().flush();
        }
        catch (BackingStoreException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static synchronized Preferences getNode() {
        if (node == null) {
            node = NbPreferences.forModule(FileSearchOptions.class);
        }
        return node;
    }
}

