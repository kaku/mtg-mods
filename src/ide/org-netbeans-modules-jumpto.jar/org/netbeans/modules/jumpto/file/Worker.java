/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.project.SourceGroup
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.api.search.provider.SearchFilter
 *  org.netbeans.api.search.provider.SearchFilter$FolderResult
 *  org.netbeans.api.search.provider.SearchInfoUtils
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.jumpto.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.api.search.provider.SearchFilter;
import org.netbeans.api.search.provider.SearchInfoUtils;
import org.netbeans.modules.jumpto.common.Models;
import org.netbeans.modules.jumpto.file.FileDescription;
import org.netbeans.modules.jumpto.file.FileProviderAccessor;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.jumpto.file.FileDescriptor;
import org.netbeans.spi.jumpto.file.FileProvider;
import org.netbeans.spi.jumpto.file.FileProviderFactory;
import org.netbeans.spi.jumpto.support.NameMatcher;
import org.netbeans.spi.jumpto.support.NameMatcherFactory;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Pair;
import org.openide.util.Parameters;

final class Worker
implements Runnable {
    private static final Logger LOG = Logger.getLogger(Worker.class.getName());
    private final Request request;
    private final Strategy strategy;
    private final Collector collector;
    private final long createTime;
    private volatile boolean cancelled;

    private Worker(@NonNull Request request, @NonNull Strategy strategy, @NonNull Collector collector) {
        Parameters.notNull((CharSequence)"request", (Object)request);
        Parameters.notNull((CharSequence)"strategy", (Object)strategy);
        Parameters.notNull((CharSequence)"collector", (Object)collector);
        this.request = request;
        this.strategy = strategy;
        this.collector = collector;
        this.createTime = System.currentTimeMillis();
        this.collector.configure(this);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Worker: {0} for: {1} handled by: {2} created after: {3}ms.", new Object[]{System.identityHashCode(this), request, strategy, this.createTime - this.collector.startTime});
        }
    }

    public void cancel() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Worker: {0} canceled after {1} ms.", new Object[]{System.identityHashCode(this), System.currentTimeMillis() - this.createTime});
        }
        this.cancelled = true;
        this.strategy.cancel();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        this.collector.start(this);
        try {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Worker: {0} started after {1} ms.", new Object[]{System.identityHashCode(this), System.currentTimeMillis() - this.createTime});
            }
            this.strategy.execute(this.request, this);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Worker: {0} finished after cancel {1} ms.", new Object[]{System.identityHashCode(this), System.currentTimeMillis() - this.createTime});
            }
        }
        finally {
            this.collector.done(this);
        }
    }

    public String toString() {
        return String.format("%s (%d) [request: %s, strategy: %s]", this.getClass().getSimpleName(), System.identityHashCode(this), this.request, this.strategy);
    }

    private void emit(@NonNull List<? extends FileDescriptor> files) {
        if (!this.cancelled) {
            this.collector.emit(this, files);
        }
    }

    @NonNull
    static Request newRequest(@NonNull String text, @NonNull QuerySupport.Kind searchType, @NullAllowed Project project, int lineNr) {
        return new Request(text, searchType, project, lineNr);
    }

    @NonNull
    static Collector newCollector(@NonNull Models.MutableListModel<FileDescriptor> model, @NonNull Runnable updateCallBack, @NonNull Runnable doneCallBack, long startTime) {
        return new Collector(model, updateCallBack, doneCallBack, startTime);
    }

    @NonNull
    static Worker newWorker(@NonNull Request request, @NonNull Collector collector, @NonNull Type type) {
        Parameters.notNull((CharSequence)"request", (Object)request);
        Parameters.notNull((CharSequence)"collector", (Object)collector);
        Parameters.notNull((CharSequence)"type", (Object)((Object)type));
        Strategy strategy = type.createStrategy();
        return new Worker(request, strategy, collector);
    }

    private static final class FSStrategy
    extends Strategy {
        private FSStrategy() {
            super();
        }

        @CheckForNull
        @Override
        void execute(@NonNull Request request, @NonNull Worker worker) {
            if (this.isCancelled()) {
                return;
            }
            SearchType jumpToSearchType = FSStrategy.toJumpToSearchType(request.getSearchKind());
            NameMatcher matcher = NameMatcherFactory.createNameMatcher(request.getText(), jumpToSearchType);
            ArrayList<FileDescription> files = new ArrayList<FileDescription>();
            HashSet<FileObject> allFolders = new HashSet<FileObject>();
            List filters = SearchInfoUtils.DEFAULT_FILTERS;
            for (FileObject root : request.getSourceRoots()) {
                allFolders.clear();
                for (FileObject folder : this.searchSources(root, allFolders, request, filters)) {
                    if (this.isCancelled()) {
                        return;
                    }
                    assert (folder.isFolder());
                    Enumeration filesInFolder = folder.getData(false);
                    while (filesInFolder.hasMoreElements()) {
                        FileObject file = (FileObject)filesInFolder.nextElement();
                        if (file.isFolder() || !matcher.accept(file.getNameExt())) continue;
                        Project project = FileOwnerQuery.getOwner((FileObject)file);
                        boolean preferred = false;
                        String relativePath = null;
                        if (project != null) {
                            FileObject pd = project.getProjectDirectory();
                            preferred = request.getCurrentProject() != null ? pd == request.getCurrentProject().getProjectDirectory() : false;
                            relativePath = FileUtil.getRelativePath((FileObject)pd, (FileObject)file);
                        }
                        if (relativePath == null) {
                            relativePath = "";
                        }
                        FileDescription fd = new FileDescription(file, relativePath, project, request.getLine());
                        FileProviderAccessor.getInstance().setFromCurrentProject(fd, preferred);
                        files.add(fd);
                    }
                    request.exclude(folder);
                }
                if (files.isEmpty()) continue;
                worker.emit(files);
                files.clear();
            }
        }

        @NonNull
        private Collection<FileObject> searchSources(@NonNull FileObject root, @NonNull Collection<FileObject> result, @NonNull Request request, @NonNull List<SearchFilter> filters) {
            if (this.isCancelled() || root.getChildren().length == 0 || request.isExcluded(root) || !this.checkAgainstFilters(root, filters)) {
                return result;
            }
            result.add(root);
            Enumeration subFolders = root.getFolders(false);
            while (subFolders.hasMoreElements()) {
                this.searchSources((FileObject)subFolders.nextElement(), result, request, filters);
            }
            return result;
        }

        private boolean checkAgainstFilters(FileObject folder, List<SearchFilter> filters) {
            assert (folder.isFolder());
            for (SearchFilter filter : filters) {
                if (filter.traverseFolder(folder) != SearchFilter.FolderResult.DO_NOT_TRAVERSE) continue;
                return false;
            }
            return true;
        }
    }

    private static final class IndexStrategy
    extends Strategy {
        private IndexStrategy() {
            super();
        }

        @Override
        void execute(@NonNull Request request, @NonNull Worker worker) {
            if (this.isCancelled()) {
                return;
            }
            Pair<String, String> query = this.createQuery(request);
            Map<Project, Collection<FileObject>> rbp = this.collectRoots(request);
            try {
                for (Project p : request.getOpenProjects()) {
                    Collection<FileObject> roots = rbp.get((Object)p);
                    if (roots == null) continue;
                    this.doQuery(query, request, worker, IndexStrategy.filterExcluded(roots, request));
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }

        @NonNull
        private Map<Project, Collection<FileObject>> collectRoots(@NonNull Request request) {
            return QuerySupport.findRoots((Collection)request.getOpenProjects(), (Collection)null, Collections.emptyList(), Collections.emptyList());
        }

        private boolean doQuery(@NonNull Pair<String, String> query, @NonNull Request request, @NonNull Worker worker, @NonNull Collection<? extends FileObject> roots) throws IOException {
            if (this.isCancelled()) {
                return false;
            }
            QuerySupport q = QuerySupport.forRoots((String)"org-netbeans-modules-jumpto-file-FileIndexer", (int)1, (FileObject[])roots.toArray((T[])new FileObject[roots.size()]));
            if (this.isCancelled()) {
                return false;
            }
            ArrayList<FileDescription> files = new ArrayList<FileDescription>();
            Collection results = q.query((String)query.first(), (String)query.second(), request.getSearchKind(), new String[0]);
            for (IndexResult r : results) {
                FileObject file = r.getFile();
                if (file == null || !file.isValid()) continue;
                Project project = FileOwnerQuery.getOwner((FileObject)file);
                FileDescription fd = new FileDescription(file, r.getRelativePath().substring(0, Math.max(r.getRelativePath().length() - file.getNameExt().length() - 1, 0)), project, request.getLine());
                boolean preferred = project != null && request.getCurrentProject() != null ? project.getProjectDirectory() == request.getCurrentProject().getProjectDirectory() : false;
                FileProviderAccessor.getInstance().setFromCurrentProject(fd, preferred);
                files.add(fd);
                LOG.log(Level.FINER, "Found: {0}, project={1}, currentProject={2}, preferred={3}", new Object[]{file.getPath(), project, request.getCurrentProject(), preferred});
            }
            for (FileObject root : roots) {
                request.exclude(root);
            }
            worker.emit(files);
            return true;
        }

        @NonNull
        private Pair<String, String> createQuery(@NonNull Request request) {
            String searchField;
            String indexQueryText;
            switch (request.getSearchKind()) {
                case CASE_INSENSITIVE_PREFIX: {
                    searchField = "ci-file-name";
                    indexQueryText = request.getText();
                    break;
                }
                case CASE_INSENSITIVE_REGEXP: {
                    searchField = "ci-file-name";
                    indexQueryText = NameMatcherFactory.wildcardsToRegexp(request.getText(), true);
                    Pattern.compile(indexQueryText);
                    break;
                }
                case REGEXP: {
                    searchField = "file-name";
                    indexQueryText = NameMatcherFactory.wildcardsToRegexp(request.getText(), true);
                    Pattern.compile(indexQueryText);
                    break;
                }
                default: {
                    searchField = "file-name";
                    indexQueryText = request.getText();
                }
            }
            return Pair.of((Object)searchField, (Object)indexQueryText);
        }

        @NonNull
        private static Collection<FileObject> filterExcluded(@NonNull Collection<? extends FileObject> roots, @NonNull Request request) {
            ArrayList<FileObject> result = new ArrayList<FileObject>(roots.size());
            for (FileObject root : roots) {
                if (request.isExcluded(root)) continue;
                result.add(root);
            }
            return result;
        }
    }

    private static final class ProviderStrategy
    extends Strategy {
        private List<? extends FileProvider> providers;
        private volatile FileProvider currentProvider;

        private ProviderStrategy() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        void execute(@NonNull Request request, @NonNull Worker worker) {
            if (this.isCancelled()) {
                return;
            }
            ArrayList files = new ArrayList();
            SearchType jumpToSearchType = ProviderStrategy.toJumpToSearchType(request.getSearchKind());
            FileProvider.Context ctx = FileProviderAccessor.getInstance().createContext(request.getText(), jumpToSearchType, request.getLine(), request.getCurrentProject());
            FileProvider.Result fpR = FileProviderAccessor.getInstance().createResult(files, new String[1], ctx);
            for (FileObject root : request.getSourceRoots()) {
                if (request.isExcluded(root)) continue;
                FileProviderAccessor.getInstance().setRoot(ctx, root);
                boolean recognized = false;
                for (FileProvider provider : this.getProviders()) {
                    if (this.isCancelled()) {
                        return;
                    }
                    this.currentProvider = provider;
                    try {
                        recognized = provider.computeFiles(ctx, fpR);
                        if (!recognized) continue;
                        break;
                    }
                    finally {
                        this.currentProvider = null;
                        continue;
                    }
                }
                if (recognized) {
                    request.exclude(root);
                }
                if (files.isEmpty()) continue;
                worker.emit(files);
                files.clear();
            }
        }

        @Override
        void cancel() {
            super.cancel();
            FileProvider fp = this.currentProvider;
            if (fp != null) {
                fp.cancel();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Iterable<? extends FileProvider> getProviders() {
            ProviderStrategy providerStrategy = this;
            synchronized (providerStrategy) {
                if (this.providers != null) {
                    return this.providers;
                }
            }
            ArrayList<FileProvider> result = new ArrayList<FileProvider>();
            for (FileProviderFactory fpf : Lookup.getDefault().lookupAll(FileProviderFactory.class)) {
                result.add(fpf.createFileProvider());
            }
            ProviderStrategy i$ = this;
            synchronized (i$) {
                if (this.providers == null) {
                    this.providers = Collections.unmodifiableList(result);
                }
                return this.providers;
            }
        }
    }

    private static abstract class Strategy {
        private volatile boolean cancelled;

        private Strategy() {
        }

        @CheckForNull
        abstract void execute(@NonNull Request var1, @NonNull Worker var2);

        void cancel() {
            this.cancelled = true;
        }

        final boolean isCancelled() {
            return this.cancelled;
        }

        static SearchType toJumpToSearchType(QuerySupport.Kind searchType) {
            switch (searchType) {
                case CAMEL_CASE: 
                case CASE_INSENSITIVE_CAMEL_CASE: {
                    return SearchType.CAMEL_CASE;
                }
                case CASE_INSENSITIVE_PREFIX: {
                    return SearchType.CASE_INSENSITIVE_PREFIX;
                }
                case CASE_INSENSITIVE_REGEXP: {
                    return SearchType.CASE_INSENSITIVE_REGEXP;
                }
                case EXACT: {
                    return SearchType.EXACT_NAME;
                }
                case PREFIX: {
                    return SearchType.PREFIX;
                }
                case REGEXP: {
                    return SearchType.REGEXP;
                }
            }
            throw new IllegalArgumentException();
        }
    }

    static final class Collector {
        private final Models.MutableListModel<FileDescriptor> model;
        private final Runnable updateCallBack;
        private final Runnable doneCallBack;
        private final long startTime;
        private final Set<Worker> active = Collections.newSetFromMap(new ConcurrentHashMap());
        private volatile boolean frozen;

        private Collector(@NonNull Models.MutableListModel<FileDescriptor> model, @NonNull Runnable updateCallBack, @NonNull Runnable doneCallBack, long startTime) {
            Parameters.notNull((CharSequence)"model", model);
            Parameters.notNull((CharSequence)"updateCallBack", (Object)updateCallBack);
            Parameters.notNull((CharSequence)"doneCallBack", (Object)doneCallBack);
            this.model = model;
            this.updateCallBack = updateCallBack;
            this.doneCallBack = doneCallBack;
            this.startTime = startTime;
        }

        public String toString() {
            return String.format("%s (%d) [frozen: %s, active: %s]", this.getClass().getSimpleName(), System.identityHashCode(this), this.frozen, this.active);
        }

        boolean isDone() {
            return this.frozen && this.active.isEmpty();
        }

        private void configure(@NonNull Worker worker) {
            Parameters.notNull((CharSequence)"worker", (Object)worker);
            if (this.frozen) {
                throw new IllegalStateException(String.format("Adding worker: %s to already frozen collector: %s", worker, this));
            }
            if (!this.active.add(worker)) {
                throw new IllegalArgumentException(String.format("Adding already added worker: %s to collector: %s", worker, this));
            }
        }

        private void start(@NonNull Worker worker) {
            Parameters.notNull((CharSequence)"worker", (Object)worker);
            this.frozen = true;
        }

        private void emit(@NonNull Worker worker, @NonNull List<? extends FileDescriptor> files) {
            Parameters.notNull((CharSequence)"worker", (Object)worker);
            Parameters.notNull((CharSequence)"files", files);
            this.model.add(files);
            this.updateCallBack.run();
        }

        private void done(@NonNull Worker worker) {
            Parameters.notNull((CharSequence)"worker", (Object)worker);
            if (!this.active.remove(worker)) {
                throw new IllegalStateException(String.format("Trying to removed unknown worker: %s from collector: %s", worker, this));
            }
            if (this.active.isEmpty()) {
                this.doneCallBack.run();
            }
        }
    }

    static final class Request {
        private final String text;
        private final QuerySupport.Kind searchType;
        private final Project currentProject;
        private final int lineNr;
        private final Set<FileObject> excludes;
        private Collection<? extends FileObject> sgRoots;
        private Collection<? extends Project> projects;

        private Request(@NonNull String text, @NonNull QuerySupport.Kind searchType, @NullAllowed Project currentProject, int lineNr) {
            Parameters.notNull((CharSequence)"text", (Object)text);
            Parameters.notNull((CharSequence)"searchType", (Object)searchType);
            this.text = text;
            this.searchType = searchType;
            this.currentProject = currentProject;
            this.lineNr = lineNr;
            this.excludes = Collections.newSetFromMap(new ConcurrentHashMap());
        }

        @NonNull
        String getText() {
            return this.text;
        }

        @NonNull
        QuerySupport.Kind getSearchKind() {
            return this.searchType;
        }

        @CheckForNull
        Project getCurrentProject() {
            return this.currentProject;
        }

        int getLine() {
            return this.lineNr;
        }

        public String toString() {
            return String.format("%s[text: %s, search kind: %s, project: %s, line: %d]", new Object[]{this.getClass().getSimpleName(), this.text, this.searchType, this.currentProject, this.lineNr});
        }

        @NonNull
        private synchronized Collection<? extends Project> getOpenProjects() {
            if (this.projects == null) {
                Project[] opa = OpenProjects.getDefault().getOpenProjects();
                ArrayList<Project> pl = new ArrayList<Project>(opa.length);
                if (this.currentProject != null) {
                    pl.add(this.currentProject);
                }
                for (Project p : opa) {
                    Project getRidOfFod = (Project)p.getLookup().lookup(Project.class);
                    if (getRidOfFod != null) {
                        p = getRidOfFod;
                    }
                    if (Objects.equals((Object)p, (Object)this.currentProject)) continue;
                    pl.add(p);
                }
                this.projects = Collections.unmodifiableCollection(pl);
            }
            return this.projects;
        }

        private synchronized Collection<? extends FileObject> getSourceRoots() {
            if (this.sgRoots == null) {
                Collection<? extends Project> projects = this.getOpenProjects();
                ArrayList<FileObject> newSgRoots = new ArrayList<FileObject>();
                for (Project p : projects) {
                    for (SourceGroup group : ProjectUtils.getSources((Project)p).getSourceGroups("generic")) {
                        newSgRoots.add(group.getRootFolder());
                    }
                }
                this.sgRoots = Collections.unmodifiableCollection(newSgRoots);
            }
            return this.sgRoots;
        }

        private boolean isExcluded(@NonNull FileObject file) {
            return this.excludes.contains((Object)file);
        }

        private void exclude(@NonNull FileObject file) {
            this.excludes.add(file);
        }
    }

    static enum Type {
        PROVIDER{

            @NonNull
            @Override
            Strategy createStrategy() {
                return new ProviderStrategy();
            }
        }
        ,
        INDEX{

            @NonNull
            @Override
            Strategy createStrategy() {
                return new IndexStrategy();
            }
        }
        ,
        FS{

            @NonNull
            @Override
            Strategy createStrategy() {
                return new FSStrategy();
            }
        };
        

        private Type() {
        }

        @NonNull
        abstract Strategy createStrategy();

    }

}

