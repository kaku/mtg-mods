/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.jumpto.file;

import org.netbeans.modules.jumpto.EntityComparator;
import org.netbeans.modules.jumpto.file.FileProviderAccessor;
import org.netbeans.spi.jumpto.file.FileDescriptor;

public class FileComarator
extends EntityComparator<FileDescriptor> {
    private boolean usePrefered;
    private boolean caseSensitive;

    public FileComarator(boolean usePrefered, boolean caseSensitive) {
        this.usePrefered = usePrefered;
        this.caseSensitive = caseSensitive;
    }

    @Override
    public int compare(FileDescriptor e1, FileDescriptor e2) {
        int result;
        String e2projectName;
        String e1projectName;
        if (this.usePrefered) {
            FileProviderAccessor fpa = FileProviderAccessor.getInstance();
            boolean isE1Curr = fpa.isFromCurrentProject(e1);
            boolean isE2Curr = fpa.isFromCurrentProject(e2);
            if (isE1Curr && !isE2Curr) {
                return -1;
            }
            if (!isE1Curr && isE2Curr) {
                return 1;
            }
        }
        if ((result = this.compareProjects(e1projectName = e1.getProjectName(), e2projectName = e2.getProjectName())) != 0) {
            return result;
        }
        int r = this.compare(e1.getFileName(), e2.getFileName(), this.caseSensitive);
        if (r != 0) {
            return r;
        }
        r = this.compare(e1.getProjectName(), e2.getProjectName(), this.caseSensitive);
        if (r != 0) {
            return r;
        }
        r = this.compare(e1.getOwnerPath(), e2.getOwnerPath(), this.caseSensitive);
        return r;
    }
}

