/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Editable
 *  org.netbeans.api.actions.Openable
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.openide.cookies.LineCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.text.Line$ShowOpenType
 *  org.openide.text.Line$ShowVisibilityType
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.jumpto.file;

import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.actions.Editable;
import org.netbeans.api.actions.Openable;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.jumpto.file.FileDescriptor;
import org.openide.cookies.LineCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public class FileDescription
extends FileDescriptor {
    private static final Logger LOG = Logger.getLogger(FileDescription.class.getName());
    public static ImageIcon UNKNOWN_PROJECT_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/resources/find.gif", (boolean)false);
    private final FileObject fileObject;
    private final String ownerPath;
    private final Project project;
    private final int lineNr;
    private volatile Icon icon;
    private volatile String projectName;
    private volatile Icon projectIcon;
    private volatile ProjectInformation projectInfo;

    public FileDescription(@NonNull FileObject file, @NonNull String ownerPath, @NullAllowed Project project, int lineNr) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        Parameters.notNull((CharSequence)"ownerPath", (Object)ownerPath);
        this.fileObject = file;
        this.ownerPath = ownerPath;
        this.project = project;
        this.lineNr = lineNr;
    }

    @Override
    public String getFileName() {
        return this.fileObject.getNameExt();
    }

    @NonNull
    @Override
    public Icon getIcon() {
        Icon res = this.icon;
        if (res == null) {
            DataObject od = this.getDataObject();
            Image img = od == null ? UNKNOWN_PROJECT_ICON.getImage() : od.getNodeDelegate().getIcon(1);
            res = this.icon = new ImageIcon(img);
        }
        return res;
    }

    @Override
    public String getOwnerPath() {
        return this.ownerPath;
    }

    @NonNull
    @Override
    public String getProjectName() {
        String res = this.projectName;
        if (res == null) {
            ProjectInformation pi = this.getProjectInfo();
            this.projectName = pi == null ? "" : pi.getDisplayName();
            res = this.projectName;
        }
        return res;
    }

    @NonNull
    @Override
    public Icon getProjectIcon() {
        Icon res = this.projectIcon;
        if (res == null) {
            ProjectInformation pi = this.getProjectInfo();
            this.projectIcon = pi == null ? UNKNOWN_PROJECT_ICON : pi.getIcon();
            res = this.projectIcon;
        }
        return res;
    }

    @Override
    public void open() {
        DataObject od = this.getDataObject();
        if (od != null) {
            Editable editable;
            OpenCookie oc;
            LineCookie lineCookie = (LineCookie)od.getLookup().lookup(LineCookie.class);
            if (lineCookie != null && this.lineNr != -1) {
                try {
                    Line l = lineCookie.getLineSet().getCurrent(this.lineNr - 1);
                    if (l != null) {
                        l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS, -1);
                        return;
                    }
                }
                catch (IndexOutOfBoundsException oob) {
                    LOG.log(Level.FINE, "Line no more exists.", oob);
                }
            }
            if ((editable = (Editable)od.getLookup().lookup(Editable.class)) != null) {
                editable.edit();
                return;
            }
            Openable openable = (Openable)od.getLookup().lookup(Openable.class);
            if (openable != null) {
                openable.open();
            }
            if ((oc = (OpenCookie)od.getLookup().lookup(OpenCookie.class)) != null) {
                oc.open();
            }
        }
    }

    @Override
    public FileObject getFileObject() {
        return this.fileObject;
    }

    private DataObject getDataObject() {
        try {
            FileObject fo = this.getFileObject();
            return DataObject.find((FileObject)fo);
        }
        catch (DataObjectNotFoundException ex) {
            return null;
        }
    }

    @CheckForNull
    private ProjectInformation getProjectInfo() {
        if (this.project == null) {
            return null;
        }
        ProjectInformation res = this.projectInfo;
        if (res == null) {
            res = this.projectInfo = (ProjectInformation)this.project.getLookup().lookup(ProjectInformation.class);
        }
        return res;
    }
}

