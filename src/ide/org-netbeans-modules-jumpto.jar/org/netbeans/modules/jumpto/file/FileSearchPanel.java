/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.openide.awt.Mnemonics
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.NbCollections
 */
package org.netbeans.modules.jumpto.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.modules.jumpto.SearchHistory;
import org.netbeans.modules.jumpto.common.UiUtils;
import org.netbeans.modules.jumpto.file.FileSearchAction;
import org.netbeans.modules.jumpto.file.FileSearchOptions;
import org.netbeans.spi.jumpto.file.FileDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbCollections;

public class FileSearchPanel
extends JPanel
implements ActionListener {
    public static final String SEARCH_IN_PROGRES = NbBundle.getMessage(FileSearchPanel.class, (String)"TXT_SearchingOtherProjects");
    private static Icon WARN_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/resources/warning.png", (boolean)false);
    private static final Logger LOG = Logger.getLogger(FileSearchPanel.class.getName());
    private static final int BRIGHTER_COLOR_COMPONENT = 10;
    private final ContentProvider contentProvider;
    private final Project currentProject;
    private boolean containsScrollPane;
    private JLabel messageLabel;
    private String oldText;
    private List<?> selectedItems;
    long time;
    private FileDescriptor[] selectedFile;
    private final SearchHistory searchHistory;
    private boolean pastedFromClipboard = false;
    private JCheckBox caseSensitiveCheckBox;
    private JLabel fileNameLabel;
    private JTextField fileNameTextField;
    private JCheckBox hiddenFilesCheckBox;
    private JLabel jLabelLocation;
    private JLabel jLabelWarningMessage;
    private JTextField jTextFieldLocation;
    private JPanel listPanel;
    private JCheckBox mainProjectCheckBox;
    private JLabel resultLabel;
    private JList resultList;
    private JScrollPane resultScrollPane;

    public FileSearchPanel(ContentProvider contentProvider, Project currentProject) {
        this.contentProvider = contentProvider;
        this.currentProject = currentProject;
        this.initComponents();
        ((AbstractDocument)this.fileNameTextField.getDocument()).setDocumentFilter(UiUtils.newUserInputFilter());
        this.containsScrollPane = true;
        Color bgColorBrighter = new Color(Math.min(this.getBackground().getRed() + 10, 255), Math.min(this.getBackground().getGreen() + 10, 255), Math.min(this.getBackground().getBlue() + 10, 255));
        this.messageLabel = new JLabel();
        this.messageLabel.setBackground(bgColorBrighter);
        this.messageLabel.setHorizontalAlignment(0);
        this.messageLabel.setEnabled(true);
        this.messageLabel.setText(NbBundle.getMessage(FileSearchPanel.class, (String)"TXT_NoTypesFound"));
        this.messageLabel.setFont(this.resultList.getFont());
        this.caseSensitiveCheckBox.setSelected(FileSearchOptions.getCaseSensitive());
        this.hiddenFilesCheckBox.setSelected(FileSearchOptions.getShowHiddenFiles());
        this.mainProjectCheckBox.setSelected(FileSearchOptions.getPreferMainProject());
        if (currentProject == null) {
            this.mainProjectCheckBox.setEnabled(false);
            this.mainProjectCheckBox.setSelected(false);
        } else {
            ProjectInformation pi = (ProjectInformation)currentProject.getLookup().lookup(ProjectInformation.class);
            this.mainProjectCheckBox.setText(NbBundle.getMessage(FileSearchPanel.class, (String)"FMT_CurrentProjectLabel", (Object)pi.getDisplayName()));
        }
        this.mainProjectCheckBox.addActionListener(this);
        this.caseSensitiveCheckBox.addActionListener(this);
        this.hiddenFilesCheckBox.addActionListener(this);
        this.hiddenFilesCheckBox.setVisible(false);
        this.resultList.setCellRenderer(contentProvider.getListCellRenderer(this.resultList, this.fileNameTextField.getDocument(), this.caseSensitiveCheckBox.getModel()));
        this.resultList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                FileSearchPanel.this.selectedItems = FileSearchPanel.this.resultList.getSelectedValuesList();
                LOG.log(Level.FINE, "New selected items: {0}", FileSearchPanel.this.selectedItems);
            }
        });
        contentProvider.setListModel(this, null);
        this.fileNameTextField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void changedUpdate(DocumentEvent e) {
                FileSearchPanel.this.update();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                FileSearchPanel.this.update();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (FileSearchPanel.this.pastedFromClipboard) {
                    FileSearchPanel.this.pastedFromClipboard = false;
                } else {
                    FileSearchPanel.this.update();
                }
            }
        });
        this.searchHistory = new SearchHistory(FileSearchPanel.class, this.fileNameTextField);
    }

    @Override
    public void removeNotify() {
        this.searchHistory.saveHistory();
        super.removeNotify();
    }

    void setModel(final @NonNull ListModel model, final boolean done) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                LOG.log(Level.FINE, "Reset selected items");
                FileSearchPanel.this.selectedItems = null;
                FileSearchPanel.this.resultList.setModel(model);
                ((FileSearchAction.Renderer)FileSearchPanel.this.resultList.getCellRenderer()).setColorPrefered(FileSearchPanel.this.isPreferedProject());
                if (done) {
                    FileSearchPanel.this.setListPanelContent(null, false);
                }
            }
        });
    }

    void searchProgress() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                if (FileSearchPanel.this.resultList.getModel().getSize() > 0) {
                    if (!FileSearchPanel.this.containsScrollPane) {
                        FileSearchPanel.this.setListPanelContent(null, false);
                        FileSearchPanel.this.setWarning(NbBundle.getMessage(FileSearchPanel.class, (String)"TXT_PartialResults"));
                        int index = FileSearchPanel.this.resultList.getSelectedIndex();
                        if (index == -1) {
                            LOG.log(Level.FINE, "Select first item.");
                            FileSearchPanel.this.resultList.setSelectedIndex(0);
                        }
                    } else if (FileSearchPanel.this.selectedItems != null && !FileSearchPanel.this.selectedItems.isEmpty()) {
                        LOG.log(Level.FINE, "Reselect selected items");
                        int[] indexes = new int[FileSearchPanel.this.selectedItems.size()];
                        ListModel model = FileSearchPanel.this.resultList.getModel();
                        int startj = 0;
                        int i = 0;
                        for (Object selectedItem : FileSearchPanel.this.selectedItems) {
                            for (int j = startj; j < model.getSize(); ++j) {
                                if (selectedItem != model.getElementAt(j)) continue;
                                startj = j;
                                indexes[i] = j;
                                break;
                            }
                            ++i;
                        }
                        FileSearchPanel.this.resultList.setSelectedIndices(indexes);
                        FileSearchPanel.this.resultList.ensureIndexIsVisible(indexes[0]);
                    }
                }
            }
        });
    }

    void searchCompleted() {
        Mutex.EVENT.readAccess(new Runnable(){

            @Override
            public void run() {
                if (FileSearchPanel.this.resultList.getModel().getSize() > 0) {
                    FileSearchPanel.this.setWarning(null);
                } else {
                    try {
                        Pattern.compile(FileSearchPanel.this.getText().replace(".", "\\.").replace("*", ".*").replace('?', '.'), 2);
                        FileSearchPanel.this.setListPanelContent(NbBundle.getMessage(FileSearchPanel.class, (String)"TXT_NoTypesFound"), false);
                    }
                    catch (PatternSyntaxException pse) {
                        FileSearchPanel.this.setListPanelContent(NbBundle.getMessage(FileSearchPanel.class, (String)"TXT_SyntaxError", (Object)pse.getDescription(), (Object)pse.getIndex()), false);
                    }
                }
            }
        });
    }

    void setListPanelContent(String message, boolean waitIcon) {
        if (message == null && !this.containsScrollPane) {
            this.listPanel.remove(this.messageLabel);
            this.listPanel.add(this.resultScrollPane);
            this.containsScrollPane = true;
            this.revalidate();
            this.repaint();
        } else if (message != null) {
            this.jTextFieldLocation.setText("");
            this.messageLabel.setText(message);
            this.messageLabel.setIcon(waitIcon ? FileSearchAction.Renderer.WAIT_ICON : null);
            if (this.containsScrollPane) {
                this.listPanel.remove(this.resultScrollPane);
                this.listPanel.add(this.messageLabel);
                this.containsScrollPane = false;
            }
            this.revalidate();
            this.repaint();
        }
    }

    public boolean isShowHiddenFiles() {
        return this.hiddenFilesCheckBox.isSelected();
    }

    public boolean isPreferedProject() {
        return this.mainProjectCheckBox.isSelected();
    }

    public boolean isCaseSensitive() {
        return this.caseSensitiveCheckBox.isSelected();
    }

    private void update() {
        this.time = System.currentTimeMillis();
        String text = this.getText();
        if (this.oldText == null || this.oldText.trim().length() == 0 || !text.startsWith(this.oldText)) {
            this.setListPanelContent(NbBundle.getMessage(FileSearchPanel.class, (String)"TXT_Searching"), true);
        }
        this.oldText = text;
        this.contentProvider.setListModel(this, text);
    }

    private void setWarning(String warningMessage) {
        if (warningMessage != null) {
            this.jLabelWarningMessage.setIcon(WARN_ICON);
            this.jLabelWarningMessage.setBorder(BorderFactory.createEmptyBorder(3, 1, 1, 1));
        } else {
            this.jLabelWarningMessage.setIcon(null);
            this.jLabelWarningMessage.setBorder(null);
        }
        this.jLabelWarningMessage.setText(warningMessage);
    }

    private void initComponents() {
        this.fileNameLabel = new JLabel();
        this.fileNameTextField = new JTextField();
        this.resultLabel = new JLabel();
        this.listPanel = new JPanel();
        this.resultScrollPane = new JScrollPane();
        this.resultList = new JList();
        this.jLabelWarningMessage = new JLabel();
        this.caseSensitiveCheckBox = new JCheckBox();
        this.hiddenFilesCheckBox = new JCheckBox();
        this.mainProjectCheckBox = new JCheckBox();
        this.jLabelLocation = new JLabel();
        this.jTextFieldLocation = new JTextField();
        this.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        this.setPreferredSize(new Dimension(540, 280));
        this.setLayout(new GridBagLayout());
        this.fileNameLabel.setFont(this.fileNameLabel.getFont());
        this.fileNameLabel.setLabelFor(this.fileNameTextField);
        Mnemonics.setLocalizedText((JLabel)this.fileNameLabel, (String)NbBundle.getMessage(FileSearchPanel.class, (String)"CTL_FileName"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 0, 4, 0);
        this.add((Component)this.fileNameLabel, gridBagConstraints);
        this.fileNameTextField.setFont(new Font("Monospaced", 0, this.getFontSize()));
        this.fileNameTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FileSearchPanel.this.fileNameTextFieldActionPerformed(evt);
            }
        });
        this.fileNameTextField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                FileSearchPanel.this.fileNameTextFieldKeyPressed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(0, 0, 7, 0);
        this.add((Component)this.fileNameTextField, gridBagConstraints);
        this.fileNameTextField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileSearchPanel.class, (String)"AN_SearchText"));
        this.fileNameTextField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchPanel.class, (String)"AD_SearchText"));
        this.resultLabel.setLabelFor(this.resultList);
        Mnemonics.setLocalizedText((JLabel)this.resultLabel, (String)NbBundle.getMessage(FileSearchPanel.class, (String)"CTL_MatchingFiles"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(0, 0, 4, 0);
        this.add((Component)this.resultLabel, gridBagConstraints);
        this.listPanel.setBorder(BorderFactory.createEtchedBorder());
        this.listPanel.setLayout(new BorderLayout());
        this.resultScrollPane.setBorder(null);
        this.resultList.setFont(new Font("Monospaced", 0, this.getFontSize()));
        this.resultList.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent evt) {
                FileSearchPanel.this.resultListMouseReleased(evt);
            }
        });
        this.resultList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent evt) {
                FileSearchPanel.this.resultListValueChanged(evt);
            }
        });
        this.resultScrollPane.setViewportView(this.resultList);
        this.resultList.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileSearchPanel.class, (String)"AN_MatchingList"));
        this.resultList.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchPanel.class, (String)"AD_MatchingList"));
        this.listPanel.add((Component)this.resultScrollPane, "Center");
        this.listPanel.add((Component)this.jLabelWarningMessage, "Last");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 8, 0);
        this.add((Component)this.listPanel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.caseSensitiveCheckBox, (String)NbBundle.getMessage(FileSearchPanel.class, (String)"LBL_CaseSensitive"));
        this.caseSensitiveCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 18;
        this.add((Component)this.caseSensitiveCheckBox, gridBagConstraints);
        this.caseSensitiveCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchPanel.class, (String)"AD_CaseSensitive"));
        Mnemonics.setLocalizedText((AbstractButton)this.hiddenFilesCheckBox, (String)NbBundle.getMessage(FileSearchPanel.class, (String)"LBL_HiddenFiles"));
        this.hiddenFilesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 8, 0, 0);
        this.add((Component)this.hiddenFilesCheckBox, gridBagConstraints);
        this.hiddenFilesCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchPanel.class, (String)"AD_HiddenFiles"));
        Mnemonics.setLocalizedText((AbstractButton)this.mainProjectCheckBox, (String)NbBundle.getMessage(FileSearchPanel.class, (String)"LBL_PreferMainProject"));
        this.mainProjectCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 8, 0, 0);
        this.add((Component)this.mainProjectCheckBox, gridBagConstraints);
        this.mainProjectCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchPanel.class, (String)"AD_PreferMainProject"));
        this.jLabelLocation.setLabelFor(this.jTextFieldLocation);
        Mnemonics.setLocalizedText((JLabel)this.jLabelLocation, (String)NbBundle.getMessage(FileSearchPanel.class, (String)"LBL_Location"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(8, 0, 4, 0);
        this.add((Component)this.jLabelLocation, gridBagConstraints);
        this.jLabelLocation.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FileSearchPanel.class, (String)"AN_Location"));
        this.jLabelLocation.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FileSearchPanel.class, (String)"AD_Location"));
        this.jTextFieldLocation.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 8, 0);
        this.add((Component)this.jTextFieldLocation, gridBagConstraints);
    }

    private void fileNameTextFieldActionPerformed(ActionEvent evt) {
        if (this.contentProvider.hasValidContent()) {
            this.contentProvider.closeDialog();
            this.setSelectedFile();
        }
    }

    private void resultListMouseReleased(MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            this.fileNameTextFieldActionPerformed(null);
        }
    }

    private void resultListValueChanged(ListSelectionEvent evt) {
        Object svObject = this.resultList.getSelectedValue();
        if (svObject instanceof FileDescriptor) {
            this.jTextFieldLocation.setText(((FileDescriptor)svObject).getFileDisplayPath());
        } else {
            this.jTextFieldLocation.setText("");
        }
    }

    private void fileNameTextFieldKeyPressed(KeyEvent evt) {
        Object actionKey = this.resultList.getInputMap().get(KeyStroke.getKeyStrokeForEvent(evt));
        boolean isListScrollAction = "selectPreviousRow".equals(actionKey) || "selectPreviousRowExtendSelection".equals(actionKey) || "selectNextRow".equals(actionKey) || "selectNextRowExtendSelection".equals(actionKey) || "scrollUp".equals(actionKey) || "scrollUpExtendSelection".equals(actionKey) || "scrollDown".equals(actionKey) || "scrollDownExtendSelection".equals(actionKey);
        int selectedIndex = this.resultList.getSelectedIndex();
        ListModel model = this.resultList.getModel();
        int modelSize = model.getSize();
        if ("selectNextRow".equals(actionKey) && (selectedIndex == modelSize - 1 || selectedIndex == modelSize - 2 && model.getElementAt(modelSize - 1) == SEARCH_IN_PROGRES)) {
            this.resultList.setSelectedIndex(0);
            this.resultList.ensureIndexIsVisible(0);
            return;
        }
        if ("selectPreviousRow".equals(actionKey) && selectedIndex == 0) {
            int last = modelSize - 1;
            if (model.getElementAt(last) == SEARCH_IN_PROGRES) {
                --last;
            }
            this.resultList.setSelectedIndex(last);
            this.resultList.ensureIndexIsVisible(last);
            return;
        }
        if (isListScrollAction) {
            Action a = this.resultList.getActionMap().get(actionKey);
            a.actionPerformed(new ActionEvent(this.resultList, 0, (String)actionKey));
            evt.consume();
        } else {
            String action;
            Object o = this.fileNameTextField.getInputMap().get(KeyStroke.getKeyStrokeForEvent(evt));
            if (o instanceof String && "paste-from-clipboard".equals(action = (String)o)) {
                String selectedTxt = this.fileNameTextField.getSelectedText();
                String txt = this.fileNameTextField.getText();
                if (selectedTxt != null && txt != null && selectedTxt.length() == txt.length()) {
                    this.pastedFromClipboard = true;
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.caseSensitiveCheckBox) {
            FileSearchOptions.setCaseSensitive(this.caseSensitiveCheckBox.isSelected());
        } else if (e.getSource() == this.hiddenFilesCheckBox) {
            FileSearchOptions.setShowHiddenFiles(this.hiddenFilesCheckBox.isSelected());
        } else if (e.getSource() == this.mainProjectCheckBox) {
            FileSearchOptions.setPreferMainProject(this.isPreferedProject());
        }
        this.update();
    }

    public void setInitialText(final String text) {
        this.oldText = text;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                String textInField = FileSearchPanel.this.fileNameTextField.getText();
                if (textInField == null || textInField.trim().length() == 0) {
                    FileSearchPanel.this.fileNameTextField.setText(text);
                    int len = FileSearchPanel.this.fileNameTextField.getText().length();
                    FileSearchPanel.this.fileNameTextField.setCaretPosition(len);
                    FileSearchPanel.this.fileNameTextField.setSelectionStart(0);
                    FileSearchPanel.this.fileNameTextField.setSelectionEnd(len);
                }
            }
        });
    }

    private String getText() {
        try {
            String text = this.fileNameTextField.getDocument().getText(0, this.fileNameTextField.getDocument().getLength());
            return text;
        }
        catch (BadLocationException ex) {
            return null;
        }
    }

    private int getFontSize() {
        return this.resultLabel.getFont().getSize();
    }

    public void setSelectedFile() {
        List list = NbCollections.checkedListByCopy(Arrays.asList(this.resultList.getSelectedValues()), FileDescriptor.class, (boolean)true);
        this.selectedFile = list.toArray(new FileDescriptor[0]);
    }

    public FileDescriptor[] getSelectedFiles() {
        return this.selectedFile;
    }

    public Project getCurrentProject() {
        return this.currentProject;
    }

    public static interface ContentProvider {
        public ListCellRenderer getListCellRenderer(@NonNull JList var1, @NonNull Document var2, @NonNull ButtonModel var3);

        public void setListModel(FileSearchPanel var1, String var2);

        public void closeDialog();

        public boolean hasValidContent();
    }

}

