/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.jumpto;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.openide.util.NbPreferences;

public final class SearchHistory {
    private static final Logger LOGGER = Logger.getLogger(SearchHistory.class.getName());
    private static final String SEARCH_HISTORY = "SearchHistory";
    private static final int HISTORY_LIMIT = 10;
    private static final String HISTORY_BACK = "historyBack";
    private static final String HISTORY_FORWARD = "historyForward";
    private static final String DELIMITER = ";";
    private final String historyKey;
    private final JTextField textField;
    private final List<String> data;
    private int offset = -1;

    public SearchHistory(Class clazz, JTextField textField) {
        this(clazz.getName(), textField);
    }

    public SearchHistory(String historyKey, JTextField textField) {
        assert (historyKey != null && historyKey.trim().length() > 0);
        assert (textField != null);
        this.historyKey = historyKey;
        this.textField = textField;
        this.data = this.readHistory();
        this.storeHistory();
        this.registerListeners();
    }

    public void saveHistory() {
        this.saveHistory(this.textField.getText());
    }

    public void saveHistory(String text) {
        this.addHistoryItem(text);
        while (this.data.size() > 10) {
            String removed = this.data.remove(this.getLastIndex());
            LOGGER.fine("History item removed: " + removed);
        }
        this.storeHistory();
    }

    private void addHistoryItem(String text) {
        this.addHistoryItem(text, MoveOffset.CURRENT);
    }

    private void addHistoryItem(String text, MoveOffset moveOffset) {
        if (text == null || text.trim().length() == 0) {
            LOGGER.fine("String to store is empty => ignoring.");
            return;
        }
        if (text.contains(";")) {
            LOGGER.fine("String to store contain delimeter => ignoring.");
            return;
        }
        if (this.data.remove(text)) {
            LOGGER.fine(String.format("Text %s already in history, removing and readding.", text));
        }
        this.data.add(this.getFirstIndex(), text);
        switch (moveOffset) {
            case NEXT: {
                this.offset = this.getFirstIndex() + 1;
                break;
            }
            case PREVIOUS: {
                this.offset = this.getLastIndex();
                break;
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("History item added: " + text);
            LOGGER.fine("History: " + this.data);
            LOGGER.fine(String.format("Offset %d, moved %s", new Object[]{this.offset, moveOffset}));
        }
    }

    private Preferences getPreferences() {
        return NbPreferences.forModule(SearchHistory.class).node("SearchHistory");
    }

    private void registerListeners() {
        this.textField.getInputMap().put(KeyStroke.getKeyStroke(38, 2, true), "historyBack");
        this.textField.getInputMap().put(KeyStroke.getKeyStroke(40, 2, true), "historyForward");
        this.textField.getActionMap().put("historyBack", new AbstractAction(){
            private static final long serialVersionUID = -29128233042020099L;

            @Override
            public void actionPerformed(ActionEvent e) {
                SearchHistory.this.navigateBack();
            }
        });
        this.textField.getActionMap().put("historyForward", new AbstractAction(){
            private static final long serialVersionUID = 2341042130613885L;

            @Override
            public void actionPerformed(ActionEvent e) {
                SearchHistory.this.navigateForward();
            }
        });
    }

    void navigateBack() {
        LOGGER.fine("History back called, offset is " + this.offset);
        if (this.data.size() == 0) {
            LOGGER.fine("No data in history.");
            return;
        }
        String cachedText = this.getCachedText();
        ++this.offset;
        if (this.offset > this.getLastIndex()) {
            this.offset = this.getFirstIndex();
        }
        this.navigate(cachedText, MoveOffset.NEXT);
        LOGGER.fine(String.format("History: %s, offset: %d", this.data, this.offset));
    }

    void navigateForward() {
        LOGGER.fine("History forward called, offset is " + this.offset);
        if (this.data.size() == 0) {
            LOGGER.fine("No data in history.");
            return;
        }
        String cachedText = this.getCachedText();
        --this.offset;
        if (this.offset < this.getFirstIndex()) {
            this.offset = this.getLastIndex();
        }
        this.navigate(cachedText, MoveOffset.PREVIOUS);
        LOGGER.fine(String.format("History: %s, offset: %d", this.data, this.offset));
    }

    private void navigate(String oldText, MoveOffset moveOffset) {
        assert (this.data.size() > 0);
        String newText = null;
        String userText = this.textField.getText();
        if (oldText != null && oldText.equals(userText)) {
            LOGGER.fine("Text not changed => not saving, just iterating.");
            newText = this.getCachedText();
            LOGGER.fine("New text is: " + newText);
        } else {
            LOGGER.fine("Text changed => remember the current one & set the last (or first) one to the text field.");
            int index = moveOffset.equals((Object)MoveOffset.PREVIOUS) ? this.getLastIndex() : this.getFirstIndex();
            newText = this.getCachedText(index);
            LOGGER.fine("New text is: " + newText);
            this.addHistoryItem(userText, moveOffset);
        }
        assert (newText != null && newText.trim().length() > 0);
        this.textField.setText(newText);
    }

    private List<String> readHistory() {
        String history = this.getPreferences().get(this.historyKey, null);
        if (history == null || history.trim().length() == 0) {
            LOGGER.fine("No history found");
            return new ArrayList<String>(20);
        }
        List deserialized = new ArrayList<String>(Arrays.asList(history.split(";")));
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("History found: " + deserialized);
        }
        deserialized = SearchHistory.trimToLimit(deserialized, 10);
        assert (deserialized.size() <= 10);
        return deserialized;
    }

    private void storeHistory() {
        assert (this.data.size() <= 10);
        StringBuilder serialized = new StringBuilder(200);
        boolean prepend = false;
        for (String item : this.data) {
            if (prepend) {
                serialized.append(";");
            }
            serialized.append(item);
            prepend = true;
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("History to save: " + serialized.toString());
        }
        this.getPreferences().put(this.historyKey, serialized.toString());
    }

    private int getFirstIndex() {
        return 0;
    }

    private int getLastIndex() {
        return this.data.size() - 1;
    }

    private String getCachedText() {
        return this.getCachedText(this.offset);
    }

    private String getCachedText(int offset) {
        if (offset >= this.getFirstIndex() && offset <= this.getLastIndex()) {
            return this.data.get(offset);
        }
        return null;
    }

    private static List<String> trimToLimit(List<String> list, int limit) {
        int size = list.size();
        return size > limit ? list.subList(size - limit, size) : list;
    }

    private static enum MoveOffset {
        CURRENT,
        NEXT,
        PREVIOUS;
        

        private MoveOffset() {
        }
    }

}

