/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.jumpto;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.jumpto.EntityComparator;

public abstract class EntitiesListCellRenderer
extends DefaultListCellRenderer
implements ChangeListener {
    private final String mainProjectName = EntityComparator.getMainProjectName();

    protected void setProjectName(JLabel jlPrj, String projectName) {
        if (this.isMainProject(projectName)) {
            jlPrj.setText(this.getBoldText(projectName));
        } else {
            jlPrj.setText(projectName);
        }
    }

    private String getBoldText(String text) {
        StringBuilder sb = new StringBuilder("<html><b>");
        sb.append(text);
        sb.append("</b></html>");
        return sb.toString();
    }

    private boolean isMainProject(String projectName) {
        return projectName != null && projectName.equals(this.mainProjectName);
    }
}

