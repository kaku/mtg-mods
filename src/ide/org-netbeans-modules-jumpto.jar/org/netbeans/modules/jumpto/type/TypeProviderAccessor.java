/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 */
package org.netbeans.modules.jumpto.type;

import java.util.Collection;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;

public abstract class TypeProviderAccessor {
    public static TypeProviderAccessor DEFAULT;

    public abstract TypeProvider.Context createContext(Project var1, String var2, SearchType var3);

    @NonNull
    public abstract TypeProvider.Result createResult(@NonNull Collection<? super TypeDescriptor> var1, @NonNull String[] var2, @NonNull TypeProvider.Context var3);

    public abstract int getRetry(TypeProvider.Result var1);

    @NonNull
    public abstract String getHighlightText(@NonNull TypeDescriptor var1);

    static {
        try {
            Class.forName(TypeProvider.Context.class.getName(), true, TypeProvider.Context.class.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

