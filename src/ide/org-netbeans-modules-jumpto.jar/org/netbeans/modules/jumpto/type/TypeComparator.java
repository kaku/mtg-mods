/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.jumpto.type;

import org.netbeans.modules.jumpto.EntityComparator;
import org.netbeans.spi.jumpto.type.TypeDescriptor;

public class TypeComparator
extends EntityComparator<TypeDescriptor> {
    private final boolean caseSensitive;

    public TypeComparator(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public TypeComparator() {
        this(true);
    }

    @Override
    public int compare(TypeDescriptor e1, TypeDescriptor e2) {
        String e2projectName;
        String e1projectName = e1.getProjectName();
        int result = this.compareProjects(e1projectName, e2projectName = e2.getProjectName());
        if (result != 0) {
            return result;
        }
        result = this.compare(e1.getTypeName(), e2.getTypeName(), this.caseSensitive);
        if (result != 0) {
            return result;
        }
        result = this.compare(e1.getOuterName(), e2.getOuterName());
        if (result != 0) {
            return result;
        }
        return this.compare(e1.getContextName(), e2.getContextName());
    }
}

