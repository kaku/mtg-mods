/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.jumpto.type;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public final class UiOptions {
    private UiOptions() {
    }

    public static final class GoToSymbolDialog {
        private static final String GO_TO_SYMBOL_DIALOG = "GoToSymbolDialog";
        private static final String CASE_SENSITIVE = "caseSensitive";
        private static final String WIDTH = "width";
        private static final String HEIGHT = "height";
        private static Preferences node;

        public static boolean getCaseSensitive() {
            return GoToSymbolDialog.getNode().getBoolean("caseSensitive", false);
        }

        public static void setCaseSensitive(boolean caseSensitive) {
            GoToSymbolDialog.getNode().putBoolean("caseSensitive", caseSensitive);
        }

        public static int getHeight() {
            return GoToSymbolDialog.getNode().getInt("height", -1);
        }

        public static void setHeight(int height) {
            GoToSymbolDialog.getNode().putInt("height", height);
        }

        public static int getWidth() {
            return GoToSymbolDialog.getNode().getInt("width", -1);
        }

        public static void setWidth(int width) {
            GoToSymbolDialog.getNode().putInt("width", width);
        }

        private static synchronized Preferences getNode() {
            if (node == null) {
                Preferences p = NbPreferences.forModule(UiOptions.class);
                node = p.node("GoToSymbolDialog");
            }
            return node;
        }
    }

    static final class GoToTypeDialog {
        private static final String GO_TO_TYPE_DIALOG = "GoToTypeDialog";
        private static final String CASE_SENSITIVE = "caseSensitive";
        private static final String WIDTH = "width";
        private static final String HEIGHT = "height";
        private static Preferences node;

        GoToTypeDialog() {
        }

        public static boolean getCaseSensitive() {
            return GoToTypeDialog.getNode().getBoolean("caseSensitive", false);
        }

        public static void setCaseSensitive(boolean caseSensitive) {
            GoToTypeDialog.getNode().putBoolean("caseSensitive", caseSensitive);
        }

        public static int getHeight() {
            return GoToTypeDialog.getNode().getInt("height", -1);
        }

        public static void setHeight(int height) {
            GoToTypeDialog.getNode().putInt("height", height);
        }

        public static int getWidth() {
            return GoToTypeDialog.getNode().getInt("width", -1);
        }

        public static void setWidth(int width) {
            GoToTypeDialog.getNode().putInt("width", width);
        }

        private static synchronized Preferences getNode() {
            if (node == null) {
                Preferences p = NbPreferences.forModule(UiOptions.class);
                node = p.node("GoToTypeDialog");
            }
            return node;
        }
    }

}

