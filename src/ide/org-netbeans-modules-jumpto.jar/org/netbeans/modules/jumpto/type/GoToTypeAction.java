/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.sampler.Sampler
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.cookies.EditorCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.jumpto.type;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.jumpto.type.TypeBrowser;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.jumpto.EntitiesListCellRenderer;
import org.netbeans.modules.jumpto.common.HighlightingNameFormatter;
import org.netbeans.modules.jumpto.common.Models;
import org.netbeans.modules.jumpto.common.Utils;
import org.netbeans.modules.jumpto.file.LazyListModel;
import org.netbeans.modules.jumpto.type.GoToPanel;
import org.netbeans.modules.jumpto.type.TypeComparator;
import org.netbeans.modules.jumpto.type.TypeProviderAccessor;
import org.netbeans.modules.jumpto.type.UiOptions;
import org.netbeans.modules.sampler.Sampler;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.awt.HtmlRenderer;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.windows.TopComponent;

public class GoToTypeAction
extends AbstractAction
implements GoToPanel.ContentProvider,
LazyListModel.Filter {
    static final Logger LOGGER = Logger.getLogger(GoToTypeAction.class.getName());
    private Collection<? extends SearchType> nameKinds;
    private static ListModel EMPTY_LIST_MODEL = new DefaultListModel();
    private static final RequestProcessor rp = new RequestProcessor("GoToTypeAction-RequestProcessor", 1);
    private static final RequestProcessor PROFILE_RP = new RequestProcessor("GoToTypeAction-Profile", 1);
    private Worker running;
    private RequestProcessor.Task task;
    GoToPanel panel;
    private Dialog dialog;
    private JButton okButton;
    private Collection<? extends TypeProvider> typeProviders;
    private final Collection<? extends TypeProvider> implicitTypeProviders;
    private final TypeBrowser.Filter typeFilter;
    private final String title;
    private final boolean multiSelection;
    private Dimension initialDimension;

    public GoToTypeAction() {
        this(NbBundle.getMessage(GoToTypeAction.class, (String)"DLG_GoToType"), null, true, new TypeProvider[0]);
    }

    public /* varargs */ GoToTypeAction(String title, TypeBrowser.Filter typeFilter, boolean multiSelection, TypeProvider ... typeProviders) {
        super(NbBundle.getMessage(GoToTypeAction.class, (String)"TXT_GoToType"));
        this.putValue("PopupMenuText", NbBundle.getBundle(GoToTypeAction.class).getString("editor-popup-TXT_GoToType"));
        this.title = title;
        this.typeFilter = typeFilter;
        this.implicitTypeProviders = typeProviders.length == 0 ? null : Collections.unmodifiableCollection(Arrays.asList(typeProviders));
        this.multiSelection = multiSelection;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Iterable<? extends TypeDescriptor> selectedTypes = this.getSelectedTypes();
        if (selectedTypes.iterator().hasNext()) {
            JumpList.checkAddEntry();
            for (TypeDescriptor td : selectedTypes) {
                td.open();
            }
        }
    }

    public Iterable<? extends TypeDescriptor> getSelectedTypes() {
        return this.getSelectedTypes(true);
    }

    public Iterable<? extends TypeDescriptor> getSelectedTypes(boolean visible) {
        return this.getSelectedTypes(visible, null);
    }

    public Iterable<? extends TypeDescriptor> getSelectedTypes(boolean visible, String initSearchText) {
        Iterable result = Collections.emptyList();
        try {
            this.panel = new GoToPanel(this, this.multiSelection);
            this.dialog = this.createDialog(this.panel);
            if (initSearchText != null) {
                this.panel.setInitialText(initSearchText);
            } else {
                JEditorPane recentPane;
                EditorCookie ec;
                Node[] arr = TopComponent.getRegistry().getActivatedNodes();
                if (arr.length > 0 && (ec = (EditorCookie)arr[0].getCookie(EditorCookie.class)) != null && (recentPane = NbDocument.findRecentEditorPane((EditorCookie)ec)) != null) {
                    initSearchText = Utilities.getSelectionOrIdentifier((JTextComponent)recentPane);
                    if (initSearchText != null && org.openide.util.Utilities.isJavaIdentifier((String)initSearchText)) {
                        this.panel.setInitialText(initSearchText);
                    } else {
                        this.panel.setInitialText(arr[0].getName());
                    }
                }
            }
            this.dialog.setVisible(visible);
            result = this.panel.getSelectedTypes();
        }
        catch (IOException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
        return result;
    }

    @Override
    public boolean isEnabled() {
        return OpenProjects.getDefault().getOpenProjects().length > 0;
    }

    @Override
    public boolean accept(Object obj) {
        return this.typeFilter == null ? true : this.typeFilter.accept((TypeDescriptor)obj);
    }

    @Override
    public void scheduleUpdate(Runnable run) {
        SwingUtilities.invokeLater(run);
    }

    @Override
    public ListCellRenderer getListCellRenderer(@NonNull JList list, @NonNull ButtonModel caseSensitive) {
        Parameters.notNull((CharSequence)"list", (Object)list);
        Parameters.notNull((CharSequence)"caseSensitive", (Object)caseSensitive);
        return new Renderer(list, caseSensitive);
    }

    @Override
    public void setListModel(GoToPanel panel, String text) {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.okButton != null) {
            this.okButton.setEnabled(false);
        }
        final GoToPanel goToPanel = panel;
        MouseAdapter warningMouseListener = new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent evt) {
                if (GoToTypeAction.this.running != null) {
                    GoToTypeAction.this.running.cancel();
                    GoToTypeAction.this.task.cancel();
                    GoToTypeAction.this.running = null;
                }
                goToPanel.setListPanelContent(NbBundle.getMessage(GoToPanel.class, (String)"TXT_SearchCanceled"), false);
            }
        };
        panel.setMouseListener(warningMouseListener);
        if (this.running != null) {
            this.running.cancel();
            this.task.cancel();
            this.running = null;
        }
        if (text == null) {
            panel.setModel(EMPTY_LIST_MODEL, -1);
            return;
        }
        boolean exact = text.endsWith(" ");
        if ((text = text.trim()).length() == 0 || !Utils.isValidInput(text)) {
            panel.setModel(EMPTY_LIST_MODEL, -1);
            return;
        }
        int wildcard = Utils.containsWildCard(text);
        if (exact) {
            this.nameKinds = Collections.singleton(panel.isCaseSensitive() ? SearchType.EXACT_NAME : SearchType.CASE_INSENSITIVE_EXACT_NAME);
        } else if (Utils.isAllUpper(text) && text.length() > 1 || Utils.isCamelCase(text)) {
            SearchType[] arrsearchType = new SearchType[2];
            arrsearchType[0] = SearchType.CAMEL_CASE;
            arrsearchType[1] = panel.isCaseSensitive() ? SearchType.PREFIX : SearchType.CASE_INSENSITIVE_PREFIX;
            this.nameKinds = Arrays.asList(arrsearchType);
        } else if (wildcard != -1) {
            this.nameKinds = Collections.singleton(panel.isCaseSensitive() ? SearchType.REGEXP : SearchType.CASE_INSENSITIVE_REGEXP);
            text = Utils.removeNonNeededWildCards(text);
        } else {
            this.nameKinds = Collections.singleton(panel.isCaseSensitive() ? SearchType.PREFIX : SearchType.CASE_INSENSITIVE_PREFIX);
        }
        this.running = new Worker(text, panel.isCaseSensitive(), panel.getTextId());
        this.task = rp.post((Runnable)this.running, 220);
        if (panel.time != -1) {
            LOGGER.log(Level.FINE, "Worker posted after {0} ms.", System.currentTimeMillis() - panel.time);
        }
    }

    @Override
    public void closeDialog() {
        if (this.dialog == null) {
            return;
        }
        this.dialog.setVisible(false);
        this.cleanup();
    }

    @Override
    public boolean hasValidContent() {
        return this.okButton != null && this.okButton.isEnabled();
    }

    private Dialog createDialog(GoToPanel panel) {
        this.okButton = new JButton(NbBundle.getMessage(GoToTypeAction.class, (String)"CTL_OK"));
        this.okButton.getAccessibleContext().setAccessibleDescription(this.okButton.getText());
        this.okButton.setEnabled(false);
        panel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GoToTypeAction.class, (String)"AN_GoToType"));
        panel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GoToTypeAction.class, (String)"AD_GoToType"));
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panel, this.title, true, new Object[]{this.okButton, DialogDescriptor.CANCEL_OPTION}, (Object)this.okButton, 0, HelpCtx.DEFAULT_HELP, (ActionListener)new DialogButtonListener(panel));
        dialogDescriptor.setClosingOptions(new Object[]{this.okButton, DialogDescriptor.CANCEL_OPTION});
        Dialog d = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        int width = UiOptions.GoToTypeDialog.getWidth();
        int height = UiOptions.GoToTypeDialog.getHeight();
        if (width != -1 && height != -1) {
            d.setPreferredSize(new Dimension(width, height));
        }
        Rectangle r = org.openide.util.Utilities.getUsableScreenBounds();
        int maxW = r.width * 9 / 10;
        int maxH = r.height * 9 / 10;
        Dimension dim = d.getPreferredSize();
        dim.width = Math.min(dim.width, maxW);
        dim.height = Math.min(dim.height, maxH);
        d.setBounds(org.openide.util.Utilities.findCenterBounds((Dimension)dim));
        this.initialDimension = dim;
        d.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosed(WindowEvent e) {
                GoToTypeAction.this.cleanup();
            }
        });
        return d;
    }

    private void cleanup() {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.dialog != null) {
            int currentWidth = this.dialog.getWidth();
            int currentHeight = this.dialog.getHeight();
            if (this.initialDimension != null && (this.initialDimension.width != currentWidth || this.initialDimension.height != currentHeight)) {
                UiOptions.GoToTypeDialog.setHeight(currentHeight);
                UiOptions.GoToTypeDialog.setWidth(currentWidth);
            }
            this.initialDimension = null;
            this.dialog.dispose();
            this.dialog = null;
            if (this.running != null) {
                this.running.cancel();
                this.task.cancel();
                this.running = null;
            }
            rp.submit(new Runnable(){

                @Override
                public void run() {
                    assert (rp.isRequestProcessorThread());
                    if (GoToTypeAction.this.typeProviders != null) {
                        for (TypeProvider provider : GoToTypeAction.this.typeProviders) {
                            provider.cleanup();
                        }
                        GoToTypeAction.this.typeProviders = null;
                    }
                }
            });
        }
    }

    final void waitSearchFinished() {
        assert (SwingUtilities.isEventDispatchThread());
        this.task.waitFinished();
    }

    private Profile initializeProfiling() {
        boolean assertsOn = false;
        if (!$assertionsDisabled) {
            assertsOn = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (!assertsOn) {
            return null;
        }
        Sampler profiler = Sampler.createSampler((String)"jumpto");
        if (profiler == null) {
            return null;
        }
        return new Profile(profiler).start();
    }

    private class Profile
    implements Runnable {
        private final long time;
        private volatile Sampler profiler;
        private volatile boolean profiling;

        public Profile(Sampler profiler) {
            this.time = System.currentTimeMillis();
            this.profiler = profiler;
        }

        Profile start() {
            PROFILE_RP.post((Runnable)this, 3000);
            return this;
        }

        @Override
        public synchronized void run() {
            if (this.profiler != null) {
                this.profiling = true;
                this.profiler.start();
            }
        }

        private synchronized void stop() throws Exception {
            long delta = System.currentTimeMillis() - this.time;
            Sampler ss = this.profiler;
            this.profiler = null;
            if (!this.profiling) {
                return;
            }
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(out);
                ss.stopAndWriteTo(dos);
                dos.close();
                if (dos.size() > 0) {
                    Object[] params = new Object[]{out.toByteArray(), delta, "GoToType"};
                    Logger.getLogger("org.netbeans.ui.performance").log(Level.CONFIG, "Slowness detected", params);
                } else {
                    GoToTypeAction.LOGGER.log(Level.WARNING, "no snapshot taken");
                }
            }
            catch (Exception ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private class DialogButtonListener
    implements ActionListener {
        private GoToPanel panel;

        public DialogButtonListener(GoToPanel panel) {
            this.panel = panel;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == GoToTypeAction.this.okButton) {
                this.panel.setSelectedTypes();
            }
        }
    }

    private static final class Renderer
    extends EntitiesListCellRenderer
    implements ActionListener {
        private MyPanel rendererComponent;
        private JLabel jlName = HtmlRenderer.createLabel();
        private JLabel jlPkg = new JLabel();
        private JLabel jlPrj = new JLabel();
        private int DARKER_COLOR_COMPONENT = 15;
        private int LIGHTER_COLOR_COMPONENT = 80;
        private Color fgColor;
        private Color fgColorLighter;
        private Color bgColor;
        private Color bgColorDarker;
        private Color bgSelectionColor;
        private Color fgSelectionColor;
        private JList jList;
        private boolean caseSensitive;
        private final HighlightingNameFormatter typeNameFormatter;

        public Renderer(@NonNull JList list, @NonNull ButtonModel caseSensitive) {
            this.jList = list;
            this.caseSensitive = caseSensitive.isSelected();
            this.resetName();
            Container container = list.getParent();
            if (container instanceof JViewport) {
                ((JViewport)container).addChangeListener(this);
                this.stateChanged(new ChangeEvent(container));
            }
            this.rendererComponent = new MyPanel();
            this.rendererComponent.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 0;
            c.weightx = 0.0;
            c.anchor = 17;
            c.insets = new Insets(0, 0, 0, 7);
            this.rendererComponent.add((Component)this.jlName, c);
            c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 2;
            c.weightx = 0.1;
            c.anchor = 17;
            c.insets = new Insets(0, 0, 0, 7);
            this.rendererComponent.add((Component)this.jlPkg, c);
            c = new GridBagConstraints();
            c.gridx = 2;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 0;
            c.weightx = 0.0;
            c.anchor = 13;
            this.rendererComponent.add((Component)this.jlPrj, c);
            this.jlPkg.setOpaque(false);
            this.jlPrj.setOpaque(false);
            this.jlPkg.setFont(list.getFont());
            this.jlPrj.setFont(list.getFont());
            this.jlPrj.setHorizontalAlignment(4);
            this.jlPrj.setHorizontalTextPosition(2);
            this.fgColor = list.getForeground();
            this.fgColorLighter = new Color(Math.min(255, this.fgColor.getRed() + this.LIGHTER_COLOR_COMPONENT), Math.min(255, this.fgColor.getGreen() + this.LIGHTER_COLOR_COMPONENT), Math.min(255, this.fgColor.getBlue() + this.LIGHTER_COLOR_COMPONENT));
            this.bgColor = new Color(list.getBackground().getRGB());
            this.bgColorDarker = new Color(Math.abs(this.bgColor.getRed() - this.DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getGreen() - this.DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getBlue() - this.DARKER_COLOR_COMPONENT));
            this.bgSelectionColor = list.getSelectionBackground();
            this.fgSelectionColor = list.getSelectionForeground();
            this.typeNameFormatter = HighlightingNameFormatter.createBoldFormatter();
            caseSensitive.addActionListener(this);
            this.jlName.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
            int height = list.getFixedCellHeight();
            int width = list.getFixedCellWidth() - 1;
            width = width < 200 ? 200 : width;
            Dimension size = new Dimension(width, height);
            this.rendererComponent.setMaximumSize(size);
            this.rendererComponent.setPreferredSize(size);
            this.resetName();
            if (isSelected) {
                this.jlName.setForeground(this.fgSelectionColor);
                this.jlName.setBackground(this.bgSelectionColor);
                this.jlPkg.setForeground(this.fgSelectionColor);
                this.jlPrj.setForeground(this.fgSelectionColor);
                this.rendererComponent.setBackground(this.bgSelectionColor);
            } else {
                this.jlName.setForeground(this.fgColor);
                this.jlPkg.setForeground(this.fgColorLighter);
                this.jlPrj.setForeground(this.fgColor);
                Color bgc = index % 2 == 0 ? this.bgColor : this.bgColorDarker;
                this.jlName.setBackground(bgc);
                this.rendererComponent.setBackground(bgc);
            }
            if (value instanceof TypeDescriptor) {
                long time = System.currentTimeMillis();
                TypeDescriptor td = (TypeDescriptor)value;
                this.jlName.setIcon(td.getIcon());
                String formattedTypeName = this.typeNameFormatter.formatName(td.getTypeName(), TypeProviderAccessor.DEFAULT.getHighlightText(td), this.caseSensitive, isSelected ? this.fgSelectionColor : this.fgColor);
                this.jlName.setText(formattedTypeName);
                this.jlPkg.setText(td.getContextName());
                this.setProjectName(this.jlPrj, td.getProjectName());
                this.jlPrj.setIcon(td.getProjectIcon());
                this.rendererComponent.setDescriptor(td);
                GoToTypeAction.LOGGER.log(Level.FINE, "  Time in paint {0} ms.", System.currentTimeMillis() - time);
            } else {
                this.jlName.setText(value.toString());
            }
            return this.rendererComponent;
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            JViewport jv = (JViewport)event.getSource();
            this.jlName.setText("Sample");
            this.jlName.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/type/sample.png", (boolean)false));
            this.jList.setFixedCellHeight(this.jlName.getPreferredSize().height);
            this.jList.setFixedCellWidth(jv.getExtentSize().width);
        }

        @Override
        public void actionPerformed(@NonNull ActionEvent e) {
            this.caseSensitive = ((ButtonModel)e.getSource()).isSelected();
        }

        private void resetName() {
            ((HtmlRenderer.Renderer)this.jlName).reset();
            this.jlName.setFont(this.jList.getFont());
            this.jlName.setOpaque(true);
            ((HtmlRenderer.Renderer)this.jlName).setHtml(true);
            ((HtmlRenderer.Renderer)this.jlName).setRenderStyle(1);
        }
    }

    private static class MyPanel
    extends JPanel {
        private TypeDescriptor td;

        private MyPanel() {
        }

        void setDescriptor(TypeDescriptor td) {
            this.td = td;
            this.putClientProperty("ToolTipText", null);
        }

        @Override
        public String getToolTipText() {
            String text = (String)this.getClientProperty("ToolTipText");
            if (text == null) {
                if (this.td != null) {
                    text = this.td.getFileDisplayPath();
                }
                this.putClientProperty("ToolTipText", text);
            }
            return text;
        }
    }

    private class Worker
    implements Runnable {
        private volatile boolean isCanceled;
        private volatile TypeProvider current;
        private final String text;
        private final boolean caseSensitive;
        private final long createTime;
        private int lastSize;
        private final int textId;

        public Worker(String text, boolean caseSensitive, int textId) {
            this.isCanceled = false;
            this.lastSize = -1;
            this.text = text;
            this.caseSensitive = caseSensitive;
            this.createTime = System.currentTimeMillis();
            this.textId = textId;
            GoToTypeAction.LOGGER.log(Level.FINE, "Worker for {0} - created after {1} ms.", new Object[]{text, System.currentTimeMillis() - GoToTypeAction.this.panel.time});
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Loose catch block
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         * Lifted jumps to return sites
         */
        @Override
        public void run() {
            Future f = OpenProjects.getDefault().openProjects();
            if (!f.isDone()) {
                try {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            GoToTypeAction.this.panel.updateMessage(NbBundle.getMessage(GoToTypeAction.class, (String)"TXT_LoadingProjects"));
                        }
                    });
                    f.get();
                }
                catch (InterruptedException ex) {
                    GoToTypeAction.LOGGER.fine(ex.getMessage());
                    {
                        catch (Throwable throwable) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    GoToTypeAction.this.panel.updateMessage(NbBundle.getMessage(GoToTypeAction.class, (String)"TXT_Searching"));
                                }
                            });
                            throw throwable;
                        }
                    }
                    SwingUtilities.invokeLater(new );
                    catch (ExecutionException ex) {
                        GoToTypeAction.LOGGER.fine(ex.getMessage());
                        SwingUtilities.invokeLater(new );
                    }
                }
                SwingUtilities.invokeLater(new );
            }
            do {
                int[] retry;
                retry = new int[1];
                Profile profile = GoToTypeAction.this.initializeProfiling();
                try {
                    GoToTypeAction.LOGGER.log(Level.FINE, "Worker for {0} - started {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                    final List<? extends TypeDescriptor> types = this.getTypeNames(this.text, retry);
                    if (this.isCanceled) {
                        GoToTypeAction.LOGGER.log(Level.FINE, "Worker for {0} exited after cancel {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                        return;
                    }
                    int newSize = types.size();
                    if (this.lastSize != newSize) {
                        this.lastSize = newSize;
                        ListModel model = Models.fromList(types);
                        if (GoToTypeAction.this.typeFilter != null) {
                            model = LazyListModel.create(model, GoToTypeAction.this, 0.1, "Not computed yet");
                        }
                        final ListModel fmodel = model;
                        if (this.isCanceled) {
                            GoToTypeAction.LOGGER.log(Level.FINE, "Worker for {0} exited after cancel {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                            return;
                        }
                        if (!this.isCanceled && fmodel != null) {
                            GoToTypeAction.LOGGER.log(Level.FINE, "Worker for text {0} finished after {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    GoToTypeAction.this.panel.setModel(fmodel, Worker.this.textId);
                                    if (GoToTypeAction.this.okButton != null && !types.isEmpty()) {
                                        GoToTypeAction.this.okButton.setEnabled(true);
                                    }
                                }
                            });
                        }
                    }
                }
                finally {
                    if (profile != null) {
                        try {
                            profile.stop();
                        }
                        catch (Exception ex) {
                            GoToTypeAction.LOGGER.log(Level.INFO, "Cannot stop profiling", ex);
                        }
                    }
                }
                if (retry[0] <= 0) return;
                try {
                    Thread.sleep(retry[0]);
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                    continue;
                }
                break;
            } while (true);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void cancel() {
            TypeProvider _provider;
            if (GoToTypeAction.this.panel.time != -1) {
                GoToTypeAction.LOGGER.log(Level.FINE, "Worker for text {0} canceled after {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
            }
            Worker worker = this;
            synchronized (worker) {
                this.isCanceled = true;
                _provider = this.current;
            }
            if (_provider != null) {
                _provider.cancel();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private List<? extends TypeDescriptor> getTypeNames(String text, int[] retry) {
            HashSet items = new HashSet();
            String[] message = new String[1];
            HashSet<TypeProvider.Context> contexts = new HashSet<TypeProvider.Context>(2);
            for (SearchType nameKind : GoToTypeAction.this.nameKinds) {
                contexts.add(TypeProviderAccessor.DEFAULT.createContext(null, text, nameKind));
            }
            assert (!contexts.isEmpty());
            assert (rp.isRequestProcessorThread());
            if (GoToTypeAction.this.typeProviders == null) {
                GoToTypeAction.this.typeProviders = GoToTypeAction.this.implicitTypeProviders != null ? GoToTypeAction.this.implicitTypeProviders : Lookup.getDefault().lookupAll(TypeProvider.class);
            }
            for (TypeProvider provider : GoToTypeAction.this.typeProviders) {
                long start;
                if (this.isCanceled) {
                    return null;
                }
                this.current = provider;
                start = System.currentTimeMillis();
                try {
                    GoToTypeAction.LOGGER.log(Level.FINE, "Calling TypeProvider: {0}", provider);
                    for (TypeProvider.Context context : contexts) {
                        TypeProvider.Result result = TypeProviderAccessor.DEFAULT.createResult(items, message, context);
                        provider.computeTypeNames(context, result);
                        retry[0] = this.mergeRetryTimeOut(retry[0], TypeProviderAccessor.DEFAULT.getRetry(result));
                    }
                }
                finally {
                    this.current = null;
                }
                long delta = System.currentTimeMillis() - start;
                GoToTypeAction.LOGGER.log(Level.FINE, "Provider ''{0}'' took {1} ms.", new Object[]{provider.getDisplayName(), delta});
            }
            if (!this.isCanceled) {
                ArrayList result = new ArrayList(items);
                Collections.sort(result, new TypeComparator(this.caseSensitive));
                GoToTypeAction.this.panel.setWarning(message[0]);
                return result;
            }
            return null;
        }

        private int mergeRetryTimeOut(int t1, int t2) {
            if (t1 == 0) {
                return t2;
            }
            if (t2 == 0) {
                return t1;
            }
            return Math.min(t1, t2);
        }

    }

}

