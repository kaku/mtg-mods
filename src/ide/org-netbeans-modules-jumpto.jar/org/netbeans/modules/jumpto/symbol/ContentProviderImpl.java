/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.jumpto.symbol;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jumpto.EntitiesListCellRenderer;
import org.netbeans.modules.jumpto.common.HighlightingNameFormatter;
import org.netbeans.modules.jumpto.common.Models;
import org.netbeans.modules.jumpto.common.Utils;
import org.netbeans.modules.jumpto.symbol.DialogFactory;
import org.netbeans.modules.jumpto.symbol.GoToPanel;
import org.netbeans.modules.jumpto.symbol.GoToSymbolAction;
import org.netbeans.modules.jumpto.symbol.SymbolComparator;
import org.netbeans.modules.jumpto.symbol.SymbolProviderAccessor;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.symbol.SymbolProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.awt.HtmlRenderer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

final class ContentProviderImpl
implements GoToPanel.ContentProvider {
    private static final Logger LOG = Logger.getLogger(ContentProviderImpl.class.getName());
    private static final Pattern camelCasePattern = Pattern.compile("(?:\\p{javaUpperCase}(?:\\p{javaLowerCase}|\\p{Digit}|\\.|\\$)*){2,}");
    private static final RequestProcessor rp = new RequestProcessor(ContentProviderImpl.class);
    private final JButton okButton;
    private final AtomicReference<Collection<? extends SymbolProvider>> typeProviders = new AtomicReference();
    private RequestProcessor.Task task;
    private Worker running;
    private Dialog dialog;

    public ContentProviderImpl(JButton okButton) {
        this.okButton = okButton;
    }

    void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public ListCellRenderer getListCellRenderer(@NonNull JList list, @NonNull ButtonModel caseSensitive) {
        Parameters.notNull((CharSequence)"list", (Object)list);
        Parameters.notNull((CharSequence)"caseSensitive", (Object)caseSensitive);
        return new Renderer(list, caseSensitive);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setListModel(GoToPanel panel, String text) {
        Worker workToCancel;
        RequestProcessor.Task taskToCancel;
        if (this.okButton != null) {
            this.okButton.setEnabled(false);
        }
        ContentProviderImpl contentProviderImpl = this;
        synchronized (contentProviderImpl) {
            workToCancel = this.running;
            taskToCancel = this.task;
            this.running = null;
            this.task = null;
        }
        if (workToCancel != null) {
            workToCancel.cancel();
        }
        if (taskToCancel != null) {
            taskToCancel.cancel();
        }
        if (text == null) {
            panel.setModel(new DefaultListModel(), -1);
            return;
        }
        boolean exact = text.endsWith(" ");
        boolean isCaseSensitive = panel.isCaseSensitive();
        if ((text = text.trim()).length() == 0 || !Utils.isValidInput(text)) {
            panel.setModel(new DefaultListModel(), -1);
            return;
        }
        ContentProviderImpl contentProviderImpl2 = this;
        synchronized (contentProviderImpl2) {
            this.running = new Worker(text, exact, isCaseSensitive, panel);
            this.task = rp.post((Runnable)this.running, 220);
            if (panel.time != -1) {
                LOG.log(Level.FINE, "Worker posted after {0} ms.", System.currentTimeMillis() - panel.time);
            }
        }
    }

    @Override
    public void closeDialog() {
        if (this.dialog != null) {
            this.dialog.setVisible(false);
            DialogFactory.storeDialogDimensions(new Dimension(this.dialog.getWidth(), this.dialog.getHeight()));
            this.dialog.dispose();
            this.dialog = null;
            this.cleanUp();
        }
    }

    @Override
    public boolean hasValidContent() {
        return this.okButton.isEnabled();
    }

    private void cleanUp() {
        for (SymbolProvider provider : this.getTypeProviders()) {
            provider.cleanup();
        }
    }

    private Collection<? extends SymbolProvider> getTypeProviders() {
        Collection<? extends SymbolProvider> res = this.typeProviders.get();
        if (res == null && !this.typeProviders.compareAndSet(null, res = Arrays.asList(Lookup.getDefault().lookupAll(SymbolProvider.class).toArray(new SymbolProvider[0])))) {
            res = this.typeProviders.get();
        }
        return res;
    }

    @NonNull
    private static SearchType getSearchType(@NonNull String text, boolean exact, boolean isCaseSensitive) {
        int wildcard = Utils.containsWildCard(text);
        if (exact) {
            return SearchType.EXACT_NAME;
        }
        if (Utils.isAllUpper(text) && text.length() > 1 || Utils.isCamelCase(text)) {
            return SearchType.CAMEL_CASE;
        }
        if (wildcard != -1) {
            return isCaseSensitive ? SearchType.REGEXP : SearchType.CASE_INSENSITIVE_REGEXP;
        }
        return isCaseSensitive ? SearchType.PREFIX : SearchType.CASE_INSENSITIVE_PREFIX;
    }

    private class Worker
    implements Runnable {
        private final String text;
        private final boolean exact;
        private final boolean isCaseSensitive;
        private final long createTime;
        private final GoToPanel panel;
        private volatile boolean isCanceled;
        private volatile SymbolProvider current;
        private final int textId;

        public Worker(String text, boolean exact, @NonNull boolean isCaseSensitive, GoToPanel panel) {
            this.isCanceled = false;
            this.text = text;
            this.exact = exact;
            this.isCaseSensitive = isCaseSensitive;
            this.panel = panel;
            this.createTime = System.currentTimeMillis();
            this.textId = panel.getTextId();
            LOG.log(Level.FINE, "Worker for {0} - created after {1} ms.", new Object[]{text, System.currentTimeMillis() - panel.time});
        }

        @Override
        public void run() {
            LOG.log(Level.FINE, "Worker for {0} - started {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
            final List<? extends SymbolDescriptor> types = this.getSymbolNames(this.text);
            if (this.isCanceled) {
                LOG.log(Level.FINE, "Worker for {0} exited after cancel {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                return;
            }
            final ListModel fmodel = Models.fromList(types);
            if (this.isCanceled) {
                LOG.log(Level.FINE, "Worker for {0} exited after cancel {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                return;
            }
            if (!this.isCanceled && fmodel != null) {
                LOG.log(Level.FINE, "Worker for text {0} finished after {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Worker.this.panel.setModel(fmodel, Worker.this.textId);
                        if (ContentProviderImpl.this.okButton != null && !types.isEmpty()) {
                            ContentProviderImpl.this.okButton.setEnabled(true);
                        }
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void cancel() {
            SymbolProvider _provider;
            if (this.panel.time != -1) {
                LOG.log(Level.FINE, "Worker for text {0} canceled after {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
            }
            Worker worker = this;
            synchronized (worker) {
                this.isCanceled = true;
                _provider = this.current;
            }
            if (_provider != null) {
                _provider.cancel();
            }
        }

        private List<? extends SymbolDescriptor> getSymbolNames(String text) {
            ArrayList items = new ArrayList(128);
            String[] message = new String[1];
            Iterator i$ = ContentProviderImpl.this.getTypeProviders().iterator();
            while (i$.hasNext()) {
                SymbolProvider provider;
                this.current = provider = (SymbolProvider)i$.next();
                if (this.isCanceled) {
                    return null;
                }
                LOG.log(Level.FINE, "Calling SymbolProvider: {0}", provider);
                SearchType searchType = ContentProviderImpl.getSearchType(text, this.exact, this.isCaseSensitive);
                if (searchType == SearchType.REGEXP || searchType == SearchType.CASE_INSENSITIVE_REGEXP) {
                    text = Utils.removeNonNeededWildCards(text);
                }
                SymbolProvider.Context context = SymbolProviderAccessor.DEFAULT.createContext(null, text, searchType);
                SymbolProvider.Result result = SymbolProviderAccessor.DEFAULT.createResult(items, message, context);
                provider.computeSymbolNames(context, result);
                this.current = null;
            }
            if (!this.isCanceled) {
                Collections.sort(items, new SymbolComparator());
                this.panel.setWarning(message[0]);
                return items;
            }
            return null;
        }

    }

    private static class Renderer
    extends EntitiesListCellRenderer
    implements ActionListener {
        private final HighlightingNameFormatter symbolNameFormatter;
        private MyPanel rendererComponent;
        private JLabel jlName = HtmlRenderer.createLabel();
        private JLabel jlOwner = new JLabel();
        private JLabel jlPrj = new JLabel();
        private int DARKER_COLOR_COMPONENT = 15;
        private int LIGHTER_COLOR_COMPONENT = 80;
        private Color fgColor;
        private Color fgColorLighter;
        private Color bgColor;
        private Color bgColorDarker;
        private Color bgSelectionColor;
        private Color fgSelectionColor;
        private JList jList;
        private boolean caseSensitive;

        public Renderer(@NonNull JList list, @NonNull ButtonModel caseSensitive) {
            this.jList = list;
            this.caseSensitive = caseSensitive.isSelected();
            this.resetName();
            Container container = list.getParent();
            if (container instanceof JViewport) {
                ((JViewport)container).addChangeListener(this);
                this.stateChanged(new ChangeEvent(container));
            }
            this.rendererComponent = new MyPanel();
            this.rendererComponent.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 0;
            c.weightx = 0.0;
            c.anchor = 17;
            c.insets = new Insets(0, 0, 0, 7);
            this.rendererComponent.add((Component)this.jlName, c);
            this.jlOwner.setOpaque(false);
            this.jlOwner.setFont(list.getFont());
            c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 2;
            c.weightx = 0.1;
            c.anchor = 17;
            c.insets = new Insets(0, 0, 0, 7);
            this.rendererComponent.add((Component)this.jlOwner, c);
            c = new GridBagConstraints();
            c.gridx = 2;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 0;
            c.weightx = 0.0;
            c.anchor = 13;
            this.rendererComponent.add((Component)this.jlPrj, c);
            this.jlPrj.setOpaque(false);
            this.jlPrj.setFont(list.getFont());
            this.jlPrj.setHorizontalAlignment(4);
            this.jlPrj.setHorizontalTextPosition(2);
            this.fgColor = list.getForeground();
            this.fgColorLighter = new Color(Math.min(255, this.fgColor.getRed() + this.LIGHTER_COLOR_COMPONENT), Math.min(255, this.fgColor.getGreen() + this.LIGHTER_COLOR_COMPONENT), Math.min(255, this.fgColor.getBlue() + this.LIGHTER_COLOR_COMPONENT));
            this.bgColor = new Color(list.getBackground().getRGB());
            this.bgColorDarker = new Color(Math.abs(this.bgColor.getRed() - this.DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getGreen() - this.DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getBlue() - this.DARKER_COLOR_COMPONENT));
            this.bgSelectionColor = list.getSelectionBackground();
            this.fgSelectionColor = list.getSelectionForeground();
            this.symbolNameFormatter = HighlightingNameFormatter.createBoldFormatter();
            caseSensitive.addActionListener(this);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
            int height = list.getFixedCellHeight();
            int width = list.getFixedCellWidth() - 1;
            width = width < 200 ? 200 : width;
            Dimension size = new Dimension(width, height);
            this.rendererComponent.setMaximumSize(size);
            this.rendererComponent.setPreferredSize(size);
            this.resetName();
            if (isSelected) {
                this.jlName.setForeground(this.fgSelectionColor);
                this.jlOwner.setForeground(this.fgSelectionColor);
                this.jlPrj.setForeground(this.fgSelectionColor);
                this.rendererComponent.setBackground(this.bgSelectionColor);
            } else {
                this.jlName.setForeground(this.fgColor);
                this.jlOwner.setForeground(this.fgColorLighter);
                this.jlPrj.setForeground(this.fgColor);
                this.rendererComponent.setBackground(index % 2 == 0 ? this.bgColor : this.bgColorDarker);
            }
            if (value instanceof SymbolDescriptor) {
                long time = System.currentTimeMillis();
                SymbolDescriptor td = (SymbolDescriptor)value;
                this.jlName.setIcon(td.getIcon());
                String formattedSymbolName = this.symbolNameFormatter.formatName(td.getSymbolName(), SymbolProviderAccessor.DEFAULT.getHighlightText(td), this.caseSensitive, isSelected ? this.fgSelectionColor : this.fgColor);
                this.jlName.setText(formattedSymbolName);
                this.jlOwner.setText(NbBundle.getMessage(GoToSymbolAction.class, (String)"MSG_DeclaredIn", (Object)td.getOwnerName()));
                this.setProjectName(this.jlPrj, td.getProjectName());
                this.jlPrj.setIcon(td.getProjectIcon());
                this.rendererComponent.setDescriptor(td);
                LOG.fine("  Time in paint " + (System.currentTimeMillis() - time) + " ms.");
            } else {
                this.jlName.setText(value.toString());
            }
            return this.rendererComponent;
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            JViewport jv = (JViewport)event.getSource();
            this.jlName.setText("Sample");
            this.jlName.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/type/sample.png", (boolean)false));
            this.jList.setFixedCellHeight(this.jlName.getPreferredSize().height);
            this.jList.setFixedCellWidth(jv.getExtentSize().width);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.caseSensitive = ((ButtonModel)e.getSource()).isSelected();
        }

        private void resetName() {
            ((HtmlRenderer.Renderer)this.jlName).reset();
            this.jlName.setFont(this.jList.getFont());
            this.jlName.setOpaque(false);
            ((HtmlRenderer.Renderer)this.jlName).setHtml(true);
            ((HtmlRenderer.Renderer)this.jlName).setRenderStyle(1);
        }
    }

    private static class MyPanel
    extends JPanel {
        private SymbolDescriptor td;

        private MyPanel() {
        }

        void setDescriptor(SymbolDescriptor td) {
            this.td = td;
            this.putClientProperty("ToolTipText", null);
        }

        @Override
        public String getToolTipText() {
            String text = (String)this.getClientProperty("ToolTipText");
            if (text == null) {
                if (this.td != null) {
                    text = this.td.getFileDisplayPath();
                }
                this.putClientProperty("ToolTipText", text);
            }
            return text;
        }
    }

}

