/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.jumpto.symbol;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import org.netbeans.modules.jumpto.symbol.GoToPanel;
import org.netbeans.modules.jumpto.symbol.GoToSymbolAction;
import org.netbeans.modules.jumpto.type.UiOptions;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class DialogFactory {
    private static Dimension initialDimension;

    DialogFactory() {
    }

    static Dialog createDialog(String title, GoToPanel panel, final GoToPanel.ContentProvider contentProvider, JButton okButton) {
        okButton.setEnabled(false);
        panel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GoToSymbolAction.class, (String)"AN_GoToSymbol"));
        panel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GoToSymbolAction.class, (String)"AD_GoToSymbol"));
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panel, title, true, new Object[]{okButton, DialogDescriptor.CANCEL_OPTION}, (Object)okButton, 0, HelpCtx.DEFAULT_HELP, (ActionListener)new DialogButtonListener(panel, okButton));
        dialogDescriptor.setClosingOptions(new Object[]{okButton, DialogDescriptor.CANCEL_OPTION});
        Dialog d = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        int width = UiOptions.GoToSymbolDialog.getWidth();
        int height = UiOptions.GoToSymbolDialog.getHeight();
        if (width != -1 && height != -1) {
            d.setPreferredSize(new Dimension(width, height));
        }
        Rectangle r = Utilities.getUsableScreenBounds();
        int maxW = r.width * 9 / 10;
        int maxH = r.height * 9 / 10;
        Dimension dim = d.getPreferredSize();
        dim.width = Math.min(dim.width, maxW);
        dim.height = Math.min(dim.height, maxH);
        d.setBounds(Utilities.findCenterBounds((Dimension)dim));
        initialDimension = dim;
        d.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosed(WindowEvent e) {
                contentProvider.closeDialog();
            }
        });
        return d;
    }

    static void storeDialogDimensions(Dimension dim) {
        int currentWidth = dim.width;
        int currentHeight = dim.height;
        if (initialDimension != null && (DialogFactory.initialDimension.width != currentWidth || DialogFactory.initialDimension.height != currentHeight)) {
            UiOptions.GoToSymbolDialog.setHeight(currentHeight);
            UiOptions.GoToSymbolDialog.setWidth(currentWidth);
        }
        initialDimension = null;
    }

    private static class DialogButtonListener
    implements ActionListener {
        private final GoToPanel panel;
        private final JButton okButton;

        public DialogButtonListener(GoToPanel panel, JButton okButton) {
            this.panel = panel;
            this.okButton = okButton;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == this.okButton) {
                this.panel.setSelectedSymbol();
            }
        }
    }

}

