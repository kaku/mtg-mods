/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.jumpto.symbol;

import org.netbeans.modules.jumpto.EntityComparator;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;

public class SymbolComparator
extends EntityComparator<SymbolDescriptor> {
    @Override
    public int compare(SymbolDescriptor e1, SymbolDescriptor e2) {
        String e2projectName;
        String e1projectName = e1.getProjectName();
        int result = this.compareProjects(e1projectName, e2projectName = e2.getProjectName());
        if (result != 0) {
            return result;
        }
        result = this.compare(e1.getSymbolName(), e2.getSymbolName());
        if (result != 0) {
            return result;
        }
        return this.compare(e1.getOwnerName(), e2.getOwnerName());
    }
}

