/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.awt.Mnemonics
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.jumpto.symbol;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.jumpto.SearchHistory;
import org.netbeans.modules.jumpto.common.UiUtils;
import org.netbeans.modules.jumpto.symbol.GoToSymbolAction;
import org.netbeans.modules.jumpto.type.UiOptions;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class GoToPanel
extends JPanel {
    private static Icon WAIT_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/resources/wait.gif", (boolean)false);
    private static Icon WARN_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/jumpto/resources/warning.png", (boolean)false);
    private static final int BRIGHTER_COLOR_COMPONENT = 10;
    private ContentProvider contentProvider;
    private boolean containsScrollPane;
    private JLabel messageLabel;
    private SymbolDescriptor selectedSymbol;
    private String oldText;
    private volatile int textId;
    long time = -1;
    private final SearchHistory searchHistory;
    private boolean pastedFromClipboard = false;
    private JCheckBox caseSensitive;
    private JLabel jLabelList;
    private JLabel jLabelLocation;
    private JLabel jLabelText;
    private JLabel jLabelWarning;
    private JTextField jTextFieldLocation;
    private JPanel listPanel;
    private JList matchesList;
    private JScrollPane matchesScrollPane1;
    private JTextField nameField;

    public GoToPanel(ContentProvider contentProvider) throws IOException {
        this.contentProvider = contentProvider;
        this.initComponents();
        this.containsScrollPane = true;
        ((AbstractDocument)this.nameField.getDocument()).setDocumentFilter(UiUtils.newUserInputFilter());
        this.matchesList.setSelectionMode(0);
        this.matchesList.addListSelectionListener(null);
        Color bgColorBrighter = new Color(Math.min(this.getBackground().getRed() + 10, 255), Math.min(this.getBackground().getGreen() + 10, 255), Math.min(this.getBackground().getBlue() + 10, 255));
        this.messageLabel = new JLabel();
        this.messageLabel.setBackground(bgColorBrighter);
        this.messageLabel.setHorizontalAlignment(0);
        this.messageLabel.setEnabled(true);
        this.messageLabel.setText(NbBundle.getMessage(GoToPanel.class, (String)"TXT_NoSymbolsFound"));
        this.messageLabel.setFont(this.matchesList.getFont());
        this.matchesList.setCellRenderer(contentProvider.getListCellRenderer(this.matchesList, this.caseSensitive.getModel()));
        contentProvider.setListModel(this, null);
        PatternListener pl = new PatternListener(this);
        this.nameField.getDocument().addDocumentListener(pl);
        this.matchesList.addListSelectionListener(pl);
        this.caseSensitive.setSelected(UiOptions.GoToSymbolDialog.getCaseSensitive());
        this.caseSensitive.addItemListener(pl);
        this.searchHistory = new SearchHistory(GoToPanel.class, this.nameField);
    }

    @Override
    public void removeNotify() {
        this.searchHistory.saveHistory();
        super.removeNotify();
    }

    public boolean isCaseSensitive() {
        return this.caseSensitive.isSelected();
    }

    public void setModel(final ListModel model, int id) {
        final int fid = id == -1 ? this.textId : id;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (fid != GoToPanel.this.textId) {
                    return;
                }
                if (model.getSize() > 0 || GoToPanel.this.getText() == null || GoToPanel.this.getText().trim().length() == 0) {
                    GoToPanel.this.matchesList.setModel(model);
                    GoToPanel.this.matchesList.setSelectedIndex(0);
                    GoToPanel.this.setListPanelContent(null, false);
                    if (GoToPanel.this.time != -1) {
                        GoToSymbolAction.LOGGER.fine("Real search time " + (System.currentTimeMillis() - GoToPanel.this.time) + " ms.");
                        GoToPanel.this.time = -1;
                    }
                } else {
                    GoToPanel.this.setListPanelContent(NbBundle.getMessage(GoToPanel.class, (String)"TXT_NoSymbolsFound"), false);
                }
            }
        });
    }

    public void setInitialText(final String text) {
        this.oldText = text;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                String textInField = GoToPanel.this.nameField.getText();
                if (textInField == null || textInField.trim().length() == 0) {
                    GoToPanel.this.nameField.setText(text);
                    GoToPanel.this.nameField.setCaretPosition(text.length());
                    GoToPanel.this.nameField.setSelectionStart(0);
                    GoToPanel.this.nameField.setSelectionEnd(text.length());
                }
            }
        });
    }

    public void setSelectedSymbol() {
        this.selectedSymbol = (SymbolDescriptor)this.matchesList.getSelectedValue();
    }

    public SymbolDescriptor getSelectedSymbol() {
        return this.selectedSymbol;
    }

    void setWarning(String warningMessage) {
        if (warningMessage != null) {
            this.jLabelWarning.setIcon(WARN_ICON);
            this.jLabelWarning.setBorder(BorderFactory.createEmptyBorder(3, 1, 1, 1));
        } else {
            this.jLabelWarning.setIcon(null);
            this.jLabelWarning.setBorder(null);
        }
        this.jLabelWarning.setText(warningMessage);
    }

    private void initComponents() {
        this.jLabelText = new JLabel();
        this.nameField = new JTextField();
        this.jLabelList = new JLabel();
        this.listPanel = new JPanel();
        this.matchesScrollPane1 = new JScrollPane();
        this.matchesList = new JList();
        this.jLabelWarning = new JLabel();
        this.caseSensitive = new JCheckBox();
        this.jLabelLocation = new JLabel();
        this.jTextFieldLocation = new JTextField();
        this.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        this.setFocusable(false);
        this.setNextFocusableComponent(this.nameField);
        this.setLayout(new GridBagLayout());
        this.jLabelText.setLabelFor(this.nameField);
        Mnemonics.setLocalizedText((JLabel)this.jLabelText, (String)NbBundle.getMessage(GoToPanel.class, (String)"TXT_GoToSymbol_TypeName_Label"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 16;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 4, 0);
        this.add((Component)this.jLabelText, gridBagConstraints);
        this.nameField.setFont(new Font("Monospaced", 0, this.getFontSize()));
        this.nameField.setBorder(BorderFactory.createEtchedBorder());
        this.nameField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GoToPanel.this.nameFieldActionPerformed(evt);
            }
        });
        this.nameField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                GoToPanel.this.nameFieldKeyPressed(evt);
            }

            @Override
            public void keyReleased(KeyEvent evt) {
                GoToPanel.this.nameFieldKeyReleased(evt);
            }

            @Override
            public void keyTyped(KeyEvent evt) {
                GoToPanel.this.nameFieldKeyTyped(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(0, 0, 8, 0);
        this.add((Component)this.nameField, gridBagConstraints);
        this.nameField.getAccessibleContext().setAccessibleName("Symbol &Name (prefix, camel case: \"AA\" or \"AbcAb\", wildcards: \"?\" \"*\", exact match: end with space):");
        this.nameField.getAccessibleContext().setAccessibleDescription("Symbol Name (prefix, camel case: \"AA\" or \"AbcAb\", wildcards: \"?\" \"*\", exact match: end with space)");
        this.jLabelList.setLabelFor(this.matchesList);
        Mnemonics.setLocalizedText((JLabel)this.jLabelList, (String)NbBundle.getMessage(GoToPanel.class, (String)"TXT_GoToSymbol_MatchesList_Label"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 16;
        gridBagConstraints.insets = new Insets(0, 0, 4, 0);
        this.add((Component)this.jLabelList, gridBagConstraints);
        this.listPanel.setBorder(BorderFactory.createEtchedBorder());
        this.listPanel.setName("dataPanel");
        this.listPanel.setLayout(new BorderLayout());
        this.matchesScrollPane1.setBorder(null);
        this.matchesScrollPane1.setFocusable(false);
        this.matchesList.setFont(new Font("Monospaced", 0, this.getFontSize()));
        this.matchesList.setVisibleRowCount(15);
        this.matchesList.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent evt) {
                GoToPanel.this.matchesListMouseReleased(evt);
            }
        });
        this.matchesScrollPane1.setViewportView(this.matchesList);
        this.matchesList.getAccessibleContext().setAccessibleName("Symbols &Found :");
        this.matchesList.getAccessibleContext().setAccessibleDescription("Symbols Found");
        this.listPanel.add((Component)this.matchesScrollPane1, "Center");
        this.jLabelWarning.setFocusable(false);
        this.listPanel.add((Component)this.jLabelWarning, "Last");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 8, 0);
        this.add((Component)this.listPanel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.caseSensitive, (String)NbBundle.getMessage(GoToPanel.class, (String)"CTL_CaseSensitive"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 8, 0);
        this.add((Component)this.caseSensitive, gridBagConstraints);
        this.caseSensitive.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GoToPanel.class, (String)"AD_CaseSensitive"));
        this.jLabelLocation.setText(NbBundle.getMessage(GoToPanel.class, (String)"LBL_GoToSymbol_LocationJLabel"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 4, 0);
        this.add((Component)this.jLabelLocation, gridBagConstraints);
        this.jTextFieldLocation.setEditable(false);
        this.jTextFieldLocation.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        this.add((Component)this.jTextFieldLocation, gridBagConstraints);
    }

    private void matchesListMouseReleased(MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            this.nameFieldActionPerformed(null);
        }
    }

    private void nameFieldKeyTyped(KeyEvent evt) {
        if (this.boundScrollingKey(evt)) {
            this.delegateScrollingKey(evt);
        }
    }

    private void nameFieldKeyReleased(KeyEvent evt) {
        if (this.boundScrollingKey(evt)) {
            this.delegateScrollingKey(evt);
        }
    }

    private void nameFieldKeyPressed(KeyEvent evt) {
        if (this.boundScrollingKey(evt)) {
            this.delegateScrollingKey(evt);
        } else {
            String action;
            Object o = this.nameField.getInputMap().get(KeyStroke.getKeyStrokeForEvent(evt));
            if (o instanceof String && "paste-from-clipboard".equals(action = (String)o)) {
                String selectedTxt = this.nameField.getSelectedText();
                String txt = this.nameField.getText();
                if (selectedTxt != null && txt != null && selectedTxt.length() == txt.length()) {
                    this.pastedFromClipboard = true;
                }
            }
        }
    }

    private void nameFieldActionPerformed(ActionEvent evt) {
        if (this.contentProvider.hasValidContent()) {
            this.contentProvider.closeDialog();
            this.setSelectedSymbol();
        }
    }

    public int getTextId() {
        return this.textId;
    }

    private String getText() {
        try {
            String text = this.nameField.getDocument().getText(0, this.nameField.getDocument().getLength());
            return text;
        }
        catch (BadLocationException ex) {
            return null;
        }
    }

    private int getFontSize() {
        return this.jLabelList.getFont().getSize();
    }

    void setListPanelContent(String message, boolean waitIcon) {
        if (message == null && !this.containsScrollPane) {
            this.listPanel.remove(this.messageLabel);
            this.listPanel.add(this.matchesScrollPane1);
            this.containsScrollPane = true;
            this.revalidate();
            this.repaint();
        } else if (message != null) {
            this.jTextFieldLocation.setText("");
            this.messageLabel.setText(message);
            this.messageLabel.setIcon(waitIcon ? WAIT_ICON : null);
            if (this.containsScrollPane) {
                this.listPanel.remove(this.matchesScrollPane1);
                this.listPanel.add(this.messageLabel);
                this.containsScrollPane = false;
            }
            this.revalidate();
            this.repaint();
        }
    }

    private String listActionFor(KeyEvent ev) {
        InputMap map = this.matchesList.getInputMap();
        Object o = map.get(KeyStroke.getKeyStrokeForEvent(ev));
        if (o instanceof String) {
            return (String)o;
        }
        return null;
    }

    private boolean boundScrollingKey(KeyEvent ev) {
        String action = this.listActionFor(ev);
        return "selectPreviousRow".equals(action) || "selectNextRow".equals(action) || "scrollUp".equals(action) || "scrollDown".equals(action);
    }

    private void delegateScrollingKey(KeyEvent ev) {
        String action = this.listActionFor(ev);
        if ("selectNextRow".equals(action) && this.matchesList.getSelectedIndex() == this.matchesList.getModel().getSize() - 1) {
            this.matchesList.setSelectedIndex(0);
            this.matchesList.ensureIndexIsVisible(0);
            return;
        }
        if ("selectPreviousRow".equals(action) && this.matchesList.getSelectedIndex() == 0) {
            int last = this.matchesList.getModel().getSize() - 1;
            this.matchesList.setSelectedIndex(last);
            this.matchesList.ensureIndexIsVisible(last);
            return;
        }
        Action a = this.matchesList.getActionMap().get(action);
        if (a != null) {
            a.actionPerformed(new ActionEvent(this.matchesList, 0, action));
        }
    }

    public static interface ContentProvider {
        public ListCellRenderer getListCellRenderer(JList var1, ButtonModel var2);

        public void setListModel(GoToPanel var1, String var2);

        public void closeDialog();

        public boolean hasValidContent();
    }

    private static class PatternListener
    implements DocumentListener,
    ListSelectionListener,
    ItemListener {
        private final GoToPanel dialog;

        PatternListener(GoToPanel dialog) {
            this.dialog = dialog;
        }

        PatternListener(DocumentEvent e, GoToPanel dialog) {
            this.dialog = dialog;
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.update();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            if (this.dialog.pastedFromClipboard) {
                this.dialog.pastedFromClipboard = false;
                this.dialog.textId++;
            } else {
                this.update();
            }
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.update();
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            UiOptions.GoToSymbolDialog.setCaseSensitive(this.dialog.isCaseSensitive());
            this.update();
        }

        @Override
        public void valueChanged(@NonNull ListSelectionEvent ev) {
            Object obj = this.dialog.matchesList.getSelectedValue();
            if (obj instanceof SymbolDescriptor) {
                SymbolDescriptor selectedValue = (SymbolDescriptor)obj;
                String fileName = selectedValue.getFileDisplayPath();
                this.dialog.jTextFieldLocation.setText(fileName);
            } else {
                this.dialog.jTextFieldLocation.setText("");
            }
        }

        private void update() {
            this.dialog.time = System.currentTimeMillis();
            String text = this.dialog.getText();
            if (this.dialog.oldText == null || this.dialog.oldText.trim().length() == 0 || !text.startsWith(this.dialog.oldText)) {
                this.dialog.setListPanelContent(NbBundle.getMessage(GoToPanel.class, (String)"TXT_Searching"), true);
            }
            this.dialog.oldText = text;
            this.dialog.textId++;
            this.dialog.contentProvider.setListModel(this.dialog, text);
        }
    }

}

