/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.Utilities
 *  org.openide.ErrorManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.jumpto.symbol;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.text.JTextComponent;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.editor.JumpList;
import org.netbeans.modules.jumpto.symbol.ContentProviderImpl;
import org.netbeans.modules.jumpto.symbol.DialogFactory;
import org.netbeans.modules.jumpto.symbol.GoToPanel;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.openide.ErrorManager;
import org.openide.cookies.EditorCookie;
import org.openide.nodes.Node;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public class GoToSymbolAction
extends AbstractAction {
    static final Logger LOGGER = Logger.getLogger(GoToSymbolAction.class.getName());
    private String title;

    public GoToSymbolAction() {
        this(NbBundle.getMessage(GoToSymbolAction.class, (String)"DLG_GoToSymbol"));
    }

    public GoToSymbolAction(String title) {
        super(NbBundle.getMessage(GoToSymbolAction.class, (String)"TXT_GoToSymbol"));
        this.title = title;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SymbolDescriptor typeDescriptor = this.getSelectedSymbol();
        if (typeDescriptor != null) {
            JumpList.checkAddEntry();
            typeDescriptor.open();
        }
    }

    public SymbolDescriptor getSelectedSymbol() {
        SymbolDescriptor result = null;
        try {
            JEditorPane recentPane;
            String initSearchText;
            EditorCookie ec;
            JButton okButton = new JButton(NbBundle.getMessage(GoToSymbolAction.class, (String)"CTL_OK"));
            ContentProviderImpl cp = new ContentProviderImpl(okButton);
            GoToPanel panel = new GoToPanel(cp);
            Dialog dialog = DialogFactory.createDialog(this.title, panel, cp, okButton);
            cp.setDialog(dialog);
            Node[] arr = TopComponent.getRegistry().getActivatedNodes();
            if (arr.length > 0 && (ec = (EditorCookie)arr[0].getCookie(EditorCookie.class)) != null && (recentPane = NbDocument.findRecentEditorPane((EditorCookie)ec)) != null && (initSearchText = org.netbeans.editor.Utilities.getSelectionOrIdentifier((JTextComponent)recentPane)) != null && Utilities.isJavaIdentifier((String)initSearchText)) {
                panel.setInitialText(initSearchText);
            }
            dialog.setVisible(true);
            result = panel.getSelectedSymbol();
        }
        catch (IOException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
        return result;
    }

    @Override
    public boolean isEnabled() {
        return OpenProjects.getDefault().getOpenProjects().length > 0;
    }
}

