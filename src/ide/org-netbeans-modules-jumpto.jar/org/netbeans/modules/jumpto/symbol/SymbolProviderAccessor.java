/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.jumpto.symbol;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.symbol.SymbolProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.util.Exceptions;

public abstract class SymbolProviderAccessor {
    public static SymbolProviderAccessor DEFAULT;

    public abstract SymbolProvider.Context createContext(Project var1, String var2, SearchType var3);

    @NonNull
    public abstract SymbolProvider.Result createResult(@NonNull List<? super SymbolDescriptor> var1, @NonNull String[] var2, @NonNull SymbolProvider.Context var3);

    public abstract int getRetry(SymbolProvider.Result var1);

    @NonNull
    public abstract String getHighlightText(@NonNull SymbolDescriptor var1);

    static {
        try {
            Class.forName(SymbolProvider.Context.class.getName(), true, SymbolProviderAccessor.class.getClassLoader());
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }
}

