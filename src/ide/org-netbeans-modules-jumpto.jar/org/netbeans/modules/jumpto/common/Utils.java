/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.jumpto.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.NonNull;

public class Utils {
    private static Pattern camelCasePattern = Pattern.compile("(?:\\p{javaUpperCase}(?:\\p{javaLowerCase}|\\p{Digit}|\\.|\\$)*){2,}");
    private static final int MAX_INPUT_LENGTH = 1024;
    private static final char[] INVALID_CHARS = new char[]{'\n'};

    private Utils() {
        throw new IllegalStateException();
    }

    public static int containsWildCard(String text) {
        for (int i = 0; i < text.length(); ++i) {
            if (text.charAt(i) != '?' && text.charAt(i) != '*') continue;
            return i;
        }
        return -1;
    }

    public static boolean isCamelCase(String text) {
        return camelCasePattern.matcher(text).matches();
    }

    public static boolean isAllUpper(String text) {
        for (int i = 0; i < text.length(); ++i) {
            if (Character.isUpperCase(text.charAt(i))) continue;
            return false;
        }
        return true;
    }

    @NonNull
    public static String removeNonNeededWildCards(@NonNull String text) {
        StringBuilder sb = new StringBuilder();
        boolean lastAny = false;
        block4 : for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            switch (c) {
                case '*': {
                    if (!lastAny) {
                        sb.append(c);
                    }
                    lastAny = true;
                    continue block4;
                }
                case '?': {
                    if (lastAny) continue block4;
                    sb.append(c);
                    continue block4;
                }
                default: {
                    sb.append(c);
                    lastAny = false;
                }
            }
        }
        return sb.toString();
    }

    public static boolean isValidInput(@NonNull String input) {
        if (input.length() > 1024) {
            return false;
        }
        for (char c : INVALID_CHARS) {
            if (input.indexOf(c) < 0) continue;
            return false;
        }
        return true;
    }
}

