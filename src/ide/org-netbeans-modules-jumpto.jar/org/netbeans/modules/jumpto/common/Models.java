/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Pair
 */
package org.netbeans.modules.jumpto.common;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.AbstractListModel;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.jumpto.common.Factory;
import org.openide.util.Pair;

public final class Models {
    private Models() {
    }

    public static <T> ListModel fromList(List<? extends T> list) {
        return new ListListModel<T>(list);
    }

    public static <T, P> ListModel translating(ListModel model, Factory<T, P> factory) {
        return new TranslatingListModel<T, P>(model, factory);
    }

    public static <R, P> ListModel refreshable(@NonNull ListModel<P> model, @NonNull Factory<R, Pair<P, Runnable>> convertor) {
        return new RefreshableListModel<R, P>(model, convertor);
    }

    public static <T> MutableListModel<T> mutable(@NullAllowed Comparator<? super T> comparator) {
        return new MutableListModelImpl<T>(comparator);
    }

    private static final class MutableListModelImpl<T>
    extends AbstractListModel<T>
    implements MutableListModel<T> {
        private final Comparator<T> comparator;
        private List<T> items;

        MutableListModelImpl(@NullAllowed Comparator<T> comparator) {
            this.comparator = comparator;
            this.items = Collections.emptyList();
        }

        @Override
        public int getSize() {
            assert (SwingUtilities.isEventDispatchThread());
            return this.items.size();
        }

        @Override
        public T getElementAt(int index) {
            assert (SwingUtilities.isEventDispatchThread());
            return this.items.get(index);
        }

        @Override
        public void add(Collection<? extends T> values) {
            Pair<List<T>, List<T>> data;
            boolean success;
            do {
                data = this.getData();
                ((List)data.second()).addAll(values);
                if (this.comparator == null) continue;
                Collections.sort((List)data.second(), this.comparator);
            } while (!(success = this.casData((List)data.first(), (List)data.second())));
        }

        @Override
        public void remove(Collection<? extends T> values) {
            Pair<List<T>, List<T>> data;
            boolean success;
            do {
                data = this.getData();
                ((List)data.second()).removeAll(values);
            } while (!(success = this.casData((List)data.first(), (List)data.second())));
        }

        private Pair<List<T>, List<T>> getData() {
            try {
                return (Pair)MutableListModelImpl.invokeInEDT(new Callable<Pair<List<T>, List<T>>>(){

                    @Override
                    public Pair<List<T>, List<T>> call() throws Exception {
                        ArrayList copy = new ArrayList(MutableListModelImpl.this.items);
                        return Pair.of((Object)MutableListModelImpl.this.items, copy);
                    }
                });
            }
            catch (InterruptedException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }

        private boolean casData(final List<T> expected, final List<T> update) {
            try {
                return (Boolean)MutableListModelImpl.invokeInEDT(new Callable<Boolean>(){

                    @Override
                    public Boolean call() throws Exception {
                        if (MutableListModelImpl.this.items == expected) {
                            int oldSize = MutableListModelImpl.this.items.size();
                            MutableListModelImpl.this.items = update;
                            int newSize = MutableListModelImpl.this.items.size();
                            MutableListModelImpl.this.fireContentsChanged(this, 0, Math.min(oldSize, newSize));
                            if (oldSize < newSize) {
                                MutableListModelImpl.this.fireIntervalAdded(this, oldSize, newSize);
                            } else if (oldSize > newSize) {
                                MutableListModelImpl.this.fireIntervalRemoved(this, newSize, oldSize);
                            }
                            return true;
                        }
                        return false;
                    }
                });
            }
            catch (InterruptedException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }

        private static <R> R invokeInEDT(final @NonNull Callable<R> call) throws InterruptedException, InvocationTargetException {
            final AtomicReference res = new AtomicReference();
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    try {
                        res.set(call.call());
                    }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            return (R)res.get();
        }

    }

    private static final class RefreshableListModel<R, P>
    extends AbstractListModel
    implements ListDataListener {
        private final ListModel delegate;
        private final Factory<R, Pair<P, Runnable>> convertor;
        private final Map<P, R> cache;

        RefreshableListModel(@NonNull ListModel delegate, @NonNull Factory<R, Pair<P, Runnable>> convertor) {
            this.delegate = delegate;
            this.convertor = convertor;
            this.cache = new IdentityHashMap<P, R>();
            delegate.addListDataListener(this);
        }

        @Override
        public int getSize() {
            return this.delegate.getSize();
        }

        @Override
        public Object getElementAt(int index) {
            if (index < 0 || index >= this.delegate.getSize()) {
                throw new IllegalArgumentException(String.format("Invalid index: %d, model size: %d.", index, this.delegate.getSize()));
            }
            final Object orig = this.delegate.getElementAt(index);
            R result = this.cache.get(orig);
            if (result != null) {
                return result;
            }
            result = this.convertor.create((Pair)Pair.of(orig, (Object)new Runnable(){

                @Override
                public void run() {
                    int index = -1;
                    for (int i = 0; i < RefreshableListModel.this.delegate.getSize(); ++i) {
                        if (orig != RefreshableListModel.this.delegate.getElementAt(i)) continue;
                        index = i;
                        break;
                    }
                    if (index != -1) {
                        RefreshableListModel.this.fireContentsChanged(RefreshableListModel.this, index, index);
                    }
                }
            }));
            this.cache.put(orig, result);
            return result;
        }

        @Override
        public void intervalAdded(ListDataEvent e) {
            this.fireIntervalAdded(this, e.getIndex0(), e.getIndex1());
        }

        @Override
        public void intervalRemoved(ListDataEvent e) {
            this.fireIntervalRemoved(this, e.getIndex0(), e.getIndex1());
        }

        @Override
        public void contentsChanged(ListDataEvent e) {
            this.fireContentsChanged(this, e.getIndex0(), e.getIndex1());
        }

    }

    private static class TranslatingListModel<T, P>
    implements ListModel {
        private Factory<T, P> factory;
        private ListModel listModel;

        public TranslatingListModel(ListModel model, Factory<T, P> factory) {
            this.listModel = model;
            this.factory = factory;
        }

        public T getElementAt(int index) {
            Object original = this.listModel.getElementAt(index);
            return this.factory.create(original);
        }

        @Override
        public int getSize() {
            return this.listModel.getSize();
        }

        @Override
        public void removeListDataListener(ListDataListener l) {
        }

        @Override
        public void addListDataListener(ListDataListener l) {
        }
    }

    private static class ListListModel<T>
    implements ListModel {
        private List<? extends T> list;

        public ListListModel(List<? extends T> list) {
            this.list = list;
        }

        public T getElementAt(int index) {
            return this.list.get(index);
        }

        @Override
        public int getSize() {
            return this.list.size();
        }

        @Override
        public void removeListDataListener(ListDataListener l) {
        }

        @Override
        public void addListDataListener(ListDataListener l) {
        }
    }

    public static interface MutableListModel<T>
    extends ListModel<T> {
        public void add(@NonNull Collection<? extends T> var1);

        public void remove(@NonNull Collection<? extends T> var1);
    }

}

