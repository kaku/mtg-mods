/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.jumpto.common;

public interface Factory<C, P> {
    public C create(P var1);
}

