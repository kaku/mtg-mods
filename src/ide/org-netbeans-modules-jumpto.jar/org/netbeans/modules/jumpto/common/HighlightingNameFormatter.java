/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.jumpto.common;

import java.awt.Color;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Parameters;

public class HighlightingNameFormatter {
    private static final String COLOR_FORMAT_PATTERN = "<font style=\"background-color:%s; font-weight:bold; color:%s; white-space:nowrap\">%s</font>";
    private static final String BOLD_FORMAT_PATTERN = "<b>%s</b>";
    private static final String BASE_COLOR_FORMAT_PATTERN = "<font color=\"#%s\">%s</font>";
    private final String formatPattern;

    private HighlightingNameFormatter(@NonNull String pattern) {
        Parameters.notNull((CharSequence)"pattern", (Object)pattern);
        this.formatPattern = pattern;
    }

    @NonNull
    public String formatName(@NonNull String name, @NonNull String textToFind, boolean caseSensitive) {
        if (null == textToFind || "".equals(textToFind)) {
            return name;
        }
        BitSet bitSet = new BitSet(name.length());
        List<String> parts = this.splitByCamelCaseAndWildcards(textToFind);
        String convertedTypeName = caseSensitive ? name : name.toLowerCase();
        int startIndex = 0;
        for (String camelCasePart : parts) {
            int indexOf = convertedTypeName.indexOf(caseSensitive ? camelCasePart : camelCasePart.toLowerCase(), startIndex);
            if (indexOf == -1) break;
            bitSet.set(indexOf, indexOf + camelCasePart.length(), true);
            startIndex = indexOf + camelCasePart.length();
        }
        StringBuilder formattedTypeName = new StringBuilder();
        int i = 0;
        while (i < name.toCharArray().length) {
            boolean isMarked = bitSet.get(i);
            if (isMarked) {
                int numberOfContinuousHighlights = bitSet.nextClearBit(i) - i;
                String part = name.substring(i, i + numberOfContinuousHighlights);
                formattedTypeName.append(String.format(this.formatPattern, part));
                i += numberOfContinuousHighlights;
                continue;
            }
            formattedTypeName.append(name.charAt(i));
            ++i;
        }
        return formattedTypeName.toString();
    }

    @NonNull
    public String formatName(@NonNull String name, @NonNull String textToFind, boolean caseSensitive, @NonNull Color baseColor) {
        String res = this.formatName(name, textToFind, caseSensitive);
        return String.format("<font color=\"#%s\">%s</font>", Integer.toHexString(baseColor.getRGB()).substring(2), res);
    }

    private List<String> splitByCamelCaseAndWildcards(String searchText) {
        StringBuilder sb = new StringBuilder(searchText.length());
        for (char c : searchText.toCharArray()) {
            if (Character.isUpperCase(c)) {
                sb.append("&");
                sb.append(c);
                continue;
            }
            sb.append(c);
        }
        String[] split = sb.toString().split("[&|\\*|\\?]");
        return Arrays.asList(split);
    }

    @NonNull
    public static HighlightingNameFormatter createColorFormatter(@NonNull Color bgColor, @NonNull Color fgColor) {
        String bgColorHighlight = Integer.toHexString(bgColor.getRGB()).substring(2);
        String fgColorHighlight = Integer.toHexString(fgColor.getRGB()).substring(2);
        return new HighlightingNameFormatter(String.format("<font style=\"background-color:%s; font-weight:bold; color:%s; white-space:nowrap\">%s</font>", bgColorHighlight, fgColorHighlight, "%s"));
    }

    @NonNull
    public static HighlightingNameFormatter createBoldFormatter() {
        return new HighlightingNameFormatter(String.format("<b>%s</b>", "%s"));
    }

    @NonNull
    static HighlightingNameFormatter createCustomFormatter(@NonNull String format) {
        return new HighlightingNameFormatter(format);
    }
}

