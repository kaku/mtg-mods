/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.jumpto.common;

import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.jumpto.common.Utils;
import org.openide.awt.StatusDisplayer;
import org.openide.util.NbBundle;

public final class UiUtils {
    private UiUtils() {
        throw new IllegalStateException("No instance allowed.");
    }

    @NonNull
    public static DocumentFilter newUserInputFilter() {
        return new UserInputFilter();
    }

    private static final class UserInputFilter
    extends DocumentFilter {
        private UserInputFilter() {
        }

        @Override
        public void insertString(@NonNull DocumentFilter.FilterBypass fb, int offset, @NonNull String string, @NonNull AttributeSet attr) throws BadLocationException {
            if (Utils.isValidInput(string)) {
                super.insertString(fb, offset, string, attr);
            } else {
                UserInputFilter.handleWrongInput();
            }
        }

        @Override
        public void replace(@NonNull DocumentFilter.FilterBypass fb, int offset, int length, @NonNull String text, @NonNull AttributeSet attrs) throws BadLocationException {
            if (Utils.isValidInput(text)) {
                super.replace(fb, offset, length, text, attrs);
            } else {
                UserInputFilter.handleWrongInput();
            }
        }

        private static void handleWrongInput() {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(UiUtils.class, (String)"TXT_IllegalContent"));
            Toolkit.getDefaultToolkit().beep();
        }
    }

}

