/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.quicksearch.SearchProvider
 *  org.netbeans.spi.quicksearch.SearchRequest
 *  org.netbeans.spi.quicksearch.SearchResponse
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.jumpto.quicksearch;

import java.util.List;
import org.netbeans.modules.jumpto.quicksearch.GoToSymbolWorker;
import org.netbeans.modules.jumpto.symbol.GoToSymbolAction;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;
import org.openide.util.NbBundle;

public class GoToSymbolProvider
implements SearchProvider {
    private GoToSymbolWorker worker;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void evaluate(SearchRequest request, SearchResponse response) {
        GoToSymbolWorker local;
        String text = GoToSymbolProvider.removeNonJavaChars(request.getText());
        if (text.length() == 0) {
            return;
        }
        GoToSymbolProvider goToSymbolProvider = this;
        synchronized (goToSymbolProvider) {
            if (this.worker != null) {
                this.worker.cancel();
            }
            local = this.worker = new GoToSymbolWorker(text);
        }
        local.run();
        for (SymbolDescriptor td : local.getTypes()) {
            String displayHint = td.getFileDisplayPath();
            String htmlDisplayName = GoToSymbolProvider.escapeLtGt(td.getSymbolName()) + " " + NbBundle.getMessage(GoToSymbolAction.class, (String)"MSG_DeclaredIn", (Object)GoToSymbolProvider.escapeLtGt(td.getOwnerName()));
            String projectName = td.getProjectName();
            if (projectName != null && !projectName.isEmpty()) {
                htmlDisplayName = String.format("%s [%s]", htmlDisplayName, projectName);
            }
            if (response.addResult((Runnable)new GoToSymbolCommand(td), htmlDisplayName, displayHint, null)) continue;
            break;
        }
    }

    private static String escapeLtGt(String input) {
        String temp = input.replaceAll("<", "&lt;");
        temp = temp.replaceAll(">", "&gt;");
        return temp;
    }

    private static String removeNonJavaChars(String text) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (!Character.isJavaIdentifierPart(c)) continue;
            sb.append(c);
        }
        return sb.toString();
    }

    private static class GoToSymbolCommand
    implements Runnable {
        private SymbolDescriptor command;

        public GoToSymbolCommand(SymbolDescriptor command) {
            this.command = command;
        }

        @Override
        public void run() {
            this.command.open();
        }
    }

}

