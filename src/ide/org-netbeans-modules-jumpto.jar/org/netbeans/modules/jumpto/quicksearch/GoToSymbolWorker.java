/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.jumpto.quicksearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.ListModel;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jumpto.common.Models;
import org.netbeans.modules.jumpto.symbol.SymbolComparator;
import org.netbeans.modules.jumpto.symbol.SymbolProviderAccessor;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.symbol.SymbolProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.util.Lookup;

public class GoToSymbolWorker
implements Runnable {
    private volatile boolean isCanceled = false;
    private volatile SymbolProvider current;
    private final String text;
    private final long createTime;
    private Logger LOGGER = Logger.getLogger(GoToSymbolWorker.class.getName());
    private Collection<? extends SymbolProvider> typeProviders;
    private List<? extends SymbolDescriptor> types;

    public GoToSymbolWorker(String text) {
        this.text = text;
        this.createTime = System.currentTimeMillis();
    }

    public List<? extends SymbolDescriptor> getTypes() {
        return this.types == null ? Collections.emptyList() : this.types;
    }

    @Override
    public void run() {
        this.LOGGER.fine("Worker for " + this.text + " - started " + (System.currentTimeMillis() - this.createTime) + " ms.");
        this.types = this.getSymbolNames(this.text);
        if (this.isCanceled) {
            this.LOGGER.fine("Worker for " + this.text + " exited after cancel " + (System.currentTimeMillis() - this.createTime) + " ms.");
            return;
        }
        ListModel fmodel = Models.fromList(this.types);
        if (this.isCanceled) {
            this.LOGGER.fine("Worker for " + this.text + " exited after cancel " + (System.currentTimeMillis() - this.createTime) + " ms.");
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        SymbolProvider _provider;
        GoToSymbolWorker goToSymbolWorker = this;
        synchronized (goToSymbolWorker) {
            this.isCanceled = true;
            _provider = this.current;
        }
        if (_provider != null) {
            _provider.cancel();
        }
    }

    private List<? extends SymbolDescriptor> getSymbolNames(String text) {
        ArrayList items = new ArrayList(128);
        String[] message = new String[1];
        SymbolProvider.Context context = SymbolProviderAccessor.DEFAULT.createContext(null, text, SearchType.CASE_INSENSITIVE_PREFIX);
        SymbolProvider.Result result = SymbolProviderAccessor.DEFAULT.createResult(items, message, context);
        Iterator<? extends SymbolProvider> i$ = this.getTypeProviders().iterator();
        while (i$.hasNext()) {
            SymbolProvider provider;
            this.current = provider = i$.next();
            if (this.isCanceled) {
                return null;
            }
            this.LOGGER.fine("Calling SymbolProvider: " + provider);
            provider.computeSymbolNames(context, result);
            this.current = null;
        }
        if (!this.isCanceled) {
            Collections.sort(items, new SymbolComparator());
            return items;
        }
        return null;
    }

    private Collection<? extends SymbolProvider> getTypeProviders() {
        if (this.typeProviders == null) {
            this.typeProviders = Arrays.asList(Lookup.getDefault().lookupAll(SymbolProvider.class).toArray(new SymbolProvider[0]));
        }
        return this.typeProviders;
    }
}

