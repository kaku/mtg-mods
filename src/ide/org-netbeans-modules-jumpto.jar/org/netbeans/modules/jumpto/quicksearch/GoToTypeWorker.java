/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.jumpto.quicksearch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jumpto.EntityComparator;
import org.netbeans.modules.jumpto.type.TypeComparator;
import org.netbeans.modules.jumpto.type.TypeProviderAccessor;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class GoToTypeWorker
implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(GoToTypeWorker.class.getName());
    private volatile boolean isCanceled = false;
    private final String text;
    private final long createTime;
    private List<? extends TypeDescriptor> types = Collections.emptyList();

    public GoToTypeWorker(String text) {
        this.text = text;
        this.createTime = System.currentTimeMillis();
    }

    public List<? extends TypeDescriptor> getTypes() {
        return this.types;
    }

    @Override
    public void run() {
        LOGGER.log(Level.FINE, "Worker for {0} - started {1} ms.", new Object[]{this.text, System.currentTimeMillis() - this.createTime});
        this.types = this.getTypeNames(this.text);
    }

    public void cancel() {
        this.isCanceled = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private List<? extends TypeDescriptor> getTypeNames(String text) {
        ArrayList ccItems;
        ArrayList items;
        items = new ArrayList(128);
        ccItems = new ArrayList(128);
        String[] message = new String[1];
        TypeProvider.Context context = TypeProviderAccessor.DEFAULT.createContext(null, text, SearchType.CASE_INSENSITIVE_PREFIX);
        TypeProvider.Result result = TypeProviderAccessor.DEFAULT.createResult(items, message, context);
        TypeProvider.Context ccContext = TypeProviderAccessor.DEFAULT.createContext(null, text, SearchType.CAMEL_CASE);
        TypeProvider.Result ccResult = TypeProviderAccessor.DEFAULT.createResult(ccItems, message, context);
        Collection providers = Lookup.getDefault().lookupAll(TypeProvider.class);
        try {
            this.computeTypeNames(providers, context, result);
            this.computeTypeNames(providers, ccContext, ccResult);
            if (this.isCanceled) {
                throw new InterruptedException();
            }
        }
        catch (InterruptedException ie) {
            List list = Collections.emptyList();
            return list;
        }
        finally {
            this.cleanUp(providers);
        }
        TreeSet ts = new TreeSet(new TypeComparatorFO());
        ts.addAll(ccItems);
        ts.addAll(items);
        items.clear();
        items.addAll(ts);
        Collections.sort(items, new TypeComparator());
        return items;
    }

    private void computeTypeNames(Collection<? extends TypeProvider> providers, TypeProvider.Context context, TypeProvider.Result result) throws InterruptedException {
        for (TypeProvider provider : providers) {
            if (this.isCanceled) {
                throw new InterruptedException();
            }
            provider.computeTypeNames(context, result);
        }
    }

    private void cleanUp(Collection<? extends TypeProvider> providers) {
        for (TypeProvider tp : providers) {
            try {
                tp.cleanup();
            }
            catch (Throwable t) {
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath)t;
                }
                Exceptions.printStackTrace((Throwable)t);
            }
        }
    }

    private class TypeComparatorFO
    extends EntityComparator<TypeDescriptor> {
        private TypeComparatorFO() {
        }

        @Override
        public int compare(TypeDescriptor t1, TypeDescriptor t2) {
            int cmpr = this.compare(t1.getTypeName(), t2.getTypeName());
            if (cmpr != 0) {
                return cmpr;
            }
            cmpr = this.compare(t1.getOuterName(), t2.getOuterName());
            if (cmpr != 0) {
                return cmpr;
            }
            String fdp1 = t1.getFileDisplayPath();
            if (!fdp1.isEmpty() && !fdp1.equals(t2.getFileDisplayPath())) {
                return -1;
            }
            return this.compare(t1.getContextName(), t2.getContextName());
        }
    }

}

