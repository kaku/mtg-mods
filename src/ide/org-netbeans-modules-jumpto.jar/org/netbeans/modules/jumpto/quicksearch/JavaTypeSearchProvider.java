/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.quicksearch.SearchProvider
 *  org.netbeans.spi.quicksearch.SearchRequest
 *  org.netbeans.spi.quicksearch.SearchResponse
 */
package org.netbeans.modules.jumpto.quicksearch;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.netbeans.modules.jumpto.quicksearch.GoToTypeWorker;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.quicksearch.SearchProvider;
import org.netbeans.spi.quicksearch.SearchRequest;
import org.netbeans.spi.quicksearch.SearchResponse;

public class JavaTypeSearchProvider
implements SearchProvider {
    private final AtomicReference<GoToTypeWorker> workerRef = new AtomicReference();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void evaluate(SearchRequest request, SearchResponse response) {
        String text = JavaTypeSearchProvider.removeNonJavaChars(request.getText());
        if (text.length() == 0) {
            return;
        }
        GoToTypeWorker newWorker = new GoToTypeWorker(text);
        GoToTypeWorker toCancel = this.workerRef.getAndSet(newWorker);
        if (toCancel != null) {
            toCancel.cancel();
        }
        try {
            newWorker.run();
        }
        finally {
            this.workerRef.compareAndSet(newWorker, null);
        }
        for (TypeDescriptor td : newWorker.getTypes()) {
            String displayHint = td.getFileDisplayPath();
            String htmlDisplayName = td.getSimpleName() + td.getContextName();
            String projectName = td.getProjectName();
            if (projectName != null && !projectName.isEmpty()) {
                htmlDisplayName = String.format("%s [%s]", htmlDisplayName, projectName);
            }
            if (response.addResult((Runnable)new GoToTypeCommand(td), htmlDisplayName, displayHint, null)) continue;
            break;
        }
    }

    private static String removeNonJavaChars(String text) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (!Character.isJavaIdentifierPart(c)) continue;
            sb.append(c);
        }
        return sb.toString();
    }

    private static class GoToTypeCommand
    implements Runnable {
        private TypeDescriptor command;

        public GoToTypeCommand(TypeDescriptor command) {
            this.command = command;
        }

        @Override
        public void run() {
            this.command.open();
        }
    }

}

