/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.jumpto.support;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.spi.jumpto.support.NameMatcher;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.util.Parameters;

public final class NameMatcherFactory {
    private NameMatcherFactory() {
    }

    public static NameMatcher createNameMatcher(String text, SearchType type) throws IllegalArgumentException {
        Parameters.notNull((CharSequence)"text", (Object)text);
        Parameters.notNull((CharSequence)"type", (Object)((Object)type));
        try {
            switch (type) {
                case EXACT_NAME: {
                    return new ExactNameMatcher(text);
                }
                case CASE_INSENSITIVE_EXACT_NAME: {
                    return new CaseInsensitiveExactNameMatcher(text);
                }
                case PREFIX: {
                    return new PrefixNameMatcher(text);
                }
                case REGEXP: {
                    return new RegExpNameMatcher(NameMatcherFactory.wildcardsToRegexp(text, true), true);
                }
                case CASE_INSENSITIVE_REGEXP: {
                    return new RegExpNameMatcher(NameMatcherFactory.wildcardsToRegexp(text, true), false);
                }
                case CASE_INSENSITIVE_PREFIX: {
                    return new CaseInsensitivePrefixNameMatcher(text);
                }
                case CAMEL_CASE: {
                    return new CamelCaseNameMatcher(text);
                }
            }
            throw new IllegalArgumentException("Unsupported type: " + (Object)((Object)type));
        }
        catch (PatternSyntaxException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    public static String wildcardsToRegexp(String pattern, boolean prefix) {
        String result = pattern.replace("{", "").replace("}", "").replace("[", "").replace("]", "").replace("(", "").replace(")", "").replace("\\", "").replace(".", "\\.").replace("*", ".*").replace('?', '.');
        if (prefix) {
            result = result.concat(".*");
        }
        return result;
    }

    private static final class CamelCaseNameMatcher
    implements NameMatcher {
        private final Pattern pattern;

        public CamelCaseNameMatcher(String name) {
            int index;
            if (name.length() == 0) {
                throw new IllegalArgumentException();
            }
            StringBuilder patternString = new StringBuilder();
            int lastIndex = 0;
            do {
                String token = name.substring(lastIndex, (index = CamelCaseNameMatcher.findNextUpper(name, lastIndex + 1)) == -1 ? name.length() : index);
                patternString.append(Pattern.quote(token));
                patternString.append(index != -1 ? "[\\p{Lower}\\p{Digit}_]*" : ".*");
                lastIndex = index;
            } while (index != -1);
            this.pattern = Pattern.compile(patternString.toString());
        }

        private static int findNextUpper(String text, int offset) {
            for (int i = offset; i < text.length(); ++i) {
                if (!Character.isUpperCase(text.charAt(i))) continue;
                return i;
            }
            return -1;
        }

        @Override
        public final boolean accept(String name) {
            return name != null && this.pattern.matcher(name).matches();
        }
    }

    private static final class RegExpNameMatcher
    implements NameMatcher {
        private final Pattern pattern;

        public RegExpNameMatcher(String patternText, boolean caseSensitive) {
            this.pattern = Pattern.compile(patternText, caseSensitive ? 0 : 2);
        }

        @Override
        public final boolean accept(String name) {
            return name != null && this.pattern.matcher(name).matches();
        }
    }

    private static final class CaseInsensitivePrefixNameMatcher
    extends BaseNameMatcher {
        public CaseInsensitivePrefixNameMatcher(String patternText) {
            super(patternText.toLowerCase());
        }

        @Override
        public final boolean accept(String name) {
            return name != null && name.toLowerCase().startsWith(this.patternText);
        }
    }

    private static final class PrefixNameMatcher
    extends BaseNameMatcher {
        public PrefixNameMatcher(String patternText) {
            super(patternText);
        }

        @Override
        public final boolean accept(String name) {
            return name != null && name.startsWith(this.patternText);
        }
    }

    private static final class CaseInsensitiveExactNameMatcher
    extends BaseNameMatcher {
        public CaseInsensitiveExactNameMatcher(String patternText) {
            super(patternText);
        }

        @Override
        public final boolean accept(String name) {
            return this.patternText.equalsIgnoreCase(name);
        }
    }

    private static final class ExactNameMatcher
    extends BaseNameMatcher {
        public ExactNameMatcher(String patternText) {
            super(patternText);
        }

        @Override
        public final boolean accept(String name) {
            return this.patternText.equals(name);
        }
    }

    private static abstract class BaseNameMatcher
    implements NameMatcher {
        protected final String patternText;

        protected BaseNameMatcher(String patternText) {
            this.patternText = patternText;
        }
    }

}

