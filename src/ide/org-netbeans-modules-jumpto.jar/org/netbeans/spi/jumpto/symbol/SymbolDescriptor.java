/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.jumpto.symbol;

import javax.swing.Icon;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public abstract class SymbolDescriptor {
    private String matchedSymbolName;

    public abstract Icon getIcon();

    public abstract String getSymbolName();

    public abstract String getOwnerName();

    public abstract String getProjectName();

    public abstract Icon getProjectIcon();

    public abstract FileObject getFileObject();

    public abstract int getOffset();

    public abstract void open();

    @NonNull
    public String getFileDisplayPath() {
        FileObject fo = this.getFileObject();
        return fo == null ? "" : FileUtil.getFileDisplayName((FileObject)fo);
    }

    final void setHighlightText(@NonNull String matchedSymbolName) {
        Parameters.notNull((CharSequence)"matchedSymbolName", (Object)matchedSymbolName);
        this.matchedSymbolName = matchedSymbolName;
    }

    @NonNull
    final String getHighlightText() {
        return this.matchedSymbolName;
    }
}

