/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.jumpto.symbol;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jumpto.symbol.SymbolProviderAccessor;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.util.Parameters;

public interface SymbolProvider {
    public String name();

    public String getDisplayName();

    public void computeSymbolNames(Context var1, Result var2);

    public void cancel();

    public void cleanup();

    public static final class Result {
        private final List<? super SymbolDescriptor> result;
        private final String[] message;
        private String highlightText;
        private boolean dirty;
        private boolean highlightTextAlreadySet;
        private int retry;

        Result(@NonNull List<? super SymbolDescriptor> result, @NonNull String[] message, @NonNull Context context) {
            Parameters.notNull((CharSequence)"result", result);
            Parameters.notNull((CharSequence)"message", (Object)message);
            Parameters.notNull((CharSequence)"context", (Object)context);
            if (message.length != 1) {
                throw new IllegalArgumentException("message.length != 1");
            }
            this.result = result;
            this.message = message;
            this.highlightText = context.getText();
        }

        public void setMessage(String msg) {
            this.message[0] = msg;
        }

        public void addResult(SymbolDescriptor symbolDescriptor) {
            this.dirty = true;
            symbolDescriptor.setHighlightText(this.highlightText);
            this.result.add(symbolDescriptor);
        }

        public void addResult(List<? extends SymbolDescriptor> symbolDescriptors) {
            for (SymbolDescriptor symbolDescriptor : symbolDescriptors) {
                this.addResult(symbolDescriptor);
            }
        }

        public void setHighlightText(@NonNull String textToHighlight) {
            Parameters.notNull((CharSequence)"textToHighlight", (Object)textToHighlight);
            if (this.dirty) {
                throw new IllegalStateException("Calling setHighlightText after addResult");
            }
            if (this.highlightTextAlreadySet) {
                throw new IllegalStateException("Highlight text already set");
            }
            this.highlightText = textToHighlight;
            this.highlightTextAlreadySet = true;
        }

        public void pendingResult() {
            this.retry = 2000;
        }
    }

    public static final class Context {
        private final Project project;
        private final String text;
        private final SearchType type;

        Context(Project project, String text, SearchType type) {
            this.project = project;
            this.text = text;
            this.type = type;
        }

        public Project getProject() {
            return this.project;
        }

        public String getText() {
            return this.text;
        }

        public SearchType getSearchType() {
            return this.type;
        }

        static {
            SymbolProviderAccessor.DEFAULT = new SymbolProviderAccessor(){

                @Override
                public Context createContext(Project p, String text, SearchType t) {
                    return new Context(p, text, t);
                }

                @NonNull
                @Override
                public Result createResult(@NonNull List<? super SymbolDescriptor> result, @NonNull String[] message, @NonNull Context context) {
                    return new Result(result, message, context);
                }

                @Override
                public int getRetry(Result result) {
                    return result.retry;
                }

                @NonNull
                @Override
                public String getHighlightText(@NonNull SymbolDescriptor desc) {
                    return desc.getHighlightText();
                }
            };
        }

    }

}

