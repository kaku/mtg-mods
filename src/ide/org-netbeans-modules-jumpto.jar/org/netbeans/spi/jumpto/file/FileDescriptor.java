/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.spi.jumpto.file;

import javax.swing.Icon;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public abstract class FileDescriptor {
    private boolean preferred;

    public abstract String getFileName();

    public abstract String getOwnerPath();

    public abstract Icon getIcon();

    public abstract String getProjectName();

    public abstract Icon getProjectIcon();

    public abstract void open();

    public abstract FileObject getFileObject();

    public String getFileDisplayPath() {
        FileObject fo = this.getFileObject();
        return fo == null ? "" : FileUtil.getFileDisplayName((FileObject)fo);
    }

    final boolean isFromCurrentProject() {
        return this.preferred;
    }

    final void setFromCurrentProject(boolean preferred) {
        this.preferred = preferred;
    }
}

