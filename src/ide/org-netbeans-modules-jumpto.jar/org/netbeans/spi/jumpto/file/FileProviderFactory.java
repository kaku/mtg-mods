/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.jumpto.file;

import org.netbeans.spi.jumpto.file.FileProvider;

public interface FileProviderFactory {
    public String name();

    public String getDisplayName();

    public FileProvider createFileProvider();
}

