/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.jumpto.file;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jumpto.file.FileDescription;
import org.netbeans.modules.jumpto.file.FileProviderAccessor;
import org.netbeans.spi.jumpto.file.FileDescriptor;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public interface FileProvider {
    public boolean computeFiles(Context var1, Result var2);

    public void cancel();

    public static final class Result {
        private final List<? super FileDescriptor> result;
        private final String[] message;
        private final Context ctx;
        private int retry;

        public void setMessage(String msg) {
            this.message[0] = msg;
        }

        public void addFile(FileObject file) {
            Parameters.notNull((CharSequence)"file", (Object)file);
            String path = FileUtil.getRelativePath((FileObject)this.ctx.getRoot(), (FileObject)file);
            if (path == null) {
                path = FileUtil.getFileDisplayName((FileObject)file);
            }
            Project prj = this.ctx.getProject();
            int lineNr = this.ctx.getLineNumber();
            this.result.add(this.setFromCurrentProject(new FileDescription(file, path, prj, lineNr)));
        }

        public void addFileDescriptor(FileDescriptor desc) {
            Parameters.notNull((CharSequence)"desc", (Object)desc);
            this.result.add(this.setFromCurrentProject(desc));
        }

        public void pendingResult() {
            this.retry = 2000;
        }

        private Result(@NonNull List<? super FileDescriptor> result, @NonNull String[] message, @NonNull Context ctx) {
            Parameters.notNull((CharSequence)"result", result);
            Parameters.notNull((CharSequence)"message", (Object)message);
            Parameters.notNull((CharSequence)"ctx", (Object)ctx);
            if (message.length != 1) {
                throw new IllegalArgumentException("message.length != 1");
            }
            this.result = result;
            this.message = message;
            this.ctx = ctx;
        }

        private FileDescriptor setFromCurrentProject(FileDescriptor desc) {
            Project prj = this.ctx.getProject();
            Project curPrj = this.ctx.getCurrentProject();
            desc.setFromCurrentProject(curPrj != null && prj != null && curPrj.getProjectDirectory() == prj.getProjectDirectory());
            return desc;
        }
    }

    public static final class Context {
        private final String text;
        private final SearchType type;
        private final int lineNr;
        private final Project currentProject;
        private FileObject sourceGroupRoot;
        private Project project;

        public Project getProject() {
            if (this.project == null) {
                this.project = FileOwnerQuery.getOwner((FileObject)this.sourceGroupRoot);
            }
            return this.project;
        }

        public FileObject getRoot() {
            return this.sourceGroupRoot;
        }

        public String getText() {
            return this.text;
        }

        public SearchType getSearchType() {
            return this.type;
        }

        public int getLineNumber() {
            return this.lineNr;
        }

        private Context(@NonNull String text, @NonNull SearchType type, int lineNr, @NullAllowed Project currentProject) {
            Parameters.notNull((CharSequence)"text", (Object)text);
            Parameters.notNull((CharSequence)"type", (Object)((Object)type));
            this.text = text;
            this.type = type;
            this.lineNr = lineNr;
            this.currentProject = currentProject;
        }

        Project getCurrentProject() {
            return this.currentProject;
        }

        static {
            FileProviderAccessor.setInstance(new FileProviderAccessor(){

                @Override
                public Context createContext(@NonNull String text, @NonNull SearchType searchType, int lineNr, @NullAllowed Project currentProject) {
                    return new Context(text, searchType, lineNr, currentProject);
                }

                @Override
                public Result createResult(List<? super FileDescriptor> result, String[] message, Context ctx) {
                    return new Result(result, message, ctx);
                }

                @Override
                public int getRetry(Result result) {
                    return result.retry;
                }

                @Override
                public void setRoot(Context ctx, FileObject root) {
                    ctx.sourceGroupRoot = root;
                    ctx.project = null;
                }

                @Override
                public void setFromCurrentProject(FileDescriptor desc, boolean value) {
                    desc.setFromCurrentProject(value);
                }

                @Override
                public boolean isFromCurrentProject(FileDescriptor desc) {
                    return desc.isFromCurrentProject();
                }
            });
        }

    }

}

