/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.jumpto.type;

import java.util.Collection;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.modules.jumpto.type.TypeProviderAccessor;
import org.netbeans.spi.jumpto.type.SearchType;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.openide.util.Parameters;

public interface TypeProvider {
    public String name();

    public String getDisplayName();

    public void computeTypeNames(Context var1, Result var2);

    public void cancel();

    public void cleanup();

    public static final class Result {
        private Collection<? super TypeDescriptor> result;
        private String[] message;
        private int retry;
        private String highlightText;
        private boolean highlightTextAlreadySet;
        private boolean modified;

        Result(@NonNull Collection<? super TypeDescriptor> result, @NonNull String[] message, @NonNull Context context) {
            Parameters.notNull((CharSequence)"result", result);
            Parameters.notNull((CharSequence)"message", (Object)message);
            Parameters.notNull((CharSequence)"context", (Object)context);
            if (message.length != 1) {
                throw new IllegalArgumentException("Message.length != 1");
            }
            this.result = result;
            this.message = message;
            this.highlightText = context.getText();
        }

        public void setMessage(String msg) {
            this.message[0] = msg;
        }

        public void addResult(@NonNull TypeDescriptor typeDescriptor) {
            typeDescriptor.setHighlightText(this.highlightText);
            this.result.add(typeDescriptor);
            this.modified = true;
        }

        public void addResult(@NonNull List<? extends TypeDescriptor> typeDescriptors) {
            for (TypeDescriptor typeDescriptor : typeDescriptors) {
                this.addResult(typeDescriptor);
            }
        }

        public void pendingResult() {
            this.retry = 2000;
        }

        public void setHighlightText(@NonNull String textToHighlight) {
            Parameters.notNull((CharSequence)"textToHighlight", (Object)textToHighlight);
            if (this.modified) {
                throw new IllegalStateException("Calling setHighlightText after addResult");
            }
            if (this.highlightTextAlreadySet) {
                throw new IllegalStateException("Highlight text already set");
            }
            this.highlightText = textToHighlight;
            this.highlightTextAlreadySet = true;
        }
    }

    public static final class Context {
        private final Project project;
        private final String text;
        private final SearchType type;

        Context(Project project, String text, SearchType type) {
            this.project = project;
            this.text = text;
            this.type = type;
        }

        public Project getProject() {
            return this.project;
        }

        public String getText() {
            return this.text;
        }

        public SearchType getSearchType() {
            return this.type;
        }

        static {
            TypeProviderAccessor.DEFAULT = new TypeProviderAccessor(){

                @Override
                public Context createContext(Project p, String text, SearchType t) {
                    return new Context(p, text, t);
                }

                @NonNull
                @Override
                public Result createResult(@NonNull Collection<? super TypeDescriptor> result, @NonNull String[] message, @NonNull Context context) {
                    return new Result(result, message, context);
                }

                @Override
                public int getRetry(Result result) {
                    return result.retry;
                }

                @NonNull
                @Override
                public String getHighlightText(@NonNull TypeDescriptor td) {
                    return td.getHighlightText();
                }
            };
        }

    }

}

