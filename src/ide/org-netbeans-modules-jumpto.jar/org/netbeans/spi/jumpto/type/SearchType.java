/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.jumpto.type;

public enum SearchType {
    EXACT_NAME,
    CASE_INSENSITIVE_EXACT_NAME,
    PREFIX,
    CASE_INSENSITIVE_PREFIX,
    CAMEL_CASE,
    REGEXP,
    CASE_INSENSITIVE_REGEXP;
    

    private SearchType() {
    }
}

