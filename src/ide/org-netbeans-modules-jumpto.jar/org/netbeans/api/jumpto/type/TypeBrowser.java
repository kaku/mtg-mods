/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.jumpto.type;

import java.util.Iterator;
import org.netbeans.modules.jumpto.type.GoToTypeAction;
import org.netbeans.spi.jumpto.type.TypeDescriptor;
import org.netbeans.spi.jumpto.type.TypeProvider;

public final class TypeBrowser {
    public static /* varargs */ TypeDescriptor browse(String title, Filter filter, TypeProvider ... typeProviders) {
        return TypeBrowser.browse(title, null, filter, typeProviders);
    }

    public static /* varargs */ TypeDescriptor browse(String title, String initialText, Filter filter, TypeProvider ... typeProviders) {
        GoToTypeAction goToTypeAction = new GoToTypeAction(title, filter, false, typeProviders);
        Iterable<? extends TypeDescriptor> tds = goToTypeAction.getSelectedTypes(true, initialText);
        return tds.iterator().hasNext() ? tds.iterator().next() : null;
    }

    public static interface Filter {
        public boolean accept(TypeDescriptor var1);
    }

}

