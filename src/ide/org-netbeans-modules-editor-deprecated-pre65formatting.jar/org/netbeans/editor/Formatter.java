/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.Analyzer
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.GuardedException
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib.KitsTracker
 *  org.netbeans.modules.editor.lib.SettingsConversions
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.awt.Toolkit;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.EventListener;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.deprecated.pre65formatting.LegacyFormattersProvider;
import org.netbeans.modules.editor.lib.KitsTracker;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public class Formatter {
    private static final Map<Class, Formatter> kitClass2Formatter = new WeakHashMap<Class, Formatter>();
    private static final Map<MimePath, Formatter> mimePath2Formatter = new WeakHashMap<MimePath, Formatter>();
    private static final int ISC_MAX_TAB_SIZE = 16;
    private static final int ISC_MAX_INDENT_SIZE = 32;
    private static final String[][] indentStringCache = new String[16][];
    private final Class kitClass;
    private boolean inited;
    private int tabSize;
    private boolean customTabSize;
    private Integer shiftWidth;
    private boolean customShiftWidth;
    private boolean expandTabs;
    private boolean customExpandTabs;
    private int spacesPerTab;
    private boolean customSpacesPerTab;
    private final Preferences prefs;
    private final PreferenceChangeListener prefsListener;
    private static boolean noIndentUtils = false;
    private static WeakReference<Class> indentUtilsClassRef = null;

    public static synchronized Formatter getFormatter(Class kitClass) {
        String mimeType = KitsTracker.getInstance().findMimeType(kitClass);
        if (mimeType != null) {
            return Formatter.getFormatter(mimeType);
        }
        Formatter formatter = kitClass2Formatter.get(kitClass);
        if (formatter == null) {
            BaseKit kit = BaseKit.getKit((Class)kitClass);
            formatter = Formatter.callCreateFormatterMethod(kit);
            kitClass2Formatter.put(kitClass, formatter);
        }
        return formatter;
    }

    public static synchronized Formatter getFormatter(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        Formatter formatter = mimePath2Formatter.get((Object)mimePath);
        if (formatter == null) {
            EditorKit editorKit = (EditorKit)MimeLookup.getLookup((MimePath)mimePath).lookup(EditorKit.class);
            formatter = editorKit instanceof BaseKit ? Formatter.callCreateFormatterMethod((BaseKit)editorKit) : Formatter.callCreateFormatterMethod(BaseKit.getKit(BaseKit.class));
            mimePath2Formatter.put(mimePath, formatter);
        }
        return formatter;
    }

    private static synchronized void setFormatter(String mimeType, Formatter formatter) {
        mimePath2Formatter.put(MimePath.parse((String)mimeType), formatter);
    }

    public static synchronized void setFormatter(Class kitClass, Formatter formatter) {
        String mimeType = KitsTracker.getInstance().findMimeType(kitClass);
        if (mimeType != null) {
            Formatter.setFormatter(mimeType, formatter);
        } else {
            kitClass2Formatter.put(kitClass, formatter);
        }
    }

    public Formatter(Class kitClass) {
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String key;
                int shw;
                String string = key = evt == null ? null : evt.getKey();
                if (!(Formatter.this.inited && key != null && !"tab-size".equals(key) || Formatter.this.customTabSize)) {
                    Formatter.this.tabSize = Formatter.this.prefs.getInt("tab-size", 8);
                }
                if (!Formatter.this.customShiftWidth && (shw = Formatter.this.prefs.getInt("indent-shift-width", -1)) >= 0) {
                    Formatter.this.shiftWidth = shw;
                }
                if (!(Formatter.this.inited && key != null && !"expand-tabs".equals(key) || Formatter.this.customExpandTabs)) {
                    Formatter.this.expandTabs = Formatter.this.prefs.getBoolean("expand-tabs", true);
                }
                if (!(Formatter.this.inited && key != null && !"spaces-per-tab".equals(key) || Formatter.this.customSpacesPerTab)) {
                    Formatter.this.spacesPerTab = Formatter.this.prefs.getInt("spaces-per-tab", 4);
                }
                Formatter.this.inited = true;
                SettingsConversions.callSettingsChange((Object)Formatter.this);
            }
        };
        this.kitClass = kitClass;
        String mimeType = BaseKit.getKit((Class)kitClass).getContentType();
        this.prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.parse((String)mimeType)).lookup(Preferences.class);
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs));
    }

    public Class getKitClass() {
        return this.kitClass;
    }

    public int getTabSize() {
        Object ret;
        Document doc = LegacyFormattersProvider.getFormattingContextDocument();
        if (doc != null && (ret = Formatter.callIndentUtils("tabSize", doc)) instanceof Integer) {
            return (Integer)ret;
        }
        if (!this.customTabSize && !this.inited) {
            this.prefsListener.preferenceChange(null);
        }
        return this.tabSize;
    }

    public void setTabSize(int tabSize) {
        this.customTabSize = true;
        this.tabSize = tabSize;
    }

    public int getShiftWidth() {
        Object ret;
        Document doc = LegacyFormattersProvider.getFormattingContextDocument();
        if (doc != null && (ret = Formatter.callIndentUtils("indentLevelSize", doc)) instanceof Integer) {
            return (Integer)ret;
        }
        if (!this.customShiftWidth && !this.inited) {
            this.prefsListener.preferenceChange(null);
        }
        return this.shiftWidth != null ? this.shiftWidth.intValue() : this.getSpacesPerTab();
    }

    public void setShiftWidth(int shiftWidth) {
        this.customShiftWidth = true;
        if (this.shiftWidth == null || this.shiftWidth != shiftWidth) {
            this.shiftWidth = new Integer(shiftWidth);
        }
    }

    public boolean expandTabs() {
        Object ret;
        Document doc = LegacyFormattersProvider.getFormattingContextDocument();
        if (doc != null && (ret = Formatter.callIndentUtils("isExpandTabs", doc)) instanceof Boolean) {
            return (Boolean)ret;
        }
        if (!this.customExpandTabs && !this.inited) {
            this.prefsListener.preferenceChange(null);
        }
        return this.expandTabs;
    }

    public void setExpandTabs(boolean expandTabs) {
        this.customExpandTabs = true;
        this.expandTabs = expandTabs;
    }

    public int getSpacesPerTab() {
        Object ret;
        Document doc = LegacyFormattersProvider.getFormattingContextDocument();
        if (doc != null && (ret = Formatter.callIndentUtils("indentLevelSize", doc)) instanceof Integer) {
            return (Integer)ret;
        }
        if (!this.customSpacesPerTab && !this.inited) {
            this.prefsListener.preferenceChange(null);
        }
        return this.spacesPerTab;
    }

    public void setSpacesPerTab(int spacesPerTab) {
        this.customSpacesPerTab = true;
        this.spacesPerTab = spacesPerTab;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getIndentString(BaseDocument doc, int indent) {
        LegacyFormattersProvider.pushFormattingContextDocument((Document)doc);
        try {
            String string = Formatter.getIndentString(indent, this.expandTabs(), doc.getTabSize());
            return string;
        }
        finally {
            LegacyFormattersProvider.popFormattingContextDocument((Document)doc);
        }
    }

    public String getIndentString(int indent) {
        return Formatter.getIndentString(indent, this.expandTabs(), this.getTabSize());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void insertTabString(final BaseDocument doc, final int dotPos) throws BadLocationException {
        LegacyFormattersProvider.pushFormattingContextDocument((Document)doc);
        try {
            final BadLocationException[] badLocationExceptions = new BadLocationException[1];
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        int ind;
                        int rsPos = Utilities.getRowStart((BaseDocument)doc, (int)dotPos);
                        int startPos = Utilities.getFirstNonWhiteBwd((BaseDocument)doc, (int)dotPos, (int)rsPos);
                        startPos = startPos >= 0 ? startPos + 1 : rsPos;
                        int startCol = Utilities.getVisualColumn((BaseDocument)doc, (int)startPos);
                        int endCol = Utilities.getNextTabColumn((BaseDocument)doc, (int)dotPos);
                        String tabStr = Analyzer.getWhitespaceString((int)startCol, (int)endCol, (boolean)Formatter.this.expandTabs(), (int)doc.getTabSize());
                        char[] removeChars = doc.getChars(startPos, dotPos - startPos);
                        for (ind = 0; ind < removeChars.length && removeChars[ind] == tabStr.charAt(ind); ++ind) {
                        }
                        doc.remove(startPos, dotPos - (startPos += ind));
                        doc.insertString(startPos, tabStr.substring(ind), null);
                    }
                    catch (BadLocationException ex) {
                        badLocationExceptions[0] = ex;
                    }
                }
            });
            if (badLocationExceptions[0] != null) {
                throw badLocationExceptions[0];
            }
        }
        finally {
            LegacyFormattersProvider.popFormattingContextDocument((Document)doc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void changeRowIndent(final BaseDocument doc, final int pos, final int newIndent) throws BadLocationException {
        LegacyFormattersProvider.pushFormattingContextDocument((Document)doc);
        try {
            final BadLocationException[] badLocationExceptions = new BadLocationException[1];
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        int indent = newIndent < 0 ? 0 : newIndent;
                        int firstNW = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)pos);
                        if (firstNW == -1) {
                            firstNW = Utilities.getRowEnd((BaseDocument)doc, (int)pos);
                        }
                        int replacePos = Utilities.getRowStart((BaseDocument)doc, (int)pos);
                        int removeLen = firstNW - replacePos;
                        CharSequence removeText = DocumentUtilities.getText((Document)doc, (int)replacePos, (int)removeLen);
                        String newIndentText = Formatter.this.getIndentString(doc, indent);
                        if (CharSequenceUtilities.startsWith((CharSequence)newIndentText, (CharSequence)removeText)) {
                            newIndentText = newIndentText.substring(removeLen);
                            replacePos += removeLen;
                            removeLen = 0;
                        } else if (CharSequenceUtilities.endsWith((CharSequence)newIndentText, (CharSequence)removeText)) {
                            newIndentText = newIndentText.substring(0, newIndentText.length() - removeLen);
                            removeLen = 0;
                        }
                        if (removeLen != 0) {
                            doc.remove(replacePos, removeLen);
                        }
                        doc.insertString(replacePos, newIndentText, null);
                    }
                    catch (BadLocationException ex) {
                        badLocationExceptions[0] = ex;
                    }
                }
            });
            if (badLocationExceptions[0] != null) {
                throw badLocationExceptions[0];
            }
        }
        finally {
            LegacyFormattersProvider.popFormattingContextDocument((Document)doc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void changeBlockIndent(final BaseDocument doc, final int startPos, final int endPos, final int shiftCnt) throws BadLocationException {
        LegacyFormattersProvider.pushFormattingContextDocument((Document)doc);
        try {
            GuardedDocument gdoc;
            GuardedDocument guardedDocument = gdoc = doc instanceof GuardedDocument ? (GuardedDocument)doc : null;
            if (gdoc != null) {
                for (int i = startPos; i < endPos; ++i) {
                    if (!gdoc.isPosGuarded(i)) continue;
                    Toolkit.getDefaultToolkit().beep();
                    return;
                }
            }
            final BadLocationException[] badLocationExceptions = new BadLocationException[1];
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        int indentDelta = shiftCnt * doc.getShiftWidth();
                        int end = endPos > 0 && Utilities.getRowStart((BaseDocument)doc, (int)endPos) == endPos ? endPos - 1 : endPos;
                        int pos = Utilities.getRowStart((BaseDocument)doc, (int)startPos);
                        for (int lineCnt = Utilities.getRowCount((BaseDocument)doc, (int)startPos, (int)end); lineCnt > 0; --lineCnt) {
                            int indent = Utilities.getRowIndent((BaseDocument)doc, (int)pos);
                            if (Utilities.isRowWhite((BaseDocument)doc, (int)pos)) {
                                indent = - indentDelta;
                            }
                            Formatter.this.changeRowIndent(doc, pos, Math.max(indent + indentDelta, 0));
                            pos = Utilities.getRowStart((BaseDocument)doc, (int)pos, (int)1);
                        }
                    }
                    catch (BadLocationException ex) {
                        badLocationExceptions[0] = ex;
                    }
                }
            });
            if (badLocationExceptions[0] != null) {
                throw badLocationExceptions[0];
            }
        }
        finally {
            LegacyFormattersProvider.popFormattingContextDocument((Document)doc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void shiftLine(BaseDocument doc, int dotPos, boolean right) throws BadLocationException {
        LegacyFormattersProvider.pushFormattingContextDocument((Document)doc);
        try {
            int ind = doc.getShiftWidth();
            if (!right) {
                ind = - ind;
            }
            ind = Utilities.isRowWhite((BaseDocument)doc, (int)dotPos) ? (ind += Utilities.getVisualColumn((BaseDocument)doc, (int)dotPos)) : (ind += Utilities.getRowIndent((BaseDocument)doc, (int)dotPos));
            ind = Math.max(ind, 0);
            this.changeRowIndent(doc, dotPos, ind);
        }
        finally {
            LegacyFormattersProvider.popFormattingContextDocument((Document)doc);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int reformat(BaseDocument doc, int startOffset, int endOffset) throws BadLocationException {
        LegacyFormattersProvider.pushFormattingContextDocument((Document)doc);
        try {
            CharArrayWriter cw = new CharArrayWriter();
            Writer w = this.createWriter((Document)doc, startOffset, cw);
            String originalString = doc.getText(startOffset, endOffset - startOffset);
            w.write(originalString);
            w.close();
            String out = new String(cw.toCharArray());
            if (!out.equals(originalString)) {
                doc.remove(startOffset, endOffset - startOffset);
                doc.insertString(startOffset, out, null);
                int n = out.length();
                return n;
            }
            int n = 0;
            return n;
        }
        catch (IOException e) {
            Utilities.annotateLoggable((Throwable)e);
            int w = 0;
            return w;
        }
        finally {
            LegacyFormattersProvider.popFormattingContextDocument((Document)doc);
        }
    }

    public int indentLine(Document doc, int offset) {
        return offset;
    }

    public int indentNewLine(Document doc, int offset) {
        try {
            doc.insertString(offset, "\n", null);
            ++offset;
        }
        catch (GuardedException e) {
            Toolkit.getDefaultToolkit().beep();
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable((Throwable)e);
        }
        return offset;
    }

    public Writer createWriter(Document doc, int offset, Writer writer) {
        return writer;
    }

    public void indentLock() {
    }

    public void indentUnlock() {
    }

    public void reformatLock() {
    }

    public void reformatUnlock() {
    }

    private static Object callIndentUtils(String methodName, Document doc) {
        Class indentUtilsClass;
        if (noIndentUtils) {
            return null;
        }
        Class class_ = indentUtilsClass = indentUtilsClassRef == null ? null : indentUtilsClassRef.get();
        if (indentUtilsClass == null) {
            try {
                ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                indentUtilsClass = loader.loadClass("org.netbeans.modules.editor.indent.api.IndentUtils");
                indentUtilsClassRef = new WeakReference(indentUtilsClass);
            }
            catch (Exception e) {
                noIndentUtils = true;
                return null;
            }
        }
        try {
            Method m = indentUtilsClass.getDeclaredMethod(methodName, Document.class);
            return m.invoke(null, doc);
        }
        catch (Exception e) {
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String getIndentString(int indent, boolean expandTabs, int tabSize) {
        if (indent <= 0) {
            return "";
        }
        if (expandTabs) {
            tabSize = 0;
        }
        String[][] arrstring = indentStringCache;
        synchronized (arrstring) {
            boolean large = tabSize >= indentStringCache.length || indent > 32;
            String indentString = null;
            String[] tabCache = null;
            if (!large) {
                tabCache = indentStringCache[tabSize];
                if (tabCache == null) {
                    tabCache = new String[32];
                    Formatter.indentStringCache[tabSize] = tabCache;
                }
                indentString = tabCache[indent - 1];
            }
            if (indentString == null) {
                indentString = Analyzer.getIndentString((int)indent, (boolean)expandTabs, (int)tabSize);
                if (!large) {
                    tabCache[indent - 1] = indentString;
                }
            }
            return indentString;
        }
    }

    private static Formatter callCreateFormatterMethod(BaseKit kit) {
        try {
            Method m = kit.getClass().getMethod("createFormatter", new Class[0]);
            return (Formatter)m.invoke((Object)kit, new Object[0]);
        }
        catch (Exception e) {
            return new Formatter(kit.getClass());
        }
    }

}

