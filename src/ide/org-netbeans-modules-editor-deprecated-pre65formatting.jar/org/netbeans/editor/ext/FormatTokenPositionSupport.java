/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.TokenItem
 */
package org.netbeans.editor.ext;

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.text.Position;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.ext.FormatTokenPosition;
import org.netbeans.editor.ext.FormatWriter;

class FormatTokenPositionSupport {
    private final FormatWriter formatWriter;
    private SaveSet firstSet;
    private SaveSet lastSet;
    private final HashMap tokens2positionLists = new HashMap();

    FormatTokenPositionSupport(FormatWriter formatWriter) {
        this.formatWriter = formatWriter;
    }

    private ArrayList getPosList(TokenItem token) {
        ArrayList ret = (ArrayList)this.tokens2positionLists.get((Object)token);
        if (ret == null) {
            ret = new ArrayList(3);
            this.tokens2positionLists.put(token, ret);
        }
        return ret;
    }

    synchronized ExtTokenPosition getTokenPosition(TokenItem token, int offset, Position.Bias bias) {
        ExtTokenPosition etp;
        if (token == null) {
            if (offset != 0) {
                throw new IllegalArgumentException("Ending token position has non-zero offset=" + offset);
            }
        } else if (offset >= token.getImage().length()) {
            throw new IllegalArgumentException("Offset=" + offset + " >= tokenLength=" + token.getImage().length());
        }
        ArrayList posList = this.getPosList(token);
        int cnt = posList.size();
        for (int i = 0; i < cnt; ++i) {
            etp = (ExtTokenPosition)posList.get(i);
            if (etp.getOffset() != offset || etp.getBias() != bias) continue;
            return etp;
        }
        etp = new ExtTokenPosition(token, offset, bias);
        posList.add(etp);
        return etp;
    }

    synchronized void splitStartTokenPositions(TokenItem token, int startLength) {
        TokenItem prevToken = token.getPrevious();
        if (prevToken != null) {
            prevToken = this.formatWriter.findNonEmptyToken(prevToken, true);
        }
        ArrayList posList = this.getPosList(token);
        int len = posList.size();
        ArrayList prevPosList = this.getPosList(prevToken);
        for (int i = 0; i < len; ++i) {
            ExtTokenPosition etp = (ExtTokenPosition)posList.get(i);
            if (etp.offset >= startLength) continue;
            etp.token = prevToken;
            posList.remove(i);
            prevPosList.add(etp);
            --i;
            --len;
        }
    }

    synchronized void splitEndTokenPositions(TokenItem token, int endLength) {
        TokenItem nextToken = token.getNext();
        if (nextToken != null) {
            nextToken = this.formatWriter.findNonEmptyToken(nextToken, false);
        }
        ArrayList nextPosList = this.getPosList(nextToken);
        ArrayList posList = this.getPosList(token);
        int len = posList.size();
        int offset = token.getImage().length() - endLength;
        for (int i = 0; i < len; ++i) {
            ExtTokenPosition etp = (ExtTokenPosition)posList.get(i);
            if (etp.offset < offset) continue;
            etp.token = nextToken;
            etp.offset -= offset;
            posList.remove(i);
            nextPosList.add(etp);
            --i;
            --len;
        }
    }

    synchronized void tokenTextInsert(TokenItem token, int offset, int length) {
        ArrayList posList = this.getPosList(token);
        int len = posList.size();
        for (int i = 0; i < len; ++i) {
            ExtTokenPosition etp = (ExtTokenPosition)posList.get(i);
            if (!(etp.bias == Position.Bias.Backward ? etp.offset > offset : etp.offset >= offset)) continue;
            etp.offset += length;
        }
        if (token.getImage().length() == offset) {
            TokenItem nextToken = token.getNext();
            if (nextToken != null) {
                nextToken = this.formatWriter.findNonEmptyToken(nextToken, false);
            }
            posList = this.getPosList(nextToken);
            len = posList.size();
            for (int i2 = 0; i2 < len; ++i2) {
                ExtTokenPosition etp = (ExtTokenPosition)posList.get(i2);
                if (etp.bias != Position.Bias.Backward || etp.offset != 0) continue;
                etp.token = token;
                etp.offset = offset;
            }
        }
    }

    synchronized void tokenTextRemove(TokenItem token, int offset, int length) {
        ArrayList posList = this.getPosList(token);
        int len = posList.size();
        int newLen = token.getImage().length() - length;
        ArrayList nextList = this.getPosList(token.getNext());
        for (int i = 0; i < len; ++i) {
            ExtTokenPosition etp = (ExtTokenPosition)posList.get(i);
            if (etp.offset >= offset + length) {
                etp.offset -= length;
            } else if (etp.offset >= offset) {
                etp.offset = offset;
            }
            if (etp.offset < newLen) continue;
            etp.token = token.getNext();
            etp.offset = 0;
            posList.remove(i);
            nextList.add(etp);
            --i;
            --len;
        }
    }

    synchronized void tokenRemove(TokenItem token) {
        TokenItem nextToken = token.getNext();
        if (nextToken != null) {
            nextToken = this.formatWriter.findNonEmptyToken(nextToken, false);
        }
        ArrayList nextPosList = this.getPosList(nextToken);
        ArrayList posList = this.getPosList(token);
        int len = posList.size();
        for (int i = 0; i < len; ++i) {
            ExtTokenPosition etp = (ExtTokenPosition)posList.get(i);
            etp.token = nextToken;
            etp.offset = 0;
            nextPosList.add(etp);
        }
        posList.clear();
        this.tokens2positionLists.remove((Object)token);
    }

    synchronized void tokenInsert(TokenItem token) {
        if (token.getImage().length() > 0) {
            ArrayList posList = this.getPosList(token);
            TokenItem nextToken = token.getNext();
            if (nextToken != null) {
                nextToken = this.formatWriter.findNonEmptyToken(nextToken, false);
            }
            ArrayList nextPosList = this.getPosList(nextToken);
            int nextLen = nextPosList.size();
            for (int i = 0; i < nextLen; ++i) {
                ExtTokenPosition etp = (ExtTokenPosition)nextPosList.get(i);
                if (etp.offset != 0 || etp.getBias() != Position.Bias.Backward) continue;
                etp.token = token;
                nextPosList.remove(i);
                --i;
                --nextLen;
                posList.add(etp);
            }
        }
    }

    synchronized void clearSaveSets() {
        this.firstSet = null;
        this.lastSet = null;
    }

    synchronized void addSaveSet(int baseOffset, int writtenLen, int[] offsets, Position.Bias[] biases) {
        for (int i = 0; i < offsets.length; ++i) {
            if (offsets[i] >= 0 && offsets[i] <= writtenLen) continue;
            throw new IllegalArgumentException("Invalid save-offset=" + offsets[i] + " at index=" + i + ". Written length is " + writtenLen);
        }
        SaveSet newSet = new SaveSet(baseOffset, offsets, biases);
        if (this.firstSet != null) {
            this.lastSet.next = newSet;
            this.lastSet = newSet;
        } else {
            this.firstSet = this.lastSet = newSet;
        }
    }

    synchronized void createPositions(FormatTokenPosition formatStartPosition) {
        boolean noText;
        this.updateSaveOffsets(formatStartPosition);
        SaveSet curSet = this.firstSet;
        FormatWriter.FormatTokenItem token = (FormatWriter.FormatTokenItem)formatStartPosition.getToken();
        boolean bl = noText = token == null;
        while (curSet != null) {
            int len = curSet.offsets.length;
            for (int i = 0; i < len; ++i) {
                if (noText) {
                    curSet.positions[i] = this.getTokenPosition(null, 0, curSet.biases[i]);
                    continue;
                }
                int offset = curSet.offsets[i];
                while (token != null) {
                    if (offset < token.getSaveOffset()) {
                        token = (FormatWriter.FormatTokenItem)token.getPrevious();
                        continue;
                    }
                    if (offset > token.getSaveOffset() + token.getImage().length() || token.getImage().length() == 0) {
                        token = (FormatWriter.FormatTokenItem)token.getNext();
                        continue;
                    }
                    curSet.positions[i] = this.getTokenPosition(token, offset - token.getSaveOffset(), curSet.biases[i]);
                    break;
                }
                if (token != null) continue;
                curSet.positions[i] = this.getTokenPosition(null, 0, curSet.biases[i]);
                token = (FormatWriter.FormatTokenItem)this.formatWriter.getLastToken();
            }
            curSet = curSet.next;
        }
    }

    synchronized void updateSaveSets(FormatTokenPosition formatStartPosition) {
        this.updateSaveOffsets(formatStartPosition);
        SaveSet curSet = this.firstSet;
        int endOffset = 0;
        if (formatStartPosition.getToken() != null) {
            endOffset = ((FormatWriter.FormatTokenItem)this.formatWriter.getLastToken()).getSaveOffset() + this.formatWriter.getLastToken().getImage().length();
        }
        while (curSet != null) {
            int len = curSet.offsets.length;
            for (int i = 0; i < len; ++i) {
                FormatWriter.FormatTokenItem token = (FormatWriter.FormatTokenItem)curSet.positions[i].getToken();
                curSet.offsets[i] = token == null ? endOffset : token.getSaveOffset() + curSet.positions[i].getOffset();
            }
        }
    }

    private void updateSaveOffsets(FormatTokenPosition formatStartPosition) {
        if (this.firstSet != null) {
            int offset = - formatStartPosition.getOffset();
            for (FormatWriter.FormatTokenItem ti = (FormatWriter.FormatTokenItem)formatStartPosition.getToken(); ti != null; ti = (FormatWriter.FormatTokenItem)ti.getNext()) {
                ti.setSaveOffset(offset);
                offset += ti.getImage().length();
            }
        }
    }

    static class SaveSet {
        SaveSet next;
        int baseOffset;
        int[] offsets;
        Position.Bias[] biases;
        FormatTokenPosition[] positions;

        SaveSet(int baseOffset, int[] offsets, Position.Bias[] biases) {
            this.baseOffset = baseOffset;
            this.offsets = offsets;
            this.biases = biases;
        }
    }

    class ExtTokenPosition
    implements FormatTokenPosition {
        TokenItem token;
        int offset;
        Position.Bias bias;

        ExtTokenPosition(TokenItem token, int offset) {
            this(token, offset, Position.Bias.Forward);
        }

        ExtTokenPosition(TokenItem token, int offset, Position.Bias bias) {
            this.token = token;
            this.offset = offset;
            this.bias = bias;
        }

        @Override
        public TokenItem getToken() {
            return this.token;
        }

        @Override
        public int getOffset() {
            return this.token != null ? this.offset : 0;
        }

        @Override
        public Position.Bias getBias() {
            return this.bias;
        }

        public boolean equals(Object o) {
            return this.equals(o, true);
        }

        public boolean equals(Object o, boolean ignoreBias) {
            if (o instanceof FormatTokenPosition) {
                FormatTokenPosition tp = (FormatTokenPosition)o;
                return this.token == tp.getToken() && this.offset == tp.getOffset() && (ignoreBias || this.bias == tp.getBias());
            }
            return false;
        }

        public String toString() {
            return "<" + (Object)this.getToken() + ", " + this.getOffset() + ", " + this.getBias() + ">";
        }
    }

}

