/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import org.netbeans.editor.ext.FormatLayer;
import org.netbeans.editor.ext.FormatSupport;
import org.netbeans.editor.ext.FormatWriter;

public abstract class AbstractFormatLayer
implements FormatLayer {
    private String name;

    public AbstractFormatLayer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    protected FormatSupport createFormatSupport(FormatWriter fw) {
        return null;
    }
}

