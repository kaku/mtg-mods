/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.Acceptor
 *  org.netbeans.editor.AcceptorFactory
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.GuardedException
 *  org.netbeans.editor.Syntax
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.lib.SettingsConversions
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor.ext;

import java.awt.Toolkit;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.Acceptor;
import org.netbeans.editor.AcceptorFactory;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.Formatter;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.FormatLayer;
import org.netbeans.editor.ext.FormatWriter;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public class ExtFormatter
extends Formatter
implements FormatLayer {
    private List formatLayerList = new ArrayList();
    private static final Object NULL_VALUE = new Object();
    private final HashMap settingsMap = new HashMap();
    private Acceptor indentHotCharsAcceptor;
    private boolean reindentWithTextBefore;
    private final String mimeType;
    private final Preferences prefs;
    private final PreferenceChangeListener prefsListener;

    public ExtFormatter(Class kitClass) {
        super(kitClass);
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String key;
                String string = key = evt == null ? null : evt.getKey();
                if (key == null || "indent-hot-chars-acceptor".equals(key)) {
                    ExtFormatter.this.indentHotCharsAcceptor = (Acceptor)SettingsConversions.callFactory((Preferences)ExtFormatter.this.prefs, (MimePath)MimePath.parse((String)ExtFormatter.this.mimeType), (String)"indent-hot-chars-acceptor", (Object)AcceptorFactory.FALSE);
                }
                if (key == null || "reindent-with-text-before".equals(key)) {
                    ExtFormatter.this.reindentWithTextBefore = ExtFormatter.this.prefs.getBoolean("reindent-with-text-before", false);
                }
            }
        };
        this.initFormatLayers();
        this.mimeType = BaseKit.getKit((Class)kitClass).getContentType();
        this.prefs = (Preferences)MimeLookup.getLookup((String)this.mimeType).lookup(Preferences.class);
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs));
    }

    protected void initFormatLayers() {
    }

    @Override
    public String getName() {
        return this.getKitClass().getName().substring(this.getKitClass().getName().lastIndexOf(46) + 1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Object getSettingValue(String settingName) {
        HashMap hashMap = this.settingsMap;
        synchronized (hashMap) {
            Object value = this.settingsMap.get(settingName);
            if (value != null) {
                return value != NULL_VALUE ? value : null;
            }
        }
        try {
            ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            Class settingsClass = loader.loadClass("org.netbeans.editor.Settings");
            Method m = settingsClass.getMethod("getValue", Class.class, String.class);
            return m.invoke(null, this.getKitClass(), settingName);
        }
        catch (Exception e) {
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setSettingValue(String settingName, Object settingValue) {
        HashMap hashMap = this.settingsMap;
        synchronized (hashMap) {
            this.settingsMap.put(settingName, settingValue == null ? NULL_VALUE : settingValue);
        }
    }

    public synchronized void addFormatLayer(FormatLayer layer) {
        this.formatLayerList.add(layer);
    }

    public synchronized boolean replaceFormatLayer(String layerName, FormatLayer layer) {
        int cnt = this.formatLayerList.size();
        for (int i = 0; i < cnt; ++i) {
            if (!layerName.equals(((FormatLayer)this.formatLayerList.get(i)).getName())) continue;
            this.formatLayerList.set(i, layer);
            return true;
        }
        return false;
    }

    public synchronized void removeFormatLayer(String layerName) {
        Iterator it = this.formatLayerIterator();
        while (it.hasNext()) {
            if (!layerName.equals(((FormatLayer)it.next()).getName())) continue;
            it.remove();
            return;
        }
    }

    public Iterator formatLayerIterator() {
        return this.formatLayerList.iterator();
    }

    public boolean isSimple() {
        return false;
    }

    @Override
    public synchronized void format(FormatWriter fw) {
        boolean done = false;
        int safetyCounter = 0;
        do {
            fw.setChainModified(false);
            fw.setRestartFormat(false);
            Iterator it = this.formatLayerIterator();
            while (it.hasNext()) {
                ((FormatLayer)it.next()).format(fw);
                if (!fw.isRestartFormat()) continue;
            }
            if (!it.hasNext() && !fw.isRestartFormat()) {
                done = true;
            }
            if (safetyCounter <= 1000) continue;
            new Exception("Indentation infinite loop detected").printStackTrace();
            break;
        } while (!done);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Writer reformat(BaseDocument doc, int startOffset, int endOffset, boolean indentOnly) throws BadLocationException, IOException {
        ExtFormatter.pushFormattingContextDocument((Document)doc);
        try {
            Object out;
            CharArrayWriter cw = new CharArrayWriter();
            Writer w = this.createWriter((Document)doc, startOffset, cw);
            FormatWriter fw = w instanceof FormatWriter ? (FormatWriter)w : null;
            boolean fix5620 = true;
            if (fw != null) {
                fw.setIndentOnly(indentOnly);
                if (fix5620) {
                    fw.setReformatting(true);
                }
            }
            w.write(doc.getChars(startOffset, endOffset - startOffset));
            w.close();
            if (!fix5620 || fw == null) {
                out = new String(cw.toCharArray());
                doc.remove(startOffset, endOffset - startOffset);
                doc.insertString(startOffset, (String)out, null);
            }
            out = w;
            return out;
        }
        finally {
            ExtFormatter.popFormattingContextDocument((Document)doc);
        }
    }

    @Override
    public int reformat(BaseDocument doc, int startOffset, int endOffset) throws BadLocationException {
        try {
            Position pos = doc.createPosition(endOffset);
            this.reformat(doc, startOffset, endOffset, false);
            return pos.getOffset() - startOffset;
        }
        catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int[] getReformatBlock(JTextComponent target, String typedText) {
        if (this.indentHotCharsAcceptor == null) {
            this.prefsListener.preferenceChange(null);
        }
        if (this.indentHotCharsAcceptor.accept(typedText.charAt(0))) {
            if (!this.reindentWithTextBefore && this.hasTextBefore(target, typedText)) {
                return null;
            }
            int dotPos = target.getCaret().getDot();
            return new int[]{Math.max(dotPos - 1, 0), dotPos};
        }
        return null;
    }

    protected boolean hasTextBefore(JTextComponent target, String typedText) {
        BaseDocument doc = Utilities.getDocument((JTextComponent)target);
        int dotPos = target.getCaret().getDot();
        try {
            int fnw = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)dotPos);
            return dotPos != fnw + typedText.length();
        }
        catch (BadLocationException e) {
            return false;
        }
    }

    @Override
    public Writer createWriter(Document doc, int offset, Writer writer) {
        return new FormatWriter(this, doc, offset, writer, false);
    }

    @Override
    public int indentLine(Document doc, int offset) {
        if (doc instanceof BaseDocument) {
            try {
                BaseDocument bdoc = (BaseDocument)doc;
                int lineStart = Utilities.getRowStart((BaseDocument)bdoc, (int)offset);
                int nextLineStart = Utilities.getRowStart((BaseDocument)bdoc, (int)offset, (int)1);
                if (nextLineStart < 0) {
                    nextLineStart = bdoc.getLength();
                }
                this.reformat(bdoc, lineStart, nextLineStart, false);
                return Utilities.getRowEnd((BaseDocument)bdoc, (int)lineStart);
            }
            catch (GuardedException e) {
                Toolkit.getDefaultToolkit().beep();
            }
            catch (BadLocationException e) {
                Utilities.annotateLoggable((Throwable)e);
            }
            catch (IOException e) {
                Utilities.annotateLoggable((Throwable)e);
            }
            return offset;
        }
        return super.indentLine(doc, offset);
    }

    protected int getEOLOffset(BaseDocument bdoc, int offset) throws BadLocationException {
        return Utilities.getRowEnd((BaseDocument)bdoc, (int)offset);
    }

    @Override
    public int indentNewLine(Document doc, int offset) {
        final int[] result = new int[]{offset};
        if (doc instanceof BaseDocument) {
            final BaseDocument bdoc = (BaseDocument)doc;
            bdoc.runAtomicAsUser(new Runnable(){

                @Override
                public void run() {
                    boolean newLineInserted = false;
                    try {
                        bdoc.insertString(result[0], "\n", null);
                        int[] arrn = result;
                        arrn[0] = arrn[0] + 1;
                        newLineInserted = true;
                        int eolOffset = Utilities.getRowEnd((BaseDocument)bdoc, (int)result[0]);
                        Writer w = ExtFormatter.this.reformat(bdoc, result[0], eolOffset, true);
                        eolOffset = Utilities.getRowFirstNonWhite((BaseDocument)bdoc, (int)result[0]);
                        if (eolOffset < 0) {
                            eolOffset = ExtFormatter.this.getEOLOffset(bdoc, result[0]);
                        }
                        result[0] = eolOffset;
                        if (w instanceof FormatWriter) {
                            int[] arrn2 = result;
                            arrn2[0] = arrn2[0] + ((FormatWriter)w).getIndentShift();
                        }
                    }
                    catch (GuardedException e) {
                        if (!newLineInserted) {
                            Toolkit.getDefaultToolkit().beep();
                        }
                    }
                    catch (BadLocationException e) {
                        Utilities.annotateLoggable((Throwable)e);
                    }
                    catch (IOException e) {
                        Utilities.annotateLoggable((Throwable)e);
                    }
                }
            });
        } else {
            try {
                doc.insertString(result[0], "\n", null);
                int[] arrn = result;
                arrn[0] = arrn[0] + 1;
            }
            catch (BadLocationException ex) {
                // empty catch block
            }
        }
        return result[0];
    }

    protected boolean acceptSyntax(Syntax syntax) {
        return true;
    }

    static void pushFormattingContextDocument(Document doc) {
        try {
            Method m = Formatter.class.getDeclaredMethod("pushFormattingContextDocument", Document.class);
            m.setAccessible(true);
            m.invoke(null, doc);
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    static void popFormattingContextDocument(Document doc) {
        try {
            Method m = Formatter.class.getDeclaredMethod("popFormattingContextDocument", Document.class);
            m.setAccessible(true);
            m.invoke(null, doc);
        }
        catch (Exception e) {
            // empty catch block
        }
    }

    public static class Simple
    extends ExtFormatter {
        public Simple(Class kitClass) {
            super(kitClass);
        }

        @Override
        public boolean isSimple() {
            return true;
        }

        @Override
        protected int getEOLOffset(BaseDocument bdoc, int offset) throws BadLocationException {
            return offset;
        }
    }

}

