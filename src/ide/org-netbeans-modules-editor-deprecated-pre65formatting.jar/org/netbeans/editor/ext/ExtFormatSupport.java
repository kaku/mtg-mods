/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.ImageTokenID
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 */
package org.netbeans.editor.ext;

import org.netbeans.editor.ImageTokenID;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.ext.FormatSupport;
import org.netbeans.editor.ext.FormatTokenPosition;
import org.netbeans.editor.ext.FormatWriter;

public class ExtFormatSupport
extends FormatSupport {
    public ExtFormatSupport(FormatWriter formatWriter) {
        super(formatWriter);
    }

    public int findLineDistance(FormatTokenPosition fromPosition, FormatTokenPosition toPosition) {
        TokenItem token;
        int lineCounter = 0;
        int offset = fromPosition.getOffset();
        TokenItem targetToken = toPosition.getToken();
        int targetOffset = toPosition.getOffset();
        if (token == null && targetToken == null) {
            return 0;
        }
        for (token = fromPosition.getToken(); token != null; token = token.getNext()) {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (token == targetToken && offset == targetOffset) {
                    return lineCounter;
                }
                if (text.charAt(offset) == '\n') {
                    ++lineCounter;
                }
                ++offset;
            }
            offset = 0;
        }
        throw new IllegalStateException("Tokens don't follow in chain.");
    }

    public boolean isComment(TokenItem token, int offset) {
        return false;
    }

    public boolean isComment(FormatTokenPosition pos) {
        return this.isComment(pos.getToken(), pos.getOffset());
    }

    public boolean isImportant(TokenItem token, int offset) {
        return !this.isComment(token, offset) && !this.isWhitespace(token, offset);
    }

    public boolean isImportant(FormatTokenPosition pos) {
        return this.isImportant(pos.getToken(), pos.getOffset());
    }

    public FormatTokenPosition findImportant(FormatTokenPosition startPosition, FormatTokenPosition limitPosition, boolean stopOnEOL, boolean backward) {
        int limitOffset;
        TokenItem limitToken;
        if (startPosition.equals(limitPosition)) {
            return null;
        }
        if (backward) {
            int limitOffset2;
            TokenItem limitToken2;
            if (limitPosition == null) {
                limitToken2 = null;
                limitOffset2 = 0;
            } else if ((limitPosition = this.getPreviousPosition(limitPosition)) == null) {
                limitToken2 = null;
                limitOffset2 = 0;
            } else {
                limitToken2 = limitPosition.getToken();
                limitOffset2 = limitPosition.getOffset();
            }
            startPosition = this.getPreviousPosition(startPosition);
            if (startPosition == null) {
                return null;
            }
            TokenItem token = startPosition.getToken();
            int offset = startPosition.getOffset();
            do {
                String text = token.getImage();
                while (offset >= 0) {
                    if (stopOnEOL && text.charAt(offset) == '\n') {
                        return null;
                    }
                    if (this.isImportant(token, offset)) {
                        return this.getPosition(token, offset);
                    }
                    if (token == limitToken2 && offset == limitOffset2) {
                        return null;
                    }
                    --offset;
                }
                if ((token = token.getPrevious()) == null) {
                    return null;
                }
                offset = token.getImage().length() - 1;
            } while (true);
        }
        if (limitPosition == null) {
            limitToken = null;
            limitOffset = 0;
        } else {
            limitToken = limitPosition.getToken();
            limitOffset = limitPosition.getOffset();
        }
        TokenItem token = startPosition.getToken();
        int offset = startPosition.getOffset();
        if (token == null) {
            return null;
        }
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (token == limitToken && offset == limitOffset) {
                    return null;
                }
                if (stopOnEOL && text.charAt(offset) == '\n') {
                    return null;
                }
                if (this.isImportant(token, offset)) {
                    return this.getPosition(token, offset);
                }
                ++offset;
            }
            if ((token = token.getNext()) == null) {
                return null;
            }
            offset = 0;
        } while (true);
    }

    public FormatTokenPosition findLineFirstImportant(FormatTokenPosition pos) {
        pos = this.findLineStart(pos);
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        if (token == null) {
            return null;
        }
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (text.charAt(offset) == '\n') {
                    return null;
                }
                if (this.isImportant(token, offset)) {
                    return this.getPosition(token, offset);
                }
                ++offset;
            }
            if (token.getNext() == null) {
                return null;
            }
            token = token.getNext();
            offset = 0;
        } while (true);
    }

    public FormatTokenPosition findLineEndNonImportant(FormatTokenPosition pos) {
        if (this.isChainStartPosition(pos = this.findLineEnd(pos))) {
            return pos;
        }
        pos = this.getPreviousPosition(pos);
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset >= 0) {
                if (offset < textLen && (text.charAt(offset) == '\n' || this.isImportant(token, offset))) {
                    return this.getNextPosition(token, offset);
                }
                --offset;
            }
            if (token.getPrevious() == null) {
                return this.getPosition(token, 0);
            }
            token = token.getPrevious();
            offset = token.getImage().length() - 1;
        } while (true);
    }

    public TokenItem insertImageToken(TokenItem beforeToken, ImageTokenID tokenID, TokenContextPath tokenContextPath) {
        return super.insertToken(beforeToken, (TokenID)tokenID, tokenContextPath, tokenID.getImage());
    }

    public TokenItem findToken(TokenItem startToken, TokenItem limitToken, TokenID tokenID, TokenContextPath tokenContextPath, String tokenImage, boolean backward) {
        if (backward) {
            if (startToken != null && startToken == limitToken) {
                return null;
            }
            startToken = this.getPreviousToken(startToken);
            if (limitToken != null) {
                limitToken = limitToken.getPrevious();
            }
        }
        while (startToken != null && startToken != limitToken) {
            if (this.tokenEquals(startToken, tokenID, tokenContextPath, tokenImage)) {
                return startToken;
            }
            startToken = backward ? startToken.getPrevious() : startToken.getNext();
        }
        return null;
    }

    public TokenItem findImportantToken(TokenItem startToken, TokenItem limitToken, boolean backward) {
        if (backward) {
            if (startToken != null && startToken == limitToken) {
                return null;
            }
            startToken = this.getPreviousToken(startToken);
            if (limitToken != null) {
                limitToken = limitToken.getPrevious();
            }
        }
        while (startToken != null && startToken != limitToken) {
            if (this.isImportant(startToken, 0)) {
                return startToken;
            }
            startToken = backward ? startToken.getPrevious() : startToken.getNext();
        }
        return null;
    }

    public TokenItem findMatchingToken(TokenItem startToken, TokenItem limitToken, TokenID matchTokenID, String matchTokenImage, boolean backward) {
        TokenItem token;
        int depth = 0;
        TokenID startTokenID = startToken.getTokenID();
        TokenContextPath startTokenContextPath = startToken.getTokenContextPath();
        String startText = startToken.getImage();
        TokenItem tokenItem = token = backward ? startToken.getPrevious() : startToken.getNext();
        while (token != null && token != limitToken) {
            if (this.tokenEquals(token, matchTokenID, startTokenContextPath, matchTokenImage)) {
                if (depth-- == 0) {
                    return token;
                }
            } else if (this.tokenEquals(token, startTokenID, startTokenContextPath, startText)) {
                ++depth;
            }
            token = backward ? token.getPrevious() : token.getNext();
        }
        return null;
    }

    public TokenItem findMatchingToken(TokenItem startToken, TokenItem limitToken, ImageTokenID matchTokenID, boolean backward) {
        return this.findMatchingToken(startToken, limitToken, (TokenID)matchTokenID, matchTokenID.getImage(), backward);
    }

    public TokenItem findAnyToken(TokenItem startToken, TokenItem limitToken, TokenID[] tokenIDArray, TokenContextPath tokenContextPath, boolean backward) {
        if (backward) {
            if (startToken != null && startToken == limitToken) {
                return null;
            }
            startToken = this.getPreviousToken(startToken);
            if (limitToken != null) {
                limitToken = limitToken.getPrevious();
            }
        }
        while (startToken != null && startToken != limitToken) {
            for (int i = 0; i < tokenIDArray.length; ++i) {
                if (!this.tokenEquals(startToken, tokenIDArray[i], tokenContextPath)) continue;
                return startToken;
            }
            startToken = backward ? startToken.getPrevious() : startToken.getNext();
        }
        return null;
    }

    public int getIndex(TokenItem token, TokenID[] tokenIDArray) {
        for (int i = 0; i < tokenIDArray.length; ++i) {
            if (!this.tokenEquals(token, tokenIDArray[i])) continue;
            return i;
        }
        return -1;
    }

    public FormatTokenPosition removeLineEndWhitespace(FormatTokenPosition pos) {
        FormatTokenPosition endWS = this.findLineEndWhitespace(pos);
        if (endWS == null || endWS.getToken() == null) {
            return this.findLineEnd(pos);
        }
        TokenItem token = endWS.getToken();
        int offset = endWS.getOffset();
        do {
            String text = token.getImage();
            int textLen = text.length();
            int removeInd = offset;
            while (offset < textLen) {
                if (text.charAt(offset) == '\n') {
                    this.remove(token, removeInd, offset - removeInd);
                    return this.getPosition(token, removeInd);
                }
                ++offset;
            }
            TokenItem nextToken = token.getNext();
            if (removeInd == 0) {
                this.removeToken(token);
            } else {
                this.remove(token, removeInd, textLen - removeInd);
            }
            token = nextToken;
            if (token == null) {
                return this.getPosition(null, 0);
            }
            offset = 0;
        } while (true);
    }

    public char getChar(FormatTokenPosition pos) {
        return pos.getToken().getImage().charAt(pos.getOffset());
    }

    public boolean isLineStart(FormatTokenPosition pos) {
        return this.isChainStartPosition(pos) || this.getChar(this.getPreviousPosition(pos)) == '\n';
    }

    public boolean isNewLine(FormatTokenPosition pos) {
        return pos.getToken() != null && this.getChar(pos) == '\n';
    }
}

