/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.TokenItem
 */
package org.netbeans.editor.ext;

import javax.swing.text.Position;
import org.netbeans.editor.TokenItem;

public interface FormatTokenPosition {
    public TokenItem getToken();

    public int getOffset();

    public Position.Bias getBias();
}

