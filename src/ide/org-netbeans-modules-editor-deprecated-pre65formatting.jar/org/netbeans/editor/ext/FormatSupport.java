/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.Analyzer
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 */
package org.netbeans.editor.ext;

import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.ext.ExtFormatter;
import org.netbeans.editor.ext.FormatTokenPosition;
import org.netbeans.editor.ext.FormatWriter;

public class FormatSupport {
    private FormatWriter formatWriter;

    public FormatSupport(FormatWriter formatWriter) {
        this.formatWriter = formatWriter;
    }

    public FormatWriter getFormatWriter() {
        return this.formatWriter;
    }

    public int getTabSize() {
        Document doc = this.formatWriter.getDocument();
        return doc instanceof BaseDocument ? ((BaseDocument)doc).getTabSize() : this.formatWriter.getFormatter().getTabSize();
    }

    public int getShiftWidth() {
        Document doc = this.formatWriter.getDocument();
        return doc instanceof BaseDocument ? ((BaseDocument)doc).getShiftWidth() : this.formatWriter.getFormatter().getShiftWidth();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean expandTabs() {
        ExtFormatter.pushFormattingContextDocument(this.formatWriter.getDocument());
        try {
            boolean bl = this.formatWriter.getFormatter().expandTabs();
            return bl;
        }
        finally {
            ExtFormatter.popFormattingContextDocument(this.formatWriter.getDocument());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int getSpacesPerTab() {
        ExtFormatter.pushFormattingContextDocument(this.formatWriter.getDocument());
        try {
            int n = this.formatWriter.getFormatter().getSpacesPerTab();
            return n;
        }
        finally {
            ExtFormatter.popFormattingContextDocument(this.formatWriter.getDocument());
        }
    }

    public Object getSettingValue(String settingName) {
        return this.formatWriter.getFormatter().getSettingValue(settingName);
    }

    public Object getSettingValue(String settingName, Object defaultValue) {
        Object value = this.getSettingValue(settingName);
        return value != null ? value : defaultValue;
    }

    public boolean getSettingBoolean(String settingName, Boolean defaultValue) {
        return (Boolean)this.getSettingValue(settingName, defaultValue);
    }

    public boolean getSettingBoolean(String settingName, boolean defaultValue) {
        return (Boolean)this.getSettingValue(settingName, defaultValue ? Boolean.TRUE : Boolean.FALSE);
    }

    public int getSettingInteger(String settingName, Integer defaultValue) {
        return (Integer)this.getSettingValue(settingName, defaultValue);
    }

    public int getSettingInteger(String settingName, int defaultValue) {
        Object value = this.getSettingValue(settingName);
        return value instanceof Integer ? (Integer)value : defaultValue;
    }

    public final boolean isIndentOnly() {
        return this.formatWriter.isIndentOnly();
    }

    public FormatTokenPosition getFormatStartPosition() {
        return this.formatWriter.getFormatStartPosition();
    }

    public FormatTokenPosition getTextStartPosition() {
        return this.formatWriter.getTextStartPosition();
    }

    public TokenItem findFirstToken(TokenItem token) {
        return this.formatWriter.findFirstToken(token);
    }

    public TokenItem getLastToken() {
        return this.formatWriter.getLastToken();
    }

    public FormatTokenPosition getLastPosition() {
        TokenItem lt = this.findNonEmptyToken(this.getLastToken(), true);
        return lt == null ? null : this.getPosition(lt, lt.getImage().length() - 1);
    }

    public boolean canInsertToken(TokenItem beforeToken) {
        return this.formatWriter.canInsertToken(beforeToken);
    }

    public TokenItem insertToken(TokenItem beforeToken, TokenID tokenID, TokenContextPath tokenContextPath, String tokenImage) {
        return this.formatWriter.insertToken(beforeToken, tokenID, tokenContextPath, tokenImage);
    }

    public void insertSpaces(TokenItem beforeToken, int spaceCount) {
        TokenID whitespaceTokenID = this.getWhitespaceTokenID();
        if (whitespaceTokenID == null) {
            throw new IllegalStateException("Valid whitespace token-id required.");
        }
        this.insertToken(beforeToken, whitespaceTokenID, null, new String(Analyzer.getSpacesBuffer((int)spaceCount), 0, spaceCount));
    }

    public boolean canRemoveToken(TokenItem token) {
        return this.formatWriter.canRemoveToken(token);
    }

    public void removeToken(TokenItem token) {
        this.formatWriter.removeToken(token);
    }

    public void removeTokenChain(TokenItem startToken, TokenItem endToken) {
        while (startToken != null && startToken != endToken) {
            TokenItem t = startToken.getNext();
            this.removeToken(startToken);
            startToken = t;
        }
    }

    public TokenItem splitStart(TokenItem token, int startLength, TokenID newTokenID, TokenContextPath newTokenContextPath) {
        return this.formatWriter.splitStart(token, startLength, newTokenID, newTokenContextPath);
    }

    public TokenItem splitEnd(TokenItem token, int endLength, TokenID newTokenID, TokenContextPath newTokenContextPath) {
        return this.formatWriter.splitEnd(token, endLength, newTokenID, newTokenContextPath);
    }

    public void insertString(TokenItem token, int offset, String text) {
        this.formatWriter.insertString(token, offset, text);
    }

    public void insertString(FormatTokenPosition pos, String text) {
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        if (token == null) {
            token = this.getLastToken();
            if (token == null) {
                throw new IllegalStateException("Cannot insert string. No tokens.");
            }
            offset = token.getImage().length();
        }
        this.insertString(token, offset, text);
    }

    public void remove(TokenItem token, int offset, int length) {
        this.formatWriter.remove(token, offset, length);
    }

    public void remove(FormatTokenPosition pos, int length) {
        this.remove(pos.getToken(), pos.getOffset(), length);
    }

    public TokenItem findNonEmptyToken(TokenItem token, boolean backward) {
        return this.formatWriter.findNonEmptyToken(token, backward);
    }

    public FormatTokenPosition getPosition(TokenItem token, int offset) {
        return this.getPosition(token, offset, Position.Bias.Forward);
    }

    public FormatTokenPosition getPosition(TokenItem token, int offset, Position.Bias bias) {
        return this.formatWriter.getPosition(token, offset, bias);
    }

    public FormatTokenPosition getNextPosition(TokenItem token, int offset, Position.Bias bias) {
        if (token == null) {
            return null;
        }
        if (++offset >= token.getImage().length()) {
            token = token.getNext();
            offset = 0;
        }
        return this.getPosition(token, offset, bias);
    }

    public FormatTokenPosition getPreviousPosition(TokenItem token, int offset, Position.Bias bias) {
        FormatTokenPosition ret = null;
        if (token == null) {
            TokenItem lastToken = this.findNonEmptyToken(this.getLastToken(), true);
            if (lastToken != null) {
                ret = this.getPosition(lastToken, lastToken.getImage().length() - 1, Position.Bias.Forward);
            }
        } else if (--offset < 0) {
            if ((token = token.getPrevious()) != null) {
                ret = this.getPosition(token, token.getImage().length() - 1, Position.Bias.Forward);
            }
        } else {
            ret = this.getPosition(token, offset, Position.Bias.Forward);
        }
        return ret;
    }

    public FormatTokenPosition getPreviousPosition(FormatTokenPosition pos) {
        return this.getPreviousPosition(pos.getToken(), pos.getOffset(), pos.getBias());
    }

    public FormatTokenPosition getPreviousPosition(FormatTokenPosition pos, Position.Bias bias) {
        return this.getPreviousPosition(pos.getToken(), pos.getOffset(), bias);
    }

    public FormatTokenPosition getPreviousPosition(TokenItem token, int offset) {
        return this.getPreviousPosition(token, offset, Position.Bias.Forward);
    }

    public FormatTokenPosition getNextPosition(FormatTokenPosition pos) {
        return this.getNextPosition(pos.getToken(), pos.getOffset(), pos.getBias());
    }

    public FormatTokenPosition getNextPosition(FormatTokenPosition pos, Position.Bias bias) {
        return this.getNextPosition(pos.getToken(), pos.getOffset(), bias);
    }

    public FormatTokenPosition getNextPosition(TokenItem token, int offset) {
        return this.getNextPosition(token, offset, Position.Bias.Forward);
    }

    public boolean isAfter(TokenItem testedToken, TokenItem afterToken) {
        return this.formatWriter.isAfter(testedToken, afterToken);
    }

    public boolean isAfter(FormatTokenPosition testedPosition, FormatTokenPosition afterPosition) {
        return this.formatWriter.isAfter(testedPosition, afterPosition);
    }

    public boolean isChainStartPosition(FormatTokenPosition pos) {
        return this.formatWriter.isChainStartPosition(pos);
    }

    public boolean canReplaceToken(TokenItem token) {
        return this.canRemoveToken(token);
    }

    public void replaceToken(TokenItem originalToken, TokenID tokenID, TokenContextPath tokenContextPath, String tokenImage) {
        if (!this.canReplaceToken(originalToken)) {
            throw new IllegalStateException("Cannot insert token into chain");
        }
        TokenItem next = originalToken.getNext();
        this.removeToken(originalToken);
        this.insertToken(next, tokenID, tokenContextPath, tokenImage);
    }

    public boolean isRestartFormat() {
        return this.formatWriter.isRestartFormat();
    }

    public void setRestartFormat(boolean restartFormat) {
        this.formatWriter.setRestartFormat(restartFormat);
    }

    public int getIndentShift() {
        return this.formatWriter.getIndentShift();
    }

    public void setIndentShift(int indentShift) {
        this.formatWriter.setIndentShift(indentShift);
    }

    public boolean tokenEquals(TokenItem compareToken, TokenID withTokenID) {
        return this.tokenEquals(compareToken, withTokenID, null, null);
    }

    public boolean tokenEquals(TokenItem compareToken, TokenID withTokenID, TokenContextPath withTokenContextPath) {
        return this.tokenEquals(compareToken, withTokenID, withTokenContextPath, null);
    }

    public boolean tokenEquals(TokenItem compareToken, TokenID withTokenID, TokenContextPath withTokenContextPath, String withTokenImage) {
        return !(withTokenID != null && compareToken.getTokenID() != withTokenID || withTokenContextPath != null && compareToken.getTokenContextPath() != withTokenContextPath || withTokenImage != null && !compareToken.getImage().equals(withTokenImage));
    }

    public boolean isWhitespace(TokenItem token, int offset) {
        return Character.isWhitespace(token.getImage().charAt(offset));
    }

    public boolean isWhitespace(FormatTokenPosition pos) {
        return this.isWhitespace(pos.getToken(), pos.getOffset());
    }

    public FormatTokenPosition findLineStart(FormatTokenPosition pos) {
        if (this.isChainStartPosition(pos)) {
            return pos;
        }
        pos = this.getPreviousPosition(pos);
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        do {
            String text = token.getImage();
            while (offset >= 0) {
                if (text.charAt(offset) == '\n') {
                    return this.getNextPosition(token, offset);
                }
                --offset;
            }
            if (token.getPrevious() == null) {
                return this.getPosition(token, 0);
            }
            token = token.getPrevious();
            offset = token.getImage().length() - 1;
        } while (true);
    }

    public FormatTokenPosition findLineEnd(FormatTokenPosition pos) {
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        if (token == null) {
            return pos;
        }
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (text.charAt(offset) == '\n') {
                    return this.getPosition(token, offset);
                }
                ++offset;
            }
            if (token.getNext() == null) {
                return this.getPosition(null, 0);
            }
            token = token.getNext();
            offset = 0;
        } while (true);
    }

    public FormatTokenPosition findLineFirstNonWhitespace(FormatTokenPosition pos) {
        pos = this.findLineStart(pos);
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        if (token == null) {
            return null;
        }
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (text.charAt(offset) == '\n') {
                    return null;
                }
                if (!this.isWhitespace(token, offset)) {
                    return this.getPosition(token, offset);
                }
                ++offset;
            }
            if (token.getNext() == null) {
                return null;
            }
            token = token.getNext();
            offset = 0;
        } while (true);
    }

    public FormatTokenPosition findLineEndWhitespace(FormatTokenPosition pos) {
        if (this.isChainStartPosition(pos = this.findLineEnd(pos))) {
            return pos;
        }
        pos = this.getPreviousPosition(pos);
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset >= 0) {
                if (!(offset >= textLen || text.charAt(offset) != '\n' && this.isWhitespace(token, offset))) {
                    return this.getNextPosition(token, offset);
                }
                --offset;
            }
            if (token.getPrevious() == null) {
                return this.getPosition(token, 0);
            }
            token = token.getPrevious();
            offset = token.getImage().length() - 1;
        } while (true);
    }

    public FormatTokenPosition findPreviousEOL(FormatTokenPosition pos) {
        if ((pos = this.getPreviousPosition(pos)) == null) {
            return null;
        }
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        do {
            String text = token.getImage();
            while (offset >= 0) {
                if (text.charAt(offset) == '\n') {
                    return this.getPosition(token, offset);
                }
                --offset;
            }
            if (token.getPrevious() == null) {
                return null;
            }
            token = token.getPrevious();
            offset = token.getImage().length() - 1;
        } while (true);
    }

    public FormatTokenPosition findNextEOL(FormatTokenPosition pos) {
        if ((pos = this.getNextPosition(pos)) == null) {
            return null;
        }
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        if (token == null) {
            return null;
        }
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (text.charAt(offset) == '\n') {
                    return this.getPosition(token, offset);
                }
                ++offset;
            }
            if (token.getNext() == null) {
                return null;
            }
            token = token.getNext();
            offset = 0;
        } while (true);
    }

    public boolean isLineEmpty(FormatTokenPosition pos) {
        return this.findLineStart(pos).equals(this.findLineEnd(pos));
    }

    public boolean isLineWhite(FormatTokenPosition pos) {
        FormatTokenPosition lineStart = this.findLineStart(pos);
        return this.findLineEndWhitespace(pos).equals(lineStart);
    }

    public int getVisualColumnOffset(FormatTokenPosition pos) {
        TokenItem targetToken = pos.getToken();
        int targetOffset = pos.getOffset();
        FormatTokenPosition lineStart = this.findLineStart(pos);
        int offset = lineStart.getOffset();
        int col = 0;
        int tabSize = this.getTabSize();
        for (TokenItem token = lineStart.getToken(); token != null; token = token.getNext()) {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (token == targetToken && offset == targetOffset) {
                    return col;
                }
                switch (text.charAt(offset)) {
                    case '\t': {
                        col = (col + tabSize) / tabSize * tabSize;
                        break;
                    }
                    default: {
                        ++col;
                    }
                }
                ++offset;
            }
            offset = 0;
        }
        return col;
    }

    public FormatTokenPosition findNonWhitespace(FormatTokenPosition startPosition, FormatTokenPosition limitPosition, boolean stopOnEOL, boolean backward) {
        int limitOffset;
        TokenItem limitToken;
        if (startPosition.equals(limitPosition)) {
            return null;
        }
        if (backward) {
            int limitOffset2;
            TokenItem limitToken2;
            if (limitPosition == null) {
                limitToken2 = null;
                limitOffset2 = 0;
            } else if ((limitPosition = this.getPreviousPosition(limitPosition)) == null) {
                limitToken2 = null;
                limitOffset2 = 0;
            } else {
                limitToken2 = limitPosition.getToken();
                limitOffset2 = limitPosition.getOffset();
            }
            startPosition = this.getPreviousPosition(startPosition);
            if (startPosition == null) {
                return null;
            }
            TokenItem token = startPosition.getToken();
            int offset = startPosition.getOffset();
            do {
                String text = token.getImage();
                while (offset >= 0) {
                    if (stopOnEOL && text.charAt(offset) == '\n') {
                        return null;
                    }
                    if (!this.isWhitespace(token, offset)) {
                        return this.getPosition(token, offset);
                    }
                    if (token == limitToken2 && offset == limitOffset2) {
                        return null;
                    }
                    --offset;
                }
                if ((token = token.getPrevious()) == null) {
                    return null;
                }
                offset = token.getImage().length() - 1;
            } while (true);
        }
        if (limitPosition == null) {
            limitToken = null;
            limitOffset = 0;
        } else {
            limitToken = limitPosition.getToken();
            limitOffset = limitPosition.getOffset();
        }
        TokenItem token = startPosition.getToken();
        int offset = startPosition.getOffset();
        do {
            String text = token.getImage();
            int textLen = text.length();
            while (offset < textLen) {
                if (token == limitToken && offset == limitOffset) {
                    return null;
                }
                if (stopOnEOL && text.charAt(offset) == '\n') {
                    return null;
                }
                if (!this.isWhitespace(token, offset)) {
                    return this.getPosition(token, offset);
                }
                ++offset;
            }
            if ((token = token.getNext()) == null) {
                return null;
            }
            offset = 0;
        } while (true);
    }

    public TokenItem getPreviousToken(TokenItem token) {
        return token == null ? this.getLastToken() : token.getPrevious();
    }

    public TokenID getWhitespaceTokenID() {
        return null;
    }

    public TokenID getValidWhitespaceTokenID() {
        TokenID wsID = this.getWhitespaceTokenID();
        if (wsID == null) {
            throw new IllegalStateException("Null whitespace token-id");
        }
        return wsID;
    }

    public TokenContextPath getWhitespaceTokenContextPath() {
        return null;
    }

    public TokenContextPath getValidWhitespaceTokenContextPath() {
        TokenContextPath wsTCP = this.getWhitespaceTokenContextPath();
        if (wsTCP == null) {
            throw new IllegalStateException("Null whitespace token-context-path");
        }
        return wsTCP;
    }

    public boolean canModifyWhitespace(TokenItem inToken) {
        return false;
    }

    public String getIndentString(int indent) {
        return this.formatWriter.getFormatter().getIndentString(indent);
    }

    public int getLineIndent(FormatTokenPosition pos, boolean zeroForWSLine) {
        FormatTokenPosition firstNWS = this.findLineFirstNonWhitespace(pos);
        if (firstNWS == null) {
            if (zeroForWSLine) {
                return 0;
            }
            firstNWS = this.findLineEnd(pos);
        }
        return this.getVisualColumnOffset(firstNWS);
    }

    public FormatTokenPosition changeLineIndent(FormatTokenPosition pos, int indent) {
        int textLen;
        String text;
        pos = this.findLineStart(pos);
        String indentString = this.getIndentString(indent);
        int indentStringLen = indentString.length();
        int indentStringInd = 0;
        TokenItem token = pos.getToken();
        int offset = pos.getOffset();
        if (token == null) {
            if (indentString.length() > 0) {
                token = this.insertToken(null, this.getValidWhitespaceTokenID(), this.getValidWhitespaceTokenContextPath(), indentString);
            }
            return pos;
        }
        do {
            text = token.getImage();
            textLen = text.length();
            while (indentStringInd < indentStringLen && offset < textLen) {
                if (indentString.charAt(indentStringInd) != text.charAt(offset)) {
                    if (this.canModifyWhitespace(token)) {
                        this.insertString(token, offset, indentString.substring(indentStringInd));
                        offset += indentStringLen - indentStringInd;
                        indentStringInd = indentStringLen;
                        continue;
                    }
                    if (this.isWhitespace(token, offset) || offset > 0) {
                        throw new IllegalStateException("Cannot modify token=" + (Object)token);
                    }
                    this.insertToken(token, this.getValidWhitespaceTokenID(), this.getValidWhitespaceTokenContextPath(), indentString.substring(indentStringInd));
                    return this.getPosition(token, 0);
                }
                ++indentStringInd;
                ++offset;
            }
            if (indentStringInd >= indentStringLen) break;
            if ((token = token.getNext()) == null) {
                token = this.insertToken(null, this.getValidWhitespaceTokenID(), this.getValidWhitespaceTokenContextPath(), indentString.substring(indentStringInd));
                return this.getPosition(token, 0);
            }
            offset = 0;
        } while (true);
        do {
            text = token.getImage();
            textLen = text.length();
            int removeInd = -1;
            while (offset < textLen) {
                if (!this.isWhitespace(token, offset) || text.charAt(offset) == '\n') {
                    if (removeInd >= 0) {
                        this.remove(token, removeInd, offset - removeInd);
                        offset = removeInd;
                    }
                    return this.getPosition(token, offset);
                }
                if (removeInd < 0) {
                    removeInd = offset;
                }
                ++offset;
            }
            if (removeInd == -1) {
                token = token.getNext();
            } else if (removeInd == 0) {
                TokenItem nextToken = token.getNext();
                this.removeToken(token);
                token = nextToken;
            } else {
                this.remove(token, removeInd, textLen - removeInd);
                token = token.getNext();
            }
            offset = 0;
        } while (token != null);
        return this.getPosition(null, 0);
    }

    public String chainToString(TokenItem token) {
        return this.formatWriter.chainToString(token);
    }

    public String chainToString(TokenItem token, int maxDocumentTokens) {
        return this.formatWriter.chainToString(token, maxDocumentTokens);
    }
}

