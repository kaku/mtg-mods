/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import org.netbeans.editor.ext.FormatWriter;

public interface FormatLayer {
    public String getName();

    public void format(FormatWriter var1);
}

