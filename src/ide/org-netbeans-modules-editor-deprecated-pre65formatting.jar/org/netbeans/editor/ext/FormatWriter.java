/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.EditorDebug
 *  org.netbeans.editor.Syntax
 *  org.netbeans.editor.Syntax$StateInfo
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 *  org.netbeans.editor.TokenItem$AbstractItem
 *  org.netbeans.editor.TokenItem$FilterItem
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 */
package org.netbeans.editor.ext;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorDebug;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtFormatter;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.editor.ext.FormatTokenPosition;
import org.netbeans.editor.ext.FormatTokenPositionSupport;

public final class FormatWriter
extends Writer {
    public static final boolean debug = Boolean.getBoolean("netbeans.debug.editor.format");
    public static final boolean debugModify = Boolean.getBoolean("netbeans.debug.editor.format.modify");
    private static final char[] EMPTY_BUFFER = new char[0];
    private ExtFormatter formatter;
    private Document doc;
    private int offset;
    private Writer underWriter;
    private Syntax syntax;
    private boolean indentOnly;
    private char[] buffer;
    private int bufferSize;
    private FormatTokenPositionSupport ftps;
    private int offsetPreScan;
    private boolean firstFlush;
    private ExtTokenItem lastToken;
    private FormatTokenPosition formatStartPosition;
    private FormatTokenPosition textStartPosition;
    private boolean chainModified;
    private boolean restartFormat;
    private boolean lastFlush;
    private int indentShift;
    private boolean simple;
    private boolean reformatting;

    void setReformatting(boolean reformatting) {
        this.reformatting = reformatting;
    }

    FormatWriter(ExtFormatter formatter, Document doc, int offset, Writer underWriter, boolean indentOnly) {
        block18 : {
            Class kitClass;
            this.formatter = formatter;
            this.doc = doc;
            this.offset = offset;
            this.underWriter = underWriter;
            this.setIndentOnly(indentOnly);
            if (debug) {
                System.err.println("FormatWriter() created, formatter=" + formatter + ", document=" + doc.getClass() + ", expandTabs=" + formatter.expandTabs() + ", spacesPerTab=" + formatter.getSpacesPerTab() + ", tabSize=" + (doc instanceof BaseDocument ? ((BaseDocument)doc).getTabSize() : formatter.getTabSize()) + ", shiftWidth=" + (doc instanceof BaseDocument ? ((BaseDocument)doc).getShiftWidth() : formatter.getShiftWidth()));
            }
            if (formatter.isSimple()) {
                this.simple = true;
                return;
            }
            this.buffer = EMPTY_BUFFER;
            this.firstFlush = true;
            Class class_ = kitClass = doc instanceof BaseDocument ? ((BaseDocument)doc).getKitClass() : formatter.getKitClass();
            if (kitClass == null || !BaseKit.class.isAssignableFrom(kitClass)) {
                this.simple = true;
                return;
            }
            this.syntax = BaseKit.getKit((Class)kitClass).createFormatSyntax(doc);
            if (!formatter.acceptSyntax(this.syntax)) {
                this.simple = true;
                return;
            }
            this.ftps = new FormatTokenPositionSupport(this);
            if (doc instanceof BaseDocument) {
                try {
                    TokenItem ti;
                    BaseDocument bdoc = (BaseDocument)doc;
                    bdoc.getSyntaxSupport().initSyntax(this.syntax, offset, offset, false, true);
                    this.offsetPreScan = this.syntax.getPreScan();
                    if (debug) {
                        System.err.println("FormatWriter: preScan=" + this.offsetPreScan + " at offset=" + offset);
                    }
                    if (offset <= 0) break block18;
                    ExtSyntaxSupport sup = (ExtSyntaxSupport)bdoc.getSyntaxSupport();
                    Integer lines = (Integer)bdoc.getProperty((Object)"line-batch-size");
                    int startOffset = Utilities.getRowStart((BaseDocument)bdoc, (int)Math.max(offset - this.offsetPreScan, 0), (int)(- Math.max(lines, 1)));
                    if (startOffset < 0) {
                        startOffset = 0;
                    }
                    if ((ti = sup.getTokenChain(startOffset, offset)) != null && ti.getOffset() < offset - this.offsetPreScan) {
                        this.lastToken = new FilterDocumentItem(ti, null, false);
                        if (debug) {
                            System.err.println("FormatWriter: first doc token=" + this.lastToken);
                        }
                        while (this.lastToken.getNext() != null && this.lastToken.getNext().getOffset() < offset - this.offsetPreScan) {
                            this.lastToken = (ExtTokenItem)this.lastToken.getNext();
                            if (!debug) continue;
                            System.err.println("FormatWriter: doc token=" + this.lastToken);
                        }
                        ((FilterDocumentItem)this.lastToken).terminate();
                    }
                }
                catch (BadLocationException e) {
                    Utilities.annotateLoggable((Throwable)e);
                }
            } else {
                try {
                    String text = doc.getText(0, offset);
                    char[] charBuffer = text.toCharArray();
                    this.syntax.load(null, charBuffer, 0, charBuffer.length, false, 0);
                    TokenID tokenID = this.syntax.nextToken();
                    while (tokenID != null) {
                        int tokenOffset = this.syntax.getTokenOffset();
                        this.lastToken = new FormatTokenItem(tokenID, this.syntax.getTokenContextPath(), tokenOffset, text.substring(tokenOffset, tokenOffset + this.syntax.getTokenLength()), this.lastToken);
                        if (debug) {
                            System.err.println("FormatWriter: non-bd token=" + this.lastToken);
                        }
                        ((FormatTokenItem)this.lastToken).markWritten();
                        tokenID = this.syntax.nextToken();
                    }
                    this.offsetPreScan = this.syntax.getPreScan();
                }
                catch (BadLocationException e) {
                    Utilities.annotateLoggable((Throwable)e);
                }
            }
        }
        char[] buf = this.syntax.getBuffer();
        int bufOffset = this.syntax.getOffset();
        if (debug) {
            System.err.println("FormatWriter: writing preScan chars='" + EditorDebug.debugChars((char[])buf, (int)(bufOffset - this.offsetPreScan), (int)this.offsetPreScan) + "'" + ", length=" + this.offsetPreScan);
        }
        this.addToBuffer(buf, bufOffset - this.offsetPreScan, this.offsetPreScan);
    }

    public final ExtFormatter getFormatter() {
        return this.formatter;
    }

    public final Document getDocument() {
        return this.doc;
    }

    public final int getOffset() {
        return this.offset;
    }

    public final boolean isIndentOnly() {
        return this.indentOnly;
    }

    public void setIndentOnly(boolean indentOnly) {
        this.indentOnly = indentOnly;
    }

    public FormatTokenPosition getFormatStartPosition() {
        return this.formatStartPosition;
    }

    public FormatTokenPosition getTextStartPosition() {
        return this.textStartPosition;
    }

    public TokenItem getLastToken() {
        return this.lastToken;
    }

    public TokenItem findFirstToken(TokenItem token) {
        if (token == null) {
            TokenItem tokenItem = token = this.textStartPosition != null ? this.textStartPosition.getToken() : null;
            if (token == null && (token = this.formatStartPosition.getToken()) == null && (token = this.lastToken) == null) {
                return null;
            }
        }
        while (token.getPrevious() != null) {
            token = token.getPrevious();
        }
        return token;
    }

    public boolean isAfter(TokenItem testedToken, TokenItem afterToken) {
        while (afterToken != null) {
            if ((afterToken = afterToken.getNext()) != testedToken) continue;
            return true;
        }
        return false;
    }

    public boolean isAfter(FormatTokenPosition testedPosition, FormatTokenPosition afterPosition) {
        if (testedPosition.getToken() == afterPosition.getToken()) {
            return testedPosition.getOffset() > afterPosition.getOffset();
        }
        return this.isAfter(testedPosition.getToken(), afterPosition.getToken());
    }

    public TokenItem findNonEmptyToken(TokenItem token, boolean backward) {
        while (token != null && token.getImage().length() == 0) {
            token = backward ? token.getPrevious() : token.getNext();
        }
        return token;
    }

    public boolean canInsertToken(TokenItem beforeToken) {
        return beforeToken == null || !((ExtTokenItem)beforeToken).isWritten();
    }

    public TokenItem insertToken(TokenItem beforeToken, TokenID tokenID, TokenContextPath tokenContextPath, String tokenImage) {
        FormatTokenItem fti;
        if (debugModify) {
            System.err.println("FormatWriter.insertToken(): beforeToken=" + (Object)beforeToken + ", tokenID=" + (Object)tokenID + ", contextPath=" + (Object)tokenContextPath + ", tokenImage='" + tokenImage + "'");
        }
        if (!this.canInsertToken(beforeToken)) {
            throw new IllegalStateException("Can't insert token into chain");
        }
        if (this.reformatting) {
            try {
                this.doc.insertString(this.getDocOffset(beforeToken), tokenImage, null);
            }
            catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        if (beforeToken != null) {
            fti = ((FormatTokenItem)beforeToken).insertToken(tokenID, tokenContextPath, -1, tokenImage);
        } else {
            fti = new FormatTokenItem(tokenID, tokenContextPath, -1, tokenImage, this.lastToken);
            this.lastToken = fti;
        }
        this.ftps.tokenInsert(fti);
        this.chainModified = true;
        return fti;
    }

    private int getDocOffset(TokenItem token) {
        int len = 0;
        token = token != null ? token.getPrevious() : this.lastToken;
        while (token != null) {
            len += token.getImage().length();
            if (token instanceof FilterDocumentItem) {
                return len + token.getOffset();
            }
            token = token.getPrevious();
        }
        return len;
    }

    public boolean canRemoveToken(TokenItem token) {
        return !((ExtTokenItem)token).isWritten();
    }

    public void removeToken(TokenItem token) {
        if (debugModify) {
            System.err.println("FormatWriter.removeToken(): token=" + (Object)token);
        }
        if (!this.canRemoveToken(token)) {
            return;
        }
        if (this.reformatting) {
            try {
                this.doc.remove(this.getDocOffset(token), token.getImage().length());
            }
            catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        this.ftps.tokenRemove(token);
        if (this.lastToken == token) {
            this.lastToken = (ExtTokenItem)token.getPrevious();
        }
        ((FormatTokenItem)token).remove();
        this.chainModified = true;
    }

    public boolean canSplitStart(TokenItem token, int startLength) {
        return !((ExtTokenItem)token).isWritten();
    }

    public TokenItem splitStart(TokenItem token, int startLength, TokenID newTokenID, TokenContextPath newTokenContextPath) {
        if (!this.canSplitStart(token, startLength)) {
            throw new IllegalStateException("Can't split the token=" + (Object)token);
        }
        String text = token.getImage();
        if (startLength > text.length()) {
            throw new IllegalArgumentException("startLength=" + startLength + " is greater than token length=" + text.length());
        }
        String newText = text.substring(0, startLength);
        ExtTokenItem newToken = (ExtTokenItem)this.insertToken(token, newTokenID, newTokenContextPath, newText);
        this.ftps.splitStartTokenPositions(token, startLength);
        this.remove(token, 0, startLength);
        return newToken;
    }

    public boolean canSplitEnd(TokenItem token, int endLength) {
        int splitOffset = token.getImage().length() - endLength;
        return ((ExtTokenItem)token).getWrittenLength() <= splitOffset;
    }

    public TokenItem splitEnd(TokenItem token, int endLength, TokenID newTokenID, TokenContextPath newTokenContextPath) {
        if (!this.canSplitEnd(token, endLength)) {
            throw new IllegalStateException("Can't split the token=" + (Object)token);
        }
        String text = token.getImage();
        if (endLength > text.length()) {
            throw new IllegalArgumentException("endLength=" + endLength + " is greater than token length=" + text.length());
        }
        String newText = text.substring(0, endLength);
        ExtTokenItem newToken = (ExtTokenItem)this.insertToken(token.getNext(), newTokenID, newTokenContextPath, newText);
        this.ftps.splitEndTokenPositions(token, endLength);
        this.remove(token, text.length() - endLength, endLength);
        return newToken;
    }

    public boolean canModifyToken(TokenItem token, int offset) {
        int wrLen = ((ExtTokenItem)token).getWrittenLength();
        return offset >= 0 && wrLen <= offset;
    }

    public void insertString(TokenItem token, int offset, String text) {
        if (debugModify) {
            System.err.println("FormatWriter.insertString(): token=" + (Object)token + ", offset=" + offset + ", text='" + text + "'");
        }
        if (text.length() == 0) {
            return;
        }
        if (!this.canModifyToken(token, offset)) {
            return;
        }
        if (this.reformatting) {
            try {
                this.doc.insertString(this.getDocOffset(token) + offset, text, null);
            }
            catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        this.ftps.tokenTextInsert(token, offset, text.length());
        String image = token.getImage();
        ((ExtTokenItem)token).setImage(image.substring(0, offset) + text + image.substring(offset));
    }

    public void remove(TokenItem token, int offset, int length) {
        if (debugModify) {
            String removedText = offset >= 0 && length >= 0 && offset + length <= token.getImage().length() ? token.getImage().substring(offset, offset + length) : "<INVALID>";
            System.err.println("FormatWriter.remove(): token=" + (Object)token + ", offset=" + offset + ", length=" + length + "removing text='" + removedText + "'");
        }
        if (length == 0) {
            return;
        }
        if (!this.canModifyToken(token, offset)) {
            return;
        }
        if (this.reformatting) {
            try {
                this.doc.remove(this.getDocOffset(token) + offset, length);
            }
            catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        this.ftps.tokenTextRemove(token, offset, length);
        String text = token.getImage();
        ((ExtTokenItem)token).setImage(text.substring(0, offset) + text.substring(offset + length));
    }

    public FormatTokenPosition getPosition(TokenItem token, int offset, Position.Bias bias) {
        return this.ftps.getTokenPosition(token, offset, bias);
    }

    public boolean isChainStartPosition(FormatTokenPosition pos) {
        TokenItem token = pos.getToken();
        return pos.getOffset() == 0 && (token == null && this.getLastToken() == null || token != null && token.getPrevious() == null);
    }

    private void addToBuffer(char[] buf, int off, int len) {
        if (len > this.buffer.length - this.bufferSize) {
            char[] tmp = new char[len + 2 * this.buffer.length];
            System.arraycopy(this.buffer, 0, tmp, 0, this.bufferSize);
            this.buffer = tmp;
        }
        System.arraycopy(buf, off, this.buffer, this.bufferSize, len);
        this.bufferSize += len;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        if (this.simple) {
            this.underWriter.write(cbuf, off, len);
            return;
        }
        this.write(cbuf, off, len, null, null);
    }

    public synchronized void write(char[] cbuf, int off, int len, int[] saveOffsets, Position.Bias[] saveBiases) throws IOException {
        if (this.simple) {
            this.underWriter.write(cbuf, off, len);
            return;
        }
        if (saveOffsets != null) {
            this.ftps.addSaveSet(this.bufferSize, len, saveOffsets, saveBiases);
        }
        this.lastFlush = false;
        if (debug) {
            System.err.println("FormatWriter.write(): '" + EditorDebug.debugChars((char[])cbuf, (int)off, (int)len) + "', length=" + len + ", bufferSize=" + this.bufferSize);
        }
        this.addToBuffer(cbuf, off, len);
    }

    public boolean isChainModified() {
        return this.chainModified;
    }

    public void setChainModified(boolean chainModified) {
        this.chainModified = chainModified;
    }

    public boolean isRestartFormat() {
        return this.restartFormat;
    }

    public void setRestartFormat(boolean restartFormat) {
        this.restartFormat = restartFormat;
    }

    public int getIndentShift() {
        return this.indentShift;
    }

    public void setIndentShift(int indentShift) {
        this.indentShift = indentShift;
    }

    @Override
    public void flush() throws IOException {
        String text;
        if (debug) {
            System.err.println("FormatWriter.flush() called");
        }
        if (this.simple) {
            this.underWriter.flush();
            return;
        }
        if (this.lastFlush) {
            return;
        }
        this.lastFlush = true;
        int startOffset = 0;
        if (this.firstFlush) {
            startOffset = this.offsetPreScan;
        }
        this.syntax.relocate(this.buffer, startOffset, this.bufferSize - startOffset, true, -1);
        this.formatStartPosition = null;
        TokenID tokenID = this.syntax.nextToken();
        if (this.firstFlush && startOffset > 0) {
            do {
                text = new String(this.buffer, this.syntax.getTokenOffset(), this.syntax.getTokenLength());
                this.lastToken = new FormatTokenItem(tokenID, this.syntax.getTokenContextPath(), -1, text, this.lastToken);
                if (debug) {
                    System.err.println("FormatWriter.flush(): doc&format token=" + this.lastToken);
                }
                this.lastToken.setWrittenLength(startOffset);
                if (text.length() > startOffset) {
                    this.formatStartPosition = this.getPosition(this.lastToken, startOffset, Position.Bias.Backward);
                }
                tokenID = this.syntax.nextToken();
                if (text.length() >= startOffset) break;
                this.lastToken.setWrittenLength(Integer.MAX_VALUE);
                startOffset -= text.length();
            } while (true);
        }
        while (tokenID != null) {
            text = new String(this.buffer, this.syntax.getTokenOffset(), this.syntax.getTokenLength());
            this.lastToken = new FormatTokenItem(tokenID, this.syntax.getTokenContextPath(), -1, text, this.lastToken);
            if (this.formatStartPosition == null) {
                this.formatStartPosition = this.getPosition(this.lastToken, 0, Position.Bias.Backward);
            }
            if (debug) {
                System.err.println("FormatWriter.flush(): format token=" + this.lastToken);
            }
            tokenID = this.syntax.nextToken();
        }
        if (this.formatStartPosition == null) {
            this.formatStartPosition = this.getPosition(null, 0, Position.Bias.Backward);
        }
        if (this.firstFlush) {
            this.textStartPosition = this.formatStartPosition;
        }
        this.bufferSize = 0;
        if (debug) {
            System.err.println("FormatWriter.flush(): formatting ...");
        }
        this.formatter.format(this);
        StringBuffer sb = new StringBuffer();
        ExtTokenItem token = (ExtTokenItem)this.formatStartPosition.getToken();
        ExtTokenItem prevToken = null;
        if (token != null) {
            switch (token.getWrittenLength()) {
                case -1: {
                    sb.append(token.getImage());
                    break;
                }
                case Integer.MAX_VALUE: {
                    throw new IllegalStateException("Wrong formatStartPosition");
                }
                default: {
                    sb.append(token.getImage().substring(this.formatStartPosition.getOffset()));
                }
            }
            token.markWritten();
            prevToken = token;
            for (token = (ExtTokenItem)token.getNext(); token != null; token = (ExtTokenItem)token.getNext()) {
                prevToken.setWrittenLength(Integer.MAX_VALUE);
                sb.append(token.getImage());
                token.markWritten();
                prevToken = token;
            }
        }
        if (sb.length() > 0) {
            char[] outBuf = new char[sb.length()];
            sb.getChars(0, outBuf.length, outBuf, 0);
            if (debug) {
                System.err.println("FormatWriter.flush(): chars to underlying writer='" + EditorDebug.debugChars((char[])outBuf, (int)0, (int)outBuf.length) + "'");
            }
            this.underWriter.write(outBuf, 0, outBuf.length);
        }
        this.underWriter.flush();
        this.firstFlush = false;
    }

    @Override
    public void close() throws IOException {
        if (debug) {
            System.err.println("FormatWriter: close() called (-> flush())");
        }
        this.flush();
        this.underWriter.close();
    }

    public void checkChain() {
        TokenItem lt = this.getLastToken();
        if (lt.getNext() != null) {
            throw new IllegalStateException("Successor of last token exists.");
        }
        FormatTokenPosition fsp = this.getFormatStartPosition();
        if (fsp == null) {
            throw new IllegalStateException("getFormatStartPosition() returns null.");
        }
        this.checkFSPFollowsTSP();
    }

    private void checkFSPFollowsTSP() {
        if (!this.formatStartPosition.equals(this.textStartPosition) && !this.isAfter(this.formatStartPosition, this.textStartPosition)) {
            throw new IllegalStateException("formatStartPosition doesn't follow textStartPosition");
        }
    }

    public String chainToString(TokenItem token) {
        return this.chainToString(token, 5);
    }

    public String chainToString(TokenItem token, int maxDocumentTokens) {
        this.checkChain();
        StringBuffer sb = new StringBuffer();
        sb.append("D - document tokens, W - written tokens, F - tokens being formatted\n");
        this.checkFSPFollowsTSP();
        TokenItem tst = this.getTextStartPosition().getToken();
        TokenItem fst = this.getFormatStartPosition().getToken();
        TokenItem t = tst;
        if (t == null) {
            t = this.getLastToken();
        }
        while (t != null && t.getPrevious() != null && --maxDocumentTokens > 0) {
            t = t.getPrevious();
        }
        while (t != tst) {
            sb.append(t == token ? '>' : ' ');
            sb.append("D  ");
            sb.append(t.toString());
            sb.append('\n');
            t = t.getNext();
        }
        while (t != fst) {
            sb.append(t == token ? '>' : ' ');
            if (t == tst) {
                sb.append("D(" + this.getTextStartPosition().getOffset() + ')');
            }
            sb.append("W ");
            sb.append(t.toString());
            sb.append('\n');
            t = t.getNext();
        }
        sb.append(t == token ? '>' : ' ');
        if (this.getFormatStartPosition().getOffset() > 0) {
            if (fst == tst) {
                sb.append('D');
            } else {
                sb.append('W');
            }
        }
        sb.append("F ");
        sb.append(t != null ? t.toString() : "NULL");
        sb.append('\n');
        if (t != null) {
            t = t.getNext();
        }
        while (t != null) {
            sb.append(t == token ? '>' : ' ');
            sb.append("F ");
            sb.append(t.toString());
            sb.append('\n');
            t = t.getNext();
        }
        return sb.toString();
    }

    static interface ExtTokenItem
    extends TokenItem {
        public void setNext(TokenItem var1);

        public void setPrevious(TokenItem var1);

        public boolean isWritten();

        public void markWritten();

        public int getWrittenLength();

        public void setWrittenLength(int var1);

        public void setImage(String var1);
    }

    static class FilterDocumentItem
    extends TokenItem.FilterItem
    implements ExtTokenItem {
        private static final FilterDocumentItem NULL_ITEM = new FilterDocumentItem(null, null, false);
        private TokenItem previous;
        private TokenItem next;

        FilterDocumentItem(TokenItem delegate, FilterDocumentItem neighbour, boolean isNeighbourPrevious) {
            super(delegate);
            if (neighbour != null) {
                if (isNeighbourPrevious) {
                    this.previous = neighbour;
                } else {
                    this.next = neighbour;
                }
            }
        }

        public TokenItem getNext() {
            TokenItem ti;
            if (this.next == null && (ti = super.getNext()) != null) {
                this.next = new FilterDocumentItem(ti, this, true);
            }
            return this.next != NULL_ITEM ? this.next : null;
        }

        @Override
        public void setNext(TokenItem next) {
            this.next = next;
        }

        public void terminate() {
            this.setNext(NULL_ITEM);
        }

        @Override
        public void setPrevious(TokenItem previous) {
            this.previous = previous;
        }

        @Override
        public boolean isWritten() {
            return true;
        }

        @Override
        public void markWritten() {
        }

        @Override
        public int getWrittenLength() {
            return Integer.MAX_VALUE;
        }

        @Override
        public void setWrittenLength(int writtenLength) {
            if (writtenLength != Integer.MAX_VALUE) {
                throw new IllegalArgumentException("Wrong writtenLength=" + writtenLength);
            }
        }

        @Override
        public void setImage(String image) {
            throw new IllegalStateException("Cannot set image of the document-token.");
        }

        public TokenItem getPrevious() {
            TokenItem ti;
            if (this.previous == null && (ti = super.getPrevious()) != null) {
                this.previous = new FilterDocumentItem(ti, this, false);
            }
            return this.previous;
        }
    }

    static class FormatTokenItem
    extends TokenItem.AbstractItem
    implements ExtTokenItem {
        int writtenLength = -1;
        TokenItem next;
        TokenItem previous;
        String image;
        int saveOffset;

        FormatTokenItem(TokenID tokenID, TokenContextPath tokenContextPath, int offset, String image, TokenItem previous) {
            super(tokenID, tokenContextPath, offset, image);
            this.image = image;
            this.previous = previous;
            if (previous instanceof ExtTokenItem) {
                ((ExtTokenItem)previous).setNext(this);
            }
        }

        public TokenItem getNext() {
            return this.next;
        }

        public TokenItem getPrevious() {
            return this.previous;
        }

        @Override
        public void setNext(TokenItem next) {
            this.next = next;
        }

        @Override
        public void setPrevious(TokenItem previous) {
            this.previous = previous;
        }

        @Override
        public boolean isWritten() {
            return this.writtenLength >= 0;
        }

        @Override
        public void markWritten() {
            if (this.writtenLength == Integer.MAX_VALUE) {
                throw new IllegalStateException("Already marked unextendable.");
            }
            this.writtenLength = this.getImage().length();
        }

        @Override
        public int getWrittenLength() {
            return this.writtenLength;
        }

        @Override
        public void setWrittenLength(int writtenLength) {
            if (writtenLength <= this.writtenLength) {
                throw new IllegalArgumentException("this.writtenLength=" + this.writtenLength + " < writtenLength=" + writtenLength);
            }
            this.writtenLength = writtenLength;
        }

        public String getImage() {
            return this.image;
        }

        @Override
        public void setImage(String image) {
            this.image = image;
        }

        FormatTokenItem insertToken(TokenID tokenID, TokenContextPath tokenContextPath, int offset, String image) {
            FormatTokenItem fti = new FormatTokenItem(tokenID, tokenContextPath, offset, image, this.previous);
            fti.next = this;
            this.previous = fti;
            return fti;
        }

        void remove() {
            if (this.previous instanceof ExtTokenItem) {
                ((ExtTokenItem)this.previous).setNext(this.next);
            }
            if (this.next instanceof ExtTokenItem) {
                ((ExtTokenItem)this.next).setPrevious(this.previous);
            }
        }

        int getSaveOffset() {
            return this.saveOffset;
        }

        void setSaveOffset(int saveOffset) {
            this.saveOffset = saveOffset;
        }
    }

}

