/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.LocaleSupport
 */
package org.netbeans.modules.editor;

import java.beans.BeanDescriptor;
import org.netbeans.editor.LocaleSupport;
import org.netbeans.modules.editor.FormatterIndentEngineBeanInfo;
import org.netbeans.modules.editor.SimpleIndentEngine;

public class SimpleIndentEngineBeanInfo
extends FormatterIndentEngineBeanInfo {
    private BeanDescriptor beanDescriptor;

    @Override
    public BeanDescriptor getBeanDescriptor() {
        if (this.beanDescriptor == null) {
            this.beanDescriptor = new BeanDescriptor(this.getBeanClass());
            this.beanDescriptor.setDisplayName(this.getString("LAB_SimpleIndentEngine"));
            this.beanDescriptor.setShortDescription(this.getString("HINT_SimpleIndentEngine"));
            this.beanDescriptor.setValue("global", Boolean.TRUE);
        }
        return this.beanDescriptor;
    }

    @Override
    protected Class getBeanClass() {
        return SimpleIndentEngine.class;
    }

    @Override
    protected String getString(String key) {
        String retVal = LocaleSupport.getString((String)key);
        if (retVal == null) {
            retVal = super.getString(key);
        }
        return retVal;
    }
}

