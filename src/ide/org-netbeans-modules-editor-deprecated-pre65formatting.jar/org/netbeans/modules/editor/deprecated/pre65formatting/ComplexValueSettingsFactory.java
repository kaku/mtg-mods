/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.impl.ComplexValueSettingsFactory
 *  org.openide.text.IndentEngine
 */
package org.netbeans.modules.editor.deprecated.pre65formatting;

import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.ext.ExtFormatter;
import org.netbeans.modules.editor.FormatterIndentEngine;
import org.netbeans.modules.editor.IndentEngineFormatter;
import org.openide.text.IndentEngine;

public final class ComplexValueSettingsFactory {
    private ComplexValueSettingsFactory() {
    }

    public static Object getFormatterValue(MimePath mimePath, String settingName) {
        assert (settingName.equals("formatter"));
        IndentEngine eng = org.netbeans.modules.editor.impl.ComplexValueSettingsFactory.getIndentEngine((MimePath)mimePath);
        if (eng != null) {
            if (eng instanceof FormatterIndentEngine) {
                return ((FormatterIndentEngine)eng).getFormatter();
            }
            EditorKit kit = (EditorKit)MimeLookup.getLookup((MimePath)mimePath).lookup(EditorKit.class);
            if (kit != null) {
                return new IndentEngineFormatter(kit.getClass(), eng);
            }
        }
        return null;
    }
}

