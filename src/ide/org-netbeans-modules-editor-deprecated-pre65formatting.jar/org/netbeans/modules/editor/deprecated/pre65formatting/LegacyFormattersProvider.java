/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.ExtraLock
 *  org.netbeans.modules.editor.indent.spi.IndentTask
 *  org.netbeans.modules.editor.indent.spi.IndentTask$Factory
 *  org.netbeans.modules.editor.indent.spi.ReformatTask
 *  org.netbeans.modules.editor.indent.spi.ReformatTask$Factory
 *  org.netbeans.spi.editor.mimelookup.MimeDataProvider
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$MutableContext
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.deprecated.pre65formatting;

import java.io.IOException;
import java.io.Writer;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Stack;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Formatter;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtFormatter;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.IndentTask;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.netbeans.spi.editor.mimelookup.MimeDataProvider;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public final class LegacyFormattersProvider
implements MimeDataProvider {
    private static final Logger LOG = Logger.getLogger(LegacyFormattersProvider.class.getName());
    private static ThreadLocal<Stack<Reference<Document>>> FORMATTING_CONTEXT_DOCUMENT = new ThreadLocal<Stack<Reference<Document>>>(){

        @Override
        protected Stack<Reference<Document>> initialValue() {
            return new Stack<Reference<Document>>();
        }
    };

    public Lookup getLookup(final MimePath mimePath) {
        if (mimePath.size() == 1) {
            return new ProxyLookup(){
                private final AtomicBoolean initialized;

                protected void beforeLookup(Lookup.Template<?> template) {
                    IndentReformatTaskFactoriesProvider provider;
                    super.beforeLookup(template);
                    Class clz = template.getType();
                    if ((IndentTask.Factory.class.isAssignableFrom(clz) || ReformatTask.Factory.class.isAssignableFrom(clz) || TypedTextInterceptor.Factory.class.isAssignableFrom(clz)) && !this.initialized.getAndSet(true) && (provider = IndentReformatTaskFactoriesProvider.get(mimePath)) != null) {
                        IndentTask.Factory legacyIndenter = provider.getIndentTaskFactory();
                        ReformatTask.Factory legacyFormatter = provider.getReformatTaskFactory();
                        TypedTextInterceptor.Factory legacyAutoIndenter = provider.getTypedTextInterceptorFactory();
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.log(Level.FINE, "'{0}' uses legacyIndenter={1}, legacyFormatter={2}, legacyAutoIndenter={3}", new Object[]{mimePath.getPath(), legacyIndenter, legacyFormatter, legacyAutoIndenter});
                        }
                        this.setLookups(new Lookup[]{Lookups.fixed((Object[])new Object[]{legacyIndenter, legacyFormatter, legacyAutoIndenter})});
                    }
                }
            };
        }
        return null;
    }

    public static Document getFormattingContextDocument() {
        Stack<Reference<Document>> stack = FORMATTING_CONTEXT_DOCUMENT.get();
        return stack.isEmpty() ? null : stack.peek().get();
    }

    public static void pushFormattingContextDocument(Document doc) {
        FORMATTING_CONTEXT_DOCUMENT.get().push(new WeakReference<Document>(doc));
    }

    public static void popFormattingContextDocument(Document doc) {
        Stack<Reference<Document>> stack = FORMATTING_CONTEXT_DOCUMENT.get();
        assert (!stack.empty());
        Reference<Document> ref = stack.pop();
        Document docFromStack = ref.get();
        assert (docFromStack == doc);
        ref.clear();
    }

    private static final class AutoIndenter
    implements TypedTextInterceptor {
        private final ExtFormatter formatter;

        public AutoIndenter(ExtFormatter formatter) {
            this.formatter = formatter;
        }

        public boolean beforeInsert(TypedTextInterceptor.Context context) throws BadLocationException {
            return false;
        }

        public void insert(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        }

        public void afterInsert(TypedTextInterceptor.Context context) throws BadLocationException {
            if (context.getDocument() instanceof BaseDocument) {
                BaseDocument doc = (BaseDocument)context.getDocument();
                int[] fmtBlk = this.formatter.getReformatBlock(context.getComponent(), context.getText());
                if (fmtBlk != null) {
                    try {
                        fmtBlk[0] = Utilities.getRowStart((BaseDocument)doc, (int)fmtBlk[0]);
                        fmtBlk[1] = Utilities.getRowEnd((BaseDocument)doc, (int)fmtBlk[1]);
                        this.formatter.reformat(doc, fmtBlk[0], fmtBlk[1], true);
                    }
                    catch (BadLocationException e) {
                    }
                    catch (IOException e) {
                        // empty catch block
                    }
                }
            }
        }

        public void cancelled(TypedTextInterceptor.Context context) {
        }
    }

    private static final class Reformatter
    implements ReformatTask {
        private final Context context;
        private final Formatter formatter;

        public Reformatter(Context context, Formatter formatter) {
            this.context = context;
            this.formatter = formatter;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void reformat() throws BadLocationException {
            LegacyFormattersProvider.pushFormattingContextDocument(this.context.document());
            try {
                this.formatter.reformat((BaseDocument)this.context.document(), this.context.startOffset(), this.context.endOffset());
            }
            finally {
                LegacyFormattersProvider.popFormattingContextDocument(this.context.document());
            }
        }

        public ExtraLock reformatLock() {
            return new ExtraLock(){

                public void lock() {
                    Reformatter.this.formatter.reformatLock();
                }

                public void unlock() {
                    Reformatter.this.formatter.reformatUnlock();
                }
            };
        }

    }

    private static final class Indenter
    implements IndentTask {
        private final Context context;
        private final Formatter formatter;

        public Indenter(Context context, Formatter formatter) {
            this.context = context;
            this.formatter = formatter;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void reindent() throws BadLocationException {
            Document doc = this.context.document();
            int startOffset = this.context.startOffset();
            int endOffset = this.context.endOffset();
            LegacyFormattersProvider.pushFormattingContextDocument(doc);
            try {
                int startLineIndex;
                Element lineElem;
                Element lineRootElem = Indenter.lineRootElement(doc);
                Position endPos = doc.createPosition(endOffset);
                while ((startLineIndex = lineRootElem.getElementIndex(startOffset = this.formatter.indentLine(doc, startOffset)) + 1) < lineRootElem.getElementCount() && (startOffset = (lineElem = lineRootElem.getElement(startLineIndex)).getStartOffset()) < endPos.getOffset()) {
                }
            }
            finally {
                LegacyFormattersProvider.popFormattingContextDocument(doc);
            }
        }

        public ExtraLock indentLock() {
            return new ExtraLock(){

                public void lock() {
                    Indenter.this.formatter.indentLock();
                }

                public void unlock() {
                    Indenter.this.formatter.indentUnlock();
                }
            };
        }

        private static Element lineRootElement(Document doc) {
            return doc instanceof StyledDocument ? ((StyledDocument)doc).getParagraphElement(0).getParentElement() : doc.getDefaultRootElement();
        }

    }

    private static final class IndentReformatTaskFactoriesProvider {
        private static final Map<MimePath, Reference<IndentReformatTaskFactoriesProvider>> cache = new WeakHashMap<MimePath, Reference<IndentReformatTaskFactoriesProvider>>();
        private static final String NO_FORMATTER = "NO_FORMATTER";
        private final MimePath mimePath;
        private IndentTask.Factory indentTaskFactory;
        private ReformatTask.Factory reformatTaskFactory;
        private TypedTextInterceptor.Factory typedTextInterceptorFactory;
        private Object legacyFormatter;

        public static IndentReformatTaskFactoriesProvider get(MimePath mimePath) {
            IndentReformatTaskFactoriesProvider provider;
            Reference<IndentReformatTaskFactoriesProvider> ref = cache.get((Object)mimePath);
            IndentReformatTaskFactoriesProvider indentReformatTaskFactoriesProvider = provider = ref == null ? null : ref.get();
            if (provider == null) {
                provider = new IndentReformatTaskFactoriesProvider(mimePath);
                cache.put(mimePath, new WeakReference<IndentReformatTaskFactoriesProvider>(provider));
            }
            return provider;
        }

        public IndentTask.Factory getIndentTaskFactory() {
            if (this.indentTaskFactory == null) {
                this.indentTaskFactory = new IndentTask.Factory(){

                    public IndentTask createTask(Context context) {
                        Formatter formatter = IndentReformatTaskFactoriesProvider.this.getFormatter();
                        if (formatter != null && context.document() instanceof BaseDocument) {
                            return new Indenter(context, formatter);
                        }
                        return null;
                    }
                };
            }
            return this.indentTaskFactory;
        }

        public ReformatTask.Factory getReformatTaskFactory() {
            if (this.reformatTaskFactory == null) {
                this.reformatTaskFactory = new ReformatTask.Factory(){

                    public ReformatTask createTask(Context context) {
                        Formatter formatter = IndentReformatTaskFactoriesProvider.this.getFormatter();
                        if (formatter != null && context.document() instanceof BaseDocument) {
                            return new Reformatter(context, formatter);
                        }
                        return null;
                    }
                };
            }
            return this.reformatTaskFactory;
        }

        public TypedTextInterceptor.Factory getTypedTextInterceptorFactory() {
            if (this.typedTextInterceptorFactory == null) {
                this.typedTextInterceptorFactory = new TypedTextInterceptor.Factory(){

                    public TypedTextInterceptor createTypedTextInterceptor(MimePath mimePath) {
                        Formatter formatter = IndentReformatTaskFactoriesProvider.this.getFormatter();
                        if (formatter instanceof ExtFormatter) {
                            return new AutoIndenter((ExtFormatter)formatter);
                        }
                        return null;
                    }
                };
            }
            return this.typedTextInterceptorFactory;
        }

        private IndentReformatTaskFactoriesProvider(MimePath mimePath) {
            this.mimePath = mimePath;
        }

        private Formatter getFormatter() {
            if (this.legacyFormatter == null) {
                EditorKit kit = (EditorKit)MimeLookup.getLookup((MimePath)this.mimePath).lookup(EditorKit.class);
                if (kit != null) {
                    try {
                        Method createFormatterMethod = kit.getClass().getDeclaredMethod("createFormatter", new Class[0]);
                        this.legacyFormatter = createFormatterMethod.invoke(kit, new Object[0]);
                    }
                    catch (Exception e) {
                        this.legacyFormatter = e;
                    }
                } else {
                    this.legacyFormatter = "NO_FORMATTER";
                }
            }
            return this.legacyFormatter instanceof Formatter ? (Formatter)this.legacyFormatter : null;
        }

    }

}

