/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseKit
 */
package org.netbeans.modules.editor;

import org.netbeans.editor.BaseKit;
import org.netbeans.editor.ext.ExtFormatter;
import org.netbeans.modules.editor.FormatterIndentEngine;

public class SimpleIndentEngine
extends FormatterIndentEngine {
    static final long serialVersionUID = -6445463074939516878L;

    @Override
    protected ExtFormatter createFormatter() {
        return new ExtFormatter.Simple(BaseKit.class);
    }

    @Override
    protected boolean acceptMimeType(String mimeType) {
        return true;
    }
}

