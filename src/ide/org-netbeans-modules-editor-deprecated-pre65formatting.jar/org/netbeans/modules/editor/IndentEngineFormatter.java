/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.IndentEngine
 */
package org.netbeans.modules.editor;

import java.io.Writer;
import javax.swing.text.Document;
import org.netbeans.editor.Formatter;
import org.openide.text.IndentEngine;

public class IndentEngineFormatter
extends Formatter {
    private IndentEngine indentEngine;

    public IndentEngineFormatter(Class kitClass, IndentEngine indentEngine) {
        super(kitClass);
        this.indentEngine = indentEngine;
    }

    public IndentEngine getIndentEngine() {
        return this.indentEngine;
    }

    @Override
    public int indentLine(Document doc, int offset) {
        return this.indentEngine.indentLine(doc, offset);
    }

    @Override
    public int indentNewLine(Document doc, int offset) {
        return this.indentEngine.indentNewLine(doc, offset);
    }

    @Override
    public Writer createWriter(Document doc, int offset, Writer writer) {
        return this.indentEngine.createWriter(doc, offset, writer);
    }
}

