/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.LocaleSupport
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.editor;

import java.awt.Image;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import java.lang.reflect.Method;
import org.netbeans.editor.LocaleSupport;
import org.openide.util.ImageUtilities;

public abstract class FormatterIndentEngineBeanInfo
extends SimpleBeanInfo {
    private String iconPrefix;
    private Image icon;
    private Image icon32;
    private PropertyDescriptor[] propertyDescriptors;
    private String[] propertyNames;

    public FormatterIndentEngineBeanInfo() {
        this(null);
    }

    public FormatterIndentEngineBeanInfo(String iconPrefix) {
        this.iconPrefix = iconPrefix;
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        if (this.propertyDescriptors == null) {
            String[] propNames = this.getPropertyNames();
            PropertyDescriptor[] pds = new PropertyDescriptor[propNames.length];
            for (int i = 0; i < propNames.length; ++i) {
                pds[i] = this.createPropertyDescriptor(propNames[i]);
                pds[i].setDisplayName(this.getString("PROP_indentEngine_" + propNames[i]));
                pds[i].setShortDescription(this.getString("HINT_indentEngine_" + propNames[i]));
            }
            this.propertyDescriptors = pds;
            this.updatePropertyDescriptors();
        }
        return this.propertyDescriptors;
    }

    protected PropertyDescriptor createPropertyDescriptor(String propName) {
        PropertyDescriptor pd;
        block9 : {
            try {
                pd = new PropertyDescriptor(propName, this.getBeanClass());
            }
            catch (IntrospectionException e) {
                try {
                    pd = new PropertyDescriptor(propName, null, null);
                }
                catch (IntrospectionException e2) {
                    throw new IllegalStateException("Invalid property name=" + propName);
                }
                String cap = FormatterIndentEngineBeanInfo.capitalize(propName);
                Method m = this.findMethod("get" + cap);
                if (m != null) {
                    try {
                        pd.setReadMethod(m);
                    }
                    catch (IntrospectionException e2) {
                        // empty catch block
                    }
                }
                if ((m = this.findMethod("set" + cap)) == null) break block9;
                try {
                    pd.setWriteMethod(m);
                }
                catch (IntrospectionException e2) {
                    // empty catch block
                }
            }
        }
        return pd;
    }

    protected void updatePropertyDescriptors() {
    }

    private Method findMethod(String name) {
        try {
            Method[] ma = this.getBeanClass().getDeclaredMethods();
            for (int i = 0; i < ma.length; ++i) {
                if (!name.equals(ma[i].getName())) continue;
                return ma[i];
            }
        }
        catch (SecurityException e) {
            // empty catch block
        }
        return null;
    }

    private static String capitalize(String s) {
        if (s.length() == 0) {
            return s;
        }
        char[] chars = s.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);
        return new String(chars);
    }

    protected abstract Class getBeanClass();

    protected String[] getPropertyNames() {
        if (this.propertyNames == null) {
            this.propertyNames = this.createPropertyNames();
        }
        return this.propertyNames;
    }

    protected String[] createPropertyNames() {
        return new String[]{"expandTabs", "spacesPerTab"};
    }

    protected PropertyDescriptor getPropertyDescriptor(String propertyName) {
        String[] propNames = this.getPropertyNames();
        for (int i = 0; i < propNames.length; ++i) {
            if (!propertyName.equals(propNames[i])) continue;
            return this.getPropertyDescriptors()[i];
        }
        return null;
    }

    protected void setPropertyEditor(String propertyName, Class propertyEditor) {
        PropertyDescriptor pd = this.getPropertyDescriptor(propertyName);
        if (pd != null) {
            pd.setPropertyEditorClass(propertyEditor);
        }
    }

    protected void setExpert(String[] expertPropertyNames) {
        for (int i = 0; i < expertPropertyNames.length; ++i) {
            PropertyDescriptor pd = this.getPropertyDescriptor(expertPropertyNames[i]);
            if (pd == null) continue;
            pd.setExpert(true);
        }
    }

    protected void setHidden(String[] hiddenPropertyNames) {
        for (int i = 0; i < hiddenPropertyNames.length; ++i) {
            PropertyDescriptor pd = this.getPropertyDescriptor(hiddenPropertyNames[i]);
            if (pd == null) continue;
            pd.setHidden(true);
        }
    }

    private String getValidIconPrefix() {
        return this.iconPrefix != null ? this.iconPrefix : "org/netbeans/modules/editor/resources/indentEngine";
    }

    private Image getDefaultIcon(String iconResource) {
        return ImageUtilities.loadImage((String)iconResource);
    }

    @Override
    public Image getIcon(int type) {
        if (type == 1 || type == 3) {
            if (this.icon == null) {
                this.icon = this.loadImage(this.getValidIconPrefix() + ".gif");
            }
            return this.icon != null ? this.icon : this.getDefaultIcon(this.getValidIconPrefix() + ".gif");
        }
        if (this.icon32 == null) {
            this.icon32 = this.loadImage(this.getValidIconPrefix() + "32.gif");
        }
        return this.icon32 != null ? this.icon32 : this.getDefaultIcon(this.getValidIconPrefix() + "32.gif");
    }

    protected String getString(String key) {
        return LocaleSupport.getString((String)key);
    }
}

