/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseKit
 *  org.openide.ErrorManager
 *  org.openide.text.IndentEngine
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor;

import java.awt.Toolkit;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.io.Writer;
import java.util.Date;
import javax.swing.text.Document;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.ext.ExtFormatter;
import org.openide.ErrorManager;
import org.openide.text.IndentEngine;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public abstract class FormatterIndentEngine
extends IndentEngine {
    public static final String EXPAND_TABS_PROP = "expandTabs";
    public static final String SPACES_PER_TAB_PROP = "spacesPerTab";
    static final long serialVersionUID = -3408217516931076216L;
    private transient ExtFormatter formatter;
    private String[] acceptedMimeTypes;
    private static final ObjectStreamField[] serialPersistentFields = new ObjectStreamField[]{new ObjectStreamField("expandTabs", Boolean.TYPE), new ObjectStreamField("spacesPerTab", Integer.TYPE)};

    public ExtFormatter getFormatter() {
        if (this.formatter == null) {
            this.formatter = this.createFormatter();
            if (this.formatter == null) {
                this.formatter = new ExtFormatter(BaseKit.class);
            }
        }
        return this.formatter;
    }

    protected abstract ExtFormatter createFormatter();

    public Object getValue(String settingName) {
        return this.getFormatter().getSettingValue(settingName);
    }

    public void setValue(String settingName, Object newValue, String propertyName) {
        Object oldValue = this.getValue(settingName);
        if (oldValue == null && newValue == null || oldValue != null && oldValue.equals(newValue)) {
            return;
        }
        this.getFormatter().setSettingValue(settingName, newValue);
        if (propertyName != null) {
            this.firePropertyChange(propertyName, oldValue, newValue);
        }
    }

    public void setValue(String settingName, Object newValue) {
        this.setValue(settingName, newValue, null);
    }

    public int indentLine(Document doc, int offset) {
        return this.getFormatter().indentLine(doc, offset);
    }

    public int indentNewLine(Document doc, int offset) {
        return this.getFormatter().indentNewLine(doc, offset);
    }

    public Writer createWriter(Document doc, int offset, Writer writer) {
        return this.getFormatter().createWriter(doc, offset, writer);
    }

    protected boolean acceptMimeType(String mimeType) {
        if (this.acceptedMimeTypes != null) {
            for (int i = this.acceptedMimeTypes.length - 1; i >= 0; --i) {
                if (!this.acceptedMimeTypes[i].equals(mimeType)) continue;
                return true;
            }
        }
        return false;
    }

    public boolean isExpandTabs() {
        return this.getFormatter().expandTabs();
    }

    public void setExpandTabs(boolean expandTabs) {
        boolean old = this.getFormatter().expandTabs();
        this.getFormatter().setExpandTabs(expandTabs);
        if (old != expandTabs) {
            this.setValue("expand-tabs", expandTabs, "expandTabs");
            this.firePropertyChange("expandTabs", (Object)(old ? Boolean.TRUE : Boolean.FALSE), (Object)(expandTabs ? Boolean.TRUE : Boolean.FALSE));
        }
    }

    public int getSpacesPerTab() {
        return this.getFormatter().getSpacesPerTab();
    }

    public void setSpacesPerTab(int spacesPerTab) {
        if (spacesPerTab <= 0) {
            IllegalArgumentException iae = new IllegalArgumentException("Invalid argument");
            ErrorManager errMan = (ErrorManager)Lookup.getDefault().lookup(ErrorManager.class);
            if (errMan != null) {
                Toolkit.getDefaultToolkit().beep();
                errMan.annotate((Throwable)iae, 256, iae.getMessage(), NbBundle.getMessage(FormatterIndentEngine.class, (String)"MSG_NegativeValue"), null, null);
            } else {
                throw iae;
            }
        }
        int old = this.getFormatter().getSpacesPerTab();
        this.getFormatter().setSpacesPerTab(spacesPerTab);
        if (old != spacesPerTab) {
            this.setValue("spaces-per-tab", new Integer(spacesPerTab), "spacesPerTab");
            this.firePropertyChange("spacesPerTab", (Object)new Integer(old), (Object)new Integer(spacesPerTab));
        }
    }

    public void setAcceptedMimeTypes(String[] mimes) {
        this.acceptedMimeTypes = mimes;
    }

    public String[] getAcceptedMimeTypes() {
        return this.acceptedMimeTypes;
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ObjectInputStream.GetField fields = ois.readFields();
        this.setExpandTabs(fields.get("expandTabs", true));
        this.setSpacesPerTab(fields.get("spacesPerTab", 4));
    }

    private void writeObject(ObjectOutputStream oos) throws IOException, ClassNotFoundException {
        ObjectOutputStream.PutField fields = oos.putFields();
        fields.put("expandTabs", this.isExpandTabs());
        fields.put("spacesPerTab", this.getSpacesPerTab());
        oos.writeFields();
    }
}

