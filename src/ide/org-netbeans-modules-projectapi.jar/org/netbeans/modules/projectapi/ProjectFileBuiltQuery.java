/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileBuiltQuery
 *  org.netbeans.api.queries.FileBuiltQuery$Status
 *  org.netbeans.spi.queries.FileBuiltQueryImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.projectapi;

import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.queries.FileBuiltQuery;
import org.netbeans.spi.queries.FileBuiltQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class ProjectFileBuiltQuery
implements FileBuiltQueryImplementation {
    public FileBuiltQuery.Status getStatus(FileObject file) {
        FileBuiltQueryImplementation fbqi;
        Project p = FileOwnerQuery.getOwner(file);
        if (p != null && (fbqi = (FileBuiltQueryImplementation)p.getLookup().lookup(FileBuiltQueryImplementation.class)) != null) {
            return fbqi.getStatus(file);
        }
        return null;
    }
}

