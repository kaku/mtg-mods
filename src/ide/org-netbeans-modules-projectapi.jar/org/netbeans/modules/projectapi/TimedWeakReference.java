/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.projectapi;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public final class TimedWeakReference<T>
extends WeakReference<T>
implements Runnable {
    public static int TIMEOUT = 15000;
    private static final RequestProcessor RP = new RequestProcessor("TimedWeakReference");
    private RequestProcessor.Task task;
    private T o;
    private long touched;

    public TimedWeakReference(T o) {
        super(o, Utilities.activeReferenceQueue());
        this.o = o;
        this.task = RP.create((Runnable)this);
        this.task.schedule(TIMEOUT);
    }

    @Override
    public synchronized void run() {
        if (this.o != null) {
            long unused = System.currentTimeMillis() - this.touched;
            if (unused > (long)(TIMEOUT / 2)) {
                this.o = null;
                this.touched = 0;
            } else {
                this.task.schedule(TIMEOUT - (int)unused);
            }
        }
    }

    @Override
    public synchronized T get() {
        if (this.o == null) {
            this.o = WeakReference.super.get();
        }
        if (this.o != null) {
            if (this.touched == 0) {
                this.task.schedule(TIMEOUT);
            }
            this.touched = System.currentTimeMillis();
            return this.o;
        }
        return null;
    }
}

