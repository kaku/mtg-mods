/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.queries.CollocationQueryImplementation2
 */
package org.netbeans.modules.projectapi;

import java.net.URI;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.spi.queries.CollocationQueryImplementation2;

public class FileOwnerCollocationQueryImpl
implements CollocationQueryImplementation2 {
    public URI findRoot(URI uri) {
        if (FileOwnerQuery.getOwner(uri) == null) {
            return null;
        }
        URI parent = uri;
        do {
            uri = parent;
        } while (FileOwnerQuery.getOwner(parent = parent.resolve(parent.toString().endsWith("/") ? ".." : ".")) != null && !parent.getPath().equals("/"));
        return uri;
    }

    public boolean areCollocated(URI file1, URI file2) {
        URI root = this.findRoot(file1);
        boolean first = true;
        if (root == null) {
            root = this.findRoot(file2);
            first = false;
        }
        if (root != null) {
            String check = (first ? file2.toString() : file1.toString()) + '/';
            return check.startsWith(root.toString());
        }
        return false;
    }
}

