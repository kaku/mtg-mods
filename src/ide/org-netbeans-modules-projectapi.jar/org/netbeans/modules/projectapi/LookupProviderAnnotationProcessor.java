/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.projectapi;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.NestingKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import org.netbeans.api.project.Project;
import org.netbeans.modules.projectapi.LazyLookupProviders;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.project.LookupProvider;
import org.netbeans.spi.project.ProjectServiceProvider;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;
import org.openide.util.Lookup;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class LookupProviderAnnotationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(LookupProvider.Registration.class.getCanonicalName(), ProjectServiceProvider.class.getCanonicalName(), LookupMerger.Registration.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e22 : roundEnv.getElementsAnnotatedWith(LookupProvider.Registration.class)) {
            LookupProvider.Registration lpr = e22.getAnnotation(LookupProvider.Registration.class);
            if (lpr.projectType().length == 0 && lpr.projectTypes().length == 0) {
                throw new LayerGenerationException("You must specify either projectType or projectTypes", e22, this.processingEnv, (Annotation)lpr);
            }
            for (String type2 : lpr.projectType()) {
                this.layer(new Element[]{e22}).instanceFile("Projects/" + type2 + "/Lookup", null, LookupProvider.class, (Annotation)lpr, null).write();
            }
            for (String type : lpr.projectTypes()) {
                this.layer(new Element[]{e22}).instanceFile("Projects/" + type.id() + "/Lookup", null, LookupProvider.class, (Annotation)((Object)type), null).position(type.position()).write();
            }
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(ProjectServiceProvider.class)) {
            LayerBuilder.File f;
            ProjectServiceProvider psp = e.getAnnotation(ProjectServiceProvider.class);
            List<TypeMirror> services = this.findServiceAnnotation(e);
            if (services.isEmpty()) {
                throw new LayerGenerationException("Must specify at least one service", e, this.processingEnv, (Annotation)psp);
            }
            String servicesBinName = null;
            for (TypeMirror service : services) {
                String n = this.processingEnv.getElementUtils().getBinaryName((TypeElement)this.processingEnv.getTypeUtils().asElement(service)).toString();
                if (n.equals(LookupMerger.class.getName())) {
                    throw new LayerGenerationException("@ProjectServiceProvider should not be used on LookupMerger; use @LookupMerger.Registration instead", e, this.processingEnv, (Annotation)psp);
                }
                servicesBinName = servicesBinName == null ? n : servicesBinName + "," + n;
            }
            String[] binAndMethodNames = this.findPSPDefinition(e, services, psp);
            if (psp.projectType().length == 0 && psp.projectTypes().length == 0) {
                throw new LayerGenerationException("You must specify either projectType or projectTypes", e, this.processingEnv, (Annotation)psp);
            }
            String fileBaseName = binAndMethodNames[0].replace('.', '-');
            if (binAndMethodNames[1] != null) {
                fileBaseName = fileBaseName + "-" + binAndMethodNames[1];
            }
            for (String type32 : psp.projectType()) {
                f = this.layer(new Element[]{e}).file("Projects/" + type32 + "/Lookup/" + (String)fileBaseName + ".instance").methodvalue("instanceCreate", LazyLookupProviders.class.getName(), "forProjectServiceProvider").stringvalue("class", binAndMethodNames[0]).stringvalue("service", servicesBinName);
                if (binAndMethodNames[1] != null) {
                    f.stringvalue("method", binAndMethodNames[1]);
                }
                f.write();
            }
            for (String type : psp.projectTypes()) {
                f = this.layer(new Element[]{e}).file("Projects/" + type.id() + "/Lookup/" + (String)fileBaseName + ".instance").methodvalue("instanceCreate", LazyLookupProviders.class.getName(), "forProjectServiceProvider").stringvalue("class", binAndMethodNames[0]).stringvalue("service", servicesBinName).position(type.position());
                if (binAndMethodNames[1] != null) {
                    f.stringvalue("method", binAndMethodNames[1]);
                }
                f.write();
            }
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(LookupMerger.Registration.class)) {
            String fileBaseName;
            DeclaredType impl;
            LookupMerger.Registration lmr = e2.getAnnotation(LookupMerger.Registration.class);
            if (e2.getKind() == ElementKind.CLASS) {
                fileBaseName = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e2).toString().replace('.', '-');
                impl = (DeclaredType)e2.asType();
            } else {
                fileBaseName = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e2.getEnclosingElement()).toString().replace('.', '-') + "-" + e2.getSimpleName().toString();
                impl = (DeclaredType)((ExecutableElement)e2).getReturnType();
            }
            DeclaredType service = this.findLookupMergerType(impl);
            if (service == null) {
                throw new LayerGenerationException("Not assignable to LookupMerger<T> for some T", e2, this.processingEnv, (Annotation)lmr);
            }
            String serviceBinName = this.processingEnv.getElementUtils().getBinaryName((TypeElement)service.asElement()).toString();
            if (lmr.projectType().length == 0 && lmr.projectTypes().length == 0) {
                throw new LayerGenerationException("You must specify either projectType or projectTypes", e2, this.processingEnv, (Annotation)lmr);
            }
            for (String type : lmr.projectType()) {
                this.layer(new Element[]{e2}).file("Projects/" + type + "/Lookup/" + fileBaseName + ".instance").methodvalue("instanceCreate", LazyLookupProviders.class.getName(), "forLookupMerger").instanceAttribute("lookupMergerInstance", LookupMerger.class).stringvalue("service", serviceBinName).write();
            }
            for (String type2 : lmr.projectTypes()) {
                this.layer(new Element[]{e2}).file("Projects/" + type2.id() + "/Lookup/" + fileBaseName + ".instance").methodvalue("instanceCreate", LazyLookupProviders.class.getName(), "forLookupMerger").instanceAttribute("lookupMergerInstance", LookupMerger.class).stringvalue("service", serviceBinName).position(type2.position()).write();
            }
        }
        return true;
    }

    private List<TypeMirror> findServiceAnnotation(Element e) throws LayerGenerationException {
        for (AnnotationMirror ann : e.getAnnotationMirrors()) {
            if (!ProjectServiceProvider.class.getName().equals(ann.getAnnotationType().toString())) continue;
            for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> attr : ann.getElementValues().entrySet()) {
                if (!attr.getKey().getSimpleName().contentEquals("service")) continue;
                ArrayList<TypeMirror> r = new ArrayList<TypeMirror>();
                for (Object item : (List)attr.getValue().getValue()) {
                    TypeMirror type = (TypeMirror)((AnnotationValue)item).getValue();
                    Types typeUtils = this.processingEnv.getTypeUtils();
                    for (TypeMirror otherType : r) {
                        for (boolean swap : new boolean[]{false, true}) {
                            TypeMirror t2;
                            TypeMirror t1 = swap ? type : otherType;
                            TypeMirror typeMirror = t2 = swap ? otherType : type;
                            if (!typeUtils.isSubtype(t1, t2)) continue;
                            this.processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "registering under both " + typeUtils.asElement(t2).getSimpleName() + " and its subtype " + typeUtils.asElement(t1).getSimpleName() + " will not work if LookupMerger<" + typeUtils.asElement(t2).getSimpleName() + "> is used (#205151)", e, ann, attr.getValue());
                        }
                    }
                    r.add(type);
                }
                return r;
            }
            throw new LayerGenerationException("No service attr found", e);
        }
        throw new LayerGenerationException("No @ProjectServiceProvider found", e);
    }

    private String[] findPSPDefinition(Element e, List<TypeMirror> services, ProjectServiceProvider psp) throws LayerGenerationException {
        if (e.getKind() == ElementKind.CLASS) {
            TypeElement clazz = (TypeElement)e;
            if (clazz.getNestingKind().isNested() && !clazz.getModifiers().contains((Object)Modifier.STATIC)) {
                throw new LayerGenerationException("An inner class cannot be constructed as a service", e, this.processingEnv, (Annotation)psp);
            }
            for (TypeMirror service : services) {
                if (this.processingEnv.getTypeUtils().isAssignable(clazz.asType(), service)) continue;
                throw new LayerGenerationException("Not assignable to " + service, e, this.processingEnv, (Annotation)psp);
            }
            int constructorCount = 0;
            block1 : for (ExecutableElement constructor : ElementFilter.constructorsIn(clazz.getEnclosedElements())) {
                List<? extends VariableElement> params;
                if (!constructor.getModifiers().contains((Object)Modifier.PUBLIC) || (params = constructor.getParameters()).size() > 2) continue;
                for (VariableElement param : params) {
                    if (param.asType().equals(this.processingEnv.getElementUtils().getTypeElement(Project.class.getCanonicalName()).asType()) || param.asType().equals(this.processingEnv.getElementUtils().getTypeElement(Lookup.class.getCanonicalName()).asType())) continue;
                    continue block1;
                }
                ++constructorCount;
            }
            if (constructorCount != 1) {
                throw new LayerGenerationException("Must have exactly one public constructor optionally taking Project and/or Lookup", e, this.processingEnv, (Annotation)psp);
            }
            if (!clazz.getModifiers().contains((Object)Modifier.PUBLIC)) {
                throw new LayerGenerationException("Class must be public", e, this.processingEnv, (Annotation)psp);
            }
            return new String[]{this.processingEnv.getElementUtils().getBinaryName(clazz).toString(), null};
        }
        ExecutableElement meth = (ExecutableElement)e;
        for (TypeMirror service : services) {
            if (this.processingEnv.getTypeUtils().isAssignable(meth.getReturnType(), service)) continue;
            throw new LayerGenerationException("Not assignable to " + service, e, this.processingEnv, (Annotation)psp);
        }
        if (!meth.getModifiers().contains((Object)Modifier.PUBLIC)) {
            throw new LayerGenerationException("Method must be public", e, this.processingEnv, (Annotation)psp);
        }
        if (!meth.getModifiers().contains((Object)Modifier.STATIC)) {
            throw new LayerGenerationException("Method must be static", e, this.processingEnv, (Annotation)psp);
        }
        List<? extends VariableElement> params = meth.getParameters();
        if (params.size() > 2) {
            throw new LayerGenerationException("Method must take at most two parameters", e, this.processingEnv, (Annotation)psp);
        }
        for (VariableElement param : params) {
            if (param.asType().equals(this.processingEnv.getElementUtils().getTypeElement(Project.class.getCanonicalName()).asType()) || param.asType().equals(this.processingEnv.getElementUtils().getTypeElement(Lookup.class.getCanonicalName()).asType())) continue;
            throw new LayerGenerationException("Method parameters may be either Lookup or Project", e, this.processingEnv, (Annotation)psp);
        }
        if (!meth.getEnclosingElement().getModifiers().contains((Object)Modifier.PUBLIC)) {
            throw new LayerGenerationException("Class must be public", e, this.processingEnv, (Annotation)psp);
        }
        return new String[]{this.processingEnv.getElementUtils().getBinaryName((TypeElement)meth.getEnclosingElement()).toString(), meth.getSimpleName().toString()};
    }

    private DeclaredType findLookupMergerType(DeclaredType t) {
        String rawName = this.processingEnv.getTypeUtils().erasure(t).toString();
        if (rawName.equals(LookupMerger.class.getName())) {
            List<? extends TypeMirror> args = t.getTypeArguments();
            if (args.size() == 1) {
                return (DeclaredType)args.get(0);
            }
            return null;
        }
        for (TypeMirror supe : this.processingEnv.getTypeUtils().directSupertypes(t)) {
            DeclaredType result = this.findLookupMergerType((DeclaredType)supe);
            if (result == null) continue;
            return result;
        }
        return null;
    }
}

