/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.projectapi;

import org.netbeans.modules.projectapi.SimpleFileOwnerQueryImplementation;

public class Installer
implements Runnable {
    @Override
    public void run() {
        SimpleFileOwnerQueryImplementation.deserialize();
    }

    public static final class Down
    implements Runnable {
        @Override
        public void run() {
            SimpleFileOwnerQueryImplementation.serialize();
        }
    }

}

