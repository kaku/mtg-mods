/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.queries.FileEncodingQueryImplementation
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.projectapi;

import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.spi.queries.FileEncodingQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class ProjectFileEncodingQueryImplementation
extends FileEncodingQueryImplementation {
    private static final Logger LOG = Logger.getLogger(ProjectFileEncodingQueryImplementation.class.getName());

    public Charset getEncoding(FileObject file) {
        Project p = FileOwnerQuery.getOwner(file);
        if (p == null) {
            LOG.log(Level.FINER, "{0}: no owner", (Object)file);
            return null;
        }
        FileEncodingQueryImplementation delegate = (FileEncodingQueryImplementation)p.getLookup().lookup(FileEncodingQueryImplementation.class);
        if (delegate == null) {
            LOG.log(Level.FINE, "{0}: no FEQI in {1}", new Object[]{file, p});
            return null;
        }
        Charset encoding = delegate.getEncoding(file);
        LOG.log(Level.FINE, "{0}: got {1} from {2}", new Object[]{file, encoding, delegate});
        return encoding;
    }
}

