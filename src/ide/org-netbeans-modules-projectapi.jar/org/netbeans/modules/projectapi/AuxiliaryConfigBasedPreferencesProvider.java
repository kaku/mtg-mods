/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Modules
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.MutexException
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.projectapi;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.spi.project.AuxiliaryConfiguration;
import org.netbeans.spi.project.AuxiliaryProperties;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Modules;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.MutexException;
import org.openide.util.RequestProcessor;
import org.openide.xml.XMLUtil;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AuxiliaryConfigBasedPreferencesProvider {
    private static Map<Project, Reference<AuxiliaryConfigBasedPreferencesProvider>> projects2SharedPrefs = new WeakHashMap<Project, Reference<AuxiliaryConfigBasedPreferencesProvider>>();
    private static Map<Project, Reference<AuxiliaryConfigBasedPreferencesProvider>> projects2PrivatePrefs = new WeakHashMap<Project, Reference<AuxiliaryConfigBasedPreferencesProvider>>();
    static final String NAMESPACE = "http://www.netbeans.org/ns/auxiliary-configuration-preferences/1";
    static final String EL_PREFERENCES = "preferences";
    private static final String EL_MODULE = "module";
    private static final String EL_PROPERTY = "property";
    private static final String EL_NODE = "node";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_VALUE = "value";
    private static final String INVALID_KEY_CHARACTERS = "_.";
    private static final RequestProcessor WORKER = new RequestProcessor("AuxiliaryConfigBasedPreferencesProvider worker", 1);
    private static final int AUTOFLUSH_TIMEOUT = 5000;
    private final Project project;
    private final AuxiliaryConfiguration ac;
    private final AuxiliaryProperties ap;
    private final boolean shared;
    private final Map<String, Reference<AuxiliaryConfigBasedPreferences>> module2Preferences = new HashMap<String, Reference<AuxiliaryConfigBasedPreferences>>();
    private Element configRoot;
    private boolean modified;
    private final RequestProcessor.Task autoFlushTask;
    private final Map<String, Map<String, String>> path2Data;
    private final Map<String, Set<String>> path2Removed;
    private final Set<String> removedNodes;
    private final Set<String> createdNodes;

    static synchronized AuxiliaryConfigBasedPreferencesProvider findProvider(Project p, boolean shared) {
        AuxiliaryConfigBasedPreferencesProvider prov;
        Map<Project, Reference<AuxiliaryConfigBasedPreferencesProvider>> target = shared ? projects2SharedPrefs : projects2PrivatePrefs;
        Reference<AuxiliaryConfigBasedPreferencesProvider> provRef = target.get(p);
        AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = prov = provRef != null ? provRef.get() : null;
        if (prov != null) {
            return prov;
        }
        AuxiliaryConfiguration ac = ProjectUtils.getAuxiliaryConfiguration(p);
        assert (p.getLookup() != null);
        AuxiliaryProperties ap = (AuxiliaryProperties)p.getLookup().lookup(AuxiliaryProperties.class);
        prov = new AuxiliaryConfigBasedPreferencesProvider(p, ac, ap, shared);
        target.put(p, new WeakReference<AuxiliaryConfigBasedPreferencesProvider>(prov));
        return prov;
    }

    public static Preferences getPreferences(final Project project, final Class clazz, final boolean shared) {
        return (Preferences)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Preferences>(){

            public Preferences run() {
                AuxiliaryConfigBasedPreferencesProvider provider = AuxiliaryConfigBasedPreferencesProvider.findProvider(project, shared);
                if (provider == null) {
                    return null;
                }
                return provider.findModule(AuxiliaryConfigBasedPreferencesProvider.findCNBForClass(clazz));
            }
        });
    }

    private static String encodeString(String s) {
        StringBuilder result = new StringBuilder();
        for (char c : s.toCharArray()) {
            if ("_.".indexOf(c) == -1) {
                result.append(c);
                continue;
            }
            result.append("_");
            result.append(Integer.toHexString(c));
            result.append("_");
        }
        return result.toString();
    }

    private static String decodeString(String s) {
        StringBuilder result = new StringBuilder();
        String[] parts = s.split("_");
        for (int cntr = 0; cntr < parts.length; cntr += 2) {
            result.append(parts[cntr]);
            if (cntr + 1 >= parts.length) continue;
            result.append((char)Integer.parseInt(parts[cntr + 1], 16));
        }
        return result.toString();
    }

    AuxiliaryConfigBasedPreferencesProvider(Project project, AuxiliaryConfiguration ac, AuxiliaryProperties ap, boolean shared) {
        this.autoFlushTask = WORKER.create(new Runnable(){

            @Override
            public void run() {
                AuxiliaryConfigBasedPreferencesProvider.this.flush();
            }
        });
        this.path2Data = new HashMap<String, Map<String, String>>();
        this.path2Removed = new HashMap<String, Set<String>>();
        this.removedNodes = new HashSet<String>();
        this.createdNodes = new HashSet<String>();
        this.project = project;
        this.ac = ac;
        this.ap = ap;
        this.shared = shared;
        this.loadConfigRoot();
    }

    private void loadConfigRoot() {
        if (this.ac == null) {
            return;
        }
        Element configRootLoc = this.ac.getConfigurationFragment("preferences", "http://www.netbeans.org/ns/auxiliary-configuration-preferences/1", this.shared);
        if (configRootLoc == null) {
            configRootLoc = XMLUtil.createDocument((String)"preferences", (String)"http://www.netbeans.org/ns/auxiliary-configuration-preferences/1", (String)null, (String)null).createElementNS("http://www.netbeans.org/ns/auxiliary-configuration-preferences/1", "preferences");
        }
        this.configRoot = configRootLoc;
    }

    void flush() {
        ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

            public Void run() {
                AuxiliaryConfigBasedPreferencesProvider.this.flushImpl();
                return null;
            }
        });
    }

    private synchronized void flushImpl() {
        Element p;
        Element el;
        if (!this.modified) {
            return;
        }
        boolean domModified = false;
        for (String removedNode : this.removedNodes) {
            if (this.ac != null && (el = this.findRelative(removedNode, false)) != null) {
                el.getParentNode().removeChild(el);
                domModified = true;
            }
            if (this.ap == null) continue;
            String propName = this.toPropertyName(removedNode, "");
            for (String key : this.ap.listKeys(this.shared)) {
                if (!key.startsWith(propName)) continue;
                this.ap.put(key, null, this.shared);
            }
        }
        for (Map.Entry e2 : this.path2Data.entrySet()) {
            if (this.ap != null) {
                for (Map.Entry value : ((Map)e2.getValue()).entrySet()) {
                    this.ap.put(this.toPropertyName((String)e2.getKey(), (String)value.getKey()), (String)value.getValue(), this.shared);
                }
                continue;
            }
            el = this.findRelative((String)e2.getKey(), true);
            if (el == null) continue;
            for (Map.Entry value : ((Map)e2.getValue()).entrySet()) {
                p = AuxiliaryConfigBasedPreferencesProvider.find(el, (String)value.getKey(), "property", true);
                p.setAttribute("value", (String)value.getValue());
            }
            domModified = true;
        }
        for (Map.Entry e : this.path2Removed.entrySet()) {
            if (this.ac != null && (el = this.findRelative((String)e.getKey(), false)) != null) {
                for (String removed : (Set)e.getValue()) {
                    p = AuxiliaryConfigBasedPreferencesProvider.find(el, removed, "property", true);
                    el.removeChild(p);
                }
                domModified = true;
            }
            if (this.ap == null) continue;
            for (String removed : (Set)e.getValue()) {
                this.ap.put(this.toPropertyName((String)e.getKey(), removed), null, this.shared);
            }
        }
        if (domModified) {
            this.ac.putConfigurationFragment(this.configRoot, this.shared);
        }
        try {
            ProjectManager.getDefault().saveProject(this.project);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        this.path2Data.clear();
        this.path2Removed.clear();
        this.removedNodes.clear();
        this.modified = false;
    }

    void sync() {
        ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

            public Void run() {
                AuxiliaryConfigBasedPreferencesProvider.this.syncImpl();
                return null;
            }
        });
    }

    private synchronized void syncImpl() {
        this.loadConfigRoot();
        this.flush();
    }

    private void markModified() {
        this.autoFlushTask.cancel();
        this.autoFlushTask.schedule(5000);
        this.modified = true;
    }

    public static String findCNBForClass(@NonNull Class<?> cls) {
        ModuleInfo owner = Modules.getDefault().ownerOf(cls);
        String absolutePath = owner != null ? owner.getCodeNameBase() : cls.getName().replaceFirst("(^|\\.)[^.]+$", "");
        return absolutePath.replace('.', '-');
    }

    public synchronized Preferences findModule(String moduleName) {
        AuxiliaryConfigBasedPreferences pref;
        Reference<AuxiliaryConfigBasedPreferences> prefRef = this.module2Preferences.get(moduleName);
        AuxiliaryConfigBasedPreferences auxiliaryConfigBasedPreferences = pref = prefRef != null ? prefRef.get() : null;
        if (pref == null) {
            pref = new AuxiliaryConfigBasedPreferences(null, "", moduleName);
            this.module2Preferences.put(moduleName, new WeakReference<AuxiliaryConfigBasedPreferences>(pref));
        }
        return pref;
    }

    private Element findRelative(String path, boolean createIfMissing) {
        if (this.ac == null) {
            return null;
        }
        String[] sep = path.split("/");
        assert (sep.length > 0);
        Element e = AuxiliaryConfigBasedPreferencesProvider.find(this.configRoot, sep[0], "module", createIfMissing);
        for (int cntr = 1; cntr < sep.length && e != null; ++cntr) {
            e = AuxiliaryConfigBasedPreferencesProvider.find(e, sep[cntr], "node", createIfMissing);
        }
        return e;
    }

    private Map<String, String> getData(String path) {
        Map<String, String> data = this.path2Data.get(path);
        if (data == null) {
            data = new HashMap<String, String>();
            this.path2Data.put(path, data);
        }
        return data;
    }

    private Set<String> getRemoved(String path) {
        Set<String> removed = this.path2Removed.get(path);
        if (removed == null) {
            removed = new HashSet<String>();
            this.path2Removed.put(path, removed);
        }
        return removed;
    }

    private void removeNode(String path) {
        this.path2Data.remove(path);
        this.path2Removed.remove(path);
        this.createdNodes.remove(path);
        this.removedNodes.add(path);
    }

    private boolean isRemovedNode(String path) {
        return this.removedNodes.contains(path);
    }

    private static Element find(Element dom, String key, String elementName, boolean createIfMissing) {
        NodeList nl = dom.getChildNodes();
        for (int cntr = 0; cntr < nl.getLength(); ++cntr) {
            Node n = nl.item(cntr);
            if (n.getNodeType() != 1 || !"http://www.netbeans.org/ns/auxiliary-configuration-preferences/1".equals(n.getNamespaceURI()) || !elementName.equals(n.getLocalName()) || !key.equals(((Element)n).getAttribute("name"))) continue;
            return (Element)n;
        }
        if (!createIfMissing) {
            return null;
        }
        Element el = dom.getOwnerDocument().createElementNS("http://www.netbeans.org/ns/auxiliary-configuration-preferences/1", elementName);
        el.setAttribute("name", key);
        dom.appendChild(el);
        return el;
    }

    private String toPropertyName(String path, String propertyName) {
        return AuxiliaryConfigBasedPreferencesProvider.encodeString(path).replace('/', '.') + '.' + AuxiliaryConfigBasedPreferencesProvider.encodeString(propertyName);
    }

    private class AuxiliaryConfigBasedPreferences
    extends AbstractPreferences {
        private final String path;

        public AuxiliaryConfigBasedPreferences(AbstractPreferences parent, String name, String path) {
            super(parent, name);
            this.path = path;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected void putSpi(String key, String value) {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                AuxiliaryConfigBasedPreferencesProvider.this.getData(this.path).put(key, value);
                AuxiliaryConfigBasedPreferencesProvider.this.getRemoved(this.path).remove(key);
                AuxiliaryConfigBasedPreferencesProvider.this.markModified();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected String getSpi(String key) {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                Element p;
                if (AuxiliaryConfigBasedPreferencesProvider.this.getRemoved(this.path).contains(key)) {
                    return null;
                }
                if (AuxiliaryConfigBasedPreferencesProvider.this.getData(this.path).containsKey(key)) {
                    return (String)AuxiliaryConfigBasedPreferencesProvider.this.getData(this.path).get(key);
                }
                if (AuxiliaryConfigBasedPreferencesProvider.this.isRemovedNode(this.path)) {
                    return null;
                }
                if (AuxiliaryConfigBasedPreferencesProvider.this.ap != null) {
                    String keyProp = AuxiliaryConfigBasedPreferencesProvider.this.toPropertyName(this.path, key);
                    String res = AuxiliaryConfigBasedPreferencesProvider.this.ap.get(keyProp, AuxiliaryConfigBasedPreferencesProvider.this.shared);
                    if (res != null) {
                        return res;
                    }
                }
                Element element = p = (p = AuxiliaryConfigBasedPreferencesProvider.this.findRelative(this.path, false)) != null ? AuxiliaryConfigBasedPreferencesProvider.find(p, key, "property", false) : null;
                if (p == null) {
                    return null;
                }
                return p.getAttribute("value");
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected void removeSpi(String key) {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                AuxiliaryConfigBasedPreferencesProvider.this.getData(this.path).remove(key);
                AuxiliaryConfigBasedPreferencesProvider.this.getRemoved(this.path).add(key);
                AuxiliaryConfigBasedPreferencesProvider.this.markModified();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                AuxiliaryConfigBasedPreferencesProvider.this.removeNode(this.path);
                AuxiliaryConfigBasedPreferencesProvider.this.markModified();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected String[] keysSpi() throws BackingStoreException {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                LinkedHashSet<String> result = new LinkedHashSet<String>();
                if (!AuxiliaryConfigBasedPreferencesProvider.this.isRemovedNode(this.path)) {
                    result.addAll(this.list("property"));
                }
                if (AuxiliaryConfigBasedPreferencesProvider.this.ap != null) {
                    String prefix = AuxiliaryConfigBasedPreferencesProvider.this.toPropertyName(this.path, "");
                    for (String key : AuxiliaryConfigBasedPreferencesProvider.this.ap.listKeys(AuxiliaryConfigBasedPreferencesProvider.this.shared)) {
                        String name;
                        if (!key.startsWith(prefix) || (name = key.substring(prefix.length())).length() <= 0 || name.indexOf(46) != -1) continue;
                        result.add(AuxiliaryConfigBasedPreferencesProvider.decodeString(name));
                    }
                }
                result.addAll(AuxiliaryConfigBasedPreferencesProvider.this.getData(this.path).keySet());
                result.removeAll(AuxiliaryConfigBasedPreferencesProvider.this.getRemoved(this.path));
                return result.toArray(new String[0]);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                return this.getChildrenNames().toArray(new String[0]);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        protected AbstractPreferences childSpi(String name) {
            AuxiliaryConfigBasedPreferencesProvider auxiliaryConfigBasedPreferencesProvider = AuxiliaryConfigBasedPreferencesProvider.this;
            synchronized (auxiliaryConfigBasedPreferencesProvider) {
                String nuePath = this.path + "/" + name;
                if (!this.getChildrenNames().contains(name)) {
                    AuxiliaryConfigBasedPreferencesProvider.this.createdNodes.add(nuePath);
                }
                return new AuxiliaryConfigBasedPreferences(this, name, nuePath);
            }
        }

        @Override
        public void sync() throws BackingStoreException {
            AuxiliaryConfigBasedPreferencesProvider.this.sync();
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Should never be called.");
        }

        @Override
        public void flush() throws BackingStoreException {
            AuxiliaryConfigBasedPreferencesProvider.this.flush();
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Should never be called.");
        }

        private Collection<String> getChildrenNames() {
            int slash;
            LinkedHashSet<String> result = new LinkedHashSet<String>();
            if (!AuxiliaryConfigBasedPreferencesProvider.this.isRemovedNode(this.path)) {
                result.addAll(this.list("node"));
            }
            for (String removed : AuxiliaryConfigBasedPreferencesProvider.this.removedNodes) {
                if (!this.path.equals(removed.substring(slash = removed.lastIndexOf(47)))) continue;
                result.remove(removed.substring(slash + 1));
            }
            if (AuxiliaryConfigBasedPreferencesProvider.this.ap != null) {
                String prefix = AuxiliaryConfigBasedPreferencesProvider.this.toPropertyName(this.path, "");
                for (String key : AuxiliaryConfigBasedPreferencesProvider.this.ap.listKeys(AuxiliaryConfigBasedPreferencesProvider.this.shared)) {
                    String name;
                    if (!key.startsWith(prefix) || (name = key.substring(prefix.length())).length() <= 0 || name.indexOf(46) == -1) continue;
                    name = name.substring(0, name.indexOf(46));
                    result.add(AuxiliaryConfigBasedPreferencesProvider.decodeString(name));
                }
            }
            for (String created : AuxiliaryConfigBasedPreferencesProvider.this.createdNodes) {
                if (!this.path.equals(created.substring(slash = created.lastIndexOf(47)))) continue;
                result.add(created.substring(slash + 1));
            }
            return result;
        }

        private Collection<String> list(String elementName) throws DOMException {
            Element dom = AuxiliaryConfigBasedPreferencesProvider.this.findRelative(this.path, false);
            if (dom == null) {
                return Collections.emptyList();
            }
            LinkedList<String> names = new LinkedList<String>();
            NodeList nl = dom.getElementsByTagNameNS("http://www.netbeans.org/ns/auxiliary-configuration-preferences/1", elementName);
            for (int cntr = 0; cntr < nl.getLength(); ++cntr) {
                Node n = nl.item(cntr);
                names.add(((Element)n).getAttribute("name"));
            }
            return names;
        }

        @Override
        public void put(final String key, final String value) {
            ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

                public Void run() {
                    String oldValue = AuxiliaryConfigBasedPreferences.this.getSpi(key);
                    if (value.equals(oldValue)) {
                        return null;
                    }
                    try {
                        AuxiliaryConfigBasedPreferences.this.put(key, value);
                    }
                    catch (IllegalArgumentException iae) {
                        if (iae.getMessage().contains("too long")) {
                            AuxiliaryConfigBasedPreferences.this.putSpi(key, value);
                        }
                        throw iae;
                    }
                    return null;
                }
            });
        }

        @Override
        public String get(final String key, final String def) {
            return (String)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<String>(){

                public String run() {
                    return AuxiliaryConfigBasedPreferences.this.get(key, def);
                }
            });
        }

        @Override
        public void remove(final String key) {
            ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

                public Void run() {
                    AuxiliaryConfigBasedPreferences.this.remove(key);
                    return null;
                }
            });
        }

        @Override
        public void clear() throws BackingStoreException {
            try {
                ProjectManager.mutex().writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Void>(){

                    public Void run() throws BackingStoreException {
                        AuxiliaryConfigBasedPreferences.this.clear();
                        return null;
                    }
                });
            }
            catch (MutexException ex) {
                throw (BackingStoreException)ex.getException();
            }
        }

        @Override
        public String[] keys() throws BackingStoreException {
            try {
                return (String[])ProjectManager.mutex().readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<String[]>(){

                    public String[] run() throws BackingStoreException {
                        return AuxiliaryConfigBasedPreferences.this.keys();
                    }
                });
            }
            catch (MutexException ex) {
                throw (BackingStoreException)ex.getException();
            }
        }

        @Override
        public String[] childrenNames() throws BackingStoreException {
            try {
                return (String[])ProjectManager.mutex().readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<String[]>(){

                    public String[] run() throws BackingStoreException {
                        return AuxiliaryConfigBasedPreferences.this.childrenNames();
                    }
                });
            }
            catch (MutexException ex) {
                throw (BackingStoreException)ex.getException();
            }
        }

        @Override
        public Preferences node(final String path) {
            return (Preferences)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Preferences>(){

                public Preferences run() {
                    return AuxiliaryConfigBasedPreferences.this.node(path);
                }
            });
        }

        @Override
        public boolean nodeExists(final String path) throws BackingStoreException {
            try {
                return (Boolean)ProjectManager.mutex().readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Boolean>(){

                    public Boolean run() throws BackingStoreException {
                        return AuxiliaryConfigBasedPreferences.this.nodeExists(path);
                    }
                });
            }
            catch (MutexException ex) {
                throw (BackingStoreException)ex.getException();
            }
        }

        @Override
        public void removeNode() throws BackingStoreException {
            try {
                ProjectManager.mutex().writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Void>(){

                    public Void run() throws BackingStoreException {
                        AuxiliaryConfigBasedPreferences.this.removeNode();
                        return null;
                    }
                });
            }
            catch (MutexException ex) {
                throw (BackingStoreException)ex.getException();
            }
        }

        @Override
        protected AbstractPreferences getChild(final String nodeName) throws BackingStoreException {
            try {
                return (AbstractPreferences)ProjectManager.mutex().readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<AbstractPreferences>(){

                    public AbstractPreferences run() throws BackingStoreException {
                        return AuxiliaryConfigBasedPreferences.this.getChild(nodeName);
                    }
                });
            }
            catch (MutexException ex) {
                throw (BackingStoreException)ex.getException();
            }
        }

    }

}

