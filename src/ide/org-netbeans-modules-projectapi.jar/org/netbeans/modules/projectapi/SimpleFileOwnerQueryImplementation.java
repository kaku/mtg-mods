/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.projectapi;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.spi.project.FileOwnerQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.util.WeakSet;

public class SimpleFileOwnerQueryImplementation
implements FileOwnerQueryImplementation {
    private static final Logger LOG = Logger.getLogger(SimpleFileOwnerQueryImplementation.class.getName());
    private static final URI UNOWNED_URI = URI.create("http:unowned");
    private final Set<FileObject> warnedAboutBrokenProjects = new WeakSet();
    private Reference<FileObject> lastFoundKey = null;
    private Reference<Project> lastFoundValue = null;
    private static final Map<URI, URI> externalOwners = Collections.synchronizedMap(new HashMap());
    private static final Map<URI, FileObject> deserializedExternalOwners = Collections.synchronizedMap(new HashMap());
    private static boolean externalRootsIncludeNonFolders = false;
    private static final boolean WINDOWS = Utilities.isWindows();

    @Override
    public Project getOwner(URI fileURI) {
        FileObject file;
        URI test = fileURI;
        do {
            file = SimpleFileOwnerQueryImplementation.uri2FileObject(test);
            test = SimpleFileOwnerQueryImplementation.goUp(test);
        } while (file == null && test != null);
        if (file == null) {
            return null;
        }
        return this.getOwner(file);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resetLastFoundReferences() {
        SimpleFileOwnerQueryImplementation simpleFileOwnerQueryImplementation = this;
        synchronized (simpleFileOwnerQueryImplementation) {
            this.lastFoundValue = null;
            this.lastFoundKey = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Project getOwner(FileObject f) {
        while (f != null) {
            FileObject externalOwner;
            URI externalOwnersURI;
            Project p;
            SimpleFileOwnerQueryImplementation simpleFileOwnerQueryImplementation = this;
            synchronized (simpleFileOwnerQueryImplementation) {
                if (this.lastFoundKey != null && this.lastFoundKey.get() == f && (p = this.lastFoundValue.get()) != null) {
                    return p;
                }
            }
            boolean folder = f.isFolder();
            if (folder) {
                try {
                    p = ProjectManager.getDefault().findProject(f);
                }
                catch (IOException e) {
                    if (this.warnedAboutBrokenProjects.add(f)) {
                        LOG.log(Level.FINE, "Cannot load project.", e);
                    }
                    return null;
                }
                if (p != null) {
                    SimpleFileOwnerQueryImplementation e = this;
                    synchronized (e) {
                        this.lastFoundKey = new WeakReference<FileObject>(f);
                        this.lastFoundValue = new WeakReference<Project>(p);
                    }
                    return p;
                }
            }
            if (!externalOwners.isEmpty() && (folder || externalRootsIncludeNonFolders) && (externalOwnersURI = externalOwners.get(f.toURI())) != null) {
                if (externalOwnersURI == UNOWNED_URI) {
                    return FileOwnerQuery.UNOWNED;
                }
                FileObject externalOwner2 = SimpleFileOwnerQueryImplementation.uri2FileObject(externalOwnersURI);
                if (externalOwner2 != null && externalOwner2.isValid()) {
                    try {
                        Project p2 = ProjectManager.getDefault().findProject(externalOwner2);
                        SimpleFileOwnerQueryImplementation simpleFileOwnerQueryImplementation2 = this;
                        synchronized (simpleFileOwnerQueryImplementation2) {
                            this.lastFoundKey = new WeakReference<FileObject>(f);
                            this.lastFoundValue = new WeakReference<Project>(p2);
                        }
                        return p2;
                    }
                    catch (IOException e) {
                        LOG.log(Level.FINE, "Cannot load project.", e);
                        return null;
                    }
                }
            }
            if (!deserializedExternalOwners.isEmpty() && (folder || externalRootsIncludeNonFolders) && (externalOwner = deserializedExternalOwners.get(f.toURI())) != null && externalOwner.isValid()) {
                try {
                    Project p3 = ProjectManager.getDefault().findProject(externalOwner);
                    SimpleFileOwnerQueryImplementation e = this;
                    synchronized (e) {
                        this.lastFoundKey = new WeakReference<FileObject>(f);
                        this.lastFoundValue = new WeakReference<Project>(p3);
                    }
                    return p3;
                }
                catch (IOException e) {
                    LOG.log(Level.FINE, "Cannot load project.", e);
                    return null;
                }
            }
            f = f.getParent();
        }
        return null;
    }

    static void deserialize() {
        try {
            Preferences p = NbPreferences.forModule(SimpleFileOwnerQueryImplementation.class).node("externalOwners");
            for (String name : p.keys()) {
                URL u = new URL(p.get(name, null));
                URI i = new URI(name);
                deserializedExternalOwners.put(i, URLMapper.findFileObject((URL)u));
            }
        }
        catch (Exception ex) {
            LOG.log(Level.INFO, null, ex);
        }
        try {
            NbPreferences.forModule(SimpleFileOwnerQueryImplementation.class).node("externalOwners").removeNode();
        }
        catch (BackingStoreException ex) {
            LOG.log(Level.INFO, null, ex);
        }
    }

    static void serialize() {
        try {
            Preferences p = NbPreferences.forModule(SimpleFileOwnerQueryImplementation.class).node("externalOwners");
            for (URI uri : externalOwners.keySet()) {
                URI ownerURI = externalOwners.get(uri);
                if (ownerURI == UNOWNED_URI) continue;
                p.put(uri.toString(), ownerURI.toString());
            }
            p.sync();
        }
        catch (Exception ex) {
            LOG.log(Level.WARNING, null, ex);
        }
    }

    public static void reset() {
        externalOwners.clear();
    }

    public static void markExternalOwnerTransient(FileObject root, Project owner) {
        SimpleFileOwnerQueryImplementation.markExternalOwnerTransient(root.toURI(), owner);
    }

    public static void markExternalOwnerTransient(URI root, Project owner) {
        externalRootsIncludeNonFolders |= !root.getPath().endsWith("/");
        if (owner != null) {
            FileObject fo = owner.getProjectDirectory();
            externalOwners.put(root, owner == FileOwnerQuery.UNOWNED ? UNOWNED_URI : fo.toURI());
            deserializedExternalOwners.remove(root);
        } else {
            externalOwners.remove(root);
        }
    }

    private static FileObject uri2FileObject(URI u) {
        URL url;
        try {
            url = u.toURL();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            assert (false);
            return null;
        }
        return URLMapper.findFileObject((URL)url);
    }

    private static URI goUp(URI u) {
        String pth;
        URI nue;
        assert (u.isAbsolute());
        assert (u.getFragment() == null);
        assert (u.getQuery() == null);
        String path = u.getPath();
        if (path == null || path.equals("/")) {
            return null;
        }
        String us = u.toString();
        if (us.endsWith("/")) {
            us = us.substring(0, us.length() - 1);
            assert (path.endsWith("/"));
            path = path.substring(0, path.length() - 1);
        }
        int idx = us.lastIndexOf(47);
        assert (idx != -1);
        us = path.lastIndexOf(47) == 0 ? us.substring(0, idx + 1) : us.substring(0, idx);
        try {
            nue = new URI(us);
        }
        catch (URISyntaxException e) {
            throw new AssertionError(e);
        }
        if (WINDOWS && ((pth = nue.getPath()).length() == 3 && pth.endsWith(":") || pth.length() == 1 && pth.endsWith("/"))) {
            return null;
        }
        assert (nue.isAbsolute());
        assert (u.toString().startsWith(nue.toString()));
        return nue;
    }
}

