/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.projectapi;

import javax.swing.event.ChangeListener;
import org.netbeans.spi.project.LookupMerger;

public interface MetaLookupMerger {
    public void probing(Class<?> var1);

    public LookupMerger merger();

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

