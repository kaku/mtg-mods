/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.SharabilityQuery
 *  org.netbeans.api.queries.SharabilityQuery$Sharability
 *  org.netbeans.spi.queries.SharabilityQueryImplementation2
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.projectapi;

import java.net.URI;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.queries.SharabilityQuery;
import org.netbeans.spi.queries.SharabilityQueryImplementation2;
import org.openide.util.Lookup;

public class ProjectSharabilityQuery2
implements SharabilityQueryImplementation2 {
    public SharabilityQuery.Sharability getSharability(URI uri) {
        SharabilityQueryImplementation2 sqi;
        Project p = FileOwnerQuery.getOwner(uri);
        if (p != null && (sqi = (SharabilityQueryImplementation2)p.getLookup().lookup(SharabilityQueryImplementation2.class)) != null) {
            return sqi.getSharability(uri);
        }
        return SharabilityQuery.Sharability.UNKNOWN;
    }
}

