/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.queries.SharabilityQueryImplementation
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.projectapi;

import java.io.File;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.spi.queries.SharabilityQueryImplementation;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public class ProjectSharabilityQuery
implements SharabilityQueryImplementation {
    public int getSharability(File file) {
        SharabilityQueryImplementation sqi;
        Project p = FileOwnerQuery.getOwner(Utilities.toURI((File)file));
        if (p != null && (sqi = (SharabilityQueryImplementation)p.getLookup().lookup(SharabilityQueryImplementation.class)) != null) {
            return sqi.getSharability(file);
        }
        return 0;
    }
}

