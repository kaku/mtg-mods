/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.projectapi;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.modules.projectapi.MetaLookupMerger;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.project.LookupProvider;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class LazyLookupProviders {
    private static final Logger LOG = Logger.getLogger(LazyLookupProviders.class.getName());
    private static final Map<Lookup, ThreadLocal<Member>> INSIDE_LOAD = new WeakHashMap<Lookup, ThreadLocal<Member>>();
    private static final Collection<Member> WARNED = Collections.synchronizedSet(new HashSet());

    private LazyLookupProviders() {
    }

    public static LookupProvider forProjectServiceProvider(final Map<String, Object> attrs) throws ClassNotFoundException {
        return new LookupProvider(){

            @Override
            public Lookup createAdditionalLookup(final Lookup lkp) {
                ProxyLookup result = new ProxyLookup(){
                    Collection<String> serviceNames;
                    final ThreadLocal<Boolean> insideBeforeLookup;
                    final Object LOCK;

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    protected void beforeLookup(Lookup.Template<?> template) {
                        block21 : {
                            this.safeToLoad();
                            Class service = template.getType();
                            Object object = this.LOCK;
                            synchronized (object) {
                                if (this.serviceNames == null || !this.serviceNames.contains(service.getName())) {
                                    return;
                                }
                            }
                            if (Boolean.TRUE.equals(this.insideBeforeLookup.get())) {
                                return;
                            }
                            this.insideBeforeLookup.set(true);
                            boolean doSet = false;
                            try {
                                Object object2 = this.LOCK;
                                synchronized (object2) {
                                    block20 : {
                                        if (this.serviceNames != null) break block20;
                                        return;
                                    }
                                    doSet = true;
                                    this.serviceNames = null;
                                }
                                if (doSet) {
                                    Object instance = LazyLookupProviders.loadPSPInstance((String)attrs.get("class"), (String)attrs.get("method"), lkp);
                                    if (!service.isInstance(instance)) {
                                        throw new ClassCastException("Instance of " + instance.getClass() + " unassignable to " + service);
                                    }
                                    this.setLookups(new Lookup[]{Lookups.singleton((Object)instance)});
                                }
                            }
                            catch (Exception x) {
                                Exceptions.attachMessage((Throwable)x, (String)("while loading from " + attrs));
                                Exceptions.printStackTrace((Throwable)x);
                                if (!doSet) break block21;
                                Object object3 = this.LOCK;
                                synchronized (object3) {
                                    this.serviceNames = Arrays.asList(((String)attrs.get("service")).split(","));
                                }
                            }
                            finally {
                                this.insideBeforeLookup.set(false);
                            }
                        }
                    }

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    private void safeToLoad() {
                        ThreadLocal memberRef;
                        Map map = INSIDE_LOAD;
                        synchronized (map) {
                            memberRef = (ThreadLocal)INSIDE_LOAD.get((Object)lkp);
                        }
                        if (memberRef == null) {
                            return;
                        }
                        Member member = (Member)memberRef.get();
                        if (member != null && WARNED.add(member)) {
                            LOG.log(Level.WARNING, null, new IllegalStateException("may not call Project.getLookup().lookup(...) inside " + member.getName() + " registered under @ProjectServiceProvider"));
                        }
                    }
                };
                LOG.log(Level.FINE, "Additional lookup created: {0} service class: {1} for base lookup: {2}", new Object[]{System.identityHashCode((Object)result), attrs.get("class"), System.identityHashCode((Object)lkp)});
                return result;
            }

            public String toString() {
                return "LazyLookupProviders.LookupProvider[" + (String)attrs.get("service") + "]";
            }

        };
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Object loadPSPInstance(String implName, String methodName, Lookup lkp) throws Exception {
        ThreadLocal member;
        ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        if (loader == null) {
            loader = Thread.currentThread().getContextClassLoader();
        }
        Class clazz = loader.loadClass(implName);
        Map<Lookup, ThreadLocal<Member>> map = INSIDE_LOAD;
        synchronized (map) {
            member = INSIDE_LOAD.get((Object)lkp);
            if (member == null) {
                member = new ThreadLocal();
                INSIDE_LOAD.put(lkp, member);
            }
        }
        if (methodName == null) {
            for (Constructor c : clazz.getConstructors()) {
                Object[] vals = LazyLookupProviders.valuesFor(c.getParameterTypes(), lkp);
                if (vals == null) continue;
                member.set(c);
                try {
                    Object obj = c.newInstance(vals);
                    return obj;
                }
                finally {
                    member.remove();
                }
            }
        } else {
            for (Constructor m : clazz.getMethods()) {
                Object[] vals;
                if (!m.getName().equals(methodName) || (vals = LazyLookupProviders.valuesFor(m.getParameterTypes(), lkp)) == null) continue;
                member.set(m);
                try {
                    Object object = m.invoke(null, vals);
                    return object;
                }
                finally {
                    member.remove();
                }
            }
        }
        throw new RuntimeException(implName + "." + methodName);
    }

    private static Object[] valuesFor(Class[] params, Lookup lkp) {
        if (params.length > 2) {
            return null;
        }
        ArrayList<Object> values = new ArrayList<Object>();
        for (Class param : params) {
            if (param == Lookup.class) {
                values.add((Object)lkp);
                continue;
            }
            if (param == Project.class) {
                Project project = (Project)lkp.lookup(Project.class);
                if (project == null) {
                    throw new IllegalArgumentException("Lookup " + (Object)lkp + " did not contain any Project instance");
                }
                values.add(project);
                continue;
            }
            return null;
        }
        return values.toArray();
    }

    public static MetaLookupMerger forLookupMerger(final Map<String, Object> attrs) throws ClassNotFoundException {
        return new MetaLookupMerger(){
            private final String serviceName;
            private LookupMerger<?> delegate;
            private final ChangeSupport cs;

            @Override
            public void probing(Class<?> service) {
                if (this.delegate == null && service.getName().equals(this.serviceName)) {
                    try {
                        LookupMerger m = (LookupMerger)attrs.get("lookupMergerInstance");
                        if (service != m.getMergeableClass()) {
                            throw new ClassCastException(service + " vs. " + m.getMergeableClass());
                        }
                        this.delegate = m;
                        this.cs.fireChange();
                    }
                    catch (Exception x) {
                        Exceptions.printStackTrace((Throwable)x);
                    }
                }
            }

            @Override
            public LookupMerger merger() {
                return this.delegate;
            }

            @Override
            public void addChangeListener(ChangeListener listener) {
                this.cs.addChangeListener(listener);
                assert (this.cs.hasListeners());
            }

            @Override
            public void removeChangeListener(ChangeListener listener) {
                this.cs.removeChangeListener(listener);
            }

            public String toString() {
                return "MetaLookupMerger[" + this.serviceName + "]";
            }
        };
    }

}

