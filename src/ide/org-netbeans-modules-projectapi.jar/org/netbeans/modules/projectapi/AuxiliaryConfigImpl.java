/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.projectapi;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.spi.project.AuxiliaryConfiguration;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.xml.XMLUtil;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class AuxiliaryConfigImpl
implements AuxiliaryConfiguration {
    private static final Logger LOG = Logger.getLogger(AuxiliaryConfigImpl.class.getName());
    static final String AUX_CONFIG_ATTR_BASE = AuxiliaryConfiguration.class.getName();
    static final String AUX_CONFIG_FILENAME = ".netbeans.xml";
    private final Project project;

    public AuxiliaryConfigImpl(Project proj) {
        this.project = proj;
    }

    @Override
    public Element getConfigurationFragment(final String elementName, final String namespace, final boolean shared) {
        return (Element)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Element>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Element run() {
                block17 : {
                    Element fragment;
                    assert (AuxiliaryConfigImpl.this.project != null);
                    Lookup lookup = AuxiliaryConfigImpl.this.project.getLookup();
                    assert (lookup != null);
                    AuxiliaryConfiguration delegate = (AuxiliaryConfiguration)lookup.lookup(AuxiliaryConfiguration.class);
                    if (delegate != null && (fragment = delegate.getConfigurationFragment(elementName, namespace, shared)) != null) {
                        if (elementName.equals(fragment.getLocalName()) && namespace.equals(fragment.getNamespaceURI())) {
                            return fragment;
                        }
                        LOG.log(Level.INFO, delegate.getClass().getName() + " produced wrong local name or namespace for " + namespace + "#" + elementName + " in " + AuxiliaryConfigImpl.this.project);
                    }
                    FileObject dir = AuxiliaryConfigImpl.this.project.getProjectDirectory();
                    if (shared) {
                        FileObject config = dir.getFileObject(".netbeans.xml");
                        if (config != null) {
                            Element element;
                            InputStream is = config.getInputStream();
                            try {
                                InputSource input = new InputSource(is);
                                input.setSystemId(config.toURL().toString());
                                Element root = XMLUtil.parse((InputSource)input, (boolean)false, (boolean)true, (ErrorHandler)null, (EntityResolver)null).getDocumentElement();
                                element = XMLUtil.findElement((Element)root, (String)elementName, (String)namespace);
                            }
                            catch (Throwable var9_15) {
                                try {
                                    is.close();
                                    throw var9_15;
                                }
                                catch (Exception x) {
                                    LOG.log(Level.INFO, "Cannot parse" + (Object)config, x);
                                }
                            }
                            is.close();
                            return element;
                        }
                    } else {
                        String attrName = AuxiliaryConfigImpl.AUX_CONFIG_ATTR_BASE + "." + namespace + "#" + elementName;
                        Object attr = dir.getAttribute(attrName);
                        if (attr instanceof String) {
                            try {
                                Element fragment2 = XMLUtil.parse((InputSource)new InputSource(new StringReader((String)attr)), (boolean)false, (boolean)true, (ErrorHandler)null, (EntityResolver)null).getDocumentElement();
                                if (elementName.equals(fragment2.getLocalName()) && namespace.equals(fragment2.getNamespaceURI())) {
                                    return fragment2;
                                }
                                LOG.log(Level.INFO, "Value " + attr + " of " + attrName + " on " + (Object)dir + " has the wrong local name or namespace");
                            }
                            catch (SAXException x) {
                                LOG.log(Level.INFO, "Cannot parse value " + attr + " of " + attrName + " on " + (Object)dir + ": " + x.getMessage());
                            }
                            catch (IOException x) {
                                if ($assertionsDisabled) break block17;
                                throw new AssertionError(x);
                            }
                        }
                    }
                }
                return null;
            }
        });
    }

    @Override
    public void putConfigurationFragment(final Element fragment, final boolean shared) throws IllegalArgumentException {
        ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Void run() {
                String elementName = fragment.getLocalName();
                String namespace = fragment.getNamespaceURI();
                if (namespace == null) {
                    throw new IllegalArgumentException();
                }
                AuxiliaryConfiguration delegate = (AuxiliaryConfiguration)AuxiliaryConfigImpl.this.project.getLookup().lookup(AuxiliaryConfiguration.class);
                if (delegate != null) {
                    delegate.putConfigurationFragment(fragment, shared);
                    AuxiliaryConfigImpl.this.removeFallbackImpl(elementName, namespace, shared);
                    return null;
                }
                FileObject dir = AuxiliaryConfigImpl.this.project.getProjectDirectory();
                try {
                    if (shared) {
                        Element root;
                        Element oldFragment;
                        Document doc;
                        FileObject config = dir.getFileObject(".netbeans.xml");
                        if (config != null) {
                            InputStream is = config.getInputStream();
                            try {
                                InputSource input = new InputSource(is);
                                input.setSystemId(config.toURL().toString());
                                doc = XMLUtil.parse((InputSource)input, (boolean)false, (boolean)true, (ErrorHandler)null, (EntityResolver)null);
                            }
                            finally {
                                is.close();
                            }
                        }
                        config = dir.createData(".netbeans.xml");
                        doc = XMLUtil.createDocument((String)"auxiliary-configuration", (String)"http://www.netbeans.org/ns/auxiliary-configuration/1", (String)null, (String)null);
                        if ((oldFragment = XMLUtil.findElement((Element)(root = doc.getDocumentElement()), (String)elementName, (String)namespace)) != null) {
                            root.removeChild(oldFragment);
                        }
                        Node ref = null;
                        NodeList list = root.getChildNodes();
                        for (int i = 0; i < list.getLength(); ++i) {
                            Node node = list.item(i);
                            if (node.getNodeType() != 1) continue;
                            int comparison = node.getNodeName().compareTo(elementName);
                            if (comparison == 0) {
                                comparison = node.getNamespaceURI().compareTo(namespace);
                            }
                            if (comparison <= 0) continue;
                            ref = node;
                            break;
                        }
                        root.insertBefore(root.getOwnerDocument().importNode(fragment, true), ref);
                        OutputStream os = config.getOutputStream();
                        try {
                            XMLUtil.write((Document)doc, (OutputStream)os, (String)"UTF-8");
                        }
                        finally {
                            os.close();
                        }
                    }
                    String attrName = AuxiliaryConfigImpl.AUX_CONFIG_ATTR_BASE + "." + namespace + "#" + elementName;
                    dir.setAttribute(attrName, (Object)AuxiliaryConfigImpl.elementToString(fragment));
                }
                catch (Exception x) {
                    LOG.log(Level.WARNING, "Cannot save configuration to " + (Object)dir, x);
                }
                return null;
            }
        });
    }

    static String elementToString(Element e) throws ParserConfigurationException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        DOMImplementationLS ls = (DOMImplementationLS)doc.getImplementation().getFeature("LS", "3.0");
        assert (ls != null);
        doc.appendChild(doc.importNode(e, true));
        LSSerializer serializer = ls.createLSSerializer();
        serializer.getDomConfig().setParameter("xml-declaration", false);
        return serializer.writeToString(doc);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean removeFallbackImpl(String elementName, String namespace, boolean shared) {
        block14 : {
            FileObject dir = this.project.getProjectDirectory();
            try {
                if (shared) {
                    FileObject config = dir.getFileObject(".netbeans.xml");
                    if (config == null) break block14;
                    try {
                        Document doc;
                        InputStream is = config.getInputStream();
                        try {
                            InputSource input = new InputSource(is);
                            input.setSystemId(config.toURL().toString());
                            doc = XMLUtil.parse((InputSource)input, (boolean)false, (boolean)true, (ErrorHandler)null, (EntityResolver)null);
                        }
                        finally {
                            is.close();
                        }
                        Element root = doc.getDocumentElement();
                        Element toRemove = XMLUtil.findElement((Element)root, (String)elementName, (String)namespace);
                        if (toRemove != null) {
                            root.removeChild(toRemove);
                            if (root.getElementsByTagName("*").getLength() > 0) {
                                OutputStream os = config.getOutputStream();
                                try {
                                    XMLUtil.write((Document)doc, (OutputStream)os, (String)"UTF-8");
                                }
                                finally {
                                    os.close();
                                }
                            }
                            config.delete();
                            return true;
                        }
                        break block14;
                    }
                    catch (SAXException x) {
                        LOG.log(Level.INFO, "Cannot parse" + (Object)config, x);
                        break block14;
                    }
                }
                String attrName = AUX_CONFIG_ATTR_BASE + "." + namespace + "#" + elementName;
                if (dir.getAttribute(attrName) != null) {
                    dir.setAttribute(attrName, (Object)null);
                    return true;
                }
            }
            catch (IOException x) {
                LOG.warning("Cannot remove configuration from " + (Object)dir);
            }
        }
        return false;
    }

    @Override
    public boolean removeConfigurationFragment(final String elementName, final String namespace, final boolean shared) throws IllegalArgumentException {
        return (Boolean)ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            public Boolean run() {
                AuxiliaryConfiguration delegate = (AuxiliaryConfiguration)AuxiliaryConfigImpl.this.project.getLookup().lookup(AuxiliaryConfiguration.class);
                boolean result = false;
                if (delegate != null) {
                    result |= delegate.removeConfigurationFragment(elementName, namespace, shared);
                }
                return result |= AuxiliaryConfigImpl.this.removeFallbackImpl(elementName, namespace, shared);
            }
        });
    }

}

