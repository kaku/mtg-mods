/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 */
package org.netbeans.api.project;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.modules.projectapi.SimpleFileOwnerQueryImplementation;
import org.netbeans.spi.project.FileOwnerQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;

public class FileOwnerQuery {
    private static final Logger LOG = Logger.getLogger(FileOwnerQuery.class.getName());
    private static Lookup.Result<FileOwnerQueryImplementation> implementations;
    private static volatile List<FileOwnerQueryImplementation> cache;
    public static final Project UNOWNED;
    public static final int EXTERNAL_ALGORITHM_TRANSIENT = 0;

    private FileOwnerQuery() {
    }

    public static Project getOwner(FileObject file) {
        if (file == null) {
            throw new NullPointerException("Passed null to FileOwnerQuery.getOwner(FileObject)");
        }
        FileObject archiveRoot = FileUtil.getArchiveFile((FileObject)file);
        if (archiveRoot != null) {
            file = archiveRoot;
        }
        for (FileOwnerQueryImplementation q : FileOwnerQuery.getInstances()) {
            Project p = q.getOwner(file);
            if (p == null) continue;
            LOG.log(Level.FINE, "getOwner({0}) -> {1} @{2} from {3}", new Object[]{file, p, p.hashCode(), q});
            return p == UNOWNED ? null : p;
        }
        LOG.log(Level.FINE, "getOwner({0}) -> nil", (Object)file);
        return null;
    }

    public static Project getOwner(URI uri) {
        if ("jar".equals(uri.getScheme())) {
            try {
                URL jar = FileUtil.getArchiveFile((URL)uri.toURL());
                if (jar != null) {
                    uri = jar.toURI();
                }
            }
            catch (Exception x) {
                LOG.log(Level.INFO, null, x);
            }
        }
        if (!uri.isAbsolute() || uri.isOpaque()) {
            throw new IllegalArgumentException("Bad URI: " + uri);
        }
        for (FileOwnerQueryImplementation q : FileOwnerQuery.getInstances()) {
            Project p = q.getOwner(uri);
            if (p == null) continue;
            LOG.log(Level.FINE, "getOwner({0}) -> {1} from {2}", new Object[]{uri, p, q});
            return p == UNOWNED ? null : p;
        }
        LOG.log(Level.FINE, "getOwner({0}) -> nil", uri);
        return null;
    }

    static void reset() {
        SimpleFileOwnerQueryImplementation.reset();
    }

    public static void markExternalOwner(FileObject root, Project owner, int algorithm) throws IllegalArgumentException {
        LOG.log(Level.FINE, "markExternalOwner({0}, {1}, {2})", new Object[]{root, owner, algorithm});
        switch (algorithm) {
            case 0: {
                SimpleFileOwnerQueryImplementation.markExternalOwnerTransient(root, owner);
                break;
            }
            default: {
                throw new IllegalArgumentException("No such algorithm: " + algorithm);
            }
        }
    }

    public static void markExternalOwner(URI root, Project owner, int algorithm) throws IllegalArgumentException {
        LOG.log(Level.FINE, "markExternalOwner({0}, {1}, {2})", new Object[]{root, owner, algorithm});
        switch (algorithm) {
            case 0: {
                SimpleFileOwnerQueryImplementation.markExternalOwnerTransient(root, owner);
                break;
            }
            default: {
                throw new IllegalArgumentException("No such algorithm: " + algorithm);
            }
        }
    }

    private static List<FileOwnerQueryImplementation> getInstances() {
        List<FileOwnerQueryImplementation> fpRes = cache;
        if (fpRes != null) {
            return fpRes;
        }
        return (List)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<List<FileOwnerQueryImplementation>>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public List<FileOwnerQueryImplementation> run() {
                Class<FileOwnerQuery> class_ = FileOwnerQuery.class;
                synchronized (FileOwnerQuery.class) {
                    ArrayList res;
                    if (implementations == null) {
                        implementations = Lookup.getDefault().lookupResult(FileOwnerQueryImplementation.class);
                        implementations.addLookupListener(new LookupListener(){

                            public void resultChanged(LookupEvent ev) {
                                cache = null;
                            }
                        });
                    }
                    if ((res = cache) == null) {
                        res = new ArrayList(implementations.allInstances());
                        cache = res;
                    }
                    // ** MonitorExit[var1_1] (shouldn't be in output)
                    return res;
                }
            }

        });
    }

    static {
        UNOWNED = new Project(){

            @Override
            public FileObject getProjectDirectory() {
                return FileUtil.createMemoryFileSystem().getRoot();
            }

            @Override
            public Lookup getLookup() {
                return Lookup.EMPTY;
            }

            public String toString() {
                return "UNOWNED";
            }
        };
    }

}

