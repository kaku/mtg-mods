/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.project;

import javax.swing.event.ChangeListener;
import org.netbeans.api.project.SourceGroup;

public interface Sources {
    public static final String TYPE_GENERIC = "generic";

    public SourceGroup[] getSourceGroups(String var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

