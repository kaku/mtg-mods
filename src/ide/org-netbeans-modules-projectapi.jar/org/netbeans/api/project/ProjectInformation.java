/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.project;

import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import org.netbeans.api.project.Project;

public interface ProjectInformation {
    public static final String PROP_NAME = "name";
    public static final String PROP_DISPLAY_NAME = "displayName";
    public static final String PROP_ICON = "icon";

    public String getName();

    public String getDisplayName();

    public Icon getIcon();

    public Project getProject();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

