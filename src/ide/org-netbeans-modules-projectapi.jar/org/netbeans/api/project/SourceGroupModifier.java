/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.api.project;

import org.netbeans.api.project.Project;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.spi.project.SourceGroupModifierImplementation;
import org.openide.util.Lookup;

public final class SourceGroupModifier {
    private SourceGroupModifier() {
    }

    public static final SourceGroup createSourceGroup(Project project, String type, String hint) {
        assert (project != null);
        SourceGroupModifierImplementation impl = (SourceGroupModifierImplementation)project.getLookup().lookup(SourceGroupModifierImplementation.class);
        if (impl == null) {
            return null;
        }
        return impl.createSourceGroup(type, hint);
    }

    public static final Future createSourceGroupFuture(Project project, String type, String hint) {
        assert (project != null);
        SourceGroupModifierImplementation impl = (SourceGroupModifierImplementation)project.getLookup().lookup(SourceGroupModifierImplementation.class);
        if (impl == null) {
            return null;
        }
        if (impl.canCreateSourceGroup(type, hint)) {
            return new Future(impl, type, hint);
        }
        return null;
    }

    public static final class Future {
        SourceGroupModifierImplementation impl;
        private String hint;
        private String type;

        private Future(SourceGroupModifierImplementation im, String type, String hint) {
            this.impl = im;
            this.hint = hint;
            this.type = type;
        }

        public final SourceGroup createSourceGroup() {
            return this.impl.createSourceGroup(this.type, this.hint);
        }

        public String getType() {
            return this.type;
        }

        public String getHint() {
            return this.hint;
        }
    }

}

