/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.project;

import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import org.openide.filesystems.FileObject;

public interface SourceGroup {
    public static final String PROP_CONTAINERSHIP = "containership";

    public FileObject getRootFolder();

    public String getName();

    public String getDisplayName();

    public Icon getIcon(boolean var1);

    public boolean contains(FileObject var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

