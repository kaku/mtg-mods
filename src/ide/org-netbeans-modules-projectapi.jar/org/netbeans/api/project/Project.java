/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.netbeans.api.project;

import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public interface Project
extends Lookup.Provider {
    public FileObject getProjectDirectory();

    public Lookup getLookup();
}

