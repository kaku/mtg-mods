/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.MutexException
 *  org.openide.util.Union2
 *  org.openide.util.WeakSet
 */
package org.netbeans.api.project;

import java.awt.EventQueue;
import java.io.IOException;
import java.lang.ref.Reference;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.Icon;
import org.netbeans.api.project.Project;
import org.netbeans.modules.projectapi.SimpleFileOwnerQueryImplementation;
import org.netbeans.modules.projectapi.TimedWeakReference;
import org.netbeans.spi.project.FileOwnerQueryImplementation;
import org.netbeans.spi.project.ProjectFactory;
import org.netbeans.spi.project.ProjectFactory2;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.MutexException;
import org.openide.util.Union2;
import org.openide.util.WeakSet;

public final class ProjectManager {
    private static final Logger LOG = Logger.getLogger(ProjectManager.class.getName());
    private static final Logger TIMERS = Logger.getLogger("TIMER.projects");
    private static final Lookup.Result<ProjectFactory> factories = Lookup.getDefault().lookupResult(ProjectFactory.class);
    private static final ProjectManager DEFAULT = new ProjectManager();
    private static final Mutex MUTEX = new Mutex();
    private final Map<FileObject, Union2<Reference<Project>, LoadStatus>> dir2Proj = new WeakHashMap<FileObject, Union2<Reference<Project>, LoadStatus>>();
    private final Set<Project> modifiedProjects = new HashSet<Project>();
    private final Set<Project> removedProjects = Collections.synchronizedSet(new WeakSet());
    private final Map<Project, ProjectFactory> proj2Factory = Collections.synchronizedMap(new WeakHashMap());
    private final FileChangeListener projectDeletionListener;
    private ThreadLocal<Set<FileObject>> loadingThread;

    private ProjectManager() {
        this.projectDeletionListener = new ProjectDeletionListener();
        this.loadingThread = new ThreadLocal();
        factories.addLookupListener(new LookupListener(){

            public void resultChanged(LookupEvent e) {
                ProjectManager.this.clearNonProjectCache();
            }
        });
    }

    public static ProjectManager getDefault() {
        return DEFAULT;
    }

    public static Mutex mutex() {
        return MUTEX;
    }

    void reset() {
        this.dir2Proj.clear();
        this.modifiedProjects.clear();
        this.proj2Factory.clear();
        this.removedProjects.clear();
    }

    public Project findProject(final FileObject projectDirectory) throws IOException, IllegalArgumentException {
        if (projectDirectory == null) {
            throw new IllegalArgumentException("Attempted to pass a null directory to findProject");
        }
        if (!projectDirectory.isFolder()) {
            throw new IllegalArgumentException("Attempted to pass a non-directory to findProject: " + (Object)projectDirectory);
        }
        try {
            return (Project)ProjectManager.mutex().readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Project>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 * Loose catch block
                 * Enabled aggressive block sorting
                 * Enabled unnecessary exception pruning
                 * Enabled aggressive exception aggregation
                 * Converted monitor instructions to comments
                 * Lifted jumps to return sites
                 */
                public Project run() throws IOException {
                    try {
                        boolean wasSomeSuchProject;
                        boolean resetLP;
                        block35 : {
                            Object ldng;
                            Union2 o;
                            Map map = ProjectManager.this.dir2Proj;
                            // MONITORENTER : map
                            do {
                                if (!LoadStatus.LOADING_PROJECT.is(o = (Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory))) continue;
                                try {
                                    ldng = (HashSet<FileObject>)ProjectManager.this.loadingThread.get();
                                    if (ldng != null && ldng.contains((Object)projectDirectory)) {
                                        throw new IllegalStateException("Attempt to call ProjectManager.findProject within the body of ProjectFactory.loadProject (hint: try using ProjectManager.mutex().postWriteRequest(...) within the body of your Project's constructor to prevent this)");
                                    }
                                    LOG.log(Level.FINE, "findProject({0}) in {1}: waiting for LOADING_PROJECT...", new Object[]{projectDirectory, Thread.currentThread().getName()});
                                    if (LOG.isLoggable(Level.FINE) && EventQueue.isDispatchThread()) {
                                        LOG.log(Level.WARNING, "loading " + (Object)projectDirectory, new IllegalStateException("trying to load a prpject from EQ"));
                                    }
                                    ProjectManager.this.dir2Proj.wait();
                                    LOG.log(Level.FINE, "findProject({0}) in {1}: ...done waiting for LOADING_PROJECT", new Object[]{projectDirectory, Thread.currentThread().getName()});
                                    continue;
                                }
                                catch (InterruptedException e) {
                                    LOG.log(Level.INFO, null, e);
                                    // MONITOREXIT : map
                                    return null;
                                }
                            } while (LoadStatus.LOADING_PROJECT.is(o));
                            assert (!LoadStatus.LOADING_PROJECT.is(o));
                            wasSomeSuchProject = LoadStatus.SOME_SUCH_PROJECT.is(o);
                            if (LoadStatus.NO_SUCH_PROJECT.is(o)) {
                                LOG.log(Level.FINE, "findProject({0}) in {1}: NO_SUCH_PROJECT", new Object[]{projectDirectory, Thread.currentThread().getName()});
                                // MONITOREXIT : map
                                return null;
                            }
                            if (o != null && !LoadStatus.SOME_SUCH_PROJECT.is(o)) {
                                Project p = (Project)((Reference)o.first()).get();
                                if (p != null) {
                                    LOG.log(Level.FINE, "findProject({0}) in {1}: cached project @{2}", new Object[]{projectDirectory, Thread.currentThread().getName(), p.hashCode()});
                                    // MONITOREXIT : map
                                    return p;
                                }
                                LOG.log(Level.FINE, "findProject({0}) in {1}: null project reference", new Object[]{projectDirectory, Thread.currentThread().getName()});
                            } else {
                                LOG.log(Level.FINE, "findProject({0} in {1}: no entries among {2}", new Object[]{projectDirectory, Thread.currentThread().getName(), ProjectManager.this.dir2Proj});
                            }
                            ProjectManager.this.dir2Proj.put(projectDirectory, LoadStatus.LOADING_PROJECT.wrap());
                            ldng = (Set)ProjectManager.this.loadingThread.get();
                            if (ldng == null) {
                                ldng = new HashSet<FileObject>();
                                ProjectManager.this.loadingThread.set(ldng);
                            }
                            ldng.add((FileObject)projectDirectory);
                            LOG.log(Level.FINE, "findProject({0}) in {1}: may load new project...", new Object[]{projectDirectory, Thread.currentThread().getName()});
                            // MONITOREXIT : map
                            resetLP = false;
                            Project p = ProjectManager.this.createProject(projectDirectory);
                            ldng = ProjectManager.this.dir2Proj;
                            // MONITORENTER : ldng
                            ProjectManager.this.dir2Proj.notifyAll();
                            if (p == null) break block35;
                            LOG.log(Level.FINE, "findProject({0}) in {1}: created new project @{2}", new Object[]{projectDirectory, Thread.currentThread().getName(), p.hashCode()});
                            projectDirectory.addFileChangeListener(ProjectManager.this.projectDeletionListener);
                            ProjectManager.this.dir2Proj.put(projectDirectory, Union2.createFirst(new TimedWeakReference<Project>(p)));
                            resetLP = true;
                            Project project = p;
                            // MONITOREXIT : ldng
                            ((Set)ProjectManager.this.loadingThread.get()).remove((Object)projectDirectory);
                            if (resetLP) return project;
                            LOG.log(Level.FINE, "findProject({0}) in {1}: cleaning up after error", new Object[]{projectDirectory, Thread.currentThread().getName()});
                            Map map2 = ProjectManager.this.dir2Proj;
                            // MONITORENTER : map2
                            assert (LoadStatus.LOADING_PROJECT.is((Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory)));
                            ProjectManager.this.dir2Proj.remove((Object)projectDirectory);
                            ProjectManager.this.dir2Proj.notifyAll();
                            // MONITOREXIT : map2
                            return project;
                        }
                        ProjectManager.this.dir2Proj.put(projectDirectory, LoadStatus.NO_SUCH_PROJECT.wrap());
                        resetLP = true;
                        if (wasSomeSuchProject) {
                            LOG.log(Level.FINE, "Directory {0} was initially claimed to be a project folder but really was not", FileUtil.getFileDisplayName((FileObject)projectDirectory));
                        }
                        Project project = null;
                        // MONITOREXIT : ldng
                        ((Set)ProjectManager.this.loadingThread.get()).remove((Object)projectDirectory);
                        if (resetLP) return project;
                        {
                            catch (IOException e) {
                                LOG.log(Level.FINE, "findProject({0}) in {1}: error loading project: {2}", new Object[]{projectDirectory, Thread.currentThread().getName(), e});
                                throw e;
                            }
                        }
                        LOG.log(Level.FINE, "findProject({0}) in {1}: cleaning up after error", new Object[]{projectDirectory, Thread.currentThread().getName()});
                        Map map = ProjectManager.this.dir2Proj;
                        // MONITORENTER : map
                        assert (LoadStatus.LOADING_PROJECT.is((Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory)));
                        ProjectManager.this.dir2Proj.remove((Object)projectDirectory);
                        ProjectManager.this.dir2Proj.notifyAll();
                        // MONITOREXIT : map
                        return project;
                        catch (Throwable throwable) {
                            ((Set)ProjectManager.this.loadingThread.get()).remove((Object)projectDirectory);
                            if (resetLP) throw throwable;
                            LOG.log(Level.FINE, "findProject({0}) in {1}: cleaning up after error", new Object[]{projectDirectory, Thread.currentThread().getName()});
                            Map map3 = ProjectManager.this.dir2Proj;
                            // MONITORENTER : map3
                            assert (LoadStatus.LOADING_PROJECT.is((Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory)));
                            ProjectManager.this.dir2Proj.remove((Object)projectDirectory);
                            ProjectManager.this.dir2Proj.notifyAll();
                            // MONITOREXIT : map3
                            throw throwable;
                        }
                    }
                    catch (Error e) {
                        LOG.log(Level.FINE, null, e);
                        throw e;
                    }
                    catch (RuntimeException e) {
                        LOG.log(Level.FINE, null, e);
                        throw e;
                    }
                    catch (IOException e) {
                        LOG.log(Level.FINE, null, e);
                        throw e;
                    }
                }
            });
        }
        catch (MutexException e) {
            throw (IOException)e.getException();
        }
    }

    private Project createProject(FileObject dir) throws IOException {
        assert (dir != null);
        assert (dir.isFolder());
        assert (ProjectManager.mutex().isReadAccess());
        ProjectStateImpl state = new ProjectStateImpl();
        for (ProjectFactory factory : factories.allInstances()) {
            Project p = factory.loadProject(dir, state);
            if (p == null) continue;
            if (TIMERS.isLoggable(Level.FINE)) {
                LogRecord rec = new LogRecord(Level.FINE, "Project");
                rec.setParameters(new Object[]{p});
                TIMERS.log(rec);
            }
            this.proj2Factory.put(p, factory);
            state.attach(p);
            return p;
        }
        return null;
    }

    public boolean isProject(FileObject projectDirectory) throws IllegalArgumentException {
        return this.isProject2(projectDirectory, false) != null;
    }

    public Result isProject2(FileObject projectDirectory) throws IllegalArgumentException {
        return this.isProject2(projectDirectory, true);
    }

    private Result isProject2(final FileObject projectDirectory, final boolean preferResult) throws IllegalArgumentException {
        if (projectDirectory == null) {
            throw new IllegalArgumentException("Attempted to pass a null directory to isProject");
        }
        if (!projectDirectory.isFolder()) {
            if (projectDirectory.isValid()) {
                throw new IllegalArgumentException("Attempted to pass a non-directory to isProject: " + (Object)projectDirectory);
            }
            return null;
        }
        return (Result)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Result>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             * Enabled aggressive block sorting
             * Enabled unnecessary exception pruning
             * Enabled aggressive exception aggregation
             * Converted monitor instructions to comments
             * Lifted jumps to return sites
             */
            public Result run() {
                boolean resetLP;
                block24 : {
                    Result result;
                    Union2 o;
                    Map map = ProjectManager.this.dir2Proj;
                    // MONITORENTER : map
                    do {
                        if (!LoadStatus.LOADING_PROJECT.is(o = (Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory))) continue;
                        if (EventQueue.isDispatchThread()) {
                            // MONITOREXIT : map
                            return new Result(null);
                        }
                        try {
                            ProjectManager.this.dir2Proj.wait();
                            continue;
                        }
                        catch (InterruptedException e) {
                            LOG.log(Level.INFO, null, e);
                            // MONITOREXIT : map
                            return null;
                        }
                    } while (LoadStatus.LOADING_PROJECT.is(o));
                    assert (!LoadStatus.LOADING_PROJECT.is(o));
                    if (LoadStatus.NO_SUCH_PROJECT.is(o)) {
                        // MONITOREXIT : map
                        return null;
                    }
                    if (o != null) {
                        // MONITOREXIT : map
                        return ProjectManager.this.checkForProject(projectDirectory, preferResult);
                    }
                    ProjectManager.this.dir2Proj.put(projectDirectory, LoadStatus.LOADING_PROJECT.wrap());
                    // MONITOREXIT : map
                    resetLP = false;
                    try {
                        Result p = ProjectManager.this.checkForProject(projectDirectory, preferResult);
                        Map e = ProjectManager.this.dir2Proj;
                        // MONITORENTER : e
                        resetLP = true;
                        ProjectManager.this.dir2Proj.notifyAll();
                        if (p == null) break block24;
                        ProjectManager.this.dir2Proj.put(projectDirectory, LoadStatus.SOME_SUCH_PROJECT.wrap());
                        result = p;
                        // MONITOREXIT : e
                        if (resetLP) return result;
                    }
                    catch (Throwable var9_10) {
                        if (resetLP) throw var9_10;
                        Map map2 = ProjectManager.this.dir2Proj;
                        // MONITORENTER : map2
                        assert (LoadStatus.LOADING_PROJECT.is((Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory)));
                        ProjectManager.this.dir2Proj.remove((Object)projectDirectory);
                        // MONITOREXIT : map2
                        throw var9_10;
                    }
                    Map map3 = ProjectManager.this.dir2Proj;
                    // MONITORENTER : map3
                    assert (LoadStatus.LOADING_PROJECT.is((Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory)));
                    ProjectManager.this.dir2Proj.remove((Object)projectDirectory);
                    // MONITOREXIT : map3
                    return result;
                }
                ProjectManager.this.dir2Proj.put(projectDirectory, LoadStatus.NO_SUCH_PROJECT.wrap());
                Result result = null;
                // MONITOREXIT : e
                if (resetLP) return result;
                Map map = ProjectManager.this.dir2Proj;
                // MONITORENTER : map
                assert (LoadStatus.LOADING_PROJECT.is((Union2)ProjectManager.this.dir2Proj.get((Object)projectDirectory)));
                ProjectManager.this.dir2Proj.remove((Object)projectDirectory);
                // MONITOREXIT : map
                return result;
            }
        });
    }

    private Result checkForProject(FileObject dir, boolean preferResult) {
        assert (dir != null);
        assert (dir.isFolder());
        assert (ProjectManager.mutex().isReadAccess());
        for (ProjectFactory factory : factories.allInstances()) {
            if (factory instanceof ProjectFactory2 && preferResult) {
                Result res = ((ProjectFactory2)factory).isProject2(dir);
                if (res == null) continue;
                return res;
            }
            if (!factory.isProject(dir)) continue;
            return new Result(null);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clearNonProjectCache() {
        Map<FileObject, Union2<Reference<Project>, LoadStatus>> map = this.dir2Proj;
        synchronized (map) {
            this.dir2Proj.values().removeAll(Arrays.asList(new Object[]{LoadStatus.NO_SUCH_PROJECT.wrap(), LoadStatus.SOME_SUCH_PROJECT.wrap()}));
        }
    }

    private void resetSimpleFileOwnerQuery() {
        Collection col = Lookup.getDefault().lookupAll(FileOwnerQueryImplementation.class);
        for (FileOwnerQueryImplementation impl : col) {
            if (!(impl instanceof SimpleFileOwnerQueryImplementation)) continue;
            ((SimpleFileOwnerQueryImplementation)impl).resetLastFoundReferences();
        }
    }

    public Set<Project> getModifiedProjects() {
        return (Set)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Set<Project>>(){

            public Set<Project> run() {
                return new HashSet<Project>(ProjectManager.this.modifiedProjects);
            }
        });
    }

    public boolean isModified(final Project p) {
        return (Boolean)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Boolean run() {
                Map map = ProjectManager.this.dir2Proj;
                synchronized (map) {
                    if (!ProjectManager.this.proj2Factory.containsKey(p)) {
                        LOG.log(Level.WARNING, "Project {0} was already deleted", p);
                    }
                }
                return ProjectManager.this.modifiedProjects.contains(p);
            }
        });
    }

    public void saveProject(final Project p) throws IOException {
        try {
            ProjectManager.mutex().writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Void>(){

                public Void run() throws IOException {
                    if (ProjectManager.this.removedProjects.contains(p)) {
                        return null;
                    }
                    if (ProjectManager.this.modifiedProjects.contains(p)) {
                        ProjectFactory f = (ProjectFactory)ProjectManager.this.proj2Factory.get(p);
                        if (f != null) {
                            f.saveProject(p);
                            LOG.log(Level.FINE, "saveProject({0})", (Object)p.getProjectDirectory());
                        } else {
                            LOG.log(Level.WARNING, "Project {0} was already deleted", p);
                        }
                        ProjectManager.this.modifiedProjects.remove(p);
                    }
                    return null;
                }
            });
        }
        catch (MutexException e) {
            if (!p.getProjectDirectory().canWrite()) {
                throw new IOException("Project folder is not writeable.");
            }
            throw (IOException)e.getException();
        }
    }

    public void saveAllProjects() throws IOException {
        try {
            ProjectManager.mutex().writeAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Void>(){

                public Void run() throws IOException {
                    Iterator it = ProjectManager.this.modifiedProjects.iterator();
                    while (it.hasNext()) {
                        Project p = (Project)it.next();
                        ProjectFactory f = (ProjectFactory)ProjectManager.this.proj2Factory.get(p);
                        if (f != null) {
                            f.saveProject(p);
                            LOG.log(Level.FINE, "saveProject({0})", (Object)p.getProjectDirectory());
                        } else {
                            LOG.log(Level.WARNING, "Project {0} was already deleted", p);
                        }
                        it.remove();
                    }
                    return null;
                }
            });
        }
        catch (MutexException e) {
            throw (IOException)e.getException();
        }
    }

    public boolean isValid(final Project p) {
        return (Boolean)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public Boolean run() {
                Map map = ProjectManager.this.dir2Proj;
                synchronized (map) {
                    return ProjectManager.this.proj2Factory.containsKey(p);
                }
            }
        });
    }

    public static final class Result {
        private Icon icon;
        private final String displayName;
        private final String projectType;

        public Result(Icon icon) {
            this(null, null, icon);
        }

        public Result(String displayName, String projectType, Icon icon) {
            this.icon = icon;
            this.displayName = displayName;
            this.projectType = projectType;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getProjectType() {
            return this.projectType;
        }

        public Icon getIcon() {
            return this.icon;
        }
    }

    private final class ProjectDeletionListener
    extends FileChangeAdapter {
        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void fileDeleted(FileEvent fe) {
            Map map = ProjectManager.this.dir2Proj;
            synchronized (map) {
                LOG.log(Level.FINE, "deleted: {0}", (Object)fe.getFile());
                ProjectManager.this.dir2Proj.remove((Object)fe.getFile());
                ProjectManager.this.resetSimpleFileOwnerQuery();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void fileRenamed(FileRenameEvent fe) {
            Map map = ProjectManager.this.dir2Proj;
            synchronized (map) {
                LOG.log(Level.FINE, "renamed: {0}", (Object)fe.getFile());
                ProjectManager.this.dir2Proj.remove((Object)fe.getFile());
                ProjectManager.this.resetSimpleFileOwnerQuery();
            }
        }
    }

    private final class ProjectStateImpl
    implements ProjectState {
        private Project p;

        private ProjectStateImpl() {
        }

        void attach(Project p) {
            assert (p != null);
            assert (this.p == null);
            this.p = p;
        }

        @Override
        public void markModified() {
            assert (this.p != null);
            LOG.log(Level.FINE, "markModified({0})", (Object)this.p.getProjectDirectory());
            ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

                public Void run() {
                    if (ProjectManager.this.proj2Factory.containsKey(ProjectStateImpl.this.p)) {
                        ProjectManager.this.modifiedProjects.add(ProjectStateImpl.this.p);
                    } else {
                        LOG.log(Level.WARNING, "An attempt to call ProjectState.markModified on an unknown project: {0}", (Object)ProjectStateImpl.this.p.getProjectDirectory());
                    }
                    return null;
                }
            });
        }

        @Override
        public void notifyDeleted() throws IllegalStateException {
            assert (this.p != null);
            final FileObject dir = this.p.getProjectDirectory();
            LOG.log(Level.FINE, "notifyDeleted: {0}", (Object)dir);
            ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Void>(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public Void run() {
                    Map map = ProjectManager.this.dir2Proj;
                    synchronized (map) {
                        Union2 o = (Union2)ProjectManager.this.dir2Proj.get((Object)dir);
                        if (o != null && o.hasFirst() && ((Reference)o.first()).get() == ProjectStateImpl.this.p) {
                            ProjectManager.this.dir2Proj.remove((Object)dir);
                        } else {
                            LOG.log(Level.FINE, "notifyDeleted skipping dir2Proj update since {0} @{1} != {2}", new Object[]{ProjectStateImpl.this.p, ProjectStateImpl.this.p.hashCode(), o});
                        }
                    }
                    ProjectManager.this.proj2Factory.remove(ProjectStateImpl.this.p);
                    ProjectManager.this.modifiedProjects.remove(ProjectStateImpl.this.p);
                    if (!ProjectManager.this.removedProjects.add(ProjectStateImpl.this.p)) {
                        LOG.log(Level.WARNING, "An attempt to call notifyDeleted more than once. Project: {0}", (Object)dir);
                    }
                    ProjectManager.this.resetSimpleFileOwnerQuery();
                    return null;
                }
            });
        }

    }

    private static enum LoadStatus {
        NO_SUCH_PROJECT,
        SOME_SUCH_PROJECT,
        LOADING_PROJECT;
        

        private LoadStatus() {
        }

        public boolean is(Union2<Reference<Project>, LoadStatus> o) {
            return o != null && o.hasSecond() && o.second() == this;
        }

        public Union2<Reference<Project>, LoadStatus> wrap() {
            return Union2.createSecond((Object)((Object)this));
        }
    }

}

