/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 */
package org.netbeans.api.project;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.Icon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.Sources;
import org.netbeans.modules.projectapi.AuxiliaryConfigBasedPreferencesProvider;
import org.netbeans.modules.projectapi.AuxiliaryConfigImpl;
import org.netbeans.spi.project.AuxiliaryConfiguration;
import org.netbeans.spi.project.CacheDirectoryProvider;
import org.netbeans.spi.project.DependencyProjectProvider;
import org.netbeans.spi.project.ProjectContainerProvider;
import org.netbeans.spi.project.ProjectIconAnnotator;
import org.netbeans.spi.project.SubprojectProvider;
import org.netbeans.spi.project.support.GenericSources;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Mutex;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;

public class ProjectUtils {
    private static final Logger LOG = Logger.getLogger(ProjectUtils.class.getName());

    private ProjectUtils() {
    }

    public static ProjectInformation getInformation(@NonNull Project p) {
        Lookup l = p.getLookup();
        ProjectInformation pi = (ProjectInformation)l.lookup(ProjectInformation.class);
        return new AnnotateIconProxyProjectInformation(pi != null ? pi : new BasicInformation(p));
    }

    public static Sources getSources(@NonNull Project p) {
        Lookup l = p.getLookup();
        Sources s = (Sources)l.lookup(Sources.class);
        if (s != null) {
            return s;
        }
        return GenericSources.genericOnly(p);
    }

    public static boolean hasSubprojectCycles(final Project master, final Project candidate) {
        return (Boolean)ProjectManager.mutex().readAccess((Mutex.Action)new Mutex.Action<Boolean>(){

            public Boolean run() {
                return ProjectUtils.visit(new HashMap(), master, master, candidate);
            }
        });
    }

    public static Set<Project> getDependencyProjects(@NonNull Project root, boolean recursive) {
        DependencyProjectProvider prov = (DependencyProjectProvider)root.getLookup().lookup(DependencyProjectProvider.class);
        if (prov != null) {
            HashSet<Project> toRet = new HashSet<Project>();
            DependencyProjectProvider.Result res = prov.getDependencyProjects();
            toRet.addAll(res.getProjects());
            if (recursive && !res.isRecursive()) {
                for (Project p : res.getProjects()) {
                    Set<Project> subs = ProjectUtils.getDependencyProjects(p, recursive);
                    if (subs == null) continue;
                    toRet.addAll(subs);
                }
            }
            return toRet;
        }
        return null;
    }

    public static Set<Project> getContainedProjects(@NonNull Project root, boolean recursive) {
        ProjectContainerProvider prov = (ProjectContainerProvider)root.getLookup().lookup(ProjectContainerProvider.class);
        if (prov != null) {
            HashSet<Project> toRet = new HashSet<Project>();
            ProjectContainerProvider.Result res = prov.getContainedProjects();
            toRet.addAll(res.getProjects());
            if (recursive && !res.isRecursive()) {
                for (Project p : res.getProjects()) {
                    Set<Project> subs = ProjectUtils.getContainedProjects(p, recursive);
                    if (subs == null) continue;
                    toRet.addAll(subs);
                }
            }
            return toRet;
        }
        return null;
    }

    public static Preferences getPreferences(@NonNull Project project, @NonNull Class clazz, boolean shared) {
        Parameters.notNull((CharSequence)"project", (Object)project);
        Parameters.notNull((CharSequence)"clazz", (Object)clazz);
        return AuxiliaryConfigBasedPreferencesProvider.getPreferences(project, clazz, shared);
    }

    private static boolean visit(@NonNull Map<Project, Boolean> encountered, @NonNull Project curr, Project master, @NullAllowed Project candidate) {
        if (encountered.containsKey(curr)) {
            if (encountered.get(curr).booleanValue()) {
                return false;
            }
            LOG.log(Level.FINE, "Encountered cycle in {0} from {1} at {2} via {3}", new Object[]{master, candidate, curr, encountered});
            return true;
        }
        encountered.put(curr, false);
        SubprojectProvider spp = (SubprojectProvider)curr.getLookup().lookup(SubprojectProvider.class);
        if (spp != null) {
            Set<? extends Project> subprojects = spp.getSubprojects();
            LOG.log(Level.FINEST, "Found subprojects {0} from {1}", new Object[]{subprojects, curr});
            for (Project child : subprojects) {
                if (ProjectUtils.visit(encountered, child, master, candidate)) {
                    return true;
                }
                if (candidate != child) continue;
                candidate = null;
            }
        }
        if (candidate != null && curr == master && ProjectUtils.visit(encountered, candidate, master, candidate)) {
            return true;
        }
        assert (!encountered.get(curr).booleanValue());
        encountered.put(curr, true);
        return false;
    }

    public static AuxiliaryConfiguration getAuxiliaryConfiguration(@NonNull Project project) {
        Parameters.notNull((CharSequence)"project", (Object)project);
        return new AuxiliaryConfigImpl(project);
    }

    public static FileObject getCacheDirectory(@NonNull Project project, @NonNull Class<?> owner) throws IOException {
        CacheDirectoryProvider cdp = (CacheDirectoryProvider)project.getLookup().lookup(CacheDirectoryProvider.class);
        FileObject d = cdp != null ? cdp.getCacheDirectory() : FileUtil.createFolder((FileObject)FileUtil.getConfigRoot(), (String)String.format("Projects/extra/%s-%08x", ProjectUtils.getInformation(project).getName().replace('/', '_'), project.getProjectDirectory().getPath().hashCode()));
        return FileUtil.createFolder((FileObject)d, (String)AuxiliaryConfigBasedPreferencesProvider.findCNBForClass(owner));
    }

    private static final class AnnotateIconProxyProjectInformation
    implements ProjectInformation,
    PropertyChangeListener,
    ChangeListener,
    LookupListener {
        private final ProjectInformation pinfo;
        private final PropertyChangeSupport pcs;
        private final Set<ProjectIconAnnotator> annotators;
        private boolean annotatorsInitialized;
        private boolean addedPropertyListener;
        private final Object LOCK;
        private Lookup.Result<ProjectIconAnnotator> annotatorResult;
        private Icon icon;

        public AnnotateIconProxyProjectInformation(ProjectInformation pi) {
            this.pcs = new PropertyChangeSupport(this);
            this.annotators = new WeakSet();
            this.annotatorsInitialized = false;
            this.addedPropertyListener = false;
            this.LOCK = new Object();
            this.pinfo = pi;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void annotatorsChanged() {
            Object object = this.LOCK;
            synchronized (object) {
                if (!this.annotatorsInitialized) {
                    this.annotatorResult = Lookup.getDefault().lookupResult(ProjectIconAnnotator.class);
                    this.annotatorResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.annotatorResult));
                    this.annotatorsInitialized = true;
                }
                for (ProjectIconAnnotator pa : this.annotatorResult.allInstances()) {
                    if (!this.annotators.add(pa)) continue;
                    pa.addChangeListener(WeakListeners.change((ChangeListener)this, (Object)pa));
                }
            }
        }

        public void resultChanged(LookupEvent ev) {
            this.annotatorsChanged();
            this.updateIcon(true);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("icon".equals(evt.getPropertyName())) {
                Object object = this.LOCK;
                synchronized (object) {
                    if (!this.annotatorsInitialized) {
                        this.annotatorsChanged();
                    }
                }
                this.updateIcon(true);
            } else {
                this.pcs.firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
            }
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.updateIcon(true);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void updateIcon(boolean fireChange) {
            Icon newOne;
            Icon original = this.pinfo.getIcon();
            if (original == null) {
                return;
            }
            Image _icon = ImageUtilities.icon2Image((Icon)original);
            Project prj = this.getProject();
            assert (prj != null);
            if (prj != null) {
                for (ProjectIconAnnotator pa : this.annotatorResult.allInstances()) {
                    _icon = pa.annotateIcon(prj, _icon, false);
                }
            }
            Icon old = this.icon;
            Object object = this.LOCK;
            synchronized (object) {
                newOne = this.icon = ImageUtilities.image2Icon((Image)_icon);
            }
            if (fireChange) {
                this.pcs.firePropertyChange("icon", old, newOne);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Icon getIcon() {
            Object object = this.LOCK;
            synchronized (object) {
                if (this.icon == null) {
                    if (!this.annotatorsInitialized) {
                        this.annotatorsChanged();
                    }
                    this.updateIcon(false);
                }
                return this.icon;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            Object object = this.LOCK;
            synchronized (object) {
                if (!this.addedPropertyListener) {
                    this.pinfo.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.pinfo));
                    this.addedPropertyListener = true;
                }
            }
            this.pcs.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.pcs.removePropertyChangeListener(listener);
        }

        @Override
        public Project getProject() {
            return this.pinfo.getProject();
        }

        @Override
        public String getName() {
            return this.pinfo.getName();
        }

        @Override
        public String getDisplayName() {
            return this.pinfo.getDisplayName();
        }
    }

    private static final class BasicInformation
    implements ProjectInformation {
        private final Project p;

        public BasicInformation(Project p) {
            this.p = p;
        }

        @Override
        public String getName() {
            return this.getProjectDirectory().toURL().toExternalForm();
        }

        @Override
        public String getDisplayName() {
            return this.getProjectDirectory().getNameExt();
        }

        @Override
        public Icon getIcon() {
            return ImageUtilities.loadImageIcon((String)"org/netbeans/modules/projectapi/resources/empty.gif", (boolean)false);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
        }

        @Override
        public Project getProject() {
            return this.p;
        }

        @NonNull
        private FileObject getProjectDirectory() {
            FileObject pd = this.p.getProjectDirectory();
            if (pd == null) {
                throw new IllegalStateException(String.format("Project: %s returned null project directory.", this.p));
            }
            return pd;
        }
    }

}

