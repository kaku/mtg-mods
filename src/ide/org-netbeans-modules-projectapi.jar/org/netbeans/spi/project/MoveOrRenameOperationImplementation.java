/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.io.IOException;
import org.netbeans.spi.project.MoveOperationImplementation;

public interface MoveOrRenameOperationImplementation
extends MoveOperationImplementation {
    public void notifyRenaming() throws IOException;

    public void notifyRenamed(String var1) throws IOException;
}

