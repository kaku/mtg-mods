/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project;

import java.net.URI;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;

public interface FileOwnerQueryImplementation {
    public Project getOwner(URI var1);

    public Project getOwner(FileObject var1);
}

