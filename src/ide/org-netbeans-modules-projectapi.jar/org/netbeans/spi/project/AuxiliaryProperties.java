/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

public interface AuxiliaryProperties {
    public String get(String var1, boolean var2);

    public void put(String var1, String var2, boolean var3);

    public Iterable<String> listKeys(boolean var1);
}

