/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project;

import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;

public interface ProjectFactory {
    public boolean isProject(FileObject var1);

    public Project loadProject(FileObject var1, ProjectState var2) throws IOException;

    public void saveProject(Project var1) throws IOException, ClassCastException;
}

