/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.awt.Image;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;

public interface ProjectIconAnnotator {
    public Image annotateIcon(Project var1, Image var2, boolean var3);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

