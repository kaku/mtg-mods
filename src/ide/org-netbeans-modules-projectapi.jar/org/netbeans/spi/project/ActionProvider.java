/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.project;

import org.openide.util.Lookup;

public interface ActionProvider {
    public static final String COMMAND_BUILD = "build";
    public static final String COMMAND_COMPILE_SINGLE = "compile.single";
    public static final String COMMAND_CLEAN = "clean";
    public static final String COMMAND_REBUILD = "rebuild";
    public static final String COMMAND_RUN = "run";
    public static final String COMMAND_RUN_SINGLE = "run.single";
    public static final String COMMAND_TEST = "test";
    public static final String COMMAND_TEST_SINGLE = "test.single";
    public static final String COMMAND_DEBUG = "debug";
    public static final String COMMAND_DEBUG_SINGLE = "debug.single";
    public static final String COMMAND_DEBUG_TEST_SINGLE = "debug.test.single";
    public static final String COMMAND_DEBUG_STEP_INTO = "debug.stepinto";
    public static final String COMMAND_PROFILE = "profile";
    public static final String COMMAND_PROFILE_SINGLE = "profile.single";
    public static final String COMMAND_PROFILE_TEST_SINGLE = "profile.test.single";
    public static final String COMMAND_DELETE = "delete";
    public static final String COMMAND_COPY = "copy";
    public static final String COMMAND_MOVE = "move";
    public static final String COMMAND_RENAME = "rename";

    public String[] getSupportedActions();

    public void invokeAction(String var1, Lookup var2) throws IllegalArgumentException;

    public boolean isActionEnabled(String var1, Lookup var2) throws IllegalArgumentException;
}

