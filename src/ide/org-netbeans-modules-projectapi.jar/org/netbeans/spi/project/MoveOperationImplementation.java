/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.io.File;
import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.DataFilesProviderImplementation;

public interface MoveOperationImplementation
extends DataFilesProviderImplementation {
    public void notifyMoving() throws IOException;

    public void notifyMoved(Project var1, File var2, String var3) throws IOException;
}

