/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

public interface ProjectState {
    public void markModified();

    public void notifyDeleted() throws IllegalStateException;
}

