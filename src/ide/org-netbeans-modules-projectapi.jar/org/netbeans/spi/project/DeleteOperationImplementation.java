/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.io.IOException;
import org.netbeans.spi.project.DataFilesProviderImplementation;

public interface DeleteOperationImplementation
extends DataFilesProviderImplementation {
    public void notifyDeleting() throws IOException;

    public void notifyDeleted() throws IOException;
}

