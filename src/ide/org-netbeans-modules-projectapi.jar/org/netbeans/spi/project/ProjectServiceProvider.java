/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.spi.project.LookupProvider;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE, ElementType.METHOD})
public @interface ProjectServiceProvider {
    public Class<?>[] service();

    public String[] projectType() default {};

    public LookupProvider.Registration.ProjectType[] projectTypes() default {};
}

