/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import org.netbeans.api.project.SourceGroup;

public interface SourceGroupModifierImplementation {
    public SourceGroup createSourceGroup(String var1, String var2);

    public boolean canCreateSourceGroup(String var1, String var2);
}

