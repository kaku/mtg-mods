/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.project;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.openide.util.Lookup;

public interface LookupProvider {
    public Lookup createAdditionalLookup(Lookup var1);

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE, ElementType.METHOD})
    public static @interface Registration {
        public String[] projectType() default {};

        public ProjectType[] projectTypes() default {};

        @Retention(value=RetentionPolicy.SOURCE)
        @Target(value={})
        public static @interface ProjectType {
            public String id();

            public int position() default Integer.MAX_VALUE;
        }

    }

}

