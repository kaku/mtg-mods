/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project;

import java.io.IOException;
import org.openide.filesystems.FileObject;

public interface CacheDirectoryProvider {
    public FileObject getCacheDirectory() throws IOException;
}

