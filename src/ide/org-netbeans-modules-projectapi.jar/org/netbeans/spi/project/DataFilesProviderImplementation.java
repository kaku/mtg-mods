/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project;

import java.util.List;
import org.openide.filesystems.FileObject;

public interface DataFilesProviderImplementation {
    public List<FileObject> getMetadataFiles();

    public List<FileObject> getDataFiles();
}

