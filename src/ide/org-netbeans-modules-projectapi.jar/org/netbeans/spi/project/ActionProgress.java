/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.project;

import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Lookup;

public abstract class ActionProgress {
    @NonNull
    public static ActionProgress start(@NonNull Lookup context) {
        ActionProgress ap = (ActionProgress)context.lookup(ActionProgress.class);
        if (ap != null) {
            ap.started();
            return ap;
        }
        return new ActionProgress(){

            @Override
            public void started() {
            }

            @Override
            public void finished(boolean success) {
            }
        };
    }

    protected ActionProgress() {
    }

    protected abstract void started();

    public abstract void finished(boolean var1);

}

