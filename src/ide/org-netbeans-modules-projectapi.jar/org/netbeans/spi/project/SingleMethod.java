/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project;

import org.openide.filesystems.FileObject;

public final class SingleMethod {
    private FileObject file;
    private String methodName;
    public static final String COMMAND_RUN_SINGLE_METHOD = "run.single.method";
    public static final String COMMAND_DEBUG_SINGLE_METHOD = "debug.single.method";

    public SingleMethod(FileObject file, String methodName) {
        if (file == null) {
            throw new IllegalArgumentException("file is <null>");
        }
        if (methodName == null) {
            throw new IllegalArgumentException("methodName is <null>");
        }
        this.file = file;
        this.methodName = methodName;
    }

    public FileObject getFile() {
        return this.file;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != SingleMethod.class) {
            return false;
        }
        SingleMethod other = (SingleMethod)obj;
        return other.file.equals((Object)this.file) && other.methodName.equals(this.methodName);
    }

    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.file.hashCode();
        hash = 29 * hash + this.methodName.hashCode();
        return hash;
    }
}

