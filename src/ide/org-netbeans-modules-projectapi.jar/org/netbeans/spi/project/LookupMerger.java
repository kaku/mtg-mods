/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.project;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.spi.project.LookupProvider;
import org.openide.util.Lookup;

public interface LookupMerger<T> {
    public Class<T> getMergeableClass();

    public T merge(Lookup var1);

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE, ElementType.METHOD})
    public static @interface Registration {
        public String[] projectType() default {};

        public LookupProvider.Registration.ProjectType[] projectTypes() default {};
    }

}

