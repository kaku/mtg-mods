/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.project.support;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.CopyOperationImplementation;
import org.netbeans.spi.project.DataFilesProviderImplementation;
import org.netbeans.spi.project.DeleteOperationImplementation;
import org.netbeans.spi.project.MoveOperationImplementation;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public final class ProjectOperations {
    private ProjectOperations() {
    }

    public static List<FileObject> getMetadataFiles(Project prj) {
        ArrayList<FileObject> result = new ArrayList<FileObject>();
        for (DataFilesProviderImplementation i : prj.getLookup().lookupAll(DataFilesProviderImplementation.class)) {
            result.addAll(i.getMetadataFiles());
            assert (!result.contains(null));
        }
        return result;
    }

    public static List<FileObject> getDataFiles(Project prj) {
        ArrayList<FileObject> result = new ArrayList<FileObject>();
        for (DataFilesProviderImplementation i : prj.getLookup().lookupAll(DataFilesProviderImplementation.class)) {
            result.addAll(i.getDataFiles());
            assert (!result.contains(null));
        }
        return result;
    }

    public static boolean isDeleteOperationSupported(Project prj) {
        return prj.getLookup().lookup(DeleteOperationImplementation.class) != null;
    }

    public static void notifyDeleting(Project prj) throws IOException {
        for (DeleteOperationImplementation i : prj.getLookup().lookupAll(DeleteOperationImplementation.class)) {
            i.notifyDeleting();
        }
    }

    public static void notifyDeleted(Project prj) throws IOException {
        for (DeleteOperationImplementation i : prj.getLookup().lookupAll(DeleteOperationImplementation.class)) {
            i.notifyDeleted();
        }
    }

    public static boolean isCopyOperationSupported(Project prj) {
        return prj.getLookup().lookup(CopyOperationImplementation.class) != null;
    }

    public static void notifyCopying(Project prj) throws IOException {
        for (CopyOperationImplementation i : prj.getLookup().lookupAll(CopyOperationImplementation.class)) {
            i.notifyCopying();
        }
    }

    public static void notifyCopied(Project original, Project nue, File originalPath, String name) throws IOException {
        for (CopyOperationImplementation i2 : original.getLookup().lookupAll(CopyOperationImplementation.class)) {
            i2.notifyCopied(null, originalPath, name);
        }
        for (CopyOperationImplementation i2 : nue.getLookup().lookupAll(CopyOperationImplementation.class)) {
            i2.notifyCopied(original, originalPath, name);
        }
    }

    public static void notifyMoving(Project prj) throws IOException {
        for (MoveOperationImplementation i : prj.getLookup().lookupAll(MoveOperationImplementation.class)) {
            i.notifyMoving();
        }
    }

    public static void notifyMoved(Project original, Project nue, File originalPath, String name) throws IOException {
        for (MoveOperationImplementation i2 : original.getLookup().lookupAll(MoveOperationImplementation.class)) {
            i2.notifyMoved(null, originalPath, name);
        }
        for (MoveOperationImplementation i2 : nue.getLookup().lookupAll(MoveOperationImplementation.class)) {
            i2.notifyMoved(original, originalPath, name);
        }
    }

    public static boolean isMoveOperationSupported(Project prj) {
        return prj.getLookup().lookup(MoveOperationImplementation.class) != null;
    }
}

