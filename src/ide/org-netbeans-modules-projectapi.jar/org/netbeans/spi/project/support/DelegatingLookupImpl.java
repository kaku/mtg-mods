/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.spi.project.support;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.projectapi.MetaLookupMerger;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.project.LookupProvider;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

class DelegatingLookupImpl
extends ProxyLookup
implements LookupListener,
ChangeListener {
    private static final Logger LOG = Logger.getLogger(DelegatingLookupImpl.class.getName());
    private final Lookup baseLookup;
    private final String pathDescriptor;
    private final UnmergedLookup unmergedLookup = new UnmergedLookup();
    private final Map<LookupMerger<?>, Object> mergerResults = new HashMap();
    private final Lookup.Result<LookupProvider> providerResult;
    private final LookupListener providerListener;
    private List<LookupProvider> old = Collections.emptyList();
    private List<Lookup> currentLookups;
    private final ChangeListener metaMergerListener;
    private Lookup.Result<LookupMerger> mergers;
    private final Lookup.Result<MetaLookupMerger> metaMergers;
    private Reference<LookupListener> listenerRef;
    private final List<Lookup.Result<?>> results = new ArrayList();

    DelegatingLookupImpl(Lookup base, Lookup providerLookup, String pathDescriptor) {
        assert (base != null);
        this.baseLookup = base;
        this.pathDescriptor = pathDescriptor;
        this.providerResult = providerLookup.lookupResult(LookupProvider.class);
        this.metaMergers = providerLookup.lookupResult(MetaLookupMerger.class);
        this.metaMergerListener = WeakListeners.change((ChangeListener)this, (Object)null);
        assert (this.isAllJustLookupProviders(providerLookup));
        this.doDelegate();
        this.providerListener = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                DelegatingLookupImpl.this.doDelegate();
            }
        };
        this.providerResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.providerListener, this.providerResult));
        this.metaMergers.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.providerListener, this.metaMergers));
    }

    public void resultChanged(LookupEvent ev) {
        this.doDelegate();
    }

    protected void beforeLookup(Lookup.Template<?> template) {
        for (MetaLookupMerger metaMerger : this.metaMergers.allInstances()) {
            metaMerger.probing(template.getType());
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.doDelegate();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void doDelegate() {
        List list = this.results;
        synchronized (list) {
            LookupListener l;
            for (Lookup.Result r : this.results) {
                r.removeLookupListener((LookupListener)this);
            }
            this.results.clear();
            Collection providers = this.providerResult.allInstances();
            LOG.log(Level.FINE, "New providers count: {0} for: {1}", new Object[]{providers.size(), System.identityHashCode(this)});
            ArrayList<Lookup> newLookups = new ArrayList<Lookup>();
            for (LookupProvider elem : providers) {
                if (this.old.contains(elem)) {
                    int index = this.old.indexOf(elem);
                    newLookups.add(this.currentLookups.get(index));
                    continue;
                }
                Lookup newone = elem.createAdditionalLookup(this.baseLookup);
                assert (newone != null);
                newLookups.add(newone);
            }
            this.old = new ArrayList<LookupProvider>(providers);
            this.currentLookups = newLookups;
            newLookups.add(this.baseLookup);
            this.unmergedLookup._setLookups(newLookups.toArray((T[])new Lookup[newLookups.size()]));
            ArrayList filteredClasses = new ArrayList();
            ArrayList<Object> mergedInstances = new ArrayList<Object>();
            LookupListener lookupListener = l = this.listenerRef != null ? this.listenerRef.get() : null;
            if (l != null) {
                this.mergers.removeLookupListener(l);
            }
            this.mergers = this.unmergedLookup.lookupResult(LookupMerger.class);
            l = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.mergers);
            this.listenerRef = new WeakReference<LookupListener>(l);
            this.mergers.addLookupListener(l);
            ArrayList<LookupMerger> allMergers = new ArrayList<LookupMerger>(this.mergers.allInstances());
            for (MetaLookupMerger metaMerger : this.metaMergers.allInstances()) {
                LookupMerger merger = metaMerger.merger();
                if (merger != null) {
                    allMergers.add(merger);
                }
                metaMerger.removeChangeListener(this.metaMergerListener);
                metaMerger.addChangeListener(this.metaMergerListener);
            }
            for (LookupMerger lm : allMergers) {
                Class c = lm.getMergeableClass();
                if (filteredClasses.contains(c)) {
                    LOG.log(Level.WARNING, "Two LookupMerger instances for {0} among {1} in {2}. Only first one will be used", new Object[]{c, allMergers, this.pathDescriptor});
                    continue;
                }
                filteredClasses.add(c);
                Object merge = this.mergerResults.get(lm);
                if (merge == null) {
                    merge = lm.merge((Lookup)this.unmergedLookup);
                    this.mergerResults.put(lm, merge);
                }
                mergedInstances.add(merge);
                Lookup.Result result = this.unmergedLookup.lookupResult(c);
                result.addLookupListener((LookupListener)this);
                this.results.add((Lookup.Result)result);
            }
            Lookup filtered = Lookups.exclude((Lookup)this.unmergedLookup, (Class[])filteredClasses.toArray(new Class[filteredClasses.size()]));
            Lookup fixed = Lookups.fixed((Object[])mergedInstances.toArray(new Object[mergedInstances.size()]));
            this.setLookups(new Lookup[]{fixed, filtered});
        }
    }

    private boolean isAllJustLookupProviders(Lookup lkp) {
        for (Lookup.Item item : lkp.lookupResult(Object.class).allItems()) {
            Class clzz = item.getType();
            if (LookupProvider.class.isAssignableFrom(clzz) || MetaLookupMerger.class.isAssignableFrom(clzz)) continue;
            LOG.log(Level.WARNING, "{0} from {1} is not a LookupProvider", new Object[]{clzz.getName(), item.getId()});
        }
        return true;
    }

    private static class UnmergedLookup
    extends ProxyLookup {
        private UnmergedLookup() {
        }

        /* varargs */ void _setLookups(Lookup ... lookups) {
            this.setLookups(lookups);
        }
    }

}

