/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.SharabilityQuery
 *  org.netbeans.api.queries.SharabilityQuery$Sharability
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.project.support;

import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.api.queries.SharabilityQuery;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public class GenericSources {
    private GenericSources() {
    }

    public static Sources genericOnly(Project p) {
        return new GenericOnlySources(p);
    }

    public static SourceGroup group(Project p, FileObject rootFolder, String name, String displayName, Icon icon, Icon openedIcon) {
        Parameters.notNull((CharSequence)"p", (Object)p);
        Parameters.notNull((CharSequence)"rootFolder", (Object)rootFolder);
        Parameters.notNull((CharSequence)"name", (Object)name);
        Parameters.notNull((CharSequence)"displayName", (Object)displayName);
        return new Group(p, rootFolder, name, displayName, icon, openedIcon);
    }

    private static final class Group
    implements SourceGroup {
        private final Project p;
        private final FileObject rootFolder;
        private final String name;
        private final String displayName;
        private final Icon icon;
        private final Icon openedIcon;

        Group(Project p, FileObject rootFolder, String name, String displayName, Icon icon, Icon openedIcon) {
            this.p = p;
            this.rootFolder = rootFolder;
            this.name = name;
            this.displayName = displayName;
            this.icon = icon;
            this.openedIcon = openedIcon;
        }

        @Override
        public FileObject getRootFolder() {
            return this.rootFolder;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getDisplayName() {
            return this.displayName;
        }

        @Override
        public Icon getIcon(boolean opened) {
            return opened ? this.icon : this.openedIcon;
        }

        @Override
        public boolean contains(FileObject file) {
            if (file != this.rootFolder && !FileUtil.isParentOf((FileObject)this.rootFolder, (FileObject)file)) {
                return false;
            }
            if (file.isFolder() && file != this.p.getProjectDirectory() && ProjectManager.getDefault().isProject(file)) {
                return false;
            }
            if (FileOwnerQuery.getOwner(file) != this.p) {
                return false;
            }
            return SharabilityQuery.getSharability((FileObject)file) != SharabilityQuery.Sharability.NOT_SHARABLE;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener l) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener l) {
        }

        public String toString() {
            return "GenericSources.Group[name=" + this.name + ",rootFolder=" + (Object)this.rootFolder + "]";
        }
    }

    private static final class GenericOnlySources
    implements Sources {
        private final Project p;

        GenericOnlySources(Project p) {
            this.p = p;
        }

        @Override
        public SourceGroup[] getSourceGroups(String type) {
            if (type.equals("generic")) {
                return new SourceGroup[]{GenericSources.group(this.p, this.p.getProjectDirectory(), "generic", ProjectUtils.getInformation(this.p).getDisplayName(), null, null)};
            }
            return new SourceGroup[0];
        }

        @Override
        public void addChangeListener(ChangeListener listener) {
        }

        @Override
        public void removeChangeListener(ChangeListener listener) {
        }
    }

}

