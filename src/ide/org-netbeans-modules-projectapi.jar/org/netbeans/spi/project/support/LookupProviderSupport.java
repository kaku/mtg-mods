/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.spi.project.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.project.support.DelegatingLookupImpl;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.Lookups;

public final class LookupProviderSupport {
    private LookupProviderSupport() {
    }

    public static Lookup createCompositeLookup(Lookup baseLookup, String folderPath) {
        return new DelegatingLookupImpl(baseLookup, Lookups.forPath((String)folderPath), folderPath);
    }

    public static Lookup createCompositeLookup(Lookup baseLookup, Lookup providers) {
        return new DelegatingLookupImpl(baseLookup, providers, "<multiplePaths>");
    }

    public static LookupMerger<Sources> createSourcesMerger() {
        return new SourcesMerger();
    }

    public static LookupMerger<ActionProvider> createActionProviderMerger() {
        return new ActionProviderMerger();
    }

    private static final class MergedActionProvider
    implements ActionProvider,
    LookupListener {
        private final Lookup.Result<ActionProvider> lkpResult;
        private volatile String[] actionNamesCache;

        private MergedActionProvider(Lookup lkp) {
            this.lkpResult = lkp.lookupResult(ActionProvider.class);
            this.lkpResult.addLookupListener((LookupListener)this);
        }

        @Override
        public String[] getSupportedActions() {
            String[] result = this.actionNamesCache;
            if (result == null) {
                LinkedHashSet<String> actionNames = new LinkedHashSet<String>();
                for (ActionProvider ap : this.lkpResult.allInstances()) {
                    actionNames.addAll(Arrays.asList(ap.getSupportedActions()));
                }
                result = actionNames.toArray(new String[actionNames.size()]);
                this.actionNamesCache = result;
            }
            assert (result != null);
            return result;
        }

        @Override
        public boolean isActionEnabled(String command, Lookup context) throws IllegalArgumentException {
            boolean found = false;
            for (ActionProvider ap : this.lkpResult.allInstances()) {
                if (!Arrays.asList(ap.getSupportedActions()).contains(command)) continue;
                if (ap.isActionEnabled(command, context)) {
                    return true;
                }
                found = true;
            }
            if (found) {
                return false;
            }
            throw new IllegalArgumentException("Misimplemented command '" + command + "' in " + Arrays.toString(this.lkpResult.allInstances().toArray()));
        }

        @Override
        public void invokeAction(String command, Lookup context) throws IllegalArgumentException {
            for (ActionProvider ap : this.lkpResult.allInstances()) {
                if (!Arrays.asList(ap.getSupportedActions()).contains(command) || !ap.isActionEnabled(command, context)) continue;
                ap.invokeAction(command, context);
                return;
            }
            throw new IllegalArgumentException(String.format(command, new Object[0]));
        }

        public void resultChanged(LookupEvent ev) {
            this.actionNamesCache = null;
        }
    }

    private static final class ActionProviderMerger
    implements LookupMerger<ActionProvider> {
        private ActionProviderMerger() {
        }

        @Override
        public Class<ActionProvider> getMergeableClass() {
            return ActionProvider.class;
        }

        @Override
        public ActionProvider merge(Lookup lookup) {
            return new MergedActionProvider(lookup);
        }
    }

    private static class SourcesImpl
    implements Sources,
    ChangeListener,
    LookupListener {
        private final ChangeSupport changeSupport;
        private final Lookup.Result<Sources> delegates;
        private Sources[] currentDelegates;

        SourcesImpl(Lookup lookup) {
            this.changeSupport = new ChangeSupport((Object)this);
            this.delegates = lookup.lookupResult(Sources.class);
            this.delegates.addLookupListener((LookupListener)this);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public SourceGroup[] getSourceGroups(String type) {
            Sources[] _currentDelegates;
            assert (this.delegates != null);
            SourcesImpl sourcesImpl = this;
            synchronized (sourcesImpl) {
                if (this.currentDelegates == null) {
                    Collection instances = this.delegates.allInstances();
                    for (Sources ns : this.currentDelegates = instances.toArray(new Sources[instances.size()])) {
                        ns.addChangeListener(this);
                    }
                }
                _currentDelegates = this.currentDelegates;
            }
            ArrayList<SourceGroup> result = new ArrayList<SourceGroup>();
            for (Sources ns : _currentDelegates) {
                SourceGroup[] sourceGroups = ns.getSourceGroups(type);
                if (sourceGroups == null) continue;
                for (SourceGroup sourceGroup : sourceGroups) {
                    if (sourceGroup == null) {
                        Exceptions.printStackTrace((Throwable)new NullPointerException(ns + " returns null source group!"));
                        continue;
                    }
                    result.add(sourceGroup);
                }
            }
            return result.toArray(new SourceGroup[result.size()]);
        }

        @Override
        public synchronized void addChangeListener(ChangeListener listener) {
            this.changeSupport.addChangeListener(listener);
        }

        @Override
        public synchronized void removeChangeListener(ChangeListener listener) {
            this.changeSupport.removeChangeListener(listener);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            this.changeSupport.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            SourcesImpl sourcesImpl = this;
            synchronized (sourcesImpl) {
                if (this.currentDelegates != null) {
                    for (Sources old : this.currentDelegates) {
                        old.removeChangeListener(this);
                    }
                    this.currentDelegates = null;
                }
            }
            this.changeSupport.fireChange();
        }
    }

    private static class SourcesMerger
    implements LookupMerger<Sources> {
        private SourcesMerger() {
        }

        @Override
        public Class<Sources> getMergeableClass() {
            return Sources.class;
        }

        @Override
        public Sources merge(Lookup lookup) {
            return new SourcesImpl(lookup);
        }
    }

}

