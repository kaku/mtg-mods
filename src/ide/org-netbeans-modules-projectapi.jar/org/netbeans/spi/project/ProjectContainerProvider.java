/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.project;

import java.util.Collections;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;

public interface ProjectContainerProvider {
    @NonNull
    public Result getContainedProjects();

    public void addChangeListener(@NonNull ChangeListener var1);

    public void removeChangeListener(@NonNull ChangeListener var1);

    public static final class Result {
        private final boolean recursive;
        private final Set<? extends Project> projects;

        public Result(@NonNull Set<? extends Project> projects, boolean recursive) {
            this.projects = Collections.unmodifiableSet(projects);
            this.recursive = recursive;
        }

        public boolean isRecursive() {
            return this.recursive;
        }

        @NonNull
        public Set<? extends Project> getProjects() {
            return this.projects;
        }
    }

}

