/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.util.Set;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;

public interface SubprojectProvider {
    public Set<? extends Project> getSubprojects();

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);
}

