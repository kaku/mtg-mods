/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 */
package org.netbeans.spi.project;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Collection;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.spi.project.ProjectConfiguration;

public interface ProjectConfigurationProvider<C extends ProjectConfiguration> {
    public static final String PROP_CONFIGURATION_ACTIVE = "activeConfiguration";
    public static final String PROP_CONFIGURATIONS = "configurations";

    public Collection<C> getConfigurations();

    @CheckForNull
    public C getActiveConfiguration();

    public void setActiveConfiguration(C var1) throws IllegalArgumentException, IOException;

    public boolean hasCustomizer();

    public void customize();

    public boolean configurationsAffectAction(String var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

