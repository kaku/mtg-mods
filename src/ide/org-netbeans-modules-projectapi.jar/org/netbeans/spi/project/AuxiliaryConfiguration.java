/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import org.w3c.dom.Element;

public interface AuxiliaryConfiguration {
    public Element getConfigurationFragment(String var1, String var2, boolean var3);

    public void putConfigurationFragment(Element var1, boolean var2) throws IllegalArgumentException;

    public boolean removeConfigurationFragment(String var1, String var2, boolean var3) throws IllegalArgumentException;
}

