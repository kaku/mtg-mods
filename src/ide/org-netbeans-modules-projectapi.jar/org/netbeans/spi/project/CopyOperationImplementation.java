/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project;

import java.io.File;
import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.DataFilesProviderImplementation;

public interface CopyOperationImplementation
extends DataFilesProviderImplementation {
    public void notifyCopying() throws IOException;

    public void notifyCopied(Project var1, File var2, String var3) throws IOException;
}

