/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project;

import org.netbeans.api.project.ProjectManager;
import org.netbeans.spi.project.ProjectFactory;
import org.openide.filesystems.FileObject;

public interface ProjectFactory2
extends ProjectFactory {
    public ProjectManager.Result isProject2(FileObject var1);
}

