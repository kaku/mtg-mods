/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.url;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Parser {
    private static final Pattern URL_PATTERN = Pattern.compile("(ht|f)(tp(s?)|ile)://[0-9a-zA-Z/.?%+_~=\\\\&$\\-#,:!/(/)]*");
    private static final Pattern CHARSET = Pattern.compile("charset=([^;]+)(;|$)", 8);

    private Parser() {
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public static Iterable<int[]> recognizeURLs(CharSequence text) {
        result = new LinkedList<int[]>();
        state = STATE.START;
        lastURLStart = -1;
        cntr = 0;
        while (cntr < text.length()) {
            ch = text.charAt(cntr);
            if (state == STATE.END) {
                if (!Character.isLetterOrDigit(ch)) {
                    switch (ch) {
                        case '!': 
                        case '#': 
                        case '$': 
                        case '%': 
                        case '&': 
                        case '(': 
                        case ')': 
                        case '+': 
                        case ',': 
                        case '-': 
                        case '.': 
                        case '/': 
                        case ':': 
                        case ';': 
                        case '=': 
                        case '?': 
                        case '\\': 
                        case '_': 
                        case '~': {
                            ** break;
                        }
                    }
                    if (!Parser.$assertionsDisabled && lastURLStart == -1) {
                        throw new AssertionError();
                    }
                    result.add(new int[]{lastURLStart, cntr});
                    lastURLStart = -1;
                    state = STATE.START;
                    ** break;
                }
lbl18: // 4 sources:
            } else {
                switch (ch) {
                    case 'h': {
                        if (state != STATE.START) break;
                        lastURLStart = cntr;
                        state = STATE.H;
                        ** break;
                    }
                    case 't': {
                        if (state == STATE.H) {
                            state = STATE.HT;
                            ** break;
                        }
                        if (state == STATE.HT) {
                            state = STATE.HTT_FT;
                            ** break;
                        }
                        if (state != STATE.F) break;
                        state = STATE.HTT_FT;
                        ** break;
                    }
                    case 'f': {
                        if (state != STATE.START) break;
                        lastURLStart = cntr;
                        state = STATE.F;
                        ** break;
                    }
                    case 'i': {
                        if (state == STATE.F) {
                            state = STATE.FI;
                            ** break;
                        }
                    }
                    case 'l': {
                        if (state == STATE.FI) {
                            state = STATE.FIL;
                            ** break;
                        }
                    }
                    case 'e': {
                        if (state == STATE.FIL) {
                            state = STATE.FILE;
                            ** break;
                        }
                    }
                    case 'p': {
                        if (state != STATE.HTT_FT) break;
                        state = STATE.HTTP_FTP;
                        ** break;
                    }
                    case 's': {
                        if (state != STATE.HTTP_FTP) break;
                        state = STATE.HTTPS;
                        ** break;
                    }
                    case ':': {
                        if (state != STATE.HTTP_FTP && state != STATE.HTTPS && state != STATE.FILE) break;
                        state = STATE.HTTPC;
                        ** break;
                    }
                    case '/': {
                        if (state == STATE.HTTPC) {
                            state = STATE.HTTPCS;
                            ** break;
                        }
                        if (state != STATE.HTTPCS) break;
                        state = STATE.END;
                        ** break;
                    }
                }
                state = STATE.START;
                lastURLStart = -1;
            }
lbl73: // 15 sources:
            ++cntr;
        }
        return result;
    }

    public static Iterable<int[]> recognizeURLsREBased(CharSequence text) {
        Matcher m = URL_PATTERN.matcher(text);
        LinkedList<int[]> result = new LinkedList<int[]>();
        while (m.find()) {
            result.add(new int[]{m.start(), m.start() + m.group(0).length()});
        }
        return result;
    }

    public static String decodeContentType(String contentType) {
        Matcher m;
        if (contentType == null) {
            return null;
        }
        if (contentType != null && (m = CHARSET.matcher(contentType)).find()) {
            return m.group(1);
        }
        return null;
    }

    private static enum STATE {
        START,
        H,
        HT,
        F,
        HTT_FT,
        HTTP_FTP,
        HTTPS,
        HTTPC,
        HTTPCS,
        FI,
        FIL,
        FILE,
        END;
        

        private STATE() {
        }
    }

}

