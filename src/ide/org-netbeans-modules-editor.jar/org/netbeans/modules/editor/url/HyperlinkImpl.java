/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkType
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.editor.url;

import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.EnumSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.url.Parser;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class HyperlinkImpl
implements HyperlinkProviderExt {
    public static final Logger LOG = Logger.getLogger(HyperlinkImpl.class.getName());
    private static final int TIME_VALID = 86400000;
    private static final int TIME_INVALID = 60000;
    private static final Pattern TITLE = Pattern.compile("<title>(.*)</title>");

    public Set<HyperlinkType> getSupportedHyperlinkTypes() {
        return EnumSet.of(HyperlinkType.GO_TO_DECLARATION);
    }

    public boolean isHyperlinkPoint(Document doc, int offset, HyperlinkType type) {
        return this.getHyperlinkSpan(doc, offset, type) != null;
    }

    public int[] getHyperlinkSpan(Document doc, int offset, HyperlinkType type) {
        if (!(doc instanceof BaseDocument)) {
            return null;
        }
        try {
            BaseDocument bdoc = (BaseDocument)doc;
            int start = Utilities.getRowStart((BaseDocument)bdoc, (int)offset);
            int end = Utilities.getRowEnd((BaseDocument)bdoc, (int)offset);
            for (int[] span : Parser.recognizeURLs(DocumentUtilities.getText((Document)doc, (int)start, (int)(end - start)))) {
                if (span[0] + start > offset || offset > span[1] + start) continue;
                return new int[]{span[0] + start, span[1] + start};
            }
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        return null;
    }

    public void performClickAction(Document doc, int offset, HyperlinkType type) {
        int[] span = this.getHyperlinkSpan(doc, offset, type);
        if (span == null) {
            Toolkit.getDefaultToolkit().beep();
            return;
        }
        try {
            String urlText = doc.getText(span[0], span[1] - span[0]);
            URL url = new URL(urlText);
            HtmlBrowser.URLDisplayer.getDefault().showURL(url);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (MalformedURLException ex) {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(HyperlinkImpl.class, (String)"WARN_Invalid_URL", (Object)ex.getMessage()));
            LOG.log(Level.FINE, null, ex);
        }
    }

    public String getTooltipText(Document doc, int offset, HyperlinkType type) {
        int[] span = this.getHyperlinkSpan(doc, offset, type);
        if (span == null) {
            return null;
        }
        String title = HyperlinkImpl.getTitleImpl(doc, span);
        if (title != null && title.length() == 0) {
            title = null;
        }
        return title;
    }

    private static String getTitleImpl(Document doc, int[] span) {
        try {
            String urlText = doc.getText(span[0], span[1] - span[0]);
            URL url = new URL(urlText);
            Preferences p = NbPreferences.forModule(HyperlinkImpl.class);
            p = p.node("url");
            String timestampKey = url.toExternalForm() + "-timestamp";
            String titleKey = url.toExternalForm() + "-title";
            long prevTime = p.getLong(timestampKey, Long.MIN_VALUE);
            long lastModified = System.currentTimeMillis();
            if (prevTime >= lastModified) {
                return p.get(titleKey, null);
            }
            String title = HyperlinkImpl.readTitle(url);
            if (title == null && (title = p.get(titleKey, null)) != null) {
                return title;
            }
            p.putLong(timestampKey, lastModified + (long)(title != null ? 86400000 : 60000));
            if (title != null) {
                p.put(titleKey, title);
            } else {
                p.remove(titleKey);
            }
            return title;
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (MalformedURLException ex) {
            LOG.log(Level.FINE, null, ex);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String readTitle(URL url) {
        ByteArrayOutputStream baos = null;
        InputStream ins = null;
        URLConnection c = null;
        try {
            int read;
            c = url.openConnection();
            String encoding = null;
            try {
                encoding = c.getContentEncoding();
            }
            catch (Throwable ex) {
                Utilities.setStatusText((JTextComponent)EditorRegistry.lastFocusedComponent(), (String)"Invalid URL");
                String string = null;
                if (baos != null) {
                    try {
                        baos.close();
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                if (ins != null) {
                    try {
                        ins.close();
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                return string;
            }
            if (encoding == null) {
                encoding = Parser.decodeContentType(c.getContentType());
            }
            baos = new ByteArrayOutputStream();
            ins = c.getInputStream();
            while ((read = ins.read()) != -1) {
                baos.write(read);
            }
            ins.close();
            baos.close();
            String content = new String(baos.toByteArray(), encoding != null ? encoding : Charset.defaultCharset().name());
            Matcher m = TITLE.matcher(content);
            if (m.find()) {
                String string = m.group(1).trim();
                return string;
            }
            String string = "";
            return string;
        }
        catch (IOException ex) {
            LOG.log(Level.FINE, url != null ? url.toString() : null, ex);
        }
        catch (IllegalArgumentException iae) {
            LOG.log(Level.FINE, url != null ? url.toString() : null, iae);
        }
        finally {
            if (baos != null) {
                try {
                    baos.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            if (ins != null) {
                try {
                    ins.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        return null;
    }
}

