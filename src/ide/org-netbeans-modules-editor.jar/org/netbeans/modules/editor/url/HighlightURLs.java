/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.url;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.url.Parser;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public final class HighlightURLs
implements DocumentListener,
Runnable {
    private static final Logger LOG = Logger.getLogger(HighlightURLs.class.getName());
    private static final Object REGISTERED_KEY = new Object();
    private final BaseDocument doc;
    private final AttributeSet coloring;
    private final List<Position[]> modifiedSpans = new ArrayList<Position[]>();
    private static final RequestProcessor WORKER_THREAD = new RequestProcessor(HighlightURLs.class.getName(), 1, false, false);
    private final RequestProcessor.Task WORKER;

    static void ensureAttached(final BaseDocument doc) {
        if (doc.getProperty(REGISTERED_KEY) != null) {
            return;
        }
        final HighlightURLs h = new HighlightURLs(doc);
        doc.putProperty(REGISTERED_KEY, (Object)true);
        if (h.coloring == null) {
            LOG.log(Level.WARNING, "'url' coloring cannot be found");
            return;
        }
        doc.addDocumentListener((DocumentListener)h);
        doc.render(new Runnable(){

            @Override
            public void run() {
                try {
                    h.modifiedSpans.add(new Position[]{doc.createPosition(0, Position.Bias.Backward), doc.createPosition(doc.getLength(), Position.Bias.Forward)});
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        });
        h.schedule();
    }

    private HighlightURLs(BaseDocument doc) {
        this.WORKER = WORKER_THREAD.create((Runnable)this);
        this.doc = doc;
        String mimeType = DocumentUtilities.getMimeType((Document)doc);
        FontColorSettings fcs = mimeType == null ? null : (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        this.coloring = fcs == null ? null : fcs.getTokenFontColors("url");
    }

    @Override
    public synchronized void insertUpdate(DocumentEvent e) {
        try {
            this.modifiedSpans.add(new Position[]{this.doc.createPosition(e.getOffset(), Position.Bias.Backward), this.doc.createPosition(e.getOffset() + e.getLength(), Position.Bias.Forward)});
        }
        catch (BadLocationException ex) {
            LOG.log(Level.FINE, null, ex);
        }
        this.schedule();
    }

    @Override
    public synchronized void removeUpdate(DocumentEvent e) {
        try {
            this.modifiedSpans.add(new Position[]{this.doc.createPosition(e.getOffset(), Position.Bias.Backward), this.doc.createPosition(e.getOffset(), Position.Bias.Forward)});
        }
        catch (BadLocationException ex) {
            LOG.log(Level.FINE, null, ex);
        }
        this.schedule();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    private void schedule() {
        this.WORKER.schedule(100);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        final Position[] span = new Position[2];
        final CharSequence[] text = new CharSequence[1];
        final long[] version = new long[1];
        final OffsetsBag workingBag = new OffsetsBag((Document)this.doc);
        final int[] length = new int[1];
        this.doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                HighlightURLs highlightURLs = HighlightURLs.this;
                synchronized (highlightURLs) {
                    try {
                        int startOffset = Integer.MAX_VALUE;
                        int endOffset = -1;
                        for (Position[] sp : HighlightURLs.this.modifiedSpans) {
                            startOffset = Math.min(startOffset, sp[0].getOffset());
                            endOffset = Math.max(endOffset, sp[1].getOffset());
                        }
                        HighlightURLs.this.modifiedSpans.clear();
                        if (endOffset == -1) {
                            return;
                        }
                        startOffset = Utilities.getRowStart((BaseDocument)HighlightURLs.this.doc, (int)startOffset);
                        endOffset = Math.min(HighlightURLs.this.doc.getLength(), endOffset);
                        endOffset = Utilities.getRowEnd((BaseDocument)HighlightURLs.this.doc, (int)endOffset);
                        text[0] = DocumentUtilities.getText((Document)HighlightURLs.this.doc, (int)startOffset, (int)(endOffset - startOffset));
                        version[0] = DocumentUtilities.getDocumentVersion((Document)HighlightURLs.this.doc);
                        workingBag.setHighlights(HighlightURLs.getBag((Document)HighlightURLs.this.doc));
                        workingBag.removeHighlights(startOffset, endOffset + 1, false);
                        length[0] = text[0].length();
                        span[0] = HighlightURLs.this.doc.createPosition(startOffset, Position.Bias.Backward);
                        span[1] = HighlightURLs.this.doc.createPosition(endOffset, Position.Bias.Forward);
                    }
                    catch (BadLocationException ex) {
                        LOG.log(Level.WARNING, null, ex);
                    }
                }
            }
        });
        if (span[0] == null) {
            return;
        }
        CharSequence seq = new CharSequence(){

            @Override
            public int length() {
                return length[0];
            }

            @Override
            public char charAt(final int index) {
                final char[] result = new char[1];
                HighlightURLs.this.doc.render(new Runnable(){

                    @Override
                    public void run() {
                        if (DocumentUtilities.getDocumentVersion((Document)HighlightURLs.this.doc) != version[0]) {
                            class Stop
                            extends Error {
                                Stop() {
                                }
                            }
                            throw new Stop();
                        }
                        result[0] = text[0].charAt(index);
                    }
                });
                return result[0];
            }

            @Override
            public CharSequence subSequence(int start, int end) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

        };
        try {
            final Iterable<int[]> toHighlight = Parser.recognizeURLs(seq);
            this.doc.render(new Runnable(){

                @Override
                public void run() {
                    if (DocumentUtilities.getDocumentVersion((Document)HighlightURLs.this.doc) != version[0]) {
                        throw new Stop();
                    }
                    for (int[] s : toHighlight) {
                        workingBag.addHighlight(span[0].getOffset() + s[0], span[0].getOffset() + s[1], HighlightURLs.this.coloring);
                    }
                    HighlightURLs.getBag((Document)HighlightURLs.this.doc).setHighlights(workingBag);
                }
            });
        }
        catch (1Stop u) {
            HighlightURLs highlightURLs = this;
            synchronized (highlightURLs) {
                this.modifiedSpans.add(span);
            }
            this.schedule();
        }
    }

    private static OffsetsBag getBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(HighlightURLs.class);
        if (bag == null) {
            bag = new OffsetsBag(doc);
            doc.putProperty(HighlightURLs.class, (Object)bag);
        }
        return bag;
    }

    public static final class FactoryImpl
    implements HighlightsLayerFactory {
        public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
            Document doc = context.getDocument();
            if (!(doc instanceof BaseDocument)) {
                return null;
            }
            HighlightURLs.ensureAttached((BaseDocument)doc);
            return new HighlightsLayer[]{HighlightsLayer.create((String)HighlightURLs.class.getName(), (ZOrder)ZOrder.SYNTAX_RACK.forPosition(4950), (boolean)false, (HighlightsContainer)HighlightURLs.getBag(doc))};
        }
    }

}

