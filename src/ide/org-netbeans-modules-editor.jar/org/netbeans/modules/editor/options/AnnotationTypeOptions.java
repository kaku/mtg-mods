/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationType
 */
package org.netbeans.modules.editor.options;

import java.awt.Color;
import org.netbeans.editor.AnnotationType;

public class AnnotationTypeOptions {
    private AnnotationType delegate;

    public AnnotationTypeOptions(AnnotationType delegate) {
        this.delegate = delegate;
    }

    public boolean isVisible() {
        return this.delegate.isVisible();
    }

    public boolean isWholeLine() {
        return this.delegate.isWholeLine();
    }

    public Color getHighlightColor() {
        return this.delegate.getHighlight();
    }

    public void setHighlightColor(Color col) {
        this.delegate.setHighlight(col);
    }

    public boolean isUseHighlightColor() {
        return this.delegate.isUseHighlightColor();
    }

    public void setUseHighlightColor(boolean use) {
        this.delegate.setUseHighlightColor(use);
    }

    public Color getForegroundColor() {
        return this.delegate.getForegroundColor();
    }

    public void setForegroundColor(Color col) {
        this.delegate.setForegroundColor(col);
    }

    public boolean isInheritForegroundColor() {
        return this.delegate.isInheritForegroundColor();
    }

    public void setInheritForegroundColor(boolean inherit) {
        this.delegate.setInheritForegroundColor(inherit);
    }

    public Color getWaveUnderlineColor() {
        return this.delegate.getWaveUnderlineColor();
    }

    public void setWaveUnderlineColor(Color col) {
        this.delegate.setWaveUnderlineColor(col);
    }

    public boolean isUseWaveUnderlineColor() {
        return this.delegate.isUseWaveUnderlineColor();
    }

    public void setUseWaveUnderlineColor(boolean use) {
        this.delegate.setUseWaveUnderlineColor(use);
    }
}

