/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationTypes
 *  org.openide.actions.PropertiesAction
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.BeanNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Array
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.editor.options;

import java.awt.Image;
import java.awt.Toolkit;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.modules.editor.options.AnnotationTypeOptions;
import org.netbeans.modules.editor.options.AnnotationTypesFolder;
import org.openide.actions.PropertiesAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;

public class AnnotationTypesNode
extends AbstractNode {
    private static final String HELP_ID = "editing.configuring.annotations";
    private static final String ICON_BASE = "org/netbeans/modules/editor/resources/annotationtypes";

    public AnnotationTypesNode() {
        super((Children)new AnnotationTypesSubnodes());
        this.setName("annotationtypes");
        this.setDisplayName(this.getBundleString("ATN_AnnotationTypesNode_Name"));
        this.setShortDescription(this.getBundleString("ATN_AnnotationTypesNode_Description"));
        this.setIconBase("org/netbeans/modules/editor/resources/annotationtypes");
    }

    private String getBundleString(String s) {
        return NbBundle.getMessage(AnnotationTypesNode.class, (String)s);
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("editing.configuring.annotations");
    }

    protected SystemAction[] createActions() {
        return new SystemAction[]{SystemAction.get(PropertiesAction.class)};
    }

    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set ps = sheet.get("properties");
        if (ps == null) {
            ps = Sheet.createPropertiesSet();
        }
        ps.put((Node.Property)this.createProperty("backgroundDrawing", Boolean.TYPE));
        ps.put((Node.Property)this.createProperty("backgroundGlyphAlpha", Integer.TYPE));
        ps.put((Node.Property)this.createProperty("combineGlyphs", Boolean.TYPE));
        ps.put((Node.Property)this.createProperty("glyphsOverLineNumbers", Boolean.TYPE));
        ps.put((Node.Property)this.createProperty("showGlyphGutter", Boolean.TYPE));
        sheet.put(ps);
        return sheet;
    }

    private PropertySupport createProperty(final String name, Class clazz) {
        return new PropertySupport.ReadWrite(name, clazz, this.getBundleString("PROP_" + name), this.getBundleString("HINT_" + name)){

            public Object getValue() {
                return AnnotationTypesNode.this.getProperty(name);
            }

            public void setValue(Object value) {
                AnnotationTypesNode.this.setProperty(name, value);
            }

            public boolean supportsDefaultValue() {
                return false;
            }
        };
    }

    private void setProperty(String property, Object value) {
        if (property.equals("backgroundDrawing")) {
            AnnotationTypes.getTypes().setBackgroundDrawing((Boolean)value);
        }
        if (property.equals("backgroundGlyphAlpha")) {
            AnnotationTypes.getTypes().setBackgroundGlyphAlpha(((Integer)value).intValue());
        }
        if (property.equals("combineGlyphs")) {
            AnnotationTypes.getTypes().setCombineGlyphs((Boolean)value);
        }
        if (property.equals("glyphsOverLineNumbers")) {
            AnnotationTypes.getTypes().setGlyphsOverLineNumbers((Boolean)value);
        }
        if (property.equals("showGlyphGutter")) {
            AnnotationTypes.getTypes().setShowGlyphGutter((Boolean)value);
        }
    }

    private Object getProperty(String property) {
        if (property.equals("backgroundDrawing")) {
            return AnnotationTypes.getTypes().isBackgroundDrawing();
        }
        if (property.equals("backgroundGlyphAlpha")) {
            return AnnotationTypes.getTypes().getBackgroundGlyphAlpha();
        }
        if (property.equals("combineGlyphs")) {
            return AnnotationTypes.getTypes().isCombineGlyphs();
        }
        if (property.equals("glyphsOverLineNumbers")) {
            return AnnotationTypes.getTypes().isGlyphsOverLineNumbers();
        }
        if (property.equals("showGlyphGutter")) {
            return AnnotationTypes.getTypes().isShowGlyphGutter();
        }
        return null;
    }

    private static class AnnotationTypesSubnodes
    extends Children.Array {
        private PropertyChangeListener listener;

        public AnnotationTypesSubnodes() {
            this.listener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName() == "annotationTypes") {
                        AnnotationTypesSubnodes.this.nodes = AnnotationTypesSubnodes.this.initCollection();
                        AnnotationTypesSubnodes.this.refresh();
                    }
                }
            };
            AnnotationTypes.getTypes().addPropertyChangeListener(this.listener);
        }

        protected Collection initCollection() {
            AnnotationTypesFolder folder = AnnotationTypesFolder.getAnnotationTypesFolder();
            Iterator types = AnnotationTypes.getTypes().getAnnotationTypeNames();
            LinkedList<AnnotationTypesSubnode> list = new LinkedList<AnnotationTypesSubnode>();
            while (types.hasNext()) {
                String name = (String)types.next();
                AnnotationType type = AnnotationTypes.getTypes().getType(name);
                if (type == null || !type.isVisible()) continue;
                try {
                    list.add(new AnnotationTypesSubnode(type));
                }
                catch (IntrospectionException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            return list;
        }

        private static final class AnnotationTypesSubnode
        extends BeanNode {
            private final URL iconURL;

            public AnnotationTypesSubnode(AnnotationType type) throws IntrospectionException {
                super((Object)new AnnotationTypeOptions(type));
                this.setName(type.getDescription());
                this.iconURL = type.getGlyph();
            }

            public Image getIcon(int type) {
                if (this.iconURL.getProtocol().equals("nbresloc")) {
                    return ImageUtilities.loadImage((String)this.iconURL.getPath().substring(1));
                }
                return Toolkit.getDefaultToolkit().getImage(this.iconURL);
            }

            public boolean canDestroy() {
                return false;
            }

            public HelpCtx getHelpCtx() {
                return new HelpCtx("editing.configuring.annotations");
            }
        }

    }

}

