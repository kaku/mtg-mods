/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationType$CombinationMember
 *  org.netbeans.editor.AnnotationType$Severity
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.XMLDataObject
 *  org.openide.loaders.XMLDataObject$Processor
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.options;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.netbeans.editor.AnnotationType;
import org.netbeans.modules.editor.options.AnnotationTypeActionsFolder;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.XMLDataObject;
import org.openide.util.Exceptions;
import org.xml.sax.AttributeList;
import org.xml.sax.DocumentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.HandlerBase;
import org.xml.sax.InputSource;
import org.xml.sax.Parser;
import org.xml.sax.SAXException;

public class AnnotationTypeProcessor
implements XMLDataObject.Processor,
InstanceCookie {
    static final String DTD_PUBLIC_ID = "-//NetBeans//DTD annotation type 1.0//EN";
    static final String DTD_SYSTEM_ID = "http://www.netbeans.org/dtds/annotation-type-1_0.dtd";
    static final String DTD_PUBLIC_ID11 = "-//NetBeans//DTD annotation type 1.1//EN";
    static final String DTD_SYSTEM_ID11 = "http://www.netbeans.org/dtds/annotation-type-1_1.dtd";
    static final String TAG_TYPE = "type";
    static final String ATTR_TYPE_NAME = "name";
    static final String ATTR_TYPE_LOCALIZING_BUNDLE = "localizing_bundle";
    static final String ATTR_TYPE_DESCRIPTION_KEY = "description_key";
    static final String ATTR_TYPE_VISIBLE = "visible";
    static final String ATTR_TYPE_GLYPH = "glyph";
    static final String ATTR_TYPE_HIGHLIGHT = "highlight";
    static final String ATTR_TYPE_FOREGROUND = "foreground";
    static final String ATTR_TYPE_WAVEUNDERLINE = "waveunderline";
    static final String ATTR_TYPE_TYPE = "type";
    static final String ATTR_TYPE_CONTENTTYPE = "contenttype";
    static final String ATTR_TYPE_ACTIONS = "actions";
    static final String ATTR_ACTION_NAME = "name";
    static final String TAG_COMBINATION = "combination";
    static final String ATTR_COMBINATION_TIPTEXT_KEY = "tiptext_key";
    static final String ATTR_COMBINATION_ORDER = "order";
    static final String ATTR_COMBINATION_MIN_OPTIONALS = "min_optionals";
    static final String TAG_COMBINE = "combine";
    static final String ATTR_COMBINE_ANNOTATIONTYPE = "annotationtype";
    static final String ATTR_COMBINE_ABSORBALL = "absorb_all";
    static final String ATTR_COMBINE_OPTIONAL = "optional";
    static final String ATTR_COMBINE_MIN = "min";
    static final String ATTR_USE_HIHGLIGHT_COLOR = "use_highlight_color";
    static final String ATTR_USE_WAVE_UNDERLINE_COLOR = "use_wave_underline_color";
    static final String ATTR_INHERIT_FOREGROUND_COLOR = "inherit_foreground_color";
    static final String ATTR_USE_CUSTOM_SIDEBAR_COLOR = "use_custom_sidebar_color";
    static final String ATTR_CUSTOM_SIDEBAR_COLOR = "custom_sidebar_color";
    static final String ATTR_SEVERITY = "severity";
    static final String ATTR_BROWSEABLE = "browseable";
    static final String ATTR_PRIORITY = "priority";
    private FileObject xmlDataObject;
    private AnnotationType annotationType;

    public void attachTo(XMLDataObject xmlDO) {
        this.xmlDataObject = xmlDO.getPrimaryFile();
    }

    public void attachTo(FileObject xmlDO) {
        this.xmlDataObject = xmlDO;
    }

    public Object instanceCreate() throws IOException, ClassNotFoundException {
        this.annotationType = null;
        this.parse();
        return this.annotationType;
    }

    public Class instanceClass() {
        return AnnotationType.class;
    }

    public String instanceName() {
        return this.instanceClass().getName();
    }

    private synchronized AnnotationType parse() {
        if (this.annotationType == null) {
            AnnotationType at = new AnnotationType();
            Handler h = new Handler(at);
            try {
                SAXParserFactory factory = SAXParserFactory.newInstance();
                factory.setValidating(false);
                factory.setNamespaceAware(false);
                Parser xp = factory.newSAXParser().getParser();
                xp.setEntityResolver(h);
                xp.setDocumentHandler(h);
                xp.setErrorHandler(h);
                xp.parse(new InputSource(this.xmlDataObject.getInputStream()));
                at.putProp((Object)"file", (Object)this.xmlDataObject);
                this.annotationType = at;
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return this.annotationType;
    }

    private static class Handler
    extends HandlerBase {
        private AnnotationType at;
        private int depth = 0;
        private List combinations;

        Handler(AnnotationType at) {
            this.at = at;
        }

        private void rethrow(Exception e) throws SAXException {
            throw new SAXException(e);
        }

        @Override
        public void startElement(String name, AttributeList amap) throws SAXException {
            switch (this.depth++) {
                case 0: {
                    if (!"type".equals(name)) {
                        throw new SAXException("malformed AnnotationType xml file");
                    }
                    this.at.setName(amap.getValue("name"));
                    if (amap.getValue("type") == null) {
                        this.at.setWholeLine(true);
                    } else {
                        this.at.setWholeLine("line".equals(amap.getValue("type")));
                    }
                    if (amap.getValue("visible") == null) {
                        this.at.setVisible(true);
                    } else {
                        this.at.setVisible(amap.getValue("visible"));
                    }
                    if (this.at.isVisible()) {
                        String localizer = amap.getValue("localizing_bundle");
                        String key = amap.getValue("description_key");
                        this.at.putProp((Object)"bundle", (Object)localizer);
                        this.at.putProp((Object)"desciptionKey", (Object)key);
                    }
                    String useHighlightString = amap.getValue("use_highlight_color");
                    String useWaveUnderlineString = amap.getValue("use_wave_underline_color");
                    String inheritForeString = amap.getValue("inherit_foreground_color");
                    String useCustomSidebarColor = amap.getValue("use_custom_sidebar_color");
                    this.at.setUseHighlightColor(Boolean.valueOf(useHighlightString).booleanValue());
                    this.at.setUseWaveUnderlineColor(Boolean.valueOf(useWaveUnderlineString).booleanValue());
                    this.at.setInheritForegroundColor(Boolean.valueOf(inheritForeString).booleanValue());
                    this.at.setUseCustomSidebarColor(Boolean.valueOf(useCustomSidebarColor).booleanValue());
                    try {
                        String color = amap.getValue("highlight");
                        if (color != null) {
                            this.at.setHighlight(Color.decode(color));
                            if (useHighlightString == null) {
                                this.at.setUseHighlightColor(true);
                            }
                        } else if (useHighlightString == null) {
                            this.at.setUseHighlightColor(false);
                        }
                        if ((color = amap.getValue("foreground")) != null) {
                            this.at.setForegroundColor(Color.decode(color));
                            if (inheritForeString == null) {
                                this.at.setInheritForegroundColor(false);
                            }
                        } else if (inheritForeString == null) {
                            this.at.setInheritForegroundColor(true);
                        }
                        if ((color = amap.getValue("waveunderline")) != null) {
                            this.at.setWaveUnderlineColor(Color.decode(color));
                            if (useWaveUnderlineString == null) {
                                this.at.setUseWaveUnderlineColor(true);
                            }
                        } else if (useWaveUnderlineString == null) {
                            this.at.setUseWaveUnderlineColor(false);
                        }
                        if ((color = amap.getValue("custom_sidebar_color")) != null) {
                            this.at.setCustomSidebarColor(Color.decode(color));
                            if (useCustomSidebarColor == null) {
                                this.at.setUseCustomSidebarColor(true);
                            }
                        } else if (useCustomSidebarColor == null) {
                            this.at.setUseCustomSidebarColor(false);
                        }
                    }
                    catch (NumberFormatException ex) {
                        this.rethrow(ex);
                    }
                    try {
                        String uri = amap.getValue("glyph");
                        if (uri != null) {
                            this.at.setGlyph(new URL(uri));
                        }
                    }
                    catch (MalformedURLException ex) {
                        this.rethrow(ex);
                    }
                    String actions = amap.getValue("actions");
                    if (actions != null) {
                        AnnotationTypeActionsFolder.readActions(this.at, actions);
                        this.at.putProp((Object)"actionsFolder", (Object)actions);
                    }
                    this.at.setSeverity(AnnotationType.Severity.valueOf((String)amap.getValue("severity")));
                    this.at.setBrowseable(Boolean.valueOf(amap.getValue("browseable")).booleanValue());
                    String priorityString = amap.getValue("priority");
                    int priority = 0;
                    if (priorityString != null) {
                        try {
                            priority = Integer.parseInt(priorityString);
                        }
                        catch (NumberFormatException e) {
                            Logger.getLogger("global").log(Level.INFO, null, e);
                        }
                    }
                    this.at.setPriority(priority);
                    break;
                }
                case 1: {
                    String order;
                    String min;
                    if (!"combination".equals(name)) {
                        throw new SAXException("malformed AnnotationType xml file");
                    }
                    this.combinations = new ArrayList();
                    String key = amap.getValue("tiptext_key");
                    if (key != null) {
                        this.at.putProp((Object)"tooltipTextKey", (Object)key);
                    }
                    if ((order = amap.getValue("order")) != null) {
                        this.at.setCombinationOrder(order);
                    }
                    if ((min = amap.getValue("min_optionals")) == null) break;
                    this.at.setMinimumOptionals(min);
                    break;
                }
                case 2: {
                    this.combinations.add(new AnnotationType.CombinationMember(amap.getValue("annotationtype"), amap.getValue("absorb_all") == null ? false : "true".equals(amap.getValue("absorb_all")), amap.getValue("optional") == null ? false : "true".equals(amap.getValue("optional")), amap.getValue("min")));
                    break;
                }
                default: {
                    throw new SAXException("malformed AnnotationType xml file");
                }
            }
        }

        @Override
        public void endElement(String name) throws SAXException {
            if (--this.depth == 1) {
                AnnotationType.CombinationMember[] combs = new AnnotationType.CombinationMember[this.combinations.size()];
                this.combinations.toArray((T[])combs);
                this.at.setCombinations(combs);
            }
        }

        @Override
        public InputSource resolveEntity(String pid, String sid) throws SAXException {
            if ("-//NetBeans//DTD annotation type 1.0//EN".equals(pid)) {
                return new InputSource(new ByteArrayInputStream(new byte[0]));
            }
            if ("-//NetBeans//DTD annotation type 1.1//EN".equals(pid)) {
                return new InputSource(new ByteArrayInputStream(new byte[0]));
            }
            return new InputSource(sid);
        }
    }

}

