/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationType$CombinationMember
 *  org.netbeans.editor.AnnotationType$Severity
 *  org.netbeans.editor.AnnotationTypes
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.FolderInstance
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.editor.options;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.AnnotationTypes;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.FolderInstance;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AnnotationTypesFolder
extends FolderInstance {
    private static final String FOLDER = "Editors/AnnotationTypes";
    private static AnnotationTypesFolder folder;
    private Map annotationTypes;

    private AnnotationTypesFolder(FileObject fo, DataFolder fld) {
        super(fld);
        this.recreate();
        this.instanceFinished();
        fo.addFileChangeListener((FileChangeListener)new FileChangeAdapter(){

            public void fileDeleted(FileEvent fe) {
                Iterator it = AnnotationTypes.getTypes().getAnnotationTypeNames();
                while (it.hasNext()) {
                    AnnotationType type = AnnotationTypes.getTypes().getType((String)it.next());
                    if (type == null || !((FileObject)type.getProp("file")).equals((Object)fe.getFile())) continue;
                    AnnotationTypes.getTypes().removeType(type.getName());
                    break;
                }
            }
        });
    }

    public static synchronized AnnotationTypesFolder getAnnotationTypesFolder() {
        if (folder != null) {
            return folder;
        }
        FileObject f = FileUtil.getConfigFile((String)"Editors/AnnotationTypes");
        if (f == null) {
            return null;
        }
        try {
            DataObject d = DataObject.find((FileObject)f);
            DataFolder df = (DataFolder)d.getCookie(DataFolder.class);
            if (df != null) {
                folder = new AnnotationTypesFolder(f, df);
            }
        }
        catch (DataObjectNotFoundException ex) {
            Logger.getLogger("global").log(Level.INFO, null, (Throwable)ex);
            return null;
        }
        return folder;
    }

    protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
        this.annotationTypes = new HashMap(cookies.length * 4 / 3 + 1);
        for (int i = 0; i < cookies.length; ++i) {
            Object o = cookies[i].instanceCreate();
            if (!(o instanceof AnnotationType)) continue;
            AnnotationType type = (AnnotationType)o;
            this.annotationTypes.put(type.getName(), type);
        }
        AnnotationTypes.getTypes().setTypes(this.annotationTypes);
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void saveAnnotationType(AnnotationType type) {
        FileObject fo = (FileObject)type.getProp("file");
        Document doc = XMLUtil.createDocument((String)"type", (String)null, (String)"-//NetBeans//DTD annotation type 1.0//EN", (String)"http://www.netbeans.org/dtds/annotation-type-1_0.dtd");
        Element typeElem = doc.getDocumentElement();
        typeElem.setAttribute("name", type.getName());
        if (type.getProp("bundle") != null) {
            typeElem.setAttribute("localizing_bundle", (String)type.getProp("bundle"));
        }
        if (type.getProp("desciptionKey") != null) {
            typeElem.setAttribute("description_key", (String)type.getProp("desciptionKey"));
        }
        typeElem.setAttribute("visible", type.isVisible() ? "true" : "false");
        typeElem.setAttribute("use_highlight_color", type.isUseHighlightColor() ? "true" : "false");
        typeElem.setAttribute("use_wave_underline_color", type.isUseWaveUnderlineColor() ? "true" : "false");
        typeElem.setAttribute("inherit_foreground_color", type.isInheritForegroundColor() ? "true" : "false");
        typeElem.setAttribute("use_custom_sidebar_color", type.isUseCustomSidebarColor() ? "true" : "false");
        typeElem.setAttribute("type", type.isWholeLine() ? "line" : "linepart");
        if (type.getProp("glyph") != null) {
            typeElem.setAttribute("glyph", type.getGlyph().toExternalForm());
        }
        if (type.getProp("highlight") != null) {
            typeElem.setAttribute("highlight", "0x" + Integer.toHexString(type.getHighlight().getRGB() & 16777215));
        }
        if (type.getProp("waveunderline") != null) {
            typeElem.setAttribute("waveunderline", "0x" + Integer.toHexString(type.getWaveUnderlineColor().getRGB() & 16777215));
        }
        if (type.getProp("foreground") != null) {
            typeElem.setAttribute("foreground", "0x" + Integer.toHexString(type.getForegroundColor().getRGB() & 16777215));
        }
        if (type.getProp("customSidebarColor") != null) {
            typeElem.setAttribute("custom_sidebar_color", "0x" + Integer.toHexString(type.getCustomSidebarColor().getRGB() & 16777215));
        }
        if (type.getProp("actionsFolder") != null) {
            typeElem.setAttribute("actions", (String)type.getProp("actionsFolder"));
        }
        if (type.getCombinations() != null) {
            Element combsElem = doc.createElement("combination");
            combsElem.setAttribute("tiptext_key", (String)type.getProp("tooltipTextKey"));
            if (type.getProp("combinationOrder") != null) {
                combsElem.setAttribute("order", "" + type.getCombinationOrder());
            }
            if (type.getProp("combinationMinimumOptionals") != null) {
                combsElem.setAttribute("min_optionals", "" + type.getMinimumOptionals());
            }
            typeElem.appendChild(combsElem);
            AnnotationType.CombinationMember[] combs = type.getCombinations();
            for (int i = 0; i < combs.length; ++i) {
                Element combElem = doc.createElement("combine");
                combElem.setAttribute("annotationtype", combs[i].getName());
                combElem.setAttribute("absorb_all", combs[i].isAbsorbAll() ? "true" : "false");
                combElem.setAttribute("optional", combs[i].isOptional() ? "true" : "false");
                if (combs[i].getMinimumCount() > 0) {
                    combElem.setAttribute("min", "" + combs[i].getMinimumCount());
                }
                combsElem.appendChild(combElem);
            }
        }
        typeElem.setAttribute("severity", type.getSeverity().getName());
        typeElem.setAttribute("browseable", Boolean.toString(type.isBrowseable()));
        typeElem.setAttribute("priority", Integer.toString(type.getPriority()));
        doc.getDocumentElement().normalize();
        try {
            FileLock lock = fo.lock();
            OutputStream os = null;
            try {
                os = fo.getOutputStream(lock);
                XMLUtil.write((Document)doc, (OutputStream)os, (String)"UTF-8");
            }
            catch (Exception ex) {
                Logger.getLogger("global").log(Level.INFO, null, ex);
            }
            finally {
                if (os != null) {
                    try {
                        os.close();
                    }
                    catch (IOException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
                lock.releaseLock();
            }
        }
        catch (IOException ex) {
            Logger.getLogger("global").log(Level.INFO, null, ex);
        }
    }

}

