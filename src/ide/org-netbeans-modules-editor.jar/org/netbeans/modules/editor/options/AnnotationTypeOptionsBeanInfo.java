/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.options;

import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import java.util.ResourceBundle;
import org.netbeans.modules.editor.options.AnnotationTypeOptions;
import org.openide.util.NbBundle;

public class AnnotationTypeOptionsBeanInfo
extends SimpleBeanInfo {
    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] descriptors;
        try {
            descriptors = new PropertyDescriptor[]{new PropertyDescriptor("highlightColor", AnnotationTypeOptions.class), new PropertyDescriptor("useHighlightColor", AnnotationTypeOptions.class), new PropertyDescriptor("foregroundColor", AnnotationTypeOptions.class), new PropertyDescriptor("inheritForegroundColor", AnnotationTypeOptions.class), new PropertyDescriptor("waveUnderlineColor", AnnotationTypeOptions.class), new PropertyDescriptor("useWaveUnderlineColor", AnnotationTypeOptions.class), new PropertyDescriptor("wholeLine", AnnotationTypeOptions.class, "isWholeLine", null)};
            ResourceBundle bundle = NbBundle.getBundle(AnnotationTypeOptionsBeanInfo.class);
            descriptors[0].setDisplayName(bundle.getString("PROP_AT_HIGHLIGHT"));
            descriptors[0].setShortDescription(bundle.getString("HINT_AT_HIGHLIGHT"));
            descriptors[1].setDisplayName(bundle.getString("PROP_AT_USE_HIGHLIGHT"));
            descriptors[1].setShortDescription(bundle.getString("HINT_AT_USE_HIGHLIGHT"));
            descriptors[2].setDisplayName(bundle.getString("PROP_AT_FOREGROUND"));
            descriptors[2].setShortDescription(bundle.getString("HINT_AT_FOREGROUND"));
            descriptors[3].setDisplayName(bundle.getString("PROP_AT_INHERIT_FOREGROUND"));
            descriptors[3].setShortDescription(bundle.getString("HINT_AT_INHERIT_FOREGROUND"));
            descriptors[4].setDisplayName(bundle.getString("PROP_AT_WAVEUNDERLINE"));
            descriptors[4].setShortDescription(bundle.getString("HINT_AT_WAVEUNDERLINE"));
            descriptors[5].setDisplayName(bundle.getString("PROP_AT_USE_WAVEUNDERLINE"));
            descriptors[5].setShortDescription(bundle.getString("HINT_AT_USE_WAVEUNDERLINE"));
            descriptors[6].setDisplayName(bundle.getString("PROP_AT_WHOLELINE"));
            descriptors[6].setShortDescription(bundle.getString("HINT_AT_WHOLELINE"));
        }
        catch (Exception e) {
            descriptors = new PropertyDescriptor[]{};
        }
        return descriptors;
    }
}

