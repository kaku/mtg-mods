/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationType
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.FolderInstance
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package org.netbeans.modules.editor.options;

import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.editor.AnnotationType;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.FolderInstance;
import org.openide.nodes.Node;

public class AnnotationTypeActionsFolder
extends FolderInstance {
    private static final String FOLDER = "Editors/AnnotationTypes/";
    private AnnotationType type;

    private AnnotationTypeActionsFolder(AnnotationType type, DataFolder fld) {
        super(fld);
        this.type = type;
        this.recreate();
        this.instanceFinished();
    }

    public static boolean readActions(AnnotationType type, String subFolder) {
        FileObject f = FileUtil.getConfigFile((String)("Editors/AnnotationTypes/" + subFolder));
        if (f == null) {
            return false;
        }
        try {
            DataObject d = DataObject.find((FileObject)f);
            DataFolder df = (DataFolder)d.getCookie(DataFolder.class);
            if (df != null) {
                AnnotationTypeActionsFolder folder = new AnnotationTypeActionsFolder(type, df);
                return true;
            }
        }
        catch (DataObjectNotFoundException ex) {
            Logger.getLogger("global").log(Level.INFO, null, (Throwable)ex);
            return false;
        }
        return false;
    }

    protected Object createInstance(InstanceCookie[] cookies) throws IOException, ClassNotFoundException {
        LinkedList<Action> annotationActions = new LinkedList<Action>();
        for (int i = 0; i < cookies.length; ++i) {
            if (!AnnotationTypeActionsFolder.isAction(cookies[i])) continue;
            Action action = (Action)cookies[i].instanceCreate();
            annotationActions.add(action);
        }
        this.type.setActions(annotationActions.toArray(new Action[0]));
        return null;
    }

    private static boolean isAction(InstanceCookie ic) {
        if (ic instanceof InstanceCookie.Of) {
            return ((InstanceCookie.Of)ic).instanceOf(Action.class);
        }
        return Action.class.isAssignableFrom(ic.getClass());
    }
}

