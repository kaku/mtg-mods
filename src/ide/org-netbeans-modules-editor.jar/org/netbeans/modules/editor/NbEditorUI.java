/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.ActionPerformer
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.NbEditorToolBar;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.impl.CustomizableSideBar;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.ActionPerformer;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class NbEditorUI
extends EditorUI {
    private FocusListener focusL;
    private boolean attached = false;
    private ChangeListener listener;
    private static final RequestProcessor WORKER = new RequestProcessor(NbEditorUI.class.getName(), 1, false, false);
    private static final LinkedHashSet<FileObject> objectsToRefresh = new LinkedHashSet();
    private static final Object lock = new Object();
    private static final RequestProcessor.Task TASK = WORKER.create(new Runnable(){

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            FileObject fo;
            do {
                Object object = lock;
                synchronized (object) {
                    Iterator iterator = objectsToRefresh.iterator();
                    if (iterator.hasNext()) {
                        fo = (FileObject)iterator.next();
                        objectsToRefresh.remove((Object)fo);
                    } else {
                        fo = null;
                    }
                }
                if (fo == null) continue;
                fo.refresh();
            } while (fo != null);
        }
    });

    protected SystemActionUpdater createSystemActionUpdater(String editorActionName, boolean updatePerformer, boolean syncEnabling) {
        return new SystemActionUpdater(editorActionName, updatePerformer, syncEnabling);
    }

    public NbEditorUI() {
        this.focusL = new FocusAdapter(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void focusGained(FocusEvent evt) {
                FileObject fo;
                DataObject dob;
                BaseDocument doc = NbEditorUI.this.getDocument();
                if (doc != null && (dob = NbEditorUtilities.getDataObject((Document)doc)) != null && (fo = dob.getPrimaryFile()) != null) {
                    Object object = lock;
                    synchronized (object) {
                        objectsToRefresh.add(fo);
                    }
                    TASK.schedule(0);
                }
            }
        };
    }

    private static Lookup getContextLookup(Component component) {
        Lookup lookup = null;
        for (Component c = component; !(c == null || c instanceof Lookup.Provider && (lookup = ((Lookup.Provider)c).getLookup()) != null); c = c.getParent()) {
        }
        return lookup;
    }

    protected void attachSystemActionPerformer(String editorActionName) {
        new SystemActionPerformer(editorActionName);
    }

    protected void installUI(JTextComponent c) {
        super.installUI(c);
        if (!this.attached) {
            this.attachSystemActionPerformer("find");
            this.attachSystemActionPerformer("replace");
            this.attachSystemActionPerformer("goto");
            this.attachSystemActionPerformer("show-popup-menu");
            this.attached = true;
        }
        c.addFocusListener(this.focusL);
    }

    protected void uninstallUI(JTextComponent c) {
        super.uninstallUI(c);
        c.removeFocusListener(this.focusL);
    }

    protected int textLimitWidth() {
        int textLimit;
        BaseDocument doc = this.getDocument();
        if (doc != null && (textLimit = CodeStylePreferences.get((Document)doc).getPreferences().getInt("text-limit-width", 80)) > 0) {
            return textLimit;
        }
        return super.textLimitWidth();
    }

    protected JComponent createExtComponent() {
        JTextComponent component = this.getComponent();
        JScrollPane scroller = new JScrollPane(component);
        scroller.getViewport().setMinimumSize(new Dimension(4, 4));
        Border empty = BorderFactory.createEmptyBorder();
        scroller.setBorder(empty);
        scroller.setViewportBorder(empty);
        if (component.getClientProperty("nbeditorui.vScrollPolicy") != null) {
            scroller.setVerticalScrollBarPolicy((Integer)component.getClientProperty("nbeditorui.vScrollPolicy"));
        }
        if (component.getClientProperty("nbeditorui.hScrollPolicy") != null) {
            scroller.setHorizontalScrollBarPolicy((Integer)component.getClientProperty("nbeditorui.hScrollPolicy"));
        }
        JPanel ec = new JPanel(new BorderLayout());
        ec.putClientProperty(JTextComponent.class, component);
        ec.add(scroller);
        CustomizableSideBar.resetSideBars(component);
        Map<CustomizableSideBar.SideBarPosition, JComponent> sideBars = CustomizableSideBar.getSideBars(component);
        NbEditorUI.processSideBars(sideBars, ec, scroller);
        if (this.listener == null) {
            this.listener = new SideBarsListener(component);
            CustomizableSideBar.addChangeListener(NbEditorUtilities.getMimeType(component), this.listener);
        }
        this.initGlyphCorner(scroller);
        return ec;
    }

    public boolean isLineNumberEnabled() {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        return prefs.getBoolean("line-number-visible", true);
    }

    public void setLineNumberEnabled(boolean lineNumberEnabled) {
        Preferences prefs;
        boolean visible = (prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class)).getBoolean("line-number-visible", true);
        prefs.putBoolean("line-number-visible", !visible);
    }

    private static void processSideBars(Map sideBars, JComponent ec, JScrollPane scroller) {
        ec.removeAll();
        ec.add(scroller);
        scroller.setRowHeader(null);
        scroller.setColumnHeaderView(null);
        for (Map.Entry entry : sideBars.entrySet()) {
            CustomizableSideBar.SideBarPosition position = (CustomizableSideBar.SideBarPosition)entry.getKey();
            JComponent sideBar = (JComponent)entry.getValue();
            if (position.isScrollable()) {
                if (position.getPosition() == 1) {
                    scroller.setRowHeaderView(sideBar);
                    continue;
                }
                if (position.getPosition() == 2) {
                    scroller.setColumnHeaderView(sideBar);
                    continue;
                }
                throw new IllegalArgumentException("Unsupported side bar position, scrollable = true, position=" + position.getBorderLayoutPosition());
            }
            ec.add((Component)sideBar, position.getBorderLayoutPosition());
        }
    }

    protected JToolBar createToolBarComponent() {
        return new NbEditorToolBar(this.getComponent());
    }

    private static final class SideBarsListener
    implements ChangeListener {
        private final JTextComponent component;

        public SideBarsListener(JTextComponent component) {
            this.component = component;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    JComponent ec;
                    EditorUI eui = Utilities.getEditorUI((JTextComponent)SideBarsListener.this.component);
                    if (eui != null && (ec = eui.getExtComponent()) != null) {
                        JScrollPane scroller = (JScrollPane)ec.getComponent(0);
                        ec.removeAll();
                        scroller.setRowHeaderView(null);
                        scroller.setColumnHeaderView(null);
                        Map<CustomizableSideBar.SideBarPosition, JComponent> newMap = CustomizableSideBar.getSideBars(SideBarsListener.this.component);
                        NbEditorUI.processSideBars(newMap, ec, scroller);
                        ec.revalidate();
                        ec.repaint();
                    }
                }
            });
        }

    }

    static class EnabledPropertySyncListener
    implements PropertyChangeListener {
        Action action;

        EnabledPropertySyncListener(Action actionToBeSynced) {
            this.action = actionToBeSynced;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("enabled".equals(evt.getPropertyName())) {
                this.action.setEnabled((Boolean)evt.getNewValue());
            }
        }
    }

    public final class SystemActionUpdater
    implements PropertyChangeListener,
    ActionPerformer {
        private String editorActionName;
        private boolean updatePerformer;
        private boolean syncEnabling;
        private Action editorAction;
        private Action systemAction;
        private PropertyChangeListener enabledPropertySyncL;
        private boolean listeningOnTCRegistry;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        SystemActionUpdater(String editorActionName, boolean updatePerformer, boolean syncEnabling) {
            this.editorActionName = editorActionName;
            this.updatePerformer = updatePerformer;
            this.syncEnabling = syncEnabling;
            Object object = NbEditorUI.this.getComponentLock();
            synchronized (object) {
                JTextComponent component = NbEditorUI.this.getComponent();
                if (component != null) {
                    this.propertyChange(new PropertyChangeEvent((Object)NbEditorUI.this, "component", null, component));
                }
                NbEditorUI.this.addPropertyChangeListener((PropertyChangeListener)this);
            }
        }

        public void editorActivated() {
            Action ea = this.getEditorAction();
            Action sa = this.getSystemAction();
            if (ea != null && sa != null) {
                if (this.updatePerformer && ea.isEnabled() && sa instanceof CallbackSystemAction) {
                    ((CallbackSystemAction)sa).setActionPerformer((ActionPerformer)this);
                }
                if (this.syncEnabling) {
                    if (this.enabledPropertySyncL == null) {
                        this.enabledPropertySyncL = new EnabledPropertySyncListener(sa);
                    }
                    ea.addPropertyChangeListener(this.enabledPropertySyncL);
                }
            }
        }

        public void editorDeactivated() {
            Action ea = this.getEditorAction();
            Action sa = this.getSystemAction();
            if (ea != null && sa != null && this.syncEnabling && this.enabledPropertySyncL != null) {
                ea.removePropertyChangeListener(this.enabledPropertySyncL);
            }
        }

        private void reset() {
            if (this.enabledPropertySyncL != null) {
                this.editorAction.removePropertyChangeListener(this.enabledPropertySyncL);
            }
            this.editorAction = null;
            this.systemAction = null;
            this.enabledPropertySyncL = null;
        }

        public void performAction(SystemAction action) {
            JTextComponent component = NbEditorUI.this.getComponent();
            Action ea = this.getEditorAction();
            if (component != null && ea != null) {
                ea.actionPerformed(new ActionEvent(component, 0, ""));
            }
        }

        private void startTCRegistryListening() {
            if (!this.listeningOnTCRegistry) {
                this.listeningOnTCRegistry = true;
                TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
            }
        }

        private void stopTCRegistryListening() {
            if (this.listeningOnTCRegistry) {
                this.listeningOnTCRegistry = false;
                TopComponent.getRegistry().removePropertyChangeListener((PropertyChangeListener)this);
            }
        }

        @Override
        public synchronized void propertyChange(PropertyChangeEvent evt) {
            String propName = evt.getPropertyName();
            if ("activated".equals(propName)) {
                TopComponent activated = (TopComponent)evt.getNewValue();
                if (activated instanceof CloneableEditorSupport.Pane) {
                    this.editorActivated();
                } else {
                    this.editorDeactivated();
                }
            } else if ("component".equals(propName)) {
                JTextComponent component = (JTextComponent)evt.getNewValue();
                if (component != null) {
                    component.addPropertyChangeListener(this);
                    if (component.isDisplayable()) {
                        this.startTCRegistryListening();
                    }
                } else {
                    component = (JTextComponent)evt.getOldValue();
                    component.removePropertyChangeListener(this);
                    this.stopTCRegistryListening();
                }
                this.reset();
            } else if ("editorKit".equals(propName)) {
                this.reset();
            } else if ("ancestor".equals(propName)) {
                if (((Component)evt.getSource()).isDisplayable()) {
                    this.startTCRegistryListening();
                } else {
                    this.stopTCRegistryListening();
                }
            }
        }

        private synchronized Action getEditorAction() {
            BaseKit kit;
            if (this.editorAction == null && (kit = Utilities.getKit((JTextComponent)NbEditorUI.this.getComponent())) != null) {
                this.editorAction = kit.getActionByName(this.editorActionName);
            }
            return this.editorAction;
        }

        private Action getSystemAction() {
            String saClassName;
            Action ea;
            if (this.systemAction == null && (ea = this.getEditorAction()) != null && (saClassName = (String)ea.getValue("systemActionClassName")) != null) {
                Class saClass;
                try {
                    saClass = Class.forName(saClassName);
                }
                catch (Throwable t) {
                    saClass = null;
                }
                if (saClass != null) {
                    this.systemAction = SystemAction.get(saClass);
                }
            }
            return this.systemAction;
        }

        protected void finalize() throws Throwable {
            this.reset();
        }
    }

    private class SystemActionPerformer
    implements PropertyChangeListener {
        private String editorActionName;
        private Action editorAction;
        private Action systemAction;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        SystemActionPerformer(String editorActionName) {
            this.editorActionName = editorActionName;
            Object object = NbEditorUI.this.getComponentLock();
            synchronized (object) {
                JTextComponent component = NbEditorUI.this.getComponent();
                if (component != null) {
                    this.propertyChange(new PropertyChangeEvent((Object)NbEditorUI.this, "component", null, component));
                }
                NbEditorUI.this.addPropertyChangeListener((PropertyChangeListener)this);
            }
        }

        private void attachSystemActionPerformer(JTextComponent c) {
            if (c == null) {
                return;
            }
            Action action = this.getEditorAction(c);
            if (action == null) {
                return;
            }
            Action globalSystemAction = this.getSystemAction(c);
            if (globalSystemAction == null) {
                return;
            }
            if (globalSystemAction instanceof CallbackSystemAction) {
                Object key = ((CallbackSystemAction)globalSystemAction).getActionMapKey();
                c.getActionMap().put(key, action);
            }
        }

        private void detachSystemActionPerformer(JTextComponent c) {
            if (c == null) {
                return;
            }
            Action action = this.getEditorAction(c);
            if (action == null) {
                return;
            }
            Action globalSystemAction = this.getSystemAction(c);
            if (globalSystemAction == null) {
                return;
            }
            if (globalSystemAction instanceof CallbackSystemAction) {
                Action ea;
                Object key = ((CallbackSystemAction)globalSystemAction).getActionMapKey();
                ActionMap am = c.getActionMap();
                if (am != null && action.equals(ea = am.get(key))) {
                    am.remove(key);
                }
            }
        }

        @Override
        public synchronized void propertyChange(PropertyChangeEvent evt) {
            String propName = evt.getPropertyName();
            if ("component".equals(propName)) {
                JTextComponent component = (JTextComponent)evt.getNewValue();
                if (component != null) {
                    component.addPropertyChangeListener(this);
                    this.attachSystemActionPerformer(component);
                } else {
                    component = (JTextComponent)evt.getOldValue();
                    component.removePropertyChangeListener(this);
                    this.detachSystemActionPerformer(component);
                }
            }
        }

        private synchronized Action getEditorAction(JTextComponent component) {
            BaseKit kit;
            if (this.editorAction == null && (kit = Utilities.getKit((JTextComponent)component)) != null) {
                this.editorAction = kit.getActionByName(this.editorActionName);
            }
            return this.editorAction;
        }

        private Action getSystemAction(JTextComponent c) {
            String saClassName;
            Action ea;
            if (this.systemAction == null && (ea = this.getEditorAction(c)) != null && (saClassName = (String)ea.getValue("systemActionClassName")) != null) {
                Class saClass;
                try {
                    saClass = Class.forName(saClassName);
                }
                catch (Throwable t) {
                    saClass = null;
                }
                if (saClass != null) {
                    Lookup lookup;
                    this.systemAction = SystemAction.get(saClass);
                    if (this.systemAction instanceof ContextAwareAction && (lookup = NbEditorUI.getContextLookup(c)) != null) {
                        this.systemAction = ((ContextAwareAction)this.systemAction).createContextAwareInstance(lookup);
                    }
                }
            }
            return this.systemAction;
        }
    }

}

