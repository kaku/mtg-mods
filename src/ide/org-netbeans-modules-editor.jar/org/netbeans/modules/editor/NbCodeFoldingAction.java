/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 */
package org.netbeans.modules.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseKit;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorKit;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

public class NbCodeFoldingAction
implements Presenter.Menu {
    public final HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public String getName() {
        return NbBundle.getBundle(NbCodeFoldingAction.class).getString("Menu/View/CodeFolds");
    }

    public boolean isEnabled() {
        return false;
    }

    public JMenuItem getMenuPresenter() {
        return new CodeFoldsMenu(this, this.getName());
    }

    private static JTextComponent getComponent() {
        return org.netbeans.editor.Utilities.getFocusedComponent();
    }

    public void actionPerformed(ActionEvent ev) {
    }

    private BaseKit getKit() {
        JTextComponent component = NbCodeFoldingAction.getComponent();
        return component == null ? BaseKit.getKit(NbEditorKit.class) : org.netbeans.editor.Utilities.getKit((JTextComponent)component);
    }

    public class CodeFoldsMenu
    extends JMenu
    implements DynamicMenuContent {
        final /* synthetic */ NbCodeFoldingAction this$0;

        public CodeFoldsMenu(NbCodeFoldingAction nbCodeFoldingAction) {
            this.this$0 = nbCodeFoldingAction;
        }

        public CodeFoldsMenu(NbCodeFoldingAction nbCodeFoldingAction, String s) {
            this.this$0 = nbCodeFoldingAction;
            super(s);
            Mnemonics.setLocalizedText((AbstractButton)this, (String)s);
        }

        public JComponent[] getMenuPresenters() {
            return new JComponent[]{this};
        }

        public JComponent[] synchMenuPresenters(JComponent[] items) {
            this.getPopupMenu();
            return items;
        }

        @Override
        public JPopupMenu getPopupMenu() {
            Action action;
            JPopupMenu pm = super.getPopupMenu();
            pm.removeAll();
            boolean enable = false;
            BaseKit bKit = this.this$0.getKit();
            if (bKit == null) {
                bKit = BaseKit.getKit(NbEditorKit.class);
            }
            if (bKit != null && (action = bKit.getActionByName("generate-fold-popup")) instanceof BaseAction) {
                JMenu menu;
                ActionMap contextActionmap;
                JTextComponent component = NbCodeFoldingAction.getComponent();
                MimePath mimePath = component == null ? MimePath.EMPTY : MimePath.parse((String)DocumentUtilities.getMimeType((JTextComponent)component));
                Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
                boolean foldingAvailable = prefs.getBoolean("code-folding-enable", true);
                if (foldingAvailable && (contextActionmap = (ActionMap)Utilities.actionsGlobalContext().lookup(ActionMap.class)) != null) {
                    Action defaultAction;
                    boolean bl = foldingAvailable = contextActionmap.get("collapse-fold") != null && component != null;
                    if (!foldingAvailable && (bKit = BaseKit.getKit(NbEditorKit.class)) != null && (defaultAction = bKit.getActionByName("generate-fold-popup")) instanceof BaseAction) {
                        action = defaultAction;
                    }
                }
                if ((menu = (JMenu)((BaseAction)action).getPopupMenuItem(foldingAvailable ? component : null)) != null) {
                    Component[] comps = menu.getMenuComponents();
                    for (int i = 0; i < comps.length; ++i) {
                        pm.add(comps[i]);
                        if (!comps[i].isEnabled() || comps[i] instanceof JSeparator) continue;
                        enable = true;
                    }
                }
            }
            this.setEnabled(enable);
            pm.pack();
            return pm;
        }
    }

}

