/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationTypes
 *  org.netbeans.editor.AnnotationTypes$Loader
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.LocaleSupport
 *  org.netbeans.editor.LocaleSupport$Localizer
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.editor.lib.EditorPackageAccessor
 *  org.netbeans.modules.editor.lib2.actions.EditorRegistryWatcher
 *  org.netbeans.modules.editor.lib2.document.ReadWriteUtils
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataLoaderPool
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.OperationEvent
 *  org.openide.loaders.OperationEvent$Copy
 *  org.openide.loaders.OperationEvent$Move
 *  org.openide.loaders.OperationEvent$Rename
 *  org.openide.loaders.OperationListener
 *  org.openide.modules.ModuleInstall
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditor
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.ClipboardListener
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.editor;

import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.rtf.RTFEditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.LocaleSupport;
import org.netbeans.modules.editor.NbLocalizer;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistory;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.lib.EditorPackageAccessor;
import org.netbeans.modules.editor.lib2.actions.EditorRegistryWatcher;
import org.netbeans.modules.editor.lib2.document.ReadWriteUtils;
import org.netbeans.modules.editor.options.AnnotationTypesFolder;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataLoaderPool;
import org.openide.loaders.DataObject;
import org.openide.loaders.OperationEvent;
import org.openide.loaders.OperationListener;
import org.openide.modules.ModuleInstall;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditor;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ClipboardListener;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class EditorModule
extends ModuleInstall {
    private static final Logger LOG = Logger.getLogger(EditorModule.class.getName());
    private static final boolean debug = Boolean.getBoolean("netbeans.debug.editor.kits");
    private PropertyChangeListener topComponentRegistryListener;

    public void restored() {
        LocaleSupport.addLocalizer((LocaleSupport.Localizer)new NbLocalizer(BaseKit.class));
        AnnotationTypes.getTypes().registerLoader(new AnnotationTypes.Loader(){

            public void loadTypes() {
                AnnotationTypesFolder.getAnnotationTypesFolder();
            }

            public void loadSettings() {
                Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
                int i = prefs.getInt("backgroundGlyphAlpha", Integer.MIN_VALUE);
                if (i != Integer.MIN_VALUE) {
                    AnnotationTypes.getTypes().setBackgroundGlyphAlpha(i);
                }
                boolean b = prefs.getBoolean("backgroundDrawing", false);
                AnnotationTypes.getTypes().setBackgroundDrawing(Boolean.valueOf(b));
                b = prefs.getBoolean("combineGlyphs", true);
                AnnotationTypes.getTypes().setCombineGlyphs(Boolean.valueOf(b));
                b = prefs.getBoolean("glyphsOverLineNumbers", true);
                AnnotationTypes.getTypes().setGlyphsOverLineNumbers(Boolean.valueOf(b));
                b = prefs.getBoolean("showGlyphGutter", true);
                AnnotationTypes.getTypes().setShowGlyphGutter(Boolean.valueOf(b));
            }

            public void saveType(AnnotationType type) {
                AnnotationTypesFolder.getAnnotationTypesFolder().saveAnnotationType(type);
            }

            public void saveSetting(String settingName, Object value) {
                Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
                if (value instanceof Integer) {
                    prefs.putInt(settingName, (Integer)value);
                } else if (value instanceof Boolean) {
                    prefs.putBoolean(settingName, (Boolean)value);
                } else if (value != null) {
                    prefs.put(settingName, value.toString());
                } else {
                    prefs.remove(settingName);
                }
            }
        });
        this.initAndCheckEditorKitTypeRegistry("text/plain", null);
        this.initAndCheckEditorKitTypeRegistry("text/html", HTMLEditorKit.class.getName());
        this.initAndCheckEditorKitTypeRegistry("text/rtf", RTFEditorKit.class.getName());
        this.initAndCheckEditorKitTypeRegistry("application/rtf", RTFEditorKit.class.getName());
        try {
            Field keyField = JEditorPane.class.getDeclaredField("kitRegistryKey");
            keyField.setAccessible(true);
            Object key = keyField.get(JEditorPane.class);
            Class appContextClass = ClassLoader.getSystemClassLoader().loadClass("sun.awt.AppContext");
            Method getAppContext = appContextClass.getDeclaredMethod("getAppContext", new Class[0]);
            Method get = appContextClass.getDeclaredMethod("get", Object.class);
            Method put = appContextClass.getDeclaredMethod("put", Object.class, Object.class);
            Object appContext = getAppContext.invoke(null, new Object[0]);
            Hashtable kitMapping = (Hashtable)get.invoke(appContext, key);
            put.invoke(appContext, key, new HackMap(kitMapping));
        }
        catch (Throwable t) {
            if (debug) {
                LOG.log(Level.WARNING, "Can't hack in to the JEditorPane's registry for kits.", t);
            }
            LOG.log(Level.WARNING, "Can''t hack in to the JEditorPane''s registry for kits: {0}", new Object[]{t});
        }
        if (this.topComponentRegistryListener == null) {
            this.topComponentRegistryListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("activated".equals(evt.getPropertyName())) {
                        EditorRegistryWatcher.get().notifyActiveTopComponentChanged((Component)TopComponent.getRegistry().getActivated());
                    }
                }
            };
            TopComponent.getRegistry().addPropertyChangeListener(this.topComponentRegistryListener);
        }
        if (GraphicsEnvironment.isHeadless()) {
            return;
        }
        ExClipboard clipboard = (ExClipboard)Lookup.getDefault().lookup(ExClipboard.class);
        if (clipboard != null) {
            clipboard.addClipboardListener((ClipboardListener)ClipboardHistory.getInstance());
        }
        if (LOG.isLoggable(Level.FINE)) {
            WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

                @Override
                public void run() {
                    try {
                        Field kitsField = BaseKit.class.getDeclaredField("kits");
                        kitsField.setAccessible(true);
                        Map kitsMap = (Map)kitsField.get(null);
                        LOG.fine("Number of loaded editor kits: " + kitsMap.size());
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                }
            });
        }
        DataLoaderPool.getDefault().addOperationListener(new OperationListener(){

            public void operationPostCreate(OperationEvent ev) {
            }

            public void operationCopy(OperationEvent.Copy ev) {
            }

            public void operationMove(OperationEvent.Move ev) {
            }

            public void operationDelete(OperationEvent ev) {
            }

            public void operationRename(OperationEvent.Rename ev) {
            }

            public void operationCreateShadow(OperationEvent.Copy ev) {
            }

            public void operationCreateFromTemplate(OperationEvent.Copy ev) {
                if (!ev.getOriginalDataObject().getPrimaryFile().canRevert()) {
                    EditorModule.this.reformat(ev.getObject());
                }
            }
        });
    }

    public void uninstalled() {
        if (this.topComponentRegistryListener != null) {
            TopComponent.getRegistry().removePropertyChangeListener(this.topComponentRegistryListener);
        }
        try {
            Field keyField = JEditorPane.class.getDeclaredField("kitRegistryKey");
            keyField.setAccessible(true);
            Object key = keyField.get(JEditorPane.class);
            Class appContextClass = ClassLoader.getSystemClassLoader().loadClass("sun.awt.AppContext");
            Method getAppContext = appContextClass.getDeclaredMethod("getAppContext", new Class[0]);
            Method get = appContextClass.getDeclaredMethod("get", Object.class);
            Method put = appContextClass.getDeclaredMethod("put", Object.class, Object.class);
            Method remove = appContextClass.getDeclaredMethod("remove", Object.class, Object.class);
            Object appContext = getAppContext.invoke(null, new Object[0]);
            Hashtable kitMapping = (Hashtable)get.invoke(appContext, key);
            if (kitMapping instanceof HackMap) {
                if (((HackMap)kitMapping).getOriginal() != null) {
                    put.invoke(appContext, key, new HackMap(kitMapping));
                } else {
                    remove.invoke(appContext, key);
                }
            }
        }
        catch (Throwable t) {
            if (debug) {
                LOG.log(Level.WARNING, "Can't release the hack from the JEditorPane's registry for kits.", t);
            }
            LOG.log(Level.WARNING, "Can't release the hack from the JEditorPane's registry for kits.");
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                HashSet set = new HashSet();
                set.addAll(TopComponent.getRegistry().getOpened());
                for (TopComponent topComp : set) {
                    Node[] arr;
                    if (!(topComp instanceof CloneableEditor) || (arr = topComp.getActivatedNodes()) == null) continue;
                    for (int i = 0; i < arr.length; ++i) {
                        JEditorPane[] pane;
                        EditorCookie ec = (EditorCookie)arr[i].getCookie(EditorCookie.class);
                        if (ec == null || (pane = ec.getOpenedPanes()) == null) continue;
                        for (int j = 0; j < pane.length; ++j) {
                            if (!(pane[j].getEditorKit() instanceof BaseKit)) continue;
                            topComp.close();
                        }
                    }
                }
            }
        });
    }

    private void initAndCheckEditorKitTypeRegistry(String mimeType, String expectedKitClass) {
        String kitClass = JEditorPane.getEditorKitClassNameForContentType(mimeType);
        if (kitClass == null) {
            LOG.log(Level.WARNING, "Can't find JDK editor kit class for " + mimeType);
        } else if (expectedKitClass != null && !expectedKitClass.equals(kitClass)) {
            LOG.log(Level.WARNING, "Wrong JDK editor kit class for " + mimeType + ". Expecting: " + expectedKitClass + ", but was: " + kitClass);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void reformat(DataObject file) {
        try {
            EditorCookie ec = (EditorCookie)file.getLookup().lookup(EditorCookie.class);
            if (ec == null) {
                return;
            }
            final StyledDocument doc = ec.openDocument();
            final Reformat reformat = Reformat.get((Document)doc);
            String defaultLineSeparator = (String)file.getPrimaryFile().getAttribute("default-line-separator");
            if (defaultLineSeparator != null) {
                doc.putProperty("default-line-separator", defaultLineSeparator);
            }
            reformat.lock();
            try {
                NbDocument.runAtomicAsUser((StyledDocument)doc, (Runnable)new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        doc.putProperty("code-template-insert-handler", true);
                        try {
                            EditorPackageAccessor.get().ActionFactory_reformat(reformat, (Document)doc, 0, doc.getLength(), new AtomicBoolean());
                        }
                        catch (BadLocationException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                        finally {
                            doc.putProperty("code-template-insert-handler", null);
                        }
                    }
                });
            }
            finally {
                reformat.unlock();
                defaultLineSeparator = (String)doc.getProperty("default-line-separator");
                if (defaultLineSeparator != null) {
                    doc.putProperty("__EndOfLine__", defaultLineSeparator);
                } else {
                    doc.putProperty("__EndOfLine__", ReadWriteUtils.getSystemLineSeparator());
                }
                ec.saveDocument();
            }
            ec.close();
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static final class DebugHashtable
    extends Hashtable {
        DebugHashtable(Hashtable h) {
            if (h != null) {
                this.putAll(h);
                LOG.log(Level.INFO, "Existing kit classNames mappings: " + this);
            }
        }

        @Override
        public Object put(Object key, Object value) {
            Object ret = super.put(key, value);
            LOG.log(Level.INFO, "registering mimeType=" + key + " -> kitClassName=" + value + " original was " + ret);
            return ret;
        }

        @Override
        public Object remove(Object key) {
            Object ret = super.remove(key);
            LOG.log(Level.INFO, "removing kitClassName=" + ret + " for mimeType=" + key);
            return ret;
        }
    }

    private static class HackMap
    extends Hashtable {
        private final Object LOCK = new String("EditorModule.HackMap.LOCK");
        private Hashtable delegate;

        HackMap(Hashtable h) {
            this.delegate = h;
            if (debug) {
                LOG.log(Level.INFO, "Original kit mappings: " + h);
                try {
                    Field keyField = JEditorPane.class.getDeclaredField("kitTypeRegistryKey");
                    keyField.setAccessible(true);
                    Object key = keyField.get(JEditorPane.class);
                    Class appContextClass = ClassLoader.getSystemClassLoader().loadClass("sun.awt.AppContext");
                    Method getAppContext = appContextClass.getDeclaredMethod("getAppContext", new Class[0]);
                    Method get = appContextClass.getDeclaredMethod("get", Object.class);
                    Method put = appContextClass.getDeclaredMethod("put", Object.class, Object.class);
                    Object appContext = getAppContext.invoke(null, new Object[0]);
                    Hashtable kitTypeMapping = (Hashtable)get.invoke(appContext, key);
                    if (kitTypeMapping != null) {
                        put.invoke(appContext, key, new DebugHashtable(kitTypeMapping));
                    }
                }
                catch (Throwable t) {
                    LOG.log(Level.WARNING, "Can't hack in to the JEditorPane's registry for kit types.", t);
                }
            }
        }

        private String getKitClassName(String type) {
            try {
                Field keyField = JEditorPane.class.getDeclaredField("kitTypeRegistryKey");
                keyField.setAccessible(true);
                Object key = keyField.get(JEditorPane.class);
                Class appContextClass = ClassLoader.getSystemClassLoader().loadClass("sun.awt.AppContext");
                Method getAppContext = appContextClass.getDeclaredMethod("getAppContext", new Class[0]);
                Method get = appContextClass.getDeclaredMethod("get", Object.class);
                Object appContext = getAppContext.invoke(null, new Object[0]);
                Hashtable kitTypeMapping = (Hashtable)get.invoke(appContext, key);
                if (kitTypeMapping != null) {
                    return (String)kitTypeMapping.get(type);
                }
            }
            catch (Throwable t) {
                if (debug) {
                    LOG.log(Level.WARNING, "Can't hack in to the JEditorPane's registry for kit types.", t);
                }
                LOG.log(Level.WARNING, "Can't hack in to the JEditorPane's registry for kit types.");
            }
            return null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Object get(Object key) {
            Object object = this.LOCK;
            synchronized (object) {
                if (debug) {
                    LOG.log(Level.INFO, "HackMap.get key=" + key);
                }
                Object retVal = null;
                if (this.delegate != null) {
                    retVal = this.delegate.get(key);
                    if (debug && retVal != null) {
                        LOG.log(Level.INFO, "Found cached instance kit=" + retVal + " for mimeType=" + key);
                    }
                }
                if (key instanceof String) {
                    String mimeType = (String)key;
                    if (retVal == null || this.shouldUseNbKit(retVal.getClass().getName(), mimeType)) {
                        EditorKit kit;
                        String kitClassName = this.getKitClassName(mimeType);
                        if (debug) {
                            LOG.log(Level.INFO, "Found kitClassName=" + kitClassName + " for mimeType=" + mimeType);
                        }
                        if ((kitClassName == null || this.shouldUseNbKit(kitClassName, mimeType)) && (kit = this.findKit(mimeType)) != null) {
                            retVal = kit;
                            if (debug) {
                                LOG.log(Level.INFO, "Found kit=" + retVal + " in xml layers for mimeType=" + mimeType);
                            }
                        }
                    }
                }
                return retVal;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Object put(Object key, Object value) {
            Object object = this.LOCK;
            synchronized (object) {
                if (debug) {
                    LOG.log(Level.INFO, "HackMap.put key=" + key + " value=" + value);
                }
                if (this.delegate == null) {
                    this.delegate = new Hashtable();
                }
                Object ret = this.delegate.put(key, value);
                if (debug) {
                    LOG.log(Level.INFO, "registering mimeType=" + key + " -> kitInstance=" + value + " original was " + ret);
                }
                return ret;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Object remove(Object key) {
            Object object = this.LOCK;
            synchronized (object) {
                Object ret;
                if (debug) {
                    LOG.log(Level.INFO, "HackMap.remove key=" + key);
                }
                Object v0 = ret = this.delegate != null ? this.delegate.remove(key) : null;
                if (debug) {
                    LOG.log(Level.INFO, "removing kitInstance=" + ret + " for mimeType=" + key);
                }
                return ret;
            }
        }

        Hashtable getOriginal() {
            return this.delegate;
        }

        private boolean shouldUseNbKit(String kitClass, String mimeType) {
            if (mimeType.startsWith("text/html") || mimeType.startsWith("text/rtf") || mimeType.startsWith("application/rtf")) {
                return false;
            }
            return kitClass.startsWith("javax.swing.");
        }

        private EditorKit findKit(String mimeType) {
            if (!MimePath.validate((CharSequence)mimeType)) {
                return null;
            }
            Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)mimeType));
            EditorKit kit = (EditorKit)lookup.lookup(EditorKit.class);
            return kit == null ? null : (EditorKit)kit.clone();
        }
    }

}

