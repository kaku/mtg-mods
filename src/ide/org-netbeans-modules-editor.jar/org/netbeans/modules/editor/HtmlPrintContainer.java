/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.PrintContainer
 *  org.netbeans.modules.editor.lib.ColoringMap
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.PrintContainer;
import org.netbeans.modules.editor.lib.ColoringMap;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class HtmlPrintContainer
implements PrintContainer {
    private static final String DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";
    private static final String T_HTML_S = "<html>";
    private static final String T_HTML_E = "</html>";
    private static final String T_HEAD_S = "<head>";
    private static final String T_HEAD_E = "</head>";
    private static final String T_BODY_S = "<body>";
    private static final String T_BODY_E = "</body>";
    private static final String T_TITLE = "<title>{0}</title>";
    private static final String T_PRE_S = "<pre>";
    private static final String T_PRE_E = "</pre>";
    private static final String T_BLOCK_S = "<span class=\"{0}\">";
    private static final String T_BLOCK_E = "</span>";
    private static final String T_NAME_TABLE = "<table width=\"100%\"><tr><td align=\"center\">{0}</td></tr></table>";
    private static final String T_CHARSET = "<meta http-equiv=\"content-type\" content=\"text/html; charset={0}\">";
    private static final String T_STYLE_S = "<style type=\"text/css\">";
    private static final String T_STYLE_E = "</style>";
    private static final String T_COMMENT_S = "<!--";
    private static final String T_COMMENT_E = "-->";
    private static final String ST_BODY = "body";
    private static final String ST_PRE = "pre";
    private static final String ST_TABLE = "table";
    private static final String ST_BEGIN = "{";
    private static final String ST_COLOR = "color: ";
    private static final String ST_BGCOLOR = "background-color: ";
    private static final String ST_BOLD = "font-weight: bold";
    private static final String ST_ITALIC = "font-style: italic";
    private static final String ST_SIZE = "font-size: ";
    private static final String ST_FONT_FAMILY = "font-family: ";
    private static final String ST_SEPARATOR = "; ";
    private static final String ST_END = "}";
    private static final String EOL = "\n";
    private static final String WS = " ";
    private static final String ESC_LT = "&lt;";
    private static final String ESC_GT = "&gt;";
    private static final String ESC_AMP = "&amp;";
    private static final String ESC_QUOT = "&quot;";
    private static final String ESC_APOS = "&#39;";
    private static final char ZERO = '0';
    private static final char DOT = '.';
    private static final String STYLE_PREFIX = "ST";
    private Color defaultBackgroundColor;
    private Color defaultForegroundColor;
    private Color headerBackgroundColor;
    private Color headerForegroundColor;
    private Font defaultFont;
    private StringBuffer buffer;
    private String fileName;
    private String shortFileName;
    private Styles styles;
    private boolean[] boolHolder;
    private Map syntaxColoring;
    private String charset;

    public final void begin(FileObject fo, Font font, Color fgColor, Color bgColor, Color hfgColor, Color hbgColor, Class kitClass, String charset) {
        this.begin(fo, font, fgColor, bgColor, hfgColor, hbgColor, MimePath.parse((String)BaseKit.getKit((Class)kitClass).getContentType()), charset);
    }

    final void begin(FileObject fo, Font font, Color fgColor, Color bgColor, Color hfgColor, Color hbgColor, MimePath mimePath, String charset) {
        this.styles = new Styles();
        this.buffer = new StringBuffer();
        this.fileName = FileUtil.getFileDisplayName((FileObject)fo);
        this.shortFileName = fo.getNameExt();
        this.boolHolder = new boolean[1];
        this.defaultForegroundColor = fgColor;
        this.defaultBackgroundColor = bgColor;
        this.defaultFont = font;
        this.headerForegroundColor = hfgColor;
        this.headerBackgroundColor = hbgColor;
        this.syntaxColoring = ColoringMap.get((String)mimePath.getPath()).getMap();
        this.charset = charset;
    }

    public void addLines(List<AttributedCharacterIterator> lines) {
        for (int i = 0; i < lines.size(); ++i) {
            AttributedCharacterIterator line = lines.get(i);
            int endIndex = line.getEndIndex();
            int index = 0;
            line.setIndex(0);
            while (index < endIndex) {
                Color foreground;
                Color background;
                Font font = (Font)line.getAttribute(TextAttribute.FONT);
                if (font == null) {
                    String family = (String)line.getAttribute(TextAttribute.FAMILY);
                    boolean italic = TextAttribute.POSTURE_OBLIQUE.equals(line.getAttribute(TextAttribute.POSTURE));
                    boolean bold = TextAttribute.WEIGHT_BOLD.equals(line.getAttribute(TextAttribute.WEIGHT));
                    Integer size = (Integer)line.getAttribute(TextAttribute.SIZE);
                    font = family != null && size != null ? new Font(family, (bold ? 1 : 0) | (italic ? 2 : 0), size) : this.getDefaultFont();
                }
                if ((foreground = (Color)line.getAttribute(TextAttribute.FOREGROUND)) == null) {
                    foreground = this.getDefaultColor();
                }
                if ((background = (Color)line.getAttribute(TextAttribute.BACKGROUND)) == null) {
                    background = this.getDefaultBackgroundColor();
                }
                int runEndIndex = line.getRunLimit();
                char[] text = new char[runEndIndex - index];
                int j = 0;
                while (index < runEndIndex) {
                    text[j] = j == 0 ? line.current() : line.next();
                    ++j;
                    ++index;
                }
                line.next();
                this.add(text, font, foreground, background);
            }
            this.eol();
        }
    }

    public final void add(char[] chars, Font font, Color foreColor, Color backColor) {
        String text = this.escape(chars, this.boolHolder);
        String styleId = this.styles.getStyleId(font, foreColor, backColor);
        boolean[] arrbl = this.boolHolder;
        arrbl[0] = arrbl[0] & styleId != null;
        if (this.boolHolder[0]) {
            this.buffer.append(MessageFormat.format("<span class=\"{0}\">", styleId));
        }
        this.buffer.append(text);
        if (this.boolHolder[0]) {
            this.buffer.append("</span>");
        }
    }

    public final void eol() {
        this.buffer.append("\n");
    }

    public final String end() {
        StringBuffer result = new StringBuffer();
        result.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
        result.append("\n");
        result.append("<html>");
        result.append("\n");
        result.append("<head>");
        result.append("\n");
        result.append(MessageFormat.format("<title>{0}</title>", this.shortFileName));
        result.append("\n");
        result.append(MessageFormat.format("<meta http-equiv=\"content-type\" content=\"text/html; charset={0}\">", this.charset));
        result.append("\n");
        result.append("<style type=\"text/css\">");
        result.append("\n");
        result.append("<!--");
        result.append("\n");
        result.append(this.createStyle("body", null, this.getDefaultFont(), this.getDefaultColor(), this.getDefaultBackgroundColor(), false));
        result.append("\n");
        result.append(this.createStyle("pre", null, this.getDefaultFont(), this.getDefaultColor(), this.getDefaultBackgroundColor(), false));
        result.append("\n");
        result.append(this.createStyle("table", null, this.getDefaultFont(), this.headerForegroundColor, this.headerBackgroundColor, false));
        result.append("\n");
        result.append(this.styles.toExternalForm());
        result.append("-->");
        result.append("\n");
        result.append("</style>");
        result.append("\n");
        result.append("</head>");
        result.append("\n");
        result.append("<body>");
        result.append("\n");
        result.append(MessageFormat.format("<table width=\"100%\"><tr><td align=\"center\">{0}</td></tr></table>", this.fileName));
        result.append("\n");
        result.append("<pre>");
        result.append("\n");
        result.append(this.buffer);
        result.append("</pre>");
        result.append("</body>");
        result.append("\n");
        result.append("</html>");
        result.append("\n");
        this.styles = null;
        this.buffer = null;
        this.fileName = null;
        this.shortFileName = null;
        this.defaultBackgroundColor = null;
        this.defaultForegroundColor = null;
        this.defaultFont = null;
        return result.toString();
    }

    public final boolean initEmptyLines() {
        return false;
    }

    private String escape(char[] buffer, boolean[] boolHolder) {
        StringBuffer result = new StringBuffer();
        boolHolder[0] = false;
        for (int i = 0; i < buffer.length; ++i) {
            if (buffer[i] == '<') {
                result.append("&lt;");
                boolean[] arrbl = boolHolder;
                arrbl[0] = arrbl[0] | true;
                continue;
            }
            if (buffer[i] == '>') {
                result.append("&gt;");
                boolean[] arrbl = boolHolder;
                arrbl[0] = arrbl[0] | true;
                continue;
            }
            if (buffer[i] == '&') {
                result.append("&amp;");
                boolean[] arrbl = boolHolder;
                arrbl[0] = arrbl[0] | true;
                continue;
            }
            if (buffer[i] == '\'') {
                result.append("&#39;");
                boolean[] arrbl = boolHolder;
                arrbl[0] = arrbl[0] | true;
                continue;
            }
            if (buffer[i] == '\"') {
                result.append("&quot;");
                boolean[] arrbl = boolHolder;
                arrbl[0] = arrbl[0] | true;
                continue;
            }
            if (Character.isWhitespace(buffer[i])) {
                result.append(buffer[i]);
                continue;
            }
            result.append(buffer[i]);
            boolean[] arrbl = boolHolder;
            arrbl[0] = arrbl[0] | true;
        }
        return result.toString();
    }

    private Color getDefaultColor() {
        return this.defaultForegroundColor;
    }

    private Color getDefaultBackgroundColor() {
        return this.defaultBackgroundColor;
    }

    private Font getDefaultFont() {
        return this.defaultFont;
    }

    private String createStyle(String element, String selector, Font font, Color fg, Color bg, boolean useDefaults) {
        StringBuffer sb = new StringBuffer();
        if (element != null) {
            sb.append(element);
            sb.append(" ");
        }
        if (selector != null) {
            sb.append('.');
            sb.append(selector);
            sb.append(" ");
        }
        sb.append("{");
        boolean first = true;
        if (!(useDefaults && fg.equals(this.getDefaultColor()) || fg == null)) {
            sb.append("color: ");
            sb.append(HtmlPrintContainer.getHtmlColor(fg));
            first = false;
        }
        if (!(useDefaults && bg.equals(this.getDefaultBackgroundColor()) || bg == null)) {
            if (!first) {
                sb.append("; ");
            }
            sb.append("background-color: ");
            sb.append(HtmlPrintContainer.getHtmlColor(bg));
            first = false;
        }
        if (!(useDefaults && font.equals(this.getDefaultFont()) || font == null)) {
            Font df;
            if (!first) {
                sb.append("; ");
            }
            sb.append("font-family: ");
            sb.append(font.getFamily());
            if (font.isBold()) {
                sb.append("; ");
                sb.append("font-weight: bold");
            }
            if (font.isItalic()) {
                sb.append("; ");
                sb.append("font-style: italic");
            }
            if ((df = this.getDefaultFont()) != null && df.getSize() != font.getSize()) {
                sb.append("; ");
                sb.append("font-size: ");
                sb.append(String.valueOf(font.getSize()));
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private static String getHtmlColor(Color c) {
        int r = c.getRed();
        int g = c.getGreen();
        int b = c.getBlue();
        StringBuffer result = new StringBuffer();
        result.append("#");
        String rs = Integer.toHexString(r);
        String gs = Integer.toHexString(g);
        String bs = Integer.toHexString(b);
        if (r < 16) {
            result.append('0');
        }
        result.append(rs);
        if (g < 16) {
            result.append('0');
        }
        result.append(gs);
        if (b < 16) {
            result.append('0');
        }
        result.append(bs);
        return result.toString();
    }

    private class Styles {
        private Map<StyleDescriptor, String> descs;
        private int sequence;

        public Styles() {
            this.descs = new HashMap<StyleDescriptor, String>();
        }

        private boolean coloringEquals(Coloring coloring, Font f, Color fc, Color bc) {
            Color coloringBackColor;
            Color coloringForeColor;
            if (coloring == null) {
                return false;
            }
            Font coloringFont = coloring.getFont();
            if (coloringFont == null) {
                coloringFont = HtmlPrintContainer.this.getDefaultFont();
            }
            if ((coloringForeColor = coloring.getForeColor()) == null) {
                coloringForeColor = HtmlPrintContainer.this.getDefaultColor();
            }
            if ((coloringBackColor = coloring.getBackColor()) == null) {
                coloringBackColor = HtmlPrintContainer.this.getDefaultBackgroundColor();
            }
            return f.equals(coloringFont) && fc.equals(coloringForeColor) && bc.equals(coloringBackColor);
        }

        public final String getStyleId(Font f, Color fc, Color bc) {
            if (!(fc.equals(HtmlPrintContainer.this.getDefaultColor()) && bc.equals(HtmlPrintContainer.this.getDefaultBackgroundColor()) && f.equals(HtmlPrintContainer.this.getDefaultFont()))) {
                StyleDescriptor sd = new StyleDescriptor(f, fc, bc);
                String id = this.descs.get(sd);
                if (id == null) {
                    Set keySet = HtmlPrintContainer.this.syntaxColoring.keySet();
                    for (Object key : keySet) {
                        if (!this.coloringEquals((Coloring)HtmlPrintContainer.this.syntaxColoring.get(key), f, fc, bc)) continue;
                        id = (String)key;
                        break;
                    }
                    if (id == null) {
                        id = "ST" + this.sequence++;
                    }
                    sd.name = id;
                    this.descs.put(sd, id);
                }
                return id;
            }
            return null;
        }

        public final String toExternalForm() {
            StringBuffer result = new StringBuffer();
            for (StyleDescriptor sd : this.descs.keySet()) {
                result.append(sd.toExternalForm());
                result.append("\n");
            }
            return result.toString();
        }

        public final String toString() {
            return this.toExternalForm();
        }

        private class StyleDescriptor {
            String name;
            private Font font;
            private Color fgColor;
            private Color bgColor;

            public StyleDescriptor(Font font, Color fgColor, Color bgColor) {
                this.font = font;
                this.fgColor = fgColor;
                this.bgColor = bgColor;
            }

            public final String getName() {
                return this.name;
            }

            public final String toExternalForm() {
                return HtmlPrintContainer.this.createStyle(null, this.name, this.font, this.fgColor, this.bgColor, true);
            }

            public final String toString() {
                return this.toExternalForm();
            }

            public final boolean equals(Object object) {
                if (!(object instanceof StyleDescriptor)) {
                    return false;
                }
                StyleDescriptor od = (StyleDescriptor)object;
                return Styles.this.coloringEquals(new Coloring(this.font, this.fgColor, this.bgColor), od.font, od.fgColor, od.bgColor);
            }

            public final int hashCode() {
                return this.font.hashCode() ^ this.fgColor.hashCode() ^ this.bgColor.hashCode();
            }
        }

    }

}

