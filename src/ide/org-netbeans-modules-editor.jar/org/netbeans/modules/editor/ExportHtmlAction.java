/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.EditorState
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.view.PrintUtils
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.CookieAction
 */
package org.netbeans.modules.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.AttributedCharacterIterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorState;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.HtmlPrintContainer;
import org.netbeans.modules.editor.lib2.view.PrintUtils;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.Mnemonics;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.CookieAction;

public class ExportHtmlAction
extends CookieAction {
    private static final String HTML_EXT = ".html";
    private static final String OPEN_HTML_HIST = "ExportHtmlAction_open_html_history";
    private static final String SHOW_LINES_HIST = "ExportHtmlAction_show_lines_history";
    private static final String SELECTION_HIST = "ExportHtmlAction_selection_history";
    private static final String FOLDER_NAME_HIST = "ExportHtmlAction_folder_name_history";
    private static final String CHARSET = "UTF-8";
    private Dialog dlg;

    protected final int mode() {
        return 8;
    }

    protected final Class[] cookieClasses() {
        return new Class[]{EditorCookie.class};
    }

    protected final void performAction(Node[] activatedNodes) {
        EditorCookie ec = (EditorCookie)activatedNodes[0].getCookie(EditorCookie.class);
        if (ec == null) {
            return;
        }
        StyledDocument doc = null;
        try {
            doc = ec.openDocument();
        }
        catch (IOException ioe) {
            // empty catch block
        }
        if (doc instanceof BaseDocument) {
            final BaseDocument bdoc = (BaseDocument)doc;
            final JTextComponent jtc = Utilities.getLastActiveComponent();
            Presenter p = new Presenter();
            String folderName = (String)EditorState.get((Object)"ExportHtmlAction_folder_name_history");
            if (folderName == null) {
                folderName = System.getProperty("user.home");
            }
            p.setFileName(folderName + File.separatorChar + ((DataObject)bdoc.getProperty((Object)"stream")).getPrimaryFile().getName() + ".html");
            MimePath mimePath = jtc == null ? MimePath.EMPTY : MimePath.parse((String)DocumentUtilities.getMimeType((JTextComponent)jtc));
            Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
            Boolean bool = (Boolean)EditorState.get((Object)"ExportHtmlAction_show_lines_history");
            boolean showLineNumbers = bool != null ? bool.booleanValue() : prefs.getBoolean("line-number-visible", true);
            p.setShowLines(showLineNumbers);
            p.setSelectionActive(jtc != null && jtc.getSelectionStart() != jtc.getSelectionEnd());
            bool = (Boolean)EditorState.get((Object)"ExportHtmlAction_selection_history");
            boolean selection = jtc != null && jtc.getSelectionStart() != jtc.getSelectionEnd() && (bool == null || bool != false);
            p.setSelection(selection);
            bool = (Boolean)EditorState.get((Object)"ExportHtmlAction_open_html_history");
            boolean setOpen = bool != null ? bool : false;
            p.setOpenHtml(setOpen);
            DialogDescriptor dd = new DialogDescriptor((Object)p, NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_ExportHtml"));
            boolean overwrite = true;
            this.dlg = DialogDisplayer.getDefault().createDialog(dd);
            do {
                this.dlg.setVisible(true);
                overwrite = true;
                if (dd.getValue() != DialogDescriptor.OK_OPTION || p.isToClipboard() || !new File(p.getFileName()).exists()) continue;
                NotifyDescriptor.Confirmation descriptor = new NotifyDescriptor.Confirmation((Object)NbBundle.getMessage(ExportHtmlAction.class, (String)"MSG_FileExists", (Object)p.getFileName()), 0, 2);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)descriptor);
                if (descriptor.getValue() == NotifyDescriptor.YES_OPTION) continue;
                overwrite = false;
            } while (!overwrite);
            this.dlg.dispose();
            this.dlg = null;
            if (dd.getValue() == DialogDescriptor.OK_OPTION) {
                String file;
                int idx;
                boolean open;
                boolean lineNumbers;
                if (selection != p.isSelection()) {
                    selection = p.isSelection();
                    EditorState.put((Object)"ExportHtmlAction_selection_history", (Object)(selection ? Boolean.TRUE : Boolean.FALSE));
                }
                if ((idx = (file = p.getFileName()).lastIndexOf(File.separatorChar)) != -1) {
                    EditorState.put((Object)"ExportHtmlAction_folder_name_history", (Object)file.substring(0, idx));
                }
                if (showLineNumbers != (lineNumbers = p.isShowLines())) {
                    EditorState.put((Object)"ExportHtmlAction_show_lines_history", (Object)(lineNumbers ? Boolean.TRUE : Boolean.FALSE));
                }
                if (setOpen != (open = p.isOpenHtml())) {
                    EditorState.put((Object)"ExportHtmlAction_open_html_history", (Object)(open ? Boolean.TRUE : Boolean.FALSE));
                }
                final boolean toClipboard = p.isToClipboard();
                final int selectionStart = selection ? jtc.getSelectionStart() : 0;
                final int selectionEnd = selection ? jtc.getSelectionEnd() : bdoc.getLength();
                RequestProcessor.getDefault().post(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        try {
                            if (jtc != null) {
                                this.setCursor(Cursor.getPredefinedCursor(3));
                            }
                            ExportHtmlAction.this.export(bdoc, file, lineNumbers, selectionStart, selectionEnd, toClipboard);
                            if (!toClipboard && open) {
                                HtmlBrowser.URLDisplayer.getDefault().showURL(new File(file).toURI().toURL());
                            }
                        }
                        catch (MalformedURLException mue) {
                            Exceptions.printStackTrace((Throwable)mue);
                        }
                        catch (IOException ioe) {
                            NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)NbBundle.getMessage(ExportHtmlAction.class, (String)"ERR_IOError", (Object[])new Object[]{((DataObject)bdoc.getProperty((Object)"stream")).getPrimaryFile().getNameExt() + ".html", file}), 0);
                            DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                            return;
                        }
                        finally {
                            if (jtc != null) {
                                this.setCursor(null);
                            }
                        }
                    }

                    private void setCursor(final Cursor c) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                jtc.setCursor(c);
                            }
                        });
                    }

                });
            }
        } else {
            Logger.getLogger("global").log(Level.FINE, NbBundle.getMessage(ExportHtmlAction.class, (String)"MSG_DocError"));
        }
    }

    public final String getName() {
        return NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_ExportHtmlAction");
    }

    public final HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected final boolean asynchronous() {
        return false;
    }

    private void export(final BaseDocument bdoc, final String fileName, final boolean lineNumbers, final int selectionStart, final int selectionEnd, final boolean toClipboard) throws IOException {
        MimePath mimePath = MimePath.parse((String)((String)bdoc.getProperty((Object)"mimeType")));
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)mimePath).lookup(FontColorSettings.class);
        AttributeSet defaultAttribs = fcs.getFontColors("default");
        Coloring coloring = Coloring.fromAttributeSet((AttributeSet)defaultAttribs);
        Color bgColor = coloring.getBackColor();
        Color fgColor = coloring.getForeColor();
        Font font = coloring.getFont();
        AttributeSet lineNumberAttribs = fcs.getFontColors("line-number");
        Coloring lineNumberColoring = Coloring.fromAttributeSet((AttributeSet)lineNumberAttribs);
        Color lnbgColor = lineNumberColoring.getBackColor();
        Color lnfgColor = lineNumberColoring.getForeColor();
        FileObject fo = ((DataObject)bdoc.getProperty((Object)"stream")).getPrimaryFile();
        final HtmlPrintContainer htmlPrintContainer = new HtmlPrintContainer();
        htmlPrintContainer.begin(fo, font, fgColor, bgColor, lnfgColor, lnbgColor, mimePath, "UTF-8");
        IOException[] ioExc = new IOException[1];
        SwingUtilities.invokeLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                List lines = PrintUtils.printDocument((Document)bdoc, (boolean)lineNumbers, (int)selectionStart, (int)selectionEnd);
                htmlPrintContainer.addLines(lines);
                String result = htmlPrintContainer.end();
                if (toClipboard) {
                    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(result), null);
                } else {
                    PrintWriter out = null;
                    try {
                        out = new PrintWriter(new OutputStreamWriter((OutputStream)new FileOutputStream(fileName), "UTF-8"));
                        out.print(result);
                    }
                    catch (IOException ex) {
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)NbBundle.getMessage(ExportHtmlAction.class, (String)"ERR_IOError", (Object[])new Object[]{fileName}), 0);
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                        return;
                    }
                    finally {
                        if (out != null) {
                            out.close();
                        }
                    }
                }
            }
        });
    }

    private static final class HtmlOrDirFilter
    extends FileFilter {
        private HtmlOrDirFilter() {
        }

        @Override
        public boolean accept(File f) {
            if (f.isFile() && f.getName().endsWith(".html") || f.isDirectory()) {
                return true;
            }
            return false;
        }

        @Override
        public String getDescription() {
            return NbBundle.getMessage(ExportHtmlAction.class, (String)"TXT_HTMLFileType");
        }
    }

    private static final class Presenter
    extends JPanel
    implements ActionListener {
        private static final String KEY_OPEN = "open";
        private static final String KEY_LINE_NUMBERS = "lineNumbers";
        private static final String KEY_TO_FILE = "toFile";
        private static final String KEY_SELECTION = "selection";
        private JTextField fileName;
        private JCheckBox showLineNumbers;
        private JCheckBox openHtml;
        private JCheckBox selection;
        private JButton browseButton;
        private final JRadioButton toFileButton = new JRadioButton();
        private final JRadioButton toClipboardButton = new JRadioButton();
        private final ButtonGroup group = new ButtonGroup();
        private boolean wasOpen = NbPreferences.forModule(Presenter.class).getBoolean("open", true);
        private boolean programmaticDisableOpen = false;

        public Presenter() {
            this.initGUI();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent ae) {
            if (ae.getSource() == this.toClipboardButton || ae.getSource() == this.toFileButton) {
                boolean isFile;
                block11 : {
                    isFile = this.toFileButton.isSelected();
                    this.browseButton.setEnabled(isFile);
                    this.fileName.setEnabled(isFile);
                    this.openHtml.setEnabled(isFile);
                    this.programmaticDisableOpen = true;
                    try {
                        if (isFile) {
                            this.openHtml.setSelected(this.wasOpen);
                            break block11;
                        }
                        this.openHtml.setSelected(false);
                    }
                    finally {
                        this.programmaticDisableOpen = false;
                    }
                }
                NbPreferences.forModule(ExportHtmlAction.class).putBoolean("toFile", isFile);
            } else if (ae.getSource() == this.openHtml && !this.programmaticDisableOpen) {
                NbPreferences.forModule(Presenter.class).putBoolean("open", this.openHtml.isSelected());
            } else if (ae.getSource() == this.selection) {
                NbPreferences.forModule(Presenter.class).putBoolean("selection", this.selection.isSelected());
            } else if (ae.getSource() == this.showLineNumbers) {
                NbPreferences.forModule(Presenter.class).putBoolean("lineNumbers", this.showLineNumbers.isSelected());
            }
        }

        public final String getFileName() {
            return this.fileName.getText();
        }

        public final void setFileName(String name) {
            this.fileName.setText(name);
        }

        public final boolean isShowLines() {
            return this.showLineNumbers.isSelected();
        }

        public final void setShowLines(boolean value) {
            this.showLineNumbers.setSelected(value);
        }

        public final boolean isSelection() {
            return this.selection.isSelected();
        }

        public final void setSelection(boolean value) {
            this.selection.setSelected(value);
        }

        public final boolean isOpenHtml() {
            return this.openHtml.isSelected();
        }

        public final void setOpenHtml(boolean value) {
            this.openHtml.setSelected(value);
        }

        public final void setSelectionActive(boolean value) {
            this.selection.setEnabled(value);
        }

        public final boolean isToClipboard() {
            return this.toClipboardButton.isSelected();
        }

        private void initGUI() {
            boolean isToFile = NbPreferences.forModule(ExportHtmlAction.class).getBoolean("toFile", true);
            this.toFileButton.setSelected(isToFile);
            this.toClipboardButton.setSelected(!isToFile);
            Mnemonics.setLocalizedText((AbstractButton)this.toClipboardButton, (String)NbBundle.getMessage(ExportHtmlAction.class, (String)"LBL_PRINT_TO_CLIPBOARD"));
            this.toClipboardButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACSN_PRINT_TO_CLIPBOARD"));
            this.toClipboardButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACSD_PRINT_TO_CLIPBOARD"));
            Mnemonics.setLocalizedText((AbstractButton)this.toFileButton, (String)NbBundle.getMessage(ExportHtmlAction.class, (String)"LBL_PRINT_TO_FILE"));
            this.toFileButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACSN_PRINT_TO_FILE"));
            this.toFileButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACSD_PRINT_TO_FILE"));
            this.group.add(this.toFileButton);
            this.group.add(this.toClipboardButton);
            this.toFileButton.addActionListener(this);
            this.toClipboardButton.addActionListener(this);
            this.setLayout(new GridBagLayout());
            this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACSN_ExportToHTML"));
            this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACSD_ExportToHTML"));
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;
            c.anchor = 17;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 1;
            c.insets = new Insets(12, 6, 6, 6);
            this.add((Component)this.toFileButton, c);
            c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            c.insets = new Insets(12, 6, 6, 6);
            this.fileName = new JTextField();
            this.fileName.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"AN_OutputDir"));
            this.fileName.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"AD_OutputDir"));
            this.fileName.setColumns(25);
            c = new GridBagConstraints();
            c.gridx = 2;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.fill = 2;
            c.ipadx = 275;
            c.anchor = 17;
            c.insets = new Insets(12, 6, 6, 6);
            c.weightx = 1.0;
            ((GridBagLayout)this.getLayout()).setConstraints(this.fileName, c);
            this.add(this.fileName);
            this.browseButton = new JButton();
            Mnemonics.setLocalizedText((AbstractButton)this.browseButton, (String)NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_Select"));
            this.browseButton.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"AN_Select"));
            this.browseButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"AD_Select"));
            this.browseButton.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    Presenter.this.selectFile();
                }
            });
            c = new GridBagConstraints();
            c.gridx = 3;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.anchor = 17;
            c.insets = new Insets(12, 6, 6, 12);
            ((GridBagLayout)this.getLayout()).setConstraints(this.browseButton, c);
            this.add(this.browseButton);
            c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 1;
            c.fill = 1;
            c.gridy = 1;
            c.gridwidth = 0;
            c.gridheight = 1;
            c.anchor = 17;
            c.insets = new Insets(12, 6, 6, 6);
            this.add((Component)this.toClipboardButton, c);
            this.selection = new JCheckBox();
            Mnemonics.setLocalizedText((AbstractButton)this.selection, (String)NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_Selection"));
            this.selection.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"AN_Selection"));
            this.selection.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"AD_Selection"));
            this.selection.setSelected(NbPreferences.forModule(ExportHtmlAction.class).getBoolean("selection", true));
            this.selection.addActionListener(this);
            c = new GridBagConstraints();
            c.gridx = 2;
            c.gridy = 2;
            c.gridwidth = 0;
            c.gridheight = 1;
            c.anchor = 17;
            c.fill = 2;
            c.insets = new Insets(6, 6, 6, 12);
            c.weightx = 1.0;
            ((GridBagLayout)this.getLayout()).setConstraints(this.selection, c);
            this.add(this.selection);
            this.showLineNumbers = new JCheckBox();
            Mnemonics.setLocalizedText((AbstractButton)this.showLineNumbers, (String)NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_ShowLineNumbers"));
            this.showLineNumbers.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"AN_ShowLineNumbers"));
            this.showLineNumbers.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"AD_ShowLineNumbers"));
            this.showLineNumbers.setSelected(NbPreferences.forModule(ExportHtmlAction.class).getBoolean("lineNumbers", true));
            this.showLineNumbers.addActionListener(this);
            c = new GridBagConstraints();
            c.gridx = 2;
            c.gridy = 3;
            c.gridwidth = 0;
            c.gridheight = 1;
            c.anchor = 17;
            c.fill = 2;
            c.insets = new Insets(6, 6, 6, 12);
            c.weightx = 1.0;
            ((GridBagLayout)this.getLayout()).setConstraints(this.showLineNumbers, c);
            this.add(this.showLineNumbers);
            this.openHtml = new JCheckBox();
            Mnemonics.setLocalizedText((AbstractButton)this.openHtml, (String)NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_OpenHTML"));
            this.openHtml.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"AN_OpenHTML"));
            this.openHtml.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"AD_OpenHTML"));
            this.openHtml.setSelected(NbPreferences.forModule(ExportHtmlAction.class).getBoolean("open", true));
            this.openHtml.addActionListener(this);
            c = new GridBagConstraints();
            c.gridx = 2;
            c.gridy = 4;
            c.gridwidth = 0;
            c.gridheight = 1;
            c.anchor = 17;
            c.fill = 2;
            c.insets = new Insets(6, 6, 12, 12);
            c.weightx = 1.0;
            ((GridBagLayout)this.getLayout()).setConstraints(this.openHtml, c);
            this.add(this.openHtml);
            this.actionPerformed(new ActionEvent(this.toClipboardButton, 1001, ""));
        }

        private void selectFile() {
            JFileChooser chooser = new FileChooserBuilder(Presenter.class).setFileFilter((FileFilter)new HtmlOrDirFilter()).setAccessibleDescription(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACD_Browse_Dialog")).setTitle(NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_Browse_Dialog_Title")).createFileChooser();
            chooser.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ExportHtmlAction.class, (String)"ACN_Browse_Dialog"));
            chooser.setSelectedFile(new File(this.fileName.getText()));
            if (chooser.showDialog(this, NbBundle.getMessage(ExportHtmlAction.class, (String)"CTL_Approve_Label")) == 0) {
                this.fileName.setText(chooser.getSelectedFile().getAbsolutePath());
            }
        }

    }

}

