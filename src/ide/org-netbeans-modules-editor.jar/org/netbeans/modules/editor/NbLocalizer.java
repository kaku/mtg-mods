/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.LocaleSupport
 *  org.netbeans.editor.LocaleSupport$Localizer
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor;

import java.util.MissingResourceException;
import org.netbeans.editor.LocaleSupport;
import org.openide.util.NbBundle;

public class NbLocalizer
implements LocaleSupport.Localizer {
    private Class bundleClass;

    public NbLocalizer(Class bundleClass) {
        this.bundleClass = bundleClass;
    }

    public String getString(String key) {
        try {
            return NbBundle.getBundle((Class)this.bundleClass).getString(key);
        }
        catch (MissingResourceException e) {
            return null;
        }
    }

    public String toString() {
        return "NbLocalizer(" + this.bundleClass + ")";
    }
}

