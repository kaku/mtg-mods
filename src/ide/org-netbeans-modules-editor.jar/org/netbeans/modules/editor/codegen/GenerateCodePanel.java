/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.codegen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.codegen.PopupUtil;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class GenerateCodePanel
extends JPanel {
    private JTextComponent component;
    public JLabel jLabel1;
    public JList jList1;
    public JScrollPane jScrollPane1;

    public GenerateCodePanel(JTextComponent component, List<? extends CodeGenerator> generators) {
        this.component = component;
        this.initComponents();
        this.setFocusable(false);
        this.setNextFocusableComponent(this.jList1);
        this.setBackground(this.jList1.getBackground());
        this.jScrollPane1.setBackground(this.jList1.getBackground());
        this.jList1.setModel(this.createModel(generators));
        this.jList1.setSelectedIndex(0);
        this.jList1.setVisibleRowCount(generators.size() > 16 ? 16 : generators.size());
        this.jList1.setCellRenderer(new Renderer(this.jList1));
        this.jList1.grabFocus();
        this.jList1.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent e) {
                PopupUtil.hidePopup();
            }
        });
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jList1 = new JList();
        this.jLabel1 = new JLabel();
        this.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64)));
        this.setLayout(new BorderLayout());
        this.jScrollPane1.setBorder(BorderFactory.createEmptyBorder(2, 4, 4, 4));
        this.jList1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent evt) {
                GenerateCodePanel.this.listMouseReleased(evt);
            }
        });
        this.jList1.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent evt) {
                GenerateCodePanel.this.listMouseMoved(evt);
            }
        });
        this.jList1.addKeyListener(new KeyAdapter(){

            @Override
            public void keyPressed(KeyEvent evt) {
                GenerateCodePanel.this.listKeyPressed(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jList1);
        this.add((Component)this.jScrollPane1, "Center");
        this.jLabel1.setText(NbBundle.getMessage(GenerateCodePanel.class, (String)"LBL_generate_code"));
        this.jLabel1.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.jLabel1.setOpaque(true);
        this.add((Component)this.jLabel1, "First");
    }

    private void listMouseReleased(MouseEvent evt) {
        this.invokeSelected();
    }

    private void listMouseMoved(MouseEvent evt) {
        int idx = this.jList1.locationToIndex(evt.getPoint());
        if (idx != this.jList1.getSelectedIndex()) {
            this.jList1.setSelectedIndex(idx);
        }
    }

    private void listKeyPressed(KeyEvent evt) {
        int size;
        KeyStroke ks = KeyStroke.getKeyStrokeForEvent(evt);
        if (ks.getKeyCode() == 10 || ks.getKeyCode() == 32) {
            this.invokeSelected();
        } else if (ks.getKeyCode() == 40) {
            int size2 = this.jList1.getModel().getSize();
            if (size2 > 0) {
                int idx = (this.jList1.getSelectedIndex() + 1) % size2;
                if (idx == size2) {
                    idx = 0;
                }
                this.jList1.setSelectedIndex(idx);
                this.jList1.ensureIndexIsVisible(idx);
                evt.consume();
            }
        } else if (ks.getKeyCode() == 38 && (size = this.jList1.getModel().getSize()) > 0) {
            int idx = (this.jList1.getSelectedIndex() - 1 + size) % size;
            this.jList1.setSelectedIndex(idx);
            this.jList1.ensureIndexIsVisible(idx);
            evt.consume();
        }
    }

    private DefaultListModel createModel(List<? extends CodeGenerator> generators) {
        DefaultListModel<CodeGenerator> model = new DefaultListModel<CodeGenerator>();
        for (CodeGenerator generator : generators) {
            model.addElement(generator);
        }
        return model;
    }

    private void invokeSelected() {
        Object value;
        PopupUtil.hidePopup();
        if (Utilities.isMac()) {
            this.component.requestFocus();
        }
        if ((value = this.jList1.getSelectedValue()) instanceof CodeGenerator) {
            ((CodeGenerator)value).invoke();
        }
    }

    private static class Renderer
    extends DefaultListCellRenderer {
        private static int DARKER_COLOR_COMPONENT = 5;
        private Color fgColor;
        private Color bgColor;
        private Color bgColorDarker;
        private Color bgSelectionColor;
        private Color fgSelectionColor;

        public Renderer(JList list) {
            this.setFont(list.getFont());
            this.fgColor = list.getForeground();
            this.bgColor = list.getBackground();
            this.bgColorDarker = new Color(Math.abs(this.bgColor.getRed() - DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getGreen() - DARKER_COLOR_COMPONENT), Math.abs(this.bgColor.getBlue() - DARKER_COLOR_COMPONENT));
            this.bgSelectionColor = list.getSelectionBackground();
            this.fgSelectionColor = list.getSelectionForeground();
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
            if (isSelected) {
                this.setForeground(this.fgSelectionColor);
                this.setBackground(this.bgSelectionColor);
            } else {
                this.setForeground(this.fgColor);
                this.setBackground(index % 2 == 0 ? this.bgColor : this.bgColorDarker);
            }
            this.setText(value instanceof CodeGenerator ? ((CodeGenerator)value).getDisplayName() : value.toString());
            return this;
        }
    }

}

