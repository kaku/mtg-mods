/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.editor.codegen;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.PrintStream;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public final class PopupUtil {
    private static final String CLOSE_KEY = "CloseKey";
    private static final Action CLOSE_ACTION = new CloseAction();
    private static final KeyStroke ESC_KEY_STROKE = KeyStroke.getKeyStroke(27, 0);
    private static final String POPUP_NAME = "popupComponent";
    private static JDialog popupWindow;
    private static Frame owner;
    private static HideAWTListener hideListener;
    private static final int X_INSET = 10;
    private static final int Y_INSET = 10;

    private PopupUtil() {
    }

    public static void showPopup(JComponent content, Frame parent, int x, int y, boolean undecorated, int altHeight) {
        String a11yDesc;
        if (popupWindow != null) {
            return;
        }
        Toolkit.getDefaultToolkit().addAWTEventListener(hideListener, 16);
        owner = parent;
        popupWindow = new JDialog(PopupUtil.getMainWindow());
        popupWindow.setName("popupComponent");
        popupWindow.setUndecorated(undecorated);
        popupWindow.getRootPane().getInputMap(1).put(ESC_KEY_STROKE, "CloseKey");
        popupWindow.getRootPane().getActionMap().put("CloseKey", CLOSE_ACTION);
        String a11yName = content.getAccessibleContext().getAccessibleName();
        if (a11yName != null && !a11yName.equals("")) {
            popupWindow.getAccessibleContext().setAccessibleName(a11yName);
        }
        if ((a11yDesc = content.getAccessibleContext().getAccessibleDescription()) != null && !a11yDesc.equals("")) {
            popupWindow.getAccessibleContext().setAccessibleDescription(a11yDesc);
        }
        popupWindow.getContentPane().add(content);
        PopupUtil.getMainWindow().addWindowStateListener(hideListener);
        PopupUtil.getMainWindow().addComponentListener(hideListener);
        PopupUtil.resizePopup();
        if (x != -1) {
            Point p = PopupUtil.fitToScreen(x, y, altHeight);
            Rectangle screen = Utilities.getUsableScreenBounds();
            if (p.y < screen.y) {
                int yAdjustment = screen.y - p.y;
                p.y += yAdjustment;
                popupWindow.setSize(popupWindow.getWidth(), popupWindow.getHeight() - yAdjustment);
            }
            popupWindow.setLocation(p.x, p.y);
        }
        popupWindow.setVisible(true);
        content.requestFocus();
        content.requestFocusInWindow();
    }

    public static void hidePopup() {
        if (popupWindow != null) {
            Toolkit.getDefaultToolkit().removeAWTEventListener(hideListener);
            popupWindow.setVisible(false);
            popupWindow.dispose();
        }
        PopupUtil.getMainWindow().removeWindowStateListener(hideListener);
        PopupUtil.getMainWindow().removeComponentListener(hideListener);
        popupWindow = null;
        owner = null;
    }

    private static void resizePopup() {
        popupWindow.pack();
        Point point = new Point(0, 0);
        SwingUtilities.convertPointToScreen(point, PopupUtil.getMainWindow());
        popupWindow.setLocation(point.x + (PopupUtil.getMainWindow().getWidth() - popupWindow.getWidth()) / 2, point.y + (PopupUtil.getMainWindow().getHeight() - popupWindow.getHeight()) / 3);
    }

    private static Point fitToScreen(int x, int y, int altHeight) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        Point p = new Point(x, y);
        if (p.x + popupWindow.getWidth() > screen.x + screen.width - 10) {
            p.x = screen.x + screen.width - 10 - popupWindow.getWidth();
        }
        if (p.y + popupWindow.getHeight() > screen.y + screen.height - 10) {
            p.y = p.y - popupWindow.getHeight() - altHeight;
        }
        return p;
    }

    private static Frame getMainWindow() {
        return owner != null ? owner : WindowManager.getDefault().getMainWindow();
    }

    static {
        hideListener = new HideAWTListener();
    }

    private static class CloseAction
    extends AbstractAction {
        private CloseAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            PopupUtil.hidePopup();
        }
    }

    private static class MyFocusListener
    implements FocusListener {
        private MyFocusListener() {
        }

        @Override
        public void focusLost(FocusEvent e) {
            System.out.println(e);
        }

        @Override
        public void focusGained(FocusEvent e) {
            System.out.println(e);
        }
    }

    private static class HideAWTListener
    extends ComponentAdapter
    implements AWTEventListener,
    WindowStateListener {
        private HideAWTListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mv;
            if (aWTEvent instanceof MouseEvent && (mv = (MouseEvent)aWTEvent).getID() == 500 && mv.getClickCount() > 0) {
                if (!(aWTEvent.getSource() instanceof Component)) {
                    PopupUtil.hidePopup();
                    return;
                }
                Component comp = (Component)aWTEvent.getSource();
                Container par = SwingUtilities.getAncestorNamed("popupComponent", comp);
                if (par == null) {
                    PopupUtil.hidePopup();
                }
            }
        }

        @Override
        public void windowStateChanged(WindowEvent windowEvent) {
            if (popupWindow != null) {
                int oldState = windowEvent.getOldState();
                int newState = windowEvent.getNewState();
                if ((oldState & 1) == 0 && (newState & 1) == 1) {
                    PopupUtil.hidePopup();
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent evt) {
            if (popupWindow != null) {
                PopupUtil.resizePopup();
            }
        }

        @Override
        public void componentMoved(ComponentEvent evt) {
            if (popupWindow != null) {
                PopupUtil.resizePopup();
            }
        }
    }

}

