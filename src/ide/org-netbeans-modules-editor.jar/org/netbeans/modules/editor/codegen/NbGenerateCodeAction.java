/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.spi.editor.codegen.CodeGenerator
 *  org.netbeans.spi.editor.codegen.CodeGenerator$Factory
 *  org.netbeans.spi.editor.codegen.CodeGeneratorContextProvider
 *  org.netbeans.spi.editor.codegen.CodeGeneratorContextProvider$Task
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.editor.codegen;

import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.MainMenuAction;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.codegen.GenerateCodePanel;
import org.netbeans.modules.editor.codegen.PopupUtil;
import org.netbeans.spi.editor.codegen.CodeGenerator;
import org.netbeans.spi.editor.codegen.CodeGeneratorContextProvider;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public class NbGenerateCodeAction
extends BaseAction {
    public static final String generateCode = "generate-code";

    public NbGenerateCodeAction() {
        this.putValue("trimmed-text", (Object)NbBundle.getBundle(NbGenerateCodeAction.class).getString("generate-code-trimmed"));
    }

    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        final Task task = new Task(NbGenerateCodeAction.getFullMimePath(target.getDocument(), target.getCaretPosition()));
        final AtomicBoolean cancel = new AtomicBoolean();
        ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                if (cancel != null && cancel.get()) {
                    return;
                }
                task.run(Lookups.singleton((Object)target));
                if (cancel != null && cancel.get()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (task.codeGenerators.size() > 0) {
                            int altHeight = -1;
                            Point where = null;
                            try {
                                Rectangle carretRectangle = target.modelToView(target.getCaretPosition());
                                altHeight = carretRectangle.height;
                                where = new Point(carretRectangle.x, carretRectangle.y + carretRectangle.height);
                                SwingUtilities.convertPointToScreen(where, target);
                            }
                            catch (BadLocationException ble) {
                                // empty catch block
                            }
                            if (where == null) {
                                where = new Point(-1, -1);
                            }
                            PopupUtil.showPopup(new GenerateCodePanel(target, task.codeGenerators), (Frame)SwingUtilities.getAncestorOfClass(Frame.class, target), where.x, where.y, true, altHeight);
                        } else {
                            target.getToolkit().beep();
                        }
                    }
                });
            }

        }, (String)NbBundle.getBundle(NbGenerateCodeAction.class).getString("generate-code-trimmed"), (AtomicBoolean)cancel, (boolean)false);
    }

    static String[] test(Document doc, int pos) {
        Task task = new Task(NbGenerateCodeAction.getFullMimePath(doc, pos));
        task.run(Lookups.fixed((Object[])new Object[0]));
        String[] ret = new String[task.codeGenerators.size()];
        int i = 0;
        for (CodeGenerator codeGenerator : task.codeGenerators) {
            ret[i++] = codeGenerator.getDisplayName();
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static MimePath getFullMimePath(Document document, int offset) {
        String langPath;
        langPath = null;
        if (document instanceof AbstractDocument) {
            AbstractDocument adoc = (AbstractDocument)document;
            adoc.readLock();
            try {
                List list = TokenHierarchy.get((Document)document).embeddedTokenSequences(offset, true);
                if (list.size() > 1) {
                    langPath = ((TokenSequence)list.get(list.size() - 1)).languagePath().mimePath();
                }
            }
            finally {
                adoc.readUnlock();
            }
        }
        if (langPath == null) {
            langPath = NbEditorUtilities.getMimeType(document);
        }
        if (langPath != null) {
            return MimePath.parse((String)langPath);
        }
        return null;
    }

    public static final class GlobalAction
    extends MainMenuAction {
        public GlobalAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(GlobalAction.class).getString("generate-code-main-menu-source-item");
        }

        @Override
        protected String getActionName() {
            return "generate-code";
        }
    }

    private static class Task
    implements CodeGeneratorContextProvider.Task {
        private MimePath mimePath;
        private Iterator<? extends CodeGeneratorContextProvider> contextProviders;
        private List<CodeGenerator> codeGenerators = new ArrayList<CodeGenerator>();

        private Task(MimePath mimePath) {
            this.mimePath = mimePath;
            this.contextProviders = MimeLookup.getLookup((MimePath)mimePath).lookupAll(CodeGeneratorContextProvider.class).iterator();
        }

        public void run(Lookup context) {
            if (this.contextProviders.hasNext()) {
                this.contextProviders.next().runTaskWithinContext(context, (CodeGeneratorContextProvider.Task)this);
            } else {
                for (CodeGenerator.Factory factory : MimeLookup.getLookup((MimePath)this.mimePath).lookupAll(CodeGenerator.Factory.class)) {
                    this.codeGenerators.addAll(factory.create(context));
                }
            }
        }
    }

}

