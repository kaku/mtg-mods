/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.actions.EditorActionUtilities
 */
package org.netbeans.modules.editor;

import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.options.AnnotationTypesFolder;

public class EditorWarmUpTask
implements Runnable {
    @Override
    public void run() {
        AnnotationTypesFolder.getAnnotationTypesFolder();
        EditorActionUtilities.getGlobalPreferences();
    }
}

