/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.ImplementationProvider
 */
package org.netbeans.modules.editor;

import java.util.ResourceBundle;
import javax.swing.Action;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.ImplementationProvider;
import org.netbeans.modules.editor.impl.NbEditorImplementationProvider;

public class NbImplementationProvider
extends ImplementationProvider {
    public static final String GLYPH_GUTTER_ACTIONS_FOLDER_NAME = "GlyphGutterActions";
    private transient NbEditorImplementationProvider provider = new NbEditorImplementationProvider();

    public ResourceBundle getResourceBundle(String localizer) {
        return this.provider.getResourceBundle(localizer);
    }

    public Action[] getGlyphGutterActions(JTextComponent target) {
        return this.provider.getGlyphGutterActions(target);
    }

    public boolean activateComponent(JTextComponent c) {
        return this.provider.activateComponent(c);
    }
}

