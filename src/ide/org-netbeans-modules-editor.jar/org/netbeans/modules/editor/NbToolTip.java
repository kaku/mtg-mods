/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.Annotations
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.PopupManager
 *  org.netbeans.editor.PopupManager$HorizontalBounds
 *  org.netbeans.editor.PopupManager$Placement
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ToolTipSupport
 *  org.netbeans.lib.editor.hyperlink.HyperlinkOperation
 *  org.netbeans.lib.editor.hyperlink.HyperlinkOperation$TooltipInfo
 *  org.netbeans.modules.editor.lib2.highlighting.HighlightingManager
 *  org.netbeans.modules.editor.lib2.view.DocumentView
 *  org.netbeans.spi.editor.highlighting.HighlightAttributeValue
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.Annotatable
 *  org.openide.text.Annotation
 *  org.openide.text.Line
 *  org.openide.text.Line$Part
 *  org.openide.text.Line$Set
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.editor;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.PopupManager;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.lib.editor.hyperlink.HyperlinkOperation;
import org.netbeans.modules.editor.NbEditorKit;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.spi.editor.highlighting.HighlightAttributeValue;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.Annotatable;
import org.openide.text.Annotation;
import org.openide.text.Line;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.Lookups;

public class NbToolTip
extends FileChangeAdapter {
    private static final Logger LOG = Logger.getLogger(NbToolTip.class.getName());
    private static final HashMap<String, WeakReference<NbToolTip>> mime2tip = new HashMap();
    private static final AtomicInteger lastRequestId = new AtomicInteger(0);
    private final String mimeType;
    private Annotation[] tipAnnotations;
    private static final RequestProcessor toolTipRP = new RequestProcessor("ToolTip-Evaluator", 1);
    private static volatile Reference<RequestProcessor.Task> lastToolTipTask = new WeakReference<Object>(null);

    static synchronized void buildToolTip(JTextComponent target) {
        String mimeType = NbEditorUtilities.getMimeType(target.getDocument());
        NbToolTip tip = NbToolTip.getTip(mimeType);
        tip.buildTip(target);
    }

    private static int newRequestId() {
        return lastRequestId.incrementAndGet();
    }

    private static int getLastRequestId() {
        return lastRequestId.get();
    }

    private NbToolTip(String mimeType) {
        this.mimeType = mimeType;
    }

    private static NbToolTip getTip(String mimeType) {
        NbToolTip tip;
        WeakReference<NbToolTip> nttWr = mime2tip.get(mimeType);
        NbToolTip nbToolTip = tip = nttWr == null ? null : nttWr.get();
        if (tip == null) {
            tip = new NbToolTip(mimeType);
            mime2tip.put(mimeType, new WeakReference<NbToolTip>(tip));
        }
        return tip;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private Annotation[] getTipAnnotations() {
        Class<NbToolTip> class_ = NbToolTip.class;
        synchronized (NbToolTip.class) {
            Collection res;
            Lookup l;
            Annotation[] annos = this.tipAnnotations;
            // ** MonitorExit[var2_1] (shouldn't be in output)
            if (annos != null) return annos;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Searching for tooltip annotations for mimeType = '" + this.mimeType + "'");
            }
            if ((res = (l = Lookups.forPath((String)("Editors/" + this.mimeType + "/ToolTips"))).lookupAll(Annotation.class)).contains(null)) {
                throw new IllegalStateException("Lookup returning null instance: " + (Object)l);
            }
            annos = res.toArray((T[])new Annotation[res.size()]);
            Class<NbToolTip> class_2 = NbToolTip.class;
            synchronized (NbToolTip.class) {
                this.tipAnnotations = annos;
                // ** MonitorExit[var4_5] (shouldn't be in output)
                return annos;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void buildTip(JTextComponent target) {
        DataObject dob;
        ToolTipSupport tts;
        EditorCookie ec;
        BaseDocument doc;
        Point p;
        EditorUI eui = Utilities.getEditorUI((JTextComponent)target);
        ToolTipSupport toolTipSupport = tts = eui == null ? null : eui.getToolTipSupport();
        if (tts == null) {
            return;
        }
        MouseEvent lastMouseEvent = tts.getLastMouseEvent();
        if (lastMouseEvent == null) {
            return;
        }
        doc = Utilities.getDocument((JTextComponent)target);
        p = lastMouseEvent.getPoint();
        doc.readLock();
        try {
            JComponent toolTip;
            Rectangle2D alloc;
            String toolTipText = target.getUI().getToolTipText(target, tts.getLastMouseEvent().getPoint());
            if (toolTipText != null) {
                return;
            }
            assert (tts.getLastMouseEvent().getSource() == target);
            DocumentView docView = DocumentView.get((JTextComponent)target);
            if (docView != null && (alloc = docView.getAllocation()) != null && (toolTip = docView.getToolTip(p.getX(), p.getY(), (Shape)alloc)) != null) {
                String tooltipType = (String)toolTip.getClientProperty("tooltip-type");
                if ("fold-preview".equals(tooltipType)) {
                    tts.setToolTip(toolTip, PopupManager.ViewPortBounds, PopupManager.BelowPreferred, -1, 0);
                } else {
                    tts.setToolTip(toolTip);
                }
                return;
            }
        }
        finally {
            doc.readUnlock();
        }
        Annotation[] annos = this.getTipAnnotations();
        if (doc != null && (dob = NbEditorUtilities.getDataObject((Document)doc)) != null && dob.isValid() && (ec = (EditorCookie)dob.getCookie(EditorCookie.class)) != null) {
            StyledDocument openedDoc = ec.getDocument();
            if (openedDoc != doc) {
                return;
            }
            doc.readLock();
            try {
                BaseKit kit;
                int offset = NbToolTip.getOffsetForPoint(p, target, doc);
                if (offset >= 0 && (kit = Utilities.getKit((JTextComponent)target)) instanceof NbEditorKit) {
                    Object tooltipAttributeValue = null;
                    Line.Part lp = null;
                    Annotation[] tooltipAnnotations = null;
                    AnnotationDesc annoDesc = null;
                    boolean tops = false;
                    do {
                        HighlightsSequence seq;
                        HighlightsContainer highlights;
                        if ((seq = (highlights = tops ? HighlightingManager.getInstance((JTextComponent)target).getBottomHighlights() : HighlightingManager.getInstance((JTextComponent)target).getTopHighlights()).getHighlights(offset, offset + 1)).moveNext()) {
                            tooltipAttributeValue = seq.getAttributes().getAttribute(EditorStyleConstants.Tooltip);
                        }
                        boolean bl = tops = !tops;
                    } while (tooltipAttributeValue == null && tops);
                    if (annos != null) {
                        Line l;
                        int line = Utilities.getLineOffset((BaseDocument)doc, (int)offset);
                        int col = offset - Utilities.getRowStartFromLineOffset((BaseDocument)doc, (int)line);
                        Line.Set ls = ec.getLineSet();
                        if (ls != null && (l = ls.getCurrent(line)) != null && (lp = l.createPart(col, 0)) != null) {
                            annoDesc = doc.getAnnotations().getActiveAnnotation(line);
                            if (annoDesc != null && (offset < annoDesc.getOffset() || offset >= annoDesc.getOffset() + annoDesc.getLength())) {
                                annoDesc = null;
                            }
                            tooltipAnnotations = annos;
                        }
                    }
                    if (lp != null && tooltipAnnotations != null || tooltipAttributeValue != null) {
                        int requestId = NbToolTip.newRequestId();
                        RequestProcessor.Task lttt = lastToolTipTask.get();
                        if (lttt != null) {
                            lttt.cancel();
                        }
                        lastToolTipTask = new WeakReference<RequestProcessor.Task>(toolTipRP.post((Runnable)new Request(annoDesc, tooltipAnnotations, lp, offset, tooltipAttributeValue, tts, target, (AbstractDocument)doc, (NbEditorKit)kit, requestId)));
                    }
                }
            }
            catch (BadLocationException ble) {
                LOG.log(Level.FINE, null, ble);
            }
            finally {
                doc.readUnlock();
            }
        }
    }

    private static int getOffsetForPoint(Point p, JTextComponent c, BaseDocument doc) throws BadLocationException {
        if (p.x >= 0 && p.y >= 0) {
            int offset = c.viewToModel(p);
            Rectangle r = c.modelToView(offset);
            EditorUI eui = Utilities.getEditorUI((JTextComponent)c);
            int relY = p.y - r.y;
            if (eui != null && relY < eui.getLineHeight() && offset < Utilities.getRowEnd((BaseDocument)doc, (int)offset)) {
                return offset;
            }
        }
        return -1;
    }

    private static class Request
    implements Runnable,
    PropertyChangeListener,
    DocumentListener {
        private ToolTipSupport tts;
        private final Annotation[] annos;
        private final AnnotationDesc annoDesc;
        private final Line.Part linePart;
        private final JTextComponent component;
        private final AbstractDocument doc;
        private final NbEditorKit kit;
        private final int offset;
        private final Object tooltipAttributeValue;
        private final int requestId;
        private boolean documentModified;

        Request(AnnotationDesc annoDesc, Annotation[] annos, Line.Part lp, int offset, Object tooltipAttributeValue, ToolTipSupport tts, JTextComponent component, AbstractDocument doc, NbEditorKit kit, int requestId) {
            this.annoDesc = annoDesc;
            this.annos = annos;
            this.linePart = lp;
            this.tts = tts;
            this.component = component;
            this.doc = doc;
            this.kit = kit;
            this.offset = offset;
            this.tooltipAttributeValue = tooltipAttributeValue;
            this.requestId = requestId;
        }

        @Override
        public void run() {
            CharSequence tooltipText;
            if (this.tts == null) {
                return;
            }
            if (this.tts == null || this.tts.getStatus() == 0) {
                return;
            }
            if (!this.isRequestValid()) {
                return;
            }
            if (this.tts != null) {
                this.tts.addPropertyChangeListener((PropertyChangeListener)this);
            }
            if ((tooltipText = this.resolveTooltipText()) != null && tooltipText.length() > 0 && this.isRequestValid()) {
                Utilities.runInEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        ToolTipSupport ftts = Request.this.tts;
                        if (ftts != null) {
                            JComponent tt;
                            ftts.setToolTipText(tooltipText.toString());
                            if (tooltipText instanceof HyperlinkOperation.TooltipInfo && (tt = ftts.getToolTip()) instanceof JEditorPane) {
                                ((JEditorPane)tt).addHyperlinkListener(((HyperlinkOperation.TooltipInfo)tooltipText).getListener());
                                ((JEditorPane)tt).setEditable(false);
                            }
                        }
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private CharSequence resolveTooltipText() {
            this.kit.toolTipAnnotationsLock(this.doc);
            try {
                this.doc.readLock();
                try {
                    if (!this.isRequestValid()) {
                        CharSequence charSequence = null;
                        return charSequence;
                    }
                    String tooltipFromAnnotations = null;
                    if (this.annos != null) {
                        int i;
                        for (i = 0; i < this.annos.length; ++i) {
                            this.annos[i].attach((Annotatable)this.linePart);
                        }
                        if (this.annoDesc != null) {
                            tooltipFromAnnotations = this.annoDesc.getShortDescription();
                            this.annoDesc.addPropertyChangeListener((PropertyChangeListener)this);
                        } else {
                            for (i = 0; i < this.annos.length; ++i) {
                                String desc = this.annos[i].getShortDescription();
                                if (desc != null) {
                                    tooltipFromAnnotations = desc;
                                }
                                this.annos[i].addPropertyChangeListener((PropertyChangeListener)this);
                            }
                        }
                    }
                    if (tooltipFromAnnotations != null) {
                        String i = tooltipFromAnnotations;
                        return i;
                    }
                }
                finally {
                    this.doc.readUnlock();
                }
            }
            finally {
                this.kit.toolTipAnnotationsUnlock(this.doc);
            }
            if (!this.isRequestValid()) {
                return null;
            }
            return this.getTooltipFromHighlightingLayers();
        }

        private CharSequence getTooltipFromHighlightingLayers() {
            CharSequence tooltipFromHighlightingLayers = null;
            if (this.tooltipAttributeValue != null) {
                if (this.tooltipAttributeValue instanceof String) {
                    tooltipFromHighlightingLayers = (String)this.tooltipAttributeValue;
                } else if (this.tooltipAttributeValue instanceof HighlightAttributeValue) {
                    CharSequence value = (CharSequence)((HighlightAttributeValue)this.tooltipAttributeValue).getValue(this.component, (Document)this.doc, EditorStyleConstants.Tooltip, this.offset, this.offset + 1);
                    tooltipFromHighlightingLayers = value;
                } else {
                    LOG.fine("Invalid '" + EditorStyleConstants.Tooltip + "' attribute value " + this.tooltipAttributeValue);
                }
            }
            return tooltipFromHighlightingLayers;
        }

        private boolean isRequestValid() {
            return NbToolTip.getLastRequestId() == this.requestId && !this.documentModified && this.isDocumentValid();
        }

        private boolean isDocumentValid() {
            EditorCookie ec;
            DataObject dob = NbEditorUtilities.getDataObject(this.doc);
            if (dob != null && (ec = (EditorCookie)dob.getCookie(EditorCookie.class)) != null) {
                StyledDocument openedDoc;
                try {
                    openedDoc = ec.openDocument();
                }
                catch (IOException e) {
                    openedDoc = null;
                }
                return openedDoc == this.doc;
            }
            return false;
        }

        private void dismiss() {
            if (this.tts != null) {
                this.tts.removePropertyChangeListener((PropertyChangeListener)this);
            }
            this.tts = null;
            if (this.annoDesc != null) {
                this.annoDesc.removePropertyChangeListener((PropertyChangeListener)this);
            } else if (this.annos != null) {
                for (int i = 0; i < this.annos.length; ++i) {
                    this.annos[i].removePropertyChangeListener((PropertyChangeListener)this);
                    this.annos[i].detach();
                }
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propName = evt.getPropertyName();
            if ("shortDescription".equals(propName) || "shortDescription".equals(propName)) {
                final Object newValue = evt.getNewValue();
                toolTipRP.post(new Runnable(){

                    @Override
                    public void run() {
                        String tipText;
                        if (newValue != null) {
                            tipText = (String)newValue;
                        } else if (Request.this.isRequestValid()) {
                            tipText = Request.this.getTooltipFromHighlightingLayers().toString();
                            if (tipText == null || tipText.isEmpty()) {
                                return;
                            }
                        } else {
                            return;
                        }
                        if (tipText != null) {
                            Utilities.runInEventDispatchThread((Runnable)new Runnable(){

                                @Override
                                public void run() {
                                    ToolTipSupport ftts = Request.this.tts;
                                    if (ftts != null) {
                                        ftts.setToolTipText(tipText);
                                    }
                                }
                            });
                        }
                    }

                });
            } else if ("status".equals(propName) && (Integer)evt.getNewValue() == 0) {
                this.dismiss();
            }
        }

        @Override
        public void insertUpdate(DocumentEvent evt) {
            this.documentModified = true;
        }

        @Override
        public void removeUpdate(DocumentEvent evt) {
            this.documentModified = true;
        }

        @Override
        public void changedUpdate(DocumentEvent evt) {
        }

    }

}

