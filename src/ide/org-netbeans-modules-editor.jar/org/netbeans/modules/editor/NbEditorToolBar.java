/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.MultiKeymap
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.actions.EditorActionUtilities
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.ToolbarWithOverflow
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Toolbar
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.TextUI;
import javax.swing.plaf.ToolBarUI;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.MultiKeymap;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.impl.ToolbarActionsProvider;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.openide.awt.MouseUtils;
import org.openide.awt.ToolbarWithOverflow;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.ContextAwareAction;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

final class NbEditorToolBar
extends ToolbarWithOverflow {
    private static final Logger LOG = Logger.getLogger(NbEditorToolBar.class.getName());
    private static final boolean debugSort = Boolean.getBoolean("netbeans.debug.editor.toolbar.sort");
    private static final Insets BUTTON_INSETS = new Insets(2, 1, 0, 1);
    private static final Lookup NO_ACTION_CONTEXT = Lookups.fixed((Object[])new Object[0]);
    private FileChangeListener moduleRegListener;
    private static final MouseListener sharedMouseListener = new MouseUtils.PopupMouseAdapter(){

        public void mouseEntered(MouseEvent evt) {
            AbstractButton button;
            Object src = evt.getSource();
            if (src instanceof AbstractButton && (button = (AbstractButton)evt.getSource()).isEnabled()) {
                button.setContentAreaFilled(true);
                button.setBorderPainted(true);
            }
        }

        public void mouseExited(MouseEvent evt) {
            Object src = evt.getSource();
            if (src instanceof AbstractButton) {
                AbstractButton button = (AbstractButton)evt.getSource();
                NbEditorToolBar.removeButtonContentAreaAndBorder(button);
            }
        }

        protected void showPopup(MouseEvent evt) {
        }
    };
    private Reference componentRef;
    private boolean presentersAdded;
    private boolean addListener = true;
    private static final String NOOP_ACTION_KEY = "noop-action-key";
    private static final Action NOOP_ACTION = new NoOpAction();
    private final Lookup.Result<KeyBindingSettings> lookupResult;
    private final LookupListener keybindingsTracker;
    private final Preferences preferences;
    private final PreferenceChangeListener prefsTracker;

    public NbEditorToolBar(JTextComponent component) {
        this.keybindingsTracker = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                NbEditorToolBar.this.refreshToolbarButtons();
            }
        };
        this.prefsTracker = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String settingName;
                String string = settingName = evt == null ? null : evt.getKey();
                if (settingName == null || "toolbarVisible".equals(settingName)) {
                    NbEditorToolBar.this.refreshToolbarButtons();
                }
            }
        };
        this.componentRef = new WeakReference<JTextComponent>(component);
        this.setFloatable(false);
        this.addMouseListener(sharedMouseListener);
        this.installModulesInstallationListener();
        this.installNoOpActionMappings();
        this.lookupResult = MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)component)).lookupResult(KeyBindingSettings.class);
        this.lookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.keybindingsTracker, this.lookupResult));
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)component);
        this.preferences = (Preferences)MimeLookup.getLookup((MimePath)(mimeType == null ? MimePath.EMPTY : MimePath.parse((String)mimeType))).lookup(Preferences.class);
        this.preferences.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsTracker, (Object)this.preferences));
        this.refreshToolbarButtons();
        this.setBorderPainted(true);
    }

    public void addNotify() {
        super.addNotify();
        this.setBorder((Border)new LineBorder(this.getBackground(), 1));
    }

    private void installNoOpActionMappings() {
        int i;
        InputMap im = this.getInputMap(1);
        KeyStroke[] keys = this.findEditorKeys("cut-to-clipboard", KeyStroke.getKeyStroke(88, 2));
        for (i = 0; i < keys.length; ++i) {
            im.put(keys[i], "noop-action-key");
        }
        keys = this.findEditorKeys("copy-to-clipboard", KeyStroke.getKeyStroke(67, 2));
        for (i = 0; i < keys.length; ++i) {
            im.put(keys[i], "noop-action-key");
        }
        keys = this.findEditorKeys("delete-next", KeyStroke.getKeyStroke(127, 0));
        for (i = 0; i < keys.length; ++i) {
            im.put(keys[i], "noop-action-key");
        }
        keys = this.findEditorKeys("paste-from-clipboard", KeyStroke.getKeyStroke(86, 2));
        for (i = 0; i < keys.length; ++i) {
            im.put(keys[i], "noop-action-key");
        }
        this.getActionMap().put("noop-action-key", NOOP_ACTION);
    }

    private void installModulesInstallationListener() {
        this.moduleRegListener = new FileChangeAdapter(){

            public void fileChanged(FileEvent fe) {
                Runnable r = new Runnable(){

                    @Override
                    public void run() {
                        if (NbEditorToolBar.this.isToolbarVisible()) {
                            NbEditorToolBar.this.checkPresentersRemoved();
                            NbEditorToolBar.this.checkPresentersAdded();
                        }
                    }
                };
                Utilities.runInEventDispatchThread((Runnable)r);
            }

        };
        FileObject moduleRegistry = FileUtil.getConfigFile((String)"Modules");
        if (moduleRegistry != null) {
            moduleRegistry.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this.moduleRegListener, (Object)moduleRegistry));
        }
    }

    public String getUIClassID() {
        if (UIManager.get("Nb.Toolbar.ui") != null) {
            return "Nb.Toolbar.ui";
        }
        return super.getUIClassID();
    }

    public String getName() {
        return "editorToolbar";
    }

    public void setUI(ToolBarUI ui) {
        this.addListener = false;
        super.setUI(ui);
        this.addListener = true;
    }

    public synchronized void addMouseListener(MouseListener l) {
        if (this.addListener) {
            super.addMouseListener(l);
        }
    }

    public synchronized void addMouseMotionListener(MouseMotionListener l) {
        if (this.addListener) {
            super.addMouseMotionListener(l);
        }
    }

    private boolean isToolbarVisible() {
        return this.preferences.getBoolean("toolbarVisible", true);
    }

    private void refreshToolbarButtons() {
        final JTextComponent c = this.getComponent();
        final boolean visible = this.isToolbarVisible();
        Runnable r = new Runnable(){

            @Override
            public void run() {
                if (visible) {
                    NbEditorToolBar.this.checkPresentersAdded();
                    if (c != null) {
                        NbEditorToolBar.this.installNoOpActionMappings();
                        Map keybsMap = NbEditorToolBar.this.getKeyBindingMap();
                        Component[] comps = NbEditorToolBar.this.getComponents();
                        for (int i = 0; i < comps.length; ++i) {
                            Action action;
                            int index;
                            String actionName;
                            JButton button;
                            MultiKeyBinding mkb;
                            Component comp = comps[i];
                            if (!(comp instanceof JButton) || (action = (button = (JButton)comp).getAction()) == null || (actionName = (String)action.getValue("Name")) == null) continue;
                            String tooltipText = button.getToolTipText();
                            if (tooltipText != null && (index = tooltipText.indexOf("(")) > 0) {
                                tooltipText = tooltipText.substring(0, index - 1);
                            }
                            if ((mkb = (MultiKeyBinding)keybsMap.get(actionName)) != null) {
                                button.setToolTipText(tooltipText + " (" + EditorActionUtilities.getKeyMnemonic((MultiKeyBinding)mkb) + ")");
                                continue;
                            }
                            button.setToolTipText(tooltipText);
                        }
                    }
                } else {
                    NbEditorToolBar.this.checkPresentersRemoved();
                }
                NbEditorToolBar.this.setVisible(visible);
            }
        };
        Utilities.runInEventDispatchThread((Runnable)r);
    }

    private void checkPresentersAdded() {
        if (!this.presentersAdded) {
            this.presentersAdded = true;
            this.addPresenters();
        }
    }

    private void checkPresentersRemoved() {
        this.presentersAdded = false;
        this.removeAll();
    }

    private Map<String, MultiKeyBinding> getKeyBindingMap() {
        HashMap<String, MultiKeyBinding> map = new HashMap<String, MultiKeyBinding>();
        List<? extends MultiKeyBinding> list = this.getKeyBindingList();
        for (MultiKeyBinding mkb : list) {
            map.put(mkb.getActionName(), mkb);
        }
        return map;
    }

    private List<? extends MultiKeyBinding> getKeyBindingList() {
        Collection c = this.lookupResult.allInstances();
        if (!c.isEmpty()) {
            KeyBindingSettings kbs = (KeyBindingSettings)c.iterator().next();
            return kbs.getKeyBindings();
        }
        return Collections.emptyList();
    }

    public static void initKeyBindingList(String mimeType) {
        Collection c = MimeLookup.getLookup((String)mimeType).lookupAll(KeyBindingSettings.class);
        if (!c.isEmpty()) {
            c.iterator().next();
        }
    }

    private JTextComponent getComponent() {
        return (JTextComponent)this.componentRef.get();
    }

    private void addPresenters() {
        String mimeType;
        JTextComponent c = this.getComponent();
        String string = mimeType = c == null ? null : NbEditorUtilities.getMimeType(c);
        if (mimeType == null) {
            return;
        }
        List keybindings = null;
        Lookup actionContext = null;
        ArrayList<JSeparator> items = ToolbarActionsProvider.getToolbarItems(mimeType);
        List oldTextBaseItems = ToolbarActionsProvider.getToolbarItems("text/base");
        if (oldTextBaseItems.size() > 0) {
            items = new ArrayList<JSeparator>(items);
            items.add(new JSeparator());
            items.addAll(oldTextBaseItems);
        }
        Iterator i$ = items.iterator();
        while (i$.hasNext()) {
            Component presenter;
            EditorKit kit;
            Object item = i$.next();
            LOG.log(Level.FINE, "Adding item {0}", item);
            if (item == null || item instanceof JSeparator) {
                this.addSeparator();
                continue;
            }
            if (item instanceof String && (kit = c.getUI().getEditorKit(c)) instanceof BaseKit) {
                Action a = ((BaseKit)kit).getActionByName((String)item);
                if (a == null) continue;
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Item {0} converted to an editor action {1}", new Object[]{item, NbEditorToolBar.s2s(a)});
                }
                item = a;
            }
            if (item instanceof ContextAwareAction) {
                Action caa;
                if (actionContext == null) {
                    Lookup context = NbEditorToolBar.createActionContext(c);
                    Lookup lookup = actionContext = context == null ? NO_ACTION_CONTEXT : context;
                }
                if (actionContext != NO_ACTION_CONTEXT && ((caa = ((ContextAwareAction)item).createContextAwareInstance(actionContext)) instanceof Presenter.Toolbar || caa instanceof Component)) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.log(Level.FINE, "Item {0} converted to a context-aware Action {1}", new Object[]{NbEditorToolBar.s2s(item), NbEditorToolBar.s2s(caa)});
                    }
                    item = caa;
                }
            }
            if (item instanceof Presenter.Toolbar && (presenter = ((Presenter.Toolbar)item).getToolbarPresenter()) != null) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Item {0} converted to a Presenter.Toolbar {1}", new Object[]{NbEditorToolBar.s2s(item), NbEditorToolBar.s2s(presenter)});
                }
                if (presenter instanceof JComponent) {
                    ((JComponent)presenter).putClientProperty(JTextComponent.class, c);
                }
                item = presenter;
            }
            if (item instanceof Component) {
                this.add((Component)item);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Adding component {0}", NbEditorToolBar.s2s(item));
                }
            } else if (item instanceof Action) {
                WrapperAction a = new WrapperAction(this.componentRef, (Action)item);
                NbEditorToolBar.updateIcon(a);
                item = this.add((Action)a);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Adding action {0} as {1}", new Object[]{NbEditorToolBar.s2s(a), NbEditorToolBar.s2s(item)});
                }
            } else {
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.log(Level.FINE, "Ignoring item {0}", NbEditorToolBar.s2s(item));
                continue;
            }
            if (!(item instanceof AbstractButton)) continue;
            AbstractButton button = (AbstractButton)item;
            this.processButton(button);
            if (keybindings == null) {
                List l = this.getKeyBindingList();
                keybindings = l == null ? Collections.emptyList() : l;
            }
            NbEditorToolBar.updateTooltip(button, keybindings);
            if (!LOG.isLoggable(Level.FINE)) continue;
            LOG.log(Level.FINE, "Special treatment for button {0}", NbEditorToolBar.s2s(item));
        }
    }

    private static void updateIcon(Action a) {
        ImageIcon img;
        String resourceId;
        Object icon = a.getValue("SmallIcon");
        if (icon == null && (resourceId = (String)a.getValue("IconResource")) != null && (img = ImageUtilities.loadImageIcon((String)resourceId, (boolean)true)) != null) {
            a.putValue("SmallIcon", img);
        }
    }

    private static void updateTooltip(AbstractButton b, List<? extends MultiKeyBinding> keybindings) {
        String actionName;
        Action a = b.getAction();
        String string = actionName = a == null ? null : (String)a.getValue("Name");
        if (actionName == null) {
            return;
        }
        for (MultiKeyBinding mkb : keybindings) {
            if (!actionName.equals(mkb.getActionName())) continue;
            b.setToolTipText(b.getToolTipText() + " (" + EditorActionUtilities.getKeyMnemonic((MultiKeyBinding)mkb) + ")");
            break;
        }
    }

    static Lookup createActionContext(JTextComponent c) {
        DataObject dobj;
        Lookup nodeLookup = null;
        DataObject dataObject = dobj = c != null ? NbEditorUtilities.getDataObject(c.getDocument()) : null;
        if (dobj != null && dobj.isValid()) {
            nodeLookup = dobj.getNodeDelegate().getLookup();
        }
        Lookup ancestorLookup = null;
        for (Container comp = c; comp != null; comp = comp.getParent()) {
            Lookup lookup;
            if (!(comp instanceof Lookup.Provider) || (lookup = ((Lookup.Provider)comp).getLookup()) == null) continue;
            ancestorLookup = lookup;
            break;
        }
        Lookup componentLookup = Lookups.singleton((Object)c);
        if (nodeLookup == null && ancestorLookup == null) {
            return componentLookup;
        }
        if (nodeLookup == null) {
            return new ProxyLookup(new Lookup[]{ancestorLookup, componentLookup});
        }
        if (ancestorLookup == null) {
            return new ProxyLookup(new Lookup[]{nodeLookup, componentLookup});
        }
        assert (nodeLookup != null && ancestorLookup != null);
        Node node = (Node)nodeLookup.lookup(Node.class);
        boolean ancestorLookupContainsNode = ancestorLookup.lookup(new Lookup.Template(Node.class)).allInstances().contains((Object)node);
        if (ancestorLookupContainsNode) {
            return new ProxyLookup(new Lookup[]{ancestorLookup, componentLookup});
        }
        return new ProxyLookup(new Lookup[]{nodeLookup, ancestorLookup, componentLookup});
    }

    private void processButton(AbstractButton button) {
        NbEditorToolBar.removeButtonContentAreaAndBorder(button);
        button.setMargin(BUTTON_INSETS);
        if (button instanceof AbstractButton) {
            button.addMouseListener(sharedMouseListener);
        }
        button.setFocusable(false);
    }

    private static void removeButtonContentAreaAndBorder(AbstractButton button) {
        boolean canRemove = true;
        if (button instanceof JToggleButton) {
            boolean bl = canRemove = !button.isSelected();
        }
        if (canRemove) {
            button.setContentAreaFilled(false);
            button.setBorderPainted(false);
        }
    }

    private KeyStroke[] findEditorKeys(String editorActionName, KeyStroke defaultKey) {
        KeyStroke[] ret = new KeyStroke[]{defaultKey};
        JTextComponent comp = this.getComponent();
        if (editorActionName != null && comp != null) {
            EditorKit kit;
            Action a;
            TextUI textUI = comp.getUI();
            Keymap km = comp.getKeymap();
            if (textUI != null && km != null && (kit = textUI.getEditorKit(comp)) instanceof BaseKit && (a = ((BaseKit)kit).getActionByName(editorActionName)) != null) {
                KeyStroke[] keys = km.getKeyStrokesForAction(a);
                if (keys != null && keys.length > 0) {
                    ret = keys;
                } else {
                    MultiKeymap km2 = ((BaseKit)kit).getKeymap();
                    KeyStroke[] keys2 = km2.getKeyStrokesForAction(a);
                    if (keys2 != null && keys2.length > 0) {
                        ret = keys2;
                    }
                }
            }
        }
        return ret;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    private static final class WrapperAction
    implements Action {
        private final Reference componentRef;
        private final Action delegate;

        WrapperAction(Reference componentRef, Action delegate) {
            this.componentRef = componentRef;
            assert (delegate != null);
            this.delegate = delegate;
        }

        @Override
        public Object getValue(String key) {
            return this.delegate.getValue(key);
        }

        @Override
        public void putValue(String key, Object value) {
            this.delegate.putValue(key, value);
        }

        @Override
        public void setEnabled(boolean b) {
            this.delegate.setEnabled(b);
        }

        @Override
        public boolean isEnabled() {
            return this.delegate.isEnabled();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            this.delegate.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            this.delegate.removePropertyChangeListener(listener);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JTextComponent c = (JTextComponent)this.componentRef.get();
            if (c != null) {
                e = new ActionEvent(c, e.getID(), e.getActionCommand());
            }
            this.delegate.actionPerformed(e);
        }
    }

    private static final class NoOpAction
    extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }

}

