/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.AcceleratorBinding
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JSeparator;
import org.openide.awt.AcceleratorBinding;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public class ActionsList {
    private static final Logger LOG = Logger.getLogger(ActionsList.class.getName());
    private final List<Object> all;
    private final List<Action> actions;

    protected ActionsList(List<FileObject> keys, boolean ignoreFolders, boolean prohibitSeparatorsAndActionNames) {
        Pair p = ActionsList.convertImpl(keys == null ? Collections.emptyList() : keys, ignoreFolders, prohibitSeparatorsAndActionNames);
        this.all = p.all;
        this.actions = p.actions;
    }

    public List<Object> getAllInstances() {
        return this.all;
    }

    public List<Action> getActionsOnly() {
        return this.actions;
    }

    public static List<Object> convert(List<FileObject> keys, boolean prohibitSeparatorsAndActionNames) {
        return ActionsList.convertImpl(keys, (boolean)false, (boolean)prohibitSeparatorsAndActionNames).all;
    }

    private static Pair convertImpl(List<FileObject> keys, boolean ignoreFolders, boolean prohibitSeparatorsAndActionNames) {
        ArrayList<Object> all = new ArrayList<Object>();
        ArrayList<Action> actions = new ArrayList<Action>();
        for (FileObject item : keys) {
            Object lastOne;
            Object toAdd;
            block14 : {
                DataObject dob;
                block13 : {
                    try {
                        dob = DataObject.find((FileObject)item);
                        if (dob != null || !prohibitSeparatorsAndActionNames || !LOG.isLoggable(Level.INFO)) break block13;
                        LOG.info("ActionsList: DataObject is null for item=" + (Object)item + "\n");
                    }
                    catch (DataObjectNotFoundException dnfe) {
                        if (prohibitSeparatorsAndActionNames) {
                            if (!LOG.isLoggable(Level.INFO)) continue;
                            LOG.info("ActionsList: DataObject not found for item=" + (Object)item + "\n");
                            continue;
                        }
                        if (!LOG.isLoggable(Level.FINE)) continue;
                        LOG.log(Level.FINE, "DataObject not found for action fileObject=" + (Object)item);
                        continue;
                    }
                }
                toAdd = null;
                InstanceCookie ic = (InstanceCookie)dob.getLookup().lookup(InstanceCookie.class);
                if (prohibitSeparatorsAndActionNames && ic == null) {
                    if (!LOG.isLoggable(Level.INFO)) continue;
                    LOG.info("ActionsList: InstanceCookie not found for item=" + (Object)item + "\n");
                    continue;
                }
                if (ic != null) {
                    try {
                        if (!ActionsList.isSeparator(ic) && (toAdd = ic.instanceCreate()) == null && prohibitSeparatorsAndActionNames && LOG.isLoggable(Level.INFO)) {
                            LOG.info("ActionsList: InstanceCookie.instanceCreate() null for item=" + (Object)item + "\n");
                        }
                        break block14;
                    }
                    catch (Exception e) {
                        LOG.log(Level.INFO, "Can't instantiate object", e);
                        continue;
                    }
                }
                toAdd = dob instanceof DataFolder ? dob : dob.getName();
            }
            if (all.size() > 0 && (Utilities.compareObjects(lastOne = all.get(all.size() - 1), (Object)toAdd) || ActionsList.isSeparator(lastOne) && ActionsList.isSeparator(toAdd))) continue;
            if (toAdd instanceof Action) {
                Action action = (Action)toAdd;
                actions.add(action);
                AcceleratorBinding.setAccelerator((Action)action, (FileObject)item);
            } else if (ActionsList.isSeparator(toAdd)) {
                if (prohibitSeparatorsAndActionNames && LOG.isLoggable(Level.INFO)) {
                    LOG.info("ActionsList: Separator for item=" + (Object)item + "\n");
                }
                actions.add(null);
            }
            all.add(toAdd);
        }
        Pair p = new Pair();
        p.all = all.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(all);
        p.actions = actions.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(actions);
        return p;
    }

    private static boolean isSeparator(Object o) {
        return o == null || o instanceof JSeparator;
    }

    private static boolean isSeparator(InstanceCookie o) throws Exception {
        return o instanceof InstanceCookie.Of && ((InstanceCookie.Of)o).instanceOf(JSeparator.class) || JSeparator.class.isAssignableFrom(o.instanceClass());
    }

    private static class Pair {
        List<Object> all;
        List<Action> actions;

        private Pair() {
        }
    }

}

