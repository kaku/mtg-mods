/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.impl.actions.clipboardhistory;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistory;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistoryElement;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.DocumentationScrollPane;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ListCompletionView;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ScrollCompletionPane;
import org.openide.util.Utilities;

public class CompletionLayoutPopup {
    static final double COMPL_COVERAGE = 0.4;
    static final double MAX_COMPL_COVERAGE = 0.9;
    private static final String POPUP_NAME = "clipboardHistoryPopup";
    private ScrollCompletionPane layout;
    private Popup popup;
    private Rectangle popupBounds;
    private JComponent contentComponent;
    private Reference<JTextComponent> compRef;
    private int anchorOffset;
    private Rectangle anchorOffsetBounds;
    private boolean displayAboveCaret;
    private boolean preferDisplayAboveCaret;
    private boolean showRetainedPreferredSize;
    private ChSelectionListener chSelectionListener;
    private final ChAWTEventListener chAWTEventListener;
    private ChKeyListener chKeyListener;
    private MouseListener mouseListener;

    public CompletionLayoutPopup() {
        this.chSelectionListener = new ChSelectionListener();
        this.chAWTEventListener = new ChAWTEventListener();
        this.chKeyListener = new ChKeyListener();
        this.mouseListener = new ChMouseAdapter();
    }

    public final boolean isVisible() {
        return this.popup != null;
    }

    public final boolean isActive() {
        return this.contentComponent != null;
    }

    public void hide() {
        if (this.isVisible()) {
            this.popup.hide();
            this.popup = null;
            this.popupBounds = null;
            this.contentComponent = null;
            this.anchorOffset = -1;
            this.setEditorComponent(null);
        }
    }

    public void setEditorComponent(JTextComponent comp) {
        boolean change;
        JTextComponent thisComp = this.getEditorComponent();
        boolean bl = change = thisComp != comp;
        if (change) {
            if (thisComp != null) {
                Toolkit.getDefaultToolkit().removeAWTEventListener(this.chAWTEventListener);
            }
            this.compRef = new WeakReference<JTextComponent>(comp);
            if (comp != null) {
                Toolkit.getDefaultToolkit().addAWTEventListener(this.chAWTEventListener, 16);
            }
        }
    }

    public JTextComponent getEditorComponent() {
        return this.compRef == null ? null : this.compRef.get();
    }

    ChKeyListener getChKeyListener() {
        return this.chKeyListener;
    }

    public void show(JTextComponent editorComponent, int anchorOffset) {
        if (editorComponent == null || ClipboardHistory.getInstance().getData().isEmpty()) {
            return;
        }
        if (!this.isVisible()) {
            ScrollCompletionPane scrollCompletionPane = new ScrollCompletionPane(editorComponent, ClipboardHistory.getInstance(), null, this.chSelectionListener, this.mouseListener);
            scrollCompletionPane.setName("clipboardHistoryPopup");
            this.setContentComponent(scrollCompletionPane);
            this.setLayout(scrollCompletionPane);
            this.setEditorComponent(editorComponent);
        }
        if (!this.isVisible()) {
            this.setAnchorOffset(anchorOffset);
            this.updateLayout(this);
            this.chSelectionListener.valueChanged(null);
        }
    }

    public final boolean isDisplayAboveCaret() {
        return this.displayAboveCaret;
    }

    public final Rectangle getPopupBounds() {
        return this.popupBounds;
    }

    final void setLayout(ScrollCompletionPane layout) {
        assert (layout != null);
        this.layout = layout;
    }

    final void setPreferDisplayAboveCaret(boolean preferDisplayAboveCaret) {
        this.preferDisplayAboveCaret = preferDisplayAboveCaret;
    }

    final void setContentComponent(JComponent contentComponent) {
        assert (contentComponent != null);
        this.contentComponent = contentComponent;
    }

    final void setAnchorOffset(int anchorOffset) {
        this.anchorOffset = anchorOffset;
        this.anchorOffsetBounds = null;
    }

    final int getAnchorOffset() {
        JTextComponent editorComponent;
        int offset = this.anchorOffset;
        if (offset == -1 && (editorComponent = this.getEditorComponent()) != null) {
            offset = editorComponent.getSelectionStart();
        }
        return offset;
    }

    final JComponent getContentComponent() {
        return this.contentComponent;
    }

    final Dimension getPreferredSize() {
        JComponent comp = this.getContentComponent();
        if (comp == null) {
            return new Dimension(0, 0);
        }
        int screenWidth = Utilities.getUsableScreenBounds().width;
        Dimension maxSize = new Dimension((int)((double)screenWidth * 0.9), comp.getMaximumSize().height);
        this.setMaxSize(comp, maxSize);
        return comp.getPreferredSize();
    }

    private void setMaxSize(JComponent comp, Dimension maxSize) {
        if (comp instanceof JPanel) {
            comp.getComponent(0).setMaximumSize(maxSize);
        } else {
            comp.setMaximumSize(maxSize);
        }
    }

    final void resetPreferredSize() {
        JComponent comp = this.getContentComponent();
        if (comp == null) {
            return;
        }
        comp.setPreferredSize(null);
    }

    final boolean isShowRetainedPreferredSize() {
        return this.showRetainedPreferredSize;
    }

    final ScrollCompletionPane getLayout() {
        return this.layout;
    }

    protected int getAnchorHorizontalShift() {
        return 0;
    }

    final Rectangle getAnchorOffsetBounds() {
        JTextComponent editorComponent = this.getEditorComponent();
        if (editorComponent == null) {
            return new Rectangle();
        }
        if (this.anchorOffsetBounds == null) {
            int currentAnchorOffset = this.getAnchorOffset();
            try {
                this.anchorOffsetBounds = editorComponent.modelToView(currentAnchorOffset);
                if (this.anchorOffsetBounds != null) {
                    this.anchorOffsetBounds.x -= this.getAnchorHorizontalShift();
                } else {
                    this.anchorOffsetBounds = new Rectangle();
                }
            }
            catch (BadLocationException e) {
                this.anchorOffsetBounds = new Rectangle();
            }
            Point anchorOffsetPoint = this.anchorOffsetBounds.getLocation();
            SwingUtilities.convertPointToScreen(anchorOffsetPoint, editorComponent);
            this.anchorOffsetBounds.setLocation(anchorOffsetPoint);
        }
        return this.anchorOffsetBounds;
    }

    final Popup getPopup() {
        return this.popup;
    }

    private Rectangle findPopupBounds(Rectangle occupiedBounds, boolean aboveOccupiedBounds) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        Dimension prefSize = this.getPreferredSize();
        Rectangle curPopupBounds = new Rectangle();
        curPopupBounds.x = Math.min(occupiedBounds.x, screen.x + screen.width - prefSize.width);
        curPopupBounds.x = Math.max(curPopupBounds.x, screen.x);
        curPopupBounds.width = Math.min(prefSize.width, screen.width);
        if (aboveOccupiedBounds) {
            curPopupBounds.height = Math.min(prefSize.height, occupiedBounds.y - screen.y - 1);
            curPopupBounds.y = occupiedBounds.y - 1 - curPopupBounds.height;
        } else {
            curPopupBounds.y = occupiedBounds.y + occupiedBounds.height + 1;
            curPopupBounds.height = Math.min(prefSize.height, screen.y + screen.height - curPopupBounds.y);
        }
        return curPopupBounds;
    }

    private void show(Rectangle popupBounds, boolean displayAboveCaret) {
        if (this.popup != null) {
            this.popup.hide();
            this.popup = null;
        }
        Dimension origPrefSize = this.getPreferredSize();
        Dimension newPrefSize = popupBounds.getSize();
        JComponent contComp = this.getContentComponent();
        if (contComp == null) {
            return;
        }
        contComp.setPreferredSize(newPrefSize);
        this.showRetainedPreferredSize = newPrefSize.equals(origPrefSize);
        PopupFactory factory = PopupFactory.getSharedInstance();
        JTextComponent owner = this.getEditorComponent();
        if (owner != null && owner.getClientProperty("ForceHeavyweightCompletionPopup") != null) {
            owner = null;
        }
        if (displayAboveCaret && Utilities.isMac()) {
            popupBounds.y -= 10;
        }
        this.popup = factory.getPopup(owner, contComp, popupBounds.x, popupBounds.y);
        this.popup.show();
        this.popupBounds = popupBounds;
        this.displayAboveCaret = displayAboveCaret;
    }

    void showAlongAnchorBounds() {
        this.showAlongOccupiedBounds(this.getAnchorOffsetBounds());
    }

    void showAlongAnchorBounds(boolean aboveCaret) {
        this.showAlongOccupiedBounds(this.getAnchorOffsetBounds(), aboveCaret);
    }

    void showAlongOccupiedBounds(Rectangle occupiedBounds) {
        boolean aboveCaret = this.isEnoughSpace(occupiedBounds, this.preferDisplayAboveCaret) ? this.preferDisplayAboveCaret : this.isMoreSpaceAbove(occupiedBounds);
        Rectangle bounds = this.findPopupBounds(occupiedBounds, aboveCaret);
        this.show(bounds, aboveCaret);
    }

    void showAlongOrNextOccupiedBounds(Rectangle occupiedBounds, Rectangle unionBounds) {
        if (occupiedBounds != null) {
            Rectangle screen = Utilities.getUsableScreenBounds();
            Dimension prefSize = this.getPreferredSize();
            Rectangle bounds = new Rectangle();
            boolean aboveCaret = this.isEnoughSpace(occupiedBounds, this.preferDisplayAboveCaret) ? this.preferDisplayAboveCaret : false;
            boolean left = false;
            boolean right = false;
            if (occupiedBounds.x + occupiedBounds.width + prefSize.width < screen.width && occupiedBounds.y + prefSize.height < screen.height) {
                bounds.x = occupiedBounds.x + occupiedBounds.width + 1;
                right = true;
            }
            if (!right && occupiedBounds.x - prefSize.width > 0 && occupiedBounds.y + prefSize.height < screen.height) {
                bounds.x = occupiedBounds.x - prefSize.width - 1;
                left = true;
            }
            if (right || left) {
                bounds.width = prefSize.width;
                bounds.height = Math.min(prefSize.height, screen.height);
                bounds.y = aboveCaret ? occupiedBounds.y + occupiedBounds.height - prefSize.height : occupiedBounds.y;
                this.show(bounds, aboveCaret);
                return;
            }
        }
        this.showAlongOccupiedBounds(unionBounds);
    }

    void showAlongOccupiedBounds(Rectangle occupiedBounds, boolean aboveCaret) {
        Rectangle bounds = this.findPopupBounds(occupiedBounds, aboveCaret);
        this.show(bounds, aboveCaret);
    }

    boolean isMoreSpaceAbove(Rectangle bounds) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        int above = bounds.y - screen.y;
        int below = screen.y + screen.height - (bounds.y + bounds.height);
        return above > below;
    }

    boolean isEnoughSpace(Rectangle occupiedBounds) {
        return this.isEnoughSpace(occupiedBounds, this.preferDisplayAboveCaret);
    }

    boolean isEnoughSpace(Rectangle occupiedBounds, boolean aboveOccupiedBounds) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        int freeHeight = aboveOccupiedBounds ? occupiedBounds.y - screen.y : screen.y + screen.height - (occupiedBounds.y + occupiedBounds.height);
        Dimension prefSize = this.getPreferredSize();
        return prefSize.height < freeHeight;
    }

    boolean isEnoughSpace(boolean aboveCaret) {
        return this.isEnoughSpace(this.getAnchorOffsetBounds(), aboveCaret);
    }

    public boolean isOverlapped(Rectangle bounds) {
        return this.isVisible() ? this.popupBounds.intersects(bounds) : false;
    }

    public boolean isOverlapped(CompletionLayoutPopup popup) {
        return popup.isVisible() ? this.isOverlapped(popup.getPopupBounds()) : false;
    }

    public Rectangle unionBounds(Rectangle bounds) {
        return this.isVisible() ? bounds.union(this.getPopupBounds()) : bounds;
    }

    void updateLayout(CompletionLayoutPopup popup) {
        popup.resetPreferredSize();
        if (!(popup instanceof FullTextPopup)) {
            popup.showAlongAnchorBounds();
            if (FullTextPopup.getInstance().isVisible() && (FullTextPopup.getInstance().isOverlapped(popup) || FullTextPopup.getInstance().getAnchorOffset() != CompletionPopup.getInstance().getAnchorOffset() || !FullTextPopup.getInstance().isShowRetainedPreferredSize())) {
                this.updateLayout(FullTextPopup.getInstance());
            }
        } else {
            if (CompletionPopup.getInstance().isVisible()) {
                popup.setAnchorOffset(CompletionPopup.getInstance().getAnchorOffset());
            }
            Rectangle occupiedBounds = popup.getAnchorOffsetBounds();
            occupiedBounds = CompletionPopup.getInstance().unionBounds(occupiedBounds);
            popup.showAlongOccupiedBounds(occupiedBounds);
        }
    }

    private void pasteContent() throws HeadlessException {
        Transferable transferable = this.layout.getSelectedValue().getTransferable();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        if (transferable != null) {
            clipboard.setContents(transferable, this.layout.getSelectedValue());
        } else {
            StringSelection contents = new StringSelection(this.layout.getSelectedValue().getFullText());
            clipboard.setContents(contents, this.layout.getSelectedValue());
        }
        this.getEditorComponent().paste();
    }

    public static class CompletionPopup
    extends CompletionLayoutPopup {
        private static CompletionPopup instance;

        @Override
        public void setEditorComponent(JTextComponent comp) {
            boolean change;
            JTextComponent thisComp = this.getEditorComponent();
            boolean bl = change = thisComp != comp;
            if (thisComp != null && change) {
                thisComp.removeKeyListener(this.getChKeyListener());
            }
            super.setEditorComponent(comp);
            if (comp != null && change) {
                comp.addKeyListener(this.getChKeyListener());
            }
        }

        public static synchronized CompletionPopup getInstance() {
            if (instance == null) {
                instance = new CompletionPopup();
            }
            return instance;
        }

        @Override
        public void hide() {
            super.hide();
            FullTextPopup.getInstance().hide();
        }
    }

    static class FullTextPopup
    extends CompletionLayoutPopup {
        private static FullTextPopup instance;

        FullTextPopup() {
        }

        public static synchronized FullTextPopup getInstance() {
            if (instance == null) {
                instance = new FullTextPopup();
            }
            return instance;
        }

        public void showFullTextPopup(int anchorOffset, String fullText) {
            JTextComponent editorComponent = this.getEditorComponent();
            if (editorComponent == null) {
                return;
            }
            if (!this.isVisible()) {
                this.setContentComponent(new DocumentationScrollPane(editorComponent));
            }
            DocumentationScrollPane doc = (DocumentationScrollPane)this.getContentComponent();
            doc.setData(fullText);
            if (!this.isVisible()) {
                this.setAnchorOffset(anchorOffset);
                this.updateLayout(this);
            }
        }
    }

    private class ChKeyListener
    implements KeyListener {
        private ChKeyListener() {
        }

        @Override
        public void keyPressed(KeyEvent evt) {
            if (CompletionLayoutPopup.this.layout == null) {
                return;
            }
            boolean popupShowing = CompletionLayoutPopup.this.isVisible();
            if (evt.getKeyCode() == 10) {
                CompletionLayoutPopup.this.pasteContent();
                evt.consume();
                CompletionLayoutPopup.this.hide();
            } else if (evt.getKeyCode() == 27) {
                CompletionLayoutPopup.this.hide();
                evt.consume();
            } else if (popupShowing) {
                if (evt.getKeyChar() != '\n') {
                    Action action;
                    Object actionMapKey = CompletionLayoutPopup.this.layout.getInputMap().get(KeyStroke.getKeyStrokeForEvent(evt));
                    if (actionMapKey != null && (action = CompletionLayoutPopup.this.layout.getActionMap().get(actionMapKey)) != null) {
                        action.actionPerformed(new ActionEvent(this, 0, null));
                        evt.consume();
                    }
                } else {
                    evt.consume();
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void keyTyped(KeyEvent evt) {
            if (CompletionLayoutPopup.this.layout == null) {
                return;
            }
            if (CompletionLayoutPopup.this.isVisible() && evt.getKeyChar() >= '1' && evt.getKeyChar() <= 48 + CompletionLayoutPopup.this.layout.getView().getModel().getSize()) {
                CompletionLayoutPopup.this.layout.getView().setSelectedIndex(evt.getKeyChar() - 49);
                CompletionLayoutPopup.this.pasteContent();
                evt.consume();
                CompletionLayoutPopup.this.hide();
            }
        }
    }

    private class ChSelectionListener
    implements ListSelectionListener {
        private ChSelectionListener() {
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (CompletionLayoutPopup.this.layout != null && CompletionLayoutPopup.this.layout.getSelectedValue() != null && CompletionLayoutPopup.this.layout.getSelectedValue().isShorten()) {
                FullTextPopup.getInstance().setEditorComponent(CompletionLayoutPopup.this.getEditorComponent());
                FullTextPopup.getInstance().showFullTextPopup(-1, CompletionLayoutPopup.this.layout.getSelectedValue().getFullText());
            } else {
                FullTextPopup.getInstance().hide();
            }
        }
    }

    private class ChMouseAdapter
    extends MouseAdapter {
        private ChMouseAdapter() {
        }

        @Override
        public void mouseClicked(MouseEvent evt) {
            JTextComponent c = CompletionLayoutPopup.this.getEditorComponent();
            if (SwingUtilities.isLeftMouseButton(evt) && c != null && evt.getClickCount() == 2) {
                CompletionLayoutPopup.this.pasteContent();
                CompletionLayoutPopup.this.hide();
            }
        }
    }

    private class ChAWTEventListener
    implements AWTEventListener {
        private ChAWTEventListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mv;
            if (aWTEvent instanceof MouseEvent && (mv = (MouseEvent)aWTEvent).getID() == 500 && mv.getClickCount() > 0) {
                if (!(aWTEvent.getSource() instanceof Component)) {
                    CompletionLayoutPopup.this.hide();
                    return;
                }
                Component comp = (Component)aWTEvent.getSource();
                Container par1 = SwingUtilities.getAncestorNamed("clipboardHistoryPopup", comp);
                if (par1 == null) {
                    CompletionLayoutPopup.this.hide();
                }
            }
        }
    }

}

