/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.impl.actions.clipboardhistory;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.JTextComponent;

public final class DocumentationScrollPane
extends JScrollPane {
    private JEditorPane view;
    private Dimension documentationPreferredSize = new Dimension(500, 300);

    public DocumentationScrollPane(JTextComponent editorComponent) {
        this.setPreferredSize(null);
        Color bgColor = new JEditorPane().getBackground();
        bgColor = new Color(Math.max(bgColor.getRed() - 8, 0), Math.max(bgColor.getGreen() - 8, 0), bgColor.getBlue());
        this.view = new JEditorPane();
        this.view.setEditable(false);
        this.view.setFocusable(true);
        this.view.setBackground(bgColor);
        this.view.setMargin(new Insets(0, 3, 3, 3));
        this.setViewportView(this.view);
        this.setFocusable(true);
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        if (preferredSize == null) {
            preferredSize = this.documentationPreferredSize;
        }
        super.setPreferredSize(preferredSize);
    }

    public void setData(String doc) {
        this.view.setText(doc);
        this.view.setCaretPosition(0);
    }
}

