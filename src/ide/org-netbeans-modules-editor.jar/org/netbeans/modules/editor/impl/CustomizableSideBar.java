/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.SideBarFactory
 *  org.netbeans.editor.WeakEventListenerList
 *  org.netbeans.spi.editor.SideBarFactory
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.modules.editor.impl;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.editor.WeakEventListenerList;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.impl.SideBarFactoriesProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class CustomizableSideBar {
    public static final String PROP_SELECT_SIDEBAR_LOCATIONS = "nbeditorui.selectSidebarLocations";
    private static final Logger LOG = Logger.getLogger(CustomizableSideBar.class.getName());
    private static final Map<JTextComponent, Map<SideBarPosition, Reference<JPanel>>> CACHE = new WeakHashMap<JTextComponent, Map<SideBarPosition, Reference<JPanel>>>(5);
    private static final Map<String, WeakEventListenerList> LISTENERS = new HashMap<String, WeakEventListenerList>(5);
    private static final Map<MimePath, Lookup.Result<SideBarFactoriesProvider>> LR = new WeakHashMap<MimePath, Lookup.Result<SideBarFactoriesProvider>>(5);
    private static final Map<Lookup.Result<SideBarFactoriesProvider>, LookupListener> LL = new WeakHashMap<Lookup.Result<SideBarFactoriesProvider>, LookupListener>(5);

    private CustomizableSideBar() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void addChangeListener(String mimeType, ChangeListener l) {
        Map<String, WeakEventListenerList> map = LISTENERS;
        synchronized (map) {
            WeakEventListenerList listenerList = LISTENERS.get(mimeType);
            if (listenerList == null) {
                listenerList = new WeakEventListenerList();
                LISTENERS.put(mimeType, listenerList);
            }
            listenerList.add(ChangeListener.class, (EventListener)l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void removeChangeListener(String mimeType, ChangeListener l) {
        Map<String, WeakEventListenerList> map = LISTENERS;
        synchronized (map) {
            WeakEventListenerList listenerList = LISTENERS.get(mimeType);
            if (listenerList != null) {
                listenerList.remove(ChangeListener.class, (EventListener)l);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void fireChange(String mimeType) {
        ChangeListener[] listeners = null;
        Map<String, WeakEventListenerList> map = LISTENERS;
        synchronized (map) {
            WeakEventListenerList listenerList = LISTENERS.get(mimeType);
            if (listenerList != null) {
                listeners = (ChangeListener[])listenerList.getListeners(ChangeListener.class);
            }
        }
        if (listeners != null && listeners.length > 0) {
            ChangeEvent evt = new ChangeEvent(CustomizableSideBar.class);
            for (ChangeListener l : listeners) {
                l.stateChanged(evt);
            }
        }
    }

    public static Map<SideBarPosition, JComponent> getSideBars(JTextComponent target) {
        assert (SwingUtilities.isEventDispatchThread());
        return CustomizableSideBar.getSideBarsInternal(target);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void resetSideBars(JTextComponent target) {
        Map<JTextComponent, Map<SideBarPosition, Reference<JPanel>>> map = CACHE;
        synchronized (map) {
            CACHE.put(target, null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Map<SideBarPosition, JComponent> getSideBarsInternal(JTextComponent target) {
        Map panelsMap;
        Map<JTextComponent, Map<SideBarPosition, Reference<JPanel>>> map = CACHE;
        synchronized (map) {
            panelsMap = CACHE.get(target);
            if (panelsMap != null) {
                HashMap<SideBarPosition, JComponent> map2 = new HashMap<SideBarPosition, JComponent>();
                for (SideBarPosition pos : panelsMap.keySet()) {
                    Reference ref = (Reference)panelsMap.get(pos);
                    if (ref == null) continue;
                    JPanel panel = (JPanel)ref.get();
                    if (panel == null) break;
                    map2.put(pos, panel);
                }
                if (map2.size() == panelsMap.size()) {
                    return map2;
                }
            }
        }
        Map<SideBarPosition, List<JComponent>> sideBarsMap = CustomizableSideBar.createSideBarsMap(target);
        panelsMap = CACHE;
        synchronized (panelsMap) {
            HashMap<SideBarPosition, WeakReference<JPanel>> panelsMap2 = new HashMap<SideBarPosition, WeakReference<JPanel>>();
            HashMap<SideBarPosition, JComponent> map3 = new HashMap<SideBarPosition, JComponent>();
            for (SideBarPosition pos : sideBarsMap.keySet()) {
                List<JComponent> sideBars = sideBarsMap.get(pos);
                JPanel panel = new JPanel();
                panel.setLayout(new BoxLayout(panel, pos.getAxis()));
                for (JComponent c : sideBars) {
                    panel.add(c);
                }
                panelsMap2.put(pos, new WeakReference<JPanel>(panel));
                map3.put(pos, panel);
            }
            CACHE.put(target, panelsMap2);
            return map3;
        }
    }

    private static Map<SideBarPosition, List<JComponent>> createSideBarsMap(JTextComponent target) {
        String mimeType = NbEditorUtilities.getMimeType(target);
        Map<SideBarPosition, List> factoriesMap = CustomizableSideBar.getFactoriesMap(mimeType);
        HashMap<SideBarPosition, List<JComponent>> sideBarsMap = new HashMap<SideBarPosition, List<JComponent>>(factoriesMap.size());
        List<String> locations = null;
        String constraint = (String)target.getClientProperty("nbeditorui.selectSidebarLocations");
        if (constraint != null) {
            locations = Arrays.asList(constraint.split(","));
        }
        boolean errorStripeOnly = Boolean.TRUE.equals(target.getClientProperty("errorStripeOnly"));
        for (SideBarPosition pos : factoriesMap.keySet()) {
            if (locations != null && !locations.contains(pos.getPositionName())) continue;
            List factoriesList = factoriesMap.get(pos);
            List<JComponent> sideBars = sideBarsMap.get(pos);
            if (sideBars == null) {
                sideBars = new ArrayList<JComponent>();
                sideBarsMap.put(pos, sideBars);
            }
            for (Object f : factoriesList) {
                JComponent sideBar;
                if (f instanceof SideBarFactory) {
                    sideBar = ((SideBarFactory)f).createSideBar(target);
                } else if (f instanceof org.netbeans.spi.editor.SideBarFactory) {
                    sideBar = ((org.netbeans.spi.editor.SideBarFactory)f).createSideBar(target);
                } else {
                    LOG.fine("Unexpected sidebar instance: " + f);
                    continue;
                }
                if (sideBar == null) {
                    LOG.fine("Ignoring null side bar created by the factory: " + f);
                    continue;
                }
                if (errorStripeOnly && !"errorStripe".equals(sideBar.getName())) {
                    LOG.fine("Error stripe sidebar only. Ignoring '" + sideBar.getName() + "' side bar created by the factory: " + f);
                    continue;
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Created sidebar " + sideBar + "; IHC=" + System.identityHashCode(sideBar) + '\n');
                }
                sideBars.add(sideBar);
            }
        }
        return sideBarsMap;
    }

    public static Map<SideBarPosition, List> getFactoriesMap(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        Lookup.Result lR = LR.get((Object)mimePath);
        if (lR == null) {
            lR = MimeLookup.getLookup((MimePath)mimePath).lookupResult(SideBarFactoriesProvider.class);
            LookupListener listener = LL.get((Object)lR);
            if (listener == null) {
                listener = new MyLookupListener(mimeType);
                LL.put((Lookup.Result)lR, listener);
            }
            lR.addLookupListener(listener);
            LR.put(mimePath, (Lookup.Result)lR);
        }
        Collection providers = lR.allInstances();
        assert (providers.size() == 1);
        SideBarFactoriesProvider provider = (SideBarFactoriesProvider)providers.iterator().next();
        return provider.getFactories();
    }

    private static class MyLookupListener
    implements LookupListener {
        private String mimeType;

        public MyLookupListener(String mimeType) {
            this.mimeType = mimeType;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            Map map = CACHE;
            synchronized (map) {
                ArrayList<JTextComponent> toRemove = new ArrayList<JTextComponent>();
                for (JTextComponent jtc : CACHE.keySet()) {
                    String mimeType = NbEditorUtilities.getMimeType(jtc);
                    if (!mimeType.equals(this.mimeType)) continue;
                    toRemove.add(jtc);
                }
                CACHE.keySet().removeAll(toRemove);
            }
            CustomizableSideBar.fireChange(this.mimeType);
        }
    }

    public static final class SideBarPosition {
        public static final int WEST = 1;
        public static final int NORTH = 2;
        public static final int SOUTH = 3;
        public static final int EAST = 4;
        public static final String WEST_NAME = "West";
        public static final String NORTH_NAME = "North";
        public static final String SOUTH_NAME = "South";
        public static final String EAST_NAME = "East";
        private int position;
        private boolean scrollable;
        private static String[] borderLayoutConstants = new String[]{"", "West", "North", "South", "East"};
        private static int[] axisConstants = new int[]{-1, 0, 1, 1, 0};

        SideBarPosition(FileObject fo) {
            Object position = fo.getAttribute("location");
            if (position == null) {
                position = fo.getAttribute("position");
            }
            if (position != null && position instanceof String) {
                String positionName = (String)position;
                if ("West".equals(positionName)) {
                    this.position = 1;
                } else if ("North".equals(positionName)) {
                    this.position = 2;
                } else if ("South".equals(positionName)) {
                    this.position = 3;
                } else if ("East".equals(positionName)) {
                    this.position = 4;
                } else {
                    if (Logger.getLogger("global").isLoggable(Level.FINE)) {
                        Logger.getLogger("global").log(Level.FINE, "Unsupported position: " + positionName);
                    }
                    this.position = 1;
                }
            } else {
                this.position = 1;
            }
            Object scrollable = fo.getAttribute("scrollable");
            this.scrollable = scrollable != null && scrollable instanceof Boolean ? (Boolean)scrollable : true;
            if (this.scrollable && (this.position == 3 || this.position == 4) && Logger.getLogger("global").isLoggable(Level.FINE)) {
                Logger.getLogger("global").log(Level.FINE, "Unsupported combination: scrollable == true, position=" + this.getBorderLayoutPosition());
            }
        }

        public String getPositionName() {
            switch (this.position) {
                case 4: {
                    return "East";
                }
                case 3: {
                    return "South";
                }
                case 1: {
                    return "West";
                }
                case 2: {
                    return "North";
                }
            }
            throw new IllegalArgumentException();
        }

        public int hashCode() {
            return this.scrollable ? this.position : - this.position;
        }

        public boolean equals(Object o) {
            if (o instanceof SideBarPosition) {
                SideBarPosition p = (SideBarPosition)o;
                if (this.scrollable != p.scrollable) {
                    return false;
                }
                if (this.position != p.position) {
                    return false;
                }
                return true;
            }
            return false;
        }

        public int getPosition() {
            return this.position;
        }

        public String getBorderLayoutPosition() {
            return borderLayoutConstants[this.getPosition()];
        }

        private int getAxis() {
            return axisConstants[this.getPosition()];
        }

        public boolean isScrollable() {
            return this.scrollable;
        }

        public String toString() {
            return "[SideBarPosition: scrollable=" + this.scrollable + ", position=" + this.position + "]";
        }
    }

}

