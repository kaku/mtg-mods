/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.impl;

import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.impl.ActionsList;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.filesystems.FileObject;

@MimeLocation(subfolderName="GlyphGutterActions", instanceProviderClass=GlyphGutterActionsProvider.class)
public final class GlyphGutterActionsProvider
extends ActionsList
implements InstanceProvider<GlyphGutterActionsProvider> {
    public static final String GLYPH_GUTTER_ACTIONS_FOLDER_NAME = "GlyphGutterActions";

    public static List getGlyphGutterActions(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        GlyphGutterActionsProvider provider = (GlyphGutterActionsProvider)MimeLookup.getLookup((MimePath)mimePath).lookup(GlyphGutterActionsProvider.class);
        return provider == null ? Collections.emptyList() : provider.getActionsOnly();
    }

    public GlyphGutterActionsProvider() {
        super(null, false, false);
    }

    private GlyphGutterActionsProvider(List<FileObject> keys) {
        super(keys, false, false);
    }

    public GlyphGutterActionsProvider createInstance(List<FileObject> fileObjectList) {
        return new GlyphGutterActionsProvider(fileObjectList);
    }
}

