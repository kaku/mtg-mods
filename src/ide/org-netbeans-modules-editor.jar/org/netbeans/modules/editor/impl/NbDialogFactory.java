/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.DialogFactory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 */
package org.netbeans.modules.editor.impl;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import org.netbeans.modules.editor.lib2.DialogFactory;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;

public class NbDialogFactory
implements DialogFactory {
    private static HashMap helpIDs;
    private static final String HELP_ID_MacroSavePanel = "editing.macros.recording";
    private static final String HELP_ID_JavaFastImportPanel = "editing.fastimport";
    private static final String HELP_ID_ScrollCompletionPane = "editing.codecompletion";

    public NbDialogFactory() {
        if (helpIDs == null) {
            helpIDs = new HashMap(7);
            helpIDs.put("org.netbeans.editor.MacroSavePanel", "editing.macros.recording");
            helpIDs.put("org.netbeans.editor.ext.ScrollCompletionPane", "editing.codecompletion");
            helpIDs.put("org.netbeans.editor.ext.java.JavaFastImportPanel", "editing.fastimport");
        }
    }

    public Dialog createDialog(String title, JPanel panel, boolean modal, JButton[] buttons, boolean sideButtons, int defaultIndex, int cancelIndex, ActionListener listener) {
        String helpID = (String)helpIDs.get(panel.getClass().getName());
        Dialog d = DialogDisplayer.getDefault().createDialog(new DialogDescriptor((Object)panel, title, modal, (Object[])buttons, (Object)(defaultIndex == -1 ? buttons[0] : buttons[defaultIndex]), sideButtons ? 1 : 0, helpID != null ? new HelpCtx(helpID) : null, listener));
        if (cancelIndex >= 0 && d instanceof JDialog) {
            final JButton cancelButton = buttons[cancelIndex];
            ((JDialog)d).getRootPane().registerKeyboardAction(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent evt) {
                    cancelButton.doClick(10);
                }
            }, KeyStroke.getKeyStroke(27, 0, true), 0);
            ((JDialog)d).getRootPane().setFocusable(false);
            d.addWindowListener(new WindowAdapter(){

                @Override
                public void windowClosing(WindowEvent evt) {
                    cancelButton.doClick(10);
                }
            });
        }
        return d;
    }

}

