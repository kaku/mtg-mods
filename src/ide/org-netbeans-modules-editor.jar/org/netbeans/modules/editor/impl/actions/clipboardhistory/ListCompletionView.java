/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.StringEscapeUtils
 *  org.openide.awt.HtmlRenderer
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.editor.impl.actions.clipboardhistory;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import org.netbeans.lib.editor.util.StringEscapeUtils;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistory;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistoryElement;
import org.openide.awt.HtmlRenderer;
import org.openide.util.ImageUtilities;

public class ListCompletionView
extends JList {
    public static final int COMPLETION_ITEM_HEIGHT = 16;
    private static final int DARKER_COLOR_COMPONENT = 5;
    private static final Icon icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/hints/resources/suggestion.gif", (boolean)false);
    private final int fixedItemHeight;
    private Font font;
    private final RenderComponent renderComponent;
    private Graphics cellPreferredSizeGraphics;
    private static final int BEFORE_ICON_GAP = 1;
    private static final int AFTER_ICON_GAP = 4;
    private static final int AFTER_TEXT_GAP = 5;

    public ListCompletionView(MouseListener mouseListener) {
        this.addMouseListener(mouseListener);
        this.setSelectionMode(0);
        this.font = this.getFont();
        if (this.font.getSize() < 15) {
            this.font = this.font.deriveFont(this.font.getSize2D() + 1.0f);
        }
        this.setFont(this.font);
        this.fixedItemHeight = Math.max(16, this.getFontMetrics(this.getFont()).getHeight());
        this.setFixedCellHeight(this.fixedItemHeight);
        this.renderComponent = new RenderComponent();
        this.setCellRenderer(new ListCellRenderer(){
            private final ListCellRenderer defaultRenderer;

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof ClipboardHistoryElement) {
                    Color bgColor;
                    Color fgColor;
                    ClipboardHistoryElement val = (ClipboardHistoryElement)value;
                    ListCompletionView.this.renderComponent.setClipboardHistoryValue(val);
                    ListCompletionView.this.renderComponent.setSelected(isSelected);
                    if (isSelected) {
                        bgColor = list.getSelectionBackground();
                        fgColor = list.getSelectionForeground();
                    } else {
                        bgColor = list.getBackground();
                        if (index % 2 == 0) {
                            bgColor = new Color(Math.abs(bgColor.getRed() - 5), Math.abs(bgColor.getGreen() - 5), Math.abs(bgColor.getBlue() - 5));
                        }
                        fgColor = list.getForeground();
                    }
                    if (ListCompletionView.this.renderComponent.getBackground() != bgColor) {
                        ListCompletionView.this.renderComponent.setBackground(bgColor);
                    }
                    if (ListCompletionView.this.renderComponent.getForeground() != fgColor) {
                        ListCompletionView.this.renderComponent.setForeground(fgColor);
                    }
                    return ListCompletionView.this.renderComponent;
                }
                return this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
        this.setBorder(BorderFactory.createEmptyBorder());
    }

    public void setResult(ClipboardHistory data) {
        if (data != null) {
            Model model = new Model(data);
            this.setModel(model);
            if (model.getSize() > 0) {
                this.setSelectedIndex(0);
            }
        }
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return this.getPreferredSize();
    }

    public void up() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx = (this.getSelectedIndex() - 1 + size) % size;
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
            this.repaint();
        }
    }

    public void down() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx = (this.getSelectedIndex() + 1) % size;
            if (idx == size) {
                idx = 0;
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
            this.validate();
        }
    }

    public void pageUp() {
        if (this.getModel().getSize() > 0) {
            int pageSize = Math.max(this.getLastVisibleIndex() - this.getFirstVisibleIndex(), 0);
            int ind = Math.max(this.getSelectedIndex() - pageSize, 0);
            this.setSelectedIndex(ind);
            this.ensureIndexIsVisible(ind);
        }
    }

    public void pageDown() {
        int lastInd = this.getModel().getSize() - 1;
        if (lastInd >= 0) {
            int pageSize = Math.max(this.getLastVisibleIndex() - this.getFirstVisibleIndex(), 0);
            int ind = Math.min(this.getSelectedIndex() + pageSize, lastInd);
            this.setSelectedIndex(ind);
            this.ensureIndexIsVisible(ind);
        }
    }

    public void begin() {
        if (this.getModel().getSize() > 0) {
            this.setSelectedIndex(0);
            this.ensureIndexIsVisible(0);
        }
    }

    public void end() {
        int lastInd = this.getModel().getSize() - 1;
        if (lastInd >= 0) {
            this.setSelectedIndex(lastInd);
            this.ensureIndexIsVisible(lastInd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        Map renderingHints;
        Object value = Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
        Map map = renderingHints = value instanceof Map ? (Map)value : null;
        if (renderingHints != null && g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D)g;
            RenderingHints oldHints = g2d.getRenderingHints();
            g2d.setRenderingHints(renderingHints);
            try {
                super.paint(g2d);
            }
            finally {
                g2d.setRenderingHints(oldHints);
            }
        }
        super.paint(g);
    }

    private static int getPreferredWidth(ClipboardHistoryElement f, Graphics g, Font defaultFont) {
        int width = 1 + icon.getIconWidth() + 4 + 5;
        return width += (int)HtmlRenderer.renderHTML((String)StringEscapeUtils.escapeHtml((String)f.getShortenText()), (Graphics)g, (int)0, (int)0, (int)Integer.MAX_VALUE, (int)0, (Font)defaultFont, (Color)Color.black, (int)0, (boolean)false);
    }

    private static void renderHtml(ClipboardHistoryElement f, Graphics g, Font defaultFont, Color defaultColor, int width, int height, boolean selected) {
        int textEnd = width - 4 - 5;
        FontMetrics fm = g.getFontMetrics(defaultFont);
        int textY = (height - fm.getHeight()) / 2 + fm.getHeight() - fm.getDescent();
        HtmlRenderer.renderHTML((String)(f.getNumber() + " " + StringEscapeUtils.escapeHtml((String)f.getShortenText())), (Graphics)g, (int)1, (int)textY, (int)textEnd, (int)textY, (Font)defaultFont, (Color)defaultColor, (int)1, (boolean)true);
    }

    private final class RenderComponent
    extends JComponent {
        private ClipboardHistoryElement value;
        private boolean selected;

        private RenderComponent() {
        }

        void setClipboardHistoryValue(ClipboardHistoryElement value) {
            this.value = value;
        }

        void setSelected(boolean selected) {
            this.selected = selected;
        }

        @Override
        public void paintComponent(Graphics g) {
            int itemRenderWidth = ((JViewport)ListCompletionView.this.getParent()).getWidth();
            Color bgColor = this.getBackground();
            Color fgColor = this.getForeground();
            int height = this.getHeight();
            g.setColor(bgColor);
            g.fillRect(0, 0, itemRenderWidth, height);
            g.setColor(fgColor);
            ListCompletionView.renderHtml(this.value, g, ListCompletionView.this.getFont(), this.getForeground(), itemRenderWidth, this.getHeight(), this.selected);
        }

        @Override
        public Dimension getPreferredSize() {
            if (ListCompletionView.this.cellPreferredSizeGraphics == null) {
                ListCompletionView.this.cellPreferredSizeGraphics = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(1, 1).getGraphics();
                assert (ListCompletionView.this.cellPreferredSizeGraphics != null);
            }
            return new Dimension(ListCompletionView.getPreferredWidth(this.value, ListCompletionView.this.cellPreferredSizeGraphics, ListCompletionView.this.getFont()), ListCompletionView.this.fixedItemHeight);
        }
    }

    static class Model
    extends AbstractListModel {
        private ClipboardHistory data;
        static final long serialVersionUID = 3292276783870598274L;

        public Model(ClipboardHistory data) {
            this.data = data;
        }

        @Override
        public synchronized int getSize() {
            return this.data.getData().size();
        }

        @Override
        public synchronized Object getElementAt(int index) {
            return this.data.getData().get(index);
        }
    }

}

