/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 */
package org.netbeans.modules.editor.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.impl.ActionsList;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;

@MimeLocation(subfolderName="Toolbars/Default", instanceProviderClass=ToolbarActionsProvider.class)
public final class ToolbarActionsProvider
extends ActionsList
implements InstanceProvider<ToolbarActionsProvider> {
    private static final Logger LOG = Logger.getLogger(ToolbarActionsProvider.class.getName());
    static final String TOOLBAR_ACTIONS_FOLDER_NAME = "Toolbars/Default";
    private static final String TEXT_BASE_PATH = "Editors/text/base/";

    public static List getToolbarItems(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        ActionsList provider = mimeType.equals("text/base") ? (ActionsList)MimeLookup.getLookup((MimePath)mimePath).lookup(LegacyToolbarActionsProvider.class) : (ActionsList)MimeLookup.getLookup((MimePath)mimePath).lookup(ToolbarActionsProvider.class);
        return provider == null ? Collections.emptyList() : provider.getAllInstances();
    }

    public ToolbarActionsProvider() {
        super(null, false, false);
    }

    private ToolbarActionsProvider(List<FileObject> keys) {
        super(keys, true, false);
    }

    public ToolbarActionsProvider createInstance(List<FileObject> fileObjectList) {
        return new ToolbarActionsProvider(fileObjectList);
    }

    @MimeLocation(subfolderName="Toolbars/Default", instanceProviderClass=LegacyToolbarActionsProvider.class)
    public static final class LegacyToolbarActionsProvider
    extends ActionsList
    implements InstanceProvider<LegacyToolbarActionsProvider> {
        public LegacyToolbarActionsProvider() {
            this(null);
        }

        private LegacyToolbarActionsProvider(List<FileObject> keys) {
            super(keys, false, false);
        }

        public LegacyToolbarActionsProvider createInstance(List<FileObject> fileObjectList) {
            ArrayList<FileObject> textBaseFilesList = new ArrayList<FileObject>();
            for (FileObject o : fileObjectList) {
                FileObject fileObject = null;
                if (o instanceof DataObject) {
                    fileObject = ((DataObject)o).getPrimaryFile();
                } else {
                    if (!(o instanceof FileObject)) continue;
                    fileObject = o;
                }
                String fullPath = fileObject.getPath();
                int idx = fullPath.lastIndexOf("Toolbars/Default");
                assert (idx != -1);
                String path = fullPath.substring(0, idx);
                if (!"Editors/text/base/".equals(path)) continue;
                textBaseFilesList.add(fileObject);
                if (!LOG.isLoggable(Level.WARNING)) continue;
                LOG.warning("The 'text/base' mime type is deprecated, please move your file to the root. Offending file: " + fullPath);
            }
            return new LegacyToolbarActionsProvider(textBaseFilesList);
        }
    }

}

