/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.impl;

import java.util.Collections;
import java.util.List;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.impl.ActionsList;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.filesystems.FileObject;

@MimeLocation(subfolderName="Popup", instanceProviderClass=PopupMenuActionsProvider.class)
public final class PopupMenuActionsProvider
extends ActionsList
implements InstanceProvider<PopupMenuActionsProvider> {
    static final String POPUP_MENU_ACTIONS_FOLDER_NAME = "Popup";

    public static List getPopupMenuItems(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        PopupMenuActionsProvider provider = (PopupMenuActionsProvider)MimeLookup.getLookup((MimePath)mimePath).lookup(PopupMenuActionsProvider.class);
        return provider == null ? Collections.emptyList() : provider.getAllInstances();
    }

    public PopupMenuActionsProvider() {
        super(null, false, false);
    }

    private PopupMenuActionsProvider(List<FileObject> keys) {
        super(keys, false, false);
    }

    public PopupMenuActionsProvider createInstance(List<FileObject> fileObjectList) {
        return new PopupMenuActionsProvider(fileObjectList);
    }
}

