/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MarkBlock
 *  org.netbeans.editor.MarkBlockChain
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.impl.highlighting;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MarkBlock;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.util.WeakListeners;

public final class GuardedBlocksHighlighting
extends AbstractHighlightsContainer
implements PropertyChangeListener,
DocumentListener {
    private static final Logger LOG = Logger.getLogger(GuardedBlocksHighlighting.class.getName());
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.oldlibbridge.GuardedBlocksHighlighting";
    static final AttributeSet EXTENDS_EOL_ATTR_SET = AttributesUtilities.createImmutable((Object[])new Object[]{"org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", Boolean.TRUE});
    private final Document document;
    private final MarkBlockChain guardedBlocksChain;
    private final MimePath mimePath;
    private AttributeSet attribs = null;

    public GuardedBlocksHighlighting(Document document, String mimeType) {
        this.document = document;
        if (document instanceof GuardedDocument) {
            this.guardedBlocksChain = ((GuardedDocument)document).getGuardedBlockChain();
            this.guardedBlocksChain.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.guardedBlocksChain));
            this.document.addDocumentListener((DocumentListener)WeakListeners.create(DocumentListener.class, (EventListener)this, (Object)this.document));
        } else {
            this.guardedBlocksChain = null;
        }
        this.mimePath = MimePath.parse((String)mimeType);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        GuardedBlocksHighlighting guardedBlocksHighlighting = this;
        synchronized (guardedBlocksHighlighting) {
            if (this.guardedBlocksChain != null) {
                return new HSImpl(this.guardedBlocksChain.getChain(), startOffset, endOffset);
            }
            return HighlightsSequence.EMPTY;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == null || evt.getPropertyName().equals("MarkBlockChain.PROP_BLOCKS_CHANGED")) {
            int end;
            int start = evt.getOldValue() == null ? -1 : (Integer)evt.getOldValue();
            int n = end = evt.getNewValue() == null ? -1 : (Integer)evt.getNewValue();
            if (start < 0 || start >= this.document.getLength()) {
                start = 0;
            }
            if (end <= start || end > this.document.getLength()) {
                end = Integer.MAX_VALUE;
            }
            this.fireHighlightsChange(start, end);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        int changeEnd;
        int changeStart = e.getOffset();
        if (this.isAffectedByChange(changeStart, changeEnd = e.getOffset() + e.getLength())) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        int changeEnd;
        int changeStart = e.getOffset();
        if (this.isAffectedByChange(changeStart, changeEnd = e.getOffset() + e.getLength())) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    private boolean isAffectedByChange(int startOffset, int endOffset) {
        for (MarkBlock b = this.guardedBlocksChain.getChain(); b != null; b = b.getNext()) {
            int c = b.compare(startOffset, endOffset);
            if ((c & 1) != 0 || (c & 2) != 0) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("<" + startOffset + ", " + endOffset + "> collides with guarded block <" + b.getStartOffset() + ", " + b.getEndOffset() + ">");
                }
                return true;
            }
            if (!LOG.isLoggable(Level.FINE)) continue;
            LOG.fine("<" + startOffset + ", " + endOffset + "> is outside of guarded block <" + b.getStartOffset() + ", " + b.getEndOffset() + ">");
        }
        return false;
    }

    private final class HSImpl
    implements HighlightsSequence {
        private final int startOffset;
        private final int endOffset;
        private boolean init;
        private MarkBlock block;

        public HSImpl(MarkBlock block, int startOffset, int endOffset) {
            this.init = false;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.block = block;
        }

        public boolean moveNext() {
            if (!this.init) {
                this.init = true;
                while (null != this.block && this.block.getEndOffset() <= this.startOffset) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Skipping block: " + (Object)this.block + ", blockStart = " + this.block.getStartOffset() + ", blockEnd = " + this.block.getEndOffset() + ", startOffset = " + this.startOffset + ", endOffset = " + this.endOffset);
                    }
                    this.block = this.block.getNext();
                }
            } else if (this.block != null) {
                this.block = this.block.getNext();
            }
            if (this.block != null && this.block.getStartOffset() > this.endOffset) {
                this.block = null;
            }
            if (LOG.isLoggable(Level.FINE)) {
                if (this.block != null) {
                    LOG.fine("Next block: " + (Object)this.block + ", blockStart = " + this.block.getStartOffset() + ", blockEnd = " + this.block.getEndOffset() + ", startOffset = " + this.startOffset + ", endOffset = " + this.endOffset);
                } else {
                    LOG.fine("Next block: null");
                }
            }
            return this.block != null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getStartOffset() {
            GuardedBlocksHighlighting guardedBlocksHighlighting = GuardedBlocksHighlighting.this;
            synchronized (guardedBlocksHighlighting) {
                if (!this.init) {
                    throw new NoSuchElementException("Call moveNext() first.");
                }
                if (this.block == null) {
                    throw new NoSuchElementException();
                }
                return Math.max(this.block.getStartOffset(), this.startOffset);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getEndOffset() {
            GuardedBlocksHighlighting guardedBlocksHighlighting = GuardedBlocksHighlighting.this;
            synchronized (guardedBlocksHighlighting) {
                if (!this.init) {
                    throw new NoSuchElementException("Call moveNext() first.");
                }
                if (this.block == null) {
                    throw new NoSuchElementException();
                }
                return Math.min(this.block.getEndOffset(), this.endOffset);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public AttributeSet getAttributes() {
            GuardedBlocksHighlighting guardedBlocksHighlighting = GuardedBlocksHighlighting.this;
            synchronized (guardedBlocksHighlighting) {
                if (!this.init) {
                    throw new NoSuchElementException("Call moveNext() first.");
                }
                if (this.block == null) {
                    throw new NoSuchElementException();
                }
                if (GuardedBlocksHighlighting.this.attribs == null) {
                    FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)GuardedBlocksHighlighting.this.mimePath).lookup(FontColorSettings.class);
                    if (fcs != null) {
                        GuardedBlocksHighlighting.this.attribs = fcs.getFontColors("guarded");
                    }
                    if (GuardedBlocksHighlighting.this.attribs == null) {
                        GuardedBlocksHighlighting.this.attribs = SimpleAttributeSet.EMPTY;
                    } else {
                        GuardedBlocksHighlighting.this.attribs = AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[]{GuardedBlocksHighlighting.this.attribs, GuardedBlocksHighlighting.EXTENDS_EOL_ATTR_SET});
                    }
                }
                return GuardedBlocksHighlighting.this.attribs;
            }
        }
    }

}

