/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MarkBlock
 *  org.netbeans.lib.editor.util.GapList
 *  org.netbeans.lib.editor.util.swing.BlockCompare
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.editor.lib2.document.DocumentInternalUtils
 *  org.netbeans.modules.editor.lib2.document.ModRootElement
 *  org.netbeans.spi.editor.document.OnSaveTask
 *  org.netbeans.spi.editor.document.OnSaveTask$Context
 *  org.netbeans.spi.editor.document.OnSaveTask$Factory
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MarkBlock;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.lib.editor.util.swing.BlockCompare;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.lib2.document.DocumentInternalUtils;
import org.netbeans.modules.editor.lib2.document.ModRootElement;
import org.netbeans.spi.editor.document.OnSaveTask;
import org.openide.util.Exceptions;

public class ReformatBeforeSaveTask
implements OnSaveTask {
    private static final Logger LOG = Logger.getLogger(ReformatBeforeSaveTask.class.getName());
    private final Document doc;
    private Reformat reformat;
    private boolean modifiedLinesOnly;
    private List<PositionRegion> guardedBlocks;
    private int guardedBlockIndex;
    private Position guardedBlockStartPos;
    private Position guardedBlockEndPos;
    private AtomicBoolean canceled = new AtomicBoolean();

    ReformatBeforeSaveTask(Document doc) {
        this.doc = doc;
    }

    public void performTask() {
        if (this.reformat != null) {
            this.reformat();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runLocked(Runnable run) {
        String policy;
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((Document)this.doc)).lookup(Preferences.class);
        if (prefs.getBoolean("on-save-use-global-settings", Boolean.TRUE)) {
            prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        }
        if (!"never".equals(policy = prefs.get("on-save-reformat", "never"))) {
            this.modifiedLinesOnly = "modified-lines".equals(policy);
            this.reformat = Reformat.get((Document)this.doc);
            this.reformat.lock();
            try {
                run.run();
            }
            finally {
                this.reformat.unlock();
            }
        }
        run.run();
    }

    public boolean cancel() {
        this.canceled.set(true);
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive exception aggregation
     */
    void reformat() {
        block46 : {
            modRootElement = ModRootElement.get((Document)this.doc);
            if (modRootElement != null) {
                origEnabled = modRootElement.isEnabled();
                modRootElement.setEnabled(false);
                try {
                    this.guardedBlocks = new GapList();
                    if (this.doc instanceof GuardedDocument) {
                        for (block = ((GuardedDocument)this.doc).getGuardedBlockChain().getChain(); block != null; block = block.getNext()) {
                            try {
                                this.guardedBlocks.add(new PositionRegion(this.doc, block.getStartOffset(), block.getEndOffset()));
                                continue;
                            }
                            catch (BadLocationException ex) {
                                Exceptions.printStackTrace((Throwable)ex);
                            }
                        }
                    }
                    this.guardedBlockIndex = 0;
                    this.fetchNextGuardedBlock();
                    modRootOrDocElement = this.modifiedLinesOnly != false ? modRootElement : DocumentInternalUtils.customElement((Document)this.doc, (int)0, (int)this.doc.getLength());
                    modElementCount = modRootOrDocElement.getElementCount();
                    formatBlocks = new LinkedList<PositionRegion>();
                    rootElement = this.doc.getDefaultRootElement();
                    for (i = 0; i < modElementCount; ++i) {
                        if (this.canceled.get()) {
                            return;
                        }
                        modElement = modRootOrDocElement.getElement(i);
                        startOffset = modElement.getStartOffset();
                        modElementEndOffset = modElement.getEndOffset();
                        lineNumber = rootElement.getElementIndex(startOffset);
                        if (lineNumber >= 0) {
                            lineElement = rootElement.getElement(lineNumber);
                            startOffset = lineElement.getStartOffset();
                            if (lineElement.getEndOffset() >= modElementEndOffset) {
                                modElementEndOffset = lineElement.getEndOffset();
                            } else {
                                lineNumber = rootElement.getElementIndex(modElementEndOffset);
                                lineElement = rootElement.getElement(lineNumber);
                                modElementEndOffset = lineElement.getEndOffset();
                            }
                        }
                        endOffset = modElementEndOffset;
                        do {
                            if (this.guardedBlockStartPos != null) {
                                guardedStartOffset = this.guardedBlockStartPos.getOffset();
                                blockCompare = BlockCompare.get((int)startOffset, (int)endOffset, (int)guardedStartOffset, (int)(guardedEndOffset = this.guardedBlockEndPos.getOffset()));
                                if (blockCompare.before()) {
                                    add = true;
                                    modElementFinished = true;
                                } else if (blockCompare.after()) {
                                    this.fetchNextGuardedBlock();
                                    add = false;
                                    modElementFinished = false;
                                } else if (blockCompare.equal()) {
                                    this.fetchNextGuardedBlock();
                                    add = false;
                                    modElementFinished = true;
                                } else if (blockCompare.overlapStart()) {
                                    endOffset = guardedStartOffset;
                                    add = true;
                                    modElementFinished = true;
                                } else if (blockCompare.overlapEnd()) {
                                    endOffset = guardedEndOffset;
                                    this.fetchNextGuardedBlock();
                                    add = false;
                                    modElementFinished = false;
                                } else if (blockCompare.contains()) {
                                    if (blockCompare.equalStart()) {
                                        startOffset = guardedEndOffset;
                                        add = true;
                                        modElementFinished = true;
                                    } else {
                                        endOffset = guardedStartOffset;
                                        add = true;
                                        modElementFinished = false;
                                    }
                                } else if (blockCompare.inside()) {
                                    add = false;
                                    modElementFinished = true;
                                } else {
                                    ReformatBeforeSaveTask.LOG.info("Unexpected blockCompare=" + (Object)blockCompare);
                                    add = false;
                                    modElementFinished = true;
                                }
                            } else {
                                add = true;
                                modElementFinished = true;
                            }
                            if (add) {
                                try {
                                    if (startOffset != endOffset) {
                                        last = (PositionRegion)formatBlocks.peek();
                                        if (last != null && startOffset <= last.getEndOffset()) {
                                            formatBlocks.remove();
                                            if (ReformatBeforeSaveTask.LOG.isLoggable(Level.FINE)) {
                                                ReformatBeforeSaveTask.LOG.fine("Reformat-at-save: remove block=" + (Object)last);
                                            }
                                            startOffset = last.getStartOffset();
                                        }
                                        block = new PositionRegion(this.doc, startOffset, endOffset);
                                        if (ReformatBeforeSaveTask.LOG.isLoggable(Level.FINE)) {
                                            ReformatBeforeSaveTask.LOG.fine("Reformat-at-save: add block=" + (Object)block);
                                        }
                                        formatBlocks.addFirst(block);
                                    }
                                }
                                catch (BadLocationException ex) {
                                    Exceptions.printStackTrace((Throwable)ex);
                                }
                            }
                            startOffset = endOffset;
                            endOffset = modElementEndOffset;
                        } while (!modElementFinished);
                    }
                    for (PositionRegion block : formatBlocks) {
                        if (!this.canceled.get()) ** break block45
                        return;
                    }
                    break block46;
                    {
                        
                        try {
                            this.reformat.reformat(block.getStartOffset(), block.getEndOffset());
                            continue;
                        }
                        catch (Exception ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                            break block46;
                            break;
                        }
                    }
                }
                finally {
                    modRootElement.setEnabled(origEnabled);
                }
            }
        }
    }

    private void fetchNextGuardedBlock() {
        if (this.guardedBlockIndex < this.guardedBlocks.size()) {
            PositionRegion guardedBlock = this.guardedBlocks.get(this.guardedBlockIndex++);
            this.guardedBlockStartPos = guardedBlock.getStartPosition();
            this.guardedBlockEndPos = guardedBlock.getEndPosition();
        } else {
            this.guardedBlockStartPos = null;
            this.guardedBlockEndPos = null;
        }
    }

    public static final class FactoryImpl
    implements OnSaveTask.Factory {
        public OnSaveTask createTask(OnSaveTask.Context context) {
            return new ReformatBeforeSaveTask(context.getDocument());
        }
    }

}

