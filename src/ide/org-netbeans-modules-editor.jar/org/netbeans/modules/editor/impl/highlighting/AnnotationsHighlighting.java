/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.Annotations
 *  org.netbeans.editor.Annotations$AnnotationsListener
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeEvent
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeListener
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.impl.highlighting;

import java.awt.Color;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class AnnotationsHighlighting
extends AbstractHighlightsContainer
implements Annotations.AnnotationsListener,
HighlightsChangeListener {
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.oldlibbridge.AnnotationsHighlighting";
    private static final Logger LOG = Logger.getLogger(AnnotationsHighlighting.class.getName());
    private static final RequestProcessor RP = new RequestProcessor("org.netbeans.modules.editor.oldlibbridge.AnnotationsHighlighting", 1, false, false);
    private final BaseDocument document;
    private final Annotations annotations;
    private final OffsetsBag bag;
    private final Map<AnnotationType, AttributeSet> cache = new WeakHashMap<AnnotationType, AttributeSet>();
    private R refreshAllLines = null;
    private RequestProcessor.Task refreshAllLinesTask = null;

    public AnnotationsHighlighting(Document document) {
        if (document instanceof BaseDocument) {
            this.document = (BaseDocument)document;
            this.annotations = this.document.getAnnotations();
            this.annotations.addAnnotationsListener((Annotations.AnnotationsListener)WeakListeners.create(Annotations.AnnotationsListener.class, (EventListener)((Object)this), (Object)this.annotations));
            this.bag = new OffsetsBag(document, true);
            this.bag.addHighlightsChangeListener((HighlightsChangeListener)this);
            this.changedAll();
        } else {
            this.document = null;
            this.annotations = null;
            this.bag = null;
        }
    }

    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        if (this.bag != null) {
            return this.bag.getHighlights(startOffset, endOffset);
        }
        return HighlightsSequence.EMPTY;
    }

    public void highlightChanged(HighlightsChangeEvent event) {
        this.fireHighlightsChange(event.getStartOffset(), event.getEndOffset());
    }

    public void changedLine(int line) {
        this.scheduleRefresh(AnnotationsHighlighting.isTyping() ? 343 : 43);
    }

    public void changedAll() {
        this.scheduleRefresh(AnnotationsHighlighting.isTyping() ? 343 : 43);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void scheduleRefresh(int delay) {
        LOG.log(Level.FINE, "scheduleRefresh: delay={0}", delay);
        AnnotationsHighlighting annotationsHighlighting = this;
        synchronized (annotationsHighlighting) {
            if (this.refreshAllLines == null) {
                this.refreshAllLines = new R();
                this.refreshAllLinesTask = RP.post((Runnable)this.refreshAllLines, delay);
            } else {
                this.refreshAllLines.cancelled.set(true);
                this.refreshAllLinesTask.schedule(delay);
            }
        }
    }

    private static boolean isTyping() {
        JTextComponent jtc = EditorRegistry.focusedComponent();
        return jtc != null ? DocumentUtilities.isWriteLocked((Document)jtc.getDocument()) : false;
    }

    private final class R
    implements Runnable {
        public final AtomicBoolean cancelled;

        private R() {
            this.cancelled = new AtomicBoolean(false);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            this.cancelled.set(false);
            this.refreshAllLines();
            AnnotationsHighlighting annotationsHighlighting = AnnotationsHighlighting.this;
            synchronized (annotationsHighlighting) {
                AnnotationsHighlighting.this.refreshAllLines = null;
                AnnotationsHighlighting.this.refreshAllLinesTask = null;
            }
        }

        private void refreshAllLines() {
            final OffsetsBag b = new OffsetsBag((Document)AnnotationsHighlighting.this.document, true);
            try {
                int line = AnnotationsHighlighting.this.annotations.getNextLineWithAnnotation(0);
                while (line != -1) {
                    if (this.cancelled.get()) {
                        return;
                    }
                    this.refreshLine(line, b, -1, -1);
                    line = AnnotationsHighlighting.this.annotations.getNextLineWithAnnotation(line + 1);
                }
            }
            catch (Exception e) {
                return;
            }
            if (!this.cancelled.get()) {
                AnnotationsHighlighting.this.document.render(new Runnable(){

                    @Override
                    public void run() {
                        AnnotationsHighlighting.this.bag.setHighlights(b);
                    }
                });
            }
        }

        private void refreshLine(int line, OffsetsBag b, int lineStartOffset, int lineEndOffset) {
            AnnotationDesc active;
            LOG.log(Level.FINE, "Refreshing line {0}", line);
            AnnotationDesc[] allPassive = AnnotationsHighlighting.this.annotations.getPassiveAnnotationsForLine(line);
            if (allPassive != null) {
                for (AnnotationDesc passive : allPassive) {
                    AttributeSet attribs = this.getAttributes(passive.getAnnotationTypeInstance());
                    if (!passive.isVisible()) continue;
                    if (passive.isWholeLine()) {
                        if (lineStartOffset == -1 || lineEndOffset == -1) {
                            Element lineElement = AnnotationsHighlighting.this.document.getDefaultRootElement().getElement(line);
                            lineStartOffset = lineElement.getStartOffset();
                            lineEndOffset = lineElement.getEndOffset();
                        }
                        b.addHighlight(lineStartOffset, lineEndOffset, attribs);
                        continue;
                    }
                    b.addHighlight(passive.getOffset(), passive.getOffset() + passive.getLength(), attribs);
                }
            }
            if ((active = AnnotationsHighlighting.this.annotations.getActiveAnnotation(line)) != null && active.isVisible()) {
                AttributeSet attribs = this.getAttributes(active.getAnnotationTypeInstance());
                if (active.isWholeLine()) {
                    if (lineStartOffset == -1 || lineEndOffset == -1) {
                        Element lineElement = AnnotationsHighlighting.this.document.getDefaultRootElement().getElement(line);
                        lineStartOffset = lineElement.getStartOffset();
                        lineEndOffset = lineElement.getEndOffset();
                    }
                    b.addHighlight(lineStartOffset, lineEndOffset, attribs);
                } else {
                    b.addHighlight(active.getOffset(), active.getOffset() + active.getLength(), attribs);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private AttributeSet getAttributes(AnnotationType annotationType) {
            Map map = AnnotationsHighlighting.this.cache;
            synchronized (map) {
                AttributeSet attrs = (AttributeSet)AnnotationsHighlighting.this.cache.get((Object)annotationType);
                if (attrs == null) {
                    LinkedList<Object> l = new LinkedList<Object>();
                    if (!annotationType.isInheritForegroundColor()) {
                        l.add(StyleConstants.Foreground);
                        l.add(annotationType.getForegroundColor());
                    }
                    if (annotationType.isUseHighlightColor()) {
                        l.add(StyleConstants.Background);
                        l.add(annotationType.getHighlight());
                    }
                    if (annotationType.isUseWaveUnderlineColor()) {
                        l.add(EditorStyleConstants.WaveUnderlineColor);
                        l.add(annotationType.getWaveUnderlineColor());
                    }
                    if (annotationType.isWholeLine()) {
                        l.add("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EMPTY_LINE");
                        l.add(annotationType.isWholeLine());
                        l.add("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL");
                        l.add(annotationType.isWholeLine());
                    }
                    attrs = AttributesUtilities.createImmutable((Object[])l.toArray());
                    AnnotationsHighlighting.this.cache.put(annotationType, attrs);
                }
                return attrs;
            }
        }

    }

}

