/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.lib.KitsTracker
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.lib.KitsTracker;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

public final class KitsTrackerImpl
extends KitsTracker {
    private static final Logger LOG = Logger.getLogger(KitsTrackerImpl.class.getName());
    private static final Set<String> ALREADY_LOGGED = Collections.synchronizedSet(new HashSet(10));
    private static final Logger TIMER = Logger.getLogger("TIMER");
    private final Map<String, FileObject> mimeType2kitClass = new HashMap<String, FileObject>();
    private final Map<Class, List<String>> kitClass2mimeTypes = new HashMap<Class, List<String>>();
    private final Set<String> knownMimeTypes = new HashSet<String>();
    private List<FileObject> eventSources = null;
    private boolean needsReloading = true;
    private boolean mimeType2kitClassLoaded = false;
    private static final Set<String> WELL_KNOWN_PARENTS = new HashSet<String>(Arrays.asList("java.lang.Object", "javax.swing.text.EditorKit", "javax.swing.text.DefaultEditorKit", "org.netbeans.editor.BaseKit", "org.netbeans.editor.ext.ExtKit", "org.netbeans.modules.editor.NbEditorKit", "org.netbeans.modules.xml.text.syntax.UniKit"));
    private static final Set<String> DYNAMIC_LANGUAGES = new HashSet<String>(Arrays.asList("org.netbeans.modules.languages.dataobject.LanguagesEditorKit", "org.netbeans.modules.gsf.GsfEditorKitFactory$GsfEditorKit"));
    private final ThreadLocal<Stack<String>> contexts = new ThreadLocal();
    private final FileChangeListener fcl;
    private static final ThreadLocal<Boolean> inReload = new ThreadLocal<Boolean>(){

        @Override
        protected Boolean initialValue() {
            return false;
        }
    };

    public KitsTrackerImpl() {
        this.fcl = new FileChangeAdapter(){

            public void fileFolderCreated(FileEvent fe) {
                KitsTrackerImpl.this.invalidateCache();
            }

            public void fileDeleted(FileEvent fe) {
                if (fe.getFile().isFolder()) {
                    KitsTrackerImpl.this.invalidateCache();
                }
            }

            public void fileRenamed(FileRenameEvent fe) {
                if (fe.getFile().isFolder()) {
                    KitsTrackerImpl.this.invalidateCache();
                }
            }
        };
    }

    public List<String> getMimeTypesForKitClass(Class kitClass) {
        if (kitClass != null) {
            if (WELL_KNOWN_PARENTS.contains(kitClass.getName())) {
                return Collections.emptyList();
            }
            return (List)this.updateAndGet(kitClass);
        }
        return Collections.singletonList("");
    }

    public String findMimeType(Class kitClass) {
        if (kitClass != null) {
            if (WELL_KNOWN_PARENTS.contains(kitClass.getName())) {
                return null;
            }
            String contextMimeType = null;
            Stack<String> context = this.contexts.get();
            if (context != null && !context.empty()) {
                contextMimeType = context.peek();
            }
            if (contextMimeType == null || contextMimeType.length() == 0) {
                List<String> mimeTypes = this.getMimeTypesForKitClass(kitClass);
                if (mimeTypes.size() == 0) {
                    if (LOG.isLoggable(Level.WARNING) && !DYNAMIC_LANGUAGES.contains(kitClass.getName())) {
                        KitsTrackerImpl.logOnce(Level.WARNING, "No mime type uses editor kit implementation class: " + kitClass);
                    }
                    return null;
                }
                if (mimeTypes.size() == 1) {
                    return mimeTypes.get(0);
                }
                if (LOG.isLoggable(Level.WARNING)) {
                    KitsTrackerImpl.logOnce(Level.WARNING, "Ambiguous mime types for editor kit implementation class: " + kitClass + "; mime types: " + mimeTypes);
                }
                return null;
            }
            return contextMimeType;
        }
        return "";
    }

    public Class<?> findKitClass(String mimeType) {
        if (mimeType.length() > 0) {
            return (Class)this.updateAndGet(mimeType);
        }
        return null;
    }

    public Set<String> getMimeTypes() {
        return (Set)this.updateAndGet(null);
    }

    public String setContextMimeType(String mimeType) {
        String previousMimeType = null;
        Stack context = this.contexts.get();
        if (context == null) {
            context = new Stack();
            this.contexts.set(context);
        }
        if (mimeType != null) {
            assert (MimePath.validate((CharSequence)mimeType));
            if (!context.empty()) {
                previousMimeType = (String)context.peek();
            }
            context.push(mimeType);
        } else if (!context.empty()) {
            previousMimeType = (String)context.pop();
        }
        return previousMimeType;
    }

    public String toString() {
        return "KitsTrackerImpl";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void reload(Set<String> set, List<FileObject> eventSources) {
        assert (!inReload.get().booleanValue());
        inReload.set(true);
        try {
            KitsTrackerImpl._reload(set, eventSources);
        }
        finally {
            inReload.set(false);
        }
    }

    private static void _reload(Set<String> set, List<FileObject> eventSources) {
        FileObject fo = FileUtil.getConfigFile((String)"Editors");
        if (fo != null) {
            FileObject[] types = fo.getChildren();
            for (int i = 0; i < types.length; ++i) {
                if (!KitsTrackerImpl.isValidType(types[i])) continue;
                FileObject[] subTypes = types[i].getChildren();
                for (int j = 0; j < subTypes.length; ++j) {
                    if (!KitsTrackerImpl.isValidSubtype(subTypes[j])) continue;
                    String mimeType = types[i].getNameExt() + "/" + subTypes[j].getNameExt();
                    set.add(mimeType);
                }
                eventSources.add(types[i]);
            }
            eventSources.add(fo);
        }
    }

    private static FileObject findKitRegistration(FileObject folder) {
        HashSet<FileObject> filesWithInstanceOfAttribute = new HashSet<FileObject>();
        HashSet<FileObject> otherFiles = new HashSet<FileObject>();
        for (FileObject f : folder.getChildren()) {
            if (f.getAttribute("instanceOf") != null) {
                filesWithInstanceOfAttribute.add(f);
                continue;
            }
            otherFiles.add(f);
        }
        for (FileObject f2 : filesWithInstanceOfAttribute) {
            if (!KitsTrackerImpl.isInstanceOf(f2, EditorKit.class, false)) continue;
            return f2;
        }
        for (FileObject f3 : otherFiles) {
            if (!KitsTrackerImpl.isInstanceOf(f3, EditorKit.class, false)) continue;
            return f3;
        }
        return null;
    }

    private static boolean isInstanceOf(FileObject f, Class clazz, boolean exactMatch) {
        try {
            DataObject d = DataObject.find((FileObject)f);
            InstanceCookie ic = (InstanceCookie)d.getLookup().lookup(InstanceCookie.class);
            if (ic != null) {
                if (ic instanceof InstanceCookie.Of) {
                    if (!exactMatch) {
                        return ((InstanceCookie.Of)ic).instanceOf(clazz);
                    }
                    if (!((InstanceCookie.Of)ic).instanceOf(clazz)) {
                        return false;
                    }
                    return ic.instanceClass() == clazz;
                }
                Class instanceClass = ic.instanceClass();
                if (!exactMatch ? clazz.isAssignableFrom(instanceClass) : clazz == instanceClass) {
                    return true;
                }
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return false;
    }

    private static Class<?> instanceClass(FileObject f) {
        try {
            DataObject d = DataObject.find((FileObject)f);
            InstanceCookie ic = (InstanceCookie)d.getLookup().lookup(InstanceCookie.class);
            if (ic != null) {
                return ic.instanceClass();
            }
        }
        catch (Exception e) {
            // empty catch block
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void invalidateCache() {
        Map<String, FileObject> map = this.mimeType2kitClass;
        synchronized (map) {
            this.needsReloading = true;
        }
        this.firePropertyChange(null, (Object)null, (Object)null);
    }

    private static boolean isValidType(FileObject typeFile) {
        if (!typeFile.isFolder()) {
            return false;
        }
        String typeName = typeFile.getNameExt();
        return MimePath.validate((CharSequence)typeName, (CharSequence)null);
    }

    private static boolean isValidSubtype(FileObject subtypeFile) {
        if (!subtypeFile.isFolder()) {
            return false;
        }
        String typeName = subtypeFile.getNameExt();
        return MimePath.validate((CharSequence)null, (CharSequence)typeName);
    }

    private static void logOnce(Level level, String msg) {
        if (!ALREADY_LOGGED.contains(msg)) {
            LOG.log(level, msg);
            ALREADY_LOGGED.add(msg);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Object updateAndGet(Object kitClassOrMimeType) {
        boolean reload;
        HashSet<String> reloadedSet = new HashSet<String>();
        ArrayList<FileObject> newEventSources = new ArrayList<FileObject>();
        Map<String, FileObject> map = this.mimeType2kitClass;
        synchronized (map) {
            reload = this.needsReloading;
        }
        if (reload) {
            KitsTrackerImpl.reload(reloadedSet, newEventSources);
        }
        map = this.mimeType2kitClass;
        synchronized (map) {
            if (reload) {
                if (this.eventSources != null) {
                    for (FileObject fo : this.eventSources) {
                        fo.removeFileChangeListener(this.fcl);
                    }
                }
                this.knownMimeTypes.clear();
                this.knownMimeTypes.addAll(reloadedSet);
                this.eventSources = newEventSources;
                for (FileObject fo : this.eventSources) {
                    fo.addFileChangeListener(this.fcl);
                }
                this.needsReloading = false;
                this.mimeType2kitClassLoaded = false;
            }
            if (kitClassOrMimeType == null) {
                HashSet<String> set = new HashSet<String>(this.knownMimeTypes);
                TIMER.log(Level.FINE, "[M] Mime types", new Object[]{this, set.size()});
                return set;
            }
            if (!this.mimeType2kitClassLoaded) {
                long tm = System.currentTimeMillis();
                this.mimeType2kitClassLoaded = true;
                this.mimeType2kitClass.clear();
                this.kitClass2mimeTypes.clear();
                FileObject root = FileUtil.getConfigFile((String)"Editors");
                for (String mimeType : this.knownMimeTypes) {
                    FileObject mimeTypeFolder = root.getFileObject(mimeType);
                    FileObject kitInstanceFile = KitsTrackerImpl.findKitRegistration(mimeTypeFolder);
                    if (kitInstanceFile != null) {
                        this.mimeType2kitClass.put(mimeType, kitInstanceFile);
                        continue;
                    }
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.fine("No kit for '" + mimeType + "'");
                }
                long tm2 = System.currentTimeMillis();
                TIMER.log(Level.FINE, "Kits scan", new Object[]{this, tm2 - tm});
                TIMER.log(Level.FINE, "[M] Kits", new Object[]{this, this.mimeType2kitClass.size()});
            }
            if (kitClassOrMimeType instanceof Class) {
                Class kitClass = (Class)kitClassOrMimeType;
                List<String> list = this.kitClass2mimeTypes.get(kitClass);
                if (list == null) {
                    list = new ArrayList<String>();
                    this.kitClass2mimeTypes.put(kitClass, list);
                    for (Class clazz = kitClass; clazz != null && !WELL_KNOWN_PARENTS.contains(clazz.getName()); clazz = clazz.getSuperclass()) {
                        boolean kitClassFinal = (clazz.getModifiers() & 16) != 0;
                        for (String mimeType : this.mimeType2kitClass.keySet()) {
                            FileObject f = this.mimeType2kitClass.get(mimeType);
                            if (!KitsTrackerImpl.isInstanceOf(f, clazz, !kitClassFinal)) continue;
                            list.add(mimeType);
                        }
                        if (!list.isEmpty()) break;
                    }
                }
                TIMER.log(Level.FINE, "[M] " + kitClass.getSimpleName() + " used", new Object[]{this, list.size()});
                return list;
            }
            assert (kitClassOrMimeType instanceof String);
            FileObject f = this.mimeType2kitClass.get((String)kitClassOrMimeType);
            return KitsTrackerImpl.instanceClass(f);
        }
    }

}

