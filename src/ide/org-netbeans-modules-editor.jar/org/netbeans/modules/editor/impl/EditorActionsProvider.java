/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.impl;

import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.impl.ActionsList;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.filesystems.FileObject;

@MimeLocation(subfolderName="Actions", instanceProviderClass=EditorActionsProvider.class)
public final class EditorActionsProvider
extends ActionsList
implements InstanceProvider<EditorActionsProvider> {
    static final String EDITOR_ACTIONS_FOLDER_NAME = "Actions";

    public static List<Action> getEditorActions(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        EditorActionsProvider provider = (EditorActionsProvider)MimeLookup.getLookup((MimePath)mimePath).lookup(EditorActionsProvider.class);
        return provider == null ? Collections.emptyList() : provider.getActionsOnly();
    }

    public static List<Object> getItems(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        EditorActionsProvider provider = (EditorActionsProvider)MimeLookup.getLookup((MimePath)mimePath).lookup(EditorActionsProvider.class);
        return provider == null ? Collections.emptyList() : provider.getAllInstances();
    }

    public EditorActionsProvider() {
        this(null);
    }

    private EditorActionsProvider(List<FileObject> keys) {
        super(keys, false, true);
    }

    public EditorActionsProvider createInstance(List<FileObject> fileObjectList) {
        return new EditorActionsProvider(fileObjectList);
    }
}

