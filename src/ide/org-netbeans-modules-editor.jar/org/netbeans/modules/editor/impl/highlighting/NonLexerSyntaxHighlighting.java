/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.TokenCategory
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.impl.highlighting;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.util.WeakListeners;

public final class NonLexerSyntaxHighlighting
extends AbstractHighlightsContainer
implements DocumentListener {
    private static final Logger LOG = Logger.getLogger(NonLexerSyntaxHighlighting.class.getName());
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.oldlibbridge.NonLexerSyntaxHighlighting";
    private final Document document;
    private long version = 0;
    private final MimePath mimePath;
    private final WeakHashMap<TokenID, AttributeSet> attribsCache = new WeakHashMap();

    public NonLexerSyntaxHighlighting(Document document, String mimeType) {
        this.mimePath = MimePath.parse((String)mimeType);
        this.document = document;
        this.document.addDocumentListener(WeakListeners.document((DocumentListener)this, (Object)document));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        NonLexerSyntaxHighlighting nonLexerSyntaxHighlighting = this;
        synchronized (nonLexerSyntaxHighlighting) {
            if (this.document instanceof BaseDocument) {
                return new HSImpl(this.version, (BaseDocument)this.document, startOffset, endOffset);
            }
            return HighlightsSequence.EMPTY;
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.documentChanged(e.getOffset(), e.getLength());
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.documentChanged(e.getOffset(), e.getLength());
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.documentChanged(e.getOffset(), e.getLength());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private AttributeSet findAttribs(TokenItem tokenItem) {
        NonLexerSyntaxHighlighting nonLexerSyntaxHighlighting = this;
        synchronized (nonLexerSyntaxHighlighting) {
            AttributeSet attribs = this.attribsCache.get((Object)tokenItem.getTokenID());
            if (attribs == null) {
                FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)this.mimePath).lookup(FontColorSettings.class);
                if (fcs != null) {
                    attribs = NonLexerSyntaxHighlighting.findFontAndColors(fcs, tokenItem);
                    if (attribs == null) {
                        attribs = SimpleAttributeSet.EMPTY;
                    }
                    this.attribsCache.put(tokenItem.getTokenID(), attribs);
                } else {
                    LOG.warning("Can't find FCS for mime path: '" + this.mimePath.getPath() + "'");
                }
            }
            return attribs == null ? SimpleAttributeSet.EMPTY : attribs;
        }
    }

    private static AttributeSet findFontAndColors(FontColorSettings fcs, TokenItem tokenItem) {
        String categoryName;
        TokenCategory category;
        AttributeSet attribs = null;
        TokenContextPath tokenContextPath = tokenItem.getTokenContextPath();
        String name = tokenContextPath.getFullTokenName((TokenCategory)tokenItem.getTokenID());
        if (name != null) {
            attribs = fcs.getTokenFontColors(name);
        }
        if (attribs == null && (category = tokenItem.getTokenID().getCategory()) != null && (categoryName = tokenContextPath.getFullTokenName(category)) != null) {
            attribs = fcs.getTokenFontColors(categoryName);
        }
        return attribs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void documentChanged(int offset, int lenght) {
        NonLexerSyntaxHighlighting nonLexerSyntaxHighlighting = this;
        synchronized (nonLexerSyntaxHighlighting) {
            ++this.version;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Document changed: changeStart = " + offset + ", changeEnd = " + (offset + lenght));
        }
        if (offset < 0 || offset > this.document.getLength()) {
            offset = 0;
        }
        if (lenght <= 0 || offset + lenght > this.document.getLength()) {
            lenght = this.document.getLength() - offset;
        }
        this.fireHighlightsChange(offset, offset + lenght);
    }

    private final class HSImpl
    implements HighlightsSequence {
        private final long version;
        private final BaseDocument baseDocument;
        private final int startOffset;
        private final int endOffset;
        private boolean init;
        private TokenItem tokenItem;

        public HSImpl(long version, BaseDocument baseDocument, int startOffset, int endOffset) {
            this.init = false;
            this.version = version;
            this.baseDocument = baseDocument;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        public boolean moveNext() {
            if (!this.init) {
                this.init = true;
                try {
                    SyntaxSupport syntax = this.baseDocument.getSyntaxSupport();
                    if (syntax instanceof ExtSyntaxSupport) {
                        ExtSyntaxSupport ess = (ExtSyntaxSupport)syntax;
                        this.tokenItem = ess.getTokenChain(this.startOffset, this.endOffset);
                    } else {
                        LOG.log(Level.WARNING, "Token sequence not an ExtSyntaxSupport: document " + (Object)this.baseDocument + " (lexer.nbbridge module missing?)");
                        this.tokenItem = null;
                    }
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, "Can't get token sequence: document " + (Object)this.baseDocument + ", startOffset = " + this.startOffset + ", endOffset = " + this.endOffset, e);
                    this.tokenItem = null;
                }
                while (null != this.tokenItem && this.tokenItem.getOffset() + this.tokenItem.getImage().length() <= this.startOffset) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Skipping tokenId: " + (Object)this.tokenItem.getTokenID() + ", tokenStart = " + this.tokenItem.getOffset() + ", tokenEnd = " + (this.tokenItem.getOffset() + this.tokenItem.getImage().length()) + ", startOffset = " + this.startOffset + ", endOffset = " + this.endOffset);
                    }
                    this.tokenItem = this.tokenItem.getNext();
                }
            } else if (this.tokenItem != null) {
                this.tokenItem = this.tokenItem.getNext();
            }
            if (this.tokenItem != null && this.tokenItem.getOffset() > this.endOffset) {
                this.tokenItem = null;
            }
            if (LOG.isLoggable(Level.FINE)) {
                if (this.tokenItem != null) {
                    LOG.fine("Next tokenId: " + (Object)this.tokenItem.getTokenID() + ", tokenStart = " + this.tokenItem.getOffset() + ", tokenEnd = " + (this.tokenItem.getOffset() + this.tokenItem.getImage().length()) + ", startOffset = " + this.startOffset + ", endOffset = " + this.endOffset);
                } else {
                    LOG.fine("Next tokenId: null");
                }
            }
            return this.tokenItem != null;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getStartOffset() {
            NonLexerSyntaxHighlighting nonLexerSyntaxHighlighting = NonLexerSyntaxHighlighting.this;
            synchronized (nonLexerSyntaxHighlighting) {
                this.checkVersion();
                if (!this.init) {
                    throw new NoSuchElementException("Call moveNext() first.");
                }
                if (this.tokenItem == null) {
                    throw new NoSuchElementException();
                }
                return Math.max(this.tokenItem.getOffset(), this.startOffset);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getEndOffset() {
            NonLexerSyntaxHighlighting nonLexerSyntaxHighlighting = NonLexerSyntaxHighlighting.this;
            synchronized (nonLexerSyntaxHighlighting) {
                this.checkVersion();
                if (!this.init) {
                    throw new NoSuchElementException("Call moveNext() first.");
                }
                if (this.tokenItem == null) {
                    throw new NoSuchElementException();
                }
                return Math.min(this.tokenItem.getOffset() + this.tokenItem.getImage().length(), this.endOffset);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public AttributeSet getAttributes() {
            NonLexerSyntaxHighlighting nonLexerSyntaxHighlighting = NonLexerSyntaxHighlighting.this;
            synchronized (nonLexerSyntaxHighlighting) {
                this.checkVersion();
                if (!this.init) {
                    throw new NoSuchElementException("Call moveNext() first.");
                }
                if (this.tokenItem == null) {
                    throw new NoSuchElementException();
                }
                return NonLexerSyntaxHighlighting.this.findAttribs(this.tokenItem);
            }
        }

        private void checkVersion() {
            if (this.version != NonLexerSyntaxHighlighting.this.version) {
                throw new ConcurrentModificationException();
            }
        }
    }

}

