/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.openide.text.IndentEngine
 */
package org.netbeans.modules.editor.impl;

import java.io.IOException;
import java.io.Writer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.openide.text.IndentEngine;

public final class DefaultIndentEngine
extends IndentEngine {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive exception aggregation
     */
    public int indentLine(Document doc, int offset) {
        Indent indent = Indent.get((Document)doc);
        indent.lock();
        try {
            int n;
            if (doc instanceof BaseDocument) {
                ((BaseDocument)doc).atomicLock();
            }
            try {
                Position pos = doc.createPosition(offset);
                indent.reindent(offset);
                n = pos.getOffset();
            }
            catch (BadLocationException ble) {
                int n2;
                block12 : {
                    n2 = offset;
                    if (!(doc instanceof BaseDocument)) break block12;
                    ((BaseDocument)doc).atomicUnlock();
                }
                indent.unlock();
                return n2;
                {
                    catch (Throwable throwable) {
                        if (doc instanceof BaseDocument) {
                            ((BaseDocument)doc).atomicUnlock();
                        }
                        throw throwable;
                    }
                }
            }
            if (doc instanceof BaseDocument) {
                ((BaseDocument)doc).atomicUnlock();
            }
            return n;
        }
        finally {
            indent.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Loose catch block
     * Enabled aggressive exception aggregation
     */
    public int indentNewLine(Document doc, int offset) {
        Indent indent = Indent.get((Document)doc);
        indent.lock();
        try {
            int n;
            if (doc instanceof BaseDocument) {
                ((BaseDocument)doc).atomicLock();
            }
            try {
                n = indent.indentNewLine(offset);
            }
            catch (BadLocationException ble) {
                int n2;
                block12 : {
                    n2 = offset;
                    if (!(doc instanceof BaseDocument)) break block12;
                    ((BaseDocument)doc).atomicUnlock();
                }
                indent.unlock();
                return n2;
                {
                    catch (Throwable throwable) {
                        if (doc instanceof BaseDocument) {
                            ((BaseDocument)doc).atomicUnlock();
                        }
                        throw throwable;
                    }
                }
            }
            if (doc instanceof BaseDocument) {
                ((BaseDocument)doc).atomicUnlock();
            }
            return n;
        }
        finally {
            indent.unlock();
        }
    }

    public Writer createWriter(Document doc, int offset, Writer writer) {
        return new WriterImpl(doc, offset, writer);
    }

    private static class WriterImpl
    extends Writer {
        private Document doc;
        private int offset;
        private Writer writer;
        private StringBuilder buffer;
        private int writtenLen = 0;

        private WriterImpl(Document doc, int offset, Writer writer) {
            if (offset < 0) {
                throw new IllegalArgumentException("offset=" + offset + " < 0");
            }
            if (offset > doc.getLength()) {
                throw new IllegalArgumentException("offset=" + offset + " > docLen=" + doc.getLength());
            }
            this.doc = doc;
            this.offset = offset;
            this.writer = writer;
            this.buffer = new StringBuilder();
        }

        @Override
        public void write(int c) throws IOException {
            this.write(new char[]{(char)c}, 0, 1);
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            this.buffer.append(cbuf, off, len);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void flush() throws IOException {
            block5 : {
                Reformat reformat = Reformat.get((Document)this.doc);
                reformat.lock();
                try {
                    String text = this.buffer.toString();
                    if (text.length() <= 0 || this.offset > this.doc.getLength()) break block5;
                    try {
                        this.doc.insertString(this.offset, text, null);
                        Position endPos = this.doc.createPosition(this.offset + text.length());
                        reformat.reformat(this.offset, endPos.getOffset());
                        int len = endPos.getOffset() - this.offset;
                        String reformattedText = this.doc.getText(this.offset, len);
                        this.doc.remove(this.offset, len);
                        this.writer.write(reformattedText.substring(this.writtenLen));
                        this.writtenLen = len;
                    }
                    catch (BadLocationException e) {
                        // empty catch block
                    }
                }
                finally {
                    reformat.unlock();
                }
            }
        }

        @Override
        public void close() throws IOException {
            this.flush();
        }
    }

}

