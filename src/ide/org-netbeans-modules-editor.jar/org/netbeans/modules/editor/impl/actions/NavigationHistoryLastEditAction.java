/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.netbeans.editor.BaseAction
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.impl.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.MainMenuAction;
import org.netbeans.modules.editor.impl.actions.NavigationHistoryBackAction;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public final class NavigationHistoryLastEditAction
extends BaseAction
implements PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(NavigationHistoryLastEditAction.class.getName());

    public NavigationHistoryLastEditAction() {
        this.update();
        NavigationHistory nav = NavigationHistory.getEdits();
        nav.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)nav));
    }

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        NavigationHistory nav = NavigationHistory.getEdits();
        NavigationHistory.Waypoint wpt = nav.getCurrentWaypoint();
        if (wpt != null) {
            wpt = this.isStandingThere(target, wpt) ? nav.navigateBack() : null;
        }
        if (wpt == null) {
            wpt = nav.navigateLast();
        }
        if (wpt != null) {
            NavigationHistoryBackAction.show(wpt);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.update();
    }

    private void update() {
        NavigationHistory nav = NavigationHistory.getEdits();
        this.setEnabled(nav.hasNextWaypoints() || nav.hasPreviousWaypoints() || null != nav.getCurrentWaypoint());
    }

    private boolean isStandingThere(JTextComponent target, NavigationHistory.Waypoint wpt) {
        return target == wpt.getComponent() && target.getCaret().getDot() == wpt.getOffset();
    }

    public static final class MainMenu
    extends MainMenuAction {
        public MainMenu() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(NavigationHistoryLastEditAction.class).getString("jump_back_main_menu_item-main-menu");
        }

        @Override
        protected String getActionName() {
            return "jump-list-last-edit";
        }
    }

}

