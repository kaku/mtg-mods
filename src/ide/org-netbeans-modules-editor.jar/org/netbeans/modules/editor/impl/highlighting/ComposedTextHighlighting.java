/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocumentEvent
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeEvent
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeListener
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.impl.highlighting;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.font.TextAttribute;
import java.awt.im.InputMethodHighlight;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.WeakListeners;

public final class ComposedTextHighlighting
extends AbstractHighlightsContainer
implements DocumentListener,
HighlightsChangeListener {
    private static final Logger LOG = Logger.getLogger(ComposedTextHighlighting.class.getName());
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.impl.highlighting.ComposedTextHighlighting";
    private static final String PROP_COMPLETION_ACTIVE = "completion-active";
    private final JTextComponent component;
    private final Document document;
    private final OffsetsBag bag;
    private final AttributeSet highlightInverse;
    private final AttributeSet highlightUnderlined;
    private boolean isComposingText = false;

    public ComposedTextHighlighting(JTextComponent component, Document document, String mimeType) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.parse((String)mimeType)).lookup(FontColorSettings.class);
        AttributeSet dc = fcs.getFontColors("default");
        Color background = (Color)dc.getAttribute(StyleConstants.Background);
        Color foreground = (Color)dc.getAttribute(StyleConstants.Foreground);
        this.highlightInverse = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Background, foreground, StyleConstants.Foreground, background});
        this.highlightUnderlined = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Underline, foreground});
        this.bag = new OffsetsBag(document);
        this.bag.addHighlightsChangeListener((HighlightsChangeListener)this);
        this.document = document;
        this.document.addDocumentListener(WeakListeners.document((DocumentListener)this, (Object)this.document));
        this.component = component;
    }

    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        return this.bag.getHighlights(startOffset, endOffset);
    }

    public void highlightChanged(HighlightsChangeEvent event) {
        this.fireHighlightsChange(event.getStartOffset(), event.getEndOffset());
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        AttributedString composedText = ComposedTextHighlighting.getComposedTextAttribute(e);
        if (composedText != null) {
            char c;
            Map<AttributedCharacterIterator.Attribute, Object> attributes;
            if (!this.isComposingText) {
                ComposedTextHighlighting.enableParsing(this.component, false);
            }
            this.isComposingText = true;
            if (LOG.isLoggable(Level.FINE)) {
                StringBuilder sb = new StringBuilder();
                AttributedCharacterIterator aci = composedText.getIterator();
                sb.append("\nInsertUpdate: \n");
                c = aci.first();
                while (c != '\uffff') {
                    sb.append("'").append(c).append("' = {");
                    attributes = aci.getAttributes();
                    for (AttributedCharacterIterator.Attribute key : attributes.keySet()) {
                        Object value = attributes.get(key);
                        if (value instanceof InputMethodHighlight) {
                            sb.append("'").append(key).append("' = {");
                            Map style = ((InputMethodHighlight)value).getStyle();
                            if (style == null) {
                                style = Toolkit.getDefaultToolkit().mapInputMethodHighlight((InputMethodHighlight)value);
                            }
                            if (style != null) {
                                for (TextAttribute ta : style.keySet()) {
                                    Object tav = style.get(ta);
                                    sb.append("'").append(ta).append("' = '").append(tav).append("', ");
                                }
                            } else {
                                sb.append("null");
                            }
                            sb.append("}, ");
                            continue;
                        }
                        sb.append("'").append(key).append("' = '").append(value).append("', ");
                    }
                    sb.append("}\n");
                    c = aci.next();
                }
                sb.append("-------------------------------------\n");
                LOG.fine(sb.toString());
            }
            AttributedCharacterIterator aci = composedText.getIterator();
            this.bag.clear();
            int offset = e.getOffset();
            c = aci.first();
            while (c != '\uffff') {
                attributes = aci.getAttributes();
                AttributeSet attrs = this.translateAttributes(attributes);
                this.bag.addHighlight(offset, offset + 1, attrs);
                ++offset;
                c = aci.next();
            }
        } else {
            if (this.isComposingText) {
                ComposedTextHighlighting.enableParsing(this.component, true);
            }
            this.isComposingText = false;
            this.bag.clear();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    private static AttributedString getComposedTextAttribute(DocumentEvent e) {
        Object value;
        AttributeSet attribs;
        if (e instanceof BaseDocumentEvent && (attribs = ((BaseDocumentEvent)e).getChangeAttributes()) != null && (value = attribs.getAttribute(StyleConstants.ComposedTextAttribute)) instanceof AttributedString) {
            return (AttributedString)value;
        }
        return null;
    }

    private AttributeSet translateAttributes(Map<AttributedCharacterIterator.Attribute, ?> source) {
        for (AttributedCharacterIterator.Attribute sourceKey : source.keySet()) {
            Object sourceValue = source.get(sourceKey);
            if (!(sourceValue instanceof InputMethodHighlight)) continue;
            InputMethodHighlight imh = (InputMethodHighlight)sourceValue;
            if (imh.isSelected()) {
                return this.highlightInverse;
            }
            return this.highlightUnderlined;
        }
        LOG.fine("No translation for " + source);
        return SimpleAttributeSet.EMPTY;
    }

    private static void enableParsing(JTextComponent component, boolean enable) {
        boolean newCompletionActive = !enable;
        Boolean oldCompletionActive = (Boolean)component.getClientProperty("completion-active");
        if (oldCompletionActive == null || oldCompletionActive != newCompletionActive) {
            component.putClientProperty("completion-active", newCompletionActive);
        }
    }
}

