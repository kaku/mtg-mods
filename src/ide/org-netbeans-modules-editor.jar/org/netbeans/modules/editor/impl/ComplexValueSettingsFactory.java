/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.Acceptor
 *  org.netbeans.editor.AcceptorFactory
 *  org.netbeans.modules.editor.lib.SettingsConversions
 *  org.openide.text.IndentEngine
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.modules.editor.impl;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.Acceptor;
import org.netbeans.editor.AcceptorFactory;
import org.netbeans.modules.editor.impl.DefaultIndentEngine;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.openide.text.IndentEngine;
import org.openide.util.Lookup;

public final class ComplexValueSettingsFactory {
    private ComplexValueSettingsFactory() {
    }

    public static final Object getRenderingHintsValue(MimePath mimePath, String settingName) {
        assert (settingName.equals("rendering-hints"));
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)mimePath).lookup(FontColorSettings.class);
        return fcs.getFontColors("default").getAttribute(EditorStyleConstants.RenderingHints);
    }

    public static final IndentEngine getIndentEngine(MimePath mimePath) {
        IndentEngine eng = null;
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        String handle = prefs.get("indentEngine", null);
        if (handle != null && handle.indexOf(46) == -1) {
            Lookup.Template query = new Lookup.Template(IndentEngine.class, handle, (Object)null);
            Collection all = Lookup.getDefault().lookup(query).allInstances();
            if (!all.isEmpty()) {
                eng = (IndentEngine)all.iterator().next();
            }
        } else {
            eng = (IndentEngine)SettingsConversions.callFactory((Preferences)prefs, (MimePath)mimePath, (String)"indentEngine", (Object)null);
        }
        if (eng == null) {
            EditorKit kit = (EditorKit)MimeLookup.getLookup((MimePath)mimePath).lookup(EditorKit.class);
            Object legacyFormatter = null;
            if (kit != null) {
                try {
                    Method createFormatterMethod = kit.getClass().getDeclaredMethod("createFormatter", new Class[0]);
                    legacyFormatter = createFormatterMethod.invoke(kit, new Object[0]);
                }
                catch (Exception e) {
                    // empty catch block
                }
            }
            if (legacyFormatter == null) {
                eng = new DefaultIndentEngine();
            }
        }
        return eng;
    }

    public static final Object getIdentifierAcceptorValue(MimePath mimePath, String settingName) {
        assert (settingName.equals("identifier-acceptor"));
        return AcceptorFactory.LETTER_DIGIT;
    }

    public static final Object getWhitespaceAcceptorValue(MimePath mimePath, String settingName) {
        assert (settingName.equals("whitespace-acceptor"));
        return AcceptorFactory.WHITESPACE;
    }
}

