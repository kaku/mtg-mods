/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.impl.actions;

import org.netbeans.modules.editor.MainMenuAction;
import org.openide.util.NbBundle;

public class LineActionsMainMenu {
    private static final String moveSelectionElseLineUpAction = "move-selection-else-line-up";
    private static final String moveSelectionElseLineDownAction = "move-selection-else-line-down";
    private static final String copySelectionElseLineUpAction = "copy-selection-else-line-up";
    private static final String copySelectionElseLineDownAction = "copy-selection-else-line-down";

    public static final class DuplicateDown
    extends MainMenuAction {
        public DuplicateDown() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(LineActionsMainMenu.class).getString("copy-selection-else-line-down-main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "copy-selection-else-line-down";
        }
    }

    public static final class DuplicateUp
    extends MainMenuAction {
        public DuplicateUp() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(LineActionsMainMenu.class).getString("copy-selection-else-line-up-main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "copy-selection-else-line-up";
        }
    }

    public static final class MoveDown
    extends MainMenuAction {
        public MoveDown() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(LineActionsMainMenu.class).getString("move-selection-else-line-down-main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "move-selection-else-line-down";
        }
    }

    public static final class MoveUp
    extends MainMenuAction {
        public MoveUp() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(LineActionsMainMenu.class).getString("move-selection-else-line-up-main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "move-selection-else-line-up";
        }
    }

}

