/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.StatusBar
 *  org.netbeans.editor.Utilities
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.StatusDisplayer$Message
 *  org.openide.awt.StatusLineElementProvider
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.editor.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.StatusBar;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.impl.StatusLineComponent;
import org.openide.awt.StatusDisplayer;
import org.openide.awt.StatusLineElementProvider;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

public final class StatusLineFactories {
    private static final RequestProcessor WORKER = new RequestProcessor(StatusLineFactories.class.getName(), 1, false, false);
    private static final Logger LOG = Logger.getLogger(StatusLineFactories.class.getName());
    public static JLabel LINE_COLUMN_CELL = new StatusLineComponent(StatusLineComponent.Type.LINE_COLUMN);
    public static JLabel TYPING_MODE_CELL = new StatusLineComponent(StatusLineComponent.Type.TYPING_MODE);
    public static final JLabel MAIN_CELL = new JLabel();

    private static void clearStatusLine() {
        LINE_COLUMN_CELL.setText("");
        TYPING_MODE_CELL.setText("");
    }

    static void refreshStatusLine() {
        LOG.fine("StatusLineFactories.refreshStatusLine()\n");
        List componentList = EditorRegistry.componentList();
        for (JTextComponent component : componentList) {
            boolean underMainWindow = SwingUtilities.isDescendingFrom(component, WindowManager.getDefault().getMainWindow());
            EditorUI editorUI = Utilities.getEditorUI((JTextComponent)component);
            if (LOG.isLoggable(Level.FINE)) {
                Object streamDesc;
                String componentDesc = component.toString();
                Document doc = component.getDocument();
                if (doc != null && (streamDesc = doc.getProperty("stream")) != null) {
                    componentDesc = streamDesc.toString();
                }
                LOG.fine("  underMainWindow=" + underMainWindow + ", text-component: " + componentDesc + "\n");
            }
            if (editorUI == null) continue;
            StatusBar statusBar = editorUI.getStatusBar();
            statusBar.setVisible(!underMainWindow);
            boolean shouldUpdateGlobal = underMainWindow && component.isShowing();
            if (!shouldUpdateGlobal) continue;
            statusBar.updateGlobal();
            LOG.fine("  end of refreshStatusLine() - found main window component\n\n");
            return;
        }
        StatusLineFactories.clearStatusLine();
        LOG.fine("  end of refreshStatusLine() - no components - status line cleared\n\n");
    }

    static Component panelWithSeparator(JLabel cell) {
        JSeparator separator = new JSeparator(1){

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(3, 3);
            }
        };
        separator.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        JPanel panel = new JPanel(new BorderLayout());
        panel.add((Component)separator, "West");
        panel.add(cell);
        return panel;
    }

    static {
        MAIN_CELL.addPropertyChangeListener(new PropertyChangeListener(){
            private final AtomicReference<StatusDisplayer.Message> previous = new AtomicReference();

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("text".equals(evt.getPropertyName())) {
                    String text = StatusLineFactories.MAIN_CELL.getText();
                    if ("".equals(text)) {
                        StatusDisplayer.Message message = this.previous.getAndSet(null);
                        if (message != null) {
                            message.clear(0);
                        }
                        return;
                    }
                    Integer importance = (Integer)StatusLineFactories.MAIN_CELL.getClientProperty("importance");
                    final StatusDisplayer.Message msg = StatusDisplayer.getDefault().setStatusText(text, importance.intValue());
                    this.previous.set(msg);
                    WORKER.post(new Runnable(){

                        @Override
                        public void run() {
                            if (1.this.previous.compareAndSet(msg, null)) {
                                msg.clear(0);
                            }
                        }
                    }, 5000);
                }
            }

        });
        StatusBar.setGlobalCell((String)"main", (JLabel)MAIN_CELL);
        StatusBar.setGlobalCell((String)"position", (JLabel)LINE_COLUMN_CELL);
        StatusBar.setGlobalCell((String)"typing-mode", (JLabel)TYPING_MODE_CELL);
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                StatusLineFactories.refreshStatusLine();
            }
        });
    }

    public static final class TypingMode
    implements StatusLineElementProvider {
        public Component getStatusLineElement() {
            return StatusLineFactories.panelWithSeparator(StatusLineFactories.TYPING_MODE_CELL);
        }
    }

    public static final class LineColumn
    implements StatusLineElementProvider {
        public Component getStatusLineElement() {
            return StatusLineFactories.panelWithSeparator(StatusLineFactories.LINE_COLUMN_CELL);
        }
    }

}

