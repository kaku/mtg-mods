/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 */
package org.netbeans.modules.editor.impl.highlighting;

import java.util.ArrayList;
import javax.swing.plaf.TextUI;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.modules.editor.impl.highlighting.AnnotationsHighlighting;
import org.netbeans.modules.editor.impl.highlighting.ComposedTextHighlighting;
import org.netbeans.modules.editor.impl.highlighting.GuardedBlocksHighlighting;
import org.netbeans.modules.editor.impl.highlighting.NonLexerSyntaxHighlighting;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

public final class HLFactory
implements HighlightsLayerFactory {
    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        ArrayList<HighlightsLayer> layers = new ArrayList<HighlightsLayer>();
        Document d = context.getDocument();
        JTextComponent c = context.getComponent();
        String mimeType = HLFactory.getMimeType(c, d);
        layers.add(HighlightsLayer.create((String)"org.netbeans.modules.editor.oldlibbridge.GuardedBlocksHighlighting", (ZOrder)ZOrder.BOTTOM_RACK, (boolean)true, (HighlightsContainer)new GuardedBlocksHighlighting(d, mimeType)));
        layers.add(HighlightsLayer.create((String)"org.netbeans.modules.editor.impl.highlighting.ComposedTextHighlighting", (ZOrder)ZOrder.TOP_RACK, (boolean)true, (HighlightsContainer)new ComposedTextHighlighting(c, d, mimeType)));
        layers.add(HighlightsLayer.create((String)"org.netbeans.modules.editor.oldlibbridge.AnnotationsHighlighting", (ZOrder)ZOrder.DEFAULT_RACK, (boolean)true, (HighlightsContainer)new AnnotationsHighlighting(d)));
        if (!new TokenHierarchyActiveRunnable(context.getDocument()).isActive()) {
            layers.add(HighlightsLayer.create((String)"org.netbeans.modules.editor.oldlibbridge.NonLexerSyntaxHighlighting", (ZOrder)ZOrder.SYNTAX_RACK, (boolean)true, (HighlightsContainer)new NonLexerSyntaxHighlighting(d, mimeType)));
        }
        return layers.toArray((T[])new HighlightsLayer[layers.size()]);
    }

    private static String getMimeType(JTextComponent c, Document d) {
        String mimeType = (String)d.getProperty("mimeType");
        if (mimeType == null) {
            mimeType = c.getUI().getEditorKit(c).getContentType();
        }
        return mimeType == null ? "" : mimeType;
    }

    private static final class TokenHierarchyActiveRunnable
    implements Runnable {
        private Document doc;
        private boolean tokenHierarchyActive;

        TokenHierarchyActiveRunnable(Document doc) {
            this.doc = doc;
        }

        boolean isActive() {
            this.doc.render(this);
            return this.tokenHierarchyActive;
        }

        @Override
        public void run() {
            this.tokenHierarchyActive = TokenHierarchy.get((Document)this.doc).isActive();
        }
    }

}

