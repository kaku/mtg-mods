/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.datatransfer.ClipboardEvent
 *  org.openide.util.datatransfer.ClipboardListener
 *  org.openide.util.datatransfer.ExClipboard
 */
package org.netbeans.modules.editor.impl.actions.clipboardhistory;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.Icon;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistoryElement;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.datatransfer.ClipboardEvent;
import org.openide.util.datatransfer.ClipboardListener;
import org.openide.util.datatransfer.ExClipboard;

public final class ClipboardHistory
implements ClipboardListener {
    private final ArrayList<ClipboardHistoryElement> data = new ArrayList();
    private static ClipboardHistory instance;
    private static int MAXSIZE;
    private static final int MAX_ITEM_LENGTH = 5000000;
    private static Preferences prefs;
    private static final String PREFS_NODE = "ClipboardHistory";
    private static final String PROP_ITEM_PREFIX = "item_";
    private static final boolean PERSISTENT_STATE;

    public static synchronized ClipboardHistory getInstance() {
        if (instance == null) {
            instance = new ClipboardHistory();
        }
        return instance;
    }

    private ClipboardHistory() {
        if (PERSISTENT_STATE) {
            this.load();
        }
    }

    private void addHistory(Transferable transferable, String text) {
        if (transferable == null || text == null || text.length() > 5000000) {
            return;
        }
        ClipboardHistoryElement newHistory = new ClipboardHistoryElement(transferable, text);
        if (PERSISTENT_STATE) {
            this.addAndPersist(newHistory);
        } else {
            this.addHistory(newHistory);
        }
    }

    private synchronized void addHistory(ClipboardHistoryElement newHistory) {
        this.data.remove(newHistory);
        this.data.add(0, newHistory);
        if (this.data.size() > 2 * MAXSIZE) {
            this.data.remove(this.data.size() - 1);
        }
    }

    public synchronized List<ClipboardHistoryElement> getData() {
        if (this.data.size() > MAXSIZE) {
            return Collections.unmodifiableList(this.data.subList(0, MAXSIZE));
        }
        return Collections.unmodifiableList(this.data);
    }

    public void clipboardChanged(ClipboardEvent ev) {
        ExClipboard clipboard = ev.getClipboard();
        Transferable transferable = null;
        String clipboardContent = null;
        try {
            transferable = clipboard.getContents((Object)null);
            if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                clipboardContent = (String)transferable.getTransferData(DataFlavor.stringFlavor);
            }
        }
        catch (OutOfMemoryError oom) {
            NotificationDisplayer.getDefault().notify(NbBundle.getBundle(ClipboardHistory.class).getString("clipboard-history-oom"), NotificationDisplayer.Priority.NORMAL.getIcon(), NbBundle.getBundle(ClipboardHistory.class).getString("clipboard-history-oom-details"), null);
            return;
        }
        catch (IOException ioe) {
        }
        catch (UnsupportedFlavorException ufe) {
            // empty catch block
        }
        if (clipboardContent != null) {
            this.addHistory(transferable, clipboardContent);
        }
    }

    public synchronized int getPosition(ClipboardHistoryElement history) {
        return this.data.indexOf(history);
    }

    private void load() {
        for (int i = 0; i < MAXSIZE; ++i) {
            String item = prefs.get("item_" + i, null);
            if (item == null) continue;
            this.data.add(i, new ClipboardHistoryElement(item));
        }
    }

    public synchronized void addAndPersist(ClipboardHistoryElement newHistoryElement) {
        int i;
        if (this.data.size() > 0 && newHistoryElement.equals(this.data.get(0))) {
            return;
        }
        for (i = 0; i < this.data.size(); ++i) {
            if (!newHistoryElement.equals(this.data.get(i))) continue;
            this.data.remove(i);
            break;
        }
        if (this.data.size() == MAXSIZE) {
            this.data.remove(MAXSIZE - 1);
        }
        this.data.add(0, newHistoryElement);
        i = 0;
        for (ClipboardHistoryElement elem : this.data) {
            prefs.put("item_" + i, elem.getFullText());
            ++i;
        }
    }

    static {
        MAXSIZE = 9;
        PERSISTENT_STATE = Boolean.getBoolean("netbeans.clipboard.history.persistent");
        Integer maxsize = Integer.getInteger("netbeans.clipboard.history.maxsize");
        if (maxsize != null) {
            MAXSIZE = maxsize;
        }
        if (PERSISTENT_STATE) {
            prefs = NbPreferences.forModule(ClipboardHistory.class).node("ClipboardHistory");
        }
    }
}

