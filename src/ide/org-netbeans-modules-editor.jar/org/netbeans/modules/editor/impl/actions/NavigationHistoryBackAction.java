/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.netbeans.editor.BaseDocument
 *  org.openide.awt.DropDownButtonFactory
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.LineCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.text.Line$ShowOpenType
 *  org.openide.text.Line$ShowVisibilityType
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.netbeans.modules.editor.impl.actions;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.editor.BaseDocument;
import org.openide.awt.DropDownButtonFactory;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.Line;
import org.openide.util.ContextAwareAction;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;

public final class NavigationHistoryBackAction
extends TextAction
implements ContextAwareAction,
Presenter.Toolbar,
PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(NavigationHistoryBackAction.class.getName());
    private final JTextComponent component;
    private final NavigationHistory.Waypoint waypoint;
    private final JPopupMenu popupMenu;
    private boolean updatePopupMenu = false;

    public NavigationHistoryBackAction() {
        this(null, null, null);
    }

    private NavigationHistoryBackAction(JTextComponent component, NavigationHistory.Waypoint waypoint, String actionName) {
        super("jump-list-prev");
        this.component = component;
        this.waypoint = waypoint;
        this.putValue("menuText", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryBackAction_Tooltip_simple"));
        if (waypoint != null) {
            this.putValue("Name", actionName);
            this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryBackAction_Tooltip", (Object)actionName));
            this.popupMenu = null;
        } else if (component != null) {
            this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/navigate_back_16.png", (boolean)false));
            this.popupMenu = new JPopupMenu(){

                @Override
                public int getComponentCount() {
                    if (NavigationHistoryBackAction.this.updatePopupMenu) {
                        NavigationHistoryBackAction.this.updatePopupMenu = false;
                        List waypoints = NavigationHistory.getNavigations().getPreviousWaypoints();
                        this.removeAll();
                        int count = 0;
                        String lastFileName = null;
                        NavigationHistory.Waypoint lastWpt = null;
                        for (int i = waypoints.size() - 1; i >= 0; --i) {
                            NavigationHistory.Waypoint wpt = (NavigationHistory.Waypoint)waypoints.get(i);
                            String fileName = NavigationHistoryBackAction.getWaypointName(wpt);
                            if (fileName == null) continue;
                            if (lastFileName == null || !fileName.equals(lastFileName)) {
                                if (lastFileName != null) {
                                    NavigationHistoryBackAction.this.popupMenu.add(new NavigationHistoryBackAction(NavigationHistoryBackAction.this.component, lastWpt, count > 1 ? lastFileName + ":" + count : lastFileName));
                                }
                                lastFileName = fileName;
                                lastWpt = wpt;
                                count = 1;
                                continue;
                            }
                            ++count;
                        }
                        if (lastFileName != null) {
                            this.add(new NavigationHistoryBackAction(NavigationHistoryBackAction.this.component, lastWpt, count > 1 ? lastFileName + ":" + count : lastFileName));
                        }
                    }
                    return super.getComponentCount();
                }
            };
            this.update();
            NavigationHistory nav = NavigationHistory.getNavigations();
            nav.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)nav));
        } else {
            this.popupMenu = null;
            this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryBackAction_Tooltip_simple"));
        }
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        JTextComponent c = NavigationHistoryBackAction.findComponent(actionContext);
        return new NavigationHistoryBackAction(c, null, null);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        NavigationHistory.Waypoint wpt;
        NavigationHistory history = NavigationHistory.getNavigations();
        if (null == history.getCurrentWaypoint()) {
            JTextComponent target;
            JTextComponent jTextComponent = target = this.component != null ? this.component : this.getTextComponent(evt);
            if (target != null) {
                try {
                    history.markWaypoint(target, target.getCaret().getDot(), true, false);
                }
                catch (BadLocationException ble) {
                    LOG.log(Level.WARNING, "Can't mark current position", ble);
                }
            }
        }
        NavigationHistory.Waypoint waypoint = wpt = this.waypoint != null ? history.navigateTo(this.waypoint) : history.navigateBack();
        if (wpt != null) {
            NavigationHistoryBackAction.show(wpt);
        }
    }

    public Component getToolbarPresenter() {
        if (this.popupMenu != null) {
            JButton button = DropDownButtonFactory.createDropDownButton((Icon)((ImageIcon)this.getValue("SmallIcon")), (JPopupMenu)this.popupMenu);
            button.putClientProperty("hideActionText", Boolean.TRUE);
            button.setAction(this);
            return button;
        }
        return new JButton(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.update();
    }

    private void update() {
        List waypoints = NavigationHistory.getNavigations().getPreviousWaypoints();
        if (this.popupMenu != null) {
            this.updatePopupMenu = true;
        }
        if (!waypoints.isEmpty()) {
            NavigationHistory.Waypoint wpt = (NavigationHistory.Waypoint)waypoints.get(waypoints.size() - 1);
            String fileName = NavigationHistoryBackAction.getWaypointName(wpt);
            if (fileName != null) {
                this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryBackAction_Tooltip", (Object)fileName));
            } else {
                this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryBackAction_Tooltip_simple"));
            }
            this.setEnabled(true);
        } else {
            this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryBackAction_Tooltip_simple"));
            this.setEnabled(false);
        }
    }

    static void show(NavigationHistory.Waypoint wpt) {
        JTextComponent component;
        final int offset = wpt.getOffset();
        if (offset < 0) {
            return;
        }
        Lookup lookup = NavigationHistoryBackAction.findLookupFor(wpt);
        if (lookup != null) {
            EditorCookie editorCookie = (EditorCookie)lookup.lookup(EditorCookie.class);
            final LineCookie lineCookie = (LineCookie)lookup.lookup(LineCookie.class);
            StyledDocument doc = null;
            if (editorCookie != null && lineCookie != null) {
                try {
                    doc = editorCookie.openDocument();
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, "Can't open document", ioe);
                }
            }
            if (doc instanceof BaseDocument) {
                final BaseDocument baseDoc = (BaseDocument)doc;
                final Line[] line = new Line[1];
                final int[] column = new int[1];
                baseDoc.render(new Runnable(){

                    @Override
                    public void run() {
                        Element lineRoot = baseDoc.getParagraphElement(0).getParentElement();
                        int lineIndex = lineRoot.getElementIndex(offset);
                        if (lineIndex != -1) {
                            Element lineElement = lineRoot.getElement(lineIndex);
                            column[0] = offset - lineElement.getStartOffset();
                            line[0] = lineCookie.getLineSet().getCurrent(lineIndex);
                        }
                    }
                });
                if (line[0] != null) {
                    line[0].show(Line.ShowOpenType.REUSE, Line.ShowVisibilityType.FOCUS, column[0]);
                    return;
                }
            }
        }
        if ((component = wpt.getComponent()) != null) {
            component.setCaretPosition(offset);
            component.requestFocusInWindow();
        }
    }

    private static Lookup findLookupFor(NavigationHistory.Waypoint wpt) {
        URL url;
        FileObject f;
        JTextComponent component = wpt.getComponent();
        if (component != null) {
            for (Container c = component; c != null; c = c.getParent()) {
                Lookup lookup;
                if (!(c instanceof Lookup.Provider) || (lookup = ((Lookup.Provider)c).getLookup()) == null) continue;
                return lookup;
            }
        }
        FileObject fileObject = f = (url = wpt.getUrl()) == null ? null : URLMapper.findFileObject((URL)url);
        if (f != null) {
            try {
                return DataObject.find((FileObject)f).getLookup();
            }
            catch (DataObjectNotFoundException e) {
                LOG.log(Level.WARNING, "Can't get DataObject for " + (Object)f, (Throwable)e);
            }
        }
        return null;
    }

    static String getWaypointName(NavigationHistory.Waypoint wpt) {
        URL url = wpt.getUrl();
        if (url != null) {
            String path = url.getPath();
            int idx = path.lastIndexOf(47);
            if (idx != -1) {
                return path.substring(idx + 1);
            }
            return path;
        }
        return null;
    }

    static JTextComponent findComponent(Lookup lookup) {
        JEditorPane[] panes;
        EditorCookie ec = (EditorCookie)lookup.lookup(EditorCookie.class);
        if (ec != null && (panes = ec.getOpenedPanes()) != null && panes.length > 0) {
            return panes[0];
        }
        return (JTextComponent)lookup.lookup(JTextComponent.class);
    }

}

