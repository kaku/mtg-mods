/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.ext.GotoDialogSupport
 *  org.netbeans.editor.ext.KeyEventBlocker
 *  org.openide.awt.MouseUtils
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.impl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.ext.GotoDialogSupport;
import org.netbeans.editor.ext.KeyEventBlocker;
import org.openide.awt.MouseUtils;
import org.openide.util.NbBundle;

public final class StatusLineComponent
extends JLabel {
    private static final Insets NULL_INSETS = new Insets(0, 0, 0, 0);
    private static final String MAX_LINE_COLUMN_STRING = "99999:999/9999:9999";
    private static final String MAX_LINE_COLUMN_OFFSET_STRING = "99999:999/9999:9999 <99999999>";
    private static final String INSERT_LOCALE = "status-bar-insert";
    private static final String OVERWRITE_LOCALE = "status-bar-overwrite";
    private static final Logger LOG = Logger.getLogger(StatusLineComponent.class.getName());
    private static final Logger CARET_OFFSET_LOG = Logger.getLogger("org.netbeans.editor.caret.offset");
    private Dimension minDimension;

    StatusLineComponent(Type type) {
        switch (type) {
            case LINE_COLUMN: {
                String[] arrstring = new String[1];
                arrstring[0] = CARET_OFFSET_LOG.isLoggable(Level.FINE) ? "99999:999/9999:9999 <99999999>" : "99999:999/9999:9999";
                this.initMinDimension(arrstring);
                break;
            }
            case TYPING_MODE: {
                ResourceBundle bundle = NbBundle.getBundle(BaseKit.class);
                String insText = bundle.getString("status-bar-insert");
                String ovrText = bundle.getString("status-bar-overwrite");
                this.initMinDimension(insText, ovrText);
                break;
            }
            default: {
                throw new IllegalStateException();
            }
        }
        this.setHorizontalAlignment(0);
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                JTextComponent lastFocusedComponent;
                if (MouseUtils.isDoubleClick((MouseEvent)e) && (lastFocusedComponent = EditorRegistry.lastFocusedComponent()) != null) {
                    new GotoDialogSupport().showGotoDialog(new KeyEventBlocker(lastFocusedComponent, false));
                }
            }
        });
    }

    @Override
    public Dimension getPreferredSize() {
        return this.minDimension;
    }

    private /* varargs */ void initMinDimension(String ... maxStrings) {
        FontMetrics fm = this.getFontMetrics(this.getFont());
        int minWidth = 0;
        for (String s : maxStrings) {
            minWidth = Math.max(minWidth, fm.stringWidth(s));
        }
        Border b = this.getBorder();
        Insets ins = b != null ? b.getBorderInsets(this) : NULL_INSETS;
        int minHeight = fm.getHeight() + ins.top + ins.bottom;
        this.minDimension = new Dimension(minWidth += ins.left + ins.right, minHeight);
    }

    static enum Type {
        LINE_COLUMN,
        TYPING_MODE;
        

        private Type() {
        }
    }

}

