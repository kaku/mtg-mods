/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.URLMapper
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.netbeans.modules.editor.impl;

import java.awt.Container;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.text.JTextComponent;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;

public final class NbURLMapper
extends org.netbeans.modules.editor.lib2.URLMapper {
    private static final Logger LOG = Logger.getLogger(NbURLMapper.class.getName());

    protected JTextComponent getTextComponent(URL url) {
        FileObject f = URLMapper.findFileObject((URL)url);
        if (f != null) {
            JEditorPane[] allJeps;
            EditorCookie cookie;
            DataObject d = null;
            try {
                d = DataObject.find((FileObject)f);
            }
            catch (DataObjectNotFoundException e) {
                LOG.log(Level.WARNING, "Can't get DataObject for " + (Object)f, (Throwable)e);
            }
            if (d != null && (cookie = (EditorCookie)d.getLookup().lookup(EditorCookie.class)) != null && (allJeps = cookie.getOpenedPanes()) != null) {
                return allJeps[0];
            }
        }
        return null;
    }

    protected URL getUrl(JTextComponent comp) {
        FileObject f = null;
        if (comp instanceof Lookup.Provider) {
            f = (FileObject)((Lookup.Provider)comp).getLookup().lookup(FileObject.class);
        }
        if (f == null) {
            for (Container container = comp.getParent(); !(container == null || container instanceof Lookup.Provider && (f = (FileObject)((Lookup.Provider)container).getLookup().lookup(FileObject.class)) != null); container = container.getParent()) {
            }
        }
        if (f != null) {
            try {
                return f.getURL();
            }
            catch (FileStateInvalidException e) {
                LOG.log(Level.WARNING, "Can't get URL for " + (Object)f, (Throwable)e);
            }
        }
        return null;
    }
}

