/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.openide.awt.DropDownButtonFactory
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.netbeans.modules.editor.impl.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.modules.editor.impl.actions.NavigationHistoryBackAction;
import org.openide.awt.DropDownButtonFactory;
import org.openide.util.ContextAwareAction;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;

public final class NavigationHistoryForwardAction
extends TextAction
implements ContextAwareAction,
Presenter.Toolbar,
PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(NavigationHistoryForwardAction.class.getName());
    private final JTextComponent component;
    private final NavigationHistory.Waypoint waypoint;
    private final JPopupMenu popupMenu;
    private boolean updatePopupMenu = false;

    public NavigationHistoryForwardAction() {
        this(null, null, null);
    }

    private NavigationHistoryForwardAction(JTextComponent component, NavigationHistory.Waypoint waypoint, String actionName) {
        super("jump-list-next");
        this.component = component;
        this.waypoint = waypoint;
        this.putValue("menuText", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryForwardAction_Tooltip_simple"));
        if (waypoint != null) {
            this.putValue("Name", actionName);
            this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryBackAction.class, (String)"NavigationHistoryForwardAction_Tooltip", (Object)actionName));
            this.popupMenu = null;
        } else if (component != null) {
            this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/navigate_forward_16.png", (boolean)false));
            this.popupMenu = new JPopupMenu(){

                @Override
                public int getComponentCount() {
                    if (NavigationHistoryForwardAction.this.updatePopupMenu) {
                        NavigationHistoryForwardAction.this.updatePopupMenu = false;
                        NavigationHistoryForwardAction.this.popupMenu.removeAll();
                        int count = 0;
                        String lastFileName = null;
                        NavigationHistory.Waypoint lastWpt = null;
                        List waypoints = NavigationHistory.getNavigations().getNextWaypoints();
                        for (int i = 0; i < waypoints.size(); ++i) {
                            NavigationHistory.Waypoint wpt = (NavigationHistory.Waypoint)waypoints.get(i);
                            String fileName = NavigationHistoryBackAction.getWaypointName(wpt);
                            if (fileName == null) continue;
                            if (lastFileName == null || !fileName.equals(lastFileName)) {
                                if (lastFileName != null) {
                                    NavigationHistoryForwardAction.this.popupMenu.add(new NavigationHistoryForwardAction(NavigationHistoryForwardAction.this.component, lastWpt, count > 1 ? lastFileName + ":" + count : lastFileName));
                                }
                                lastFileName = fileName;
                                lastWpt = wpt;
                                count = 1;
                                continue;
                            }
                            ++count;
                        }
                        if (lastFileName != null) {
                            NavigationHistoryForwardAction.this.popupMenu.add(new NavigationHistoryForwardAction(NavigationHistoryForwardAction.this.component, lastWpt, count > 1 ? lastFileName + ":" + count : lastFileName));
                        }
                    }
                    return super.getComponentCount();
                }
            };
            this.update();
            NavigationHistory nav = NavigationHistory.getNavigations();
            nav.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)nav));
        } else {
            this.popupMenu = null;
            this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryForwardAction.class, (String)"NavigationHistoryForwardAction_Tooltip_simple"));
        }
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        JTextComponent c = NavigationHistoryBackAction.findComponent(actionContext);
        return new NavigationHistoryForwardAction(c, null, null);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        NavigationHistory.Waypoint wpt;
        NavigationHistory history = NavigationHistory.getNavigations();
        NavigationHistory.Waypoint waypoint = wpt = this.waypoint != null ? history.navigateTo(this.waypoint) : history.navigateForward();
        if (wpt != null) {
            NavigationHistoryBackAction.show(wpt);
        }
    }

    public Component getToolbarPresenter() {
        if (this.popupMenu != null) {
            JButton button = DropDownButtonFactory.createDropDownButton((Icon)((ImageIcon)this.getValue("SmallIcon")), (JPopupMenu)this.popupMenu);
            button.putClientProperty("hideActionText", Boolean.TRUE);
            button.setAction(this);
            return button;
        }
        return new JButton(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.update();
    }

    private void update() {
        List waypoints = NavigationHistory.getNavigations().getNextWaypoints();
        if (this.popupMenu != null) {
            this.updatePopupMenu = true;
        }
        if (!waypoints.isEmpty()) {
            NavigationHistory.Waypoint wpt = (NavigationHistory.Waypoint)waypoints.get(0);
            String fileName = NavigationHistoryBackAction.getWaypointName(wpt);
            if (fileName != null) {
                this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryForwardAction.class, (String)"NavigationHistoryForwardAction_Tooltip", (Object)fileName));
            } else {
                this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryForwardAction.class, (String)"NavigationHistoryForwardAction_Tooltip_simple"));
            }
            this.setEnabled(true);
        } else {
            this.putValue("ShortDescription", NbBundle.getMessage(NavigationHistoryForwardAction.class, (String)"NavigationHistoryForwardAction_Tooltip_simple"));
            this.setEnabled(false);
        }
    }

}

