/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.impl.actions.clipboardhistory;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.ClipboardHistory;

public class ClipboardHistoryElement
implements ClipboardOwner {
    private final String content;
    private Transferable transferable = null;
    private static final int MAXSIZE = 30;
    private static final String ENDING = "...";

    public ClipboardHistoryElement(Transferable transferable, String text) {
        this(text);
        this.transferable = transferable;
    }

    ClipboardHistoryElement(String text) {
        this.content = text;
    }

    public String getShortenText() {
        String output = this.content.trim();
        if (this.isShorten()) {
            if (output.length() < 30) {
                return output + "...";
            }
            return output.substring(0, 30) + "...";
        }
        return output;
    }

    public Transferable getTransferable() {
        return this.transferable;
    }

    public String getFullText() {
        return this.content;
    }

    public boolean isShorten() {
        return this.content.length() > 30 || this.content.trim().isEmpty();
    }

    public String getNumber() {
        return "" + (ClipboardHistory.getInstance().getPosition(this) + 1);
    }

    public boolean equals(Object obj) {
        if (obj instanceof ClipboardHistoryElement) {
            return this.content.equals(((ClipboardHistoryElement)obj).content);
        }
        return false;
    }

    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.content != null ? this.content.hashCode() : 0);
        return hash;
    }

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }
}

