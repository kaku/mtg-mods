/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.EditorImplementationProvider
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.editor.impl;

import java.awt.Container;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.impl.GlyphGutterActionsProvider;
import org.netbeans.modules.editor.lib2.EditorImplementationProvider;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public final class NbEditorImplementationProvider
implements EditorImplementationProvider {
    private static final Action[] NO_ACTIONS = new Action[0];

    public ResourceBundle getResourceBundle(String localizer) {
        return NbBundle.getBundle((String)localizer);
    }

    public Action[] getGlyphGutterActions(JTextComponent target) {
        String mimeType = NbEditorUtilities.getMimeType(target);
        if (mimeType != null) {
            List actions = GlyphGutterActionsProvider.getGlyphGutterActions(mimeType);
            return actions.toArray(new Action[actions.size()]);
        }
        return NO_ACTIONS;
    }

    public boolean activateComponent(JTextComponent c) {
        Container container = SwingUtilities.getAncestorOfClass(TopComponent.class, c);
        if (container != null) {
            ((TopComponent)container).requestActive();
            return true;
        }
        return false;
    }
}

