/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.SideBarFactory
 *  org.netbeans.spi.editor.SideBarFactory
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.cookies.InstanceCookie
 *  org.openide.cookies.InstanceCookie$Of
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package org.netbeans.modules.editor.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.modules.editor.impl.CustomizableSideBar;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;

@MimeLocation(subfolderName="SideBar", instanceProviderClass=SideBarFactoriesProvider.class)
public final class SideBarFactoriesProvider
implements InstanceProvider<SideBarFactoriesProvider> {
    private static final Logger LOG = Logger.getLogger(SideBarFactoriesProvider.class.getName());
    public static final String SIDEBAR_COMPONENTS_FOLDER_NAME = "SideBar";
    private final List<FileObject> instanceFiles;
    private Map<CustomizableSideBar.SideBarPosition, List> factories;

    public SideBarFactoriesProvider() {
        this(Collections.emptyList());
    }

    private SideBarFactoriesProvider(List<FileObject> instanceFiles) {
        this.instanceFiles = instanceFiles;
    }

    public Map<CustomizableSideBar.SideBarPosition, List> getFactories() {
        if (this.factories == null) {
            this.factories = this.computeInstances();
        }
        return this.factories;
    }

    public SideBarFactoriesProvider createInstance(List<FileObject> fileObjectList) {
        return new SideBarFactoriesProvider(fileObjectList);
    }

    private Map<CustomizableSideBar.SideBarPosition, List> computeInstances() {
        HashMap<CustomizableSideBar.SideBarPosition, List> factoriesMap = new HashMap<CustomizableSideBar.SideBarPosition, List>();
        for (FileObject f : this.instanceFiles) {
            org.netbeans.spi.editor.SideBarFactory factory2;
            SideBarFactory factory;
            block7 : {
                factory = null;
                factory2 = null;
                if (!f.isValid() || !f.isData()) continue;
                try {
                    DataObject dob = DataObject.find((FileObject)f);
                    InstanceCookie.Of ic = (InstanceCookie.Of)dob.getCookie(InstanceCookie.Of.class);
                    if (ic == null) break block7;
                    if (ic.instanceOf(SideBarFactory.class)) {
                        factory = (SideBarFactory)ic.instanceCreate();
                        break block7;
                    }
                    if (!ic.instanceOf(org.netbeans.spi.editor.SideBarFactory.class)) break block7;
                    factory2 = (org.netbeans.spi.editor.SideBarFactory)ic.instanceCreate();
                }
                catch (ClassNotFoundException cnfe) {
                    LOG.log(Level.INFO, null, cnfe);
                    continue;
                }
                catch (Exception e) {
                    LOG.log(Level.WARNING, null, e);
                    continue;
                }
            }
            if (factory == null && factory2 == null) continue;
            CustomizableSideBar.SideBarPosition position = new CustomizableSideBar.SideBarPosition(f);
            ArrayList<Object> factoriesList = factoriesMap.get(position);
            if (factoriesList == null) {
                factoriesList = new ArrayList<Object>();
                factoriesMap.put(position, factoriesList);
            }
            if (factory != null) {
                factoriesList.add((Object)factory);
                continue;
            }
            factoriesList.add((Object)factory2);
        }
        return factoriesMap;
    }
}

