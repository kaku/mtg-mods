/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.AbstractEditorAction
 */
package org.netbeans.modules.editor.impl.actions;

import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.impl.actions.clipboardhistory.CompletionLayoutPopup;
import org.netbeans.spi.editor.AbstractEditorAction;

public final class ClipboardHistoryAction
extends AbstractEditorAction {
    private static final long serialVersionUID = 1;

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        if (target != null && target.isEditable()) {
            CompletionLayoutPopup.CompletionPopup completionPopup = CompletionLayoutPopup.CompletionPopup.getInstance();
            completionPopup.show(target, -1);
        }
    }
}

