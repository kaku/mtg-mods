/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.ActionFactory
 *  org.netbeans.editor.ActionFactory$RedoAction
 *  org.netbeans.editor.ActionFactory$StopMacroRecordingAction
 *  org.netbeans.editor.ActionFactory$ToggleLineNumbersAction
 *  org.netbeans.editor.ActionFactory$UndoAction
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.MacroDialogSupport
 *  org.netbeans.editor.MimeTypeInitializer
 *  org.netbeans.editor.MultiKeymap
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtKit
 *  org.netbeans.editor.ext.ExtKit$BuildPopupMenuAction
 *  org.netbeans.editor.ext.ExtKit$BuildToolTipAction
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.highlighting.HighlightingManager
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.openide.actions.GotoAction
 *  org.openide.actions.PasteAction
 *  org.openide.actions.PopupAction
 *  org.openide.actions.RedoAction
 *  org.openide.actions.UndoAction
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.awt.JPopupMenuPlus
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.editor;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.TextAction;
import javax.swing.undo.UndoManager;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.ActionFactory;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.MacroDialogSupport;
import org.netbeans.editor.MimeTypeInitializer;
import org.netbeans.editor.MultiKeymap;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtKit;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorDocument;
import org.netbeans.modules.editor.NbEditorToolBar;
import org.netbeans.modules.editor.NbEditorUI;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.NbToolTip;
import org.netbeans.modules.editor.impl.ActionsList;
import org.netbeans.modules.editor.impl.CustomizableSideBar;
import org.netbeans.modules.editor.impl.EditorActionsProvider;
import org.netbeans.modules.editor.impl.PopupMenuActionsProvider;
import org.netbeans.modules.editor.impl.ToolbarActionsProvider;
import org.netbeans.modules.editor.impl.actions.NavigationHistoryBackAction;
import org.netbeans.modules.editor.impl.actions.NavigationHistoryForwardAction;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.modules.editor.options.AnnotationTypesFolder;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.openide.actions.GotoAction;
import org.openide.actions.PasteAction;
import org.openide.actions.PopupAction;
import org.openide.actions.RedoAction;
import org.openide.actions.UndoAction;
import org.openide.awt.DynamicMenuContent;
import org.openide.awt.JPopupMenuPlus;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class NbEditorKit
extends ExtKit
implements Callable {
    private static final Logger LOG = Logger.getLogger(NbEditorKit.class.getName());
    public static final String SYSTEM_ACTION_CLASS_NAME_PROPERTY = "systemActionClassName";
    static final long serialVersionUID = 4482122073483644089L;
    private static final Map<String, String> contentTypeTable;
    public static final String generateGoToPopupAction = "generate-goto-popup";
    public static final String generateFoldPopupAction = "generate-fold-popup";
    private static final NbUndoAction nbUndoActionDef;
    private static final NbRedoAction nbRedoActionDef;
    private Map systemAction2editorAction = new HashMap();
    static final Object SEPARATOR;
    static final int ACTIONS_TOPCOMPONENT = 1;
    static final int ACTION_SYSTEM = 2;
    static final int ACTION_EXTKIT_BYNAME = 3;
    static final int ACTION_CREATEITEM = 4;
    static final int ACTION_FOLDER = 5;
    static final int ACTION_SEPARATOR = 11;

    public Document createDefaultDocument() {
        return new NbEditorDocument(this.getContentType());
    }

    protected void toolTipAnnotationsLock(Document doc) {
    }

    protected void toolTipAnnotationsUnlock(Document doc) {
    }

    protected EditorUI createEditorUI() {
        return new NbEditorUI();
    }

    protected Action[] createActions() {
        Action[] nbEditorActions = new Action[]{nbUndoActionDef, nbRedoActionDef, new GenerateFoldPopupAction(), new NavigationHistoryBackAction(), new NavigationHistoryForwardAction(), new NbGenerateGoToPopupAction()};
        return TextAction.augmentList(super.createActions(), nbEditorActions);
    }

    protected Action[] getDeclaredActions() {
        List<Action> declaredActionList = EditorActionsProvider.getEditorActions(this.getContentType());
        Action[] declaredActions = new Action[declaredActionList.size()];
        declaredActionList.toArray(declaredActions);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Declared Actions (found ({0})): ", new Object[]{declaredActions.length});
            for (Action a : declaredActions) {
                LOG.log(Level.FINE, "Action: {0}", new Object[]{a});
            }
        }
        return declaredActions;
    }

    protected void addSystemActionMapping(String editorActionName, Class systemActionClass) {
        Action a = this.getActionByName(editorActionName);
        if (a != null) {
            a.putValue("systemActionClassName", systemActionClass.getName());
        }
        this.systemAction2editorAction.put(systemActionClass.getName(), editorActionName);
    }

    private void addSystemActionMapping(String editorActionName, String systemActionId) {
        Action a = this.getActionByName(editorActionName);
        if (a != null) {
            a.putValue("systemActionClassName", systemActionId.replaceAll("\\.", "-"));
        }
    }

    protected void updateActions() {
        this.addSystemActionMapping("cut-to-clipboard", "cut-to-clipboard");
        this.addSystemActionMapping("copy-to-clipboard", "copy-to-clipboard");
        this.addSystemActionMapping("paste-from-clipboard", PasteAction.class);
        this.addSystemActionMapping("delete-next", "delete");
        this.addSystemActionMapping("show-popup-menu", PopupAction.class);
        this.addSystemActionMapping("goto", GotoAction.class);
        this.addSystemActionMapping("undo", UndoAction.class);
        this.addSystemActionMapping("redo", RedoAction.class);
    }

    private boolean isInheritorOfNbEditorKit() {
        Class clz = this.getClass();
        while (clz.getSuperclass() != null) {
            if (NbEditorKit.class != (clz = clz.getSuperclass())) continue;
            return true;
        }
        return false;
    }

    public String getContentType() {
        if (this.isInheritorOfNbEditorKit()) {
            Logger.getLogger("global").log(Level.WARNING, "Warning: KitClass " + this.getClass().getName() + " doesn't override the method getContentType.");
        }
        return contentTypeTable.containsKey(this.getClass().getName()) ? contentTypeTable.get(this.getClass().getName()) : super.getContentType();
    }

    private static ResourceBundle getBundleFromName(String name) {
        ResourceBundle bundle = null;
        if (name != null) {
            try {
                bundle = NbBundle.getBundle((String)name);
            }
            catch (MissingResourceException mre) {
                // empty catch block
            }
        }
        return bundle;
    }

    private static Lookup getContextLookup(Component component) {
        Lookup lookup = null;
        for (Component c = component; !(c == null || c instanceof Lookup.Provider && (lookup = ((Lookup.Provider)c).getLookup()) != null); c = c.getParent()) {
        }
        return lookup;
    }

    private static Action translateContextLookupAction(Lookup contextLookup, Action action) {
        if (action instanceof ContextAwareAction && contextLookup != null) {
            action = ((ContextAwareAction)action).createContextAwareInstance(contextLookup);
        }
        return action;
    }

    private static JMenuItem createLocalizedMenuItem(Action action) {
        JMenuItem item;
        if (action instanceof Presenter.Popup) {
            item = ((Presenter.Popup)action).getPopupPresenter();
        } else {
            item = new JMenuItem(action);
            Mnemonics.setLocalizedText((AbstractButton)item, (String)item.getText());
            if (item.getIcon() != null) {
                item.setIcon(null);
            }
        }
        return item;
    }

    private static void assignAccelerator(Keymap km, Action action, JMenuItem item) {
        if (item.getAccelerator() == null) {
            KeyStroke[] keys;
            KeyStroke ks = (KeyStroke)action.getValue("AcceleratorKey");
            if (ks != null) {
                item.setMnemonic(ks.getKeyCode());
            } else if (km != null && (keys = km.getKeyStrokesForAction(action)) != null && keys.length > 0) {
                item.setMnemonic(keys[0].getKeyCode());
            }
        }
    }

    public Object call() {
        Map<CustomizableSideBar.SideBarPosition, List> factoriesMap = CustomizableSideBar.getFactoriesMap(this.getContentType());
        for (Map.Entry<CustomizableSideBar.SideBarPosition, List> e : factoriesMap.entrySet()) {
            for (Object f : e.getValue()) {
                if (!(f instanceof MimeTypeInitializer)) continue;
                try {
                    ((MimeTypeInitializer)f).init(this.getContentType());
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        NbEditorToolBar.initKeyBindingList(this.getContentType());
        ToolbarActionsProvider.getToolbarItems(this.getContentType());
        ToolbarActionsProvider.getToolbarItems("text/base");
        AnnotationTypesFolder.getAnnotationTypesFolder();
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.get((String)this.getContentType())).lookup(FontColorSettings.class);
        fcs.getFontColors("default");
        Document doc = this.createDefaultDocument();
        JEditorPane pane = new JEditorPane(){

            @Override
            public void updateUI() {
                this.setUI((TextUI)new BaseTextUI(){

                    public void installUI(JComponent c) {
                    }

                    public void uninstallUI(JComponent c) {
                    }
                });
            }

        };
        pane.setDocument(doc);
        HighlightingManager.getInstance((JTextComponent)pane).getBottomHighlights();
        FoldHierarchy.get((JTextComponent)pane).getRootFold();
        PopupMenuActionsProvider.getPopupMenuItems(this.getContentType());
        this.getKeymap();
        EditorUI ui = Utilities.getEditorUI((JTextComponent)pane);
        String settingName = ui == null || ui.hasExtComponent() ? "popup-menu-action-name-list" : "dialog-popup-menu-action-name-list";
        PopupInitializer init = new PopupInitializer(NbEditorKit.getContextLookup(pane), this, NbEditorUtilities.getOuterTopComponent(pane), this.getContentType(), settingName);
        init.run();
        return null;
    }

    static {
        nbUndoActionDef = new NbUndoAction();
        nbRedoActionDef = new NbRedoAction();
        contentTypeTable = new HashMap<String, String>();
        contentTypeTable.put("org.netbeans.modules.properties.syntax.PropertiesKit", "text/x-properties");
        contentTypeTable.put("org.netbeans.modules.web.core.syntax.JSPKit", "text/x-jsp");
        contentTypeTable.put("org.netbeans.modules.css.text.syntax.CSSEditorKit", "text/css");
        contentTypeTable.put("org.netbeans.modules.xml.css.editor.CSSEditorKit", "text/css");
        contentTypeTable.put("org.netbeans.modules.xml.text.syntax.DTDKit", "text/x-dtd");
        contentTypeTable.put("org.netbeans.modules.xml.text.syntax.XMLKit", "text/xml");
        contentTypeTable.put("org.netbeans.modules.corba.idl.editor.coloring.IDLKit", "text/x-idl");
        SEPARATOR = new String("separator");
    }

    private static final class LayerSubFolderMenu
    extends JMenu {
        private static String getLocalizedName(FileObject f) {
            Object displayName = f.getAttribute("displayName");
            if (displayName instanceof String) {
                return (String)displayName;
            }
            try {
                return f.getFileSystem().getStatus().annotateName(f.getNameExt(), Collections.singleton(f));
            }
            catch (FileStateInvalidException e) {
                return f.getNameExt();
            }
        }

        private static List<FileObject> sort(FileObject[] children) {
            List fos = Arrays.asList(children);
            fos = FileUtil.getOrder(fos, (boolean)true);
            return fos;
        }

        public LayerSubFolderMenu(JTextComponent target, FileObject folder) {
            this(target, LayerSubFolderMenu.getLocalizedName(folder), ActionsList.convert(LayerSubFolderMenu.sort(folder.getChildren()), false));
        }

        private LayerSubFolderMenu(JTextComponent target, String text, List items) {
            Mnemonics.setLocalizedText((AbstractButton)this, (String)text);
            for (Object obj : items) {
                if (obj == null || obj instanceof JSeparator) {
                    this.addSeparator();
                    continue;
                }
                if (obj instanceof String) {
                    LayerSubFolderMenu.addAction(target, (JMenu)this, (String)obj);
                    continue;
                }
                if (obj instanceof Action) {
                    LayerSubFolderMenu.addAction(target, (JMenu)this, (Action)obj);
                    continue;
                }
                if (!(obj instanceof DataFolder)) continue;
                this.add(new LayerSubFolderMenu(target, ((DataFolder)obj).getPrimaryFile()));
            }
        }

        private static void addAcceleretors(Action a, JMenuItem item, JTextComponent target) {
            Keymap km;
            Keymap keymap = km = target == null ? BaseKit.getKit(BaseKit.class).getKeymap() : target.getKeymap();
            if (km != null) {
                KeyStroke ks;
                KeyStroke[] keys = km.getKeyStrokesForAction(a);
                if (keys != null && keys.length > 0) {
                    boolean added = false;
                    for (int i = 0; i < keys.length; ++i) {
                        if (keys[i].getKeyCode() != 106 && keys[i].getKeyCode() != 107) continue;
                        item.setMnemonic(keys[i].getKeyCode());
                        added = true;
                        break;
                    }
                    if (!added) {
                        item.setMnemonic(keys[0].getKeyCode());
                    }
                } else if (a != null && (ks = (KeyStroke)a.getValue("AcceleratorKey")) != null) {
                    item.setMnemonic(ks.getKeyCode());
                }
            }
        }

        private static String getItemText(JTextComponent target, String actionName, Action a) {
            String itemText;
            if (a instanceof BaseAction) {
                itemText = ((BaseAction)a).getPopupMenuText(target);
            } else {
                itemText = (String)a.getValue("popupText");
                if (itemText == null && (itemText = (String)a.getValue("menuText")) == null) {
                    itemText = actionName;
                }
            }
            return itemText;
        }

        private static void addAction(JTextComponent target, JMenu menu, String actionName) {
            assert (target != null);
            assert (menu != null);
            assert (actionName != null);
            BaseKit kit = Utilities.getKit((JTextComponent)target);
            if (kit == null) {
                return;
            }
            Action a = kit.getActionByName(actionName);
            if (a != null) {
                LayerSubFolderMenu.addAction(target, menu, a);
            }
        }

        private static void addAction(JTextComponent target, JMenu menu, Action action) {
            assert (target != null);
            assert (menu != null);
            assert (action != null);
            JMenuItem item = null;
            if (action instanceof BaseAction) {
                item = ((BaseAction)action).getPopupMenuItem(target);
            }
            if (item == null) {
                Lookup contextLookup = NbEditorKit.getContextLookup(target);
                Action nonContextAction = action;
                if ((action = NbEditorKit.translateContextLookupAction(contextLookup, action)) != null) {
                    item = NbEditorKit.createLocalizedMenuItem(action);
                    String actionName = (String)action.getValue("Name");
                    String itemText = LayerSubFolderMenu.getItemText(target, actionName, action);
                    if (itemText != null) {
                        item.setText(itemText);
                        Mnemonics.setLocalizedText((AbstractButton)item, (String)itemText);
                    }
                    LayerSubFolderMenu.addAcceleretors(nonContextAction, item, target);
                    item.setEnabled(action.isEnabled());
                    Object helpID = action.getValue("helpID");
                    if (helpID != null && helpID instanceof String) {
                        item.putClientProperty("HelpID", helpID);
                    }
                }
            }
            if (item != null) {
                menu.add(item);
            }
        }
    }

    public static class GenerateFoldPopupAction
    extends BaseAction {
        private boolean addSeparatorBeforeNextAction;

        public GenerateFoldPopupAction() {
            super("generate-fold-popup");
            this.putValue("no-keybinding", (Object)Boolean.TRUE);
        }

        protected Class getShortDescriptionBundleClass() {
            return NbEditorKit.class;
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }

        private void addAcceleretors(Action a, JMenuItem item, JTextComponent target) {
            Keymap km;
            KeyStroke[] keys;
            Keymap keymap = km = target == null ? BaseKit.getKit(BaseKit.class).getKeymap() : target.getKeymap();
            if (km != null && (keys = km.getKeyStrokesForAction(a)) != null && keys.length > 0) {
                boolean added = false;
                for (int i = 0; i < keys.length; ++i) {
                    if (keys[i].getKeyCode() != 106 && keys[i].getKeyCode() != 107) continue;
                    item.setMnemonic(keys[i].getKeyCode());
                    added = true;
                    break;
                }
                if (!added) {
                    item.setMnemonic(keys[0].getKeyCode());
                }
            }
        }

        protected String getItemText(JTextComponent target, String actionName, Action a) {
            String itemText;
            if (a instanceof BaseAction) {
                itemText = ((BaseAction)a).getPopupMenuText(target);
            } else {
                itemText = (String)a.getValue("popupText");
                if (itemText == null && (itemText = (String)a.getValue("menuText")) == null) {
                    itemText = actionName;
                }
            }
            return itemText;
        }

        protected void addAction(JTextComponent target, JMenu menu, String actionName) {
            FoldHierarchy h;
            BaseKit kit;
            Action a;
            if (this.addSeparatorBeforeNextAction) {
                this.addSeparatorBeforeNextAction = false;
                menu.addSeparator();
            }
            BaseKit baseKit = kit = target == null ? BaseKit.getKit(BaseKit.class) : Utilities.getKit((JTextComponent)target);
            if (!(kit instanceof BaseKit)) {
                kit = BaseKit.getKit(BaseKit.class);
                target = null;
            }
            if (kit == null) {
                return;
            }
            boolean foldingEnabled = false;
            if (target != null && (h = FoldHierarchy.get((JTextComponent)target)).isActive()) {
                Preferences prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)target)).lookup(Preferences.class);
                foldingEnabled = prefs.getBoolean("code-folding-enable", true);
            }
            if ((a = kit.getActionByName(actionName)) != null) {
                String itemText;
                JMenuItem item = null;
                if (a instanceof Presenter.Menu) {
                    item = ((Presenter.Menu)a).getMenuPresenter();
                } else if (a instanceof BaseAction) {
                    item = ((BaseAction)a).getPopupMenuItem(target);
                }
                if (item == null && (itemText = this.getItemText(target, actionName, a)) != null) {
                    item = new JMenuItem(itemText);
                    item.addActionListener(a);
                    Mnemonics.setLocalizedText((AbstractButton)item, (String)itemText);
                    this.addAcceleretors(a, item, target);
                    Object helpID = a.getValue("helpID");
                    if (helpID != null && helpID instanceof String) {
                        item.putClientProperty("HelpID", helpID);
                    }
                }
                if (item != null) {
                    item.setEnabled(a.isEnabled() && foldingEnabled);
                    menu.add(item);
                }
            } else if (actionName == null) {
                menu.addSeparator();
            }
        }

        protected void setAddSeparatorBeforeNextAction(boolean addSeparator) {
            this.addSeparatorBeforeNextAction = addSeparator;
        }

        protected void addAdditionalItems(JTextComponent target, JMenu menu) {
            this.setAddSeparatorBeforeNextAction(false);
        }

        public JMenuItem getPopupMenuItem(JTextComponent target) {
            String menuText = NbBundle.getBundle(NbEditorKit.class).getString("Menu/View/CodeFolds");
            JMenu menu = new JMenu(menuText);
            Mnemonics.setLocalizedText((AbstractButton)menu, (String)menuText);
            this.setAddSeparatorBeforeNextAction(false);
            this.addAction(target, menu, "collapse-fold");
            this.addAction(target, menu, "expand-fold");
            this.setAddSeparatorBeforeNextAction(true);
            this.addAction(target, menu, "collapse-all-folds");
            this.addAction(target, menu, "expand-all-folds");
            this.addAction(target, menu, "collapse-fold-tree");
            this.addAction(target, menu, "expand-fold-tree");
            this.setAddSeparatorBeforeNextAction(true);
            if (target != null) {
                this.addAdditionalItems(target, menu);
            }
            return menu;
        }
    }

    public static class NbBuildToolTipAction
    extends ExtKit.BuildToolTipAction {
        public NbBuildToolTipAction() {
        }

        public NbBuildToolTipAction(Map attrs) {
            super(attrs);
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                NbToolTip.buildToolTip(target);
            }
        }
    }

    public static class NbGenerateGoToPopupAction
    extends BaseAction {
        public NbGenerateGoToPopupAction() {
            super("generate-goto-popup");
            this.putValue("no-keybinding", (Object)Boolean.TRUE);
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }

        protected Class getShortDescriptionBundleClass() {
            return NbEditorKit.class;
        }
    }

    public static final class NbToggleLineNumbersAction
    extends ActionFactory.ToggleLineNumbersAction {
        protected boolean isLineNumbersVisible() {
            Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
            return prefs.getBoolean("line-number-visible", true);
        }

        protected void toggleLineNumbers() {
            Preferences prefs;
            boolean visible = (prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class)).getBoolean("line-number-visible", true);
            prefs.putBoolean("line-number-visible", !visible);
        }
    }

    public static class NbRedoAction
    extends ActionFactory.RedoAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            Document doc = target.getDocument();
            if (doc.getProperty("undo-manager") != null || doc.getProperty(UndoManager.class) != null) {
                super.actionPerformed(evt, target);
            } else {
                RedoAction ra = (RedoAction)SystemAction.get(RedoAction.class);
                if (ra != null && ra.isEnabled()) {
                    ra.actionPerformed(evt);
                }
            }
        }
    }

    public static class NbUndoAction
    extends ActionFactory.UndoAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            Document doc = target.getDocument();
            if (doc.getProperty("undo-manager") != null || doc.getProperty(UndoManager.class) != null) {
                super.actionPerformed(evt, target);
            } else {
                UndoAction ua = (UndoAction)SystemAction.get(UndoAction.class);
                if (ua != null && ua.isEnabled()) {
                    ua.actionPerformed(evt);
                }
            }
        }
    }

    public class NbStopMacroRecordingAction
    extends ActionFactory.StopMacroRecordingAction {
        protected MacroDialogSupport getMacroDialogSupport(Class kitClass) {
            return super.getMacroDialogSupport(kitClass);
        }
    }

    public static class NbBuildPopupMenuAction
    extends ExtKit.BuildPopupMenuAction {
        static final long serialVersionUID = -8623762627678464181L;

        public NbBuildPopupMenuAction() {
        }

        public NbBuildPopupMenuAction(Map attrs) {
            super(attrs);
        }

        protected JPopupMenu createPopupMenu(JTextComponent component) {
            return new JPopupMenuPlus();
        }

        protected JPopupMenu buildPopupMenu(JTextComponent component) {
            EditorUI ui = Utilities.getEditorUI((JTextComponent)component);
            if (!ui.hasExtComponent()) {
                return null;
            }
            JPopupMenu pm = this.createPopupMenu(component);
            String mimeType = NbEditorUtilities.getMimeType(component);
            ArrayList<String> l = PopupMenuActionsProvider.getPopupMenuItems(mimeType);
            if (l.isEmpty()) {
                String settingName = ui == null || ui.hasExtComponent() ? "popup-menu-action-name-list" : "dialog-popup-menu-action-name-list";
                Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                String actionNames = prefs.get(settingName, null);
                if (actionNames != null) {
                    l = new ArrayList<String>();
                    StringTokenizer t = new StringTokenizer(actionNames, ",");
                    while (t.hasMoreTokens()) {
                        String action = t.nextToken().trim();
                        l.add(action);
                    }
                }
            }
            if (l != null) {
                for (Object obj : l) {
                    if (obj == null || obj instanceof JSeparator) {
                        this.addAction(component, pm, (String)null);
                        continue;
                    }
                    if (obj instanceof String) {
                        this.addAction(component, pm, (String)obj);
                        continue;
                    }
                    if (obj instanceof Action) {
                        this.addAction(component, pm, (Action)obj);
                        continue;
                    }
                    if (!(obj instanceof DataFolder)) continue;
                    pm.add(new LayerSubFolderMenu(component, ((DataFolder)obj).getPrimaryFile()));
                }
            }
            return pm;
        }

        protected void addAction(JTextComponent component, JPopupMenu popupMenu, Action action) {
            Lookup contextLookup = NbEditorKit.getContextLookup(component);
            EditorKit kit = component.getUI().getEditorKit(component);
            if (contextLookup == null && kit instanceof NbEditorKit && ((NbEditorKit)((Object)kit)).systemAction2editorAction.containsKey(action.getClass().getName())) {
                this.addAction(component, popupMenu, (String)((NbEditorKit)((Object)kit)).systemAction2editorAction.get(action.getClass().getName()));
                return;
            }
            if ((action = NbEditorKit.translateContextLookupAction(contextLookup, action)) != null) {
                JMenuItem item = NbEditorKit.createLocalizedMenuItem(action);
                if (item instanceof DynamicMenuContent) {
                    JComponent[] cmps = ((DynamicMenuContent)item).getMenuPresenters();
                    for (int i = 0; i < cmps.length; ++i) {
                        if (cmps[i] != null) {
                            popupMenu.add(cmps[i]);
                            continue;
                        }
                        popupMenu.addSeparator();
                    }
                } else {
                    if (Boolean.TRUE.equals(action.getValue("hideWhenDisabled")) && !action.isEnabled()) {
                        return;
                    }
                    if (item == null) {
                        LOG.log(Level.WARNING, "Null menu item produced by action {0}.", action);
                    } else {
                        item.setEnabled(action.isEnabled());
                        Object helpID = action.getValue("helpID");
                        if (helpID != null && helpID instanceof String) {
                            item.putClientProperty("HelpID", helpID);
                        }
                        NbEditorKit.assignAccelerator(component.getKeymap(), action, item);
                        this.debugPopupMenuItem(item, action);
                        popupMenu.add(item);
                    }
                }
            }
        }

        private void addTopComponentActions(JTextComponent component, JPopupMenu popupMenu) {
            Lookup contextLookup = NbEditorKit.getContextLookup(component);
            TopComponent tc = NbEditorUtilities.getOuterTopComponent(component);
            if (tc != null) {
                Action[] actions = tc.getActions();
                Component[] comps = org.openide.util.Utilities.actionsToPopup((Action[])actions, (Lookup)contextLookup).getComponents();
                for (int i = 0; i < comps.length; ++i) {
                    popupMenu.add(comps[i]);
                }
            }
        }

        protected void addAction(JTextComponent component, JPopupMenu popupMenu, String actionName) {
            if (actionName != null) {
                if (TopComponent.class.getName().equals(actionName)) {
                    this.addTopComponentActions(component, popupMenu);
                    return;
                }
                Class saClass = null;
                try {
                    ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    saClass = Class.forName(actionName, false, loader);
                }
                catch (Throwable t) {
                    // empty catch block
                }
                if (saClass != null && SystemAction.class.isAssignableFrom(saClass)) {
                    JMenuItem item;
                    Lookup contextLookup = NbEditorKit.getContextLookup(component);
                    Object action = SystemAction.get(saClass);
                    JMenuItem jMenuItem = item = (action = NbEditorKit.translateContextLookupAction(contextLookup, (Action)action)) != null ? NbEditorKit.createLocalizedMenuItem((Action)action) : null;
                    if (item != null) {
                        if (item instanceof DynamicMenuContent) {
                            JComponent[] cmps = ((DynamicMenuContent)item).getMenuPresenters();
                            for (int i = 0; i < cmps.length; ++i) {
                                if (cmps[i] != null) {
                                    popupMenu.add(cmps[i]);
                                    continue;
                                }
                                popupMenu.addSeparator();
                            }
                        } else {
                            if (!(item instanceof JMenu)) {
                                if (Boolean.TRUE.equals(action.getValue("hideWhenDisabled")) && !action.isEnabled()) {
                                    return;
                                }
                                NbEditorKit.assignAccelerator((Keymap)Lookup.getDefault().lookup(Keymap.class), (Action)action, item);
                            }
                            this.debugPopupMenuItem(item, (Action)action);
                            popupMenu.add(item);
                        }
                    }
                    return;
                }
            }
            super.addAction(component, popupMenu, actionName);
        }
    }

    static final class SubFolderData {
        private final NbEditorKit editorKit;
        private final String localizedName;
        List objects;
        int[] instructions;
        int index;

        private static List<FileObject> sort(FileObject[] children) {
            List fos = Arrays.asList(children);
            fos = FileUtil.getOrder(fos, (boolean)true);
            return fos;
        }

        private static String getLocalizedName(FileObject f) {
            Object displayName = f.getAttribute("displayName");
            if (displayName instanceof String) {
                return (String)displayName;
            }
            try {
                return f.getFileSystem().getStatus().annotateName(f.getNameExt(), Collections.singleton(f));
            }
            catch (FileStateInvalidException e) {
                return f.getNameExt();
            }
        }

        private SubFolderData(NbEditorKit editorKit, FileObject folder) {
            this.editorKit = editorKit;
            this.localizedName = SubFolderData.getLocalizedName(folder);
            List<Object> items = ActionsList.convert(SubFolderData.sort(folder.getChildren()), false);
            this.objects = new ArrayList(items.size());
            this.instructions = new int[items.size()];
            for (Object obj : items) {
                if (obj == null || obj instanceof JSeparator) {
                    this.objects.add(null);
                    this.instructions[this.index++] = 11;
                    continue;
                }
                if (obj instanceof String) {
                    this.initActionByName((String)obj);
                    continue;
                }
                if (obj instanceof Action) {
                    this.objects.add(obj);
                    this.instructions[this.index++] = 4;
                    continue;
                }
                if (!(obj instanceof DataFolder)) continue;
                this.objects.add(new SubFolderData(editorKit, ((DataFolder)obj).getPrimaryFile()));
                this.instructions[this.index++] = 5;
            }
        }

        private void initActionByName(String actionName) {
            if (this.editorKit == null) {
                return;
            }
            Action a = this.editorKit.getActionByName(actionName);
            if (a != null) {
                this.objects.add(a);
                this.instructions[this.index++] = 3;
            }
        }
    }

    static class PopupInitializer
    implements Runnable {
        private final Lookup contextLookup;
        private final NbEditorKit editorKit;
        private final TopComponent outerTopComponent;
        private final String mimeType;
        private final String settingName;
        private List<Object> actionObjects;
        private List<Object> initedObjects;
        private int[] instructions;
        int index;

        public PopupInitializer(Lookup contextLookup, NbEditorKit editorKit, TopComponent outerTopComponent, String mimeType, String settingName) {
            this.contextLookup = contextLookup;
            this.editorKit = editorKit;
            this.outerTopComponent = outerTopComponent;
            this.mimeType = mimeType;
            this.settingName = settingName;
        }

        @Override
        public void run() {
            String actionNames;
            Preferences prefs;
            ArrayList<String> l = PopupMenuActionsProvider.getPopupMenuItems(this.mimeType);
            if (l.isEmpty() && (actionNames = (prefs = (Preferences)MimeLookup.getLookup((String)this.mimeType).lookup(Preferences.class)).get(this.settingName, null)) != null) {
                l = new ArrayList<String>();
                StringTokenizer t = new StringTokenizer(actionNames, ",");
                while (t.hasMoreTokens()) {
                    String action = t.nextToken().trim();
                    l.add(action);
                }
            }
            this.initedObjects = new ArrayList<Object>(l.size());
            this.instructions = new int[l.size()];
            for (Object obj : l) {
                if (obj == null || obj instanceof JSeparator) {
                    this.initActionByName(null);
                    continue;
                }
                if (obj instanceof String) {
                    this.initActionByName((String)obj);
                    continue;
                }
                if (obj instanceof Action) {
                    this.initActionInstance((Action)obj);
                    continue;
                }
                if (!(obj instanceof DataFolder)) continue;
                this.addInitedObject(new SubFolderData(this.editorKit, ((DataFolder)obj).getPrimaryFile()), 5);
            }
        }

        private void addInitedObject(Object inited, int instr) {
            this.initedObjects.add(inited);
            this.instructions[this.index++] = instr;
        }

        protected void initActionByName(String actionName) {
            if (actionName != null) {
                if (TopComponent.class.getName().equals(actionName)) {
                    Action[] actions = this.outerTopComponent.getActions();
                    this.addInitedObject(actions, 1);
                    return;
                }
                Class saClass = null;
                try {
                    ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                    saClass = Class.forName(actionName, false, loader);
                }
                catch (Throwable t) {
                    // empty catch block
                }
                if (saClass != null && SystemAction.class.isAssignableFrom(saClass)) {
                    Object action = SystemAction.get(saClass);
                    if ((action = NbEditorKit.translateContextLookupAction(this.contextLookup, (Action)action)) != null) {
                        this.addInitedObject(action, 2);
                    }
                    return;
                }
            }
            Action a = this.editorKit == null ? null : this.editorKit.getActionByName(actionName);
            this.addInitedObject(actionName, 3);
        }

        protected void initActionInstance(Action action) {
            if (this.contextLookup == null && this.editorKit instanceof NbEditorKit && this.editorKit.systemAction2editorAction.containsKey(action.getClass().getName())) {
                this.initActionByName((String)this.editorKit.systemAction2editorAction.get(action.getClass().getName()));
                return;
            }
            Action naction = NbEditorKit.translateContextLookupAction(this.contextLookup, action);
            if (naction != null) {
                this.addInitedObject(naction, 4);
            }
        }
    }

    public static class ToggleToolbarAction
    extends BaseAction {
        public ToggleToolbarAction() {
            super("toggle-toolbar");
            this.putValue("helpID", (Object)ToggleToolbarAction.class.getName());
        }

        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            Preferences prefs;
            boolean toolbarVisible = (prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class)).getBoolean("toolbarVisible", true);
            prefs.putBoolean("toolbarVisible", !toolbarVisible);
        }

        public JMenuItem getPopupMenuItem(JTextComponent target) {
            Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
            boolean toolbarVisible = prefs.getBoolean("toolbarVisible", true);
            JCheckBoxMenuItem item = new JCheckBoxMenuItem(NbBundle.getBundle(ToggleToolbarAction.class).getString("PROP_base_toolbarVisible"), toolbarVisible);
            item.addItemListener(new ItemListener(){

                @Override
                public void itemStateChanged(ItemEvent e) {
                    ToggleToolbarAction.this.actionPerformed(null, null);
                }
            });
            return item;
        }

        protected Class getShortDescriptionBundleClass() {
            return BaseKit.class;
        }

    }

}

