/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.Annotations
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseDocument$LazyPropertyMap
 *  org.netbeans.editor.BaseDocument$PropertyEvaluator
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.PrintContainer
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.netbeans.modules.editor.lib.BaseDocument_PropertyHandler
 *  org.netbeans.modules.editor.lib2.view.PrintUtils
 *  org.openide.text.Annotation
 *  org.openide.text.AttributedCharacters
 *  org.openide.text.NbDocument
 *  org.openide.text.NbDocument$Annotatable
 *  org.openide.text.NbDocument$CustomEditor
 *  org.openide.text.NbDocument$CustomToolbar
 *  org.openide.text.NbDocument$PositionBiasable
 *  org.openide.text.NbDocument$Printable
 *  org.openide.text.NbDocument$WriteLockable
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JToolBar;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.PrintContainer;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.impl.ComplexValueSettingsFactory;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.netbeans.modules.editor.lib.BaseDocument_PropertyHandler;
import org.netbeans.modules.editor.lib2.view.PrintUtils;
import org.openide.text.Annotation;
import org.openide.text.AttributedCharacters;
import org.openide.text.NbDocument;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.Lookups;

public class NbEditorDocument
extends GuardedDocument
implements NbDocument.PositionBiasable,
NbDocument.WriteLockable,
NbDocument.Printable,
NbDocument.CustomEditor,
NbDocument.CustomToolbar,
NbDocument.Annotatable {
    public static final String INDENT_ENGINE = "indentEngine";
    private static final RequestProcessor RP = new RequestProcessor("NbEditorDocument", 1, false, false);
    private final HashMap annoMap = new HashMap(20);

    public NbEditorDocument(Class kitClass) {
        super(kitClass);
        this.init();
    }

    public NbEditorDocument(String mimeType) {
        super(mimeType);
        this.init();
    }

    private void init() {
        this.setNormalStyleName("NbNormalStyle");
        this.putProperty((Object)"indentEngine", (Object)new BaseDocument.PropertyEvaluator(){

            public Object getValue() {
                MimePath mimePath = MimePath.parse((String)((String)NbEditorDocument.this.getProperty((Object)"mimeType")));
                return ComplexValueSettingsFactory.getIndentEngine(mimePath);
            }
        });
        this.putProperty((Object)"text-line-wrap", (Object)new BaseDocument_PropertyHandler(){

            public Object getValue() {
                return CodeStylePreferences.get((Document)((Object)NbEditorDocument.this)).getPreferences().get("text-line-wrap", "none");
            }

            public Object setValue(Object value) {
                return null;
            }
        });
        this.putProperty((Object)"text-limit-width", (Object)new BaseDocument_PropertyHandler(){

            public Object getValue() {
                return CodeStylePreferences.get((Document)((Object)NbEditorDocument.this)).getPreferences().getInt("text-limit-width", 80);
            }

            public Object setValue(Object value) {
                return null;
            }
        });
        this.putProperty((Object)"Issue-222763-debug", (Object)new Exception());
    }

    public int getShiftWidth() {
        return IndentUtils.indentLevelSize((Document)((Object)this));
    }

    public int getTabSize() {
        return IndentUtils.tabSize((Document)((Object)this));
    }

    public void setCharacterAttributes(int offset, int length, AttributeSet s, boolean replace) {
        if (s != null) {
            Object val = s.getAttribute(NbDocument.GUARDED);
            if (val != null && val instanceof Boolean) {
                if (((Boolean)val).booleanValue()) {
                    super.setCharacterAttributes(offset, length, (AttributeSet)guardedSet, replace);
                } else {
                    super.setCharacterAttributes(offset, length, (AttributeSet)unguardedSet, replace);
                }
            } else {
                super.setCharacterAttributes(offset, length, s, replace);
            }
        }
    }

    public AttributedCharacterIterator[] createPrintIterators() {
        List lineList = PrintUtils.printDocument((Document)((Object)this), (boolean)true, (int)0, (int)(this.getLength() + 1));
        AttributedCharacterIterator[] lines = new AttributedCharacterIterator[lineList.size()];
        lineList.toArray(lines);
        return lines;
    }

    public Component createEditor(JEditorPane j) {
        EditorUI editorUI = Utilities.getEditorUI((JTextComponent)j);
        if (editorUI == null) {
            TextUI ui = j.getUI();
            EditorKit kit = j.getEditorKit();
            throw new IllegalStateException("NbEditorDocument.createEditor(): ui=" + ui + ", kit=" + kit + ", pane=" + j);
        }
        return editorUI.getExtComponent();
    }

    public JToolBar createToolbar(JEditorPane j) {
        EditorUI ui = Utilities.getEditorUI((JTextComponent)j);
        return ui != null ? ui.getToolBarComponent() : null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addAnnotation(Position startPos, int length, Annotation annotation) {
        this.readLock();
        try {
            int docLen = this.getLength();
            int offset = startPos.getOffset();
            assert (offset >= 0);
            if (offset > docLen) {
                try {
                    startPos = this.createPosition(offset);
                }
                catch (BadLocationException e) {
                    throw new IllegalStateException("Cannot create position at offset=" + offset + ", docLen=" + docLen, e);
                }
            }
            HashMap hashMap = this.annoMap;
            synchronized (hashMap) {
                AnnotationDescDelegate a = (AnnotationDescDelegate)((Object)this.annoMap.get((Object)annotation));
                if (a != null) {
                    throw new IllegalStateException("Annotation already added: " + (Object)((Object)a));
                }
                if (annotation.getAnnotationType() != null) {
                    a = new AnnotationDescDelegate((BaseDocument)this, startPos, length, annotation);
                    this.annoMap.put(annotation, a);
                    this.getAnnotations().addAnnotation((AnnotationDesc)a);
                }
            }
        }
        finally {
            this.readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeAnnotation(Annotation annotation) {
        block8 : {
            if (annotation == null) {
                throw new IllegalStateException("Trying to remove null annotation.");
            }
            this.readLock();
            try {
                AnnotationDescDelegate a;
                if (annotation.getAnnotationType() == null) break block8;
                HashMap hashMap = this.annoMap;
                synchronized (hashMap) {
                    a = (AnnotationDescDelegate)((Object)this.annoMap.remove((Object)annotation));
                    if (a == null) {
                        throw new IllegalStateException("Annotation not added: " + annotation.getAnnotationType() + annotation.getShortDescription());
                    }
                }
                a.detachListeners();
                this.getAnnotations().removeAnnotation((AnnotationDesc)a);
            }
            finally {
                this.readUnlock();
            }
        }
    }

    Map getAnnoMap() {
        return this.annoMap;
    }

    protected Dictionary createDocumentProperties(Dictionary origDocumentProperties) {
        return new BaseDocument.LazyPropertyMap(origDocumentProperties){

            public Object put(Object key, Object value) {
                Object origValue = super.put(key, value);
                if ("stream".equals(key)) {
                    assert (value != null);
                    if (origValue == null) {
                        RP.post(new Runnable(){

                            @Override
                            public void run() {
                                IndentUtils.indentLevelSize((Document)((Object)NbEditorDocument.this));
                            }
                        });
                    } else assert (origValue.equals(value));
                }
                return origValue;
            }

        };
    }

    class NbPrintContainer
    extends AttributedCharacters
    implements PrintContainer {
        ArrayList acl;
        AttributedCharacters a;

        NbPrintContainer() {
            this.acl = new ArrayList();
            this.a = new AttributedCharacters();
        }

        public void add(char[] chars, Font font, Color foreColor, Color backColor) {
            this.a.append(chars, font, foreColor);
        }

        public void eol() {
            this.acl.add(this.a);
            this.a = new AttributedCharacters();
        }

        public boolean initEmptyLines() {
            return true;
        }

        public AttributedCharacterIterator[] getIterators() {
            int cnt = this.acl.size();
            AttributedCharacterIterator[] acis = new AttributedCharacterIterator[cnt];
            for (int i = 0; i < cnt; ++i) {
                AttributedCharacters ac = (AttributedCharacters)this.acl.get(i);
                acis[i] = ac.iterator();
            }
            return acis;
        }
    }

    static class AnnotationDescDelegate
    extends AnnotationDesc
    implements Lookup.Provider {
        private Annotation delegate;
        private PropertyChangeListener l;
        private Position pos;
        private BaseDocument doc;
        private int lastKnownOffset = -1;
        private int lastKnownLine = -1;

        AnnotationDescDelegate(BaseDocument doc, Position pos, int length, Annotation anno) {
            super(pos.getOffset(), length);
            this.pos = pos;
            this.delegate = anno;
            this.doc = doc;
            this.updateAnnotationType();
            this.l = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName() == null || "shortDescription".equals(evt.getPropertyName())) {
                        AnnotationDescDelegate.this.firePropertyChange("shortDescription", null, null);
                    }
                    if (evt.getPropertyName() == null || "moveToFront".equals(evt.getPropertyName())) {
                        AnnotationDescDelegate.this.firePropertyChange("moveToFront", null, null);
                    }
                    if (evt.getPropertyName() == null || "annotationType".equals(evt.getPropertyName())) {
                        AnnotationDescDelegate.this.updateAnnotationType();
                        AnnotationDescDelegate.this.firePropertyChange("annotationType", null, null);
                    }
                }
            };
            this.delegate.addPropertyChangeListener(this.l);
        }

        public String getAnnotationType() {
            return this.delegate.getAnnotationType();
        }

        public String getShortDescription() {
            return this.delegate.getShortDescription();
        }

        void detachListeners() {
            this.delegate.removePropertyChangeListener(this.l);
        }

        public int getOffset() {
            return this.pos.getOffset();
        }

        public int getLine() {
            int offset = this.pos.getOffset();
            if (this.lastKnownOffset != -1 && this.lastKnownLine != -1 && this.lastKnownOffset == offset) {
                return this.lastKnownLine;
            }
            try {
                this.lastKnownLine = Utilities.getLineOffset((BaseDocument)this.doc, (int)offset);
                this.lastKnownOffset = offset;
            }
            catch (BadLocationException e) {
                this.lastKnownOffset = -1;
                this.lastKnownLine = 0;
            }
            return this.lastKnownLine;
        }

        public Lookup getLookup() {
            return Lookups.singleton((Object)this.delegate);
        }

    }

}

