/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 */
package org.netbeans.modules.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorKit;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileUtil;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;

public abstract class MainMenuAction
implements Presenter.Menu,
ChangeListener,
LookupListener {
    public static final Icon BLANK_ICON = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/empty.gif", (boolean)false);
    public boolean menuInitialized = false;
    private final Icon forcedIcon;
    private final boolean forceIcon;
    private Lookup.Result<KeyBindingSettings> kbs = null;
    private Lookup.Result<ActionMap> globalActionMap = null;
    private JMenuItem menuPresenter = null;
    private static final RequestProcessor RP = new RequestProcessor(MainMenuAction.class.getName(), 1, false, false);
    private static boolean IS_SET_POST_SET_MENU_LISTENER = false;

    public MainMenuAction() {
        this(true, null);
    }

    public MainMenuAction(boolean forceIcon, Icon forcedIcon) {
        this.forceIcon = forceIcon;
        this.forcedIcon = forcedIcon;
    }

    public void resultChanged(LookupEvent ev) {
        this.postSetMenu();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.postSetMenu();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public String getName() {
        return this.getMenuItemText();
    }

    private static JTextComponent getComponent() {
        return Utilities.getFocusedComponent();
    }

    protected static Action getActionByName(String actionName) {
        BaseKit bKit = MainMenuAction.getKit();
        if (bKit != null) {
            Action action = bKit.getActionByName(actionName);
            return action;
        }
        return null;
    }

    protected static void addAccelerators(Action a, JMenuItem item, JTextComponent target) {
        Keymap km;
        if (target == null || a == null || item == null) {
            return;
        }
        Action kitAction = MainMenuAction.getActionByName((String)a.getValue("Name"));
        if (kitAction != null) {
            a = kitAction;
        }
        if ((km = target.getKeymap()) != null) {
            KeyStroke[] keys = km.getKeyStrokesForAction(a);
            KeyStroke itemAccelerator = item.getAccelerator();
            if (keys != null && keys.length > 0) {
                if (itemAccelerator == null || !itemAccelerator.equals(keys[0])) {
                    item.setAccelerator(keys[0]);
                }
            } else if (itemAccelerator != null && kitAction != null) {
                item.setAccelerator(null);
            }
        }
    }

    private static BaseKit getKit() {
        JTextComponent component = MainMenuAction.getComponent();
        return component == null ? null : Utilities.getKit((JTextComponent)component);
    }

    public boolean isEnabled() {
        return false;
    }

    protected Action getGlobalKitAction() {
        return null;
    }

    protected final ActionMap getContextActionMap() {
        Collection am;
        if (this.globalActionMap == null) {
            this.globalActionMap = org.openide.util.Utilities.actionsGlobalContext().lookupResult(ActionMap.class);
            this.globalActionMap.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.globalActionMap));
        }
        return (am = this.globalActionMap.allInstances()).size() > 0 ? (ActionMap)am.iterator().next() : null;
    }

    protected final void postSetMenu() {
        Utilities.runInEventDispatchThread((Runnable)new Runnable(){

            @Override
            public void run() {
                MainMenuAction.this.setMenu();
            }
        });
    }

    protected void setMenu() {
        if (!IS_SET_POST_SET_MENU_LISTENER) {
            EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    MainMenuAction.this.postSetMenu();
                }
            });
            IS_SET_POST_SET_MENU_LISTENER = true;
        }
        if (this.kbs == null) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    MainMenuAction.this.kbs = MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookupResult(KeyBindingSettings.class);
                    MainMenuAction.this.kbs.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)MainMenuAction.this, (Object)MainMenuAction.this.kbs));
                    MainMenuAction.this.kbs.allInstances();
                }
            });
        }
        ActionMap am = this.getContextActionMap();
        Action action = null;
        if (am != null) {
            action = am.get(this.getActionName());
        }
        if (action == null) {
            action = this.getGlobalKitAction();
        }
        JMenuItem presenter = this.getMenuPresenter();
        assert (presenter != null);
        Action presenterAction = presenter.getAction();
        if (presenterAction == null) {
            if (action != null) {
                presenter.setAction(action);
                presenter.setToolTipText(null);
                this.menuInitialized = false;
            }
        } else if (action != null && !action.equals(presenterAction)) {
            presenter.setAction(action);
            presenter.setToolTipText(null);
            this.menuInitialized = false;
        }
        if (!this.menuInitialized) {
            Mnemonics.setLocalizedText((AbstractButton)presenter, (String)this.getMenuItemText());
            this.menuInitialized = true;
        }
        presenter.setEnabled(action != null);
        JTextComponent comp = Utilities.getFocusedComponent();
        if (comp != null && comp instanceof JEditorPane) {
            MainMenuAction.addAccelerators(action, presenter, comp);
        } else {
            presenter.setAccelerator(this.getDefaultAccelerator());
        }
        if (this.forceIcon) {
            presenter.setIcon(this.forcedIcon);
        }
    }

    protected abstract String getMenuItemText();

    protected abstract String getActionName();

    public JMenuItem getMenuPresenter() {
        if (this.menuPresenter == null) {
            this.menuPresenter = new JMenuItem(){

                @Override
                public void setText(String text) {
                    if (this.getText() == null || this.getText().length() == 0) {
                        super.setText(text);
                    }
                }
            };
            Mnemonics.setLocalizedText((AbstractButton)this.menuPresenter, (String)this.getMenuItemText());
        }
        return this.menuPresenter;
    }

    protected KeyStroke getDefaultAccelerator() {
        return null;
    }

    public static final class PasteFormattedAction
    extends MainMenuAction {
        public PasteFormattedAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(PasteFormattedAction.class).getString("paste_formatted_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "paste-formated";
        }
    }

    public static final class RemoveTrailingSpacesAction
    extends MainMenuAction {
        public RemoveTrailingSpacesAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(RemoveTrailingSpacesAction.class).getString("remove_trailing_spaces_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "remove-trailing-spaces";
        }
    }

    public static final class StopMacroRecordingAction
    extends MainMenuAction {
        public StopMacroRecordingAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(StopMacroRecordingAction.class).getString("stop_macro_recording_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "stop-macro-recording";
        }
    }

    public static final class SelectIdentifierAction
    extends MainMenuAction {
        public SelectIdentifierAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(SelectIdentifierAction.class).getString("select_identifier_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "select-identifier";
        }
    }

    public static final class SelectAllAction
    extends MainMenuAction {
        public SelectAllAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(SelectAllAction.class).getString("select_all_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "select-all";
        }
    }

    public static final class StartMacroRecordingAction
    extends MainMenuAction {
        public StartMacroRecordingAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(StartMacroRecordingAction.class).getString("start_macro_recording_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "start-macro-recording";
        }
    }

    public static final class FindSelectionAction
    extends MainMenuAction {
        public FindSelectionAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(FindNextAction.class).getString("find_selection_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "find-selection";
        }

        @Override
        public boolean isEnabled() {
            JTextComponent focused = EditorRegistry.focusedComponent();
            return focused != null;
        }
    }

    public static final class FindPreviousAction
    extends MainMenuAction {
        public FindPreviousAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(FindNextAction.class).getString("find_previous_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "find-previous";
        }
    }

    public static final class FindNextAction
    extends MainMenuAction {
        public FindNextAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(FindNextAction.class).getString("find_next_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "find-next";
        }
    }

    public static final class WordMatchPrevAction
    extends MainMenuAction {
        public WordMatchPrevAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(WordMatchPrevAction.class).getString("word_match_previous_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "word-match-prev";
        }
    }

    public static final class WordMatchNextAction
    extends MainMenuAction {
        public WordMatchNextAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(WordMatchNextAction.class).getString("word_match_next_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "word-match-next";
        }
    }

    public static final class ToggleCommentAction
    extends MainMenuAction {
        public ToggleCommentAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(ToggleCommentAction.class).getString("toggle_comment_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "toggle-comment";
        }
    }

    public static final class UncommentAction
    extends MainMenuAction {
        public UncommentAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(UncommentAction.class).getString("uncomment_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "uncomment";
        }
    }

    public static final class CommentAction
    extends MainMenuAction {
        public CommentAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(CommentAction.class).getString("comment_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "comment";
        }
    }

    public static final class ShiftLineRightAction
    extends MainMenuAction {
        public ShiftLineRightAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(ShiftLineRightAction.class).getString("shift_line_right_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "shift-line-right";
        }
    }

    public static final class ShiftLineLeftAction
    extends MainMenuAction {
        public ShiftLineLeftAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(ShiftLineLeftAction.class).getString("shift_line_left_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "shift-line-left";
        }
    }

    public static final class FormatAction
    extends MainMenuAction {
        public FormatAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(FormatAction.class).getString("format_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "format";
        }

        @Override
        protected Action getGlobalKitAction() {
            return (Action)FileUtil.getConfigObject((String)"Editors/private/GlobalFormatAction.instance", Action.class);
        }

        @Override
        protected KeyStroke getDefaultAccelerator() {
            List lst;
            Lookup ml = MimeLookup.getLookup((MimePath)MimePath.EMPTY);
            KeyBindingSettings kbs = (KeyBindingSettings)ml.lookup(KeyBindingSettings.class);
            if (kbs != null && (lst = kbs.getKeyBindings()) != null) {
                for (int i = 0; i < lst.size(); ++i) {
                    MultiKeyBinding mkb = (MultiKeyBinding)lst.get(i);
                    String an = mkb.getActionName();
                    if (an == null || !an.equals(this.getActionName()) || mkb.getKeyStrokeCount() != 1) continue;
                    return mkb.getKeyStroke(0);
                }
            }
            return null;
        }
    }

    public static final class JumpForwardAction
    extends MainMenuAction {
        public JumpForwardAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(JumpForwardAction.class).getString("jump_forward_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "jump-list-next";
        }
    }

    public static final class JumpBackAction
    extends MainMenuAction {
        public JumpBackAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(JumpBackAction.class).getString("jump_back_main_menu_item");
        }

        @Override
        protected String getActionName() {
            return "jump-list-prev";
        }
    }

    public static class GoToDeclarationAction
    extends MainMenuAction {
        public GoToDeclarationAction() {
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(MainMenuAction.class).getString("goto_declaration_main_menu_edit_item");
        }

        @Override
        protected String getActionName() {
            return "goto-declaration";
        }
    }

    public static class GoToSuperAction
    extends MainMenuAction {
        public GoToSuperAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(MainMenuAction.class).getString("goto_super_implementation_main_menu_edit_item");
        }

        @Override
        protected String getActionName() {
            return "goto-super-implementation";
        }
    }

    public static class GoToSourceAction
    extends MainMenuAction {
        public GoToSourceAction() {
            super(true, null);
            this.postSetMenu();
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(MainMenuAction.class).getString("goto_source_main_menu_edit_item");
        }

        @Override
        protected String getActionName() {
            return "goto-source";
        }
    }

    public static class ShowLineNumbersAction
    extends MainMenuAction {
        private static JCheckBoxMenuItem SHOW_LINE_MENU = null;
        private Action delegate = null;

        public ShowLineNumbersAction() {
            super(false, null);
        }

        @Override
        protected void setMenu() {
            super.setMenu();
            JTextComponent c = MainMenuAction.getComponent();
            MimePath mimePath = c == null ? MimePath.EMPTY : MimePath.parse((String)DocumentUtilities.getMimeType((JTextComponent)c));
            Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
            boolean visible = prefs.getBoolean("line-number-visible", true);
            SHOW_LINE_MENU.setState(visible);
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(MainMenuAction.class).getString("show_line_numbers_main_menu_view_item");
        }

        @Override
        public String getName() {
            return this.getMenuItemText();
        }

        @Override
        public JMenuItem getMenuPresenter() {
            if (SHOW_LINE_MENU == null) {
                SHOW_LINE_MENU = new JCheckBoxMenuItem(){

                    @Override
                    public void setText(String text) {
                        if (this.getText() == null || this.getText().length() == 0) {
                            super.setText(text);
                        }
                    }
                };
                Mnemonics.setLocalizedText((AbstractButton)SHOW_LINE_MENU, (String)this.getMenuItemText());
                this.setMenu();
            }
            return SHOW_LINE_MENU;
        }

        @Override
        protected String getActionName() {
            return "toggle-line-numbers";
        }

        @Override
        protected Action getGlobalKitAction() {
            if (this.delegate == null) {
                this.delegate = new NbEditorKit.NbToggleLineNumbersAction();
            }
            return this.delegate;
        }

    }

    public static class ShowToolBarAction
    extends MainMenuAction {
        private static JCheckBoxMenuItem SHOW_TOOLBAR_MENU = null;
        private Action delegate = null;

        public ShowToolBarAction() {
            super(false, null);
        }

        @Override
        protected void setMenu() {
            super.setMenu();
            JTextComponent c = MainMenuAction.getComponent();
            MimePath mimePath = c == null ? MimePath.EMPTY : MimePath.parse((String)DocumentUtilities.getMimeType((JTextComponent)c));
            Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
            boolean visible = prefs.getBoolean("toolbarVisible", true);
            SHOW_TOOLBAR_MENU.setState(visible);
        }

        @Override
        public JMenuItem getMenuPresenter() {
            if (SHOW_TOOLBAR_MENU == null) {
                SHOW_TOOLBAR_MENU = new JCheckBoxMenuItem(){

                    @Override
                    public void setText(String text) {
                        if (this.getText() == null || this.getText().length() == 0) {
                            super.setText(text);
                        }
                    }
                };
                Mnemonics.setLocalizedText((AbstractButton)SHOW_TOOLBAR_MENU, (String)this.getMenuItemText());
                this.setMenu();
            }
            return SHOW_TOOLBAR_MENU;
        }

        @Override
        protected String getMenuItemText() {
            return NbBundle.getBundle(MainMenuAction.class).getString("show_editor_toolbar_main_menu_view_item");
        }

        @Override
        protected String getActionName() {
            return "toggle-toolbar";
        }

        @Override
        protected Action getGlobalKitAction() {
            if (this.delegate == null) {
                this.delegate = new NbEditorKit.ToggleToolbarAction();
            }
            return this.delegate;
        }

    }

}

