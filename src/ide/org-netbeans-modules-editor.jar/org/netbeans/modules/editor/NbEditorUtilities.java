/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.ErrorManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.LineCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.Line
 *  org.openide.text.Line$Set
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.editor;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.ErrorManager;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public class NbEditorUtilities {
    public static DataObject getDataObject(Document doc) {
        Object sdp;
        Object object = sdp = doc == null ? null : doc.getProperty("stream");
        if (sdp instanceof DataObject) {
            return (DataObject)sdp;
        }
        return null;
    }

    public static boolean isDocumentActive(Document doc) {
        StyledDocument ecDoc;
        EditorCookie editorCookie;
        DataObject dob = NbEditorUtilities.getDataObject(doc);
        if (dob != null && (editorCookie = (EditorCookie)dob.getCookie(EditorCookie.class)) != null && (ecDoc = editorCookie.getDocument()) == doc) {
            return true;
        }
        return false;
    }

    public static FileObject getFileObject(Document doc) {
        Object sdp = doc.getProperty("stream");
        if (sdp instanceof FileObject) {
            return (FileObject)sdp;
        }
        if (sdp instanceof DataObject) {
            return ((DataObject)sdp).getPrimaryFile();
        }
        return null;
    }

    public static int[] getIdentifierAndMethodBlock(BaseDocument doc, int offset) throws BadLocationException {
        int[] funBlk;
        int[] idBlk = Utilities.getIdentifierBlock((BaseDocument)doc, (int)offset);
        if (idBlk != null && (funBlk = ((ExtSyntaxSupport)doc.getSyntaxSupport()).getFunctionBlock(idBlk)) != null) {
            return new int[]{idBlk[0], idBlk[1], funBlk[1]};
        }
        return idBlk;
    }

    public static Line getLine(BaseDocument doc, int offset, boolean original) {
        LineCookie lc;
        Line.Set lineSet;
        DataObject dob = NbEditorUtilities.getDataObject((Document)doc);
        if (dob != null && (lc = (LineCookie)dob.getCookie(LineCookie.class)) != null && (lineSet = lc.getLineSet()) != null) {
            try {
                int lineOffset = Utilities.getLineOffset((BaseDocument)doc, (int)offset);
                return original ? lineSet.getOriginal(lineOffset) : lineSet.getCurrent(lineOffset);
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        return null;
    }

    public static Line getLine(Document doc, int offset, boolean original) {
        LineCookie lc;
        Line.Set lineSet;
        DataObject dob = NbEditorUtilities.getDataObject(doc);
        if (dob != null && (lc = (LineCookie)dob.getCookie(LineCookie.class)) != null && (lineSet = lc.getLineSet()) != null) {
            Element lineRoot = doc instanceof AbstractDocument ? ((AbstractDocument)doc).getParagraphElement(0).getParentElement() : doc.getDefaultRootElement();
            int lineIndex = lineRoot.getElementIndex(offset);
            return original ? lineSet.getOriginal(lineIndex) : lineSet.getCurrent(lineIndex);
        }
        return null;
    }

    public static Line getLine(JTextComponent target, boolean original) {
        return NbEditorUtilities.getLine((BaseDocument)target.getDocument(), target.getCaret().getDot(), original);
    }

    public static TopComponent getTopComponent(JTextComponent target) {
        return (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, target);
    }

    public static TopComponent getOuterTopComponent(JTextComponent target) {
        TopComponent tc = null;
        TopComponent parent = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, target);
        while (parent != null) {
            tc = parent;
            parent = (TopComponent)SwingUtilities.getAncestorOfClass(TopComponent.class, (Component)tc);
        }
        return tc;
    }

    public static void addJumpListEntry(DataObject dob) {
        final EditorCookie ec = (EditorCookie)dob.getCookie(EditorCookie.class);
        if (ec != null) {
            final Timer timer = new Timer(500, null);
            timer.addActionListener(new ActionListener(){
                private int countDown;

                @Override
                public void actionPerformed(ActionEvent evt) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (--1.this.countDown >= 0) {
                                JEditorPane[] panes = ec.getOpenedPanes();
                                if (panes != null && panes.length > 0) {
                                    JumpList.checkAddEntry((JTextComponent)panes[0]);
                                    timer.stop();
                                }
                            } else {
                                timer.stop();
                            }
                        }
                    });
                }

            });
            timer.start();
        }
    }

    public static String[] mergeStringArrays(String[] a1, String[] a2) {
        int i;
        String[] ret = new String[a1.length + a2.length];
        for (i = 0; i < a1.length; ++i) {
            ret[i] = a1[i];
        }
        for (i = 0; i < a2.length; ++i) {
            ret[a1.length + i] = a2[i];
        }
        return ret;
    }

    public static String getMimeType(Document doc) {
        return DocumentUtilities.getMimeType((Document)doc);
    }

    public static String getMimeType(JTextComponent component) {
        return DocumentUtilities.getMimeType((JTextComponent)component);
    }

    public static void invalidArgument(String bundleKey) {
        IllegalArgumentException iae = new IllegalArgumentException("Invalid argument");
        Toolkit.getDefaultToolkit().beep();
        ErrorManager errMan = (ErrorManager)Lookup.getDefault().lookup(ErrorManager.class);
        if (errMan != null) {
            errMan.annotate((Throwable)iae, 256, iae.getMessage(), NbEditorUtilities.getString(bundleKey), null, null);
        }
        throw iae;
    }

    private static String getString(String key) {
        try {
            return NbBundle.getBundle(NbEditorUtilities.class).getString(key);
        }
        catch (MissingResourceException e) {
            Logger.getLogger("global").log(Level.INFO, null, e);
            return key;
        }
    }

}

