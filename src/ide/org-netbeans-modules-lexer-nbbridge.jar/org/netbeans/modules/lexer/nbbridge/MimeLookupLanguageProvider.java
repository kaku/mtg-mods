/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.netbeans.spi.lexer.LanguageProvider
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.lexer.nbbridge;

import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.lexer.nbbridge.LanguagesEmbeddingMap;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageProvider;
import org.openide.util.Lookup;

public final class MimeLookupLanguageProvider
extends LanguageProvider {
    public Language<?> findLanguage(String mimeType) {
        Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)mimeType));
        return (Language)lookup.lookup(Language.class);
    }

    public LanguageEmbedding<?> findLanguageEmbedding(Token<?> token, LanguagePath languagePath, InputAttributes inputAttributes) {
        Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)languagePath.mimePath()));
        LanguagesEmbeddingMap map = (LanguagesEmbeddingMap)lookup.lookup(LanguagesEmbeddingMap.class);
        return map == null ? null : map.getLanguageEmbeddingForTokenName(token.id().name());
    }
}

