/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.lib.lexer.LanguageManager
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.lexer.nbbridge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Language;
import org.netbeans.lib.lexer.LanguageManager;
import org.netbeans.modules.lexer.nbbridge.LanguagesEmbeddingMap;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.openide.filesystems.FileObject;

@MimeLocation(subfolderName="languagesEmbeddingMap", instanceProviderClass=MimeLookupFolderInfo.class)
public class MimeLookupFolderInfo
implements InstanceProvider {
    private static final Logger LOG = Logger.getLogger(MimeLookupFolderInfo.class.getName());

    public Object createInstance(List fileObjectList) {
        HashMap map = new HashMap();
        for (Object o : fileObjectList) {
            assert (o instanceof FileObject);
            FileObject f = (FileObject)o;
            try {
                Object[] info = this.parseFile(f);
                String mimeType = (String)info[0];
                int startSkipLength = (Integer)info[1];
                int endSkipLength = (Integer)info[2];
                if (this.isMimeTypeValid(mimeType)) {
                    Language language = LanguageManager.getInstance().findLanguage(mimeType);
                    if (language != null) {
                        map.put(f.getName(), (LanguageEmbedding)LanguageEmbedding.create((Language)language, (int)startSkipLength, (int)endSkipLength));
                        continue;
                    }
                    LOG.warning("Can't find Language for mime type '" + mimeType + "', ignoring.");
                    continue;
                }
                LOG.log(Level.WARNING, "Ignoring invalid mime type '" + mimeType + "' from: " + f.getPath());
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, "Can't read language embedding definition from: " + f.getPath());
            }
        }
        return new LanguagesEmbeddingMap(map);
    }

    private boolean isMimeTypeValid(String mimeType) {
        if (mimeType == null) {
            return false;
        }
        int slashIndex = mimeType.indexOf(47);
        if (slashIndex == -1) {
            return false;
        }
        if (mimeType.indexOf(47, slashIndex + 1) != -1) {
            return false;
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Object[] parseFile(FileObject f) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(f.getInputStream()));
        try {
            String line;
            while (null != (line = r.readLine())) {
                line.trim();
                if (line.length() == 0) continue;
                String[] parts = line.split(",");
                Object[] arrobject = new Object[3];
                arrobject[0] = parts[0];
                arrobject[1] = parts.length > 1 ? this.toInt(parts[1], "Ignoring invalid start-skip-length '{0}' in " + f.getPath()) : 0;
                arrobject[2] = parts.length > 2 ? this.toInt(parts[2], "Ignoring invalid end-skip-length '{0}' in " + f.getPath()) : 0;
                Object[] arrobject2 = arrobject;
                return arrobject2;
            }
            Object[] parts = null;
            return parts;
        }
        finally {
            r.close();
        }
    }

    private int toInt(String s, String errorMsg) {
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException e) {
            LOG.log(Level.WARNING, MessageFormat.format(errorMsg, s), e);
            return 0;
        }
    }
}

