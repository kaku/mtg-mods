/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.netbeans.spi.lexer.LanguageEmbedding
 */
package org.netbeans.modules.lexer.nbbridge;

import java.util.Map;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.netbeans.spi.lexer.LanguageEmbedding;

@MimeLocation(subfolderName="languagesEmbeddingMap")
public final class LanguagesEmbeddingMap {
    private Map<String, LanguageEmbedding<?>> map;

    public LanguagesEmbeddingMap(Map<String, LanguageEmbedding<?>> map) {
        this.map = map;
    }

    public synchronized LanguageEmbedding<?> getLanguageEmbeddingForTokenName(String tokenName) {
        return this.map.get(tokenName);
    }
}

