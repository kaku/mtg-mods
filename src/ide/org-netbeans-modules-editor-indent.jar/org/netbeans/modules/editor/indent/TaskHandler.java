/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.indent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.modules.editor.indent.IndentSpiPackageAccessor;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.IndentTask;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ProxyLookup;

public final class TaskHandler {
    private static final Logger LOG = Logger.getLogger(TaskHandler.class.getName());
    private final boolean indent;
    private final Document doc;
    private List<MimeItem> items;
    private Position startPos;
    private Position endPos;
    private Position caretPos;
    private final Set<Object> existingFactories = new HashSet<Object>();
    private Lookup lookup = null;

    TaskHandler(boolean indent, Document doc) {
        this.indent = indent;
        this.doc = doc;
    }

    public Lookup getLookup() {
        return this.lookup;
    }

    public boolean isIndent() {
        return this.indent;
    }

    public Document document() {
        return this.doc;
    }

    public int caretOffset() {
        return this.caretPos.getOffset();
    }

    public void setCaretOffset(int offset) throws BadLocationException {
        this.caretPos = this.doc.createPosition(offset);
    }

    public Position startPos() {
        return this.startPos;
    }

    public Position endPos() {
        return this.endPos;
    }

    void setGlobalBounds(Position startPos, Position endPos) {
        assert (startPos.getOffset() <= endPos.getOffset());
        this.startPos = startPos;
        this.endPos = endPos;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean collectTasks() {
        ArrayList<MimeItem> newItems;
        Set languagePathSet;
        TokenHierarchy th = TokenHierarchy.get((Document)this.document());
        languagePathSet = Collections.emptySet();
        if (this.doc instanceof AbstractDocument) {
            AbstractDocument adoc = (AbstractDocument)this.doc;
            adoc.readLock();
            try {
                languagePathSet = th.languagePaths();
                ArrayList languagePaths = new ArrayList(languagePathSet);
                Collections.sort(languagePaths, LanguagePathSizeComparator.ASCENDING);
                for (LanguagePath lp : languagePaths) {
                    this.addItem(MimePath.parse((String)lp.mimePath()), lp);
                }
            }
            finally {
                adoc.readUnlock();
            }
        }
        if (languagePathSet.isEmpty()) {
            this.addItem(MimePath.parse((String)this.docMimeType()), null);
        }
        if (this.items != null && "application/x-httpd-eruby".equals(this.docMimeType())) {
            newItems = new ArrayList<MimeItem>(this.items.size());
            MimeItem rubyItem = null;
            for (MimeItem item : this.items) {
                if (item.mimePath().getPath().endsWith("text/x-ruby")) {
                    rubyItem = item;
                    continue;
                }
                newItems.add(item);
            }
            if (rubyItem != null) {
                newItems.add(rubyItem);
            }
            this.items = newItems;
        }
        if (this.items != null && "text/x-php5".equals(this.docMimeType())) {
            newItems = new ArrayList(this.items.size());
            MimeItem phpItem = null;
            for (MimeItem item : this.items) {
                if (item.mimePath().getPath().endsWith("text/x-php5")) {
                    phpItem = item;
                    continue;
                }
                newItems.add(item);
            }
            if (phpItem != null) {
                newItems.add(phpItem);
            }
            this.items = newItems;
        }
        if (this.items != null && "text/x-jsp".equals(this.docMimeType()) || "text/x-tag".equals(this.docMimeType())) {
            newItems = new ArrayList(this.items.size());
            MimeItem htmlItem = null;
            MimeItem jspItem = null;
            for (MimeItem item : this.items) {
                if (item.mimePath().getPath().endsWith("text/html")) {
                    htmlItem = item;
                    continue;
                }
                if (item.mimePath().getPath().endsWith("text/x-jsp") || item.mimePath().getPath().endsWith("text/x-tag")) {
                    jspItem = item;
                    continue;
                }
                newItems.add(item);
            }
            if (htmlItem != null) {
                newItems.add(0, htmlItem);
            }
            if (jspItem != null) {
                newItems.add(0, jspItem);
            }
            this.items = newItems;
        }
        if (this.items != null) {
            ArrayList<Lookup> lookups = new ArrayList<Lookup>();
            for (MimeItem mi : this.items) {
                Lookup l = mi.getLookup();
                if (l == null) continue;
                lookups.add(l);
            }
            if (lookups.size() > 0) {
                this.lookup = new ProxyLookup(lookups.toArray((T[])new Lookup[lookups.size()]));
            }
        }
        if (this.lookup == null) {
            this.lookup = Lookup.EMPTY;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Collected items: ");
            if (this.items != null) {
                for (MimeItem mi : this.items) {
                    LOG.fine("  Item: " + mi);
                }
            }
            LOG.fine("-----------------");
        }
        return this.items != null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void lock() {
        if (this.items != null) {
            MimeItem item;
            int i;
            try {
                for (i = 0; i < this.items.size(); ++i) {
                    item = this.items.get(i);
                    item.lock();
                }
            }
            finally {
                if (i < this.items.size()) {
                    while (--i >= 0) {
                        item = this.items.get(i);
                        item.unlock();
                    }
                }
            }
        }
    }

    void unlock() {
        if (this.items != null) {
            for (MimeItem item : this.items) {
                item.unlock();
            }
        }
    }

    boolean hasFactories() {
        String mimeType = this.docMimeType();
        return mimeType != null && new MimeItem(this, MimePath.get((String)mimeType), null).hasFactories();
    }

    boolean hasItems() {
        return this.items != null;
    }

    void runTasks() throws BadLocationException {
        if (this.items == null) {
            return;
        }
        for (MimeItem item : this.items) {
            item.runTask();
        }
    }

    private boolean addItem(MimePath mimePath, LanguagePath languagePath) {
        MimeItem item = new MimeItem(this, mimePath, languagePath);
        if (item.createTask(this.existingFactories)) {
            if (this.items == null) {
                this.items = new ArrayList<MimeItem>();
            }
            this.items.add(item);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Adding MimeItem: " + item);
            }
            return true;
        }
        return false;
    }

    private Collection<LanguagePath> getActiveEmbeddedPaths(TokenSequence ts) {
        HashSet<LanguagePath> lps = new HashSet<LanguagePath>();
        lps.add(ts.languagePath());
        List tsStack = null;
        do {
            if (ts.moveNext()) {
                TokenSequence eTS = ts.embedded();
                if (eTS == null) continue;
                tsStack.add(ts);
                ts = eTS;
                lps.add(ts.languagePath());
                continue;
            }
            if (tsStack == null || tsStack.size() <= 0) break;
            ts = (TokenSequence)tsStack.get(tsStack.size() - 1);
            tsStack.remove(tsStack.size() - 1);
        } while (true);
        return lps;
    }

    private String docMimeType() {
        return (String)this.document().getProperty("mimeType");
    }

    private static final class LanguagePathSizeComparator
    implements Comparator<LanguagePath> {
        static final LanguagePathSizeComparator ASCENDING = new LanguagePathSizeComparator(false);
        private final boolean reverse;

        public LanguagePathSizeComparator(boolean reverse) {
            this.reverse = reverse;
        }

        @Override
        public int compare(LanguagePath lp1, LanguagePath lp2) {
            return this.reverse ? lp2.size() - lp1.size() : lp1.size() - lp2.size();
        }
    }

    public static final class MimeItem {
        private final TaskHandler handler;
        private final MimePath mimePath;
        private final LanguagePath languagePath;
        private IndentTask indentTask;
        private ReformatTask reformatTask;
        private ExtraLock extraLock;
        private Context context;

        MimeItem(TaskHandler handler, MimePath mimePath, LanguagePath languagePath) {
            this.handler = handler;
            this.mimePath = mimePath;
            this.languagePath = languagePath;
        }

        public MimePath mimePath() {
            return this.mimePath;
        }

        public LanguagePath languagePath() {
            return this.languagePath;
        }

        public Context context() {
            if (this.context == null) {
                this.context = IndentSpiPackageAccessor.get().createContext(this);
            }
            return this.context;
        }

        public TaskHandler handler() {
            return this.handler;
        }

        boolean hasFactories() {
            Lookup lookup = MimeLookup.getLookup((MimePath)this.mimePath);
            return this.handler().isIndent() ? lookup.lookup(IndentTask.Factory.class) != null : lookup.lookup(ReformatTask.Factory.class) != null;
        }

        public List<Context.Region> indentRegions() {
            Document doc = this.handler.document();
            ArrayList<Context.Region> indentRegions = new ArrayList();
            try {
                int startOffset = this.handler.startPos().getOffset();
                int endOffset = this.handler.endPos().getOffset();
                if (endOffset > doc.getLength()) {
                    endOffset = Integer.MAX_VALUE;
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("indentRegions: startOffset=" + startOffset + ", endOffset=" + endOffset + '\n');
                }
                if (this.languagePath != null && startOffset < endOffset) {
                    List tsl = TokenHierarchy.get((Document)doc).tokenSequenceList(this.languagePath, startOffset, endOffset);
                    for (TokenSequence ts : tsl) {
                        ts.moveStart();
                        if (!ts.moveNext()) continue;
                        int regionStartOffset = ts.offset();
                        ts.moveEnd();
                        ts.movePrevious();
                        int regionEndOffset = ts.offset() + ts.token().length();
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.fine("  Region[" + indentRegions.size() + "]: startOffset=" + regionStartOffset + ", endOffset=" + regionEndOffset + '\n');
                        }
                        if (regionStartOffset > endOffset || regionEndOffset < startOffset) continue;
                        regionStartOffset = Math.max(regionStartOffset, startOffset);
                        regionEndOffset = Math.min(regionEndOffset, endOffset);
                        MutablePositionRegion region = new MutablePositionRegion(doc.createPosition(regionStartOffset), doc.createPosition(regionEndOffset));
                        indentRegions.add(IndentSpiPackageAccessor.get().createContextRegion(region));
                    }
                } else {
                    MutablePositionRegion wholeDocRegion = new MutablePositionRegion(this.handler.startPos, this.handler.endPos);
                    indentRegions.add(IndentSpiPackageAccessor.get().createContextRegion(wholeDocRegion));
                }
            }
            catch (BadLocationException e) {
                Exceptions.printStackTrace((Throwable)e);
                indentRegions = Collections.emptyList();
            }
            return indentRegions;
        }

        boolean createTask(Set<Object> existingFactories) {
            Object factory;
            Lookup lookup = MimeLookup.getLookup((MimePath)this.mimePath);
            if (!this.handler.isIndent()) {
                factory = (ReformatTask.Factory)lookup.lookup(ReformatTask.Factory.class);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("'" + this.mimePath.getPath() + "' supplied ReformatTask.Factory: " + factory);
                }
                if (factory != null && (this.reformatTask = factory.createTask(this.context())) != null && !existingFactories.contains(factory)) {
                    this.extraLock = this.reformatTask.reformatLock();
                    existingFactories.add(factory);
                    return true;
                }
            }
            if (this.handler.isIndent() || this.reformatTask == null) {
                factory = (IndentTask.Factory)lookup.lookup(IndentTask.Factory.class);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("'" + this.mimePath.getPath() + "' supplied IndentTask.Factory: " + factory);
                }
                if (factory != null && (this.indentTask = factory.createTask(this.context())) != null && !existingFactories.contains(factory)) {
                    this.extraLock = this.indentTask.indentLock();
                    existingFactories.add(factory);
                    return true;
                }
            }
            return false;
        }

        void lock() {
            if (this.extraLock != null) {
                this.extraLock.lock();
            }
        }

        void runTask() throws BadLocationException {
            if (this.indentTask != null) {
                this.indentTask.reindent();
            } else {
                this.reformatTask.reformat();
            }
        }

        void unlock() {
            if (this.extraLock != null) {
                this.extraLock.unlock();
            }
        }

        public String toString() {
            return (Object)this.mimePath + ": " + (this.indentTask != null ? new StringBuilder().append("IT: ").append(this.indentTask).toString() : new StringBuilder().append("RT: ").append(this.reformatTask).toString());
        }

        private Lookup getLookup() {
            if (this.indentTask != null && this.indentTask instanceof Lookup.Provider) {
                return ((Lookup.Provider)this.indentTask).getLookup();
            }
            if (this.reformatTask != null && this.reformatTask instanceof Lookup.Provider) {
                return ((Lookup.Provider)this.reformatTask).getLookup();
            }
            return null;
        }
    }

}

