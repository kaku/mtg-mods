/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 */
package org.netbeans.modules.editor.indent;

import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.modules.editor.indent.TaskHandler;
import org.netbeans.modules.editor.indent.spi.Context;

public abstract class IndentSpiPackageAccessor {
    private static IndentSpiPackageAccessor INSTANCE;

    public static IndentSpiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName(Context.class.getName(), true, Context.class.getClassLoader());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        return INSTANCE;
    }

    public static void register(IndentSpiPackageAccessor accessor) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already registered");
        }
        INSTANCE = accessor;
    }

    public abstract Context createContext(TaskHandler.MimeItem var1);

    public abstract Context.Region createContextRegion(MutablePositionRegion var1);

    public abstract MutablePositionRegion positionRegion(Context.Region var1);
}

