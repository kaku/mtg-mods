/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.indent.spi;

import javax.swing.text.BadLocationException;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;

public interface ReformatTask {
    public void reformat() throws BadLocationException;

    public ExtraLock reformatLock();

    public static interface Factory {
        public ReformatTask createTask(Context var1);
    }

}

