/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$MutableContext
 */
package org.netbeans.modules.editor.indent.spi.support;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;

public final class AutomatedIndenting {
    private static final Logger LOG = Logger.getLogger(AutomatedIndenting.class.getName());

    public static /* varargs */ TypedTextInterceptor createHotCharsIndenter(Pattern ... linePatterns) {
        return new RegExBasedIndenter(linePatterns);
    }

    public static TypedTextInterceptor.Factory createHotCharsIndenter(Map<Object, Object> fileAttributes) {
        final ArrayList<Pattern> linePatterns = new ArrayList<Pattern>();
        for (Object key : fileAttributes.keySet()) {
            if (!key.toString().startsWith("regex")) continue;
            Object value = fileAttributes.get(key);
            try {
                Pattern pattern = Pattern.compile(value.toString());
                linePatterns.add(pattern);
            }
            catch (PatternSyntaxException pse) {
                LOG.log(Level.WARNING, null, pse);
            }
        }
        return new TypedTextInterceptor.Factory(){

            public TypedTextInterceptor createTypedTextInterceptor(MimePath mimePath) {
                return AutomatedIndenting.createHotCharsIndenter(linePatterns.toArray(new Pattern[linePatterns.size()]));
            }
        };
    }

    private static final class RegExBasedIndenter
    implements TypedTextInterceptor {
        private final Pattern[] linePatterns;

        public /* varargs */ RegExBasedIndenter(Pattern ... linePatterns) {
            this.linePatterns = linePatterns;
        }

        public boolean beforeInsert(TypedTextInterceptor.Context context) {
            return false;
        }

        public void insert(TypedTextInterceptor.MutableContext context) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void afterInsert(TypedTextInterceptor.Context context) {
            int textLen = context.getText().length();
            if (textLen > 0) {
                CharSequence lineText;
                int lineEndOffset;
                int lineStartOffset;
                try {
                    Element lineElement = DocumentUtilities.getParagraphElement((Document)context.getDocument(), (int)context.getOffset());
                    lineText = DocumentUtilities.getText((Document)context.getDocument(), (int)lineElement.getStartOffset(), (int)(context.getOffset() - lineElement.getStartOffset() + textLen));
                    lineStartOffset = lineElement.getStartOffset();
                    lineEndOffset = Math.max(lineStartOffset, lineElement.getEndOffset() - 1);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, null, e);
                    return;
                }
                for (Pattern p : this.linePatterns) {
                    if (!p.matcher(lineText).matches()) continue;
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("The line '" + lineText + "' matches '" + p.pattern() + "' -> calling Indent.reindent(" + lineStartOffset + ", " + lineEndOffset + ")");
                    }
                    final Indent indenter = Indent.get(context.getDocument());
                    indenter.lock();
                    try {
                        RegExBasedIndenter.runAtomicAsUser(context.getDocument(), new Runnable(){

                            @Override
                            public void run() {
                                try {
                                    indenter.reindent(lineStartOffset, lineEndOffset);
                                }
                                catch (BadLocationException ble) {
                                    LOG.log(Level.INFO, null, ble);
                                }
                            }
                        });
                    }
                    finally {
                        indenter.unlock();
                    }
                }
            }
        }

        public void cancelled(TypedTextInterceptor.Context context) {
        }

        private static void runAtomicAsUser(Document doc, Runnable run) {
            try {
                Method runAtomicAsUserMethod = doc.getClass().getMethod("runAtomicAsUser", Runnable.class);
                runAtomicAsUserMethod.invoke(doc, run);
            }
            catch (Exception e) {
                LOG.log(Level.INFO, null, e);
            }
        }

    }

}

