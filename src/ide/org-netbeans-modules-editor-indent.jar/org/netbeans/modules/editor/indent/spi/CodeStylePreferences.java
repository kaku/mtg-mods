/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.indent.spi;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public final class CodeStylePreferences {
    private static final Logger LOG = Logger.getLogger(CodeStylePreferences.class.getName());
    private final Object docOrFile;
    private final String mimeType;
    private static final Provider defaultProvider = new Provider(){

        @Override
        public Preferences forFile(FileObject file, String mimeType) {
            return (Preferences)MimeLookup.getLookup((MimePath)(mimeType == null ? MimePath.EMPTY : MimePath.parse((String)mimeType))).lookup(Preferences.class);
        }

        @Override
        public Preferences forDocument(Document doc, String mimeType) {
            return (Preferences)MimeLookup.getLookup((MimePath)(mimeType == null ? MimePath.EMPTY : MimePath.parse((String)mimeType))).lookup(Preferences.class);
        }
    };

    public static CodeStylePreferences get(Document doc) {
        return CodeStylePreferences.get(doc, doc != null ? (String)doc.getProperty("mimeType") : null);
    }

    public static CodeStylePreferences get(Document doc, String mimeType) {
        if (doc != null) {
            return new CodeStylePreferences(doc, mimeType);
        }
        return new CodeStylePreferences(null, null);
    }

    public static CodeStylePreferences get(FileObject file) {
        return CodeStylePreferences.get(file, file != null ? file.getMIMEType() : null);
    }

    public static CodeStylePreferences get(FileObject file, String mimeType) {
        if (file != null) {
            return new CodeStylePreferences((Object)file, mimeType);
        }
        return new CodeStylePreferences(null, null);
    }

    public Preferences getPreferences() {
        Object o;
        Document doc = this.docOrFile instanceof Document ? (Document)this.docOrFile : null;
        Object object = o = doc == null ? null : doc.getProperty("Tools-Options->Editor->Formatting->Preview - Preferences");
        if (o instanceof Preferences) {
            return (Preferences)o;
        }
        Preferences prefs = null;
        Provider provider = null;
        Collection providers = Lookup.getDefault().lookupAll(Provider.class);
        for (Provider p : providers) {
            prefs = doc != null ? p.forDocument(doc, this.mimeType) : p.forFile((FileObject)this.docOrFile, this.mimeType);
            if (prefs == null) continue;
            provider = p;
            break;
        }
        if (prefs == null) {
            provider = defaultProvider;
            prefs = doc != null ? provider.forDocument(doc, this.mimeType) : provider.forFile((FileObject)this.docOrFile, this.mimeType);
        }
        assert (prefs != null);
        Preferences parent = prefs.parent();
        if (parent == null || parent instanceof AbstractPreferences) {
            return new CachingPreferences((AbstractPreferences)parent, prefs.name(), prefs);
        }
        return prefs;
    }

    private CodeStylePreferences(Object docOrFile, String mimeType) {
        this.docOrFile = docOrFile;
        this.mimeType = mimeType;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    private static final class CachingPreferences
    extends AbstractPreferences {
        private final Preferences delegate;
        private final Map<String, Object> values = new HashMap<String, Object>(7);
        private String[] childNames;
        private String[] keys;
        private Map<String, AbstractPreferences> prefs;
        private static final Object NULL = new Object();

        public CachingPreferences(AbstractPreferences parent, String name, Preferences delegate) {
            super(parent, name);
            this.delegate = delegate;
        }

        private void writeDisallowed() {
            throw new UnsupportedOperationException("Writing not supported");
        }

        @Override
        protected void putSpi(String key, String value) {
            this.values.put(key, value);
            this.delegate.put(key, value);
        }

        @Override
        protected String getSpi(String key) {
            Object v = this.values.get(key);
            if (v == null) {
                v = this.delegate.get(key, null);
                if (v == null) {
                    v = NULL;
                }
                this.values.put(key, v);
            }
            return v == NULL ? null : v.toString();
        }

        @Override
        protected void removeSpi(String key) {
            this.values.remove(key);
            this.delegate.remove(key);
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            this.writeDisallowed();
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            if (this.keys == null) {
                this.keys = this.delegate.keys();
            }
            return this.keys;
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            if (this.childNames == null) {
                this.childNames = this.delegate.childrenNames();
            }
            return this.childNames;
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            AbstractPreferences p;
            if (this.prefs == null) {
                this.prefs = new HashMap<String, AbstractPreferences>(3);
            }
            if ((p = this.prefs.get(name)) == null) {
                Preferences r = this.delegate.node(name);
                p = new CachingPreferences(this, name, r);
                this.prefs.put(name, p);
            }
            return p;
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
        }
    }

    public static interface Provider {
        public Preferences forFile(FileObject var1, String var2);

        public Preferences forDocument(Document var1, String var2);
    }

}

