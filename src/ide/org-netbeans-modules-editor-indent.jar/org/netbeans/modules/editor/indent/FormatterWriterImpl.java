/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.indent;

import java.io.IOException;
import java.io.Writer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.modules.editor.indent.IndentImpl;
import org.openide.util.Exceptions;

public final class FormatterWriterImpl
extends Writer {
    private IndentImpl indentImpl;
    private int offset;
    private Writer writer;
    private StringBuilder buffer;

    FormatterWriterImpl(IndentImpl indentImpl, int offset, Writer writer) {
        if (offset < 0) {
            throw new IllegalArgumentException("offset=" + offset + " < 0");
        }
        if (offset > indentImpl.document().getLength()) {
            throw new IllegalArgumentException("offset=" + offset + " > docLen=" + indentImpl.document().getLength());
        }
        this.indentImpl = indentImpl;
        this.offset = offset;
        this.writer = writer;
        this.buffer = new StringBuilder();
    }

    @Override
    public void write(int c) throws IOException {
        this.write(new char[]{(char)c}, 0, 1);
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        this.buffer.append(cbuf, off, len);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void close() throws IOException {
        block5 : {
            this.indentImpl.reformatLock();
            try {
                Document doc = this.indentImpl.document();
                String text = this.buffer.toString();
                if (text.length() <= 0 || this.offset > doc.getLength()) break block5;
                try {
                    doc.insertString(this.offset, text, null);
                    Position startPos = doc.createPosition(this.offset);
                    Position endPos = doc.createPosition(this.offset + text.length());
                    this.indentImpl.reformat(startPos.getOffset(), endPos.getOffset(), startPos.getOffset());
                    int len = endPos.getOffset() - startPos.getOffset();
                    String reformattedText = doc.getText(startPos.getOffset(), len);
                    doc.remove(startPos.getOffset(), len);
                    this.writer.write(reformattedText);
                }
                catch (BadLocationException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            finally {
                this.indentImpl.reformatUnlock();
            }
        }
    }

    @Override
    public void flush() throws IOException {
    }
}

