/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.indent.api;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.IndentImpl;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;

public final class IndentUtils {
    private static final int MAX_CACHED_INDENT = 80;
    private static final Logger LOG = Logger.getLogger(IndentUtils.class.getName());
    private static final String[] cachedSpacesStrings = new String[81];
    private static final int MAX_CACHED_TAB_SIZE = 8;
    private static final String[][] cachedTabIndents;

    private IndentUtils() {
    }

    public static int indentLevelSize(Document doc) {
        int indentLevel;
        Preferences prefs = CodeStylePreferences.get(doc).getPreferences();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("INDENT_SHIFT_WIDTH='" + prefs.get("indent-shift-width", null) + "', EXPAND_TABS='" + prefs.get("expand-tabs", null) + "', SPACES_PER_TAB='" + prefs.get("spaces-per-tab", null) + "', TAB_SIZE='" + prefs.get("tab-size", null) + "' for " + doc);
        }
        if ((indentLevel = prefs.getInt("indent-shift-width", -1)) < 0) {
            boolean expandTabs = prefs.getBoolean("expand-tabs", true);
            indentLevel = expandTabs ? prefs.getInt("spaces-per-tab", 4) : prefs.getInt("tab-size", 8);
        }
        assert (indentLevel >= 0);
        return indentLevel;
    }

    public static int tabSize(Document doc) {
        int tabSize = CodeStylePreferences.get(doc).getPreferences().getInt("tab-size", 8);
        assert (tabSize > 0);
        return tabSize;
    }

    public static boolean isExpandTabs(Document doc) {
        return CodeStylePreferences.get(doc).getPreferences().getBoolean("expand-tabs", true);
    }

    public static int lineStartOffset(Document doc, int offset) throws BadLocationException {
        IndentImpl.checkOffsetInDocument(doc, offset);
        Element lineRootElement = IndentImpl.lineRootElement(doc);
        return lineRootElement.getElement(lineRootElement.getElementIndex(offset)).getStartOffset();
    }

    public static int lineIndent(Document doc, int lineStartOffset) throws BadLocationException {
        IndentImpl.checkOffsetInDocument(doc, lineStartOffset);
        CharSequence docText = DocumentUtilities.getText((Document)doc);
        int indent = 0;
        int tabSize = -1;
        while (lineStartOffset < docText.length()) {
            char ch = docText.charAt(lineStartOffset);
            switch (ch) {
                case '\n': {
                    return indent;
                }
                case '\t': {
                    if (tabSize == -1) {
                        tabSize = IndentUtils.tabSize(doc);
                    }
                    indent = (indent + tabSize) / tabSize * tabSize;
                    break;
                }
                default: {
                    if (Character.isWhitespace(ch)) {
                        ++indent;
                        break;
                    }
                    return indent;
                }
            }
            ++lineStartOffset;
        }
        return indent;
    }

    public static String createIndentString(Document doc, int indent) {
        if (indent < 0) {
            throw new IllegalArgumentException("indent=" + indent + " < 0");
        }
        return IndentUtils.cachedOrCreatedIndentString(indent, IndentUtils.isExpandTabs(doc), IndentUtils.tabSize(doc));
    }

    public static String createIndentString(int indent, boolean expandTabs, int tabSize) {
        return IndentUtils.cachedOrCreatedIndentString(indent, expandTabs, tabSize);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static String cachedOrCreatedIndentString(int indent, boolean expandTabs, int tabSize) {
        String indentString;
        if (expandTabs || indent < tabSize) {
            if (indent <= 80) {
                String[] arrstring = cachedSpacesStrings;
                synchronized (arrstring) {
                    indentString = cachedSpacesStrings[indent];
                    if (indentString == null) {
                        indentString = cachedSpacesStrings[80];
                        if (indentString == null) {
                            IndentUtils.cachedSpacesStrings[80] = indentString = IndentUtils.createSpacesString(80);
                        }
                        IndentUtils.cachedSpacesStrings[indent] = indentString = indentString.substring(0, indent);
                    }
                }
            } else {
                indentString = IndentUtils.createSpacesString(indent);
            }
        } else if (indent <= 80 && tabSize <= 8) {
            String[][] arrstring = cachedTabIndents;
            synchronized (arrstring) {
                String[] tabIndents = cachedTabIndents[tabSize];
                if (tabIndents == null) {
                    tabIndents = new String[80 - tabSize + 1];
                    IndentUtils.cachedTabIndents[tabSize] = tabIndents;
                }
                if ((indentString = tabIndents[indent - tabSize]) == null) {
                    tabIndents[indent - tabSize] = indentString = IndentUtils.createTabIndentString(indent, tabSize);
                }
            }
        } else {
            indentString = IndentUtils.createTabIndentString(indent, tabSize);
        }
        return indentString;
    }

    private static String createSpacesString(int spaceCount) {
        StringBuilder sb = new StringBuilder(spaceCount);
        ArrayUtilities.appendSpaces((StringBuilder)sb, (int)spaceCount);
        return sb.toString();
    }

    private static String createTabIndentString(int indent, int tabSize) {
        StringBuilder sb = new StringBuilder();
        while (indent >= tabSize) {
            sb.append('\t');
            indent -= tabSize;
        }
        ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
        return sb.toString();
    }

    static {
        IndentUtils.cachedSpacesStrings[0] = "";
        cachedTabIndents = new String[9][];
    }
}

