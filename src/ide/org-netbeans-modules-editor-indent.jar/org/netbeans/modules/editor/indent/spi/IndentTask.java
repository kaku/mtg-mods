/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.indent.spi;

import javax.swing.text.BadLocationException;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;

public interface IndentTask {
    public void reindent() throws BadLocationException;

    public ExtraLock indentLock();

    public static interface Factory {
        public IndentTask createTask(Context var1);
    }

}

