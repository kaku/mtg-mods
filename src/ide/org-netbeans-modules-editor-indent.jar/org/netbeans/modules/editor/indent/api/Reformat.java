/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.indent.api;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.editor.indent.IndentImpl;

public final class Reformat {
    private final IndentImpl impl;

    public static Reformat get(Document doc) {
        IndentImpl indentImpl = IndentImpl.get(doc);
        Reformat reformat = indentImpl.getReformat();
        if (reformat == null) {
            reformat = new Reformat(indentImpl);
            indentImpl.setReformat(reformat);
        }
        return reformat;
    }

    private Reformat(IndentImpl impl) {
        this.impl = impl;
    }

    public void lock() {
        this.impl.reformatLock();
    }

    public void unlock() {
        this.impl.reformatUnlock();
    }

    public void reformat(int startOffset, int endOffset) throws BadLocationException {
        this.impl.reformat(startOffset, endOffset, startOffset);
    }
}

