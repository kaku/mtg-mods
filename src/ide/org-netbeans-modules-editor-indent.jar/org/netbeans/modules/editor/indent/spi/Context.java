/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.indent.spi;

import java.util.List;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.modules.editor.indent.IndentImpl;
import org.netbeans.modules.editor.indent.IndentSpiPackageAccessor;
import org.netbeans.modules.editor.indent.TaskHandler;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.openide.util.Lookup;

public final class Context {
    private TaskHandler.MimeItem mimeItem;

    Context(TaskHandler.MimeItem mimeItem) {
        this.mimeItem = mimeItem;
    }

    public Lookup getLookup() {
        assert (this.mimeItem.handler().getLookup() != null);
        return this.mimeItem.handler().getLookup();
    }

    public Document document() {
        return this.mimeItem.handler().document();
    }

    public String mimePath() {
        return this.mimeItem.mimePath().getPath();
    }

    public int startOffset() {
        Position startPos = this.mimeItem.handler().startPos();
        return startPos != null ? startPos.getOffset() : -1;
    }

    public int endOffset() {
        Position endPos = this.mimeItem.handler().endPos();
        return endPos != null ? endPos.getOffset() : -1;
    }

    public int lineStartOffset(int offset) throws BadLocationException {
        return IndentUtils.lineStartOffset(this.mimeItem.handler().document(), offset);
    }

    public int lineIndent(int lineStartOffset) throws BadLocationException {
        return IndentUtils.lineIndent(this.mimeItem.handler().document(), lineStartOffset);
    }

    public void modifyIndent(int lineStartOffset, int newIndent) throws BadLocationException {
        int oldIndentEndOffset;
        char ch;
        Document doc = this.document();
        IndentImpl.checkOffsetInDocument(doc, lineStartOffset);
        int indent = 0;
        int tabSize = -1;
        CharSequence docText = DocumentUtilities.getText((Document)doc);
        for (oldIndentEndOffset = lineStartOffset; oldIndentEndOffset < docText.length() && (ch = docText.charAt(oldIndentEndOffset)) != '\n'; ++oldIndentEndOffset) {
            if (ch == '\t') {
                if (tabSize == -1) {
                    tabSize = IndentUtils.tabSize(doc);
                }
                indent = (indent + tabSize) / tabSize * tabSize;
                continue;
            }
            if (!Character.isWhitespace(ch)) break;
            ++indent;
        }
        String newIndentString = IndentUtils.createIndentString(doc, newIndent);
        int offset = lineStartOffset;
        for (int i = 0; i < newIndentString.length() && lineStartOffset + i < oldIndentEndOffset; ++i) {
            if (newIndentString.charAt(i) == docText.charAt(lineStartOffset + i)) continue;
            offset = lineStartOffset + i;
            newIndentString = newIndentString.substring(i);
            break;
        }
        if (offset < oldIndentEndOffset) {
            doc.remove(offset, oldIndentEndOffset - offset);
        }
        if (newIndentString.length() > 0) {
            doc.insertString(offset, newIndentString, null);
        }
    }

    public int caretOffset() {
        return this.mimeItem.handler().caretOffset();
    }

    public void setCaretOffset(int offset) throws BadLocationException {
        this.mimeItem.handler().setCaretOffset(offset);
    }

    public List<Region> indentRegions() {
        return this.mimeItem.indentRegions();
    }

    public boolean isIndent() {
        return this.mimeItem.handler().isIndent();
    }

    static {
        IndentSpiPackageAccessor.register(new PackageAccessor());
    }

    private static final class PackageAccessor
    extends IndentSpiPackageAccessor {
        private PackageAccessor() {
        }

        @Override
        public Context createContext(TaskHandler.MimeItem mimeItem) {
            return new Context(mimeItem);
        }

        @Override
        public Region createContextRegion(MutablePositionRegion region) {
            return new Region(region);
        }

        @Override
        public MutablePositionRegion positionRegion(Region region) {
            return region.positionRegion();
        }
    }

    public static final class Region {
        MutablePositionRegion region;

        Region(MutablePositionRegion region) {
            this.region = region;
        }

        public int getStartOffset() {
            return this.region.getStartOffset();
        }

        public int getEndOffset() {
            return this.region.getEndOffset();
        }

        MutablePositionRegion positionRegion() {
            return this.region;
        }
    }

}

