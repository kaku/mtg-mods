/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.indent.api;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.editor.indent.IndentImpl;

public final class Indent {
    private final IndentImpl impl;

    public static Indent get(Document doc) {
        IndentImpl indentImpl = IndentImpl.get(doc);
        Indent indent = indentImpl.getIndent();
        if (indent == null) {
            indent = new Indent(indentImpl);
            indentImpl.setIndent(indent);
        }
        return indent;
    }

    private Indent(IndentImpl impl) {
        this.impl = impl;
    }

    public void lock() {
        this.impl.indentLock();
    }

    public void unlock() {
        this.impl.indentUnlock();
    }

    public void reindent(int offset) throws BadLocationException {
        this.reindent(offset, offset);
    }

    public void reindent(int startOffset, int endOffset) throws BadLocationException {
        this.impl.reindent(startOffset, endOffset, startOffset, false);
    }

    public int indentNewLine(int offset) throws BadLocationException {
        return this.impl.reindent(offset, offset, offset, true);
    }
}

