/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.indent;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.indent.TaskHandler;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.editor.indent.api.Reformat;

public final class IndentImpl {
    private static final Logger LOG = Logger.getLogger(IndentImpl.class.getName());
    private final Document doc;
    private Indent indent;
    private Reformat reformat;
    private TaskHandler indentHandler;
    private TaskHandler reformatHandler;
    private Thread indentLockThread;
    private int indentLockExtraDepth;
    private final Object indentLock = new Object();
    private Thread reformatLockThread;
    private int reformatLockExtraDepth;
    private final Object reformatLock = new Object();

    public static IndentImpl get(Document doc) {
        IndentImpl indentImpl = (IndentImpl)doc.getProperty(IndentImpl.class);
        if (indentImpl == null) {
            indentImpl = new IndentImpl(doc);
            doc.putProperty(IndentImpl.class, indentImpl);
        }
        return indentImpl;
    }

    public IndentImpl(Document doc) {
        this.doc = doc;
    }

    public Document document() {
        return this.doc;
    }

    public Indent getIndent() {
        return this.indent;
    }

    public void setIndent(Indent indent) {
        this.indent = indent;
    }

    public Reformat getReformat() {
        return this.reformat;
    }

    public void setReformat(Reformat reformat) {
        this.reformat = reformat;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void indentLock() {
        Object object = this.indentLock;
        synchronized (object) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("indentLock() on " + this);
            }
            Thread currentThread = Thread.currentThread();
            while (this.indentLockThread != null) {
                if (currentThread == this.indentLockThread) {
                    ++this.indentLockExtraDepth;
                    return;
                }
                try {
                    this.indentLock.wait();
                    continue;
                }
                catch (InterruptedException e) {
                    throw new Error("Interrupted at acquiring indent-lock");
                }
            }
            this.indentLockThread = currentThread;
            this.indentHandler = new TaskHandler(true, this.doc);
            try {
                if (this.indentHandler.collectTasks()) {
                    this.indentHandler.lock();
                }
            }
            catch (Exception e) {
                this.indentUnlock();
                throw e;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void indentUnlock() {
        Object object = this.indentLock;
        synchronized (object) {
            Thread currentThread;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("indentUnlock() on " + this);
            }
            if ((currentThread = Thread.currentThread()) != this.indentLockThread) {
                throw new IllegalStateException("Invalid indentUnlock(): current-thread=" + currentThread + ", lockThread=" + this.indentLockThread + ", lockExtraDepth=" + this.indentLockExtraDepth);
            }
            if (this.indentLockExtraDepth == 0) {
                this.indentHandler.unlock();
                this.indentHandler = null;
                this.indentLockThread = null;
                this.indentLock.notifyAll();
            } else {
                --this.indentLockExtraDepth;
            }
        }
    }

    public TaskHandler indentHandler() {
        return this.indentHandler;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void reformatLock() {
        Object object = this.reformatLock;
        synchronized (object) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("reformatLock() on " + this);
            }
            Thread currentThread = Thread.currentThread();
            while (this.reformatLockThread != null) {
                if (currentThread == this.reformatLockThread) {
                    ++this.reformatLockExtraDepth;
                    return;
                }
                try {
                    this.reformatLock.wait();
                    continue;
                }
                catch (InterruptedException e) {
                    throw new Error("Interrupted at acquiring reformat-lock");
                }
            }
            this.reformatLockThread = currentThread;
            this.reformatHandler = new TaskHandler(false, this.doc);
            try {
                if (this.reformatHandler.collectTasks()) {
                    this.reformatHandler.lock();
                }
            }
            catch (Exception e) {
                this.reformatUnlock();
                throw e;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void reformatUnlock() {
        Object object = this.reformatLock;
        synchronized (object) {
            Thread currentThread;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("reformatUnlock() on " + this);
            }
            if ((currentThread = Thread.currentThread()) != this.reformatLockThread) {
                throw new IllegalStateException("Invalid reformatUnlock(): current-thread=" + currentThread + ", lockThread=" + this.reformatLockThread + ", lockExtraDepth=" + this.reformatLockExtraDepth);
            }
            if (this.reformatLockExtraDepth == 0) {
                this.reformatHandler.unlock();
                this.reformatHandler = null;
                this.reformatLockThread = null;
                this.reformatLock.notifyAll();
            } else {
                --this.reformatLockExtraDepth;
            }
        }
    }

    public TaskHandler reformatHandler() {
        return this.reformatHandler;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int reindent(int startOffset, int endOffset, int caretOffset, boolean indentNewLine) throws BadLocationException {
        if (startOffset > endOffset) {
            throw new IllegalArgumentException("startOffset=" + startOffset + " > endOffset=" + endOffset);
        }
        boolean runUnlocked = false;
        if (this.indentHandler == null) {
            LOG.log(Level.SEVERE, null, new Exception("Not locked. Use Indent.lock()."));
            runUnlocked = true;
            this.indentHandler = new TaskHandler(true, this.doc);
        }
        try {
            int startLineIndex;
            if (runUnlocked) {
                this.indentHandler.collectTasks();
            }
            this.indentHandler.setCaretOffset(caretOffset);
            Element lineRootElem = IndentImpl.lineRootElement(this.doc);
            if (this.indentHandler.hasItems()) {
                if (indentNewLine) {
                    this.doc.insertString(startOffset, "\n", null);
                    ++startOffset;
                    ++endOffset;
                    if (this.indentHandler.caretOffset() == 0) {
                        this.indentHandler.setCaretOffset(1);
                    }
                }
                startLineIndex = lineRootElem.getElementIndex(startOffset);
                Element lineElem = lineRootElem.getElement(startLineIndex);
                int startLineOffset = lineElem.getStartOffset();
                if (endOffset > lineElem.getEndOffset()) {
                    int endLineIndex = lineRootElem.getElementIndex(endOffset);
                    lineElem = lineRootElem.getElement(endLineIndex);
                    if (endLineIndex > 0 && lineElem.getStartOffset() == endOffset) {
                        lineElem = lineRootElem.getElement(--endLineIndex);
                    }
                }
                this.indentHandler.setGlobalBounds(this.doc.createPosition(startLineOffset), this.doc.createPosition(lineElem.getEndOffset() - 1));
                this.indentHandler.runTasks();
            }
            startLineIndex = this.indentHandler.caretOffset();
            return startLineIndex;
        }
        finally {
            if (runUnlocked) {
                this.indentHandler = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void reformat(int startOffset, int endOffset, int caretOffset) throws BadLocationException {
        if (startOffset > endOffset) {
            throw new IllegalArgumentException("startOffset=" + startOffset + " > endOffset=" + endOffset);
        }
        boolean runUnlocked = false;
        if (this.reformatHandler == null) {
            LOG.log(Level.SEVERE, "Not locked. Use Reformat.lock().", new Exception());
            runUnlocked = true;
            this.reformatHandler = new TaskHandler(false, this.doc);
        }
        try {
            if (runUnlocked) {
                this.reformatHandler.collectTasks();
            }
            this.reformatHandler.setCaretOffset(caretOffset);
            if (this.reformatHandler.hasItems()) {
                this.reformatHandler.setGlobalBounds(this.doc.createPosition(startOffset), this.doc.createPosition(endOffset));
                this.reformatHandler.runTasks();
            }
        }
        finally {
            if (runUnlocked) {
                this.reformatHandler = null;
            }
        }
    }

    public static Element lineRootElement(Document doc) {
        return doc instanceof StyledDocument ? ((StyledDocument)doc).getParagraphElement(0).getParentElement() : doc.getDefaultRootElement();
    }

    public static void checkOffsetInDocument(Document doc, int offset) throws BadLocationException {
        if (offset < 0) {
            throw new BadLocationException("offset=" + offset + " < 0", offset);
        }
        if (offset > doc.getLength()) {
            throw new BadLocationException("offset=" + offset + " > doc.getLength()=" + doc.getLength(), offset);
        }
    }
}

