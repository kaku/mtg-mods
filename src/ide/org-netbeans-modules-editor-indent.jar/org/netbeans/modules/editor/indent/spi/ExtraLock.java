/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.indent.spi;

public interface ExtraLock {
    public void lock();

    public void unlock();
}

