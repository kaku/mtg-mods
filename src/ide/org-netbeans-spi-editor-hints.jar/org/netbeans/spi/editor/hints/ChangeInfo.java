/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.editor.hints;

import java.util.ArrayList;
import java.util.List;
import javax.swing.text.Position;
import org.openide.filesystems.FileObject;

public final class ChangeInfo {
    private List<Change> changes = null;

    public ChangeInfo(FileObject fileObject, Position start, Position end) {
        this.add(fileObject, start, end);
    }

    public ChangeInfo(Position start, Position end) {
        this.add(null, start, end);
    }

    public ChangeInfo() {
    }

    public final int size() {
        return this.changes != null ? this.changes.size() : 0;
    }

    public final void add(FileObject fileObject, Position start, Position end) {
        if (this.changes == null) {
            this.changes = new ArrayList<Change>(5);
        }
        this.changes.add(new ChangeImpl(fileObject, start, end));
    }

    public final Change get(int i) {
        if (this.changes == null) {
            throw new ArrayIndexOutOfBoundsException("No changes");
        }
        return this.changes.get(i);
    }

    public String toString() {
        int size = this.size();
        if (size == 0) {
            return "Empty ChangeInfo";
        }
        StringBuffer sb = new StringBuffer(100);
        sb.append("ChangeInfo [");
        for (int i = 0; i < size; ++i) {
            sb.append(this.get(i));
            if (i == size - 1) continue;
            sb.append(',');
        }
        sb.append("]");
        return sb.toString();
    }

    private static final class ChangeImpl
    implements Change {
        Position start;
        Position end;
        FileObject fileObject;

        ChangeImpl(FileObject fileObject, Position start, Position end) {
            this.fileObject = fileObject;
            this.start = start;
            this.end = end;
        }

        @Override
        public Position getStart() {
            return this.start;
        }

        @Override
        public Position getEnd() {
            return this.end;
        }

        @Override
        public FileObject getFileObject() {
            return this.fileObject;
        }

        public String toString() {
            return "Change from " + this.start.getOffset() + " to " + this.end.getOffset() + " in " + (Object)this.fileObject;
        }
    }

    public static interface Change {
        public Position getStart();

        public Position getEnd();

        public FileObject getFileObject();
    }

}

