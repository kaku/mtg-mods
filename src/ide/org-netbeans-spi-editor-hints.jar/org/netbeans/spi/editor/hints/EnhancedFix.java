/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.hints;

import org.netbeans.spi.editor.hints.Fix;

public interface EnhancedFix
extends Fix {
    public CharSequence getSortText();
}

