/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.hints;

import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.modules.editor.hints.ContextAccessor;

public final class Context {
    final AtomicBoolean cancel;
    final int position;

    Context(int position, AtomicBoolean cancel) {
        this.position = position;
        this.cancel = cancel;
    }

    public boolean isCanceled() {
        return this.cancel.get();
    }

    public int getPosition() {
        return this.position;
    }

    public AtomicBoolean getCancel() {
        return this.cancel;
    }

    static {
        ContextAccessor.DEFAULT = new ContextAccessorImpl();
    }

    private static final class ContextAccessorImpl
    extends ContextAccessor {
        private ContextAccessorImpl() {
        }

        @Override
        public Context newContext(int position, AtomicBoolean cancel) {
            return new Context(position, cancel);
        }
    }

}

