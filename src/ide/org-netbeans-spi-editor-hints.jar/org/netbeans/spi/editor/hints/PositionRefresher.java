/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.hints;

import java.util.List;
import java.util.Map;
import javax.swing.text.Document;
import org.netbeans.spi.editor.hints.Context;
import org.netbeans.spi.editor.hints.ErrorDescription;

public interface PositionRefresher {
    public Map<String, List<ErrorDescription>> getErrorDescriptionsAt(Context var1, Document var2);
}

