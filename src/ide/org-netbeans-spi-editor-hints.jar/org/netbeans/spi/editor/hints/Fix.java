/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.hints;

import org.netbeans.spi.editor.hints.ChangeInfo;

public interface Fix {
    public String getText();

    public ChangeInfo implement() throws Exception;
}

