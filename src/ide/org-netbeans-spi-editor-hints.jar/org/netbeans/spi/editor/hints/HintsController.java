/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.spi.editor.hints;

import java.util.ArrayList;
import java.util.Collection;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.editor.hints.HintsControllerImpl;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public final class HintsController {
    private static RequestProcessor WORKER = new RequestProcessor("HintsController worker");

    private HintsController() {
    }

    public static void setErrors(final @NonNull Document doc, final @NonNull String layer, @NonNull Collection<? extends ErrorDescription> errors) {
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        Parameters.notNull((CharSequence)"layer", (Object)layer);
        Parameters.notNull((CharSequence)"errors", errors);
        final ArrayList<? extends ErrorDescription> errorsCopy = new ArrayList<ErrorDescription>(errors);
        WORKER.post(new Runnable(){

            @Override
            public void run() {
                HintsControllerImpl.setErrors(doc, layer, errorsCopy);
            }
        });
    }

    public static void setErrors(final @NonNull FileObject file, final @NonNull String layer, @NonNull Collection<? extends ErrorDescription> errors) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        Parameters.notNull((CharSequence)"layer", (Object)layer);
        Parameters.notNull((CharSequence)"errors", errors);
        final ArrayList<? extends ErrorDescription> errorsCopy = new ArrayList<ErrorDescription>(errors);
        WORKER.post(new Runnable(){

            @Override
            public void run() {
                HintsControllerImpl.setErrors(file, layer, errorsCopy);
            }
        });
    }

}

