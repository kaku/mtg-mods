/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.spi.editor.hints.settings;

import java.util.Collection;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.event.ChangeListener;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.editor.hints.settings.friend.FileHintPreferencesProvider;
import org.openide.filesystems.FileObject;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public class FileHintPreferences {
    private static final ChangeSupport cs = new ChangeSupport(FileHintPreferences.class);
    private static final RequestProcessor FIRE_WORKER = new RequestProcessor(FileHintPreferences.class.getName(), 1, false, false);

    public static Preferences getFilePreferences(FileObject file, String preferencesMimeType) {
        Preferences prefs;
        for (Object p2 : Lookup.getDefault().lookupAll(FileHintPreferencesProvider.class)) {
            prefs = p2.getFilePreferences(file, preferencesMimeType);
            if (prefs == null) continue;
            return prefs;
        }
        for (Object p2 : MimeLookup.getLookup((String)preferencesMimeType).lookupAll(GlobalHintPreferencesProvider.class)) {
            prefs = p2.getGlobalPreferences();
            if (prefs == null) continue;
            return prefs;
        }
        throw new IllegalStateException("Must have some working GlobalHintPreferencesProvider!");
    }

    public static void addChangeListener(ChangeListener l) {
        cs.addChangeListener(l);
    }

    public static void removeChangeListener(ChangeListener l) {
        cs.removeChangeListener(l);
    }

    public static void fireChange() {
        cs.fireChange();
    }

    private static void fireChangeEventually() {
        FIRE_WORKER.post(new Runnable(){

            @Override
            public void run() {
                FileHintPreferences.fireChange();
            }
        });
    }

    private static final class WrapperPreferences
    extends AbstractPreferences {
        private final Preferences delegate;

        public WrapperPreferences(WrapperPreferences parent, String name, Preferences delegate) {
            super(parent, name);
            this.delegate = delegate;
        }

        @Override
        protected void putSpi(String key, String value) {
            this.delegate.put(key, value);
            FileHintPreferences.fireChangeEventually();
        }

        @Override
        protected String getSpi(String key) {
            return this.delegate.get(key, null);
        }

        @Override
        protected void removeSpi(String key) {
            this.delegate.remove(key);
            FileHintPreferences.fireChangeEventually();
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            this.delegate.removeNode();
            FileHintPreferences.fireChangeEventually();
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            return this.delegate.keys();
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            return this.delegate.childrenNames();
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            return new WrapperPreferences(this, name, this.delegate.node(name));
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            this.delegate.sync();
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            this.delegate.flush();
        }
    }

    public static interface GlobalHintPreferencesProvider {
        public Preferences getGlobalPreferences();
    }

}

