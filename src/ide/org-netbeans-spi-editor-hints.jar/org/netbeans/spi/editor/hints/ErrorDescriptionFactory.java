/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.text.PositionBounds
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.editor.hints;

import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.editor.hints.HintsControllerImpl;
import org.netbeans.modules.editor.hints.StaticFixList;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.LazyFixList;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.PositionBounds;
import org.openide.util.Parameters;

public class ErrorDescriptionFactory {
    private ErrorDescriptionFactory() {
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull Document doc, int lineNumber) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        return ErrorDescriptionFactory.createErrorDescription(severity, description, new StaticFixList(), doc, lineNumber);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull List<Fix> fixes, @NonNull Document doc, int lineNumber) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", fixes);
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        return ErrorDescriptionFactory.createErrorDescription(severity, description, new StaticFixList(fixes), doc, lineNumber);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull LazyFixList fixes, @NonNull Document doc, int lineNumber) {
        return ErrorDescriptionFactory.createErrorDescription(null, severity, description, null, fixes, doc, lineNumber);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NullAllowed String id, @NonNull Severity severity, @NonNull String description, @NullAllowed CharSequence details, @NonNull LazyFixList fixes, @NonNull Document doc, int lineNumber) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", (Object)fixes);
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        DataObject od = (DataObject)doc.getProperty("stream");
        FileObject file = od != null ? od.getPrimaryFile() : null;
        return new ErrorDescription(file, id, description, details, severity, fixes, HintsControllerImpl.fullLine(doc, lineNumber));
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull Document doc, @NonNull Position start, @NonNull Position end) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        Parameters.notNull((CharSequence)"start", (Object)start);
        Parameters.notNull((CharSequence)"end", (Object)end);
        return ErrorDescriptionFactory.createErrorDescription(severity, description, (LazyFixList)new StaticFixList(), doc, start, end);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull List<Fix> fixes, @NonNull Document doc, @NonNull Position start, @NonNull Position end) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", fixes);
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        Parameters.notNull((CharSequence)"start", (Object)start);
        Parameters.notNull((CharSequence)"end", (Object)end);
        return ErrorDescriptionFactory.createErrorDescription(severity, description, (LazyFixList)new StaticFixList(fixes), doc, start, end);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull LazyFixList fixes, @NonNull Document doc, @NonNull Position start, @NonNull Position end) {
        return ErrorDescriptionFactory.createErrorDescription(null, severity, description, null, fixes, doc, start, end);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NullAllowed String id, @NonNull Severity severity, @NonNull String description, @NullAllowed CharSequence details, @NonNull LazyFixList fixes, @NonNull Document doc, @NonNull Position start, @NonNull Position end) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", (Object)fixes);
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        Parameters.notNull((CharSequence)"start", (Object)start);
        Parameters.notNull((CharSequence)"end", (Object)end);
        DataObject od = (DataObject)doc.getProperty("stream");
        FileObject file = od != null ? od.getPrimaryFile() : null;
        return new ErrorDescription(file, id, description, details, severity, fixes, HintsControllerImpl.linePart(doc, start, end));
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull FileObject file, int start, int end) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"file", (Object)file);
        if (start < 0) {
            throw new IndexOutOfBoundsException("start < 0 (" + start + " < 0)");
        }
        if (end < start) {
            throw new IndexOutOfBoundsException("end < start (" + end + " < " + start + ")");
        }
        return ErrorDescriptionFactory.createErrorDescription(severity, description, (LazyFixList)new StaticFixList(), file, start, end);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull List<Fix> fixes, @NonNull FileObject file, int start, int end) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", fixes);
        Parameters.notNull((CharSequence)"file", (Object)file);
        if (start < 0) {
            throw new IndexOutOfBoundsException("start < 0 (" + start + " < 0)");
        }
        if (end < start) {
            throw new IndexOutOfBoundsException("end < start (" + end + " < " + start + ")");
        }
        return ErrorDescriptionFactory.createErrorDescription(severity, description, (LazyFixList)new StaticFixList(fixes), file, start, end);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NonNull Severity severity, @NonNull String description, @NonNull LazyFixList fixes, @NonNull FileObject file, int start, int end) {
        return ErrorDescriptionFactory.createErrorDescription(null, severity, description, null, fixes, file, start, end);
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NullAllowed String id, @NonNull Severity severity, @NonNull String description, @NullAllowed CharSequence details, @NonNull LazyFixList fixes, @NonNull FileObject file, int start, int end) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", (Object)fixes);
        Parameters.notNull((CharSequence)"file", (Object)file);
        if (start < 0) {
            throw new IndexOutOfBoundsException("start < 0 (" + start + " < 0)");
        }
        if (end < start) {
            throw new IndexOutOfBoundsException("end < start (" + end + " < " + start + ")");
        }
        return new ErrorDescription(file, id, description, details, severity, fixes, HintsControllerImpl.linePart(file, start, end));
    }

    @NonNull
    public static ErrorDescription createErrorDescription(@NullAllowed String id, @NonNull Severity severity, @NonNull String description, @NullAllowed CharSequence details, @NonNull LazyFixList fixes, @NonNull FileObject file, @NonNull PositionBounds errorBounds) {
        Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
        Parameters.notNull((CharSequence)"description", (Object)description);
        Parameters.notNull((CharSequence)"fixes", (Object)fixes);
        Parameters.notNull((CharSequence)"file", (Object)file);
        return new ErrorDescription(file, id, description, details, severity, fixes, errorBounds);
    }

    @NonNull
    public static LazyFixList lazyListForFixes(@NonNull List<Fix> fixes) {
        Parameters.notNull((CharSequence)"fixes", fixes);
        return new StaticFixList(fixes);
    }

    @NonNull
    public static LazyFixList lazyListForDelegates(@NonNull List<LazyFixList> delegates) {
        Parameters.notNull((CharSequence)"delegates", delegates);
        return new HintsControllerImpl.CompoundLazyFixList(delegates);
    }

    @NonNull
    public static Fix attachSubfixes(@NonNull Fix to, @NonNull Iterable<? extends Fix> subfixes) {
        Parameters.notNull((CharSequence)"to", (Object)to);
        Parameters.notNull((CharSequence)"subfixes", subfixes);
        HintsControllerImpl.attachSubfixes(to, subfixes);
        return to;
    }
}

