/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.openide.filesystems.FileObject
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 */
package org.netbeans.spi.editor.hints;

import java.io.IOException;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.spi.editor.hints.LazyFixList;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.filesystems.FileObject;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;

public final class ErrorDescription {
    private final String id;
    private final String description;
    private final CharSequence details;
    private final Severity severity;
    private final LazyFixList fixes;
    private final PositionBounds span;
    private final FileObject file;

    ErrorDescription(FileObject file, String id, String description, CharSequence details, Severity severity, LazyFixList fixes, PositionBounds span) {
        this.id = id;
        this.description = description;
        this.details = details;
        this.severity = severity;
        this.fixes = fixes;
        this.span = span;
        this.file = file;
    }

    @CheckForNull
    public String getId() {
        return this.id;
    }

    public String getDescription() {
        return this.description;
    }

    @CheckForNull
    public CharSequence getDetails() {
        return this.details;
    }

    public Severity getSeverity() {
        return this.severity;
    }

    public LazyFixList getFixes() {
        return this.fixes;
    }

    public PositionBounds getRange() {
        return this.span;
    }

    public FileObject getFile() {
        return this.file;
    }

    public String toString() {
        try {
            return (this.span != null ? new StringBuilder().append(this.span.getBegin().getLine()).append(":").append(this.span.getBegin().getColumn()).append("-").append(this.span.getEnd().getLine()).append(":").append(this.span.getEnd().getColumn()).toString() : "<no-span>") + ":" + this.severity.getDisplayName() + ":" + this.description;
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ErrorDescription other = (ErrorDescription)obj;
        if (this.description == null ? other.description != null : !this.description.equals(other.description)) {
            return false;
        }
        if (!(this.severity == other.severity || this.severity != null && this.severity.equals((Object)other.severity))) {
            return false;
        }
        if (this.span != null && other.span != null) {
            if (this.span.getBegin().getOffset() != other.span.getBegin().getOffset()) {
                return false;
            }
            if (this.span.getEnd().getOffset() != other.span.getEnd().getOffset()) {
                return false;
            }
        } else if (this.span != other.span) {
            return false;
        }
        if (!(this.file == other.file || this.file != null && this.file.equals((Object)other.file))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 17 * hash + (this.severity != null ? this.severity.hashCode() : 0);
        hash = 17 * hash + (this.span != null ? this.span.getBegin().getOffset() : 0);
        hash = 17 * hash + (this.span != null ? this.span.getEnd().getOffset() : 0);
        hash = 17 * hash + (this.file != null ? this.file.hashCode() : 0);
        return hash;
    }
}

