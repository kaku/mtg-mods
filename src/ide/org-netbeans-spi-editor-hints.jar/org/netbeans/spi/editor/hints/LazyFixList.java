/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.hints;

import java.beans.PropertyChangeListener;
import java.util.List;
import org.netbeans.spi.editor.hints.Fix;

public interface LazyFixList {
    public static final String PROP_FIXES = "fixes";
    public static final String PROP_COMPUTED = "computed";

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);

    public boolean probablyContainsFixes();

    public List<Fix> getFixes();

    public boolean isComputed();
}

