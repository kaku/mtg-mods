/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.hints;

public enum Severity {
    ERROR("error"),
    WARNING("warning"),
    VERIFIER("verifier"),
    HINT("hint");
    
    private String displayName;

    String getDisplayName() {
        return this.displayName;
    }

    private Severity(String displayName) {
        this.displayName = displayName;
    }
}

