/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.EditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.hints;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.hints.AnnotationHolder;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.LazyFixList;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.EditorSupport;
import org.openide.text.NbDocument;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;

public final class HintsControllerImpl {
    private static final Logger LOG = Logger.getLogger(HintsControllerImpl.class.getName());
    private static List<ChangeListener> listeners = new ArrayList<ChangeListener>();
    private static final Map<Reference<Fix>, Iterable<? extends Fix>> fix2Subfixes = new HashMap<Reference<Fix>, Iterable<? extends Fix>>();

    private HintsControllerImpl() {
    }

    public static void setErrors(Document doc, String layer, Collection<? extends ErrorDescription> errors) {
        DataObject od = (DataObject)doc.getProperty("stream");
        if (od == null) {
            return;
        }
        try {
            HintsControllerImpl.setErrorsImpl(od.getPrimaryFile(), layer, errors);
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    public static void setErrors(FileObject file, String layer, Collection<? extends ErrorDescription> errors) {
        try {
            HintsControllerImpl.setErrorsImpl(file, layer, errors);
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
    }

    private static void setErrorsImpl(FileObject file, String layer, Collection<? extends ErrorDescription> errors) throws IOException {
        AnnotationHolder holder = AnnotationHolder.getInstance(file);
        if (holder != null) {
            holder.setErrorDescriptions(layer, errors);
        }
    }

    private static void computeLineSpan(Document doc, int[] offsets) throws BadLocationException {
        int column;
        String text = doc.getText(offsets[0], offsets[1] - offsets[0]);
        int length = text.length();
        for (column = 0; column < text.length() && Character.isWhitespace(text.charAt(column)); ++column) {
        }
        while (length > 0 && Character.isWhitespace(text.charAt(length - 1))) {
            --length;
        }
        offsets[1] = offsets[0] + length;
        int[] arrn = offsets;
        arrn[0] = arrn[0] + column;
        if (offsets[1] < offsets[0]) {
            offsets[0] = offsets[1];
        }
    }

    static int[] computeLineSpan(Document doc, int lineNumber) throws BadLocationException {
        int lineEndOffset;
        lineNumber = Math.min(lineNumber, NbDocument.findLineRootElement((StyledDocument)((StyledDocument)doc)).getElementCount());
        int lineStartOffset = NbDocument.findLineOffset((StyledDocument)((StyledDocument)doc), (int)(lineNumber - 1));
        if (doc instanceof BaseDocument) {
            lineEndOffset = Utilities.getRowEnd((BaseDocument)((BaseDocument)doc), (int)lineStartOffset);
        } else {
            String lineText = doc.getText(lineStartOffset, doc.getLength() - lineStartOffset);
            lineText = lineText.indexOf(10) != -1 ? lineText.substring(0, lineText.indexOf(10)) : lineText;
            lineEndOffset = lineStartOffset + lineText.length();
        }
        int[] span = new int[]{lineStartOffset, lineEndOffset};
        HintsControllerImpl.computeLineSpan(doc, span);
        return span;
    }

    public static PositionBounds fullLine(final Document doc, final int lineNumber) {
        final PositionBounds[] result = new PositionBounds[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                result[0] = HintsControllerImpl.fullLineImpl(doc, lineNumber);
            }
        });
        return result[0];
    }

    private static PositionBounds fullLineImpl(Document doc, int lineNumber) {
        DataObject file = (DataObject)doc.getProperty("stream");
        if (file == null) {
            return null;
        }
        try {
            int[] span = HintsControllerImpl.computeLineSpan(doc, lineNumber);
            return HintsControllerImpl.linePart(file.getPrimaryFile(), span[0], span[1]);
        }
        catch (BadLocationException e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
    }

    public static PositionBounds linePart(Document doc, final Position start, final Position end) {
        DataObject od = (DataObject)doc.getProperty("stream");
        if (od == null) {
            return null;
        }
        EditorCookie ec = (EditorCookie)od.getCookie(EditorCookie.class);
        if (ec instanceof CloneableEditorSupport) {
            final CloneableEditorSupport ces = (CloneableEditorSupport)ec;
            final PositionRef[] refs = new PositionRef[2];
            doc.render(new Runnable(){

                @Override
                public void run() {
                    HintsControllerImpl.checkOffsetsAndLog(start.getOffset(), end.getOffset());
                    refs[0] = ces.createPositionRef(start.getOffset(), Position.Bias.Forward);
                    refs[1] = ces.createPositionRef(end.getOffset(), Position.Bias.Backward);
                }
            });
            return new PositionBounds(refs[0], refs[1]);
        }
        if (ec instanceof EditorSupport) {
            final EditorSupport es = (EditorSupport)ec;
            final PositionRef[] refs = new PositionRef[2];
            doc.render(new Runnable(){

                @Override
                public void run() {
                    HintsControllerImpl.checkOffsetsAndLog(start.getOffset(), end.getOffset());
                    refs[0] = es.createPositionRef(start.getOffset(), Position.Bias.Forward);
                    refs[1] = es.createPositionRef(end.getOffset(), Position.Bias.Backward);
                }
            });
            return new PositionBounds(refs[0], refs[1]);
        }
        return null;
    }

    public static PositionBounds linePart(FileObject file, int start, int end) {
        try {
            DataObject od = DataObject.find((FileObject)file);
            if (od == null) {
                return null;
            }
            EditorCookie ec = (EditorCookie)od.getCookie(EditorCookie.class);
            if (!(ec instanceof CloneableEditorSupport)) {
                return null;
            }
            CloneableEditorSupport ces = (CloneableEditorSupport)ec;
            HintsControllerImpl.checkOffsetsAndLog(start, end);
            return new PositionBounds(ces.createPositionRef(start, Position.Bias.Forward), ces.createPositionRef(end, Position.Bias.Backward));
        }
        catch (IOException e) {
            LOG.log(Level.INFO, null, e);
            return null;
        }
    }

    private static void checkOffsetsAndLog(int start, int end) {
        if (start <= end) {
            return;
        }
        Logger.getLogger(HintsControllerImpl.class.getName()).log(Level.INFO, "Incorrect span, please attach your messages.log to issue #112566. start=" + start + ", end=" + end, new Exception());
    }

    public static synchronized void addChangeListener(ChangeListener l) {
        listeners.add(l);
    }

    public static synchronized void removeChangeListener(ChangeListener l) {
        listeners.remove(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void attachSubfixes(Fix fix, Iterable<? extends Fix> subfixes) {
        Map<Reference<Fix>, Iterable<? extends Fix>> map = fix2Subfixes;
        synchronized (map) {
            fix2Subfixes.put(new CleaningReference(fix), subfixes);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Iterable<? extends Fix> getSubfixes(Fix fix) {
        Map<Reference<Fix>, Iterable<? extends Fix>> map = fix2Subfixes;
        synchronized (map) {
            Iterable<? extends Fix> ret = fix2Subfixes.get(new CleaningReference(fix));
            return ret != null ? ret : Collections.emptyList();
        }
    }

    public static class CompoundLazyFixList
    implements LazyFixList,
    PropertyChangeListener {
        final List<LazyFixList> delegates;
        private List<Fix> fixesCache;
        private Boolean computedCache;
        private Boolean probablyContainsFixesCache;
        private PropertyChangeSupport pcs;

        public CompoundLazyFixList(List<LazyFixList> delegates) {
            this.delegates = delegates;
            this.pcs = new PropertyChangeSupport(this);
            for (LazyFixList l : delegates) {
                l.addPropertyChangeListener(this);
            }
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener l) {
            this.pcs.addPropertyChangeListener(l);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener l) {
            this.pcs.removePropertyChangeListener(l);
        }

        @Override
        public synchronized boolean probablyContainsFixes() {
            if (this.probablyContainsFixesCache == null) {
                boolean result = false;
                for (LazyFixList l : this.delegates) {
                    result |= l.probablyContainsFixes();
                }
                this.probablyContainsFixesCache = result;
            }
            return this.probablyContainsFixesCache;
        }

        @Override
        public synchronized List<Fix> getFixes() {
            if (this.fixesCache == null) {
                this.fixesCache = new ArrayList<Fix>();
                for (LazyFixList l : this.delegates) {
                    this.fixesCache.addAll(l.getFixes());
                }
            }
            return this.fixesCache;
        }

        @Override
        public synchronized boolean isComputed() {
            if (this.computedCache == null) {
                boolean result = true;
                for (LazyFixList l : this.delegates) {
                    result &= l.isComputed();
                }
                this.computedCache = result;
            }
            return this.computedCache;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("fixes".equals(evt.getPropertyName())) {
                CompoundLazyFixList compoundLazyFixList = this;
                synchronized (compoundLazyFixList) {
                    this.fixesCache = null;
                }
                this.pcs.firePropertyChange("fixes", null, null);
                return;
            }
            if ("computed".equals(evt.getPropertyName())) {
                CompoundLazyFixList compoundLazyFixList = this;
                synchronized (compoundLazyFixList) {
                    this.computedCache = null;
                }
                this.pcs.firePropertyChange("computed", null, null);
            }
        }
    }

    private static final class CleaningReference
    extends WeakReference<Fix>
    implements Runnable {
        private final int hashCode;

        public CleaningReference(Fix referent) {
            super(referent, org.openide.util.Utilities.activeReferenceQueue());
            this.hashCode = System.identityHashCode(referent);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Map map = fix2Subfixes;
            synchronized (map) {
                Iterator it = fix2Subfixes.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e = it.next();
                    if (e.getKey() != this) continue;
                    it.remove();
                    return;
                }
            }
        }

        public boolean equals(Object obj) {
            Fix thatFix;
            if (!(obj instanceof Reference)) {
                return false;
            }
            Reference that = (Reference)obj;
            Fix thisFix = (Fix)this.get();
            return thisFix == (thatFix = (Fix)that.get());
        }

        public int hashCode() {
            return this.hashCode;
        }
    }

}

