/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.hints.settings.friend;

import java.util.prefs.Preferences;
import org.openide.filesystems.FileObject;

public interface FileHintPreferencesProvider {
    public Preferences getFilePreferences(FileObject var1, String var2);
}

