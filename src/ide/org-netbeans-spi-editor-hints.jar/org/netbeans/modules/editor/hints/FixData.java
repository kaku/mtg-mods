/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.hints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.editor.hints.HintsControllerImpl;
import org.netbeans.spi.editor.hints.EnhancedFix;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.LazyFixList;

public class FixData
extends HintsControllerImpl.CompoundLazyFixList {
    private static final String DEFAULT_SORT_TEXT = "\uffff";

    public FixData(LazyFixList errorFixes, LazyFixList otherFixes) {
        super(Arrays.asList(errorFixes, otherFixes));
    }

    public List<Fix> getSortedFixes() {
        LazyFixList errorFixes = (LazyFixList)this.delegates.get(0);
        LazyFixList otherFixes = (LazyFixList)this.delegates.get(1);
        LinkedList<Fix> result = new LinkedList<Fix>();
        result.addAll(this.sortFixes(new LinkedHashSet<Fix>(errorFixes.getFixes())));
        result.addAll(this.sortFixes(new LinkedHashSet<Fix>(otherFixes.getFixes())));
        return result;
    }

    private List<Fix> sortFixes(Collection<Fix> fixes) {
        ArrayList<Fix> result = new ArrayList<Fix>(fixes);
        Collections.sort(result, new FixComparator());
        return result;
    }

    private static CharSequence getSortText(Fix f) {
        if (f instanceof EnhancedFix) {
            return ((EnhancedFix)f).getSortText();
        }
        return "\uffff";
    }

    private static int compareText(CharSequence text1, CharSequence text2) {
        int len = Math.min(text1.length(), text2.length());
        for (int i = 0; i < len; ++i) {
            char ch2;
            char ch1 = text1.charAt(i);
            if (ch1 == (ch2 = text2.charAt(i))) continue;
            return ch1 - ch2;
        }
        return text1.length() - text2.length();
    }

    private static final class FixComparator
    implements Comparator<Fix> {
        private FixComparator() {
        }

        @Override
        public int compare(Fix o1, Fix o2) {
            return FixData.compareText(FixData.getSortText(o1), FixData.getSortText(o2));
        }
    }

}

