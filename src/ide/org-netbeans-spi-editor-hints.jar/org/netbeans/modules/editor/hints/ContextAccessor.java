/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.hints;

import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.spi.editor.hints.Context;

public abstract class ContextAccessor {
    public static ContextAccessor DEFAULT;

    public static ContextAccessor getDefault() {
        block4 : {
            if (DEFAULT != null) {
                return DEFAULT;
            }
            Class<Context> c = Context.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block4;
                throw new AssertionError(ex);
            }
        }
        assert (DEFAULT != null);
        return DEFAULT;
    }

    public abstract Context newContext(int var1, AtomicBoolean var2);
}

