/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.Annotations
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.GuardedException
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.StatusBar
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.highlighting.HighlightingManager
 *  org.netbeans.spi.editor.highlighting.HighlightAttributeValue
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.openide.ErrorManager
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.StatusDisplayer$Message
 *  org.openide.cookies.EditCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.Annotation
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.hints;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.Utilities;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.StatusBar;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.hints.AnnotationHolder;
import org.netbeans.modules.editor.hints.ContextAccessor;
import org.netbeans.modules.editor.hints.FixData;
import org.netbeans.modules.editor.hints.ParseErrorAnnotation;
import org.netbeans.modules.editor.hints.borrowed.ListCompletionView;
import org.netbeans.modules.editor.hints.borrowed.ScrollCompletionPane;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.spi.editor.highlighting.HighlightAttributeValue;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.hints.ChangeInfo;
import org.netbeans.spi.editor.hints.Context;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.LazyFixList;
import org.netbeans.spi.editor.hints.PositionRefresher;
import org.openide.ErrorManager;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.EditCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.Annotation;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;

public final class HintsUI
implements MouseListener,
MouseMotionListener,
KeyListener,
PropertyChangeListener,
AWTEventListener,
CaretListener,
FocusListener {
    private static final boolean ALWAYS_SHOW_ERROR_MESSAGE = Boolean.getBoolean(HintsUI.class.getName() + ".always.show.error");
    private static HintsUI INSTANCE;
    private static final Set<String> fixableAnnotations;
    private static final String POPUP_NAME = "hintsPopup";
    private static final String SUB_POPUP_NAME = "subHintsPopup";
    private static final int POPUP_VERTICAL_OFFSET = 5;
    private static final RequestProcessor WORKER;
    static final Logger UI_GESTURES_LOGGER;
    private Reference<JTextComponent> compRef;
    private Popup listPopup;
    private Popup sublistPopup;
    private Popup tooltipPopup;
    private JLabel hintIcon;
    private ScrollCompletionPane hintListComponent;
    private ScrollCompletionPane subhintListComponent;
    private JTextArea errorTooltip;
    private AtomicBoolean cancel;
    private boolean altEnterPressed = false;
    private boolean altReleased = false;
    private PopupFactory pf = null;
    private ChangeInfo changes;
    private static String[] c;
    private static String[] tags;

    public static synchronized HintsUI getDefault() {
        if (INSTANCE == null) {
            INSTANCE = new HintsUI();
        }
        return INSTANCE;
    }

    private HintsUI() {
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)this);
        this.propertyChange(null);
        this.cancel = new AtomicBoolean(false);
    }

    public JTextComponent getComponent() {
        return this.compRef == null ? null : this.compRef.get();
    }

    public void removeHints() {
        this.removePopups();
        this.setComponent(null);
    }

    public void setComponent(JTextComponent comp) {
        boolean change;
        JTextComponent thisComp = this.getComponent();
        boolean bl = change = thisComp != comp;
        if (change) {
            this.unregister();
            this.compRef = new WeakReference<JTextComponent>(comp);
            this.register();
            this.caretUpdate(null);
        }
    }

    private AnnotationHolder getAnnotationHolder(Document doc) {
        DataObject od = (DataObject)doc.getProperty("stream");
        if (od == null) {
            return null;
        }
        return AnnotationHolder.getInstance(od.getPrimaryFile());
    }

    private void register() {
        JTextComponent comp = this.getComponent();
        if (comp == null) {
            return;
        }
        comp.addKeyListener(this);
        comp.addCaretListener(this);
    }

    private void unregister() {
        JTextComponent comp = this.getComponent();
        if (comp == null) {
            return;
        }
        comp.removeKeyListener(this);
        comp.removeCaretListener(this);
    }

    public void removePopups() {
        JTextComponent comp = this.getComponent();
        if (comp == null) {
            return;
        }
        this.removeIconHint();
        this.removePopup();
    }

    private void removeIconHint() {
        Container cont;
        if (this.hintIcon != null && (cont = this.hintIcon.getParent()) != null) {
            Rectangle bds = this.hintIcon.getBounds();
            cont.remove(this.hintIcon);
            cont.repaint(bds.x, bds.y, bds.width, bds.height);
        }
    }

    private void removePopup() {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        if (this.listPopup != null) {
            this.closeSubList();
            if (this.tooltipPopup != null) {
                this.tooltipPopup.hide();
            }
            this.tooltipPopup = null;
            this.listPopup.hide();
            if (this.hintListComponent != null) {
                this.hintListComponent.getView().removeMouseListener(this);
                this.hintListComponent.getView().removeMouseMotionListener(this);
            }
            if (this.errorTooltip != null) {
                this.errorTooltip.removeMouseListener(this);
            }
            this.hintListComponent = null;
            this.errorTooltip = null;
            this.listPopup = null;
            if (this.hintIcon != null) {
                this.hintIcon.setToolTipText(NbBundle.getMessage(HintsUI.class, (String)"HINT_Bulb"));
            }
        }
    }

    public void closeSubList() {
        if (this.sublistPopup != null) {
            this.sublistPopup.hide();
            if (this.subhintListComponent != null) {
                this.subhintListComponent.getView().removeMouseListener(this);
                this.subhintListComponent.getView().removeMouseMotionListener(this);
            }
            this.subhintListComponent = null;
            this.sublistPopup = null;
        }
    }

    public void openSubList(Iterable<? extends Fix> fixes, Point p) {
        JTextComponent comp = this.getComponent();
        if (comp == null) {
            return;
        }
        if (this.subhintListComponent != null) {
            this.closeSubList();
        }
        LinkedList<Fix> ff = new LinkedList<Fix>();
        for (Fix f : fixes) {
            ff.add(f);
        }
        Rectangle maxSize = this.getScreenBounds();
        maxSize.width -= p.x;
        maxSize.height -= p.y;
        this.subhintListComponent = new ScrollCompletionPane(comp, new FixData(ErrorDescriptionFactory.lazyListForFixes(ff), ErrorDescriptionFactory.lazyListForFixes(Arrays.asList(new Fix[0]))), null, null, new Dimension(maxSize.width, maxSize.height));
        this.subhintListComponent.getView().addMouseListener(this);
        this.subhintListComponent.getView().addMouseMotionListener(this);
        this.subhintListComponent.setName("subHintsPopup");
        assert (this.sublistPopup == null);
        this.sublistPopup = this.getPopupFactory().getPopup(comp, this.subhintListComponent, p.x, p.y);
        this.sublistPopup.show();
    }

    boolean isKnownComponent(Component c) {
        JTextComponent comp = this.getComponent();
        return c != null && (c == comp || c == this.hintIcon || c == this.hintListComponent || c instanceof Container && ((Container)c).isAncestorOf(this.hintListComponent));
    }

    public void showPopup(FixData hints) {
        JTextComponent comp = this.getComponent();
        if (comp == null || hints.isComputed() && hints.getFixes().isEmpty()) {
            return;
        }
        if (this.hintIcon != null) {
            this.hintIcon.setToolTipText(null);
        }
        ToolTipManager.sharedInstance().setEnabled(false);
        ToolTipManager.sharedInstance().setEnabled(true);
        assert (this.hintListComponent == null);
        Toolkit.getDefaultToolkit().addAWTEventListener(this, 16);
        try {
            int pos = Utilities.getRowStart(comp, comp.getCaret().getDot());
            Rectangle r = comp.modelToView(pos);
            Point p = new Point(r.x + 5, r.y + 20);
            SwingUtilities.convertPointToScreen(p, comp);
            Rectangle maxSize = this.getScreenBounds();
            maxSize.width -= p.x;
            maxSize.height -= p.y;
            this.hintListComponent = new ScrollCompletionPane(comp, hints, null, null, new Dimension(maxSize.width, maxSize.height));
            this.hintListComponent.getView().addMouseListener(this);
            this.hintListComponent.getView().addMouseMotionListener(this);
            this.hintListComponent.setName("hintsPopup");
            assert (this.listPopup == null);
            this.listPopup = this.getPopupFactory().getPopup(comp, this.hintListComponent, p.x, p.y);
            this.listPopup.show();
        }
        catch (BadLocationException ble) {
            ErrorManager.getDefault().notify((Throwable)ble);
            this.removeHints();
        }
    }

    public void showPopup(FixData fixes, String description, JTextComponent component, Point position) {
        this.removeHints();
        this.setComponent(component);
        JTextComponent comp = this.getComponent();
        if (comp == null || fixes == null) {
            return;
        }
        Point p = new Point(position);
        SwingUtilities.convertPointToScreen(p, comp);
        if (this.hintIcon != null) {
            this.hintIcon.setToolTipText(null);
        }
        ToolTipManager.sharedInstance().setEnabled(false);
        ToolTipManager.sharedInstance().setEnabled(true);
        Toolkit.getDefaultToolkit().addAWTEventListener(this, 16);
        Rectangle screen = this.getScreenBounds();
        Dimension screenDim = new Dimension(screen.width, screen.height);
        this.errorTooltip = new JTextArea(description);
        this.errorTooltip.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        this.errorTooltip.setEditable(false);
        Dimension pref = this.errorTooltip.getPreferredSize();
        int usableWidth = this.getUsableWidth(component);
        if (pref.width > usableWidth) {
            this.errorTooltip.setLineWrap(true);
            this.errorTooltip.setWrapStyleWord(true);
            this.errorTooltip.setSize(new Dimension(usableWidth, screenDim.height));
            pref = this.errorTooltip.getPreferredSize();
        }
        if (pref.height > screenDim.height) {
            pref.height = screenDim.height;
        }
        this.errorTooltip.setSize(pref);
        this.errorTooltip.addMouseListener(this);
        if (!fixes.isComputed() || fixes.getFixes().isEmpty()) {
            assert (this.listPopup == null);
            this.listPopup = this.getPopupFactory().getPopup(comp, this.errorTooltip, p.x, p.y);
        } else {
            boolean placeUp;
            assert (this.hintListComponent == null);
            int rowHeight = 14;
            this.hintListComponent = new ScrollCompletionPane(comp, fixes, null, null, screenDim);
            Dimension hintPopup = this.hintListComponent.getPreferredSize();
            int ySpaceWhenPlacedUp = p.y - (rowHeight + 5);
            boolean exceedsHeight = p.y + hintPopup.height > screen.height;
            boolean bl = placeUp = exceedsHeight && ySpaceWhenPlacedUp > screenDim.height - p.y;
            if (ALWAYS_SHOW_ERROR_MESSAGE) {
                try {
                    int pos = Utilities.getRowStart(comp, comp.getCaret().getDot());
                    Rectangle r = comp.modelToView(pos);
                    rowHeight = r.height;
                    Dimension errorPopup = this.errorTooltip.getPreferredSize();
                    int y = placeUp ? p.y + 5 : p.y - rowHeight - errorPopup.height - 5;
                    int xPos = p.x;
                    if (p.x - screen.x + errorPopup.width > screen.width) {
                        xPos -= p.x - screen.x + errorPopup.width - screen.width;
                    }
                    this.tooltipPopup = this.getPopupFactory().getPopup(comp, this.errorTooltip, xPos, y);
                }
                catch (BadLocationException blE) {
                    ErrorManager.getDefault().notify((Throwable)blE);
                    this.errorTooltip = null;
                }
            }
            if (placeUp) {
                this.hintListComponent = new ScrollCompletionPane(comp, fixes, null, null, new Dimension(screenDim.width, Math.min(ySpaceWhenPlacedUp, hintPopup.height)));
                hintPopup = this.hintListComponent.getPreferredSize();
                p.y -= hintPopup.height + rowHeight + 5;
                assert (p.y >= 0);
            } else if (exceedsHeight) {
                this.hintListComponent = new ScrollCompletionPane(comp, fixes, null, null, new Dimension(screenDim.width, Math.min(screenDim.height - p.y, hintPopup.height)));
            }
            if (p.x - screen.x + hintPopup.width > screen.width) {
                p.x -= p.x - screen.x + hintPopup.width - screen.width;
            }
            this.hintListComponent.getView().addMouseListener(this);
            this.hintListComponent.getView().addMouseMotionListener(this);
            this.hintListComponent.setName("hintsPopup");
            assert (this.listPopup == null);
            this.listPopup = this.getPopupFactory().getPopup(comp, this.hintListComponent, p.x, p.y);
        }
        if (this.tooltipPopup != null) {
            this.tooltipPopup.show();
        }
        this.listPopup.show();
    }

    private PopupFactory getPopupFactory() {
        if (this.pf == null) {
            this.pf = PopupFactory.getSharedInstance();
        }
        return this.pf;
    }

    private Rectangle getScreenBounds() throws HeadlessException {
        Rectangle virtualBounds = new Rectangle();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        if (gs.length == 0 || gs.length == 1) {
            return new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        }
        for (GraphicsDevice gd : gs) {
            virtualBounds = virtualBounds.union(gd.getDefaultConfiguration().getBounds());
        }
        return virtualBounds;
    }

    private int getUsableWidth(JTextComponent component) {
        Container parent = component.getParent();
        return parent instanceof JViewport ? ((JViewport)parent).getExtentSize().width : component.getSize().width;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == this.hintListComponent.getView() && this.hintListComponent.getView().getSize().width - ListCompletionView.arrowSpan() <= e.getPoint().x && this.hintListComponent.getView().right()) {
            e.consume();
            return;
        }
        if (e.getSource() instanceof ListCompletionView) {
            Fix f = null;
            Object selected = ((ListCompletionView)e.getSource()).getSelectedValue();
            if (selected instanceof Fix) {
                f = (Fix)selected;
            }
            if (f != null) {
                e.consume();
                JTextComponent tc = this.getComponent();
                this.invokeHint(f);
                if (tc != null && org.openide.util.Utilities.isMac()) {
                    tc.requestFocus();
                }
                this.removeHints();
                this.setComponent(tc);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (e.getSource() instanceof ListCompletionView) {
            ListCompletionView view = (ListCompletionView)e.getSource();
            int wasSelected = view.getSelectedIndex();
            view.setSelectedIndex(view.locationToIndex(e.getPoint()));
            if (wasSelected != view.getSelectedIndex() && view == this.hintListComponent.getView()) {
                this.closeSubList();
            }
            if (this.sublistPopup != null && e.getSource() == this.hintListComponent.getView() && this.hintListComponent.getView().getSize().width - ListCompletionView.arrowSpan() > e.getPoint().x) {
                this.closeSubList();
            }
            if (this.sublistPopup == null && e.getSource() == this.hintListComponent.getView() && this.hintListComponent.getView().getSize().width - ListCompletionView.arrowSpan() <= e.getPoint().x && this.hintListComponent.getView().right()) {
                e.consume();
            }
        }
    }

    public boolean isActive() {
        boolean bulbShowing = this.hintIcon != null && this.hintIcon.isShowing();
        boolean popupShowing = this.hintListComponent != null && this.hintListComponent.isShowing();
        return bulbShowing || popupShowing;
    }

    public boolean isPopupActive() {
        return this.hintListComponent != null && this.hintListComponent.isShowing();
    }

    private ParseErrorAnnotation findAnnotation(Document doc, AnnotationDesc desc, int lineNum) {
        AnnotationHolder annotations = this.getAnnotationHolder(doc);
        if (annotations != null) {
            for (Annotation a : annotations.getAnnotations()) {
                ParseErrorAnnotation pa;
                if (!(a instanceof ParseErrorAnnotation) || lineNum != (pa = (ParseErrorAnnotation)a).getLineNumber() || desc == null || !org.openide.util.Utilities.compareObjects((Object)desc.getShortDescription(), (Object)a.getShortDescription())) continue;
                return pa;
            }
        }
        return null;
    }

    boolean invokeDefaultAction(boolean onlyActive) {
        JTextComponent comp = this.getComponent();
        if (comp == null) {
            Logger.getLogger(HintsUI.class.getName()).log(Level.WARNING, "HintsUI.invokeDefaultAction called, but comp == null");
            return false;
        }
        Document doc = comp.getDocument();
        this.cancel.set(false);
        if (doc instanceof BaseDocument) {
            try {
                String description;
                FixData fixes;
                Rectangle carretRectangle = comp.modelToView(comp.getCaretPosition());
                int line = org.netbeans.editor.Utilities.getLineOffset((BaseDocument)((BaseDocument)doc), (int)comp.getCaretPosition());
                if (!onlyActive) {
                    Pair<FixData, String> fixData;
                    this.refresh(doc, comp.getCaretPosition());
                    AnnotationHolder holder = this.getAnnotationHolder(doc);
                    Pair<FixData, String> pair = fixData = holder != null ? holder.buildUpFixDataForLine(line) : null;
                    if (fixData == null) {
                        return false;
                    }
                    fixes = (FixData)fixData.first();
                    description = (String)fixData.second();
                } else {
                    AnnotationDesc activeAnnotation = ((BaseDocument)doc).getAnnotations().getActiveAnnotation(line);
                    if (activeAnnotation == null) {
                        return false;
                    }
                    String type = activeAnnotation.getAnnotationType();
                    if (!fixableAnnotations.contains(type) && onlyActive) {
                        return false;
                    }
                    if (onlyActive) {
                        this.refresh(doc, comp.getCaretPosition());
                    }
                    Annotations annotations = ((BaseDocument)doc).getAnnotations();
                    AnnotationDesc desc = annotations.getAnnotation(line, type);
                    ParseErrorAnnotation annotation = null;
                    if (desc != null) {
                        annotations.frontAnnotation(desc);
                        annotation = this.findAnnotation(doc, desc, line);
                    }
                    if (annotation == null) {
                        return false;
                    }
                    fixes = annotation.getFixes();
                    description = annotation.getDescription();
                }
                Point p = comp.modelToView(org.netbeans.editor.Utilities.getRowStartFromLineOffset((BaseDocument)((BaseDocument)doc), (int)line)).getLocation();
                p.y += carretRectangle.height;
                if (comp.getParent() instanceof JViewport) {
                    p.x += ((JViewport)comp.getParent()).getViewPosition().x;
                }
                this.showPopup(fixes, description, comp, p);
                return true;
            }
            catch (BadLocationException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
            }
        }
        return false;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        boolean popupShowing;
        boolean codeComplationShowing;
        JTextComponent comp = this.getComponent();
        if (comp == null || e.isConsumed()) {
            return;
        }
        boolean bl = codeComplationShowing = comp.getClientProperty("completion-visible") == Boolean.TRUE;
        if (codeComplationShowing) {
            return;
        }
        boolean errorTooltipShowing = this.errorTooltip != null && this.errorTooltip.isShowing();
        boolean bl2 = popupShowing = this.hintListComponent != null && this.hintListComponent.isShowing();
        if (errorTooltipShowing && !popupShowing) {
            this.removePopup();
            return;
        }
        if (e.getKeyCode() == 10) {
            if (e.getModifiersEx() == 512) {
                if (!popupShowing) {
                    if (org.openide.util.Utilities.isWindows() && !Boolean.getBoolean("HintsUI.disable.AltEnter.hack")) {
                        this.altEnterPressed = true;
                        this.altReleased = false;
                    }
                    this.invokeDefaultAction(false);
                    e.consume();
                }
            } else if ((e.getModifiersEx() & 16383) == 0 && popupShowing) {
                Fix f = null;
                ScrollCompletionPane listPane = this.subhintListComponent != null ? this.subhintListComponent : this.hintListComponent;
                Object selected = listPane.getView().getSelectedValue();
                if (selected instanceof Fix) {
                    f = (Fix)selected;
                }
                if (f != null) {
                    this.invokeHint(f);
                }
                e.consume();
            }
        } else if (e.getKeyCode() == 27) {
            if (popupShowing) {
                this.removePopup();
            } else {
                this.cancel.set(true);
            }
        } else if (popupShowing) {
            ScrollCompletionPane listPane = this.subhintListComponent != null ? this.subhintListComponent : this.hintListComponent;
            InputMap input = listPane.getInputMap();
            Object actionTag = input.get(KeyStroke.getKeyStrokeForEvent(e));
            if (actionTag != null) {
                Action a = listPane.getActionMap().get(actionTag);
                a.actionPerformed(null);
                e.consume();
                return;
            }
            this.removePopup();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (org.openide.util.Utilities.isWindows()) {
            if (Boolean.getBoolean("HintsUI.disable.AltEnter.hack")) {
                return;
            }
            if (this.altEnterPressed && e.getKeyCode() == 18) {
                e.consume();
                this.altReleased = true;
            } else if (this.altEnterPressed && e.getKeyCode() == 10) {
                this.altEnterPressed = false;
                if (this.altReleased) {
                    try {
                        Robot r = new Robot();
                        r.keyRelease(18);
                    }
                    catch (AWTException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void invokeHint(final Fix f) {
        if (UI_GESTURES_LOGGER.isLoggable(Level.FINE)) {
            LogRecord rec = new LogRecord(Level.FINE, "GEST_HINT_INVOKED");
            rec.setResourceBundle(NbBundle.getBundle(HintsUI.class));
            rec.setParameters(new Object[]{f.getText()});
            UI_GESTURES_LOGGER.log(rec);
        }
        this.removePopups();
        final JTextComponent component = this.getComponent();
        JumpList.checkAddEntry((JTextComponent)component);
        Cursor cur = component.getCursor();
        component.setCursor(Cursor.getPredefinedCursor(3));
        RequestProcessor.Task t = null;
        try {
            t = RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    try {
                        HintsUI.this.changes = f.implement();
                    }
                    catch (GuardedException ge) {
                        HintsUI.reportGuardedException(component, (Exception)ge);
                    }
                    catch (IOException e) {
                        if (e.getCause() instanceof GuardedException) {
                            HintsUI.reportGuardedException(component, e);
                        } else {
                            Exceptions.printStackTrace((Throwable)e);
                        }
                    }
                    catch (Exception e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
            });
            if (t == null) return;
        }
        catch (Throwable var5_5) {
            if (t == null) throw var5_5;
            t.addTaskListener(new TaskListener(component, cur){
                final /* synthetic */ JTextComponent val$component;
                final /* synthetic */ Cursor val$cur;

                public void taskFinished(Task task) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            HintsUI.open(HintsUI.this.changes, 2.this.val$component);
                            2.this.val$component.setCursor(2.this.val$cur);
                        }
                    });
                }

            });
            throw var5_5;
        }
        t.addTaskListener(new );
    }

    private static void reportGuardedException(final JTextComponent component, final Exception e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                String message = NbBundle.getMessage(HintsUI.class, (String)"ERR_CannotApplyGuarded");
                org.netbeans.editor.Utilities.setStatusBoldText((JTextComponent)component, (String)message);
                Logger.getLogger(HintsUI.class.getName()).log(Level.FINE, null, e);
            }
        });
    }

    private static void open(ChangeInfo changes, JTextComponent component) {
        JTextComponent tc = component;
        if (changes != null && changes.size() > 0) {
            ChangeInfo.Change change;
            block9 : {
                change = changes.get(0);
                FileObject file = change.getFileObject();
                if (file != null) {
                    try {
                        DataObject dob = DataObject.find((FileObject)file);
                        EditCookie ck = (EditCookie)dob.getCookie(EditCookie.class);
                        if (ck != null) {
                            ck.edit();
                        } else {
                            OpenCookie oc = (OpenCookie)dob.getCookie(OpenCookie.class);
                            oc.open();
                        }
                        EditorCookie edit = (EditorCookie)dob.getCookie(EditorCookie.class);
                        JEditorPane[] panes = edit.getOpenedPanes();
                        if (panes != null && panes.length > 0) {
                            tc = panes[0];
                            break block9;
                        }
                        return;
                    }
                    catch (DataObjectNotFoundException donfe) {
                        Logger.getLogger(HintsUI.class.getName()).log(Level.FINE, null, (Throwable)donfe);
                        return;
                    }
                }
            }
            Position start = change.getStart();
            Position end = change.getEnd();
            if (start != null) {
                tc.setSelectionStart(start.getOffset());
            }
            if (end != null) {
                tc.setSelectionEnd(end.getOffset());
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        JTextComponent active = EditorRegistry.lastFocusedComponent();
        if (this.getComponent() != active) {
            this.removeHints();
            this.setComponent(active);
            if (this.getComponent() != null) {
                this.getComponent().removeFocusListener(this);
            }
            if (active != null) {
                active.addFocusListener(this);
            }
        }
    }

    @Override
    public void eventDispatched(AWTEvent aWTEvent) {
        MouseEvent mv;
        if (aWTEvent instanceof MouseEvent && (mv = (MouseEvent)aWTEvent).getID() == 500 && mv.getClickCount() > 0) {
            if (!(aWTEvent.getSource() instanceof Component)) {
                this.removePopup();
                return;
            }
            Component comp = (Component)aWTEvent.getSource();
            Container par1 = SwingUtilities.getAncestorNamed("hintsPopup", comp);
            Container par2 = SwingUtilities.getAncestorNamed("subHintsPopup", comp);
            if (par1 == null && par2 == null) {
                this.removePopup();
            }
        }
    }

    private String translate(String input) {
        for (int cntr = 0; cntr < c.length; ++cntr) {
            input = input.replaceAll(c[cntr], tags[cntr]);
        }
        return input;
    }

    private void refresh(Document doc, int pos) {
        AnnotationHolder holder = this.getAnnotationHolder(doc);
        if (holder == null) {
            Logger.getLogger(HintsUI.class.getName()).log(Level.FINE, "No AnnotationHolder associated to: {0} (stream description property: {1})", new Object[]{doc, doc.getProperty("stream")});
            return;
        }
        Context context = ContextAccessor.getDefault().newContext(pos, this.cancel);
        String mimeType = DocumentUtilities.getMimeType((Document)doc);
        Lookup lookup = MimeLookup.getLookup((String)mimeType);
        Collection refreshers = lookup.lookupAll(PositionRefresher.class);
        for (PositionRefresher ref : refreshers) {
            Map<String, List<ErrorDescription>> layer2Errs = ref.getErrorDescriptionsAt(context, doc);
            holder.setErrorsForLine(pos, layer2Errs);
        }
    }

    public void undoOnePopup() {
        if (this.sublistPopup != null) {
            this.closeSubList();
        } else {
            this.removePopups();
        }
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        JTextComponent currentComponent;
        JTextComponent jTextComponent = currentComponent = this.compRef != null ? this.compRef.get() : null;
        if (currentComponent == null) {
            return;
        }
        final HighlightingManager hm = HighlightingManager.getInstance((JTextComponent)currentComponent);
        final Document doc = currentComponent.getDocument();
        Caret caretInstance = currentComponent.getCaret();
        if (caretInstance == null) {
            return;
        }
        final int caret = caretInstance.getDot();
        WORKER.post(new Runnable(){

            @Override
            public void run() {
                final String[] warning = new String[]{AnnotationHolder.resolveWarnings(doc, caret, caret)};
                if (warning[0] == null || warning[0].trim().isEmpty()) {
                    Object res;
                    final HighlightAttributeValue[] hav = new HighlightAttributeValue[1];
                    doc.render(new Runnable(){

                        @Override
                        public void run() {
                            Object tp;
                            HighlightsSequence hit = hm.getBottomHighlights().getHighlights(caret, caret + 1);
                            if (hit.moveNext() && hit.getAttributes().containsAttribute("unused-browseable", Boolean.TRUE) && (tp = hit.getAttributes().getAttribute(EditorStyleConstants.Tooltip)) instanceof HighlightAttributeValue) {
                                hav[0] = (HighlightAttributeValue)tp;
                            }
                        }
                    });
                    if (hav[0] != null && (res = hav[0].getValue((JTextComponent)HintsUI.this.errorTooltip, doc, (Object)hav[0], caret, caret)) instanceof String) {
                        warning[0] = (String)res;
                    }
                } else {
                    warning[0] = warning[0].replace('\n', ' ');
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        StatusBar sb;
                        JTextComponent currentComponent = (JTextComponent)HintsUI.this.compRef.get();
                        if (currentComponent == null) {
                            return;
                        }
                        if (warning[0] == null || warning[0].trim().isEmpty()) {
                            CaretLocationAndMessage clam = (CaretLocationAndMessage)currentComponent.getClientProperty(CaretLocationAndMessage.class);
                            if (clam != null) {
                                clam.message.clear(0);
                                currentComponent.putClientProperty(CaretLocationAndMessage.class, null);
                            }
                            return;
                        }
                        CaretLocationAndMessage clam = (CaretLocationAndMessage)currentComponent.getClientProperty(CaretLocationAndMessage.class);
                        if (clam != null && clam.caret == caret && warning[0].equals(clam.lastMessage)) {
                            return;
                        }
                        EditorUI editorUI = org.netbeans.editor.Utilities.getEditorUI((JTextComponent)currentComponent);
                        StatusBar statusBar = sb = editorUI != null ? editorUI.getStatusBar() : null;
                        if (sb != null && sb.isVisible()) {
                            org.netbeans.editor.Utilities.setStatusText((JTextComponent)currentComponent, (String)warning[0], (int)700);
                        } else {
                            StatusDisplayer.Message m = StatusDisplayer.getDefault().setStatusText(warning[0], 700);
                            currentComponent.putClientProperty(CaretLocationAndMessage.class, new CaretLocationAndMessage(caret, warning[0], m));
                            m.clear(5000);
                        }
                    }
                });
            }

        });
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.removePopups();
    }

    static {
        WORKER = new RequestProcessor(HintsUI.class.getName(), 1, false, false);
        fixableAnnotations = new HashSet<String>(3);
        fixableAnnotations.add("org-netbeans-spi-editor-hints-parser_annotation_err_fixable");
        fixableAnnotations.add("org-netbeans-spi-editor-hints-parser_annotation_hint_fixable");
        fixableAnnotations.add("org-netbeans-spi-editor-hints-parser_annotation_verifier_fixable");
        fixableAnnotations.add("org-netbeans-spi-editor-hints-parser_annotation_warn_fixable");
        UI_GESTURES_LOGGER = Logger.getLogger("org.netbeans.ui.editor.hints");
        c = new String[]{"&", "<", ">", "\n", "\""};
        tags = new String[]{"&amp;", "&lt;", "&gt;", "<br>", "&quot;"};
    }

    private static final class CaretLocationAndMessage {
        final int caret;
        final String lastMessage;
        final StatusDisplayer.Message message;

        public CaretLocationAndMessage(int caret, String lastMessage, StatusDisplayer.Message message) {
            this.caret = caret;
            this.lastMessage = lastMessage;
            this.message = message;
        }
    }

}

