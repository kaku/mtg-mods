/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.Annotation
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.hints;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.hints.AnnotationHolder;
import org.netbeans.modules.editor.hints.FixData;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.text.Annotation;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class ParseErrorAnnotation
extends Annotation
implements PropertyChangeListener {
    private final Severity severity;
    private final FixData fixes;
    private final String description;
    private final String shortDescription;
    private final Position lineStart;
    private final AnnotationHolder holder;
    private StyledDocument attachedTo;

    public ParseErrorAnnotation(Severity severity, FixData fixes, String description, Position lineStart, AnnotationHolder holder) {
        this.severity = severity;
        this.fixes = fixes;
        this.description = description;
        this.shortDescription = description + NbBundle.getMessage(ParseErrorAnnotation.class, (String)"LBL_shortcut_promotion");
        this.lineStart = lineStart;
        this.holder = holder;
        if (!fixes.isComputed()) {
            fixes.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)fixes));
        }
    }

    public String getAnnotationType() {
        boolean hasFixes = this.fixes.isComputed() && !this.fixes.getFixes().isEmpty();
        switch (this.severity) {
            case ERROR: {
                if (hasFixes) {
                    return "org-netbeans-spi-editor-hints-parser_annotation_err_fixable";
                }
                return "org-netbeans-spi-editor-hints-parser_annotation_err";
            }
            case WARNING: {
                if (hasFixes) {
                    return "org-netbeans-spi-editor-hints-parser_annotation_warn_fixable";
                }
                return "org-netbeans-spi-editor-hints-parser_annotation_warn";
            }
            case VERIFIER: {
                if (hasFixes) {
                    return "org-netbeans-spi-editor-hints-parser_annotation_verifier_fixable";
                }
                return "org-netbeans-spi-editor-hints-parser_annotation_verifier";
            }
            case HINT: {
                if (hasFixes) {
                    return "org-netbeans-spi-editor-hints-parser_annotation_hint_fixable";
                }
                return "org-netbeans-spi-editor-hints-parser_annotation_hint";
            }
        }
        throw new IllegalArgumentException(String.valueOf((Object)this.severity));
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.fixes.isComputed()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ParseErrorAnnotation.this.firePropertyChange("annotationType", null, ParseErrorAnnotation.this.getAnnotationType());
                }
            });
        }
    }

    public FixData getFixes() {
        return this.fixes;
    }

    public String getDescription() {
        return this.description;
    }

    public int getLineNumber() {
        return this.holder.lineNumber(this.lineStart);
    }

    Severity getSeverity() {
        return this.severity;
    }

    synchronized void attachAnnotation(StyledDocument doc, Position lineStart) {
        if (this.attachedTo == null) {
            this.attachedTo = doc;
            NbDocument.addAnnotation((StyledDocument)doc, (Position)lineStart, (int)-1, (Annotation)this);
        } else {
            Level toLog = Level.FINE;
            assert ((toLog = Level.INFO) != null);
            AnnotationHolder.LOG.log(toLog, "Attempt to attach already attached annotation", new Exception());
        }
    }

    synchronized void detachAnnotation(StyledDocument doc) {
        if (this.attachedTo != null) {
            assert (this.attachedTo == doc);
            NbDocument.removeAnnotation((StyledDocument)this.attachedTo, (Annotation)this);
            this.attachedTo = null;
        } else {
            Level toLog = Level.FINE;
            assert ((toLog = Level.INFO) != null);
            AnnotationHolder.LOG.log(toLog, "Attempt to detach not attached annotation", new Exception());
        }
    }

}

