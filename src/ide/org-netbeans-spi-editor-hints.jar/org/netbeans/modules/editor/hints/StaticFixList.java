/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.hints;

import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.LazyFixList;

public class StaticFixList
implements LazyFixList {
    @NonNull
    private List<Fix> fixes;

    public StaticFixList() {
        this.fixes = Collections.emptyList();
    }

    public StaticFixList(@NonNull List<Fix> fixes) {
        this.fixes = fixes;
    }

    @Override
    public boolean probablyContainsFixes() {
        return !this.fixes.isEmpty();
    }

    @NonNull
    @Override
    public List<Fix> getFixes() {
        return this.fixes;
    }

    @Override
    public boolean isComputed() {
        return true;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
    }
}

