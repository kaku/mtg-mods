/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.hints.borrowed;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.hints.FixData;
import org.netbeans.modules.editor.hints.HintsUI;
import org.netbeans.modules.editor.hints.borrowed.ListCompletionView;
import org.netbeans.spi.editor.hints.Fix;

public class ScrollCompletionPane
extends JScrollPane {
    private static final Logger LOG = Logger.getLogger(ScrollCompletionPane.class.getName());
    private static final String COMPLETION_UP = "completion-up";
    private static final String COMPLETION_DOWN = "completion-down";
    private static final String COMPLETION_PGUP = "completion-pgup";
    private static final String COMPLETION_PGDN = "completion-pgdn";
    private static final String COMPLETION_BEGIN = "completion-begin";
    private static final String COMPLETION_END = "completion-end";
    private static final String COMPLETION_RIGHT = "completion-right";
    private static final String COMPLETION_LEFT = "completion-left";
    private static final int ACTION_COMPLETION_UP = 1;
    private static final int ACTION_COMPLETION_DOWN = 2;
    private static final int ACTION_COMPLETION_PGUP = 3;
    private static final int ACTION_COMPLETION_PGDN = 4;
    private static final int ACTION_COMPLETION_BEGIN = 5;
    private static final int ACTION_COMPLETION_END = 6;
    private static final int ACTION_COMPLETION_RIGHT = 7;
    private static final int ACTION_COMPLETION_LEFT = 8;
    private JTextComponent component;
    private ListCompletionView view;
    private JLabel topLabel;
    private Dimension minSize;
    private Dimension maxSize;
    private Dimension scrollBarSize;

    public ScrollCompletionPane(JTextComponent component, FixData fixes, String title, ListSelectionListener listener) {
        this(component, fixes, title, listener, null);
    }

    public ScrollCompletionPane(JTextComponent component, FixData fixes, String title, ListSelectionListener listener, Dimension maxSize) {
        this.component = component;
        Dimension smallSize = super.getPreferredSize();
        this.setHorizontalScrollBarPolicy(31);
        this.setVerticalScrollBarPolicy(22);
        this.scrollBarSize = super.getPreferredSize();
        this.scrollBarSize.width -= smallSize.width;
        this.scrollBarSize.height -= smallSize.height;
        this.setVerticalScrollBarPolicy(20);
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(SystemColor.controlDkShadow), BorderFactory.createEmptyBorder(4, 4, 4, 4)));
        this.setViewportBorder(null);
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)component);
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        this.minSize = ScrollCompletionPane.parseDimension(prefs.get("completion-pane-min-size", null), new Dimension(60, 17));
        this.setMinimumSize(this.minSize);
        if (maxSize != null) {
            this.maxSize = maxSize;
            this.setMaximumSize(maxSize);
        }
        this.view = new ListCompletionView();
        this.setBackground(this.view.getBackground());
        this.view.setResult(fixes);
        this.resetViewSize();
        this.setViewportView(this.view);
        this.setTitle(title);
        this.installKeybindings();
        this.setFocusable(false);
        this.view.setFocusable(false);
    }

    public ListCompletionView getView() {
        return this.view;
    }

    public void reset(FixData fixes, String title) {
        this.view.setResult(fixes);
        this.resetViewSize();
        this.setTitle(title);
    }

    public Fix getSelectedFix() {
        Object ret = this.view.getSelectedValue();
        return ret instanceof Fix ? (Fix)ret : null;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension ps = super.getPreferredSize();
        int width = ps.width + this.scrollBarSize.width;
        boolean displayHorizontalScrollbar = width > this.maxSize.width;
        width = Math.max(Math.max(width, this.minSize.width), this.getTitleComponentPreferredSize().width);
        width = Math.min(width, this.maxSize.width);
        int height = displayHorizontalScrollbar ? ps.height + this.scrollBarSize.height : ps.height;
        height = Math.min(height, this.maxSize.height);
        height = Math.max(height, this.minSize.height);
        return new Dimension(width, height);
    }

    private void resetViewSize() {
        Dimension viewSize = this.view.getPreferredSize();
        if (viewSize.width > this.maxSize.width - this.scrollBarSize.width) {
            viewSize.width = this.maxSize.width - this.scrollBarSize.width;
            this.view.setPreferredSize(viewSize);
        }
    }

    private void setTitle(String title) {
        if (title == null) {
            if (this.topLabel != null) {
                this.setColumnHeader(null);
                this.topLabel = null;
            }
        } else if (this.topLabel != null) {
            this.topLabel.setText(title);
        } else {
            this.topLabel = new JLabel(title);
            this.topLabel.setForeground(Color.blue);
            this.topLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
            this.setColumnHeaderView(this.topLabel);
        }
    }

    private Dimension getTitleComponentPreferredSize() {
        return this.topLabel != null ? this.topLabel.getPreferredSize() : new Dimension();
    }

    private KeyStroke[] findEditorKeys(String editorActionName, KeyStroke defaultKey) {
        KeyStroke[] ret = new KeyStroke[]{defaultKey};
        if (this.component != null) {
            KeyStroke[] keys;
            Action a = this.component.getActionMap().get(editorActionName);
            Keymap km = this.component.getKeymap();
            if (a != null && km != null && (keys = km.getKeyStrokesForAction(a)) != null && keys.length > 0) {
                ret = keys;
            }
        }
        return ret;
    }

    private void registerKeybinding(int action, String actionName, KeyStroke stroke, String editorActionName) {
        KeyStroke[] keys = this.findEditorKeys(editorActionName, stroke);
        for (int i = 0; i < keys.length; ++i) {
            this.getInputMap().put(keys[i], actionName);
        }
        this.getActionMap().put(actionName, new CompletionPaneAction(action));
    }

    private void installKeybindings() {
        this.registerKeybinding(1, "completion-up", KeyStroke.getKeyStroke(38, 0), "caret-up");
        this.registerKeybinding(2, "completion-down", KeyStroke.getKeyStroke(40, 0), "caret-down");
        this.registerKeybinding(4, "completion-pgdn", KeyStroke.getKeyStroke(34, 0), "page-down");
        this.registerKeybinding(3, "completion-pgup", KeyStroke.getKeyStroke(33, 0), "page-up");
        this.registerKeybinding(5, "completion-begin", KeyStroke.getKeyStroke(36, 0), "caret-begin-line");
        this.registerKeybinding(6, "completion-end", KeyStroke.getKeyStroke(35, 0), "caret-end-line");
        this.registerKeybinding(7, "completion-right", KeyStroke.getKeyStroke(39, 0), "caret-forward");
        this.registerKeybinding(8, "completion-left", KeyStroke.getKeyStroke(37, 0), "caret-backward");
    }

    private static Dimension parseDimension(String s, Dimension d) {
        int[] arr = new int[2];
        int i = 0;
        if (s != null) {
            StringTokenizer st = new StringTokenizer(s, ",");
            while (st.hasMoreElements()) {
                if (i > 1) {
                    return d;
                }
                try {
                    arr[i] = Integer.parseInt(st.nextToken());
                }
                catch (NumberFormatException nfe) {
                    LOG.log(Level.WARNING, null, nfe);
                    return d;
                }
                ++i;
            }
        }
        if (i != 2) {
            return d;
        }
        return new Dimension(arr[0], arr[1]);
    }

    private class CompletionPaneAction
    extends AbstractAction {
        private int action;

        private CompletionPaneAction(int action) {
            this.action = action;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            switch (this.action) {
                case 1: {
                    ScrollCompletionPane.this.view.up();
                    break;
                }
                case 2: {
                    ScrollCompletionPane.this.view.down();
                    break;
                }
                case 3: {
                    ScrollCompletionPane.this.view.pageUp();
                    break;
                }
                case 4: {
                    ScrollCompletionPane.this.view.pageDown();
                    break;
                }
                case 5: {
                    ScrollCompletionPane.this.view.begin();
                    break;
                }
                case 6: {
                    ScrollCompletionPane.this.view.end();
                    break;
                }
                case 7: {
                    ScrollCompletionPane.this.view.right();
                    break;
                }
                case 8: {
                    HintsUI.getDefault().undoOnePopup();
                }
            }
        }
    }

}

