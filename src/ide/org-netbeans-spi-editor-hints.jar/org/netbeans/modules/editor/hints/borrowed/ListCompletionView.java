/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.LocaleSupport
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.editor.hints.borrowed;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthLookAndFeel;
import javax.swing.plaf.synth.SynthStyle;
import org.netbeans.editor.LocaleSupport;
import org.netbeans.modules.editor.hints.FixData;
import org.netbeans.modules.editor.hints.HintsControllerImpl;
import org.netbeans.modules.editor.hints.HintsUI;
import org.netbeans.spi.editor.hints.Fix;
import org.openide.awt.HtmlRenderer;
import org.openide.util.ImageUtilities;

public class ListCompletionView
extends JList {
    private static final Logger LOG;
    public static final int COMPLETION_ITEM_HEIGHT = 16;
    private static final int DARKER_COLOR_COMPONENT = 5;
    private static final Icon icon;
    private static final Icon subMenuIcon;
    private final int fixedItemHeight;
    private final HtmlRenderer.Renderer defaultRenderer = HtmlRenderer.createRenderer();
    private Font font;
    private final RenderComponent renderComponent;
    private Graphics cellPreferredSizeGraphics;
    private static final int BEFORE_ICON_GAP = 1;
    private static final int AFTER_ICON_GAP = 4;
    private static final int AFTER_TEXT_GAP = 5;
    private static final int AFTER_RIGHT_ICON_GAP = 3;
    private static final Class<?> synthIcon;
    private static final boolean subMenuIconIsSynthIcon;

    public ListCompletionView() {
        this.setSelectionMode(0);
        this.font = this.getFont();
        if (this.font.getSize() < 15) {
            this.font = this.font.deriveFont(this.font.getSize2D() + 1.0f);
        }
        this.setFont(this.font);
        this.fixedItemHeight = Math.max(16, this.getFontMetrics(this.getFont()).getHeight());
        this.setFixedCellHeight(this.fixedItemHeight);
        this.renderComponent = new RenderComponent();
        this.setCellRenderer(new ListCellRenderer(){
            private final ListCellRenderer defaultRenderer;

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof Fix) {
                    Color bgColor;
                    Color fgColor;
                    Fix fix = (Fix)value;
                    ListCompletionView.this.renderComponent.setFix(fix);
                    ListCompletionView.this.renderComponent.setSelected(isSelected);
                    if (isSelected) {
                        bgColor = list.getSelectionBackground();
                        fgColor = list.getSelectionForeground();
                    } else {
                        bgColor = list.getBackground();
                        if (index % 2 == 0) {
                            bgColor = new Color(Math.abs(bgColor.getRed() - 5), Math.abs(bgColor.getGreen() - 5), Math.abs(bgColor.getBlue() - 5));
                        }
                        fgColor = list.getForeground();
                    }
                    if (ListCompletionView.this.renderComponent.getBackground() != bgColor) {
                        ListCompletionView.this.renderComponent.setBackground(bgColor);
                    }
                    if (ListCompletionView.this.renderComponent.getForeground() != fgColor) {
                        ListCompletionView.this.renderComponent.setForeground(fgColor);
                    }
                    return ListCompletionView.this.renderComponent;
                }
                return this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
        this.setBorder(BorderFactory.createEmptyBorder());
        this.getAccessibleContext().setAccessibleName(LocaleSupport.getString((String)"ACSN_CompletionView"));
        this.getAccessibleContext().setAccessibleDescription(LocaleSupport.getString((String)"ACSD_CompletionView"));
    }

    public void setResult(FixData data) {
        if (data != null) {
            Model model = new Model(data);
            this.setModel(model);
            if (model.fixes != null && !model.fixes.isEmpty()) {
                this.setSelectedIndex(0);
            }
        }
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return this.getPreferredSize();
    }

    public void up() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx = (this.getSelectedIndex() - 1 + size) % size;
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
            this.repaint();
        }
    }

    public void down() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx = (this.getSelectedIndex() + 1) % size;
            if (idx == size) {
                idx = 0;
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
            this.validate();
        }
    }

    public void pageUp() {
        if (this.getModel().getSize() > 0) {
            int pageSize = Math.max(this.getLastVisibleIndex() - this.getFirstVisibleIndex(), 0);
            int ind = Math.max(this.getSelectedIndex() - pageSize, 0);
            this.setSelectedIndex(ind);
            this.ensureIndexIsVisible(ind);
        }
    }

    public void pageDown() {
        int lastInd = this.getModel().getSize() - 1;
        if (lastInd >= 0) {
            int pageSize = Math.max(this.getLastVisibleIndex() - this.getFirstVisibleIndex(), 0);
            int ind = Math.min(this.getSelectedIndex() + pageSize, lastInd);
            this.setSelectedIndex(ind);
            this.ensureIndexIsVisible(ind);
        }
    }

    public void begin() {
        if (this.getModel().getSize() > 0) {
            this.setSelectedIndex(0);
            this.ensureIndexIsVisible(0);
        }
    }

    public void end() {
        int lastInd = this.getModel().getSize() - 1;
        if (lastInd >= 0) {
            this.setSelectedIndex(lastInd);
            this.ensureIndexIsVisible(lastInd);
        }
    }

    public boolean right() {
        Fix f = (Fix)this.getSelectedValue();
        Iterable<? extends Fix> subfixes = HintsControllerImpl.getSubfixes(f);
        if (subfixes.iterator().hasNext()) {
            Rectangle r = this.getCellBounds(this.getSelectedIndex(), this.getSelectedIndex());
            Point p = new Point(r.getLocation());
            SwingUtilities.convertPointToScreen(p, this);
            p.x += r.width;
            HintsUI.getDefault().openSubList(subfixes, p);
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        Map renderingHints;
        Map value = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
        Map map = renderingHints = value instanceof Map ? value : null;
        if (renderingHints != null && g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D)g;
            RenderingHints oldHints = g2d.getRenderingHints();
            g2d.setRenderingHints(renderingHints);
            try {
                super.paint(g2d);
            }
            finally {
                g2d.setRenderingHints(oldHints);
            }
        }
        super.paint(g);
    }

    private static int getPreferredWidth(Fix f, Graphics g, Font defaultFont) {
        int width = 1 + icon.getIconWidth() + 4 + 5;
        width += (int)HtmlRenderer.renderHTML((String)f.getText(), (Graphics)g, (int)0, (int)0, (int)Integer.MAX_VALUE, (int)0, (Font)defaultFont, (Color)Color.black, (int)0, (boolean)false);
        if (HintsControllerImpl.getSubfixes(f).iterator().hasNext()) {
            width += subMenuIcon.getIconWidth() + 3;
        }
        return width;
    }

    public static int arrowSpan() {
        return 5 + subMenuIcon.getIconWidth() + 3;
    }

    private void renderHtml(Fix f, Graphics g, Font defaultFont, Color defaultColor, int width, int height, boolean selected) {
        if (icon != null) {
            g.drawImage(ImageUtilities.icon2Image((Icon)icon), 1, (height - icon.getIconHeight()) / 2, this);
        }
        int iconWidth = 1 + icon.getIconWidth() + 4;
        int textEnd = width - 4 - subMenuIcon.getIconWidth() - 5;
        FontMetrics fm = g.getFontMetrics(defaultFont);
        int textY = (height - fm.getHeight()) / 2 + fm.getHeight() - fm.getDescent();
        if (textEnd > iconWidth) {
            HtmlRenderer.renderHTML((String)f.getText(), (Graphics)g, (int)iconWidth, (int)textY, (int)textEnd, (int)textY, (Font)defaultFont, (Color)defaultColor, (int)1, (boolean)true);
        }
        if (HintsControllerImpl.getSubfixes(f).iterator().hasNext()) {
            ListCompletionView.paintArrowIcon(g, textEnd + 5, (height - subMenuIcon.getIconHeight()) / 2);
        }
    }

    private static void paintArrowIcon(Graphics g, int x, int y) {
        JMenuItem menu = new JMenuItem();
        if (subMenuIconIsSynthIcon) {
            try {
                Region region = SynthLookAndFeel.getRegion(menu);
                SynthStyle style = SynthLookAndFeel.getStyle(menu, region);
                SynthContext c = new SynthContext(menu, region, style, 1);
                Method paitIcon = synthIcon.getDeclaredMethod("paintIcon", SynthContext.class, Graphics.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE);
                paitIcon.invoke(subMenuIcon, c, g, x, y, subMenuIcon.getIconWidth(), subMenuIcon.getIconHeight());
                return;
            }
            catch (IllegalAccessException ex) {
                LOG.log(Level.FINE, null, ex);
                return;
            }
            catch (IllegalArgumentException ex) {
                LOG.log(Level.FINE, null, ex);
                return;
            }
            catch (InvocationTargetException ex) {
                LOG.log(Level.FINE, null, ex);
                return;
            }
            catch (NoSuchMethodException ex) {
                LOG.log(Level.FINE, null, ex);
                return;
            }
        }
        subMenuIcon.paintIcon(menu, g, x, y);
    }

    static {
        Class icon;
        LOG = Logger.getLogger(ListCompletionView.class.getName());
        ListCompletionView.icon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/hints/resources/suggestion.gif", (boolean)false);
        try {
            icon = ClassLoader.getSystemClassLoader().loadClass("sun.swing.plaf.synth.SynthIcon");
        }
        catch (ClassNotFoundException ex) {
            LOG.log(Level.FINEST, null, ex);
            icon = null;
        }
        Icon subMenuIconTemp = UIManager.getIcon("Menu.arrowIcon");
        if (subMenuIconTemp == null) {
            LookAndFeel laf = UIManager.getLookAndFeel();
            LOG.log(Level.INFO, "emptyMenuIcon, look and feel: {0}", laf != null ? laf.getClass().getName() : "<null>");
            subMenuIconTemp = new Icon(){

                @Override
                public void paintIcon(Component c, Graphics g, int x, int y) {
                }

                @Override
                public int getIconWidth() {
                    return 0;
                }

                @Override
                public int getIconHeight() {
                    return 0;
                }
            };
        }
        subMenuIcon = subMenuIconTemp;
        synthIcon = icon;
        subMenuIconIsSynthIcon = synthIcon != null && subMenuIcon != null && synthIcon.isAssignableFrom(subMenuIcon.getClass());
    }

    private final class RenderComponent
    extends JComponent {
        private Fix fix;
        private boolean selected;

        private RenderComponent() {
        }

        void setFix(Fix fix) {
            this.fix = fix;
        }

        void setSelected(boolean selected) {
            this.selected = selected;
        }

        @Override
        public void paintComponent(Graphics g) {
            int itemRenderWidth = ((JViewport)ListCompletionView.this.getParent()).getWidth();
            Color bgColor = this.getBackground();
            Color fgColor = this.getForeground();
            int height = this.getHeight();
            g.setColor(bgColor);
            g.fillRect(0, 0, itemRenderWidth, height);
            g.setColor(fgColor);
            ListCompletionView.this.renderHtml(this.fix, g, ListCompletionView.this.getFont(), this.getForeground(), itemRenderWidth, this.getHeight(), this.selected);
        }

        @Override
        public Dimension getPreferredSize() {
            if (ListCompletionView.this.cellPreferredSizeGraphics == null) {
                ListCompletionView.this.cellPreferredSizeGraphics = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(1, 1).getGraphics();
                assert (ListCompletionView.this.cellPreferredSizeGraphics != null);
            }
            return new Dimension(ListCompletionView.getPreferredWidth(this.fix, ListCompletionView.this.cellPreferredSizeGraphics, ListCompletionView.this.getFont()), ListCompletionView.this.fixedItemHeight);
        }
    }

    static class Model
    extends AbstractListModel
    implements PropertyChangeListener {
        private FixData data;
        private List<Fix> fixes;
        private boolean computed;
        static final long serialVersionUID = 3292276783870598274L;

        public Model(FixData data) {
            this.data = data;
            data.addPropertyChangeListener(this);
            this.update();
        }

        private synchronized void update() {
            this.computed = this.data.isComputed();
            if (this.computed) {
                this.fixes = this.data.getSortedFixes();
            } else {
                this.data.getSortedFixes();
            }
        }

        @Override
        public synchronized int getSize() {
            return this.computed ? this.fixes.size() : 1;
        }

        @Override
        public synchronized Object getElementAt(int index) {
            if (!this.computed) {
                return "computing...";
            }
            return this.fixes.get(index);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    HintsUI.getDefault().removePopups();
                    HintsUI.getDefault().showPopup(Model.this.data);
                }
            });
        }

    }

}

