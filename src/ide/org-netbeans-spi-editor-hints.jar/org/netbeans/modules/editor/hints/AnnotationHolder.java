/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightAttributeValue
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.Annotation
 *  org.openide.text.NbDocument
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.hints;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.hints.FixData;
import org.netbeans.modules.editor.hints.HintsUI;
import org.netbeans.modules.editor.hints.ParseErrorAnnotation;
import org.netbeans.spi.editor.highlighting.HighlightAttributeValue;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.LazyFixList;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.Annotation;
import org.openide.text.NbDocument;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class AnnotationHolder
implements ChangeListener,
DocumentListener {
    static final Logger LOG = Logger.getLogger(AnnotationHolder.class.getName());
    private static Map<String, Map<Severity, AttributeSet>> COLORINGS = Collections.synchronizedMap(new HashMap());
    private static Map<String, LookupListener> COLORINGS_LISTENERS = Collections.synchronizedMap(new HashMap());
    private static final AttributeSet DEFUALT_ERROR = AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.WaveUnderlineColor, new Color(255, 0, 0)});
    private static final AttributeSet DEFUALT_WARNING = AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.WaveUnderlineColor, new Color(192, 192, 0)});
    private static final AttributeSet DEFUALT_VERIFIER = AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.WaveUnderlineColor, new Color(255, 213, 85)});
    private static final AttributeSet TOOLTIP = AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.Tooltip, new TooltipResolver()});
    private Map<ErrorDescription, List<Position>> errors2Lines;
    private Map<Position, List<ErrorDescription>> line2Errors;
    private Map<Position, ParseErrorAnnotation> line2Annotations;
    private Map<String, List<ErrorDescription>> layer2Errors;
    private final Set<JEditorPane> openedComponents;
    private FileObject file;
    private DataObject od;
    private final BaseDocument doc;
    private static Map<DataObject, AnnotationHolder> file2Holder = new HashMap<DataObject, AnnotationHolder>();
    Attacher attacher;
    private static final RequestProcessor ATTACHING_THREAD;
    private List<ToDo> todo;
    private final Object todoLock;
    private final RequestProcessor.Task ATTACHER;
    private List<Reference<Position>> knownPositions;
    private static RuntimeException ABORT;
    private static final RequestProcessor INSTANCE;
    private static final boolean ENABLE_ASSERTS;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void resolveAllComponents() {
        Object c2;
        HashMap<DataObject, HashSet<Object>> file2Components = new HashMap<DataObject, HashSet<Object>>();
        for (Object c2 : EditorRegistry.componentList()) {
            Object o = c2.getDocument().getProperty("stream");
            if (!(o instanceof DataObject)) continue;
            DataObject od = (DataObject)o;
            HashSet<Object> components = (HashSet<Object>)file2Components.get((Object)od);
            if (components == null) {
                components = new HashSet<Object>();
                file2Components.put(od, components);
            }
            components.add(c2);
        }
        HashMap<DataObject, AnnotationHolder> file2HolderCopy = new HashMap<DataObject, AnnotationHolder>();
        c2 = AnnotationHolder.class;
        synchronized (AnnotationHolder.class) {
            file2HolderCopy.putAll(file2Holder);
            // ** MonitorExit[c] (shouldn't be in output)
            for (Map.Entry e : file2HolderCopy.entrySet()) {
                Set<JTextComponent> components = (Set<JTextComponent>)file2Components.get(e.getKey());
                if (components == null) {
                    components = Collections.emptySet();
                }
                ((AnnotationHolder)e.getValue()).setComponents(components);
            }
            return;
        }
    }

    public static synchronized AnnotationHolder getInstance(FileObject file) {
        if (file == null) {
            return null;
        }
        try {
            DataObject od = DataObject.find((FileObject)file);
            AnnotationHolder result = file2Holder.get((Object)od);
            if (result == null) {
                EditorCookie editorCookie = (EditorCookie)od.getCookie(EditorCookie.class);
                if (editorCookie == null) {
                    LOG.log(Level.WARNING, "No EditorCookie.Observable for file: {0}", FileUtil.getFileDisplayName((FileObject)file));
                } else {
                    StyledDocument doc = editorCookie.getDocument();
                    if (doc instanceof BaseDocument) {
                        result = new AnnotationHolder(file, od, (BaseDocument)doc);
                        file2Holder.put(od, result);
                    }
                }
            }
            return result;
        }
        catch (IOException e) {
            LOG.log(Level.FINE, null, e);
            return null;
        }
    }

    private AnnotationHolder(FileObject file, DataObject od, BaseDocument doc) {
        this.attacher = new NbDocumentAttacher();
        this.todoLock = new Object();
        this.ATTACHER = ATTACHING_THREAD.create(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                List todo = null;
                Object object = AnnotationHolder.this.todoLock;
                synchronized (object) {
                    todo = AnnotationHolder.this.todo;
                    AnnotationHolder.this.todo = null;
                }
                if (todo == null) {
                    return;
                }
                for (ToDo t : todo) {
                    AnnotationHolder.this.attachDetach(t);
                }
            }
        });
        this.knownPositions = new ArrayList<Reference<Position>>();
        this.openedComponents = new HashSet<JEditorPane>();
        this.doc = doc;
        if (file == null) {
            return;
        }
        this.init();
        this.file = file;
        this.od = od;
        AnnotationHolder.getBag((Document)doc);
        DocumentUtilities.addPriorityDocumentListener((Document)this.doc, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                AnnotationHolder.resolveAllComponents();
            }
        });
        Logger.getLogger("TIMER").log(Level.FINE, "Annotation Holder", new Object[]{file, this});
    }

    private synchronized void init() {
        this.errors2Lines = new IdentityHashMap<ErrorDescription, List<Position>>();
        this.line2Errors = new HashMap<Position, List<ErrorDescription>>();
        this.line2Annotations = new HashMap<Position, ParseErrorAnnotation>();
        this.layer2Errors = new HashMap<String, List<ErrorDescription>>();
    }

    @Override
    public void stateChanged(ChangeEvent evt) {
        this.updateVisibleRanges();
    }

    void attachAnnotation(Position line, ParseErrorAnnotation a, boolean synchronous) throws BadLocationException {
        this.attacher.attachAnnotation(line, a, synchronous);
    }

    void detachAnnotation(ParseErrorAnnotation a, boolean synchronous) {
        this.attacher.detachAnnotation(a, synchronous);
    }

    private void attachDetach(ToDo t) {
        if (t.lineStart != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("addAnnotation: pos=" + t.lineStart.getOffset() + ", a=" + t.a + ", doc=" + System.identityHashCode((Object)this.doc) + "\n");
            }
            t.a.attachAnnotation((StyledDocument)this.doc, t.lineStart);
        } else if (this.doc != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("removeAnnotation: a=" + t.a + ", doc=" + System.identityHashCode((Object)this.doc) + "\n");
            }
            t.a.detachAnnotation((StyledDocument)this.doc);
        }
    }

    private synchronized void clearAll() {
        for (ParseErrorAnnotation a : this.line2Annotations.values()) {
            this.detachAnnotation(a, false);
        }
        this.line2Annotations.clear();
        file2Holder.remove((Object)this.od);
        DocumentUtilities.removePriorityDocumentListener((Document)this.doc, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
        AnnotationHolder.getBag((Document)this.doc).clear();
    }

    private synchronized void maybeAddComponent(JTextComponent c) {
        if (!(c instanceof JEditorPane)) {
            return;
        }
        JEditorPane pane = (JEditorPane)c;
        if (!this.openedComponents.add(pane)) {
            return;
        }
        this.addViewportListener(pane);
        this.updateVisibleRanges();
    }

    private void addViewportListener(JEditorPane pane) {
        Container parent = pane.getParent();
        if (parent instanceof JViewport) {
            JViewport viewport = (JViewport)parent;
            viewport.addChangeListener(WeakListeners.change((ChangeListener)this, (Object)viewport));
        }
    }

    private synchronized void setComponents(Set<JTextComponent> newComponents) {
        if (newComponents.isEmpty()) {
            this.clearAll();
            return;
        }
        HashSet<JEditorPane> addedPanes = new HashSet<JEditorPane>();
        for (JTextComponent c : newComponents) {
            if (!(c instanceof JEditorPane)) continue;
            addedPanes.add((JEditorPane)c);
        }
        HashSet<JEditorPane> removedPanes = new HashSet<JEditorPane>(this.openedComponents);
        removedPanes.removeAll(addedPanes);
        addedPanes.removeAll(this.openedComponents);
        for (JEditorPane pane : addedPanes) {
            this.addViewportListener(pane);
        }
        this.openedComponents.removeAll(removedPanes);
        this.openedComponents.addAll(addedPanes);
        this.updateVisibleRanges();
    }

    @Override
    public synchronized void insertUpdate(DocumentEvent e) {
        try {
            int offset = Utilities.getRowStart((BaseDocument)this.doc, (int)e.getOffset());
            HashSet<Position> modifiedLines = new HashSet<Position>();
            int index = this.findPositionGE(offset);
            if (index == this.knownPositions.size()) {
                return;
            }
            Position line = this.knownPositions.get(index).get();
            if (line == null) {
                return;
            }
            int endOffset = Utilities.getRowEnd((BaseDocument)this.doc, (int)(e.getOffset() + e.getLength()));
            if (endOffset < line.getOffset()) {
                return;
            }
            this.clearLineErrors(line, modifiedLines);
            try {
                int rowStart = e.getOffset();
                int rowEnd = Utilities.getRowEnd((BaseDocument)this.doc, (int)(e.getOffset() + e.getLength()));
                AnnotationHolder.getBag((Document)this.doc).removeHighlights(rowStart, rowEnd, false);
            }
            catch (BadLocationException ex) {
                throw new IOException(ex);
            }
            for (Position lineToken : modifiedLines) {
                this.updateAnnotationOnLine(lineToken, false);
                this.updateHighlightsOnLine(lineToken);
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public synchronized void removeUpdate(DocumentEvent e) {
        try {
            Position current = null;
            int index = -1;
            int startOffset = Utilities.getRowStart((BaseDocument)this.doc, (int)e.getOffset());
            while (current == null) {
                index = this.findPositionGE(startOffset);
                if (this.knownPositions.isEmpty()) break;
                if (index == this.knownPositions.size()) {
                    return;
                }
                current = this.knownPositions.get(index).get();
            }
            if (current == null) {
                return;
            }
            int endOffset = Utilities.getRowEnd((BaseDocument)this.doc, (int)e.getOffset());
            if (endOffset < current.getOffset()) {
                return;
            }
            assert (index != -1);
            while (index > 0) {
                Position minusOne = this.knownPositions.get(index - 1).get();
                if (minusOne == null) {
                    --index;
                    continue;
                }
                if (minusOne.getOffset() != current.getOffset()) break;
                --index;
            }
            HashSet<Position> modifiedLinesTokens = new HashSet<Position>();
            while (index < this.knownPositions.size()) {
                Position next = this.knownPositions.get(index).get();
                if (next == null) {
                    ++index;
                    continue;
                }
                if (next.getOffset() != current.getOffset()) break;
                modifiedLinesTokens.add(next);
                ++index;
            }
            for (Position line2 : new LinkedList(modifiedLinesTokens)) {
                this.clearLineErrors(line2, modifiedLinesTokens);
            }
            for (Position line2 : modifiedLinesTokens) {
                this.updateAnnotationOnLine(line2, false);
                this.updateHighlightsOnLine(line2);
            }
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private void clearLineErrors(Position line, Set<Position> modifiedLinesTokens) {
        List<ErrorDescription> eds = this.getErrorsForLine(line, false);
        if (eds == null) {
            return;
        }
        eds = new LinkedList<ErrorDescription>(eds);
        for (ErrorDescription ed : eds) {
            List<Position> lines = this.errors2Lines.remove(ed);
            if (lines == null) {
                LOG.log(Level.WARNING, "Inconsistent error2Lines for file {1}.", new Object[]{this.file.getPath()});
                continue;
            }
            for (Position i : lines) {
                if (this.line2Errors.get(i) != null) {
                    this.line2Errors.get(i).remove(ed);
                }
                modifiedLinesTokens.add(i);
            }
            for (List edsForLayer : this.layer2Errors.values()) {
                edsForLayer.remove(ed);
            }
        }
        this.line2Errors.remove(line);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    private void updateVisibleRanges() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                final ArrayList visibleRanges = new ArrayList();
                AnnotationHolder.this.doc.render(new Runnable(){

                    @Override
                    public void run() {
                        for (JEditorPane pane : AnnotationHolder.this.openedComponents) {
                            Container parent = pane.getParent();
                            if (!(parent instanceof JViewport)) continue;
                            JViewport viewport = (JViewport)parent;
                            Point start = viewport.getViewPosition();
                            Dimension size = viewport.getExtentSize();
                            Point end = new Point(start.x + size.width, start.y + size.height);
                            int startPosition = pane.viewToModel(start);
                            int endPosition = pane.viewToModel(end);
                            visibleRanges.add(new int[]{startPosition, endPosition});
                        }
                    }
                });
                INSTANCE.post(new Runnable(){

                    @Override
                    public void run() {
                        for (int[] span : visibleRanges) {
                            AnnotationHolder.this.updateAnnotations(span[0], span[1]);
                        }
                    }
                });
                long endTime = System.currentTimeMillis();
                AnnotationHolder.LOG.log(Level.FINE, "updateVisibleRanges: time={0}", endTime - startTime);
            }

        });
    }

    private void updateAnnotations(final int startPosition, final int endPosition) {
        long startTime = System.currentTimeMillis();
        final ArrayList errorsToUpdate = new ArrayList();
        this.doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                AnnotationHolder annotationHolder = AnnotationHolder.this;
                synchronized (annotationHolder) {
                    try {
                        int end;
                        if (AnnotationHolder.this.doc.getLength() == 0) {
                            return;
                        }
                        int start = startPosition < AnnotationHolder.this.doc.getLength() ? startPosition : AnnotationHolder.this.doc.getLength() - 1;
                        int n = end = endPosition < AnnotationHolder.this.doc.getLength() ? endPosition : AnnotationHolder.this.doc.getLength() - 1;
                        if (start < 0) {
                            start = 0;
                        }
                        if (end < 0) {
                            end = 0;
                        }
                        int startLine = Utilities.getRowStart((BaseDocument)AnnotationHolder.this.doc, (int)start);
                        int endLine = Utilities.getRowEnd((BaseDocument)AnnotationHolder.this.doc, (int)end) + 1;
                        int index = AnnotationHolder.this.findPositionGE(startLine);
                        while (index < AnnotationHolder.this.knownPositions.size()) {
                            Position lineToken;
                            Reference r = (Reference)AnnotationHolder.this.knownPositions.get(index++);
                            if (r == null || (lineToken = (Position)r.get()) == null) continue;
                            if (lineToken.getOffset() <= endLine) {
                                List errors = (List)AnnotationHolder.this.line2Errors.get(lineToken);
                                if (errors == null) continue;
                                errorsToUpdate.addAll(errors);
                                continue;
                            }
                            break;
                        }
                    }
                    catch (BadLocationException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
            }
        });
        LOG.log(Level.FINE, "updateAnnotations: errorsToUpdate={0}", errorsToUpdate);
        for (ErrorDescription e : errorsToUpdate) {
            LazyFixList l;
            if (e == null || !(l = e.getFixes()).probablyContainsFixes() || l.isComputed()) continue;
            l.getFixes();
        }
        long endTime = System.currentTimeMillis();
        LOG.log(Level.FINE, "updateAnnotations: time={0}", endTime - startTime);
    }

    private List<ErrorDescription> getErrorsForLayer(String layer) {
        List<ErrorDescription> errors = this.layer2Errors.get(layer);
        if (errors == null) {
            errors = new ArrayList<ErrorDescription>();
            this.layer2Errors.put(layer, errors);
        }
        return errors;
    }

    private List<ErrorDescription> getErrorsForLine(Position line, boolean create) {
        List<ErrorDescription> errors = this.line2Errors.get(line);
        if (errors == null && create) {
            errors = new ArrayList<ErrorDescription>();
            this.line2Errors.put(line, errors);
        }
        if (errors != null && errors.isEmpty() && !create) {
            this.line2Errors.remove(line);
            errors = null;
        }
        return errors;
    }

    private static List<ErrorDescription> filter(List<ErrorDescription> errors, boolean onlyErrors) {
        ArrayList<ErrorDescription> result = new ArrayList<ErrorDescription>();
        for (ErrorDescription e : errors) {
            if (e.getSeverity() == Severity.ERROR) {
                if (!onlyErrors) continue;
                result.add(e);
                continue;
            }
            if (onlyErrors) continue;
            result.add(e);
        }
        return result;
    }

    private static void concatDescription(List<ErrorDescription> errors, StringBuffer description) {
        boolean first = true;
        for (ErrorDescription e : errors) {
            String desc = e.getDescription();
            if (desc == null || desc.length() <= 0) continue;
            if (!first) {
                description.append("\n\n");
            }
            description.append(desc);
            first = false;
        }
    }

    private LazyFixList computeFixes(List<ErrorDescription> errors) {
        ArrayList<LazyFixList> result = new ArrayList<LazyFixList>();
        for (ErrorDescription e : errors) {
            result.add(e.getFixes());
        }
        return ErrorDescriptionFactory.lazyListForDelegates(result);
    }

    private void updateAnnotationOnLine(Position line, boolean synchronous) throws BadLocationException {
        List errorDescriptions = this.getErrorsForLine(line, false);
        errorDescriptions = errorDescriptions == null ? Collections.emptyList() : new ArrayList<ErrorDescription>(errorDescriptions);
        Severity mostImportantSeverity = Severity.HINT;
        Iterator it = errorDescriptions.iterator();
        while (it.hasNext()) {
            ErrorDescription ed = (ErrorDescription)it.next();
            List<Position> positions = this.errors2Lines.get(ed);
            if (positions == null || positions.isEmpty() || positions.get(0) != line) {
                it.remove();
                continue;
            }
            if (mostImportantSeverity.compareTo(ed.getSeverity()) <= 0) continue;
            mostImportantSeverity = ed.getSeverity();
        }
        if (errorDescriptions.isEmpty()) {
            ParseErrorAnnotation ann = this.line2Annotations.remove(line);
            if (ann != null) {
                this.detachAnnotation(ann, synchronous);
            }
            return;
        }
        Pair<FixData, String> fixData = this.buildUpFixDataForLine(line);
        ParseErrorAnnotation pea = new ParseErrorAnnotation(mostImportantSeverity, (FixData)fixData.first(), (String)fixData.second(), line, this);
        ParseErrorAnnotation previous = this.line2Annotations.put(line, pea);
        if (previous != null) {
            this.detachAnnotation(previous, synchronous);
        }
        this.attachAnnotation(line, pea, synchronous);
    }

    public Pair<FixData, String> buildUpFixDataForLine(int caretLine) {
        try {
            Position line = this.getPosition(caretLine, false);
            if (line == null) {
                return null;
            }
            return this.buildUpFixDataForLine(line);
        }
        catch (BadLocationException ex) {
            LOG.log(Level.FINE, null, ex);
            return null;
        }
    }

    private Pair<FixData, String> buildUpFixDataForLine(Position line) {
        List<ErrorDescription> errorDescriptions = this.getErrorsForLine(line, true);
        if (errorDescriptions.isEmpty()) {
            return null;
        }
        List<ErrorDescription> trueErrors = AnnotationHolder.filter(errorDescriptions, true);
        List<ErrorDescription> others = AnnotationHolder.filter(errorDescriptions, false);
        StringBuffer description = new StringBuffer();
        AnnotationHolder.concatDescription(trueErrors, description);
        if (!trueErrors.isEmpty() && !others.isEmpty()) {
            description.append("\n\n");
        }
        AnnotationHolder.concatDescription(others, description);
        return Pair.of((Object)new FixData(this.computeFixes(trueErrors), this.computeFixes(others)), (Object)description.toString());
    }

    void updateHighlightsOnLine(Position line) throws IOException {
        List<ErrorDescription> errorDescriptions = this.getErrorsForLine(line, false);
        OffsetsBag bag = AnnotationHolder.getBag((Document)this.doc);
        AnnotationHolder.updateHighlightsOnLine(bag, this.doc, line, errorDescriptions);
    }

    static void updateHighlightsOnLine(OffsetsBag bag, BaseDocument doc, Position line, List<ErrorDescription> errorDescriptions) throws IOException {
        try {
            int rowStart = line.getOffset();
            int rowEnd = Utilities.getRowEnd((BaseDocument)doc, (int)rowStart);
            int rowHighlightStart = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)rowStart);
            int rowHighlightEnd = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)rowStart) + 1;
            if (rowStart <= rowEnd) {
                bag.removeHighlights(rowStart, rowEnd, false);
            }
            if (errorDescriptions != null) {
                bag.addAllHighlights(AnnotationHolder.computeHighlights((Document)doc, errorDescriptions).getHighlights(rowHighlightStart, rowHighlightEnd));
            }
        }
        catch (BadLocationException ex) {
            throw new IOException(ex);
        }
    }

    /*
     * Enabled aggressive block sorting
     */
    static OffsetsBag computeHighlights(Document doc, List<ErrorDescription> errorDescriptions) throws IOException, BadLocationException {
        OffsetsBag bag = new OffsetsBag(doc);
        Iterator<Severity> i$ = Arrays.asList(new Severity[]{Severity.VERIFIER, Severity.WARNING, Severity.ERROR}).iterator();
        block6 : while (i$.hasNext()) {
            Severity s = i$.next();
            ArrayList<ErrorDescription> filteredDescriptions = new ArrayList<ErrorDescription>();
            for (ErrorDescription e : errorDescriptions) {
                if (e.getSeverity() != s) continue;
                filteredDescriptions.add(e);
            }
            ArrayList<int[]> currentHighlights = new ArrayList<int[]>();
            Iterator i$2 = filteredDescriptions.iterator();
            do {
                if (!i$2.hasNext()) break;
                ErrorDescription e2 = (ErrorDescription)i$2.next();
                int beginOffset = e2.getRange().getBegin().getPosition().getOffset();
                int endOffset = e2.getRange().getEnd().getPosition().getOffset();
                if (endOffset < beginOffset) {
                    int swap = endOffset;
                    endOffset = beginOffset;
                    beginOffset = swap;
                    LOG.log(Level.WARNING, "Incorrect highlight in ErrorDescription, attach your messages.log to issue #112566: {0}", e2.toString());
                }
                int[] h = new int[]{beginOffset, endOffset};
                Iterator it = currentHighlights.iterator();
                block9 : while (it.hasNext() && h != null) {
                    int[] hl = (int[])it.next();
                    switch (AnnotationHolder.detectCollisions(hl, h)) {
                        case 0: {
                            break;
                        }
                        case 1: {
                            it.remove();
                            break;
                        }
                        case 2: {
                            h = null;
                            break block9;
                        }
                        case 3: 
                        case 4: {
                            int start = Math.min(hl[0], h[0]);
                            int end = Math.max(hl[1], h[1]);
                            h = new int[]{start, end};
                            it.remove();
                        }
                    }
                }
                if (h == null) continue;
                currentHighlights.add(h);
            } while (true);
            i$2 = currentHighlights.iterator();
            do {
                if (!i$2.hasNext()) continue block6;
                int[] h = (int[])i$2.next();
                if (h[0] <= h[1]) {
                    bag.addHighlight(h[0], h[1], AnnotationHolder.getColoring(s, doc));
                    continue;
                }
                StringBuilder sb = new StringBuilder();
                for (ErrorDescription e3 : filteredDescriptions) {
                    sb.append("[");
                    sb.append(e3.getRange().getBegin().getOffset());
                    sb.append("-");
                    sb.append(e3.getRange().getEnd().getOffset());
                    sb.append("]");
                }
                sb.append("=>");
                for (int[] h2 : currentHighlights) {
                    sb.append("[");
                    sb.append(h2[0]);
                    sb.append("-");
                    sb.append(h2[1]);
                    sb.append("]");
                }
                LOG.log(Level.WARNING, "Incorrect highlight computed, please reopen issue #112566 and attach the following output: {0}", sb.toString());
            } while (true);
            break;
        }
        return bag;
    }

    static AttributeSet getColoring(Severity s, Document d) {
        final String mimeType = DocumentUtilities.getMimeType((Document)d);
        Map<Severity, AttributeSet> coloring = COLORINGS.get(mimeType);
        if (coloring == null) {
            AttributeSet error;
            AttributeSet verifier;
            AttributeSet warning;
            Iterator it;
            coloring = new EnumMap<Severity, AttributeSet>(Severity.class);
            Lookup lookup = MimeLookup.getLookup((String)mimeType);
            Lookup.Result result = lookup.lookupResult(FontColorSettings.class);
            LookupListener lookupListener = COLORINGS_LISTENERS.get(mimeType);
            if (lookupListener == null) {
                lookupListener = new LookupListener(){

                    public void resultChanged(LookupEvent ev) {
                        COLORINGS.remove(mimeType);
                    }
                };
                COLORINGS_LISTENERS.put(mimeType, lookupListener);
                result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)lookupListener, (Object)result));
            }
            if ((it = result.allInstances().iterator()).hasNext()) {
                FontColorSettings fcs = (FontColorSettings)it.next();
                AttributeSet attributes = fcs.getTokenFontColors("errors");
                error = attributes != null ? attributes : ((attributes = fcs.getTokenFontColors("error")) != null ? attributes : DEFUALT_ERROR);
                attributes = fcs.getTokenFontColors("warning");
                if (attributes != null) {
                    warning = attributes;
                    verifier = attributes;
                } else {
                    warning = DEFUALT_WARNING;
                    verifier = DEFUALT_VERIFIER;
                }
            } else {
                error = DEFUALT_ERROR;
                warning = DEFUALT_WARNING;
                verifier = DEFUALT_VERIFIER;
            }
            coloring.put(Severity.ERROR, AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{error, TOOLTIP}));
            coloring.put(Severity.WARNING, AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{warning, TOOLTIP}));
            coloring.put(Severity.VERIFIER, AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{verifier, TOOLTIP}));
            coloring.put(Severity.HINT, TOOLTIP);
            COLORINGS.put(mimeType, coloring);
        }
        return coloring.get((Object)s);
    }

    private static int detectCollisions(int[] h1, int[] h2) {
        if (h2[1] < h1[0]) {
            return 0;
        }
        if (h1[1] < h2[0]) {
            return 0;
        }
        if (h2[0] < h1[0] && h2[1] > h1[1]) {
            return 1;
        }
        if (h1[0] < h2[0] && h1[1] > h2[1]) {
            return 2;
        }
        if (h1[0] < h2[0]) {
            return 3;
        }
        return 4;
    }

    public void setErrorDescriptions(String layer, Collection<? extends ErrorDescription> errors) {
        this.setErrorDescriptions(layer, errors, false);
    }

    private void setErrorDescriptions(final String layer, final Collection<? extends ErrorDescription> errors, final boolean synchronous) {
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                try {
                    AnnotationHolder.this.setErrorDescriptionsImpl(AnnotationHolder.this.file, layer, errors, synchronous);
                }
                catch (IOException e) {
                    AnnotationHolder.LOG.log(Level.WARNING, e.getMessage(), e);
                }
            }
        });
    }

    private synchronized void setErrorDescriptionsImpl(FileObject file, String layer, Collection<? extends ErrorDescription> errors, boolean synchronous) throws IOException {
        long start;
        block17 : {
            start = System.currentTimeMillis();
            if (file != null) break block17;
            long end = System.currentTimeMillis();
            Logger.getLogger("TIMER").log(Level.FINE, "Errors update for " + layer, new Object[]{file, end - start});
            return;
        }
        try {
            List<ErrorDescription> layersErrors = this.getErrorsForLayer(layer);
            HashSet<Position> primaryLines = new HashSet<Position>();
            HashSet<Position> allLines = new HashSet<Position>();
            for (ErrorDescription ed : layersErrors) {
                List<Position> lines = this.errors2Lines.remove(ed);
                if (lines == null) {
                    LOG.log(Level.WARNING, "Inconsistent error2Lines for layer {0}, file {1}.", new Object[]{layer, file.getPath()});
                    continue;
                }
                boolean first = true;
                for (Position line : lines) {
                    List<ErrorDescription> errorsForLine = this.getErrorsForLine(line, false);
                    if (errorsForLine != null) {
                        errorsForLine.remove(ed);
                    }
                    if (first) {
                        primaryLines.add(line);
                    }
                    allLines.add(line);
                    first = false;
                }
            }
            ArrayList<ErrorDescription> validatedErrors = new ArrayList<ErrorDescription>();
            for (ErrorDescription ed2 : errors) {
                if (ed2 == null) {
                    LOG.log(Level.WARNING, "'null' ErrorDescription in layer {0}.", layer);
                    continue;
                }
                if (ed2.getRange() == null) continue;
                validatedErrors.add(ed2);
                ArrayList<Position> lines = new ArrayList<Position>();
                int startLine = ed2.getRange().getBegin().getLine();
                int endLine = ed2.getRange().getEnd().getLine();
                for (int cntr = startLine; cntr <= endLine; ++cntr) {
                    Position p = this.getPosition(cntr, true);
                    lines.add(p);
                }
                this.errors2Lines.put(ed2, lines);
                boolean first = true;
                for (Position line : lines) {
                    this.getErrorsForLine(line, true).add(ed2);
                    if (first) {
                        primaryLines.add(line);
                    }
                    allLines.add(line);
                    first = false;
                }
            }
            layersErrors.clear();
            layersErrors.addAll(validatedErrors);
            for (Position line2 : primaryLines) {
                this.updateAnnotationOnLine(line2, synchronous);
            }
            for (Position line : allLines) {
                this.updateHighlightsOnLine(line);
            }
        }
        catch (BadLocationException ex) {
            try {
                throw new IOException(ex);
            }
            catch (Throwable var19_30) {
                long end = System.currentTimeMillis();
                Logger.getLogger("TIMER").log(Level.FINE, "Errors update for " + layer, new Object[]{file, end - start});
                throw var19_30;
            }
        }
        this.updateVisibleRanges();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                HintsUI.getDefault().caretUpdate(null);
            }
        });
        long end = System.currentTimeMillis();
        Logger.getLogger("TIMER").log(Level.FINE, "Errors update for " + layer, new Object[]{file, end - start});
    }

    private synchronized int findPositionGE(int offset) {
        do {
            try {
                int index = Collections.binarySearch(this.knownPositions, offset, new PositionComparator());
                if (index >= 0) {
                    return index;
                }
                return - index + 1;
            }
            catch (Abort a) {
                LOG.log(Level.FINE, "a null Position detected - clearing");
                int removedCount = 0;
                Iterator<Reference<Position>> it = this.knownPositions.iterator();
                while (it.hasNext()) {
                    if (it.next().get() != null) continue;
                    ++removedCount;
                    it.remove();
                }
                LOG.log(Level.FINE, "clearing finished, {0} positions cleared", removedCount);
                continue;
            }
            break;
        } while (true);
    }

    private synchronized Position getPosition(int lineNumber, boolean create) throws BadLocationException {
        do {
            int lineStart;
            int index;
            Position p;
            block12 : {
                lineStart = Utilities.getRowStartFromLineOffset((BaseDocument)this.doc, (int)lineNumber);
                if (lineStart < 0) {
                    Element lineRoot = this.doc.getDefaultRootElement();
                    int lineElementCount = lineRoot.getElementCount();
                    LOG.info("AnnotationHolder: Invalid lineNumber=" + lineNumber + ", lineStartOffset=" + lineStart + ", lineElementCount=" + lineElementCount + ", docReadLocked=" + DocumentUtilities.isReadLocked((Document)this.doc) + ", doc:\n" + (Object)this.doc + '\n');
                    lineStart = lineNumber < 0 ? 0 : lineRoot.getElement(lineRoot.getElementCount() - 1).getStartOffset();
                }
                try {
                    Reference<Position> r22;
                    index = Collections.binarySearch(this.knownPositions, lineStart, new PositionComparator());
                    if (index >= 0 && (p = (r22 = this.knownPositions.get(index)).get()) != null) {
                        Position position = p;
                        return position;
                    }
                    if (create) break block12;
                    Position r22 = null;
                    return r22;
                }
                catch (Abort a) {
                    LOG.log(Level.FINE, "a null Position detected - clearing");
                    int removedCount = 0;
                    Iterator<Reference<Position>> it = this.knownPositions.iterator();
                    while (it.hasNext()) {
                        if (it.next().get() != null) continue;
                        ++removedCount;
                        it.remove();
                    }
                    LOG.log(Level.FINE, "clearing finished, {0} positions cleared", removedCount);
                    continue;
                }
            }
            Position p2 = NbDocument.createPosition((Document)this.doc, (int)lineStart, (Position.Bias)Position.Bias.Forward);
            this.knownPositions.add(- index + 1, new WeakReference<Position>(p2));
            Logger.getLogger("TIMER").log(Level.FINE, "Annotation Holder - Line Token", new Object[]{this.file, p2});
            p = p2;
            return p;
            break;
        } while (true);
        finally {
            LOG.log(Level.FINE, "knownPositions.size={0}", this.knownPositions.size());
        }
    }

    public synchronized boolean hasErrors() {
        for (ErrorDescription e : this.errors2Lines.keySet()) {
            if (e.getSeverity() != Severity.ERROR) continue;
            return true;
        }
        return false;
    }

    public synchronized List<ErrorDescription> getErrors() {
        return new ArrayList<ErrorDescription>(this.errors2Lines.keySet());
    }

    public synchronized List<Annotation> getAnnotations() {
        return new ArrayList<Annotation>(this.line2Annotations.values());
    }

    public void setErrorsForLine(final int offset, final Map<String, List<ErrorDescription>> errs) {
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                if (offset > AnnotationHolder.this.doc.getLength()) {
                    return;
                }
                try {
                    if (offset > AnnotationHolder.this.doc.getLength()) {
                        return;
                    }
                    Position pos = AnnotationHolder.this.getPosition(Utilities.getLineOffset((BaseDocument)AnnotationHolder.this.doc, (int)offset), true);
                    List errsForCurrentLine = AnnotationHolder.this.getErrorsForLine(pos, true);
                    for (Map.Entry e : errs.entrySet()) {
                        HashSet errorsForLayer = new HashSet(AnnotationHolder.this.getErrorsForLayer((String)e.getKey()));
                        errorsForLayer.removeAll(errsForCurrentLine);
                        HashSet toSet = new HashSet();
                        toSet.addAll((Collection)e.getValue());
                        toSet.addAll(errorsForLayer);
                        ((List)e.getValue()).clear();
                        ((List)e.getValue()).addAll(toSet);
                    }
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        });
        for (Map.Entry<String, List<ErrorDescription>> e : errs.entrySet()) {
            List<ErrorDescription> eds = e.getValue();
            this.setErrorDescriptions(e.getKey(), eds, true);
        }
    }

    public synchronized List<ErrorDescription> getErrorsGE(int offset) {
        try {
            int index = this.findPositionGE(Utilities.getRowStart((BaseDocument)this.doc, (int)offset));
            if (index < 0) {
                return Collections.emptyList();
            }
            while (index < this.knownPositions.size()) {
                List<ErrorDescription> errors;
                Position current;
                if ((current = this.knownPositions.get(index++).get()) == null || (errors = this.line2Errors.get(current)) == null) continue;
                TreeMap<Integer, LinkedList<ErrorDescription>> sortedErrors = new TreeMap<Integer, LinkedList<ErrorDescription>>();
                for (ErrorDescription ed : errors) {
                    LinkedList<ErrorDescription> errs = (LinkedList<ErrorDescription>)sortedErrors.get(ed.getRange().getBegin().getOffset());
                    if (errs == null) {
                        errs = new LinkedList<ErrorDescription>();
                        sortedErrors.put(ed.getRange().getBegin().getOffset(), errs);
                    }
                    errs.add(ed);
                }
                SortedMap tail = sortedErrors.tailMap(offset);
                if (tail.isEmpty()) continue;
                Integer k = tail.firstKey();
                return new ArrayList<ErrorDescription>((Collection)sortedErrors.get(k));
            }
            return Collections.emptyList();
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return Collections.emptyList();
        }
    }

    public static OffsetsBag getBag(Document doc) {
        OffsetsBag ob = (OffsetsBag)doc.getProperty(AnnotationHolder.class);
        if (ob == null) {
            ob = new OffsetsBag(doc);
            doc.putProperty(AnnotationHolder.class, (Object)ob);
        }
        return ob;
    }

    public int lineNumber(final Position offset) {
        final int[] result = new int[]{-1};
        this.doc.render(new Runnable(){

            @Override
            public void run() {
                try {
                    result[0] = Utilities.getLineOffset((BaseDocument)AnnotationHolder.this.doc, (int)offset.getOffset());
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        });
        return result[0];
    }

    public static String resolveWarnings(final Document document, final int startOffset, final int endOffset) {
        final Object source = document.getProperty("stream");
        if (!(source instanceof DataObject) || !(document instanceof BaseDocument)) {
            return null;
        }
        final String[] result = new String[1];
        document.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    if (endOffset > document.getLength()) {
                        return;
                    }
                    int lineNumber = Utilities.getLineOffset((BaseDocument)((BaseDocument)document), (int)startOffset);
                    if (lineNumber < 0) {
                        return;
                    }
                    FileObject file = ((DataObject)source).getPrimaryFile();
                    AnnotationHolder h = AnnotationHolder.getInstance(file);
                    if (h == null) {
                        AnnotationHolder.LOG.log(Level.INFO, "File: {0}\nStartOffset: {1}", new Object[]{file.getPath(), startOffset});
                        return;
                    }
                    AnnotationHolder annotationHolder = h;
                    synchronized (annotationHolder) {
                        Position p = h.getPosition(lineNumber, false);
                        if (p == null) {
                            return;
                        }
                        List errors = (List)h.line2Errors.get(p);
                        if (errors == null || errors.isEmpty()) {
                            return;
                        }
                        LinkedList<ErrorDescription> trueErrors = new LinkedList<ErrorDescription>();
                        LinkedList<ErrorDescription> others = new LinkedList<ErrorDescription>();
                        for (ErrorDescription ed : errors) {
                            PositionBounds pb;
                            if (ed == null || startOffset > (pb = ed.getRange()).getEnd().getOffset() || pb.getBegin().getOffset() > endOffset || pb.getBegin().getOffset() == pb.getEnd().getOffset()) continue;
                            if (ed.getSeverity() == Severity.ERROR) {
                                trueErrors.add(ed);
                                continue;
                            }
                            others.add(ed);
                        }
                        StringBuffer description = new StringBuffer();
                        AnnotationHolder.concatDescription(trueErrors, description);
                        if (!trueErrors.isEmpty() && !others.isEmpty()) {
                            description.append("\n\n");
                        }
                        AnnotationHolder.concatDescription(others, description);
                        result[0] = description.toString();
                    }
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        });
        return result[0];
    }

    static {
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName() == null || "componentRemoved".equals(evt.getPropertyName())) {
                    AnnotationHolder.resolveAllComponents();
                } else if ("focusGained".equals(evt.getPropertyName())) {
                    JTextComponent c = EditorRegistry.focusedComponent();
                    if (c == null) {
                        AnnotationHolder.resolveAllComponents();
                        return;
                    }
                    Object o = c.getDocument().getProperty("stream");
                    AnnotationHolder holder = (AnnotationHolder)file2Holder.get(o);
                    if (holder != null) {
                        holder.maybeAddComponent(c);
                    }
                }
            }
        });
        ATTACHING_THREAD = new RequestProcessor(AnnotationHolder.class.getName(), 1, false, false);
        ABORT = new Abort();
        INSTANCE = new RequestProcessor("AnnotationHolder");
        ENABLE_ASSERTS = Boolean.getBoolean(AnnotationHolder.class.getName() + ".enableAsserts200469");
    }

    private static final class TooltipResolver
    implements HighlightAttributeValue<String> {
        private TooltipResolver() {
        }

        public String getValue(JTextComponent component, Document document, Object attributeKey, int startOffset, int endOffset) {
            return AnnotationHolder.resolveWarnings(document, startOffset, endOffset) + NbBundle.getMessage(AnnotationHolder.class, (String)"LBL_shortcut_promotion");
        }
    }

    private static class PositionComparator
    implements Comparator<Object> {
        private PositionComparator() {
        }

        @Override
        public int compare(Object o1, Object o2) {
            int left = -1;
            if (o1 instanceof Reference) {
                Position value = (Position)((Reference)o1).get();
                if (value == null) {
                    throw ABORT;
                }
                left = value.getOffset();
                assert (!ENABLE_ASSERTS || left != -1);
            } else if (o1 instanceof Integer) {
                left = (Integer)o1;
                assert (!ENABLE_ASSERTS || left != -1);
            } else assert (!ENABLE_ASSERTS);
            int right = -1;
            if (o2 instanceof Reference) {
                Position value = (Position)((Reference)o2).get();
                if (value == null) {
                    throw ABORT;
                }
                right = value.getOffset();
                assert (!ENABLE_ASSERTS || right != -1);
            } else if (o2 instanceof Integer) {
                right = (Integer)o2;
                assert (!ENABLE_ASSERTS || right != -1);
            } else assert (!ENABLE_ASSERTS);
            return left - right;
        }
    }

    private static class Abort
    extends RuntimeException {
        private Abort() {
        }

        @Override
        public synchronized Throwable fillInStackTrace() {
            return this;
        }
    }

    private static class ToDo {
        private final Position lineStart;
        private final ParseErrorAnnotation a;

        public ToDo(Position lineStart, ParseErrorAnnotation a) {
            this.lineStart = lineStart;
            this.a = a;
        }
    }

    final class NbDocumentAttacher
    implements Attacher {
        NbDocumentAttacher() {
        }

        @Override
        public void attachAnnotation(Position lineStart, ParseErrorAnnotation a, boolean synchronous) throws BadLocationException {
            this.addToToDo(new ToDo(lineStart, a), synchronous);
        }

        @Override
        public void detachAnnotation(ParseErrorAnnotation a, boolean synchronous) {
            this.addToToDo(new ToDo(null, a), synchronous);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void addToToDo(ToDo item, boolean synchronous) {
            if (synchronous) {
                AnnotationHolder.this.attachDetach(item);
                return;
            }
            Object object = AnnotationHolder.this.todoLock;
            synchronized (object) {
                if (AnnotationHolder.this.todo == null) {
                    AnnotationHolder.this.todo = new ArrayList();
                    AnnotationHolder.this.ATTACHER.schedule(50);
                }
                AnnotationHolder.this.todo.add(item);
            }
        }
    }

    static interface Attacher {
        public void attachAnnotation(Position var1, ParseErrorAnnotation var2, boolean var3) throws BadLocationException;

        public void detachAnnotation(ParseErrorAnnotation var1, boolean var2);
    }

}

