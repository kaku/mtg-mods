/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 */
package org.netbeans.modules.editor.hints;

import javax.swing.text.Document;
import org.netbeans.modules.editor.hints.AnnotationHolder;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;

public class HighlightsLayerFactoryImpl
implements HighlightsLayerFactory {
    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        OffsetsBag bag = AnnotationHolder.getBag(context.getDocument());
        return new HighlightsLayer[]{HighlightsLayer.create((String)AnnotationHolder.class.getName(), (ZOrder)ZOrder.SHOW_OFF_RACK.forPosition(420), (boolean)true, (HighlightsContainer)bag)};
    }
}

