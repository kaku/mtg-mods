/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.lib2.highlighting.HighlightingManager
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.hints;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.hints.AnnotationHolder;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class NextErrorAction
extends AbstractAction
implements PropertyChangeListener {
    public NextErrorAction() {
        this.putValue("Name", NbBundle.getMessage(NextErrorAction.class, (String)"LBL_Next_Error"));
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)WeakListeners.propertyChange((PropertyChangeListener)this, EditorRegistry.class));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final JTextComponent comp = EditorRegistry.focusedComponent();
        if (comp == null) {
            return;
        }
        comp.getDocument().render(new Runnable(){

            @Override
            public void run() {
                List errors = null;
                int errorOffset = -1;
                int unusedOffset = -1;
                int offsetToTest = comp.getCaretPosition() + 1;
                if (offsetToTest < comp.getDocument().getLength()) {
                    errors = NextErrorAction.this.findNextError(comp, offsetToTest);
                    errorOffset = errors.isEmpty() ? -1 : ((ErrorDescription)errors.iterator().next()).getRange().getBegin().getOffset();
                    unusedOffset = NextErrorAction.this.findNextUnused(comp, offsetToTest);
                }
                if (errorOffset == -1 && unusedOffset == -1) {
                    errors = NextErrorAction.this.findNextError(comp, 0);
                    errorOffset = errors.isEmpty() ? -1 : ((ErrorDescription)errors.iterator().next()).getRange().getBegin().getOffset();
                    unusedOffset = NextErrorAction.this.findNextUnused(comp, 0);
                }
                if (errorOffset == -1 && unusedOffset == -1) {
                    Toolkit.getDefaultToolkit().beep();
                } else if (errorOffset != -1 && (errorOffset < unusedOffset || unusedOffset == -1)) {
                    comp.getCaret().setDot(errorOffset);
                    Utilities.setStatusText((JTextComponent)comp, (String)NextErrorAction.this.buildText(errors), (int)700);
                } else {
                    comp.getCaret().setDot(unusedOffset);
                    Utilities.setStatusText((JTextComponent)comp, (String)NbBundle.getMessage(NextErrorAction.class, (String)"LBL_UnusedElement"), (int)700);
                }
            }
        });
    }

    private List<ErrorDescription> findNextError(JTextComponent comp, int offset) {
        BaseDocument doc = Utilities.getDocument((JTextComponent)comp);
        Object stream = doc.getProperty("stream");
        if (!(stream instanceof DataObject)) {
            return Collections.emptyList();
        }
        AnnotationHolder holder = AnnotationHolder.getInstance(((DataObject)stream).getPrimaryFile());
        List<ErrorDescription> errors = holder.getErrorsGE(offset);
        return errors;
    }

    private int findNextUnused(JTextComponent comp, int offset) {
        try {
            BaseDocument doc = Utilities.getDocument((JTextComponent)comp);
            int lineStart = Utilities.getRowStart((BaseDocument)doc, (int)offset);
            HighlightsSequence s = HighlightingManager.getInstance((JTextComponent)comp).getBottomHighlights().getHighlights(lineStart, Integer.MAX_VALUE);
            int lastUnusedEndOffset = -1;
            while (s.moveNext()) {
                if (!s.getAttributes().containsAttribute("unused-browseable", Boolean.TRUE)) continue;
                if (lastUnusedEndOffset != s.getStartOffset() && s.getStartOffset() >= offset) {
                    return s.getStartOffset();
                }
                lastUnusedEndOffset = s.getEndOffset();
            }
            return -1;
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return -1;
        }
    }

    private String buildText(List<ErrorDescription> errors) {
        LinkedList<ErrorDescription> trueErrors = new LinkedList<ErrorDescription>();
        LinkedList<ErrorDescription> others = new LinkedList<ErrorDescription>();
        for (ErrorDescription ed : errors) {
            if (ed == null) continue;
            if (ed.getSeverity() == Severity.ERROR) {
                trueErrors.add(ed);
                continue;
            }
            others.add(ed);
        }
        StringBuffer description = new StringBuffer();
        NextErrorAction.concatDescription(trueErrors, description);
        if (!trueErrors.isEmpty() && !others.isEmpty()) {
            description.append(" ");
        }
        NextErrorAction.concatDescription(others, description);
        return description.toString().replace('\n', ' ');
    }

    private static void concatDescription(List<ErrorDescription> errors, StringBuffer description) {
        boolean first = true;
        for (ErrorDescription e : errors) {
            if (!first) {
                description.append(" ");
            }
            description.append(e.getDescription());
            first = false;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.setEnabled(EditorRegistry.focusedComponent() != null);
    }

}

