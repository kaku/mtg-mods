/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.modules.editor.hints;

import org.netbeans.modules.editor.hints.HintsUI;
import org.openide.modules.ModuleInstall;

public final class HintsModule
extends ModuleInstall {
    public void restored() {
        HintsUI.getDefault();
    }
}

