/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.ImplementationProvider
 *  org.openide.awt.StatusDisplayer
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.editor.hints;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.ImplementationProvider;
import org.netbeans.modules.editor.hints.HintsUI;
import org.openide.awt.StatusDisplayer;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public class FixAction
extends AbstractAction {
    public FixAction() {
        this.putValue("Name", NbBundle.getMessage(FixAction.class, (String)"NM_FixAction"));
        this.putValue("supported-annotation-types", new String[]{"org-netbeans-spi-editor-hints-parser_annotation_err_fixable", "org-netbeans-spi-editor-hints-parser_annotation_warn_fixable", "org-netbeans-spi-editor-hints-parser_annotation_verifier_fixable", "org-netbeans-spi-editor-hints-parser_annotation_hint_fixable"});
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!HintsUI.getDefault().invokeDefaultAction(true)) {
            Action a;
            int nextAction;
            Object source = e.getSource();
            if (!(source instanceof JTextComponent)) {
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(FixAction.class, (String)"ERR_NoFixableError"));
                return;
            }
            Action[] actions = ImplementationProvider.getDefault().getGlyphGutterActions((JTextComponent)source);
            if (actions == null) {
                return;
            }
            for (nextAction = 0; nextAction < actions.length && actions[nextAction] != this; ++nextAction) {
            }
            if (actions.length > ++nextAction && (a = actions[nextAction]) != null && a.isEnabled()) {
                a.actionPerformed(e);
            }
        }
    }

    @Override
    public boolean isEnabled() {
        TopComponent activetc = TopComponent.getRegistry().getActivated();
        if (activetc instanceof CloneableEditorSupport.Pane) {
            return true;
        }
        return false;
    }
}

