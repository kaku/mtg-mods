/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.settings.storage.api;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.SettingsType;
import org.netbeans.modules.editor.settings.storage.StorageImpl;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.openide.util.RequestProcessor;

public final class EditorSettingsStorage<K, V> {
    public static final String PROP_DATA = "EditorSettingsStorage.PROP_DATA";
    private static RequestProcessor RP = new RequestProcessor(EditorSettingsStorage.class);
    private static final Map<StorageDescription<?, ?>, EditorSettingsStorage> cache = new HashMap();
    private final PropertyChangeSupport PCS;
    private final StorageImpl<K, V> storageImpl;

    public static <K, V> EditorSettingsStorage<K, V> get(String settingsTypeId) {
        EditorSettingsStorage<K, V> ess = EditorSettingsStorage.find(settingsTypeId);
        assert (ess != null);
        return ess;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <K, V> EditorSettingsStorage<K, V> find(String settingsTypeId) {
        Map map = cache;
        synchronized (map) {
            EditorSettingsStorage ess = null;
            StorageDescription sd = SettingsType.find(settingsTypeId);
            if (sd != null && (ess = cache.get(sd)) == null) {
                ess = new EditorSettingsStorage(sd);
                cache.put(sd, ess);
            }
            return ess;
        }
    }

    public Map<K, V> load(MimePath mimePath, String profile, boolean defaults) throws IOException {
        return this.storageImpl.load(mimePath, profile, defaults);
    }

    public void save(final MimePath mimePath, final String profile, final boolean defaults, final Map<K, V> data) throws IOException {
        if (SwingUtilities.isEventDispatchThread()) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    try {
                        EditorSettingsStorage.this.storageImpl.save(mimePath, profile, defaults, data);
                    }
                    catch (IOException ioe) {
                        Logger.getLogger(EditorSettingsStorage.class.getName()).log(Level.WARNING, null, ioe);
                    }
                }
            });
        } else {
            this.storageImpl.save(mimePath, profile, defaults, data);
        }
    }

    public void delete(MimePath mimePath, String profile, boolean defaults) throws IOException {
        this.storageImpl.delete(mimePath, profile, defaults);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.PCS.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.PCS.removePropertyChangeListener(l);
    }

    private EditorSettingsStorage(StorageDescription<K, V> storageDescription) {
        this.PCS = new PropertyChangeSupport(this);
        this.storageImpl = new StorageImpl<K, V>(storageDescription, new Callable<Void>(){

            @Override
            public Void call() {
                EditorSettingsStorage.this.PCS.firePropertyChange("EditorSettingsStorage.PROP_DATA", null, null);
                return null;
            }
        });
    }

}

