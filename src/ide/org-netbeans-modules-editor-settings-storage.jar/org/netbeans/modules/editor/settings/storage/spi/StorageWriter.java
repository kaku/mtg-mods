/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings.storage.spi;

import java.util.Map;
import java.util.Set;
import org.w3c.dom.Document;

public abstract class StorageWriter<K, V> {
    private Map<? extends K, ? extends V> added = null;
    private Set<? extends K> removed = null;

    protected StorageWriter() {
    }

    public final void setAdded(Map<? extends K, ? extends V> added) {
        this.added = added;
    }

    public final void setRemoved(Set<? extends K> removed) {
        this.removed = removed;
    }

    protected final Map<? extends K, ? extends V> getAdded() {
        assert (this.added != null);
        return this.added;
    }

    protected final Set<? extends K> getRemoved() {
        assert (this.removed != null);
        return this.removed;
    }

    public abstract Document getDocument();
}

