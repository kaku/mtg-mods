/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Utilities
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.editor.settings.storage.keybindings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.KeyStroke;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.netbeans.modules.editor.settings.storage.spi.support.StorageSupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Utilities;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public final class KeyMapsStorage
implements StorageDescription<Collection<KeyStroke>, MultiKeyBinding> {
    private static final Logger LOG = Logger.getLogger(KeyMapsStorage.class.getName());
    public static final String ID = "Keybindings";
    static final String MIME_TYPE = "text/x-nbeditor-keybindingsettings";
    private static final String ROOT = "bindings";
    private static final String E_BIND = "bind";
    private static final String A_ACTION_NAME = "actionName";
    private static final String A_KEY = "key";
    private static final String A_REMOVE = "remove";
    private static final String V_TRUE = "true";
    private static final String PUBLIC_ID = "-//NetBeans//DTD Editor KeyBindings settings 1.1//EN";
    private static final String SYSTEM_ID = "http://www.netbeans.org/dtds/EditorKeyBindings-1_1.dtd";

    @Override
    public String getId() {
        return "Keybindings";
    }

    @Override
    public boolean isUsingProfiles() {
        return true;
    }

    @Override
    public String getMimeType() {
        return "text/x-nbeditor-keybindingsettings";
    }

    @Override
    public String getLegacyFileName() {
        return "keybindings.xml";
    }

    @Override
    public StorageReader<Collection<KeyStroke>, MultiKeyBinding> createReader(FileObject f, String mimePath) {
        return new KeyMapsReader(f, mimePath);
    }

    @Override
    public StorageWriter<Collection<KeyStroke>, MultiKeyBinding> createWriter(FileObject f, String mimePath) {
        return new KeyMapsWriter();
    }

    private static final class KeyMapsWriter
    extends StorageWriter<Collection<KeyStroke>, MultiKeyBinding> {
        private static final Comparator<MultiKeyBinding> ACTION_NAME_COMPARATOR = new Comparator<MultiKeyBinding>(){

            @Override
            public int compare(MultiKeyBinding mkb1, MultiKeyBinding mkb2) {
                String actionName1 = mkb1.getActionName();
                String actionName2 = mkb2.getActionName();
                return actionName1.compareToIgnoreCase(actionName2);
            }
        };

        @Override
        public Document getDocument() {
            Document doc = XMLUtil.createDocument((String)"bindings", (String)null, (String)"-//NetBeans//DTD Editor KeyBindings settings 1.1//EN", (String)"http://www.netbeans.org/dtds/EditorKeyBindings-1_1.dtd");
            Node root = doc.getElementsByTagName("bindings").item(0);
            ArrayList added = new ArrayList(this.getAdded().values());
            Collections.sort(added, ACTION_NAME_COMPARATOR);
            for (MultiKeyBinding mkb : added) {
                Element bind = doc.createElement("bind");
                root.appendChild(bind);
                bind.setAttribute("actionName", mkb.getActionName());
                bind.setAttribute("key", StorageSupport.keyStrokesToString(mkb.getKeyStrokeList(), true));
            }
            for (Collection keyStrokes : this.getRemoved()) {
                String shortcut = StorageSupport.keyStrokesToString(keyStrokes, true);
                Element bind = doc.createElement("bind");
                root.appendChild(bind);
                bind.setAttribute("key", shortcut);
                bind.setAttribute("remove", "true");
            }
            return doc;
        }

    }

    private static class KeyMapsReader
    extends StorageReader<Collection<KeyStroke>, MultiKeyBinding> {
        private Map<Collection<KeyStroke>, MultiKeyBinding> keyMap = new HashMap<Collection<KeyStroke>, MultiKeyBinding>();
        private Set<Collection<KeyStroke>> removedShortcuts = new HashSet<Collection<KeyStroke>>();

        public KeyMapsReader(FileObject f, String mimePath) {
            super(f, mimePath);
            LOG.log(Level.FINEST, "Processing file: {0}", f.getPath());
        }

        @Override
        public Map<Collection<KeyStroke>, MultiKeyBinding> getAdded() {
            return this.keyMap;
        }

        @Override
        public Set<Collection<KeyStroke>> getRemoved() {
            return this.removedShortcuts;
        }

        @Override
        public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
            try {
                if (!name.equals("bindings") && name.equals("bind")) {
                    String key = attributes.getValue("key");
                    if (this.isModuleFile() && this.isDefaultProfile() && key != null && key.length() > 0) {
                        int idx = key.indexOf(45);
                        String proccessedFilePath = this.getProcessedFile().getPath();
                        if (!(idx == -1 || key.charAt(0) != 'A' && key.charAt(0) != 'C' || proccessedFilePath.endsWith("-mac.xml"))) {
                            LOG.warning("The keybinding '" + key + "' in " + proccessedFilePath + " may not work correctly on Mac. " + "Keybindings starting with Alt or Ctrl should " + "be coded with latin capital letters 'O' " + "or 'D' respectively. For details see org.openide.util.Utilities.stringToKey().");
                        }
                    }
                    KeyStroke[] shortcut = Utilities.stringToKeys((String)key.replaceAll("\\$", " "));
                    String remove = attributes.getValue("remove");
                    if (Boolean.valueOf(remove).booleanValue()) {
                        this.removedShortcuts.add(Arrays.asList(shortcut));
                    } else {
                        String actionName = attributes.getValue("actionName");
                        if (actionName != null) {
                            MultiKeyBinding mkb = new MultiKeyBinding(shortcut, actionName);
                            LOG.fine("Adding: Key: '" + key + "' Action: '" + mkb.getActionName() + "'");
                            MultiKeyBinding duplicate = this.keyMap.put(mkb.getKeyStrokeList(), mkb);
                            if (duplicate != null && !duplicate.getActionName().equals(mkb.getActionName())) {
                                LOG.warning("Duplicate shortcut '" + key + "' definition; rebound from '" + duplicate.getActionName() + "' to '" + mkb.getActionName() + "' in (" + this.getProcessedFile().getPath() + ").");
                            }
                        } else {
                            LOG.warning("Ignoring keybinding '" + key + "' with no action name.");
                        }
                    }
                }
            }
            catch (Exception ex) {
                LOG.log(Level.WARNING, "Can't parse keybindings file " + this.getProcessedFile().getPath(), ex);
            }
        }
    }

}

