/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.xml.EntityCatalog
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.editor.settings.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.xml.EntityCatalog;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class Utils {
    private static final Logger LOG = Logger.getLogger(Utils.class.getName());
    private static final Set<String> ALREADY_LOGGED = Collections.synchronizedSet(new HashSet());

    public static String getLocalizedName(FileObject fo, String defaultValue) {
        try {
            return fo.getFileSystem().getStatus().annotateName(defaultValue, Collections.singleton(fo));
        }
        catch (FileStateInvalidException ex) {
            if (LOG.isLoggable(Level.FINE)) {
                Utils.logOnce(LOG, Level.FINE, "Can't find localized name of " + (Object)fo, (Throwable)ex);
            }
            return defaultValue;
        }
    }

    public static String getLocalizedName(FileObject fo, String key, String defaultValue) {
        return Utils.getLocalizedName(fo, key, defaultValue, false);
    }

    public static String getLocalizedName(FileObject fo, String key, String defaultValue, boolean silent) {
        block4 : {
            assert (key != null);
            Object[] bundleInfo = Utils.findResourceBundle(fo, silent);
            if (bundleInfo[1] != null) {
                try {
                    return ((ResourceBundle)bundleInfo[1]).getString(key);
                }
                catch (MissingResourceException ex) {
                    if (silent || !LOG.isLoggable(Level.FINE)) break block4;
                    Utils.logOnce(LOG, Level.FINE, "The bundle '" + bundleInfo[0] + "' is missing key '" + key + "'.", ex);
                }
            }
        }
        return defaultValue;
    }

    private static Object[] findResourceBundle(FileObject fo, boolean silent) {
        assert (fo != null);
        Object[] bundleInfo = null;
        String bundleName = null;
        Object attrValue = fo.getAttribute("SystemFileSystem.localizingBundle");
        if (attrValue instanceof String) {
            bundleName = (String)attrValue;
        }
        if (bundleName != null) {
            try {
                bundleInfo = new Object[]{bundleName, NbBundle.getBundle((String)bundleName)};
            }
            catch (MissingResourceException ex) {
                if (!silent && LOG.isLoggable(Level.FINE)) {
                    Utils.logOnce(LOG, Level.FINE, "Can't find resource bundle for " + fo.getPath(), ex);
                }
            }
        } else if (!silent && LOG.isLoggable(Level.FINE)) {
            Utils.logOnce(LOG, Level.FINE, "The file " + fo.getPath() + " does not specify its resource bundle.", null);
        }
        if (bundleInfo == null) {
            bundleInfo = new Object[]{bundleName, null};
        }
        return bundleInfo;
    }

    public static void logOnce(Logger logger, Level level, String msg, Throwable t) {
        if (!ALREADY_LOGGED.contains(msg)) {
            ALREADY_LOGGED.add(msg);
            if (t != null) {
                logger.log(level, msg, t);
            } else {
                logger.log(level, msg);
            }
            if (ALREADY_LOGGED.size() > 100) {
                ALREADY_LOGGED.clear();
            }
        }
    }

    public static MimePath mimeTypes2mimePath(String[] mimeTypes) {
        MimePath mimePath = MimePath.EMPTY;
        for (int i = 0; i < mimeTypes.length; ++i) {
            mimePath = MimePath.get((MimePath)mimePath, (String)mimeTypes[i]);
        }
        return mimePath;
    }

    public static /* varargs */ Map<String, AttributeSet> immutize(Map<String, ? extends AttributeSet> map, Object ... filterOutKeys) {
        HashMap<String, AttributeSet> immutizedMap = new HashMap<String, AttributeSet>();
        for (String name : map.keySet()) {
            AttributeSet attribs = map.get(name);
            if (filterOutKeys.length == 0) {
                immutizedMap.put(name, AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[]{attribs}));
                continue;
            }
            ArrayList<Object> pairs = new ArrayList<Object>();
            Enumeration keys = attribs.getAttributeNames();
            block1 : while (keys.hasMoreElements()) {
                Object key = keys.nextElement();
                for (Object filterOutKey : filterOutKeys) {
                    if (Utilities.compareObjects(key, (Object)filterOutKey)) continue block1;
                }
                pairs.add(key);
                pairs.add(attribs.getAttribute(key));
            }
            immutizedMap.put(name, AttributesUtilities.createImmutable((Object[])pairs.toArray()));
        }
        return Collections.unmodifiableMap(immutizedMap);
    }

    public static Map<String, AttributeSet> immutize(Collection<AttributeSet> set) {
        HashMap<String, AttributeSet> immutizedMap = new HashMap<String, AttributeSet>();
        for (AttributeSet as : set) {
            Object nameObject = as.getAttribute(StyleConstants.NameAttribute);
            if (nameObject instanceof String) {
                immutizedMap.put((String)nameObject, as);
                continue;
            }
            LOG.warning("Ignoring AttributeSet with invalid StyleConstants.NameAttribute. AttributeSet: " + as);
        }
        return Collections.unmodifiableMap(immutizedMap);
    }

    public static <A, B> void diff(Map<A, B> oldMap, Map<A, B> newMap, Map<A, B> addedEntries, Map<A, B> removedEntries) {
        for (A key2 : oldMap.keySet()) {
            if (!newMap.containsKey(key2)) {
                removedEntries.put(key2, oldMap.get(key2));
                continue;
            }
            if (Utilities.compareObjects(oldMap.get(key2), newMap.get(key2))) continue;
            addedEntries.put(key2, newMap.get(key2));
        }
        for (A key2 : newMap.keySet()) {
            if (oldMap.containsKey(key2)) continue;
            addedEntries.put(key2, newMap.get(key2));
        }
    }

    public static <A, B> boolean quickDiff(Map<A, B> oldMap, Map<A, B> newMap) {
        for (A key2 : oldMap.keySet()) {
            if (!newMap.containsKey(key2)) {
                return true;
            }
            if (Utilities.compareObjects(oldMap.get(key2), newMap.get(key2))) continue;
            return true;
        }
        for (A key2 : newMap.keySet()) {
            if (oldMap.containsKey(key2)) continue;
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void save(FileObject fo, StorageWriter writer) {
        assert (fo != null);
        assert (writer != null);
        try {
            FileLock lock = fo.lock();
            try {
                OutputStream os = fo.getOutputStream(lock);
                try {
                    XMLUtil.write((Document)writer.getDocument(), (OutputStream)os, (String)"UTF-8");
                }
                finally {
                    os.close();
                }
            }
            finally {
                lock.releaseLock();
            }
        }
        catch (IOException ex) {
            LOG.log(Level.WARNING, "Can't save editor settings to " + fo.getPath(), ex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void load(FileObject fo, StorageReader handler, boolean validate) {
        assert (fo != null);
        assert (handler != null);
        try {
            XMLReader reader = XMLUtil.createXMLReader((boolean)validate);
            reader.setEntityResolver((EntityResolver)EntityCatalog.getDefault());
            reader.setContentHandler(handler);
            reader.setErrorHandler(handler);
            reader.setProperty("http://xml.org/sax/properties/lexical-handler", handler);
            InputStream is = fo.getInputStream();
            try {
                reader.parse(new InputSource(is));
            }
            finally {
                is.close();
            }
        }
        catch (Exception ex) {
            LOG.log(Level.WARNING, "Invalid or corrupted file: " + fo.getPath(), ex);
        }
    }

    public static final class NoNetworkAccessEntityCatalog
    extends EntityCatalog {
        private final boolean NO_NETWORK_ACCESS = Boolean.getBoolean("editor.storage.no.network.access");

        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
            if (!this.NO_NETWORK_ACCESS) {
                return null;
            }
            return new InputSource(new ByteArrayInputStream(new byte[0]));
        }
    }

}

