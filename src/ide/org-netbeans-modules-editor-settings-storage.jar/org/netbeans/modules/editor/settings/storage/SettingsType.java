/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class SettingsType {
    private static final Logger LOG = Logger.getLogger(SettingsType.class.getName());

    public static <K, V> StorageDescription<K, V> find(String id) {
        assert (id != null);
        StorageDescription sd = Cache.getInstance().find(id);
        return sd;
    }

    public static Locator getLocator(StorageDescription sd) {
        assert (sd != null);
        DefaultLocator locator = "FontsColors".equals(sd.getId()) ? new FontsColorsLocator(sd.getId(), sd.isUsingProfiles(), sd.getMimeType(), sd.getLegacyFileName()) : ("Keybindings".equals(sd.getId()) || "Preferences".equals(sd.getId()) ? new LegacyTextBaseLocator(sd.getId(), sd.isUsingProfiles(), sd.getMimeType(), sd.getLegacyFileName()) : new DefaultLocator(sd.getId(), sd.isUsingProfiles(), sd.getMimeType(), sd.getLegacyFileName()));
        return locator;
    }

    private SettingsType() {
    }

    private static final class LegacyTextBaseLocator
    extends DefaultLocator {
        public LegacyTextBaseLocator(String settingTypeId, boolean hasProfiles, String mimeType, String legacyFileName) {
            super(settingTypeId, hasProfiles, mimeType, legacyFileName);
        }

        @Override
        protected FileObject getLegacyMimeFolder(FileObject baseFolder, String mimeType) {
            if (mimeType == null || mimeType.length() == 0) {
                return baseFolder.getFileObject("text/base");
            }
            return this.getMimeFolder(baseFolder, mimeType);
        }
    }

    private static final class FontsColorsLocator
    extends DefaultLocator {
        private static final String[] M_LEGACY_FILE_NAMES = new String[]{"Defaults/defaultColoring.xml", "Defaults/coloring.xml", "Defaults/editorColoring.xml"};
        private static final String[] U_LEGACY_FILE_NAMES = new String[]{"defaultColoring.xml", "coloring.xml", "editorColoring.xml"};

        public FontsColorsLocator(String settingTypeId, boolean hasProfiles, String mimeType, String legacyFileName) {
            super(settingTypeId, hasProfiles, mimeType, legacyFileName);
        }

        @Override
        protected void addModulesLegacyFiles(FileObject mimeFolder, String profileId, boolean fullScan, Map<String, List<Object[]>> files) {
            this.addFiles(mimeFolder, profileId, fullScan, M_LEGACY_FILE_NAMES, files, true);
        }

        @Override
        protected void addUsersLegacyFiles(FileObject mimeFolder, String profileId, boolean fullScan, Map<String, List<Object[]>> files) {
            this.addFiles(mimeFolder, profileId, fullScan, U_LEGACY_FILE_NAMES, files, false);
        }

        private void addFiles(FileObject mimeFolder, String profileId, boolean fullScan, String[] filePaths, Map<String, List<Object[]>> files, boolean moduleFiles) {
            if (profileId == null) {
                FileObject[] profileHomes;
                for (FileObject f : profileHomes = mimeFolder.getChildren()) {
                    if (!f.isFolder()) continue;
                    String id = f.getNameExt();
                    this.addFiles(f, filePaths, fullScan, files, id, f, moduleFiles);
                }
            } else {
                FileObject profileHome = mimeFolder.getFileObject(profileId);
                if (profileHome != null && profileHome.isFolder()) {
                    this.addFiles(profileHome, filePaths, fullScan, files, profileId, profileHome, moduleFiles);
                }
            }
        }

        private void addFiles(FileObject folder, String[] filePaths, boolean fullScan, Map<String, List<Object[]>> files, String profileId, FileObject profileHome, boolean moduleFiles) {
            for (String filePath : filePaths) {
                FileObject f = folder.getFileObject(filePath);
                if (f == null) continue;
                List<Object[]> pair = files.get(profileId);
                if (pair == null) {
                    pair = new ArrayList<Object[]>();
                    files.put(profileId, pair);
                }
                pair.add(new Object[]{profileHome, f, moduleFiles, null, true});
                if (LOG.isLoggable(Level.INFO)) {
                    Utils.logOnce(LOG, Level.INFO, this.settingTypeId + " settings " + "should reside in '" + this.settingTypeId + "' subfolder, " + "see #90403 for details. Offending file '" + f.getPath() + "'", null);
                }
                if (!fullScan) break;
            }
        }
    }

    private static class DefaultLocator
    implements Locator {
        protected static final String MODULE_FILES_FOLDER = "Defaults";
        protected static final String DEFAULT_PROFILE_NAME = "NetBeans";
        private static final String WRITABLE_FILE_PREFIX = "org-netbeans-modules-editor-settings-Custom";
        private static final String WRITABLE_FILE_SUFFIX = ".xml";
        private static final String FA_TARGET_OS = "nbeditor-settings-targetOS";
        private static final String LINK_EXTENSION = "shadow";
        protected final String settingTypeId;
        protected final boolean isUsingProfiles;
        protected final String mimeType;
        protected final String legacyFileName;
        protected final String writableFilePrefix;
        protected final String modulesWritableFilePrefix;
        protected final ThreadLocal<FileObject> threadsBaseFolder = new ThreadLocal();

        public DefaultLocator(String settingTypeId, boolean hasProfiles, String mimeType, String legacyFileName) {
            this.settingTypeId = settingTypeId;
            this.isUsingProfiles = hasProfiles;
            this.mimeType = mimeType;
            this.legacyFileName = legacyFileName;
            this.writableFilePrefix = "org-netbeans-modules-editor-settings-Custom" + settingTypeId;
            this.modulesWritableFilePrefix = "Defaults/" + this.writableFilePrefix;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public final void scan(FileObject baseFolder, String mimeType, String profileId, boolean fullScan, boolean scanModules, boolean scanUsers, boolean resolveLinks, Map<String, List<Object[]>> results) {
            assert (results != null);
            this.threadsBaseFolder.set(baseFolder);
            try {
                FileObject mimeFolder = null;
                FileObject legacyMimeFolder = null;
                if (baseFolder != null) {
                    mimeFolder = this.getMimeFolder(baseFolder, mimeType);
                    legacyMimeFolder = this.getLegacyMimeFolder(baseFolder, mimeType);
                }
                if (scanModules) {
                    if (legacyMimeFolder != null && legacyMimeFolder.isFolder()) {
                        this.addModulesLegacyFiles(legacyMimeFolder, profileId, fullScan, results);
                    }
                    if (mimeFolder != null && mimeFolder.isFolder()) {
                        this.addModulesFiles(mimeFolder, profileId, fullScan, results, resolveLinks);
                    }
                }
                if (scanUsers) {
                    if (legacyMimeFolder != null && legacyMimeFolder.isFolder()) {
                        this.addUsersLegacyFiles(legacyMimeFolder, profileId, fullScan, results);
                    }
                    if (mimeFolder != null && mimeFolder.isFolder()) {
                        this.addUsersFiles(mimeFolder, profileId, fullScan, results);
                    }
                }
            }
            finally {
                this.threadsBaseFolder.remove();
            }
        }

        @Override
        public final String getWritableFileName(String mimeType, String profileId, String fileId, boolean modulesFile) {
            StringBuilder part = new StringBuilder(127);
            if (mimeType == null || mimeType.length() == 0) {
                part.append(this.settingTypeId).append('/');
            } else {
                part.append(mimeType).append('/').append(this.settingTypeId).append('/');
            }
            if (this.isUsingProfiles) {
                assert (profileId != null);
                part.append(profileId).append('/');
            }
            if (modulesFile) {
                part.append(this.modulesWritableFilePrefix);
            } else {
                part.append(this.writableFilePrefix);
            }
            if (fileId != null && fileId.length() != 0) {
                part.append(fileId);
            }
            part.append(".xml");
            return part.toString();
        }

        @Override
        public final boolean isUsingProfiles() {
            return this.isUsingProfiles;
        }

        protected FileObject getLegacyMimeFolder(FileObject baseFolder, String mimeType) {
            return mimeType == null ? baseFolder : baseFolder.getFileObject(mimeType);
        }

        protected void addModulesLegacyFiles(FileObject mimeFolder, String profileId, boolean fullScan, Map<String, List<Object[]>> files) {
            if (this.legacyFileName != null) {
                this.addLegacyFiles(mimeFolder, profileId, "Defaults/" + this.legacyFileName, files, true);
            }
        }

        protected void addUsersLegacyFiles(FileObject mimeFolder, String profileId, boolean fullScan, Map<String, List<Object[]>> files) {
            if (this.legacyFileName != null) {
                this.addLegacyFiles(mimeFolder, profileId, this.legacyFileName, files, true);
            }
        }

        private FileObject getMimeFolder(FileObject baseFolder, String mimeType) {
            return mimeType == null ? baseFolder : baseFolder.getFileObject(mimeType);
        }

        private void addModulesFiles(FileObject mimeFolder, String profileId, boolean fullScan, Map<String, List<Object[]>> files, boolean resolveLinks) {
            if (profileId == null) {
                FileObject settingHome = mimeFolder.getFileObject(this.settingTypeId);
                if (settingHome != null && settingHome.isFolder()) {
                    if (this.isUsingProfiles) {
                        FileObject[] profileHomes;
                        for (FileObject f : profileHomes = settingHome.getChildren()) {
                            if (!f.isFolder()) continue;
                            String id = f.getNameExt();
                            FileObject folder = f.getFileObject("Defaults");
                            if (folder == null || !folder.isFolder()) continue;
                            this.addFiles(folder, fullScan, files, id, f, true, resolveLinks);
                        }
                    } else {
                        FileObject folder = settingHome.getFileObject("Defaults");
                        if (folder != null && folder.isFolder()) {
                            this.addFiles(folder, fullScan, files, null, null, true, resolveLinks);
                        }
                    }
                }
            } else {
                FileObject folder = mimeFolder.getFileObject(this.settingTypeId + "/" + profileId + "/" + "Defaults");
                if (folder != null && folder.isFolder()) {
                    this.addFiles(folder, fullScan, files, profileId, folder.getParent(), true, resolveLinks);
                }
            }
        }

        private void addUsersFiles(FileObject mimeFolder, String profileId, boolean fullScan, Map<String, List<Object[]>> files) {
            if (profileId == null) {
                FileObject settingHome = mimeFolder.getFileObject(this.settingTypeId);
                if (settingHome != null && settingHome.isFolder()) {
                    if (this.isUsingProfiles) {
                        FileObject[] profileHomes;
                        for (FileObject f : profileHomes = settingHome.getChildren()) {
                            if (!f.isFolder()) continue;
                            String id = f.getNameExt();
                            this.addFiles(f, fullScan, files, id, f, false, false);
                        }
                    } else {
                        this.addFiles(settingHome, fullScan, files, null, null, false, false);
                    }
                }
            } else {
                FileObject folder = mimeFolder.getFileObject(this.settingTypeId + "/" + profileId);
                if (folder != null && folder.isFolder()) {
                    this.addFiles(folder, fullScan, files, profileId, folder, false, false);
                }
            }
        }

        private final void addFiles(FileObject folder, boolean fullScan, Map<String, List<Object[]>> files, String profileId, FileObject profileHome, boolean moduleFiles, boolean resolveLinks) {
            List<Object[]> infos;
            ArrayList<Object[]> writableFiles = new ArrayList<Object[]>();
            ArrayList<Object[]> osSpecificFiles = new ArrayList<Object[]>();
            List ff = FileUtil.getOrder(Arrays.asList(folder.getChildren()), (boolean)false);
            for (FileObject f : ff) {
                Object[] oo;
                List<Object[]> infos2;
                if (!f.isData()) continue;
                if (resolveLinks && "shadow".equals(f.getExt())) {
                    FileObject linkTarget = this.resolveLink(f);
                    if (linkTarget == null) continue;
                    infos2 = files.get(profileId);
                    if (infos2 == null) {
                        infos2 = new ArrayList<Object[]>();
                        files.put(profileId, infos2);
                    }
                    oo = new Object[]{profileHome, f, true, linkTarget, false};
                    infos2.add(oo);
                    continue;
                }
                if (this.mimeType.equals(FileUtil.getMIMEType((FileObject)f, (String[])new String[]{this.mimeType}))) {
                    Object targetOs;
                    block17 : {
                        targetOs = f.getAttribute("nbeditor-settings-targetOS");
                        if (targetOs != null) {
                            try {
                                if (!this.isApplicableForThisTargetOs(targetOs)) {
                                    LOG.fine("Ignoring OS specific file: '" + f.getPath() + "', it's targetted for '" + targetOs + "'");
                                }
                                break block17;
                            }
                            catch (Exception e) {
                                LOG.log(Level.WARNING, "Ignoring editor settings file with invalid OS type mask '" + targetOs + "' file: '" + f.getPath() + "'");
                            }
                            continue;
                        }
                    }
                    if ((infos2 = files.get(profileId)) == null) {
                        infos2 = new ArrayList<Object[]>();
                        files.put(profileId, infos2);
                    }
                    oo = new Object[]{profileHome, f, moduleFiles, null, false};
                    if (moduleFiles) {
                        if (f.getNameExt().startsWith(this.writableFilePrefix)) {
                            writableFiles.add(oo);
                        } else if (targetOs != null) {
                            osSpecificFiles.add(oo);
                        } else {
                            infos2.add(oo);
                        }
                    } else {
                        infos2.add(oo);
                    }
                    if (fullScan) continue;
                    break;
                }
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine("Ignoring file: '" + f.getPath() + "' of type " + f.getMIMEType());
            }
            if (!osSpecificFiles.isEmpty()) {
                infos = files.get(profileId);
                infos.addAll(osSpecificFiles);
            }
            if (!writableFiles.isEmpty()) {
                infos = files.get(profileId);
                infos.addAll(writableFiles);
            }
        }

        private boolean isApplicableForThisTargetOs(Object targetOs) throws NoSuchFieldException, IllegalAccessException {
            if (targetOs instanceof Boolean) {
                return (Boolean)targetOs;
            }
            if (targetOs instanceof String) {
                Field field = Utilities.class.getDeclaredField((String)targetOs);
                int targetOsMask = field.getInt(null);
                int currentOsId = Utilities.getOperatingSystem();
                return (currentOsId & targetOsMask) != 0;
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private FileObject resolveLink(FileObject link) {
            FileObject targetFile;
            block23 : {
                String targetFilePath = null;
                String targetFilesystem = null;
                if (link.getSize() == 0) {
                    Object fileName = link.getAttribute("originalFile");
                    if (fileName instanceof String) {
                        targetFilePath = (String)fileName;
                    } else if (fileName instanceof URL) {
                        targetFilePath = ((URL)fileName).toExternalForm();
                    } else {
                        LOG.warning("Invalid originalFile '" + fileName + "', ignoring editor settings link " + link.getPath());
                    }
                    Object filesystemName = link.getAttribute("originalFileSystem");
                    if (filesystemName instanceof String) {
                        targetFilesystem = (String)filesystemName;
                    }
                } else {
                    try {
                        BufferedReader ois = new BufferedReader(new InputStreamReader(link.getInputStream(), "UTF-8"));
                        try {
                            targetFilePath = ois.readLine();
                            targetFilesystem = ois.readLine();
                        }
                        finally {
                            ois.close();
                        }
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.WARNING, "Can't read editor settings link, ignoring " + link.getPath(), ioe);
                        targetFilePath = null;
                    }
                }
                if (targetFilesystem != null && !targetFilesystem.equals("SystemFileSystem")) {
                    LOG.warning("Editor settings links can only link targets on SystemFileSystem, ignoring link " + link.getPath());
                    targetFilePath = null;
                }
                targetFile = null;
                if (targetFilePath != null) {
                    URI u;
                    try {
                        u = new URI(targetFilePath);
                    }
                    catch (URISyntaxException e) {
                        u = null;
                    }
                    if (u != null && u.isAbsolute()) {
                        try {
                            FileObject ff = URLMapper.findFileObject((URL)u.toURL());
                            if (ff == null) break block23;
                            if (ff.getFileSystem().isDefault()) {
                                targetFile = ff;
                                break block23;
                            }
                            LOG.warning("Editor settings links can only link targets on SystemFileSystem, ignoring link " + link.getPath());
                        }
                        catch (IOException ioe) {
                            LOG.log(Level.WARNING, "Can't resolve editor settings link, ignoring link " + link.getPath(), ioe);
                        }
                    } else {
                        targetFile = FileUtil.getConfigFile((String)targetFilePath);
                    }
                }
            }
            if (targetFile != null) {
                FileObject baseFolder = this.threadsBaseFolder.get();
                if (!targetFile.isFolder() || targetFile == baseFolder || !FileUtil.isParentOf((FileObject)baseFolder, (FileObject)targetFile)) {
                    LOG.warning("Editor settings link '" + link.getPath() + "' is not pointing to " + "a real subfolder of '" + baseFolder.getPath() + "', but to '" + targetFile.getPath() + "'. Ignoring the link.");
                    targetFile = null;
                }
            }
            return targetFile;
        }

        private void addLegacyFiles(FileObject mimeFolder, String profileId, String filePath, Map<String, List<Object[]>> files, boolean moduleFiles) {
            if (profileId == null) {
                String defaultProfileId;
                if (this.isUsingProfiles) {
                    FileObject[] profileHomes;
                    for (FileObject f : profileHomes = mimeFolder.getChildren()) {
                        if (!f.isFolder() || f.getNameExt().equals("Defaults")) continue;
                        String id = f.getNameExt();
                        FileObject legacyFile = f.getFileObject(filePath);
                        if (legacyFile == null) continue;
                        this.addFile(legacyFile, files, id, f, true);
                    }
                    defaultProfileId = "NetBeans";
                } else {
                    defaultProfileId = null;
                }
                FileObject legacyFile = mimeFolder.getFileObject(filePath);
                if (legacyFile != null) {
                    this.addFile(legacyFile, files, defaultProfileId, null, true);
                }
            } else if (profileId.equals("NetBeans")) {
                FileObject file = mimeFolder.getFileObject(filePath);
                if (file != null) {
                    this.addFile(file, files, profileId, null, moduleFiles);
                }
            } else {
                FileObject file;
                FileObject profileHome = mimeFolder.getFileObject(profileId);
                if (profileHome != null && profileHome.isFolder() && (file = profileHome.getFileObject(filePath)) != null) {
                    this.addFile(file, files, profileId, profileHome, moduleFiles);
                }
            }
        }

        private void addFile(FileObject file, Map<String, List<Object[]>> files, String profileId, FileObject profileHome, boolean moduleFiles) {
            List<Object[]> pair = files.get(profileId);
            if (pair == null) {
                pair = new ArrayList<Object[]>();
                files.put(profileId, pair);
            }
            pair.add(new Object[]{profileHome, file, moduleFiles, null, true});
            if (LOG.isLoggable(Level.INFO)) {
                Utils.logOnce(LOG, Level.INFO, this.settingTypeId + " settings " + "should reside in '" + this.settingTypeId + "' subfolder, " + "see #90403 for details. Offending file '" + file.getPath() + "'", null);
            }
        }
    }

    static final class Cache
    implements LookupListener {
        private static Cache INSTANCE = null;
        private final Lookup.Result<StorageDescription> lookupResult = Lookup.getDefault().lookupResult(StorageDescription.class);
        private final Map<String, StorageDescription> cache = new HashMap<String, StorageDescription>();

        public static synchronized Cache getInstance() {
            if (INSTANCE == null) {
                INSTANCE = new Cache();
            }
            return INSTANCE;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public StorageDescription find(String id) {
            Map<String, StorageDescription> map = this.cache;
            synchronized (map) {
                return this.cache.get(id);
            }
        }

        public void resultChanged(LookupEvent ev) {
            this.rebuild();
        }

        private Cache() {
            this.rebuild();
            this.lookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.lookupResult));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void rebuild() {
            Map<String, StorageDescription> map = this.cache;
            synchronized (map) {
                Collection allInstances = this.lookupResult.allInstances();
                HashSet<String> allIds = new HashSet<String>();
                for (StorageDescription sd2 : allInstances) {
                    allIds.add(sd2.getId());
                }
                this.cache.keySet().retainAll(allIds);
                for (StorageDescription sd2 : allInstances) {
                    if (this.cache.containsKey(sd2.getId())) continue;
                    this.cache.put(sd2.getId(), sd2);
                }
            }
        }
    }

    public static interface Locator {
        public void scan(FileObject var1, String var2, String var3, boolean var4, boolean var5, boolean var6, boolean var7, Map<String, List<Object[]>> var8);

        public String getWritableFileName(String var1, String var2, String var3, boolean var4);

        public boolean isUsingProfiles();
    }

}

