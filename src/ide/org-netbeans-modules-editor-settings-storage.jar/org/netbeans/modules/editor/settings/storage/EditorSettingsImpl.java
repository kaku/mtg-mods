/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.editor.settings.storage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.MimeTypesTracker;
import org.netbeans.modules.editor.settings.storage.ProfilesTracker;
import org.netbeans.modules.editor.settings.storage.StorageImpl;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory;
import org.netbeans.modules.editor.settings.storage.api.KeyBindingSettingsFactory;
import org.netbeans.modules.editor.settings.storage.fontscolors.ColoringStorage;
import org.netbeans.modules.editor.settings.storage.fontscolors.FontColorSettingsImpl;
import org.netbeans.modules.editor.settings.storage.keybindings.KeyBindingSettingsImpl;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class EditorSettingsImpl
extends EditorSettings {
    private static final Logger LOG = Logger.getLogger(EditorSettingsImpl.class.getName());
    private final PropertyChangeSupport pcs;
    public static final String PROP_HIGHLIGHT_COLORINGS = "editorFontColors";
    public static final String PROP_ANNOTATION_COLORINGS = "editorAnnotationFontColors";
    public static final String PROP_TOKEN_COLORINGS = "fontColors";
    public static final String DEFAULT_PROFILE = "NetBeans";
    private static final String FATTR_CURRENT_FONT_COLOR_PROFILE = "currentFontColorProfile";
    private static final String FATTR_CURRENT_KEYMAP_PROFILE = "currentKeymap";
    public static final String EDITORS_FOLDER = "Editors";
    private static final String KEYMAPS_FOLDER = "Keymaps";
    public static final String TEXT_BASE_MIME_TYPE = "text/base";
    private static final String[] EMPTY = new String[0];
    private static EditorSettingsImpl instance = null;
    private final Set<String> cacheFontColorProfiles;
    private String currentFontColorProfile;
    private final Map<String, Map<String, AttributeSet>> highlightings;
    private final StorageImpl<String, AttributeSet> highlightingsStorage;
    private final Map<String, Map<String, AttributeSet>> annotations;
    private final StorageImpl<String, AttributeSet> annotationsStorage;
    private String currentKeyMapProfile;

    public static synchronized EditorSettingsImpl getInstance() {
        if (instance == null) {
            instance = new EditorSettingsImpl();
        }
        return instance;
    }

    public void notifyMimeTypesChange(Object old, Object nue) {
        this.pcs.firePropertyChange(new PropertyChangeEvent(this, "mime-types", old, nue));
    }

    @Override
    public Set<String> getAllMimeTypes() {
        return MimeTypesTracker.get(null, "Editors").getMimeTypes();
    }

    @Override
    public Set<String> getMimeTypes() {
        return MimeTypesTracker.get("FontsColors", "Editors").getMimeTypes();
    }

    @Override
    public String getLanguageName(String mimeType) {
        return MimeTypesTracker.get(null, "Editors").getMimeTypeDisplayName(mimeType);
    }

    public void notifyTokenFontColorChange(MimePath mimePath, String profile) {
        this.pcs.firePropertyChange("fontColors", (Object)mimePath, profile);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Set<String> getFontColorProfiles() {
        HashSet<String> result = new HashSet<String>();
        result.addAll(ProfilesTracker.get("FontsColors", "Editors").getProfilesDisplayNames());
        EditorSettingsImpl editorSettingsImpl = this;
        synchronized (editorSettingsImpl) {
            this.cacheFontColorProfiles.removeAll(result);
            result.addAll(this.cacheFontColorProfiles);
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isCustomFontColorProfile(String profile) {
        ProfilesTracker tracker = ProfilesTracker.get("FontsColors", "Editors");
        ProfilesTracker.ProfileDescription pd = tracker.getProfileByDisplayName(profile);
        EditorSettingsImpl editorSettingsImpl = this;
        synchronized (editorSettingsImpl) {
            boolean inCache = this.cacheFontColorProfiles.contains(profile);
            return pd != null && !pd.isRollbackAllowed() || inCache;
        }
    }

    @Override
    public synchronized String getCurrentFontColorProfile() {
        if (this.currentFontColorProfile == null) {
            Object o;
            FileObject fo = FileUtil.getConfigFile((String)"Editors");
            if (fo != null && (o = fo.getAttribute("currentFontColorProfile")) instanceof String) {
                this.currentFontColorProfile = (String)o;
            }
            if (this.currentFontColorProfile == null) {
                this.currentFontColorProfile = "NetBeans";
            }
        }
        if (!this.getFontColorProfiles().contains(this.currentFontColorProfile)) {
            this.currentFontColorProfile = "NetBeans";
        }
        return this.currentFontColorProfile;
    }

    @Override
    public synchronized void setCurrentFontColorProfile(String profile) {
        FileObject fo;
        String oldProfile = this.getCurrentFontColorProfile();
        if (oldProfile.equals(profile)) {
            return;
        }
        this.currentFontColorProfile = profile;
        if (!this.getFontColorProfiles().contains(this.currentFontColorProfile)) {
            this.cacheFontColorProfiles.add(this.currentFontColorProfile);
        }
        if ((fo = FileUtil.getConfigFile((String)"Editors")) != null) {
            try {
                fo.setAttribute("currentFontColorProfile", (Object)profile);
            }
            catch (IOException ex) {
                LOG.log(Level.WARNING, "Can't persist change in current font&colors profile.", ex);
            }
        }
        this.pcs.firePropertyChange("currentFontColorProfile", oldProfile, this.currentFontColorProfile);
    }

    @Override
    public Collection<AttributeSet> getDefaultFontColors(String profile) {
        return this.getFontColorSettings(new String[0]).getAllFontColors(profile);
    }

    @Override
    public Collection<AttributeSet> getDefaultFontColorDefaults(String profile) {
        return this.getFontColorSettings(new String[0]).getAllFontColorDefaults(profile);
    }

    @Override
    public void setDefaultFontColors(String profile, Collection<AttributeSet> fontColors) {
        this.getFontColorSettings(new String[0]).setAllFontColors(profile, fontColors);
    }

    @Override
    public Map<String, AttributeSet> getHighlightings(String profile) {
        Map h;
        boolean specialProfile = profile.startsWith("test");
        profile = FontColorSettingsImpl.get(MimePath.EMPTY).getInternalFontColorProfile(profile);
        if (!this.highlightings.containsKey(profile)) {
            Map profileColorings = null;
            try {
                profileColorings = this.highlightingsStorage.load(MimePath.EMPTY, specialProfile ? "NetBeans" : profile, false);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
            Map<String, AttributeSet> defaultProfileColorings = null;
            if (!specialProfile && !"NetBeans".equals(profile)) {
                try {
                    defaultProfileColorings = this.highlightingsStorage.load(MimePath.EMPTY, "NetBeans", false);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            HashMap<String, AttributeSet> m = new HashMap<String, AttributeSet>();
            if (defaultProfileColorings != null) {
                m.putAll(defaultProfileColorings);
            }
            if (profileColorings != null) {
                m.putAll(profileColorings);
            }
            profileColorings = Collections.unmodifiableMap(m);
            this.highlightings.put(profile, profileColorings);
        }
        return (h = this.highlightings.get(profile)) == null ? Collections.emptyMap() : h;
    }

    @Override
    public Map<String, AttributeSet> getHighlightingDefaults(String profile) {
        profile = FontColorSettingsImpl.get(MimePath.EMPTY).getInternalFontColorProfile(profile);
        try {
            return this.highlightingsStorage.load(MimePath.EMPTY, profile, true);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return Collections.emptyMap();
        }
    }

    @Override
    public void setHighlightings(String profile, Map<String, AttributeSet> fontColors) {
        boolean specialProfile = profile.startsWith("test");
        profile = FontColorSettingsImpl.get(MimePath.EMPTY).getInternalFontColorProfile(profile);
        if (fontColors == null) {
            try {
                this.highlightingsStorage.delete(MimePath.EMPTY, profile, false);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
            this.highlightings.remove(profile);
        } else {
            Map<String, AttributeSet> m = Utils.immutize(fontColors, new Object[0]);
            if (!specialProfile) {
                try {
                    this.highlightingsStorage.save(MimePath.EMPTY, profile, false, m);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            this.highlightings.put(profile, m);
        }
        this.pcs.firePropertyChange("editorFontColors", (Object)MimePath.EMPTY, profile);
    }

    @Override
    public Map<String, AttributeSet> getAnnotations(String profile) {
        Map h;
        boolean specialProfile = profile.startsWith("test");
        profile = FontColorSettingsImpl.get(MimePath.EMPTY).getInternalFontColorProfile(profile);
        if (!this.annotations.containsKey(profile)) {
            Map profileColorings = null;
            try {
                profileColorings = this.annotationsStorage.load(MimePath.EMPTY, specialProfile ? "NetBeans" : profile, false);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
            Map<String, AttributeSet> defaultProfileColorings = null;
            if (!specialProfile && !"NetBeans".equals(profile)) {
                try {
                    defaultProfileColorings = this.annotationsStorage.load(MimePath.EMPTY, "NetBeans", false);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            HashMap<String, AttributeSet> m = new HashMap<String, AttributeSet>();
            if (defaultProfileColorings != null) {
                m.putAll(defaultProfileColorings);
            }
            if (profileColorings != null) {
                m.putAll(profileColorings);
            }
            profileColorings = Collections.unmodifiableMap(m);
            this.annotations.put(profile, profileColorings);
        }
        return (h = this.annotations.get(profile)) == null ? Collections.emptyMap() : h;
    }

    @Override
    public Map<String, AttributeSet> getAnnotationDefaults(String profile) {
        profile = FontColorSettingsImpl.get(MimePath.EMPTY).getInternalFontColorProfile(profile);
        try {
            return this.annotationsStorage.load(MimePath.EMPTY, profile, true);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return Collections.emptyMap();
        }
    }

    @Override
    public void setAnnotations(String profile, Map<String, AttributeSet> fontColors) {
        boolean specialProfile = profile.startsWith("test");
        profile = FontColorSettingsImpl.get(MimePath.EMPTY).getInternalFontColorProfile(profile);
        if (fontColors == null) {
            try {
                this.annotationsStorage.delete(MimePath.EMPTY, profile, false);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
            this.annotations.remove(profile);
        } else {
            Map<String, AttributeSet> m = Utils.immutize(fontColors, new Object[0]);
            if (!specialProfile) {
                try {
                    this.annotationsStorage.save(MimePath.EMPTY, profile, false, m);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            this.annotations.put(profile, m);
        }
        this.pcs.firePropertyChange("editorAnnotationFontColors", (Object)MimePath.EMPTY, profile);
    }

    @Override
    public Set<String> getKeyMapProfiles() {
        return ProfilesTracker.get("Keybindings", "Editors").getProfilesDisplayNames();
    }

    @Override
    public boolean isCustomKeymapProfile(String profile) {
        ProfilesTracker tracker = ProfilesTracker.get("Keybindings", "Editors");
        ProfilesTracker.ProfileDescription pd = tracker.getProfileByDisplayName(profile);
        return pd == null || !pd.isRollbackAllowed();
    }

    @Override
    public String getCurrentKeyMapProfile() {
        if (this.currentKeyMapProfile == null) {
            Object o;
            FileObject fo = FileUtil.getConfigFile((String)"Keymaps");
            if (fo != null && (o = fo.getAttribute("currentKeymap")) instanceof String) {
                this.currentKeyMapProfile = (String)o;
            }
            if (this.currentKeyMapProfile == null) {
                this.currentKeyMapProfile = "NetBeans";
            }
        }
        return this.currentKeyMapProfile;
    }

    @Override
    public void setCurrentKeyMapProfile(String keyMapName) {
        String oldKeyMap = this.getCurrentKeyMapProfile();
        if (oldKeyMap.equals(keyMapName)) {
            return;
        }
        this.currentKeyMapProfile = keyMapName;
        try {
            FileObject fo = FileUtil.getConfigFile((String)"Keymaps");
            if (fo == null) {
                fo = FileUtil.getConfigRoot().createFolder("Keymaps");
            }
            fo.setAttribute("currentKeymap", (Object)keyMapName);
        }
        catch (IOException ex) {
            LOG.log(Level.WARNING, "Can't persist change in current keybindings profile.", ex);
        }
        this.pcs.firePropertyChange("currentKeyMapProfile", oldKeyMap, this.currentKeyMapProfile);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(propertyName, l);
    }

    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(propertyName, l);
    }

    private EditorSettingsImpl() {
        this.pcs = new PropertyChangeSupport(this);
        this.cacheFontColorProfiles = new HashSet<String>();
        this.highlightings = new HashMap<String, Map<String, AttributeSet>>();
        this.highlightingsStorage = new StorageImpl(new ColoringStorage("highlight"), null);
        this.annotations = new HashMap<String, Map<String, AttributeSet>>();
        this.annotationsStorage = new StorageImpl(new ColoringStorage("annotation"), null);
    }

    @Override
    public KeyBindingSettingsFactory getKeyBindingSettings(String[] mimeTypes) {
        mimeTypes = this.filter(mimeTypes);
        return KeyBindingSettingsImpl.get(Utils.mimeTypes2mimePath(mimeTypes));
    }

    @Override
    public FontColorSettingsFactory getFontColorSettings(String[] mimeTypes) {
        mimeTypes = this.filter(mimeTypes);
        return FontColorSettingsImpl.get(Utils.mimeTypes2mimePath(mimeTypes));
    }

    private String[] filter(String[] mimeTypes) {
        if (mimeTypes.length > 0) {
            String[] filtered = mimeTypes;
            if (mimeTypes[0].contains("text/base")) {
                if (mimeTypes.length == 1) {
                    filtered = EMPTY;
                } else {
                    filtered = new String[mimeTypes.length - 1];
                    System.arraycopy(mimeTypes, 1, filtered, 0, mimeTypes.length - 1);
                }
                if (LOG.isLoggable(Level.INFO)) {
                    LOG.log(Level.INFO, "text/base has been deprecated, use MimePath.EMPTY instead.");
                }
            } else if (mimeTypes[0].startsWith("test")) {
                filtered = new String[mimeTypes.length];
                System.arraycopy(mimeTypes, 0, filtered, 0, mimeTypes.length);
                filtered[0] = mimeTypes[0].substring(mimeTypes[0].indexOf(95) + 1);
                LOG.log(Level.INFO, "Don't use 'test' mime type to access settings through the editor/settings/storage API!", new Throwable("Stacktrace"));
            }
            return filtered;
        }
        return mimeTypes;
    }

    private MimePath filter(MimePath mimePath) {
        if (mimePath.size() > 0) {
            MimePath filtered = mimePath;
            String first = mimePath.getMimeType(0);
            if (first.contains("text/base")) {
                if (mimePath.size() == 1) {
                    filtered = MimePath.EMPTY;
                } else {
                    String path = mimePath.getPath().substring(first.length() + 1);
                    filtered = MimePath.parse((String)path);
                }
                if (LOG.isLoggable(Level.INFO)) {
                    LOG.log(Level.INFO, "text/base has been deprecated, use MimePath.EMPTY instead.");
                }
            } else if (first.startsWith("test")) {
                String filteredFirst = first.substring(first.indexOf(95) + 1);
                String path = filteredFirst + mimePath.getPath().substring(first.length() + 1);
                filtered = MimePath.parse((String)path);
                LOG.log(Level.INFO, "Don't use 'test' mime type to access settings through the editor/settings/storage API!", new Throwable("Stacktrace"));
            }
            return filtered;
        }
        return mimePath;
    }
}

