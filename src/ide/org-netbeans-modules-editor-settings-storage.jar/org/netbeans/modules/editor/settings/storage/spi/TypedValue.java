/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.settings.storage.spi;

import org.openide.util.Utilities;

public final class TypedValue {
    private final String value;
    private String javaType;
    private String apiCategory;

    public TypedValue(String value, String javaType) {
        this.value = value;
        this.javaType = javaType;
    }

    public String getApiCategory() {
        return this.apiCategory;
    }

    public void setApiCategory(String apiCategory) {
        this.apiCategory = apiCategory;
    }

    public String getJavaType() {
        return this.javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getValue() {
        return this.value;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        TypedValue other = (TypedValue)obj;
        if (!Utilities.compareObjects((Object)this.value, (Object)other.value)) {
            return false;
        }
        if (!Utilities.compareObjects((Object)this.javaType, (Object)other.javaType)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.value != null ? this.value.hashCode() : 0);
        hash = 37 * hash + (this.javaType != null ? this.javaType.hashCode() : 0);
        return hash;
    }

    public String toString() {
        return super.toString() + "['" + this.value + "', " + this.javaType;
    }
}

