/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.SettingsType;
import org.netbeans.modules.editor.settings.storage.SpiPackageAccessor;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.netbeans.modules.editor.settings.storage.spi.StorageFilter;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class StorageImpl<K, V> {
    private static final Logger LOG = Logger.getLogger(StorageImpl.class.getName());
    private final StorageDescription<K, V> storageDescription;
    private final Callable<Void> dataChangedCallback;
    private final FileObject baseFolder;
    private final StorageImpl<K, V> tracker;
    private final Object lock = new String("StorageImpl.lock");
    private final Map<MimePath, Map<CacheKey, Map<K, V>>> profilesCache = new WeakHashMap<MimePath, Map<CacheKey, Map<K, V>>>();
    private static volatile boolean ignoreFilesystemEvents = false;

    public StorageImpl(StorageDescription<K, V> sd, Callable<Void> callback) {
        this.storageDescription = sd;
        this.dataChangedCallback = callback;
        this.baseFolder = FileUtil.getConfigFile((String)"Editors");
        try {
            this.tracker = new FilesystemTracker(FileUtil.getConfigRoot().getFileSystem());
        }
        catch (FileStateInvalidException ex) {
            throw new IllegalStateException((Throwable)ex);
        }
        Filters.registerCallback(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Map<K, V> load(MimePath mimePath, String profile, boolean defaults) throws IOException {
        assert (mimePath != null);
        if (this.storageDescription.isUsingProfiles()) {
            assert (profile != null);
        } else assert (profile == null);
        Object object = this.lock;
        synchronized (object) {
            Map<K, V> data;
            Map<CacheKey, Map<CacheKey, Map<K, V>>> profilesData = this.profilesCache.get((Object)mimePath);
            CacheKey cacheKey = StorageImpl.cacheKey(profile, defaults);
            if (profilesData == null) {
                data = null;
                profilesData = new HashMap<CacheKey, Map<K, V>>();
                this.profilesCache.put(mimePath, profilesData);
            } else {
                data = profilesData.get(cacheKey);
            }
            if (data == null) {
                data = this._load(mimePath, profile, defaults);
                this.filterAfterLoad(data, mimePath, profile, defaults);
                data = Collections.unmodifiableMap(data);
                profilesData.put(cacheKey, data);
            }
            return data;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void save(MimePath mimePath, String profile, boolean defaults, Map<K, V> data) throws IOException {
        assert (mimePath != null);
        if (this.storageDescription.isUsingProfiles()) {
            assert (profile != null);
        } else assert (profile == null);
        Object object = this.lock;
        synchronized (object) {
            CacheKey cacheKey = null;
            Map<CacheKey, Map<K, V>> profilesData = this.profilesCache.get((Object)mimePath);
            if (profilesData == null) {
                profilesData = new HashMap<CacheKey, Map<K, V>>();
                this.profilesCache.put(mimePath, profilesData);
            } else {
                cacheKey = StorageImpl.cacheKey(profile, defaults);
                Map<K, V> cacheData = profilesData.get(cacheKey);
                if (cacheData != null && !Utils.quickDiff(cacheData, data)) {
                    return;
                }
            }
            HashMap<K, V> dataForSave = new HashMap<K, V>(data);
            this.filterBeforeSave(dataForSave, mimePath, profile, defaults);
            this._save(mimePath, profile, defaults, dataForSave);
            this.profilesCache.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void delete(MimePath mimePath, String profile, boolean defaults) throws IOException {
        assert (mimePath != null);
        if (this.storageDescription.isUsingProfiles()) {
            assert (profile != null);
        } else assert (profile == null);
        Object object = this.lock;
        synchronized (object) {
            Map<CacheKey, Map<K, V>> profilesData = this.profilesCache.get((Object)mimePath);
            if (profilesData != null) {
                profilesData.remove(StorageImpl.cacheKey(profile, defaults));
            }
            this._delete(mimePath, profile, defaults);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void refresh() {
        Object object = this.lock;
        synchronized (object) {
            this.profilesCache.clear();
        }
        if (this.dataChangedCallback != null) {
            try {
                this.dataChangedCallback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }

    static void ignoreFilesystemEvents(boolean ignore) {
        ignoreFilesystemEvents = ignore;
    }

    private List<Object[]> scan(MimePath mimePath, String profile, boolean scanModules, boolean scanUsers) {
        HashMap<String, List<Object[]>> files = new HashMap<String, List<Object[]>>();
        SettingsType.getLocator(this.storageDescription).scan(this.baseFolder, mimePath.getPath(), profile, true, scanModules, scanUsers, mimePath.size() > 1, files);
        assert (files.size() <= 1);
        return files.get(profile);
    }

    private Map<K, V> _load(MimePath mimePath, String profile, boolean defaults) throws IOException {
        if (this.storageDescription instanceof Operations) {
            Operations operations = (Operations)((Object)this.storageDescription);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Forwarding loading of '" + this.storageDescription.getId() + "' to: " + operations);
            }
            return operations.load(mimePath, profile, defaults);
        }
        List<Object[]> profileInfos = this.scan(mimePath, profile, true, !defaults);
        HashMap<K, V> map = new HashMap<K, V>();
        if (profileInfos != null) {
            for (Object[] info : profileInfos) {
                assert (info.length == 5);
                FileObject profileHome = (FileObject)info[0];
                FileObject settingFile = (FileObject)info[1];
                boolean modulesFile = (Boolean)info[2];
                FileObject linkTarget = (FileObject)info[3];
                boolean legacyFile = (Boolean)info[4];
                if (linkTarget != null) {
                    MimePath linkedMimePath = MimePath.parse((String)linkTarget.getPath().substring(this.baseFolder.getPath().length() + 1));
                    assert (linkedMimePath != mimePath);
                    if (linkedMimePath.size() == 1) {
                        Map<K, V> linkedMap = this.load(linkedMimePath, profile, defaults);
                        map.putAll(linkedMap);
                        LOG.fine("Adding linked '" + this.storageDescription.getId() + "' from: '" + linkedMimePath.getPath() + "'");
                        continue;
                    }
                    if (!LOG.isLoggable(Level.WARNING)) continue;
                    LOG.warning("Linking to other than top level mime types is prohibited. Ignoring editor settings link from '" + mimePath.getPath() + "' to '" + linkedMimePath.getPath() + "'");
                    continue;
                }
                StorageReader<K, V> reader = this.storageDescription.createReader(settingFile, mimePath.getPath());
                Utils.load(settingFile, reader, !legacyFile);
                Map<K, V> added = reader.getAdded();
                Set<K> removed = reader.getRemoved();
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Loading '" + this.storageDescription.getId() + "' from: '" + settingFile.getPath() + "'");
                }
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest("--- Removing '" + this.storageDescription.getId() + "': " + removed);
                }
                for (K key2 : removed) {
                    map.remove(key2);
                }
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest("--- Adding '" + this.storageDescription.getId() + "': " + added);
                }
                for (K key2 : added.keySet()) {
                    V value = added.get(key2);
                    Object origValue = map.put(key2, value);
                    if (!LOG.isLoggable(Level.FINEST) || origValue == null || origValue.equals(value)) continue;
                    LOG.finest("--- Replacing old entry for '" + key2 + "', orig value = '" + origValue + "', new value = '" + value + "'");
                }
                if (!LOG.isLoggable(Level.FINEST)) continue;
                LOG.finest("-------------------------------------");
            }
        }
        return map;
    }

    private boolean _save(MimePath mimePath, String profile, boolean defaults, Map<K, V> data) throws IOException {
        Map<K, V> defaultData = this.load(mimePath, profile, true);
        if (this.storageDescription instanceof Operations) {
            Operations operations = (Operations)((Object)this.storageDescription);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Forwarding saving of '" + this.storageDescription.getId() + "' to: " + operations);
            }
            return operations.save(mimePath, profile, defaults, data, defaultData);
        }
        final HashMap added = new HashMap();
        final HashMap removed = new HashMap();
        Utils.diff(defaultData, data, added, removed);
        final String mimePathString = mimePath.getPath();
        final String settingFileName = SettingsType.getLocator(this.storageDescription).getWritableFileName(mimePathString, profile, null, defaults);
        this.tracker.runAtomicAction(new FileSystem.AtomicAction(){

            public void run() throws IOException {
                if (added.size() > 0 || removed.size() > 0) {
                    FileObject f = FileUtil.createData((FileObject)StorageImpl.this.baseFolder, (String)settingFileName);
                    StorageWriter writer = StorageImpl.this.storageDescription.createWriter(f, mimePathString);
                    writer.setAdded(added);
                    writer.setRemoved(removed.keySet());
                    Utils.save(f, writer);
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Saving '" + StorageImpl.this.storageDescription.getId() + "' to: '" + f.getPath() + "'");
                    }
                } else {
                    FileObject f = StorageImpl.this.baseFolder.getFileObject(settingFileName);
                    if (f != null) {
                        f.delete();
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.fine("Saving '" + StorageImpl.this.storageDescription.getId() + "', no changes from defaults therefore deleting: '" + f.getPath() + "'");
                        }
                    }
                }
            }
        });
        return false;
    }

    private void _delete(MimePath mimePath, String profile, boolean defaults) throws IOException {
        if (this.storageDescription instanceof Operations) {
            Operations operations = (Operations)((Object)this.storageDescription);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Forwarding deletion of '" + this.storageDescription.getId() + "' to: " + operations);
            }
            operations.delete(mimePath, profile, defaults);
        } else {
            final List<Object[]> profileInfos = this.scan(mimePath, profile, defaults, !defaults);
            if (profileInfos != null) {
                this.tracker.runAtomicAction(new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        for (Object[] info : profileInfos) {
                            assert (info.length == 5);
                            FileObject profileHome = (FileObject)info[0];
                            FileObject settingFile = (FileObject)info[1];
                            boolean modulesFile = (Boolean)info[2];
                            settingFile.delete();
                            if (!LOG.isLoggable(Level.FINE)) continue;
                            LOG.fine("Deleting '" + StorageImpl.this.storageDescription.getId() + "' file: '" + settingFile.getPath() + "'");
                        }
                    }
                });
            }
        }
    }

    private void filterAfterLoad(Map<K, V> data, MimePath mimePath, String profile, boolean defaults) throws IOException {
        List<StorageFilter> filters = Filters.getFilters(this.storageDescription.getId());
        for (int i = 0; i < filters.size(); ++i) {
            StorageFilter filter = filters.get(i);
            filter.afterLoad(data, mimePath, profile, defaults);
        }
    }

    private void filterBeforeSave(Map<K, V> data, MimePath mimePath, String profile, boolean defaults) throws IOException {
        List<StorageFilter> filters = Filters.getFilters(this.storageDescription.getId());
        for (int i = filters.size() - 1; i >= 0; --i) {
            StorageFilter filter = filters.get(i);
            filter.beforeSave(data, mimePath, profile, defaults);
        }
    }

    private static CacheKey cacheKey(String profile, boolean defaults) {
        return new CacheKey(profile, defaults);
    }

    private final class FilesystemTracker
    implements FileChangeListener,
    Runnable {
        private final FileSystem fileSystem;
        private final RequestProcessor.Task refreshCacheTask;
        private final List<Reference<FileEvent>> recentEvents;
        private final Pattern controlledFilesPattern;
        private volatile FileSystem.AtomicAction atomicAction;

        public void fileAttributeChanged(FileAttributeEvent fe) {
        }

        public void fileChanged(FileEvent fe) {
            if (!this.filterEvents(fe)) {
                boolean processed = this.processFile(fe.getFile());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("fileChanged (" + (processed ? "processed" : "ignored") + "): " + fe.getFile().getPath());
                }
            }
        }

        public void fileDataCreated(FileEvent fe) {
            if (!this.filterEvents(fe)) {
                boolean processed = this.processFile(fe.getFile());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("fileDataCreated (" + (processed ? "processed" : "ignored") + "): " + fe.getFile().getPath());
                }
            }
        }

        public void fileDeleted(FileEvent fe) {
            if (!this.filterEvents(fe)) {
                boolean processed = this.processFile(fe.getFile());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("fileDeleted (" + (processed ? "processed" : "ignored") + "): " + fe.getFile().getPath());
                }
            }
        }

        public void fileFolderCreated(FileEvent fe) {
            if (!this.filterEvents(fe)) {
                boolean processed = this.processKids(fe.getFile());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("fileFolderCreated (" + (processed ? "processed" : "ignored") + "): " + fe.getFile().getPath());
                }
            }
        }

        public void fileRenamed(FileRenameEvent fe) {
            if (!this.filterEvents((FileEvent)fe)) {
                boolean processed = this.processKids(fe.getFile().getParent());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("fileRenamed (" + (processed ? "processed" : "ignored") + "): " + fe.getFile().getPath());
                }
            }
        }

        @Override
        public void run() {
            StorageImpl.this.refresh();
        }

        public FilesystemTracker(FileSystem fileSystem) {
            this.refreshCacheTask = new RequestProcessor("Editor-Setting-Files-Tracker-" + StorageImpl.this.storageDescription.getId()).create((Runnable)this);
            this.recentEvents = new LinkedList<Reference<FileEvent>>();
            this.controlledFilesPattern = Pattern.compile("^Editors/(.*)" + StorageImpl.this.storageDescription.getId() + "(.*)");
            this.fileSystem = fileSystem;
            this.fileSystem.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)this.fileSystem));
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(this + " sensitive to " + this.controlledFilesPattern.pattern() + " paths and " + StorageImpl.this.storageDescription.getMimeType() + "setting files");
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void runAtomicAction(FileSystem.AtomicAction task) throws IOException {
            assert (this.atomicAction == null);
            this.atomicAction = task;
            try {
                this.fileSystem.runAtomicAction(task);
            }
            finally {
                this.atomicAction = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean filterEvents(FileEvent event) {
            if (!this.controlledFilesPattern.matcher(event.getFile().getPath()).matches()) {
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer(event.getFile().getPath() + " does not match: " + this.controlledFilesPattern.pattern());
                }
                return true;
            }
            FileSystem.AtomicAction aa = this.atomicAction;
            if (aa != null && event.firedFrom(aa)) {
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("Filesystem event for " + event.getFile().getPath() + " caused by saving settings");
                }
                return true;
            }
            List<Reference<FileEvent>> list = this.recentEvents;
            synchronized (list) {
                Iterator<Reference<FileEvent>> i = this.recentEvents.iterator();
                while (i.hasNext()) {
                    Reference<FileEvent> ref = i.next();
                    FileEvent e = ref.get();
                    if (e == null) {
                        i.remove();
                        continue;
                    }
                    if (e == event) {
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.fine("Filtering out duplicate filesystem event (1): original=[" + this.printEvent(e) + "]" + ", duplicate=[" + this.printEvent(event) + "]");
                        }
                        return true;
                    }
                    if (e.getTime() != event.getTime() || !e.getFile().getPath().equals(event.getFile().getPath())) continue;
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Filtering out duplicate filesystem event (2): original=[" + this.printEvent(e) + "]" + ", duplicate=[" + this.printEvent(event) + "]");
                    }
                    return true;
                }
                if (this.recentEvents.size() > 100) {
                    this.recentEvents.remove(this.recentEvents.size() - 1);
                }
                this.recentEvents.add(0, new WeakReference<FileEvent>(event));
                return false;
            }
        }

        private boolean processFile(FileObject f) {
            if (!ignoreFilesystemEvents && f.isData() && (f.getMIMEType().equals(StorageImpl.this.storageDescription.getMimeType()) || f.getNameExt().equals(StorageImpl.this.storageDescription.getLegacyFileName()))) {
                this.refreshCacheTask.schedule(71);
                return true;
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Not a settings file: " + f.getPath() + ", mimeType=" + f.getMIMEType());
            }
            return false;
        }

        private boolean processKids(FileObject f) {
            assert (f.isFolder());
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(f.getPath() + " has " + f.getChildren().length + " children");
            }
            for (FileObject ff : f.getChildren()) {
                if (ff.isData()) {
                    if (!this.processFile(ff)) continue;
                    return true;
                }
                if (this.controlledFilesPattern.matcher(ff.getPath()).matches()) {
                    if (!this.processKids(ff)) continue;
                    return true;
                }
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine(ff.getPath() + " does not match: " + this.controlledFilesPattern.pattern());
            }
            return false;
        }

        private String printEvent(FileEvent event) {
            return event.getClass().getSimpleName() + "@" + Integer.toHexString(System.identityHashCode((Object)event)) + ", ts=" + event.getTime() + ", path=" + event.getFile().getPath();
        }
    }

    private static final class Filters
    implements Callable<Void> {
        private static final Map<String, Filters> filters = new HashMap<String, Filters>();
        private static Lookup.Result<StorageFilter> allFilters = null;
        private static final LookupListener allFiltersTracker = new LookupListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void resultChanged(LookupEvent ev) {
                Set changedIds;
                Map map = filters;
                synchronized (map) {
                    if (!rebuilding) {
                        rebuilding = true;
                        try {
                            changedIds = Filters.rebuild();
                        }
                        finally {
                            rebuilding = false;
                        }
                    }
                    return;
                }
                Filters.resetCaches(changedIds);
            }
        };
        private static final Map<String, Reference<StorageImpl>> callbacks = new HashMap<String, Reference<StorageImpl>>();
        private static boolean rebuilding = false;
        private final String storageDescriptionId;
        private final List<StorageFilter> filtersForId = new ArrayList<StorageFilter>();

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public static List<StorageFilter> getFilters(String storageDescriptionId) {
            Map<String, Filters> map = filters;
            synchronized (map) {
                if (allFilters == null) {
                    allFilters = Lookup.getDefault().lookupResult(StorageFilter.class);
                    allFilters.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)allFiltersTracker, allFilters));
                    Filters.rebuild();
                }
                // MONITOREXIT [0, 1] lbl8 : MonitorExitStatement: MONITOREXIT : var1_1
                Filters filtersForId = filters.get(storageDescriptionId);
                return filtersForId == null ? Collections.emptyList() : filtersForId.filtersForId;
            }
        }

        public static void registerCallback(StorageImpl storageImpl) {
            callbacks.put(storageImpl.storageDescription.getId(), new WeakReference<StorageImpl>(storageImpl));
        }

        @Override
        public Void call() {
            Filters.resetCaches(Collections.singleton(this.storageDescriptionId));
            return null;
        }

        private static Set<String> rebuild() {
            filters.clear();
            Collection all = allFilters.allInstances();
            for (StorageFilter f : all) {
                String id = SpiPackageAccessor.get().storageFilterGetStorageDescriptionId(f);
                Filters filterForId = filters.get(id);
                if (filterForId == null) {
                    filterForId = new Filters(id);
                    filters.put(id, filterForId);
                }
                SpiPackageAccessor.get().storageFilterInitialize(f, filterForId);
                filterForId.filtersForId.add(f);
            }
            HashSet<String> changedIds = new HashSet<String>(filters.keySet());
            return changedIds;
        }

        private static void resetCaches(Set<String> storageDescriptionIds) {
            for (String id : storageDescriptionIds) {
                Reference<StorageImpl> ref = callbacks.get(id);
                StorageImpl storageImpl = ref == null ? null : ref.get();
                if (storageImpl == null) continue;
                storageImpl.refresh();
            }
        }

        private Filters(String storageDescriptionId) {
            this.storageDescriptionId = storageDescriptionId;
        }

    }

    private static final class CacheKey {
        private final String profile;
        private final boolean defaults;

        public CacheKey(String profile, boolean defaults) {
            this.profile = profile;
            this.defaults = defaults;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            CacheKey other = (CacheKey)obj;
            if (this.profile == null && other.profile != null || this.profile != null && other.profile == null || this.profile != null && !this.profile.equals(other.profile)) {
                return false;
            }
            if (this.defaults != other.defaults) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = this.profile != null ? this.profile.hashCode() : 7;
            hash = 37 * hash + Boolean.valueOf(this.defaults).hashCode();
            return hash;
        }
    }

    public static interface Operations<K, V> {
        public Map<K, V> load(MimePath var1, String var2, boolean var3) throws IOException;

        public boolean save(MimePath var1, String var2, boolean var3, Map<K, V> var4, Map<K, V> var5) throws IOException;

        public void delete(MimePath var1, String var2, boolean var3) throws IOException;
    }

}

