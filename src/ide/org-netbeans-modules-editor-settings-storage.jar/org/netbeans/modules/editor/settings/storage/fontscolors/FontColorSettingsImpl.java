/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.editor.settings.storage.fontscolors;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.EditorSettingsImpl;
import org.netbeans.modules.editor.settings.storage.ProfilesTracker;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage;
import org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory;

public final class FontColorSettingsImpl
extends FontColorSettingsFactory {
    private static final Logger LOG = Logger.getLogger(FontColorSettingsImpl.class.getName());
    private static final Map<MimePath, WeakReference<FontColorSettingsImpl>> INSTANCES = new WeakHashMap<MimePath, WeakReference<FontColorSettingsImpl>>();
    private final MimePath mimePath;
    private final Map<String, Map<String, AttributeSet>> colorings = new HashMap<String, Map<String, AttributeSet>>();

    public static synchronized FontColorSettingsImpl get(MimePath mimePath) {
        FontColorSettingsImpl result;
        WeakReference<FontColorSettingsImpl> reference = INSTANCES.get((Object)mimePath);
        FontColorSettingsImpl fontColorSettingsImpl = result = reference == null ? null : reference.get();
        if (result == null) {
            result = new FontColorSettingsImpl(mimePath);
            INSTANCES.put(mimePath, new WeakReference<FontColorSettingsImpl>(result));
        }
        return result;
    }

    private FontColorSettingsImpl(MimePath mimePath) {
        this.mimePath = mimePath;
    }

    public MimePath getMimePath() {
        return this.mimePath;
    }

    public String getInternalFontColorProfile(String profile) {
        ProfilesTracker tracker = ProfilesTracker.get("FontsColors", "Editors");
        ProfilesTracker.ProfileDescription pd = tracker.getProfileByDisplayName(profile);
        return pd == null ? profile : pd.getId();
    }

    @Override
    public Collection<AttributeSet> getAllFontColors(String profile) {
        profile = this.getInternalFontColorProfile(profile);
        Map<String, AttributeSet> m = this.getColorings(profile);
        return Collections.unmodifiableCollection(m.values());
    }

    @Override
    public Collection<AttributeSet> getAllFontColorDefaults(String profile) {
        profile = this.getInternalFontColorProfile(profile);
        Map profileColorings = this.getDefaultColorings(profile);
        Map<String, AttributeSet> defaultProfileColorings = null;
        if (!"NetBeans".equals(profile)) {
            defaultProfileColorings = this.getDefaultColorings("NetBeans");
        }
        HashMap<String, AttributeSet> m = new HashMap<String, AttributeSet>();
        if (defaultProfileColorings != null) {
            m.putAll(defaultProfileColorings);
        }
        if (profileColorings != null) {
            m.putAll(profileColorings);
        }
        profileColorings = Collections.unmodifiableMap(m);
        return profileColorings.values();
    }

    @Override
    public void setAllFontColors(String profile, Collection<AttributeSet> fontColors) {
        EditorSettingsStorage<String, AttributeSet> ess = EditorSettingsStorage.get("FontsColors");
        boolean specialProfile = profile.startsWith("test");
        profile = this.getInternalFontColorProfile(profile);
        if (fontColors == null) {
            try {
                ess.delete(this.mimePath, profile, false);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
            this.colorings.remove(profile);
        } else {
            Map<String, AttributeSet> map = Utils.immutize(fontColors);
            if (!specialProfile) {
                try {
                    ess.save(this.mimePath, profile, false, map);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            this.colorings.put(profile, map);
        }
        EditorSettingsImpl.getInstance().notifyTokenFontColorChange(this.mimePath, profile);
    }

    @Override
    public void setAllFontColorsDefaults(String profile, Collection<AttributeSet> fontColors) {
        EditorSettingsStorage<String, AttributeSet> ess = EditorSettingsStorage.get("FontsColors");
        boolean specialProfile = profile.startsWith("test");
        profile = this.getInternalFontColorProfile(profile);
        try {
            if (fontColors == null) {
                ess.delete(this.mimePath, profile, true);
            } else {
                ess.save(this.mimePath, profile, true, Utils.immutize(fontColors));
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
        }
        EditorSettingsImpl.getInstance().notifyTokenFontColorChange(this.mimePath, profile);
    }

    Map<String, AttributeSet> getColorings(String profile) {
        Map c;
        if (!this.colorings.containsKey(profile)) {
            boolean specialProfile = profile.startsWith("test");
            EditorSettingsStorage ess = EditorSettingsStorage.get("FontsColors");
            Map profileColorings = null;
            try {
                profileColorings = ess.load(this.mimePath, specialProfile ? "NetBeans" : profile, false);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
            Map defaultProfileColorings = null;
            if (!specialProfile && !"NetBeans".equals(profile)) {
                try {
                    defaultProfileColorings = ess.load(this.mimePath, "NetBeans", false);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                }
            }
            HashMap m = new HashMap();
            if (defaultProfileColorings != null) {
                m.putAll(defaultProfileColorings);
            }
            if (profileColorings != null) {
                m.putAll(profileColorings);
            }
            profileColorings = Collections.unmodifiableMap(m);
            this.colorings.put(profile, profileColorings);
        }
        return (c = this.colorings.get(profile)) == null ? Collections.emptyMap() : c;
    }

    Map<String, AttributeSet> getDefaultColorings(String profile) {
        EditorSettingsStorage ess = EditorSettingsStorage.get("FontsColors");
        try {
            return ess.load(this.mimePath, profile, true);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return Collections.emptyMap();
        }
    }
}

