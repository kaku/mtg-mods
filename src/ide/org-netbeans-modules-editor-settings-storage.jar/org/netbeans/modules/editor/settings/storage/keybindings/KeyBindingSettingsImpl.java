/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.settings.storage.keybindings;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.KeyStroke;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.modules.editor.settings.storage.EditorSettingsImpl;
import org.netbeans.modules.editor.settings.storage.ProfilesTracker;
import org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage;
import org.netbeans.modules.editor.settings.storage.api.KeyBindingSettingsFactory;
import org.openide.util.Utilities;

public final class KeyBindingSettingsImpl
extends KeyBindingSettingsFactory {
    private static final Logger LOG = Logger.getLogger(KeyBindingSettingsImpl.class.getName());
    private static final Map<MimePath, WeakReference<KeyBindingSettingsImpl>> INSTANCES = new WeakHashMap<MimePath, WeakReference<KeyBindingSettingsImpl>>();
    private MimePath mimePath;
    private PropertyChangeSupport pcs;
    private KeyBindingSettingsImpl baseKBS;
    private Listener listener;
    private String logActionName = null;
    private boolean init = false;

    public static synchronized KeyBindingSettingsImpl get(MimePath mimePath) {
        KeyBindingSettingsImpl result;
        WeakReference<KeyBindingSettingsImpl> reference = INSTANCES.get((Object)mimePath);
        KeyBindingSettingsImpl keyBindingSettingsImpl = result = reference == null ? null : reference.get();
        if (result == null) {
            result = new KeyBindingSettingsImpl(mimePath);
            INSTANCES.put(mimePath, new WeakReference<KeyBindingSettingsImpl>(result));
        }
        return result;
    }

    private KeyBindingSettingsImpl(MimePath mimePath) {
        this.mimePath = mimePath;
        this.pcs = new PropertyChangeSupport(this);
        String myClassName = KeyBindingSettingsImpl.class.getName();
        String value = System.getProperty(myClassName);
        if (value != null) {
            if (!value.equals("true")) {
                this.logActionName = System.getProperty(myClassName);
            }
        } else if (mimePath.size() == 1) {
            this.logActionName = System.getProperty(myClassName + '.' + mimePath.getMimeType(0));
        }
    }

    private void init() {
        if (this.init) {
            return;
        }
        this.init = true;
        if (this.mimePath.size() > 0) {
            this.baseKBS = KeyBindingSettingsImpl.get(MimePath.EMPTY);
        }
        this.listener = new Listener(this, this.baseKBS);
    }

    private String getInternalKeymapProfile(String profile) {
        ProfilesTracker tracker = ProfilesTracker.get("Keybindings", "Editors");
        ProfilesTracker.ProfileDescription pd = tracker.getProfileByDisplayName(profile);
        return pd == null ? profile : pd.getId();
    }

    @Override
    public List<MultiKeyBinding> getKeyBindings() {
        return this.getKeyBindings(EditorSettingsImpl.getInstance().getCurrentKeyMapProfile());
    }

    @Override
    public List<MultiKeyBinding> getKeyBindings(String profile) {
        profile = this.getInternalKeymapProfile(profile);
        return Collections.unmodifiableList(new ArrayList<MultiKeyBinding>(this.getShortcuts(profile, false).values()));
    }

    private Map<Collection<KeyStroke>, MultiKeyBinding> getShortcuts(String profile, boolean defaults) {
        EditorSettingsStorage ess = EditorSettingsStorage.get("Keybindings");
        try {
            return ess.load(this.mimePath, profile, defaults);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return Collections.emptyMap();
        }
    }

    @Override
    public List<MultiKeyBinding> getKeyBindingDefaults(String profile) {
        profile = this.getInternalKeymapProfile(profile);
        return Collections.unmodifiableList(new ArrayList<MultiKeyBinding>(this.getShortcuts(profile, true).values()));
    }

    @Override
    public void setKeyBindings(String profile, List<MultiKeyBinding> keyBindings) {
        this.init();
        profile = this.getInternalKeymapProfile(profile);
        EditorSettingsStorage ess = EditorSettingsStorage.get("Keybindings");
        try {
            if (keyBindings == null) {
                ess.delete(this.mimePath, profile, false);
            } else {
                HashMap<List, MultiKeyBinding> shortcuts = new HashMap<List, MultiKeyBinding>();
                for (MultiKeyBinding mkb : keyBindings) {
                    shortcuts.put(mkb.getKeyStrokeList(), mkb);
                }
                this.listener.removeListeners();
                ess.save(this.mimePath, profile, false, shortcuts);
                this.listener.addListeners();
                this.pcs.firePropertyChange(null, null, null);
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private void log(String text, Collection keymap) {
        if (!LOG.isLoggable(Level.FINE)) {
            return;
        }
        if (text.length() != 0) {
            if (this.mimePath.size() == 1) {
                text = text + " " + this.mimePath.getMimeType(0);
            }
            text = text + " " + EditorSettingsImpl.getInstance().getCurrentKeyMapProfile();
        }
        if (keymap == null) {
            LOG.fine(text + " : null");
            return;
        }
        LOG.fine(text);
        for (Object mkb : keymap) {
            if (this.logActionName == null || !(mkb instanceof MultiKeyBinding)) {
                LOG.fine("  " + mkb);
                continue;
            }
            if (!(mkb instanceof MultiKeyBinding) || !this.logActionName.equals(((MultiKeyBinding)mkb).getActionName())) continue;
            LOG.fine("  " + mkb);
        }
    }

    public Object createInstanceForLookup() {
        this.init();
        String profile = this.getInternalKeymapProfile(EditorSettingsImpl.getInstance().getCurrentKeyMapProfile());
        HashMap<Collection<KeyStroke>, MultiKeyBinding> allShortcuts = new HashMap<Collection<KeyStroke>, MultiKeyBinding>();
        if (this.baseKBS != null) {
            Map<Collection<KeyStroke>, MultiKeyBinding> baseShortcuts = this.baseKBS.getShortcuts(profile, false);
            allShortcuts.putAll(baseShortcuts);
        }
        Map<Collection<KeyStroke>, MultiKeyBinding> localShortcuts = this.getShortcuts(profile, false);
        allShortcuts.putAll(localShortcuts);
        ArrayList<MultiKeyBinding> result = new ArrayList<MultiKeyBinding>(allShortcuts.values());
        return new Immutable(result);
    }

    static final class Immutable
    extends KeyBindingSettings {
        private List<MultiKeyBinding> keyBindings;

        public Immutable(List<MultiKeyBinding> keyBindings) {
            this.keyBindings = keyBindings;
        }

        public List<MultiKeyBinding> getKeyBindings() {
            return Collections.unmodifiableList(this.keyBindings);
        }
    }

    private static final class Listener
    extends WeakReference<KeyBindingSettingsImpl>
    implements PropertyChangeListener,
    Runnable {
        private final KeyBindingSettingsFactory baseKBS;
        private final EditorSettingsStorage<Collection<KeyStroke>, MultiKeyBinding> storage;

        public Listener(KeyBindingSettingsImpl kb, KeyBindingSettingsFactory baseKBS) {
            super(kb, Utilities.activeReferenceQueue());
            this.baseKBS = baseKBS;
            this.storage = EditorSettingsStorage.get("Keybindings");
            this.addListeners();
        }

        private KeyBindingSettingsImpl getSettings() {
            KeyBindingSettingsImpl r = (KeyBindingSettingsImpl)this.get();
            if (r != null) {
                return r;
            }
            this.removeListeners();
            return null;
        }

        private void addListeners() {
            EditorSettingsImpl.getInstance().addPropertyChangeListener("currentKeyMapProfile", this);
            this.storage.addPropertyChangeListener(this);
            if (this.baseKBS != null) {
                this.baseKBS.addPropertyChangeListener(this);
            }
        }

        private void removeListeners() {
            if (this.baseKBS != null) {
                this.baseKBS.removePropertyChangeListener(this);
            }
            this.storage.removePropertyChangeListener(this);
            EditorSettingsImpl.getInstance().removePropertyChangeListener("currentKeyMapProfile", this);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            KeyBindingSettingsImpl r = this.getSettings();
            if (r == null) {
                return;
            }
            r.log("refresh2", Collections.EMPTY_SET);
            r.pcs.firePropertyChange(null, null, null);
        }

        @Override
        public void run() {
            this.removeListeners();
        }
    }

}

