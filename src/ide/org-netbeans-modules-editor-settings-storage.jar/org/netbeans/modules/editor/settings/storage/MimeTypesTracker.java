/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.EditorSettingsImpl;
import org.netbeans.modules.editor.settings.storage.SettingsType;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class MimeTypesTracker {
    private static final Logger LOG = Logger.getLogger(MimeTypesTracker.class.getName());
    public static final String PROP_MIME_TYPES = "mime-types";
    private static final Map<String, Map<StorageDescription, MimeTypesTracker>> settingMimeTypes = new HashMap<String, Map<StorageDescription, MimeTypesTracker>>();
    private final String LOCK = new String("MimeTypesTracker.LOCK");
    private final String basePath;
    private final String[] basePathElements;
    private final SettingsType.Locator locator;
    private FileObject folder;
    private boolean isBaseFolder;
    private Map<String, String> mimeTypes = Collections.emptyMap();
    private final PropertyChangeSupport pcs;
    private final FileChangeListener listener;
    static boolean synchronous = false;
    static final RequestProcessor RP = new RequestProcessor(MimeTypesTracker.class.getName());
    private final RequestProcessor.Task task;
    private final Set<FileObject> trackedFolders;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static MimeTypesTracker get(String settingsTypeId, String basePath) {
        assert (basePath != null);
        StorageDescription sd = null;
        if (settingsTypeId != null) {
            sd = SettingsType.find(settingsTypeId);
            assert (sd != null);
        }
        Map<String, Map<StorageDescription, MimeTypesTracker>> map = settingMimeTypes;
        synchronized (map) {
            MimeTypesTracker tracker;
            Map<StorageDescription, MimeTypesTracker> map2 = settingMimeTypes.get(basePath);
            if (map2 == null) {
                map2 = new WeakHashMap<StorageDescription, MimeTypesTracker>();
                settingMimeTypes.put(basePath, map2);
            }
            if ((tracker = map2.get(sd)) == null) {
                tracker = new MimeTypesTracker(sd == null ? null : SettingsType.getLocator(sd), basePath);
                map2.put(sd, tracker);
            }
            return tracker;
        }
    }

    MimeTypesTracker(SettingsType.Locator locator, String basePath) {
        this.pcs = new PropertyChangeSupport(this);
        this.task = RP.create(new Runnable(){

            @Override
            public void run() {
                MimeTypesTracker.this.rebuild();
            }
        });
        this.trackedFolders = new HashSet<FileObject>();
        this.locator = locator;
        this.basePath = basePath;
        this.basePathElements = basePath.split("/");
        this.rebuild();
        this.listener = new Listener();
        FileSystem sfs = null;
        try {
            sfs = FileUtil.getConfigRoot().getFileSystem();
        }
        catch (FileStateInvalidException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        sfs.addFileChangeListener((FileChangeListener)WeakListeners.create(FileChangeListener.class, (EventListener)this.listener, (Object)sfs));
    }

    public String getBasePath() {
        return this.basePath;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<String> getMimeTypes() {
        String string = this.LOCK;
        synchronized (string) {
            return this.mimeTypes.keySet();
        }
    }

    public String getMimeTypeDisplayName(String mimeType) {
        String displayName = this.mimeTypes.get(mimeType);
        return displayName == null ? mimeType : displayName;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void rebuild() {
        PropertyChangeEvent event = null;
        String string = this.LOCK;
        synchronized (string) {
            Map<String, String> newMimeTypes;
            Object[] ret = MimeTypesTracker.findTarget(this.basePathElements);
            FileObject f = (FileObject)ret[0];
            boolean isBase = (Boolean)ret[1];
            if (f != this.folder) {
                this.folder = f;
                this.isBaseFolder = isBase;
                LOG.finest("folder = '" + this.folder.getPath() + "'");
                LOG.finest("isBaseFolder = '" + this.isBaseFolder + "'");
            }
            if (this.isBaseFolder) {
                newMimeTypes = new HashMap();
                FileObject[] types = this.folder.getChildren();
                for (int i = 0; i < types.length; ++i) {
                    if (!MimeTypesTracker.isValidType(types[i])) continue;
                    this.trackedFolders.add(types[i]);
                    FileObject[] subTypes = types[i].getChildren();
                    for (int j = 0; j < subTypes.length; ++j) {
                        boolean add;
                        if (!MimeTypesTracker.isValidSubtype(subTypes[j])) continue;
                        String mimeType = types[i].getNameExt() + "/" + subTypes[j].getNameExt();
                        if (this.locator != null) {
                            HashMap<String, List<Object[]>> scan = new HashMap<String, List<Object[]>>();
                            this.locator.scan(this.folder, mimeType, null, false, true, true, false, scan);
                            add = !scan.isEmpty();
                            this.trackedFolders.add(subTypes[j]);
                        } else {
                            add = true;
                        }
                        if (!add) continue;
                        String displayName = Utils.getLocalizedName(subTypes[j], null);
                        if (displayName == null) {
                            displayName = Utils.getLocalizedName(subTypes[j], mimeType, mimeType);
                        }
                        newMimeTypes.put(mimeType, displayName);
                    }
                }
                newMimeTypes = Collections.unmodifiableMap(newMimeTypes);
            } else {
                newMimeTypes = Collections.emptyMap();
            }
            if (!this.mimeTypes.equals(newMimeTypes)) {
                event = new PropertyChangeEvent(this, "mime-types", this.mimeTypes, newMimeTypes);
                this.mimeTypes = newMimeTypes;
            }
            Iterator<FileObject> i = this.trackedFolders.iterator();
            while (i.hasNext()) {
                if (i.next().isValid()) continue;
                i.remove();
            }
        }
        if (event != null) {
            this.pcs.firePropertyChange(event);
            EditorSettingsImpl.getInstance().notifyMimeTypesChange(event.getOldValue(), event.getNewValue());
        }
    }

    private static boolean isValidType(FileObject typeFile) {
        if (!typeFile.isFolder()) {
            return false;
        }
        String typeName = typeFile.getNameExt();
        return MimePath.validate((CharSequence)typeName, (CharSequence)null);
    }

    private static boolean isValidSubtype(FileObject subtypeFile) {
        if (!subtypeFile.isFolder()) {
            return false;
        }
        String typeName = subtypeFile.getNameExt();
        return MimePath.validate((CharSequence)null, (CharSequence)typeName) && !typeName.equals("base");
    }

    private static Object[] findTarget(String[] path) {
        FileObject f;
        FileObject target = FileUtil.getConfigRoot();
        boolean isTarget = 0 == path.length;
        for (int i = 0; i < path.length && (f = target.getFileObject(path[i])) != null && f.isFolder() && f.isValid() && !f.isVirtual(); ++i) {
            target = f;
            isTarget = i + 1 == path.length;
        }
        return new Object[]{target, isTarget};
    }

    private final class Listener
    extends FileChangeAdapter {
        public void fileFolderCreated(FileEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        public void fileDeleted(FileEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        private void notifyRebuild(FileObject f) {
            String path = f.getPath();
            if (path.startsWith(MimeTypesTracker.this.basePath)) {
                if (MimeTypesTracker.synchronous) {
                    MimeTypesTracker.this.rebuild();
                } else {
                    MimeTypesTracker.this.task.schedule(1000);
                }
            }
        }
    }

}

