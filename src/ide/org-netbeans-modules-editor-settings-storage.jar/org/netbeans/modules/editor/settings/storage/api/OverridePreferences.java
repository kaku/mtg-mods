/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings.storage.api;

public interface OverridePreferences {
    public boolean isOverriden(String var1);
}

