/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import org.netbeans.modules.editor.settings.storage.MimeTypesTracker;
import org.netbeans.modules.editor.settings.storage.SettingsType;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class ProfilesTracker {
    public static final String PROP_PROFILES = "profiles";
    private static final Map<String, Map<StorageDescription, ProfilesTracker>> settingProfiles = new HashMap<String, Map<StorageDescription, ProfilesTracker>>();
    private static final Logger LOG = Logger.getLogger(ProfilesTracker.class.getName());
    private final SettingsType.Locator locator;
    private final MimeTypesTracker mimeTypes;
    private final FileSystem systemFileSystem;
    private final Listener listener;
    private final PropertyChangeSupport pcs;
    private final String LOCK;
    private Map<String, ProfileDescription> profiles;
    private Map<String, ProfileDescription> profilesByDisplayName;
    static boolean synchronous = false;
    private final RequestProcessor.Task task;
    private final Set<FileObject> trackedFolders;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static ProfilesTracker get(String settingsTypeId, String basePath) {
        assert (settingsTypeId != null);
        assert (basePath != null);
        StorageDescription sd = SettingsType.find(settingsTypeId);
        assert (sd != null);
        Map<String, Map<StorageDescription, ProfilesTracker>> map = settingProfiles;
        synchronized (map) {
            ProfilesTracker tracker;
            Map<StorageDescription, ProfilesTracker> map2 = settingProfiles.get(basePath);
            if (map2 == null) {
                map2 = new WeakHashMap<StorageDescription, ProfilesTracker>();
                settingProfiles.put(basePath, map2);
            }
            if ((tracker = map2.get(sd)) == null) {
                SettingsType.Locator locator = SettingsType.getLocator(sd);
                assert (locator.isUsingProfiles());
                tracker = new ProfilesTracker(locator, MimeTypesTracker.get(null, basePath));
                map2.put(sd, tracker);
            }
            return tracker;
        }
    }

    ProfilesTracker(SettingsType.Locator locator, MimeTypesTracker mimeTypes) {
        this.pcs = new PropertyChangeSupport(this);
        this.LOCK = new String("ProfilesTracker.LOCK");
        this.profiles = Collections.emptyMap();
        this.profilesByDisplayName = Collections.emptyMap();
        this.task = MimeTypesTracker.RP.create(new Runnable(){

            @Override
            public void run() {
                ProfilesTracker.this.rebuild();
            }
        });
        this.trackedFolders = new HashSet<FileObject>();
        this.locator = locator;
        this.mimeTypes = mimeTypes;
        this.rebuild();
        this.listener = new Listener();
        FileSystem sfs = null;
        try {
            sfs = FileUtil.getConfigRoot().getFileSystem();
        }
        catch (FileStateInvalidException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        this.systemFileSystem = sfs;
        this.systemFileSystem.addFileChangeListener((FileChangeListener)WeakListeners.create(FileChangeListener.class, (EventListener)this.listener, (Object)this.systemFileSystem));
        this.mimeTypes.addPropertyChangeListener(this.listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<String> getProfilesDisplayNames() {
        String string = this.LOCK;
        synchronized (string) {
            return this.profilesByDisplayName.keySet();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ProfileDescription getProfileByDisplayName(String displayName) {
        String string = this.LOCK;
        synchronized (string) {
            return this.profilesByDisplayName.get(displayName);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void rebuild() {
        PropertyChangeEvent event = null;
        String string = this.LOCK;
        synchronized (string) {
            HashMap<String, List<Object[]>> scan = new HashMap<String, List<Object[]>>();
            FileObject baseFolder = FileUtil.getConfigFile((String)this.mimeTypes.getBasePath());
            if (baseFolder != null && baseFolder.isFolder()) {
                this.locator.scan(baseFolder, null, null, false, true, true, false, scan);
                Set<String> mimes = this.mimeTypes.getMimeTypes();
                for (String mime : mimes) {
                    this.locator.scan(baseFolder, mime, null, false, true, true, false, scan);
                }
            }
            HashMap<String, ProfileDescription> newProfiles = new HashMap<String, ProfileDescription>();
            HashMap<String, ProfileDescription> newProfilesByDisplayName = new HashMap<String, ProfileDescription>();
            for (String id : scan.keySet()) {
                ProfileDescription maybeDupl;
                List<Object[]> profileInfos = scan.get(id);
                String displayName = null;
                boolean canRollback = false;
                String profileOrigin = null;
                for (Object[] info : profileInfos) {
                    FileObject profileHome = (FileObject)info[0];
                    FileObject settingFile = (FileObject)info[1];
                    boolean modulesFile = (Boolean)info[2];
                    if (profileHome != null) {
                        this.trackedFolders.add(profileHome.getParent());
                        profileOrigin = profileHome.getPath();
                        if (displayName == null && (displayName = Utils.getLocalizedName(profileHome, null)) == null) {
                            displayName = Utils.getLocalizedName(profileHome, id, null);
                        }
                    } else {
                        profileOrigin = settingFile.getPath();
                    }
                    if (!canRollback) {
                        canRollback = modulesFile;
                    }
                    if (displayName == null || !canRollback) continue;
                    break;
                }
                if ((maybeDupl = newProfilesByDisplayName.get(displayName = displayName == null ? id : displayName)) != null) {
                    String writableFile = baseFolder.getPath() + "/" + this.locator.getWritableFileName(null, id, null, false);
                    if (writableFile.startsWith(profileOrigin)) {
                        newProfiles.remove(maybeDupl.getId());
                        newProfilesByDisplayName.remove(displayName);
                        LOG.warning("Ignoring profile '" + maybeDupl.getId() + "' (" + maybeDupl.profileOrigin + ") in favor of '" + id + "' (" + profileOrigin + ") with the same display name.");
                    } else {
                        LOG.warning("Ignoring profile '" + id + "' (" + profileOrigin + "), it's got the same display name as '" + maybeDupl.getId() + "' (" + maybeDupl.profileOrigin + ").");
                        continue;
                    }
                }
                ProfileDescription desc = this.reuseOrCreate(id, displayName, canRollback, profileOrigin);
                newProfiles.put(id, desc);
                newProfilesByDisplayName.put(displayName, desc);
            }
            assert (newProfilesByDisplayName.size() == newProfiles.size());
            if (!this.profiles.equals(newProfiles)) {
                event = new PropertyChangeEvent(this, "profiles", this.profiles, newProfiles);
                this.profiles = newProfiles;
                this.profilesByDisplayName = newProfilesByDisplayName;
            }
            Iterator<FileObject> i = this.trackedFolders.iterator();
            while (i.hasNext()) {
                if (i.next().isValid()) continue;
                i.remove();
            }
        }
        if (event != null) {
            this.pcs.firePropertyChange(event);
        }
    }

    private ProfileDescription reuseOrCreate(String id, String displayName, boolean rollback, String profileOrigin) {
        ProfileDescription desc = this.profiles.get(id);
        if (desc != null && desc.getDisplayName().equals(displayName) && desc.isRollbackAllowed() == rollback) {
            return desc;
        }
        return new ProfileDescription(id, displayName, rollback, profileOrigin);
    }

    private final class Listener
    extends FileChangeAdapter
    implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            ProfilesTracker.this.rebuild();
        }

        public void fileDataCreated(FileEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        public void fileFolderCreated(FileEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        public void fileDeleted(FileEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.notifyRebuild(fe.getFile());
        }

        private void notifyRebuild(FileObject file) {
            String path = file.getPath();
            if (path.startsWith(ProfilesTracker.this.mimeTypes.getBasePath())) {
                if (ProfilesTracker.synchronous) {
                    ProfilesTracker.this.rebuild();
                } else {
                    ProfilesTracker.this.task.schedule(1000);
                }
            }
        }
    }

    public static final class ProfileDescription {
        private final String id;
        private final String displayName;
        private final boolean isRollbackAllowed;
        private final String profileOrigin;

        private ProfileDescription(String id, String displayName, boolean isRollbackAllowed, String profileOrigin) {
            this.id = id;
            this.displayName = displayName;
            this.isRollbackAllowed = isRollbackAllowed;
            this.profileOrigin = profileOrigin;
        }

        public boolean isRollbackAllowed() {
            return this.isRollbackAllowed;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getId() {
            return this.id;
        }
    }

}

