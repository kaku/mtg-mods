/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.settings.storage.spi;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.editor.settings.storage.SpiPackageAccessor;
import org.netbeans.modules.editor.settings.storage.spi.StorageFilter;
import org.openide.filesystems.FileObject;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

public abstract class StorageReader<K, V>
extends DefaultHandler
implements LexicalHandler {
    private static final Logger LOG = Logger.getLogger(StorageReader.class.getName());
    private final FileObject file;
    private final boolean isModuleFile;
    private final boolean isDefaultProfile;
    private final String mimePath;

    protected StorageReader(FileObject processedFile, String mimePath) {
        assert (processedFile != null);
        assert (mimePath != null);
        this.file = processedFile;
        this.mimePath = mimePath;
        boolean moduleFile = false;
        boolean defaultProfile = false;
        FileObject parent = this.file.getParent();
        if (parent != null) {
            moduleFile = parent.getNameExt().contains("Default");
            if ((parent = parent.getParent()) != null) {
                defaultProfile = parent.getNameExt().contains("NetBeans");
            }
        }
        this.isModuleFile = moduleFile;
        this.isDefaultProfile = defaultProfile;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        this.log("warning", e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        this.log("error", e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw this.log("fatal error", e);
    }

    @Override
    public InputSource resolveEntity(String pubid, String sysid) {
        return new InputSource(new ByteArrayInputStream(new byte[0]));
    }

    @Override
    public void startCDATA() throws SAXException {
    }

    @Override
    public void endCDATA() throws SAXException {
    }

    @Override
    public void comment(char[] ch, int start, int length) throws SAXException {
    }

    @Override
    public void endDTD() throws SAXException {
    }

    @Override
    public void endEntity(String name) throws SAXException {
    }

    @Override
    public void startDTD(String name, String publicId, String systemId) throws SAXException {
    }

    @Override
    public void startEntity(String name) throws SAXException {
    }

    public abstract Map<K, V> getAdded();

    public abstract Set<K> getRemoved();

    protected final FileObject getProcessedFile() {
        return this.file;
    }

    protected final boolean isModuleFile() {
        return this.isModuleFile;
    }

    protected final boolean isDefaultProfile() {
        return this.isDefaultProfile;
    }

    protected final String getMimePath() {
        return this.mimePath;
    }

    private SAXException log(String errorType, SAXParseException e) {
        Level level;
        String message;
        if (this.file == null) {
            level = Level.FINE;
            message = "XML parser " + errorType;
        } else {
            level = this.isModuleFile() ? Level.WARNING : Level.FINE;
            message = "XML parser " + errorType + " in file " + this.file.getPath();
        }
        SAXException saxe = new SAXException(message);
        saxe.initCause(e);
        LOG.log(level, message, saxe);
        return saxe;
    }

    static {
        SpiPackageAccessor.register(new SpiPackageAccessorImpl());
    }

    private static final class SpiPackageAccessorImpl
    extends SpiPackageAccessor {
        private SpiPackageAccessorImpl() {
        }

        @Override
        public String storageFilterGetStorageDescriptionId(StorageFilter f) {
            return f.getStorageDescriptionId();
        }

        @Override
        public void storageFilterInitialize(StorageFilter f, Callable<Void> notificationCallback) {
            f.initialize(notificationCallback);
        }
    }

}

