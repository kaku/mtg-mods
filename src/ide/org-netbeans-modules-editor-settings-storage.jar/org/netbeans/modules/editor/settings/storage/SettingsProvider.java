/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.spi.editor.mimelookup.MimeDataProvider
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.settings.storage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.fontscolors.CompositeFCS;
import org.netbeans.modules.editor.settings.storage.fontscolors.FontColorSettingsImpl;
import org.netbeans.modules.editor.settings.storage.keybindings.KeyBindingSettingsImpl;
import org.netbeans.modules.editor.settings.storage.preferences.PreferencesImpl;
import org.netbeans.spi.editor.mimelookup.MimeDataProvider;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public final class SettingsProvider
implements MimeDataProvider {
    private static final Logger LOG = Logger.getLogger(SettingsProvider.class.getName());
    private final Map<MimePath, WeakReference<Lookup>> cache = new WeakHashMap<MimePath, WeakReference<Lookup>>();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Lookup getLookup(MimePath mimePath) {
        if (mimePath.size() > 0 && mimePath.getMimeType(0).contains("text/base")) {
            if (LOG.isLoggable(Level.INFO)) {
                LOG.log(Level.INFO, "Won't provide any settings for text/base It's been deprecated, use MimePath.EMPTY instead.");
            }
            return null;
        }
        Map<MimePath, WeakReference<Lookup>> map = this.cache;
        synchronized (map) {
            Object lookup;
            WeakReference<Lookup> ref = this.cache.get((Object)mimePath);
            Object object = lookup = ref == null ? null : ref.get();
            if (lookup == null) {
                String path = mimePath.getPath();
                if (path.startsWith("test")) {
                    int idx = path.indexOf(95);
                    if (idx == -1) {
                        throw new IllegalStateException("Invalid mimePath: " + path);
                    }
                    String profile = path.substring(0, idx);
                    MimePath realMimePath = MimePath.parse((String)path.substring(idx + 1));
                    lookup = new ProxyLookup(new Lookup[]{new MyLookup(realMimePath, profile), Lookups.exclude((Lookup)MimeLookup.getLookup((MimePath)realMimePath), (Class[])new Class[]{FontColorSettings.class, KeyBindingSettings.class})});
                } else {
                    lookup = new MyLookup(mimePath, null);
                }
                this.cache.put(mimePath, new WeakReference<Object>(lookup));
            }
            return lookup;
        }
    }

    private static final class MyLookup
    extends AbstractLookup
    implements PropertyChangeListener,
    PreferenceChangeListener {
        private final MimePath mimePath;
        private final boolean specialFcsProfile;
        private String fcsProfile;
        private final InstanceContent ic;
        private CompositeFCS fontColorSettings = null;
        private Object keyBindingSettings = null;
        private PreferencesImpl preferences = null;
        private KeyBindingSettingsImpl kbsi;

        public MyLookup(MimePath mimePath, String profile) {
            this(mimePath, profile, new InstanceContent());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            MyLookup myLookup = this;
            synchronized (myLookup) {
                boolean fcsChanged = false;
                boolean kbsChanged = false;
                if (this.kbsi == evt.getSource()) {
                    kbsChanged = true;
                } else if (evt.getPropertyName() == null) {
                    if (!this.specialFcsProfile) {
                        String currentProfile = EditorSettings.getDefault().getCurrentFontColorProfile();
                        this.fcsProfile = FontColorSettingsImpl.get(this.mimePath).getInternalFontColorProfile(currentProfile);
                    }
                    fcsChanged = true;
                } else if (evt.getPropertyName().equals("editorFontColors")) {
                    String changedProfile = (String)evt.getNewValue();
                    if (changedProfile.equals(this.fcsProfile)) {
                        fcsChanged = true;
                    }
                } else if (evt.getPropertyName().equals("fontColors")) {
                    String changedProfile = (String)evt.getNewValue();
                    if (changedProfile.equals(this.fcsProfile)) {
                        MimePath changedMimePath = (MimePath)evt.getOldValue();
                        if (this.fontColorSettings != null && this.fontColorSettings.isDerivedFromMimePath(changedMimePath)) {
                            fcsChanged = true;
                        }
                    }
                } else if (evt.getPropertyName().equals("currentFontColorProfile") && !this.specialFcsProfile) {
                    String newProfile = (String)evt.getNewValue();
                    this.fcsProfile = FontColorSettingsImpl.get(this.mimePath).getInternalFontColorProfile(newProfile);
                    fcsChanged = true;
                }
                this.updateContents(kbsChanged, fcsChanged);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            String settingName;
            String string = settingName = evt == null ? null : evt.getKey();
            if (settingName == null || settingName.equals("textAntialiasing")) {
                MyLookup myLookup = this;
                synchronized (myLookup) {
                    this.updateContents(false, true);
                }
            }
        }

        private MyLookup(MimePath mimePath, String profile, InstanceContent ic) {
            super((AbstractLookup.Content)ic);
            this.mimePath = mimePath;
            if (profile == null) {
                String currentProfile = EditorSettings.getDefault().getCurrentFontColorProfile();
                this.fcsProfile = FontColorSettingsImpl.get(mimePath).getInternalFontColorProfile(currentProfile);
                this.specialFcsProfile = false;
            } else {
                this.fcsProfile = profile;
                this.specialFcsProfile = true;
            }
            this.ic = ic;
            EditorSettings es = EditorSettings.getDefault();
            es.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)es));
            this.kbsi = KeyBindingSettingsImpl.get(mimePath);
            this.kbsi.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.kbsi));
            if (this.preferences == null) {
                this.preferences = PreferencesImpl.get(mimePath);
                this.preferences.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.preferences));
            }
            this.fontColorSettings = new CompositeFCS(mimePath, this.fcsProfile, this.preferences);
            this.keyBindingSettings = this.kbsi.createInstanceForLookup();
            ic.set(Arrays.asList(new Object[]{this.fontColorSettings, this.keyBindingSettings, this.preferences}), null);
        }

        private void updateContents(boolean kbsChanged, boolean fcsChanged) {
            boolean updateContents = false;
            if (fcsChanged && this.fontColorSettings != null) {
                this.fontColorSettings = new CompositeFCS(this.mimePath, this.fcsProfile, this.preferences);
                updateContents = true;
            }
            if (kbsChanged && this.keyBindingSettings != null) {
                this.keyBindingSettings = this.kbsi.createInstanceForLookup();
                updateContents = true;
            }
            if (updateContents) {
                ArrayList<Object> list = new ArrayList<Object>();
                if (this.fontColorSettings != null) {
                    list.add((Object)this.fontColorSettings);
                }
                if (this.keyBindingSettings != null) {
                    list.add(this.keyBindingSettings);
                }
                if (this.preferences != null) {
                    list.add(this.preferences);
                }
                this.ic.set(list, null);
            }
        }
    }

}

