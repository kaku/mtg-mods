/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.editor.settings.storage.spi;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimePath;

public abstract class StorageFilter<K, V> {
    private static final Logger LOG = Logger.getLogger(StorageFilter.class.getName());
    private final String storageDescriptionId;
    private Callable<?> notificationCallback;

    protected StorageFilter(String storageDescriptionId) {
        this.storageDescriptionId = storageDescriptionId;
    }

    public abstract void afterLoad(Map<K, V> var1, MimePath var2, String var3, boolean var4) throws IOException;

    public abstract void beforeSave(Map<K, V> var1, MimePath var2, String var3, boolean var4) throws IOException;

    protected final void notifyChanges() {
        assert (this.notificationCallback != null);
        try {
            this.notificationCallback.call();
        }
        catch (Exception ex) {
            LOG.log(Level.WARNING, null, ex);
        }
    }

    final void initialize(Callable<?> notificationCallback) {
        this.notificationCallback = notificationCallback;
    }

    final String getStorageDescriptionId() {
        return this.storageDescriptionId;
    }
}

