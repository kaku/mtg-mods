/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.settings.storage.spi;

import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.openide.filesystems.FileObject;

public interface StorageDescription<K, V> {
    public String getId();

    public boolean isUsingProfiles();

    public String getMimeType();

    public String getLegacyFileName();

    public StorageReader<K, V> createReader(FileObject var1, String var2);

    public StorageWriter<K, V> createWriter(FileObject var1, String var2);
}

