/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharacterConversions
 *  org.openide.filesystems.FileObject
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.editor.settings.storage.preferences;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.CharacterConversions;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;
import org.netbeans.modules.editor.settings.storage.spi.support.StorageSupport;
import org.openide.filesystems.FileObject;
import org.openide.xml.XMLUtil;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public final class PreferencesStorage
implements StorageDescription<String, TypedValue> {
    private static final Logger LOG = Logger.getLogger(PreferencesStorage.class.getName());
    public static final String ID = "Preferences";
    private static final String E_ROOT = "editor-preferences";
    private static final String E_ENTRY = "entry";
    private static final String E_VALUE = "value";
    private static final String A_NAME = "name";
    private static final String A_VALUE = "value";
    private static final String A_VALUE_ID = "valueId";
    private static final String A_JAVA_TYPE = "javaType";
    private static final String A_CATEGORY = "category";
    private static final String A_REMOVE = "remove";
    private static final String A_XML_SPACE = "xml:space";
    private static final String V_PRESERVE = "preserve";
    private static final String PUBLIC_ID = "-//NetBeans//DTD Editor Preferences 1.0//EN";
    private static final String SYSTEM_ID = "http://www.netbeans.org/dtds/EditorPreferences-1_0.dtd";
    private static final String MIME_TYPE = "text/x-nbeditor-preferences";

    @Override
    public String getId() {
        return "Preferences";
    }

    @Override
    public boolean isUsingProfiles() {
        return false;
    }

    @Override
    public String getMimeType() {
        return "text/x-nbeditor-preferences";
    }

    @Override
    public String getLegacyFileName() {
        return "properties.xml";
    }

    @Override
    public StorageReader<String, TypedValue> createReader(FileObject f, String mimePath) {
        if ("text/x-nbeditor-preferences".equals(f.getMIMEType())) {
            return new Reader(f, mimePath);
        }
        return new LegacyReader(f, mimePath);
    }

    @Override
    public StorageWriter<String, TypedValue> createWriter(FileObject f, String mimePath) {
        return new Writer();
    }

    private static final class Writer
    extends StorageWriter<String, TypedValue> {
        @Override
        public Document getDocument() {
            Document doc = XMLUtil.createDocument((String)"editor-preferences", (String)null, (String)"-//NetBeans//DTD Editor Preferences 1.0//EN", (String)"http://www.netbeans.org/dtds/EditorPreferences-1_0.dtd");
            Node root = doc.getElementsByTagName("editor-preferences").item(0);
            TreeMap added = new TreeMap(this.getAdded());
            for (String name : added.keySet()) {
                String apiCategory;
                Element element = doc.createElement("entry");
                root.appendChild(element);
                element.setAttribute("name", name);
                String value = ((TypedValue)this.getAdded().get(name)).getValue();
                if (value.length() > 0) {
                    Element valueElement = doc.createElement("value");
                    valueElement.appendChild(doc.createCDATASection(CharacterConversions.lineFeedToLineSeparator((CharSequence)value)));
                    element.appendChild(valueElement);
                } else {
                    element.setAttribute(name, value);
                }
                String javaType = ((TypedValue)this.getAdded().get(name)).getJavaType();
                if (javaType != null && javaType.length() > 0) {
                    element.setAttribute("javaType", javaType);
                }
                if ((apiCategory = ((TypedValue)this.getAdded().get(name)).getApiCategory()) != null && apiCategory.length() > 0) {
                    element.setAttribute("category", apiCategory);
                }
                element.setAttribute("xml:space", "preserve");
            }
            ArrayList removed = new ArrayList(this.getRemoved());
            Collections.sort(removed);
            for (String name2 : removed) {
                Element element = doc.createElement("entry");
                root.appendChild(element);
                element.setAttribute("name", name2);
                element.setAttribute("remove", Boolean.TRUE.toString());
            }
            return doc;
        }
    }

    private static final class LegacyReader
    extends PreferencesReader {
        private static final String EL_ROOT = "properties";
        private static final String EL_PROPERTY = "property";
        private static final String AL_NAME = "name";
        private static final String AL_CLASS = "class";
        private static final String AL_VALUE = "value";
        private Map<String, TypedValue> entriesMap = new HashMap<String, TypedValue>();
        private String name = null;
        private String value = null;
        private String javaType = null;

        public LegacyReader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        @Override
        public Map<String, TypedValue> getAdded() {
            return this.entriesMap;
        }

        @Override
        public Set<String> getRemoved() {
            return Collections.emptySet();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (!qName.equals("properties") && qName.equals("property")) {
                this.name = attributes.getValue("name");
                this.value = attributes.getValue("value");
                this.javaType = attributes.getValue("class");
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (!qName.equals("properties") && qName.equals("property")) {
                if (this.name != null && this.value != null) {
                    this.entriesMap.put(this.name, new TypedValue(this.value, this.javaType));
                } else {
                    LOG.warning("Ignoring editor preferences legacy entry {'" + this.name + "', '" + this.value + "'}!");
                }
            }
        }
    }

    private static final class Reader
    extends PreferencesReader {
        private Map<String, TypedValue> entriesMap = new HashMap<String, TypedValue>();
        private Set<String> removedEntries = new HashSet<String>();
        private String name = null;
        private String value = null;
        private String javaType = null;
        private String apiCategory = null;
        private StringBuilder text = null;
        private StringBuilder cdataText = null;
        private boolean insideCdata = false;
        private static final String[] ALL_API_CATEGORIES = new String[]{"private", "stable", "devel", "friend", "deprecated"};

        public Reader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        @Override
        public Map<String, TypedValue> getAdded() {
            return this.entriesMap;
        }

        @Override
        public Set<String> getRemoved() {
            return this.removedEntries;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (this.text != null) {
                this.text.append(ch, start, length);
                if (this.insideCdata) {
                    this.cdataText.append(ch, start, length);
                }
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (!qName.equals("editor-preferences")) {
                if (qName.equals("entry")) {
                    boolean removed = Boolean.valueOf(attributes.getValue("remove"));
                    this.name = null;
                    this.value = null;
                    this.javaType = null;
                    this.text = null;
                    this.cdataText = null;
                    if (removed) {
                        String entryName = attributes.getValue("name");
                        this.removedEntries.add(entryName);
                    } else {
                        String valueValue;
                        String localizedValue;
                        this.name = attributes.getValue("name");
                        String valueId = attributes.getValue("valueId");
                        if (valueId != null && (localizedValue = StorageSupport.getLocalizingBundleMessage(this.getProcessedFile(), valueId, null)) != null) {
                            this.value = localizedValue;
                        }
                        if ((valueValue = attributes.getValue("value")) != null) {
                            if (this.value == null) {
                                this.value = valueValue;
                            } else {
                                LOG.warning("The 'valueId' attribute specified valid resource bundle key, ignoring the 'value' attribute!");
                            }
                        }
                        this.javaType = attributes.getValue("javaType");
                        this.apiCategory = attributes.getValue("category");
                    }
                } else if (this.name != null && qName.equals("value")) {
                    if (this.value == null) {
                        this.text = new StringBuilder();
                        this.cdataText = new StringBuilder();
                        this.insideCdata = false;
                    } else {
                        LOG.warning("The 'value' or 'valueId' attribute was specified, ignoring the <value/> element!");
                    }
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (!qName.equals("editor-preferences")) {
                if (qName.equals("entry")) {
                    if (this.name != null) {
                        if (this.value != null) {
                            if (!this.entriesMap.containsKey(this.name)) {
                                TypedValue typedValue = new TypedValue(CharacterConversions.lineSeparatorToLineFeed((CharSequence)this.value), this.javaType);
                                if (this.apiCategory != null && this.apiCategory.length() > 0) {
                                    typedValue.setApiCategory(Reader.checkApiCategory(this.apiCategory));
                                }
                                this.entriesMap.put(this.name, typedValue);
                            } else {
                                LOG.warning("Ignoring duplicate editor preferences entry '" + this.name + "'!");
                            }
                        } else {
                            LOG.warning("Ignoring editor preferences entry '" + this.name + "' that does not specify any value!");
                        }
                    }
                } else if (qName.equals("value") && this.text != null) {
                    this.value = this.cdataText.length() > 0 ? this.cdataText.toString() : this.text.toString();
                }
            }
        }

        @Override
        public void startCDATA() throws SAXException {
            if (this.cdataText != null) {
                this.insideCdata = true;
            }
        }

        @Override
        public void endCDATA() throws SAXException {
            if (this.cdataText != null) {
                this.insideCdata = false;
            }
        }

        private static String checkApiCategory(String apiCategory) {
            for (String c : ALL_API_CATEGORIES) {
                if (!c.equalsIgnoreCase(apiCategory)) continue;
                return c;
            }
            return ALL_API_CATEGORIES[0];
        }
    }

    private static abstract class PreferencesReader
    extends StorageReader<String, TypedValue> {
        protected PreferencesReader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        @Override
        public abstract Map<String, TypedValue> getAdded();

        @Override
        public abstract Set<String> getRemoved();
    }

}

