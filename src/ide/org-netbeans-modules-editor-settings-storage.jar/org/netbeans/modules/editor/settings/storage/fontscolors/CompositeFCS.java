/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.settings.storage.fontscolors;

import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.UIManager;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.fontscolors.FontColorSettingsImpl;
import org.openide.util.Utilities;

public final class CompositeFCS
extends FontColorSettings {
    private static final Logger LOG = Logger.getLogger(CompositeFCS.class.getName());
    public static final String TEXT_ANTIALIASING_PROP = "textAntialiasing";
    private static final AttributeSet NULL = new SimpleAttributeSet();
    private final FontColorSettingsImpl[] allFcsi;
    final String profile;
    private final Map<String, AttributeSet> tokensCache = new HashMap<String, AttributeSet>();
    private final Preferences preferences;
    private static final Map<Object, String> renderingHintsConstants = new HashMap<Object, String>();
    private static AttributeSet hardCodedDefaultColoring = null;
    private static final int DEFAULT_FONTSIZE = 13;

    public CompositeFCS(MimePath mimePath, String profile, Preferences preferences) {
        boolean empty;
        String lastMimeType;
        assert (mimePath != null);
        assert (profile != null);
        while (mimePath.size() > 1 && (empty = FontColorSettingsImpl.get(MimePath.parse((String)(lastMimeType = mimePath.getMimeType(mimePath.size() - 1)))).getColorings(profile).isEmpty())) {
            mimePath = mimePath.getPrefix(mimePath.size() - 1);
        }
        List allPaths = mimePath.getIncludedPaths();
        assert (allPaths.size() > 0);
        this.allFcsi = new FontColorSettingsImpl[allPaths.size()];
        for (int i = 0; i < allPaths.size(); ++i) {
            this.allFcsi[i] = FontColorSettingsImpl.get((MimePath)allPaths.get(i));
        }
        this.profile = profile;
        this.preferences = preferences;
    }

    public AttributeSet getFontColors(String highlightName) {
        assert (highlightName != null);
        AttributeSet attribs = null;
        Map<String, AttributeSet> coloringsMap = EditorSettings.getDefault().getHighlightings(this.profile);
        if (coloringsMap != null) {
            attribs = coloringsMap.get(highlightName);
        }
        if (highlightName.equals("default") && (attribs == null || attribs.getAttribute(StyleConstants.FontFamily) == null)) {
            ArrayList<AttributeSet> colorings = new ArrayList<AttributeSet>();
            String name = highlightName;
            for (FontColorSettingsImpl fcsi : this.allFcsi) {
                name = this.processLayer(fcsi, name, colorings);
            }
            colorings.add(CompositeFCS.getHardcodedDefaultColoring());
            colorings.add(AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.RenderingHints, this.getRenderingHints()}));
            return AttributesUtilities.createImmutable((AttributeSet[])colorings.toArray(new AttributeSet[colorings.size()]));
        }
        return attribs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public AttributeSet getTokenFontColors(String tokenName) {
        assert (tokenName != null);
        Map<String, AttributeSet> map = this.tokensCache;
        synchronized (map) {
            AttributeSet attribs = this.tokensCache.get(tokenName);
            if (attribs == null) {
                attribs = this.findColoringForToken(tokenName);
                this.tokensCache.put(tokenName, attribs);
            }
            return attribs == NULL ? null : attribs;
        }
    }

    public boolean isDerivedFromMimePath(MimePath mimePath) {
        for (FontColorSettingsImpl fcsi : this.allFcsi) {
            if (fcsi.getMimePath() != mimePath) continue;
            return true;
        }
        return false;
    }

    private AttributeSet findColoringForToken(String tokenName) {
        ArrayList<AttributeSet> colorings = new ArrayList<AttributeSet>();
        String name = tokenName;
        for (FontColorSettingsImpl fcsi : this.allFcsi) {
            name = this.processLayer(fcsi, name, colorings);
        }
        if (colorings.size() > 0) {
            return AttributesUtilities.createImmutable((AttributeSet[])colorings.toArray(new AttributeSet[colorings.size()]));
        }
        return NULL;
    }

    private String processLayer(FontColorSettingsImpl fcsi, String name, ArrayList<AttributeSet> colorings) {
        AttributeSet as = fcsi.getColorings(this.profile).get(name);
        if (as == null) {
            as = fcsi.getDefaultColorings(this.profile).get(name);
        }
        if (as != null) {
            colorings.add(as);
            String nameOfColoring = (String)as.getAttribute(StyleConstants.NameAttribute);
            String nameOfDelegate = (String)as.getAttribute(EditorStyleConstants.Default);
            if (nameOfDelegate != null && !nameOfDelegate.equals("default")) {
                if (!nameOfDelegate.equals(nameOfColoring)) {
                    nameOfDelegate = this.processLayer(fcsi, nameOfDelegate, colorings);
                }
            } else {
                nameOfDelegate = nameOfColoring;
            }
            name = nameOfDelegate;
        }
        return name;
    }

    private void dumpAttribs(AttributeSet attribs, String name, boolean tokenColoring) {
        StringBuilder sb = new StringBuilder();
        sb.append("Attribs for base mime path '");
        sb.append(this.allFcsi[0].getMimePath().getPath());
        sb.append("' and ");
        if (tokenColoring) {
            sb.append("token '");
        } else {
            sb.append("highlight '");
        }
        sb.append(name);
        sb.append("' = {");
        Enumeration keys = attribs.getAttributeNames();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = attribs.getAttribute(key);
            sb.append("'").append(key).append("' = '").append(value).append("'");
            if (!keys.hasMoreElements()) continue;
            sb.append(", ");
        }
        sb.append("} CompoundFCS.this = ");
        sb.append(this.toString());
        System.out.println(sb.toString());
    }

    private Map<?, ?> getRenderingHints() {
        Map<RenderingHints.Key, Object> hints;
        Map desktopHints = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("System provided desktop hints:");
            if (desktopHints != null) {
                for (Object key : desktopHints.keySet()) {
                    Object value = desktopHints.get(key);
                    String humanReadableKey = CompositeFCS.translateRenderingHintsConstant(key);
                    String humanReadableValue = CompositeFCS.translateRenderingHintsConstant(value);
                    LOG.fine("  " + humanReadableKey + " = " + humanReadableValue);
                }
            } else {
                LOG.fine("There are no desktop hints");
            }
            LOG.fine("----------------");
        }
        Boolean aaOn = null;
        String reason = null;
        String systemProperty = System.getProperty("javax.aatext");
        if (systemProperty == null) {
            systemProperty = System.getProperty("swing.aatext");
        }
        if (systemProperty != null) {
            aaOn = Boolean.valueOf(systemProperty);
            reason = "system property 'javax.aatext' or 'swing.aatext'";
        } else if (Utilities.isMac()) {
            aaOn = Boolean.TRUE;
            reason = "running on Mac OSX";
        }
        if (aaOn == null) {
            LOG.fine("Text Antialiasing setting was not determined, using defaults.");
            if (desktopHints != null) {
                LOG.fine("Using system provided desktop hints");
                hints = new HashMap<RenderingHints.Key, Object>(desktopHints);
            } else {
                LOG.fine("No system provided desktop hints available, using hardcoded defaults");
                hints = Collections.singletonMap(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);
            }
        } else {
            LOG.fine("Text Antialiasing was set " + (aaOn != false ? "ON" : "OFF") + " by " + reason + ".");
            if (desktopHints != null) {
                LOG.fine("Using system provided desktop hints");
                hints = new HashMap<RenderingHints.Key, Object>(desktopHints);
            } else {
                LOG.fine("No system provided desktop hints available, using hardcoded defaults");
                hints = new HashMap<RenderingHints.Key, Object>();
            }
            if (aaOn.booleanValue()) {
                if (!hints.containsKey(RenderingHints.KEY_TEXT_ANTIALIASING)) {
                    hints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                }
            } else {
                hints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            }
        }
        if (Boolean.getBoolean("org.netbeans.editor.aa.extra.hints")) {
            hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            hints.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Editor Rendering hints:");
            for (RenderingHints.Key key : hints.keySet()) {
                Object value = hints.get(key);
                String humanReadableKey = CompositeFCS.translateRenderingHintsConstant(key);
                String humanReadableValue = CompositeFCS.translateRenderingHintsConstant(value);
                LOG.fine("  " + humanReadableKey + " = " + humanReadableValue);
            }
            LOG.fine("----------------");
        }
        return hints;
    }

    private static synchronized String translateRenderingHintsConstant(Object c) {
        String s = null;
        if (c != null && (s = renderingHintsConstants.get(c)) == null) {
            for (Field f : RenderingHints.class.getFields()) {
                try {
                    f.setAccessible(true);
                    if ((f.getModifiers() & 8) == 0 || f.get(null) != c) continue;
                    s = f.getName();
                    break;
                }
                catch (IllegalAccessException iae) {
                    // empty catch block
                }
            }
            if (s != null) {
                renderingHintsConstants.put(c, s);
            }
        }
        return s != null ? s : (c != null ? c.toString() : null);
    }

    private static AttributeSet getHardcodedDefaultColoring() {
        if (hardCodedDefaultColoring == null) {
            Integer customFontSize;
            int defaultFontSize = GraphicsEnvironment.isHeadless() ? 13 : ((customFontSize = (Integer)UIManager.get("customFontSize")) != null ? customFontSize : (UIManager.getFont("EditorPane.font") != null ? UIManager.getFont("EditorPane.font").getSize() : 13));
            hardCodedDefaultColoring = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.NameAttribute, "default", StyleConstants.Foreground, Color.black, StyleConstants.Background, Color.white, StyleConstants.FontFamily, "Monospaced", StyleConstants.FontSize, defaultFontSize});
        }
        assert (hardCodedDefaultColoring != null);
        return hardCodedDefaultColoring;
    }
}

