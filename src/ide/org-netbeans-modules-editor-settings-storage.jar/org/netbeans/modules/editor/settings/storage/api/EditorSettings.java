/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings.storage.api;

import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import javax.swing.text.AttributeSet;
import org.netbeans.modules.editor.settings.storage.EditorSettingsImpl;
import org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory;
import org.netbeans.modules.editor.settings.storage.api.KeyBindingSettingsFactory;

public abstract class EditorSettings {
    public static final String PROP_CURRENT_FONT_COLOR_PROFILE = "currentFontColorProfile";
    public static final String PROP_DEFAULT_FONT_COLORS = "defaultFontColors";
    public static final String PROP_EDITOR_FONT_COLORS = EditorSettingsImpl.PROP_EDITOR_FONT_COLORS;
    public static final String PROP_MIME_TYPES = "mime-types";
    public static final String PROP_CURRENT_KEY_MAP_PROFILE = "currentKeyMapProfile";

    public static EditorSettings getDefault() {
        return EditorSettingsImpl.getInstance();
    }

    public abstract Set<String> getAllMimeTypes();

    public abstract Set<String> getMimeTypes();

    public abstract String getLanguageName(String var1);

    public abstract Set<String> getFontColorProfiles();

    public abstract boolean isCustomFontColorProfile(String var1);

    public abstract String getCurrentFontColorProfile();

    public abstract void setCurrentFontColorProfile(String var1);

    public abstract Collection<AttributeSet> getDefaultFontColors(String var1);

    public abstract Collection<AttributeSet> getDefaultFontColorDefaults(String var1);

    public abstract void setDefaultFontColors(String var1, Collection<AttributeSet> var2);

    public abstract Map<String, AttributeSet> getHighlightings(String var1);

    public abstract Map<String, AttributeSet> getHighlightingDefaults(String var1);

    public abstract void setHighlightings(String var1, Map<String, AttributeSet> var2);

    public abstract Map<String, AttributeSet> getAnnotations(String var1);

    public abstract Map<String, AttributeSet> getAnnotationDefaults(String var1);

    public abstract void setAnnotations(String var1, Map<String, AttributeSet> var2);

    public abstract FontColorSettingsFactory getFontColorSettings(String[] var1);

    public abstract KeyBindingSettingsFactory getKeyBindingSettings(String[] var1);

    public abstract Set<String> getKeyMapProfiles();

    public abstract boolean isCustomKeymapProfile(String var1);

    public abstract String getCurrentKeyMapProfile();

    public abstract void setCurrentKeyMapProfile(String var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    public abstract void addPropertyChangeListener(String var1, PropertyChangeListener var2);

    public abstract void removePropertyChangeListener(String var1, PropertyChangeListener var2);
}

