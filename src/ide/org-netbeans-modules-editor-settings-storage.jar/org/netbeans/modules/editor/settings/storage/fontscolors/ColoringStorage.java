/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.modules.editor.settings.storage.fontscolors;

import java.awt.Color;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.editor.settings.storage.SettingsType;
import org.netbeans.modules.editor.settings.storage.StorageImpl;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.xml.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public final class ColoringStorage
implements StorageDescription<String, AttributeSet>,
StorageImpl.Operations<String, AttributeSet> {
    private static final Logger LOG = Logger.getLogger(ColoringStorage.class.getName());
    public static final String ID = "FontsColors";
    static final String MIME_TYPE = "text/x-nbeditor-fontcolorsettings";
    private static final String HIGHLIGHTING_FILE_NAME = "editorColoring.xml";
    private static final String E_ROOT = "fontscolors";
    private static final String E_FONTCOLOR = "fontcolor";
    private static final String E_FONT = "font";
    private static final String A_NAME = "name";
    private static final String A_FOREGROUND = "foreColor";
    private static final String A_BACKGROUND = "bgColor";
    private static final String A_STRIKETHROUGH = "strikeThrough";
    private static final String A_WAVEUNDERLINE = "waveUnderlined";
    private static final String A_UNDERLINE = "underline";
    private static final String A_DEFAULT = "default";
    private static final String A_SIZE = "size";
    private static final String A_STYLE = "style";
    private static final String V_BOLD_ITALIC = "bold+italic";
    private static final String V_BOLD = "bold";
    private static final String V_ITALIC = "italic";
    private static final String V_PLAIN = "plain";
    private static final String PUBLIC_ID = "-//NetBeans//DTD Editor Fonts and Colors settings 1.1//EN";
    private static final String SYSTEM_ID = "http://www.netbeans.org/dtds/EditorFontsColors-1_1.dtd";
    private static final String FA_TYPE = "nbeditor-settings-ColoringType";
    public static final String FAV_TOKEN = "token";
    public static final String FAV_HIGHLIGHT = "highlight";
    public static final String FAV_ANNOTATION = "annotation";
    private static final Object ATTR_MODULE_SUPPLIED = new Object();
    private final String type;
    private static final Map<Color, String> colorToName = new HashMap<Color, String>();
    private static final Map<String, Color> nameToColor = new HashMap<String, Color>();

    public ColoringStorage(String type) {
        this.type = type;
    }

    public ColoringStorage() {
        this("token");
    }

    @Override
    public String getId() {
        return "FontsColors";
    }

    @Override
    public boolean isUsingProfiles() {
        return true;
    }

    @Override
    public String getMimeType() {
        return "text/x-nbeditor-fontcolorsettings";
    }

    @Override
    public String getLegacyFileName() {
        return null;
    }

    @Override
    public StorageReader<String, AttributeSet> createReader(FileObject f, String mimePath) {
        throw new UnsupportedOperationException("Should not be called.");
    }

    @Override
    public StorageWriter<String, AttributeSet> createWriter(FileObject f, String mimePath) {
        throw new UnsupportedOperationException("Should not be called.");
    }

    @Override
    public Map<String, AttributeSet> load(MimePath mimePath, String profile, boolean defaults) {
        List filesForLocalization2;
        assert (mimePath != null);
        assert (profile != null);
        FileObject baseFolder = FileUtil.getConfigFile((String)"Editors");
        HashMap<String, List<Object[]>> files = new HashMap<String, List<Object[]>>();
        SettingsType.Locator locator = SettingsType.getLocator(this);
        locator.scan(baseFolder, mimePath.getPath(), profile, true, true, !defaults, false, files);
        assert (files.size() <= 1);
        List<Object[]> profileInfos = files.get(profile);
        if (profileInfos == null) {
            return Collections.emptyMap();
        }
        if (!profile.equals("NetBeans")) {
            HashMap<String, List<Object[]>> defaultProfileModulesFiles = new HashMap<String, List<Object[]>>();
            locator.scan(baseFolder, mimePath.getPath(), "NetBeans", true, true, false, false, defaultProfileModulesFiles);
            filesForLocalization2 = defaultProfileModulesFiles.get("NetBeans");
            if (filesForLocalization2 == null) {
                filesForLocalization2 = Collections.emptyList();
            }
        } else {
            filesForLocalization2 = profileInfos;
        }
        HashMap<String, SimpleAttributeSet> fontsColorsMap = new HashMap<String, SimpleAttributeSet>();
        for (Object[] info : profileInfos) {
            assert (info.length == 5);
            FileObject profileHome = (FileObject)info[0];
            FileObject settingFile = (FileObject)info[1];
            boolean modulesFile = (Boolean)info[2];
            boolean legacyFile = (Boolean)info[4];
            if (!this.type.equals(ColoringStorage.getColoringFileType(settingFile))) continue;
            ColoringsReader reader = new ColoringsReader(settingFile, mimePath.getPath());
            Utils.load(settingFile, reader, !legacyFile);
            Map<String, SimpleAttributeSet> sets = reader.getAdded();
            for (SimpleAttributeSet as : sets.values()) {
                int idx;
                if ("annotation".equals(this.type)) {
                    fontsColorsMap.put((String)as.getAttribute(StyleConstants.NameAttribute), as);
                    continue;
                }
                String name = (String)as.getAttribute(StyleConstants.NameAttribute);
                String translatedName = null;
                SimpleAttributeSet previous = (SimpleAttributeSet)fontsColorsMap.get(name);
                if (previous == null && !modulesFile && "token".equals(this.type) && (idx = name.indexOf(45)) != -1 && (previous = (SimpleAttributeSet)fontsColorsMap.get(translatedName = name.substring(idx + 1))) != null) {
                    as.addAttribute(StyleConstants.NameAttribute, translatedName);
                    name = translatedName;
                }
                if (previous == null) {
                    List filesForLocalization2;
                    String displayName = ColoringStorage.findDisplayName(name, settingFile, filesForLocalization2);
                    if (displayName == null && !modulesFile) {
                        if (translatedName != null) {
                            displayName = ColoringStorage.findDisplayName(translatedName, settingFile, filesForLocalization2);
                        }
                        if (displayName == null) {
                            if (!LOG.isLoggable(Level.FINE)) continue;
                            LOG.fine("Ignoring an extra coloring '" + name + "' that was not defined by modules.");
                            continue;
                        }
                        as.addAttribute(StyleConstants.NameAttribute, translatedName);
                        name = translatedName;
                    }
                    if (displayName == null) {
                        displayName = name;
                    }
                    as.addAttribute(EditorStyleConstants.DisplayName, displayName);
                    as.addAttribute(ATTR_MODULE_SUPPLIED, modulesFile);
                    fontsColorsMap.put(name, as);
                    continue;
                }
                boolean moduleSupplied = (Boolean)previous.getAttribute(ATTR_MODULE_SUPPLIED);
                if (moduleSupplied == modulesFile) {
                    ColoringStorage.mergeAttributeSets(previous, as);
                    continue;
                }
                as.addAttribute(EditorStyleConstants.DisplayName, previous.getAttribute(EditorStyleConstants.DisplayName));
                Object df = previous.getAttribute(EditorStyleConstants.Default);
                if (df != null) {
                    as.addAttribute(EditorStyleConstants.Default, df);
                }
                as.addAttribute(ATTR_MODULE_SUPPLIED, modulesFile);
                fontsColorsMap.put(name, as);
            }
        }
        return Utils.immutize(fontsColorsMap, ATTR_MODULE_SUPPLIED);
    }

    @Override
    public boolean save(MimePath mimePath, String profile, boolean defaults, final Map<String, AttributeSet> fontColors, final Map<String, AttributeSet> defaultFontColors) throws IOException {
        assert (mimePath != null);
        assert (profile != null);
        final String settingFileName = SettingsType.getLocator(this).getWritableFileName(mimePath.getPath(), profile, "token".equals(this.type) ? "-tokenColorings" : ("highlight".equals(this.type) ? "-highlights" : "-annotations"), defaults);
        FileUtil.runAtomicAction((FileSystem.AtomicAction)new FileSystem.AtomicAction(){

            public void run() throws IOException {
                FileObject baseFolder = FileUtil.getConfigFile((String)"Editors");
                FileObject f = FileUtil.createData((FileObject)baseFolder, (String)settingFileName);
                f.setAttribute("nbeditor-settings-ColoringType", (Object)ColoringStorage.this.type);
                HashMap added = new HashMap();
                HashMap removed = new HashMap();
                Utils.diff(defaultFontColors, fontColors, added, removed);
                ColoringsWriter writer = new ColoringsWriter();
                writer.setAdded(added);
                writer.setRemoved(removed.keySet());
                Utils.save(f, writer);
            }
        });
        return true;
    }

    @Override
    public void delete(MimePath mimePath, String profile, boolean defaults) throws IOException {
        assert (mimePath != null);
        assert (profile != null);
        FileObject baseFolder = FileUtil.getConfigFile((String)"Editors");
        HashMap<String, List<Object[]>> files = new HashMap<String, List<Object[]>>();
        SettingsType.getLocator(this).scan(baseFolder, mimePath.getPath(), profile, true, defaults, !defaults, false, files);
        assert (files.size() <= 1);
        final List<Object[]> profileInfos = files.get(profile);
        if (profileInfos != null) {
            FileUtil.runAtomicAction((FileSystem.AtomicAction)new FileSystem.AtomicAction(){

                public void run() throws IOException {
                    for (Object[] info : profileInfos) {
                        FileObject settingFile = (FileObject)info[1];
                        if (!ColoringStorage.this.type.equals(ColoringStorage.getColoringFileType(settingFile))) continue;
                        settingFile.delete();
                    }
                }
            });
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static String findDisplayName(String name, FileObject settingFile, List<Object[]> filesForLocalization) {
        FileObject locFile;
        Object[] locFileInfo;
        String displayName = Utils.getLocalizedName(settingFile, name, null, true);
        if (displayName != null) return displayName;
        Iterator<Object[]> i$ = filesForLocalization.iterator();
        while (i$.hasNext() && (displayName = Utils.getLocalizedName(locFile = (FileObject)(locFileInfo = i$.next())[1], name, null, true)) == null) {
        }
        return displayName;
    }

    private static void mergeAttributeSets(SimpleAttributeSet original, AttributeSet toMerge) {
        Enumeration names = toMerge.getAttributeNames();
        while (names.hasMoreElements()) {
            Object key = names.nextElement();
            Object value = toMerge.getAttribute(key);
            original.addAttribute(key, value);
        }
    }

    private static String getColoringFileType(FileObject f) {
        Object typeValue = f.getAttribute("nbeditor-settings-ColoringType");
        if (typeValue instanceof String) {
            return (String)typeValue;
        }
        if (f.getNameExt().equals("editorColoring.xml")) {
            return "highlight";
        }
        return "token";
    }

    private static String colorToString(Color color) {
        if (colorToName.containsKey(color)) {
            return colorToName.get(color);
        }
        return Integer.toHexString(color.getRGB());
    }

    private static Color stringToColor(String color) throws Exception {
        if (nameToColor.containsKey(color)) {
            return nameToColor.get(color);
        }
        try {
            return new Color((int)Long.parseLong(color, 16), color.length() != 6);
        }
        catch (NumberFormatException ex) {
            throw new Exception();
        }
    }

    static {
        colorToName.put(Color.black, "black");
        nameToColor.put("black", Color.black);
        colorToName.put(Color.blue, "blue");
        nameToColor.put("blue", Color.blue);
        colorToName.put(Color.cyan, "cyan");
        nameToColor.put("cyan", Color.cyan);
        colorToName.put(Color.darkGray, "darkGray");
        nameToColor.put("darkGray", Color.darkGray);
        colorToName.put(Color.gray, "gray");
        nameToColor.put("gray", Color.gray);
        colorToName.put(Color.green, "green");
        nameToColor.put("green", Color.green);
        colorToName.put(Color.lightGray, "lightGray");
        nameToColor.put("lightGray", Color.lightGray);
        colorToName.put(Color.magenta, "magenta");
        nameToColor.put("magenta", Color.magenta);
        colorToName.put(Color.orange, "orange");
        nameToColor.put("orange", Color.orange);
        colorToName.put(Color.pink, "pink");
        nameToColor.put("pink", Color.pink);
        colorToName.put(Color.red, "red");
        nameToColor.put("red", Color.red);
        colorToName.put(Color.white, "white");
        nameToColor.put("white", Color.white);
        colorToName.put(Color.yellow, "yellow");
        nameToColor.put("yellow", Color.yellow);
    }

    private static final class ColoringsWriter
    extends StorageWriter<String, AttributeSet> {
        @Override
        public Document getDocument() {
            Document doc = XMLUtil.createDocument((String)"fontscolors", (String)null, (String)"-//NetBeans//DTD Editor Fonts and Colors settings 1.1//EN", (String)"http://www.netbeans.org/dtds/EditorFontsColors-1_1.dtd");
            Node root = doc.getElementsByTagName("fontscolors").item(0);
            for (AttributeSet category : this.getAdded().values()) {
                Element fontColor = doc.createElement("fontcolor");
                root.appendChild(fontColor);
                fontColor.setAttribute("name", (String)category.getAttribute(StyleConstants.NameAttribute));
                if (category.isDefined(StyleConstants.Foreground)) {
                    fontColor.setAttribute("foreColor", ColoringStorage.colorToString((Color)category.getAttribute(StyleConstants.Foreground)));
                }
                if (category.isDefined(StyleConstants.Background)) {
                    fontColor.setAttribute("bgColor", ColoringStorage.colorToString((Color)category.getAttribute(StyleConstants.Background)));
                }
                if (category.isDefined(StyleConstants.StrikeThrough)) {
                    fontColor.setAttribute("strikeThrough", ColoringStorage.colorToString((Color)category.getAttribute(StyleConstants.StrikeThrough)));
                }
                if (category.isDefined(EditorStyleConstants.WaveUnderlineColor)) {
                    fontColor.setAttribute("waveUnderlined", ColoringStorage.colorToString((Color)category.getAttribute(EditorStyleConstants.WaveUnderlineColor)));
                }
                if (category.isDefined(StyleConstants.Underline)) {
                    fontColor.setAttribute("underline", ColoringStorage.colorToString((Color)category.getAttribute(StyleConstants.Underline)));
                }
                if (category.isDefined(EditorStyleConstants.Default)) {
                    fontColor.setAttribute("default", (String)category.getAttribute(EditorStyleConstants.Default));
                }
                if (!category.isDefined(StyleConstants.FontFamily) && !category.isDefined(StyleConstants.FontSize) && !category.isDefined(StyleConstants.Bold) && !category.isDefined(StyleConstants.Italic)) continue;
                Element font = doc.createElement("font");
                fontColor.appendChild(font);
                if (category.isDefined(StyleConstants.FontFamily)) {
                    font.setAttribute("name", (String)category.getAttribute(StyleConstants.FontFamily));
                }
                if (category.isDefined(StyleConstants.FontSize)) {
                    font.setAttribute("size", ((Integer)category.getAttribute(StyleConstants.FontSize)).toString());
                }
                if (!category.isDefined(StyleConstants.Bold) && !category.isDefined(StyleConstants.Italic)) continue;
                Boolean bold = Boolean.FALSE;
                Boolean italic = Boolean.FALSE;
                if (category.isDefined(StyleConstants.Bold)) {
                    bold = (Boolean)category.getAttribute(StyleConstants.Bold);
                }
                if (category.isDefined(StyleConstants.Italic)) {
                    italic = (Boolean)category.getAttribute(StyleConstants.Italic);
                }
                font.setAttribute("style", bold.booleanValue() ? (italic.booleanValue() ? "bold+italic" : "bold") : (italic != false ? "italic" : "plain"));
            }
            return doc;
        }
    }

    private static class ColoringsReader
    extends StorageReader<String, SimpleAttributeSet> {
        private final Map<String, SimpleAttributeSet> colorings = new HashMap<String, SimpleAttributeSet>();
        private SimpleAttributeSet attribs = null;

        public ColoringsReader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        @Override
        public Map<String, SimpleAttributeSet> getAdded() {
            return this.colorings;
        }

        @Override
        public Set<String> getRemoved() {
            return Collections.emptySet();
        }

        @Override
        public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
            block16 : {
                try {
                    if (name.equals("fontscolors")) break block16;
                    if (name.equals("fontcolor")) {
                        assert (this.attribs == null);
                        this.attribs = new SimpleAttributeSet();
                        String nameAttributeValue = attributes.getValue("name");
                        this.attribs.addAttribute(StyleConstants.NameAttribute, nameAttributeValue);
                        String value = attributes.getValue("bgColor");
                        if (value != null) {
                            this.attribs.addAttribute(StyleConstants.Background, ColoringStorage.stringToColor(value));
                        }
                        if ((value = attributes.getValue("foreColor")) != null) {
                            this.attribs.addAttribute(StyleConstants.Foreground, ColoringStorage.stringToColor(value));
                        }
                        if ((value = attributes.getValue("underline")) != null) {
                            this.attribs.addAttribute(StyleConstants.Underline, ColoringStorage.stringToColor(value));
                        }
                        if ((value = attributes.getValue("strikeThrough")) != null) {
                            this.attribs.addAttribute(StyleConstants.StrikeThrough, ColoringStorage.stringToColor(value));
                        }
                        if ((value = attributes.getValue("waveUnderlined")) != null) {
                            this.attribs.addAttribute(EditorStyleConstants.WaveUnderlineColor, ColoringStorage.stringToColor(value));
                        }
                        if ((value = attributes.getValue("default")) != null) {
                            this.attribs.addAttribute(EditorStyleConstants.Default, value);
                        }
                        this.colorings.put(nameAttributeValue, this.attribs);
                        break block16;
                    }
                    if (!name.equals("font")) break block16;
                    assert (this.attribs != null);
                    String value = attributes.getValue("name");
                    if (value != null) {
                        this.attribs.addAttribute(StyleConstants.FontFamily, value);
                    }
                    if ((value = attributes.getValue("size")) != null) {
                        try {
                            this.attribs.addAttribute(StyleConstants.FontSize, Integer.decode(value));
                        }
                        catch (NumberFormatException ex) {
                            LOG.log(Level.WARNING, value + " is not a valid Integer; parsing attribute " + "size" + this.getProcessedFile().getPath(), ex);
                        }
                    }
                    if ((value = attributes.getValue("style")) != null) {
                        this.attribs.addAttribute(StyleConstants.Bold, value.indexOf("bold") >= 0);
                        this.attribs.addAttribute(StyleConstants.Italic, value.indexOf("italic") >= 0);
                    }
                }
                catch (Exception ex) {
                    LOG.log(Level.WARNING, "Can't parse colorings file " + this.getProcessedFile().getPath(), ex);
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String name) throws SAXException {
            if (name.equals("fontcolor")) {
                this.attribs = null;
            }
        }
    }

}

