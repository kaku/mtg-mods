/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage.preferences;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeEvent;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.xml.bind.DatatypeConverter;
import org.netbeans.modules.editor.settings.storage.api.OverridePreferences;
import org.netbeans.modules.editor.settings.storage.preferences.InheritedPreferences;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;
import org.openide.util.WeakListeners;

public final class ProxyPreferencesImpl
extends Preferences
implements PreferenceChangeListener,
NodeChangeListener,
OverridePreferences {
    private Preferences inheritedPrefs;
    private static final Logger LOG = Logger.getLogger(ProxyPreferencesImpl.class.getName());
    private final ProxyPreferencesImpl parent;
    private final String name;
    private Preferences delegate;
    private final Tree tree;
    private boolean removed;
    private final Map<String, TypedValue> data = new HashMap<String, TypedValue>();
    private final Set<String> removedKeys = new HashSet<String>();
    private final Map<String, ProxyPreferencesImpl> children = new HashMap<String, ProxyPreferencesImpl>();
    private final Set<String> removedChildren = new HashSet<String>();
    private boolean noEvents = false;
    private PreferenceChangeListener weakPrefListener;
    private final Set<PreferenceChangeListener> prefListeners = new HashSet<PreferenceChangeListener>();
    private NodeChangeListener weakNodeListener;
    private final Set<NodeChangeListener> nodeListeners = new HashSet<NodeChangeListener>();

    public static ProxyPreferencesImpl getProxyPreferences(Object token, Preferences delegate) {
        return Tree.getTree(token, delegate).get(null, delegate.name(), delegate);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isDirty() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            return !this.data.isEmpty() || !this.removedKeys.isEmpty() || !this.children.isEmpty() || !this.removedChildren.isEmpty() || this.removed;
        }
    }

    @Override
    public void put(String key, String value) {
        this._put(key, value, String.class.getName());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String get(String key, String def) {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkNotNull(key, "key");
            this.checkRemoved();
            if (this.removedKeys.contains(key)) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Key '" + key + "' removed, using default '" + def + "'");
                }
                if (this.inheritedPrefs != null) {
                    return this.inheritedPrefs.get(key, def);
                }
                return def;
            }
            TypedValue typedValue = this.data.get(key);
            if (typedValue != null) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Key '" + key + "' modified, local value '" + typedValue.getValue() + "'");
                }
                return typedValue.getValue();
            }
            if (this.delegate != null) {
                String value = this.delegate.get(key, def);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Key '" + key + "' undefined, original value '" + value + "'");
                }
                return value;
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Key '" + key + "' undefined, '" + this.name + "' is a new node, using default '" + def + "'");
            }
            return def;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void remove(String key) {
        EventBag<PreferenceChangeListener, PreferenceChangeEvent> bag = null;
        if (this.get(key, null) == null) {
            return;
        }
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkNotNull(key, "key");
            this.checkRemoved();
            if (this.removedKeys.add(key)) {
                this.data.remove(key);
                bag = new EventBag<PreferenceChangeListener, PreferenceChangeEvent>();
                bag.addListeners(this.prefListeners);
                if (this.inheritedPrefs != null) {
                    bag.addEvent(new PreferenceChangeEvent(this, key, this.inheritedPrefs.get(key, null)));
                } else {
                    bag.addEvent(new PreferenceChangeEvent(this, key, null));
                }
            }
        }
        if (bag != null) {
            this.firePrefEvents(Collections.singletonList(bag));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void clear() throws BackingStoreException {
        EventBag<PreferenceChangeListener, PreferenceChangeEvent> bag = new EventBag<PreferenceChangeListener, PreferenceChangeEvent>();
        Object object = this.tree.treeLock();
        synchronized (object) {
            String value;
            this.checkRemoved();
            HashSet<String> keys = new HashSet<String>();
            keys.addAll(this.data.keySet());
            keys.removeAll(this.removedKeys);
            if (!keys.isEmpty()) {
                for (String key : keys) {
                    value = this.delegate == null ? null : this.delegate.get(key, null);
                    bag.addEvent(new PreferenceChangeEvent(this, key, value));
                }
            }
            if (this.delegate != null) {
                for (String key : this.removedKeys) {
                    value = this.delegate.get(key, null);
                    if (value == null) continue;
                    bag.addEvent(new PreferenceChangeEvent(this, key, value));
                }
            }
            bag.addListeners(this.prefListeners);
            this.data.clear();
            this.removedKeys.clear();
        }
        this.firePrefEvents(Collections.singletonList(bag));
    }

    @Override
    public void putInt(String key, int value) {
        this._put(key, Integer.toString(value), Integer.class.getName());
    }

    @Override
    public int getInt(String key, int def) {
        String value = this.get(key, null);
        if (value != null) {
            try {
                return Integer.parseInt(value);
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        return def;
    }

    @Override
    public void putLong(String key, long value) {
        this._put(key, Long.toString(value), Long.class.getName());
    }

    @Override
    public long getLong(String key, long def) {
        String value = this.get(key, null);
        if (value != null) {
            try {
                return Long.parseLong(value);
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        return def;
    }

    @Override
    public void putBoolean(String key, boolean value) {
        this._put(key, Boolean.toString(value), Boolean.class.getName());
    }

    @Override
    public boolean getBoolean(String key, boolean def) {
        String value = this.get(key, null);
        if (value != null) {
            return Boolean.parseBoolean(value);
        }
        return def;
    }

    @Override
    public void putFloat(String key, float value) {
        this._put(key, Float.toString(value), Float.class.getName());
    }

    @Override
    public float getFloat(String key, float def) {
        String value = this.get(key, null);
        if (value != null) {
            try {
                return Float.parseFloat(value);
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        return def;
    }

    @Override
    public void putDouble(String key, double value) {
        this._put(key, Double.toString(value), Double.class.getName());
    }

    @Override
    public double getDouble(String key, double def) {
        String value = this.get(key, null);
        if (value != null) {
            try {
                return Double.parseDouble(value);
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        return def;
    }

    @Override
    public void putByteArray(String key, byte[] value) {
        this._put(key, DatatypeConverter.printBase64Binary(value), value.getClass().getName());
    }

    @Override
    public byte[] getByteArray(String key, byte[] def) {
        byte[] decoded;
        String value = this.get(key, null);
        if (value != null && (decoded = DatatypeConverter.parseBase64Binary(value)) != null) {
            return decoded;
        }
        return def;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String[] keys() throws BackingStoreException {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkRemoved();
            HashSet<String> keys = new HashSet<String>();
            if (this.delegate != null) {
                keys.addAll(Arrays.asList(this.delegate.keys()));
            }
            keys.addAll(this.data.keySet());
            keys.removeAll(this.removedKeys);
            return keys.toArray(new String[keys.size()]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String[] childrenNames() throws BackingStoreException {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkRemoved();
            HashSet<String> names = new HashSet<String>();
            if (this.delegate != null) {
                names.addAll(Arrays.asList(this.delegate.childrenNames()));
            }
            names.addAll(this.children.keySet());
            names.removeAll(this.removedChildren);
            return names.toArray(new String[names.size()]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Preferences parent() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkRemoved();
            return this.parent;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Preferences node(String pathName) {
        ProxyPreferencesImpl node;
        LinkedList<EventBag<NodeChangeListener, NodeChangeEvent>> events = new LinkedList<EventBag<NodeChangeListener, NodeChangeEvent>>();
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkNotNull(pathName, "pathName");
            this.checkRemoved();
            node = this.node(pathName, true, events);
        }
        this.fireNodeEvents(events);
        return node;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean nodeExists(String pathName) throws BackingStoreException {
        Object object = this.tree.treeLock();
        synchronized (object) {
            if (pathName.length() == 0) {
                return !this.removed;
            }
            this.checkRemoved();
            return this.node(pathName, false, null) != null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeNode() throws BackingStoreException {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkRemoved();
            ProxyPreferencesImpl p = this.parent;
            if (p == null) {
                throw new UnsupportedOperationException("Can't remove the root.");
            }
            p.removeChild(this);
        }
    }

    @Override
    public String name() {
        return this.name;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String absolutePath() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            ProxyPreferencesImpl pp = this.parent;
            if (pp != null) {
                if (pp.parent == null) {
                    return "/" + this.name();
                }
                return pp.absolutePath() + "/" + this.name();
            }
            return "/";
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isUserNode() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            if (this.delegate != null) {
                return this.delegate.isUserNode();
            }
            ProxyPreferencesImpl pp = this.parent;
            if (pp != null) {
                return pp.isUserNode();
            }
            return true;
        }
    }

    @Override
    public String toString() {
        return (this.isUserNode() ? "User" : "System") + " Preference Node: " + this.absolutePath();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void flush() throws BackingStoreException {
        Object object = this.tree.treeLock();
        synchronized (object) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Flushing " + this.absolutePath());
            }
            this.checkRemoved();
            for (ProxyPreferencesImpl pp : this.children.values()) {
                pp.flush();
            }
            if (this.delegate == null) {
                ProxyPreferencesImpl proxyRoot = this.parent.node("/", false, null);
                assert (proxyRoot != null);
                Preferences delegateRoot = proxyRoot.delegate;
                assert (delegateRoot != null);
                Preferences nueDelegate = delegateRoot.node(this.absolutePath());
                this.changeDelegate(nueDelegate);
            }
            this.delegate.removeNodeChangeListener(this.weakNodeListener);
            this.delegate.removePreferenceChangeListener(this.weakPrefListener);
            try {
                for (String childName : this.removedChildren) {
                    if (!this.delegate.nodeExists(childName)) continue;
                    this.delegate.node(childName).removeNode();
                }
                for (String key2 : this.data.keySet()) {
                    if (this.removedKeys.contains(key2)) continue;
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Writing " + this.absolutePath() + "/" + key2 + "=" + this.data.get(key2));
                    }
                    TypedValue typedValue = this.data.get(key2);
                    if (String.class.getName().equals(typedValue.getJavaType())) {
                        this.delegate.put(key2, typedValue.getValue());
                        continue;
                    }
                    if (Integer.class.getName().equals(typedValue.getJavaType())) {
                        this.delegate.putInt(key2, Integer.parseInt(typedValue.getValue()));
                        continue;
                    }
                    if (Long.class.getName().equals(typedValue.getJavaType())) {
                        this.delegate.putLong(key2, Long.parseLong(typedValue.getValue()));
                        continue;
                    }
                    if (Boolean.class.getName().equals(typedValue.getJavaType())) {
                        this.delegate.putBoolean(key2, Boolean.parseBoolean(typedValue.getValue()));
                        continue;
                    }
                    if (Float.class.getName().equals(typedValue.getJavaType())) {
                        this.delegate.putFloat(key2, Float.parseFloat(typedValue.getValue()));
                        continue;
                    }
                    if (Double.class.getName().equals(typedValue.getJavaType())) {
                        this.delegate.putDouble(key2, Double.parseDouble(typedValue.getValue()));
                        continue;
                    }
                    this.delegate.putByteArray(key2, DatatypeConverter.parseBase64Binary(typedValue.getValue()));
                }
                this.data.clear();
                for (String key2 : this.removedKeys) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Removing " + this.absolutePath() + "/" + key2);
                    }
                    this.delegate.remove(key2);
                }
                this.removedKeys.clear();
            }
            finally {
                this.delegate.addNodeChangeListener(this.weakNodeListener);
                this.delegate.addPreferenceChangeListener(this.weakPrefListener);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void sync() throws BackingStoreException {
        ArrayList<EventBag<PreferenceChangeListener, PreferenceChangeEvent>> prefEvents = new ArrayList<EventBag<PreferenceChangeListener, PreferenceChangeEvent>>();
        ArrayList<EventBag<NodeChangeListener, NodeChangeEvent>> nodeEvents = new ArrayList<EventBag<NodeChangeListener, NodeChangeEvent>>();
        Object object = this.tree.treeLock();
        synchronized (object) {
            this._sync(prefEvents, nodeEvents);
        }
        this.fireNodeEvents(nodeEvents);
        this.firePrefEvents(prefEvents);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addPreferenceChangeListener(PreferenceChangeListener pcl) {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.prefListeners.add(pcl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removePreferenceChangeListener(PreferenceChangeListener pcl) {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.prefListeners.remove(pcl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addNodeChangeListener(NodeChangeListener ncl) {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.nodeListeners.add(ncl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeNodeChangeListener(NodeChangeListener ncl) {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.nodeListeners.remove(ncl);
        }
    }

    @Override
    public void exportNode(OutputStream os) throws IOException, BackingStoreException {
        throw new UnsupportedOperationException("exportNode not supported");
    }

    @Override
    public void exportSubtree(OutputStream os) throws IOException, BackingStoreException {
        throw new UnsupportedOperationException("exportSubtree not supported");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        PreferenceChangeListener[] listeners;
        String nValue = evt.getNewValue();
        String k = evt.getKey();
        Object object = this.tree.treeLock();
        synchronized (object) {
            if (this.removed || this.data.containsKey(k)) {
                return;
            }
            if (this.removedKeys.contains(k)) {
                if (this.inheritedPrefs == null) {
                    return;
                }
                nValue = this.inheritedPrefs.get(k, null);
                if (nValue == null) {
                    return;
                }
            }
            listeners = this.prefListeners.toArray(new PreferenceChangeListener[this.prefListeners.size()]);
        }
        PreferenceChangeEvent myEvt = null;
        for (PreferenceChangeListener l : listeners) {
            if (myEvt == null) {
                myEvt = new PreferenceChangeEvent(this, k, nValue);
            }
            l.preferenceChange(myEvt);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void childAdded(NodeChangeEvent evt) {
        NodeChangeListener[] listeners;
        Preferences childNode;
        Object object = this.tree.treeLock();
        synchronized (object) {
            String childName = evt.getChild().name();
            if (this.removed || this.removedChildren.contains(childName)) {
                return;
            }
            childNode = this.children.get(childName);
            if (childNode != null) {
                ((ProxyPreferencesImpl)childNode).changeDelegate(evt.getChild());
            } else {
                childNode = this.node(evt.getChild().name());
            }
            listeners = this.nodeListeners.toArray(new NodeChangeListener[this.nodeListeners.size()]);
        }
        NodeChangeEvent myEvt = null;
        for (NodeChangeListener l : listeners) {
            if (myEvt == null) {
                myEvt = new NodeChangeEvent(this, childNode);
            }
            l.childAdded(evt);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void childRemoved(NodeChangeEvent evt) {
        NodeChangeListener[] listeners;
        Preferences childNode;
        Object object = this.tree.treeLock();
        synchronized (object) {
            String childName = evt.getChild().name();
            if (this.removed || this.removedChildren.contains(childName)) {
                return;
            }
            childNode = this.children.get(childName);
            if (childNode == null) {
                return;
            }
            ((ProxyPreferencesImpl)childNode).changeDelegate(null);
            listeners = this.nodeListeners.toArray(new NodeChangeListener[this.nodeListeners.size()]);
        }
        NodeChangeEvent myEvt = null;
        for (NodeChangeListener l : listeners) {
            if (myEvt == null) {
                myEvt = new NodeChangeEvent(this, childNode);
            }
            l.childAdded(evt);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void destroy() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.tree.destroy();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void silence() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.noEvents = true;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void noise() {
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.noEvents = false;
        }
    }

    @Override
    public boolean isOverriden(String key) {
        if (this.data.containsKey(key)) {
            return true;
        }
        if (this.delegate instanceof OverridePreferences) {
            return ((OverridePreferences)((Object)this.delegate)).isOverriden(key) && !this.removedKeys.contains(key);
        }
        return false;
    }

    private ProxyPreferencesImpl(ProxyPreferencesImpl parent, String name, Preferences delegate, Tree tree) {
        assert (name != null);
        this.parent = parent;
        this.name = name;
        this.delegate = delegate;
        if (delegate instanceof InheritedPreferences) {
            this.inheritedPrefs = ((InheritedPreferences)delegate).getParent();
        }
        if (delegate != null) {
            assert (name.equals(delegate.name()));
            this.weakPrefListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)delegate);
            delegate.addPreferenceChangeListener(this.weakPrefListener);
            this.weakNodeListener = (NodeChangeListener)WeakListeners.create(NodeChangeListener.class, (EventListener)this, (Object)delegate);
            delegate.addNodeChangeListener(this.weakNodeListener);
        }
        this.tree = tree;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void _put(String key, String value, String javaType) {
        EventBag<PreferenceChangeListener, PreferenceChangeEvent> bag = null;
        Object object = this.tree.treeLock();
        synchronized (object) {
            this.checkNotNull(key, "key");
            this.checkNotNull(value, "value");
            this.checkRemoved();
            String orig = this.get(key, null);
            if (orig == null || !orig.equals(value)) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Overwriting '" + key + "' = '" + value + "'");
                }
                this.data.put(key, new TypedValue(value, javaType));
                this.removedKeys.remove(key);
                bag = new EventBag<PreferenceChangeListener, PreferenceChangeEvent>();
                bag.addListeners(this.prefListeners);
                bag.addEvent(new PreferenceChangeEvent(this, key, value));
            }
        }
        if (bag != null) {
            this.firePrefEvents(Collections.singletonList(bag));
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private ProxyPreferencesImpl node(String pathName, boolean create, List<EventBag<NodeChangeListener, NodeChangeEvent>> events) {
        void childName3;
        String pathFromChild;
        if (pathName.length() > 0 && pathName.charAt(0) == '/') {
            if (this.parent != null) {
                void root22;
                ProxyPreferencesImpl root22 = this;
                while (root22.parent() != null) {
                    Preferences root22 = root22.parent();
                }
                return ((ProxyPreferencesImpl)root22).node(pathName, create, events);
            }
            pathName = pathName.substring(1);
        }
        if (pathName.length() <= 0) return this;
        int idx = pathName.indexOf(47);
        if (idx != -1) {
            String childName2 = pathName.substring(0, idx);
            pathFromChild = pathName.substring(idx + 1);
        } else {
            String childName3 = pathName;
            pathFromChild = null;
        }
        ProxyPreferencesImpl child = this.children.get(childName3);
        if (child == null) {
            if (this.removedChildren.contains(childName3) && !create) {
                return null;
            }
            Preferences childDelegate = null;
            try {
                if (this.delegate != null && this.delegate.nodeExists((String)childName3)) {
                    childDelegate = this.delegate.node((String)childName3);
                }
            }
            catch (BackingStoreException bse) {
                // empty catch block
            }
            if (childDelegate == null && !create) return null;
            child = this.tree.get(this, (String)childName3, childDelegate);
            this.children.put((String)childName3, child);
            this.removedChildren.remove(childName3);
            if (childDelegate != null) return pathFromChild != null ? child.node(pathFromChild, create, events) : child;
            EventBag<NodeChangeListener, NodeChangeEventExt> bag = new EventBag<NodeChangeListener, NodeChangeEventExt>();
            bag.addListeners(this.nodeListeners);
            bag.addEvent(new NodeChangeEventExt(this, child, false));
            events.add(bag);
            return pathFromChild != null ? child.node(pathFromChild, create, events) : child;
        }
        assert (!child.removed);
        return pathFromChild != null ? child.node(pathFromChild, create, events) : child;
    }

    private void addChild(ProxyPreferencesImpl child) {
        ProxyPreferencesImpl pp = this.children.get(child.name());
        if (pp == null) {
            this.children.put(child.name(), child);
        } else assert (pp == child);
    }

    private void removeChild(ProxyPreferencesImpl child) {
        assert (child != null);
        assert (this.children.get(child.name()) == child);
        child.nodeRemoved();
        this.children.remove(child.name());
        this.removedChildren.add(child.name());
    }

    private void nodeRemoved() {
        for (ProxyPreferencesImpl pp : this.children.values()) {
            pp.nodeRemoved();
        }
        this.data.clear();
        this.removedKeys.clear();
        this.children.clear();
        this.removedChildren.clear();
        this.tree.removeNode(this);
        this.removed = true;
    }

    private void checkNotNull(Object paramValue, String paramName) {
        if (paramValue == null) {
            throw new NullPointerException("The " + paramName + " must not be null");
        }
    }

    private void checkRemoved() {
        if (this.removed) {
            throw new IllegalStateException("The node '" + this + " has already been removed.");
        }
    }

    private void changeDelegate(Preferences nueDelegate) {
        if (this.delegate != null) {
            try {
                if (this.delegate.nodeExists("")) {
                    assert (this.weakPrefListener != null);
                    assert (this.weakNodeListener != null);
                    this.delegate.removePreferenceChangeListener(this.weakPrefListener);
                    this.delegate.removeNodeChangeListener(this.weakNodeListener);
                }
            }
            catch (BackingStoreException bse) {
                LOG.log(Level.WARNING, null, bse);
            }
        }
        this.delegate = nueDelegate;
        this.weakPrefListener = null;
        this.weakNodeListener = null;
        if (this.delegate != null) {
            this.weakPrefListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.delegate);
            this.delegate.addPreferenceChangeListener(this.weakPrefListener);
            this.weakNodeListener = (NodeChangeListener)WeakListeners.create(NodeChangeListener.class, (EventListener)this, (Object)this.delegate);
            this.delegate.addNodeChangeListener(this.weakNodeListener);
        }
    }

    private void _sync(List<EventBag<PreferenceChangeListener, PreferenceChangeEvent>> prefEvents, List<EventBag<NodeChangeListener, NodeChangeEvent>> nodeEvents) {
        for (ProxyPreferencesImpl pp : this.children.values()) {
            pp._sync(prefEvents, nodeEvents);
        }
        EventBag<NodeChangeListener, NodeChangeEventExt> nodeBag = new EventBag<NodeChangeListener, NodeChangeEventExt>();
        nodeBag.addListeners(this.nodeListeners);
        for (ProxyPreferencesImpl pp2 : this.children.values()) {
            if (pp2.delegate != null) continue;
            nodeBag.addEvent(new NodeChangeEventExt(this, pp2, true));
        }
        if (!nodeBag.getEvents().isEmpty()) {
            nodeEvents.add(nodeBag);
        }
        if (this.delegate != null) {
            EventBag<PreferenceChangeListener, PreferenceChangeEvent> prefBag = new EventBag<PreferenceChangeListener, PreferenceChangeEvent>();
            prefBag.addListeners(this.prefListeners);
            prefEvents.add(prefBag);
            for (String key : this.data.keySet()) {
                prefBag.addEvent(new PreferenceChangeEvent(this, key, this.delegate.get(key, this.data.get(key).getValue())));
            }
        }
        for (NodeChangeEvent nce : nodeBag.getEvents()) {
            this.children.remove(nce.getChild().name());
        }
        this.data.clear();
    }

    private void firePrefEvents(List<EventBag<PreferenceChangeListener, PreferenceChangeEvent>> events) {
        if (this.noEvents) {
            return;
        }
        for (EventBag<PreferenceChangeListener, PreferenceChangeEvent> bag : events) {
            for (PreferenceChangeEvent event : bag.getEvents()) {
                for (PreferenceChangeListener l : bag.getListeners()) {
                    try {
                        l.preferenceChange(event);
                    }
                    catch (Throwable t) {
                        LOG.log(Level.WARNING, null, t);
                    }
                }
            }
        }
    }

    private void fireNodeEvents(List<EventBag<NodeChangeListener, NodeChangeEvent>> events) {
        if (this.noEvents) {
            return;
        }
        for (EventBag<NodeChangeListener, NodeChangeEvent> bag : events) {
            for (NodeChangeEvent event : bag.getEvents()) {
                for (NodeChangeListener l : bag.getListeners()) {
                    try {
                        if (event instanceof NodeChangeEventExt && ((NodeChangeEventExt)event).isRemovalEvent()) {
                            l.childRemoved(event);
                            continue;
                        }
                        l.childAdded(event);
                    }
                    catch (Throwable t) {
                        LOG.log(Level.WARNING, null, t);
                    }
                }
            }
        }
    }

    private static final class NodeChangeEventExt
    extends NodeChangeEvent {
        private final boolean removal;

        public NodeChangeEventExt(Preferences parent, Preferences child, boolean removal) {
            super(parent, child);
            this.removal = removal;
        }

        public boolean isRemovalEvent() {
            return this.removal;
        }
    }

    private static final class EventBag<L, E extends EventObject> {
        private final Set<L> listeners = new HashSet<L>();
        private final Set<E> events = new HashSet();

        public Set<? extends L> getListeners() {
            return this.listeners;
        }

        public Set<? extends E> getEvents() {
            return this.events;
        }

        public void addListeners(Collection<? extends L> l) {
            this.listeners.addAll(l);
        }

        public void addEvent(E event) {
            this.events.add(event);
        }
    }

    static final class Tree {
        static final Map<Object, Map<Preferences, Tree>> trees = new WeakHashMap<Object, Map<Preferences, Tree>>();
        private final Preferences root;
        private final Reference<?> tokenRef;
        private final Map<String, ProxyPreferencesImpl> nodes = new HashMap<String, ProxyPreferencesImpl>();

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public static Tree getTree(Object token, Preferences prefs) {
            Map<Object, Map<Preferences, Tree>> map = trees;
            synchronized (map) {
                Preferences root;
                Tree tree;
                Map<Preferences, Tree> forest = trees.get(token);
                if (forest == null) {
                    forest = new HashMap<Preferences, Tree>();
                    trees.put(token, forest);
                }
                if ((tree = forest.get(root = prefs.node("/"))) == null) {
                    tree = new Tree(token, root);
                    forest.put(root, tree);
                }
                return tree;
            }
        }

        private Tree(Object token, Preferences root) {
            this.root = root;
            this.tokenRef = new WeakReference<Object>(token);
        }

        public Object treeLock() {
            return this;
        }

        public ProxyPreferencesImpl get(ProxyPreferencesImpl parent, String name, Preferences delegate) {
            if (delegate != null) {
                assert (name.equals(delegate.name()));
                if (parent == null) {
                    Preferences parentDelegate = delegate.parent();
                    if (parentDelegate != null) {
                        parent = this.get(null, parentDelegate.name(), parentDelegate);
                    }
                } else assert (parent.delegate == delegate.parent());
            }
            String absolutePath = parent == null ? "/" : (parent.parent() == null ? "/" + name : parent.absolutePath() + "/" + name);
            ProxyPreferencesImpl node = this.nodes.get(absolutePath);
            if (node == null) {
                node = new ProxyPreferencesImpl(parent, name, delegate, this);
                this.nodes.put(absolutePath, node);
                if (parent != null) {
                    parent.addChild(node);
                }
            } else assert (!node.removed);
            return node;
        }

        public void removeNode(ProxyPreferencesImpl node) {
            String path = node.absolutePath();
            assert (this.nodes.containsKey(path));
            ProxyPreferencesImpl pp = this.nodes.remove(path);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void destroy() {
            Map<Object, Map<Preferences, Tree>> map = trees;
            synchronized (map) {
                Object token = this.tokenRef.get();
                if (token != null) {
                    trees.remove(token);
                }
            }
        }
    }

}

