/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings.storage.api;

import java.util.prefs.Preferences;
import org.netbeans.modules.editor.settings.storage.preferences.InheritedPreferences;
import org.netbeans.modules.editor.settings.storage.preferences.ProxyPreferencesImpl;

public final class MemoryPreferences {
    private ProxyPreferencesImpl prefInstance;

    public static MemoryPreferences get(Object token, Preferences delegate) {
        return new MemoryPreferences(ProxyPreferencesImpl.getProxyPreferences(token, delegate));
    }

    public static MemoryPreferences getWithInherited(Object token, Preferences parent, Preferences delegate) {
        if (parent == null) {
            return MemoryPreferences.get(token, delegate);
        }
        InheritedPreferences inh = new InheritedPreferences(parent, delegate);
        return new MemoryPreferences(ProxyPreferencesImpl.getProxyPreferences(token, inh));
    }

    public Preferences getPreferences() {
        return this.prefInstance;
    }

    public void destroy() {
        this.prefInstance.destroy();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runWithoutEvents(Runnable r) {
        try {
            this.prefInstance.silence();
            r.run();
        }
        finally {
            this.prefInstance.noise();
        }
    }

    public boolean isDirty(Preferences pref) {
        if (!(pref instanceof ProxyPreferencesImpl)) {
            throw new IllegalArgumentException("Incompatible PreferencesImpl");
        }
        ProxyPreferencesImpl impl = (ProxyPreferencesImpl)pref;
        if (impl.node(this.prefInstance.absolutePath()) != this.prefInstance) {
            throw new IllegalArgumentException("The preferences tree root is not reachable");
        }
        return impl.isDirty();
    }

    private MemoryPreferences(ProxyPreferencesImpl delegate) {
        this.prefInstance = delegate;
    }
}

