/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 */
package org.netbeans.modules.editor.settings.storage.api;

import java.beans.PropertyChangeListener;
import java.util.List;
import org.netbeans.api.editor.settings.MultiKeyBinding;

public abstract class KeyBindingSettingsFactory {
    public abstract List<MultiKeyBinding> getKeyBindings();

    public abstract List<MultiKeyBinding> getKeyBindings(String var1);

    public abstract List<MultiKeyBinding> getKeyBindingDefaults(String var1);

    public abstract void setKeyBindings(String var1, List<MultiKeyBinding> var2);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

