/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage.preferences;

import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashSet;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.modules.editor.settings.storage.api.OverridePreferences;
import org.openide.util.WeakListeners;

public final class InheritedPreferences
extends AbstractPreferences
implements PreferenceChangeListener,
OverridePreferences {
    private Preferences inherited;
    private Preferences stored;
    private ThreadLocal<Boolean> ignorePut = new ThreadLocal();

    public InheritedPreferences(Preferences inherited, Preferences stored) {
        super(null, "");
        this.inherited = inherited;
        if (!(stored instanceof OverridePreferences)) {
            throw new IllegalArgumentException();
        }
        this.stored = stored;
        stored.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)stored));
        inherited.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)inherited));
    }

    Preferences getParent() {
        return this.inherited;
    }

    @Override
    protected void putSpi(String key, String value) {
    }

    @Override
    public void put(String key, String value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.put(key, value);
        }
        super.put(key, value);
    }

    @Override
    public void putInt(String key, int value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.putInt(key, value);
        }
        super.putInt(key, value);
    }

    @Override
    public void putLong(String key, long value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.putLong(key, value);
        }
        super.putLong(key, value);
    }

    @Override
    public void putBoolean(String key, boolean value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.putBoolean(key, value);
        }
        super.putBoolean(key, value);
    }

    @Override
    public void putFloat(String key, float value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.putFloat(key, value);
        }
        super.putFloat(key, value);
    }

    @Override
    public void putDouble(String key, double value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.putDouble(key, value);
        }
        super.putDouble(key, value);
    }

    @Override
    public void putByteArray(String key, byte[] value) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.putByteArray(key, value);
        }
        super.putByteArray(key, value);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        block12 : {
            if (evt.getKey() == null) {
                return;
            }
            this.ignorePut.set(true);
            String k = evt.getKey();
            String v = evt.getNewValue();
            try {
                if (evt.getSource() == this.stored) {
                    if (!this.isOverriden(k) && v != null) {
                        return;
                    }
                    if (v == null) {
                        v = this.inherited.get(k, null);
                    }
                } else if (this.isOverriden(k)) {
                    return;
                }
                if (v == null) {
                    this.remove(k);
                    break block12;
                }
                this.put(k, v);
            }
            finally {
                this.ignorePut.set(false);
            }
        }
    }

    @Override
    public boolean isOverriden(String k) {
        if (this.stored instanceof OverridePreferences) {
            return ((OverridePreferences)((Object)this.stored)).isOverriden(k);
        }
        return true;
    }

    @Override
    protected String getSpi(String key) {
        OverridePreferences localStored = (OverridePreferences)((Object)this.stored);
        if (localStored.isOverriden(key)) {
            return this.stored.get(key, null);
        }
        return this.inherited.get(key, null);
    }

    @Override
    protected void removeSpi(String key) {
        if (Boolean.TRUE != this.ignorePut.get()) {
            this.stored.remove(key);
        }
    }

    @Override
    protected void removeNodeSpi() throws BackingStoreException {
        this.stored.removeNode();
    }

    @Override
    protected String[] keysSpi() throws BackingStoreException {
        HashSet<String> names = new HashSet<String>();
        names.addAll(Arrays.asList(this.stored.keys()));
        names.addAll(Arrays.asList(this.inherited.keys()));
        return names.toArray(new String[names.size()]);
    }

    @Override
    protected String[] childrenNamesSpi() throws BackingStoreException {
        if (this.stored != null) {
            return this.stored.childrenNames();
        }
        return new String[0];
    }

    @Override
    protected AbstractPreferences childSpi(String name) {
        Preferences storedNode;
        Preferences preferences = storedNode = this.stored != null ? this.stored.node(name) : null;
        if (storedNode != null) {
            return new InheritedPreferences(null, storedNode);
        }
        return null;
    }

    @Override
    protected void syncSpi() throws BackingStoreException {
        this.stored.sync();
    }

    @Override
    protected void flushSpi() throws BackingStoreException {
        this.stored.flush();
    }
}

