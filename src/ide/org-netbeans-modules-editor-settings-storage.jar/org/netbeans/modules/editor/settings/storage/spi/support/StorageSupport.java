/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.options.keymap.api.KeyStrokeUtils
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.settings.storage.spi.support;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.KeyStroke;
import org.netbeans.core.options.keymap.api.KeyStrokeUtils;
import org.netbeans.modules.editor.settings.storage.Utils;
import org.openide.filesystems.FileObject;
import org.openide.util.Utilities;

@Deprecated
public final class StorageSupport {
    private static final Logger LOG = Logger.getLogger(StorageSupport.class.getName());
    private static Map<String, Integer> names;

    private StorageSupport() {
    }

    public static String getLocalizingBundleMessage(FileObject fo, String key, String defaultValue) {
        return Utils.getLocalizedName(fo, key, defaultValue, false);
    }

    public static String keyStrokesToString(Collection<? extends KeyStroke> keys, boolean emacsStyle) {
        if (!emacsStyle) {
            return KeyStrokeUtils.getKeyStrokesAsText((KeyStroke[])keys.toArray(new KeyStroke[keys.size()]), (String)" ");
        }
        StringBuilder sb = new StringBuilder();
        Iterator<? extends KeyStroke> it = keys.iterator();
        while (it.hasNext()) {
            KeyStroke keyStroke = it.next();
            sb.append(Utilities.keyToString((KeyStroke)keyStroke, (boolean)true));
            if (!it.hasNext()) continue;
            sb.append('$');
        }
        return sb.toString();
    }

    public static KeyStroke[] stringToKeyStrokes(String key, boolean emacsStyle) {
        assert (key != null);
        if (emacsStyle) {
            return Utilities.stringToKeys((String)key.replaceAll("\\$", " "));
        }
        return KeyStrokeUtils.getKeyStrokes((String)key);
    }
}

