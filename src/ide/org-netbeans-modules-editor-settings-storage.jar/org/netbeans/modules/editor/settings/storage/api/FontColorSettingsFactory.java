/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings.storage.api;

import java.util.Collection;
import javax.swing.text.AttributeSet;

public abstract class FontColorSettingsFactory {
    public abstract Collection<AttributeSet> getAllFontColors(String var1);

    public abstract Collection<AttributeSet> getAllFontColorDefaults(String var1);

    public abstract void setAllFontColors(String var1, Collection<AttributeSet> var2);

    public abstract void setAllFontColorsDefaults(String var1, Collection<AttributeSet> var2);
}

