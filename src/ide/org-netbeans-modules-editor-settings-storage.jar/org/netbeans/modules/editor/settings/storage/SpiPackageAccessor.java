/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings.storage;

import java.util.concurrent.Callable;
import org.netbeans.modules.editor.settings.storage.spi.StorageFilter;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;

public abstract class SpiPackageAccessor {
    private static SpiPackageAccessor ACCESSOR = null;

    public static synchronized void register(SpiPackageAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized SpiPackageAccessor get() {
        try {
            Class clazz = Class.forName(StorageReader.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected SpiPackageAccessor() {
    }

    public abstract String storageFilterGetStorageDescriptionId(StorageFilter var1);

    public abstract void storageFilterInitialize(StorageFilter var1, Callable<Void> var2);
}

