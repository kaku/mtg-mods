/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.settings.storage.preferences;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage;
import org.netbeans.modules.editor.settings.storage.api.OverridePreferences;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class PreferencesImpl
extends AbstractPreferences
implements PreferenceChangeListener,
OverridePreferences {
    private static final String JAVATYPE_KEY_PREFIX = "nbeditor-javaType-for-legacy-setting_";
    private static final Logger LOG = Logger.getLogger(PreferencesImpl.class.getName());
    private static final Map<MimePath, PreferencesImpl> INSTANCES = new WeakHashMap<MimePath, PreferencesImpl>();
    private static final String SLASH = "/";
    private static final String EMPTY = "";
    private static final String[] EMPTY_ARRAY = new String[0];
    private static final RequestProcessor RP = new RequestProcessor();
    private final RequestProcessor.Task flushTask;
    private boolean noEnqueueMethodAvailable;
    private final ThreadLocal<String> refiringChangeKey;
    private final ThreadLocal<String> putValueJavaType;
    private final String mimePath;
    private final EditorSettingsStorage<String, TypedValue> storage;
    private final PropertyChangeListener storageTracker;
    private Map<String, TypedValue> local;
    private Preferences inherited;

    public static synchronized PreferencesImpl get(MimePath mimePath) {
        PreferencesImpl prefs = INSTANCES.get((Object)mimePath);
        if (prefs == null) {
            prefs = new PreferencesImpl(mimePath.getPath());
            INSTANCES.put(mimePath, prefs);
        }
        return prefs;
    }

    @Override
    public String absolutePath() {
        return "/";
    }

    @Override
    public String[] childrenNames() throws BackingStoreException {
        return EMPTY_ARRAY;
    }

    @Override
    public boolean isUserNode() {
        return true;
    }

    @Override
    public String name() {
        return "";
    }

    @Override
    public Preferences node(String path) {
        if (path.length() == 0 || path.equals("/")) {
            return this;
        }
        throw new IllegalStateException("Editor Preferences does not support children nodes.");
    }

    @Override
    public boolean nodeExists(String path) throws BackingStoreException {
        if (path.length() == 0 || path.equals("/")) {
            return true;
        }
        return false;
    }

    @Override
    public Preferences parent() {
        return null;
    }

    @Override
    public void removeNode() throws BackingStoreException {
        throw new IllegalStateException("Can't remove the root!");
    }

    @Override
    public final void sync() throws BackingStoreException {
        this.flushTask.waitFinished();
        super.sync();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void put(String key, String value) {
        if (this.putValueJavaType.get() == null) {
            this.putValueJavaType.set(String.class.getName());
        }
        try {
            Object object = this.lock;
            synchronized (object) {
                block10 : {
                    if (key == null || value == null || !value.equals(this.getSpi(key))) break block10;
                    return;
                }
                super.put(key, value);
            }
        }
        finally {
            if (this.putValueJavaType.get().equals(String.class.getName())) {
                this.putValueJavaType.remove();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isOverriden(String key) {
        Object object = this.lock;
        synchronized (object) {
            String bareKey = key.startsWith("nbeditor-javaType-for-legacy-setting_") ? key.substring("nbeditor-javaType-for-legacy-setting_".length()) : key;
            return this.getLocal().containsKey(bareKey);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void remove(String key) {
        Object object = this.lock;
        synchronized (object) {
            boolean removeValue;
            String bareKey;
            if (key.startsWith("nbeditor-javaType-for-legacy-setting_")) {
                bareKey = key.substring("nbeditor-javaType-for-legacy-setting_".length());
                removeValue = false;
            } else {
                bareKey = key;
                removeValue = true;
            }
            if (this.getLocal().containsKey(bareKey)) {
                if (removeValue) {
                    this.getLocal().remove(bareKey);
                } else {
                    this.getLocal().get(bareKey).setJavaType(null);
                }
                this.firePreferenceChange(key, null);
                this.asyncInvocationOfFlushSpi();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putInt(String key, int value) {
        this.putValueJavaType.set(Integer.class.getName());
        try {
            super.putInt(key, value);
        }
        finally {
            this.putValueJavaType.remove();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putLong(String key, long value) {
        this.putValueJavaType.set(Long.class.getName());
        try {
            super.putLong(key, value);
        }
        finally {
            this.putValueJavaType.remove();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putBoolean(String key, boolean value) {
        this.putValueJavaType.set(Boolean.class.getName());
        try {
            super.putBoolean(key, value);
        }
        finally {
            this.putValueJavaType.remove();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putFloat(String key, float value) {
        this.putValueJavaType.set(Float.class.getName());
        try {
            super.putFloat(key, value);
        }
        finally {
            this.putValueJavaType.remove();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putDouble(String key, double value) {
        this.putValueJavaType.set(Double.class.getName());
        try {
            super.putDouble(key, value);
        }
        finally {
            this.putValueJavaType.remove();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putByteArray(String key, byte[] value) {
        this.putValueJavaType.set(value.getClass().getName());
        try {
            super.putByteArray(key, value);
        }
        finally {
            this.putValueJavaType.remove();
        }
    }

    @Override
    public void removePreferenceChangeListener(PreferenceChangeListener pcl) {
        try {
            super.removePreferenceChangeListener(pcl);
        }
        catch (IllegalArgumentException e) {
            // empty catch block
        }
    }

    @Override
    public void removeNodeChangeListener(NodeChangeListener ncl) {
        try {
            super.removeNodeChangeListener(ncl);
        }
        catch (IllegalArgumentException e) {
            // empty catch block
        }
    }

    @Override
    protected AbstractPreferences getChild(String nodeName) throws BackingStoreException {
        throw new IllegalStateException("Should never be called.");
    }

    @Override
    protected boolean isRemoved() {
        boolean superRemoved = super.isRemoved();
        assert (!superRemoved);
        return superRemoved;
    }

    @Override
    protected void removeNodeSpi() throws BackingStoreException {
        throw new IllegalStateException("Should never be called.");
    }

    @Override
    protected String[] childrenNamesSpi() throws BackingStoreException {
        throw new IllegalStateException("Should never be called.");
    }

    @Override
    protected AbstractPreferences childSpi(String name) {
        throw new IllegalStateException("Should never be called.");
    }

    @Override
    protected void putSpi(String key, String value) {
        if (this.refiringChangeKey.get() != key) {
            if (!key.startsWith("nbeditor-javaType-for-legacy-setting_")) {
                this.getLocal().put(key, new TypedValue(value, this.putValueJavaType.get()));
                this.asyncInvocationOfFlushSpi();
            } else {
                String bareKey = key.substring("nbeditor-javaType-for-legacy-setting_".length());
                if (this.getLocal().containsKey(bareKey)) {
                    this.getLocal().get(bareKey).setJavaType(value);
                    this.asyncInvocationOfFlushSpi();
                } else {
                    Preferences inheritedPrefs = this.getInherited();
                    assert (inheritedPrefs != null);
                    inheritedPrefs.put(key, value);
                }
            }
        }
    }

    @Override
    protected String getSpi(String key) {
        boolean returnValue;
        String bareKey;
        if (key.startsWith("nbeditor-javaType-for-legacy-setting_")) {
            bareKey = key.substring("nbeditor-javaType-for-legacy-setting_".length());
            returnValue = false;
        } else {
            bareKey = key;
            returnValue = true;
        }
        if (this.getLocal().containsKey(bareKey)) {
            TypedValue typedValue = this.getLocal().get(bareKey);
            return returnValue ? typedValue.getValue() : typedValue.getJavaType();
        }
        Preferences inheritedPrefs = this.getInherited();
        return inheritedPrefs != null ? inheritedPrefs.get(key, null) : null;
    }

    @Override
    protected void removeSpi(String key) {
        throw new IllegalStateException("Should never be called!");
    }

    @Override
    protected String[] keysSpi() throws BackingStoreException {
        Set keys;
        Preferences prefs = this.getInherited();
        if (prefs != null) {
            keys = new HashSet();
            keys.addAll(Arrays.asList(prefs.keys()));
            keys.addAll(this.getLocal().keySet());
        } else {
            keys = this.getLocal().keySet();
        }
        return keys.toArray(new String[keys.size()]);
    }

    @Override
    protected void syncSpi() throws BackingStoreException {
        this.local = null;
    }

    @Override
    protected void flushSpi() throws BackingStoreException {
        if (this.local != null) {
            try {
                this.storage.save(MimePath.parse((String)this.mimePath), null, false, this.local);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, "Can't save editor preferences for '" + this.mimePath + "'", ioe);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        Object object = this.lock;
        synchronized (object) {
            if (this.local != null && this.local.containsKey(evt.getKey())) {
                return;
            }
        }
        this.firePreferenceChange(evt.getKey(), evt.getNewValue());
    }

    private PreferencesImpl(String mimePath) {
        super(null, "");
        this.flushTask = RP.create(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = PreferencesImpl.this.lock;
                synchronized (object) {
                    try {
                        PreferencesImpl.this.flushSpi();
                    }
                    catch (BackingStoreException ex) {
                        LOG.log(Level.WARNING, null, ex);
                    }
                }
            }
        }, true);
        this.noEnqueueMethodAvailable = false;
        this.refiringChangeKey = new ThreadLocal();
        this.putValueJavaType = new ThreadLocal();
        this.storageTracker = new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt == null || "EditorSettingsStorage.PROP_DATA".equals(evt.getPropertyName())) {
                    Object object = PreferencesImpl.this.lock;
                    synchronized (object) {
                        if (PreferencesImpl.this.local == null) {
                            return;
                        }
                        PreferencesImpl.this.local = null;
                        PreferencesImpl.this.getLocal();
                    }
                    PreferencesImpl.this.firePreferenceChange(null, null);
                }
            }
        };
        this.local = null;
        this.inherited = null;
        this.mimePath = mimePath;
        this.storage = EditorSettingsStorage.get("Preferences");
        this.storage.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.storageTracker, this.storage));
    }

    private Map<String, TypedValue> getLocal() {
        if (this.local == null) {
            try {
                this.local = new HashMap<String, TypedValue>(this.storage.load(MimePath.parse((String)this.mimePath), null, false));
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, "Can't load editor preferences for '" + this.mimePath + "'", ioe);
                this.local = new HashMap<String, TypedValue>();
            }
        }
        return this.local;
    }

    private Preferences getInherited() {
        if (this.inherited == null && this.mimePath.length() > 0) {
            String type = MimePath.parse((String)this.mimePath).getInheritedType();
            this.inherited = type != null ? PreferencesImpl.get(MimePath.parse((String)type)) : PreferencesImpl.get(MimePath.EMPTY);
            this.inherited.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.inherited));
        }
        return this.inherited;
    }

    private void asyncInvocationOfFlushSpi() {
        this.flushTask.schedule(200);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void firePreferenceChange(String key, String newValue) {
        if (!this.noEnqueueMethodAvailable) {
            try {
                Method enqueueMethod = AbstractPreferences.class.getDeclaredMethod("enqueuePreferenceChangeEvent", String.class, String.class);
                enqueueMethod.setAccessible(true);
                enqueueMethod.invoke(this, key, newValue);
                return;
            }
            catch (NoSuchMethodException nsme) {
                this.noEnqueueMethodAvailable = true;
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, null, e);
            }
        }
        if (key != null && newValue != null) {
            this.refiringChangeKey.set(key);
            try {
                this.put(key, newValue);
            }
            finally {
                this.refiringChangeKey.remove();
            }
        }
        assert (false);
    }

}

