/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.PatchedPublic
 */
package org.netbeans.api.editor.settings;

import org.openide.modules.PatchedPublic;

public final class SimpleValueNames {
    public static final String TAB_SIZE = "tab-size";
    public static final String EXPAND_TABS = "expand-tabs";
    public static final String SPACES_PER_TAB = "spaces-per-tab";
    public static final String INDENT_SHIFT_WIDTH = "indent-shift-width";
    public static final String CARET_TYPE_INSERT_MODE = "caret-type-insert-mode";
    public static final String CARET_TYPE_OVERWRITE_MODE = "caret-type-overwrite-mode";
    public static final String THICK_CARET_WIDTH = "thick-caret-width";
    public static final String CARET_ITALIC_INSERT_MODE = "caret-italic-insert-mode";
    public static final String CARET_ITALIC_OVERWRITE_MODE = "caret-italic-overwrite-mode";
    @Deprecated
    public static final String CARET_COLOR_INSERT_MODE = "caret-color-insert-mode";
    @Deprecated
    public static final String CARET_COLOR_OVERWRITE_MODE = "caret-color-overwrite-mode";
    public static final String CARET_BLINK_RATE = "caret-blink-rate";
    public static final String LINE_NUMBER_VISIBLE = "line-number-visible";
    public static final String SCROLL_JUMP_INSETS = "scroll-jump-insets";
    public static final String SCROLL_FIND_INSETS = "scroll-find-insets";
    public static final String MARGIN = "margin";
    public static final String TEXT_LEFT_MARGIN_WIDTH = "text-left-margin-width";
    public static final String LINE_HEIGHT_CORRECTION = "line-height-correction";
    public static final String STATUS_BAR_VISIBLE = "status-bar-visible";
    public static final String STATUS_BAR_CARET_DELAY = "status-bar-caret-delay";
    public static final String TEXT_LIMIT_LINE_VISIBLE = "text-limit-line-visible";
    @Deprecated
    public static final String TEXT_LIMIT_LINE_COLOR = "text-limit-line-color";
    public static final String TEXT_LIMIT_WIDTH = "text-limit-width";
    public static final String TEXT_LINE_WRAP = "text-line-wrap";
    public static final String CODE_FOLDING_ENABLE = "code-folding-enable";
    public static final String CODE_FOLDING_COLLAPSE_INITIAL_COMMENT = "code-folding-collapse-initial-comment";
    public static final String CODE_FOLDING_COLLAPSE_METHOD = "code-folding-collapse-method";
    public static final String CODE_FOLDING_COLLAPSE_INNERCLASS = "code-folding-collapse-innerclass";
    public static final String CODE_FOLDING_COLLAPSE_IMPORT = "code-folding-collapse-import";
    public static final String CODE_FOLDING_COLLAPSE_JAVADOC = "code-folding-collapse-javadoc";
    public static final String CODE_FOLDING_COLLAPSE_TAGS = "code-folding-collapse-tags";
    public static final String HIGHLIGHT_CARET_ROW = "highlight-caret-row";
    public static final String HIGHLIGHT_MATCH_BRACE = "highlight-match-brace";
    public static final String COMPLETION_AUTO_POPUP = "completion-auto-popup";
    public static final String COMPLETION_CASE_SENSITIVE = "completion-case-sensitive";
    public static final String COMPLETION_NATURAL_SORT = "completion-natural-sort";
    public static final String COMPLETION_INSTANT_SUBSTITUTION = "completion-instant-substitution";
    public static final String COMPLETION_AUTO_POPUP_DELAY = "completion-auto-popup-delay";
    public static final String COMPLETION_PANE_MIN_SIZE = "completion-pane-min-size";
    public static final String COMPLETION_PANE_MAX_SIZE = "completion-pane-max-size";
    public static final String COMPLETION_PAIR_CHARACTERS = "pair-characters-completion";
    @Deprecated
    public static final String JAVADOC_BG_COLOR = "javadoc-bg-color";
    public static final String JAVADOC_AUTO_POPUP_DELAY = "javadoc-auto-popup-delay";
    public static final String JAVADOC_PREFERRED_SIZE = "javadoc-preferred-size";
    public static final String JAVADOC_AUTO_POPUP = "javadoc-auto-popup";
    public static final String SHOW_DEPRECATED_MEMBERS = "show-deprecated-members";
    public static final String HYPERLINK_ACTIVATION_MODIFIERS = "hyperlink-activation-modifiers";
    public static final String ALT_HYPERLINK_ACTIVATION_MODIFIERS = "alt-hyperlink-activation-modifiers";
    public static final String POPUP_MENU_ENABLED = "popup-menu-enabled";
    public static final String TOOLBAR_VISIBLE_PROP = "toolbarVisible";
    public static final String JAVADOC_POPUP_NEXT_TO_CC = "javadoc-popup-next-to-cc";
    public static final String NON_PRINTABLE_CHARACTERS_VISIBLE = "non-printable-characters-visible";
    public static final String ON_SAVE_REMOVE_TRAILING_WHITESPACE = "on-save-remove-trailing-whitespace";
    public static final String ON_SAVE_REFORMAT = "on-save-reformat";
    public static final String ON_SAVE_USE_GLOBAL_SETTINGS = "on-save-use-global-settings";
    public static final String EDITOR_SEARCH_TYPE = "editor-search-type";
    public static final String BRACE_SHOW_OUTLINE = "editor-brace-outline";
    public static final String BRACE_FIRST_TOOLTIP = "editor-brace-first-tooltip";

    @PatchedPublic
    private SimpleValueNames() {
    }
}

