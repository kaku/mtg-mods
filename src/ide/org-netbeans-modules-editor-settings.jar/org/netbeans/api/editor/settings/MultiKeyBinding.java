/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.List;
import java.util.RandomAccess;
import javax.swing.KeyStroke;

public final class MultiKeyBinding {
    private List<KeyStroke> keyStrokeList;
    private String actionName;

    public MultiKeyBinding(KeyStroke[] keyStrokes, String actionName) {
        if (keyStrokes == null) {
            throw new NullPointerException("keyStrokes cannot be null");
        }
        if (actionName == null) {
            throw new NullPointerException("actionName cannot be null");
        }
        this.keyStrokeList = new UnmodifiableArrayList<KeyStroke>(keyStrokes);
        this.actionName = actionName;
    }

    public MultiKeyBinding(KeyStroke keyStroke, String actionName) {
        this(new KeyStroke[]{keyStroke}, actionName);
        if (keyStroke == null) {
            throw new NullPointerException("keyStroke cannot be null");
        }
    }

    public KeyStroke getKeyStroke(int index) {
        return this.keyStrokeList.get(index);
    }

    public int getKeyStrokeCount() {
        return this.keyStrokeList.size();
    }

    public List<KeyStroke> getKeyStrokeList() {
        return this.keyStrokeList;
    }

    public String getActionName() {
        return this.actionName;
    }

    public boolean equals(Object o) {
        if (o instanceof MultiKeyBinding) {
            MultiKeyBinding kb = (MultiKeyBinding)o;
            if (this.actionName == null ? kb.actionName != null : !this.actionName.equals(kb.actionName)) {
                return false;
            }
            return this.keyStrokeList.equals(kb.keyStrokeList);
        }
        return false;
    }

    public int hashCode() {
        int result = 17;
        for (int i = 0; i < this.getKeyStrokeCount(); ++i) {
            result = 37 * result + this.getKeyStroke(i).hashCode();
        }
        if (this.actionName != null) {
            result = 37 * result + this.actionName.hashCode();
        }
        return result;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("keys(");
        for (KeyStroke ks : this.keyStrokeList) {
            sb.append(ks);
        }
        sb.append("), actionName=");
        sb.append(this.actionName);
        return sb.toString();
    }

    private static final class UnmodifiableArrayList<E>
    extends AbstractList<E>
    implements RandomAccess,
    Serializable {
        private static final long serialVersionUID = 0;
        private E[] array;

        UnmodifiableArrayList(E[] array) {
            if (array == null) {
                throw new NullPointerException();
            }
            this.array = array;
        }

        @Override
        public int size() {
            return this.array.length;
        }

        @Override
        public Object[] toArray() {
            return (Object[])this.array.clone();
        }

        @Override
        public E get(int index) {
            return this.array[index];
        }

        @Override
        public int indexOf(Object o) {
            if (o == null) {
                for (int i = 0; i < this.array.length; ++i) {
                    if (this.array[i] != null) continue;
                    return i;
                }
            } else {
                for (int i = 0; i < this.array.length; ++i) {
                    if (!o.equals(this.array[i])) continue;
                    return i;
                }
            }
            return -1;
        }

        @Override
        public boolean contains(Object o) {
            return this.indexOf(o) != -1;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof UnmodifiableArrayList ? Arrays.equals(this.array, ((UnmodifiableArrayList)o).array) : AbstractList.super.equals(o);
        }
    }

}

