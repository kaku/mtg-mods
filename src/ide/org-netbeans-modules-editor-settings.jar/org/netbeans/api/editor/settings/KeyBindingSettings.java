/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

import java.util.List;
import org.netbeans.api.editor.settings.MultiKeyBinding;

public abstract class KeyBindingSettings {
    public KeyBindingSettings() {
        if (!this.getClass().getName().startsWith("org.netbeans.modules.editor.settings.storage")) {
            throw new IllegalStateException("Instantiation prohibited. " + this.getClass().getName());
        }
    }

    public abstract List<MultiKeyBinding> getKeyBindings();
}

