/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class CodeTemplateDescription {
    private final String abbreviation;
    private final String description;
    private final String parametrizedText;
    private final List<String> contexts;
    private final String uniqueId;
    private final String mimePath;

    public CodeTemplateDescription(String abbreviation, String description, String parametrizedText) {
        this(abbreviation, description, parametrizedText, null, null, null);
    }

    public CodeTemplateDescription(String abbreviation, String description, String parametrizedText, List<String> contexts, String uniqueId) {
        this(abbreviation, description, parametrizedText, contexts, uniqueId, null);
    }

    public CodeTemplateDescription(String abbreviation, String description, String parametrizedText, List<String> contexts, String uniqueId, String mimePath) {
        assert (abbreviation != null);
        assert (parametrizedText != null);
        this.abbreviation = abbreviation;
        this.description = description;
        this.parametrizedText = parametrizedText;
        this.contexts = contexts == null ? Collections.emptyList() : Collections.unmodifiableList(new ArrayList<String>(contexts));
        this.uniqueId = uniqueId;
        this.mimePath = mimePath;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public String getDescription() {
        return this.description;
    }

    public String getParametrizedText() {
        return this.parametrizedText;
    }

    public List<String> getContexts() {
        return this.contexts;
    }

    public String getUniqueId() {
        return this.uniqueId;
    }

    public String getMimePath() {
        return this.mimePath;
    }

    public String toString() {
        return "abbrev='" + this.getAbbreviation() + "', parametrizedText='" + this.getParametrizedText() + "'";
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        CodeTemplateDescription other = (CodeTemplateDescription)obj;
        if (this.abbreviation == null && other.abbreviation != null || this.abbreviation != null && other.abbreviation == null || this.abbreviation != null && !this.abbreviation.equals(other.abbreviation)) {
            return false;
        }
        if (this.description == null && other.description != null || this.description != null && other.description == null || this.description != null && !this.description.equals(other.description)) {
            return false;
        }
        if (this.parametrizedText == null && other.parametrizedText != null || this.parametrizedText != null && other.parametrizedText == null || this.parametrizedText != null && !this.parametrizedText.equals(other.parametrizedText)) {
            return false;
        }
        if (!(this.contexts == other.contexts || this.contexts != null && this.contexts.equals(other.contexts))) {
            return false;
        }
        if (this.uniqueId == null && other.uniqueId != null || this.uniqueId != null && other.uniqueId == null || this.uniqueId != null && !this.uniqueId.equals(other.uniqueId)) {
            return false;
        }
        if (this.mimePath == null && other.mimePath != null || this.mimePath != null && other.mimePath == null || this.mimePath != null && !this.mimePath.equals(other.mimePath)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.abbreviation != null ? this.abbreviation.hashCode() : 0);
        hash = 59 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 59 * hash + (this.parametrizedText != null ? this.parametrizedText.hashCode() : 0);
        hash = 59 * hash + (this.contexts != null ? this.contexts.hashCode() : 0);
        hash = 59 * hash + (this.uniqueId != null ? this.uniqueId.hashCode() : 0);
        hash = 59 * hash + (this.mimePath != null ? this.mimePath.hashCode() : 0);
        return hash;
    }
}

