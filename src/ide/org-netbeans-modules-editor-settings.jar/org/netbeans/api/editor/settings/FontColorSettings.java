/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

import javax.swing.text.AttributeSet;

public abstract class FontColorSettings {
    public static final String PROP_FONT_COLORS = "fontColors";

    public FontColorSettings() {
        if (!this.getClass().getName().startsWith("org.netbeans.modules.editor.settings.storage")) {
            throw new IllegalStateException("Instantiation prohibited. " + this.getClass().getName());
        }
    }

    public abstract AttributeSet getFontColors(String var1);

    public abstract AttributeSet getTokenFontColors(String var1);
}

