/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.text.AttributeSet;
import org.netbeans.modules.editor.settings.AttrSet;

public final class AttributesUtilities {
    private static final String ATTR_DISMANTLED_STRUCTURE = "dismantled-structure";

    public static /* varargs */ AttributeSet createImmutable(Object ... keyValuePairs) {
        assert (keyValuePairs.length % 2 == 0);
        return AttrSet.get(keyValuePairs);
    }

    public static /* varargs */ AttributeSet createImmutable(AttributeSet ... sets) {
        return AttrSet.merge(sets);
    }

    public static /* varargs */ AttributeSet createComposite(AttributeSet ... sets) {
        return AttrSet.merge(sets);
    }

    private static List<AttributeSet> dismantle(AttributeSet set) {
        ArrayList<AttributeSet> sets = new ArrayList<AttributeSet>();
        if (set instanceof CompositeAttributeSet) {
            Collection<? extends AttributeSet> delegates = ((CompositeAttributeSet)((Object)set)).getDelegates();
            for (AttributeSet delegate : delegates) {
                sets.addAll(AttributesUtilities.dismantle(delegate));
            }
        } else {
            sets.add(set);
        }
        return sets;
    }

    private AttributesUtilities() {
    }

    private static final class Composite4
    implements AttributeSet,
    CompositeAttributeSet {
        private final AttributeSet delegate0;
        private final AttributeSet delegate1;
        private final AttributeSet delegate2;
        private final AttributeSet delegate3;

        public Composite4(AttributeSet delegate0, AttributeSet delegate1, AttributeSet delegate2, AttributeSet delegate3) {
            this.delegate0 = delegate0;
            this.delegate1 = delegate1;
            this.delegate2 = delegate2;
            this.delegate3 = delegate3;
        }

        @Override
        public Collection<? extends AttributeSet> getDelegates() {
            if (this.delegate3 == null) {
                return Arrays.asList(this.delegate0, this.delegate1, this.delegate2);
            }
            return Arrays.asList(this.delegate0, this.delegate1, this.delegate2, this.delegate3);
        }

        @Override
        public boolean isEqual(AttributeSet attr) {
            return this.containsAttributes(attr) && attr.containsAttributes(this);
        }

        @Override
        public boolean containsAttributes(AttributeSet attributes) {
            Enumeration keys = attributes.getAttributeNames();
            while (keys.hasMoreElements()) {
                Object value;
                Object key = keys.nextElement();
                if (this.containsAttribute(key, value = attributes.getAttribute(key))) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean isDefined(Object key) {
            return this.delegate0.isDefined(key) || this.delegate1.isDefined(key) || this.delegate2.isDefined(key) || this.delegate3 != null && this.delegate3.isDefined(key);
        }

        @Override
        public Object getAttribute(Object key) {
            if (key instanceof String && key.equals("dismantled-structure")) {
                return AttributesUtilities.dismantle(this);
            }
            AttributeSet[] set = this.delegate3 == null ? new AttributeSet[]{this.delegate0, this.delegate1, this.delegate2} : new AttributeSet[]{this.delegate0, this.delegate1, this.delegate2, this.delegate3};
            AttributeSet[] arr$ = set;
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; ++i$) {
                for (AttributeSet current = delegate = arr$[i$]; current != null; current = current.getResolveParent()) {
                    if (!current.isDefined(key)) continue;
                    return current.getAttribute(key);
                }
            }
            return null;
        }

        @Override
        public AttributeSet getResolveParent() {
            return null;
        }

        @Override
        public Enumeration<?> getAttributeNames() {
            return Collections.enumeration(this.getAllKeys());
        }

        @Override
        public int getAttributeCount() {
            return this.getAllKeys().size();
        }

        @Override
        public AttributeSet copyAttributes() {
            if (this.delegate3 == null) {
                return AttributesUtilities.createImmutable(this.delegate0, this.delegate1, this.delegate2);
            }
            return AttributesUtilities.createImmutable(this.delegate0, this.delegate1, this.delegate2, this.delegate3);
        }

        @Override
        public boolean containsAttribute(Object key, Object value) {
            return this.delegate0.containsAttribute(key, value) || this.delegate1.containsAttribute(key, value) || this.delegate2.containsAttribute(key, value) || this.delegate3 != null && this.delegate3.containsAttribute(key, value);
        }

        private Collection<?> getAllKeys() {
            HashSet allKeys = new HashSet();
            AttributeSet[] set = this.delegate3 == null ? new AttributeSet[]{this.delegate0, this.delegate1, this.delegate2} : new AttributeSet[]{this.delegate0, this.delegate1, this.delegate2, this.delegate3};
            for (AttributeSet delegate : set) {
                Enumeration keys = delegate.getAttributeNames();
                while (keys.hasMoreElements()) {
                    Object key = keys.nextElement();
                    allKeys.add(key);
                }
            }
            return allKeys;
        }
    }

    private static final class Composite2
    implements AttributeSet,
    CompositeAttributeSet {
        private final AttributeSet delegate0;
        private final AttributeSet delegate1;

        public Composite2(AttributeSet delegate0, AttributeSet delegate1) {
            this.delegate0 = delegate0;
            this.delegate1 = delegate1;
        }

        @Override
        public Collection<? extends AttributeSet> getDelegates() {
            return Arrays.asList(this.delegate0, this.delegate1);
        }

        @Override
        public boolean isEqual(AttributeSet attr) {
            return this.containsAttributes(attr) && attr.containsAttributes(this);
        }

        @Override
        public boolean containsAttributes(AttributeSet attributes) {
            Enumeration keys = attributes.getAttributeNames();
            while (keys.hasMoreElements()) {
                Object value;
                Object key = keys.nextElement();
                if (this.containsAttribute(key, value = attributes.getAttribute(key))) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean isDefined(Object key) {
            return this.delegate0.isDefined(key) || this.delegate1.isDefined(key);
        }

        @Override
        public Object getAttribute(Object key) {
            if (key instanceof String && key.equals("dismantled-structure")) {
                return AttributesUtilities.dismantle(this);
            }
            AttributeSet[] arr$ = new AttributeSet[]{this.delegate0, this.delegate1};
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; ++i$) {
                for (AttributeSet current = delegate = arr$[i$]; current != null; current = current.getResolveParent()) {
                    if (!current.isDefined(key)) continue;
                    return current.getAttribute(key);
                }
            }
            return null;
        }

        @Override
        public AttributeSet getResolveParent() {
            return null;
        }

        @Override
        public Enumeration<?> getAttributeNames() {
            return Collections.enumeration(this.getAllKeys());
        }

        @Override
        public int getAttributeCount() {
            return this.getAllKeys().size();
        }

        @Override
        public AttributeSet copyAttributes() {
            return AttributesUtilities.createImmutable(this.delegate0, this.delegate1);
        }

        @Override
        public boolean containsAttribute(Object key, Object value) {
            return this.delegate0.containsAttribute(key, value) || this.delegate1.containsAttribute(key, value);
        }

        private Collection<?> getAllKeys() {
            HashSet allKeys = new HashSet();
            for (AttributeSet delegate : new AttributeSet[]{this.delegate0, this.delegate1}) {
                Enumeration keys = delegate.getAttributeNames();
                while (keys.hasMoreElements()) {
                    Object key = keys.nextElement();
                    allKeys.add(key);
                }
            }
            return allKeys;
        }
    }

    private static final class BigComposite
    implements AttributeSet,
    CompositeAttributeSet {
        private final AttributeSet[] delegates;

        public BigComposite(List<AttributeSet> delegates) {
            this.delegates = delegates.toArray(new AttributeSet[delegates.size()]);
        }

        @Override
        public Collection<? extends AttributeSet> getDelegates() {
            return Arrays.asList(this.delegates);
        }

        @Override
        public boolean isEqual(AttributeSet attr) {
            return this.containsAttributes(attr) && attr.containsAttributes(this);
        }

        @Override
        public boolean containsAttributes(AttributeSet attributes) {
            Enumeration keys = attributes.getAttributeNames();
            while (keys.hasMoreElements()) {
                Object value;
                Object key = keys.nextElement();
                if (this.containsAttribute(key, value = attributes.getAttribute(key))) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean isDefined(Object key) {
            for (AttributeSet delegate : this.delegates) {
                if (!delegate.isDefined(key)) continue;
                return true;
            }
            return false;
        }

        @Override
        public Object getAttribute(Object key) {
            if (key instanceof String && key.equals("dismantled-structure")) {
                return AttributesUtilities.dismantle(this);
            }
            AttributeSet[] arr$ = this.delegates;
            int len$ = arr$.length;
            for (int i$ = 0; i$ < len$; ++i$) {
                for (AttributeSet current = delegate = arr$[i$]; current != null; current = current.getResolveParent()) {
                    if (!current.isDefined(key)) continue;
                    return current.getAttribute(key);
                }
            }
            return null;
        }

        @Override
        public AttributeSet getResolveParent() {
            return null;
        }

        @Override
        public Enumeration<?> getAttributeNames() {
            return Collections.enumeration(this.getAllKeys());
        }

        @Override
        public int getAttributeCount() {
            return this.getAllKeys().size();
        }

        @Override
        public AttributeSet copyAttributes() {
            return AttributesUtilities.createImmutable(this.delegates);
        }

        @Override
        public boolean containsAttribute(Object key, Object value) {
            for (AttributeSet delegate : this.delegates) {
                if (!delegate.containsAttribute(key, value)) continue;
                return true;
            }
            return false;
        }

        private Collection<?> getAllKeys() {
            HashSet allKeys = new HashSet();
            for (AttributeSet delegate : this.delegates) {
                Enumeration keys = delegate.getAttributeNames();
                while (keys.hasMoreElements()) {
                    Object key = keys.nextElement();
                    allKeys.add(key);
                }
            }
            return allKeys;
        }
    }

    private static interface CompositeAttributeSet {
        public Collection<? extends AttributeSet> getDelegates();
    }

    private static final class Proxy
    implements AttributeSet,
    CompositeAttributeSet {
        private AttributeSet original;

        public Proxy(AttributeSet original) {
            this.original = original;
        }

        @Override
        public Collection<? extends AttributeSet> getDelegates() {
            return Arrays.asList(this.original);
        }

        @Override
        public boolean isEqual(AttributeSet attr) {
            return this.original.isEqual(attr);
        }

        @Override
        public boolean containsAttributes(AttributeSet attributes) {
            return this.original.containsAttributes(attributes);
        }

        @Override
        public boolean isDefined(Object attrName) {
            return this.original.isDefined(attrName);
        }

        @Override
        public Object getAttribute(Object key) {
            if (key instanceof String && key.equals("dismantled-structure")) {
                return AttributesUtilities.dismantle(this);
            }
            return this.original.getAttribute(key);
        }

        @Override
        public AttributeSet getResolveParent() {
            return this.original.getResolveParent();
        }

        @Override
        public Enumeration<?> getAttributeNames() {
            return this.original.getAttributeNames();
        }

        @Override
        public int getAttributeCount() {
            return this.original.getAttributeCount();
        }

        @Override
        public AttributeSet copyAttributes() {
            return this.original.copyAttributes();
        }

        @Override
        public boolean containsAttribute(Object name, Object value) {
            return this.original.containsAttribute(name, value);
        }
    }

    private static class Immutable
    implements AttributeSet {
        private final HashMap<Object, Object> attribs;
        private AttributeSet parent = null;

        private Immutable(HashMap<Object, Object> attribs) {
            this.attribs = attribs == null ? new HashMap() : attribs;
            Object resolver = this.attribs.get(AttributeSet.ResolveAttribute);
            if (resolver instanceof AttributeSet) {
                this.setResolveParent((AttributeSet)resolver);
            }
        }

        public synchronized void setResolveParent(AttributeSet parent) {
            this.parent = parent;
        }

        @Override
        public synchronized boolean containsAttributes(AttributeSet attributes) {
            Enumeration names = attributes.getAttributeNames();
            while (names.hasMoreElements()) {
                Object value;
                Object name = names.nextElement();
                if (this.containsAttribute(name, value = attributes.getAttribute(name))) continue;
                return false;
            }
            return true;
        }

        @Override
        public synchronized boolean isEqual(AttributeSet attr) {
            return this.containsAttributes(attr) && attr.containsAttributes(this);
        }

        @Override
        public synchronized Object getAttribute(Object key) {
            if (AttributeSet.ResolveAttribute == key) {
                return this.parent;
            }
            if (this.attribs.containsKey(key)) {
                return this.attribs.get(key);
            }
            if (this.parent != null) {
                return this.parent.getAttribute(key);
            }
            return null;
        }

        @Override
        public synchronized boolean isDefined(Object key) {
            return this.attribs.containsKey(key);
        }

        @Override
        public synchronized boolean containsAttribute(Object key, Object value) {
            if (this.attribs.containsKey(key)) {
                Object attrValue = this.attribs.get(key);
                if (value == null && attrValue == null || value != null && attrValue != null && value.equals(attrValue)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public AttributeSet copyAttributes() {
            return new Proxy(this);
        }

        @Override
        public synchronized int getAttributeCount() {
            return this.attribs.size();
        }

        @Override
        public synchronized Enumeration<?> getAttributeNames() {
            return Collections.enumeration(this.attribs.keySet());
        }

        @Override
        public synchronized AttributeSet getResolveParent() {
            return this.parent;
        }
    }

}

