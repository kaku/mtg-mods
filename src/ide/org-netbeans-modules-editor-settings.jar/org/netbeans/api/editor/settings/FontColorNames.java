/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.PatchedPublic
 */
package org.netbeans.api.editor.settings;

import org.openide.modules.PatchedPublic;

public final class FontColorNames {
    public static final String DEFAULT_COLORING = "default";
    public static final String LINE_NUMBER_COLORING = "line-number";
    public static final String INDENT_GUIDE_LINES = "indent-guide-lines";
    public static final String GUARDED_COLORING = "guarded";
    public static final String CODE_FOLDING_COLORING = "code-folding";
    public static final String CODE_FOLDING_BAR_COLORING = "code-folding-bar";
    public static final String SELECTION_COLORING = "selection";
    public static final String HIGHLIGHT_SEARCH_COLORING = "highlight-search";
    public static final String INC_SEARCH_COLORING = "inc-search";
    public static final String BLOCK_SEARCH_COLORING = "block-search";
    public static final String STATUS_BAR_COLORING = "status-bar";
    public static final String STATUS_BAR_BOLD_COLORING = "status-bar-bold";
    public static final String CARET_ROW_COLORING = "highlight-caret-row";
    public static final String TEXT_LIMIT_LINE_COLORING = "text-limit-line-color";
    public static final String CARET_COLOR_INSERT_MODE = "caret-color-insert-mode";
    public static final String CARET_COLOR_OVERWRITE_MODE = "caret-color-overwrite-mode";
    public static final String DOCUMENTATION_POPUP_COLORING = "documentation-popup-coloring";

    @PatchedPublic
    private FontColorNames() {
    }
}

