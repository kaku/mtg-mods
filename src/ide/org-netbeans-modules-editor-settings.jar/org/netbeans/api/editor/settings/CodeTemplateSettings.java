/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

import java.util.List;
import javax.swing.KeyStroke;
import org.netbeans.api.editor.settings.CodeTemplateDescription;

public abstract class CodeTemplateSettings {
    public CodeTemplateSettings() {
        if (!this.getClass().getName().startsWith("org.netbeans.lib.editor.codetemplates")) {
            throw new IllegalStateException("Instantiation prohibited. " + this.getClass().getName());
        }
    }

    public abstract List<CodeTemplateDescription> getCodeTemplateDescriptions();

    public abstract KeyStroke getExpandKey();
}

