/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.settings;

public final class EditorStyleConstants {
    private String representation;
    public static final Object WaveUnderlineColor = new EditorStyleConstants("wave underline color");
    public static final Object DisplayName = new EditorStyleConstants("display name");
    public static final Object Default = new EditorStyleConstants("default");
    public static final Object Tooltip = new EditorStyleConstants("tooltip");
    public static final Object TopBorderLineColor = new EditorStyleConstants("top border line color");
    public static final Object RightBorderLineColor = new EditorStyleConstants("right border line color");
    public static final Object BottomBorderLineColor = new EditorStyleConstants("bottom border line color");
    public static final Object LeftBorderLineColor = new EditorStyleConstants("left border line color");
    public static final Object RenderingHints = new EditorStyleConstants("rendering hints");

    private EditorStyleConstants(String representation) {
        this.representation = representation;
    }

    public String toString() {
        return this.representation;
    }
}

