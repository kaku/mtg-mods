/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings;

import java.awt.Color;
import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.editor.settings.SimpleWeakSet;

public abstract class AttrSet
implements AttributeSet,
Iterable<Object> {
    static final Map<Object, KeyWrapper> sharedKeys = new HashMap<Object, KeyWrapper>(64, 0.4f);
    private static final SimpleWeakSet<Shared> cache = new SimpleWeakSet<E>();
    private static final Object[] EMPTY_ARRAY = new Object[0];
    private static final Shared EMPTY = new Shared(EMPTY_ARRAY, 0);
    private static final Logger LOG = Logger.getLogger(AttrSet.class.getName());
    private static int opCount;
    private static int nextDumpOpCount;
    private static int cacheGets;
    private static int cacheMisses;
    private static int overrideGets;
    private static int overrideMisses;
    private static int alienConvert;
    private Map<AttrSet, WeakReference<AttrSet>> overrideCache;

    public static synchronized /* varargs */ AttrSet get(Object ... keyValuePairs) {
        if (keyValuePairs.length == 0) {
            return EMPTY;
        }
        AttrSetBuilder builder = new AttrSetBuilder(keyValuePairs.length);
        int i = keyValuePairs.length;
        while (i > 0) {
            Object key;
            Object value = keyValuePairs[--i];
            if ((key = keyValuePairs[--i]) == null || value == null) continue;
            if (key == AttributeSet.ResolveAttribute) {
                throw new IllegalStateException("AttributeSet.ResolveAttribute key not supported");
            }
            builder.add(key, value);
        }
        AttrSet attrSet = builder.toAttrSet();
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("AttrSet.get():\n");
            int i2 = 0;
            while (i2 < keyValuePairs.length) {
                Object key;
                if (sharedKeys.containsKey(key = keyValuePairs[i2++])) {
                    sb.append("  S ");
                } else {
                    sb.append("    ");
                }
                sb.append(key).append(" => ").append(keyValuePairs[i2++]).append('\n');
            }
            sb.append("=> ").append(attrSet).append("; cacheSize=").append(AttrSet.cacheSize()).append('\n');
            AttrSet.dumpCache(sb);
            LOG.fine(sb.toString());
        }
        ++opCount;
        return attrSet;
    }

    public static synchronized /* varargs */ AttrSet merge(AttributeSet ... sets) {
        if (sets.length == 0) {
            return EMPTY;
        }
        AttrSet attrSet = null;
        for (int i = sets.length - 1; i >= 0; --i) {
            AttributeSet set = sets[i];
            if (set == null) continue;
            if (attrSet == null) {
                attrSet = AttrSet.toAttrSet(set);
                continue;
            }
            if (set == EMPTY) continue;
            attrSet = attrSet.findOverride(AttrSet.toAttrSet(set));
        }
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("AttrSet.merge():\n");
            for (int i2 = 0; i2 < sets.length; ++i2) {
                sb.append("    ").append(sets[i2]).append('\n');
            }
            sb.append("=> ").append(attrSet).append("; cacheSize=").append(AttrSet.cacheSize()).append('\n');
            AttrSet.dumpCache(sb);
            LOG.fine(sb.toString());
        }
        ++opCount;
        return attrSet;
    }

    private static void dumpCache(StringBuilder sb) {
        if (opCount >= nextDumpOpCount) {
            nextDumpOpCount = opCount + 100;
            sb.append("AttrSet CACHE DUMP START -------------------------\n");
            List<Shared> cacheAsList = cache.asList();
            int i = 0;
            for (Shared shared : cacheAsList) {
                AttrSet.appendSpaces(sb, 4);
                sb.append("[").append(i++).append("] ");
                shared.appendInfo(sb, true, true, 4).append('\n');
            }
            sb.append("Actual CACHE SIZE is ").append(cacheAsList.size()).append('\n');
            sb.append("  Cache gets/misses: ").append(cacheGets).append('/').append(cacheMisses).append('\n');
            sb.append("  Override cache gets/misses: ").append(overrideGets).append('/');
            sb.append(overrideMisses).append('\n');
            sb.append("  Override cache gets/misses: ").append(overrideGets).append('/');
            sb.append(overrideMisses).append('\n');
            sb.append("  Alien convert: ").append(alienConvert).append('\n');
            sb.append("AttrSet CACHE DUMP END ---------------------------\n");
        }
    }

    private static synchronized void registerSharedKey(Object key, Class valueType) {
        if (!sharedKeys.containsKey(key)) {
            KeyWrapper keyWrapper = new KeyWrapper(key, valueType);
            sharedKeys.put(key, keyWrapper);
        }
    }

    static int cacheSize() {
        return cache.size();
    }

    protected AttrSet() {
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AttrSet) {
            return this.isEqual((AttrSet)obj);
        }
        if (obj instanceof AttrSetBuilder) {
            return ((AttrSetBuilder)obj).isEqual(this);
        }
        return false;
    }

    @Override
    public final AttributeSet copyAttributes() {
        return this;
    }

    @Override
    public final boolean containsAttribute(Object key, Object value) {
        return value.equals(this.getAttribute(key));
    }

    @Override
    public boolean containsAttributes(AttributeSet attrs) {
        if (attrs instanceof AttrSet) {
            Iterator<Object> it = ((AttrSet)attrs).iterator();
            while (it.hasNext()) {
                Object value;
                Object key = it.next();
                if (this.containsAttribute(key, value = it.next())) continue;
                return false;
            }
        } else {
            Enumeration en = attrs.getAttributeNames();
            while (en.hasMoreElements()) {
                Object value;
                Object key = en.nextElement();
                if (this.containsAttribute(key, value = attrs.getAttribute(key))) continue;
                return false;
            }
        }
        return true;
    }

    @Override
    public AttributeSet getResolveParent() {
        return null;
    }

    protected abstract Object[] sharedPairs();

    protected abstract int sharedHashCode();

    protected abstract Object[] extraPairs();

    protected abstract int extrasHashCode();

    @Override
    public final Enumeration<?> getAttributeNames() {
        return new KeysEnumeration(this.sharedPairs(), this.extraPairs());
    }

    @Override
    public final Iterator<Object> iterator() {
        return new KeyValueIterator(this.sharedPairs(), this.extraPairs());
    }

    final AttrSet cachedOverride(Object override) {
        AttrSet attrSet;
        if (this.overrideCache == null) {
            this.overrideCache = new WeakHashMap<AttrSet, WeakReference<AttrSet>>(4);
            attrSet = null;
        } else {
            WeakReference<AttrSet> ref = this.overrideCache.get(override);
            attrSet = ref != null ? ref.get() : null;
        }
        ++overrideGets;
        return attrSet;
    }

    void addOverride(AttrSet override, AttrSet attrSet) {
        this.overrideCache.put(override, new WeakReference<AttrSet>(attrSet));
        ++overrideMisses;
    }

    final AttrSet findOverride(AttrSet override) {
        AttrSet attrSet = this.cachedOverride(override);
        if (attrSet == null) {
            Object value;
            Object[] oSharedPairs = override.sharedPairs();
            Object[] oExtraPairs = override.extraPairs();
            int osIndex = oSharedPairs.length;
            int oeIndex = oExtraPairs.length;
            AttrSetBuilder builder = new AttrSetBuilder(this.sharedPairs(), this.sharedHashCode(), this.extraPairs(), this.extrasHashCode(), osIndex, oeIndex);
            while (osIndex > 0) {
                value = oSharedPairs[--osIndex];
                KeyWrapper keyWrapper = (KeyWrapper)oSharedPairs[--osIndex];
                builder.addShared(keyWrapper, value);
            }
            while (oeIndex > 0) {
                value = oExtraPairs[--oeIndex];
                Object key = oExtraPairs[--oeIndex];
                builder.addExtra(key, value);
            }
            attrSet = builder.toAttrSet();
            this.addOverride(override, attrSet);
        }
        return attrSet;
    }

    static int findKeyWrapperIndex(Object[] pairs, int pairsLength, KeyWrapper keyWrapper) {
        int high = pairsLength - 2;
        int low = 0;
        while (low <= high) {
            int mid = low + high >>> 1 & -2;
            KeyWrapper kw = (KeyWrapper)pairs[mid];
            int cmp = kw.order - keyWrapper.order;
            if (cmp < 0) {
                low = mid + 2;
                continue;
            }
            if (cmp > 0) {
                high = mid - 2;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    static AttrSet toAttrSet(AttributeSet attrs) {
        if (attrs instanceof AttrSet) {
            return (AttrSet)attrs;
        }
        ++alienConvert;
        return AttrSet.toAttrSetBuilder(attrs).toAttrSet();
    }

    static AttrSetBuilder toAttrSetBuilder(AttributeSet attrs) {
        AttrSetBuilder builder = new AttrSetBuilder(attrs.getAttributeCount() << 1);
        Enumeration<?> en = attrs.getAttributeNames();
        while (en.hasMoreElements()) {
            ? key = en.nextElement();
            Object value = attrs.getAttribute(key);
            builder.add(key, value);
        }
        return builder;
    }

    static Object[] trimArray(Object[] array, int size) {
        Object[] ret = new Object[size];
        while (--size >= 0) {
            ret[size] = array[size];
        }
        return ret;
    }

    void checkIntegrity() {
        String error = this.findIntegrityError();
        if (error != null) {
            throw new IllegalStateException(error);
        }
    }

    String findIntegrityError() {
        int i;
        Object key;
        int lastOrder = -1;
        Object[] sharedPairs = this.sharedPairs();
        Object[] extraPairs = this.extraPairs();
        int sharedHashCode = this.sharedHashCode();
        int extrasHashCode = this.extrasHashCode();
        int hashCode = 0;
        for (i = 0; i < sharedPairs.length; ++i) {
            Object value;
            key = sharedPairs[i];
            if (key == null) {
                return "[" + i + "] is null";
            }
            if (!(key instanceof KeyWrapper)) {
                return "[" + i + "]=" + key + " not KeyWrapper";
            }
            KeyWrapper keyWrapper = (KeyWrapper)key;
            if (keyWrapper.order <= lastOrder) {
                return "[" + i + "] KeyWrapper.order=" + keyWrapper.order + " <= lastOrder=" + lastOrder;
            }
            hashCode ^= keyWrapper.key.hashCode();
            lastOrder = keyWrapper.order;
            if ((value = sharedPairs[++i]) == null) {
                return "[" + i + "] is null";
            }
            hashCode ^= value.hashCode();
        }
        if (hashCode != sharedHashCode) {
            return "Invalid hashCode=" + hashCode + " != sharedHashCode=" + sharedHashCode;
        }
        hashCode = 0;
        for (i = 0; i < extraPairs.length; ++i) {
            Object value;
            key = extraPairs[i];
            if (key == null) {
                return "[" + i + "] is null";
            }
            if (sharedKeys.containsKey(key)) {
                return "[" + i + "]: KeyWrapper-like key in extraPairs: " + key;
            }
            hashCode ^= key.hashCode();
            if ((value = extraPairs[++i]) == null) {
                return "[" + i + "] is null";
            }
            hashCode ^= value.hashCode();
        }
        if (hashCode != extrasHashCode) {
            return "Invalid hashCode=" + hashCode + " != extrasHashCode=" + extrasHashCode;
        }
        return null;
    }

    public String toString() {
        return this.appendInfo(new StringBuilder(100), false, false, 0).toString();
    }

    StringBuilder appendInfo(StringBuilder sb, boolean attrs, boolean overrides, int indent) {
        Object[] sharedPairs = this.sharedPairs();
        Object[] extraPairs = this.extraPairs();
        sb.append("AttrSet[").append(sharedPairs.length >> 1);
        sb.append(",").append(extraPairs.length >> 1).append("]@");
        sb.append(System.identityHashCode(this));
        if (attrs) {
            Object key;
            Object value;
            int i = 0;
            while (i < sharedPairs.length) {
                sb.append('\n');
                key = ((KeyWrapper)sharedPairs[i++]).key;
                value = sharedPairs[i++];
                AttrSet.appendSpaces(sb, indent + 4);
                sb.append("S ").append(key).append(" => ").append(value);
            }
            if (extraPairs != null) {
                i = 0;
                while (i < extraPairs.length) {
                    sb.append('\n');
                    key = extraPairs[i++];
                    value = extraPairs[i++];
                    AttrSet.appendSpaces(sb, indent + 4);
                    sb.append("E ").append(key).append(" => ").append(value);
                }
            }
            if (overrides && this.overrideCache != null) {
                sb.append('\n');
                AttrSet.appendSpaces(sb, indent + 2);
                sb.append("Overrides:");
                for (Map.Entry<AttrSet, WeakReference<AttrSet>> entry : this.overrideCache.entrySet()) {
                    sb.append('\n');
                    AttrSet key2 = entry.getKey();
                    AttrSet value2 = entry.getValue().get();
                    AttrSet.appendSpaces(sb, indent + 4);
                    key2.appendInfo(sb, true, false, indent + 4).append('\n');
                    AttrSet.appendSpaces(sb, indent + 6);
                    sb.append("=> ");
                    if (value2 != null) {
                        value2.appendInfo(sb, true, false, indent + 8);
                        continue;
                    }
                    sb.append("NULL");
                }
            }
        }
        return sb;
    }

    private static void appendSpaces(StringBuilder sb, int spaceCount) {
        while (--spaceCount >= 0) {
            sb.append(' ');
        }
    }

    static {
        AttrSet.registerSharedKey(StyleConstants.Background, Color.class);
        AttrSet.registerSharedKey(StyleConstants.Foreground, Color.class);
        AttrSet.registerSharedKey(StyleConstants.FontFamily, String.class);
        AttrSet.registerSharedKey(StyleConstants.FontSize, Integer.class);
        AttrSet.registerSharedKey(StyleConstants.Bold, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.Italic, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.Underline, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.StrikeThrough, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.Superscript, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.Subscript, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.Alignment, Boolean.class);
        AttrSet.registerSharedKey(StyleConstants.NameAttribute, String.class);
        AttrSet.registerSharedKey(EditorStyleConstants.WaveUnderlineColor, Color.class);
        AttrSet.registerSharedKey(EditorStyleConstants.DisplayName, String.class);
        AttrSet.registerSharedKey(EditorStyleConstants.Default, String.class);
        AttrSet.registerSharedKey(EditorStyleConstants.TopBorderLineColor, Color.class);
        AttrSet.registerSharedKey(EditorStyleConstants.BottomBorderLineColor, Color.class);
        AttrSet.registerSharedKey(EditorStyleConstants.LeftBorderLineColor, Color.class);
        AttrSet.registerSharedKey(EditorStyleConstants.RightBorderLineColor, Color.class);
        AttrSet.registerSharedKey("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", Boolean.class);
        AttrSet.registerSharedKey("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EMPTY_LINE", Boolean.class);
    }

    private static final class AttrSetBuilder
    implements SimpleWeakSet.ElementProvider<Shared> {
        Object[] sharedPairs;
        Object[] extraPairs;
        int sharedLength;
        int extrasLength;
        int sharedHashCode;
        int extrasHashCode;
        private boolean extrasInEquals;
        private Shared shared;

        AttrSetBuilder(int attrsLength) {
            this.sharedPairs = new Object[attrsLength];
            this.extraPairs = new Object[attrsLength];
        }

        AttrSetBuilder(Object[] sharedSrc, int sharedHashCode, Object[] extrasSrc, int extrasHashCode, int sharedPlus, int extrasPlus) {
            this.sharedPairs = new Object[sharedSrc.length + sharedPlus];
            this.extraPairs = new Object[extrasSrc.length + extrasPlus];
            this.sharedHashCode = sharedHashCode;
            this.extrasHashCode = extrasHashCode;
            this.sharedLength = sharedSrc.length;
            if (this.sharedLength > 0) {
                System.arraycopy(sharedSrc, 0, this.sharedPairs, 0, this.sharedLength);
            }
            this.extrasLength = extrasSrc.length;
            if (this.extrasLength > 0) {
                System.arraycopy(extrasSrc, 0, this.extraPairs, 0, this.extrasLength);
            }
        }

        void add(Object key, Object value) {
            KeyWrapper keyWrapper = AttrSet.sharedKeys.get(key);
            if (keyWrapper != null) {
                this.addShared(keyWrapper, value);
            } else {
                this.addExtra(key, value);
            }
        }

        void addShared(KeyWrapper keyWrapper, Object value) {
            int i = AttrSet.findKeyWrapperIndex(this.sharedPairs, this.sharedLength, keyWrapper);
            if (i < 0) {
                if ((i = - i - 1) < this.sharedLength) {
                    System.arraycopy(this.sharedPairs, i, this.sharedPairs, i + 2, this.sharedLength - i);
                }
                this.sharedPairs[i] = keyWrapper;
                this.sharedHashCode ^= keyWrapper.keyHashCode;
                this.sharedLength += 2;
            } else {
                this.sharedHashCode ^= this.sharedPairs[i + 1].hashCode();
            }
            this.sharedPairs[i + 1] = value;
            this.sharedHashCode ^= value.hashCode();
        }

        void addExtra(Object key, Object value) {
            for (int i = 0; i < this.extrasLength; i += 2) {
                if (!this.extraPairs[i].equals(key)) continue;
                this.extrasHashCode ^= this.extraPairs[i + 1].hashCode();
                this.extraPairs[i + 1] = value;
                this.extrasHashCode ^= value.hashCode();
                return;
            }
            this.extraPairs[this.extrasLength++] = key;
            this.extrasHashCode ^= key.hashCode();
            this.extraPairs[this.extrasLength++] = value;
            this.extrasHashCode ^= value.hashCode();
        }

        AttrSet toAttrSet() {
            AttrSet attrSet2;
            AttrSet attrSet2;
            SimpleWeakSet cacheL = cache;
            AttrSetBuilder elementProvider = this;
            cacheGets++;
            AttrSetBuilder o = cacheL.getOrAdd(this, elementProvider);
            assert (o != null);
            this.shared = (Shared)((Object)o);
            if (this.extrasLength > 0) {
                this.extrasInEquals = true;
                this.extraPairs = this.extrasLength > 0 ? AttrSet.trimArray(this.extraPairs, this.extrasLength) : null;
                attrSet2 = this.shared.cachedOverride(this);
                if (attrSet2 == null) {
                    attrSet2 = new Extra(this.shared, this.extraPairs, this.extrasHashCode);
                    this.shared.addOverride(new Extra(EMPTY, this.extraPairs, this.extrasHashCode), attrSet2);
                }
                this.extrasInEquals = false;
            } else {
                attrSet2 = this.shared;
            }
            return attrSet2;
        }

        @Override
        public Shared createElement() {
            this.sharedPairs = this.sharedLength > 0 ? AttrSet.trimArray(this.sharedPairs, this.sharedLength) : EMPTY_ARRAY;
            cacheMisses++;
            return new Shared(this.sharedPairs, this.sharedHashCode);
        }

        boolean isEqual(AttrSet attrs) {
            if (this.extrasInEquals) {
                if (attrs instanceof Extra) {
                    Extra extra = (Extra)attrs;
                    return extra.shared == EMPTY && extra.isEqualExtraPairs(this.extraPairs, this.extrasLength);
                }
            } else {
                return ((Shared)attrs).isEqualSharedPairs(this.sharedPairs, this.sharedLength);
            }
            return false;
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof AttrSet) {
                return this.isEqual((AttrSet)obj);
            }
            if (obj instanceof AttrSetBuilder) {
                throw new IllegalStateException("Unexpected call - not implemented.");
            }
            return false;
        }

        public int hashCode() {
            return this.extrasInEquals ? this.extrasHashCode : this.sharedHashCode;
        }
    }

    private static final class KeyWrapper {
        private static int orderCounter;
        final Object key;
        final Class valueType;
        final int order;
        final int keyHashCode;

        KeyWrapper(Object key, Class valueType) {
            this.key = key;
            this.valueType = valueType;
            this.order = orderCounter++;
            this.keyHashCode = key.hashCode();
        }

        public String toString() {
            return "key=" + this.key + ", order=" + this.order + ", hash=" + this.keyHashCode;
        }
    }

    private class KeyValueIterator
    implements Iterator<Object> {
        private Object[] sharedPairs;
        private Object[] extraPairs;
        private int index;
        private Object nextKeyOrValue;

        KeyValueIterator(Object[] sharedPairs, Object[] extraPairs) {
            this.sharedPairs = sharedPairs;
            this.extraPairs = extraPairs;
            this.fetchNext();
        }

        @Override
        public boolean hasNext() {
            return this.nextKeyOrValue != null;
        }

        @Override
        public Object next() {
            if (this.nextKeyOrValue == null) {
                throw new NoSuchElementException();
            }
            Object next = this.nextKeyOrValue;
            this.fetchNext();
            return next;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove not allowed.");
        }

        private void fetchNext() {
            if (this.sharedPairs != null) {
                if (this.index < this.sharedPairs.length) {
                    this.nextKeyOrValue = (this.index & 1) == 0 ? ((KeyWrapper)this.sharedPairs[this.index]).key : this.sharedPairs[this.index];
                    ++this.index;
                } else {
                    this.sharedPairs = null;
                    this.index = 0;
                    this.fetchNext();
                }
            } else {
                this.nextKeyOrValue = this.index < this.extraPairs.length ? this.extraPairs[this.index++] : null;
            }
        }
    }

    private class KeysEnumeration
    implements Enumeration<Object> {
        private Object[] sharedPairs;
        private Object[] extraPairs;
        private int index;
        private Object nextKey;

        KeysEnumeration(Object[] sharedPairs, Object[] extraPairs) {
            this.sharedPairs = sharedPairs;
            this.extraPairs = extraPairs;
            this.fetchNextKey();
        }

        @Override
        public boolean hasMoreElements() {
            return this.nextKey != null;
        }

        @Override
        public Object nextElement() {
            if (this.nextKey == null) {
                throw new NoSuchElementException();
            }
            Object next = this.nextKey;
            this.fetchNextKey();
            return next;
        }

        private void fetchNextKey() {
            if (this.sharedPairs != null) {
                if (this.index < this.sharedPairs.length) {
                    this.nextKey = ((KeyWrapper)this.sharedPairs[this.index]).key;
                    this.index += 2;
                } else {
                    this.sharedPairs = null;
                    this.index = 0;
                    this.fetchNextKey();
                }
            } else if (this.index < this.extraPairs.length) {
                this.nextKey = this.extraPairs[this.index];
                this.index += 2;
            } else {
                this.nextKey = null;
            }
        }
    }

    private static final class Extra
    extends AttrSet {
        final Shared shared;
        private final Object[] extraPairs;
        private final int extrasHashCode;

        Extra(Shared shared, Object[] extraPairs, int extrasHashCode) {
            this.shared = shared;
            this.extraPairs = extraPairs;
            this.extrasHashCode = extrasHashCode;
        }

        @Override
        protected Object[] sharedPairs() {
            return this.shared.sharedPairs();
        }

        @Override
        protected Object[] extraPairs() {
            return this.extraPairs;
        }

        @Override
        protected int sharedHashCode() {
            return this.shared.hashCode();
        }

        @Override
        protected int extrasHashCode() {
            return this.extrasHashCode;
        }

        public int hashCode() {
            return this.shared.hashCode() ^ this.extrasHashCode;
        }

        @Override
        public boolean isEqual(AttributeSet attrs) {
            if (attrs == this) {
                return true;
            }
            if (attrs.getClass() == Shared.class) {
                return false;
            }
            if (attrs.getClass() == Extra.class) {
                Extra extra = (Extra)attrs;
                return this.shared == extra.shared && this.isEqualExtraPairs(extra.extraPairs, extra.extraPairs.length);
            }
            return this.isEqual(Extra.toAttrSet(attrs));
        }

        boolean isEqualExtraPairs(Object[] extraPairs2, int extraPairs2Length) {
            if (this.extraPairs.length != extraPairs2Length) {
                return false;
            }
            for (int i = extraPairs2Length - 2; i >= 0; i -= 2) {
                int index = this.findExtraPairsIndex(extraPairs2[i]);
                if (index < 0) {
                    return false;
                }
                if (this.extraPairs[i + 1].equals(extraPairs2[i + 1])) continue;
                return false;
            }
            return true;
        }

        private int findExtraPairsIndex(Object key) {
            if (this.extraPairs != null) {
                for (int i = this.extraPairs.length - 2; i >= 0; i -= 2) {
                    if (!this.extraPairs[i].equals(key)) continue;
                    return i;
                }
            }
            return -1;
        }

        @Override
        public int getAttributeCount() {
            return this.shared.getAttributeCount() + this.extraPairs.length >> 1;
        }

        @Override
        public boolean isDefined(Object key) {
            KeyWrapper keyWrapper = (KeyWrapper)sharedKeys.get(key);
            return keyWrapper != null ? this.shared.findKeyWrapperIndex(keyWrapper) >= 0 : this.findExtraPairsIndex(key) >= 0;
        }

        @Override
        public Object getAttribute(Object key) {
            KeyWrapper keyWrapper = (KeyWrapper)sharedKeys.get(key);
            if (keyWrapper != null) {
                return this.shared.getAttribute(keyWrapper);
            }
            int keyIndex = this.findExtraPairsIndex(key);
            if (keyIndex >= 0) {
                return this.extraPairs[keyIndex + 1];
            }
            return null;
        }
    }

    private static final class Shared
    extends AttrSet {
        private final Object[] sharedPairs;
        private final int hashCode;

        Shared(Object[] sharedPairs, int hashCode) {
            this.sharedPairs = sharedPairs;
            this.hashCode = hashCode;
        }

        @Override
        protected Object[] sharedPairs() {
            return this.sharedPairs;
        }

        @Override
        protected Object[] extraPairs() {
            return EMPTY_ARRAY;
        }

        @Override
        protected int sharedHashCode() {
            return this.hashCode;
        }

        @Override
        protected int extrasHashCode() {
            return 0;
        }

        public int hashCode() {
            return this.hashCode;
        }

        @Override
        public boolean isEqual(AttributeSet attrs) {
            if (attrs == this) {
                return true;
            }
            if (attrs.getClass() == Shared.class) {
                return this == attrs;
            }
            if (attrs.getClass() == Extra.class) {
                return false;
            }
            return this.isEqual(Shared.toAttrSet(attrs));
        }

        boolean isEqualSharedPairs(Object[] sharedPairs2, int sharedPairs2Length) {
            if (this.sharedPairs.length != sharedPairs2Length) {
                return false;
            }
            for (int i = this.sharedPairs.length - 2; i >= 0; i -= 2) {
                if (this.sharedPairs[i] != sharedPairs2[i]) {
                    return false;
                }
                if (this.sharedPairs[i + 1].equals(sharedPairs2[i + 1])) continue;
                return false;
            }
            return true;
        }

        @Override
        public int getAttributeCount() {
            return this.sharedPairs.length >> 1;
        }

        @Override
        public boolean isDefined(Object key) {
            KeyWrapper keyWrapper = (KeyWrapper)sharedKeys.get(key);
            return keyWrapper != null && this.findKeyWrapperIndex(keyWrapper) >= 0;
        }

        int findKeyWrapperIndex(KeyWrapper keyWrapper) {
            return Shared.findKeyWrapperIndex(this.sharedPairs, this.sharedPairs.length, keyWrapper);
        }

        @Override
        public Object getAttribute(Object key) {
            KeyWrapper keyWrapper = (KeyWrapper)sharedKeys.get(key);
            return keyWrapper != null ? this.getAttribute(keyWrapper) : null;
        }

        public Object getAttribute(KeyWrapper keyWrapper) {
            int keyIndex = this.findKeyWrapperIndex(keyWrapper);
            if (keyIndex >= 0) {
                return this.sharedPairs[keyIndex + 1];
            }
            return null;
        }
    }

}

