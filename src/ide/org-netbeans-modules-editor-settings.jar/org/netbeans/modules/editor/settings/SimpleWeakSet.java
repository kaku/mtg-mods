/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.settings;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class SimpleWeakSet<E> {
    private Item<E>[] table;
    private int size;
    private final ReferenceQueue<E> queue = new ReferenceQueue();

    public SimpleWeakSet() {
        this.table = this.newArray(16);
    }

    public int size() {
        if (this.size == 0) {
            return 0;
        }
        this.expungeStaleEntries();
        return this.size;
    }

    public boolean contains(E e) {
        int hashCode = e.hashCode();
        int i = hashCode & this.table.length - 1;
        this.expungeStaleEntries();
        Item<E> item;
        while ((item = this.table[i]) != null) {
            E e2 = item.get();
            if (e2 != null && e2.hashCode() == hashCode && e.equals(e2)) {
                return true;
            }
            i = SimpleWeakSet.nextIndex(i, this.table.length);
        }
        return false;
    }

    public E getOrAdd(E e, ElementProvider<E> eProvider) {
        int hashCode = e.hashCode();
        int i = hashCode & this.table.length - 1;
        this.expungeStaleEntries();
        do {
            Item<E> item;
            if ((item = this.table[i]) == null) {
                if (eProvider != null && (e = eProvider.createElement()) == null) {
                    return null;
                }
                this.table[i] = new Item<E>(e, this.queue, hashCode);
                ++this.size;
                if (this.size >= this.table.length >> 1) {
                    this.resize();
                }
                return e;
            }
            E e2 = item.get();
            if (e2 != null && e2.hashCode() == hashCode && e.equals(e2)) {
                return e2;
            }
            i = SimpleWeakSet.nextIndex(i, this.table.length);
        } while (true);
    }

    private void resize() {
        int newLength = this.table.length << 1;
        Item<E>[] newTable = this.newArray(newLength);
        for (int i = this.table.length - 1; i >= 0; --i) {
            Item<E> item = this.table[i];
            if (item == null) continue;
            this.table[i] = null;
            int ni = item.hashCode & newLength - 1;
            while (newTable[ni] != null) {
                ni = SimpleWeakSet.nextIndex(ni, newLength);
            }
            newTable[ni] = item;
        }
        this.table = newTable;
    }

    public E remove(E e) {
        int hashCode = e.hashCode();
        int i = hashCode & this.table.length - 1;
        this.expungeStaleEntries();
        Item<E> item;
        while ((item = this.table[i]) != null) {
            E e2 = item.get();
            if (e2 != null && e2.hashCode() == hashCode && e.equals(e2)) {
                item.clear();
                this.clearAtIndex(i);
                return e2;
            }
            i = SimpleWeakSet.nextIndex(i, this.table.length);
        }
        return null;
    }

    public void clear() {
        while (this.queue.poll() != null) {
        }
        for (int i = this.table.length; i >= 0; --i) {
            Item<E> item = this.table[i];
            if (item != null) {
                item.clear();
            }
            this.table[i] = null;
        }
        this.size = 0;
    }

    public List<E> asList() {
        this.expungeStaleEntries();
        ArrayList<E> l = new ArrayList<E>(this.size());
        for (Item<E> item : this.table) {
            E e;
            if (item == null || (e = item.get()) == null) continue;
            l.add(e);
        }
        return l;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private void clearAtIndex(int r) {
        this.table[r] = null;
        --this.size;
        i = SimpleWeakSet.nextIndex(r, this.table.length);
        while ((candidate = this.table[i]) != null) {
            c = candidate.hashCode & this.table.length - 1;
            if (r > i) ** GOTO lbl9
            if (r >= c || c > i) ** GOTO lbl-1000
            ** GOTO lbl13
lbl9: // 1 sources:
            if (r >= c && c > i) lbl-1000: // 2 sources:
            {
                this.table[r] = candidate;
                this.table[i] = null;
                r = i;
            }
lbl13: // 4 sources:
            i = SimpleWeakSet.nextIndex(i, this.table.length);
        }
    }

    private void expungeStaleEntries() {
        Item item;
        block0 : while ((item = (Item)this.queue.poll()) != null) {
            int h = item.hashCode;
            int i = h & this.table.length - 1;
            do {
                Item<E> item2;
                if (item == (item2 = this.table[i])) {
                    this.clearAtIndex(i);
                    continue block0;
                }
                if (item2 == null) continue block0;
                i = SimpleWeakSet.nextIndex(i, this.table.length);
            } while (true);
        }
    }

    Item<E>[] newArray(int size) {
        Item[] array = new Item[size];
        return array;
    }

    private static int nextIndex(int i, int length) {
        return ++i < length ? i : 0;
    }

    public String toString() {
        return this.asList().toString();
    }

    private static final class Item<E>
    extends WeakReference<E> {
        final int hashCode;

        Item(E e, ReferenceQueue<E> queue, int hashCode) {
            super(e, queue);
            this.hashCode = hashCode;
        }

        public String toString() {
            return "Item@" + System.identityHashCode(this) + ";e:" + this.get();
        }
    }

    static interface ElementProvider<V> {
        public V createElement();
    }

}

