/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing.support;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.parsing.impl.indexing.DeletedIndexable;
import org.netbeans.modules.parsing.impl.indexing.FileObjectIndexable;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Parameters;

public final class IndexResult {
    private static final Logger LOG = Logger.getLogger(IndexResult.class.getName());
    private final IndexDocument spi;
    private final URL root;
    private volatile URL cachedUrl;
    private volatile FileObject cachedFile;

    IndexResult(IndexDocument spi, URL root) {
        assert (spi != null);
        assert (root != null);
        this.spi = spi;
        this.root = root;
    }

    public String getValue(String key) {
        Parameters.notEmpty((CharSequence)"key", (CharSequence)key);
        return this.spi.getValue(key);
    }

    public String[] getValues(String key) {
        Parameters.notEmpty((CharSequence)"key", (CharSequence)key);
        return this.spi.getValues(key);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public URL getUrl() {
        if (this.cachedUrl == null) {
            URL url = null;
            try {
                url = Util.resolveUrl(this.root, this.spi.getPrimaryKey(), false);
            }
            catch (MalformedURLException ex2) {
                LOG.log(Level.WARNING, null, ex2);
            }
            IndexResult ex2 = this;
            synchronized (ex2) {
                if (this.cachedUrl == null) {
                    this.cachedUrl = url;
                }
            }
        }
        return this.cachedUrl;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public FileObject getFile() {
        if (this.cachedFile == null) {
            FileObject resource = null;
            URL url = this.getUrl();
            if (url != null) {
                resource = URLMapper.findFileObject((URL)url);
            }
            IndexResult indexResult = this;
            synchronized (indexResult) {
                if (this.cachedFile == null) {
                    this.cachedFile = resource;
                }
            }
        }
        return this.cachedFile;
    }

    public String getRelativePath() {
        return this.spi.getPrimaryKey();
    }

    public URL getRoot() {
        return this.root;
    }

    public Indexable getIndexable() {
        FileObject rootFo;
        FileObject file = this.getFile();
        if (file != null && (rootFo = URLMapper.findFileObject((URL)this.root)) != null) {
            return SPIAccessor.getInstance().create(new FileObjectIndexable(rootFo, file));
        }
        return SPIAccessor.getInstance().create(new DeletedIndexable(this.root, this.getRelativePath()));
    }
}

