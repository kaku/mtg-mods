/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SourceIndexerFactory;

public abstract class EmbeddingIndexerFactory
extends SourceIndexerFactory {
    public abstract EmbeddingIndexer createIndexer(Indexable var1, Snapshot var2);
}

