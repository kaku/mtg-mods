/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.net.URL;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.Indexable;

public abstract class SourceIndexerFactory {
    public boolean scanStarted(Context context) {
        return true;
    }

    public void scanFinished(Context context) {
    }

    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    public abstract void filesDeleted(Iterable<? extends Indexable> var1, Context var2);

    public void rootsRemoved(Iterable<? extends URL> removedRoots) {
    }

    public abstract void filesDirty(Iterable<? extends Indexable> var1, Context var2);

    public abstract String getIndexerName();

    public abstract int getIndexVersion();
}

