/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

public class ParseException
extends Exception {
    public ParseException() {
    }

    public ParseException(String message) {
        super(message);
    }

    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }
}

