/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.Callable;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.FileObjectProvider;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.SuspendSupport;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.ConstrainedBinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Parameters;

public final class Indexable {
    private IndexableImpl delegate;

    Indexable(@NonNull IndexableImpl delegate) {
        assert (delegate != null);
        this.delegate = delegate;
    }

    @NonNull
    public String getRelativePath() {
        return this.delegate.getRelativePath();
    }

    @CheckForNull
    public URL getURL() {
        return this.delegate.getURL();
    }

    @NonNull
    public String getMimeType() {
        return this.delegate.getMimeType();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Indexable other = (Indexable)obj;
        return this.delegate.equals(other.delegate);
    }

    public int hashCode() {
        return this.delegate.hashCode();
    }

    public String toString() {
        return this.delegate.toString();
    }

    static {
        SPIAccessor.setInstance(new MyAccessor());
    }

    private static final class MyAccessor
    extends SPIAccessor {
        private MyAccessor() {
        }

        @Override
        public Indexable create(IndexableImpl delegate) {
            return new Indexable(delegate);
        }

        @Override
        public void index(final BinaryIndexer indexer, final Context context) {
            assert (indexer != null);
            assert (context != null);
            RepositoryUpdater.getDefault().runIndexer(new Runnable(){

                @Override
                public void run() {
                    indexer.index(context);
                }
            });
        }

        @Override
        public void index(final CustomIndexer indexer, final Iterable<? extends Indexable> files, final Context context) {
            assert (indexer != null);
            assert (files != null);
            assert (context != null);
            RepositoryUpdater.getDefault().runIndexer(new Runnable(){

                @Override
                public void run() {
                    indexer.index(files, context);
                }
            });
        }

        @Override
        public Context createContext(FileObject indexFolder, URL rootURL, String indexerName, int indexerVersion, IndexFactoryImpl factory, boolean followUpJob, boolean checkForEditorModifications, boolean sourceForBinaryRoot, @NonNull SuspendStatus suspendedStatus, @NullAllowed CancelRequest cancelRequest, @NullAllowed LogContext logContext) throws IOException {
            return new Context(indexFolder, rootURL, indexerName, indexerVersion, factory, followUpJob, checkForEditorModifications, sourceForBinaryRoot, suspendedStatus, cancelRequest, logContext);
        }

        @NonNull
        @Override
        public Context createContext(@NonNull Callable<FileObject> indexFolderFactory, @NonNull URL rootURL, @NonNull String indexerName, int indexerVersion, @NullAllowed IndexFactoryImpl factory, boolean followUpJob, boolean checkForEditorModifications, boolean sourceForBinaryRoot, @NonNull SuspendStatus suspendedStatus, @NullAllowed CancelRequest cancelRequest, @NullAllowed LogContext logContext) throws IOException {
            return new Context(indexFolderFactory, rootURL, indexerName, indexerVersion, factory, followUpJob, checkForEditorModifications, sourceForBinaryRoot, suspendedStatus, cancelRequest, logContext);
        }

        @NonNull
        @Override
        public SuspendStatus createSuspendStatus(@NonNull SuspendSupport.SuspendStatusImpl impl) {
            return new SuspendStatus(impl);
        }

        @Override
        public String getIndexerName(Context ctx) {
            assert (ctx != null);
            return ctx.getIndexerName();
        }

        @Override
        public int getIndexerVersion(Context ctx) {
            assert (ctx != null);
            return ctx.getIndexerVersion();
        }

        @Override
        public void index(final EmbeddingIndexer indexer, final Indexable indexable, final Parser.Result parserResult, final Context ctx) {
            assert (indexer != null);
            assert (indexable != null);
            assert (parserResult != null);
            assert (ctx != null);
            RepositoryUpdater.getDefault().runIndexer(new Runnable(){

                @Override
                public void run() {
                    indexer.index(indexable, parserResult, ctx);
                }
            });
        }

        @Override
        public String getIndexerPath(String indexerName, int indexerVersion) {
            assert (indexerName != null);
            return Context.getIndexerPath(indexerName, indexerVersion);
        }

        @Override
        public IndexFactoryImpl getIndexFactory(Context ctx) {
            assert (ctx != null);
            return ctx.getIndexFactory();
        }

        @Override
        public void context_attachIndexingSupport(Context context, IndexingSupport support) {
            context.attachIndexingSupport(support);
        }

        @Override
        public IndexingSupport context_getAttachedIndexingSupport(Context context) {
            return context.getAttachedIndexingSupport();
        }

        @Override
        public void context_clearAttachedIndexingSupport(Context context) {
            context.clearAttachedIndexingSupport();
        }

        @Override
        public void setAllFilesJob(Context context, boolean allFilesJob) {
            context.setAllFilesJob(allFilesJob);
        }

        @Override
        public void index(@NonNull ConstrainedBinaryIndexer indexer, @NonNull Map<String, ? extends Iterable<? extends FileObject>> files, @NonNull Context context) {
            Parameters.notNull((CharSequence)"indexer", (Object)indexer);
            Parameters.notNull((CharSequence)"files", files);
            Parameters.notNull((CharSequence)"context", (Object)context);
            indexer.index(files, context);
        }

        @Override
        public boolean scanStarted(@NonNull ConstrainedBinaryIndexer indexer, @NonNull Context context) {
            Parameters.notNull((CharSequence)"indexer", (Object)indexer);
            Parameters.notNull((CharSequence)"context", (Object)context);
            return indexer.scanStarted(context);
        }

        @Override
        public void scanFinished(@NonNull ConstrainedBinaryIndexer indexer, @NonNull Context context) {
            Parameters.notNull((CharSequence)"indexer", (Object)indexer);
            Parameters.notNull((CharSequence)"context", (Object)context);
            indexer.scanFinished(context);
        }

        @Override
        public void rootsRemoved(@NonNull ConstrainedBinaryIndexer indexer, @NonNull Iterable<? extends URL> removed) {
            Parameters.notNull((CharSequence)"indexer", (Object)indexer);
            Parameters.notNull((CharSequence)"removed", removed);
            indexer.rootsRemoved(removed);
        }

        @Override
        public void putProperty(@NonNull Context context, @NonNull String propName, @NullAllowed Object value) {
            context.putProperty(propName, value);
        }

        @Override
        public Object getProperty(@NonNull Context context, @NonNull String propName) {
            return context.getProperty(propName);
        }

        @Override
        public boolean isTypeOf(@NonNull Indexable indexable, @NonNull String mimeType) {
            return indexable.delegate.isTypeOf(mimeType);
        }

        @Override
        public FileObject getFileObject(@NonNull Indexable indexable) {
            return indexable.delegate instanceof FileObjectProvider ? ((FileObjectProvider)((Object)indexable.delegate)).getFileObject() : URLMapper.findFileObject((URL)indexable.getURL());
        }

    }

}

