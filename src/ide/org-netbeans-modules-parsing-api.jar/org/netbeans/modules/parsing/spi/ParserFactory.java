/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import java.util.Collection;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;

public abstract class ParserFactory {
    public abstract Parser createParser(Collection<Snapshot> var1);
}

