/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.lucene.LayeredDocumentIndex;
import org.netbeans.modules.parsing.impl.indexing.lucene.LuceneIndexFactory;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public final class Context {
    private final URL rootURL;
    private final String indexerName;
    private final int indexerVersion;
    private final boolean followUpJob;
    private final boolean checkForEditorModifications;
    private final boolean sourceForBinaryRoot;
    private final CancelRequest cancelRequest;
    private final SuspendStatus suspendedStatus;
    private final LogContext logContext;
    private final Map<String, Object> props;
    private final Callable<FileObject> indexBaseFolderFactory;
    private FileObject indexFolder;
    private boolean allFilesJob;
    private FileObject root;
    private FileObject indexBaseFolder;
    private IndexingSupport indexingSupport;
    private final IndexFactoryImpl factory;

    Context(@NonNull FileObject indexBaseFolder, @NonNull URL rootURL, @NonNull String indexerName, int indexerVersion, @NullAllowed IndexFactoryImpl factory, boolean followUpJob, boolean checkForEditorModifications, boolean sourceForBinaryRoot, @NonNull SuspendStatus suspendedStatus, @NullAllowed CancelRequest cancelRequest, @NullAllowed LogContext logContext) throws IOException {
        assert (indexBaseFolder != null);
        assert (rootURL != null);
        assert (indexerName != null);
        this.indexBaseFolderFactory = null;
        this.indexBaseFolder = indexBaseFolder;
        this.rootURL = rootURL;
        this.indexerName = indexerName;
        this.indexerVersion = indexerVersion;
        this.factory = factory != null ? factory : LuceneIndexFactory.getDefault();
        this.followUpJob = followUpJob;
        this.checkForEditorModifications = checkForEditorModifications;
        this.sourceForBinaryRoot = sourceForBinaryRoot;
        this.cancelRequest = cancelRequest;
        this.suspendedStatus = suspendedStatus;
        this.logContext = logContext;
        this.props = new HashMap<String, Object>();
    }

    Context(@NonNull Callable<FileObject> indexBaseFolderFactory, @NonNull URL rootURL, @NonNull String indexerName, int indexerVersion, @NullAllowed IndexFactoryImpl factory, boolean followUpJob, boolean checkForEditorModifications, boolean sourceForBinaryRoot, @NonNull SuspendStatus suspendedStatus, @NullAllowed CancelRequest cancelRequest, @NullAllowed LogContext logContext) throws IOException {
        assert (indexBaseFolderFactory != null);
        assert (rootURL != null);
        assert (indexerName != null);
        this.indexBaseFolderFactory = indexBaseFolderFactory;
        this.rootURL = rootURL;
        this.indexerName = indexerName;
        this.indexerVersion = indexerVersion;
        this.factory = factory != null ? factory : LuceneIndexFactory.getDefault();
        this.followUpJob = followUpJob;
        this.checkForEditorModifications = checkForEditorModifications;
        this.sourceForBinaryRoot = sourceForBinaryRoot;
        this.cancelRequest = cancelRequest;
        this.suspendedStatus = suspendedStatus;
        this.logContext = logContext;
        this.props = new HashMap<String, Object>();
    }

    public FileObject getIndexFolder() {
        if (this.indexFolder == null) {
            try {
                String path = Context.getIndexerPath(this.indexerName, this.indexerVersion);
                if (this.indexBaseFolder == null) {
                    try {
                        this.indexBaseFolder = this.indexBaseFolderFactory.call();
                    }
                    catch (Exception ex) {
                        throw new IllegalStateException(ex);
                    }
                    if (this.indexBaseFolder == null) {
                        throw new IllegalStateException(String.format("Factory %s returned null index base folder.", this.indexBaseFolderFactory));
                    }
                }
                this.indexFolder = FileUtil.createFolder((FileObject)this.indexBaseFolder, (String)path);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
        return this.indexFolder;
    }

    public URL getRootURI() {
        return this.rootURL;
    }

    public FileObject getRoot() {
        if (this.root == null) {
            this.root = URLMapper.findFileObject((URL)this.rootURL);
        }
        return this.root;
    }

    public void addSupplementaryFiles(URL root, Collection<? extends URL> files) {
        Logger repouLogger = Logger.getLogger(RepositoryUpdater.class.getName());
        if (repouLogger.isLoggable(Level.FINE)) {
            repouLogger.fine("addSupplementaryFiles: root=" + root + ", files=" + files);
        }
        RepositoryUpdater.getDefault().addIndexingJob(root, files, true, false, false, true, true, LogContext.create(LogContext.EventType.INDEXER, null, this.logContext).withRoot(root).addFiles(files));
    }

    public boolean isSupplementaryFilesIndexing() {
        return this.followUpJob;
    }

    public boolean isAllFilesIndexing() {
        return this.allFilesJob;
    }

    public boolean isSourceForBinaryRootIndexing() {
        return this.sourceForBinaryRoot;
    }

    public boolean checkForEditorModifications() {
        return this.checkForEditorModifications;
    }

    public boolean isCancelled() {
        return this.cancelRequest != null ? this.cancelRequest.isRaised() : false;
    }

    @NonNull
    public SuspendStatus getSuspendStatus() {
        return this.suspendedStatus;
    }

    IndexFactoryImpl getIndexFactory() {
        return this.factory;
    }

    String getIndexerName() {
        return this.indexerName;
    }

    int getIndexerVersion() {
        return this.indexerVersion;
    }

    void attachIndexingSupport(IndexingSupport support) {
        assert (this.indexingSupport == null);
        this.indexingSupport = support;
        try {
            LayeredDocumentIndex index = this.factory.getIndex(this.getIndexFolder());
            if (index != null) {
                index.begin();
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
    }

    IndexingSupport getAttachedIndexingSupport() {
        return this.indexingSupport;
    }

    void clearAttachedIndexingSupport() {
        this.indexingSupport = null;
    }

    void setAllFilesJob(boolean allFilesJob) {
        this.allFilesJob = allFilesJob;
    }

    void putProperty(@NonNull String propName, @NullAllowed Object value) {
        Parameters.notNull((CharSequence)"propName", (Object)propName);
        this.props.put(propName, value);
    }

    Object getProperty(@NonNull String propName) {
        Parameters.notNull((CharSequence)"propName", (Object)propName);
        return this.props.get(propName);
    }

    static String getIndexerPath(String indexerName, int indexerVersion) {
        StringBuilder sb = new StringBuilder();
        sb.append(indexerName);
        sb.append('/');
        sb.append(indexerVersion);
        return sb.toString();
    }
}

