/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import java.util.Collection;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public abstract class TaskFactory {
    public abstract Collection<? extends SchedulerTask> create(Snapshot var1);
}

