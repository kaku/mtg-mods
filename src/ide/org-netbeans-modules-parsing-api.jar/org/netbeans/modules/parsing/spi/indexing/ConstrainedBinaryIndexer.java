/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URL;
import java.util.Map;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.ProxyBinaryIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.openide.filesystems.FileObject;

public abstract class ConstrainedBinaryIndexer {
    protected abstract void index(@NonNull Map<String, ? extends Iterable<? extends FileObject>> var1, @NonNull Context var2);

    protected boolean scanStarted(@NonNull Context context) {
        return true;
    }

    protected void scanFinished(@NonNull Context context) {
    }

    protected void rootsRemoved(@NonNull Iterable<? extends URL> removedRoots) {
    }

    private static BinaryIndexerFactory create(Map<String, Object> params) {
        return new ProxyBinaryIndexerFactory(params);
    }

    @Target(value={ElementType.TYPE})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface Registration {
        public String indexerName();

        public int indexVersion();

        public String[] requiredResource() default {};

        public String[] mimeType() default {};

        public String namePattern() default "";
    }

}

