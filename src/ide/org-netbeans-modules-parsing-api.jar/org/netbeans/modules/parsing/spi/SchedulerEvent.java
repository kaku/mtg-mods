/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import java.util.EventObject;

public class SchedulerEvent
extends EventObject {
    protected SchedulerEvent(Object source) {
        super(source);
    }

    @Override
    public String toString() {
        return "SchedulerEvent " + this.hashCode() + "(source: " + this.source + ")";
    }
}

