/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.support;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.openide.util.Parameters;

public final class CancelSupport {
    private final SchedulerTask owner;

    private CancelSupport(@NonNull SchedulerTask owner) {
        Parameters.notNull((CharSequence)"owner", (Object)owner);
        this.owner = owner;
    }

    public boolean isCancelled() {
        return TaskProcessor.isCancelled(this.owner);
    }

    @NonNull
    public static CancelSupport create(@NonNull SchedulerTask owner) {
        return new CancelSupport(owner);
    }
}

