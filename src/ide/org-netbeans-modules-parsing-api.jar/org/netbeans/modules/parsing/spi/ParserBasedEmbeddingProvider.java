/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import java.util.List;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public abstract class ParserBasedEmbeddingProvider<T extends Parser.Result>
extends SchedulerTask {
    public abstract List<Embedding> getEmbeddings(T var1);

    @Override
    public abstract int getPriority();
}

