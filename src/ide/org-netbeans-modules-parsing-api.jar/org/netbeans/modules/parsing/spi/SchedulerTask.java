/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.Scheduler;

public abstract class SchedulerTask
extends Task {
    SchedulerTask() {
    }

    public abstract int getPriority();

    public abstract Class<? extends Scheduler> getSchedulerClass();

    public abstract void cancel();
}

