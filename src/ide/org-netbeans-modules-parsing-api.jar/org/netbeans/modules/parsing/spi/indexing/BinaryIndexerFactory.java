/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.net.URL;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.Context;

public abstract class BinaryIndexerFactory {
    public abstract BinaryIndexer createIndexer();

    public abstract void rootsRemoved(Iterable<? extends URL> var1);

    public abstract String getIndexerName();

    public abstract int getIndexVersion();

    public boolean scanStarted(Context context) {
        return true;
    }

    public void scanFinished(Context context) {
    }
}

