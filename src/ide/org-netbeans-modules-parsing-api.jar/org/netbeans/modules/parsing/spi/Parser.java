/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.parsing.spi;

import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.impl.ParserAccessor;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

public abstract class Parser {
    public abstract void parse(Snapshot var1, Task var2, SourceModificationEvent var3) throws ParseException;

    public abstract Result getResult(Task var1) throws ParseException;

    @Deprecated
    public void cancel() {
    }

    public void cancel(@NonNull CancelReason reason, @NullAllowed SourceModificationEvent event) {
    }

    public abstract void addChangeListener(ChangeListener var1);

    public abstract void removeChangeListener(ChangeListener var1);

    static {
        ParserAccessor.setINSTANCE(new MyAccessor());
    }

    private static class MyAccessor
    extends ParserAccessor {
        private MyAccessor() {
        }

        @Override
        public void invalidate(Result result) {
            assert (result != null);
            result.invalidate();
        }
    }

    public static enum CancelReason {
        SOURCE_MODIFICATION_EVENT,
        USER_TASK,
        PARSER_RESULT_TASK;
        

        private CancelReason() {
        }
    }

    public static abstract class Result {
        private final Snapshot snapshot;

        protected Result(Snapshot _snapshot) {
            this.snapshot = _snapshot;
        }

        public Snapshot getSnapshot() {
            return this.snapshot;
        }

        protected abstract void invalidate();
    }

}

