/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import org.netbeans.modules.parsing.spi.SchedulerEvent;

public class CursorMovedSchedulerEvent
extends SchedulerEvent {
    private final int caretOffset;
    private final int markOffset;

    protected CursorMovedSchedulerEvent(Object source, int _caretOffset, int _markOffset) {
        super(source);
        this.caretOffset = _caretOffset;
        this.markOffset = _markOffset;
    }

    public int getCaretOffset() {
        return this.caretOffset;
    }

    public int getMarkOffset() {
        return this.markOffset;
    }

    @Override
    public String toString() {
        return "CursorMovedSchedulerEvent " + this.hashCode() + "(source: " + this.source + ", cursor: " + this.caretOffset + ", mark: " + this.markOffset + ")";
    }
}

