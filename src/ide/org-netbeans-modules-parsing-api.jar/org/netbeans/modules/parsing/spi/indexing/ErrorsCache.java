/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.netbeans.modules.parsing.impl.indexing.errors.TaskCache;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;

public class ErrorsCache {
    public static <T> void setErrors(URL root, Indexable i, Iterable<? extends T> errors, Convertor<T> convertor) {
        TaskCache.getDefault().dumpErrors(root, i, errors, convertor);
    }

    public static boolean isInError(FileObject file, boolean recursive) {
        return TaskCache.getDefault().isInError(file, recursive);
    }

    public static Collection<? extends URL> getAllFilesInError(URL root) throws IOException {
        return Collections.unmodifiableCollection(TaskCache.getDefault().getAllFilesInError(root));
    }

    private ErrorsCache() {
    }

    public static enum ErrorKind {
        ERROR,
        ERROR_NO_BADGE,
        WARNING;
        

        private ErrorKind() {
        }
    }

    public static interface Convertor<T> {
        public ErrorKind getKind(T var1);

        public int getLineNumber(T var1);

        public String getMessage(T var1);
    }

}

