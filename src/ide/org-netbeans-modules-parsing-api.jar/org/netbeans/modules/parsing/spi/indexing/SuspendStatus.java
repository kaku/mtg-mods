/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.SuspendSupport;
import org.openide.util.Parameters;

public final class SuspendStatus {
    private final SuspendSupport.SuspendStatusImpl impl;

    SuspendStatus(@NonNull SuspendSupport.SuspendStatusImpl impl) {
        Parameters.notNull((CharSequence)"impl", (Object)impl);
        this.impl = impl;
    }

    public boolean isSuspendSupported() {
        return this.impl.isSuspendSupported();
    }

    public boolean isSuspended() {
        return this.impl.isSuspended();
    }

    public void parkWhileSuspended() throws InterruptedException {
        this.impl.parkWhileSuspended();
    }
}

