/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$Status
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing.support;

import java.io.IOException;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.ClusteredIndexables;
import org.netbeans.modules.parsing.impl.indexing.FileObjectIndexable;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.lucene.LayeredDocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexDocument;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public final class IndexingSupport {
    private static final Logger LOG = Logger.getLogger(IndexingSupport.class.getName());
    private final Context context;
    private final IndexFactoryImpl spiFactory;
    private final DocumentIndex spiIndex;

    private IndexingSupport(Context ctx) throws IOException {
        IndexFactoryImpl factory = SPIAccessor.getInstance().getIndexFactory(ctx);
        assert (factory != null);
        this.context = ctx;
        this.spiFactory = factory;
        this.spiIndex = this.spiFactory.createIndex(ctx);
    }

    @NonNull
    public static IndexingSupport getInstance(@NonNull Context context) throws IOException {
        Parameters.notNull((CharSequence)"context", (Object)context);
        IndexingSupport support = SPIAccessor.getInstance().context_getAttachedIndexingSupport(context);
        if (support == null) {
            support = new IndexingSupport(context);
            SPIAccessor.getInstance().context_attachIndexingSupport(context, support);
        }
        support.attachToCacheIfNeeded();
        return support;
    }

    public boolean isValid() {
        try {
            return this.spiIndex != null && this.spiIndex.getStatus() != Index.Status.INVALID;
        }
        catch (IOException e) {
            return false;
        }
    }

    public IndexDocument createDocument(FileObject file) {
        FileObject root = this.context.getRoot();
        if (FileUtil.isParentOf((FileObject)root, (FileObject)file)) {
            return this.createDocument(SPIAccessor.getInstance().create(new FileObjectIndexable(root, file)));
        }
        throw new IllegalArgumentException((Object)file + " is not under the root " + (Object)root);
    }

    public IndexDocument createDocument(Indexable indexable) {
        Parameters.notNull((CharSequence)"indexable", (Object)indexable);
        return new IndexDocument(this.spiFactory.createDocument(indexable));
    }

    public void addDocument(IndexDocument document) {
        Parameters.notNull((CharSequence)"document", (Object)document.spi);
        if (this.spiIndex != null) {
            this.spiIndex.addDocument(document.spi);
        }
    }

    public void removeDocuments(Indexable indexable) {
        Parameters.notNull((CharSequence)"indexable", (Object)indexable);
        if (this.spiIndex != null) {
            this.spiIndex.removeDocument(indexable.getRelativePath());
        }
    }

    public void markDirtyDocuments(Indexable indexable) {
        Parameters.notNull((CharSequence)"indexable", (Object)indexable);
        if (this.spiIndex != null) {
            this.spiIndex.markKeyDirty(indexable.getRelativePath());
        }
    }

    private boolean attachToCacheIfNeeded() throws IOException {
        ClusteredIndexables indexCi;
        DocumentIndexCache cache = this.spiFactory.getCache(this.context);
        if (!(cache instanceof ClusteredIndexables.AttachableDocumentIndexCache)) {
            return false;
        }
        boolean res = false;
        ClusteredIndexables.AttachableDocumentIndexCache ciCache = (ClusteredIndexables.AttachableDocumentIndexCache)cache;
        ClusteredIndexables delCi = (ClusteredIndexables)SPIAccessor.getInstance().getProperty(this.context, "ci-delete-set");
        if (delCi != null) {
            ciCache.attach("ci-delete-set", delCi);
            res = true;
        }
        if ((indexCi = (ClusteredIndexables)SPIAccessor.getInstance().getProperty(this.context, "ci-index-set")) != null) {
            ciCache.attach("ci-index-set", indexCi);
            res = true;
        }
        return res;
    }
}

