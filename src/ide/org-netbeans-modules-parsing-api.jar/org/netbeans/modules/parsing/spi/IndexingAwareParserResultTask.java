/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.util.Parameters;

public abstract class IndexingAwareParserResultTask<T extends Parser.Result>
extends ParserResultTask<T> {
    private final TaskIndexingMode scanMode;

    protected IndexingAwareParserResultTask(@NonNull TaskIndexingMode scanMode) {
        Parameters.notNull((CharSequence)"scanMode", (Object)((Object)scanMode));
        this.scanMode = scanMode;
    }

    public final TaskIndexingMode getIndexingMode() {
        return this.scanMode;
    }
}

