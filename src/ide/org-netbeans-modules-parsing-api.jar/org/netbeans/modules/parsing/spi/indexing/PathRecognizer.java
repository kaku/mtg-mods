/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.util.Set;

public abstract class PathRecognizer {
    public abstract Set<String> getSourcePathIds();

    public abstract Set<String> getLibraryPathIds();

    public abstract Set<String> getBinaryLibraryPathIds();

    public abstract Set<String> getMimeTypes();
}

