/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

public enum TaskIndexingMode {
    ALLOWED_DURING_SCAN,
    DISALLOWED_DURING_SCAN;
    

    private TaskIndexingMode() {
    }
}

