/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.TYPE})
@Retention(value=RetentionPolicy.SOURCE)
public @interface PathRecognizerRegistration {
    public String[] sourcePathIds() default {"ANY"};

    public String[] libraryPathIds() default {"ANY"};

    public String[] binaryLibraryPathIds() default {"ANY"};

    public String[] mimeTypes() default {};
}

