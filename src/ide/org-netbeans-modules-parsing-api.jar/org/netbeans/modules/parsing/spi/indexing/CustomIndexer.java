/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.Indexable;

public abstract class CustomIndexer {
    protected abstract void index(Iterable<? extends Indexable> var1, Context var2);
}

