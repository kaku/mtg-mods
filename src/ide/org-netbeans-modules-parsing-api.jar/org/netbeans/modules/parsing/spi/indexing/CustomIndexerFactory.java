/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.SourceIndexerFactory;

public abstract class CustomIndexerFactory
extends SourceIndexerFactory {
    public abstract CustomIndexer createIndexer();

    public abstract boolean supportsEmbeddedIndexers();
}

