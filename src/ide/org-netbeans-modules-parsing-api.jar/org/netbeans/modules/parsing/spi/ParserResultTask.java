/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public abstract class ParserResultTask<T extends Parser.Result>
extends SchedulerTask {
    public abstract void run(T var1, SchedulerEvent var2);

    @Override
    public abstract int getPriority();
}

