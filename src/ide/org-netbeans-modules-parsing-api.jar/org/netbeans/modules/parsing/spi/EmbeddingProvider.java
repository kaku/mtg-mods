/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerTask;

public abstract class EmbeddingProvider
extends SchedulerTask {
    @Override
    public final Class<? extends Scheduler> getSchedulerClass() {
        return null;
    }

    public abstract List<Embedding> getEmbeddings(Snapshot var1);

    @Override
    public abstract int getPriority();

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE})
    public static @interface Registration {
        public String mimeType();

        public String targetMimeType();
    }

}

