/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.spi;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.CurrentDocumentScheduler;
import org.netbeans.modules.parsing.impl.CursorSensitiveScheduler;
import org.netbeans.modules.parsing.impl.SchedulerAccessor;
import org.netbeans.modules.parsing.impl.SelectedNodesScheduler;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public abstract class Scheduler {
    public static final int DEFAULT_REPARSE_DELAY = 500;
    private static final Logger LOG = Logger.getLogger(Scheduler.class.getName());
    private final PropertyChangeListener listener;
    int reparseDelay;
    private Source source;
    private PropertyChangeListener wlistener;
    public static final Class<? extends Scheduler> CURSOR_SENSITIVE_TASK_SCHEDULER = CursorSensitiveScheduler.class;
    public static final Class<? extends Scheduler> EDITOR_SENSITIVE_TASK_SCHEDULER = CurrentDocumentScheduler.class;
    public static final Class<? extends Scheduler> SELECTED_NODES_SENSITIVE_TASK_SCHEDULER = SelectedNodesScheduler.class;
    private RequestProcessor requestProcessor;
    private RequestProcessor.Task task;

    public Scheduler() {
        this.listener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Source newSource;
                DataObject dobj;
                if ("primaryFile".equals(evt.getPropertyName()) && (newSource = Source.create((dobj = (DataObject)evt.getSource()).getPrimaryFile())) != null) {
                    LOG.log(Level.FINE, "Rescheduling {0} due to change of primary file.", (Object)dobj.getPrimaryFile());
                    Scheduler.this.schedule(newSource, new SchedulerEvent(newSource));
                }
            }
        };
        this.reparseDelay = 500;
    }

    protected final synchronized void schedule(SchedulerEvent event) {
        if (this.source != null) {
            this.schedule(this.source, event);
        }
    }

    protected final synchronized void schedule(final Source source, final SchedulerEvent event) {
        if (this.task != null) {
            this.task.cancel();
        }
        this.task = null;
        if (this.requestProcessor == null) {
            this.requestProcessor = new RequestProcessor(Scheduler.class.getName(), 1, false, false);
        }
        if (this.source != source) {
            FileObject fo;
            if (this.source != null) {
                FileObject fo2;
                SourceCache cache = SourceAccessor.getINSTANCE().getCache(this.source);
                cache.unscheduleTasks(this.getClass());
                if (this.wlistener != null && (fo2 = this.source.getFileObject()) != null) {
                    try {
                        DataObject dobj = DataObject.find((FileObject)fo2);
                        dobj.removePropertyChangeListener(this.wlistener);
                    }
                    catch (DataObjectNotFoundException nfe) {
                        // empty catch block
                    }
                }
            }
            this.source = source;
            if (source != null && (fo = source.getFileObject()) != null) {
                try {
                    DataObject dobj = DataObject.find((FileObject)fo);
                    this.wlistener = WeakListeners.propertyChange((PropertyChangeListener)this.listener, (Object)dobj);
                    dobj.addPropertyChangeListener(this.wlistener);
                }
                catch (DataObjectNotFoundException ex) {
                    // empty catch block
                }
            }
        }
        if (source == null) {
            return;
        }
        this.task = this.requestProcessor.create(new Runnable(){

            @Override
            public void run() {
                SourceCache cache = SourceAccessor.getINSTANCE().getCache(source);
                SourceAccessor.getINSTANCE().setSchedulerEvent(source, Scheduler.this, event);
                cache.scheduleTasks(Scheduler.this.getClass());
            }
        });
        this.task.schedule(this.reparseDelay);
    }

    @CheckForNull
    protected final synchronized Source getSource() {
        return this.source;
    }

    protected abstract SchedulerEvent createSchedulerEvent(SourceModificationEvent var1);

    static {
        SchedulerAccessor.set(new Accessor());
    }

    private static class Accessor
    extends SchedulerAccessor {
        private Accessor() {
        }

        @Override
        public SchedulerEvent createSchedulerEvent(Scheduler scheduler, SourceModificationEvent event) {
            return scheduler.createSchedulerEvent(event);
        }
    }

}

