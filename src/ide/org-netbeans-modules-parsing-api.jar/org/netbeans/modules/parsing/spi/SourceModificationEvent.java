/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi;

import java.util.EventObject;
import org.netbeans.modules.parsing.api.Source;

public class SourceModificationEvent
extends EventObject {
    private final boolean sourceChanged;

    @Deprecated
    protected SourceModificationEvent(Object source) {
        this(source, true);
    }

    protected SourceModificationEvent(Object source, boolean sourceChanged) {
        super(source);
        this.sourceChanged = sourceChanged;
    }

    public Source getModifiedSource() {
        return (Source)this.getSource();
    }

    public boolean sourceChanged() {
        return this.sourceChanged;
    }

    @Override
    public String toString() {
        return "SourceModificationEvent " + this.hashCode() + "(source: " + this.source + ")";
    }
}

