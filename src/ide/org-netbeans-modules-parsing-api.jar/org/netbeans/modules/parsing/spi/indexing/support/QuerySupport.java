/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.search.BooleanClause
 *  org.apache.lucene.search.BooleanClause$Occur
 *  org.apache.lucene.search.BooleanQuery
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$IndexClosedException
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.netbeans.modules.parsing.lucene.support.Queries
 *  org.netbeans.modules.parsing.lucene.support.Queries$QueryKind
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing.support;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.parsing.impl.Installer;
import org.netbeans.modules.parsing.impl.RunWhenScanFinishedSupport;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.PathRecognizerRegistry;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.TransientUpdateSupport;
import org.netbeans.modules.parsing.impl.indexing.URLCache;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.parsing.impl.indexing.lucene.LayeredDocumentIndex;
import org.netbeans.modules.parsing.impl.indexing.lucene.LuceneIndexFactory;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex2;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Pair;
import org.openide.util.Parameters;

public final class QuerySupport {
    private static final Logger LOG = Logger.getLogger(QuerySupport.class.getName());
    private final IndexerQuery indexerQuery;
    private final List<URL> roots;

    @NonNull
    public static Collection<FileObject> findRoots(FileObject f, Collection<String> sourcePathIds, Collection<String> libraryPathIds, Collection<String> binaryLibraryPathIds) {
        Set<FileObject> roots = QuerySupport.collectClasspathRoots(f, sourcePathIds, libraryPathIds, binaryLibraryPathIds);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Roots for file {0}, sourcePathIds={1}, libraryPathIds={2}, binaryPathIds={3}: ", new Object[]{f, sourcePathIds, libraryPathIds, binaryLibraryPathIds});
            for (FileObject root : roots) {
                LOG.log(Level.FINE, "  {0}", root.toURL());
            }
            LOG.fine("----");
        }
        return roots;
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public static Collection<FileObject> findDependentRoots(@NonNull FileObject file, boolean filterNonOpenedProjects) {
        Set urls;
        Parameters.notNull((CharSequence)"file", (Object)file);
        URL root = QuerySupport.findOwnerRoot(file);
        if (root == null) {
            return Collections.emptySet();
        }
        IndexingController ic = IndexingController.getDefault();
        Map<URL, List<URL>> binaryDeps = ic.getBinaryRootDependencies();
        Map<URL, List<URL>> sourceDeps = ic.getRootDependencies();
        Map<URL, List<URL>> peerDeps = ic.getRootPeers();
        if (sourceDeps.containsKey(root)) {
            urls = Util.findReverseSourceRoots(root, sourceDeps, peerDeps);
        } else {
            urls = new HashSet();
            FileObject rootFo = URLMapper.findFileObject((URL)root);
            if (rootFo != null) {
                for (URL binary : QuerySupport.findBinaryRootsForSourceRoot(rootFo, binaryDeps)) {
                    List<URL> deps = binaryDeps.get(binary);
                    if (deps == null) continue;
                    urls.addAll(deps);
                }
            }
        }
        if (filterNonOpenedProjects) {
            GlobalPathRegistry gpr = GlobalPathRegistry.getDefault();
            HashSet cps = new HashSet();
            for (String id : PathRecognizerRegistry.getDefault().getSourceIds()) {
                cps.addAll(gpr.getPaths(id));
            }
            HashSet<URL> toRetain = new HashSet<URL>();
            for (ClassPath cp : cps) {
                for (ClassPath.Entry e : cp.entries()) {
                    toRetain.add(e.getURL());
                }
            }
            urls.retainAll(toRetain);
        }
        return QuerySupport.mapToFileObjects(urls);
    }

    @NonNull
    public static Collection<FileObject> findRoots(@NullAllowed Project project, @NullAllowed Collection<String> sourcePathIds, @NullAllowed Collection<String> libraryPathIds, @NullAllowed Collection<String> binaryLibraryPathIds) {
        Collection roots = QuerySupport.collectClasspathRoots(null, sourcePathIds, libraryPathIds, binaryLibraryPathIds);
        if (project != null && (roots = QuerySupport.reduceRootsByProjects(roots, Collections.singleton(project)).get((Object)project)) == null) {
            roots = Collections.emptySet();
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Roots for project {0}, sourcePathIds={1}, libraryPathIds={2}, binaryPathIds={3}: ", new Object[]{project, sourcePathIds, libraryPathIds, binaryLibraryPathIds});
            for (FileObject root : roots) {
                LOG.log(Level.FINE, "  {0}", root.toURL());
            }
            LOG.fine("----");
        }
        return roots;
    }

    @NonNull
    public static Map<Project, Collection<FileObject>> findRoots(@NonNull Collection<? extends Project> projects, @NullAllowed Collection<String> sourcePathIds, @NullAllowed Collection<String> libraryPathIds, @NullAllowed Collection<String> binaryLibraryPathIds) {
        Parameters.notNull((CharSequence)"projects", projects);
        Set<FileObject> roots = QuerySupport.collectClasspathRoots(null, sourcePathIds, libraryPathIds, binaryLibraryPathIds);
        Map<Project, Collection<FileObject>> rbp = QuerySupport.reduceRootsByProjects(roots, projects);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Roots for projects {0}, sourcePathIds={1}, libraryPathIds={2}, binaryPathIds={3}: ", new Object[]{projects, sourcePathIds, libraryPathIds, binaryLibraryPathIds});
            for (Map.Entry<Project, Collection<FileObject>> e : rbp.entrySet()) {
                LOG.log(Level.FINE, "\tProject: {0}:", (Object)e.getKey());
                for (FileObject root : e.getValue()) {
                    LOG.log(Level.FINE, "\t\t{0}", root.toURL());
                }
            }
            LOG.fine("----");
        }
        return rbp;
    }

    public static /* varargs */ QuerySupport forRoots(String indexerName, int indexerVersion, URL ... roots) throws IOException {
        Parameters.notNull((CharSequence)"indexerName", (Object)indexerName);
        Parameters.notNull((CharSequence)"roots", (Object)roots);
        return new QuerySupport(indexerName, indexerVersion, roots);
    }

    public static /* varargs */ QuerySupport forRoots(String indexerName, int indexerVersion, FileObject ... roots) throws IOException {
        Parameters.notNull((CharSequence)"indexerName", (Object)indexerName);
        Parameters.notNull((CharSequence)"roots", (Object)roots);
        ArrayList<URL> rootsURL = new ArrayList<URL>(roots.length);
        for (FileObject root : roots) {
            rootsURL.add(root.toURL());
        }
        return new QuerySupport(indexerName, indexerVersion, rootsURL.toArray(new URL[rootsURL.size()]));
    }

    public /* varargs */ Collection<? extends IndexResult> query(String fieldName, String fieldValue, Kind kind, String ... fieldsToLoad) throws IOException {
        Parameters.notNull((CharSequence)"fieldName", (Object)fieldName);
        Parameters.notNull((CharSequence)"fieldValue", (Object)fieldValue);
        Parameters.notNull((CharSequence)"kind", (Object)((Object)kind));
        return this.getQueryFactory().field(fieldName, fieldValue, kind).execute(fieldsToLoad);
    }

    @NonNull
    public Query.Factory getQueryFactory() {
        return new Query.Factory(this);
    }

    private /* varargs */ QuerySupport(String indexerName, int indexerVersion, URL ... roots) throws IOException {
        this.indexerQuery = IndexerQuery.forIndexer(indexerName, indexerVersion);
        this.roots = new ArrayList<URL>(Arrays.asList(roots));
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "{0}@{1}[indexer={2}]:", new Object[]{this.getClass().getSimpleName(), Integer.toHexString(System.identityHashCode(this)), this.indexerQuery.getIndexerId()});
            for (Pair<URL, LayeredDocumentIndex> pair : this.indexerQuery.getIndices(this.roots)) {
                LOG.log(Level.FINE, " {0} -> index: {1}", new Object[]{pair.first(), pair.second()});
            }
            LOG.fine("----");
        }
    }

    @NonNull
    private static Set<FileObject> collectClasspathRoots(@NullAllowed FileObject file, @NullAllowed Collection<String> sourcePathIds, @NullAllowed Collection<String> libraryPathIds, @NullAllowed Collection<String> binaryLibraryPathIds) {
        HashSet<FileObject> roots = new HashSet<FileObject>();
        if (sourcePathIds == null) {
            sourcePathIds = PathRecognizerRegistry.getDefault().getSourceIds();
        }
        if (libraryPathIds == null) {
            libraryPathIds = PathRecognizerRegistry.getDefault().getLibraryIds();
        }
        if (binaryLibraryPathIds == null) {
            binaryLibraryPathIds = PathRecognizerRegistry.getDefault().getBinaryLibraryIds();
        }
        QuerySupport.collectClasspathRoots(file, sourcePathIds, false, roots);
        QuerySupport.collectClasspathRoots(file, libraryPathIds, false, roots);
        QuerySupport.collectClasspathRoots(file, binaryLibraryPathIds, true, roots);
        return roots;
    }

    private static void collectClasspathRoots(FileObject file, Collection<String> pathIds, boolean binaryPaths, Collection<FileObject> roots) {
        for (String id : pathIds) {
            Collection<FileObject> classpathRoots = QuerySupport.getClasspathRoots(file, id);
            if (binaryPaths) {
                for (FileObject binRoot : classpathRoots) {
                    URL binRootUrl = binRoot.toURL();
                    URL[] srcRoots = PathRegistry.getDefault().sourceForBinaryQuery(binRootUrl, null, false);
                    if (srcRoots != null) {
                        LOG.log(Level.FINE, "Translating {0} -> {1}", new Object[]{binRootUrl, srcRoots});
                        for (URL srcRootUrl : srcRoots) {
                            FileObject srcRoot = URLCache.getInstance().findFileObject(srcRootUrl, false);
                            if (srcRoot == null) continue;
                            roots.add(srcRoot);
                        }
                        continue;
                    }
                    LOG.log(Level.FINE, "No sources for {0}, adding bin root", binRootUrl);
                    roots.add(binRoot);
                }
                continue;
            }
            roots.addAll(classpathRoots);
        }
    }

    private static Collection<FileObject> getClasspathRoots(FileObject file, String classpathId) {
        Collection roots = Collections.emptySet();
        if (file != null) {
            ClassPath classpath = ClassPath.getClassPath((FileObject)file, (String)classpathId);
            if (classpath != null) {
                roots = Arrays.asList(classpath.getRoots());
            }
        } else {
            Set<URL> urls = PathRegistry.getDefault().getRootsMarkedAs(classpathId);
            roots = QuerySupport.mapToFileObjects(urls);
        }
        return roots;
    }

    private static /* varargs */ String printFiledToLoad(String ... fieldsToLoad) {
        if (fieldsToLoad == null || fieldsToLoad.length == 0) {
            return "<all-fields>";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fieldsToLoad.length; ++i) {
            sb.append("\"").append(fieldsToLoad[i]).append("\"");
            if (i + 1 >= fieldsToLoad.length) continue;
            sb.append(", ");
        }
        return sb.toString();
    }

    private static Queries.QueryKind translateQueryKind(Kind kind) {
        switch (kind) {
            case EXACT: {
                return Queries.QueryKind.EXACT;
            }
            case PREFIX: {
                return Queries.QueryKind.PREFIX;
            }
            case CASE_INSENSITIVE_PREFIX: {
                return Queries.QueryKind.CASE_INSENSITIVE_PREFIX;
            }
            case CAMEL_CASE: {
                return Queries.QueryKind.CAMEL_CASE;
            }
            case CASE_INSENSITIVE_REGEXP: {
                return Queries.QueryKind.CASE_INSENSITIVE_REGEXP;
            }
            case REGEXP: {
                return Queries.QueryKind.REGEXP;
            }
            case CASE_INSENSITIVE_CAMEL_CASE: {
                return Queries.QueryKind.CASE_INSENSITIVE_CAMEL_CASE;
            }
        }
        throw new UnsupportedOperationException(kind.toString());
    }

    @CheckForNull
    private static URL findOwnerRoot(@NonNull FileObject file) {
        PathRecognizerRegistry regs = PathRecognizerRegistry.getDefault();
        URL res = QuerySupport.findOwnerRoot(file, regs.getSourceIds());
        if (res != null) {
            return res;
        }
        res = QuerySupport.findOwnerRoot(file, regs.getBinaryLibraryIds());
        if (res != null) {
            return res;
        }
        res = QuerySupport.findOwnerRoot(file, regs.getLibraryIds());
        if (res != null) {
            return res;
        }
        return file.isFolder() ? file.toURL() : null;
    }

    @CheckForNull
    private static URL findOwnerRoot(@NonNull FileObject file, @NonNull Collection<? extends String> ids) {
        for (String id : ids) {
            FileObject owner;
            ClassPath cp = ClassPath.getClassPath((FileObject)file, (String)id);
            if (cp == null || (owner = cp.findOwnerRoot(file)) == null) continue;
            return owner.toURL();
        }
        return null;
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static Set<URL> findBinaryRootsForSourceRoot(@NonNull FileObject sourceRoot, @NonNull Map<URL, List<URL>> binaryDeps) {
        HashSet<URL> result = new HashSet<URL>();
        for (URL bin : binaryDeps.keySet()) {
            for (FileObject fo : SourceForBinaryQuery.findSourceRoots((URL)bin).getRoots()) {
                if (!sourceRoot.equals((Object)fo)) continue;
                result.add(bin);
            }
        }
        return result;
    }

    @NonNull
    private static Collection<FileObject> mapToFileObjects(@NonNull Collection<? extends URL> urls) {
        URLCache ucache = URLCache.getInstance();
        ArrayList<FileObject> result = new ArrayList<FileObject>(urls.size());
        for (URL u : urls) {
            FileObject fo = ucache.findFileObject(u, false);
            if (fo == null) continue;
            result.add(fo);
        }
        return result;
    }

    @NonNull
    private static Map<Project, Collection<FileObject>> reduceRootsByProjects(@NonNull Collection<? extends FileObject> roots, @NonNull Collection<? extends Project> projects) {
        HashMap<Project, Collection<FileObject>> rbp = new HashMap<Project, Collection<FileObject>>();
        for (FileObject root : roots) {
            Project p = FileOwnerQuery.getOwner((FileObject)root);
            if (p == null || !projects.contains((Object)p)) continue;
            Collection<FileObject> pr = rbp.get((Object)p);
            if (pr == null) {
                pr = new ArrayList<FileObject>();
                rbp.put(p, pr);
            }
            pr.add(root);
        }
        return rbp;
    }

    private static final class DocumentToResultConvertor
    implements Convertor<IndexDocument, IndexResult> {
        private final URL root;

        DocumentToResultConvertor(@NonNull URL root) {
            this.root = root;
        }

        @CheckForNull
        public IndexResult convert(@NullAllowed IndexDocument p) {
            return p == null ? null : new IndexResult(p, this.root);
        }
    }

    static final class IndexerQuery {
        private static final Map<String, IndexerQuery> queries = new HashMap<String, IndexerQuery>();
        static IndexFactoryImpl indexFactory = LuceneIndexFactory.getDefault();
        private final String indexerId;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private final Map<URL, Reference<LayeredDocumentIndex>> root2index = new HashMap<URL, Reference<LayeredDocumentIndex>>();

        public static synchronized IndexerQuery forIndexer(String indexerName, int indexerVersion) {
            String indexerId = SPIAccessor.getInstance().getIndexerPath(indexerName, indexerVersion);
            IndexerQuery q = queries.get(indexerId);
            if (q == null) {
                q = new IndexerQuery(indexerId);
                queries.put(indexerId, q);
            }
            return q;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public Iterable<? extends Pair<URL, LayeredDocumentIndex>> getIndices(List<? extends URL> roots) {
            Map<URL, Reference<LayeredDocumentIndex>> map = this.root2index;
            synchronized (map) {
                LinkedList<Pair> indices = new LinkedList<Pair>();
                for (URL r : roots) {
                    LayeredDocumentIndex index;
                    assert (PathRegistry.noHostPart(r));
                    Reference<LayeredDocumentIndex> indexRef = this.root2index.get(r);
                    LayeredDocumentIndex layeredDocumentIndex = index = indexRef != null ? indexRef.get() : null;
                    if (index == null) {
                        index = this.findIndex(r);
                        if (index != null) {
                            this.root2index.put(r, new SoftReference<LayeredDocumentIndex>(index));
                        } else {
                            this.root2index.remove(r);
                        }
                    }
                    if (index == null) continue;
                    indices.add(Pair.of((Object)r, (Object)index));
                }
                return indices;
            }
        }

        public String getIndexerId() {
            return this.indexerId;
        }

        private IndexerQuery(String indexerId) {
            this.indexerId = indexerId;
        }

        private LayeredDocumentIndex findIndex(URL root) {
            try {
                FileObject cacheFolder = CacheFolder.getDataFolder(root);
                assert (cacheFolder != null);
                FileObject indexFolder = cacheFolder.getFileObject(this.indexerId);
                if (indexFolder != null) {
                    return indexFactory.getIndex(indexFolder);
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.INFO, "Can't create index for " + this.indexerId + " and " + root, ioe);
            }
            return null;
        }
    }

    public static final class Query {
        private final QuerySupport qs;
        private final org.apache.lucene.search.Query queryImpl;

        private Query(@NonNull QuerySupport qs, @NonNull org.apache.lucene.search.Query queryImpl) {
            assert (qs != null);
            assert (queryImpl != null);
            this.qs = qs;
            this.queryImpl = queryImpl;
        }

        @NonNull
        public /* varargs */ Collection<? extends IndexResult> execute(final @NullAllowed String ... fieldsToLoad) throws IOException {
            try {
                return (Collection)Utilities.runPriorityIO(new Callable<Collection<? extends IndexResult>>(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public Collection<? extends IndexResult> call() throws Exception {
                        Iterable<? extends Pair<URL, LayeredDocumentIndex>> indices = Query.this.qs.indexerQuery.getIndices(Query.this.qs.roots);
                        for (Pair<URL, LayeredDocumentIndex> pair : indices) {
                            LayeredDocumentIndex index = (LayeredDocumentIndex)pair.second();
                            Collection<? extends String> staleFiles = index.getDirtyKeys();
                            boolean scanningThread = RunWhenScanFinishedSupport.isScanningThread();
                            LOG.log(Level.FINE, "Index: {0}, staleFiles: {1}, scanning thread: {2}", new Object[]{index, staleFiles, scanningThread});
                            if (staleFiles.isEmpty() || scanningThread || TransientUpdateSupport.isTransientUpdate()) continue;
                            URL root = (URL)pair.first();
                            LinkedList<URL> list = new LinkedList<URL>();
                            for (String staleFile : staleFiles) {
                                try {
                                    list.add(Util.resolveUrl(root, staleFile, false));
                                }
                                catch (MalformedURLException ex) {
                                    LOG.log(Level.WARNING, null, ex);
                                }
                            }
                            TransientUpdateSupport.setTransientUpdate(true);
                            try {
                                RepositoryUpdater.getDefault().enforcedFileListUpdate(root, list);
                                continue;
                            }
                            finally {
                                TransientUpdateSupport.setTransientUpdate(false);
                                continue;
                            }
                        }
                        ArrayDeque result = new ArrayDeque();
                        for (Pair<URL, LayeredDocumentIndex> pair2 : indices) {
                            DocumentIndex2 index = (DocumentIndex2)pair2.second();
                            URL root = (URL)pair2.first();
                            Collection pr = index.query(Query.this.queryImpl, (Convertor)new DocumentToResultConvertor(root), fieldsToLoad);
                            result.addAll(pr);
                            if (!LOG.isLoggable(Level.FINE)) continue;
                            LOG.log(Level.FINE, "{0} (loading fields {1}) invoked at {2}@{3}[indexer={4}]:", new Object[]{this, QuerySupport.printFiledToLoad(fieldsToLoad), Query.this.qs.getClass().getSimpleName(), Integer.toHexString(System.identityHashCode(Query.this.qs)), Query.this.qs.indexerQuery.getIndexerId()});
                            for (IndexResult idi : pr) {
                                LOG.log(Level.FINE, " {0}", idi);
                            }
                            LOG.fine("----");
                        }
                        return result;
                    }
                });
            }
            catch (Index.IndexClosedException ice) {
                if (Installer.isClosed()) {
                    return Collections.emptySet();
                }
                throw ice;
            }
            catch (IOException ioe) {
                throw ioe;
            }
            catch (RuntimeException re) {
                throw re;
            }
            catch (Exception ex) {
                throw new IOException(ex);
            }
        }

        @NonNull
        public String toString() {
            return String.format("QuerySupport.Query[%s]", new Object[]{this.queryImpl});
        }

        public static final class Factory {
            private final QuerySupport qs;

            private Factory(@NonNull QuerySupport qs) {
                assert (qs != null);
                this.qs = qs;
            }

            @NonNull
            public Query field(@NonNull String fieldName, @NonNull String fieldValue, @NonNull Kind kind) {
                Parameters.notNull((CharSequence)"fieldName", (Object)fieldName);
                Parameters.notNull((CharSequence)"fieldValue", (Object)fieldValue);
                Parameters.notNull((CharSequence)"kind", (Object)((Object)kind));
                return new Query(this.qs, Queries.createQuery((String)fieldName, (String)fieldName, (String)fieldValue, (Queries.QueryKind)QuerySupport.translateQueryKind(kind)));
            }

            @NonNull
            public Query file(@NonNull String relativePath) {
                Parameters.notNull((CharSequence)"relativePath", (Object)relativePath);
                return this.field("_sn", relativePath, Kind.EXACT);
            }

            @NonNull
            public Query file(@NonNull FileObject file) {
                FileObject root;
                Parameters.notNull((CharSequence)"file", (Object)file);
                String relativePath = null;
                Collection roots = QuerySupport.mapToFileObjects(this.qs.roots);
                Iterator i$ = roots.iterator();
                while (i$.hasNext() && (relativePath = FileUtil.getRelativePath((FileObject)(root = (FileObject)i$.next()), (FileObject)file)) == null) {
                }
                if (relativePath == null) {
                    StringBuilder rootsList = new StringBuilder();
                    boolean first = true;
                    for (FileObject root2 : roots) {
                        if (first) {
                            first = false;
                        } else {
                            rootsList.append(", ");
                        }
                        rootsList.append(FileUtil.getFileDisplayName((FileObject)root2));
                    }
                    throw new IllegalArgumentException(String.format("The file %s is not owned by QuerySupport roots: %s", FileUtil.getFileDisplayName((FileObject)file), rootsList));
                }
                return this.file(relativePath);
            }

            @NonNull
            public /* varargs */ Query and(@NonNull Query ... queries) {
                Parameters.notNull((CharSequence)"queries", (Object)queries);
                BooleanQuery bq = new BooleanQuery();
                for (Query q : queries) {
                    bq.add(new BooleanClause(q.queryImpl, BooleanClause.Occur.MUST));
                }
                return new Query(this.qs, (org.apache.lucene.search.Query)bq);
            }

            @NonNull
            public /* varargs */ Query or(@NonNull Query ... queries) {
                Parameters.notNull((CharSequence)"queries", (Object)queries);
                BooleanQuery bq = new BooleanQuery();
                for (Query q : queries) {
                    bq.add(new BooleanClause(q.queryImpl, BooleanClause.Occur.SHOULD));
                }
                return new Query(this.qs, (org.apache.lucene.search.Query)bq);
            }
        }

    }

    public static enum Kind {
        EXACT,
        PREFIX,
        CASE_INSENSITIVE_PREFIX,
        CAMEL_CASE,
        REGEXP,
        CASE_INSENSITIVE_REGEXP,
        CASE_INSENSITIVE_CAMEL_CASE;
        

        private Kind() {
        }
    }

}

