/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.spi.indexing.support;

import org.openide.util.Parameters;

public final class IndexDocument {
    final org.netbeans.modules.parsing.lucene.support.IndexDocument spi;

    IndexDocument(org.netbeans.modules.parsing.lucene.support.IndexDocument spi) {
        Parameters.notNull((CharSequence)"spi", (Object)spi);
        this.spi = spi;
    }

    public void addPair(String key, String value, boolean searchable, boolean stored) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        Parameters.notEmpty((CharSequence)"key", (CharSequence)key);
        Parameters.notNull((CharSequence)"value", (Object)value);
        this.spi.addPair(key, value, searchable, stored);
    }
}

