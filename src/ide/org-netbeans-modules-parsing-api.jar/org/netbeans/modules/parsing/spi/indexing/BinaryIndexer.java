/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.spi.indexing;

import org.netbeans.modules.parsing.spi.indexing.Context;

public abstract class BinaryIndexer {
    protected abstract void index(Context var1);
}

