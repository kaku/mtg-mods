/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.parsing.api;

import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.Installer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public final class Snapshot {
    private static final Logger LOG = Logger.getLogger(Snapshot.class.getName());
    private final CharSequence text;
    final int[] lineStartOffsets;
    private final MimePath mimePath;
    final int[][] currentToOriginal;
    private final int[][] originalToCurrent;
    private final Source source;
    private TokenHierarchy<?> tokenHierarchy;

    private Snapshot(CharSequence text, int[] lineStartOffsets, Source source, MimePath mimePath, int[][] currentToOriginal, int[][] originalToCurrent) {
        this.text = text;
        this.lineStartOffsets = lineStartOffsets;
        this.source = source;
        this.mimePath = mimePath;
        this.currentToOriginal = currentToOriginal;
        this.originalToCurrent = originalToCurrent;
    }

    @NonNull
    static Snapshot create(CharSequence text, int[] lineStartOffsets, Source source, MimePath mimePath, int[][] currentToOriginal, int[][] originalToCurrent) {
        int textLength = text.length();
        if (textLength > Installer.MAX_FILE_SIZE) {
            text = "";
            Object[] arrobject = new Object[4];
            arrobject[0] = source.getFileObject() == null ? "<unknown>" : FileUtil.getFileDisplayName((FileObject)source.getFileObject());
            arrobject[1] = mimePath;
            arrobject[2] = textLength;
            arrobject[3] = Installer.MAX_FILE_SIZE;
            LOG.log(Level.WARNING, "Embedding in file {0} of type: {1} of size: {2} has been ignored due to large size. Embeddings large then {3} chars are ignored, you can increase the size by parse.max.file.size property.", arrobject);
        }
        return new Snapshot(text, lineStartOffsets, source, mimePath, currentToOriginal, originalToCurrent);
    }

    public Embedding create(int offset, int length, String mimeType) {
        int i;
        if (offset < 0 || length < 0) {
            throw new ArrayIndexOutOfBoundsException("offset=" + offset + ", length=" + length);
        }
        if (offset + length > this.getText().length()) {
            throw new ArrayIndexOutOfBoundsException("offset=" + offset + ", length=" + length + ", snapshot-length=" + this.getText().length());
        }
        ArrayList<int[]> newCurrentToOriginal = new ArrayList<int[]>();
        ArrayList<int[]> newOriginalToCurrent = new ArrayList<int[]>();
        for (i = 1; i < this.currentToOriginal.length && this.currentToOriginal[i][0] <= offset; ++i) {
        }
        if (this.currentToOriginal[i - 1][1] < 0) {
            newCurrentToOriginal.add(new int[]{0, this.currentToOriginal[i - 1][1]});
        } else {
            newCurrentToOriginal.add(new int[]{0, this.currentToOriginal[i - 1][1] + offset - this.currentToOriginal[i - 1][0]});
            newOriginalToCurrent.add(new int[]{this.currentToOriginal[i - 1][1] + offset - this.currentToOriginal[i - 1][0], 0});
        }
        while (i < this.currentToOriginal.length && this.currentToOriginal[i][0] < offset + length) {
            newCurrentToOriginal.add(new int[]{this.currentToOriginal[i][0] - offset, this.currentToOriginal[i][1]});
            if (this.currentToOriginal[i][1] >= 0) {
                newOriginalToCurrent.add(new int[]{this.currentToOriginal[i][1], this.currentToOriginal[i][0] - offset});
            } else {
                newOriginalToCurrent.add(new int[]{this.currentToOriginal[i - 1][1] + this.currentToOriginal[i][0] - this.currentToOriginal[i - 1][0], -1});
            }
            ++i;
        }
        if (newOriginalToCurrent.size() > 0 && ((int[])newOriginalToCurrent.get(newOriginalToCurrent.size() - 1))[1] >= 0) {
            newOriginalToCurrent.add(new int[]{((int[])newOriginalToCurrent.get(newOriginalToCurrent.size() - 1))[0] + length - ((int[])newOriginalToCurrent.get(newOriginalToCurrent.size() - 1))[1], -1});
        }
        MimePath newMimePath = MimePath.get((MimePath)this.mimePath, (String)mimeType);
        Snapshot snapshot = Snapshot.create(this.getText().subSequence(offset, offset + length), null, this.source, newMimePath, (int[][])newCurrentToOriginal.toArray((T[])new int[newCurrentToOriginal.size()][]), (int[][])newOriginalToCurrent.toArray((T[])new int[newOriginalToCurrent.size()][]));
        return new Embedding(snapshot, newMimePath);
    }

    public Embedding create(CharSequence charSequence, String mimeType) {
        MimePath newMimePath = MimePath.get((MimePath)this.mimePath, (String)mimeType);
        return new Embedding(Snapshot.create(charSequence, null, this.source, newMimePath, new int[][]{{0, -1}}, new int[0][]), newMimePath);
    }

    public CharSequence getText() {
        return this.text;
    }

    public String getMimeType() {
        return this.mimePath.getMimeType(this.mimePath.size() - 1);
    }

    public MimePath getMimePath() {
        return this.mimePath;
    }

    public TokenHierarchy<?> getTokenHierarchy() {
        Language lang;
        if (this.tokenHierarchy == null && (lang = Language.find((String)this.getMimeType())) != null) {
            Document sourceDocument = this.source.getDocument(false);
            InputAttributes inputAttrs = sourceDocument != null ? (InputAttributes)sourceDocument.getProperty(InputAttributes.class) : null;
            this.tokenHierarchy = TokenHierarchy.create((CharSequence)this.text, (boolean)false, (Language)lang, (Set)null, (InputAttributes)inputAttrs);
        }
        return this.tokenHierarchy;
    }

    public int getOriginalOffset(int snapshotOffset) {
        if (snapshotOffset < 0) {
            return -1;
        }
        if (snapshotOffset > this.getText().length()) {
            return -1;
        }
        int low = 0;
        int high = this.currentToOriginal.length - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            int cmp = this.currentToOriginal[mid][0];
            if (cmp > snapshotOffset) {
                high = mid - 1;
                continue;
            }
            if (mid == this.currentToOriginal.length - 1 || this.currentToOriginal[mid + 1][0] > snapshotOffset) {
                if (this.currentToOriginal[mid][1] < 0) {
                    if (snapshotOffset == cmp && mid > 0) {
                        return snapshotOffset - this.currentToOriginal[mid - 1][0] + this.currentToOriginal[mid - 1][1];
                    }
                    return this.currentToOriginal[mid][1];
                }
                return snapshotOffset - this.currentToOriginal[mid][0] + this.currentToOriginal[mid][1];
            }
            low = mid + 1;
        }
        return -1;
    }

    public int getEmbeddedOffset(int originalOffset) {
        int low = 0;
        int high = this.originalToCurrent.length - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            int cmp = this.originalToCurrent[mid][0];
            if (cmp > originalOffset) {
                high = mid - 1;
                continue;
            }
            if (mid == this.originalToCurrent.length - 1 || this.originalToCurrent[mid + 1][0] > originalOffset) {
                if (this.originalToCurrent[mid][1] < 0) {
                    if (originalOffset == cmp && mid > 0) {
                        return originalOffset - this.originalToCurrent[mid - 1][0] + this.originalToCurrent[mid - 1][1];
                    }
                    return this.originalToCurrent[mid][1];
                }
                return originalOffset - this.originalToCurrent[mid][0] + this.originalToCurrent[mid][1];
            }
            low = mid + 1;
        }
        return -1;
    }

    public Source getSource() {
        return this.source;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Snapshot ");
        sb.append(this.hashCode());
        sb.append(": ");
        Source _source = this.getSource();
        FileObject fileObject = _source.getFileObject();
        if (fileObject != null) {
            sb.append(fileObject.getNameExt());
        } else {
            sb.append((Object)this.mimePath).append(" ").append(_source.getDocument(false));
        }
        if (!this.getMimeType().equals(_source.getMimeType())) {
            sb.append("( ").append(this.getMimeType()).append(" ");
            sb.append(this.getOriginalOffset(0)).append("-").append(this.getOriginalOffset(this.getText().length() - 1)).append(")");
        }
        return sb.toString();
    }
}

