/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.util.UserQuestionException
 */
package org.netbeans.modules.parsing.api;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.impl.Installer;
import org.netbeans.modules.parsing.impl.SchedulerAccessor;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.SourceFlags;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.impl.event.EventSupport;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.UserQuestionException;

public final class Source {
    private static final Logger LOG = Logger.getLogger(Source.class.getName());
    private static final Map<FileObject, Reference<Source>> instances = new WeakHashMap<FileObject, Reference<Source>>();
    private static final ThreadLocal<Boolean> suppressListening = new ThreadLocal<Boolean>(){

        @Override
        protected Boolean initialValue() {
            return Boolean.FALSE;
        }
    };
    private static final ThreadLocal<Boolean> preferFile = new ThreadLocal<Boolean>(){

        @Override
        protected Boolean initialValue() {
            return Boolean.FALSE;
        }
    };
    private final String mimeType;
    private final FileObject fileObject;
    private final Document document;
    private final Set<SourceFlags> flags = Collections.synchronizedSet(EnumSet.noneOf(SourceFlags.class));
    private int taskCount;
    private volatile Parser cachedParser;
    private final AtomicReference<ASourceModificationEvent> sourceModificationEvent = new AtomicReference();
    private final ASourceModificationEvent unspecifiedSourceModificationEvent;
    private Map<Class<? extends Scheduler>, SchedulerEvent> schedulerEvents;
    private SourceCache cache;
    private volatile long eventId;
    private final EventSupport support;

    public static Source create(FileObject fileObject) {
        Parameters.notNull((CharSequence)"fileObject", (Object)fileObject);
        if (!fileObject.isValid() || !fileObject.isData()) {
            return null;
        }
        return Source._get(fileObject.getMIMEType(), fileObject);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Source create(Document document) {
        Parameters.notNull((CharSequence)"document", (Object)document);
        String mimeType = DocumentUtilities.getMimeType((Document)document);
        if (mimeType == null) {
            throw new NullPointerException("Netbeans documents must have 'mimeType' property: " + document.getClass() + "@" + Integer.toHexString(System.identityHashCode(document)));
        }
        Class<Source> class_ = Source.class;
        synchronized (Source.class) {
            Source source;
            Reference sourceRef = (Reference)document.getProperty(Source.class);
            Source source2 = source = sourceRef == null ? null : (Source)sourceRef.get();
            if (source != null && source.getFileObject() != null && !source.getFileObject().isValid()) {
                source = null;
            }
            if (source == null) {
                FileObject fileObject = Util.getFileObject(document);
                if (fileObject != null) {
                    source = Source._get(mimeType, fileObject);
                } else {
                    if ("text/x-dialog-binding".equals(mimeType)) {
                        LanguagePath path;
                        InputAttributes attributes = (InputAttributes)document.getProperty(InputAttributes.class);
                        Document doc = (Document)attributes.getValue(path = LanguagePath.get((Language)((Language)MimeLookup.getLookup((String)mimeType).lookup(Language.class))), (Object)"dialogBinding.document");
                        fileObject = doc != null ? Util.getFileObject(doc) : (FileObject)attributes.getValue(path, (Object)"dialogBinding.fileObject");
                    }
                    source = new Source(mimeType, document, fileObject);
                }
                document.putProperty(Source.class, new WeakReference<Source>(source));
            }
            assert (source != null);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return source;
        }
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public Document getDocument(boolean forceOpen) {
        if (preferFile.get().booleanValue()) {
            if (!forceOpen) {
                return null;
            }
            boolean ae = false;
            if (!$assertionsDisabled) {
                ae = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (ae) {
                LOG.log(Level.INFO, "Calling Source.getDocument(forceOpen=true) for Source preferring files -> possible performance problem {0}", Arrays.asList(Thread.currentThread().getStackTrace()));
            }
        }
        if (this.document != null) {
            return this.document;
        }
        EditorCookie ec = null;
        try {
            DataObject dataObject = DataObject.find((FileObject)this.fileObject);
            ec = (EditorCookie)dataObject.getLookup().lookup(EditorCookie.class);
        }
        catch (DataObjectNotFoundException ex) {
            // empty catch block
        }
        if (ec == null) {
            return null;
        }
        StyledDocument doc = ec.getDocument();
        if (doc == null && forceOpen) {
            try {
                try {
                    doc = ec.openDocument();
                }
                catch (UserQuestionException uqe) {
                    uqe.confirmed();
                    doc = ec.openDocument();
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
        }
        return doc;
    }

    public FileObject getFileObject() {
        return this.fileObject;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Snapshot createSnapshot() {
        int[][] lineStartOffsets;
        CharSequence[] text;
        block24 : {
            text = new CharSequence[]{""};
            lineStartOffsets = new int[][]{{0}};
            Document doc = this.getDocument(false);
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.FINER, null, new Throwable("Creating snapshot: doc=" + doc + ", file=" + (Object)this.fileObject));
            } else if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Creating snapshot: doc={0}, file={1}", new Object[]{doc, this.fileObject});
            }
            try {
                if (doc == null) {
                    try {
                        if (this.fileObject.isValid()) {
                            if (this.fileObject.getSize() <= (long)Installer.MAX_FILE_SIZE) {
                                InputStream is = this.fileObject.getInputStream();
                                assert (is != null);
                                try {
                                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, FileEncodingQuery.getEncoding((FileObject)this.fileObject)));
                                    try {
                                        StringBuilder output = new StringBuilder(Math.max(16, (int)this.fileObject.getSize()));
                                        LinkedList<Integer> lso = new LinkedList<Integer>();
                                        boolean lastCharCR = false;
                                        char[] buffer = new char[1024];
                                        int size = -1;
                                        int LF = 10;
                                        int CR = 13;
                                        int LS = 8232;
                                        int PS = 8233;
                                        lso.add(0);
                                        while (-1 != (size = reader.read(buffer, 0, buffer.length))) {
                                            for (int i = 0; i < size; ++i) {
                                                char ch = buffer[i];
                                                if (lastCharCR && ch == '\n') continue;
                                                if (ch == '\r') {
                                                    output.append('\n');
                                                    lso.add(output.length());
                                                    lastCharCR = true;
                                                    continue;
                                                }
                                                if (ch == '\u2028' || ch == '\u2029') {
                                                    output.append('\n');
                                                    lso.add(output.length());
                                                    lastCharCR = false;
                                                    continue;
                                                }
                                                lastCharCR = false;
                                                output.append(ch);
                                            }
                                        }
                                        int[] lsoArr = new int[lso.size()];
                                        int idx = 0;
                                        for (Integer offset : lso) {
                                            lsoArr[idx++] = offset;
                                        }
                                        text[0] = output;
                                        lineStartOffsets[0] = lsoArr;
                                    }
                                    finally {
                                        reader.close();
                                    }
                                }
                                finally {
                                    is.close();
                                }
                            }
                            LOG.log(Level.WARNING, "Source {0} of size: {1} has been ignored due to large size. Files large then {2} bytes are ignored, you can increase the size by parse.max.file.size property.", new Object[]{FileUtil.getFileDisplayName((FileObject)this.fileObject), this.fileObject.getSize(), Installer.MAX_FILE_SIZE});
                        }
                        break block24;
                    }
                    catch (FileNotFoundException fnfe) {
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.WARNING, null, ioe);
                    }
                    break block24;
                }
                final Document d = doc;
                d.render(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            int length = d.getLength();
                            if (length < 0) {
                                text[0] = "";
                                lineStartOffsets[0] = new int[]{0};
                            } else {
                                Element root = DocumentUtilities.getParagraphRootElement((Document)d);
                                int[] lso = new int[root.getElementCount()];
                                for (int i = 0; i < lso.length; ++i) {
                                    lso[i] = root.getElement(i).getStartOffset();
                                }
                                text[0] = d.getText(0, length);
                                lineStartOffsets[0] = lso;
                            }
                        }
                        catch (BadLocationException ble) {
                            LOG.log(Level.WARNING, null, ble);
                        }
                    }
                });
            }
            catch (OutOfMemoryError oome) {
                text[0] = "";
                lineStartOffsets[0] = new int[]{0};
                LOG.log(Level.INFO, null, oome);
                if (doc != null) {
                    LOG.warning("Can't create snapshot of " + doc + ", size=" + doc.getLength() + ", mimeType=" + this.mimeType);
                }
                LOG.warning("Can't create snapshot of " + (Object)this.fileObject + ", size=" + this.fileObject.getSize() + ", mimeType=" + this.mimeType);
            }
        }
        return Snapshot.create(text[0], lineStartOffsets[0], this, MimePath.get((String)this.mimeType), new int[][]{{0, 0}}, new int[][]{{0, 0}});
    }

    public String toString() {
        return super.toString() + "[mimeType=" + this.mimeType + ", fileObject=" + (Object)this.fileObject + ", document=" + this.document;
    }

    private Source(String mimeType, Document document, FileObject fileObject) {
        this.unspecifiedSourceModificationEvent = new ASourceModificationEvent(this, true, -1, -1);
        this.support = new EventSupport(this);
        this.mimeType = mimeType;
        this.document = document;
        this.fileObject = fileObject;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Source _get(String mimeType, FileObject fileObject) {
        assert (mimeType != null);
        assert (fileObject != null);
        Class<Source> class_ = Source.class;
        synchronized (Source.class) {
            Source source;
            Reference<Source> sourceRef = instances.get((Object)fileObject);
            Source source2 = source = sourceRef == null ? null : sourceRef.get();
            if (source == null) {
                source = new Source(mimeType, null, fileObject);
                instances.put(fileObject, new WeakReference<Source>(source));
            }
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return source;
        }
    }

    private void assignListeners() {
        boolean listen;
        boolean bl = listen = suppressListening.get() == false;
        if (listen) {
            this.support.init();
        }
    }

    static {
        SourceAccessor.setINSTANCE(new MySourceAccessor());
    }

    static class ASourceModificationEvent
    extends SourceModificationEvent {
        private int startOffset;
        private int endOffset;

        ASourceModificationEvent(Object source, boolean sourceChanged, int _startOffset, int _endOffset) {
            super(source, sourceChanged);
            this.startOffset = _startOffset;
            this.endOffset = _endOffset;
        }

        void add(int _startOffset, int _endOffset) {
            this.startOffset = Math.min(this.startOffset, _startOffset);
            this.endOffset = Math.min(this.endOffset, _endOffset);
        }

        @Override
        public String toString() {
            return "SourceModificationEvent " + this.startOffset + ":" + this.endOffset;
        }
    }

    private static class MySourceAccessor
    extends SourceAccessor {
        private MySourceAccessor() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void setFlags(Source source, Set<SourceFlags> flags) {
            assert (source != null);
            assert (flags != null);
            Set set = source.flags;
            synchronized (set) {
                source.flags.addAll(flags);
                source.eventId++;
            }
        }

        @Override
        public boolean testFlag(Source source, SourceFlags flag) {
            assert (source != null);
            assert (flag != null);
            return source.flags.contains((Object)flag);
        }

        @Override
        public boolean cleanFlag(Source source, SourceFlags flag) {
            assert (source != null);
            assert (flag != null);
            return source.flags.remove((Object)flag);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean testAndCleanFlags(Source source, SourceFlags test, Set<SourceFlags> clean) {
            assert (source != null);
            assert (test != null);
            assert (clean != null);
            Set set = source.flags;
            synchronized (set) {
                boolean res = source.flags.contains((Object)test);
                source.flags.removeAll(clean);
                return res;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void invalidate(Source source, boolean force) {
            assert (source != null);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                boolean invalid = source.flags.remove((Object)SourceFlags.INVALID);
                if (force || invalid) {
                    SourceCache cache = this.getCache(source);
                    assert (cache != null);
                    cache.invalidate();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean invalidate(Source source, long id, Snapshot snapshot) {
            assert (source != null);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                long eventId;
                if (snapshot == null) {
                    return !source.flags.contains((Object)SourceFlags.INVALID);
                }
                Set set = source.flags;
                synchronized (set) {
                    eventId = source.eventId;
                }
                if (id != eventId) {
                    return false;
                }
                source.flags.remove((Object)SourceFlags.INVALID);
                SourceCache cache = this.getCache(source);
                assert (cache != null);
                cache.invalidate();
                return true;
            }
        }

        @Override
        public Parser getParser(Source source) {
            assert (source != null);
            return source.cachedParser;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void setParser(Source source, Parser parser) throws IllegalStateException {
            assert (source != null);
            assert (parser != null);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                if (source.cachedParser != null) {
                    throw new IllegalStateException();
                }
                source.cachedParser = parser;
            }
        }

        @Override
        public void assignListeners(@NonNull Source source) {
            assert (source != null);
            source.assignListeners();
        }

        @Override
        public EventSupport getEventSupport(Source source) {
            assert (source != null);
            return source.support;
        }

        @Override
        public long getLastEventId(Source source) {
            assert (source != null);
            return source.eventId;
        }

        @Override
        public void setSourceModification(Source source, boolean sourceChanged, int startOffset, int endOffset) {
            ASourceModificationEvent newSourceModificationEvent;
            ASourceModificationEvent oldSourceModificationEvent;
            assert (source != null);
            do {
                boolean mergedChange = sourceChanged | ((oldSourceModificationEvent = (ASourceModificationEvent)source.sourceModificationEvent.get()) == null ? false : oldSourceModificationEvent.sourceChanged());
                newSourceModificationEvent = new ASourceModificationEvent(source, mergedChange, startOffset, endOffset);
            } while (!source.sourceModificationEvent.compareAndSet(oldSourceModificationEvent, newSourceModificationEvent));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @Override
        public void mimeTypeMayChanged(@NonNull Source source) {
            assert (source != null);
            FileObject file = source.getFileObject();
            if (file == null || Objects.equals(source.getMimeType(), file.getMIMEType())) return;
            Class<Source> class_ = Source.class;
            synchronized (Source.class) {
                instances.remove((Object)file);
                // ** MonitorExit[var3_3] (shouldn't be in output)
                return;
            }
        }

        @Override
        public void parsed(Source source) {
            source.sourceModificationEvent.set(null);
        }

        @Override
        public SourceModificationEvent getSourceModificationEvent(Source source) {
            assert (source != null);
            SourceModificationEvent event = (SourceModificationEvent)source.sourceModificationEvent.get();
            if (event == null) {
                event = source.unspecifiedSourceModificationEvent;
            }
            return event;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Map<Class<? extends Scheduler>, SchedulerEvent> createSchedulerEvents(@NonNull Source source, @NonNull Iterable<? extends Scheduler> schedulers, @NonNull SourceModificationEvent sourceModificationEvent) {
            Parameters.notNull((CharSequence)"source", (Object)source);
            Parameters.notNull((CharSequence)"schedulers", schedulers);
            Parameters.notNull((CharSequence)"sourceModificationEvent", (Object)sourceModificationEvent);
            HashMap result = new HashMap();
            for (Scheduler scheduler : schedulers) {
                SchedulerEvent schedulerEvent = SchedulerAccessor.get().createSchedulerEvent(scheduler, sourceModificationEvent);
                if (schedulerEvent == null) continue;
                result.put(scheduler.getClass(), schedulerEvent);
            }
            Object i$ = TaskProcessor.INTERNAL_LOCK;
            synchronized (i$) {
                source.schedulerEvents = result;
            }
            return Collections.unmodifiableMap(result);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void setSchedulerEvent(@NonNull Source source, @NonNull Scheduler scheduler, @NonNull SchedulerEvent event) {
            Parameters.notNull((CharSequence)"source", (Object)source);
            Parameters.notNull((CharSequence)"scheduler", (Object)scheduler);
            Parameters.notNull((CharSequence)"event", (Object)event);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                if (source.schedulerEvents == null) {
                    source.schedulerEvents = new HashMap();
                }
                source.schedulerEvents.put(scheduler.getClass(), event);
            }
        }

        @Override
        public SchedulerEvent getSchedulerEvent(Source source, Class<? extends Scheduler> schedulerType) {
            if (schedulerType == null) {
                return null;
            }
            if (source.schedulerEvents == null) {
                return null;
            }
            return (SchedulerEvent)source.schedulerEvents.get(schedulerType);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public SourceCache getCache(Source source) {
            assert (source != null);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                if (source.cache == null) {
                    source.cache = new SourceCache(source, null);
                }
                return source.cache;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int taskAdded(Source source) {
            int ret;
            assert (source != null);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                ret = source.taskCount++;
            }
            return ret;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int taskRemoved(Source source) {
            assert (source != null);
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                return --source.taskCount;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Source get(FileObject file) {
            assert (file != null);
            Class<Source> class_ = Source.class;
            synchronized (Source.class) {
                Reference ref = (Reference)instances.get((Object)file);
                // ** MonitorExit[var3_2] (shouldn't be in output)
                return ref == null ? null : (Source)ref.get();
            }
        }

        @Override
        public void suppressListening(boolean suppress, boolean prefer) {
            suppressListening.set(suppress);
            preferFile.set(prefer);
        }

        @Override
        public int getLineStartOffset(Snapshot snapshot, int lineIdx) {
            assert (snapshot != null);
            assert (snapshot.lineStartOffsets != null);
            assert (lineIdx >= 0 && lineIdx <= snapshot.lineStartOffsets.length);
            if (lineIdx < snapshot.lineStartOffsets.length) {
                return snapshot.lineStartOffsets[lineIdx];
            }
            return snapshot.getText().length();
        }

        @Override
        public Snapshot createSnapshot(CharSequence text, int[] lineStartOffsets, Source source, MimePath mimePath, int[][] currentToOriginal, int[][] originalToCurrent) {
            return Snapshot.create(text, lineStartOffsets, source, mimePath, currentToOriginal, originalToCurrent);
        }

        @Override
        public SourceCache getAndSetCache(Source source, SourceCache sourceCache) {
            SourceCache origCache = source.cache;
            source.cache = sourceCache;
            return origCache;
        }
    }

}

