/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.api;

import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Task;

public abstract class UserTask
extends Task {
    public abstract void run(ResultIterator var1) throws Exception;
}

