/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.api;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.ParserAccessor;
import org.netbeans.modules.parsing.impl.ResultIteratorAccessor;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

public final class ResultIterator {
    private SourceCache sourceCache;
    private UserTask task;
    private Parser.Result result;
    private Parser parser;
    private final List<ResultIterator> children = new LinkedList<ResultIterator>();
    private Map<Embedding, ResultIterator> embeddingToResultIterator = new HashMap<Embedding, ResultIterator>();

    ResultIterator(Parser.Result result) {
        this.result = result;
    }

    ResultIterator(SourceCache sourceCache, UserTask task) {
        this.sourceCache = sourceCache;
        this.task = task;
    }

    ResultIterator(SourceCache sourceCache, Parser parser, UserTask task) {
        this.sourceCache = sourceCache;
        this.parser = parser;
        this.task = task;
    }

    public Snapshot getSnapshot() {
        if (this.sourceCache != null) {
            return this.sourceCache.getSnapshot();
        }
        return this.result.getSnapshot();
    }

    private void invalidate() {
        if (this.result != null) {
            ParserAccessor.getINSTANCE().invalidate(this.result);
            this.result = null;
            this.parser = null;
        }
        Iterator<ResultIterator> it = this.children.iterator();
        while (it.hasNext()) {
            ResultIterator child = it.next();
            it.remove();
            child.invalidate();
        }
    }

    public Parser.Result getParserResult() throws ParseException {
        if (this.result == null) {
            if (this.parser != null) {
                SourceModificationEvent event = SourceAccessor.getINSTANCE().getSourceModificationEvent(this.getSnapshot().getSource());
                TaskProcessor.callParse(this.parser, this.getSnapshot(), this.task, event);
                this.result = TaskProcessor.callGetResult(this.parser, this.task);
            } else {
                this.result = this.sourceCache.getResult(this.task);
            }
        }
        return this.result;
    }

    public Parser.Result getParserResult(int offset) throws ParseException {
        for (Embedding embedding : this.getEmbeddings()) {
            if (!embedding.containsOriginalOffset(offset)) continue;
            return this.getResultIterator(embedding).getParserResult(offset);
        }
        return this.getParserResult();
    }

    public Iterable<Embedding> getEmbeddings() {
        if (this.sourceCache == null) {
            return Collections.emptyList();
        }
        return this.sourceCache.getAllEmbeddings();
    }

    public ResultIterator getResultIterator(Embedding embedding) {
        if (this.sourceCache == null) {
            return null;
        }
        ResultIterator resultIterator = this.embeddingToResultIterator.get(embedding);
        if (resultIterator == null) {
            SourceCache cache = this.sourceCache.getCache(embedding);
            resultIterator = new ResultIterator(cache, this.task);
            this.embeddingToResultIterator.put(embedding, resultIterator);
            this.children.add(resultIterator);
        }
        return resultIterator;
    }

    static {
        ResultIteratorAccessor.setINSTANCE(new MyAccessor());
    }

    private static class MyAccessor
    extends ResultIteratorAccessor {
        private MyAccessor() {
        }

        @Override
        public void invalidate(ResultIterator resultIterator) {
            assert (resultIterator != null);
            resultIterator.invalidate();
        }
    }

}

