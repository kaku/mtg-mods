/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.parsing.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;

public final class Embedding {
    private Snapshot snapshot;
    private MimePath mimePath;
    private static final Comparator<int[]> TMS_VCLV = new Comparator<int[]>(){

        @Override
        public int compare(int[] o1, int[] o2) {
            return o1[0] - o2[0];
        }
    };

    public static Embedding create(List<Embedding> embeddings) {
        if (embeddings.isEmpty()) {
            throw new IllegalArgumentException();
        }
        MimePath mimePath = null;
        Source source = null;
        StringBuilder sb = new StringBuilder();
        ArrayList<int[]> currentToOriginal = new ArrayList<int[]>();
        ArrayList<int[]> originalToCurrent = new ArrayList<int[]>();
        int offset = 0;
        for (Embedding embedding : embeddings) {
            Snapshot snapshot = embedding.getSnapshot();
            if (mimePath != null) {
                if (!mimePath.equals((Object)embedding.mimePath)) {
                    throw new IllegalArgumentException();
                }
                if (source != snapshot.getSource()) {
                    throw new IllegalArgumentException();
                }
            } else {
                mimePath = embedding.mimePath;
                source = snapshot.getSource();
            }
            sb.append(snapshot.getText());
            int[][] p = snapshot.currentToOriginal;
            for (int i = 0; i < p.length; ++i) {
                if (currentToOriginal.isEmpty() || ((int[])currentToOriginal.get(currentToOriginal.size() - 1))[1] != -1 || p[i][1] != -1) {
                    currentToOriginal.add(new int[]{p[i][0] + offset, p[i][1]});
                }
                if (p[i][1] >= 0) {
                    if (!originalToCurrent.isEmpty() && ((int[])originalToCurrent.get(originalToCurrent.size() - 1))[1] >= 0) {
                        originalToCurrent.add(new int[]{((int[])originalToCurrent.get(originalToCurrent.size() - 1))[0] + p[i][0] + offset - ((int[])originalToCurrent.get(originalToCurrent.size() - 1))[1], -1});
                    }
                    originalToCurrent.add(new int[]{p[i][1], p[i][0] + offset});
                    continue;
                }
                if (originalToCurrent.isEmpty() || ((int[])originalToCurrent.get(originalToCurrent.size() - 1))[1] < 0) continue;
                originalToCurrent.add(new int[]{((int[])originalToCurrent.get(originalToCurrent.size() - 1))[0] + p[i][0] + offset - ((int[])originalToCurrent.get(originalToCurrent.size() - 1))[1], -1});
            }
            offset += snapshot.getText().length();
        }
        if (originalToCurrent.size() > 0 && ((int[])originalToCurrent.get(originalToCurrent.size() - 1))[1] >= 0) {
            originalToCurrent.add(new int[]{((int[])originalToCurrent.get(originalToCurrent.size() - 1))[0] + sb.length() - ((int[])originalToCurrent.get(originalToCurrent.size() - 1))[1], -1});
        }
        Collections.sort(originalToCurrent, TMS_VCLV);
        Snapshot snapshot = Snapshot.create(sb, null, source, mimePath, (int[][])currentToOriginal.toArray((T[])new int[currentToOriginal.size()][]), (int[][])originalToCurrent.toArray((T[])new int[originalToCurrent.size()][]));
        return new Embedding(snapshot, mimePath);
    }

    Embedding(Snapshot snapshot, MimePath mimePath) {
        this.snapshot = snapshot;
        this.mimePath = mimePath;
    }

    public final Snapshot getSnapshot() {
        return this.snapshot;
    }

    public final String getMimeType() {
        return this.mimePath.getMimeType(this.mimePath.size() - 1);
    }

    public final boolean containsOriginalOffset(int originalOffset) {
        return this.snapshot.getEmbeddedOffset(originalOffset) >= 0;
    }

    public String toString() {
        return "Embedding (" + this.getMimeType() + ", " + this.getSnapshot() + ")";
    }

}

