/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.api;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Future;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.ParserAccessor;
import org.netbeans.modules.parsing.impl.ResultIteratorAccessor;
import org.netbeans.modules.parsing.impl.RunWhenScanFinishedSupport;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.Parameters;

public final class ParserManager {
    private static Map<String, Reference<Parser>> cachedParsers = new HashMap<String, Reference<Parser>>();

    private ParserManager() {
    }

    public static void parse(@NonNull Collection<Source> sources, @NonNull UserTask userTask) throws ParseException {
        Parameters.notNull((CharSequence)"sources", sources);
        Parameters.notNull((CharSequence)"userTask", (Object)userTask);
        if (sources.size() == 1) {
            Source source = sources.iterator().next();
            Parameters.notNull((CharSequence)"sources[0]", (Object)source);
            TaskProcessor.runUserTask(new UserTaskAction(source, userTask), sources);
        } else {
            TaskProcessor.runUserTask(new MultiUserTaskAction(sources, userTask), sources);
        }
    }

    @NonNull
    public static Future<Void> parseWhenScanFinished(@NonNull Collection<Source> sources, @NonNull UserTask userTask) throws ParseException {
        Parameters.notNull((CharSequence)"sources", sources);
        Parameters.notNull((CharSequence)"userTask", (Object)userTask);
        if (sources.size() == 1) {
            return RunWhenScanFinishedSupport.runWhenScanFinished(new UserTaskAction(sources.iterator().next(), userTask), sources);
        }
        return RunWhenScanFinishedSupport.runWhenScanFinished(new MultiUserTaskAction(sources, userTask), sources);
    }

    public static void parse(@NonNull String mimeType, @NonNull UserTask userTask) throws ParseException {
        Parameters.notNull((CharSequence)"mimeType", (Object)mimeType);
        Parameters.notNull((CharSequence)"userTask", (Object)userTask);
        Parser pf = ParserManager.findParser(mimeType);
        TaskProcessor.runUserTask(new MimeTaskAction(pf, userTask), Collections.emptyList());
    }

    @NonNull
    public static Future<Void> parseWhenScanFinished(@NonNull String mimeType, @NonNull UserTask userTask) throws ParseException {
        Parameters.notNull((CharSequence)"mimeType", (Object)mimeType);
        Parameters.notNull((CharSequence)"userTask", (Object)userTask);
        Parser pf = ParserManager.findParser(mimeType);
        return RunWhenScanFinishedSupport.runWhenScanFinished(new MimeTaskAction(pf, userTask), Collections.emptyList());
    }

    private static Parser findParser(String mimeType) {
        Parser p = null;
        Reference<Parser> ref = cachedParsers.get(mimeType);
        if (ref != null) {
            p = ref.get();
        }
        if (p == null) {
            Lookup lookup = MimeLookup.getLookup((String)mimeType);
            ParserFactory parserFactory = (ParserFactory)lookup.lookup(ParserFactory.class);
            if (parserFactory == null) {
                throw new IllegalArgumentException("No parser for mime type: " + mimeType);
            }
            p = parserFactory.createParser(Collections.emptyList());
            cachedParsers.put(mimeType, new SoftReference<Parser>(p));
        }
        return p;
    }

    private static class MimeTaskAction
    implements Mutex.ExceptionAction<Void> {
        private final UserTask userTask;
        private final Parser parser;

        public MimeTaskAction(Parser parser, UserTask userTask) {
            assert (userTask != null);
            assert (parser != null);
            this.userTask = userTask;
            this.parser = parser;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Void run() throws Exception {
            TaskProcessor.callParse(this.parser, null, this.userTask, null);
            Parser.Result result = TaskProcessor.callGetResult(this.parser, this.userTask);
            try {
                TaskProcessor.callUserTask(this.userTask, new ResultIterator(result));
            }
            finally {
                if (result != null) {
                    ParserAccessor.getINSTANCE().invalidate(result);
                }
            }
            return null;
        }
    }

    private static class LazySnapshots
    implements Collection<Snapshot> {
        private final Collection<? extends Source> sources;

        public LazySnapshots(Collection<? extends Source> sources) {
            assert (sources != null);
            this.sources = sources;
        }

        @Override
        public int size() {
            return this.sources.size();
        }

        @Override
        public boolean isEmpty() {
            return this.sources.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Snapshot)) {
                return false;
            }
            Snapshot snap = (Snapshot)o;
            return this.sources.contains(snap.getSource());
        }

        @Override
        public Iterator<Snapshot> iterator() {
            return new LazySnapshotsIt(this.sources.iterator());
        }

        @Override
        public Object[] toArray() {
            Object[] result = new Object[this.sources.size()];
            this.fill(result);
            return result;
        }

        @Override
        public <T> T[] toArray(T[] a) {
            Class<Snapshot> arrayElementClass = a.getClass().getComponentType();
            if (!arrayElementClass.isAssignableFrom(Snapshot.class)) {
                throw new ArrayStoreException("Can't store Snapshot instances to an array of " + arrayElementClass.getName());
            }
            int size = this.sources.size();
            if (a.length < size) {
                Object[] arr = (Object[])Array.newInstance(arrayElementClass, size);
                a = arr;
            }
            this.fill(a);
            return a;
        }

        private void fill(Object[] array) {
            Iterator<? extends Source> it = this.sources.iterator();
            int i = 0;
            while (it.hasNext()) {
                SourceCache sourceCache = SourceAccessor.getINSTANCE().getCache(it.next());
                array[i] = sourceCache.getSnapshot();
                ++i;
            }
        }

        @Override
        public boolean add(Snapshot o) {
            throw new UnsupportedOperationException("Read only collection.");
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Read only collection.");
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            for (Object e : c) {
                if (this.contains(e)) continue;
                return false;
            }
            return true;
        }

        @Override
        public boolean addAll(Collection<? extends Snapshot> c) {
            throw new UnsupportedOperationException("Read only collection.");
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Read only collection.");
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Read only collection.");
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Read only collection.");
        }

        private static class LazySnapshotsIt
        implements Iterator<Snapshot> {
            private final Iterator<? extends Source> sourcesIt;

            public LazySnapshotsIt(Iterator<? extends Source> sourcesIt) {
                assert (sourcesIt != null);
                this.sourcesIt = sourcesIt;
            }

            @Override
            public boolean hasNext() {
                return this.sourcesIt.hasNext();
            }

            @Override
            public Snapshot next() {
                SourceCache cache = SourceAccessor.getINSTANCE().getCache(this.sourcesIt.next());
                return cache.getSnapshot();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Read only collection.");
            }
        }

    }

    private static class MultiUserTaskAction
    implements Mutex.ExceptionAction<Void> {
        private final UserTask userTask;
        private final Collection<Source> sources;

        public MultiUserTaskAction(Collection<Source> sources, UserTask userTask) {
            assert (sources != null);
            assert (userTask != null);
            this.userTask = userTask;
            this.sources = sources;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Void run() throws Exception {
            LowMemoryWatcher lMListener = LowMemoryWatcher.getInstance();
            Parser parser = null;
            LazySnapshots snapShots = new LazySnapshots(this.sources);
            for (Source source : this.sources) {
                Lookup lookup;
                ParserFactory parserFactory;
                if (parser == null && (parserFactory = (ParserFactory)(lookup = MimeLookup.getLookup((String)source.getMimeType())).lookup(ParserFactory.class)) != null) {
                    parser = parserFactory.createParser(snapShots);
                }
                SourceCache uncachedSourceCache = new SourceCache(source, null, parser);
                SourceCache origCache = SourceAccessor.getINSTANCE().getAndSetCache(source, uncachedSourceCache);
                ResultIterator resultIterator = new ResultIterator(uncachedSourceCache, parser, this.userTask);
                try {
                    TaskProcessor.callUserTask(this.userTask, resultIterator);
                }
                finally {
                    ResultIteratorAccessor.getINSTANCE().invalidate(resultIterator);
                    SourceAccessor.getINSTANCE().getAndSetCache(source, origCache);
                }
                if (!lMListener.isLowMemory()) continue;
                parser = null;
            }
            return null;
        }
    }

    private static class UserTaskAction
    implements Mutex.ExceptionAction<Void> {
        private final UserTask userTask;
        private final Source source;

        public UserTaskAction(Source source, UserTask userTask) {
            assert (source != null);
            assert (userTask != null);
            this.userTask = userTask;
            this.source = source;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Void run() throws Exception {
            SourceCache sourceCache = SourceAccessor.getINSTANCE().getCache(this.source);
            ResultIterator resultIterator = new ResultIterator(sourceCache, this.userTask);
            try {
                TaskProcessor.callUserTask(this.userTask, resultIterator);
            }
            finally {
                ResultIteratorAccessor.getINSTANCE().invalidate(resultIterator);
            }
            return null;
        }
    }

}

