/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.api.indexing;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.event.EventSupport;
import org.netbeans.modules.parsing.impl.indexing.IndexingManagerAccessor;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public final class IndexingManager {
    private static final Logger LOG = Logger.getLogger(IndexingManager.class.getName());
    private static IndexingManager instance;
    private final ThreadLocal<Boolean> inRefreshIndexAndWait = new ThreadLocal();

    public static synchronized IndexingManager getDefault() {
        if (instance == null) {
            instance = new IndexingManager();
        }
        return instance;
    }

    public boolean isIndexing() {
        return Utilities.isScanInProgress();
    }

    public void refreshIndex(URL root, Collection<? extends URL> files) {
        this.refreshIndex(root, files, true, false);
    }

    public void refreshIndex(URL root, Collection<? extends URL> files, boolean fullRescan) {
        this.refreshIndex(root, files, fullRescan, false);
    }

    public void refreshIndex(@NonNull URL root, @NullAllowed Collection<? extends URL> files, boolean fullRescan, boolean checkEditor) {
        this.addIndexingJob(root, files, false, checkEditor, false, fullRescan, true);
    }

    public void refreshIndexAndWait(URL root, Collection<? extends URL> files) {
        this.refreshIndexAndWait(root, files, true, false);
    }

    public void refreshIndexAndWait(URL root, Collection<? extends URL> files, boolean fullRescan) {
        this.refreshIndexAndWait(root, files, fullRescan, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void refreshIndexAndWait(URL root, Collection<? extends URL> files, boolean fullRescan, boolean checkEditor) throws IllegalStateException {
        if (Utilities.holdsParserLock()) {
            throw new IllegalStateException("The caller holds TaskProcessor.parserLock");
        }
        this.inRefreshIndexAndWait.set(Boolean.TRUE);
        try {
            if (IndexingManagerAccessor.getInstance().requiresReleaseOfCompletionLock()) {
                EventSupport.releaseCompletionCondition();
            }
            if (!RepositoryUpdater.getDefault().isIndexer()) {
                this.addIndexingJob(root, files, false, checkEditor, true, fullRescan, false);
            }
        }
        finally {
            this.inRefreshIndexAndWait.remove();
        }
    }

    public void refreshAllIndices(String indexerName) {
        if (indexerName != null) {
            LOG.log(Level.FINEST, "Request to refresh indexer: {0} in all roots.", indexerName);
            RepositoryUpdater.getDefault().addIndexingJob(indexerName, LogContext.create(LogContext.EventType.MANAGER, null));
        } else {
            this.refreshAll(true, false, false, new Object[0]);
        }
    }

    public /* varargs */ void refreshAllIndices(FileObject ... filesOrFolders) {
        this.refreshAllIndices(true, false, filesOrFolders);
    }

    public /* varargs */ void refreshAllIndices(boolean fullRescan, boolean wait, FileObject ... filesOrFolders) {
        this.refreshAll(fullRescan, wait, false, (Object[])filesOrFolders);
    }

    public /* varargs */ void refreshAllIndices(boolean fullRescan, boolean wait, File ... filesOrFolders) {
        this.refreshAll(fullRescan, wait, false, filesOrFolders);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public <T> T runProtected(Callable<T> operation) throws Exception {
        IndexingController.getDefault().enterProtectedMode();
        try {
            T t = operation.call();
            return t;
        }
        finally {
            IndexingController.getDefault().exitProtectedMode(null);
        }
    }

    private IndexingManager() {
        RepositoryUpdater.getDefault().start(false);
    }

    private /* varargs */ void refreshAll(boolean fullRescan, boolean wait, boolean logStatistics, @NullAllowed Object ... filesOrFileObjects) {
        LOG.log(Level.FINEST, "Request to refresh all indexes");
        RepositoryUpdater.getDefault().refreshAll(fullRescan, wait, logStatistics, LogContext.create(LogContext.EventType.MANAGER, null), filesOrFileObjects);
    }

    private void addIndexingJob(@NonNull URL rootUrl, @NullAllowed Collection<? extends URL> fileUrls, boolean followUpJob, boolean checkEditor, boolean wait, boolean forceRefresh, boolean steady) {
        LOG.log(Level.FINEST, "Request to add indexing job for root: {0}", rootUrl);
        RepositoryUpdater.getDefault().addIndexingJob(rootUrl, fileUrls, followUpJob, checkEditor, wait, forceRefresh, steady, LogContext.create(LogContext.EventType.MANAGER, null).withRoot(rootUrl));
    }

    static {
        IndexingManagerAccessor.setInstance(new MyAccessor());
    }

    private static class MyAccessor
    extends IndexingManagerAccessor {
        private MyAccessor() {
        }

        @Override
        public boolean isCalledFromRefreshIndexAndWait(@NonNull IndexingManager manager) {
            Parameters.notNull((CharSequence)"manager", (Object)manager);
            return manager.inRefreshIndexAndWait.get() == Boolean.TRUE;
        }
    }

}

