/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.EmbeddingProvider;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.openide.util.Parameters;

public class EmbeddingProviderFactory
extends TaskFactory {
    public static final String ATTR_TARGET_MIME_TYPE = "targetMimeType";
    public static final String ATTR_PROVIDER = "provider";
    private final Map<String, Object> params;
    private final String targetMimeType;

    private EmbeddingProviderFactory(@NonNull Map<String, Object> params) {
        Parameters.notNull((CharSequence)"definition", params);
        this.params = params;
        this.targetMimeType = (String)params.get("targetMimeType");
        if (this.targetMimeType == null) {
            throw new IllegalArgumentException(String.format("The definition file has no %s attribute.", "targetMimeType"));
        }
    }

    public String getTargetMimeType() {
        return this.targetMimeType;
    }

    @NonNull
    @Override
    public Collection<? extends SchedulerTask> create(@NonNull Snapshot snapshot) {
        Object delegate = this.params.get("provider");
        return delegate instanceof EmbeddingProvider ? Collections.singleton((EmbeddingProvider)delegate) : Collections.emptySet();
    }

    public static TaskFactory create(@NonNull Map<String, Object> params) {
        return new EmbeddingProviderFactory(params);
    }
}

