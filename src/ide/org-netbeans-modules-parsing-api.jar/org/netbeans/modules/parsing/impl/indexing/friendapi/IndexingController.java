/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 */
package org.netbeans.modules.parsing.impl.indexing.friendapi;

import java.net.URL;
import java.util.List;
import java.util.Map;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;

public abstract class IndexingController {
    public static synchronized IndexingController getDefault() {
        return RepositoryUpdater.getDefault().getController();
    }

    public abstract void enterProtectedMode();

    public abstract void exitProtectedMode(Runnable var1);

    public abstract boolean isInProtectedMode();

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public abstract Map<URL, List<URL>> getRootDependencies();

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public abstract Map<URL, List<URL>> getBinaryRootDependencies();

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public abstract Map<URL, List<URL>> getRootPeers();

    public abstract int getFileLocksDelay();

    protected IndexingController() {
    }
}

