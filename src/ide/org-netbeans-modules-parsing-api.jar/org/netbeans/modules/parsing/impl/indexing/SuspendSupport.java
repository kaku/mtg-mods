/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public final class SuspendSupport {
    private static final Logger LOG = Logger.getLogger(SuspendSupport.class.getName());
    private static final boolean NO_SUSPEND = Boolean.getBoolean("SuspendSupport.disabled");
    private final RequestProcessor worker;
    private final Object lock = new Object();
    private final ThreadLocal<Boolean> ignoreSuspend = new ThreadLocal();
    private final SuspendStatus suspendStatus;
    private int suspedDepth;
    public static final SuspendStatus NOP = SPIAccessor.getInstance().createSuspendStatus(new NopImpl());

    @NonNull
    public SuspendStatus getSuspendStatus() {
        return this.suspendStatus;
    }

    SuspendSupport(@NonNull RequestProcessor worker) {
        this.suspendStatus = SPIAccessor.getInstance().createSuspendStatus(new DefaultImpl());
        Parameters.notNull((CharSequence)"worker", (Object)worker);
        this.worker = worker;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void suspend() {
        if (NO_SUSPEND) {
            return;
        }
        if (this.worker.isRequestProcessorThread()) {
            return;
        }
        Object object = this.lock;
        synchronized (object) {
            ++this.suspedDepth;
            if (LOG.isLoggable(Level.FINE) && this.suspedDepth == 1) {
                LOG.log(Level.FINE, "SUSPEND: {0}", Arrays.toString(Thread.currentThread().getStackTrace()));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void resume() {
        if (NO_SUSPEND) {
            return;
        }
        if (this.worker.isRequestProcessorThread()) {
            return;
        }
        Object object = this.lock;
        synchronized (object) {
            assert (this.suspedDepth > 0);
            --this.suspedDepth;
            if (this.suspedDepth == 0) {
                this.lock.notifyAll();
                LOG.fine("RESUME");
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void runWithNoSuspend(Runnable work) {
        this.ignoreSuspend.set(Boolean.TRUE);
        try {
            work.run();
        }
        finally {
            this.ignoreSuspend.remove();
        }
    }

    private final class DefaultImpl
    implements SuspendStatusImpl {
        private DefaultImpl() {
        }

        @Override
        public boolean isSuspendSupported() {
            return SuspendSupport.this.ignoreSuspend.get() != Boolean.TRUE;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean isSuspended() {
            if (SuspendSupport.this.ignoreSuspend.get() == Boolean.TRUE) {
                return false;
            }
            Object object = SuspendSupport.this.lock;
            synchronized (object) {
                return SuspendSupport.this.suspedDepth > 0;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void parkWhileSuspended() throws InterruptedException {
            if (SuspendSupport.this.ignoreSuspend.get() == Boolean.TRUE) {
                return;
            }
            Object object = SuspendSupport.this.lock;
            synchronized (object) {
                boolean parked = false;
                while (SuspendSupport.this.suspedDepth > 0) {
                    LOG.fine("PARK");
                    SuspendSupport.this.lock.wait();
                    parked = true;
                }
                if (LOG.isLoggable(Level.FINE) && parked) {
                    LOG.fine("UNPARK");
                }
            }
        }
    }

    private static final class NopImpl
    implements SuspendStatusImpl {
        private NopImpl() {
        }

        @Override
        public boolean isSuspendSupported() {
            return true;
        }

        @Override
        public boolean isSuspended() {
            return false;
        }

        @Override
        public void parkWhileSuspended() throws InterruptedException {
        }
    }

    public static interface SuspendStatusImpl {
        public boolean isSuspendSupported();

        public boolean isSuspended();

        public void parkWhileSuspended() throws InterruptedException;
    }

}

