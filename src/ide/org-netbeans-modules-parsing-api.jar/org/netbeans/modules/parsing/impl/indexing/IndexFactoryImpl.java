/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.IOException;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.lucene.LayeredDocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;

public interface IndexFactoryImpl {
    @NonNull
    public IndexDocument createDocument(@NonNull Indexable var1);

    @CheckForNull
    public LayeredDocumentIndex createIndex(@NonNull Context var1) throws IOException;

    @CheckForNull
    public DocumentIndexCache getCache(@NonNull Context var1) throws IOException;

    @CheckForNull
    public LayeredDocumentIndex getIndex(@NonNull FileObject var1) throws IOException;
}

