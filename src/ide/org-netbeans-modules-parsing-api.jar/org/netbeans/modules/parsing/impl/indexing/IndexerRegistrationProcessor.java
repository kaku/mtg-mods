/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.ConstrainedBinaryIndexer;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public final class IndexerRegistrationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(ConstrainedBinaryIndexer.Registration.class.getCanonicalName());
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        for (Element e : roundEnv.getElementsAnnotatedWith(ConstrainedBinaryIndexer.Registration.class)) {
            assert (e.getKind().isClass());
            ConstrainedBinaryIndexer.Registration reg = e.getAnnotation(ConstrainedBinaryIndexer.Registration.class);
            Elements elements = this.processingEnv.getElementUtils();
            Types types = this.processingEnv.getTypeUtils();
            TypeElement binIndexerType = elements.getTypeElement(ConstrainedBinaryIndexer.class.getName());
            if (types.isSubtype(((TypeElement)e).asType(), binIndexerType.asType())) {
                String indexerName = reg.indexerName();
                if (indexerName == null) {
                    throw new LayerGenerationException("Indexer name has to be given.", e);
                }
                LayerBuilder.File f = this.layer(new Element[]{e}).instanceFile("Editors", null, null);
                f.stringvalue("instanceClass", BinaryIndexerFactory.class.getName());
                f.methodvalue("instanceCreate", ConstrainedBinaryIndexer.class.getName(), "create");
                f.instanceAttribute("delegate", ConstrainedBinaryIndexer.class);
                if (reg.requiredResource().length > 0) {
                    f.stringvalue("requiredResource", IndexerRegistrationProcessor.list(reg.requiredResource(), e));
                }
                if (reg.mimeType().length > 0) {
                    f.stringvalue("mimeType", IndexerRegistrationProcessor.list(reg.mimeType(), e));
                }
                if (reg.namePattern().length() > 0) {
                    f.stringvalue("namePattern", reg.namePattern());
                }
                f.stringvalue("name", indexerName);
                f.intvalue("version", reg.indexVersion());
                f.write();
                continue;
            }
            throw new LayerGenerationException("Annoated element is not a  subclass of BinaryIndexer.", e);
        }
        return true;
    }

    private static String list(String[] arr, Element e) throws LayerGenerationException {
        if (arr.length == 1) {
            return arr[0];
        }
        StringBuilder sb = new StringBuilder();
        for (String s : arr) {
            if (s.indexOf(44) >= 0) {
                throw new LayerGenerationException("',' is not allowed in the text", e);
            }
            sb.append(s).append(",");
        }
        return sb.substring(0, sb.length() - 1);
    }
}

