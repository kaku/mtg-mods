/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.queries.VisibilityQuery
 *  org.netbeans.spi.queries.VisibilityQueryChangeEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.queries.VisibilityQuery;
import org.netbeans.modules.parsing.impl.indexing.Crawler;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.TimeStamps;
import org.netbeans.spi.queries.VisibilityQueryChangeEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

class VisibilitySupport
implements ChangeListener {
    private static final int VISIBILITY_CHANGE_WINDOW = 1000;
    private static final Logger LOGGER = Logger.getLogger(VisibilitySupport.class.getName());
    private final Map<FileObject, Boolean> visibilityCache = Collections.synchronizedMap(new WeakHashMap());
    private final SlidingTask visibilityTask;
    private final RequestProcessor.Task visibilityChanged;

    private VisibilitySupport(@NonNull RepositoryUpdater ru, @NonNull RequestProcessor worker) {
        this.visibilityTask = new SlidingTask(ru);
        this.visibilityChanged = worker.create((Runnable)this.visibilityTask);
    }

    void start() {
        VisibilityQuery.getDefault().addChangeListener((ChangeListener)this);
    }

    void stop() {
        VisibilityQuery.getDefault().removeChangeListener((ChangeListener)this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean isVisible(@NonNull FileObject file, @NullAllowed FileObject root) {
        long st = 0;
        if (LOGGER.isLoggable(Level.FINER)) {
            st = System.currentTimeMillis();
        }
        try {
            VisibilityQuery vq = VisibilityQuery.getDefault();
            ArrayDeque<FileObject> fta = new ArrayDeque<FileObject>();
            Boolean vote = null;
            boolean folder = false;
            while (root != null && !root.equals((Object)file) && (vote = this.visibilityCache.get((Object)file)) == null) {
                if (folder || file.isFolder()) {
                    fta.offer(file);
                }
                if (!vq.isVisible(file)) {
                    vote = Boolean.FALSE;
                    break;
                }
                file = file.getParent();
                folder = true;
            }
            if (vote == null) {
                vote = vq.isVisible(file);
                fta.offer(file);
            }
            if (!fta.isEmpty()) {
                Map<FileObject, Boolean> map = this.visibilityCache;
                synchronized (map) {
                    for (FileObject nf : fta) {
                        this.visibilityCache.put(nf, vote);
                    }
                }
            }
            boolean bl = vote;
            return bl;
        }
        finally {
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, "reportVisibilityOverhead: {0}", System.currentTimeMillis() - st);
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.visibilityCache.clear();
        if (Crawler.listenOnVisibility()) {
            if (e instanceof VisibilityQueryChangeEvent) {
                FileObject[] affectedFiles = ((VisibilityQueryChangeEvent)e).getFileObjects();
                this.visibilityTask.localChange(affectedFiles);
            } else {
                this.visibilityTask.globalChange();
            }
            this.visibilityChanged.schedule(1000);
        }
    }

    @NonNull
    static VisibilitySupport create(@NonNull RepositoryUpdater ru, @NonNull RequestProcessor worker) {
        Parameters.notNull((CharSequence)"ru", (Object)ru);
        Parameters.notNull((CharSequence)"worker", (Object)worker);
        return new VisibilitySupport(ru, worker);
    }

    private static class SlidingTask
    implements Runnable {
        private final RepositoryUpdater ru;
        private boolean globalChange;
        private final Set<FileObject> localChanges = new HashSet<FileObject>();
        private LogContext visibilityLogCtx;

        SlidingTask(@NonNull RepositoryUpdater ru) {
            this.ru = ru;
        }

        synchronized void globalChange() {
            this.globalChange = true;
            if (this.visibilityLogCtx == null) {
                this.visibilityLogCtx = LogContext.create(LogContext.EventType.FILE, null);
            }
        }

        synchronized /* varargs */ void localChange(FileObject ... onFiles) {
            this.localChanges.addAll(Arrays.asList(onFiles));
            if (this.visibilityLogCtx == null) {
                this.visibilityLogCtx = LogContext.create(LogContext.EventType.FILE, null);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            ArrayList<FileObject> changedFiles;
            boolean global;
            LogContext logCtx;
            SlidingTask slidingTask = this;
            synchronized (slidingTask) {
                logCtx = this.visibilityLogCtx;
                this.visibilityLogCtx = null;
                global = this.globalChange;
                this.globalChange = false;
                changedFiles = new ArrayList<FileObject>(this.localChanges);
                this.localChanges.clear();
            }
            if (global) {
                LOGGER.fine("VisibilityQuery global changed, reindexing");
                this.ru.refreshAll(false, false, true, logCtx, new Object[0]);
            } else {
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "VisibilityQuery changed for {0}, reindexing these files.", Arrays.asList(changedFiles));
                }
                HashMap srcShownPerRoot = new HashMap();
                HashMap<URI, Collection> srcHiddenPerRoot = new HashMap<URI, Collection>();
                HashSet<URI> binChangedRoot = new HashSet<URI>();
                HashMap<URI, TimeStamps> tsPerRoot = new HashMap<URI, TimeStamps>();
                VisibilityQuery vq = VisibilityQuery.getDefault();
                for (FileObject chf : changedFiles) {
                    Pair<URL, FileObject> owner = this.ru.getOwningSourceRoot((Object)chf);
                    if (owner != null) {
                        boolean visible = vq.isVisible(chf);
                        try {
                            Collection files;
                            URI ownerURI = ((URL)owner.first()).toURI();
                            if (visible) {
                                files = (ArrayList<URL>)srcShownPerRoot.get(ownerURI);
                                if (files == null) {
                                    files = new ArrayList<URL>();
                                    srcShownPerRoot.put(ownerURI, files);
                                }
                                if (chf.equals(owner.second())) {
                                    for (FileObject cld : chf.getChildren()) {
                                        files.add(cld.toURL());
                                    }
                                    continue;
                                }
                                files.add((URL)chf.toURL());
                                continue;
                            }
                            if (owner.second() == null) continue;
                            files = (Set)srcHiddenPerRoot.get(ownerURI);
                            if (files == null) {
                                files = new HashSet();
                                srcHiddenPerRoot.put(ownerURI, files);
                            }
                            if (chf.isFolder()) {
                                TimeStamps ts = (TimeStamps)tsPerRoot.get(ownerURI);
                                if (ts == null) {
                                    ts = TimeStamps.forRoot((URL)owner.first(), false);
                                    tsPerRoot.put(ownerURI, ts);
                                }
                                files.addAll(ts.getEnclosedFiles(FileUtil.getRelativePath((FileObject)((FileObject)owner.second()), (FileObject)chf)));
                                continue;
                            }
                            files.add((String)FileUtil.getRelativePath((FileObject)((FileObject)owner.second()), (FileObject)chf));
                        }
                        catch (URISyntaxException e) {
                            Exceptions.printStackTrace((Throwable)e);
                        }
                        catch (IOException e) {
                            Exceptions.printStackTrace((Throwable)e);
                        }
                        continue;
                    }
                    owner = this.ru.getOwningBinaryRoot(chf);
                    if (owner == null) continue;
                    try {
                        URI ownerURI = ((URL)owner.first()).toURI();
                        binChangedRoot.add(ownerURI);
                    }
                    catch (URISyntaxException e) {
                        Exceptions.printStackTrace((Throwable)e);
                    }
                }
                for (Map.Entry e22 : srcShownPerRoot.entrySet()) {
                    try {
                        this.ru.addIndexingJob(((URI)e22.getKey()).toURL(), (Collection)e22.getValue(), false, false, false, false, true, logCtx);
                    }
                    catch (MalformedURLException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                for (Map.Entry e : srcHiddenPerRoot.entrySet()) {
                    try {
                        this.ru.addDeleteJob(((URI)e.getKey()).toURL(), (Set)e.getValue(), logCtx);
                    }
                    catch (MalformedURLException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                for (URI e2 : binChangedRoot) {
                    try {
                        this.ru.addBinaryJob(e2.toURL(), logCtx);
                    }
                    catch (MalformedURLException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
        }
    }

}

