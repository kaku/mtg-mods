/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.netbeans.api.java.classpath.GlobalPathRegistryEvent
 *  org.netbeans.api.java.classpath.GlobalPathRegistryListener
 *  org.netbeans.api.java.queries.SourceForBinaryQuery
 *  org.netbeans.api.java.queries.SourceForBinaryQuery$Result2
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.api.java.classpath.GlobalPathRegistryEvent;
import org.netbeans.api.java.classpath.GlobalPathRegistryListener;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.modules.parsing.impl.indexing.EventKind;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.PathKind;
import org.netbeans.modules.parsing.impl.indexing.PathRecognizerRegistry;
import org.netbeans.modules.parsing.impl.indexing.PathRegistryEvent;
import org.netbeans.modules.parsing.impl.indexing.PathRegistryListener;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class PathRegistry
implements Runnable {
    private static final boolean FIRE_UNKNOWN_ALWAYS = false;
    private static PathRegistry instance;
    private static final RequestProcessor firer;
    private static final RequestProcessor openProjectChange;
    private static final Logger LOGGER;
    private static final Set<String> FAST_HOST_PROTOCOLS;
    private static final String DIGEST_ALGORITHM = "SHA1";
    private static final boolean[] BOOLOUT;
    private final RequestProcessor.Task firerTask;
    private final RequestProcessor.Task openProjectChangeTask;
    private final GlobalPathRegistry regs = GlobalPathRegistry.getDefault();
    private final List<PathRegistryEvent.Change> changes = new LinkedList<PathRegistryEvent.Change>();
    private Map<ClassPath, byte[]> activeCps;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<URL, SourceForBinaryQuery.Result2> sourceResults;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<URL, URL[]> translatedRoots;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<URL, WeakValue> unknownRoots;
    private long timeStamp;
    private volatile Runnable debugCallBack;
    private Collection<URL> sourcePaths;
    private Collection<URL> libraryPath;
    private Collection<URL> binaryLibraryPath;
    private Collection<URL> unknownSourcePath;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<URL, PathIds> rootPathIds;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<String, Set<URL>> pathIdToRoots;
    private final Listener listener;
    private final List<PathRegistryListener> listeners;
    private LogContext logCtx;
    private volatile boolean firstProjectOpened;

    private PathRegistry() {
        assert (this.regs != null);
        this.listener = new Listener();
        this.firerTask = firer.create((Runnable)this, true);
        this.openProjectChangeTask = openProjectChange.create((Runnable)this.listener);
        this.timeStamp = -1;
        this.activeCps = Collections.emptyMap();
        this.sourceResults = Collections.emptyMap();
        this.unknownRoots = new HashMap<URL, WeakValue>();
        this.translatedRoots = new HashMap<URL, URL[]>();
        this.listeners = new CopyOnWriteArrayList<PathRegistryListener>();
        this.regs.addGlobalPathRegistryListener((GlobalPathRegistryListener)WeakListeners.create(GlobalPathRegistryListener.class, (EventListener)this.listener, (Object)this.regs));
        OpenProjects openProjects = OpenProjects.getDefault();
        openProjects.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.listener, (Object)openProjects));
    }

    public static synchronized PathRegistry getDefault() {
        if (instance == null) {
            instance = new PathRegistry();
        }
        return instance;
    }

    void setDebugCallBack(Runnable r) {
        this.debugCallBack = r;
    }

    public void addPathRegistryListener(PathRegistryListener listener) {
        assert (listener != null);
        this.listeners.add(listener);
    }

    public void removePathRegistryListener(PathRegistryListener listener) {
        assert (listener != null);
        this.listeners.remove(listener);
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public URL[] sourceForBinaryQuery(URL binaryRoot, ClassPath definingClassPath, boolean fire) {
        return this.sourceForBinaryQuery(binaryRoot, definingClassPath, fire, BOOLOUT);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    URL[] sourceForBinaryQuery(@NonNull URL binaryRoot, @NullAllowed ClassPath definingClassPath, boolean fire, @NonNull boolean[] newRoot) {
        assert (PathRegistry.noHostPart(binaryRoot));
        newRoot[0] = false;
        URL[] result = this.translatedRoots.get(binaryRoot);
        if (result != null) {
            if (result.length > 0) {
                return result;
            }
            return null;
        }
        if (definingClassPath != null) {
            Collection<? extends URL> unknownRes;
            ArrayList cacheRoots = new ArrayList();
            try {
                unknownRes = PathRegistry.getSources(SourceForBinaryQuery.findSourceRoots2((URL)binaryRoot), cacheRoots, null);
            }
            catch (IllegalArgumentException iae3) {
                throw new IllegalArgumentException("Defining cp URLs: " + definingClassPath.entries(), iae3);
            }
            if (unknownRes.isEmpty()) {
                return null;
            }
            result = new URL[unknownRes.size()];
            PathRegistry iae3 = this;
            synchronized (iae3) {
                int i = 0;
                for (URL u : unknownRes) {
                    result[i++] = u;
                    if (this.unknownRoots.containsKey(u)) continue;
                    this.unknownRoots.put(u, new WeakValue(definingClassPath, u));
                    newRoot[0] = true;
                }
            }
            return result;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void registerUnknownSourceRoots(ClassPath owner, Iterable<? extends URL> roots) {
        assert (owner != null);
        assert (roots != null);
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            for (URL root : roots) {
                this.unknownRoots.put(root, new WeakValue(owner, root));
            }
            this.unknownSourcePath = new HashSet<URL>(this.unknownRoots.keySet());
            this.changes.add(new PathRegistryEvent.Change(EventKind.PATHS_ADDED, PathKind.UNKNOWN_SOURCE, null, Collections.singleton(owner)));
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LinkedList<URL> l = new LinkedList<URL>();
            for (URL r : roots) {
                l.add(r);
            }
            LOGGER.log(Level.FINE, "registerUnknownSourceRoots: {0}", l);
        }
        this.scheduleFirer(roots);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void unregisterUnknownSourceRoots(@NonNull Iterable<? extends URL> roots) {
        Parameters.notNull((CharSequence)"roots", roots);
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            for (URL root : roots) {
                this.unknownRoots.remove(root);
                if (this.unknownSourcePath == null) continue;
                this.unknownSourcePath.remove(root);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<? extends URL> getSources() {
        Request request;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.sourcePaths != null) {
                return this.sourcePaths;
            }
            LOGGER.fine("Computing data due to getSources");
            request = new Request(this.getTimeStamp(), this.getSourcePaths(), this.getLibraryPaths(), this.getBinaryLibraryPaths(), new HashMap<ClassPath, byte[]>(this.activeCps), new HashMap<URL, SourceForBinaryQuery.Result2>(this.sourceResults), new HashMap<URL, WeakValue>(this.unknownRoots), this.listener, this.listener);
        }
        Result res = PathRegistry.createResources(request);
        if (this.debugCallBack != null) {
            this.debugCallBack.run();
        }
        PathRegistry pathRegistry2 = this;
        synchronized (pathRegistry2) {
            if (this.getTimeStamp() == res.timeStamp) {
                if (this.sourcePaths == null) {
                    this.sourcePaths = res.sourcePath;
                    this.libraryPath = res.libraryPath;
                    this.binaryLibraryPath = res.binaryLibraryPath;
                    this.unknownSourcePath = res.unknownSourcePath;
                    this.activeCps = res.newCps;
                    this.sourceResults = res.newSR;
                    this.translatedRoots = res.translatedRoots;
                    this.unknownRoots = res.unknownRoots;
                    this.rootPathIds = res.rootPathIds;
                    this.pathIdToRoots = res.pathIdToRoots;
                }
                return this.sourcePaths;
            }
            return res.sourcePath;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<? extends URL> getLibraries() {
        Request request;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.libraryPath != null) {
                return this.libraryPath;
            }
            LOGGER.fine("Computing data due to getLibraries");
            request = new Request(this.getTimeStamp(), this.getSourcePaths(), this.getLibraryPaths(), this.getBinaryLibraryPaths(), new HashMap<ClassPath, byte[]>(this.activeCps), new HashMap<URL, SourceForBinaryQuery.Result2>(this.sourceResults), new HashMap<URL, WeakValue>(this.unknownRoots), this.listener, this.listener);
        }
        Result res = PathRegistry.createResources(request);
        if (this.debugCallBack != null) {
            this.debugCallBack.run();
        }
        PathRegistry pathRegistry2 = this;
        synchronized (pathRegistry2) {
            if (this.getTimeStamp() == res.timeStamp) {
                if (this.libraryPath == null) {
                    this.sourcePaths = res.sourcePath;
                    this.libraryPath = res.libraryPath;
                    this.binaryLibraryPath = res.binaryLibraryPath;
                    this.unknownSourcePath = res.unknownSourcePath;
                    this.activeCps = res.newCps;
                    this.sourceResults = res.newSR;
                    this.translatedRoots = res.translatedRoots;
                    this.unknownRoots = res.unknownRoots;
                    this.rootPathIds = res.rootPathIds;
                    this.pathIdToRoots = res.pathIdToRoots;
                }
                return this.libraryPath;
            }
            return res.libraryPath;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<? extends URL> getBinaryLibraries() {
        Request request;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.binaryLibraryPath != null) {
                return this.binaryLibraryPath;
            }
            LOGGER.fine("Computing data due to getBinaryLibraries");
            request = new Request(this.getTimeStamp(), this.getSourcePaths(), this.getLibraryPaths(), this.getBinaryLibraryPaths(), new HashMap<ClassPath, byte[]>(this.activeCps), new HashMap<URL, SourceForBinaryQuery.Result2>(this.sourceResults), new HashMap<URL, WeakValue>(this.unknownRoots), this.listener, this.listener);
        }
        Result res = PathRegistry.createResources(request);
        if (this.debugCallBack != null) {
            this.debugCallBack.run();
        }
        PathRegistry pathRegistry2 = this;
        synchronized (pathRegistry2) {
            if (this.getTimeStamp() == res.timeStamp) {
                if (this.binaryLibraryPath == null) {
                    this.sourcePaths = res.sourcePath;
                    this.libraryPath = res.libraryPath;
                    this.binaryLibraryPath = res.binaryLibraryPath;
                    this.unknownSourcePath = res.unknownSourcePath;
                    this.activeCps = res.newCps;
                    this.sourceResults = res.newSR;
                    this.translatedRoots = res.translatedRoots;
                    this.unknownRoots = res.unknownRoots;
                    this.rootPathIds = res.rootPathIds;
                    this.pathIdToRoots = res.pathIdToRoots;
                }
                return this.binaryLibraryPath;
            }
            return res.binaryLibraryPath;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<? extends URL> getUnknownRoots() {
        Request request;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.unknownSourcePath != null) {
                return this.unknownSourcePath;
            }
            LOGGER.fine("Computing data due to getUnknownRoots");
            request = new Request(this.getTimeStamp(), this.getSourcePaths(), this.getLibraryPaths(), this.getBinaryLibraryPaths(), new HashMap<ClassPath, byte[]>(this.activeCps), new HashMap<URL, SourceForBinaryQuery.Result2>(this.sourceResults), new HashMap<URL, WeakValue>(this.unknownRoots), this.listener, this.listener);
        }
        Result res = PathRegistry.createResources(request);
        if (this.debugCallBack != null) {
            this.debugCallBack.run();
        }
        PathRegistry pathRegistry2 = this;
        synchronized (pathRegistry2) {
            if (this.getTimeStamp() == res.timeStamp) {
                if (this.unknownSourcePath == null) {
                    this.sourcePaths = res.sourcePath;
                    this.libraryPath = res.libraryPath;
                    this.binaryLibraryPath = res.binaryLibraryPath;
                    this.unknownSourcePath = res.unknownSourcePath;
                    this.activeCps = res.newCps;
                    this.sourceResults = res.newSR;
                    this.translatedRoots = res.translatedRoots;
                    this.unknownRoots = res.unknownRoots;
                    this.rootPathIds = res.rootPathIds;
                    this.pathIdToRoots = res.pathIdToRoots;
                }
                return this.unknownSourcePath;
            }
            return res.unknownSourcePath;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isKnownRoot(URL root) {
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            return this.rootPathIds != null && this.rootPathIds.containsKey(root) || this.unknownSourcePath != null && this.unknownSourcePath.contains(root);
        }
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public Set<String> getSourceIdsFor(URL root) {
        assert (PathRegistry.noHostPart(root));
        PathIds pathIds = this.getRootPathIds().get(root);
        return pathIds != null ? pathIds.getSids() : null;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public Set<String> getLibraryIdsFor(URL root) {
        assert (PathRegistry.noHostPart(root));
        PathIds pathIds = this.getRootPathIds().get(root);
        return pathIds != null ? pathIds.getLids() : null;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public /* varargs */ Set<URL> getRootsMarkedAs(String ... pathIds) {
        Map<String, Set<URL>> rootsByPathIds = this.getPathIdToRoots();
        HashSet<URL> roots = new HashSet<URL>();
        for (String id : pathIds) {
            Set<URL> idRoots = rootsByPathIds.get(id);
            if (idRoots == null) continue;
            roots.addAll(idRoots);
        }
        return roots;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public Set<String> getMimeTypesFor(URL root) {
        assert (PathRegistry.noHostPart(root));
        PathIds pathIds = this.getRootPathIds().get(root);
        return pathIds != null ? pathIds.getMimeTypes() : null;
    }

    public boolean isFinished() {
        return this.firerTask.isFinished();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        ArrayList<PathRegistryEvent.Change> ch;
        LogContext ctx;
        assert (firer.isRequestProcessorThread());
        long now = System.currentTimeMillis();
        try {
            LOGGER.log(Level.FINE, "resetCacheAndFire waiting for projects");
            OpenProjects.getDefault().openProjects().get();
            LOGGER.log(Level.FINE, "resetCacheAndFire blocked for {0} ms", System.currentTimeMillis() - now);
        }
        catch (Exception ex) {
            LOGGER.log(Level.FINE, "resetCacheAndFire timeout", ex);
        }
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            ch = new ArrayList<PathRegistryEvent.Change>(this.changes);
            ctx = this.logCtx;
            this.logCtx = null;
            this.changes.clear();
        }
        this.fire(ch, ctx);
        LOGGER.log(Level.FINE, "resetCacheAndFire, firing done");
    }

    public static boolean noHostPart(@NonNull URL url) {
        return url.getHost() == null || url.getHost().isEmpty() ? true : FAST_HOST_PROTOCOLS.contains("jar".equals(url.getProtocol()) ? FileUtil.getArchiveFile((URL)url).getProtocol() : url.getProtocol());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<URL, PathIds> getRootPathIds() {
        Request request;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.rootPathIds != null) {
                return this.rootPathIds;
            }
            LOGGER.fine("Computing data due to getRootPathIds");
            request = new Request(this.getTimeStamp(), this.getSourcePaths(), this.getLibraryPaths(), this.getBinaryLibraryPaths(), new HashMap<ClassPath, byte[]>(this.activeCps), new HashMap<URL, SourceForBinaryQuery.Result2>(this.sourceResults), new HashMap<URL, WeakValue>(this.unknownRoots), this.listener, this.listener);
        }
        Result res = PathRegistry.createResources(request);
        if (this.debugCallBack != null) {
            this.debugCallBack.run();
        }
        PathRegistry pathRegistry2 = this;
        synchronized (pathRegistry2) {
            if (this.getTimeStamp() == res.timeStamp) {
                if (this.rootPathIds == null) {
                    this.sourcePaths = res.sourcePath;
                    this.libraryPath = res.libraryPath;
                    this.binaryLibraryPath = res.binaryLibraryPath;
                    this.unknownSourcePath = res.unknownSourcePath;
                    this.activeCps = res.newCps;
                    this.sourceResults = res.newSR;
                    this.translatedRoots = res.translatedRoots;
                    this.unknownRoots = res.unknownRoots;
                    this.rootPathIds = res.rootPathIds;
                    this.pathIdToRoots = res.pathIdToRoots;
                }
                return this.rootPathIds;
            }
            return res.rootPathIds;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<String, Set<URL>> getPathIdToRoots() {
        Request request;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.pathIdToRoots != null) {
                return this.pathIdToRoots;
            }
            LOGGER.fine("Computing data due to getPathIdToRoots");
            request = new Request(this.getTimeStamp(), this.getSourcePaths(), this.getLibraryPaths(), this.getBinaryLibraryPaths(), new HashMap<ClassPath, byte[]>(this.activeCps), new HashMap<URL, SourceForBinaryQuery.Result2>(this.sourceResults), new HashMap<URL, WeakValue>(this.unknownRoots), this.listener, this.listener);
        }
        Result res = PathRegistry.createResources(request);
        if (this.debugCallBack != null) {
            this.debugCallBack.run();
        }
        PathRegistry pathRegistry2 = this;
        synchronized (pathRegistry2) {
            if (this.getTimeStamp() == res.timeStamp) {
                if (this.pathIdToRoots == null) {
                    this.sourcePaths = res.sourcePath;
                    this.libraryPath = res.libraryPath;
                    this.binaryLibraryPath = res.binaryLibraryPath;
                    this.unknownSourcePath = res.unknownSourcePath;
                    this.activeCps = res.newCps;
                    this.sourceResults = res.newSR;
                    this.translatedRoots = res.translatedRoots;
                    this.unknownRoots = res.unknownRoots;
                    this.rootPathIds = res.rootPathIds;
                    this.pathIdToRoots = res.pathIdToRoots;
                }
                return this.pathIdToRoots;
            }
            return res.pathIdToRoots;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean classPathChanged(@NonNull ClassPath cp) {
        byte[] oldHash;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            oldHash = this.activeCps.get((Object)cp);
        }
        if (oldHash == null) {
            return true;
        }
        StringBuilder newRoots = new StringBuilder();
        for (ClassPath.Entry e : cp.entries()) {
            newRoots.append(e.getURL().toExternalForm()).append(File.pathSeparatorChar);
        }
        return !Arrays.equals(oldHash, PathRegistry.toDigest(PathRegistry.createDigest(), newRoots));
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static Result createResources(Request request) {
        ClassPath cp;
        URL root;
        boolean isNew;
        boolean notContained;
        StringBuilder sb;
        assert (request != null);
        HashSet<URL> sourceResult = new HashSet<URL>();
        HashSet<URL> unknownResult = new HashSet<URL>();
        HashSet<URL> libraryResult = new HashSet<URL>();
        HashSet<URL> binaryLibraryResult = new HashSet<URL>();
        HashMap<URL, URL[]> translatedRoots = new HashMap<URL, URL[]>();
        HashMap<ClassPath, byte[]> newCps = new HashMap<ClassPath, byte[]>();
        HashMap<URL, SourceForBinaryQuery.Result2> newSR = new HashMap<URL, SourceForBinaryQuery.Result2>();
        HashMap<URL, PathIds> pathIdsResult = new HashMap<URL, PathIds>();
        HashMap<String, Set<URL>> pathIdToRootsResult = new HashMap<String, Set<URL>>();
        MessageDigest digest = PathRegistry.createDigest();
        for (TaggedClassPath tcp22 : request.sourceCps) {
            cp = tcp22.getClasspath();
            isNew = request.oldCps.remove((Object)cp) == null;
            sb = new StringBuilder();
            for (ClassPath.Entry entry : cp.entries()) {
                root = entry.getURL();
                assert (PathRegistry.noHostPart(root));
                sb.append(root.toExternalForm()).append(File.pathSeparatorChar);
                sourceResult.add(root);
                PathRegistry.updatePathIds(root, tcp22, pathIdsResult, pathIdToRootsResult);
            }
            boolean bl = notContained = newCps.put(cp, PathRegistry.toDigest(digest, sb)) == null;
            if (!isNew || !notContained) continue;
            cp.addPropertyChangeListener(request.propertyListener);
        }
        for (TaggedClassPath tcp : request.libraryCps) {
            cp = tcp.getClasspath();
            isNew = request.oldCps.remove((Object)cp) == null;
            sb = new StringBuilder();
            for (ClassPath.Entry entry : cp.entries()) {
                root = entry.getURL();
                assert (PathRegistry.noHostPart(root));
                sb.append(root.toExternalForm()).append(File.pathSeparatorChar);
                libraryResult.add(root);
                PathRegistry.updatePathIds(root, tcp, pathIdsResult, pathIdToRootsResult);
            }
            boolean bl = notContained = newCps.put(cp, PathRegistry.toDigest(digest, sb)) == null;
            if (!isNew || !notContained) continue;
            cp.addPropertyChangeListener(request.propertyListener);
        }
        for (TaggedClassPath tcp2 : request.binaryLibraryCps) {
            cp = tcp2.getClasspath();
            isNew = request.oldCps.remove((Object)cp) == null;
            sb = new StringBuilder();
            for (ClassPath.Entry entry : cp.entries()) {
                boolean isNewSR;
                URL binRoot = entry.getURL();
                assert (PathRegistry.noHostPart(binRoot));
                sb.append(binRoot.toExternalForm()).append(File.pathSeparatorChar);
                if (translatedRoots.containsKey(binRoot)) continue;
                PathRegistry.updatePathIds(binRoot, tcp2, pathIdsResult, pathIdToRootsResult);
                SourceForBinaryQuery.Result2 sr = request.oldSR.remove(binRoot);
                if (sr == null) {
                    sr = SourceForBinaryQuery.findSourceRoots2((URL)binRoot);
                    isNewSR = true;
                } else {
                    isNewSR = false;
                }
                assert (!newSR.containsKey(binRoot));
                newSR.put(binRoot, sr);
                LOGGER.log(Level.FINE, "{0}: preferSources={1}", new Object[]{binRoot, sr.preferSources()});
                LinkedHashSet cacheURLs = new LinkedHashSet();
                Collection<? extends URL> srcRoots = PathRegistry.getSources(sr, cacheURLs, request.unknownRoots);
                if (srcRoots.isEmpty()) {
                    binaryLibraryResult.add(binRoot);
                } else {
                    libraryResult.addAll(srcRoots);
                    PathRegistry.updateTranslatedPathIds(srcRoots, tcp2, pathIdsResult, pathIdToRootsResult);
                }
                translatedRoots.put(binRoot, cacheURLs.toArray(new URL[cacheURLs.size()]));
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "T: {0} -> {1}", new Object[]{binRoot, cacheURLs});
                }
                if (!isNewSR) continue;
                sr.addChangeListener(request.changeListener);
            }
            boolean bl = notContained = newCps.put(cp, PathRegistry.toDigest(digest, sb)) == null;
            if (!isNew || !notContained) continue;
            cp.addPropertyChangeListener(request.propertyListener);
        }
        for (ClassPath cp2 : request.oldCps.keySet()) {
            cp2.removePropertyChangeListener(request.propertyListener);
        }
        for (Map.Entry entry : request.oldSR.entrySet()) {
            ((SourceForBinaryQuery.Result2)entry.getValue()).removeChangeListener(request.changeListener);
        }
        unknownResult.addAll(request.unknownRoots.keySet());
        return new Result(request.timeStamp, sourceResult, libraryResult, binaryLibraryResult, unknownResult, newCps, newSR, translatedRoots, request.unknownRoots, pathIdsResult, pathIdToRootsResult);
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static Collection<? extends URL> getSources(SourceForBinaryQuery.Result2 sr, Collection<? super URL> cacheDirs, Map<URL, WeakValue> unknownRoots) {
        assert (sr != null);
        if (sr.preferSources()) {
            FileObject[] roots = sr.getRoots();
            assert (roots != null);
            ArrayList<URL> result = new ArrayList<URL>(roots.length);
            for (int i = 0; i < roots.length; ++i) {
                try {
                    URL url = roots[i].getURL();
                    assert (PathRegistry.noHostPart(url));
                    if (cacheDirs != null) {
                        cacheDirs.add(url);
                    }
                    if (unknownRoots != null) {
                        unknownRoots.remove(url);
                    }
                    result.add(url);
                    continue;
                }
                catch (FileStateInvalidException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
            return result;
        }
        return Collections.emptySet();
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static void updatePathIds(URL root, TaggedClassPath tcp, Map<URL, PathIds> pathIdsResult, Map<String, Set<URL>> pathIdToRootsResult) {
        PathIds pathIds = pathIdsResult.get(root);
        if (pathIds == null) {
            pathIds = new PathIds();
            pathIdsResult.put(root, pathIds);
        }
        pathIds.addAll(tcp.getPathIds());
        for (String id : tcp.getPathIds().getAllIds()) {
            Set<URL> rootsWithId = pathIdToRootsResult.get(id);
            if (rootsWithId == null) {
                rootsWithId = new HashSet<URL>();
                pathIdToRootsResult.put(id, rootsWithId);
            }
            rootsWithId.add(root);
        }
        LOGGER.log(Level.FINE, "Root {0} associated with {1}", new Object[]{root, tcp.getPathIds()});
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static void updateTranslatedPathIds(Collection<? extends URL> roots, TaggedClassPath tcp, Map<URL, PathIds> pathIdsResult, Map<String, Set<URL>> pathIdToRootsResult) {
        HashSet<String> sids = new HashSet<String>();
        HashSet<String> mimeTypes = new HashSet<String>();
        for (String blid : tcp.getPathIds().getBlids()) {
            Set<String> mts;
            Set<String> ids = PathRecognizerRegistry.getDefault().getSourceIdsForBinaryLibraryId(blid);
            if (ids != null) {
                sids.addAll(ids);
            }
            if ((mts = PathRecognizerRegistry.getDefault().getMimeTypesForBinaryLibraryId(blid)) == null) continue;
            mimeTypes.addAll(mts);
        }
        for (URL root : roots) {
            PathIds pathIds = pathIdsResult.get(root);
            if (pathIds == null) {
                pathIds = new PathIds();
                pathIdsResult.put(root, pathIds);
            }
            pathIds.getSids().addAll(sids);
            pathIds.getMimeTypes().addAll(mimeTypes);
            for (String id : sids) {
                Set<URL> rootsWithId = pathIdToRootsResult.get(id);
                if (rootsWithId == null) {
                    rootsWithId = new HashSet<URL>();
                    pathIdToRootsResult.put(id, rootsWithId);
                }
                rootsWithId.add(root);
            }
            LOGGER.log(Level.FINE, "Root {0} associated with {1}", new Object[]{root, tcp.getPathIds()});
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    private static byte[] toDigest(@NonNull MessageDigest md, @NonNull StringBuilder sb) {
        try {
            byte[] arrby = md.digest(sb.toString().getBytes());
            return arrby;
        }
        finally {
            md.reset();
        }
    }

    @NonNull
    private static MessageDigest createDigest() {
        try {
            return MessageDigest.getInstance("SHA1");
        }
        catch (NoSuchAlgorithmException ex) {
            return new IdentityDigest();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void resetCacheAndFire(@NonNull EventKind eventKind, @NonNull PathKind pathKind, String pathId, @NonNull Set<? extends ClassPath> paths) {
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            this.sourcePaths = null;
            this.libraryPath = null;
            this.binaryLibraryPath = null;
            this.unknownSourcePath = null;
            this.rootPathIds = null;
            this.pathIdToRoots = null;
            ++this.timeStamp;
            this.changes.add(new PathRegistryEvent.Change(eventKind, pathKind, pathId, paths));
        }
        LOGGER.log(Level.FINE, "resetCacheAndFire: eventKind={0}, pathKind={1}, pathId={2}, paths={3}", new Object[]{eventKind, pathKind, pathId, paths});
        this.scheduleFirer((Collection<? extends ClassPath>)paths);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void scheduleFirer(Collection<? extends ClassPath> paths) {
        LogContext _logCtx;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.logCtx == null) {
                this.logCtx = LogContext.create(LogContext.EventType.PATH, null);
            }
            _logCtx = this.logCtx;
        }
        assert (_logCtx != null);
        _logCtx.addPaths(paths);
        this.scheduleImpl();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void scheduleFirer(Iterable<? extends URL> roots) {
        LogContext _logCtx;
        PathRegistry pathRegistry = this;
        synchronized (pathRegistry) {
            if (this.logCtx == null) {
                this.logCtx = LogContext.create(LogContext.EventType.PATH, null);
            }
            _logCtx = this.logCtx;
        }
        assert (_logCtx != null);
        _logCtx.addRoots(roots);
        this.scheduleImpl();
    }

    private void scheduleImpl() {
        if (!this.firstProjectOpened) {
            this.firstProjectOpened = true;
        }
        this.firerTask.schedule(0);
    }

    private void fire(Iterable<? extends PathRegistryEvent.Change> changes, LogContext ctx) {
        PathRegistryEvent event = new PathRegistryEvent(this, changes, ctx);
        for (PathRegistryListener l : this.listeners) {
            l.pathsChanged(event);
        }
    }

    private PathKind getPathKind(String pathId) {
        assert (pathId != null);
        if (pathId == null) {
            return null;
        }
        Set<String> sIds = PathRecognizerRegistry.getDefault().getSourceIds();
        if (sIds.contains(pathId)) {
            return PathKind.SOURCE;
        }
        Set<String> lIds = PathRecognizerRegistry.getDefault().getLibraryIds();
        if (lIds.contains(pathId)) {
            return PathKind.LIBRARY;
        }
        Set<String> bIds = PathRecognizerRegistry.getDefault().getBinaryLibraryIds();
        if (bIds.contains(pathId)) {
            return PathKind.BINARY_LIBRARY;
        }
        return null;
    }

    private Collection<TaggedClassPath> getSourcePaths() {
        return this.getPaths(PathKind.SOURCE);
    }

    private Collection<TaggedClassPath> getLibraryPaths() {
        return this.getPaths(PathKind.LIBRARY);
    }

    private Collection<TaggedClassPath> getBinaryLibraryPaths() {
        return this.getPaths(PathKind.BINARY_LIBRARY);
    }

    private Collection<TaggedClassPath> getPaths(PathKind kind) {
        Set<String> ids;
        switch (kind) {
            case SOURCE: {
                ids = PathRecognizerRegistry.getDefault().getSourceIds();
                break;
            }
            case LIBRARY: {
                ids = PathRecognizerRegistry.getDefault().getLibraryIds();
                break;
            }
            case BINARY_LIBRARY: {
                ids = PathRecognizerRegistry.getDefault().getBinaryLibraryIds();
                break;
            }
            default: {
                LOGGER.log(Level.WARNING, "Not expecting PathKind of {0}", (Object)kind);
                return Collections.emptySet();
            }
        }
        HashMap<ClassPath, TaggedClassPath> result = new HashMap<ClassPath, TaggedClassPath>();
        for (String id : ids) {
            for (ClassPath cp : this.regs.getPaths(id)) {
                TaggedClassPath tcp = (TaggedClassPath)result.get((Object)cp);
                if (tcp == null) {
                    tcp = new TaggedClassPath(cp);
                    result.put(cp, tcp);
                }
                switch (kind) {
                    case SOURCE: {
                        tcp.associateWithSourceId(id);
                        tcp.associateWithMimeTypes(PathRecognizerRegistry.getDefault().getMimeTypesForSourceId(id));
                        break;
                    }
                    case LIBRARY: {
                        tcp.associateWithLibraryId(id);
                        tcp.associateWithMimeTypes(PathRecognizerRegistry.getDefault().getMimeTypesForLibraryId(id));
                        break;
                    }
                    case BINARY_LIBRARY: {
                        tcp.associateWithBinaryLibraryId(id);
                    }
                }
            }
        }
        return result.values();
    }

    private long getTimeStamp() {
        return this.timeStamp;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    static {
        firer = new RequestProcessor("Path Registry Request Processor");
        openProjectChange = new RequestProcessor("Waiting for project loading");
        LOGGER = Logger.getLogger(PathRegistry.class.getName());
        FAST_HOST_PROTOCOLS = new HashSet<String>(Arrays.asList("file", "nbfs", "rfs"));
        BOOLOUT = new boolean[1];
    }

    static class IdentityDigest
    extends MessageDigest {
        private byte[] buffer = new byte[1024];
        private int length = 0;

        IdentityDigest() {
            super(IdentityDigest.class.getSimpleName());
        }

        @Override
        protected void engineUpdate(byte input) {
            this.ensureSize(1);
            this.buffer[this.length++] = input;
        }

        @Override
        protected void engineUpdate(byte[] input, int offset, int len) {
            this.ensureSize(len);
            System.arraycopy(input, offset, this.buffer, this.length, len);
            this.length += len;
        }

        @Override
        protected byte[] engineDigest() {
            return Arrays.copyOf(this.buffer, this.length);
        }

        @Override
        protected void engineReset() {
            this.length = 0;
        }

        private void ensureSize(int needed) {
            int size;
            for (size = this.buffer.length; size < needed + this.length; size <<= 1) {
            }
            if (size != this.buffer.length) {
                this.buffer = new byte[size];
            }
        }
    }

    private static final class PathIds {
        private final Set<String> sourcePathIds = new HashSet<String>();
        private final Set<String> libraryPathIds = new HashSet<String>();
        private final Set<String> binaryLibraryPathIds = new HashSet<String>();
        private final Set<String> mimeTypes = new HashSet<String>();

        private PathIds() {
        }

        public Set<String> getSids() {
            return this.sourcePathIds;
        }

        public Set<String> getLids() {
            return this.libraryPathIds;
        }

        public Set<String> getBlids() {
            return this.binaryLibraryPathIds;
        }

        public Set<String> getMimeTypes() {
            return this.mimeTypes;
        }

        public Set<String> getAllIds() {
            HashSet<String> allIds = new HashSet<String>();
            allIds.addAll(this.sourcePathIds);
            allIds.addAll(this.libraryPathIds);
            allIds.addAll(this.binaryLibraryPathIds);
            return allIds;
        }

        public void addAll(PathIds pathIds) {
            this.sourcePathIds.addAll(pathIds.getSids());
            this.libraryPathIds.addAll(pathIds.getLids());
            this.binaryLibraryPathIds.addAll(pathIds.getBlids());
            this.mimeTypes.addAll(pathIds.getMimeTypes());
        }

        public String toString() {
            return super.toString() + ";sids=" + this.sourcePathIds + ", lids=" + this.libraryPathIds + ", blids=" + this.binaryLibraryPathIds;
        }
    }

    private static final class TaggedClassPath {
        private final ClassPath classpath;
        private final PathIds pathIds = new PathIds();

        public TaggedClassPath(ClassPath classpath) {
            this.classpath = classpath;
        }

        public ClassPath getClasspath() {
            return this.classpath;
        }

        public PathIds getPathIds() {
            return this.pathIds;
        }

        public void associateWithSourceId(String id) {
            this.pathIds.getSids().add(id);
        }

        public void associateWithLibraryId(String id) {
            this.pathIds.getLids().add(id);
        }

        public void associateWithBinaryLibraryId(String id) {
            this.pathIds.getBlids().add(id);
        }

        public void associateWithMimeTypes(Set<String> mimeTypes) {
            this.pathIds.getMimeTypes().addAll(mimeTypes);
        }
    }

    private class Listener
    implements GlobalPathRegistryListener,
    PropertyChangeListener,
    ChangeListener,
    Runnable {
        private WeakReference<Object> lastPropagationId;

        private Listener() {
        }

        public void pathsAdded(GlobalPathRegistryEvent event) {
            String pathId = event.getId();
            PathKind pk = PathRegistry.this.getPathKind(pathId);
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "pathsAdded: {0}, paths= {1}", new Object[]{event.getId(), event.getChangedPaths()});
                LOGGER.log(Level.FINE, "''{0}'' -> ''{1}''", new Object[]{pathId, pk});
            }
            if (pk != null) {
                PathRegistry.this.resetCacheAndFire(EventKind.PATHS_ADDED, pk, pathId, event.getChangedPaths());
            }
        }

        public void pathsRemoved(GlobalPathRegistryEvent event) {
            String pathId = event.getId();
            PathKind pk = PathRegistry.this.getPathKind(pathId);
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "pathsRemoved: {0}, paths={1}", new Object[]{event.getId(), event.getChangedPaths()});
                LOGGER.log(Level.FINE, "''{0}'' -> ''{1}''", new Object[]{pathId, pk});
            }
            if (pk != null) {
                PathRegistry.this.resetCacheAndFire(EventKind.PATHS_REMOVED, pk, pathId, event.getChangedPaths());
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propName;
            if (LOGGER.isLoggable(Level.FINE)) {
                String msg = "propertyChange: " + evt.getPropertyName() + ", old=" + evt.getOldValue() + ", new=" + evt.getNewValue() + ", source=" + PathRegistry.s2s(evt.getSource());
                LOGGER.log(Level.FINE, msg);
            }
            if ("entries".equals(propName = evt.getPropertyName())) {
                Object source = evt.getSource();
                if (source instanceof ClassPath && PathRegistry.this.classPathChanged((ClassPath)source)) {
                    PathRegistry.this.resetCacheAndFire(EventKind.PATHS_CHANGED, null, null, Collections.singleton((ClassPath)evt.getSource()));
                }
            } else if ("includes".equals(propName)) {
                boolean fire;
                Object newPropagationId = evt.getPropagationId();
                Listener listener = this;
                synchronized (listener) {
                    fire = newPropagationId == null || this.lastPropagationId == null || this.lastPropagationId.get() != newPropagationId;
                    this.lastPropagationId = new WeakReference<Object>(newPropagationId);
                }
                if (fire) {
                    PathRegistry.this.resetCacheAndFire(EventKind.INCLUDES_CHANGED, PathKind.SOURCE, null, Collections.singleton((ClassPath)evt.getSource()));
                }
            } else if ("openProjects".equals(propName) && !PathRegistry.this.firstProjectOpened) {
                PathRegistry.this.openProjectChangeTask.schedule(0);
            }
        }

        @Override
        public void stateChanged(ChangeEvent event) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "stateChanged: {0}", event);
            }
            PathRegistry.this.resetCacheAndFire(EventKind.PATHS_CHANGED, PathKind.BINARY_LIBRARY, null, null);
        }

        @Override
        public void run() {
            try {
                int len = ((Project[])OpenProjects.getDefault().openProjects().get()).length;
                if (!PathRegistry.this.firstProjectOpened && len > 0) {
                    PathRegistry.this.firstProjectOpened = true;
                    PathRegistry.this.fire(Collections.singleton(new PathRegistryEvent.Change(EventKind.PATHS_CHANGED, PathKind.SOURCE, null, Collections.emptySet())), LogContext.create(LogContext.EventType.PATH, "Unsupported project(s) opened."));
                }
            }
            catch (InterruptedException ex) {
                LOGGER.fine("Interrupted while waiting for projects to be loaded.");
            }
            catch (ExecutionException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private class WeakValue
    extends WeakReference<ClassPath>
    implements Runnable {
        private URL key;

        public WeakValue(ClassPath ref, URL key) {
            super(ref, Utilities.activeReferenceQueue());
            assert (key != null);
            this.key = key;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            boolean fire = false;
            PathRegistry pathRegistry = PathRegistry.this;
            synchronized (pathRegistry) {
                fire = PathRegistry.this.unknownRoots.remove(this.key) != null;
            }
        }
    }

    private static class Result {
        final long timeStamp;
        final Collection<URL> sourcePath;
        final Collection<URL> libraryPath;
        final Collection<URL> binaryLibraryPath;
        final Collection<URL> unknownSourcePath;
        final Map<ClassPath, byte[]> newCps;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<URL, SourceForBinaryQuery.Result2> newSR;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<URL, URL[]> translatedRoots;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<URL, WeakValue> unknownRoots;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<URL, PathIds> rootPathIds;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<String, Set<URL>> pathIdToRoots;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public Result(long timeStamp, Collection<URL> sourcePath, Collection<URL> libraryPath, Collection<URL> binaryLibraryPath, Collection<URL> unknownSourcePath, Map<ClassPath, byte[]> newCps, Map<URL, SourceForBinaryQuery.Result2> newSR, Map<URL, URL[]> translatedRoots, Map<URL, WeakValue> unknownRoots, Map<URL, PathIds> rootPathIds, Map<String, Set<URL>> pathIdToRoots) {
            assert (sourcePath != null);
            assert (libraryPath != null);
            assert (binaryLibraryPath != null);
            assert (unknownSourcePath != null);
            assert (newCps != null);
            assert (newSR != null);
            assert (translatedRoots != null);
            assert (rootPathIds != null);
            this.timeStamp = timeStamp;
            this.sourcePath = sourcePath;
            this.libraryPath = libraryPath;
            this.binaryLibraryPath = binaryLibraryPath;
            this.unknownSourcePath = unknownSourcePath;
            this.newCps = newCps;
            this.newSR = newSR;
            this.translatedRoots = translatedRoots;
            this.unknownRoots = unknownRoots;
            this.rootPathIds = rootPathIds;
            this.pathIdToRoots = pathIdToRoots;
        }
    }

    private static class Request {
        final long timeStamp;
        final Collection<TaggedClassPath> sourceCps;
        final Collection<TaggedClassPath> libraryCps;
        final Collection<TaggedClassPath> binaryLibraryCps;
        final Map<ClassPath, byte[]> oldCps;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<URL, SourceForBinaryQuery.Result2> oldSR;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        final Map<URL, WeakValue> unknownRoots;
        final PropertyChangeListener propertyListener;
        final ChangeListener changeListener;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public Request(long timeStamp, Collection<TaggedClassPath> sourceCps, Collection<TaggedClassPath> libraryCps, Collection<TaggedClassPath> binaryLibraryCps, Map<ClassPath, byte[]> oldCps, Map<URL, SourceForBinaryQuery.Result2> oldSR, Map<URL, WeakValue> unknownRoots, PropertyChangeListener propertyListener, ChangeListener changeListener) {
            assert (sourceCps != null);
            assert (libraryCps != null);
            assert (binaryLibraryCps != null);
            assert (oldCps != null);
            assert (oldSR != null);
            assert (unknownRoots != null);
            assert (propertyListener != null);
            assert (changeListener != null);
            this.timeStamp = timeStamp;
            this.sourceCps = sourceCps;
            this.libraryCps = libraryCps;
            this.binaryLibraryCps = binaryLibraryCps;
            this.oldCps = oldCps;
            this.oldSR = oldSR;
            this.unknownRoots = unknownRoots;
            this.propertyListener = propertyListener;
            this.changeListener = changeListener;
        }
    }

}

