/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl;

public enum SourceFlags {
    INVALID,
    CHANGE_EXPECTED,
    RESCHEDULE_FINISHED_TASKS;
    

    private SourceFlags() {
    }
}

