/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.java.classpath.ClassPath$PathConversionMode
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.netbeans.editor.AtomicLockEvent
 *  org.netbeans.editor.AtomicLockListener
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex$Transactional
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$Status
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.netbeans.modules.project.indexingbridge.IndexingBridge
 *  org.netbeans.modules.project.indexingbridge.IndexingBridge$Ordering
 *  org.netbeans.modules.sampler.Sampler
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.modules.InstalledFileLocator
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.TopologicalSortException
 *  org.openide.util.Union2
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.editor.AtomicLockEvent;
import org.netbeans.editor.AtomicLockListener;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.impl.EmbeddingProviderFactory;
import org.netbeans.modules.parsing.impl.RunWhenScanFinishedSupport;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceFlags;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.event.EventSupport;
import org.netbeans.modules.parsing.impl.indexing.ArchiveTimeStamps;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.ClusteredIndexables;
import org.netbeans.modules.parsing.impl.indexing.Crawler;
import org.netbeans.modules.parsing.impl.indexing.DeletedIndexable;
import org.netbeans.modules.parsing.impl.indexing.EventKind;
import org.netbeans.modules.parsing.impl.indexing.FileEventLog;
import org.netbeans.modules.parsing.impl.indexing.FileObjectCrawler;
import org.netbeans.modules.parsing.impl.indexing.FileObjectIndexable;
import org.netbeans.modules.parsing.impl.indexing.IndexBinaryWorkPool;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.IndexerCache;
import org.netbeans.modules.parsing.impl.indexing.InjectedTasksSupport;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.PathKind;
import org.netbeans.modules.parsing.impl.indexing.PathRecognizerRegistry;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.PathRegistryEvent;
import org.netbeans.modules.parsing.impl.indexing.PathRegistryListener;
import org.netbeans.modules.parsing.impl.indexing.ProxyIterable;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.SuspendSupport;
import org.netbeans.modules.parsing.impl.indexing.TimeStamps;
import org.netbeans.modules.parsing.impl.indexing.TransientUpdateSupport;
import org.netbeans.modules.parsing.impl.indexing.URLCache;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.impl.indexing.VisibilitySupport;
import org.netbeans.modules.parsing.impl.indexing.errors.TaskCache;
import org.netbeans.modules.parsing.impl.indexing.friendapi.DownloadedIndexPatcher;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexDownloader;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingActivityInterceptor;
import org.netbeans.modules.parsing.impl.indexing.friendapi.IndexingController;
import org.netbeans.modules.parsing.impl.indexing.lucene.LayeredDocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;
import org.netbeans.modules.parsing.spi.indexing.SourceIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.netbeans.modules.project.indexingbridge.IndexingBridge;
import org.netbeans.modules.sampler.Sampler;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.TopologicalSortException;
import org.openide.util.Union2;

public final class RepositoryUpdater
implements PathRegistryListener,
PropertyChangeListener,
DocumentListener,
AtomicLockListener {
    private static final int PROFILE_EXECUTION_DELAY_TRESHOLD = 120000;
    static final String PROP_SAMPLING = RepositoryUpdater.class.getName() + ".indexerSampling";
    private final FileEventLog eventQueue = new FileEventLog();
    private static RepositoryUpdater instance;
    private static final Logger LOGGER;
    private static final Logger TEST_LOGGER;
    private static final Logger PERF_LOGGER;
    private static final Logger SFEC_LOGGER;
    private static final Logger UI_LOGGER;
    private static final RequestProcessor RP;
    private static final RequestProcessor WORKER;
    private static final boolean notInterruptible;
    private static final boolean useRecursiveListeners;
    private static final int FILE_LOCKS_DELAY;
    private static final String PROP_LAST_INDEXED_VERSION;
    private static final String PROP_LAST_DIRTY_VERSION;
    private static final String PROP_MODIFIED_UNDER_WRITE_LOCK;
    private static final String PROP_OWNING_SOURCE_ROOT_URL;
    private static final String PROP_OWNING_SOURCE_ROOT;
    private static final String PROP_OWNING_SOURCE_UNKNOWN_IN;
    private static final String INDEX_DOWNLOAD_FOLDER = "index-download";
    private static final boolean[] FD_NEW_SFB_ROOT;
    static final List<URL> UNKNOWN_ROOT;
    static final List<URL> NONEXISTENT_ROOT;
    static volatile Source unitTestActiveSource;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Map<URL, List<URL>> scannedRoots2Dependencies = Collections.synchronizedMap(new TreeMap(new LexicographicComparator(true)));
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Map<URL, List<URL>> scannedBinaries2InvDependencies = Collections.synchronizedMap(new HashMap());
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Map<URL, List<URL>> scannedRoots2Peers = Collections.synchronizedMap(new TreeMap(new LexicographicComparator(true)));
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Set<URL> scannedUnknown = Collections.synchronizedSet(new HashSet());
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Set<URL> sourcesForBinaryRoots = Collections.synchronizedSet(new HashSet());
    private volatile State state = State.CREATED;
    private volatile Task worker;
    private volatile Reference<Document> activeDocumentRef = null;
    private volatile Reference<JTextComponent> activeComponentRef = null;
    private Lookup.Result<? extends IndexingActivityInterceptor> indexingActivityInterceptors = null;
    private IndexingController controller;
    @SuppressWarnings(value={"DM_STRING_CTOR"})
    private final String lastOwningSourceRootCacheLock = new String("lastOwningSourceRootCacheLock");
    private boolean ignoreIndexerCacheEvents = false;
    final RootsListeners rootsListeners = new RootsListeners();
    private final FileChangeListener sourceRootsListener;
    private final FileChangeListener binaryRootsListener;
    private final ThreadLocal<Boolean> inIndexer;
    private final VisibilitySupport visibilitySupport;
    private final SuspendSupport suspendSupport;
    private final AtomicLong scannedRoots2DependenciesLamport;
    private static final Map<List<StackTraceElement>, Long> lastRecordedStackTraces;
    private static long stackTraceId;
    private static final Comparator<URL> C;
    private static final RequestProcessor SAMPLER_RP;
    private static volatile SamplerInvoker currentSampler;

    public static synchronized RepositoryUpdater getDefault() {
        if (instance == null) {
            instance = new RepositoryUpdater();
        }
        return instance;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void start(boolean force) {
        InitialRootsWork work = null;
        RepositoryUpdater repositoryUpdater = this;
        synchronized (repositoryUpdater) {
            if (this.state == State.CREATED || this.state == State.STOPPED) {
                this.state = State.STARTED;
                this.getWorker().allCancelled = false;
                LOGGER.fine("Initializing...");
                this.indexingActivityInterceptors = Lookup.getDefault().lookupResult(IndexingActivityInterceptor.class);
                PathRegistry.getDefault().addPathRegistryListener(this);
                this.rootsListeners.setListener(this.sourceRootsListener, this.binaryRootsListener);
                EditorRegistry.addPropertyChangeListener((PropertyChangeListener)this);
                IndexerCache.getCifCache().addPropertyChangeListener(this);
                IndexerCache.getEifCache().addPropertyChangeListener(this);
                this.visibilitySupport.start();
                if (force) {
                    work = new InitialRootsWork(this.scannedRoots2Dependencies, this.scannedBinaries2InvDependencies, this.scannedRoots2Peers, this.sourcesForBinaryRoots, false, this.scannedRoots2DependenciesLamport, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.PATH, null));
                }
            }
        }
        if (work != null) {
            this.scheduleWork(work, false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void stop(@NullAllowed Runnable postCleanTask) throws TimeoutException {
        boolean cancel = false;
        RepositoryUpdater repositoryUpdater = this;
        synchronized (repositoryUpdater) {
            if (this.state != State.STOPPED) {
                this.state = State.STOPPED;
                LOGGER.fine("Closing...");
                PathRegistry.getDefault().removePathRegistryListener(this);
                this.rootsListeners.setListener(null, null);
                EditorRegistry.removePropertyChangeListener((PropertyChangeListener)this);
                this.visibilitySupport.stop();
                cancel = true;
            }
        }
        if (cancel) {
            this.getWorker().cancelAll(postCleanTask);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<IndexingState> getIndexingState() {
        boolean pathChanging;
        boolean beforeInitialScanStarted;
        boolean openingProjects;
        RepositoryUpdater repositoryUpdater = this;
        synchronized (repositoryUpdater) {
            beforeInitialScanStarted = this.state == State.CREATED || this.state == State.STARTED;
        }
        try {
            Future f = OpenProjects.getDefault().openProjects();
            openingProjects = !f.isDone() || ((Project[])f.get()).length > 0;
        }
        catch (Exception ie) {
            openingProjects = true;
        }
        EnumSet<IndexingState> result = EnumSet.noneOf(IndexingState.class);
        boolean starting = beforeInitialScanStarted && openingProjects;
        boolean working = this.getWorker().isWorking();
        boolean bl = pathChanging = !PathRegistry.getDefault().isFinished();
        if (starting) {
            result.add(IndexingState.STARTING);
        }
        if (pathChanging) {
            result.add(IndexingState.PATH_CHANGING);
        }
        if (working) {
            result.add(IndexingState.WORKING);
        }
        LOGGER.log(Level.FINE, "IsScanInProgress: (starting: {0} | working: {1} | path are changing: {2})", new Object[]{starting, working, pathChanging});
        return result;
    }

    public boolean isProtectedModeOwner(Thread thread) {
        return this.getWorker().isProtectedModeOwner(thread);
    }

    public boolean isIndexer() {
        return this.inIndexer.get() == Boolean.TRUE;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runIndexer(Runnable indexer) {
        assert (indexer != null);
        this.inIndexer.set(Boolean.TRUE);
        try {
            indexer.run();
        }
        finally {
            this.inIndexer.remove();
        }
    }

    public boolean waitUntilFinished(long timeout) throws InterruptedException {
        long ts1;
        long ts2 = ts1 = System.currentTimeMillis();
        do {
            boolean timedOut = !this.getWorker().waitUntilFinished(timeout);
            ts2 = System.currentTimeMillis();
            if (!timedOut) continue;
            return false;
        } while (!this.getIndexingState().isEmpty() && (timeout <= 0 || ts2 - ts1 < timeout));
        return timeout <= 0 || ts2 - ts1 < timeout;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public void addIndexingJob(@NonNull URL rootUrl, @NullAllowed Collection<? extends URL> fileUrls, boolean followUpJob, boolean checkEditor, boolean wait, boolean forceRefresh, boolean steady, @NonNull LogContext logCtx) {
        LOGGER.log(Level.FINE, "addIndexingJob: rootUrl={0}, fileUrls={1}, followUpJob={2}, checkEditor={3}, wait={4}", new Object[]{rootUrl, fileUrls, followUpJob, checkEditor, wait});
        FileListWork flw = this.createFileListWork(rootUrl, fileUrls, followUpJob, checkEditor, forceRefresh, steady, logCtx);
        if (flw != null) {
            LOGGER.log(Level.FINE, "Scheduling index refreshing: root={0}, files={1}", new Object[]{rootUrl, fileUrls});
            this.scheduleWork(flw, wait);
        }
    }

    void addDeleteJob(@NonNull URL root, @NonNull Set<String> relativePaths, @NonNull LogContext logCtx) {
        DeleteWork wrk = new DeleteWork(root, relativePaths, this.suspendSupport.getSuspendStatus(), logCtx);
        this.scheduleWork(wrk, false);
    }

    void addBinaryJob(@NonNull URL root, @NonNull LogContext logCtx) {
        BinaryWork wrk = new BinaryWork(root, this.suspendSupport.getSuspendStatus(), logCtx);
        this.scheduleWork(wrk, false);
    }

    public void enforcedFileListUpdate(@NonNull URL rootUrl, @NonNull Collection<? extends URL> fileUrls) throws IOException {
        FileListWork flw = this.createFileListWork(rootUrl, fileUrls, false, true, true, false, null);
        if (flw != null) {
            class T
            implements Callable<Void>,
            Runnable {
                final /* synthetic */ FileListWork val$flw;

                T() {
                    this.val$flw = var2_2;
                }

                @Override
                public void run() {
                    this.val$flw.doTheWork();
                }

                @Override
                public Void call() throws Exception {
                    this$0.suspendSupport.runWithNoSuspend(this);
                    return null;
                }
            }
            LOGGER.log(Level.FINE, "Transient File List Update {0}", flw);
            T t = new T(this, flw);
            try {
                Utilities.runPriorityIO(t);
            }
            catch (Exception ex) {
                throw new IOException(ex);
            }
        }
    }

    @CheckForNull
    private FileListWork createFileListWork(@NonNull URL rootUrl, @NullAllowed Collection<? extends URL> fileUrls, boolean followUpJob, boolean checkEditor, boolean forceRefresh, boolean steady, @NullAllowed LogContext logCtx) {
        assert (rootUrl != null);
        assert (PathRegistry.noHostPart(rootUrl));
        FileObject root = URLMapper.findFileObject((URL)rootUrl);
        if (root == null) {
            LOGGER.log(Level.INFO, "{0} can''t be translated to FileObject", rootUrl);
            return null;
        }
        FileListWork flw = null;
        if (fileUrls != null && fileUrls.size() > 0) {
            HashSet<FileObject> files = new HashSet<FileObject>();
            for (URL fileUrl : fileUrls) {
                FileObject file = URLMapper.findFileObject((URL)fileUrl);
                if (file == null) continue;
                if (FileUtil.isParentOf((FileObject)root, (FileObject)file)) {
                    files.add(file);
                    continue;
                }
                if (!LOGGER.isLoggable(Level.WARNING)) continue;
                LOGGER.log(Level.WARNING, "{0} does not lie under {1}, not indexing it", new Object[]{file, root});
            }
            if (files.size() > 0) {
                flw = new FileListWork(this.scannedRoots2Dependencies, rootUrl, files, followUpJob, checkEditor, forceRefresh, this.sourcesForBinaryRoots.contains(rootUrl), steady, this.suspendSupport.getSuspendStatus(), logCtx);
            }
        } else {
            flw = new FileListWork(this.scannedRoots2Dependencies, rootUrl, followUpJob, checkEditor, forceRefresh, this.sourcesForBinaryRoots.contains(rootUrl), this.suspendSupport.getSuspendStatus(), logCtx);
        }
        return flw;
    }

    public void addIndexingJob(@NonNull String indexerName, @NonNull LogContext logCtx) {
        Work w2;
        Collection<IndexerCache.IndexerInfo<CustomIndexerFactory>> cifInfos;
        Work w2;
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("addIndexingJob: indexerName=" + indexerName);
        }
        if ((cifInfos = IndexerCache.getCifCache().getIndexersByName(indexerName)) == null) {
            Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos = IndexerCache.getEifCache().getIndexersByName(indexerName);
            if (eifInfos == null) {
                throw new InvalidParameterException("No CustomIndexerFactory or EmbeddingIndexerFactory with name: '" + indexerName + "'");
            }
            w2 = new RefreshEifIndices(eifInfos, this.scannedRoots2Dependencies, this.sourcesForBinaryRoots, this.suspendSupport.getSuspendStatus(), logCtx);
        } else {
            w2 = new RefreshCifIndices(cifInfos, this.scannedRoots2Dependencies, this.sourcesForBinaryRoots, this.suspendSupport.getSuspendStatus(), logCtx);
        }
        this.scheduleWork(w2, false);
    }

    public /* varargs */ void refreshAll(boolean fullRescan, boolean wait, boolean logStatistics, @NullAllowed LogContext logCtx, @NullAllowed Object ... filesOrFileObjects) {
        boolean ae = false;
        if (!$assertionsDisabled) {
            ae = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (ae) {
            for (Object fileOrFileObject : filesOrFileObjects) {
                if (!(fileOrFileObject instanceof File)) continue;
                File file = (File)fileOrFileObject;
                assert (file.equals(FileUtil.normalizeFile((File)file)));
            }
        }
        FSRefreshInterceptor fsRefreshInterceptor = null;
        for (IndexingActivityInterceptor iai : this.indexingActivityInterceptors.allInstances()) {
            if (!(iai instanceof FSRefreshInterceptor)) continue;
            fsRefreshInterceptor = (FSRefreshInterceptor)iai;
            break;
        }
        this.scheduleWork(new RefreshWork(this.scannedRoots2Dependencies, this.scannedBinaries2InvDependencies, this.scannedRoots2Peers, this.sourcesForBinaryRoots, fullRescan, logStatistics, filesOrFileObjects == null ? Collections.emptySet() : Arrays.asList(filesOrFileObjects), fsRefreshInterceptor, this.suspendSupport.getSuspendStatus(), logCtx), wait);
    }

    public void suspend() {
        if (notInterruptible) {
            return;
        }
        this.suspendSupport.suspend();
    }

    public void resume() {
        if (notInterruptible) {
            return;
        }
        this.suspendSupport.resume();
    }

    public synchronized IndexingController getController() {
        if (this.controller == null) {
            this.controller = new Controller();
        }
        return this.controller;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part. Already verified by PathRegistry")
    @Override
    public void pathsChanged(PathRegistryEvent event) {
        assert (event != null);
        if (LOGGER.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Paths changed:\n");
            for (PathRegistryEvent.Change c : event.getChanges()) {
                sb.append(" event=").append((Object)c.getEventKind());
                sb.append(" pathKind=").append((Object)c.getPathKind());
                sb.append(" pathType=").append(c.getPathId());
                sb.append(" affected paths:\n");
                Set<? extends ClassPath> paths = c.getAffectedPaths();
                if (paths != null) {
                    for (ClassPath cp : paths) {
                        sb.append("  \"");
                        sb.append(cp.toString(ClassPath.PathConversionMode.PRINT));
                        sb.append("\"\n");
                    }
                }
                sb.append("--\n");
            }
            sb.append("====\n");
            LOGGER.fine(sb.toString());
        }
        boolean existingPathsChanged = false;
        boolean containsRelevantChanges = false;
        LogContext logContext = event.getLogContext();
        ArrayList<URL> includesChanged = new ArrayList<URL>();
        for (PathRegistryEvent.Change c : event.getChanges()) {
            if (c.getEventKind() == EventKind.INCLUDES_CHANGED) {
                for (ClassPath cp : c.getAffectedPaths()) {
                    for (ClassPath.Entry e : cp.entries()) {
                        includesChanged.add(e.getURL());
                    }
                }
                continue;
            }
            containsRelevantChanges = true;
            if (c.getEventKind() != EventKind.PATHS_CHANGED) continue;
            existingPathsChanged = true;
        }
        if (containsRelevantChanges) {
            this.scheduleWork(new RootsWork(this.scannedRoots2Dependencies, this.scannedBinaries2InvDependencies, this.scannedRoots2Peers, this.sourcesForBinaryRoots, !existingPathsChanged, false, this.scannedRoots2DependenciesLamport, this.suspendSupport.getSuspendStatus(), logContext), false);
        }
        for (URL rootUrl : includesChanged) {
            this.scheduleWork(new FileListWork(this.scannedRoots2Dependencies, rootUrl, false, false, false, this.sourcesForBinaryRoots.contains(rootUrl), this.suspendSupport.getSuspendStatus(), logContext), false);
        }
    }

    private void fileFolderCreatedImpl(FileEvent fe, Boolean source) {
        FileObject fo = fe.getFile();
        if (this.isCacheFile(fo)) {
            return;
        }
        if (!this.authorize(fe)) {
            return;
        }
        boolean processed = false;
        Pair<URL, FileObject> root = null;
        if (fo != null && fo.isValid()) {
            if ((source == null || source.booleanValue()) && (root = this.getOwningSourceRoot((Object)fo)) != null && this.visibilitySupport.isVisible(fo, (FileObject)root.second())) {
                ClassPath.Entry entry;
                if (root.second() == null) {
                    LOGGER.log(Level.INFO, "Ignoring event from non existing FileObject {0}", root.first());
                    return;
                }
                boolean sourcForBinaryRoot = this.sourcesForBinaryRoots.contains(root.first());
                ClassPath.Entry entry2 = entry = sourcForBinaryRoot ? null : RepositoryUpdater.getClassPathEntry((FileObject)root.second());
                if (entry == null || entry.includes(fo)) {
                    Work wrk2;
                    Work wrk2;
                    if (fo.equals(root.second())) {
                        if (this.scannedRoots2Dependencies.get(root.first()) == NONEXISTENT_ROOT) {
                            wrk2 = new RootsWork(this.scannedRoots2Dependencies, this.scannedBinaries2InvDependencies, this.scannedRoots2Peers, this.sourcesForBinaryRoots, false, true, this.scannedRoots2DependenciesLamport, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).addRoots(Collections.singleton(root.first())));
                        } else {
                            FileObject[] children = fo.getChildren();
                            List<FileObject> c = Arrays.asList(children);
                            wrk2 = children.length > 0 ? new FileListWork(this.scannedRoots2Dependencies, (URL)root.first(), c, false, false, true, sourcForBinaryRoot, true, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFileObjects(c)) : null;
                        }
                    } else {
                        Set<FileObject> c = Collections.singleton(fo);
                        wrk2 = new FileListWork(this.scannedRoots2Dependencies, (URL)root.first(), c, false, false, true, sourcForBinaryRoot, true, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFileObjects(c));
                    }
                    if (wrk2 != null) {
                        this.eventQueue.record(FileEventLog.FileOp.CREATE, (URL)root.first(), FileUtil.getRelativePath((FileObject)((FileObject)root.second()), (FileObject)fo), fe, wrk2);
                    }
                    processed = true;
                }
            }
            if (!(processed || source != null && source.booleanValue() || (root = this.getOwningBinaryRoot(fo)) == null || !this.visibilitySupport.isVisible(fo, (FileObject)root.second()))) {
                BinaryWork wrk = new BinaryWork((URL)root.first(), this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFileObjects(Collections.singleton(fo)));
                this.eventQueue.record(FileEventLog.FileOp.CREATE, (URL)root.first(), null, fe, wrk);
                processed = true;
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.fine("Folder created (" + (processed ? "processed" : "ignored") + "): " + FileUtil.getFileDisplayName((FileObject)fo) + " Owner: " + root);
        }
    }

    private void fileChangedImpl(FileEvent fe, Boolean source) {
        FileObject fo = fe.getFile();
        if (this.isCacheFile(fo)) {
            return;
        }
        if (!this.authorize(fe)) {
            return;
        }
        boolean processed = false;
        Pair<URL, FileObject> root = null;
        if (fo != null && fo.isValid()) {
            if ((source == null || source.booleanValue()) && (root = this.getOwningSourceRoot((Object)fo)) != null && this.visibilitySupport.isVisible(fo, (FileObject)root.second())) {
                ClassPath.Entry entry;
                if (root.second() == null) {
                    LOGGER.log(Level.INFO, "Ignoring event from non existing FileObject {0}", root.first());
                    return;
                }
                boolean sourceForBinaryRoot = this.sourcesForBinaryRoots.contains(root.first());
                ClassPath.Entry entry2 = entry = sourceForBinaryRoot ? null : RepositoryUpdater.getClassPathEntry((FileObject)root.second());
                if (entry == null || entry.includes(fo)) {
                    FileListWork wrk = new FileListWork(this.scannedRoots2Dependencies, (URL)root.first(), Collections.singleton(fo), false, false, true, sourceForBinaryRoot, true, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFiles(Collections.singleton(fo.toURL())));
                    this.eventQueue.record(FileEventLog.FileOp.CREATE, (URL)root.first(), FileUtil.getRelativePath((FileObject)((FileObject)root.second()), (FileObject)fo), fe, wrk);
                    processed = true;
                }
            }
            if (!(processed || source != null && source.booleanValue() || (root = this.getOwningBinaryRoot(fo)) == null || !this.visibilitySupport.isVisible(fo, (FileObject)root.second()))) {
                BinaryWork wrk = new BinaryWork((URL)root.first(), this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFiles(Collections.singleton(fo.toURL())));
                this.eventQueue.record(FileEventLog.FileOp.CREATE, (URL)root.first(), null, fe, wrk);
                processed = true;
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            Object[] arrobject = new Object[3];
            arrobject[0] = processed ? "processed" : "ignored";
            arrobject[1] = FileUtil.getFileDisplayName((FileObject)fo);
            arrobject[2] = root;
            LOGGER.log(Level.FINE, "File modified ({0}): {1} Owner: {2}", arrobject);
        }
    }

    private void fileDeletedImpl(FileEvent fe, Boolean source) {
        FileObject fo = fe.getFile();
        if (this.isCacheFile(fo)) {
            return;
        }
        if (!this.authorize(fe)) {
            return;
        }
        boolean processed = false;
        Pair<URL, FileObject> root = null;
        if (fo != null) {
            if ((source == null || source.booleanValue()) && (root = this.getOwningSourceRoot((Object)fo)) != null && fo.isData() && this.visibilitySupport.isVisible(fo, (FileObject)root.second())) {
                String relativePath = null;
                try {
                    relativePath = root.second() != null ? FileUtil.getRelativePath((FileObject)((FileObject)root.second()), (FileObject)fo) : ((URL)root.first()).toURI().relativize(fo.getURL().toURI()).getPath();
                    assert (relativePath != null);
                    DeleteWork wrk = new DeleteWork((URL)root.first(), Collections.singleton(relativePath), this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFiles(Collections.singleton(fo.toURL())));
                    this.eventQueue.record(FileEventLog.FileOp.DELETE, (URL)root.first(), relativePath, fe, wrk);
                    processed = true;
                }
                catch (FileStateInvalidException fse) {
                    Exceptions.printStackTrace((Throwable)fse);
                }
                catch (URISyntaxException use) {
                    Exceptions.printStackTrace((Throwable)use);
                }
            }
            if (!(processed || source != null && source.booleanValue() || (root = this.getOwningBinaryRoot(fo)) == null || !this.visibilitySupport.isVisible(fo, (FileObject)root.second()))) {
                BinaryWork wrk = new BinaryWork((URL)root.first(), this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFiles(Collections.singleton(fo.toURL())));
                this.eventQueue.record(FileEventLog.FileOp.DELETE, (URL)root.first(), null, fe, wrk);
                processed = true;
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            Object[] arrobject = new Object[3];
            arrobject[0] = processed ? "processed" : "ignored";
            arrobject[1] = FileUtil.getFileDisplayName((FileObject)fo);
            arrobject[2] = root;
            LOGGER.log(Level.FINE, "File deleted ({0}): {1} Owner: {2}", arrobject);
        }
    }

    private void fileRenamedImpl(FileRenameEvent fe, Boolean source) {
        FileObject fo = fe.getFile();
        if (this.isCacheFile(fo)) {
            return;
        }
        if (!this.authorize((FileEvent)fe)) {
            return;
        }
        FileObject newFile = fe.getFile();
        String oldNameExt = fe.getExt().length() == 0 ? fe.getName() : fe.getName() + "." + fe.getExt();
        Pair<URL, FileObject> root = null;
        boolean processed = false;
        if (newFile != null && newFile.isValid()) {
            if ((source == null || source.booleanValue()) && (root = this.getOwningSourceRoot((Object)newFile)) != null) {
                String oldFilePath;
                if (root.second() == null) {
                    LOGGER.log(Level.INFO, "Ignoring event from non existing FileObject {0}", root.first());
                    return;
                }
                FileObject rootFo = (FileObject)root.second();
                if (rootFo.equals((Object)newFile)) {
                    return;
                }
                String ownerPath = FileUtil.getRelativePath((FileObject)rootFo, (FileObject)newFile.getParent());
                String string = oldFilePath = ownerPath.length() == 0 ? oldNameExt : ownerPath + "/" + oldNameExt;
                if (newFile.isData()) {
                    DeleteWork work = new DeleteWork((URL)root.first(), Collections.singleton(oldFilePath), this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFilePaths(Collections.singleton(oldFilePath)));
                    this.eventQueue.record(FileEventLog.FileOp.DELETE, (URL)root.first(), oldFilePath, (FileEvent)fe, work);
                } else {
                    HashSet<String> oldFilePaths = new HashSet<String>();
                    RepositoryUpdater.collectFilePaths(newFile, oldFilePath, oldFilePaths);
                    DeleteWork work = new DeleteWork((URL)root.first(), oldFilePaths, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFilePaths(oldFilePaths));
                    for (String path : oldFilePaths) {
                        this.eventQueue.record(FileEventLog.FileOp.DELETE, (URL)root.first(), path, (FileEvent)fe, work);
                    }
                }
                if (this.visibilitySupport.isVisible(newFile, (FileObject)root.second())) {
                    ClassPath.Entry entry;
                    boolean sourceForBinaryRoot = this.sourcesForBinaryRoots.contains(root.first());
                    ClassPath.Entry entry2 = entry = sourceForBinaryRoot ? null : RepositoryUpdater.getClassPathEntry(rootFo);
                    if (entry == null || entry.includes(newFile)) {
                        FileListWork flw = new FileListWork(this.scannedRoots2Dependencies, (URL)root.first(), Collections.singleton(newFile), false, false, true, sourceForBinaryRoot, true, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFileObjects(Collections.singleton(newFile)));
                        this.eventQueue.record(FileEventLog.FileOp.CREATE, (URL)root.first(), FileUtil.getRelativePath((FileObject)rootFo, (FileObject)newFile), (FileEvent)fe, flw);
                    }
                }
                processed = true;
            }
            if (!(processed || source != null && source.booleanValue() || (root = this.getOwningBinaryRoot(newFile)) == null)) {
                File parentFile = FileUtil.toFile((FileObject)newFile.getParent());
                if (parentFile != null) {
                    try {
                        URL oldBinaryRoot = org.openide.util.Utilities.toURI((File)new File(parentFile, oldNameExt)).toURL();
                        this.eventQueue.record(FileEventLog.FileOp.DELETE, oldBinaryRoot, null, (FileEvent)fe, new BinaryWork(oldBinaryRoot, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).addRoots(Collections.singleton(oldBinaryRoot))));
                    }
                    catch (MalformedURLException mue) {
                        LOGGER.log(Level.WARNING, null, mue);
                    }
                }
                this.eventQueue.record(FileEventLog.FileOp.CREATE, (URL)root.first(), null, (FileEvent)fe, new BinaryWork((URL)root.first(), this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addRoots(Collections.singleton(root.first()))));
                processed = true;
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            Object[] arrobject = new Object[4];
            arrobject[0] = processed ? "processed" : "ignored";
            arrobject[1] = FileUtil.getFileDisplayName((FileObject)newFile);
            arrobject[2] = root;
            arrobject[3] = oldNameExt;
            LOGGER.log(Level.FINE, "File renamed ({0}): {1} Owner: {2} Original Name: {3}", arrobject);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JTextComponent jtc;
        if (evt.getPropertyName() != null && evt.getPropertyName().equals(CustomIndexerFactory.class.getName())) {
            if (!this.ignoreIndexerCacheEvents) {
                Set changedIndexers = (Set)evt.getNewValue();
                this.scheduleWork(new RefreshCifIndices(changedIndexers, this.scannedRoots2Dependencies, this.sourcesForBinaryRoots, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.INDEXER, null)), false);
            }
            return;
        }
        if (evt.getPropertyName() != null && evt.getPropertyName().equals(EmbeddingIndexerFactory.class.getName())) {
            if (!this.ignoreIndexerCacheEvents) {
                Set changedIndexers = (Set)evt.getNewValue();
                this.scheduleWork(new RefreshEifIndices(changedIndexers, this.scannedRoots2Dependencies, this.sourcesForBinaryRoots, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.INDEXER, null)), false);
            }
            return;
        }
        assert (SwingUtilities.isEventDispatchThread());
        List<JTextComponent> components = Collections.emptyList();
        if (evt.getPropertyName() == null) {
            components = EditorRegistry.componentList();
        } else if (evt.getPropertyName().equals("focusLost") || evt.getPropertyName().equals("lastFocusedRemoved")) {
            Object newValue;
            if (evt.getOldValue() instanceof JTextComponent && (!((newValue = evt.getNewValue()) instanceof JTextComponent) || ((JTextComponent)newValue).getClientProperty("AsTextField") == null)) {
                JTextComponent jtc2 = (JTextComponent)evt.getOldValue();
                this.handleActiveDocumentChange(jtc2.getDocument(), null);
            }
        } else if (evt.getPropertyName().equals("componentRemoved")) {
            if (evt.getOldValue() instanceof JTextComponent) {
                jtc = (JTextComponent)evt.getOldValue();
                components = Collections.singletonList(jtc);
            }
        } else if (evt.getPropertyName().equals("focusGained")) {
            if (evt.getNewValue() instanceof JTextComponent && (jtc = (JTextComponent)evt.getNewValue()).getClientProperty("AsTextField") == null) {
                JTextComponent activeComponent;
                this.handleActiveDocumentChange(null, jtc.getDocument());
                JTextComponent jTextComponent = activeComponent = this.activeComponentRef == null ? null : this.activeComponentRef.get();
                if (activeComponent != jtc) {
                    if (activeComponent != null) {
                        components = Collections.singletonList(activeComponent);
                    }
                    this.activeComponentRef = new WeakReference<JTextComponent>(jtc);
                }
            }
        } else if (evt.getPropertyName().equals("focusedDocument")) {
            jtc = EditorRegistry.focusedComponent();
            if (jtc == null) {
                jtc = EditorRegistry.lastFocusedComponent();
            }
            if (jtc != null) {
                components = Collections.singletonList(jtc);
            }
            this.handleActiveDocumentChange((Document)evt.getOldValue(), (Document)evt.getNewValue());
        }
        HashMap<Object, FileListWork> jobs = new HashMap<Object, FileListWork>();
        if (components.size() > 0) {
            for (JTextComponent jtc3 : components) {
                FileListWork job;
                Document doc = jtc3.getDocument();
                Pair<URL, FileObject> root = this.getOwningSourceRoot(doc);
                if (root == null) continue;
                if (root.second() == null) {
                    FileObject file = Util.getFileObject(doc);
                    assert (file == null || !file.isValid());
                    return;
                }
                long version = DocumentUtilities.getDocumentVersion((Document)doc);
                Long lastIndexedVersion = (Long)doc.getProperty(PROP_LAST_INDEXED_VERSION);
                Long lastDirtyVersion = (Long)doc.getProperty(PROP_LAST_DIRTY_VERSION);
                boolean reindex = false;
                boolean openedInEditor = EditorRegistry.componentList().contains(jtc3);
                reindex = openedInEditor ? (lastIndexedVersion == null ? lastDirtyVersion != null : lastIndexedVersion < version) : lastDirtyVersion != null;
                FileObject docFile = Util.getFileObject(doc);
                LOGGER.log(Level.FINE, "{0}: version={1}, lastIndexerVersion={2}, lastDirtyVersion={3}, openedInEditor={4} => reindex={5}", new Object[]{docFile.getPath(), version, lastIndexedVersion, lastDirtyVersion, openedInEditor, reindex});
                if (!reindex) continue;
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.fine("Document modified (reindexing): " + FileUtil.getFileDisplayName((FileObject)docFile) + " Owner: " + root.first());
                }
                if ((job = (FileListWork)jobs.get(root.first())) == null) {
                    Set<FileObject> c = Collections.singleton(docFile);
                    job = new FileListWork(this.scannedRoots2Dependencies, (URL)root.first(), c, false, openedInEditor, true, this.sourcesForBinaryRoots.contains(root.first()), true, this.suspendSupport.getSuspendStatus(), LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFileObjects(c));
                    jobs.put(root.first(), job);
                    continue;
                }
                job.addFile(docFile);
            }
        }
        for (FileListWork job : jobs.values()) {
            this.scheduleWork(job, false);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.removeUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        Document d = e.getDocument();
        if (d instanceof BaseDocument) {
            d.putProperty(PROP_MODIFIED_UNDER_WRITE_LOCK, true);
        } else {
            this.handleDocumentModification(d);
        }
    }

    public void atomicLock(AtomicLockEvent e) {
        Document d = (Document)e.getSource();
        d.putProperty(PROP_MODIFIED_UNDER_WRITE_LOCK, null);
    }

    public void atomicUnlock(AtomicLockEvent e) {
        Document d = (Document)e.getSource();
        Boolean modified = (Boolean)d.getProperty(PROP_MODIFIED_UNDER_WRITE_LOCK);
        d.putProperty(PROP_MODIFIED_UNDER_WRITE_LOCK, null);
        if (modified != null && modified.booleanValue()) {
            this.handleDocumentModification(d);
        }
    }

    private RepositoryUpdater() {
        this.sourceRootsListener = new FCL(useRecursiveListeners ? Boolean.TRUE : null);
        this.binaryRootsListener = new FCL(Boolean.FALSE);
        this.inIndexer = new ThreadLocal();
        this.visibilitySupport = VisibilitySupport.create(this, RP);
        this.suspendSupport = new SuspendSupport(WORKER);
        this.scannedRoots2DependenciesLamport = new AtomicLong();
        LOGGER.log(Level.FINE, "netbeans.indexing.notInterruptible={0}", notInterruptible);
        LOGGER.log(Level.FINE, "netbeans.indexing.recursiveListeners={0}", useRecursiveListeners);
        LOGGER.log(Level.FINE, "FILE_LOCKS_DELAY={0}", FILE_LOCKS_DELAY);
    }

    private void handleActiveDocumentChange(Document deactivated, Document activated) {
        Document activeDocument;
        Document document = activeDocument = this.activeDocumentRef == null ? null : this.activeDocumentRef.get();
        if (deactivated != null && deactivated == activeDocument) {
            if (activeDocument instanceof BaseDocument) {
                ((BaseDocument)activeDocument).removeAtomicLockListener((AtomicLockListener)this);
            }
            activeDocument.removeDocumentListener(this);
            this.activeDocumentRef = null;
            LOGGER.log(Level.FINE, "Unregistering active document listener: activeDocument={0}", activeDocument);
        }
        if (activated != null && activated != activeDocument) {
            if (activeDocument != null) {
                if (activeDocument instanceof BaseDocument) {
                    ((BaseDocument)activeDocument).removeAtomicLockListener((AtomicLockListener)this);
                }
                activeDocument.removeDocumentListener(this);
                LOGGER.log(Level.FINE, "Unregistering active document listener: activeDocument={0}", activeDocument);
            }
            activeDocument = activated;
            this.activeDocumentRef = new WeakReference<Document>(activeDocument);
            if (activeDocument instanceof BaseDocument) {
                ((BaseDocument)activeDocument).addAtomicLockListener((AtomicLockListener)this);
            }
            activeDocument.addDocumentListener(this);
            LOGGER.log(Level.FINE, "Registering active document listener: activeDocument={0}", activeDocument);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void handleDocumentModification(Document document) {
        Reference<Document> ref = this.activeDocumentRef;
        Document activeDocument = ref == null ? null : ref.get();
        final Pair<URL, FileObject> root = this.getOwningSourceRoot(document);
        if (root != null) {
            if (root.second() == null) {
                LOGGER.log(Level.INFO, "Ignoring event from non existing FileObject {0}", root.first());
                return;
            }
            if (activeDocument == document) {
                long version = DocumentUtilities.getDocumentVersion((Document)activeDocument);
                Long lastDirtyVersion = (Long)activeDocument.getProperty(PROP_LAST_DIRTY_VERSION);
                boolean markDirty = false;
                if (lastDirtyVersion == null || lastDirtyVersion < version) {
                    markDirty = true;
                }
                activeDocument.putProperty(PROP_LAST_DIRTY_VERSION, version);
                if (markDirty) {
                    FileObject docFile = Util.getFileObject(document);
                    if (LOGGER.isLoggable(Level.FINE)) {
                        LOGGER.fine("Active document modified (marking dirty): " + FileUtil.getFileDisplayName((FileObject)docFile) + " Owner: " + root.first());
                    }
                    Set<Indexable> dirty = Collections.singleton(SPIAccessor.getInstance().create(new FileObjectIndexable((FileObject)root.second(), docFile)));
                    String mimeType = DocumentUtilities.getMimeType((Document)document);
                    TransientUpdateSupport.setTransientUpdate(true);
                    try {
                        Callable<FileObject> indexFolderFactory = new Callable<FileObject>(){
                            private FileObject cache;

                            @Override
                            public FileObject call() throws Exception {
                                if (this.cache == null) {
                                    this.cache = CacheFolder.getDataFolder((URL)root.first());
                                }
                                return this.cache;
                            }
                        };
                        Collection<IndexerCache.IndexerInfo<CustomIndexerFactory>> cifInfos = IndexerCache.getCifCache().getIndexersFor(mimeType, true);
                        for (IndexerCache.IndexerInfo<CustomIndexerFactory> info : cifInfos) {
                            try {
                                CustomIndexerFactory factory = info.getIndexerFactory();
                                Context ctx = SPIAccessor.getInstance().createContext(indexFolderFactory, (URL)root.first(), factory.getIndexerName(), factory.getIndexVersion(), null, false, true, false, SuspendSupport.NOP, null, null);
                                factory.filesDirty(dirty, ctx);
                            }
                            catch (IOException ex) {
                                LOGGER.log(Level.WARNING, null, ex);
                            }
                        }
                        Collection<? extends IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos = RepositoryUpdater.collectEmbeddingIndexers(mimeType);
                        for (IndexerCache.IndexerInfo<EmbeddingIndexerFactory> info2 : eifInfos) {
                            try {
                                EmbeddingIndexerFactory factory = info2.getIndexerFactory();
                                Context ctx = SPIAccessor.getInstance().createContext(indexFolderFactory, (URL)root.first(), factory.getIndexerName(), factory.getIndexVersion(), null, false, true, false, SuspendSupport.NOP, null, null);
                                factory.filesDirty(dirty, ctx);
                            }
                            catch (IOException ex) {
                                LOGGER.log(Level.WARNING, null, ex);
                            }
                        }
                    }
                    finally {
                        TransientUpdateSupport.setTransientUpdate(false);
                    }
                }
            } else {
                try {
                    FileObject f = Util.getFileObject(document);
                    Set<URL> c = Collections.singleton(f.getURL());
                    this.addIndexingJob((URL)root.first(), c, false, true, false, true, true, LogContext.create(LogContext.EventType.FILE, null).withRoot((URL)root.first()).addFiles(c));
                }
                catch (FileStateInvalidException ex) {
                    LOGGER.log(Level.WARNING, null, (Throwable)ex);
                }
            }
        }
    }

    @NonNull
    private static Collection<? extends IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> collectEmbeddingIndexers(@NonNull String topMimeType) {
        ArrayDeque result = new ArrayDeque();
        RepositoryUpdater.collectEmbeddingIndexers(topMimeType, result);
        return result;
    }

    private static void collectEmbeddingIndexers(@NonNull String mimeType, @NonNull Collection<? super IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> collector) {
        collector.addAll(IndexerCache.getEifCache().getIndexersFor(mimeType, true));
        for (EmbeddingProviderFactory epf : MimeLookup.getLookup((MimePath)MimePath.get((String)mimeType)).lookupAll(EmbeddingProviderFactory.class)) {
            RepositoryUpdater.collectEmbeddingIndexers(epf.getTargetMimeType(), collector);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void scheduleWork(Iterable<? extends Work> multipleWork) {
        boolean canScheduleMultiple;
        RepositoryUpdater.recordCaller();
        RepositoryUpdater repositoryUpdater = this;
        synchronized (repositoryUpdater) {
            canScheduleMultiple = this.state == State.INITIAL_SCAN_RUNNING || this.state == State.ACTIVE;
        }
        if (canScheduleMultiple) {
            this.getWorker().schedule(multipleWork);
        } else {
            for (Work w : multipleWork) {
                this.scheduleWork(w, false);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void scheduleWork(Work work, boolean wait) {
        RepositoryUpdater.recordCaller();
        boolean scheduleExtraWork = false;
        RepositoryUpdater repositoryUpdater = this;
        synchronized (repositoryUpdater) {
            if (this.state == State.STARTED) {
                this.state = State.INITIAL_SCAN_RUNNING;
                scheduleExtraWork = !(work instanceof InitialRootsWork);
            }
        }
        if (scheduleExtraWork) {
            this.getWorker().schedule(new InitialRootsWork(this.scannedRoots2Dependencies, this.scannedBinaries2InvDependencies, this.scannedRoots2Peers, this.sourcesForBinaryRoots, true, this.scannedRoots2DependenciesLamport, this.suspendSupport.getSuspendStatus(), work == null ? LogContext.create(LogContext.EventType.PATH, null) : work.getLogContext()), false);
            if (work instanceof RootsWork) {
                return;
            }
        }
        if (work != null) {
            this.getWorker().schedule(work, wait);
        }
    }

    public void runAsWork(final @NonNull Runnable r) {
        assert (r != null);
        Work work = new Work(false, false, false, true, SuspendSupport.NOP, null){

            @Override
            protected boolean getDone() {
                r.run();
                return true;
            }
        };
        this.getWorker().schedule(work, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Task getWorker() {
        Task t = this.worker;
        if (t == null) {
            RepositoryUpdater repositoryUpdater = this;
            synchronized (repositoryUpdater) {
                if (this.worker == null) {
                    this.worker = new Task();
                }
                t = this.worker;
            }
        }
        return t;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Pair<URL, FileObject> getOwningSourceRoot(Object fileOrDoc) {
        ArrayList<URL> clone;
        FileObject file = null;
        Document doc = null;
        long current = this.scannedRoots2DependenciesLamport.get();
        String string = this.lastOwningSourceRootCacheLock;
        synchronized (string) {
            if (fileOrDoc instanceof Document) {
                doc = (Document)fileOrDoc;
                file = Util.getFileObject(doc);
                if (file == null) {
                    return null;
                }
                URL cachedSourceRootUrl = (URL)doc.getProperty(PROP_OWNING_SOURCE_ROOT_URL);
                FileObject cachedSourceRoot = (FileObject)doc.getProperty(PROP_OWNING_SOURCE_ROOT);
                if (cachedSourceRootUrl != null && cachedSourceRoot != null && cachedSourceRoot.isValid() && FileUtil.isParentOf((FileObject)cachedSourceRoot, (FileObject)file)) {
                    return Pair.of((Object)cachedSourceRootUrl, (Object)cachedSourceRoot);
                }
                Long unknownIn = (Long)doc.getProperty(PROP_OWNING_SOURCE_UNKNOWN_IN);
                if (unknownIn != null && unknownIn == current) {
                    return null;
                }
            } else if (fileOrDoc instanceof FileObject) {
                file = (FileObject)fileOrDoc;
            } else {
                return null;
            }
            clone = new ArrayList<URL>(this.scannedRoots2Dependencies.keySet());
        }
        assert (file != null);
        URL owningSourceRootUrl = null;
        FileObject owningSourceRoot = null;
        for (URL root : clone) {
            try {
                FileObject rootFo = URLCache.getInstance().findFileObject(root, false);
                if (rootFo != null) {
                    if (!rootFo.equals((Object)file) && !FileUtil.isParentOf((FileObject)rootFo, (FileObject)file)) continue;
                    owningSourceRootUrl = root;
                    owningSourceRoot = rootFo;
                    break;
                }
                if (!file.getURL().toExternalForm().startsWith(root.toExternalForm())) continue;
                owningSourceRootUrl = root;
                owningSourceRoot = rootFo;
                break;
            }
            catch (FileStateInvalidException fsi) {
                Exceptions.printStackTrace((Throwable)fsi);
                continue;
            }
        }
        String i$ = this.lastOwningSourceRootCacheLock;
        synchronized (i$) {
            if (owningSourceRootUrl != null) {
                if (doc != null && file.isValid()) {
                    assert (owningSourceRoot != null);
                    doc.putProperty(PROP_OWNING_SOURCE_ROOT_URL, owningSourceRootUrl);
                    doc.putProperty(PROP_OWNING_SOURCE_ROOT, (Object)owningSourceRoot);
                }
                return Pair.of((Object)owningSourceRootUrl, (Object)owningSourceRoot);
            }
            if (doc != null) {
                doc.putProperty(PROP_OWNING_SOURCE_UNKNOWN_IN, current);
            }
            return null;
        }
    }

    Pair<URL, FileObject> getOwningBinaryRoot(FileObject fo) {
        String foPath;
        if (fo == null) {
            return null;
        }
        try {
            foPath = fo.getURL().getPath();
        }
        catch (FileStateInvalidException fsie) {
            LOGGER.log(Level.WARNING, null, (Throwable)fsie);
            return null;
        }
        ArrayList<URL> clone = new ArrayList<URL>(this.scannedBinaries2InvDependencies.keySet());
        for (URL root : clone) {
            String filePath;
            URL fileURL = FileUtil.getArchiveFile((URL)root);
            boolean archive = true;
            if (fileURL == null) {
                fileURL = root;
                archive = false;
            }
            if ((filePath = fileURL.getPath()).equals(foPath)) {
                return Pair.of((Object)root, (Object)null);
            }
            if (archive || !foPath.startsWith(filePath)) continue;
            return Pair.of((Object)root, (Object)null);
        }
        return null;
    }

    @SuppressWarnings(value={"DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
    private static ClassPath.Entry getClassPathEntry(FileObject root) {
        try {
            Set<String> ids;
            if (root != null && (ids = PathRegistry.getDefault().getSourceIdsFor(root.getURL())) != null) {
                for (String id : ids) {
                    ClassPath cp = ClassPath.getClassPath((FileObject)root, (String)id);
                    if (cp == null) continue;
                    URL rootURL = root.getURL();
                    for (ClassPath.Entry e : cp.entries()) {
                        if (!rootURL.equals(e.getURL())) continue;
                        return e;
                    }
                }
            }
        }
        catch (FileStateInvalidException fsie) {
            // empty catch block
        }
        return null;
    }

    private boolean authorize(FileEvent event) {
        Collection interceptors = this.indexingActivityInterceptors.allInstances();
        for (IndexingActivityInterceptor i : interceptors) {
            if (i.authorizeFileSystemEvent(event) != IndexingActivityInterceptor.Authorization.IGNORE) continue;
            return false;
        }
        return true;
    }

    public boolean isCacheFile(FileObject f) {
        return FileUtil.isParentOf((FileObject)CacheFolder.getCacheFolder(), (FileObject)f);
    }

    private static void collectFilePaths(FileObject folder, String pathPrefix, Set<String> collectedPaths) {
        assert (folder.isFolder());
        if (folder.isValid()) {
            for (FileObject kid : folder.getChildren()) {
                if (!kid.isValid()) continue;
                String kidPath = pathPrefix + "/" + kid.getNameExt();
                if (kid.isData()) {
                    collectedPaths.add(kidPath);
                    continue;
                }
                RepositoryUpdater.collectFilePaths(kid, kidPath, collectedPaths);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS", "DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
    private static boolean findDependencies(URL rootURL, DependenciesContext ctx, Set<String> sourceIds, Set<String> libraryIds, Set<String> binaryLibraryIds, @NonNull CancelRequest cancelRequest, @NonNull SuspendStatus suspendStatus) {
        LinkedList<URL> peers;
        LinkedList<URL> deps;
        List deps2;
        try {
            suspendStatus.parkWhileSuspended();
        }
        catch (InterruptedException ex) {
            // empty catch block
        }
        if (cancelRequest.isRaised()) {
            return false;
        }
        if (ctx.useInitialState && (deps2 = (List)ctx.initialRoots2Deps.get(rootURL)) != null && deps2 != UNKNOWN_ROOT && (deps2 != NONEXISTENT_ROOT || !ctx.refreshNonExistentDeps)) {
            ctx.oldRoots.remove(rootURL);
            return true;
        }
        if (ctx.newRoots2Deps.containsKey(rootURL)) {
            return true;
        }
        FileObject rootFo = URLMapper.findFileObject((URL)rootURL);
        if (rootFo == null) {
            ctx.newRoots2Deps.put(rootURL, NONEXISTENT_ROOT);
            return true;
        }
        deps = new LinkedList<URL>();
        peers = new LinkedList<URL>();
        ctx.cycleDetector.push(rootURL);
        try {
            Set<String> ids2;
            ClassPath cp2;
            URL sourceRoot2;
            if (sourceIds == null || libraryIds == null || binaryLibraryIds == null) {
                ids2 = PathRegistry.getDefault().getSourceIdsFor(rootURL);
                if (null != ids2 && !ids2.isEmpty()) {
                    LOGGER.log(Level.FINER, "Resolving Ids based on sourceIds for {0}: {1}", new Object[]{rootURL, ids2});
                    HashSet<String> lids = new HashSet<String>();
                    HashSet<String> blids = new HashSet<String>();
                    for (String id : ids2) {
                        lids.addAll(PathRecognizerRegistry.getDefault().getLibraryIdsForSourceId(id));
                        blids.addAll(PathRecognizerRegistry.getDefault().getBinaryLibraryIdsForSourceId(id));
                    }
                    if (sourceIds == null) {
                        sourceIds = ids2;
                    }
                    if (libraryIds == null) {
                        libraryIds = lids;
                    }
                    if (binaryLibraryIds == null) {
                        binaryLibraryIds = blids;
                    }
                } else {
                    ids2 = PathRegistry.getDefault().getLibraryIdsFor(rootURL);
                    if (null != ids2 && !ids2.isEmpty()) {
                        LOGGER.log(Level.FINER, "Resolving Ids based on libraryIds for {0}: {1}", new Object[]{rootURL, ids2});
                        HashSet<String> blids = new HashSet<String>();
                        for (String id : ids2) {
                            blids.addAll(PathRecognizerRegistry.getDefault().getBinaryLibraryIdsForLibraryId(id));
                        }
                        if (sourceIds == null) {
                            sourceIds = Collections.emptySet();
                        }
                        if (libraryIds == null) {
                            libraryIds = ids2;
                        }
                        if (binaryLibraryIds == null) {
                            binaryLibraryIds = blids;
                        }
                    } else if (sourceIds == null) {
                        sourceIds = Collections.emptySet();
                    }
                }
            }
            if (cancelRequest.isRaised()) {
                boolean ids2 = false;
                return ids2;
            }
            LOGGER.log(Level.FINER, "SourceIds for {0}: {1}", new Object[]{rootURL, sourceIds});
            LOGGER.log(Level.FINER, "LibraryIds for {0}: {1}", new Object[]{rootURL, libraryIds});
            LOGGER.log(Level.FINER, "BinaryLibraryIds for {0}: {1}", new Object[]{rootURL, binaryLibraryIds});
            for (String id2 : sourceIds) {
                if (cancelRequest.isRaised()) {
                    boolean i$ = false;
                    return i$;
                }
                cp2 = ClassPath.getClassPath((FileObject)rootFo, (String)id2);
                if (cp2 == null) continue;
                for (ClassPath.Entry entry : cp2.entries()) {
                    if (cancelRequest.isRaised()) {
                        boolean bl = false;
                        return bl;
                    }
                    sourceRoot2 = entry.getURL();
                    assert (PathRegistry.noHostPart(sourceRoot2));
                    if (rootURL.equals(sourceRoot2)) continue;
                    peers.add(entry.getURL());
                }
            }
            if (libraryIds != null) {
                for (String id2 : libraryIds) {
                    if (cancelRequest.isRaised()) {
                        boolean cp2 = false;
                        return cp2;
                    }
                    cp2 = ClassPath.getClassPath((FileObject)rootFo, (String)id2);
                    if (cp2 == null) continue;
                    for (ClassPath.Entry entry : cp2.entries()) {
                        if (cancelRequest.isRaised()) {
                            boolean sourceRoot2 = false;
                            return sourceRoot2;
                        }
                        sourceRoot2 = entry.getURL();
                        if (sourceRoot2.equals(rootURL) || ctx.cycleDetector.contains(sourceRoot2)) continue;
                        deps.add(sourceRoot2);
                        assert (PathRegistry.noHostPart(sourceRoot2));
                        if (RepositoryUpdater.findDependencies(sourceRoot2, ctx, sourceIds, libraryIds, binaryLibraryIds, cancelRequest, suspendStatus)) continue;
                        boolean bl = false;
                        return bl;
                    }
                }
            }
            ids2 = binaryLibraryIds == null ? PathRecognizerRegistry.getDefault().getBinaryLibraryIds() : binaryLibraryIds;
            for (String id3 : ids2) {
                if (cancelRequest.isRaised()) {
                    boolean i$ = false;
                    return i$;
                }
                ClassPath cp3 = ClassPath.getClassPath((FileObject)rootFo, (String)id3);
                if (cp3 == null) continue;
                for (ClassPath.Entry entry : cp3.entries()) {
                    LinkedList<URL> binDeps;
                    Set<String> srcIdsForBinRoot;
                    if (cancelRequest.isRaised()) {
                        boolean bl = false;
                        return bl;
                    }
                    URL binaryRoot = entry.getURL();
                    URL[] sourceRoots = PathRegistry.getDefault().sourceForBinaryQuery(binaryRoot, cp3, false, FD_NEW_SFB_ROOT);
                    boolean newSFBRoot = FD_NEW_SFB_ROOT[0];
                    if (sourceRoots != null) {
                        for (URL sourceRoot3 : sourceRoots) {
                            if (cancelRequest.isRaised()) {
                                boolean bl = false;
                                return bl;
                            }
                            if (newSFBRoot) {
                                ctx.newlySFBTranslated.add(sourceRoot3);
                            }
                            if (sourceRoot3.equals(rootURL)) {
                                ctx.sourcesForBinaryRoots.add(rootURL);
                                continue;
                            }
                            if (ctx.cycleDetector.contains(sourceRoot3)) continue;
                            deps.add(sourceRoot3);
                            if (RepositoryUpdater.findDependencies(sourceRoot3, ctx, sourceIds, libraryIds, binaryLibraryIds, cancelRequest, suspendStatus)) continue;
                            boolean bl = false;
                            return bl;
                        }
                        continue;
                    }
                    if (ctx.useInitialState) {
                        if (!ctx.initialBinaries2InvDeps.keySet().contains(binaryRoot)) {
                            ctx.newBinariesToScan.add(binaryRoot);
                            binDeps = (List)ctx.newBinaries2InvDeps.get(binaryRoot);
                            if (binDeps == null) {
                                binDeps = new LinkedList();
                                ctx.newBinaries2InvDeps.put(binaryRoot, binDeps);
                            }
                            binDeps.add(rootURL);
                        }
                    } else {
                        ctx.newBinariesToScan.add(binaryRoot);
                        binDeps = (LinkedList<URL>)ctx.newBinaries2InvDeps.get(binaryRoot);
                        if (binDeps == null) {
                            binDeps = new LinkedList<URL>();
                            ctx.newBinaries2InvDeps.put(binaryRoot, binDeps);
                        }
                        binDeps.add(rootURL);
                    }
                    if ((srcIdsForBinRoot = PathRegistry.getDefault().getSourceIdsFor(binaryRoot)) == null || srcIdsForBinRoot.isEmpty()) {
                        if (binaryRoot.equals(rootURL) || ctx.cycleDetector.contains(binaryRoot)) continue;
                        deps.add(binaryRoot);
                        continue;
                    }
                    LOGGER.log(Level.INFO, "The root {0} is registered for both {1} and {2}", new Object[]{binaryRoot, id3, srcIdsForBinRoot});
                }
            }
        }
        finally {
            ctx.cycleDetector.pop();
        }
        ctx.newRoots2Deps.put(rootURL, deps);
        ctx.newRoots2Peers.put(rootURL, peers);
        return true;
    }

    private static Map<FileObject, Document> getEditorFiles() {
        HashMap<FileObject, Document> f2d = new HashMap<FileObject, Document>();
        for (JTextComponent jtc : EditorRegistry.componentList()) {
            Document d = jtc.getDocument();
            FileObject f = Util.getFileObject(d);
            if (f == null) continue;
            f2d.put(f, d);
        }
        return f2d;
    }

    private static boolean getSystemBoolean(String propertyName, boolean defaultValue) {
        if (defaultValue) {
            String value = System.getProperty(propertyName);
            return value == null || !value.equals("false");
        }
        return Boolean.getBoolean(propertyName);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void recordCaller() {
        if (!LOGGER.isLoggable(Level.FINE)) {
            return;
        }
        Map<List<StackTraceElement>, Long> map = lastRecordedStackTraces;
        synchronized (map) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            ArrayList<StackTraceElement> stackTraceList = new ArrayList<StackTraceElement>(stackTrace.length);
            stackTraceList.addAll(Arrays.asList(stackTrace));
            Long id = lastRecordedStackTraces.get(stackTraceList);
            if (id == null) {
                id = stackTraceId++;
                lastRecordedStackTraces.put(stackTraceList, id);
                StringBuilder sb = new StringBuilder();
                sb.append("RepositoryUpdater caller [id=").append(id).append("] :\n");
                for (StackTraceElement e : stackTraceList) {
                    sb.append(e.toString());
                    sb.append("\n");
                }
                LOGGER.fine(sb.toString());
            } else {
                StackTraceElement caller = Util.findCaller(stackTrace, new Object[0]);
                LOGGER.fine("RepositoryUpdater caller [refid=" + id + "]: " + caller);
            }
        }
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static void printMap(Map<URL, List<URL>> deps, Level level) {
        TreeSet<URL> sortedRoots = new TreeSet<URL>(C);
        sortedRoots.addAll(deps.keySet());
        for (URL url : sortedRoots) {
            LOGGER.log(level, "  {0}:\n", url);
        }
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static StringBuilder printMap(Map<URL, List<URL>> deps, StringBuilder sb) {
        TreeSet<URL> sortedRoots = new TreeSet<URL>(C);
        sortedRoots.addAll(deps.keySet());
        for (URL url : sortedRoots) {
            sb.append("  ").append(url).append(":\n");
        }
        return sb;
    }

    private static void printCollection(Collection<? extends URL> collection, Level level) {
        TreeSet<URL> sortedRoots = new TreeSet<URL>(C);
        sortedRoots.addAll(collection);
        for (URL url : sortedRoots) {
            LOGGER.log(level, "  {0}\n", url);
        }
    }

    private static StringBuilder printCollection(Collection<? extends URL> collection, StringBuilder sb) {
        TreeSet<URL> sortedRoots = new TreeSet<URL>(C);
        sortedRoots.addAll(collection);
        for (URL url : sortedRoots) {
            sb.append("  ").append(url).append("\n");
        }
        return sb;
    }

    private static StringBuilder printMimeTypes(Collection<? extends String> collection, StringBuilder sb) {
        Iterator<? extends String> i = collection.iterator();
        while (i.hasNext()) {
            String mimeType = i.next();
            sb.append("'").append(mimeType).append("'");
            if (!i.hasNext()) continue;
            sb.append(", ");
        }
        return sb;
    }

    State getState() {
        return this.state;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    Set<URL> getScannedBinaries() {
        return this.scannedBinaries2InvDependencies.keySet();
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    Set<URL> getScannedSources() {
        return this.scannedRoots2Dependencies.keySet();
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    Map<URL, List<URL>> getScannedRoots2Dependencies() {
        return this.scannedRoots2Dependencies;
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    Set<URL> getScannedUnknowns() {
        return this.scannedUnknown;
    }

    void ignoreIndexerCacheEvents(boolean ignore) {
        this.ignoreIndexerCacheEvents = ignore;
    }

    static /* synthetic */ boolean access$4200(URL x0, DependenciesContext x1, Set x2, Set x3, Set x4, CancelRequest x5, SuspendStatus x6) {
        return RepositoryUpdater.findDependencies(x0, x1, x2, x3, x4, x5, x6);
    }

    static {
        LOGGER = Logger.getLogger(RepositoryUpdater.class.getName());
        TEST_LOGGER = Logger.getLogger(RepositoryUpdater.class.getName() + ".tests");
        PERF_LOGGER = Logger.getLogger(RepositoryUpdater.class.getName() + ".perf");
        SFEC_LOGGER = Logger.getLogger("org.netbeans.ui.ScanForExternalChanges");
        UI_LOGGER = Logger.getLogger("org.netbeans.ui.indexing");
        RP = new RequestProcessor("RepositoryUpdater.delay");
        WORKER = new RequestProcessor("RepositoryUpdater.worker", 1, false, false);
        notInterruptible = RepositoryUpdater.getSystemBoolean("netbeans.indexing.notInterruptible", false);
        useRecursiveListeners = RepositoryUpdater.getSystemBoolean("netbeans.indexing.recursiveListeners", true);
        FILE_LOCKS_DELAY = org.openide.util.Utilities.isWindows() ? 2000 : 1000;
        PROP_LAST_INDEXED_VERSION = RepositoryUpdater.class.getName() + "-last-indexed-document-version";
        PROP_LAST_DIRTY_VERSION = RepositoryUpdater.class.getName() + "-last-dirty-document-version";
        PROP_MODIFIED_UNDER_WRITE_LOCK = RepositoryUpdater.class.getName() + "-modified-under-write-lock";
        PROP_OWNING_SOURCE_ROOT_URL = RepositoryUpdater.class.getName() + "-owning-source-root-url";
        PROP_OWNING_SOURCE_ROOT = RepositoryUpdater.class.getName() + "-owning-source-root";
        PROP_OWNING_SOURCE_UNKNOWN_IN = RepositoryUpdater.class.getName() + "-owning-source-root-unknown-in";
        FD_NEW_SFB_ROOT = new boolean[1];
        UNKNOWN_ROOT = Collections.unmodifiableList(new LinkedList());
        NONEXISTENT_ROOT = Collections.unmodifiableList(new LinkedList());
        lastRecordedStackTraces = new HashMap<List<StackTraceElement>, Long>();
        stackTraceId = 0;
        C = new Comparator<URL>(){

            @Override
            public int compare(URL o1, URL o2) {
                return o1.toString().compareTo(o2.toString());
            }
        };
        SAMPLER_RP = new RequestProcessor("Repository Updater Sampler");
    }

    private static class SamplerInvoker
    implements Runnable,
    Callable<byte[]> {
        private volatile Sampler sampler;
        private final String indexerName;
        private RequestProcessor.Task scheduled;
        private URL root;
        private int estimate;

        public SamplerInvoker(String indexerName, int delay, URL root) {
            this.estimate = delay;
            this.indexerName = indexerName;
            this.root = root;
        }

        public static void start(LogContext ctx, String indexerName, int delay, URL root) {
            if (delay <= 0) {
                return;
            }
            if (currentSampler != null) {
                return;
            }
            String prop = System.getProperty(RepositoryUpdater.PROP_SAMPLING);
            if (prop == null) {
                return;
            }
            SamplerInvoker inv = new SamplerInvoker(indexerName, delay, root);
            if (Boolean.TRUE.equals(Boolean.valueOf(prop)) || "oneshot".equals(prop)) {
                delay = 0;
            }
            ctx.setProfileSource(inv);
            inv.scheduled = SAMPLER_RP.post((Runnable)inv, delay);
            currentSampler = inv;
            LOGGER.log(Level.FINE, "Sampler scheduled after {0} + {3} for indexer {1} on {2}", new Object[]{new Date(), indexerName, root, delay});
        }

        @Override
        public synchronized void run() {
            Sampler sampler = Sampler.createManualSampler((String)"repoupdater");
            if (sampler != null && currentSampler == this) {
                sampler.start();
                this.sampler = sampler;
                LOGGER.log(Level.FINE, "Updater profiling started at {0} because of {1} runnint on {2} more than {3})", new Object[]{new Date(), this.indexerName, this.root, this.estimate});
            }
        }

        @Override
        public synchronized byte[] call() throws Exception {
            if (this.sampler == null) {
                return null;
            }
            LOGGER.log(Level.FINE, "Dumping snapshot for {0}", this.indexerName);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(out);
            this.sampler.stopAndWriteTo(dos);
            dos.close();
            this.sampler = null;
            return out.toByteArray();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean _stop(boolean release) {
            LOGGER.log(Level.FINE, "Sampler cancelled at {0} for indexer {1} on {2}", new Object[]{new Date(), this.indexerName, this.root});
            if (this.scheduled != null && !this.scheduled.cancel()) {
                LOGGER.log(Level.FINE, "Sampling has already started, release = " + release);
                if (release) {
                    SamplerInvoker samplerInvoker = this;
                    synchronized (samplerInvoker) {
                        if (this.sampler != null) {
                            this.sampler.cancel();
                            this.sampler = null;
                        }
                    }
                    return true;
                }
                return false;
            }
            return true;
        }

        public static void release() {
            if (currentSampler != null) {
                currentSampler._stop(true);
                currentSampler = null;
            }
        }

        public static boolean stop() {
            if (currentSampler == null) {
                return true;
            }
            boolean s = currentSampler._stop(false);
            if (s) {
                currentSampler = null;
            }
            return s;
        }
    }

    private static class WorkCancel
    implements Cancellable {
        private final AtomicReference<Work> work = new AtomicReference();

        private WorkCancel() {
        }

        public void setWork(@NonNull Work theWork) {
            this.work.set(theWork);
        }

        public boolean cancel() {
            LogContext logCtx;
            Work theWork = this.work.get();
            if (theWork != null && (logCtx = theWork.getLogContext()) != null) {
                logCtx.log();
            }
            return false;
        }
    }

    private final class FCL
    extends FileChangeAdapter {
        private final Boolean listeningOnSources;

        public FCL(Boolean listeningOnSources) {
            this.listeningOnSources = listeningOnSources;
        }

        public void fileFolderCreated(FileEvent fe) {
            RepositoryUpdater.this.fileFolderCreatedImpl(fe, this.listeningOnSources);
        }

        public void fileDataCreated(FileEvent fe) {
            RepositoryUpdater.this.fileChangedImpl(fe, this.listeningOnSources);
        }

        public void fileChanged(FileEvent fe) {
            RepositoryUpdater.this.fileChangedImpl(fe, this.listeningOnSources);
        }

        public void fileDeleted(FileEvent fe) {
            RepositoryUpdater.this.fileDeletedImpl(fe, this.listeningOnSources);
        }

        public void fileRenamed(FileRenameEvent fe) {
            RepositoryUpdater.this.fileRenamedImpl(fe, this.listeningOnSources);
        }
    }

    static final class RootsListeners {
        private FileChangeListener sourcesListener = null;
        private FileChangeListener binariesListener = null;
        private final Map<URL, File> sourceRoots = new HashMap<URL, File>();
        private final Map<URL, Pair<File, Boolean>> binaryRoots = new HashMap<URL, Pair<File, Boolean>>();
        private volatile boolean listens;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setListener(FileChangeListener sourcesListener, FileChangeListener binariesListener) {
            assert (sourcesListener != null && binariesListener != null || sourcesListener == null && binariesListener == null);
            RootsListeners rootsListeners = this;
            synchronized (rootsListeners) {
                if (sourcesListener != null) {
                    assert (this.sourcesListener == null);
                    assert (this.sourceRoots.isEmpty());
                    assert (this.binaryRoots.isEmpty());
                    this.sourcesListener = sourcesListener;
                    this.binariesListener = binariesListener;
                    if (!useRecursiveListeners) {
                        FileUtil.addFileChangeListener((FileChangeListener)sourcesListener);
                    }
                    this.listens = true;
                } else {
                    assert (this.sourcesListener != null);
                    if (!useRecursiveListeners) {
                        FileUtil.removeFileChangeListener((FileChangeListener)sourcesListener);
                    }
                    for (Map.Entry<URL, File> entry2 : this.sourceRoots.entrySet()) {
                        this.safeRemoveRecursiveListener(this.sourcesListener, entry2.getValue());
                    }
                    this.sourceRoots.clear();
                    for (Map.Entry<URL, File> entry : this.binaryRoots.entrySet()) {
                        if (((Boolean)((Pair)entry.getValue()).second()).booleanValue()) {
                            this.safeRemoveFileChangeListener(this.binariesListener, (File)((Pair)entry.getValue()).first());
                            continue;
                        }
                        this.safeRemoveRecursiveListener(this.binariesListener, (File)((Pair)entry.getValue()).first());
                    }
                    this.binaryRoots.clear();
                    this.sourcesListener = null;
                    this.binariesListener = null;
                    this.listens = false;
                }
            }
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public synchronized boolean add(@NonNull URL root, boolean sourceRoot, @NullAllowed ClassPath.Entry entry) {
            if (sourceRoot) {
                if (this.sourcesListener != null && !this.sourceRoots.containsKey(root) && root.getProtocol().equals("file")) {
                    try {
                        File f = org.openide.util.Utilities.toFile((URI)root.toURI());
                        this.safeAddRecursiveListener(this.sourcesListener, f, entry);
                        this.sourceRoots.put(root, f);
                    }
                    catch (URISyntaxException use) {
                        LOGGER.log(Level.INFO, null, use);
                    }
                }
            } else if (this.binariesListener != null && !this.binaryRoots.containsKey(root)) {
                File f = null;
                URL archiveUrl = FileUtil.getArchiveFile((URL)root);
                try {
                    URI uri;
                    URI uRI = uri = archiveUrl != null ? archiveUrl.toURI() : root.toURI();
                    if (uri.getScheme().equals("file")) {
                        f = org.openide.util.Utilities.toFile((URI)uri);
                    }
                }
                catch (URISyntaxException use) {
                    LOGGER.log(Level.INFO, "Can't convert " + root + " to java.io.File; archiveUrl=" + archiveUrl, use);
                }
                if (f != null) {
                    if (archiveUrl != null) {
                        this.safeAddFileChangeListener(this.binariesListener, f);
                    } else {
                        this.safeAddRecursiveListener(this.binariesListener, f, entry);
                    }
                    this.binaryRoots.put(root, (Pair)Pair.of((Object)f, (Object)(archiveUrl != null)));
                }
            }
            return this.listens;
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public synchronized void remove(Iterable<? extends URL> roots, boolean sourceRoot) {
            for (URL root : roots) {
                Pair<File, Boolean> pair;
                if (sourceRoot) {
                    File f;
                    if (this.sourcesListener == null || (f = this.sourceRoots.remove(root)) == null) continue;
                    this.safeRemoveRecursiveListener(this.sourcesListener, f);
                    continue;
                }
                if (this.binariesListener == null || (pair = this.binaryRoots.remove(root)) == null) continue;
                if (((Boolean)pair.second()).booleanValue()) {
                    this.safeRemoveFileChangeListener(this.binariesListener, (File)pair.first());
                    continue;
                }
                this.safeRemoveRecursiveListener(this.binariesListener, (File)pair.first());
            }
        }

        private void safeAddRecursiveListener(@NonNull FileChangeListener listener, @NonNull File path, final @NullAllowed ClassPath.Entry entry) {
            if (useRecursiveListeners) {
                try {
                    FileFilter filter = entry == null ? null : new FileFilter(){

                        @Override
                        public boolean accept(@NonNull File pathname) {
                            try {
                                return entry.includes(org.openide.util.Utilities.toURI((File)pathname).toURL());
                            }
                            catch (MalformedURLException ex) {
                                Exceptions.printStackTrace((Throwable)ex);
                                return true;
                            }
                        }
                    };
                    FileUtil.addRecursiveListener((FileChangeListener)listener, (File)path, (FileFilter)filter, (Callable)new Callable<Boolean>(){

                        @Override
                        public Boolean call() throws Exception {
                            return !RootsListeners.this.listens;
                        }
                    });
                }
                catch (ThreadDeath td) {
                    throw td;
                }
                catch (Throwable e) {
                    LOGGER.log(Level.FINE, null, e);
                }
            }
        }

        private void safeRemoveRecursiveListener(FileChangeListener listener, File path) {
            if (useRecursiveListeners) {
                try {
                    FileUtil.removeRecursiveListener((FileChangeListener)listener, (File)path);
                }
                catch (ThreadDeath td) {
                    throw td;
                }
                catch (Throwable e) {
                    LOGGER.log(Level.FINE, null, e);
                }
            }
        }

        private void safeAddFileChangeListener(FileChangeListener listener, File path) {
            try {
                FileUtil.addFileChangeListener((FileChangeListener)listener, (File)path);
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable e) {
                LOGGER.log(Level.FINE, null, e);
            }
        }

        private void safeRemoveFileChangeListener(FileChangeListener listener, File path) {
            try {
                FileUtil.removeFileChangeListener((FileChangeListener)listener, (File)path);
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable e) {
                LOGGER.log(Level.FINE, null, e);
            }
        }

    }

    static final class LexicographicComparator
    implements Comparator<URL> {
        private final boolean reverse;

        public LexicographicComparator(boolean reverse) {
            this.reverse = reverse;
        }

        @Override
        public int compare(URL o1, URL o2) {
            int order = o1.toString().compareTo(o2.toString());
            return this.reverse ? -1 * order : order;
        }
    }

    public static final class FSRefreshInterceptor
    implements IndexingActivityInterceptor {
        private FileSystem.AtomicAction activeAA = null;
        private boolean ignoreFsEvents = false;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public IndexingActivityInterceptor.Authorization authorizeFileSystemEvent(FileEvent event) {
            FSRefreshInterceptor fSRefreshInterceptor = this;
            synchronized (fSRefreshInterceptor) {
                if (this.activeAA != null) {
                    boolean firedFrom = event.firedFrom(this.activeAA);
                    LOGGER.log(Level.FINE, "{0} fired from {1}: {2}", new Object[]{event, this.activeAA, firedFrom});
                    return firedFrom ? IndexingActivityInterceptor.Authorization.IGNORE : IndexingActivityInterceptor.Authorization.PROCESS;
                }
                LOGGER.log(Level.FINE, "Set to ignore {0}: {1}", new Object[]{event, this.ignoreFsEvents});
                return this.ignoreFsEvents ? IndexingActivityInterceptor.Authorization.IGNORE : IndexingActivityInterceptor.Authorization.PROCESS;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setActiveAtomicAction(FileSystem.AtomicAction aa) {
            FSRefreshInterceptor fSRefreshInterceptor = this;
            synchronized (fSRefreshInterceptor) {
                LOGGER.log(Level.FINE, "setActiveAtomicAction({0})", (Object)aa);
                if (aa != null) {
                    assert (this.activeAA == null);
                    this.activeAA = aa;
                } else {
                    assert (this.activeAA != null);
                    this.activeAA = null;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setIgnoreFsEvents(boolean ignore) {
            FSRefreshInterceptor fSRefreshInterceptor = this;
            synchronized (fSRefreshInterceptor) {
                LOGGER.log(Level.FINE, "setIgnoreFsEvents({0})", ignore);
                assert (this.activeAA == null);
                this.ignoreFsEvents = ignore;
            }
        }
    }

    private final class Controller
    extends IndexingController {
        private Map<URL, List<URL>> roots2Dependencies;
        private Map<URL, List<URL>> binRoots2Dependencies;
        private Map<URL, List<URL>> roots2Peers;

        public Controller() {
            this.roots2Dependencies = Collections.emptyMap();
            this.binRoots2Dependencies = Collections.emptyMap();
            this.roots2Peers = Collections.emptyMap();
            RepositoryUpdater.this.start(false);
        }

        @Override
        public void enterProtectedMode() {
            RepositoryUpdater.this.getWorker().enterProtectedMode(Thread.currentThread().getId());
        }

        @Override
        public void exitProtectedMode(Runnable followUpTask) {
            RepositoryUpdater.this.getWorker().exitProtectedMode(Thread.currentThread().getId(), followUpTask);
        }

        @Override
        public boolean isInProtectedMode() {
            return RepositoryUpdater.this.getWorker().isInProtectedMode();
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        public synchronized Map<URL, List<URL>> getRootDependencies() {
            return this.roots2Dependencies;
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        public synchronized Map<URL, List<URL>> getBinaryRootDependencies() {
            return this.binRoots2Dependencies;
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        public Map<URL, List<URL>> getRootPeers() {
            return this.roots2Peers;
        }

        @Override
        public int getFileLocksDelay() {
            return FILE_LOCKS_DELAY;
        }

        static /* synthetic */ Map access$4402(Controller x0, Map x1) {
            x0.roots2Dependencies = x1;
            return x0.roots2Dependencies;
        }

        static /* synthetic */ Map access$4502(Controller x0, Map x1) {
            x0.binRoots2Dependencies = x1;
            return x0.binRoots2Dependencies;
        }

        static /* synthetic */ Map access$4602(Controller x0, Map x1) {
            x0.roots2Peers = x1;
            return x0.roots2Peers;
        }
    }

    private static final class BinaryIndexers {
        public final Collection<? extends BinaryIndexerFactory> bifs = MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookupAll(BinaryIndexerFactory.class);

        public static BinaryIndexers load() {
            return new BinaryIndexers();
        }

        private BinaryIndexers() {
        }
    }

    private static final class SourceIndexers {
        public final Set<IndexerCache.IndexerInfo<CustomIndexerFactory>> changedCifs;
        public final Collection<? extends IndexerCache.IndexerInfo<CustomIndexerFactory>> cifInfos;
        public final Set<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> changedEifs;
        public final Map<String, Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>>> eifInfosMap;

        public static SourceIndexers load(boolean detectChanges) {
            return new SourceIndexers(detectChanges);
        }

        private SourceIndexers(boolean detectChanges) {
            long start = System.currentTimeMillis();
            if (detectChanges) {
                this.changedCifs = new HashSet<IndexerCache.IndexerInfo<CustomIndexerFactory>>();
                this.changedEifs = new HashSet<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>>();
            } else {
                this.changedCifs = null;
                this.changedEifs = null;
            }
            this.cifInfos = IndexerCache.getCifCache().getIndexers(this.changedCifs);
            this.eifInfosMap = IndexerCache.getEifCache().getIndexersMap(this.changedEifs);
            long delta = System.currentTimeMillis() - start;
            LOGGER.log(Level.FINE, "Loading indexers took {0} ms.", delta);
        }
    }

    private static final class DependenciesContext {
        private final Map<URL, List<URL>> initialRoots2Deps;
        private final Map<URL, List<URL>> initialBinaries2InvDeps;
        private final Map<URL, List<URL>> initialRoots2Peers;
        private final Set<URL> oldRoots;
        private final Set<URL> oldBinaries;
        private final Map<URL, List<URL>> newRoots2Deps;
        private final Map<URL, List<URL>> newBinaries2InvDeps;
        private final Map<URL, List<URL>> newRoots2Peers;
        private final List<URL> newRootsToScan;
        private final Set<URL> newBinariesToScan;
        private final Set<URL> scannedRoots;
        private final Set<URL> scannedBinaries;
        private final Set<URL> sourcesForBinaryRoots;
        private final Set<URL> unknownRoots;
        private final Set<URL> newlySFBTranslated;
        private Map<URL, Collection<URL>> preInversedDeps;
        private Set<URL> fullRescanSourceRoots;
        private final Stack<URL> cycleDetector;
        private final boolean useInitialState;
        private final boolean refreshNonExistentDeps;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public DependenciesContext(Map<URL, List<URL>> scannedRoots2Deps, Map<URL, List<URL>> scannedBinaries2InvDependencies, Map<URL, List<URL>> scannedRoots2Peers, Set<URL> sourcesForBinaryRoots, boolean useInitialState, boolean refreshNonExistentDeps) {
            assert (scannedRoots2Deps != null);
            assert (scannedBinaries2InvDependencies != null);
            this.initialRoots2Deps = Collections.unmodifiableMap(scannedRoots2Deps);
            this.initialBinaries2InvDeps = Collections.unmodifiableMap(scannedBinaries2InvDependencies);
            this.initialRoots2Peers = Collections.unmodifiableMap(scannedRoots2Peers);
            this.oldRoots = new HashSet<URL>(scannedRoots2Deps.keySet());
            this.oldBinaries = new HashSet<URL>(scannedBinaries2InvDependencies.keySet());
            this.newRoots2Deps = new HashMap<URL, List<URL>>();
            this.newBinaries2InvDeps = new HashMap<URL, List<URL>>();
            this.newRoots2Peers = new HashMap<URL, List<URL>>();
            this.newRootsToScan = new ArrayList<URL>();
            this.newBinariesToScan = new HashSet<URL>();
            this.scannedRoots = new HashSet<URL>();
            this.scannedBinaries = new HashSet<URL>();
            this.sourcesForBinaryRoots = sourcesForBinaryRoots;
            this.fullRescanSourceRoots = new HashSet<URL>();
            this.useInitialState = useInitialState;
            this.refreshNonExistentDeps = refreshNonExistentDeps;
            this.cycleDetector = new Stack();
            this.unknownRoots = new HashSet<URL>();
            this.newlySFBTranslated = new HashSet<URL>();
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append(": {\n");
            sb.append("  useInitialState=").append(this.useInitialState).append("\n");
            sb.append("  initialRoots2Deps(").append(this.initialRoots2Deps.size()).append(")=\n");
            RepositoryUpdater.printMap(this.initialRoots2Deps, sb);
            sb.append("  initialBinaries(").append(this.initialBinaries2InvDeps.size()).append(")=\n");
            RepositoryUpdater.printMap(this.initialBinaries2InvDeps, sb);
            sb.append("  initialRoots2Peers(").append(this.initialRoots2Peers.size()).append(")=\n");
            RepositoryUpdater.printMap(this.initialRoots2Peers, sb);
            sb.append("  oldRoots(").append(this.oldRoots.size()).append(")=\n");
            RepositoryUpdater.printCollection(this.oldRoots, sb);
            sb.append("  oldBinaries(").append(this.oldBinaries.size()).append(")=\n");
            RepositoryUpdater.printCollection(this.oldBinaries, sb);
            sb.append("  newRootsToScan(").append(this.newRootsToScan.size()).append(")=\n");
            RepositoryUpdater.printCollection(this.newRootsToScan, sb);
            sb.append("  newBinariesToScan(").append(this.newBinariesToScan.size()).append(")=\n");
            RepositoryUpdater.printCollection(this.newBinariesToScan, sb);
            sb.append("  scannedRoots(").append(this.scannedRoots.size()).append(")=\n");
            RepositoryUpdater.printCollection(this.scannedRoots, sb);
            sb.append("  scannedBinaries(").append(this.scannedBinaries.size()).append(")=\n");
            RepositoryUpdater.printCollection(this.scannedBinaries, sb);
            sb.append("  newRoots2Deps(").append(this.newRoots2Deps.size()).append(")=\n");
            RepositoryUpdater.printMap(this.newRoots2Deps, sb);
            sb.append("  newBinaries2InvDeps(").append(this.newBinaries2InvDeps.size()).append(")=\n");
            RepositoryUpdater.printMap(this.newBinaries2InvDeps, sb);
            sb.append("  newRoots2Peers(").append(this.newRoots2Peers.size()).append(")=\n");
            RepositoryUpdater.printMap(this.newRoots2Peers, sb);
            sb.append("} ----\n");
            return sb.toString();
        }

        static /* synthetic */ Set access$3900(DependenciesContext x0) {
            return x0.unknownRoots;
        }

        static /* synthetic */ Map access$4002(DependenciesContext x0, Map x1) {
            x0.preInversedDeps = x1;
            return x0.preInversedDeps;
        }

        static /* synthetic */ Map access$4100(DependenciesContext x0) {
            return x0.initialRoots2Peers;
        }

        static /* synthetic */ Set access$4300(DependenciesContext x0) {
            return x0.oldBinaries;
        }

        static /* synthetic */ Map access$4000(DependenciesContext x0) {
            return x0.preInversedDeps;
        }
    }

    private static final class Task
    implements Runnable {
        private final List<Work> todo = new LinkedList<Work>();
        private boolean followUpWorksSorted = true;
        private Work workInProgress = null;
        private boolean scheduled = false;
        private boolean allCancelled = false;
        private List<Long> protectedOwners = new LinkedList<Long>();
        private List<Runnable> followupTasks = null;

        private Task() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void schedule(Iterable<? extends Work> multipleWork) {
            List<Work> list = this.todo;
            synchronized (list) {
                for (Work w : multipleWork) {
                    this.schedule(w, false);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void schedule(Work work, boolean wait) {
            boolean waitForWork = false;
            if (wait && Utilities.holdsParserLock()) {
                throw new IllegalStateException("Caller holds TaskProcessor.parserLock, which may cause deadlock.");
            }
            List<Work> list = this.todo;
            synchronized (list) {
                assert (work != null);
                if (!this.allCancelled) {
                    boolean canceled = false;
                    ArrayList follow = new ArrayList(1);
                    if (this.workInProgress != null && this.workInProgress.cancelBy(work, follow)) {
                        if (this.workInProgress.logCtx != null) {
                            if (work.logCtx != null) {
                                work.logCtx.absorb(this.workInProgress.logCtx);
                            } else {
                                work.logCtx = this.workInProgress.logCtx;
                            }
                        }
                        canceled = true;
                    }
                    Work absorbedBy = null;
                    if (!wait) {
                        Work lastDel = null;
                        if (work instanceof FileListWork) {
                            FileListWork flw = (FileListWork)work;
                            for (Work w : this.todo) {
                                if (!(w instanceof DeleteWork) || !((DeleteWork)w).root.equals(flw.root)) continue;
                                lastDel = w;
                            }
                        }
                        for (Work w : this.todo) {
                            if (lastDel == null) {
                                if (!w.absorb(work)) continue;
                                absorbedBy = w;
                                break;
                            }
                            if (w != lastDel) continue;
                            lastDel = null;
                        }
                    }
                    if (absorbedBy == null) {
                        LOGGER.log(Level.FINE, "Scheduling {0}", work);
                        if (canceled) {
                            this.todo.add(0, work);
                            this.todo.addAll(1, follow);
                        } else {
                            this.todo.add(work);
                        }
                    } else {
                        if (absorbedBy.logCtx != null) {
                            if (work.logCtx != null) {
                                absorbedBy.logCtx.absorb(work.logCtx);
                            }
                        } else {
                            absorbedBy.logCtx = work.logCtx;
                        }
                        if (canceled) {
                            this.todo.remove(absorbedBy);
                            this.todo.add(0, absorbedBy);
                            this.todo.addAll(1, follow);
                        }
                        LOGGER.log(Level.FINE, "Work absorbed {0}", work);
                    }
                    this.followUpWorksSorted = false;
                    if (!this.scheduled && this.protectedOwners.isEmpty()) {
                        this.scheduled = true;
                        LOGGER.fine("scheduled = true");
                        WORKER.submit((Runnable)this);
                    }
                    waitForWork = wait;
                }
            }
            if (waitForWork) {
                LOGGER.log(Level.FINE, "Waiting for {0}", work);
                work.waitUntilDone();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void cancelAll(final @NullAllowed Runnable postCleanTask) throws TimeoutException {
            List<Work> list = this.todo;
            synchronized (list) {
                if (!this.allCancelled) {
                    this.todo.clear();
                    if (postCleanTask != null) {
                        this.schedule(new Work(false, false, false, true, SuspendSupport.NOP, null){

                            @Override
                            protected boolean getDone() {
                                postCleanTask.run();
                                return true;
                            }
                        }, false);
                    }
                    this.allCancelled = true;
                    Work work = this.workInProgress;
                    if (work != null) {
                        work.setCancelled(true);
                    }
                    int cnt = 10;
                    while (this.scheduled && cnt-- > 0) {
                        LOGGER.log(Level.FINE, "Waiting for indexing jobs to finish; job in progress: {0}, jobs queue: {1}", new Object[]{work, this.todo});
                        try {
                            this.todo.wait(1000);
                            continue;
                        }
                        catch (InterruptedException ie) {
                            // empty catch block
                            break;
                        }
                    }
                    if (this.scheduled && cnt == 0) {
                        LOGGER.log(Level.INFO, "Waiting for indexing jobs to finish timed out; job in progress {0}, jobs queue: {1}", new Object[]{work, this.todo});
                        throw new TimeoutException();
                    }
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isWorking() {
            List<Work> list = this.todo;
            synchronized (list) {
                return this.scheduled;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void enterProtectedMode(@NullAllowed Long id) {
            List<Work> list = this.todo;
            synchronized (list) {
                this.protectedOwners.add(id);
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "Entering protected mode: {0}", this.protectedOwners.toString());
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void exitProtectedMode(@NullAllowed Long id, @NullAllowed Runnable followupTask) {
            List<Work> list = this.todo;
            synchronized (list) {
                if (this.protectedOwners.isEmpty()) {
                    throw new IllegalStateException("Calling exitProtectedMode without enterProtectedMode");
                }
                if (followupTask != null) {
                    if (this.followupTasks == null) {
                        this.followupTasks = new LinkedList<Runnable>();
                    }
                    this.followupTasks.add(followupTask);
                }
                this.protectedOwners.remove(id);
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "Exiting protected mode: {0}", this.protectedOwners.toString());
                }
                if (this.protectedOwners.isEmpty()) {
                    final List<Runnable> tasks = this.followupTasks;
                    RP.create(new Runnable(){

                        @Override
                        public void run() {
                            Task.this.schedule(new Work(false, false, false, true, SuspendSupport.NOP, null){

                                @Override
                                protected boolean getDone() {
                                    if (tasks != null) {
                                        for (Runnable task : tasks) {
                                            try {
                                                task.run();
                                                continue;
                                            }
                                            catch (ThreadDeath td) {
                                                throw td;
                                            }
                                            catch (Throwable t) {
                                                LOGGER.log(Level.WARNING, null, t);
                                                continue;
                                            }
                                        }
                                    }
                                    return true;
                                }
                            }, false);
                        }

                    }).schedule(FILE_LOCKS_DELAY);
                    LOGGER.log(Level.FINE, "Protected mode exited, scheduling postprocess tasks: {0}", tasks);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isInProtectedMode() {
            List<Work> list = this.todo;
            synchronized (list) {
                return !this.protectedOwners.isEmpty();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isProtectedModeOwner(Thread thread) {
            List<Work> list = this.todo;
            synchronized (list) {
                return this.protectedOwners.contains(thread.getId());
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean waitUntilFinished(long timeout) throws InterruptedException {
            if (Utilities.holdsParserLock()) {
                throw new IllegalStateException("Can't wait for indexing to finish from inside a running parser task");
            }
            List<Work> list = this.todo;
            synchronized (list) {
                while (this.scheduled) {
                    if (timeout > 0) {
                        this.todo.wait(timeout);
                        return !this.scheduled;
                    }
                    this.todo.wait();
                }
            }
            return true;
        }

        @Override
        public void run() {
            try {
                Utilities.runPriorityIO(new Callable<Void>(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public Void call() throws Exception {
                        try {
                            RunWhenScanFinishedSupport.performScan(new Runnable(){

                                @Override
                                public void run() {
                                    Task.this._run();
                                }
                            });
                        }
                        finally {
                            List list = Task.this.todo;
                            synchronized (list) {
                                if (!Task.this.protectedOwners.isEmpty() || Task.this.todo.isEmpty()) {
                                    Task.this.scheduled = false;
                                    LOGGER.fine("scheduled = false");
                                } else {
                                    WORKER.submit((Callable)this);
                                }
                                Task.this.todo.notifyAll();
                            }
                            RunWhenScanFinishedSupport.performDeferredTasks();
                        }
                        return null;
                    }

                });
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }

        /*
         * Exception decompiling
         */
        private void _run() {
            // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
            // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [3[TRYBLOCK]], but top level block is 5[CATCHBLOCK]
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
            // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
            // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
            // org.benf.cfr.reader.entities.ClassFile.analyseInnerClassesPass1(ClassFile.java:681)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:764)
            // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
            // org.benf.cfr.reader.Main.doJar(Main.java:134)
            // org.benf.cfr.reader.Main.main(Main.java:189)
            throw new IllegalStateException("Decompilation failed");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private Work getWork() {
            List<Work> list = this.todo;
            synchronized (list) {
                Work w;
                if (this.protectedOwners.isEmpty() && this.todo.size() > 0) {
                    w = this.todo.remove(0);
                    if (w instanceof FileListWork && ((FileListWork)w).isFollowUpJob() && !this.followUpWorksSorted) {
                        ArrayList sortedRoots;
                        HashMap<URL, LinkedList<FileListWork>> toSort = new HashMap<URL, LinkedList<FileListWork>>();
                        toSort.put(((FileListWork)w).root, new LinkedList<FileListWork>(Arrays.asList((FileListWork)w)));
                        Iterator<Work> it = this.todo.iterator();
                        while (it.hasNext()) {
                            Work current = it.next();
                            if (!(current instanceof FileListWork) || !((FileListWork)current).isFollowUpJob()) continue;
                            LinkedList<FileListWork> currentWorks = (LinkedList<FileListWork>)toSort.get(((FileListWork)current).root);
                            if (currentWorks == null) {
                                currentWorks = new LinkedList<FileListWork>();
                                toSort.put(((FileListWork)current).root, currentWorks);
                            }
                            currentWorks.add((FileListWork)current);
                            it.remove();
                        }
                        try {
                            sortedRoots = new ArrayList(org.openide.util.Utilities.topologicalSort(toSort.keySet(), (Map)RepositoryUpdater.getDefault().scannedRoots2Dependencies));
                        }
                        catch (TopologicalSortException tse) {
                            LOGGER.log(Level.INFO, "Cycles detected in classpath roots dependencies, using partial ordering", (Throwable)tse);
                            List partialSort = tse.partialSort();
                            sortedRoots = new ArrayList(partialSort);
                        }
                        Collections.reverse(sortedRoots);
                        for (URL url : sortedRoots) {
                            List flws = (List)toSort.get(url);
                            if (flws == null) continue;
                            this.todo.addAll(flws);
                        }
                        this.followUpWorksSorted = true;
                        w = this.todo.remove(0);
                    }
                } else {
                    w = null;
                }
                this.workInProgress = w;
                return w;
            }
        }

        public static final class IndexingBridgeImpl
        extends IndexingBridge.Ordering {
            protected void enterProtectedMode() {
                RepositoryUpdater.getDefault().getWorker().enterProtectedMode(null);
            }

            protected void exitProtectedMode() {
                RepositoryUpdater.getDefault().getWorker().exitProtectedMode(null, null);
            }

            protected void await() throws InterruptedException {
                RepositoryUpdater.getDefault().waitUntilFinished(-1);
            }
        }

    }

    private final class InitialRootsWork
    extends RootsWork {
        private final boolean waitForProjects;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public InitialRootsWork(Map<URL, List<URL>> scannedRoots2Depencencies, Map<URL, List<URL>> scannedBinaries2InvDependencies, Map<URL, List<URL>> scannedRoots2Peers, Set<URL> sourcesForBinaryRoots, @NonNull boolean waitForProjects, @NonNull AtomicLong scannedRoots2DependenciesLamport, @NullAllowed SuspendStatus suspendStatus, LogContext logCtx) {
            super(scannedRoots2Depencencies, scannedBinaries2InvDependencies, scannedRoots2Peers, sourcesForBinaryRoots, true, true, scannedRoots2DependenciesLamport, suspendStatus, logCtx);
            this.waitForProjects = waitForProjects;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean getDone() {
            boolean retry;
            block22 : {
                if (this.waitForProjects) {
                    retry = true;
                    this.suspendProgress(NbBundle.getMessage(RepositoryUpdater.class, (String)"MSG_OpeningProjects"));
                    while (retry) {
                        try {
                            OpenProjects.getDefault().openProjects().get(1000, TimeUnit.MILLISECONDS);
                            retry = false;
                        }
                        catch (TimeoutException ex) {
                            if (!this.isCancelledExternally()) continue;
                            boolean bl = false;
                            if (RepositoryUpdater.this.state == State.INITIAL_SCAN_RUNNING) {
                                RepositoryUpdater repositoryUpdater = RepositoryUpdater.this;
                                synchronized (repositoryUpdater) {
                                    if (RepositoryUpdater.this.state == State.INITIAL_SCAN_RUNNING) {
                                        RepositoryUpdater.this.state = State.ACTIVE;
                                    }
                                }
                            }
                            return bl;
                        }
                        catch (Exception ex) {
                            retry = false;
                        }
                    }
                }
                if (this.indexers != null) break block22;
                this.indexers = SourceIndexers.load(true);
            }
            retry = super.getDone();
            return retry;
            finally {
                if (RepositoryUpdater.this.state == State.INITIAL_SCAN_RUNNING) {
                    RepositoryUpdater repositoryUpdater = RepositoryUpdater.this;
                    synchronized (repositoryUpdater) {
                        if (RepositoryUpdater.this.state == State.INITIAL_SCAN_RUNNING) {
                            RepositoryUpdater.this.state = State.ACTIVE;
                        }
                    }
                }
            }
        }
    }

    private static abstract class AbstractRootsWork
    extends Work {
        private boolean logStatistics;

        protected AbstractRootsWork(boolean logStatistics, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext ctx) {
            super(false, false, true, true, suspendStatus, ctx);
            this.logStatistics = logStatistics;
        }

        protected final boolean scanBinaries(DependenciesContext ctx) {
            assert (ctx != null);
            final AtomicInteger scannedRootsCnt = new AtomicInteger(0);
            final BinaryIndexers binaryIndexers = ctx.newBinariesToScan.isEmpty() ? null : BinaryIndexers.load();
            IndexBinaryWorkPool pool = new IndexBinaryWorkPool(new IndexBinaryWorkPool.Function<URL, Boolean>(){

                @Override
                public Boolean apply(URL root) {
                    return AbstractRootsWork.this.scanBinary(root, binaryIndexers, scannedRootsCnt);
                }
            }, new Callable<Boolean>(){

                @Override
                public Boolean call() throws Exception {
                    return AbstractRootsWork.this.getCancelRequest().isRaised();
                }
            }, ctx.newBinariesToScan);
            long binaryScanStart = System.currentTimeMillis();
            Pair<Boolean, Collection<? extends URL>> res = pool.execute();
            long binaryScanEnd = System.currentTimeMillis();
            ctx.scannedBinaries.addAll((Collection)res.second());
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, "Complete indexing of {0} binary roots took: {1} ms", new Object[]{scannedRootsCnt.get(), binaryScanEnd - binaryScanStart});
            }
            TEST_LOGGER.log(Level.FINEST, "scanBinary", ctx.newBinariesToScan);
            return (Boolean)res.first();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final boolean scanBinary(URL root, BinaryIndexers binaryIndexers, AtomicInteger scannedRootsCnt) {
            boolean success;
            long tmStart;
            block17 : {
                success = false;
                tmStart = System.currentTimeMillis();
                LinkedHashMap<BinaryIndexerFactory, Context> contexts = new LinkedHashMap<BinaryIndexerFactory, Context>();
                BitSet startedIndexers = new BitSet(contexts.size());
                LogContext lctx = this.getLogContext();
                if (lctx != null) {
                    lctx.noteRootScanning(root, false);
                }
                try {
                    FileObject file;
                    boolean upToDate;
                    long currentLastModified;
                    this.createBinaryContexts(root, binaryIndexers, contexts);
                    FileObject rootFo = URLMapper.findFileObject((URL)root);
                    FileObject fileObject = file = rootFo == null ? null : FileUtil.getArchiveFile((FileObject)rootFo);
                    if (file != null) {
                        Pair<Long, Map<Pair<String, Integer>, Integer>> lastState = ArchiveTimeStamps.getLastModified(file.getURL());
                        boolean indexersUpToDate = this.checkBinaryIndexers(lastState, contexts);
                        currentLastModified = file.lastModified().getTime();
                        upToDate = indexersUpToDate && (Long)lastState.first() == currentLastModified;
                    } else {
                        currentLastModified = -1;
                        upToDate = false;
                    }
                    try {
                        this.binaryScanStarted(root, upToDate, contexts, startedIndexers);
                        this.updateProgress(root, true);
                        success = this.indexBinary(root, binaryIndexers, contexts);
                    }
                    catch (IOException ioe) {
                        LOGGER.log(Level.WARNING, null, ioe);
                    }
                    finally {
                        this.binaryScanFinished(binaryIndexers, contexts, startedIndexers, success);
                        URL archiveFile = FileUtil.getArchiveFile((URL)root);
                        if (success && archiveFile != null && !upToDate) {
                            ArchiveTimeStamps.setLastModified(archiveFile, this.createBinaryIndexersTimeStamp(currentLastModified, contexts));
                        }
                    }
                    if (lctx == null) break block17;
                    lctx.finishScannedRoot(root);
                }
                catch (IOException ioe) {
                    block18 : {
                        try {
                            LOGGER.log(Level.WARNING, null, ioe);
                            if (lctx == null) break block18;
                            lctx.finishScannedRoot(root);
                        }
                        catch (Throwable var19_22) {
                            if (lctx != null) {
                                lctx.finishScannedRoot(root);
                            }
                            long time = System.currentTimeMillis() - tmStart;
                            if (scannedRootsCnt != null) {
                                scannedRootsCnt.incrementAndGet();
                            }
                            AbstractRootsWork.reportRootScan(root, time);
                            LOGGER.log(Level.FINE, "Indexing of: {0} took: {1} ms", new Object[]{root, time});
                            throw var19_22;
                        }
                    }
                    long time = System.currentTimeMillis() - tmStart;
                    if (scannedRootsCnt != null) {
                        scannedRootsCnt.incrementAndGet();
                    }
                    AbstractRootsWork.reportRootScan(root, time);
                    LOGGER.log(Level.FINE, "Indexing of: {0} took: {1} ms", new Object[]{root, time});
                }
            }
            long time = System.currentTimeMillis() - tmStart;
            if (scannedRootsCnt != null) {
                scannedRootsCnt.incrementAndGet();
            }
            AbstractRootsWork.reportRootScan(root, time);
            LOGGER.log(Level.FINE, "Indexing of: {0} took: {1} ms", new Object[]{root, time});
            return success;
        }

        /*
         * Exception decompiling
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        protected final boolean scanSources(DependenciesContext ctx, SourceIndexers indexers, Map<URL, List<URL>> preregisterIn) {
            // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
            // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [1[TRYBLOCK]], but top level block is 17[SIMPLE_IF_TAKEN]
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
            // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
            // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
            // org.benf.cfr.reader.entities.ClassFile.analyseInnerClassesPass1(ClassFile.java:681)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:764)
            // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
            // org.benf.cfr.reader.Main.doJar(Main.java:134)
            // org.benf.cfr.reader.Main.main(Main.java:189)
            throw new IllegalStateException("Decompilation failed");
        }

        private static boolean isNoRootsScan() {
            return Boolean.getBoolean("netbeans.indexing.noRootsScan");
        }

        @CheckForNull
        private static URL getRemoteIndexURL(@NonNull URL sourceRoot) {
            for (IndexDownloader ld : Lookup.getDefault().lookupAll(IndexDownloader.class)) {
                URL indexURL = ld.getIndexURL(sourceRoot);
                if (indexURL == null) continue;
                return indexURL;
            }
            return null;
        }

        private static boolean patchDownloadedIndex(@NonNull URL sourceRoot, @NonNull URL cacheFolder) {
            boolean vote = true;
            for (DownloadedIndexPatcher patcher : Lookup.getDefault().lookupAll(DownloadedIndexPatcher.class)) {
                vote &= patcher.updateIndex(sourceRoot, cacheFolder);
            }
            return vote;
        }

        @NonNull
        private static String getSimpleName(@NonNull URL indexURL) throws IllegalArgumentException {
            String path = indexURL.getPath();
            if (path.length() == 0 || path.charAt(path.length() - 1) == '/') {
                throw new IllegalArgumentException(indexURL.toString());
            }
            int index = path.lastIndexOf(47);
            return index < 0 ? path : path.substring(index + 1);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @CheckForNull
        private File download(@NonNull URL indexURL, @NonNull File into) throws IOException {
            try {
                File packedIndex = new File(into, AbstractRootsWork.getSimpleName(indexURL));
                BufferedInputStream in = new BufferedInputStream(indexURL.openStream());
                try {
                    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(packedIndex));
                    try {
                        FileUtil.copy((InputStream)in, (OutputStream)out);
                    }
                    finally {
                        out.close();
                    }
                }
                finally {
                    in.close();
                }
                return packedIndex;
            }
            catch (IOException ioe) {
                return null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean unpack(@NonNull File packedFile, @NonNull File targetFolder) throws IOException {
            ZipFile zf = new ZipFile(packedFile);
            Enumeration<? extends ZipEntry> entries = zf.entries();
            try {
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    File target = new File(targetFolder, entry.getName().replace('/', File.separatorChar));
                    if (entry.isDirectory()) {
                        target.mkdirs();
                        continue;
                    }
                    target.getParentFile().mkdirs();
                    InputStream in = zf.getInputStream(entry);
                    try {
                        FileOutputStream out = new FileOutputStream(target);
                        try {
                            FileUtil.copy((InputStream)in, (OutputStream)out);
                            continue;
                        }
                        finally {
                            out.close();
                            continue;
                        }
                    }
                    finally {
                        in.close();
                        continue;
                    }
                }
            }
            finally {
                zf.close();
            }
            return true;
        }

        private static /* varargs */ void delete(@NullAllowed File ... toDelete) {
            if (toDelete != null) {
                for (File td : toDelete) {
                    if (td.isDirectory()) {
                        AbstractRootsWork.delete(td.listFiles());
                    }
                    td.delete();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean nopCustomIndexers(@NonNull URL root, @NonNull SourceIndexers indexers, boolean sourceForBinaryRoot) throws IOException {
            FileObject cacheRoot = CacheFolder.getDataFolder(root);
            HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> transactionContexts = new HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>>();
            Work.UsedIndexables usedIndexables = new Work.UsedIndexables();
            HashMap<SourceIndexerFactory, Boolean> votes = new HashMap<SourceIndexerFactory, Boolean>();
            boolean indexResult = false;
            try {
                this.customIndexersScanStarted(root, cacheRoot, sourceForBinaryRoot, indexers.cifInfos, votes, transactionContexts);
                for (IndexerCache.IndexerInfo<CustomIndexerFactory> info : indexers.cifInfos) {
                    if (this.getCancelRequest().isRaised()) break;
                    CustomIndexerFactory factory = info.getIndexerFactory();
                    CustomIndexer indexer = factory.createIndexer();
                    if (LOGGER.isLoggable(Level.FINE)) {
                        LOGGER.fine("Fake indexing: indexer=" + indexer);
                    }
                    long start = System.currentTimeMillis();
                    this.logStartIndexer(info.getIndexerName());
                    try {
                        Pair indexerKey = Pair.of((Object)factory.getIndexerName(), (Object)factory.getIndexVersion());
                        Pair<SourceIndexerFactory, Context> ctx = transactionContexts.get((Object)indexerKey);
                        if (ctx != null) {
                            SPIAccessor.getInstance().index(indexer, Collections.emptySet(), (Context)ctx.second());
                            continue;
                        }
                        LOGGER.log(Level.WARNING, "RefreshCifIndices ignored recently added factory: {0}", (Object)indexerKey);
                        continue;
                    }
                    catch (ThreadDeath td) {
                        throw td;
                    }
                    catch (Throwable t) {
                        LOGGER.log(Level.WARNING, null, t);
                        continue;
                    }
                    finally {
                        this.logIndexerTime(info.getIndexerName(), (int)(System.currentTimeMillis() - start));
                        continue;
                    }
                }
                indexResult = !this.getCancelRequest().isRaised();
            }
            finally {
                this.scanFinished(transactionContexts.values(), usedIndexables, indexResult);
            }
            return indexResult;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean scanSource(URL root, boolean fullRescan, boolean sourceForBinaryRoot, SourceIndexers indexers, int[] outOfDateFiles, int[] deletedFiles, long[] recursiveListenersTime) throws IOException {
            LOGGER.log(Level.FINE, "Scanning sources root: {0}", root);
            boolean rootSeen = TimeStamps.existForRoot(root);
            if (AbstractRootsWork.isNoRootsScan() && !fullRescan && rootSeen) {
                return this.nopCustomIndexers(root, indexers, sourceForBinaryRoot);
            }
            FileObject rootFo = URLCache.getInstance().findFileObject(root, true);
            if (rootFo != null) {
                URL indexURL;
                LogContext lctx = this.getLogContext();
                long t = System.currentTimeMillis();
                if (!rootSeen && (indexURL = AbstractRootsWork.getRemoteIndexURL(root)) != null) {
                    LOGGER.log(Level.FINE, "Downloading index for root: {0} from: {1}", new Object[]{root, indexURL});
                    FileObject cf = CacheFolder.getCacheFolder();
                    assert (cf != null);
                    File cacheFolder = FileUtil.toFile((FileObject)cf);
                    assert (cacheFolder != null);
                    File downloadFolder = new File(cacheFolder, "index-download");
                    if (downloadFolder.exists()) {
                        AbstractRootsWork.delete(downloadFolder.listFiles());
                    } else {
                        downloadFolder.mkdir();
                    }
                    File packedIndex = this.download(indexURL, downloadFolder);
                    if (packedIndex != null) {
                        this.unpack(packedIndex, downloadFolder);
                        packedIndex.delete();
                        if (AbstractRootsWork.patchDownloadedIndex(root, org.openide.util.Utilities.toURI((File)downloadFolder).toURL())) {
                            FileObject df = CacheFolder.getDataFolder(root);
                            assert (df != null);
                            File dataFolder = FileUtil.toFile((FileObject)df);
                            assert (dataFolder != null);
                            if (dataFolder.exists()) {
                                AbstractRootsWork.delete(dataFolder);
                            }
                            downloadFolder.renameTo(dataFolder);
                            TimeStamps timeStamps = TimeStamps.forRoot(root, false);
                            timeStamps.resetToNow();
                            timeStamps.store();
                            this.nopCustomIndexers(root, indexers, sourceForBinaryRoot);
                            for (Map.Entry e : IndexManager.getOpenIndexes().entrySet()) {
                                if (!Util.isParentOf(dataFolder, (File)e.getKey())) continue;
                                ((Index)e.getValue()).getStatus(true);
                            }
                            return true;
                        }
                    }
                }
                ClassPath.Entry entry = sourceForBinaryRoot ? null : RepositoryUpdater.getClassPathEntry(rootFo);
                EnumSet<Crawler.TimeStampAction> checkTimeStamps = EnumSet.of(Crawler.TimeStampAction.UPDATE);
                if (!fullRescan) {
                    checkTimeStamps.add(Crawler.TimeStampAction.CHECK);
                }
                FileObjectCrawler crawler = new FileObjectCrawler(rootFo, checkTimeStamps, entry, this.getCancelRequest(), this.getSuspendStatus());
                List<Indexable> resources = crawler.getResources();
                List<Indexable> allResources = crawler.getAllResources();
                List<Indexable> deleted = crawler.getDeletedResources();
                this.modifiedResourceCount = resources.size();
                this.allResourceCount = allResources.size();
                this.logCrawlerTime(crawler, t);
                if (crawler.isFinished()) {
                    IdentityHashMap<SourceIndexerFactory, Boolean> invalidatedMap = new IdentityHashMap<SourceIndexerFactory, Boolean>();
                    HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> ctxToFinish = new HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>>();
                    Work.UsedIndexables usedIterables = new Work.UsedIndexables();
                    boolean indexResult = false;
                    try {
                        this.scanStarted(root, sourceForBinaryRoot, indexers, invalidatedMap, ctxToFinish);
                        this.delete(deleted, ctxToFinish, usedIterables);
                        this.invalidateSources(resources);
                        long tm = System.currentTimeMillis();
                        boolean rlAdded = RepositoryUpdater.getDefault().rootsListeners.add(root, true, entry);
                        if (recursiveListenersTime != null) {
                            recursiveListenersTime[0] = System.currentTimeMillis() - tm;
                        }
                        if (rlAdded && (indexResult = this.index(resources, allResources, root, sourceForBinaryRoot, indexers, invalidatedMap, ctxToFinish, usedIterables))) {
                            crawler.storeTimestamps();
                            outOfDateFiles[0] = resources.size();
                            deletedFiles[0] = deleted.size();
                            if (this.logStatistics) {
                                this.logStatistics = false;
                                if (SFEC_LOGGER.isLoggable(Level.INFO)) {
                                    LogRecord r = new LogRecord(Level.INFO, "STATS_SCAN_SOURCES");
                                    Object[] arrobject = new Object[1];
                                    arrobject[0] = outOfDateFiles[0] > 0 || deletedFiles[0] > 0;
                                    r.setParameters(arrobject);
                                    r.setResourceBundle(NbBundle.getBundle(RepositoryUpdater.class));
                                    r.setResourceBundleName(RepositoryUpdater.class.getPackage().getName() + ".Bundle");
                                    r.setLoggerName(SFEC_LOGGER.getName());
                                    SFEC_LOGGER.log(r);
                                }
                            }
                            boolean r = true;
                            return r;
                        }
                    }
                    finally {
                        this.scanFinished(ctxToFinish.values(), usedIterables, indexResult);
                    }
                }
                return false;
            }
            RepositoryUpdater.getDefault().rootsListeners.add(root, true, null);
            return true;
        }

        private static void reportRootScan(URL root, long duration) {
            if (PERF_LOGGER.isLoggable(Level.FINE)) {
                PERF_LOGGER.log(Level.FINE, "reportScanOfFile: {0} {1}", new Object[]{root, duration});
            }
        }

    }

    private static class RootsWork
    extends AbstractRootsWork {
        private final Map<URL, List<URL>> scannedRoots2Dependencies;
        private final Map<URL, List<URL>> scannedBinaries2InvDependencies;
        private final Map<URL, List<URL>> scannedRoots2Peers;
        private final Set<URL> sourcesForBinaryRoots;
        private final AtomicLong scannedRoots2DependenciesLamport;
        private boolean useInitialState;
        private boolean refreshNonExistentDeps;
        private DependenciesContext depCtx;
        protected SourceIndexers indexers = null;
        private boolean shouldDoNothing;
        private Level previousLevel;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public RootsWork(Map<URL, List<URL>> scannedRoots2Depencencies, Map<URL, List<URL>> scannedBinaries2InvDependencies, Map<URL, List<URL>> scannedRoots2Peers, Set<URL> sourcesForBinaryRoots, boolean useInitialState, boolean refreshNonExistentDeps, @NonNull AtomicLong scannedRoots2DependenciesLamport, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(false, suspendStatus, logCtx);
            this.scannedRoots2Dependencies = scannedRoots2Depencencies;
            this.scannedBinaries2InvDependencies = scannedBinaries2InvDependencies;
            this.scannedRoots2Peers = scannedRoots2Peers;
            this.sourcesForBinaryRoots = sourcesForBinaryRoots;
            this.useInitialState = useInitialState;
            this.refreshNonExistentDeps = refreshNonExistentDeps;
            this.scannedRoots2DependenciesLamport = scannedRoots2DependenciesLamport;
        }

        @Override
        public String toString() {
            return super.toString() + ", useInitialState=" + this.useInitialState;
        }

        private void dumpGlobalRegistry(String n, Collection<String> pathIds) {
            boolean printed = false;
            for (String pathId : pathIds) {
                GlobalPathRegistry gpr = GlobalPathRegistry.getDefault();
                Set paths = gpr.getPaths(pathId);
                if (!paths.isEmpty() && !printed) {
                    LOGGER.log(Level.FINE, "Dumping: {0}", n);
                    printed = true;
                }
                LOGGER.log(Level.FINE, "Paths ID {0}: {1}", new Object[]{pathId, paths});
            }
        }

        private void checkRootCollection(Collection<? extends URL> roots) {
            String path;
            FileObject parent;
            URL u;
            FileObject archive;
            File archiveFile;
            FileObject f;
            if (!this.shouldDoNothing || roots.isEmpty() || this.getCancelRequest().isRaised()) {
                return;
            }
            int stubCount = 0;
            Iterator<? extends URL> i$ = roots.iterator();
            while (i$.hasNext() && (path = (u = i$.next()).getPath()) != null && path.endsWith("stubs.zip!/") && (f = URLMapper.findFileObject((URL)u)) != null && (archive = FileUtil.getArchiveFile((FileObject)f)) != null && (archiveFile = FileUtil.toFile((FileObject)archive)) != null && (parent = archive.getParent()) != null && (parent = parent.getParent()) != null) {
                String clusterPath = FileUtil.getRelativePath((FileObject)parent, (FileObject)archive);
                File file = InstalledFileLocator.getDefault().locate(clusterPath, null, false);
                if (file == null || !file.equals(archiveFile)) break;
                ++stubCount;
            }
            if (stubCount == roots.size()) {
                return;
            }
            if (this.previousLevel == null) {
                Level toSet;
                this.previousLevel = LOGGER.getLevel() == null ? Level.ALL : LOGGER.getLevel();
                try {
                    toSet = Level.parse(System.getProperty("RepositoryUpdate.increasedLogLevel", "FINE"));
                }
                catch (IllegalArgumentException ex) {
                    toSet = Level.FINE;
                }
                LOGGER.setLevel(toSet);
                LOGGER.warning("Non-empty roots encountered while no projects are opened; loglevel increased");
                Collection recogs = Lookup.getDefault().lookupAll(PathRecognizer.class);
                PathRecognizerRegistry reg = PathRecognizerRegistry.getDefault();
                this.dumpGlobalRegistry("Binary Libraries", reg.getBinaryLibraryIds());
                this.dumpGlobalRegistry("Libraries", reg.getLibraryIds());
                this.dumpGlobalRegistry("Sources", reg.getSourceIds());
            }
        }

        /*
         * Exception decompiling
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        public boolean getDone() {
            // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
            // org.benf.cfr.reader.util.ConfusedCFRException: Started 2 blocks at once
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.getStartingBlocks(Op04StructuredStatement.java:374)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:452)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
            // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
            // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
            // org.benf.cfr.reader.entities.ClassFile.analyseInnerClassesPass1(ClassFile.java:681)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:764)
            // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
            // org.benf.cfr.reader.Main.doJar(Main.java:134)
            // org.benf.cfr.reader.Main.main(Main.java:189)
            throw new IllegalStateException("Decompilation failed");
        }

        @Override
        protected boolean isCancelledBy(Work newWork, Collection<? super Work> follow) {
            boolean b;
            boolean bl = b = newWork instanceof RootsWork && this.useInitialState;
            if (b && LOGGER.isLoggable(Level.FINE)) {
                LOGGER.fine("Cancelling " + this + ", because of " + newWork);
            }
            return b;
        }

        @Override
        public boolean absorb(Work newWork) {
            if (newWork.getClass().equals(RootsWork.class)) {
                RootsWork rw = (RootsWork)newWork;
                if (!rw.useInitialState) {
                    this.useInitialState = rw.useInitialState;
                    LOGGER.log(Level.FINE, "Absorbing {0}, updating useInitialState to {1}", new Object[]{rw, this.useInitialState});
                }
                if (rw.refreshNonExistentDeps) {
                    this.refreshNonExistentDeps = rw.refreshNonExistentDeps;
                }
                return true;
            }
            return false;
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private void notifyRootsRemoved(@NonNull Collection<? extends URL> binaries, @NonNull Collection<? extends URL> sources, @NonNull Collection<? extends URL> unknown) {
            if (!binaries.isEmpty()) {
                Collection binFactories = MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookupAll(BinaryIndexerFactory.class);
                Collection<? extends URL> roots = Collections.unmodifiableCollection(binaries);
                for (BinaryIndexerFactory binFactory : binFactories) {
                    binFactory.rootsRemoved(roots);
                }
                RepositoryUpdater.getDefault().rootsListeners.remove(binaries, false);
            }
            if (!sources.isEmpty() || !unknown.isEmpty()) {
                ProxyIterable roots = new ProxyIterable(Arrays.asList(sources, unknown));
                Collection<IndexerCache.IndexerInfo<CustomIndexerFactory>> customIndexers = IndexerCache.getCifCache().getIndexers(null);
                for (IndexerCache.IndexerInfo customIndexer : customIndexers) {
                    ((CustomIndexerFactory)customIndexer.getIndexerFactory()).rootsRemoved(roots);
                }
                Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> embeddingIndexers = IndexerCache.getEifCache().getIndexers(null);
                for (IndexerCache.IndexerInfo<EmbeddingIndexerFactory> embeddingIndexer : embeddingIndexers) {
                    embeddingIndexer.getIndexerFactory().rootsRemoved(roots);
                }
                RepositoryUpdater.getDefault().rootsListeners.remove(sources, true);
            }
        }

        private static <A, B> void diff(Map<A, B> oldMap, Map<A, B> newMap, Map<A, B> addedOrChangedEntries, Map<A, B> removedEntries) {
            for (A key2 : oldMap.keySet()) {
                if (!newMap.containsKey(key2)) {
                    removedEntries.put(key2, oldMap.get(key2));
                    continue;
                }
                if (org.openide.util.Utilities.compareObjects(oldMap.get(key2), newMap.get(key2))) continue;
                addedOrChangedEntries.put(key2, newMap.get(key2));
            }
            for (A key2 : newMap.keySet()) {
                if (oldMap.containsKey(key2)) continue;
                addedOrChangedEntries.put(key2, newMap.get(key2));
            }
        }
    }

    static final class RefreshWork
    extends AbstractRootsWork {
        private final Map<URL, List<URL>> scannedRoots2Dependencies;
        private final Map<URL, List<URL>> scannedBinaries2InvDependencies;
        private final Map<URL, List<URL>> scannedRoots2Peers;
        private final Set<URL> sourcesForBinaryRoots;
        private final Set<Pair<Object, Boolean>> suspectFilesOrFileObjects;
        private final FSRefreshInterceptor interceptor;
        private DependenciesContext depCtx;
        private Map<URL, Set<FileObject>> fullRescanFiles;
        private Map<URL, Set<FileObject>> checkTimestampFiles;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public RefreshWork(Map<URL, List<URL>> scannedRoots2Depencencies, Map<URL, List<URL>> scannedBinaries2InvDependencies, Map<URL, List<URL>> scannedRoots2Peers, Set<URL> sourcesForBinaryRoots, boolean fullRescan, boolean logStatistics, Collection<? extends Object> suspectFilesOrFileObjects, FSRefreshInterceptor interceptor, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(logStatistics, suspendStatus, logCtx);
            Parameters.notNull((CharSequence)"scannedRoots2Depencencies", scannedRoots2Depencencies);
            Parameters.notNull((CharSequence)"scannedBinaries2InvDependencies", scannedBinaries2InvDependencies);
            Parameters.notNull((CharSequence)"scannedRoots2Peers", scannedRoots2Peers);
            Parameters.notNull((CharSequence)"sourcesForBinaryRoots", sourcesForBinaryRoots);
            Parameters.notNull((CharSequence)"interceptor", (Object)interceptor);
            this.scannedRoots2Dependencies = scannedRoots2Depencencies;
            this.scannedBinaries2InvDependencies = scannedBinaries2InvDependencies;
            this.scannedRoots2Peers = scannedRoots2Peers;
            this.sourcesForBinaryRoots = sourcesForBinaryRoots;
            this.suspectFilesOrFileObjects = new HashSet<Pair<Object, Boolean>>();
            if (suspectFilesOrFileObjects != null) {
                this.addSuspects(suspectFilesOrFileObjects, fullRescan);
            }
            this.interceptor = interceptor;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        protected boolean getDone() {
            if (this.depCtx == null) {
                this.depCtx = new DependenciesContext(this.scannedRoots2Dependencies, this.scannedBinaries2InvDependencies, this.scannedRoots2Peers, this.sourcesForBinaryRoots, false, false);
                if (this.suspectFilesOrFileObjects.isEmpty()) {
                    this.depCtx.newBinariesToScan.addAll(this.scannedBinaries2InvDependencies.keySet());
                    try {
                        this.depCtx.newRootsToScan.addAll(org.openide.util.Utilities.topologicalSort(this.scannedRoots2Dependencies.keySet(), this.scannedRoots2Dependencies));
                    }
                    catch (TopologicalSortException tse) {
                        LOGGER.log(Level.INFO, "Cycles detected in classpath roots dependencies, using partial ordering", (Throwable)tse);
                        List partialSort = tse.partialSort();
                        this.depCtx.newRootsToScan.addAll(partialSort);
                    }
                    Collections.reverse(this.depCtx.newRootsToScan);
                } else {
                    FileObject rootFo;
                    HashSet<Pair> suspects = new HashSet<Pair>();
                    for (Pair<Object, Boolean> fileOrFileObject : this.suspectFilesOrFileObjects) {
                        Pair fileObject = null;
                        if (fileOrFileObject.first() instanceof File) {
                            FileObject f;
                            try {
                                f = FileUtil.toFileObject((File)((File)fileOrFileObject.first()));
                            }
                            catch (IllegalArgumentException e) {
                                throw new IllegalArgumentException("Non-normalized file among files to rescan.", e){};
                            }
                            if (f != null) {
                                fileObject = Pair.of((Object)f, (Object)fileOrFileObject.second());
                            }
                        } else if (fileOrFileObject.first() instanceof FileObject) {
                            fileObject = Pair.of((Object)((FileObject)fileOrFileObject.first()), (Object)fileOrFileObject.second());
                        } else {
                            LOGGER.fine("Not File or FileObject, ignoring: " + fileOrFileObject);
                        }
                        if (fileObject == null) continue;
                        suspects.add(fileObject);
                    }
                    block10 : for (Pair f4 : suspects) {
                        for (URL root3 : this.scannedBinaries2InvDependencies.keySet()) {
                            FileObject rootFo2;
                            File rootFile = FileUtil.archiveOrDirForURL((URL)root3);
                            if (rootFile != null && (rootFo2 = FileUtil.toFileObject((File)rootFile)) != null && (f4.first() == rootFo2 || FileUtil.isParentOf((FileObject)((FileObject)f4.first()), (FileObject)rootFo2))) {
                                this.depCtx.newBinariesToScan.add(root3);
                                continue block10;
                            }
                            FileObject rootFo3 = URLCache.getInstance().findFileObject(root3, true);
                            if (rootFo3 == null || f4.first() != rootFo3 && !FileUtil.isParentOf((FileObject)rootFo3, (FileObject)((FileObject)f4.first()))) continue;
                            this.depCtx.newBinariesToScan.add(root3);
                            continue block10;
                        }
                    }
                    HashSet<Pair> containers = new HashSet<Pair>();
                    HashMap<URL, Pair> sourceRootsToScan = new HashMap<URL, Pair>();
                    for (URL root : this.scannedRoots2Dependencies.keySet()) {
                        rootFo = URLCache.getInstance().findFileObject(root, true);
                        if (rootFo == null) continue;
                        for (Pair f2 : suspects) {
                            if (f2.first() != rootFo && !FileUtil.isParentOf((FileObject)((FileObject)f2.first()), (FileObject)rootFo)) continue;
                            Pair pair = (Pair)sourceRootsToScan.get(root);
                            pair = pair == null ? Pair.of((Object)rootFo, (Object)f2.second()) : Pair.of((Object)rootFo, (Object)((Boolean)pair.second() != false || (Boolean)f2.second() != false));
                            sourceRootsToScan.put(root, pair);
                            containers.add(f2);
                        }
                    }
                    suspects.removeAll(containers);
                    for (Map.Entry entry2 : sourceRootsToScan.entrySet()) {
                        Iterator it = suspects.iterator();
                        while (it.hasNext()) {
                            Pair f3 = (Pair)it.next();
                            Pair root2 = (Pair)entry2.getValue();
                            if (!FileUtil.isParentOf((FileObject)((FileObject)root2.first()), (FileObject)((FileObject)f3.first())) || !((Boolean)root2.second()).booleanValue() && ((Boolean)f3.second()).booleanValue()) continue;
                            it.remove();
                        }
                    }
                    for (Map.Entry entry : sourceRootsToScan.entrySet()) {
                        this.depCtx.newRootsToScan.add(entry.getKey());
                        if (!((Boolean)((Pair)entry.getValue()).second()).booleanValue()) continue;
                        this.depCtx.fullRescanSourceRoots.add(entry.getKey());
                    }
                    this.fullRescanFiles = new HashMap<URL, Set<FileObject>>();
                    this.checkTimestampFiles = new HashMap<URL, Set<FileObject>>();
                    block17 : for (Pair f : suspects) {
                        for (URL root2 : this.scannedRoots2Dependencies.keySet()) {
                            rootFo = URLCache.getInstance().findFileObject(root2, true);
                            if (rootFo == null || f.first() != rootFo && !FileUtil.isParentOf((FileObject)rootFo, (FileObject)((FileObject)f.first()))) continue;
                            Map<URL, Set<FileObject>> map = (Boolean)f.second() != false ? this.fullRescanFiles : this.checkTimestampFiles;
                            Set<FileObject> files = map.get(root2);
                            if (files == null) {
                                files = new HashSet<FileObject>();
                                map.put(root2, files);
                            }
                            files.add((FileObject)f.first());
                            continue block17;
                        }
                    }
                }
                FileSystem.AtomicAction aa = new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        FileUtil.refreshFor((File[])File.listRoots());
                    }
                };
                this.interceptor.setIgnoreFsEvents(true);
                try {
                    FileUtil.runAtomicAction((FileSystem.AtomicAction)aa);
                }
                catch (IOException ex) {
                    LOGGER.log(Level.WARNING, null, ex);
                }
                finally {
                    this.interceptor.setIgnoreFsEvents(false);
                }
            }
            this.depCtx.newRootsToScan.removeAll(this.depCtx.scannedRoots);
            this.depCtx.scannedRoots.clear();
            this.depCtx.newBinariesToScan.removeAll(this.depCtx.scannedBinaries);
            this.depCtx.scannedBinaries.clear();
            boolean finished = this.scanBinaries(this.depCtx);
            if (finished && (finished = this.scanSources(this.depCtx, null, null)) && (finished = this.scanRootFiles(this.fullRescanFiles))) {
                finished = this.scanRootFiles(this.checkTimestampFiles);
            }
            Level logLevel = Level.FINE;
            if (LOGGER.isLoggable(logLevel)) {
                LOGGER.log(logLevel, this + " " + (this.getCancelRequest().isRaised() ? "cancelled" : "finished") + ": {");
                LOGGER.log(logLevel, "  scannedRoots2Dependencies(" + this.scannedRoots2Dependencies.size() + ")=");
                RepositoryUpdater.printMap(this.scannedRoots2Dependencies, logLevel);
                LOGGER.log(logLevel, "  scannedBinaries(" + this.scannedBinaries2InvDependencies.size() + ")=");
                RepositoryUpdater.printCollection(this.scannedBinaries2InvDependencies.keySet(), logLevel);
                LOGGER.log(logLevel, "} ====");
            }
            this.refreshActiveDocument();
            return finished;
        }

        @Override
        public boolean absorb(Work newWork) {
            if (newWork instanceof RefreshWork) {
                this.suspectFilesOrFileObjects.addAll(((RefreshWork)newWork).suspectFilesOrFileObjects);
                return true;
            }
            if (newWork instanceof FileListWork) {
                FileListWork flw = (FileListWork)newWork;
                if (flw.files.isEmpty()) {
                    this.suspectFilesOrFileObjects.add((Pair)Pair.of((Object)URLCache.getInstance().findFileObject(flw.root, false), (Object)flw.forceRefresh));
                } else {
                    this.addSuspects(flw.files, flw.forceRefresh);
                }
                return true;
            }
            if (newWork instanceof DeleteWork) {
                this.suspectFilesOrFileObjects.add((Pair)Pair.of((Object)URLCache.getInstance().findFileObject(((DeleteWork)newWork).root, false), (Object)false));
                return true;
            }
            return false;
        }

        public void addSuspects(Collection<? extends Object> filesOrFolders, boolean fullRescan) {
            for (Object o : filesOrFolders) {
                this.suspectFilesOrFileObjects.add((Pair)Pair.of((Object)o, (Object)fullRescan));
            }
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private boolean scanRootFiles(Map<URL, Set<FileObject>> files) {
            if (files != null && files.size() > 0) {
                Iterator<Map.Entry<URL, Set<FileObject>>> it = files.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<URL, Set<FileObject>> entry = it.next();
                    URL root = entry.getKey();
                    if (this.scanFiles(root, (Collection)entry.getValue(), true, this.sourcesForBinaryRoots.contains(root))) {
                        it.remove();
                        continue;
                    }
                    return false;
                }
            }
            return true;
        }

        @Override
        public String toString() {
            return super.toString() + ", suspectFilesOrFileObjects=" + this.suspectFilesOrFileObjects;
        }

    }

    private static class RefreshEifIndices
    extends Work {
        private final Collection<? extends IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos;
        private final Map<URL, List<URL>> scannedRoots2Dependencies;
        private final Set<URL> sourcesForBinaryRoots;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public RefreshEifIndices(Collection<? extends IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos, Map<URL, List<URL>> scannedRoots2Depencencies, Set<URL> sourcesForBinaryRoots, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(false, false, NbBundle.getMessage(RepositoryUpdater.class, (String)"MSG_RefreshingIndices"), true, suspendStatus, logCtx);
            if (eifInfos == null) {
                throw new IllegalArgumentException("eifInfos must not be null");
            }
            this.eifInfos = eifInfos;
            this.scannedRoots2Dependencies = scannedRoots2Depencencies;
            this.sourcesForBinaryRoots = sourcesForBinaryRoots;
        }

        @Override
        protected boolean isCancelledBy(Work newWork, Collection<? super Work> follow) {
            boolean b = newWork instanceof RootsWork;
            if (b) {
                follow.add(new RefreshEifIndices(this.eifInfos, this.scannedRoots2Dependencies, this.sourcesForBinaryRoots, this.getSuspendStatus(), LogContext.createAndAbsorb(this.getLogContext())));
                LOGGER.log(Level.FINE, "Cancelling {0}, because of {1}", new Object[]{this, newWork});
            }
            if (newWork instanceof RefreshEifIndices) {
                boolean b2 = ((RefreshEifIndices)newWork).eifInfos.containsAll(this.eifInfos);
                if (b2) {
                    LOGGER.log(Level.FINE, "Cancelling {0}, because of {1}", new Object[]{this, newWork});
                }
                b |= b2;
            }
            return b;
        }

        @Override
        public boolean absorb(Work newWork) {
            if (newWork instanceof RefreshEifIndices && this.eifInfos.containsAll(((RefreshEifIndices)newWork).eifInfos)) {
                LOGGER.log(Level.FINE, "Absorbing {0}", newWork);
                return true;
            }
            return false;
        }

        /*
         * Exception decompiling
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        protected boolean getDone() {
            // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
            // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [0[TRYBLOCK]], but top level block is 2[TRYBLOCK]
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
            // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
            // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
            // org.benf.cfr.reader.entities.ClassFile.analyseInnerClassesPass1(ClassFile.java:681)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:764)
            // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
            // org.benf.cfr.reader.Main.doJar(Main.java:134)
            // org.benf.cfr.reader.Main.main(Main.java:189)
            throw new IllegalStateException("Decompilation failed");
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator<? extends IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> it = this.eifInfos.iterator();
            while (it.hasNext()) {
                IndexerCache.IndexerInfo<EmbeddingIndexerFactory> eifInfo = it.next();
                sb.append(" indexer=").append(eifInfo.getIndexerName()).append('/').append(eifInfo.getIndexerVersion());
                sb.append(" (");
                RepositoryUpdater.printMimeTypes(eifInfo.getMimeTypes(), sb);
                sb.append(')');
                if (!it.hasNext()) continue;
                sb.append(',');
            }
            return super.toString() + sb.toString();
        }
    }

    private static class RefreshCifIndices
    extends Work {
        private final Collection<? extends IndexerCache.IndexerInfo<CustomIndexerFactory>> cifInfos;
        private final Map<URL, List<URL>> scannedRoots2Dependencies;
        private final Set<URL> sourcesForBinaryRoots;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public RefreshCifIndices(Collection<? extends IndexerCache.IndexerInfo<CustomIndexerFactory>> cifInfos, Map<URL, List<URL>> scannedRoots2Depencencies, Set<URL> sourcesForBinaryRoots, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(false, false, NbBundle.getMessage(RepositoryUpdater.class, (String)"MSG_RefreshingIndices"), true, suspendStatus, logCtx);
            this.cifInfos = cifInfos;
            this.scannedRoots2Dependencies = scannedRoots2Depencencies;
            this.sourcesForBinaryRoots = sourcesForBinaryRoots;
        }

        @Override
        public boolean absorb(Work newWork) {
            if (newWork instanceof RefreshCifIndices && this.cifInfos.equals(((RefreshCifIndices)newWork).cifInfos)) {
                LOGGER.log(Level.FINE, "Absorbing {0}", newWork);
                return true;
            }
            return false;
        }

        @Override
        protected boolean isCancelledBy(Work newWork, Collection<? super Work> follow) {
            boolean b = newWork instanceof RootsWork;
            if (b) {
                follow.add(new RefreshCifIndices(this.cifInfos, this.scannedRoots2Dependencies, this.sourcesForBinaryRoots, this.getSuspendStatus(), LogContext.createAndAbsorb(this.getLogContext())));
                LOGGER.log(Level.FINE, "Cancelling {0}, because of {1}", new Object[]{this, newWork});
            }
            return b;
        }

        /*
         * Exception decompiling
         */
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        @Override
        protected boolean getDone() {
            // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
            // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [0[TRYBLOCK]], but top level block is 2[TRYBLOCK]
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
            // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
            // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
            // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
            // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
            // org.benf.cfr.reader.entities.ClassFile.analyseInnerClassesPass1(ClassFile.java:681)
            // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:764)
            // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
            // org.benf.cfr.reader.Main.doJar(Main.java:134)
            // org.benf.cfr.reader.Main.main(Main.java:189)
            throw new IllegalStateException("Decompilation failed");
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator<? extends IndexerCache.IndexerInfo<CustomIndexerFactory>> it = this.cifInfos.iterator();
            while (it.hasNext()) {
                IndexerCache.IndexerInfo<CustomIndexerFactory> cifInfo = it.next();
                sb.append(" indexer=").append(cifInfo.getIndexerName()).append('/').append(cifInfo.getIndexerVersion());
                sb.append(" (");
                RepositoryUpdater.printMimeTypes(cifInfo.getMimeTypes(), sb);
                sb.append(')');
                if (!it.hasNext()) continue;
                sb.append(',');
            }
            return super.toString() + sb.toString();
        }
    }

    private static final class DeleteWork
    extends Work {
        private final URL root;
        private final Set<String> relativePaths = new HashSet<String>();

        public DeleteWork(URL root, Set<String> relativePaths, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(false, false, false, true, suspendStatus, logCtx);
            Parameters.notNull((CharSequence)"root", (Object)root);
            Parameters.notNull((CharSequence)"relativePath", relativePaths);
            this.root = root;
            this.relativePaths.addAll(relativePaths);
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.fine("DeleteWork@" + Integer.toHexString(System.identityHashCode(this)) + ": root=" + root + ", files=" + relativePaths);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean getDone() {
            LogContext lctx = this.getLogContext();
            if (lctx != null) {
                lctx.noteRootScanning(this.root, false);
            }
            try {
                boolean finished;
                ArrayList<Indexable> indexables = new ArrayList<Indexable>();
                for (String path : this.relativePaths) {
                    indexables.add(SPIAccessor.getInstance().create(new DeletedIndexable(this.root, path)));
                }
                HashMap<SourceIndexerFactory, Boolean> votes = new HashMap<SourceIndexerFactory, Boolean>();
                HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> contexts = new HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>>();
                Work.UsedIndexables usedIterables = new Work.UsedIndexables();
                SourceIndexers indexers = SourceIndexers.load(false);
                TimeStamps ts = TimeStamps.forRoot(this.root, false);
                try {
                    this.scanStarted(this.root, false, indexers, votes, contexts);
                    this.delete(indexables, contexts, usedIterables);
                    ts.remove(this.relativePaths);
                    boolean bl = finished = !this.getCancelRequest().isRaised();
                }
                catch (Throwable var9_10) {
                    boolean finished2;
                    boolean bl = finished2 = !this.getCancelRequest().isRaised();
                    if (finished2) {
                        ts.store();
                        this.scanFinished(contexts.values(), usedIterables, finished2);
                    }
                    throw var9_10;
                }
                if (finished) {
                    ts.store();
                    this.scanFinished(contexts.values(), usedIterables, finished);
                }
                TEST_LOGGER.log(Level.FINEST, "delete");
            }
            catch (IOException ioe) {
                LOGGER.log(Level.WARNING, null, ioe);
            }
            finally {
                if (lctx != null) {
                    lctx.finishScannedRoot(this.root);
                }
            }
            return true;
        }

        @SuppressWarnings(value={"DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
        @Override
        public boolean absorb(Work newWork) {
            if (newWork instanceof DeleteWork) {
                DeleteWork ndw = (DeleteWork)newWork;
                if (ndw.root.equals(this.root)) {
                    this.relativePaths.addAll(ndw.relativePaths);
                    if (LOGGER.isLoggable(Level.FINE)) {
                        LOGGER.fine(this + ", root=" + this.root + " absorbed: " + ndw.relativePaths);
                    }
                    return true;
                }
            }
            return false;
        }
    }

    private static final class BinaryWork
    extends AbstractRootsWork {
        private final URL root;

        public BinaryWork(URL root, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(false, suspendStatus, logCtx);
            this.root = root;
        }

        @Override
        protected boolean getDone() {
            boolean result = this.scanBinary(this.root, BinaryIndexers.load(), null);
            TEST_LOGGER.log(Level.FINEST, "binary", Collections.singleton(this.root));
            this.refreshActiveDocument();
            return result;
        }

        @SuppressWarnings(value={"DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
        @Override
        public boolean absorb(Work newWork) {
            if (newWork instanceof BinaryWork) {
                return this.root.equals(((BinaryWork)newWork).root);
            }
            return false;
        }
    }

    static final class FileListWork
    extends Work {
        private final URL root;
        private final Collection<FileObject> files = new HashSet<FileObject>();
        private final boolean forceRefresh;
        private final boolean sourceForBinaryRoot;
        private final Map<URL, List<URL>> scannedRoots2Depencencies;

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public FileListWork(Map<URL, List<URL>> scannedRoots2Depencencies, URL root, boolean followUpJob, boolean checkEditor, boolean forceRefresh, boolean sourceForBinaryRoot, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(followUpJob, checkEditor, true, true, suspendStatus, logCtx);
            assert (root != null);
            this.root = root;
            this.forceRefresh = forceRefresh;
            this.sourceForBinaryRoot = sourceForBinaryRoot;
            this.scannedRoots2Depencencies = scannedRoots2Depencencies;
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        public FileListWork(Map<URL, List<URL>> scannedRoots2Depencencies, URL root, Collection<FileObject> files, boolean followUpJob, boolean checkEditor, boolean forceRefresh, boolean sourceForBinaryRoot, boolean steady, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            super(followUpJob, checkEditor, followUpJob, steady, suspendStatus, logCtx);
            assert (root != null);
            assert (files != null && files.size() > 0);
            this.root = root;
            this.files.addAll(files);
            this.forceRefresh = forceRefresh;
            this.sourceForBinaryRoot = sourceForBinaryRoot;
            this.scannedRoots2Depencencies = scannedRoots2Depencencies;
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.fine("FileListWork@" + Integer.toHexString(System.identityHashCode(this)) + ": root=" + root + ", file=" + files);
            }
        }

        public void addFile(FileObject f) {
            assert (f != null);
            assert (FileUtil.isParentOf((FileObject)URLMapper.findFileObject((URL)this.root), (FileObject)f));
            this.files.add(f);
        }

        @Override
        protected boolean getDone() {
            this.updateProgress(this.root, false);
            if (this.scanFiles(this.root, this.files, this.forceRefresh, this.sourceForBinaryRoot)) {
                if (!this.files.isEmpty()) {
                    Map f2d = RepositoryUpdater.getEditorFiles();
                    for (FileObject f : this.files) {
                        Document d = (Document)f2d.get((Object)f);
                        if (d == null) continue;
                        long version = DocumentUtilities.getDocumentVersion((Document)d);
                        d.putProperty(PROP_LAST_INDEXED_VERSION, version);
                        d.putProperty(PROP_LAST_DIRTY_VERSION, null);
                    }
                }
                if (!this.scannedRoots2Depencencies.containsKey(this.root)) {
                    this.scannedRoots2Depencencies.put(this.root, RepositoryUpdater.UNKNOWN_ROOT);
                }
            }
            TEST_LOGGER.log(Level.FINEST, "filelist");
            this.refreshActiveDocument();
            return true;
        }

        @SuppressWarnings(value={"DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
        @Override
        public boolean absorb(Work newWork) {
            if (newWork instanceof FileListWork) {
                FileListWork nflw = (FileListWork)newWork;
                if (nflw.root.equals(this.root) && nflw.isFollowUpJob() == this.isFollowUpJob() && nflw.hasToCheckEditor() == this.hasToCheckEditor()) {
                    this.files.addAll(nflw.files);
                    if (LOGGER.isLoggable(Level.FINE)) {
                        LOGGER.fine(this + ", root=" + this.root + " absorbed: " + nflw.files);
                    }
                    return true;
                }
            }
            return false;
        }

        @Override
        protected void refreshActiveDocument() {
            if (this.shouldRefresh()) {
                super.refreshActiveDocument();
            }
        }

        @Override
        protected void invalidateSources(Iterable<? extends Indexable> toInvalidate) {
            if (this.shouldRefresh()) {
                super.invalidateSources(toInvalidate);
            }
        }

        private boolean shouldRefresh() {
            return !TransientUpdateSupport.isTransientUpdate();
        }
    }

    static abstract class Work {
        private static long lastScanEnded = -1;
        private final AtomicBoolean cancelled = new AtomicBoolean(false);
        private final AtomicBoolean finished = new AtomicBoolean(false);
        private final AtomicBoolean externalCancel = new AtomicBoolean(false);
        private final boolean followUpJob;
        private final boolean checkEditor;
        private final boolean steady;
        private final CountDownLatch latch = new CountDownLatch(1);
        private final CancelRequestImpl cancelRequest = new CancelRequestImpl(this.cancelled);
        private final String progressTitle;
        private final SuspendStatus suspendStatus;
        private volatile LogContext logCtx;
        private final Object progressLock = new Object();
        private ProgressHandle progressHandle = null;
        private int progress = -1;
        private final Map<String, int[]> indexerStatistics = Collections.synchronizedMap(new HashMap());
        private volatile boolean reportIndexerStatistics;
        protected int modifiedResourceCount;
        protected int allResourceCount;

        protected Work(boolean followUpJob, boolean checkEditor, boolean supportsProgress, boolean steady, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            this(followUpJob, checkEditor, supportsProgress ? NbBundle.getMessage(RepositoryUpdater.class, (String)"MSG_BackgroundCompileStart") : null, steady, suspendStatus, logCtx);
        }

        protected Work(boolean followUpJob, boolean checkEditor, String progressTitle, boolean steady, @NonNull SuspendStatus suspendStatus, @NullAllowed LogContext logCtx) {
            assert (suspendStatus != null);
            this.followUpJob = followUpJob;
            this.checkEditor = checkEditor;
            this.progressTitle = progressTitle;
            this.steady = steady;
            this.suspendStatus = suspendStatus;
            this.logCtx = logCtx;
        }

        @CheckForNull
        protected final LogContext getLogContext() {
            return this.logCtx;
        }

        protected final boolean isFollowUpJob() {
            return this.followUpJob;
        }

        protected final boolean hasToCheckEditor() {
            return this.checkEditor;
        }

        protected final boolean isSteady() {
            return this.steady;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void updateProgress(String message) {
            assert (message != null);
            Object object = this.progressLock;
            synchronized (object) {
                if (this.progressHandle == null) {
                    return;
                }
                this.progressHandle.progress(message);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void updateProgress(URL currentlyScannedRoot, boolean increment) {
            assert (currentlyScannedRoot != null);
            Object object = this.progressLock;
            synchronized (object) {
                if (this.progressHandle == null) {
                    return;
                }
                if (increment && this.progress != -1) {
                    this.progressHandle.progress(this.urlForMessage(currentlyScannedRoot), ++this.progress);
                } else {
                    this.progressHandle.progress(this.urlForMessage(currentlyScannedRoot));
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void switchProgressToDeterminate(int workunits) {
            Object object = this.progressLock;
            synchronized (object) {
                if (this.progressHandle == null) {
                    return;
                }
                this.progress = 0;
                this.progressHandle.switchToDeterminate(workunits);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void suspendProgress(@NonNull String message) {
            Object object = this.progressLock;
            synchronized (object) {
                if (this.progressHandle == null) {
                    return;
                }
                this.progressHandle.suspend(message);
            }
        }

        private Preferences indexerProfileNode(SourceIndexerFactory srcFactory) {
            String nn = srcFactory.getIndexerName();
            if (nn.length() >= 80) {
                int i = nn.lastIndexOf(46);
                if (i >= 0) {
                    nn = nn.substring(i + 1);
                }
                if (nn.length() < 3 || nn.length() >= 80) {
                    String hashCode = Integer.toHexString(nn.hashCode());
                    nn = srcFactory.getClass().getSimpleName() + "_" + hashCode;
                }
            }
            return NbPreferences.forModule(srcFactory.getClass()).node("RepositoryUpdater").node(nn);
        }

        private int estimateEmbeddingIndexer(SourceIndexerFactory srcFactory) {
            Preferences pref = this.indexerProfileNode(srcFactory);
            int c1 = pref.getInt("modifiedScanTime", 500) + pref.getInt("modifiedBaseTime", 100);
            return this.modifiedResourceCount * c1;
        }

        private int estimateCustomStartTime(CustomIndexerFactory srcFactory) {
            if (this.modifiedResourceCount == 0 && this.allResourceCount == 0) {
                return -1;
            }
            Preferences moduleNode = this.indexerProfileNode(srcFactory);
            int c1 = moduleNode.getInt("modifiedStartTime", 500);
            int c2 = moduleNode.getInt("fileStartTime", 300);
            int c3 = moduleNode.getInt("startBaseTime", 300);
            moduleNode.putBoolean("hello", true);
            int threshold = Math.max(this.modifiedResourceCount * c1, this.allResourceCount * c2) + c3;
            return threshold;
        }

        private int estimateCustomIndexingTime(CustomIndexerFactory srcFactory) {
            if (this.modifiedResourceCount == 0 && this.allResourceCount == 0) {
                return -1;
            }
            Preferences moduleNode = this.indexerProfileNode(srcFactory);
            int c1 = moduleNode.getInt("modifiedScanTime", 500);
            int c2 = moduleNode.getInt("fileScanTime", 300);
            int c3 = moduleNode.getInt("indexingBaseTime", 300);
            int threshold = Math.max(this.modifiedResourceCount * c1, this.allResourceCount * c2) + c3;
            return threshold;
        }

        private int estimateSourceEndTime(SourceIndexerFactory srcFactory) {
            if (this.modifiedResourceCount == 0 && this.allResourceCount == 0) {
                return -1;
            }
            Preferences moduleNode = this.indexerProfileNode(srcFactory);
            int c1 = moduleNode.getInt("modifiedEndTime", 500);
            int c2 = moduleNode.getInt("fileEndTime", 300);
            int c3 = moduleNode.getInt("indexingEndTime", 300);
            int threshold = Math.max(this.modifiedResourceCount * c1, this.allResourceCount * c2) + c3;
            return threshold;
        }

        protected final void scanStarted(URL root, boolean sourceForBinaryRoot, SourceIndexers indexers, Map<SourceIndexerFactory, Boolean> votes, Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> ctxToFinish) throws IOException {
            FileObject cacheRoot = CacheFolder.getDataFolder(root);
            this.customIndexersScanStarted(root, cacheRoot, sourceForBinaryRoot, indexers.cifInfos, votes, ctxToFinish);
            this.embeddingIndexersScanStarted(root, cacheRoot, sourceForBinaryRoot, indexers.eifInfosMap.values(), votes, ctxToFinish);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void customIndexersScanStarted(@NonNull URL root, @NonNull FileObject cacheRoot, boolean sourceForBinaryRoot, Collection<? extends IndexerCache.IndexerInfo<CustomIndexerFactory>> indexers, Map<SourceIndexerFactory, Boolean> votes, Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> ctxToFinish) throws IOException {
            for (IndexerCache.IndexerInfo<CustomIndexerFactory> cifInfo : indexers) {
                this.parkWhileSuspended();
                CustomIndexerFactory factory = cifInfo.getIndexerFactory();
                Pair key = Pair.of((Object)factory.getIndexerName(), (Object)factory.getIndexVersion());
                Pair value = ctxToFinish.get((Object)key);
                if (TEST_LOGGER.isLoggable(Level.FINEST)) {
                    TEST_LOGGER.log(Level.FINEST, "scanStarting:{0}:{1}", new Object[]{factory.getIndexerName(), root.toString()});
                }
                if (value == null) {
                    Context ctx = SPIAccessor.getInstance().createContext(cacheRoot, root, factory.getIndexerName(), factory.getIndexVersion(), null, this.followUpJob, this.checkEditor, sourceForBinaryRoot, this.getSuspendStatus(), this.getCancelRequest(), this.logCtx);
                    value = Pair.of((Object)factory, (Object)ctx);
                    ctxToFinish.put((Pair)key, (Pair)value);
                }
                this.logStartIndexer(factory.getIndexerName());
                try {
                    boolean vote = this.doStartCustomIndexer(factory, (Context)value.second());
                    votes.put(factory, vote);
                    continue;
                }
                catch (Throwable t) {
                    if (t instanceof ThreadDeath) {
                        throw (ThreadDeath)t;
                    }
                    votes.put(factory, false);
                    Exceptions.printStackTrace((Throwable)t);
                    continue;
                }
                finally {
                    this.logFinishIndexer(factory.getIndexerName());
                    continue;
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean doStartCustomIndexer(CustomIndexerFactory factory, Context factoryContext) {
            int estimate = this.estimateCustomStartTime(factory);
            SamplerInvoker.start(this.getLogContext(), factory.getIndexerName(), estimate, factoryContext.getRootURI());
            try {
                boolean bl = factory.scanStarted(factoryContext);
                return bl;
            }
            finally {
                SamplerInvoker.stop();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void embeddingIndexersScanStarted(@NonNull URL root, @NonNull FileObject cacheRoot, boolean sourceForBinaryRoot, Collection<Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>>> indexers, Map<SourceIndexerFactory, Boolean> votes, Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> ctxToFinish) throws IOException {
            for (Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos : indexers) {
                for (IndexerCache.IndexerInfo<EmbeddingIndexerFactory> eifInfo : eifInfos) {
                    this.parkWhileSuspended();
                    EmbeddingIndexerFactory eif = eifInfo.getIndexerFactory();
                    Pair key = Pair.of((Object)eif.getIndexerName(), (Object)eif.getIndexVersion());
                    Pair value = ctxToFinish.get((Object)key);
                    if (value == null) {
                        Context context = SPIAccessor.getInstance().createContext(cacheRoot, root, eif.getIndexerName(), eif.getIndexVersion(), null, this.followUpJob, this.checkEditor, sourceForBinaryRoot, this.getSuspendStatus(), this.getCancelRequest(), this.logCtx);
                        value = Pair.of((Object)eif, (Object)context);
                        ctxToFinish.put((Pair)key, (Pair)value);
                    }
                    this.logStartIndexer(eif.getIndexerName());
                    try {
                        boolean vote = eif.scanStarted((Context)value.second());
                        votes.put(eif, vote);
                        continue;
                    }
                    catch (Throwable t) {
                        if (t instanceof ThreadDeath) {
                            throw (ThreadDeath)t;
                        }
                        votes.put(eif, false);
                        Exceptions.printStackTrace((Throwable)t);
                        continue;
                    }
                    finally {
                        this.logFinishIndexer(eif.getIndexerName());
                        continue;
                    }
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void scanFinished(@NonNull Collection<Pair<SourceIndexerFactory, Context>> ctxToFinish, @NonNull UsedIndexables usedIterables, boolean finished) throws IOException {
            try {
                for (Pair<SourceIndexerFactory, Context> entry : ctxToFinish) {
                    this.parkWhileSuspended();
                    if (TEST_LOGGER.isLoggable(Level.FINEST)) {
                        TEST_LOGGER.log(Level.FINEST, "scanFinishing:{0}:{1}", new Object[]{((SourceIndexerFactory)entry.first()).getIndexerName(), ((Context)entry.second()).getRootURI().toExternalForm()});
                    }
                    this.logStartIndexer(((SourceIndexerFactory)entry.first()).getIndexerName());
                    SPIAccessor.getInstance().putProperty((Context)entry.second(), "ci-delete-set", null);
                    SPIAccessor.getInstance().putProperty((Context)entry.second(), "ci-index-set", null);
                    this.cancelRequest.setResult(finished);
                    try {
                        int estimate = this.estimateSourceEndTime((SourceIndexerFactory)entry.first());
                        SamplerInvoker.start(this.getLogContext(), ((SourceIndexerFactory)entry.first()).getIndexerName(), estimate, ((Context)entry.second()).getRootURI());
                        ((SourceIndexerFactory)entry.first()).scanFinished((Context)entry.second());
                    }
                    catch (Throwable t) {
                        if (t instanceof ThreadDeath) {
                            throw (ThreadDeath)t;
                        }
                        Exceptions.printStackTrace((Throwable)t);
                        SamplerInvoker.stop();
                    }
                    finally {
                        this.cancelRequest.setResult(null);
                    }
                    this.logFinishIndexer(((SourceIndexerFactory)entry.first()).getIndexerName());
                    if (!TEST_LOGGER.isLoggable(Level.FINEST)) continue;
                    TEST_LOGGER.log(Level.FINEST, "scanFinished:{0}:{1}", new Object[]{((SourceIndexerFactory)entry.first()).getIndexerName(), ((Context)entry.second()).getRootURI().toExternalForm()});
                }
            }
            finally {
                try {
                    boolean indexOk = true;
                    Union2 exception = null;
                    for (Pair<SourceIndexerFactory, Context> entry : ctxToFinish) {
                        try {
                            indexOk &= this.storeChanges(((SourceIndexerFactory)entry.first()).getIndexerName(), (Context)entry.second(), this.isSteady(), usedIterables.get(), finished);
                        }
                        catch (IOException e) {
                            exception = Union2.createFirst((Object)e);
                        }
                        catch (RuntimeException e) {
                            exception = Union2.createSecond((Object)e);
                        }
                    }
                    if (exception != null) {
                        if (exception.hasFirst()) {
                            throw (IOException)exception.first();
                        }
                        throw (RuntimeException)exception.second();
                    }
                    if (!indexOk) {
                        Context ctx = (Context)ctxToFinish.iterator().next().second();
                        RepositoryUpdater.getDefault().addIndexingJob(ctx.getRootURI(), null, false, false, false, true, true, LogContext.create(LogContext.EventType.UI, "Broken Index Found."));
                    }
                }
                finally {
                    InjectedTasksSupport.clear();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void delete(@NonNull List<Indexable> deleted, @NonNull Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> contexts, @NonNull UsedIndexables usedIterables) throws IOException {
            if (deleted == null || deleted.isEmpty()) {
                return;
            }
            ClusteredIndexables ci = new ClusteredIndexables(deleted);
            try {
                for (Pair<SourceIndexerFactory, Context> pair : contexts.values()) {
                    this.parkWhileSuspended();
                    SPIAccessor.getInstance().putProperty((Context)pair.second(), "ci-delete-set", ci);
                    ((SourceIndexerFactory)pair.first()).filesDeleted(ci.getIndexablesFor(null), (Context)pair.second());
                }
            }
            catch (Throwable var10_10) {
                for (Pair<SourceIndexerFactory, Context> pair : contexts.values()) {
                    Context ctx = (Context)pair.second();
                    FileObject indexFolder = ctx.getIndexFolder();
                    if (indexFolder == null) {
                        throw new IllegalStateException(String.format("No index folder for context: %s", ctx));
                    }
                    LayeredDocumentIndex index = SPIAccessor.getInstance().getIndexFactory(ctx).getIndex(indexFolder);
                    if (index == null) continue;
                    usedIterables.offer(ci.getIndexablesFor(null));
                }
                throw var10_10;
            }
            for (Pair<SourceIndexerFactory, Context> pair : contexts.values()) {
                Context ctx = (Context)pair.second();
                FileObject indexFolder = ctx.getIndexFolder();
                if (indexFolder == null) {
                    throw new IllegalStateException(String.format("No index folder for context: %s", ctx));
                }
                LayeredDocumentIndex index = SPIAccessor.getInstance().getIndexFactory(ctx).getIndex(indexFolder);
                if (index == null) continue;
                usedIterables.offer(ci.getIndexablesFor(null));
            }
        }

        protected final boolean index(final List<Indexable> resources, final List<Indexable> allResources, final URL root, final boolean sourceForBinaryRoot, final SourceIndexers indexers, final Map<SourceIndexerFactory, Boolean> votes, final @NonNull Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> contexts, final @NonNull UsedIndexables usedIterables) throws IOException {
            return (Boolean)TaskCache.getDefault().refreshTransaction(new Mutex.ExceptionAction<Boolean>(){

                public Boolean run() throws IOException {
                    return Work.this.doIndex(resources, allResources, root, sourceForBinaryRoot, indexers, votes, contexts, usedIterables);
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private boolean doIndex(List<Indexable> resources, List<Indexable> allResources, URL root, boolean sourceForBinaryRoot, SourceIndexers indexers, Map<SourceIndexerFactory, Boolean> votes, @NonNull Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> contexts, @NonNull UsedIndexables usedIterables) throws IOException {
            LinkedList allIndexblesSentToIndexers = new LinkedList();
            SourceAccessor.getINSTANCE().suppressListening(true, !this.checkEditor);
            try {
                Object factory;
                FileObject cacheRoot = CacheFolder.getDataFolder(root);
                ClusteredIndexables ci = new ClusteredIndexables(resources);
                ClusteredIndexables allCi = null;
                boolean ae = false;
                if (!$assertionsDisabled) {
                    ae = true;
                    if (!true) {
                        throw new AssertionError();
                    }
                }
                for (IndexerCache.IndexerInfo<CustomIndexerFactory> cifInfo : indexers.cifInfos) {
                    LogContext lc;
                    boolean allFiles;
                    Set<String> rootMimeTypes = PathRegistry.getDefault().getMimeTypesFor(root);
                    if (rootMimeTypes != null && !cifInfo.isAllMimeTypesIndexer() && !Util.containsAny(rootMimeTypes, cifInfo.getMimeTypes())) {
                        if (!LOGGER.isLoggable(Level.FINE)) continue;
                        LOGGER.log(Level.FINE, "Not using {0} registered for {1} to scan root {2} marked for {3}", new Object[]{cifInfo.getIndexerFactory().getIndexerName() + "/" + cifInfo.getIndexerFactory().getIndexVersion(), RepositoryUpdater.printMimeTypes(cifInfo.getMimeTypes(), new StringBuilder()), root, PathRegistry.getDefault().getMimeTypesFor(root)});
                        continue;
                    }
                    CustomIndexerFactory factory2 = cifInfo.getIndexerFactory();
                    Pair key = Pair.of((Object)factory2.getIndexerName(), (Object)factory2.getIndexVersion());
                    Pair value = contexts.get((Object)key);
                    if (value == null) {
                        Context ctx = SPIAccessor.getInstance().createContext(cacheRoot, root, factory2.getIndexerName(), factory2.getIndexVersion(), null, this.followUpJob, this.checkEditor, sourceForBinaryRoot, this.getSuspendStatus(), this.getCancelRequest(), this.logCtx);
                        value = Pair.of((Object)factory2, (Object)ctx);
                        contexts.put((Pair)key, (Pair)value);
                    }
                    boolean cifIsChanged = indexers.changedCifs != null && indexers.changedCifs.contains(cifInfo);
                    boolean forceReindex = votes.get(factory2) == Boolean.FALSE && allResources != null;
                    boolean bl = allFiles = cifIsChanged || forceReindex || allResources != null && allResources.size() == resources.size();
                    if (forceReindex && resources.size() != allResources.size() && (lc = this.getLogContext()) != null) {
                        lc.reindexForced(root, factory2.getIndexerName());
                    }
                    if (ae && forceReindex && LOGGER.isLoggable(Level.INFO) && resources.size() != allResources.size() && !cifInfo.getMimeTypes().isEmpty()) {
                        LOGGER.log(Level.INFO, "Refresh of custom indexer ({0}) for root: {1} forced by: {2}", new Object[]{cifInfo.getMimeTypes(), root.toExternalForm(), factory2});
                    }
                    SPIAccessor.getInstance().setAllFilesJob((Context)value.second(), allFiles);
                    LinkedList<Iterable<Indexable>> indexerIndexablesList = new LinkedList<Iterable<Indexable>>();
                    ClusteredIndexables usedCi = null;
                    for (String mimeType22 : cifInfo.getMimeTypes()) {
                        if ((cifIsChanged || forceReindex) && allResources != null && resources.size() != allResources.size()) {
                            if (allCi == null) {
                                allCi = new ClusteredIndexables(allResources);
                            }
                            indexerIndexablesList.add(allCi.getIndexablesFor(mimeType22));
                            if (usedCi != null) continue;
                            usedCi = allCi;
                            continue;
                        }
                        indexerIndexablesList.add(ci.getIndexablesFor(mimeType22));
                        if (usedCi != null) continue;
                        usedCi = ci;
                    }
                    ProxyIterable indexables = new ProxyIterable(indexerIndexablesList);
                    allIndexblesSentToIndexers.addAll(indexerIndexablesList);
                    this.parkWhileSuspended();
                    if (this.getCancelRequest().isRaised()) {
                        boolean mimeType22 = false;
                        return mimeType22;
                    }
                    CustomIndexer indexer = factory2.createIndexer();
                    long tm1 = -1;
                    long tm2 = -1;
                    this.logStartIndexer(factory2.getIndexerName());
                    SPIAccessor.getInstance().putProperty((Context)value.second(), "ci-index-set", usedCi);
                    int estimate = this.estimateCustomIndexingTime(factory2);
                    try {
                        tm1 = System.currentTimeMillis();
                        SamplerInvoker.start(this.getLogContext(), factory2.getIndexerName(), estimate, root);
                        SPIAccessor.getInstance().index(indexer, indexables, (Context)value.second());
                    }
                    catch (ThreadDeath td) {
                        throw td;
                    }
                    catch (Throwable t) {
                        LOGGER.log(Level.WARNING, null, t);
                    }
                    finally {
                        SamplerInvoker.stop();
                    }
                    tm2 = System.currentTimeMillis();
                    this.logIndexerTime(factory2.getIndexerName(), (int)(tm2 - tm1));
                    if (LOGGER.isLoggable(Level.FINE)) {
                        StringBuilder sb = RepositoryUpdater.printMimeTypes(cifInfo.getMimeTypes(), new StringBuilder());
                        LOGGER.fine("Indexing source root " + root + " using " + indexer + "; mimeTypes=" + sb.toString() + "; took " + (tm1 != -1 && tm2 != -1 ? new StringBuilder().append(tm2 - tm1).append("ms").toString() : "unknown time"));
                    }
                    InjectedTasksSupport.execute();
                }
                if (this.getCancelRequest().isRaised()) {
                    boolean i$ = false;
                    return i$;
                }
                boolean useAllCi = false;
                if (allResources != null) {
                    boolean allFiles;
                    boolean containsNewIndexers = false;
                    boolean forceReindex = false;
                    HashSet<EmbeddingIndexerFactory> reindexVoters = new HashSet<EmbeddingIndexerFactory>();
                    LogContext lc = this.getLogContext();
                    for (Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos : indexers.eifInfosMap.values()) {
                        for (IndexerCache.IndexerInfo<EmbeddingIndexerFactory> eifInfo : eifInfos) {
                            EmbeddingIndexerFactory eif;
                            boolean indexerVote;
                            if (indexers.changedEifs != null && indexers.changedEifs.contains(eifInfo)) {
                                if (lc != null) {
                                    lc.newIndexerSeen(eifInfo.getIndexerFactory().getIndexerName());
                                }
                                containsNewIndexers = true;
                            }
                            boolean bl = indexerVote = votes.get(eif = eifInfo.getIndexerFactory()) == Boolean.FALSE;
                            if (indexerVote) {
                                if (lc != null) {
                                    lc.reindexForced(root, eif.getIndexerName());
                                }
                                reindexVoters.add(eif);
                            }
                            forceReindex |= indexerVote;
                        }
                    }
                    if ((containsNewIndexers || forceReindex) && resources.size() != allResources.size()) {
                        if (allCi == null) {
                            allCi = new ClusteredIndexables(allResources);
                        }
                        useAllCi = true;
                        if (ae && !reindexVoters.isEmpty() && LOGGER.isLoggable(Level.INFO)) {
                            LOGGER.log(Level.INFO, "Refresh of embedded indexers for root: {0} forced by: {1}", new Object[]{root.toExternalForm(), reindexVoters.toString()});
                        }
                    }
                    boolean bl = allFiles = containsNewIndexers || forceReindex || allResources.size() == resources.size();
                    if (allFiles) {
                        for (Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> eifInfos2 : indexers.eifInfosMap.values()) {
                            for (IndexerCache.IndexerInfo<EmbeddingIndexerFactory> eifInfo : eifInfos2) {
                                factory = eifInfo.getIndexerFactory();
                                Pair key = Pair.of((Object)factory.getIndexerName(), (Object)factory.getIndexVersion());
                                Pair value = contexts.get((Object)key);
                                if (value == null) {
                                    Context ctx = SPIAccessor.getInstance().createContext(cacheRoot, root, factory.getIndexerName(), factory.getIndexVersion(), null, this.followUpJob, this.checkEditor, sourceForBinaryRoot, this.getSuspendStatus(), this.getCancelRequest(), this.logCtx);
                                    value = Pair.of((Object)factory, (Object)ctx);
                                    contexts.put((Pair)key, (Pair)value);
                                }
                                SPIAccessor.getInstance().setAllFilesJob((Context)value.second(), allFiles);
                            }
                        }
                    }
                }
                for (String mimeType : Util.getAllMimeTypes()) {
                    this.parkWhileSuspended();
                    if (this.getCancelRequest().isRaised()) {
                        boolean reindexVoters = false;
                        return reindexVoters;
                    }
                    if (!Util.canBeParsed(mimeType)) continue;
                    ClusteredIndexables usedCi = useAllCi ? allCi : ci;
                    Iterable<Indexable> indexables = usedCi.getIndexablesFor(mimeType);
                    allIndexblesSentToIndexers.add(indexables);
                    long tm1 = System.currentTimeMillis();
                    boolean f = this.indexEmbedding(indexers.eifInfosMap, cacheRoot, root, indexables, usedCi, contexts, sourceForBinaryRoot);
                    long tm2 = System.currentTimeMillis();
                    if (!f) {
                        factory = false;
                        return (boolean)factory;
                    }
                    if (!LOGGER.isLoggable(Level.FINE)) continue;
                    LOGGER.fine("Indexing " + mimeType + " embeddables under " + root + "; took " + (tm2 - tm1) + "ms");
                }
                boolean i$ = !this.getCancelRequest().isRaised();
                return i$;
            }
            finally {
                SourceAccessor.getINSTANCE().suppressListening(false, false);
                usedIterables.offerAll(allIndexblesSentToIndexers);
            }
        }

        protected void invalidateSources(Iterable<? extends Indexable> toInvalidate) {
            long st = System.currentTimeMillis();
            for (Indexable indexable : toInvalidate) {
                Source src;
                FileObject cheapFo = SPIAccessor.getInstance().getFileObject(indexable);
                if (cheapFo == null || (src = SourceAccessor.getINSTANCE().get(cheapFo)) == null) continue;
                SourceAccessor.getINSTANCE().setFlags(src, EnumSet.of(SourceFlags.INVALID));
            }
            long et = System.currentTimeMillis();
            LOGGER.fine("InvalidateSources took: " + (et - st));
        }

        protected final void binaryScanStarted(@NonNull URL root, boolean upToDate, @NonNull LinkedHashMap<BinaryIndexerFactory, Context> contexts, @NonNull BitSet startedIndexers) throws IOException {
            int index = 0;
            for (Map.Entry<BinaryIndexerFactory, Context> e : contexts.entrySet()) {
                boolean vote;
                Context ctx = e.getValue();
                BinaryIndexerFactory bif = e.getKey();
                SPIAccessor.getInstance().setAllFilesJob(ctx, !upToDate);
                this.parkWhileSuspended();
                long st = System.currentTimeMillis();
                this.logStartIndexer(bif.getIndexerName());
                try {
                    startedIndexers.set(index);
                    vote = bif.scanStarted(ctx);
                }
                catch (Throwable t) {
                    if (t instanceof ThreadDeath) {
                        throw (ThreadDeath)t;
                    }
                    vote = false;
                    Exceptions.printStackTrace((Throwable)t);
                }
                long et = System.currentTimeMillis();
                this.logIndexerTime(bif.getIndexerName(), (int)(et - st));
                if (!vote) {
                    SPIAccessor.getInstance().setAllFilesJob(ctx, true);
                }
                ++index;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final void binaryScanFinished(@NonNull BinaryIndexers indexers, @NonNull LinkedHashMap<BinaryIndexerFactory, Context> contexts, @NonNull BitSet startedIndexers, boolean finished) throws IOException {
            try {
                int index = 0;
                for (Map.Entry<BinaryIndexerFactory, Context> entry : contexts.entrySet()) {
                    if (!startedIndexers.get(index++)) continue;
                    try {
                        long st = System.currentTimeMillis();
                        try {
                            this.parkWhileSuspended();
                            this.logStartIndexer(entry.getKey().getIndexerName());
                            continue;
                        }
                        finally {
                            try {
                                entry.getKey().scanFinished(entry.getValue());
                                continue;
                            }
                            finally {
                                long et = System.currentTimeMillis();
                                this.logIndexerTime(entry.getKey().getIndexerName(), (int)(et - st));
                            }
                        }
                    }
                    catch (Throwable t) {
                        if (t instanceof ThreadDeath) {
                            throw (ThreadDeath)t;
                        }
                        Exceptions.printStackTrace((Throwable)t);
                    }
                }
            }
            finally {
                boolean indexOk = true;
                Union2 exception = null;
                for (Context ctx : contexts.values()) {
                    try {
                        indexOk &= this.storeChanges(null, ctx, this.isSteady(), null, finished);
                    }
                    catch (IOException e) {
                        exception = Union2.createFirst((Object)e);
                    }
                    catch (RuntimeException e) {
                        exception = Union2.createSecond((Object)e);
                    }
                }
                if (exception != null) {
                    if (exception.hasFirst()) {
                        throw (IOException)exception.first();
                    }
                    throw (RuntimeException)exception.second();
                }
                if (!indexOk) {
                    RepositoryUpdater.getDefault().addBinaryJob(contexts.values().iterator().next().getRootURI(), LogContext.create(LogContext.EventType.UI, "Broken Index Found."));
                }
            }
        }

        protected final void createBinaryContexts(@NonNull URL root, @NonNull BinaryIndexers indexers, @NonNull Map<BinaryIndexerFactory, Context> contexts) throws IOException {
            FileObject cacheRoot = CacheFolder.getDataFolder(root);
            for (BinaryIndexerFactory bif : indexers.bifs) {
                Context ctx = SPIAccessor.getInstance().createContext(cacheRoot, root, bif.getIndexerName(), bif.getIndexVersion(), null, false, false, false, this.getSuspendStatus(), this.getCancelRequest(), null);
                contexts.put(bif, ctx);
            }
        }

        protected final boolean checkBinaryIndexers(@NullAllowed Pair<Long, Map<Pair<String, Integer>, Integer>> lastState, @NonNull Map<BinaryIndexerFactory, Context> contexts) throws IOException {
            if (lastState == null || (Long)lastState.first() == 0) {
                return false;
            }
            if (contexts.size() != ((Map)lastState.second()).size()) {
                return false;
            }
            HashMap copy = new HashMap((Map)lastState.second());
            for (Map.Entry<BinaryIndexerFactory, Context> e : contexts.entrySet()) {
                BinaryIndexerFactory bif = e.getKey();
                Integer state = (Integer)copy.remove((Object)Pair.of((Object)bif.getIndexerName(), (Object)bif.getIndexVersion()));
                if (state == null) {
                    return false;
                }
                ArchiveTimeStamps.setIndexerState(e.getValue(), state);
            }
            return copy.isEmpty();
        }

        protected final Pair<Long, Map<Pair<String, Integer>, Integer>> createBinaryIndexersTimeStamp(long currentTimeStamp, @NonNull Map<BinaryIndexerFactory, Context> contexts) {
            HashMap<Pair, Integer> pairs = new HashMap<Pair, Integer>();
            for (Map.Entry<BinaryIndexerFactory, Context> e : contexts.entrySet()) {
                BinaryIndexerFactory bf = e.getKey();
                Context ctx = e.getValue();
                pairs.put(Pair.of((Object)bf.getIndexerName(), (Object)bf.getIndexVersion()), ArchiveTimeStamps.getIndexerState(ctx));
            }
            return Pair.of((Object)currentTimeStamp, pairs);
        }

        protected final boolean indexBinary(URL root, BinaryIndexers indexers, Map<BinaryIndexerFactory, Context> contexts) throws IOException {
            LOGGER.log(Level.FINE, "Scanning binary root: {0}", root);
            if (!RepositoryUpdater.getDefault().rootsListeners.add(root, false, null)) {
                return false;
            }
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.fine("Using BinaryIndexerFactories: " + indexers.bifs);
            }
            for (BinaryIndexerFactory f : indexers.bifs) {
                this.parkWhileSuspended();
                if (this.getCancelRequest().isRaised()) break;
                Context ctx = contexts.get(f);
                assert (ctx != null);
                BinaryIndexer indexer = f.createIndexer();
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.fine("Indexing binary " + root + " using " + indexer);
                }
                long st = System.currentTimeMillis();
                this.logStartIndexer(f.getIndexerName());
                try {
                    SPIAccessor.getInstance().index(indexer, ctx);
                }
                catch (ThreadDeath td) {
                    throw td;
                }
                catch (Throwable t) {
                    LOGGER.log(Level.WARNING, null, t);
                }
                long et = System.currentTimeMillis();
                this.logIndexerTime(f.getIndexerName(), (int)(et - st));
            }
            return !this.getCancelRequest().isRaised();
        }

        protected final boolean indexEmbedding(final Map<String, Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>>> eifInfosMap, final FileObject cache, final URL rootURL, Iterable<? extends Indexable> files, final ClusteredIndexables usedCi, final Map<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> transactionContexts, final boolean sourceForBinaryRoot) throws IOException {
            for (final Indexable dirty : files) {
                this.parkWhileSuspended();
                if (this.getCancelRequest().isRaised()) {
                    return false;
                }
                Collection<IndexerCache.IndexerInfo<EmbeddingIndexerFactory>> infos = eifInfosMap.get(dirty.getMimeType());
                if (infos != null && infos.size() > 0) {
                    FileObject fileObject;
                    final URL url = dirty.getURL();
                    if (url == null || (fileObject = URLMapper.findFileObject((URL)url)) == null) continue;
                    Source src = Source.create(fileObject);
                    try {
                        this.logStartIndexer(src.getMimeType());
                        ParserManager.parse(Collections.singleton(src), new UserTask(){

                            /*
                             * WARNING - Removed try catching itself - possible behaviour change.
                             */
                            @Override
                            public void run(ResultIterator resultIterator) throws Exception {
                                String mimeType = resultIterator.getSnapshot().getMimeType();
                                Collection infos = (Collection)eifInfosMap.get(mimeType);
                                if (infos != null && infos.size() > 0) {
                                    boolean finished = false;
                                    for (IndexerCache.IndexerInfo info : infos) {
                                        int indexerVersion;
                                        EmbeddingIndexer indexer;
                                        if (Work.this.getCancelRequest().isRaised()) {
                                            Work.this.logFinishIndexer(mimeType);
                                            return;
                                        }
                                        EmbeddingIndexerFactory indexerFactory = (EmbeddingIndexerFactory)info.getIndexerFactory();
                                        if (LOGGER.isLoggable(Level.FINE)) {
                                            LOGGER.fine("Indexing file " + fileObject.getPath() + " using " + indexerFactory + "; mimeType='" + mimeType + "'");
                                        }
                                        try {}
                                        finally {
                                            if (!finished) {
                                                Work.this.logFinishIndexer(mimeType);
                                                finished = true;
                                            }
                                        }
                                        Parser.Result pr = resultIterator.getParserResult();
                                        if (pr == null) continue;
                                        String indexerName = indexerFactory.getIndexerName();
                                        Pair key = Pair.of((Object)indexerName, (Object)(indexerVersion = indexerFactory.getIndexVersion()));
                                        Pair value = (Pair)transactionContexts.get((Object)key);
                                        if (value == null) {
                                            Context context = SPIAccessor.getInstance().createContext(cache, rootURL, indexerName, indexerVersion, null, Work.this.followUpJob, Work.this.checkEditor, sourceForBinaryRoot, Work.this.getSuspendStatus(), Work.this.getCancelRequest(), Work.this.logCtx);
                                            value = Pair.of((Object)indexerFactory, (Object)context);
                                            transactionContexts.put(key, value);
                                        }
                                        if ((indexer = indexerFactory.createIndexer(dirty, pr.getSnapshot())) == null) continue;
                                        SPIAccessor.getInstance().putProperty((Context)value.second(), "ci-index-set", usedCi);
                                        long st = System.currentTimeMillis();
                                        Work.this.logStartIndexer(indexerName);
                                        int estimate = Work.this.estimateEmbeddingIndexer(indexerFactory);
                                        SamplerInvoker.start(Work.this.getLogContext(), indexerFactory.getIndexerName(), estimate, url);
                                        try {
                                            SPIAccessor.getInstance().index(indexer, dirty, pr, (Context)value.second());
                                        }
                                        catch (ThreadDeath td) {
                                            throw td;
                                        }
                                        catch (Throwable t) {
                                            LOGGER.log(Level.WARNING, null, t);
                                        }
                                        finally {
                                            SamplerInvoker.stop();
                                        }
                                        long et = System.currentTimeMillis();
                                        Work.this.logIndexerTime(indexerName, (int)(et - st));
                                    }
                                } else {
                                    Work.this.logFinishIndexer(mimeType);
                                }
                                for (Embedding embedding : resultIterator.getEmbeddings()) {
                                    if (Work.this.getCancelRequest().isRaised()) {
                                        return;
                                    }
                                    Work.this.logStartIndexer(embedding.getMimeType());
                                    this.run(resultIterator.getResultIterator(embedding));
                                }
                            }
                        });
                    }
                    catch (ParseException e) {
                        this.logFinishIndexer(src.getMimeType());
                        LOGGER.log(Level.WARNING, null, e);
                    }
                }
                InjectedTasksSupport.execute();
            }
            return !this.getCancelRequest().isRaised();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final boolean scanFiles(URL root, Collection<FileObject> files, boolean forceRefresh, boolean sourceForBinaryRoot) {
            FileObject rootFo = URLMapper.findFileObject((URL)root);
            if (rootFo != null) {
                LogContext lctx = this.getLogContext();
                try {
                    FileObjectCrawler crawler;
                    boolean permanentUpdate;
                    ClassPath.Entry entry = sourceForBinaryRoot ? null : RepositoryUpdater.getClassPathEntry(rootFo);
                    EnumSet<Crawler.TimeStampAction> checkTimeStamps = EnumSet.noneOf(Crawler.TimeStampAction.class);
                    boolean bl = permanentUpdate = !TransientUpdateSupport.isTransientUpdate();
                    assert (permanentUpdate || forceRefresh && !files.isEmpty());
                    if (!forceRefresh) {
                        checkTimeStamps.add(Crawler.TimeStampAction.CHECK);
                    }
                    if (permanentUpdate) {
                        checkTimeStamps.add(Crawler.TimeStampAction.UPDATE);
                    }
                    FileObjectCrawler fileObjectCrawler = crawler = files.isEmpty() ? new FileObjectCrawler(rootFo, checkTimeStamps, entry, this.getCancelRequest(), this.getSuspendStatus()) : new FileObjectCrawler(rootFo, files.toArray((T[])new FileObject[files.size()]), checkTimeStamps, entry, this.getCancelRequest(), this.getSuspendStatus());
                    if (lctx != null) {
                        lctx.noteRootScanning(root, true);
                    }
                    long t = System.currentTimeMillis();
                    List<Indexable> resources = crawler.getResources();
                    if (crawler.isFinished()) {
                        this.logCrawlerTime(crawler, t);
                        IdentityHashMap<SourceIndexerFactory, Boolean> invalidatedMap = new IdentityHashMap<SourceIndexerFactory, Boolean>();
                        HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>> ctxToFinish = new HashMap<Pair<String, Integer>, Pair<SourceIndexerFactory, Context>>();
                        UsedIndexables usedIterables = new UsedIndexables();
                        SourceIndexers indexers = SourceIndexers.load(false);
                        this.invalidateSources(resources);
                        boolean indexResult = false;
                        try {
                            this.scanStarted(root, sourceForBinaryRoot, indexers, invalidatedMap, ctxToFinish);
                            this.delete(crawler.getDeletedResources(), ctxToFinish, usedIterables);
                            indexResult = this.index(resources, crawler.getAllResources(), root, sourceForBinaryRoot, indexers, invalidatedMap, ctxToFinish, usedIterables);
                            if (indexResult) {
                                crawler.storeTimestamps();
                                boolean bl2 = true;
                                return bl2;
                            }
                        }
                        finally {
                            this.scanFinished(ctxToFinish.values(), usedIterables, indexResult);
                        }
                    }
                    boolean invalidatedMap = false;
                    return invalidatedMap;
                }
                catch (IOException ioe) {
                    LOGGER.log(Level.WARNING, null, ioe);
                }
                finally {
                    if (lctx != null) {
                        lctx.finishScannedRoot(root);
                    }
                }
            }
            return true;
        }

        protected abstract boolean getDone();

        protected boolean isCancelledBy(Work newWork, Collection<? super Work> follow) {
            return false;
        }

        public boolean absorb(Work newWork) {
            return false;
        }

        protected final boolean isCancelledExternally() {
            return this.externalCancel.get();
        }

        protected final CancelRequest getCancelRequest() {
            return this.cancelRequest;
        }

        @NonNull
        protected final SuspendStatus getSuspendStatus() {
            return this.suspendStatus;
        }

        protected final void parkWhileSuspended() {
            try {
                this.suspendStatus.parkWhileSuspended();
            }
            catch (InterruptedException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        protected final void logCrawlerTime(Crawler crawler, long start) throws IOException {
            LogContext lctx = this.getLogContext();
            if (lctx == null) {
                return;
            }
            List<Indexable> c = crawler.getResources();
            List<Indexable> ac = crawler.getAllResources();
            lctx.addCrawlerTime(System.currentTimeMillis() - start, c == null ? -1 : c.size(), ac == null ? -1 : ac.size());
        }

        protected final void logStartIndexer(String iName) {
            LogContext lc = this.getLogContext();
            if (lc != null) {
                lc.startIndexer(iName);
            }
        }

        protected final void logFinishIndexer(String iName) {
            LogContext lc = this.getLogContext();
            if (lc != null) {
                lc.finishIndexer(iName);
            }
        }

        protected final void logIndexerTime(@NonNull String indexerName, int time) {
            LogContext lc = this.getLogContext();
            if (lc != null) {
                lc.addIndexerTime(indexerName, time);
            }
            if (!this.reportIndexerStatistics) {
                return;
            }
            int[] itime = this.indexerStatistics.get(indexerName);
            if (itime == null) {
                itime = new int[]{0, 0};
                this.indexerStatistics.put(indexerName, itime);
            }
            int[] arrn = itime;
            arrn[0] = arrn[0] + 1;
            int[] arrn2 = itime;
            arrn2[1] = arrn2[1] + time;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public final void doTheWork() {
            try {
                long startTime = -1;
                if (UI_LOGGER.isLoggable(Level.INFO) || PERF_LOGGER.isLoggable(Level.FINE)) {
                    Work.reportIndexingStart(UI_LOGGER, Level.INFO, lastScanEnded);
                    if (this.logCtx != null) {
                        this.logCtx.recordExecuted();
                    }
                    startTime = System.currentTimeMillis();
                    this.reportIndexerStatistics = true;
                }
                try {
                    this.finished.compareAndSet(false, this.getDone());
                }
                finally {
                    SamplerInvoker.release();
                    if (this.reportIndexerStatistics) {
                        lastScanEnded = System.currentTimeMillis();
                        Object[] stats = Work.createIndexerStatLogData(lastScanEnded - startTime, this.indexerStatistics);
                        Work.reportIndexerStatistics(UI_LOGGER, Level.INFO, stats);
                        Work.reportIndexerStatistics(PERF_LOGGER, Level.FINE, stats);
                    }
                    if (this.logCtx != null) {
                        this.logCtx.recordFinished();
                    }
                }
            }
            catch (Throwable t) {
                LOGGER.log(Level.WARNING, null, t);
                this.finished.set(true);
                if (t instanceof ThreadDeath) {
                    throw (ThreadDeath)t;
                }
            }
            finally {
                this.latch.countDown();
            }
        }

        public final void waitUntilDone() {
            while (this.latch.getCount() != 0) {
                try {
                    this.latch.await();
                }
                catch (InterruptedException e) {
                    LOGGER.log(Level.FINE, null, e);
                }
            }
        }

        public final void setCancelled(boolean cancelled) {
            this.cancelled.set(cancelled);
            this.externalCancel.set(cancelled);
        }

        public final boolean cancelBy(Work newWork, Collection<? super Work> follow) {
            if (this.isCancelledBy(newWork, follow)) {
                LOGGER.log(Level.FINE, "{0} cancelled by {1}", new Object[]{this, newWork});
                this.cancelled.set(true);
                this.finished.set(true);
                return true;
            }
            return false;
        }

        public final boolean isFinished() {
            return this.finished.get();
        }

        public final String getProgressTitle() {
            return this.progressTitle;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public final void setProgressHandle(ProgressHandle progressHandle) {
            Object object = this.progressLock;
            synchronized (object) {
                this.progressHandle = progressHandle;
            }
        }

        private String urlForMessage(URL currentlyScannedRoot) {
            File file = FileUtil.archiveOrDirForURL((URL)currentlyScannedRoot);
            String msg = file != null ? file.getAbsolutePath() : currentlyScannedRoot.toExternalForm();
            return msg;
        }

        public String toString() {
            return this.getClass().getSimpleName() + "@" + Integer.toHexString(System.identityHashCode(this)) + "[followUpJob=" + this.followUpJob + ", checkEditor=" + this.checkEditor;
        }

        protected final Source getActiveSource() {
            Source utActiveSrc = RepositoryUpdater.unitTestActiveSource;
            if (utActiveSrc != null) {
                return utActiveSrc;
            }
            JTextComponent jtc = EditorRegistry.lastFocusedComponent();
            if (jtc == null) {
                return null;
            }
            Document doc = jtc.getDocument();
            assert (doc != null);
            return DocumentUtilities.getMimeType((Document)doc) == null ? null : Source.create(doc);
        }

        protected void refreshActiveDocument() {
            Source source = this.getActiveSource();
            if (source != null) {
                LOGGER.fine("Invalidating source: " + source + " due to RootsWork");
                EventSupport support = SourceAccessor.getINSTANCE().getEventSupport(source);
                assert (support != null);
                support.resetState(true, false, -1, -1, false);
            }
        }

        private static void reportIndexerStatistics(@NonNull Logger logger, @NonNull Level level, @NonNull Object[] data) {
            if (logger.isLoggable(level)) {
                LogRecord r = new LogRecord(level, "INDEXING_FINISHED");
                r.setParameters(data);
                r.setResourceBundle(NbBundle.getBundle(RepositoryUpdater.class));
                r.setResourceBundleName(RepositoryUpdater.class.getPackage().getName() + ".Bundle");
                r.setLoggerName(logger.getName());
                logger.log(r);
            }
        }

        private static void reportIndexingStart(@NonNull Logger logger, @NonNull Level level, long lastScanEnded) {
            if (logger.isLoggable(level)) {
                LogRecord r = new LogRecord(level, "INDEXING_STARTED");
                Object[] arrobject = new Object[1];
                arrobject[0] = lastScanEnded == -1 ? 0 : System.currentTimeMillis() - lastScanEnded;
                r.setParameters(arrobject);
                r.setResourceBundle(NbBundle.getBundle(RepositoryUpdater.class));
                r.setResourceBundleName(RepositoryUpdater.class.getPackage().getName() + ".Bundle");
                r.setLoggerName(logger.getName());
                logger.log(r);
            }
        }

        private static Object[] createIndexerStatLogData(long indexingTime, Map<String, int[]> stats) {
            Object[] result = new Object[3 * stats.size() + 1];
            result[0] = indexingTime;
            Iterator<Map.Entry<String, int[]>> it = stats.entrySet().iterator();
            int i = 1;
            while (it.hasNext()) {
                Map.Entry<String, int[]> e = it.next();
                result[i] = e.getKey();
                int[] countTimePair = e.getValue();
                result[i + 1] = countTimePair[0];
                result[i + 2] = countTimePair[1];
                i += 3;
            }
            return result;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected final boolean storeChanges(@NullAllowed String indexerName, @NonNull Context ctx, boolean optimize, @NullAllowed Iterable<? extends Indexable> indexables, boolean finished) throws IOException {
            try {
                FileObject indexFolder = ctx.getIndexFolder();
                if (indexFolder == null) {
                    throw new IllegalStateException(String.format("No index folder for context: %s", ctx));
                }
                LayeredDocumentIndex index = SPIAccessor.getInstance().getIndexFactory(ctx).getIndex(indexFolder);
                if (index != null) {
                    TEST_LOGGER.log(Level.FINEST, "indexCommit:{0}:{1}", new Object[]{indexerName, ctx.getRootURI()});
                    try {
                        if (finished) {
                            this.storeChanges((DocumentIndex)index, optimize, indexables);
                        } else {
                            this.rollBackChanges((DocumentIndex.Transactional)index);
                        }
                    }
                    catch (IOException ioe) {
                        LOGGER.log(Level.WARNING, "Broken index for root: {0} reason: {1}, recovering.", new Object[]{ctx.getRootURI(), ioe.getMessage()});
                        index.clear();
                        boolean bl = false;
                        DocumentIndexCache cache = SPIAccessor.getInstance().getIndexFactory(ctx).getCache(ctx);
                        if (cache instanceof ClusteredIndexables.AttachableDocumentIndexCache) {
                            ((ClusteredIndexables.AttachableDocumentIndexCache)cache).detach();
                        }
                        return bl;
                    }
                }
                boolean ioe = true;
                return ioe;
            }
            finally {
                DocumentIndexCache cache = SPIAccessor.getInstance().getIndexFactory(ctx).getCache(ctx);
                if (cache instanceof ClusteredIndexables.AttachableDocumentIndexCache) {
                    ((ClusteredIndexables.AttachableDocumentIndexCache)cache).detach();
                }
            }
        }

        private void storeChanges(@NonNull DocumentIndex docIndex, boolean optimize, @NullAllowed Iterable<? extends Indexable> indexables) throws IOException {
            this.parkWhileSuspended();
            long t = System.currentTimeMillis();
            if (indexables != null) {
                ArrayList<String> keysToRemove = new ArrayList<String>();
                for (Indexable indexable : indexables) {
                    keysToRemove.add(indexable.getRelativePath());
                }
                docIndex.removeDirtyKeys(keysToRemove);
            }
            docIndex.store(optimize);
            long span = System.currentTimeMillis() - t;
            LogContext lc = this.getLogContext();
            if (lc != null) {
                lc.addStoreTime(span);
            }
        }

        private void rollBackChanges(@NonNull DocumentIndex.Transactional docIndex) throws IOException {
            docIndex.rollback();
        }

        private static final class CancelRequestImpl
        implements CancelRequest {
            private final AtomicBoolean cancelled;
            private Boolean successStatus;

            CancelRequestImpl(@NonNull AtomicBoolean cancelled) {
                Parameters.notNull((CharSequence)"cancelled", (Object)cancelled);
                this.cancelled = cancelled;
            }

            void setResult(@NullAllowed Boolean result) {
                this.successStatus = result;
            }

            @Override
            public boolean isRaised() {
                return this.successStatus != null ? !this.successStatus.booleanValue() : this.cancelled.get();
            }
        }

        final class UsedIndexables {
            private final Collection<Iterable<? extends Indexable>> usedIndexables;
            private Iterable<? extends Indexable> cache;

            UsedIndexables() {
                this.usedIndexables = new ArrayDeque<Iterable<? extends Indexable>>();
            }

            void offer(@NonNull Iterable<? extends Indexable> indexables) {
                this.usedIndexables.add(indexables);
                this.cache = null;
            }

            void offerAll(@NonNull Collection<? extends Iterable<? extends Indexable>> indexables) {
                this.usedIndexables.addAll(indexables);
                this.cache = null;
            }

            Iterable<? extends Indexable> get() {
                if (this.usedIndexables.isEmpty()) {
                    return null;
                }
                if (this.cache == null) {
                    this.cache = new ProxyIterable<Indexable>(this.usedIndexables, false, true);
                }
                return this.cache;
            }
        }

    }

    static enum State {
        CREATED,
        STARTED,
        INITIAL_SCAN_RUNNING,
        ACTIVE,
        STOPPED;
        

        private State() {
        }
    }

    public static enum IndexingState {
        STARTING,
        PATH_CHANGING,
        WORKING;
        

        private IndexingState() {
        }
    }

}

