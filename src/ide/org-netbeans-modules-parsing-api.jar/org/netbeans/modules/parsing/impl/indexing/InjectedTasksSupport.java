/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.TransientUpdateSupport;
import org.openide.util.Parameters;

class InjectedTasksSupport {
    private static final Queue<Callable<?>> tasks = new ConcurrentLinkedQueue();

    InjectedTasksSupport() {
    }

    static void enqueueTask(@NonNull Callable<?> task) {
        Parameters.notNull((CharSequence)"task", task);
        tasks.offer(task);
    }

    static void clear() {
        if (!TransientUpdateSupport.isTransientUpdate()) {
            tasks.clear();
        }
    }

    @CheckForNull
    static Callable<?> nextTask() {
        return TransientUpdateSupport.isTransientUpdate() ? null : tasks.poll();
    }

    static void execute() throws IOException {
        Callable task = InjectedTasksSupport.nextTask();
        while (task != null) {
            try {
                task.call();
            }
            catch (RuntimeException re) {
                throw re;
            }
            catch (IOException ioe) {
                throw ioe;
            }
            catch (Exception e) {
                throw new IOException(e);
            }
            task = InjectedTasksSupport.nextTask();
        }
    }
}

