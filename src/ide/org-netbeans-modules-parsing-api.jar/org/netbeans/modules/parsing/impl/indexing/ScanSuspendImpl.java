/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.lucene.spi.ScanSuspendImplementation
 */
package org.netbeans.modules.parsing.impl.indexing;

import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.lucene.spi.ScanSuspendImplementation;

public class ScanSuspendImpl
implements ScanSuspendImplementation {
    public void suspend() {
        RepositoryUpdater.getDefault().suspend();
    }

    public void resume() {
        RepositoryUpdater.getDefault().resume();
    }
}

