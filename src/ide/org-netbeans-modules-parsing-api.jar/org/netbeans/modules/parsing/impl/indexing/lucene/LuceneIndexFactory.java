/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2$Transactional
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl.indexing.lucene;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.ClusteredIndexables;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.TransientUpdateSupport;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.impl.indexing.lucene.DocumentBasedIndexManager;
import org.netbeans.modules.parsing.impl.indexing.lucene.LayeredDocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex2;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public final class LuceneIndexFactory
implements IndexFactoryImpl {
    private static final int VERSION = 1;
    private static LuceneIndexFactory instance;
    private final Map<URL, LayeredDocumentIndex> indexes = new HashMap<URL, LayeredDocumentIndex>();
    private boolean closed;

    private LuceneIndexFactory() {
    }

    @NonNull
    @Override
    public IndexDocument createDocument(@NonNull Indexable indexable) {
        Parameters.notNull((CharSequence)"indexable", (Object)indexable);
        return TransientUpdateSupport.isTransientUpdate() ? IndexManager.createDocument((String)indexable.getRelativePath()) : ClusteredIndexables.createDocument(indexable.getRelativePath());
    }

    @CheckForNull
    @Override
    public LayeredDocumentIndex createIndex(@NonNull Context ctx) throws IOException {
        Parameters.notNull((CharSequence)"ctx", (Object)ctx);
        FileObject indexBaseFolder = ctx.getIndexFolder();
        if (indexBaseFolder == null) {
            throw new IOException("No index base folder.");
        }
        return this.getIndexImpl(indexBaseFolder, DocumentBasedIndexManager.Mode.CREATE);
    }

    @CheckForNull
    @Override
    public DocumentIndexCache getCache(@NonNull Context ctx) throws IOException {
        Parameters.notNull((CharSequence)"ctx", (Object)ctx);
        FileObject indexBaseFolder = ctx.getIndexFolder();
        if (indexBaseFolder == null) {
            throw new IOException("No index base folder.");
        }
        return DocumentBasedIndexManager.getDefault().getCache(this.getIndexFolder(indexBaseFolder));
    }

    @CheckForNull
    @Override
    public LayeredDocumentIndex getIndex(@NonNull FileObject indexFolder) throws IOException {
        Parameters.notNull((CharSequence)"indexFolder", (Object)indexFolder);
        return this.getIndexImpl(indexFolder, DocumentBasedIndexManager.Mode.IF_EXIST);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    private LayeredDocumentIndex getIndexImpl(@NonNull FileObject indexBaseFolder, @NonNull DocumentBasedIndexManager.Mode mode) throws IOException {
        URL luceneIndexFolder = this.getIndexFolder(indexBaseFolder);
        Map<URL, LayeredDocumentIndex> map = this.indexes;
        synchronized (map) {
            DocumentIndex2.Transactional base;
            if (this.closed) {
                return null;
            }
            LayeredDocumentIndex res = this.indexes.get(luceneIndexFolder);
            if (res == null && (base = DocumentBasedIndexManager.getDefault().getIndex(luceneIndexFolder, mode)) != null) {
                res = new LayeredDocumentIndex(base);
                this.indexes.put(luceneIndexFolder, res);
            }
            return res;
        }
    }

    @NonNull
    private URL getIndexFolder(@NonNull FileObject indexFolder) throws IOException {
        assert (indexFolder != null);
        String indexVersion = Integer.toString(1);
        URL result = Util.resolveFile(FileUtil.toFile((FileObject)indexFolder), indexVersion, Boolean.TRUE);
        String surl = result.toExternalForm();
        if (surl.charAt(surl.length() - 1) != '/') {
            result = new URL(surl + '/');
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void close() {
        Map<URL, LayeredDocumentIndex> map = this.indexes;
        synchronized (map) {
            if (this.closed) {
                return;
            }
            this.closed = true;
            for (LayeredDocumentIndex index : this.indexes.values()) {
                try {
                    index.close();
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
    }

    @NonNull
    public static synchronized LuceneIndexFactory getDefault() {
        if (instance == null) {
            instance = new LuceneIndexFactory();
        }
        return instance;
    }
}

