/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.parsing.impl.indexing.FileObjectProvider;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Parameters;

public final class FileObjectIndexable
implements IndexableImpl,
FileObjectProvider {
    private static final Logger LOG = Logger.getLogger(FileObjectIndexable.class.getName());
    private final FileObject root;
    private final String relativePath;
    private Object url;
    private String mimeType;
    private FileObject file;

    public FileObjectIndexable(FileObject root, FileObject file) {
        this(root, FileUtil.getRelativePath((FileObject)root, (FileObject)file));
        this.file = file;
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "File: {0}", FileUtil.getFileDisplayName((FileObject)file));
        }
    }

    public FileObjectIndexable(FileObject root, String relativePath) {
        Parameters.notNull((CharSequence)"root", (Object)root);
        Parameters.notNull((CharSequence)"relativePath", (Object)relativePath);
        this.root = root;
        this.relativePath = relativePath;
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.log(Level.FINEST, "Root: {0}", FileUtil.getFileDisplayName((FileObject)root));
            LOG.log(Level.FINEST, "Path: {0}", relativePath);
        }
    }

    @Override
    public String getRelativePath() {
        return this.relativePath;
    }

    @Override
    public URL getURL() {
        if (this.url == null) {
            try {
                FileObject f = this.getFileObject();
                if (f != null) {
                    this.url = f.toURL();
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.log(Level.FINEST, "URL from existing FileObject: {0} = {1}", new Object[]{FileUtil.getFileDisplayName((FileObject)f), this.url});
                    }
                } else {
                    this.url = Util.resolveUrl(this.root.toURL(), this.relativePath, false);
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.log(Level.FINEST, "URL from non existing FileObject root: {0} ({1}), relative path: {2} = {3}", new Object[]{FileUtil.getFileDisplayName((FileObject)this.root), this.root.toURL(), this.relativePath, this.url});
                    }
                }
            }
            catch (MalformedURLException ex) {
                this.url = ex;
            }
        }
        return this.url instanceof URL ? (URL)this.url : null;
    }

    @Override
    public String getMimeType() {
        return this.mimeType == null ? "content/unknown" : this.mimeType;
    }

    @Override
    public boolean isTypeOf(String mimeType) {
        FileObject f;
        String mt;
        Parameters.notNull((CharSequence)"mimeType", (Object)mimeType);
        if (this.mimeType == null && (f = this.getFileObject()) != null && (mt = FileUtil.getMIMEType((FileObject)f, (String[])new String[]{mimeType})) != null && !mt.equals("content/unknown")) {
            this.mimeType = mt;
        }
        return this.mimeType == null ? false : this.mimeType.equals(mimeType);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        FileObjectIndexable other = (FileObjectIndexable)obj;
        if (!(this.root == other.root || this.root != null && this.root.equals((Object)other.root))) {
            return false;
        }
        if (!(this.relativePath == other.relativePath || this.relativePath != null && this.relativePath.equals(other.relativePath))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.relativePath != null ? this.relativePath.hashCode() : 0;
    }

    public String toString() {
        return "FileObjectIndexable@" + Integer.toHexString(System.identityHashCode(this)) + " [" + this.root.toURL() + "/" + this.getRelativePath() + "]";
    }

    @Override
    public FileObject getFileObject() {
        if (this.file == null) {
            this.file = this.root.getFileObject(this.relativePath);
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.FINEST, "File: {0} in Root: {1}", new Object[]{FileUtil.getFileDisplayName((FileObject)this.file), FileUtil.getFileDisplayName((FileObject)this.root)});
            }
        }
        return this.file != null && this.file.isValid() ? this.file : null;
    }
}

