/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl;

import java.util.Map;
import java.util.Set;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.SourceFlags;
import org.netbeans.modules.parsing.impl.event.EventSupport;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public abstract class SourceAccessor {
    private static volatile SourceAccessor INSTANCE;

    public static synchronized SourceAccessor getINSTANCE() {
        if (INSTANCE == null) {
            try {
                Class.forName("org.netbeans.modules.parsing.api.Source", true, SourceAccessor.class.getClassLoader());
                assert (INSTANCE != null);
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return INSTANCE;
    }

    public static void setINSTANCE(SourceAccessor instance) {
        assert (instance != null);
        INSTANCE = instance;
    }

    public abstract void setFlags(Source var1, Set<SourceFlags> var2);

    public abstract boolean testFlag(Source var1, SourceFlags var2);

    public abstract boolean cleanFlag(Source var1, SourceFlags var2);

    public abstract boolean testAndCleanFlags(Source var1, SourceFlags var2, Set<SourceFlags> var3);

    public abstract void invalidate(Source var1, boolean var2);

    public abstract boolean invalidate(Source var1, long var2, Snapshot var4);

    public abstract SourceModificationEvent getSourceModificationEvent(Source var1);

    public abstract void setSourceModification(Source var1, boolean var2, int var3, int var4);

    public abstract void parsed(Source var1);

    @NonNull
    public abstract Map<Class<? extends Scheduler>, SchedulerEvent> createSchedulerEvents(@NonNull Source var1, @NonNull Iterable<? extends Scheduler> var2, @NonNull SourceModificationEvent var3);

    public abstract void setSchedulerEvent(@NonNull Source var1, @NonNull Scheduler var2, @NonNull SchedulerEvent var3);

    public abstract void mimeTypeMayChanged(@NonNull Source var1);

    public abstract SchedulerEvent getSchedulerEvent(Source var1, Class<? extends Scheduler> var2);

    public abstract Parser getParser(Source var1);

    public abstract void setParser(Source var1, Parser var2) throws IllegalStateException;

    public abstract void assignListeners(Source var1);

    public abstract EventSupport getEventSupport(Source var1);

    public abstract long getLastEventId(Source var1);

    public abstract SourceCache getCache(Source var1);

    public abstract SourceCache getAndSetCache(Source var1, SourceCache var2);

    public abstract int taskAdded(Source var1);

    public abstract int taskRemoved(Source var1);

    public abstract Source get(FileObject var1);

    public abstract void suppressListening(boolean var1, boolean var2);

    public abstract int getLineStartOffset(Snapshot var1, int var2);

    public abstract Snapshot createSnapshot(CharSequence var1, int[] var2, Source var3, MimePath var4, int[][] var5, int[][] var6);
}

