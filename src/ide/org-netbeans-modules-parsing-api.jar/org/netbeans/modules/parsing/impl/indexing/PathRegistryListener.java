/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

import org.netbeans.modules.parsing.impl.indexing.PathRegistryEvent;

public interface PathRegistryListener {
    public void pathsChanged(PathRegistryEvent var1);
}

