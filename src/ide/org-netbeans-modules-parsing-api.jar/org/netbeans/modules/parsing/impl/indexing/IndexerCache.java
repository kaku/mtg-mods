/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.MIMEResolver
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.SourceIndexerFactory;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public abstract class IndexerCache<T extends SourceIndexerFactory> {
    private static final RequestProcessor RP = new RequestProcessor("Indexer Cache");
    private static final Logger LOG = Logger.getLogger(IndexerCache.class.getName());
    private static final String ALL_MIME_TYPES = "";
    private static final Collection<? extends String> SLOW_MIME_TYPES = Arrays.asList("text/sh", "text/x-persistence1.0", "text/x-orm1.0", "application/xhtml+xml", "text/x-maven-pom+xml", "text/x-maven-profile+xml", "text/x-maven-settings+xml", "text/x-ant+xml", "text/x-nbeditor-fontcolorsettings", "text/x-nbeditor-keybindingsettings", "text/x-nbeditor-preferences", "text/x-dd-servlet2.2", "text/x-dd-servlet2.3", "text/x-dd-servlet2.4", "text/x-dd-servlet2.5", "text/x-dd-servlet3.0", "text/x-dd-servlet-fragment3.0", "text/x-dd-ejbjar2.0", "text/x-dd-ejbjar2.1", "text/x-dd-ejbjar3.0", "text/x-dd-client1.3", "text/x-dd-client1.4", "text/x-dd-client5.0", "text/x-dd-application1.4", "text/x-dd-application5.0", "text/x-dd-sun-web+xml", "text/x-dd-sun-ejb-jar+xml", "text/x-dd-sun-application+xml", "text/x-dd-sun-app-client+xml", "text/tomcat5+xml", "text/x-tld", "text/x-jsf+xml", "text/x-struts+xml", "application/x-schema+xml", "text/x-wsdl+xml", "text/x-springconfig+xml", "text/x-tmap+xml", "text/x-bpel+xml", "application/xslt+xml", "text/x-jelly+xml", "text/x-h", "application/x-java-archive", "application/x-exe", "application/x-executable+elf", "application/x-object+elf", "application/x-core+elf", "application/x-shobj+elf", "application/x-elf", "text/x-nbeditor-codetemplatesettings", "text/x-nbeditor-macrosettings", "text/x-hibernate-cfg+xml", "text/x-hibernate-mapping+xml", "text/x-hibernate-reveng+xml", "text/x-ruby", "text/x-php5");
    private static IndexerCache<CustomIndexerFactory> instanceCIF = null;
    private static IndexerCache<EmbeddingIndexerFactory> instanceEIF = null;
    private final Comparator<IndexerInfo<T>> IIC;
    private final Class<T> type;
    private final String infoFileName;
    private final IndexerCache<T> tracker;
    private final PropertyChangeSupport pcs;
    private boolean firstGetData;
    private Map<String, Set<IndexerInfo<T>>> infosByName;
    private Map<String, Collection<IndexerInfo<T>>> infosByMimeType;
    private List<IndexerInfo<T>> orderedInfos;

    public static synchronized IndexerCache<CustomIndexerFactory> getCifCache() {
        if (instanceCIF == null) {
            instanceCIF = new IndexerCache<CustomIndexerFactory>(CustomIndexerFactory.class){

                @Override
                protected String getIndexerName(CustomIndexerFactory indexerFactory) {
                    return indexerFactory.getIndexerName();
                }

                @Override
                protected int getIndexerVersion(CustomIndexerFactory indexerFactory) {
                    return indexerFactory.getIndexVersion();
                }
            };
        }
        return instanceCIF;
    }

    public static synchronized IndexerCache<EmbeddingIndexerFactory> getEifCache() {
        if (instanceEIF == null) {
            instanceEIF = new IndexerCache<EmbeddingIndexerFactory>(EmbeddingIndexerFactory.class){

                @Override
                protected String getIndexerName(EmbeddingIndexerFactory indexerFactory) {
                    return indexerFactory.getIndexerName();
                }

                @Override
                protected int getIndexerVersion(EmbeddingIndexerFactory indexerFactory) {
                    return indexerFactory.getIndexVersion();
                }
            };
        }
        return instanceEIF;
    }

    public Collection<? extends IndexerInfo<T>> getIndexers(Set<IndexerInfo<T>> changedIndexers) {
        Object[] data = this.getData(changedIndexers, false);
        List infos = (List)data[2];
        return infos;
    }

    public Map<String, Collection<IndexerInfo<T>>> getIndexersMap(Set<IndexerInfo<T>> changedIndexers) {
        Object[] data = this.getData(changedIndexers, false);
        Map infosMap = (Map)data[1];
        return infosMap;
    }

    public Collection<? extends IndexerInfo<T>> getIndexersFor(@NonNull String mimeType, boolean transientState) {
        Object[] data = this.getData(null, transientState);
        Map infosMap = (Map)data[1];
        Set infos = (Set)infosMap.get(mimeType);
        return infos == null ? Collections.emptySet() : infos;
    }

    @CheckForNull
    public Collection<? extends IndexerInfo<T>> getIndexersByName(String indexerName) {
        Object[] data = this.getData(null, false);
        Map infosMap = (Map)data[0];
        Set info = (Set)infosMap.get(indexerName);
        return info;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    protected abstract String getIndexerName(T var1);

    protected abstract int getIndexerVersion(T var1);

    private IndexerCache(Class<T> type) {
        this.IIC = new Comparator<IndexerInfo<T>>(){

            @Override
            public int compare(@NonNull IndexerInfo<T> o1, @NonNull IndexerInfo<T> o2) {
                int p2;
                int p1 = o1.getIndexerFactory().getPriority();
                return p1 < (p2 = o2.getIndexerFactory().getPriority()) ? -1 : (p1 == p2 ? 0 : 1);
            }
        };
        this.tracker = new Tracker();
        this.pcs = new PropertyChangeSupport(this);
        this.firstGetData = true;
        this.infosByName = null;
        this.infosByMimeType = null;
        this.orderedInfos = null;
        this.type = type;
        this.infoFileName = "last-known-" + type.getSimpleName() + ".properties";
        EditorSettings.getDefault().addPropertyChangeListener(WeakListeners.propertyChange(this.tracker, (Object)EditorSettings.getDefault()));
    }

    private void collectIndexerFactoriesRegisteredForAllLanguages(Map<T, Set<String>> factories) {
        Lookup.Result<T> r = this.tracker.getLookupData("");
        for (SourceIndexerFactory factory : r.allInstances()) {
            Set<String> mimeTypes = factories.get(factory);
            if (mimeTypes != null) continue;
            mimeTypes = new HashSet<String>();
            mimeTypes.add("");
            factories.put((SourceIndexerFactory)factory, mimeTypes);
        }
    }

    private void collectIndexerFactoriesRegisteredForEachParticularLanguage(Map<T, Set<String>> factories, Set<String> mimeTypesToCheck) {
        for (String mimeType : mimeTypesToCheck) {
            Lookup.Result<T> r = this.tracker.getLookupData(mimeType);
            for (SourceIndexerFactory factory : r.allInstances()) {
                Set<String> factoryMimeTypes = factories.get(factory);
                if (factoryMimeTypes == null) {
                    factoryMimeTypes = new HashSet<String>();
                    factoryMimeTypes.add(mimeType);
                    factories.put((SourceIndexerFactory)factory, factoryMimeTypes);
                    continue;
                }
                if (factoryMimeTypes.contains("")) continue;
                factoryMimeTypes.add(mimeType);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    private Object[] getData(@NullAllowed Set<IndexerInfo<T>> changedIndexers, boolean transientUpdate) {
        boolean fire = false;
        IndexerCache indexerCache = this;
        synchronized (indexerCache) {
            if (this.infosByName == null) {
                boolean fastTrackOnly;
                Map lastKnownInfos = transientUpdate ? Collections.emptyMap() : this.readLastKnownIndexers();
                Set<String> mimeTypesToCheck = null;
                if (this.firstGetData && !transientUpdate) {
                    this.firstGetData = false;
                    if (changedIndexers != null) {
                        mimeTypesToCheck = new HashSet<String>();
                        for (IndexerInfo ii : lastKnownInfos.values()) {
                            mimeTypesToCheck.addAll(ii.getMimeTypes());
                        }
                        mimeTypesToCheck.remove("");
                    }
                }
                if (mimeTypesToCheck == null || mimeTypesToCheck.isEmpty()) {
                    mimeTypesToCheck = Util.getAllMimeTypes();
                    fastTrackOnly = false;
                } else {
                    fastTrackOnly = true;
                }
                LinkedHashMap<K, V> factories = new LinkedHashMap<K, V>();
                this.collectIndexerFactoriesRegisteredForAllLanguages(factories);
                this.collectIndexerFactoriesRegisteredForEachParticularLanguage(factories, mimeTypesToCheck);
                HashMap<String, HashSet<IndexerInfo<T>>> _infosByName = new HashMap<String, HashSet<IndexerInfo<T>>>();
                HashMap<String, Collection<IndexerInfo<T>>> _infosByMimeType = new HashMap<String, Collection<IndexerInfo<T>>>();
                ArrayList<IndexerInfo<T>> _orderedInfos = new ArrayList<IndexerInfo<T>>();
                for (SourceIndexerFactory factory : factories.keySet()) {
                    Set mimeTypes = (Set)factories.get(factory);
                    String factoryName = this.getIndexerName(factory);
                    IndexerInfo<T> info = new IndexerInfo<T>(factory, factoryName, this.getIndexerVersion(factory), mimeTypes, null);
                    HashSet<IndexerInfo<T>> infos = (HashSet<IndexerInfo<T>>)_infosByName.get(factoryName);
                    if (infos == null) {
                        infos = new HashSet<IndexerInfo<T>>();
                        _infosByName.put(factoryName, infos);
                    }
                    infos.add(info);
                    for (String mimeType : mimeTypes) {
                        Collection<IndexerInfo<T>> infos2 = _infosByMimeType.get(mimeType);
                        if (infos2 == null) {
                            infos2 = new ArrayList<IndexerInfo<T>>();
                            _infosByMimeType.put(mimeType, infos2);
                        }
                        if (infos2.contains(info)) continue;
                        infos2.add(info);
                    }
                    _orderedInfos.add(info);
                }
                Collections.sort(_orderedInfos, new C());
                this.sortInfosByMimeType(_infosByMimeType);
                if (transientUpdate) {
                    return new Object[]{_infosByName, _infosByMimeType, _orderedInfos};
                }
                this.infosByName = Collections.unmodifiableMap(_infosByName);
                this.infosByMimeType = Collections.unmodifiableMap(_infosByMimeType);
                this.orderedInfos = Collections.unmodifiableList(_orderedInfos);
                this.writeLastKnownIndexers(this.infosByName);
                HashMap<String, Set<IndexerInfo<T>>> addedOrChangedInfosMap = new HashMap<String, Set<IndexerInfo<T>>>();
                this.diff(lastKnownInfos, this.infosByName, addedOrChangedInfosMap);
                for (Set<IndexerInfo<T>> addedOrChangedInfos : addedOrChangedInfosMap.values()) {
                    if (changedIndexers == null) {
                        fire = true;
                        changedIndexers = new HashSet<IndexerInfo<T>>();
                    }
                    changedIndexers.addAll(addedOrChangedInfos);
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Ordered indexers of {0}: ", this.type.getName());
                    for (IndexerInfo ii : this.orderedInfos) {
                        Object[] arrobject = new Object[3];
                        arrobject[0] = ii.getIndexerFactory();
                        arrobject[1] = changedIndexers != null && changedIndexers.contains(ii) ? "(modified)" : "";
                        arrobject[2] = ii.getMimeTypes();
                        LOG.log(Level.FINE, "  {0} {1}: {2}", arrobject);
                    }
                }
                if (fastTrackOnly) {
                    RP.post(new Runnable(){

                        @Override
                        public void run() {
                            IndexerCache.this.resetCache();
                            IndexerCache.this.getData(null, false);
                        }
                    }, 321);
                }
            }
            if (fire && changedIndexers.size() > 0) {
                this.pcs.firePropertyChange(this.type.getName(), null, changedIndexers);
            }
            return new Object[]{this.infosByName, this.infosByMimeType, this.orderedInfos};
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void resetCache() {
        IndexerCache indexerCache = this;
        synchronized (indexerCache) {
            this.infosByName = null;
            this.infosByMimeType = null;
            this.orderedInfos = null;
            LOG.log(Level.FINE, "{0}: resetting indexer cache", this.type.getName());
        }
    }

    private void diff(Map<String, IndexerInfo<T>> lastKnownInfos, Map<String, Set<IndexerInfo<T>>> currentInfosMap, Map<String, Set<IndexerInfo<T>>> addedOrChangedInfosMap) {
        for (String indexerName : currentInfosMap.keySet()) {
            Set<IndexerInfo<T>> addedOrChangedInfos;
            if (!lastKnownInfos.containsKey(indexerName)) {
                addedOrChangedInfosMap.put(indexerName, currentInfosMap.get(indexerName));
                continue;
            }
            IndexerInfo<T> lastKnownInfo = lastKnownInfos.get(indexerName);
            Set<IndexerInfo<T>> currentInfos = currentInfosMap.get(indexerName);
            for (IndexerInfo<T> currentInfo2 : currentInfos) {
                if (lastKnownInfo.getIndexerVersion() == currentInfo2.getIndexerVersion()) continue;
                addedOrChangedInfos = addedOrChangedInfosMap.get(indexerName);
                if (addedOrChangedInfos == null) {
                    addedOrChangedInfos = new HashSet<IndexerInfo<T>>();
                    addedOrChangedInfosMap.put(indexerName, addedOrChangedInfos);
                }
                addedOrChangedInfos.add(currentInfo2);
            }
            for (IndexerInfo<T> currentInfo2 : currentInfos) {
                if (lastKnownInfo.getMimeTypes().containsAll(currentInfo2.getMimeTypes())) continue;
                addedOrChangedInfos = addedOrChangedInfosMap.get(indexerName);
                if (addedOrChangedInfos == null) {
                    addedOrChangedInfos = new HashSet<IndexerInfo<T>>();
                    addedOrChangedInfosMap.put(indexerName, addedOrChangedInfos);
                }
                addedOrChangedInfos.add(currentInfo2);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Map<String, IndexerInfo<T>> readLastKnownIndexers() {
        HashMap<String, IndexerInfo<T>> lki = new HashMap<String, IndexerInfo<T>>();
        FileObject cacheFolder = CacheFolder.getCacheFolder();
        FileObject infoFile = cacheFolder.getFileObject(this.infoFileName);
        if (infoFile != null) {
            Properties props = new Properties();
            try {
                InputStream is = infoFile.getInputStream();
                try {
                    props.load(is);
                }
                finally {
                    is.close();
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.FINE, "Can't read " + infoFile.getPath() + " file", ioe);
                props = null;
            }
            if (props != null) {
                for (Map.Entry<K, V> entry : props.entrySet()) {
                    String indexerName = ((String)entry.getKey()).trim();
                    int indexerVersion = -1;
                    HashSet<String> indexerMimeTypes = new HashSet<String>();
                    String[] indexerData = ((String)entry.getValue()).trim().split(",");
                    if (indexerData.length > 0) {
                        try {
                            indexerVersion = Integer.parseInt(indexerData[0]);
                        }
                        catch (NumberFormatException nfe) {
                            // empty catch block
                        }
                        if (indexerData.length > 1) {
                            for (int i = 1; i < indexerData.length; ++i) {
                                String mimeType = indexerData[i];
                                if (mimeType.equals("<all>")) {
                                    indexerMimeTypes.add("");
                                    break;
                                }
                                indexerMimeTypes.add(mimeType);
                            }
                        }
                    }
                    if (indexerName.length() > 0 && indexerVersion != -1 && indexerMimeTypes.size() > 0) {
                        if (!lki.containsKey(indexerName)) {
                            IndexerInfo<T> iinfo = new IndexerInfo<T>(null, indexerName, indexerVersion, indexerMimeTypes, null);
                            lki.put(indexerName, iinfo);
                            continue;
                        }
                        LOG.log(Level.FINE, "Ignoring duplicate indexers data: name={0}, version={1}, mimeTypes={2}", new Object[]{indexerName, indexerVersion, indexerMimeTypes});
                        continue;
                    }
                    LOG.log(Level.FINE, "Ignoring incomplete indexer data: name={0}, version={1}, mimeTypes={2}", new Object[]{indexerName, indexerVersion, indexerMimeTypes});
                }
            }
        }
        return lki;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void writeLastKnownIndexers(Map<String, Set<IndexerInfo<T>>> lki) {
        Properties props = new Properties();
        for (String indexerName : lki.keySet()) {
            Set<IndexerInfo<T>> iinfos = lki.get(indexerName);
            int indexerVersion = -1;
            HashSet<String> mimeTypes = new HashSet<String>();
            for (IndexerInfo<T> iinfo : iinfos) {
                if (indexerVersion == -1) {
                    indexerVersion = iinfo.getIndexerVersion();
                } else if (indexerVersion != iinfo.getIndexerVersion()) {
                    LOG.warning(iinfo.getIndexerFactory() + " has different version then other instances of the same factory: version=" + iinfo.getIndexerVersion() + ", others=" + indexerVersion);
                    continue;
                }
                mimeTypes.addAll(iinfo.getMimeTypes());
            }
            StringBuilder sb = new StringBuilder();
            sb.append(indexerVersion);
            if (mimeTypes.size() > 0) {
                sb.append(",");
                Iterator<E> i = mimeTypes.iterator();
                while (i.hasNext()) {
                    String mimeType = (String)i.next();
                    if (mimeType.length() == 0) {
                        sb.append("<all>");
                        break;
                    }
                    sb.append(mimeType);
                    if (!i.hasNext()) continue;
                    sb.append(",");
                }
            }
            props.put(indexerName, sb.toString());
        }
        FileObject cacheFolder = CacheFolder.getCacheFolder();
        try {
            FileObject infoFile = FileUtil.createData((FileObject)cacheFolder, (String)this.infoFileName);
            OutputStream os = infoFile.getOutputStream();
            try {
                props.store(os, "Last known indexer " + DateFormat.getDateTimeInstance(2, 2).format(new Date()));
            }
            finally {
                os.close();
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.FINE, "Can't write " + this.infoFileName + " file in " + cacheFolder.getPath(), ioe);
        }
    }

    private void sortInfosByMimeType(@NonNull Map<String, Collection<IndexerInfo<T>>> data) {
        for (Map.Entry<String, Collection<IndexerInfo<T>>> entry : data.entrySet()) {
            this.sortIndexerInfos((List)entry.getValue());
        }
    }

    private void sortIndexerInfos(@NonNull List<IndexerInfo<T>> data) {
        boolean needsSort = false;
        for (IndexerInfo<T> f : data) {
            if (f.getIndexerFactory().getPriority() == Integer.MAX_VALUE) continue;
            needsSort = true;
            break;
        }
        if (needsSort) {
            Collections.sort(data, this.IIC);
        }
    }

    private final class C
    implements Comparator<IndexerInfo<T>> {
        private final Map<String, Integer> orderByResolvers;

        public C() {
            HashMap<String, Integer> order = null;
            Method getMIMETypesMethod = null;
            try {
                getMIMETypesMethod = MIMEResolver.class.getDeclaredMethod("getMIMETypes", new Class[0]);
            }
            catch (Exception ex) {
                // empty catch block
            }
            if (getMIMETypesMethod != null) {
                Collection resolvers = Lookup.getDefault().lookupAll(MIMEResolver.class);
                order = new HashMap<String, Integer>();
                int idx = 0;
                for (MIMEResolver r : resolvers) {
                    String[] mimeTypes = null;
                    try {
                        mimeTypes = (String[])getMIMETypesMethod.invoke((Object)r, new Object[0]);
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                    if (mimeTypes != null) {
                        for (String mimeType : mimeTypes) {
                            order.put(mimeType, idx);
                        }
                    }
                    ++idx;
                }
            }
            this.orderByResolvers = order != null && order.size() > 0 ? order : null;
        }

        @Override
        public int compare(IndexerInfo<T> o1, IndexerInfo<T> o2) {
            if (this.orderByResolvers != null) {
                return this.compareByResolvers(o1, o2);
            }
            return this.compareBySlowMimeTypes(o1, o2);
        }

        private int compareByResolvers(IndexerInfo<T> o1, IndexerInfo<T> o2) {
            Collection<String> mimeTypes1 = o1.getMimeTypes();
            Collection<String> mimeTypes2 = o2.getMimeTypes();
            boolean all1 = mimeTypes1.contains("");
            boolean all2 = mimeTypes2.contains("");
            if (all1 && all2) {
                return 0;
            }
            if (all1) {
                return 1;
            }
            if (all2) {
                return -1;
            }
            Integer order1 = this.highestOrder(mimeTypes1);
            Integer order2 = this.highestOrder(mimeTypes2);
            if (order1 == null && order2 == null) {
                return 0;
            }
            if (order1 == null) {
                return 1;
            }
            if (order2 == null) {
                return -1;
            }
            return order1 - order2;
        }

        private int compareBySlowMimeTypes(IndexerInfo<T> o1, IndexerInfo<T> o2) {
            Collection<String> mimeTypes1 = o1.getMimeTypes();
            Collection<String> mimeTypes2 = o2.getMimeTypes();
            boolean all1 = mimeTypes1.contains("");
            boolean all2 = mimeTypes2.contains("");
            if (all1 && all2) {
                return 0;
            }
            if (all1) {
                return 1;
            }
            if (all2) {
                return -1;
            }
            boolean slow1 = Util.containsAny(mimeTypes1, SLOW_MIME_TYPES);
            boolean slow2 = Util.containsAny(mimeTypes2, SLOW_MIME_TYPES);
            if (slow1 && slow2) {
                return 0;
            }
            if (slow1) {
                return 1;
            }
            if (slow2) {
                return -1;
            }
            return 0;
        }

        private Integer highestOrder(Collection<? extends String> mimeTypes) {
            Integer highest = null;
            for (String mimeType : mimeTypes) {
                Integer order = this.orderByResolvers.get(mimeType);
                if (order == null) {
                    highest = null;
                    break;
                }
                if (highest != null && highest >= order) continue;
                highest = order;
            }
            return highest;
        }
    }

    private final class Tracker
    implements LookupListener,
    PropertyChangeListener,
    Runnable {
        private final Map<String, Lookup.Result<T>> results;
        private final RequestProcessor.Task task;

        private Tracker() {
            this.results = new HashMap<String, Lookup.Result<T>>();
            this.task = RP.create((Runnable)this);
        }

        public Lookup.Result<T> getLookupData(String mimeType) {
            Lookup.Result r = this.results.get(mimeType);
            if (r == null) {
                r = MimeLookup.getLookup((String)mimeType).lookupResult(IndexerCache.this.type);
                r.addLookupListener((LookupListener)this);
                this.results.put(mimeType, (Lookup.Result)r);
                LOG.log(Level.FINER, "{0}: listening on MimeLookup for {1}", new Object[]{IndexerCache.this.type.getName(), mimeType});
            }
            return r;
        }

        public void resultChanged(LookupEvent ev) {
            this.task.schedule(0);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName() == null || "mime-types".equals(evt.getPropertyName())) {
                this.task.schedule(123);
            }
        }

        @Override
        public void run() {
            IndexerCache.this.resetCache();
        }
    }

    public static final class IndexerInfo<T extends SourceIndexerFactory> {
        private final String indexerName;
        private final int indexerVersion;
        private final T indexerFactory;
        private final Set<String> mimeTypes;

        public T getIndexerFactory() {
            return this.indexerFactory;
        }

        public Collection<? extends String> getMimeTypes() {
            return this.mimeTypes;
        }

        public boolean isAllMimeTypesIndexer() {
            return this.mimeTypes.contains("");
        }

        public String getIndexerName() {
            return this.indexerName;
        }

        public int getIndexerVersion() {
            return this.indexerVersion;
        }

        private IndexerInfo(T indexerFactory, String indexerName, int indexerVersion, Set<String> mimeTypes) {
            this.indexerFactory = indexerFactory;
            this.indexerName = indexerName;
            this.indexerVersion = indexerVersion;
            this.mimeTypes = Collections.unmodifiableSet(mimeTypes);
        }

        public int hashCode() {
            int hash = 7;
            hash = 37 * hash + this.indexerName.hashCode();
            hash = 37 * hash + this.indexerVersion;
            hash = 37 * hash + this.indexerFactory.hashCode();
            hash = 37 * hash + this.mimeTypes.hashCode();
            return hash;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            IndexerInfo other = (IndexerInfo)obj;
            if (!this.indexerName.equals(other.indexerName)) {
                return false;
            }
            if (this.indexerVersion != other.indexerVersion) {
                return false;
            }
            if (!this.indexerFactory.equals(other.indexerFactory)) {
                return false;
            }
            if (!this.mimeTypes.equals(other.mimeTypes)) {
                return false;
            }
            return true;
        }

        /* synthetic */ IndexerInfo(SourceIndexerFactory x0, String x1, int x2, Set x3,  x4) {
            this(x0, x1, x2, x3);
        }
    }

}

