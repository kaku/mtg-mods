/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.parsing.impl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.modules.parsing.impl.Schedulers;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.lucene.DocumentBasedIndexManager;
import org.netbeans.modules.parsing.impl.indexing.lucene.LuceneIndexFactory;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

public class Installer
extends ModuleInstall {
    private static final int DEFAULT_MAX_FILE_SIZE = 52428800;
    public static final int MAX_FILE_SIZE = Integer.getInteger("parse.max.file.size", 52428800);
    private static volatile boolean closed;

    public static boolean isClosed() {
        return closed;
    }

    public void restored() {
        super.restored();
        RepositoryUpdater.getDefault().start(false);
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                RequestProcessor.getDefault().post(new Runnable(){

                    @Override
                    public void run() {
                        Schedulers.init();
                    }
                });
            }

        });
    }

    public boolean closing() {
        LogContext.notifyClosing();
        return super.closing();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void close() {
        super.close();
        closed = true;
        final CountDownLatch done = new CountDownLatch(1);
        Runnable postTask = new Runnable(){
            private AtomicBoolean started;

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                if (this.started.compareAndSet(false, true)) {
                    try {
                        LuceneIndexFactory.getDefault().close();
                        DocumentBasedIndexManager.getDefault().close();
                    }
                    finally {
                        done.countDown();
                    }
                }
            }
        };
        try {
            RepositoryUpdater.getDefault().stop(postTask);
        }
        catch (TimeoutException timeout) {
            postTask.run();
        }
        finally {
            try {
                done.await();
            }
            catch (InterruptedException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

}

