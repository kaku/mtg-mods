/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.openide.util.NbBundle;

public final class ScanForExternalChanges
extends AbstractAction {
    public ScanForExternalChanges() {
        super(NbBundle.getMessage(ScanForExternalChanges.class, (String)"ScanForExternalChanges_name"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RepositoryUpdater.getDefault().refreshAll(false, false, true, LogContext.create(LogContext.EventType.UI, null), new Object[0]);
    }

    @Override
    public boolean isEnabled() {
        return !IndexingManager.getDefault().isIndexing();
    }

    @Override
    public void setEnabled(boolean newValue) {
    }
}

