/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl;

import org.netbeans.modules.parsing.api.ResultIterator;
import org.openide.util.Exceptions;

public abstract class ResultIteratorAccessor {
    private static volatile ResultIteratorAccessor instance;

    public static void setINSTANCE(ResultIteratorAccessor _instance) {
        assert (_instance != null);
        instance = _instance;
    }

    public static synchronized ResultIteratorAccessor getINSTANCE() {
        if (instance == null) {
            try {
                Class.forName(ResultIterator.class.getName(), true, ResultIteratorAccessor.class.getClassLoader());
                assert (instance != null);
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return instance;
    }

    public abstract void invalidate(ResultIterator var1);
}

