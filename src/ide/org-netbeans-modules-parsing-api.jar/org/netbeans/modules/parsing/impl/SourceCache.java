/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Pair
 */
package org.netbeans.modules.parsing.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.impl.Schedulers;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.spi.EmbeddingProvider;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.Pair;

public final class SourceCache {
    private static final Logger LOG = Logger.getLogger(SourceCache.class.getName());
    private static final Comparator<SchedulerTask> PRIORITY_ORDER = new Comparator<SchedulerTask>(){

        @Override
        public int compare(SchedulerTask o1, SchedulerTask o2) {
            int p2;
            int p1 = o1.getPriority();
            return p1 < (p2 = o2.getPriority()) ? -1 : (p1 == p2 ? 0 : 1);
        }
    };
    private final Source source;
    private Embedding embedding;
    private final String mimeType;
    private Snapshot snapshot;
    private boolean parserInitialized = false;
    private Parser parser;
    private boolean parsed = false;
    private volatile List<Embedding> embeddings;
    private List<EmbeddingProvider> providerOrder;
    private final Map<EmbeddingProvider, List<Embedding>> embeddingProviderToEmbedings = new HashMap<EmbeddingProvider, List<Embedding>>();
    private final Set<EmbeddingProvider> upToDateEmbeddingProviders = new HashSet<EmbeddingProvider>();
    private final Map<Embedding, SourceCache> embeddingToCache = new HashMap<Embedding, SourceCache>();
    private List<SchedulerTask> tasks;
    private Set<SchedulerTask> pendingTasks;

    public SourceCache(Source source, Embedding embedding) {
        assert (source != null);
        this.source = source;
        this.embedding = embedding;
        this.mimeType = embedding != null ? embedding.getMimeType() : source.getMimeType();
        this.mimeType.getClass();
    }

    public SourceCache(Source source, Embedding embedding, Parser parser) {
        this(source, embedding);
        if (parser != null) {
            this.parserInitialized = true;
            this.parser = parser;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setEmbedding(Embedding embedding) {
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            assert (embedding.getMimeType().equals(this.mimeType));
            this.embedding = embedding;
            this.snapshot = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Snapshot getSnapshot() {
        boolean isEmbedding;
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            if (this.snapshot != null) {
                return this.snapshot;
            }
            isEmbedding = this.embedding != null;
        }
        Snapshot _snapshot = this.createSnapshot(isEmbedding);
        Object object2 = TaskProcessor.INTERNAL_LOCK;
        synchronized (object2) {
            if (this.snapshot == null) {
                this.snapshot = _snapshot;
            }
            return this.snapshot;
        }
    }

    @NonNull
    public Source getSource() {
        return this.source;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Snapshot createSnapshot(long[] idHolder) {
        boolean isEmbedding;
        assert (idHolder != null);
        assert (idHolder.length == 1);
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            isEmbedding = this.embedding != null;
        }
        idHolder[0] = SourceAccessor.getINSTANCE().getLastEventId(this.source);
        return this.createSnapshot(isEmbedding);
    }

    private Snapshot createSnapshot(boolean isEmbedding) {
        Snapshot _snapshot;
        Snapshot snapshot = _snapshot = isEmbedding ? this.embedding.getSnapshot() : this.source.createSnapshot();
        assert (this.mimeType.equals(_snapshot.getMimeType()));
        return _snapshot;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Parser getParser() {
        Set<Object> _tmp;
        Object _snapshot;
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            if (this.parserInitialized) {
                return this.parser;
            }
        }
        Parser _parser = null;
        Lookup lookup = MimeLookup.getLookup((String)this.mimeType);
        ParserFactory parserFactory = (ParserFactory)lookup.lookup(ParserFactory.class);
        if (parserFactory != null && (_parser = parserFactory.createParser(_tmp = Collections.singleton(_snapshot = this.getSnapshot()))) == null) {
            LOG.log(Level.INFO, "Parser factory: {0} returned null parser for {1}", new Object[]{parserFactory, _snapshot});
        }
        _snapshot = TaskProcessor.INTERNAL_LOCK;
        synchronized (_snapshot) {
            if (!this.parserInitialized) {
                this.parser = _parser;
                this.parserInitialized = true;
            }
            return this.parser;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Parser.Result getResult(Task task) throws ParseException {
        boolean _parsed;
        assert (TaskProcessor.holdsParserLock());
        Parser _parser = this.getParser();
        if (_parser == null) {
            return null;
        }
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            _parsed = this.parsed;
            this.parsed = true;
        }
        if (!_parsed) {
            boolean parseSuccess = false;
            try {
                Snapshot _snapshot = this.getSnapshot();
                FileObject file = _snapshot.getSource().getFileObject();
                if (file != null && !file.isValid()) {
                    Parser.Result result = null;
                    return result;
                }
                SourceModificationEvent event = SourceAccessor.getINSTANCE().getSourceModificationEvent(this.source);
                TaskProcessor.callParse(_parser, this.snapshot, task, event);
                SourceAccessor.getINSTANCE().parsed(this.source);
                parseSuccess = true;
            }
            finally {
                if (!parseSuccess) {
                    Object object2 = TaskProcessor.INTERNAL_LOCK;
                    synchronized (object2) {
                        this.parsed = false;
                    }
                }
            }
        }
        return TaskProcessor.callGetResult(_parser, task);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void invalidate() {
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            this.snapshot = null;
            this.parsed = false;
            this.embeddings = null;
            this.providerOrder = null;
            this.upToDateEmbeddingProviders.clear();
            for (SourceCache sourceCache : this.embeddingToCache.values()) {
                sourceCache.invalidate();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void invalidate(Snapshot preRendered) {
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            this.invalidate();
            this.snapshot = preRendered;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public Iterable<Embedding> getAllEmbeddings() {
        LinkedHashMap<EmbeddingProvider, List<Embedding>> newEmbeddings;
        List<Embedding> result = this.embeddings;
        if (result != null) {
            return result;
        }
        do {
            HashSet<EmbeddingProvider> currentUpToDateProviders;
            Snapshot snpsht = this.getSnapshot();
            Collection<SchedulerTask> tsks = this.createTasks();
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                currentUpToDateProviders = new HashSet<EmbeddingProvider>(this.upToDateEmbeddingProviders);
            }
            newEmbeddings = new LinkedHashMap<EmbeddingProvider, List<Embedding>>();
            for (SchedulerTask schedulerTask : tsks) {
                if (!(schedulerTask instanceof EmbeddingProvider)) continue;
                EmbeddingProvider embeddingProvider = (EmbeddingProvider)schedulerTask;
                if (newEmbeddings.containsKey(embeddingProvider)) {
                    LOG.log(Level.WARNING, "EmbeddingProvider: {0} is registered several time.", embeddingProvider);
                    continue;
                }
                if (currentUpToDateProviders.contains(embeddingProvider)) {
                    newEmbeddings.put(embeddingProvider, null);
                    continue;
                }
                List<Embedding> embForProv = TaskProcessor.callEmbeddingProvider(embeddingProvider, snpsht);
                if (embForProv == null) {
                    throw new NullPointerException(String.format("The %s returned null embeddings!", embeddingProvider));
                }
                newEmbeddings.put(embeddingProvider, embForProv);
            }
            Object i$ = TaskProcessor.INTERNAL_LOCK;
            synchronized (i$) {
                if (this.embeddings != null) break block12;
                if (this.upToDateEmbeddingProviders.equals(currentUpToDateProviders)) break;
            }
        } while (true);
        {
            block12 : {
                result = new LinkedList<Embedding>();
                for (Map.Entry entry : newEmbeddings.entrySet()) {
                    EmbeddingProvider embeddingProvider = (EmbeddingProvider)entry.getKey();
                    List embeddingsForProvider = (List)entry.getValue();
                    if (embeddingsForProvider == null) {
                        List<Embedding> _embeddings = this.embeddingProviderToEmbedings.get(embeddingProvider);
                        result.addAll(_embeddings);
                        continue;
                    }
                    List<Embedding> oldEmbeddings = this.embeddingProviderToEmbedings.get(embeddingProvider);
                    this.updateEmbeddings(embeddingsForProvider, oldEmbeddings, false, null);
                    this.embeddingProviderToEmbedings.put(embeddingProvider, embeddingsForProvider);
                    this.upToDateEmbeddingProviders.add(embeddingProvider);
                    result.addAll(embeddingsForProvider);
                }
                this.embeddings = result;
                this.providerOrder = new ArrayList(newEmbeddings.keySet());
            }
            return this.embeddings;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void refresh(EmbeddingProvider embeddingProvider, Class<? extends Scheduler> schedulerType) {
        List<Embedding> _embeddings = TaskProcessor.callEmbeddingProvider(embeddingProvider, this.getSnapshot());
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            List<Embedding> oldEmbeddings = this.embeddingProviderToEmbedings.get(embeddingProvider);
            this.updateEmbeddings(_embeddings, oldEmbeddings, true, schedulerType);
            this.embeddingProviderToEmbedings.put(embeddingProvider, _embeddings);
            this.upToDateEmbeddingProviders.add(embeddingProvider);
            if (this.embeddings != null) {
                Embedding insertBefore = this.findNextEmbedding(this.providerOrder, this.embeddingProviderToEmbedings, embeddingProvider);
                this.embeddings.removeAll(oldEmbeddings);
                int index = insertBefore == null ? -1 : this.embeddings.indexOf(insertBefore);
                this.embeddings.addAll(index == -1 ? this.embeddings.size() : index, _embeddings);
            }
        }
    }

    private Embedding findNextEmbedding(List<EmbeddingProvider> order, Map<EmbeddingProvider, List<Embedding>> providers2embs, EmbeddingProvider provider) {
        if (order != null) {
            boolean accept = false;
            for (EmbeddingProvider p : order) {
                if (accept) {
                    Collection c = providers2embs.get(p);
                    if (c == null || c.isEmpty()) continue;
                    return (Embedding)c.iterator().next();
                }
                if (!p.equals(provider)) continue;
                accept = true;
            }
        }
        return null;
    }

    private void updateEmbeddings(List<Embedding> embeddings, List<Embedding> oldEmbeddings, boolean updateTasks, Class<? extends Scheduler> schedulerType) {
        assert (Thread.holdsLock(TaskProcessor.INTERNAL_LOCK));
        ArrayList<SourceCache> toBeSchedulled = new ArrayList<SourceCache>();
        if (oldEmbeddings != null && embeddings.size() == oldEmbeddings.size()) {
            for (int i = 0; i < embeddings.size(); ++i) {
                if (embeddings.get(i) == null) {
                    throw new NullPointerException();
                }
                SourceCache cache = this.embeddingToCache.remove(oldEmbeddings.get(i));
                if (cache != null) {
                    cache.setEmbedding(embeddings.get(i));
                    assert (embeddings.get(i).getMimeType().equals(cache.getSnapshot().getMimeType()));
                    this.embeddingToCache.put(embeddings.get(i), cache);
                } else {
                    cache = this.getCache(embeddings.get(i));
                }
                if (!updateTasks) continue;
                toBeSchedulled.add(cache);
            }
        } else {
            SourceCache cache;
            if (oldEmbeddings != null) {
                for (Embedding _embedding : oldEmbeddings) {
                    cache = this.embeddingToCache.remove(_embedding);
                    if (cache == null) continue;
                    cache.removeTasks();
                }
            }
            if (updateTasks) {
                for (Embedding _embedding : embeddings) {
                    cache = this.getCache(_embedding);
                    toBeSchedulled.add(cache);
                }
            }
        }
        for (SourceCache cache : toBeSchedulled) {
            cache.scheduleTasks(schedulerType);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public SourceCache getCache(Embedding embedding) {
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            SourceCache sourceCache = this.embeddingToCache.get(embedding);
            if (sourceCache == null) {
                sourceCache = new SourceCache(this.source, embedding);
                assert (embedding.getMimeType().equals(sourceCache.getSnapshot().getMimeType()));
                this.embeddingToCache.put(embedding, sourceCache);
            }
            return sourceCache;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<SchedulerTask> createTasks() {
        Object lookup;
        ArrayList<SchedulerTask> tasks1 = null;
        HashSet<SchedulerTask> pendingTasks1 = null;
        if (this.tasks == null) {
            tasks1 = new ArrayList<SchedulerTask>();
            pendingTasks1 = new HashSet<SchedulerTask>();
            lookup = MimeLookup.getLookup((String)this.mimeType);
            Collection factories = lookup.lookupAll(TaskFactory.class);
            Snapshot fakeSnapshot = null;
            for (TaskFactory factory : factories) {
                Collection<? extends SchedulerTask> newTasks = factory.create(this.getParser() != null ? this.getSnapshot() : (fakeSnapshot == null ? SourceAccessor.getINSTANCE().createSnapshot("", new int[]{0}, this.source, MimePath.get((String)this.mimeType), new int[][]{{0, 0}}, new int[][]{{0, 0}}) : fakeSnapshot));
                if (newTasks == null) continue;
                tasks1.addAll(newTasks);
                pendingTasks1.addAll(newTasks);
            }
            Collections.sort(tasks1, PRIORITY_ORDER);
        }
        lookup = TaskProcessor.INTERNAL_LOCK;
        synchronized (lookup) {
            if (this.tasks == null && tasks1 != null) {
                this.tasks = tasks1;
                this.pendingTasks = pendingTasks1;
            }
            if (this.tasks != null) {
                return this.tasks;
            }
        }
        return this.createTasks();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void scheduleTasks(Class<? extends Scheduler> schedulerType) {
        ArrayList<SchedulerTask> remove = new ArrayList<SchedulerTask>();
        ArrayList<Pair<SchedulerTask, Class<? extends Scheduler>>> add = new ArrayList<Pair<SchedulerTask, Class<? extends Scheduler>>>();
        Collection<SchedulerTask> tsks = this.createTasks();
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            for (SchedulerTask task : tsks) {
                if (schedulerType == null || task instanceof EmbeddingProvider) {
                    if (!this.pendingTasks.remove(task)) {
                        remove.add(task);
                    }
                    add.add((Pair)Pair.of((Object)task, (Object)null));
                    continue;
                }
                if (task.getSchedulerClass() != schedulerType) continue;
                if (!this.pendingTasks.remove(task)) {
                    remove.add(task);
                }
                add.add((Pair)Pair.of((Object)task, schedulerType));
            }
        }
        if (!add.isEmpty()) {
            TaskProcessor.updatePhaseCompletionTask(add, remove, this.source, this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void unscheduleTasks(Class<? extends Scheduler> schedulerType) {
        ArrayList<SchedulerTask> remove = new ArrayList<SchedulerTask>();
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            if (this.tasks != null) {
                for (SchedulerTask task : this.tasks) {
                    if (schedulerType != null && task.getSchedulerClass() != schedulerType && !(task instanceof EmbeddingProvider)) continue;
                    remove.add(task);
                }
            }
        }
        if (!remove.isEmpty()) {
            TaskProcessor.removePhaseCompletionTasks(remove, this.source);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void sourceModified() {
        SourceModificationEvent sourceModificationEvent = SourceAccessor.getINSTANCE().getSourceModificationEvent(this.source);
        if (sourceModificationEvent == null) {
            return;
        }
        Map<Class<? extends Scheduler>, SchedulerEvent> schedulerEvents = SourceAccessor.getINSTANCE().createSchedulerEvents(this.source, Schedulers.getSchedulers(), sourceModificationEvent);
        if (schedulerEvents.isEmpty()) {
            return;
        }
        ArrayList<SchedulerTask> remove = new ArrayList<SchedulerTask>();
        ArrayList<Pair<SchedulerTask, Class<? extends Scheduler>>> add = new ArrayList<Pair<SchedulerTask, Class<? extends Scheduler>>>();
        Collection<SchedulerTask> tsks = this.createTasks();
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            for (SchedulerTask task : tsks) {
                Class<? extends Scheduler> schedulerClass = task.getSchedulerClass();
                if (schedulerClass != null && !schedulerEvents.containsKey(schedulerClass)) continue;
                if (!this.pendingTasks.remove(task)) {
                    remove.add(task);
                }
                add.add((Pair)Pair.of((Object)task, (Object)null));
            }
        }
        if (!add.isEmpty()) {
            TaskProcessor.updatePhaseCompletionTask(add, remove, this.source, this);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SourceCache ");
        sb.append(this.hashCode());
        sb.append(": ");
        Snapshot _snapshot = this.getSnapshot();
        Source _source = _snapshot.getSource();
        FileObject fileObject = _source.getFileObject();
        if (fileObject != null) {
            sb.append(fileObject.getNameExt());
        } else {
            sb.append(this.mimeType).append(" ").append(_source.getDocument(false));
        }
        if (!_snapshot.getMimeType().equals(_source.getMimeType())) {
            sb.append("( ").append(_snapshot.getMimeType()).append(" ");
            sb.append(_snapshot.getOriginalOffset(0)).append("-").append(_snapshot.getOriginalOffset(_snapshot.getText().length() - 1)).append(")");
        }
        return sb.toString();
    }

    private void removeTasks() {
        if (this.tasks != null) {
            TaskProcessor.removePhaseCompletionTasks(this.tasks, this.source);
        }
        this.tasks = null;
    }

}

