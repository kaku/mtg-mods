/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl;

import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.CurrentEditorTaskScheduler;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

public class CursorSensitiveScheduler
extends CurrentEditorTaskScheduler {
    private JTextComponent currentEditor;
    private CaretListener caretListener;
    private Document currentDocument;

    @Override
    protected void setEditor(JTextComponent editor) {
        if (this.currentEditor != null) {
            this.currentEditor.removeCaretListener(this.caretListener);
        }
        this.currentEditor = editor;
        if (editor != null) {
            if (this.caretListener == null) {
                this.caretListener = new ACaretListener();
            }
            editor.addCaretListener(this.caretListener);
            Document document = editor.getDocument();
            if (this.currentDocument == document) {
                return;
            }
            this.currentDocument = document;
            Source source = Source.create(this.currentDocument);
            this.schedule(source, new CursorMovedSchedulerEvent(this, editor.getCaret().getDot(), editor.getCaret().getMark()){});
        } else {
            this.currentDocument = null;
            this.schedule(null, null);
        }
    }

    public String toString() {
        return "CursorSensitiveScheduller";
    }

    @Override
    protected SchedulerEvent createSchedulerEvent(SourceModificationEvent event) {
        JTextComponent ce = this.currentEditor;
        Caret caret = ce != null ? ce.getCaret() : null;
        Source s = this.getSource();
        if (event.getModifiedSource() == s && caret != null) {
            return new CursorMovedSchedulerEvent(this, caret.getDot(), caret.getMark()){};
        }
        return null;
    }

    private class ACaretListener
    implements CaretListener {
        private ACaretListener() {
        }

        @Override
        public void caretUpdate(CaretEvent e) {
            CursorSensitiveScheduler.this.schedule(new CursorMovedSchedulerEvent(this, e.getDot(), e.getMark()){});
        }

    }

}

