/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$Entry
 *  org.netbeans.api.queries.VisibilityQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Pair
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.queries.VisibilityQuery;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.Crawler;
import org.netbeans.modules.parsing.impl.indexing.FileObjectIndexable;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Pair;

final class FileObjectCrawler
extends Crawler {
    private static final char SEPARATOR = '/';
    private static final Logger LOG = Logger.getLogger(FileObjectCrawler.class.getName());
    static Map<Pair<File, File>, Boolean> mockLinkTypes;
    private final FileObject root;
    private final ClassPath.Entry entry;
    private final FileObject[] files;

    FileObjectCrawler(@NonNull FileObject root, Set<? extends Crawler.TimeStampAction> checkTimeStamps, @NullAllowed ClassPath.Entry entry, @NonNull CancelRequest cancelRequest, @NonNull SuspendStatus suspendStatus) throws IOException {
        super(root.toURL(), checkTimeStamps, true, true, cancelRequest, suspendStatus);
        this.root = root;
        this.entry = entry;
        this.files = null;
    }

    FileObjectCrawler(@NonNull FileObject root, @NullAllowed FileObject[] files, Set<? extends Crawler.TimeStampAction> checkTimeStamps, @NullAllowed ClassPath.Entry entry, @NonNull CancelRequest cancelRequest, @NonNull SuspendStatus suspendStatus) throws IOException {
        super(root.toURL(), checkTimeStamps, false, FileObjectCrawler.supportsAllFiles(root, files), cancelRequest, suspendStatus);
        this.root = root;
        this.entry = entry;
        this.files = files;
    }

    @Override
    protected boolean collectResources(Collection<Indexable> resources, Collection<Indexable> allResources) {
        boolean finished;
        long tm1;
        Stats stats;
        finished = true;
        tm1 = System.currentTimeMillis();
        Stats stats2 = stats = LOG.isLoggable(Level.FINE) ? new Stats() : null;
        if (this.files != null) {
            StringBuilder relativePath;
            if (this.files.length > 1) {
                HashMap<FileObject, HashSet<FileObject>> clusters = new HashMap<FileObject, HashSet<FileObject>>();
                HashMap<FileObject, StringBuilder> relPaths = new HashMap<FileObject, StringBuilder>();
                block5 : for (FileObject f : this.files) {
                    FileObject parent = f.getParent();
                    HashSet<FileObject> cluster = (HashSet<FileObject>)clusters.get((Object)parent);
                    if (cluster == null) {
                        StringBuilder currentRelPath = this.getRelativePath(this.root, parent);
                        Iterator it = relPaths.entrySet().iterator();
                        block6 : while (it.hasNext()) {
                            Map.Entry relPath = it.next();
                            switch (FileObjectCrawler.getFileRelation(currentRelPath, (StringBuilder)relPath.getValue())) {
                                case FIRST_IN_SECOND: {
                                    Set cs = (Set)clusters.get(relPath.getKey());
                                    for (FileObject csFile : cs) {
                                        if (!csFile.isFolder() || !FileUtil.isParentOf((FileObject)csFile, (FileObject)f)) continue;
                                        continue block5;
                                    }
                                    continue block6;
                                }
                                case SECOND_IN_FIRST: {
                                    if (!f.equals(relPath.getKey()) && !FileUtil.isParentOf((FileObject)f, (FileObject)((FileObject)relPath.getKey()))) continue block6;
                                    clusters.remove(relPath.getKey());
                                    it.remove();
                                    break;
                                }
                                case UNRELATED: {
                                    break;
                                }
                                default: {
                                    throw new IllegalStateException();
                                }
                            }
                        }
                        cluster = new HashSet<FileObject>();
                        clusters.put(parent, cluster);
                        relPaths.put(parent, currentRelPath);
                    } else {
                        Iterator it = relPaths.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry relPath = it.next();
                            if (!f.equals(relPath.getKey()) && !FileUtil.isParentOf((FileObject)f, (FileObject)((FileObject)relPath.getKey()))) continue;
                            clusters.remove(relPath.getKey());
                            it.remove();
                        }
                    }
                    cluster.add(f);
                }
                for (FileObject parent : clusters.keySet()) {
                    Set cluster = (Set)clusters.get((Object)parent);
                    StringBuilder relativePath2 = (StringBuilder)relPaths.get((Object)parent);
                    if (relativePath2 == null || (finished = this.collect(cluster.toArray((T[])new FileObject[cluster.size()]), this.root, resources, allResources, stats, this.entry, FileObjectCrawler.createPathForRoot(this.root), relativePath2))) continue;
                    break;
                }
            } else if (this.files.length == 1 && (relativePath = this.getRelativePath(this.root, this.files[0].getParent())) != null) {
                finished = this.collect(this.files, this.root, resources, allResources, stats, this.entry, FileObjectCrawler.createPathForRoot(this.root), relativePath);
            }
        } else {
            finished = this.collect(this.root.getChildren(), this.root, resources, allResources, stats, this.entry, FileObjectCrawler.createPathForRoot(this.root), new StringBuilder());
        }
        long tm2 = System.currentTimeMillis();
        if (LOG.isLoggable(Level.FINE)) {
            String rootUrl = this.root.toURL().toString();
            LOG.log(Level.FINE, String.format("Up-to-date check of %d files under %s took %d ms", stats.filesCount, rootUrl, tm2 - tm1));
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.FINER, "File extensions histogram for {0}:", rootUrl);
                Stats.logHistogram(Level.FINER, stats.extensions);
                LOG.finer("----");
            }
            LOG.log(Level.FINE, "Symlink tests took {0}ms, {1} symlinks into root found.", new Object[]{stats.linkCheckTime, stats.linkCount});
        }
        return finished;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean collect(@NonNull FileObject[] fos, @NonNull FileObject root, @NonNull Collection<Indexable> resources, @NonNull Collection<Indexable> allResources, @NullAllowed Stats stats, @NullAllowed ClassPath.Entry entry, @NonNull Deque<File> path, @NonNull StringBuilder relativePathBuilder) {
        this.parkWhileSuspended();
        int parentPathEnd = relativePathBuilder.length();
        for (FileObject fo : fos) {
            if (this.isCancelled()) {
                return false;
            }
            if (!fo.isValid() || !this.isVisible(fo)) continue;
            relativePathBuilder.append(fo.getNameExt());
            boolean folder = fo.isFolder();
            if (folder) {
                relativePathBuilder.append('/');
            }
            String relativePath = relativePathBuilder.toString();
            try {
                if (entry != null && !entry.includes(relativePath)) continue;
                if (folder) {
                    File dir = null;
                    if (!path.isEmpty() && (dir = FileUtil.toFile((FileObject)fo)) != null && FileObjectCrawler.isLink(dir, path, stats)) continue;
                    if (dir != null) {
                        path.addLast(dir);
                    }
                    try {
                        if (this.collect(fo.getChildren(), root, resources, allResources, stats, entry, path, relativePathBuilder)) continue;
                        boolean bl = false;
                        return bl;
                    }
                    finally {
                        if (dir != null) {
                            path.removeLast();
                        }
                    }
                }
                if (stats != null) {
                    ++stats.filesCount;
                    Stats.inc(stats.extensions, fo.getExt());
                }
                Indexable indexable = this.createIndexable(new FileObjectIndexable(root, relativePath));
                allResources.add(indexable);
                if (this.isUpToDate(fo, relativePath)) continue;
                resources.add(indexable);
            }
            finally {
                relativePathBuilder.delete(parentPathEnd, relativePathBuilder.length());
            }
        }
        return true;
    }

    private StringBuilder getRelativePath(FileObject folder, FileObject fo) {
        String rp = FileUtil.getRelativePath((FileObject)folder, (FileObject)fo);
        if (rp != null) {
            StringBuilder relativePath = new StringBuilder(rp);
            if (relativePath.length() > 0) {
                relativePath.append('/');
            }
            return relativePath;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean isVisible(@NonNull FileObject fo) {
        try {
            boolean bl = VisibilityQuery.getDefault().isVisible(fo);
            return bl;
        }
        finally {
            FileObjectCrawler.setListenOnVisibility(true);
        }
    }

    private static /* varargs */ boolean supportsAllFiles(FileObject root, FileObject ... files) {
        for (FileObject file : files) {
            if (root != file) continue;
            return true;
        }
        return false;
    }

    private static boolean isSameFile(File check, File other) throws IOException {
        Path checkPath = check.toPath();
        if (Files.isSymbolicLink(checkPath)) {
            Path target = other.toPath();
            return target.toRealPath(new LinkOption[0]).equals(checkPath.toRealPath(new LinkOption[0]));
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean isLink(@NonNull File file, @NonNull Deque<? extends File> path, @NullAllowed Stats stats) {
        long st = System.currentTimeMillis();
        boolean hasLink = false;
        try {
            Iterator<? extends File> it = path.descendingIterator();
            while (it.hasNext()) {
                File pathElement = it.next();
                if (!file.getName().equals(pathElement.getName())) continue;
                try {
                    if (!(mockLinkTypes != null ? mockLinkTypes.get((Object)Pair.of((Object)pathElement, (Object)file)) != false : FileObjectCrawler.isSameFile(file, pathElement))) continue;
                    hasLink = true;
                }
                catch (IOException ioe) {
                    LOG.log(Level.INFO, "Cannot convert to cannonical files {0} and {1}", new Object[]{file, pathElement});
                    LOG.log(Level.FINE, null, ioe);
                }
                break;
            }
            boolean pathElement = hasLink;
            return pathElement;
        }
        finally {
            long et = System.currentTimeMillis();
            if (stats != null) {
                stats.linkCheckTime += et - st;
                if (hasLink) {
                    ++stats.linkCount;
                }
            }
        }
    }

    private static Deque<File> createPathForRoot(@NonNull FileObject root) {
        ArrayDeque<File> result = new ArrayDeque<File>();
        for (File file = FileUtil.toFile((FileObject)root); file != null; file = file.getParentFile()) {
            result.addFirst(file);
        }
        return result;
    }

    @NonNull
    private static PathRelation getFileRelation(@NonNull StringBuilder firstPath, @NonNull StringBuilder secondPath) {
        int min = Math.min(firstPath.length(), secondPath.length());
        for (int i = 0; i < min; ++i) {
            if (firstPath.charAt(i) == secondPath.charAt(i)) continue;
            return PathRelation.UNRELATED;
        }
        if (firstPath.length() > secondPath.length()) {
            assert (secondPath.length() == 0 || secondPath.charAt(secondPath.length() - 1) == '/');
            return PathRelation.FIRST_IN_SECOND;
        }
        if (firstPath.length() < secondPath.length()) {
            assert (firstPath.length() == 0 || firstPath.charAt(firstPath.length() - 1) == '/');
            return PathRelation.SECOND_IN_FIRST;
        }
        return PathRelation.EQUAL;
    }

    private static enum PathRelation {
        UNRELATED,
        EQUAL,
        FIRST_IN_SECOND,
        SECOND_IN_FIRST;
        

        private PathRelation() {
        }
    }

    private static final class Stats {
        public int filesCount;
        public long linkCheckTime;
        public int linkCount;
        public Map<String, Integer> extensions = new HashMap<String, Integer>();
        public Map<String, Integer> mimeTypes = new HashMap<String, Integer>();
        private static final Comparator<Integer> REVERSE = new Comparator<Integer>(){

            @Override
            public int compare(Integer o1, Integer o2) {
                return -1 * o1.compareTo(o2);
            }
        };

        private Stats() {
        }

        public static void inc(Map<String, Integer> m, String k) {
            Integer i = m.get(k);
            if (i == null) {
                m.put(k, 1);
            } else {
                m.put(k, i + 1);
            }
        }

        public static void logHistogram(Level level, Map<String, Integer> data) {
            TreeMap<Integer, TreeSet<String>> sortedMap = new TreeMap<Integer, TreeSet<String>>(REVERSE);
            for (String item : data.keySet()) {
                Integer freq = data.get(item);
                TreeSet<String> items = (TreeSet<String>)sortedMap.get(freq);
                if (items == null) {
                    items = new TreeSet<String>();
                    sortedMap.put(freq, items);
                }
                items.add(item);
            }
            for (Integer freq : sortedMap.keySet()) {
                for (String item2 : (Set)sortedMap.get(freq)) {
                    LOG.log(level, "{0}: {1}", new Object[]{item2, freq});
                }
            }
        }

    }

}

