/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.modules.Places
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.modules.Places;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public final class CacheFolder {
    private static final Logger LOG = Logger.getLogger(CacheFolder.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(CacheFolder.class.getName(), 1, false, false);
    private static final RequestProcessor.Task SAVER = RP.create((Runnable)new Saver());
    private static final int SLIDING_WINDOW = 500;
    private static final String SEGMENTS_FILE = "segments";
    private static final String SLICE_PREFIX = "s";
    private static FileObject cacheFolder;
    private static Properties segments;
    private static Map<String, String> invertedSegments;
    private static int index;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @SuppressWarnings(value={"LI_LAZY_INIT_UPDATE_STATIC"}, justification="Caller already holds a monitor")
    private static void loadSegments(FileObject folder) throws IOException {
        assert (Thread.holdsLock(CacheFolder.class));
        if (segments == null) {
            assert (folder != null);
            segments = new Properties();
            invertedSegments = new HashMap<String, String>();
            FileObject segmentsFile = folder.getFileObject("segments");
            if (segmentsFile != null) {
                InputStream in = segmentsFile.getInputStream();
                try {
                    segments.load(in);
                }
                finally {
                    in.close();
                }
            }
            for (Map.Entry entry : segments.entrySet()) {
                String segment = (String)entry.getKey();
                String root = (String)entry.getValue();
                invertedSegments.put(root, segment);
                try {
                    index = Math.max(index, Integer.parseInt(segment.substring("s".length())));
                }
                catch (NumberFormatException nfe) {
                    LOG.log(Level.FINE, null, nfe);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void storeSegments(FileObject folder) throws IOException {
        assert (Thread.holdsLock(CacheFolder.class));
        assert (folder != null);
        File _file = FileUtil.toFile((FileObject)folder);
        assert (_file != null);
        FileObject segmentsFile = FileUtil.createData((File)new File(_file, "segments"));
        OutputStream out = segmentsFile.getOutputStream();
        try {
            segments.store(out, null);
        }
        finally {
            out.close();
        }
    }

    public static synchronized URL getSourceRootForDataFolder(FileObject dataFolder) {
        FileObject segFolder = dataFolder.getParent();
        if (segFolder == null || !segFolder.equals((Object)cacheFolder)) {
            return null;
        }
        String source = segments.getProperty(dataFolder.getName());
        if (source != null) {
            try {
                return new URL(source);
            }
            catch (IOException ioe) {
                LOG.log(Level.FINE, null, ioe);
            }
        }
        return null;
    }

    public static FileObject getDataFolder(URL root) throws IOException {
        return CacheFolder.getDataFolder(root, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static FileObject getDataFolder(URL root, boolean onlyIfAlreadyExists) throws IOException {
        String rootName = root.toExternalForm();
        FileObject _cacheFolder = CacheFolder.getCacheFolder();
        Class<CacheFolder> class_ = CacheFolder.class;
        synchronized (CacheFolder.class) {
            CacheFolder.loadSegments(_cacheFolder);
            String slice = invertedSegments.get(rootName);
            if (slice == null) {
                if (onlyIfAlreadyExists) {
                    // ** MonitorExit[var5_4] (shouldn't be in output)
                    return null;
                }
                slice = "s" + ++index;
                while (segments.getProperty(slice) != null) {
                    slice = "s" + ++index;
                }
                segments.put(slice, rootName);
                invertedSegments.put(rootName, slice);
                SAVER.schedule(500);
            }
            // ** MonitorExit[var5_4] (shouldn't be in output)
            assert (slice != null);
            if (onlyIfAlreadyExists) {
                return cacheFolder.getFileObject(slice);
            }
            return FileUtil.createFolder((FileObject)_cacheFolder, (String)slice);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    public static Iterable<? extends FileObject> findRootsWithCacheUnderFolder(@NonNull FileObject folder) throws IOException {
        URL folderURL = folder.toURL();
        String prefix = folderURL.toExternalForm();
        Class<CacheFolder> class_ = CacheFolder.class;
        synchronized (CacheFolder.class) {
            FileObject _cacheFolder = CacheFolder.getCacheFolder();
            CacheFolder.loadSegments(_cacheFolder);
            HashMap<String, String> isdc = new HashMap<String, String>(invertedSegments);
            // ** MonitorExit[var5_3] (shouldn't be in output)
            LinkedList<FileObject> result = new LinkedList<FileObject>();
            for (Map.Entry<String, String> e : isdc.entrySet()) {
                FileObject fo;
                if (!e.getKey().startsWith(prefix) || (fo = URLMapper.findFileObject((URL)new URL(e.getKey()))) == null) continue;
                result.add(fo);
            }
            return result;
        }
    }

    public static synchronized FileObject getCacheFolder() {
        if (cacheFolder == null) {
            File cache = Places.getCacheSubdirectory((String)"index");
            if (!cache.isDirectory()) {
                throw new IllegalStateException("Indices cache folder " + cache.getAbsolutePath() + " is not a folder");
            }
            if (!cache.canRead()) {
                throw new IllegalStateException("Can't read from indices cache folder " + cache.getAbsolutePath());
            }
            if (!cache.canWrite()) {
                throw new IllegalStateException("Can't write to indices cache folder " + cache.getAbsolutePath());
            }
            cacheFolder = FileUtil.toFileObject((File)cache);
            if (cacheFolder == null) {
                throw new IllegalStateException("Can't convert indices cache folder " + cache.getAbsolutePath() + " to FileObject");
            }
        }
        return cacheFolder;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void setCacheFolder(FileObject folder) {
        SAVER.schedule(0);
        SAVER.waitFinished();
        Class<CacheFolder> class_ = CacheFolder.class;
        synchronized (CacheFolder.class) {
            assert (folder != null && folder.canRead() && folder.canWrite());
            cacheFolder = folder;
            segments = null;
            invertedSegments = null;
            index = 0;
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    private CacheFolder() {
    }

    static {
        index = 0;
    }

    private static class Saver
    implements Runnable {
        private Saver() {
        }

        @Override
        public void run() {
            try {
                final FileObject cf = CacheFolder.getCacheFolder();
                cf.getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    public void run() throws IOException {
                        Class<CacheFolder> class_ = CacheFolder.class;
                        synchronized (CacheFolder.class) {
                            if (segments == null) {
                                // ** MonitorExit[var1_1] (shouldn't be in output)
                                return;
                            }
                            CacheFolder.storeSegments(cf);
                            // ** MonitorExit[var1_1] (shouldn't be in output)
                            return;
                        }
                    }
                });
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }

    }

}

