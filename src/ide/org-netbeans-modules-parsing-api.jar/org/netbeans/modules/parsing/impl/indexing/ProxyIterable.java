/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

final class ProxyIterable<T>
implements Iterable<T> {
    private final Collection<? extends Iterable<? extends T>> delegates;
    private final boolean allowDuplicates;
    private final AtomicReference<List<T>> cache;

    public ProxyIterable(Collection<? extends Iterable<? extends T>> delegates) {
        this(delegates, true, false);
    }

    public ProxyIterable(Collection<? extends Iterable<? extends T>> delegates, boolean allowDuplicates) {
        this(delegates, allowDuplicates, false);
    }

    public ProxyIterable(Collection<? extends Iterable<? extends T>> delegates, boolean allowDuplicates, boolean cacheValues) {
        assert (delegates != null);
        this.delegates = delegates;
        this.allowDuplicates = allowDuplicates;
        this.cache = cacheValues ? new AtomicReference() : null;
    }

    @Override
    public Iterator<T> iterator() {
        List<T> _cache;
        List<T> list = _cache = this.cache == null ? null : this.cache.get();
        if (_cache != null) {
            return _cache.iterator();
        }
        return new ProxyIterator<T>(this.delegates.iterator(), this.allowDuplicates, this.cache);
    }

    public String toString() {
        return "ProxyIterable@" + Integer.toHexString(System.identityHashCode(this)) + " [" + this.delegates + "]";
    }

    private static final class ProxyIterator<T>
    implements Iterator<T> {
        private final Iterator<? extends Iterable<? extends T>> iterables;
        private final Set<T> seen;
        private final AtomicReference<List<T>> cacheRef;
        private final List<T> cache;
        private Iterator<? extends T> currentIterator;
        private T currentObject;

        public ProxyIterator(Iterator<? extends Iterable<? extends T>> iterables, boolean allowDuplicates, AtomicReference<List<T>> cacheRef) {
            assert (iterables != null);
            this.iterables = iterables;
            this.seen = allowDuplicates ? null : new HashSet();
            this.cacheRef = cacheRef;
            this.cache = this.cacheRef == null ? null : new ArrayList();
        }

        @Override
        public boolean hasNext() {
            boolean result;
            block5 : {
                if (this.currentObject != null) {
                    return true;
                }
                do {
                    if (this.currentIterator != null) {
                        while (this.currentIterator.hasNext()) {
                            T o = this.currentIterator.next();
                            if (this.seen != null && !this.seen.add(o)) continue;
                            this.currentObject = o;
                            break block5;
                        }
                    }
                    if (!this.iterables.hasNext()) break;
                    this.currentIterator = this.iterables.next().iterator();
                } while (true);
                this.currentIterator = null;
                this.currentObject = null;
            }
            boolean bl = result = this.currentObject != null;
            if (!result && this.cacheRef != null) {
                this.cacheRef.compareAndSet(null, this.cache);
            }
            return result;
        }

        @Override
        public T next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            T res = this.currentObject;
            this.currentObject = null;
            assert (res != null);
            if (this.cache != null) {
                this.cache.add(res);
            }
            return res;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}

