/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.analysis.Analyzer
 *  org.apache.lucene.analysis.KeywordAnalyzer
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.Convertors
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2$Transactional
 *  org.netbeans.modules.parsing.lucene.support.Index
 *  org.netbeans.modules.parsing.lucene.support.Index$Status
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.netbeans.modules.parsing.lucene.support.Queries
 *  org.netbeans.modules.parsing.lucene.support.Queries$QueryKind
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 */
package org.netbeans.modules.parsing.impl.indexing.lucene;

import java.io.IOException;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.search.Query;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.TransientUpdateSupport;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Convertors;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex2;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.openide.util.Exceptions;
import org.openide.util.Pair;

public final class LayeredDocumentIndex
implements DocumentIndex2.Transactional {
    private final DocumentIndex2.Transactional base;
    private final Set<String> filter = new HashSet<String>();
    private DocumentIndex2 overlay;

    LayeredDocumentIndex(@NonNull DocumentIndex2.Transactional base) {
        assert (base != null);
        this.base = base;
    }

    public void addDocument(IndexDocument document) {
        if (TransientUpdateSupport.isTransientUpdate()) {
            try {
                this.getOverlay().addDocument(document);
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        } else {
            this.base.addDocument(document);
        }
    }

    public void removeDocument(String primaryKey) {
        if (!TransientUpdateSupport.isTransientUpdate()) {
            this.base.removeDocument(primaryKey);
        }
    }

    public Index.Status getStatus() throws IOException {
        if (TransientUpdateSupport.isTransientUpdate()) {
            return Index.Status.VALID;
        }
        return this.base.getStatus();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void close() throws IOException {
        try {
            this.base.close();
        }
        finally {
            this.clearOverlay();
        }
    }

    public void store(boolean optimize) throws IOException {
        if (TransientUpdateSupport.isTransientUpdate()) {
            Pair<DocumentIndex2, Set<String>> ovl = this.getOverlayIfExists();
            if (ovl.first() != null) {
                ((DocumentIndex2)ovl.first()).store(false);
            }
        } else {
            this.base.store(optimize);
        }
    }

    public void rollback() throws IOException {
        if (TransientUpdateSupport.isTransientUpdate()) {
            this.clearOverlay();
        } else {
            this.base.rollback();
        }
    }

    public void commit() throws IOException {
        if (TransientUpdateSupport.isTransientUpdate()) {
            throw new UnsupportedOperationException("Transactions not supported for overlay.");
        }
        this.base.commit();
    }

    public void txStore() throws IOException {
        if (TransientUpdateSupport.isTransientUpdate()) {
            throw new UnsupportedOperationException("Transactions not supported for overlay.");
        }
        this.base.txStore();
    }

    public void clear() throws IOException {
        if (TransientUpdateSupport.isTransientUpdate()) {
            this.clearOverlay();
        } else {
            this.base.clear();
        }
    }

    public /* varargs */ Collection<? extends IndexDocument> query(String fieldName, String value, Queries.QueryKind kind, String ... fieldsToLoad) throws IOException, InterruptedException {
        return this.query(Queries.createQuery((String)fieldName, (String)fieldName, (String)value, (Queries.QueryKind)kind), Convertors.identity(), fieldsToLoad);
    }

    public /* varargs */ Collection<? extends IndexDocument> findByPrimaryKey(String primaryKeyValue, Queries.QueryKind kind, String ... fieldsToLoad) throws IOException, InterruptedException {
        Collection<? extends IndexDocument> br = this.base.findByPrimaryKey(primaryKeyValue, kind, fieldsToLoad);
        Pair<DocumentIndex2, Set<String>> ovl = this.getOverlayIfExists();
        if (ovl.first() == null) {
            return ovl.second() == null ? br : Filter.filter(br, (Set)ovl.second());
        }
        return new ProxyCollection<IndexDocument>(ovl.second() == null ? br : Filter.filter(br, (Set)ovl.second()), ((DocumentIndex2)ovl.first()).findByPrimaryKey(primaryKeyValue, kind, fieldsToLoad));
    }

    @NonNull
    public /* varargs */ <T> Collection<? extends T> query(@NonNull Query query, @NonNull Convertor<? super IndexDocument, ? extends T> convertor, @NullAllowed String ... fieldsToLoad) throws IOException, InterruptedException {
        Pair<DocumentIndex2, Set<String>> ovl = this.getOverlayIfExists();
        Convertor filterConvertor = ovl.second() == null ? convertor : Convertors.compose((Convertor)new Filter((Set)ovl.second()), convertor);
        Collection br = this.base.query(query, filterConvertor, fieldsToLoad);
        if (ovl.first() == null) {
            return br;
        }
        return new ProxyCollection(br, ((DocumentIndex2)ovl.first()).query(query, convertor, fieldsToLoad));
    }

    public void markKeyDirty(String primaryKey) {
        this.addToFilter(primaryKey);
        this.base.markKeyDirty(primaryKey);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeDirtyKeys(Collection<? extends String> dirtyKeys) {
        try {
            this.base.removeDirtyKeys(dirtyKeys);
        }
        finally {
            if (!TransientUpdateSupport.isTransientUpdate()) {
                this.clearOverlay();
            }
        }
    }

    public Collection<? extends String> getDirtyKeys() {
        return this.base.getDirtyKeys();
    }

    public boolean begin() {
        if (!TransientUpdateSupport.isTransientUpdate() && this.base instanceof Runnable) {
            ((Runnable)this.base).run();
            return true;
        }
        return false;
    }

    @NonNull
    private synchronized DocumentIndex2 getOverlay() throws IOException {
        if (this.overlay == null) {
            this.overlay = (DocumentIndex2)IndexManager.createDocumentIndex((Index)IndexManager.createMemoryIndex((Analyzer)new KeywordAnalyzer()));
        }
        return this.overlay;
    }

    @NonNull
    private synchronized Pair<DocumentIndex2, Set<String>> getOverlayIfExists() throws IOException {
        HashSet<String> f = this.filter.isEmpty() ? null : new HashSet<String>(this.filter);
        return Pair.of((Object)this.overlay, f);
    }

    private synchronized void addToFilter(@NonNull String primaryKey) {
        this.filter.add(primaryKey);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void clearOverlay() {
        this.filter.clear();
        if (this.overlay != null) {
            try {
                this.overlay.close();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            finally {
                this.overlay = null;
            }
        }
    }

    private static class ProxyIterator<E>
    implements Iterator<E> {
        private final Iterator<? extends E> base;
        private final Iterator<? extends E> overlay;

        ProxyIterator(@NonNull Iterator<? extends E> base, @NonNull Iterator<? extends E> overlay) {
            this.base = base;
            this.overlay = overlay;
        }

        @Override
        public boolean hasNext() {
            return this.base.hasNext() || this.overlay.hasNext();
        }

        @Override
        public E next() {
            return this.base.hasNext() ? this.base.next() : this.overlay.next();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Unmodifiable collection");
        }
    }

    private static class ProxyCollection<E>
    extends AbstractCollection<E> {
        private final Collection<? extends E> base;
        private final Collection<? extends E> overlay;

        ProxyCollection(@NonNull Collection<? extends E> base, @NonNull Collection<? extends E> overlay) {
            this.base = base;
            this.overlay = overlay;
        }

        @Override
        public Iterator<E> iterator() {
            return new ProxyIterator<E>(this.base.iterator(), this.overlay.iterator());
        }

        @Override
        public int size() {
            return this.base.size() + this.overlay.size();
        }
    }

    private static final class Filter
    implements Convertor<IndexDocument, IndexDocument> {
        private final Set<String> filter;

        Filter(@NonNull Set<String> filter) {
            assert (filter != null);
            assert (!filter.isEmpty());
            this.filter = filter;
        }

        @CheckForNull
        public IndexDocument convert(@NullAllowed IndexDocument p) {
            return Filter.filtered(p, this.filter) ? null : p;
        }

        @NonNull
        static Collection<? extends IndexDocument> filter(@NonNull Collection<? extends IndexDocument> base, @NonNull Set<String> filter) {
            assert (!filter.isEmpty());
            ArrayList<IndexDocument> res = new ArrayList<IndexDocument>(base.size());
            for (IndexDocument doc : base) {
                if (Filter.filtered(doc, filter)) continue;
                res.add(doc);
            }
            return res;
        }

        private static boolean filtered(@NullAllowed IndexDocument doc, @NonNull Set<? extends String> filter) {
            return doc == null ? true : filter.contains(doc.getPrimaryKey());
        }
    }

}

