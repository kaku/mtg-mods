/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.openide.filesystems.FileEvent
 *  org.openide.util.Pair
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.openide.filesystems.FileEvent;
import org.openide.util.Pair;

class FileEventLog
implements Runnable {
    private static final Logger LOG = Logger.getLogger(FileEventLog.class.getName());
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final ThreadLocal<Map<URL, Map<String, Pair<FileOp, RepositoryUpdater.Work>>>> changes = new ThreadLocal();

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public void record(FileOp operation, URL root, String relativePath, FileEvent event, RepositoryUpdater.Work work) {
        Map<String, Pair<FileOp, RepositoryUpdater.Work>> rootSlot;
        Map<URL, Map<String, Pair<FileOp, RepositoryUpdater.Work>>> myChanges;
        assert (operation != null);
        assert (root != null);
        assert (PathRegistry.noHostPart(root));
        if (relativePath == null) {
            relativePath = "";
        }
        if ((rootSlot = (myChanges = this.getChanges(true)).get(root)) == null) {
            rootSlot = new HashMap<String, Pair<FileOp, RepositoryUpdater.Work>>();
            myChanges.put(root, rootSlot);
        }
        rootSlot.put(relativePath, (Pair)Pair.of((Object)((Object)operation), (Object)work));
        event.runWhenDeliveryOver((Runnable)this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        try {
            this.commit();
        }
        finally {
            this.cleanUp();
        }
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private Map<URL, Map<String, Pair<FileOp, RepositoryUpdater.Work>>> getChanges(boolean create) {
        Map<URL, Map<String, Pair<FileOp, RepositoryUpdater.Work>>> res = this.changes.get();
        if (res == null && create) {
            res = new HashMap<URL, Map<String, Pair<FileOp, RepositoryUpdater.Work>>>();
            this.changes.set(res);
        }
        return res;
    }

    private void commit() {
        LinkedList<Object> first = new LinkedList<Object>();
        LinkedList<Object> rest = new LinkedList<Object>();
        IdentityHashMap<Object, Object> seenDelete = new IdentityHashMap<Object, Object>();
        Map<URL, Map<String, Pair<FileOp, RepositoryUpdater.Work>>> myChanges = this.getChanges(false);
        if (myChanges != null) {
            for (Map<String, Pair<FileOp, RepositoryUpdater.Work>> changesInRoot : myChanges.values()) {
                for (Pair<FileOp, RepositoryUpdater.Work> desc : changesInRoot.values()) {
                    if (desc.first() == FileOp.DELETE) {
                        if (seenDelete.containsKey(desc.second())) continue;
                        first.add(desc.second());
                        seenDelete.put(desc.second(), desc.second());
                        continue;
                    }
                    rest.add(desc.second());
                }
            }
        }
        RepositoryUpdater ru = RepositoryUpdater.getDefault();
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("SCHEDULING: " + first);
        }
        ru.scheduleWork(first);
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("SCHEDULING: " + rest);
        }
        ru.scheduleWork(rest);
    }

    private void cleanUp() {
        this.changes.remove();
    }

    public static enum FileOp {
        DELETE,
        CREATE;
        

        private FileOp() {
        }
    }

}

