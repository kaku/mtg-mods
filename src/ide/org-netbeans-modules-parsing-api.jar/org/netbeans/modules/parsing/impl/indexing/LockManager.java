/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockManager {
    private static final ReadWriteLock lock = new ReentrantReadWriteLock();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> T readLock(ExceptionAction<T> action) throws IOException, InterruptedException {
        lock.readLock().lock();
        try {
            T t = action.run();
            return t;
        }
        finally {
            lock.readLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> T writeLock(ExceptionAction<T> action) throws IOException, InterruptedException {
        lock.writeLock().lock();
        try {
            T t = action.run();
            return t;
        }
        finally {
            lock.writeLock().unlock();
        }
    }

    public static interface ExceptionAction<T> {
        public T run() throws IOException, InterruptedException;
    }

}

