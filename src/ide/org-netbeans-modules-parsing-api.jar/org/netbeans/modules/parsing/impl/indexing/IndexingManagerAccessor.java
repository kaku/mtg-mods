/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Exceptions
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl.indexing;

import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.openide.util.Exceptions;
import org.openide.util.Parameters;

public abstract class IndexingManagerAccessor {
    private static volatile IndexingManagerAccessor instance;
    public static volatile Boolean requiresReleaseOfCompletionLock;

    public static void setInstance(@NonNull IndexingManagerAccessor _instance) {
        Parameters.notNull((CharSequence)"_instance", (Object)_instance);
        instance = _instance;
    }

    public static synchronized IndexingManagerAccessor getInstance() {
        if (instance == null) {
            try {
                Class.forName(IndexingManager.class.getName(), true, IndexingManagerAccessor.class.getClassLoader());
                assert (instance != null);
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return instance;
    }

    protected abstract boolean isCalledFromRefreshIndexAndWait(@NonNull IndexingManager var1);

    public final boolean isCalledFromRefreshIndexAndWait() {
        return this.isCalledFromRefreshIndexAndWait(IndexingManager.getDefault());
    }

    public final boolean requiresReleaseOfCompletionLock() {
        Boolean result = requiresReleaseOfCompletionLock;
        return result != null ? result : SwingUtilities.isEventDispatchThread();
    }

    static {
        requiresReleaseOfCompletionLock = null;
    }
}

