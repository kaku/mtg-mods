/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

public final class TransientUpdateSupport {
    private static final ThreadLocal<Boolean> transientUpdate = new ThreadLocal();

    private TransientUpdateSupport() {
    }

    public static boolean isTransientUpdate() {
        return transientUpdate.get() == Boolean.TRUE;
    }

    public static void setTransientUpdate(boolean state) {
        transientUpdate.set(state);
    }
}

