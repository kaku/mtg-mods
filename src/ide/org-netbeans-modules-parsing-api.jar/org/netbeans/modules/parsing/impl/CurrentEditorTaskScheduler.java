/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.openide.filesystems.FileObject;

public abstract class CurrentEditorTaskScheduler
extends Scheduler {
    private JTextComponent currentEditor;

    public CurrentEditorTaskScheduler() {
        this.setEditor(EditorRegistry.focusedComponent());
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new AListener());
    }

    protected abstract void setEditor(JTextComponent var1);

    private class AListener
    implements PropertyChangeListener {
        private AListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            FileObject fileObject;
            Document document;
            String propName = evt.getPropertyName();
            if (propName == null || propName.equals("focusedDocument") || propName.equals("focusGained")) {
                JTextComponent editor = EditorRegistry.focusedComponent();
                if (editor == CurrentEditorTaskScheduler.this.currentEditor || editor != null && editor.getClientProperty("AsTextField") != null) {
                    return;
                }
                if (CurrentEditorTaskScheduler.this.currentEditor != null) {
                    CurrentEditorTaskScheduler.this.currentEditor.removePropertyChangeListener(this);
                }
                CurrentEditorTaskScheduler.this.currentEditor = editor;
                if (CurrentEditorTaskScheduler.this.currentEditor != null) {
                    Document document2 = CurrentEditorTaskScheduler.this.currentEditor.getDocument();
                    FileObject fileObject2 = Util.getFileObject(document2);
                    if (fileObject2 == null) {
                        return;
                    }
                    CurrentEditorTaskScheduler.this.currentEditor.addPropertyChangeListener(this);
                }
                CurrentEditorTaskScheduler.this.setEditor(CurrentEditorTaskScheduler.this.currentEditor);
            } else if (propName.equals("lastFocusedRemoved")) {
                if (CurrentEditorTaskScheduler.this.currentEditor != null) {
                    CurrentEditorTaskScheduler.this.currentEditor.removePropertyChangeListener(this);
                }
                CurrentEditorTaskScheduler.this.currentEditor = null;
                CurrentEditorTaskScheduler.this.setEditor(null);
            } else if (propName.equals("document") && CurrentEditorTaskScheduler.this.currentEditor != null && (fileObject = Util.getFileObject(document = CurrentEditorTaskScheduler.this.currentEditor.getDocument())) != null && fileObject.isValid()) {
                Source src = Source.create(document);
                CurrentEditorTaskScheduler.this.schedule(src, new SchedulerEvent(this){});
            }
        }

    }

}

