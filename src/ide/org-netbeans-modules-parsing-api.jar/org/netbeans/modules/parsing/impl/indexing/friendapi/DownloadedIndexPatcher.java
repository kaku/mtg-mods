/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.impl.indexing.friendapi;

import java.net.URL;
import org.netbeans.api.annotations.common.NonNull;

public interface DownloadedIndexPatcher {
    public boolean updateIndex(@NonNull URL var1, @NonNull URL var2);
}

