/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl;

import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Exceptions;

public abstract class ParserAccessor {
    private static volatile ParserAccessor instance;

    public static synchronized ParserAccessor getINSTANCE() {
        if (instance == null) {
            try {
                Class.forName(Parser.class.getName(), true, ParserAccessor.class.getClassLoader());
                assert (instance != null);
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return instance;
    }

    public static void setINSTANCE(ParserAccessor _instance) {
        assert (_instance != null);
        instance = _instance;
    }

    public abstract void invalidate(Parser.Result var1);
}

