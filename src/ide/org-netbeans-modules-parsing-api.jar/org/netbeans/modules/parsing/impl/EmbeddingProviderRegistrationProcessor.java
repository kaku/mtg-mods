/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.parsing.impl;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import org.netbeans.modules.parsing.impl.EmbeddingProviderFactory;
import org.netbeans.modules.parsing.spi.EmbeddingProvider;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class EmbeddingProviderRegistrationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(EmbeddingProvider.Registration.class.getCanonicalName());
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        for (Element e : roundEnv.getElementsAnnotatedWith(EmbeddingProvider.Registration.class)) {
            String targetMimeType;
            if (!e.getKind().isClass()) {
                throw new LayerGenerationException("Annotated Element has to be a class.", e);
            }
            EmbeddingProvider.Registration reg = e.getAnnotation(EmbeddingProvider.Registration.class);
            String mimeType = reg.mimeType();
            if (mimeType == null) {
                throw new LayerGenerationException("Mime type has to be given.", e);
            }
            if (!mimeType.isEmpty()) {
                mimeType = "" + '/' + mimeType;
            }
            if ((targetMimeType = reg.targetMimeType()) == null || targetMimeType.isEmpty()) {
                throw new LayerGenerationException("Target mime type has to be given.", e);
            }
            this.layer(new Element[]{e}).instanceFile("Editors" + mimeType, null, null).stringvalue("instanceOf", TaskFactory.class.getName()).methodvalue("instanceCreate", EmbeddingProviderFactory.class.getName(), "create").stringvalue("targetMimeType", targetMimeType).instanceAttribute("provider", EmbeddingProvider.class).write();
        }
        return true;
    }
}

