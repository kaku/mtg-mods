/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.CurrentEditorTaskScheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

public class CurrentDocumentScheduler
extends CurrentEditorTaskScheduler {
    private Document currentDocument;

    @Override
    protected void setEditor(JTextComponent editor) {
        if (editor != null) {
            Document document = editor.getDocument();
            if (this.currentDocument == document) {
                return;
            }
            this.currentDocument = document;
            Source source = Source.create(this.currentDocument);
            this.schedule(source, new SchedulerEvent(this){});
        } else {
            this.currentDocument = null;
            this.schedule(null, null);
        }
    }

    void schedule(Source source) {
        this.schedule(source, new SchedulerEvent(this){});
    }

    public String toString() {
        return "CurrentDocumentScheduler";
    }

    @Override
    protected SchedulerEvent createSchedulerEvent(SourceModificationEvent event) {
        if (event.getModifiedSource() == this.getSource()) {
            return new SchedulerEvent(this){};
        }
        return null;
    }

}

