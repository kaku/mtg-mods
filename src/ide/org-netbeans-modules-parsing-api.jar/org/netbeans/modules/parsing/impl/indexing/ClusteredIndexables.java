/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.Field
 *  org.apache.lucene.document.Field$Index
 *  org.apache.lucene.document.Field$Store
 *  org.apache.lucene.document.Fieldable
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.parsing.lucene.support.Convertor
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2$Transactional
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache$WithCustomIndexDocument
 *  org.netbeans.modules.parsing.lucene.support.IndexDocument
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.text.MessageFormat;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.InjectedTasksSupport;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.TransientUpdateSupport;
import org.netbeans.modules.parsing.impl.indexing.lucene.DocumentBasedIndexManager;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex2;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public final class ClusteredIndexables {
    public static final String FIELD_PRIMARY_KEY = "_sn";
    public static final String DELETE = "ci-delete-set";
    public static final String INDEX = "ci-index-set";
    private static final Logger LOG = Logger.getLogger(ClusteredIndexables.class.getName());
    private static final String ALL_MIME_TYPES = "";
    private static final String PROP_CACHE_HEAP_RATIO = "ClusteredIndexables.cacheHeapRatio";
    private static final double DEFAULT_CACHE_HEAP_RATIO = 0.1;
    private static final long DATA_CACHE_SIZE = (long)((double)Runtime.getRuntime().maxMemory() * ClusteredIndexables.getCacheHeapRatio());
    private final List<Indexable> indexables;
    private final BitSet sorted;
    private final Map<String, BitSet> mimeTypeClusters = new HashMap<String, BitSet>();
    private IndexedIterator currentIt;

    public ClusteredIndexables(@NonNull List<Indexable> indexables) {
        Parameters.notNull((CharSequence)"indexables", indexables);
        this.indexables = indexables;
        this.sorted = new BitSet(indexables.size());
    }

    @NonNull
    public Iterable<Indexable> getIndexablesFor(@NullAllowed String mimeType) {
        if (mimeType == null) {
            mimeType = "";
        }
        if (mimeType.length() == 0) {
            return new AllIndexables();
        }
        BitSet cluster = this.mimeTypeClusters.get(mimeType);
        if (cluster == null) {
            cluster = new BitSet();
            int i = this.sorted.nextClearBit(0);
            while (i < this.indexables.size()) {
                Indexable indexable = this.indexables.get(i);
                if (SPIAccessor.getInstance().isTypeOf(indexable, mimeType)) {
                    cluster.set(i);
                    this.sorted.set(i);
                }
                i = this.sorted.nextClearBit(i + 1);
            }
            this.mimeTypeClusters.put(mimeType, cluster);
        }
        return new BitSetIterable(cluster);
    }

    @NonNull
    public static AttachableDocumentIndexCache createDocumentIndexCache() {
        return new DocumentIndexCacheImpl();
    }

    @NonNull
    public static IndexDocument createDocument(@NonNull String primaryKey) {
        Parameters.notNull((CharSequence)"primaryKey", (Object)primaryKey);
        return new MemIndexDocument(primaryKey);
    }

    @NonNull
    private Indexable get(int index) {
        return this.indexables.get(index);
    }

    private int current() {
        IndexedIterator tmpIt = this.currentIt;
        return tmpIt == null ? -1 : tmpIt.index();
    }

    private static double getCacheHeapRatio() {
        String sval = System.getProperty("ClusteredIndexables.cacheHeapRatio");
        if (sval != null) {
            try {
                double val = Double.valueOf(sval);
                if (val < 0.05 || val > 1.0) {
                    throw new NumberFormatException();
                }
                return val;
            }
            catch (NumberFormatException nfe) {
                LOG.log(Level.INFO, "Invalid value of {0} property: {1}", new Object[]{"ClusteredIndexables.cacheHeapRatio", sval});
            }
        }
        return 0.1;
    }

    static final class DocumentStore
    extends AbstractCollection<IndexDocument> {
        private static final int INITIAL_DOC_COUNT = 100;
        private static final int INITIAL_DATA_SIZE = 1024;
        private final AtomicReference<Thread> ownerThread = new AtomicReference();
        private final long dataCacheSize;
        private final Map<String, Integer> fieldNames;
        private int[] docs;
        private char[] data;
        private int nameIndex;
        private int docsPointer;
        private int dataPointer;
        private int size;
        private MemIndexDocument overflowDocument;

        DocumentStore(long dataCacheSize) {
            this.dataCacheSize = dataCacheSize;
            this.fieldNames = new LinkedHashMap<String, Integer>();
            this.docs = new int[100];
            this.data = new char[1024];
            LOG.log(Level.FINE, "DocumentStore flush size: {0}", dataCacheSize);
        }

        @Override
        public boolean add(@NonNull IndexDocument doc) {
            this.addDocument(doc, true);
            return true;
        }

        boolean addDocument(@NonNull IndexDocument doc) {
            return this.addDocument(doc, false);
        }

        private boolean addDocument(@NonNull IndexDocument doc, boolean compat) {
            assert (this.sameThread());
            boolean res = false;
            if (!(doc instanceof MemIndexDocument)) {
                throw new IllegalArgumentException();
            }
            MemIndexDocument mdoc = (MemIndexDocument)doc;
            int oldDocsPointer = this.docsPointer;
            int oldDataPointer = this.dataPointer;
            for (Fieldable fld : mdoc.getFields()) {
                int index;
                String fldName = fld.name();
                boolean stored = fld.isStored();
                boolean indexed = fld.isIndexed();
                String fldValue = fld.stringValue();
                Integer indexBoxed = this.fieldNames.get(fldName);
                if (indexBoxed == null) {
                    index = this.nameIndex++;
                    this.fieldNames.put(fldName, index);
                } else {
                    index = indexBoxed;
                }
                index = index << 3 | (stored ? 4 : 0) | (indexed ? 2 : 0) | 1;
                if (this.docs.length < this.docsPointer + 2) {
                    this.docs = Arrays.copyOf(this.docs, this.docs.length << 1);
                }
                this.docs[this.docsPointer] = index;
                this.docs[this.docsPointer + 1] = this.dataPointer;
                this.docsPointer += 2;
                if (this.data.length < this.dataPointer + fldValue.length()) {
                    int newDataLength = DocumentStore.newLength(this.data.length, this.dataPointer + fldValue.length());
                    boolean bl = res = (long)(newDataLength << 1) > this.dataCacheSize;
                    if (res && !compat) {
                        this.rollBack(oldDocsPointer, oldDataPointer, newDataLength, mdoc);
                        return res;
                    }
                    try {
                        LOG.log(Level.FINEST, "alloc");
                        this.data = Arrays.copyOf(this.data, newDataLength);
                    }
                    catch (OutOfMemoryError ooe) {
                        if (compat) {
                            throw ooe;
                        }
                        this.rollBack(oldDocsPointer, oldDataPointer, newDataLength, mdoc);
                        return true;
                    }
                    LOG.log(Level.FINE, "New data size: {0}", new Object[]{this.data.length});
                }
                fldValue.getChars(0, fldValue.length(), this.data, this.dataPointer);
                this.dataPointer += fldValue.length();
            }
            if (this.docs.length < this.docsPointer + 1) {
                this.docs = Arrays.copyOf(this.docs, this.docs.length << 1);
            }
            this.docs[this.docsPointer++] = 0;
            ++this.size;
            mdoc.consumed = true;
            return res;
        }

        @Override
        public Iterator<IndexDocument> iterator() {
            return new It();
        }

        @Override
        public void clear() {
            assert (this.sameThread());
            this.fieldNames.clear();
            this.docs = new int[100];
            this.data = new char[1024];
            this.docsPointer = 0;
            this.dataPointer = 0;
            this.nameIndex = 0;
            this.size = 0;
        }

        @Override
        public int size() {
            return this.size + (this.overflowDocument == null ? 0 : 1);
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Remove not supported.");
        }

        private static int newLength(int currentLength, int minimalLength) {
            while ((currentLength <<= 1) < minimalLength) {
            }
            return currentLength;
        }

        private void rollBack(int oldDocsPointer, int oldDataPointer, int newDataLength, @NonNull MemIndexDocument mdoc) {
            Parameters.notNull((CharSequence)"mdoc", (Object)mdoc);
            assert (this.overflowDocument == null);
            this.overflowDocument = mdoc;
            this.docsPointer = oldDocsPointer;
            this.dataPointer = oldDataPointer;
            LOG.log(Level.FINE, "Data size ({0}bytes) overflow -> flush", new Object[]{newDataLength});
        }

        private boolean sameThread() {
            Thread me = Thread.currentThread();
            Thread t = this.ownerThread.get();
            if (t == null) {
                if (this.ownerThread.compareAndSet((Thread)null, me)) {
                    return true;
                }
                t = this.ownerThread.get();
            }
            return me.equals(t);
        }

        private class It
        implements Iterator<IndexDocument> {
            private int cur;
            private final List<String> names;
            private final ReusableIndexDocument doc;

            It() {
                this.cur = 0;
                this.names = new ArrayList(DocumentStore.this.fieldNames.keySet());
                this.doc = new ReusableIndexDocument();
            }

            @Override
            public boolean hasNext() {
                return this.cur < DocumentStore.this.docsPointer || DocumentStore.this.overflowDocument != null;
            }

            @Override
            public IndexDocument next() {
                assert (DocumentStore.this.sameThread());
                if (this.cur < DocumentStore.this.docsPointer) {
                    int nameIndex;
                    this.doc.clear();
                    while ((nameIndex = DocumentStore.this.docs[this.cur++]) != 0) {
                        boolean stored = (nameIndex & 4) == 4;
                        boolean indexed = (nameIndex & 2) == 2;
                        int dataStart = DocumentStore.this.docs[this.cur++];
                        int dataEnd = DocumentStore.this.docs[this.cur] != 0 ? DocumentStore.this.docs[this.cur + 1] : (this.cur + 1 == DocumentStore.this.docsPointer ? DocumentStore.this.dataPointer : DocumentStore.this.docs[this.cur + 2]);
                        String value = new String(DocumentStore.this.data, dataStart, dataEnd - dataStart);
                        this.doc.addPair(this.names.get(nameIndex >>>= 3), value, indexed, stored);
                    }
                    return this.doc;
                }
                if (DocumentStore.this.overflowDocument != null) {
                    ReusableIndexDocument res = new ReusableIndexDocument(DocumentStore.this.overflowDocument);
                    DocumentStore.this.overflowDocument = null;
                    return res;
                }
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
            }
        }

    }

    private static final class ReusableIndexDocument
    implements IndexDocument {
        private final Document doc = new Document();

        ReusableIndexDocument() {
        }

        ReusableIndexDocument(@NonNull MemIndexDocument memDoc) {
            Parameters.notNull((CharSequence)"memDoc", (Object)memDoc);
            for (Fieldable field : memDoc.getFields()) {
                this.doc.add(field);
            }
        }

        public String getPrimaryKey() {
            return this.doc.get("_sn");
        }

        public String getValue(String key) {
            return this.doc.get(key);
        }

        public String[] getValues(String key) {
            return this.doc.getValues(key);
        }

        public void addPair(String key, String value, boolean searchable, boolean stored) {
            this.doc.add((Fieldable)new Field(key, value, stored ? Field.Store.YES : Field.Store.NO, searchable ? Field.Index.NOT_ANALYZED_NO_NORMS : Field.Index.NO));
        }

        void clear() {
            this.doc.getFields().clear();
        }
    }

    private static final class MemIndexDocument
    implements IndexDocument {
        private static final String[] EMPTY = new String[0];
        private final List<Fieldable> fields = new ArrayList<Fieldable>();
        boolean consumed;

        MemIndexDocument(@NonNull String primaryKey) {
            Parameters.notNull((CharSequence)"primaryKey", (Object)primaryKey);
            this.fields.add(this.sourceNameField(primaryKey));
        }

        public List<Fieldable> getFields() {
            return this.fields;
        }

        public String getPrimaryKey() {
            return this.getValue("_sn");
        }

        public void addPair(String key, String value, boolean searchable, boolean stored) {
            if (this.consumed) {
                throw new IllegalStateException("Modifying Document after adding it into index.");
            }
            Field field = new Field(key, value, stored ? Field.Store.YES : Field.Store.NO, searchable ? Field.Index.NOT_ANALYZED_NO_NORMS : Field.Index.NO);
            this.fields.add((Fieldable)field);
        }

        public String getValue(String key) {
            for (Fieldable field : this.fields) {
                if (!field.name().equals(key)) continue;
                return field.stringValue();
            }
            return null;
        }

        public String[] getValues(String key) {
            ArrayList<String> result = new ArrayList<String>();
            for (Fieldable field : this.fields) {
                if (!field.name().equals(key)) continue;
                result.add(field.stringValue());
            }
            return result.toArray(result.isEmpty() ? EMPTY : new String[result.size()]);
        }

        private Fieldable sourceNameField(@NonNull String primaryKey) {
            return new Field("_sn", primaryKey, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
        }
    }

    private static class RemovedCollection
    extends AbstractCollection<String> {
        private final List<? extends String> outOfOrder;
        private final ClusteredIndexables deleteIndexables;
        private final BitSet deleteFromDeleted;
        private final ClusteredIndexables indexIndexables;
        private final BitSet deleteFromIndex;
        private final Pair<Long, StackTraceElement[]> attachDeleteStackTrace;
        private final Pair<Long, StackTraceElement[]> attachIndexStackTrace;
        private final Pair<Long, StackTraceElement[]> detachDeleteStackTrace;
        private final Pair<Long, StackTraceElement[]> detachIndexStackTrace;

        RemovedCollection(@NonNull List<? extends String> outOfOrder, @NullAllowed ClusteredIndexables deleteIndexables, @NonNull BitSet deleteFromDeleted, @NullAllowed ClusteredIndexables indexIndexables, @NonNull BitSet deleteFromIndex, @NullAllowed Pair<Long, StackTraceElement[]> attachDeleteStackTrace, @NullAllowed Pair<Long, StackTraceElement[]> attachIndexStackTrace, @NullAllowed Pair<Long, StackTraceElement[]> detachDeleteStackTrace, @NullAllowed Pair<Long, StackTraceElement[]> detachIndexStackTrace) {
            assert (outOfOrder != null);
            assert (deleteFromDeleted != null);
            assert (deleteFromIndex != null);
            this.outOfOrder = outOfOrder;
            this.deleteIndexables = deleteIndexables;
            this.deleteFromDeleted = deleteFromDeleted;
            this.indexIndexables = indexIndexables;
            this.deleteFromIndex = deleteFromIndex;
            this.attachDeleteStackTrace = attachDeleteStackTrace;
            this.attachIndexStackTrace = attachIndexStackTrace;
            this.detachDeleteStackTrace = detachDeleteStackTrace;
            this.detachIndexStackTrace = detachIndexStackTrace;
        }

        @Override
        public Iterator<String> iterator() {
            return new It(this.outOfOrder.iterator(), this.deleteIndexables, this.deleteFromDeleted, this.indexIndexables, this.deleteFromIndex, this.attachDeleteStackTrace, this.attachIndexStackTrace, this.detachDeleteStackTrace, this.detachIndexStackTrace);
        }

        @Override
        public int size() {
            return this.outOfOrder.size() + this.deleteFromDeleted.cardinality() + this.deleteFromIndex.cardinality();
        }

        @Override
        public boolean isEmpty() {
            return this.outOfOrder.isEmpty() && this.deleteFromDeleted.isEmpty() && this.deleteFromIndex.isEmpty();
        }

        private static class It
        implements Iterator<String> {
            private final Iterator<? extends String> outOfOrderIt;
            private final ClusteredIndexables deleteIndexables;
            private final BitSet deleteFromDeleted;
            private final ClusteredIndexables indexIndexables;
            private final BitSet deleteFromIndex;
            private int state;
            private int index;
            private String current;
            private final Pair<Long, StackTraceElement[]> attachDeleteStackTrace;
            private final Pair<Long, StackTraceElement[]> attachIndexStackTrace;
            private final Pair<Long, StackTraceElement[]> detachDeleteStackTrace;
            private final Pair<Long, StackTraceElement[]> detachIndexStackTrace;

            It(@NonNull Iterator<? extends String> outOfOrderIt, @NullAllowed ClusteredIndexables deleteIndexables, @NonNull BitSet deleteFromDeleted, @NullAllowed ClusteredIndexables indexIndexables, @NonNull BitSet deleteFromIndex, @NullAllowed Pair<Long, StackTraceElement[]> attachDeleteStackTrace, @NullAllowed Pair<Long, StackTraceElement[]> attachIndexStackTrace, @NullAllowed Pair<Long, StackTraceElement[]> detachDeleteStackTrace, @NullAllowed Pair<Long, StackTraceElement[]> detachIndexStackTrace) {
                this.outOfOrderIt = outOfOrderIt;
                this.deleteIndexables = deleteIndexables;
                this.deleteFromDeleted = deleteFromDeleted;
                this.indexIndexables = indexIndexables;
                this.deleteFromIndex = deleteFromIndex;
                this.attachDeleteStackTrace = attachDeleteStackTrace;
                this.attachIndexStackTrace = attachIndexStackTrace;
                this.detachDeleteStackTrace = detachDeleteStackTrace;
                this.detachIndexStackTrace = detachIndexStackTrace;
            }

            /*
             * Unable to fully structure code
             * Enabled aggressive block sorting
             * Enabled unnecessary exception pruning
             * Enabled aggressive exception aggregation
             * Lifted jumps to return sites
             */
            @Override
            public boolean hasNext() {
                if (this.current != null) {
                    return true;
                }
                switch (this.state) {
                    case 0: {
                        if (this.outOfOrderIt.hasNext()) {
                            this.current = this.outOfOrderIt.next();
                            return true;
                        }
                        this.index = -1;
                        this.state = 1;
                    }
                    case 1: {
                        this.index = this.deleteFromDeleted.nextSetBit(this.index + 1);
                        if (this.index < 0) ** GOTO lbl22
                        if (this.deleteIndexables == null) {
                            It.throwIllegalState("No deleteIndexables", this.attachDeleteStackTrace, this.detachDeleteStackTrace);
                        }
                        try {
                            file = ClusteredIndexables.access$600(this.deleteIndexables, this.index);
                            this.current = file.getRelativePath();
                            return true;
                        }
                        catch (IndexOutOfBoundsException e) {
                            It.throwIllegalState("Wrong deleteIndexables", this.attachDeleteStackTrace, this.detachDeleteStackTrace);
                            ** GOTO lbl24
                        }
lbl22: // 1 sources:
                        this.index = -1;
                        this.state = 2;
                    }
lbl24: // 3 sources:
                    case 2: {
                        this.index = this.deleteFromIndex.nextSetBit(this.index + 1);
                        if (this.index >= 0) {
                            if (this.indexIndexables == null) {
                                It.throwIllegalState("No indexIndexables", this.attachIndexStackTrace, this.detachIndexStackTrace);
                            }
                            try {
                                file = ClusteredIndexables.access$600(this.indexIndexables, this.index);
                                this.current = file.getRelativePath();
                                return true;
                            }
                            catch (IndexOutOfBoundsException e) {
                                It.throwIllegalState("Wrong indexIndexables", this.attachIndexStackTrace, this.detachIndexStackTrace);
                                return false;
                            }
                        }
                        this.index = -1;
                        this.state = 3;
                    }
                }
                return false;
            }

            @Override
            public String next() {
                if (!this.hasNext()) {
                    throw new NoSuchElementException();
                }
                String res = this.current;
                assert (res != null);
                this.current = null;
                return res;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Immutable collection");
            }

            private static void throwIllegalState(@NonNull String reason, @NullAllowed Pair<Long, StackTraceElement[]> attach, @NullAllowed Pair<Long, StackTraceElement[]> detach) {
                Object[] arrobject = new Object[5];
                arrobject[0] = reason;
                arrobject[1] = attach == null ? null : (Long)attach.first();
                arrobject[2] = attach == null ? null : Arrays.asList((Object[])attach.second());
                arrobject[3] = detach == null ? null : (Long)detach.first();
                arrobject[4] = detach == null ? null : Arrays.asList((Object[])detach.second());
                throw new IllegalStateException(MessageFormat.format("{0} : Attached at: {1} by: {2}, Detached at: {3} by: {4}", arrobject));
            }
        }

    }

    private static final class ClearReference
    extends SoftReference<Collection[]>
    implements Runnable,
    Callable<Void> {
        private final DocumentIndexCacheImpl owner;
        private final AtomicInteger state = new AtomicInteger();

        public ClearReference(@NonNull Collection[] data, @NonNull DocumentIndexCacheImpl owner) {
            super(data, Utilities.activeReferenceQueue());
            Parameters.notNull((CharSequence)"data", (Object)data);
            Parameters.notNull((CharSequence)"owner", (Object)owner);
            this.owner = owner;
        }

        @Override
        public void run() {
            if (!this.state.compareAndSet(0, 1)) {
                throw new IllegalStateException(Integer.toString(this.state.get()));
            }
            InjectedTasksSupport.enqueueTask(this);
            LOG.log(Level.FINEST, "Reference Task Enqueued for: {0}", this.owner);
        }

        @Override
        public Void call() throws Exception {
            if (!this.state.compareAndSet(1, 2)) {
                throw new IllegalStateException(Integer.toString(this.state.get()));
            }
            DocumentIndex2.Transactional txIndex = DocumentBasedIndexManager.getDefault().getIndex((DocumentIndexCache)this.owner);
            if (txIndex != null) {
                txIndex.txStore();
            }
            LOG.log(Level.FINEST, "Reference Task Executed for: {0}", this.owner);
            return null;
        }
    }

    private static final class DocumentIndexCacheImpl
    implements AttachableDocumentIndexCache {
        private static final Convertor<IndexDocument, Document> ADD_CONVERTOR = new Convertor<IndexDocument, Document>(){

            @NonNull
            public Document convert(@NonNull IndexDocument doc) {
                ReusableIndexDocument rdoc = (ReusableIndexDocument)doc;
                return rdoc.doc;
            }
        };
        private ClusteredIndexables deleteIndexables;
        private ClusteredIndexables indexIndexables;
        private BitSet deleteFromDeleted;
        private BitSet deleteFromIndex;
        private DocumentStore toAdd;
        private List<String> toDeleteOutOfOrder;
        private Reference<Collection[]> dataRef;
        private volatile Pair<Long, StackTraceElement[]> attachDeleteStackTrace;
        private volatile Pair<Long, StackTraceElement[]> attachIndexStackTrace;
        private volatile Pair<Long, StackTraceElement[]> detachDeleteStackTrace;
        private volatile Pair<Long, StackTraceElement[]> detachIndexStackTrace;

        private DocumentIndexCacheImpl() {
        }

        @Override
        public void attach(@NonNull String mode, @NonNull ClusteredIndexables ci) {
            Parameters.notNull((CharSequence)"mode", (Object)mode);
            Parameters.notNull((CharSequence)"ci", (Object)ci);
            if (TransientUpdateSupport.isTransientUpdate()) {
                return;
            }
            if ("ci-delete-set".equals(mode)) {
                DocumentIndexCacheImpl.ensureNotReBound(this.deleteIndexables, ci);
                if (!ci.equals(this.deleteIndexables)) {
                    this.deleteIndexables = ci;
                    this.attachDeleteStackTrace = Pair.of((Object)System.nanoTime(), (Object)Thread.currentThread().getStackTrace());
                    this.detachDeleteStackTrace = null;
                }
            } else if ("ci-index-set".equals(mode)) {
                DocumentIndexCacheImpl.ensureNotReBound(this.indexIndexables, ci);
                if (!ci.equals(this.indexIndexables)) {
                    this.indexIndexables = ci;
                    this.attachIndexStackTrace = Pair.of((Object)System.nanoTime(), (Object)Thread.currentThread().getStackTrace());
                    this.detachIndexStackTrace = null;
                }
            } else {
                throw new IllegalArgumentException(mode);
            }
        }

        @Override
        public void detach() {
            if (TransientUpdateSupport.isTransientUpdate()) {
                return;
            }
            this.detachDeleteStackTrace = this.detachIndexStackTrace = Pair.of((Object)System.nanoTime(), (Object)Thread.currentThread().getStackTrace());
            this.clear();
            this.deleteIndexables = null;
            this.indexIndexables = null;
        }

        public boolean addDocument(IndexDocument document) {
            if (!(document instanceof MemIndexDocument)) {
                throw new IllegalArgumentException(document.getClass().getName());
            }
            boolean shouldFlush = this.init();
            DocumentIndexCacheImpl.handleDelete(this.indexIndexables, this.deleteFromIndex, this.toDeleteOutOfOrder, document.getPrimaryKey());
            return shouldFlush |= this.toAdd.addDocument(document);
        }

        public boolean removeDocument(String primaryKey) {
            boolean shouldFlush = this.init();
            DocumentIndexCacheImpl.handleDelete(this.deleteIndexables, this.deleteFromDeleted, this.toDeleteOutOfOrder, primaryKey);
            return shouldFlush;
        }

        public void clear() {
            this.toAdd = null;
            this.toDeleteOutOfOrder = null;
            this.deleteFromDeleted = null;
            this.deleteFromIndex = null;
            this.dataRef = null;
        }

        public Collection<? extends String> getRemovedKeys() {
            return this.toDeleteOutOfOrder != null ? new RemovedCollection(this.toDeleteOutOfOrder, this.deleteIndexables, this.deleteFromDeleted, this.indexIndexables, this.deleteFromIndex, this.attachDeleteStackTrace, this.attachIndexStackTrace, this.detachDeleteStackTrace, this.detachIndexStackTrace) : Collections.emptySet();
        }

        public Collection<? extends IndexDocument> getAddedDocuments() {
            return this.toAdd != null ? this.toAdd : Collections.emptySet();
        }

        public Convertor<IndexDocument, Document> createAddConvertor() {
            return ADD_CONVERTOR;
        }

        public Convertor<Document, IndexDocument> createQueryConvertor() {
            return null;
        }

        private static void ensureNotReBound(@NullAllowed ClusteredIndexables oldCi, @NonNull ClusteredIndexables newCi) {
            if (oldCi != null && !oldCi.equals(newCi)) {
                throw new IllegalStateException(String.format("Cannot bind to ClusteredIndexables(%d), already bound to ClusteredIndexables(%d)", System.identityHashCode(newCi), System.identityHashCode(oldCi)));
            }
        }

        private static void handleDelete(@NullAllowed ClusteredIndexables ci, @NonNull BitSet bs, @NonNull List<? super String> toDelete, @NonNull String primaryKey) {
            int index = DocumentIndexCacheImpl.isCurrent(ci, primaryKey);
            if (index >= 0) {
                bs.set(index);
            } else {
                toDelete.add(primaryKey);
            }
        }

        private static int isCurrent(@NullAllowed ClusteredIndexables ci, @NonNull String primaryKey) {
            if (ci == null) {
                return -1;
            }
            int currentIndex = ci.current();
            if (currentIndex == -1) {
                return -1;
            }
            Indexable currentIndexable = ci.get(currentIndex);
            if (primaryKey.equals(currentIndexable.getRelativePath())) {
                return currentIndex;
            }
            return -1;
        }

        private boolean init() {
            if (this.toAdd == null || this.toDeleteOutOfOrder == null) {
                assert (this.toAdd == null && this.toDeleteOutOfOrder == null && this.deleteFromDeleted == null && this.deleteFromIndex == null);
                assert (this.dataRef == null);
                this.toAdd = new DocumentStore(DATA_CACHE_SIZE);
                this.toDeleteOutOfOrder = new ArrayList<String>();
                this.deleteFromDeleted = new BitSet();
                this.deleteFromIndex = new BitSet();
                this.dataRef = new ClearReference(new Collection[]{this.toAdd, this.toDeleteOutOfOrder}, this);
            }
            return this.dataRef.get() == null;
        }

    }

    private final class BitSetIterable
    implements Iterable<Indexable> {
        private final BitSet bs;

        BitSetIterable(BitSet bs) {
            this.bs = bs;
        }

        @NonNull
        @Override
        public Iterator<Indexable> iterator() {
            return ClusteredIndexables.this.currentIt = new BitSetIterator(this.bs);
        }
    }

    private final class BitSetIterator
    implements IndexedIterator<Indexable> {
        private final BitSet bs;
        private int index;

        BitSetIterator(BitSet bs) {
            this.bs = bs;
            this.index = -1;
        }

        @Override
        public boolean hasNext() {
            return this.bs.nextSetBit(this.index + 1) >= 0;
        }

        @Override
        public Indexable next() {
            int tmp = this.bs.nextSetBit(this.index + 1);
            if (tmp < 0) {
                throw new NoSuchElementException();
            }
            this.index = tmp;
            return (Indexable)ClusteredIndexables.this.indexables.get(tmp);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Immutable type");
        }

        @Override
        public int index() {
            return this.index;
        }
    }

    private final class AllIndexables
    implements Iterable<Indexable> {
        private AllIndexables() {
        }

        @Override
        public Iterator<Indexable> iterator() {
            return ClusteredIndexables.this.currentIt = new AllIndexablesIt(ClusteredIndexables.this.indexables.iterator());
        }
    }

    private static final class AllIndexablesIt
    implements IndexedIterator<Indexable> {
        private final Iterator<? extends Indexable> delegate;
        private int index = -1;

        AllIndexablesIt(Iterator<? extends Indexable> delegate) {
            this.delegate = delegate;
        }

        @Override
        public boolean hasNext() {
            return this.delegate.hasNext();
        }

        @Override
        public Indexable next() {
            Indexable res = this.delegate.next();
            ++this.index;
            return res;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Immutable type");
        }

        @Override
        public int index() {
            return this.index;
        }
    }

    private static interface IndexedIterator<T>
    extends Iterator<T> {
        public int index();
    }

    public static interface AttachableDocumentIndexCache
    extends DocumentIndexCache.WithCustomIndexDocument {
        public void attach(@NonNull String var1, @NonNull ClusteredIndexables var2);

        public void detach();
    }

}

