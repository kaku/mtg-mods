/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.EditableProperties
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.EditableProperties;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

final class ArchiveTimeStamps {
    static final int SAVE_DELAY = 2500;
    private static final String ARCHIVE_TIME_STAMPS = "archives.properties";
    private static final String PROP_BINARY_INDEXER_STATE = "ArchiveTimeStamps.indexerState";
    private static final Pair<Long, Map<Pair<String, Integer>, Integer>> FORCE_RESCAN = Pair.of((Object)0, Collections.emptyMap());
    private static final String SEPARATOR = ":";
    private static final RequestProcessor RP = new RequestProcessor(ArchiveTimeStamps.class.getName(), 1, false, false);
    private static final Saver saver = new Saver();
    private static final RequestProcessor.Task saverTask = RP.create((Runnable)saver);
    private static final Logger LOG = Logger.getLogger(ArchiveTimeStamps.class.getName());
    private static Reference<? extends Store> cachedStore;

    private ArchiveTimeStamps() {
    }

    @NonNull
    static Pair<Long, Map<Pair<String, Integer>, Integer>> getLastModified(@NonNull URL archiveFile) throws IOException {
        Parameters.notNull((CharSequence)"archiveFile", (Object)archiveFile);
        Store store = ArchiveTimeStamps.getStore();
        assert (store != null);
        return ArchiveTimeStamps.parse(store.get(archiveFile.toExternalForm()));
    }

    static void setLastModified(@NonNull URL archiveFile, @NonNull Pair<Long, Map<Pair<String, Integer>, Integer>> timeStamp) throws IOException {
        Parameters.notNull((CharSequence)"archiveFile", (Object)archiveFile);
        Parameters.notNull((CharSequence)"timeStamp", timeStamp);
        Store store = ArchiveTimeStamps.getStore();
        assert (store != null);
        store.put(archiveFile.toExternalForm(), ArchiveTimeStamps.generate(timeStamp));
        saver.getAndSetPropertiesToSave(store);
        saverTask.schedule(2500);
    }

    static int getIndexerState(@NonNull Context ctx) {
        Object res = SPIAccessor.getInstance().getProperty(ctx, "ArchiveTimeStamps.indexerState");
        return res instanceof Integer ? (Integer)res : 0;
    }

    static void setIndexerState(@NonNull Context ctx, int state) {
        SPIAccessor.getInstance().putProperty(ctx, "ArchiveTimeStamps.indexerState", state);
    }

    @NonNull
    private static Pair<Long, Map<Pair<String, Integer>, Integer>> parse(@NullAllowed String record) {
        if (record == null) {
            return FORCE_RESCAN;
        }
        HashMap<Pair, Integer> indexers = new HashMap<Pair, Integer>();
        long timeStamp = 0;
        StringTokenizer tk = new StringTokenizer(record, ":");
        do {
            String second;
            String first;
            String string = first = tk.hasMoreTokens() ? tk.nextToken() : null;
            if (first == null) {
                return FORCE_RESCAN;
            }
            String string2 = second = tk.hasMoreTokens() ? tk.nextToken() : null;
            if (second != null) {
                String third;
                String string3 = third = tk.hasMoreTokens() ? tk.nextToken() : null;
                if (third == null) {
                    return FORCE_RESCAN;
                }
                try {
                    int ver = Integer.parseInt(second);
                    int state = Integer.parseInt(third);
                    indexers.put(Pair.of((Object)first, (Object)ver), state);
                }
                catch (NumberFormatException nfe) {
                    return FORCE_RESCAN;
                }
            } else {
                try {
                    timeStamp = Long.parseLong(first);
                    continue;
                }
                catch (NumberFormatException nfe) {
                    return FORCE_RESCAN;
                }
            }
        } while (tk.hasMoreElements());
        return Pair.of((Object)timeStamp, indexers);
    }

    @NonNull
    private static String generate(Pair<Long, Map<Pair<String, Integer>, Integer>> data) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry r : ((Map)data.second()).entrySet()) {
            Pair key = (Pair)r.getKey();
            assert (((String)key.first()).indexOf(":") < 0);
            sb.append((String)key.first());
            sb.append(":");
            sb.append(key.second());
            sb.append(":");
            sb.append(r.getValue());
            sb.append(":");
        }
        sb.append(data.first());
        return sb.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    private static synchronized Store getStore() throws IOException {
        Store store;
        Store store2 = store = cachedStore == null ? null : cachedStore.get();
        if (store == null) {
            store = new Store();
            FileObject file = ArchiveTimeStamps.getArchiveTimeStampsFile(false);
            if (file != null) {
                BufferedInputStream in = new BufferedInputStream(file.getInputStream());
                try {
                    store.load(in);
                }
                finally {
                    in.close();
                }
            }
            cachedStore = new SoftReference<Store>(store);
        }
        return store;
    }

    @CheckForNull
    private static FileObject getArchiveTimeStampsFile(boolean create) throws IOException {
        FileObject cacheFolder = CacheFolder.getCacheFolder();
        return create ? FileUtil.createData((FileObject)cacheFolder, (String)"archives.properties") : cacheFolder.getFileObject("archives.properties");
    }

    private static final class Store {
        private final Object lock = new Object();
        private final EditableProperties properties = new EditableProperties(true);

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void put(@NonNull String key, @NonNull String value) {
            Object object = this.lock;
            synchronized (object) {
                this.properties.put(key, value);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @CheckForNull
        public String get(@NonNull String key) {
            Object object = this.lock;
            synchronized (object) {
                return this.properties.get((Object)key);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void load(@NonNull InputStream in) throws IOException {
            Object object = this.lock;
            synchronized (object) {
                this.properties.load(in);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void store(@NonNull OutputStream out) throws IOException {
            Object object = this.lock;
            synchronized (object) {
                this.properties.store(out);
            }
        }
    }

    private static class Saver
    implements Runnable {
        private Store toSave;

        private Saver() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Store store = this.getAndSetPropertiesToSave(null);
            if (store != null) {
                try {
                    LOG.fine("STORING");
                    FileObject file = ArchiveTimeStamps.getArchiveTimeStampsFile(true);
                    FileLock lock = file.lock();
                    try {
                        BufferedOutputStream out = new BufferedOutputStream(file.getOutputStream(lock));
                        try {
                            store.store(out);
                        }
                        finally {
                            out.close();
                        }
                    }
                    finally {
                        lock.releaseLock();
                    }
                    LOG.fine("STORED");
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
        }

        @CheckForNull
        public synchronized Store getAndSetPropertiesToSave(@NullAllowed Store store) {
            assert (store == null || this.toSave == null || store == this.toSave);
            Store old = this.toSave;
            this.toSave = store;
            return old;
        }
    }

}

