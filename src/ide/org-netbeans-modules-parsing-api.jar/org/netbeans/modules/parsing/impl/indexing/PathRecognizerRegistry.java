/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public final class PathRecognizerRegistry {
    private static final Logger LOG = Logger.getLogger(PathRecognizerRegistry.class.getName());
    private static PathRecognizerRegistry instance;
    private final Lookup.Result<? extends PathRecognizer> lookupResult;
    private final LookupListener tracker;
    private Object[] cachedData;

    public static synchronized PathRecognizerRegistry getDefault() {
        if (instance == null) {
            instance = new PathRecognizerRegistry();
        }
        return instance;
    }

    public Set<String> getSourceIds() {
        Object[] data = this.getData();
        return (Set)data[0];
    }

    public Set<String> getLibraryIds() {
        Object[] data = this.getData();
        return (Set)data[1];
    }

    public Set<String> getBinaryLibraryIds() {
        Object[] data = this.getData();
        return (Set)data[2];
    }

    public Set<String> getMimeTypes() {
        Object[] data = this.getData();
        return (Set)data[3];
    }

    public Set<String> getLibraryIdsForSourceId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[4]).get(id);
        return arr != null ? arr[0] : Collections.emptySet();
    }

    public Set<String> getBinaryLibraryIdsForSourceId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[4]).get(id);
        return arr != null ? arr[1] : Collections.emptySet();
    }

    public Set<String> getLibraryIdsForLibraryId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[5]).get(id);
        return arr != null ? arr[0] : Collections.emptySet();
    }

    public Set<String> getBinaryLibraryIdsForLibraryId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[5]).get(id);
        return arr != null ? arr[1] : Collections.emptySet();
    }

    public Set<String> getSourceIdsForBinaryLibraryId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[6]).get(id);
        return arr != null ? arr[0] : Collections.emptySet();
    }

    public Set<String> getMimeTypesForSourceId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[4]).get(id);
        return arr != null ? arr[2] : Collections.emptySet();
    }

    public Set<String> getMimeTypesForLibraryId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[5]).get(id);
        return arr != null ? arr[2] : Collections.emptySet();
    }

    public Set<String> getMimeTypesForBinaryLibraryId(String id) {
        Object[] data = this.getData();
        Set[] arr = (Set[])((Map)data[6]).get(id);
        return arr != null ? arr[2] : Collections.emptySet();
    }

    private PathRecognizerRegistry() {
        this.tracker = new LookupListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void resultChanged(LookupEvent ev) {
                LOG.fine("resultChanged: reseting cached PathRecognizers");
                PathRecognizerRegistry pathRecognizerRegistry = PathRecognizerRegistry.this;
                synchronized (pathRecognizerRegistry) {
                    PathRecognizerRegistry.this.cachedData = null;
                }
            }
        };
        this.lookupResult = Lookup.getDefault().lookupResult(PathRecognizer.class);
        this.lookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.tracker, this.lookupResult));
    }

    private synchronized Object[] getData() {
        if (this.cachedData == null) {
            HashSet<String> sourceIds = new HashSet<String>();
            HashSet<String> libraryIds = new HashSet<String>();
            HashSet<String> binaryLibraryIds = new HashSet<String>();
            HashSet<String> mimeTypes = new HashSet<String>();
            HashMap<String, Set[]> sidsMap = new HashMap<String, Set[]>();
            HashMap<String, Set[]> lidsMap = new HashMap<String, Set[]>();
            HashMap<String, Set[]> blidsMap = new HashMap<String, Set[]>();
            Collection recognizers = this.lookupResult.allInstances();
            for (PathRecognizer r : recognizers) {
                Set[] set;
                Set<String> sids = r.getSourcePathIds();
                Set<String> lids = r.getLibraryPathIds();
                Set<String> blids = r.getBinaryLibraryPathIds();
                Set<String> mts = r.getMimeTypes();
                if (sids != null) {
                    sourceIds.addAll(sids);
                    for (String sid : sids) {
                        if (sidsMap.containsKey(sid)) continue;
                        Set[] arrset = new Set[3];
                        arrset[0] = lids == null ? Collections.emptySet() : Collections.unmodifiableSet(lids);
                        arrset[1] = blids == null ? Collections.emptySet() : Collections.unmodifiableSet(blids);
                        arrset[2] = mts == null ? Collections.emptySet() : Collections.unmodifiableSet(mts);
                        set = arrset;
                        sidsMap.put(sid, set);
                    }
                }
                if (lids != null) {
                    libraryIds.addAll(lids);
                    for (String lid : lids) {
                        if (lidsMap.containsKey(lid)) continue;
                        Set[] arrset = new Set[3];
                        arrset[0] = lids == null ? Collections.emptySet() : Collections.unmodifiableSet(lids);
                        arrset[1] = blids == null ? Collections.emptySet() : Collections.unmodifiableSet(blids);
                        arrset[2] = mts == null ? Collections.emptySet() : Collections.unmodifiableSet(mts);
                        set = arrset;
                        lidsMap.put(lid, set);
                    }
                }
                if (blids != null) {
                    binaryLibraryIds.addAll(blids);
                    for (String blid : blids) {
                        if (blidsMap.containsKey(blid)) continue;
                        Set[] arrset = new Set[3];
                        arrset[0] = sids == null ? Collections.emptySet() : Collections.unmodifiableSet(sids);
                        arrset[1] = lids == null ? Collections.emptySet() : Collections.unmodifiableSet(lids);
                        arrset[2] = mts == null ? Collections.emptySet() : Collections.unmodifiableSet(mts);
                        set = arrset;
                        blidsMap.put(blid, set);
                    }
                }
                if (mts != null) {
                    mimeTypes.addAll(mts);
                }
                LOG.log(Level.FINE, "PathRecognizer {0} supplied sids={1}, lids={2}, blids={3}, mts={4}", new Object[]{r.toString(), sids, lids, blids, mts});
            }
            this.cachedData = new Object[]{Collections.unmodifiableSet(sourceIds), Collections.unmodifiableSet(libraryIds), Collections.unmodifiableSet(binaryLibraryIds), Collections.unmodifiableSet(mimeTypes), Collections.unmodifiableMap(sidsMap), Collections.unmodifiableMap(lidsMap), Collections.unmodifiableMap(blidsMap)};
        }
        return this.cachedData;
    }

}

