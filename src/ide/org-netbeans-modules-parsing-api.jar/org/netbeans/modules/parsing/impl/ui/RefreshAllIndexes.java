/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;

public final class RefreshAllIndexes
implements ActionListener {
    private static final Logger LOG = Logger.getLogger(RefreshAllIndexes.class.getName());
    private final List<DataObject> context;

    public RefreshAllIndexes(List<DataObject> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Collection<? extends URL> sources = PathRegistry.getDefault().getSources();
        HashSet<FileObject> roots = new HashSet<FileObject>();
        for (DataObject dobj : this.context) {
            try {
                FileObject root = this.findRoot(dobj.getPrimaryFile(), sources);
                if (root == null) continue;
                roots.add(root);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder();
            for (FileObject root : roots) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(FileUtil.getFileDisplayName((FileObject)root));
            }
            LOG.log(Level.FINE, "Refreshing: {0}", sb.toString());
        }
        IndexingManager.getDefault().refreshAllIndices(roots.toArray((T[])new FileObject[roots.size()]));
    }

    private FileObject findRoot(FileObject fobj, Collection<? extends URL> roots) throws IOException {
        while (fobj != null) {
            URL url = fobj.getURL();
            if (roots.contains(url)) {
                return fobj;
            }
            fobj = fobj.getParent();
        }
        return null;
    }
}

