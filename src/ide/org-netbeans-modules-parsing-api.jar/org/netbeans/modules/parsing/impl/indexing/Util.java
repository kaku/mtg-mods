/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.TopologicalSortException
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.TopologicalSortException;
import org.openide.util.Utilities;

public final class Util {
    static Set<String> allMimeTypes;
    private static volatile boolean hadCycles;

    public static Set<String> getAllMimeTypes() {
        return allMimeTypes != null ? allMimeTypes : EditorSettings.getDefault().getAllMimeTypes();
    }

    public static boolean canBeParsed(String mimeType) {
        if (mimeType == null || "content/unknown".equals(mimeType) || !Util.getAllMimeTypes().contains(mimeType)) {
            return false;
        }
        int slashIdx = mimeType.indexOf(47);
        assert (slashIdx != -1);
        String type = mimeType.substring(0, slashIdx);
        if (type.equals("application") ? !mimeType.equals("application/x-httpd-eruby") && !mimeType.equals("application/xml-dtd") : !type.equals("text")) {
            return false;
        }
        return true;
    }

    public static /* varargs */ StackTraceElement findCaller(StackTraceElement[] elements, Object ... classesToFilterOut) {
        block0 : for (StackTraceElement e : elements) {
            if (e.getClassName().equals(Util.class.getName()) || e.getClassName().startsWith("java.lang.")) continue;
            if (classesToFilterOut != null && classesToFilterOut.length > 0) {
                for (Object c : classesToFilterOut) {
                    if (c instanceof Class && e.getClassName().startsWith(((Class)c).getName()) || c instanceof String && e.getClassName().startsWith((String)c)) continue block0;
                }
            } else if (e.getClassName().startsWith("org.netbeans.modules.parsing.")) continue;
            return e;
        }
        return null;
    }

    public static URL resolveUrl(@NonNull URL root, @NonNull String relativePath, @NullAllowed Boolean isDirectory) throws MalformedURLException, IllegalStateException {
        try {
            if ("file".equals(root.getProtocol())) {
                if (isDirectory == Boolean.FALSE && (relativePath.isEmpty() || relativePath.charAt(relativePath.length() - 1) == '/')) {
                    throw new IllegalStateException(MessageFormat.format("relativePath: {0}", relativePath));
                }
                return Utilities.toURI((File)new FastFile(Utilities.toFile((URI)root.toURI()), relativePath, isDirectory)).toURL();
            }
            return new URL(root, relativePath);
        }
        catch (URISyntaxException use) {
            MalformedURLException mue = new MalformedURLException("Can't resolve URL: root=" + root + ", relativePath=" + relativePath);
            mue.initCause(use);
            throw mue;
        }
    }

    public static URL resolveFile(@NonNull File file, @NonNull String relativePath, @NullAllowed Boolean isDirectory) throws MalformedURLException {
        if (isDirectory == Boolean.FALSE && (relativePath.isEmpty() || relativePath.charAt(relativePath.length() - 1) == '/' || relativePath.charAt(relativePath.length() - 1) == File.separatorChar)) {
            throw new IllegalStateException(MessageFormat.format("relativePath: {0}", relativePath));
        }
        return Utilities.toURI((File)new FastFile(file, relativePath, isDirectory)).toURL();
    }

    public static boolean containsAny(Collection<? extends String> searchIn, Collection<? extends String> searchFor) {
        if (searchIn != null && searchFor != null) {
            for (String s : searchFor) {
                if (!searchIn.contains(s)) continue;
                return true;
            }
        }
        return false;
    }

    public static FileObject getFileObject(Document doc) {
        Object sdp = doc.getProperty("stream");
        if (sdp instanceof FileObject) {
            return (FileObject)sdp;
        }
        if (sdp instanceof DataObject) {
            return ((DataObject)sdp).getPrimaryFile();
        }
        return null;
    }

    public static boolean isParentOf(@NonNull File folder, @NonNull File file) {
        return file.getAbsolutePath().startsWith(folder.getAbsolutePath());
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public static Set<URL> findReverseSourceRoots(@NonNull URL thisSourceRoot, @NonNull Map<URL, List<URL>> deps, @NonNull Map<URL, List<URL>> peers) {
        Map<URL, Collection<URL>> inverseDeps = Util.findReverseDependencies(deps);
        HashSet<URL> result = new HashSet<URL>();
        LinkedList<URL> todo = new LinkedList<URL>();
        todo.add(thisSourceRoot);
        while (!todo.isEmpty()) {
            URL u = (URL)todo.removeFirst();
            if (result.contains(u)) continue;
            result.add(u);
            Collection ideps = inverseDeps.get(u);
            if (ideps != null) {
                todo.addAll(ideps);
            }
            if ((ideps = (Collection)peers.get(u)) == null) continue;
            todo.addAll(ideps);
        }
        return result;
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public static Map<URL, Collection<URL>> findTransitiveReverseDependencies(@NonNull Map<URL, List<URL>> deps, @NonNull Map<URL, List<URL>> peers) {
        Map<URL, Collection<URL>> inverseDeps = Util.findReverseDependencies(deps);
        if (!hadCycles) {
            try {
                return Util.fastTransitiveDeps(inverseDeps, peers);
            }
            catch (TopologicalSortException tse) {
                hadCycles = true;
            }
        }
        return Util.slowTransitiveDeps(inverseDeps, peers);
    }

    @NonNull
    private static Map<URL, Collection<URL>> fastTransitiveDeps(@NonNull Map<URL, ? extends Collection<URL>> inverseDeps, @NonNull Map<URL, ? extends Collection<URL>> peers) throws TopologicalSortException {
        Collection<URL> deps;
        List sortedNodes = Utilities.topologicalSort(inverseDeps.keySet(), inverseDeps);
        Collections.reverse(sortedNodes);
        HashMap<URL, Collection<URL>> result = new HashMap<URL, Collection<URL>>();
        for (Map.Entry<URL, ? extends Collection<URL>> peerEntry : peers.entrySet()) {
            Collection<URL> peerValue;
            deps = inverseDeps.get(peerEntry.getKey());
            if (deps == null || (peerValue = peerEntry.getValue()) == null) continue;
            for (URL peer : peerValue) {
                deps.add(peer);
                Collection<URL> peerDeps = inverseDeps.get(peer);
                if (peerDeps == null) continue;
                deps.addAll(peerDeps);
            }
        }
        for (URL root : sortedNodes) {
            deps = new HashSet<URL>();
            result.put(root, deps);
            deps.add(root);
            Collection<URL> directDeps = inverseDeps.get(root);
            for (URL dd : directDeps) {
                Collection<URL> transitiveDeps = result.get(dd);
                if (transitiveDeps != null) {
                    deps.addAll(transitiveDeps);
                    continue;
                }
                deps.add(dd);
            }
        }
        return result;
    }

    @NonNull
    private static Map<URL, Collection<URL>> slowTransitiveDeps(@NonNull Map<URL, ? extends Collection<URL>> inverseDeps, @NonNull Map<URL, ? extends Collection<URL>> peers) {
        HashMap<URL, Collection<URL>> result = new HashMap<URL, Collection<URL>>();
        for (URL thisSourceRoot : inverseDeps.keySet()) {
            LinkedList<URL> todo = new LinkedList<URL>();
            HashSet<URL> partialResult = new HashSet<URL>();
            todo.add(thisSourceRoot);
            while (!todo.isEmpty()) {
                URL u = (URL)todo.removeFirst();
                if (partialResult.contains(u)) continue;
                partialResult.add(u);
                Collection<URL> ideps = inverseDeps.get(u);
                if (ideps != null) {
                    todo.addAll(ideps);
                }
                if ((ideps = peers.get(u)) == null) continue;
                todo.addAll(ideps);
            }
            result.put(thisSourceRoot, partialResult);
        }
        return result;
    }

    @NonNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private static Map<URL, Collection<URL>> findReverseDependencies(@NonNull Map<URL, List<URL>> deps) {
        HashMap<URL, Collection<URL>> inverseDeps = new HashMap<URL, Collection<URL>>();
        for (Map.Entry<URL, List<URL>> entry : deps.entrySet()) {
            URL u1 = entry.getKey();
            if (inverseDeps.get(u1) == null) {
                inverseDeps.put(u1, new HashSet());
            }
            List<URL> l1 = entry.getValue();
            for (URL u2 : l1) {
                Collection<URL> l2 = inverseDeps.get(u2);
                if (l2 == null) {
                    l2 = new HashSet<URL>();
                    inverseDeps.put(u2, l2);
                }
                l2.add(u1);
            }
        }
        return inverseDeps;
    }

    private Util() {
    }

    static {
        hadCycles = Boolean.getBoolean("disable.reversedeps.fastpath");
    }

    private static final class FastFile
    extends File {
        private static final InvalidPathException IP = new InvalidPathException("", ""){

            @Override
            public Throwable fillInStackTrace() {
                return this;
            }
        };
        private final Boolean isDirectory;

        FastFile(@NonNull File file, @NonNull String path, @NullAllowed Boolean isDirectory) {
            super(file, path);
            this.isDirectory = isDirectory;
        }

        @Override
        public File getAbsoluteFile() {
            if (this.isAbsolute()) {
                return this;
            }
            return super.getAbsoluteFile();
        }

        @Override
        public boolean isDirectory() {
            return this.isDirectory == null ? super.isDirectory() : this.isDirectory.booleanValue();
        }

        @Override
        public Path toPath() {
            if (this.isDirectory != null) {
                throw IP;
            }
            return super.toPath();
        }

    }

}

