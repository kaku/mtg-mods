/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import org.netbeans.modules.parsing.impl.indexing.DefaultPathRecognizer;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizerRegistration;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedAnnotationTypes(value={"org.netbeans.modules.parsing.spi.indexing.PathRecognizerRegistration"})
@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class PathRecognizerRegistrationProcessor
extends LayerGeneratingProcessor {
    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        for (Element e : roundEnv.getElementsAnnotatedWith(PathRecognizerRegistration.class)) {
            TypeElement cls = (TypeElement)e;
            PathRecognizerRegistration prr = cls.getAnnotation(PathRecognizerRegistration.class);
            String sourcePathIds = PathRecognizerRegistrationProcessor.processIds(prr.sourcePathIds());
            String libraryPathIds = PathRecognizerRegistrationProcessor.processIds(prr.libraryPathIds());
            String binaryLibraryPathIds = PathRecognizerRegistrationProcessor.processIds(prr.binaryLibraryPathIds());
            String mimeTypes = PathRecognizerRegistrationProcessor.processMts(prr.mimeTypes());
            if (mimeTypes == null || sourcePathIds == null && libraryPathIds == null && binaryLibraryPathIds == null) continue;
            LayerBuilder lb = this.layer(new Element[]{cls});
            LayerBuilder.File f = PathRecognizerRegistrationProcessor.instanceFile(lb, "Services/Hidden/PathRecognizers", PathRecognizerRegistrationProcessor.makeFilesystemName(cls.getQualifiedName().toString()), DefaultPathRecognizer.class, "createInstance", PathRecognizer.class);
            if (sourcePathIds != null) {
                f.stringvalue("sourcePathIds", sourcePathIds);
            }
            if (libraryPathIds != null) {
                f.stringvalue("libraryPathIds", libraryPathIds);
            }
            if (binaryLibraryPathIds != null) {
                f.stringvalue("binaryLibraryPathIds", binaryLibraryPathIds);
            }
            if (mimeTypes != null) {
                f.stringvalue("mimeTypes", mimeTypes);
            }
            f.write();
        }
        return true;
    }

    private static String processIds(String[] ids) {
        if (ids.length == 0) {
            return null;
        }
        if (ids.length == 1 && ids[0].equals("ANY")) {
            return "ANY";
        }
        StringBuilder sb = new StringBuilder();
        for (String s : ids) {
            if (s == null || (s = s.trim()).length() == 0 || s.equals("ANY")) continue;
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append(s);
        }
        return sb.length() > 0 ? sb.toString() : null;
    }

    private static String processMts(String[] mts) {
        if (mts == null || mts.length == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (String s : mts) {
            if (s == null || (s = s.trim()).length() == 0) continue;
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append(s);
        }
        return sb.length() > 0 ? sb.toString() : null;
    }

    private static /* varargs */ LayerBuilder.File instanceFile(LayerBuilder b, String folder, String name, Class implClass, String factoryMethod, Class ... instanceOf) {
        String basename;
        if (name == null) {
            basename = implClass.getName().replace('.', '-');
            if (factoryMethod != null) {
                basename = basename + "-" + factoryMethod;
            }
        } else {
            basename = name;
        }
        LayerBuilder.File f = b.file(folder + "/" + basename + ".instance");
        if (implClass != null) {
            if (factoryMethod != null) {
                f.methodvalue("instanceCreate", implClass.getName(), factoryMethod);
            } else {
                f.stringvalue("instanceClass", implClass.getName());
            }
        }
        for (Class c : instanceOf) {
            f.stringvalue("instanceOf", c.getName());
        }
        return f;
    }

    private static String makeFilesystemName(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
                continue;
            }
            sb.append("-");
        }
        return sb.toString();
    }
}

