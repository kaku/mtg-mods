/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public final class URLCache {
    private static final Logger LOG = Logger.getLogger(URLCache.class.getName());
    private static URLCache instance = null;
    private final Map<URI, Reference<FileObject>> cache = Collections.synchronizedMap(new HashMap());

    public static synchronized URLCache getInstance() {
        if (instance == null) {
            instance = new URLCache();
        }
        return instance;
    }

    @CheckForNull
    public FileObject findFileObject(@NonNull URL url, boolean validate) {
        URI uri = null;
        try {
            uri = url.toURI();
        }
        catch (URISyntaxException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        FileObject f = null;
        if (uri != null) {
            Reference<FileObject> ref = this.cache.get(uri);
            if (ref != null) {
                f = ref.get();
            }
            if (f != null && f.isValid() && (!validate || f.toURI().equals(uri))) {
                LOG.log(Level.FINEST, "Found: {0} in cache for: {1}", new Object[]{f, url});
                return f;
            }
        }
        f = URLMapper.findFileObject((URL)url);
        if (uri != null && f != null && f.isValid()) {
            LOG.log(Level.FINEST, "Caching: {0} for: {1}", new Object[]{f, url});
            this.cache.put(uri, new CleanReference(f, uri));
        }
        return f;
    }

    private URLCache() {
    }

    private final class CleanReference
    extends WeakReference<FileObject>
    implements Runnable {
        private final URI uri;

        CleanReference(@NonNull FileObject referent, URI uri) {
            super(referent, Utilities.activeReferenceQueue());
            assert (referent != null);
            assert (uri != null);
            this.uri = uri;
        }

        @Override
        public void run() {
            URLCache.this.cache.remove(this.uri);
        }
    }

}

