/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileEvent
 */
package org.netbeans.modules.parsing.impl.indexing.friendapi;

import org.openide.filesystems.FileEvent;

public interface IndexingActivityInterceptor {
    public Authorization authorizeFileSystemEvent(FileEvent var1);

    public static enum Authorization {
        IGNORE,
        PROCESS;
        

        private Authorization() {
        }
    }

}

