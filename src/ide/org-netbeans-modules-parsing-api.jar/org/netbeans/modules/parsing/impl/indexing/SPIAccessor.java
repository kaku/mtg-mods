/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.Callable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.IndexFactoryImpl;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.SuspendSupport;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.ConstrainedBinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.CustomIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public abstract class SPIAccessor {
    private static volatile SPIAccessor instance;

    public static void setInstance(SPIAccessor _instance) {
        assert (_instance != null);
        instance = _instance;
    }

    public static synchronized SPIAccessor getInstance() {
        if (instance == null) {
            try {
                Class.forName(Indexable.class.getName(), true, Indexable.class.getClassLoader());
                assert (instance != null);
            }
            catch (ClassNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        return instance;
    }

    public abstract Indexable create(IndexableImpl var1);

    @NonNull
    public abstract Context createContext(@NonNull FileObject var1, @NonNull URL var2, @NonNull String var3, int var4, @NullAllowed IndexFactoryImpl var5, boolean var6, boolean var7, boolean var8, @NonNull SuspendStatus var9, @NullAllowed CancelRequest var10, @NullAllowed LogContext var11) throws IOException;

    @NonNull
    public abstract Context createContext(@NonNull Callable<FileObject> var1, @NonNull URL var2, @NonNull String var3, int var4, @NullAllowed IndexFactoryImpl var5, boolean var6, boolean var7, boolean var8, @NonNull SuspendStatus var9, @NullAllowed CancelRequest var10, @NullAllowed LogContext var11) throws IOException;

    @NonNull
    public abstract SuspendStatus createSuspendStatus(@NonNull SuspendSupport.SuspendStatusImpl var1);

    public abstract void context_attachIndexingSupport(Context var1, IndexingSupport var2);

    public abstract IndexingSupport context_getAttachedIndexingSupport(Context var1);

    public abstract void context_clearAttachedIndexingSupport(Context var1);

    public abstract String getIndexerName(Context var1);

    public abstract int getIndexerVersion(Context var1);

    public abstract String getIndexerPath(String var1, int var2);

    public abstract IndexFactoryImpl getIndexFactory(Context var1);

    public abstract void index(BinaryIndexer var1, Context var2);

    public abstract void index(@NonNull ConstrainedBinaryIndexer var1, @NonNull Map<String, ? extends Iterable<? extends FileObject>> var2, @NonNull Context var3);

    public abstract void index(CustomIndexer var1, Iterable<? extends Indexable> var2, Context var3);

    public abstract void index(EmbeddingIndexer var1, Indexable var2, Parser.Result var3, Context var4);

    public abstract void setAllFilesJob(Context var1, boolean var2);

    public abstract boolean scanStarted(@NonNull ConstrainedBinaryIndexer var1, @NonNull Context var2);

    public abstract void scanFinished(@NonNull ConstrainedBinaryIndexer var1, @NonNull Context var2);

    public abstract void rootsRemoved(@NonNull ConstrainedBinaryIndexer var1, @NonNull Iterable<? extends URL> var2);

    public abstract void putProperty(@NonNull Context var1, @NonNull String var2, @NullAllowed Object var3);

    public abstract Object getProperty(@NonNull Context var1, @NonNull String var2);

    public abstract boolean isTypeOf(@NonNull Indexable var1, @NonNull String var2);

    public abstract FileObject getFileObject(@NonNull Indexable var1);
}

