/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.masterfs.providers.ProvidedExtensions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.parsing.impl;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Callable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.impl.event.EventSupport;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public class Utilities {
    private static final ThreadLocal<Parser.CancelReason> cancelReason = new ThreadLocal();
    private static volatile IndexingStatus status;

    private Utilities() {
    }

    public static <T> T runPriorityIO(Callable<T> r) throws Exception {
        assert (r != null);
        return (T)ProvidedExtensions.priorityIO(r);
    }

    public static void acquireParserLock() {
        TaskProcessor.acquireParserLock();
    }

    public static void releaseParserLock() {
        TaskProcessor.releaseParserLock();
    }

    public static boolean holdsParserLock() {
        return TaskProcessor.holdsParserLock();
    }

    public static boolean isTaskProcessorThread() {
        return TaskProcessor.WORKER.isRequestProcessorThread();
    }

    public static void scheduleSpecialTask(Runnable runnable, int priority) {
        TaskProcessor.scheduleSpecialTask(runnable, priority);
    }

    public static void runAsScanWork(@NonNull Runnable work) {
        Parameters.notNull((CharSequence)"work", (Object)work);
        RepositoryUpdater.getDefault().runAsWork(work);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static void setIndexingStatus(@NullAllowed IndexingStatus st) {
        if (!$assertionsDisabled) {
            if (st != null) {
                if (status != null) throw new AssertionError();
            } else if (status == null) {
                throw new AssertionError();
            }
        }
        status = st;
    }

    public static Set<? extends RepositoryUpdater.IndexingState> getIndexingState() {
        if (status == null) {
            return RepositoryUpdater.getDefault().getIndexingState();
        }
        return status.getIndexingState();
    }

    public static boolean isScanInProgress() {
        return !Utilities.getIndexingState().isEmpty();
    }

    public static void revalidate(Source source) {
        EventSupport support = SourceAccessor.getINSTANCE().getEventSupport(source);
        assert (support != null);
        support.resetState(true, false, -1, -1, false);
    }

    public static void addParserResultTask(ParserResultTask<?> task, Source source) {
        Parameters.notNull((CharSequence)"task", task);
        Parameters.notNull((CharSequence)"source", (Object)source);
        SourceCache cache = SourceAccessor.getINSTANCE().getCache(source);
        TaskProcessor.addPhaseCompletionTasks(Collections.singleton(Pair.of(task, (Object)null)), cache, true);
    }

    public static void removeParserResultTask(ParserResultTask<?> task, Source source) {
        Parameters.notNull((CharSequence)"task", task);
        Parameters.notNull((CharSequence)"source", (Object)source);
        TaskProcessor.removePhaseCompletionTasks(Collections.singleton(task), source);
    }

    public static void rescheduleTask(ParserResultTask<?> task, Source source) {
        Parameters.notNull((CharSequence)"task", task);
        Parameters.notNull((CharSequence)"source", (Object)source);
        TaskProcessor.rescheduleTasks(Collections.singleton(task), source, null);
    }

    public static Parser.CancelReason getTaskCancelReason() {
        return cancelReason.get();
    }

    static void setTaskCancelReason(@NullAllowed Parser.CancelReason reason) {
        if (reason == null) {
            cancelReason.remove();
        } else {
            cancelReason.set(reason);
        }
    }

    public static interface IndexingStatus {
        public Set<? extends RepositoryUpdater.IndexingState> getIndexingState();
    }

}

