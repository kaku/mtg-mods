/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.parsing.impl.indexing.FileObjectProvider;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.openide.filesystems.FileObject;

public final class DeletedIndexable
implements IndexableImpl,
FileObjectProvider {
    private static final Logger LOG = Logger.getLogger(DeletedIndexable.class.getName());
    private final URL root;
    private final String relativePath;

    public DeletedIndexable(URL root, String relativePath) {
        assert (root != null);
        assert (relativePath != null);
        assert (relativePath.length() == 0 || relativePath.charAt(0) != '/');
        this.root = root;
        this.relativePath = relativePath;
    }

    @Override
    public String getRelativePath() {
        return this.relativePath;
    }

    @Override
    public URL getURL() {
        try {
            return Util.resolveUrl(this.root, this.relativePath, this.relativePath.isEmpty() || this.relativePath.charAt(this.relativePath.length() - 1) == '/' ? null : Boolean.FALSE);
        }
        catch (MalformedURLException ex) {
            LOG.log(Level.WARNING, null, ex);
            return null;
        }
    }

    @Override
    public String getMimeType() {
        throw new UnsupportedOperationException("Mimetype related operations are not supported by DeletedIndexable");
    }

    @Override
    public boolean isTypeOf(String mimeType) {
        throw new UnsupportedOperationException("Mimetype related operations are not supported by DeletedIndexable");
    }

    @Override
    public FileObject getFileObject() {
        return null;
    }

    @SuppressWarnings(value={"DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        DeletedIndexable other = (DeletedIndexable)obj;
        if (!(this.root == other.root || this.root != null && this.root.equals(other.root))) {
            return false;
        }
        if (!(this.relativePath == other.relativePath || this.relativePath != null && this.relativePath.equals(other.relativePath))) {
            return false;
        }
        return true;
    }

    @SuppressWarnings(value={"DMI_BLOCKING_METHODS_ON_URL"}, justification="URLs have never host part")
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.root != null ? this.root.hashCode() : 0);
        hash = 83 * hash + (this.relativePath != null ? this.relativePath.hashCode() : 0);
        return hash;
    }

    public String toString() {
        return "DeletedIndexable@" + Integer.toHexString(System.identityHashCode(this)) + " [" + this.getURL() + "]";
    }
}

