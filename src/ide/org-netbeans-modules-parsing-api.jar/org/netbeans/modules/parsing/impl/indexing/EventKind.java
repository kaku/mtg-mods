/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

public enum EventKind {
    PATHS_ADDED,
    PATHS_REMOVED,
    PATHS_CHANGED,
    INCLUDES_CHANGED;
    

    private EventKind() {
    }
}

