/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public class Schedulers {
    private static Collection<? extends Scheduler> taskSchedulers;
    private static Lookup.Result<Scheduler> result;
    private static LookupListener listener;

    static synchronized void init() {
        if (taskSchedulers == null) {
            if (result == null) {
                assert (listener == null);
                listener = new LkpListener();
                result = Lookup.getDefault().lookupResult(Scheduler.class);
                result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)listener, result));
            }
            taskSchedulers = Collections.unmodifiableCollection(new ArrayList(result.allInstances()));
        }
    }

    static synchronized Collection<? extends Scheduler> getSchedulers() {
        Schedulers.init();
        return taskSchedulers;
    }

    private static class LkpListener
    implements LookupListener {
        private LkpListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            Class<Schedulers> class_ = Schedulers.class;
            synchronized (Schedulers.class) {
                taskSchedulers = null;
                // ** MonitorExit[var2_2] (shouldn't be in output)
                return;
            }
        }
    }

}

