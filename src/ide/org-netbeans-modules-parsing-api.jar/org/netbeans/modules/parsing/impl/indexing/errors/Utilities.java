/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Pair
 */
package org.netbeans.modules.parsing.impl.indexing.errors;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.PathRecognizerRegistry;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.errors.ErrorAnnotator;
import org.openide.filesystems.FileObject;
import org.openide.util.Pair;

public class Utilities {
    private static volatile Pair<FileObject, Reference<ClassPath>> rootCache;

    public static ClassPath getSourceClassPathFor(FileObject file) {
        ClassPath cp;
        Pair<FileObject, Reference<ClassPath>> ce = rootCache;
        if (ce != null && (cp = (ClassPath)((Reference)ce.second()).get()) != null && ((FileObject)ce.first()).equals((Object)cp.findOwnerRoot(file))) {
            return cp;
        }
        for (String sourceCP : PathRecognizerRegistry.getDefault().getSourceIds()) {
            cp = ClassPath.getClassPath((FileObject)file, (String)sourceCP);
            if (cp == null) continue;
            FileObject root = cp.findOwnerRoot(file);
            if (root != null) {
                rootCache = Pair.of((Object)root, new WeakReference<ClassPath>(cp));
            }
            return cp;
        }
        return null;
    }

    public static Iterable<? extends FileObject> findIndexedRootsUnderDirectory(Project p, FileObject bigRoot) {
        LinkedList<FileObject> result = new LinkedList<FileObject>();
        try {
            Iterable<? extends FileObject> roots = CacheFolder.findRootsWithCacheUnderFolder(bigRoot);
            for (FileObject root : roots) {
                Project curr = FileOwnerQuery.getOwner((FileObject)root);
                if (curr == null || curr.getProjectDirectory() != p.getProjectDirectory() || !PathRegistry.getDefault().getSources().contains(root.toURL())) continue;
                result.add(root);
            }
        }
        catch (IOException ex) {
            Logger.getLogger(ErrorAnnotator.class.getName()).log(Level.FINE, null, ex);
        }
        return result;
    }

    private Utilities() {
    }
}

