/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.ClassPath$PathConversionMode
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.project.SourceGroup
 *  org.netbeans.spi.tasklist.PushTaskScanner
 *  org.netbeans.spi.tasklist.PushTaskScanner$Callback
 *  org.netbeans.spi.tasklist.Task
 *  org.netbeans.spi.tasklist.TaskScanningScope
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.parsing.impl.indexing.errors;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.modules.parsing.impl.indexing.errors.TaskCache;
import org.netbeans.modules.parsing.impl.indexing.errors.Utilities;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.TaskListener;
import org.openide.util.WeakSet;

public final class TaskProvider
extends PushTaskScanner {
    private static final Logger LOG = Logger.getLogger(TaskProvider.class.getName());
    private static TaskProvider INSTANCE;
    private TaskScanningScope scope;
    private PushTaskScanner.Callback callback;
    private static final Map<RequestProcessor.Task, Work> TASKS;
    private static boolean clearing;
    private static final RequestProcessor WORKER;
    private static Map<FileObject, Set<FileObject>> root2FilesWithAttachedErrors;

    public TaskProvider() {
        super(NbBundle.getBundle(TaskProvider.class).getString("LBL_ProviderName"), NbBundle.getBundle(TaskProvider.class).getString("LBL_ProviderDescription"), null);
        INSTANCE = this;
    }

    private synchronized void refreshImpl(FileObject file) {
        LOG.log(Level.FINE, "refresh: {0}", (Object)file);
        if (this.scope == null || this.callback == null) {
            return;
        }
        if (!this.scope.isInScope(file)) {
            if (!file.isFolder()) {
                return;
            }
            for (FileObject inScope : this.scope.getLookup().lookupAll(FileObject.class)) {
                if (!FileUtil.isParentOf((FileObject)file, (FileObject)inScope)) continue;
                TaskProvider.enqueue(new Work(inScope, this.callback));
            }
            return;
        }
        LOG.log(Level.FINE, "enqueing work for: {0}", (Object)file);
        TaskProvider.enqueue(new Work(file, this.callback));
    }

    public static void refresh(FileObject file) {
        if (INSTANCE != null) {
            INSTANCE.refreshImpl(file);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void refreshAll() {
        if (INSTANCE != null) {
            TaskProvider taskProvider = INSTANCE;
            synchronized (taskProvider) {
                INSTANCE.setScope(TaskProvider.INSTANCE.scope, TaskProvider.INSTANCE.callback);
            }
        }
    }

    public synchronized void setScope(TaskScanningScope scope, PushTaskScanner.Callback callback) {
        TaskProvider.cancelAllCurrent();
        this.scope = scope;
        this.callback = callback;
        if (scope == null || callback == null) {
            return;
        }
        for (FileObject file : scope.getLookup().lookupAll(FileObject.class)) {
            TaskProvider.enqueue(new Work(file, callback));
        }
        for (Project p : scope.getLookup().lookupAll(Project.class)) {
            for (SourceGroup generic : ProjectUtils.getSources((Project)p).getSourceGroups("generic")) {
                for (FileObject root : Utilities.findIndexedRootsUnderDirectory(p, generic.getRootFolder())) {
                    TaskProvider.enqueue(new Work(root, callback));
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void enqueue(Work w) {
        Map<RequestProcessor.Task, Work> map = TASKS;
        synchronized (map) {
            RequestProcessor.Task task = WORKER.post((Runnable)w);
            TASKS.put(task, w);
            task.addTaskListener(new TaskListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public void taskFinished(org.openide.util.Task task) {
                    Map map = TASKS;
                    synchronized (map) {
                        if (!clearing) {
                            TASKS.remove((Object)task);
                        }
                    }
                }
            });
            if (task.isFinished()) {
                TASKS.remove((Object)task);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void cancelAllCurrent() {
        Object object = TASKS;
        synchronized (object) {
            clearing = true;
            try {
                for (Map.Entry<RequestProcessor.Task, Work> t : TASKS.entrySet()) {
                    t.getKey().cancel();
                    t.getValue().cancel();
                }
                TASKS.clear();
            }
            finally {
                clearing = false;
            }
        }
        object = TaskProvider.class;
        synchronized (TaskProvider.class) {
            root2FilesWithAttachedErrors.clear();
            // ** MonitorExit[var0] (shouldn't be in output)
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void waitWorkFinished() throws Exception {
        do {
            RequestProcessor.Task t = null;
            Map<RequestProcessor.Task, Work> map = TASKS;
            synchronized (map) {
                if (TASKS.isEmpty()) {
                    return;
                }
                t = TASKS.keySet().iterator().next();
            }
            t.waitFinished();
        } while (true);
    }

    private static Set<FileObject> getFilesWithAttachedErrors(FileObject root) {
        WeakSet result = root2FilesWithAttachedErrors.get((Object)root);
        if (result == null) {
            result = new WeakSet();
            root2FilesWithAttachedErrors.put(root, (Set<FileObject>)result);
        }
        return result;
    }

    private static synchronized void updateErrorsInRoot(PushTaskScanner.Callback callback, FileObject root, AtomicBoolean cancelled) {
        Set<FileObject> filesWithErrors = TaskProvider.getFilesWithAttachedErrors(root);
        HashSet<FileObject> fixedFiles = new HashSet<FileObject>(filesWithErrors);
        HashSet<FileObject> nueFilesWithErrors = new HashSet<FileObject>();
        try {
            for (URL u : TaskCache.getDefault().getAllFilesWithRecord(root.toURL())) {
                if (cancelled.get()) {
                    return;
                }
                FileObject file = URLMapper.findFileObject((URL)u);
                if (file == null) continue;
                List<Task> result = TaskCache.getDefault().getErrors(file);
                LOG.log(Level.FINE, "Setting {1} for {0}\n", new Object[]{file, result});
                callback.setTasks(file, result);
                if (fixedFiles.remove((Object)file)) continue;
                nueFilesWithErrors.add(file);
            }
        }
        catch (IOException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        for (FileObject f : fixedFiles) {
            LOG.log(Level.FINE, "Clearing errors for {0}", (Object)f);
            callback.setTasks(f, Collections.emptyList());
        }
        filesWithErrors.addAll(nueFilesWithErrors);
    }

    static {
        TASKS = new HashMap<RequestProcessor.Task, Work>();
        WORKER = new RequestProcessor("Java Task Provider");
        root2FilesWithAttachedErrors = new WeakHashMap<FileObject, Set<FileObject>>();
    }

    private static final class Work
    implements Runnable {
        private final FileObject fileOrRoot;
        private final PushTaskScanner.Callback callback;
        private final AtomicBoolean canceled = new AtomicBoolean();

        public Work(FileObject fileOrRoot, PushTaskScanner.Callback callback) {
            this.fileOrRoot = fileOrRoot;
            this.callback = callback;
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.FINER, "Work created by: {0}", Arrays.toString(Thread.currentThread().getStackTrace()));
            }
        }

        public FileObject getFileOrRoot() {
            return this.fileOrRoot;
        }

        public PushTaskScanner.Callback getCallback() {
            return this.callback;
        }

        public void cancel() {
            this.canceled.set(true);
        }

        @Override
        public void run() {
            FileObject file = this.getFileOrRoot();
            LOG.log(Level.FINE, "dequeued work for: {0}", (Object)file);
            ClassPath cp = Utilities.getSourceClassPathFor(file);
            if (cp == null) {
                LOG.log(Level.FINE, "cp == null");
                return;
            }
            FileObject root = cp.findOwnerRoot(file);
            if (root == null) {
                Project p = FileOwnerQuery.getOwner((FileObject)file);
                Object[] arrobject = new Object[3];
                arrobject[0] = FileUtil.getFileDisplayName((FileObject)file);
                arrobject[1] = cp.toString(ClassPath.PathConversionMode.PRINT);
                arrobject[2] = p != null ? p.getClass() : "null";
                LOG.log(Level.WARNING, "file: {0} is not on its own source classpath: {1}, project: {2}", arrobject);
                return;
            }
            if (file.isData()) {
                List<Task> tasks = TaskCache.getDefault().getErrors(file);
                Set filesWithErrors = TaskProvider.getFilesWithAttachedErrors(root);
                if (tasks.isEmpty()) {
                    filesWithErrors.remove((Object)file);
                } else {
                    filesWithErrors.add(file);
                }
                LOG.log(Level.FINE, "setting {1} for {0}", new Object[]{file, tasks});
                this.getCallback().setTasks(file, tasks);
            } else {
                TaskProvider.updateErrorsInRoot(this.getCallback(), root, this.canceled);
            }
        }
    }

}

