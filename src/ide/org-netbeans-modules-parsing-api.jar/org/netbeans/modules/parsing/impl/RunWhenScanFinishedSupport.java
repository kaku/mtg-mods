/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 */
package org.netbeans.modules.parsing.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.spi.ParseException;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;

public class RunWhenScanFinishedSupport {
    private static final Logger LOG = Logger.getLogger(RunWhenScanFinishedSupport.class.getName());
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final List<DeferredTask> todo = Collections.synchronizedList(new LinkedList());

    private RunWhenScanFinishedSupport() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void performDeferredTasks() {
        DeferredTask[] _todo;
        List<DeferredTask> list = todo;
        synchronized (list) {
            _todo = todo.toArray(new DeferredTask[todo.size()]);
            todo.clear();
        }
        for (DeferredTask rq : _todo) {
            try {
                TaskProcessor.runUserTask(rq.task, rq.sources);
            }
            catch (ParseException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
            finally {
                rq.sync.taskFinished();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void performScan(@NonNull Runnable runnable) {
        lock.writeLock().lock();
        try {
            LOG.log(Level.FINE, "performScan:entry", runnable);
            runnable.run();
            LOG.log(Level.FINE, "performScan:exit", runnable);
        }
        finally {
            lock.writeLock().unlock();
        }
    }

    public static boolean isScanningThread() {
        return lock.isWriteLockedByCurrentThread();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    public static Future<Void> runWhenScanFinished(@NonNull Mutex.ExceptionAction<Void> task, @NonNull Collection<Source> sources) throws ParseException {
        assert (task != null);
        assert (sources != null);
        ScanSync sync = new ScanSync(task);
        DeferredTask r = new DeferredTask(sources, task, sync);
        todo.add(r);
        Set<? extends RepositoryUpdater.IndexingState> state = Utilities.getIndexingState();
        if (!state.isEmpty()) {
            return sync;
        }
        boolean locked = lock.readLock().tryLock();
        if (locked) {
            try {
                LOG.log(Level.FINE, "runWhenScanFinished:entry", task);
                if (todo.remove(r)) {
                    try {
                        TaskProcessor.runUserTask(task, sources);
                    }
                    finally {
                        sync.taskFinished();
                    }
                }
                LOG.log(Level.FINE, "runWhenScanFinished:exit", task);
            }
            finally {
                lock.readLock().unlock();
            }
        }
        return sync;
    }

    private static final class ScanSync
    implements Future<Void> {
        private Mutex.ExceptionAction<Void> task;
        private final CountDownLatch sync;
        private final AtomicBoolean canceled;

        public ScanSync(Mutex.ExceptionAction<Void> task) {
            assert (task != null);
            this.task = task;
            this.sync = new CountDownLatch(1);
            this.canceled = new AtomicBoolean(false);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            if (this.sync.getCount() == 0) {
                return false;
            }
            List list = todo;
            synchronized (list) {
                boolean _canceled = this.canceled.getAndSet(true);
                if (!_canceled) {
                    Iterator it = todo.iterator();
                    while (it.hasNext()) {
                        DeferredTask t = (DeferredTask)it.next();
                        if (t.task != this.task) continue;
                        it.remove();
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public boolean isCancelled() {
            return this.canceled.get();
        }

        @Override
        public synchronized boolean isDone() {
            return this.sync.getCount() == 0;
        }

        @Override
        public Void get() throws InterruptedException, ExecutionException {
            this.checkCaller();
            this.sync.await();
            return null;
        }

        @Override
        public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            this.checkCaller();
            if (!this.sync.await(timeout, unit)) {
                throw new TimeoutException();
            }
            return null;
        }

        private void taskFinished() {
            this.sync.countDown();
        }

        private void checkCaller() {
            if (RepositoryUpdater.getDefault().isProtectedModeOwner(Thread.currentThread())) {
                throw new IllegalStateException("ScanSync.get called by protected mode owner.");
            }
            boolean ae = false;
            if (!$assertionsDisabled) {
                ae = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (ae) {
                for (StackTraceElement stElement : Thread.currentThread().getStackTrace()) {
                    if ("org.netbeans.spi.project.ui.ProjectOpenedHook$1".equals(stElement.getClassName()) && ("projectOpened".equals(stElement.getMethodName()) || "projectClosed".equals(stElement.getMethodName()))) {
                        throw new AssertionError((Object)"Calling ParserManager.parseWhenScanFinished().get() from ProjectOpenedHook");
                    }
                }
            }
        }
    }

    private static final class DeferredTask {
        final Collection<Source> sources;
        final Mutex.ExceptionAction<Void> task;
        final ScanSync sync;

        public DeferredTask(Collection<Source> sources, Mutex.ExceptionAction<Void> task, ScanSync sync) {
            assert (sources != null);
            assert (task != null);
            assert (sync != null);
            this.sources = sources;
            this.task = task;
            this.sync = sync;
        }
    }

}

