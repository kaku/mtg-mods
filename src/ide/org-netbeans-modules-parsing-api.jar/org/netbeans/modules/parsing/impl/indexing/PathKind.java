/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

public enum PathKind {
    SOURCE,
    LIBRARY,
    BINARY_LIBRARY,
    UNKNOWN_SOURCE;
    

    private PathKind() {
    }
}

