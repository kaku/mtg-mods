/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.LongHashMap;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Parameters;

public final class TimeStamps {
    private static final Logger LOG = Logger.getLogger(TimeStamps.class.getName());
    private static final String TIME_STAMPS_FILE = "timestamps.properties";
    private static final String VERSION = "#v2";
    private final Implementation impl;

    private TimeStamps(@NonNull Implementation impl) throws IOException {
        assert (impl != null);
        this.impl = impl;
    }

    public boolean checkAndStoreTimestamp(FileObject f, String relativePath) {
        return this.impl.checkAndStoreTimestamp(f, relativePath);
    }

    public Set<String> getUnseenFiles() {
        return this.impl.getUnseenFiles();
    }

    public void store() throws IOException {
        this.impl.store();
    }

    void resetToNow() {
        long now = System.currentTimeMillis();
        this.impl.reset(now);
    }

    void remove(@NonNull Iterable<? extends String> relativePaths) {
        Parameters.notNull((CharSequence)"relativePaths", relativePaths);
        this.impl.remove(relativePaths);
    }

    @NonNull
    Collection<? extends String> getEnclosedFiles(@NonNull String folder) {
        Parameters.notNull((CharSequence)"folder", (Object)folder);
        if (!folder.isEmpty() && folder.charAt(folder.length() - 1) != '/') {
            folder = folder + '/';
        }
        Collection<? extends String> res = this.impl.getEnclosedFiles(folder);
        return res;
    }

    public static TimeStamps forRoot(@NonNull URL root, boolean detectDeletedFiles) throws IOException {
        return new TimeStamps(new RegularImpl(root, detectDeletedFiles));
    }

    public static TimeStamps changedTransient() throws IOException {
        return new TimeStamps(new AllChangedTransientImpl());
    }

    public static boolean existForRoot(URL root) throws IOException {
        assert (root != null);
        FileObject cacheDir = CacheFolder.getDataFolder(root, true);
        if (cacheDir != null) {
            return new File(FileUtil.toFile((FileObject)cacheDir), "timestamps.properties").exists();
        }
        return false;
    }

    private static class AllChangedTransientImpl
    implements Implementation {
        private AllChangedTransientImpl() {
        }

        @Override
        public boolean checkAndStoreTimestamp(FileObject root, String relativePath) {
            return false;
        }

        @Override
        public Set<String> getUnseenFiles() {
            return Collections.emptySet();
        }

        @Override
        public void reset(long time) {
        }

        @Override
        public void remove(@NonNull Iterable<? extends String> relativePaths) {
        }

        @NonNull
        @Override
        public Collection<? extends String> getEnclosedFiles(@NonNull String relativePath) {
            return Collections.emptySet();
        }

        @Override
        public void store() throws IOException {
        }
    }

    private static final class RegularImpl
    implements Implementation {
        private final URL root;
        private final LongHashMap<String> timestamps = new LongHashMap();
        private final Set<String> unseen;
        private FileObject rootFoCache;

        private RegularImpl(@NonNull URL root, boolean detectDeletedFiles) throws IOException {
            assert (root != null);
            this.root = root;
            this.unseen = detectDeletedFiles ? new HashSet() : null;
            this.load();
        }

        @Override
        public boolean checkAndStoreTimestamp(FileObject f, String relativePath) {
            boolean isUpToDate;
            String fileId;
            if (this.rootFoCache == null) {
                this.rootFoCache = URLMapper.findFileObject((URL)this.root);
            }
            String string = fileId = relativePath != null ? relativePath : URLMapper.findURL((FileObject)f, (int)1).toExternalForm();
            if (fileId == null) {
                throw new IllegalArgumentException(MessageFormat.format("The fileId == null, relativePath: {0}, FileObject: {1}, URL: {2}, external URL: {3}", new Object[]{relativePath, f, f.toURL().toExternalForm(), URLMapper.findURL((FileObject)f, (int)1).toExternalForm()}));
            }
            long fts = f.lastModified().getTime();
            long lts = this.timestamps.put(fileId, fts);
            if (lts == Long.MIN_VALUE) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "{0}: lastTimeStamp=null, fileTimeStamp={1} is out of date", new Object[]{f.getPath(), fts});
                }
                return false;
            }
            if (this.unseen != null) {
                this.unseen.remove(fileId);
            }
            boolean bl = isUpToDate = lts == fts;
            if (!isUpToDate && LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "{0}: lastTimeStamp={1}, fileTimeStamp={2} is out of date", new Object[]{f.getPath(), lts, fts});
            }
            return isUpToDate;
        }

        @Override
        public Set<String> getUnseenFiles() {
            return this.unseen;
        }

        @Override
        public void reset(long value) {
            for (LongHashMap.Entry<String> entry : this.timestamps.entrySet()) {
                entry.setValue(value);
            }
        }

        @Override
        public void remove(@NonNull Iterable<? extends String> relativePaths) {
            for (String relPath : relativePaths) {
                this.timestamps.remove(relPath);
            }
        }

        @NonNull
        @Override
        public Collection<? extends String> getEnclosedFiles(@NonNull String relativePath) {
            HashSet<String> res = new HashSet<String>();
            for (String filePath : this.timestamps.keySet()) {
                if (!filePath.startsWith(relativePath)) continue;
                res.add(filePath);
            }
            return res;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void store() throws IOException {
            File cacheDir = FileUtil.toFile((FileObject)CacheFolder.getDataFolder(this.root));
            File f = new File(cacheDir, "timestamps.properties");
            assert (f != null);
            try {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter((OutputStream)new FileOutputStream(f), "UTF-8"));
                try {
                    if (this.unseen != null) {
                        this.timestamps.keySet().removeAll(this.unseen);
                    }
                    out.write("#v2");
                    out.newLine();
                    for (LongHashMap.Entry<String> entry : this.timestamps.entrySet()) {
                        out.write(entry.getKey());
                        out.write(61);
                        out.write(Long.toString(entry.getValue()));
                        out.newLine();
                    }
                    out.flush();
                }
                finally {
                    out.close();
                }
            }
            catch (IOException e) {
                LOG.log(Level.FINE, null, e);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void load() throws IOException {
            File cacheDir = FileUtil.toFile((FileObject)CacheFolder.getDataFolder(this.root));
            File f = new File(cacheDir, "timestamps.properties");
            if (f.exists()) {
                try {
                    boolean readOldPropertiesFormat;
                    block22 : {
                        readOldPropertiesFormat = false;
                        BufferedReader in = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(f), "UTF-8"));
                        try {
                            String line = in.readLine();
                            if (line != null && line.startsWith("#v2")) {
                                LOG.log(Level.FINE, "{0}: reading {1} timestamps", new Object[]{f.getPath(), "#v2"});
                                while (null != (line = in.readLine())) {
                                    int idx = line.indexOf(61);
                                    if (idx == -1) continue;
                                    try {
                                        String path = line.substring(0, idx);
                                        if (!path.isEmpty() && path.charAt(0) != '/') {
                                            long ts = Long.parseLong(line.substring(idx + 1));
                                            this.timestamps.put(path, ts);
                                            continue;
                                        }
                                        LOG.log(Level.WARNING, "Invalid timestamp entry {0} in {1}", new Object[]{path, f.getAbsolutePath()});
                                    }
                                    catch (NumberFormatException nfe) {
                                        LOG.log(Level.FINE, "Invalid timestamp: line={0}, timestamps={1}, exception={2}", new Object[]{line, f.getPath(), nfe});
                                    }
                                }
                                break block22;
                            }
                            readOldPropertiesFormat = true;
                        }
                        finally {
                            in.close();
                        }
                    }
                    if (readOldPropertiesFormat) {
                        LOG.log(Level.FINE, "{0}: reading old Properties timestamps", f.getPath());
                        Properties p = new Properties();
                        FileInputStream in = new FileInputStream(f);
                        try {
                            p.load(in);
                        }
                        finally {
                            in.close();
                        }
                        for (Map.Entry entry : p.entrySet()) {
                            try {
                                String fileId = (String)entry.getKey();
                                if (fileId == null) continue;
                                this.timestamps.put(fileId, Long.parseLong((String)entry.getValue()));
                            }
                            catch (NumberFormatException nfe) {
                                LOG.log(Level.FINE, "Invalid timestamp: key={0}, value={1}, timestamps={2}, exception={3}", new Object[]{entry.getKey(), entry.getValue(), f, nfe});
                            }
                        }
                    }
                    if (this.unseen != null) {
                        for (String k : this.timestamps.keySet()) {
                            this.unseen.add(k);
                        }
                    }
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.log(Level.FINEST, "Timestamps loaded from {0}:", f.getPath());
                        for (LongHashMap.Entry entry : this.timestamps.entrySet()) {
                            LOG.log(Level.FINEST, "{0}={1}", new Object[]{entry.getKey(), Long.toString(entry.getValue())});
                        }
                        LOG.log(Level.FINEST, "---------------------------");
                    }
                }
                catch (Exception e) {
                    this.timestamps.clear();
                    LOG.log(Level.FINE, null, e);
                }
            }
        }
    }

    private static interface Implementation {
        public boolean checkAndStoreTimestamp(FileObject var1, String var2);

        public Set<String> getUnseenFiles();

        public void reset(long var1);

        public void remove(@NonNull Iterable<? extends String> var1);

        @NonNull
        public Collection<? extends String> getEnclosedFiles(@NonNull String var1);

        public void store() throws IOException;
    }

}

