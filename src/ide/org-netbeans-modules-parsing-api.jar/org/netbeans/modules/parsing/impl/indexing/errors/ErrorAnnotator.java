/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.fileinfo.NonRecursiveFolder
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.modules.masterfs.providers.AnnotationProvider
 *  org.netbeans.modules.masterfs.providers.InterceptionListener
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileStatusEvent
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.parsing.impl.indexing.errors;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.masterfs.providers.AnnotationProvider;
import org.netbeans.modules.masterfs.providers.InterceptionListener;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.errors.Settings;
import org.netbeans.modules.parsing.impl.indexing.errors.TaskCache;
import org.netbeans.modules.parsing.impl.indexing.errors.Utilities;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileStatusEvent;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.RequestProcessor;

public class ErrorAnnotator
extends AnnotationProvider {
    private static final Logger LOG = Logger.getLogger(ErrorAnnotator.class.getName());
    private static final String ERROR_BADGE_URL = "org/netbeans/modules/parsing/impl/resources/error-badge.gif";
    private static final int IN_ERROR_REC = 1;
    private static final int IN_ERROR_NONREC = 2;
    private static final int INVALID = 4;
    private Map<FileObject, Integer> knownFiles2Error = new WeakHashMap<FileObject, Integer>();
    private long cumulativeTime;
    private Collection<FileObject> toProcess = null;
    private final RequestProcessor WORKER_THREAD = new RequestProcessor("ErrorAnnotator worker", 1);
    private final RequestProcessor.Task WORKER;
    private Map<FileSystem, FileChangeListener> system2RecursiveListener;

    public ErrorAnnotator() {
        this.WORKER = this.WORKER_THREAD.create(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Collection toProcess;
                long startTime = System.currentTimeMillis();
                ErrorAnnotator errorAnnotator = ErrorAnnotator.this;
                synchronized (errorAnnotator) {
                    toProcess = ErrorAnnotator.this.toProcess;
                    ErrorAnnotator.this.toProcess = null;
                }
                for (FileObject f : toProcess) {
                    boolean stateChanged;
                    ErrorAnnotator errorAnnotator2 = ErrorAnnotator.this;
                    synchronized (errorAnnotator2) {
                        Integer currentState = (Integer)ErrorAnnotator.this.knownFiles2Error.get((Object)f);
                        if (currentState != null && (currentState & 4) == 0) {
                            continue;
                        }
                    }
                    ErrorAnnotator.this.ensureListensOnFS(f);
                    boolean recError = false;
                    boolean nonRecError = false;
                    if (f.isData()) {
                        recError = nonRecError = TaskCache.getDefault().isInError(f, true);
                    } else {
                        ClassPath source = Utilities.getSourceClassPathFor(f);
                        if (source == null) {
                            Project p = FileOwnerQuery.getOwner((FileObject)f);
                            if (p != null) {
                                for (FileObject root : Utilities.findIndexedRootsUnderDirectory(p, f)) {
                                    recError |= TaskCache.getDefault().isInError(root, true);
                                }
                            }
                        } else {
                            recError = TaskCache.getDefault().isInError(f, true);
                            nonRecError = TaskCache.getDefault().isInError(f, false);
                        }
                    }
                    Integer value = (recError ? 1 : 0) | (nonRecError ? 2 : 0);
                    ErrorAnnotator i$ = ErrorAnnotator.this;
                    synchronized (i$) {
                        Integer origInteger = (Integer)ErrorAnnotator.this.knownFiles2Error.get((Object)f);
                        int orig = origInteger != null ? origInteger & -5 : 0;
                        stateChanged = orig != value;
                        ErrorAnnotator.this.knownFiles2Error.put(f, value);
                    }
                    if (!stateChanged) continue;
                    ErrorAnnotator.this.fireFileStatusChanged(Collections.singleton(f));
                }
                long endTime = System.currentTimeMillis();
                Logger.getLogger(ErrorAnnotator.class.getName()).log(Level.FINE, "time spent in error annotations computation: {0}, cumulative time: {1}", new Object[]{endTime - startTime, ErrorAnnotator.access$314(ErrorAnnotator.this, endTime - startTime)});
            }
        });
        this.system2RecursiveListener = new WeakHashMap<FileSystem, FileChangeListener>();
    }

    public String annotateName(String name, Set files) {
        return null;
    }

    public Image annotateIcon(Image icon, int iconType, Set<? extends FileObject> files) {
        boolean singleFile;
        if (!Settings.isBadgesEnabled()) {
            return null;
        }
        boolean inError = false;
        boolean bl = singleFile = files.size() == 1;
        if (files instanceof NonRecursiveFolder) {
            FileObject folder = ((NonRecursiveFolder)files).getFolder();
            inError = this.isInError(folder, false, true);
            singleFile = false;
        } else {
            for (FileObject o : files) {
                if (!(o instanceof FileObject)) continue;
                FileObject f = o;
                if (f.isFolder()) {
                    singleFile = false;
                    if (this.isInError(f, true, !inError)) {
                        inError = true;
                        continue;
                    }
                    if (!inError) continue;
                    continue;
                }
                if (!f.isData() || !this.isInError(f, true, !inError)) continue;
                inError = true;
            }
        }
        Logger.getLogger(ErrorAnnotator.class.getName()).log(Level.FINE, "files={0}, in error={1}", new Object[]{files, inError});
        if (inError) {
            URL errorBadgeIconURL = ErrorAnnotator.class.getResource("/org/netbeans/modules/parsing/impl/resources/error-badge.gif");
            assert (errorBadgeIconURL != null);
            String errorBadgeSingleTP = "<img src=\"" + errorBadgeIconURL + "\">&nbsp;" + NbBundle.getMessage(ErrorAnnotator.class, (String)"TP_ErrorBadgeSingle");
            Image errorBadge = ImageUtilities.loadImage((String)"org/netbeans/modules/parsing/impl/resources/error-badge.gif");
            assert (errorBadge != null);
            String errorBadgeFolderTP = "<img src=\"" + errorBadgeIconURL + "\">&nbsp;" + NbBundle.getMessage(ErrorAnnotator.class, (String)"TP_ErrorBadgeFolder");
            Image i = ImageUtilities.mergeImages((Image)icon, (Image)(singleFile ? ImageUtilities.assignToolTipToImage((Image)errorBadge, (String)errorBadgeSingleTP) : ImageUtilities.assignToolTipToImage((Image)errorBadge, (String)errorBadgeFolderTP)), (int)0, (int)8);
            Iterator it = Lookup.getDefault().lookupAll(AnnotationProvider.class).iterator();
            boolean found = false;
            while (it.hasNext()) {
                AnnotationProvider p = (AnnotationProvider)it.next();
                if (found) {
                    Image res = p.annotateIcon(i, iconType, files);
                    if (res == null) continue;
                    return res;
                }
                found = p == this;
            }
            return i;
        }
        return null;
    }

    public String annotateNameHtml(String name, Set files) {
        return null;
    }

    public Action[] actions(Set files) {
        return null;
    }

    public InterceptionListener getInterceptionListener() {
        return null;
    }

    public void updateAllInError() {
        try {
            File[] roots;
            for (File root : roots = File.listRoots()) {
                FileObject rootFO = FileUtil.toFileObject((File)root);
                if (rootFO == null) continue;
                this.fireFileStatusChanged(new FileStatusEvent(rootFO.getFileSystem(), true, false));
            }
        }
        catch (FileStateInvalidException ex) {
            LOG.log(Level.INFO, ex.getMessage(), (Throwable)ex);
        }
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public synchronized void updateInError(Set<URL> urls) {
        HashSet<FileObject> toRefresh = new HashSet<FileObject>();
        for (FileObject f : this.knownFiles2Error.keySet()) {
            try {
                URL furl = f.getURL();
                assert (PathRegistry.noHostPart(furl));
                if (!urls.contains(furl)) continue;
                toRefresh.add(f);
                Integer i = this.knownFiles2Error.get((Object)f);
                if (i == null) continue;
                this.knownFiles2Error.put(f, i | 4);
                this.enqueue(f);
            }
            catch (IOException e) {
                LOG.log(Level.INFO, e.getMessage(), e);
            }
        }
    }

    public void fireFileStatusChanged(Set<FileObject> fos) {
        if (fos.isEmpty()) {
            return;
        }
        try {
            this.fireFileStatusChanged(new FileStatusEvent(fos.iterator().next().getFileSystem(), fos, true, false));
        }
        catch (FileStateInvalidException ex) {
            LOG.log(Level.INFO, ex.getMessage(), (Throwable)ex);
        }
    }

    public static ErrorAnnotator getAnnotator() {
        for (AnnotationProvider ap : Lookup.getDefault().lookupAll(AnnotationProvider.class)) {
            if (ap.getClass() != ErrorAnnotator.class) continue;
            return (ErrorAnnotator)ap;
        }
        return null;
    }

    private void enqueue(FileObject file) {
        if (this.toProcess == null) {
            this.toProcess = new LinkedList<FileObject>();
            this.WORKER.schedule(50);
        }
        this.toProcess.add(file);
    }

    private synchronized boolean isInError(FileObject file, boolean recursive, boolean forceValue) {
        boolean result = false;
        Integer i = this.knownFiles2Error.get((Object)file);
        if (i != null) {
            boolean bl = result = (i & (recursive ? 1 : 2)) != 0;
            if ((i & 4) == 0) {
                return result;
            }
        }
        if (!forceValue) {
            if (i == null) {
                this.knownFiles2Error.put(file, null);
            }
            return result;
        }
        this.enqueue(file);
        return result;
    }

    private void ensureListensOnFS(FileObject f) {
        try {
            FileSystem fs = f.getFileSystem();
            if (!this.system2RecursiveListener.containsKey((Object)fs)) {
                RootAddedDeletedListener l = new RootAddedDeletedListener();
                this.system2RecursiveListener.put(fs, (FileChangeListener)l);
                fs.addFileChangeListener((FileChangeListener)l);
            }
        }
        catch (FileStateInvalidException ex) {
            LOG.log(Level.FINE, null, (Throwable)ex);
        }
    }

    static /* synthetic */ long access$314(ErrorAnnotator x0, long x1) {
        return x0.cumulativeTime += x1;
    }

    private final class RootAddedDeletedListener
    extends FileChangeAdapter {
        private RootAddedDeletedListener() {
        }

        public void fileFolderCreated(FileEvent fe) {
            this.update(fe);
        }

        public void fileDeleted(FileEvent fe) {
            this.update(fe);
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.update((FileEvent)fe);
        }

        private void update(FileEvent fe) {
            FileObject fo;
            RepositoryUpdater ru = RepositoryUpdater.getDefault();
            if (!ru.isCacheFile(fo = fe.getFile()) && ru.getOwningSourceRoot((Object)fo) == null) {
                try {
                    this.update(fe.getFile().getURL());
                }
                catch (FileStateInvalidException ex) {
                    LOG.log(Level.FINE, null, (Throwable)ex);
                }
            }
        }

        private void update(final URL root) {
            assert (PathRegistry.noHostPart(root));
            ErrorAnnotator.this.WORKER_THREAD.post(new Runnable(){

                @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
                @Override
                public void run() {
                    try {
                        HashSet<URL> toRefresh = new HashSet<URL>();
                        URL current = root;
                        toRefresh.add(current);
                        current = new URL(current, ".");
                        toRefresh.add(current);
                        for (int depth = current.getPath().split((String)"/").length - 1; depth > 0; --depth) {
                            current = new URL(current, "..");
                            toRefresh.add(current);
                        }
                        ErrorAnnotator.this.updateInError(toRefresh);
                    }
                    catch (MalformedURLException ex) {
                        LOG.log(Level.FINE, null, ex);
                    }
                }
            });
        }

    }

}

