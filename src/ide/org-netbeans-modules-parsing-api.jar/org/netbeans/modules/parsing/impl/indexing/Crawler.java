/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.impl.indexing.CancelRequest;
import org.netbeans.modules.parsing.impl.indexing.DeletedIndexable;
import org.netbeans.modules.parsing.impl.indexing.IndexableImpl;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.impl.indexing.TimeStamps;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.SuspendStatus;
import org.openide.filesystems.FileObject;

abstract class Crawler {
    private final URL root;
    private final Set<? extends TimeStampAction> checkTimeStamps;
    private final boolean supportsAllFiles;
    private final TimeStamps timeStamps;
    private final CancelRequest cancelRequest;
    private final SuspendStatus suspendStatus;
    private List<Indexable> resources;
    private List<Indexable> allResources;
    private List<Indexable> deleted;
    private boolean finished;
    private boolean changed;
    private boolean initialized;
    private static volatile boolean listenOnVisibility;

    protected Crawler(@NonNull URL root, Set<? extends TimeStampAction> checkTimeStamps, boolean detectDeletedFiles, boolean supportsAllFiles, @NonNull CancelRequest cancelRequest, @NonNull SuspendStatus suspendStatus) throws IOException {
        this.root = root;
        this.checkTimeStamps = checkTimeStamps;
        this.timeStamps = checkTimeStamps.contains((Object)TimeStampAction.UPDATE) ? TimeStamps.forRoot(root, detectDeletedFiles) : TimeStamps.changedTransient();
        this.supportsAllFiles = supportsAllFiles;
        this.cancelRequest = cancelRequest;
        this.suspendStatus = suspendStatus;
    }

    public static boolean listenOnVisibility() {
        return listenOnVisibility;
    }

    @NonNull
    public final List<Indexable> getResources() throws IOException {
        this.init();
        return this.resources;
    }

    @CheckForNull
    public final List<Indexable> getAllResources() throws IOException {
        this.init();
        return this.checkTimeStamps.contains((Object)TimeStampAction.CHECK) || !this.supportsAllFiles ? this.allResources : this.resources;
    }

    @NonNull
    public final List<Indexable> getDeletedResources() throws IOException {
        this.init();
        return this.deleted;
    }

    public final void storeTimestamps() throws IOException {
        this.init();
        this.timeStamps.store();
    }

    public final boolean hasChanged() throws IOException {
        this.init();
        return this.changed;
    }

    public final boolean isFinished() throws IOException {
        this.init();
        return this.finished;
    }

    protected final boolean isUpToDate(@NonNull FileObject f, @NullAllowed String relativePath) {
        boolean upToDate = this.timeStamps.checkAndStoreTimestamp(f, relativePath);
        return this.checkTimeStamps.contains((Object)TimeStampAction.CHECK) ? upToDate : false;
    }

    protected final boolean isCancelled() {
        return this.cancelRequest.isRaised();
    }

    protected final void parkWhileSuspended() {
        try {
            this.suspendStatus.parkWhileSuspended();
        }
        catch (InterruptedException ex) {
            // empty catch block
        }
    }

    @NonNull
    protected final Indexable createIndexable(@NonNull IndexableImpl impl) {
        return SPIAccessor.getInstance().create(impl);
    }

    static void setListenOnVisibility(boolean value) {
        listenOnVisibility = value;
    }

    protected abstract boolean collectResources(@NonNull Collection<Indexable> var1, @NonNull Collection<Indexable> var2);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void init() throws IOException {
        if (!this.initialized) {
            try {
                ArrayList<Indexable> _resources = new ArrayList<Indexable>();
                NullList<Indexable> _allResources = this.checkTimeStamps.contains((Object)((Object)TimeStampAction.CHECK)) && this.supportsAllFiles ? new ArrayList<E>() : new NullList<T>();
                this.finished = this.collectResources(_resources, _allResources);
                this.resources = Collections.unmodifiableList(_resources);
                this.allResources = this.checkTimeStamps.contains((Object)((Object)TimeStampAction.CHECK)) && this.supportsAllFiles ? Collections.unmodifiableList(_allResources) : null;
                this.changed = !_resources.isEmpty();
                Set<String> unseen = this.timeStamps.getUnseenFiles();
                if (unseen != null) {
                    ArrayList<Indexable> _deleted = new ArrayList<Indexable>(unseen.size());
                    for (String u : unseen) {
                        _deleted.add(this.createIndexable(new DeletedIndexable(this.root, u)));
                    }
                    this.deleted = Collections.unmodifiableList(_deleted);
                } else {
                    this.deleted = Collections.emptyList();
                }
                this.changed |= !this.deleted.isEmpty();
            }
            finally {
                this.initialized = true;
            }
        }
    }

    private static class NullList<T>
    implements List<T> {
        private boolean changed;

        private NullList() {
        }

        @Override
        public int size() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isEmpty() {
            return !this.changed;
        }

        @Override
        public boolean contains(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterator<T> iterator() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean add(T e) {
            this.changed = true;
            return true;
        }

        @Override
        public void add(int index, T element) {
            this.changed = true;
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends T> c) {
            this.changed = true;
            return true;
        }

        @Override
        public boolean addAll(int index, Collection<? extends T> c) {
            this.changed = true;
            return true;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }

        @Override
        public T get(int index) {
            throw new UnsupportedOperationException();
        }

        @Override
        public T set(int index, T element) {
            throw new UnsupportedOperationException();
        }

        @Override
        public T remove(int index) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int indexOf(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int lastIndexOf(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ListIterator<T> listIterator() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ListIterator<T> listIterator(int index) {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<T> subList(int fromIndex, int toIndex) {
            throw new UnsupportedOperationException();
        }
    }

    static enum TimeStampAction {
        CHECK,
        UPDATE;
        

        private TimeStampAction() {
        }
    }

}

