/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

public class ScanCancelledException
extends Exception {
    private String location;

    public ScanCancelledException(String message, String location, StackTraceElement[] stack) {
        super(message);
        this.setStackTrace(stack);
        this.location = location;
    }

    public ScanCancelledException(String msg, String location, Throwable orig, StackTraceElement[] stack) {
        super(msg, orig);
        this.setStackTrace(stack);
        this.location = location;
    }

    public String getLocation() {
        return this.location;
    }
}

