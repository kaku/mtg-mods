/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.impl.ParserAccessor;
import org.netbeans.modules.parsing.impl.SelfProfile;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.SourceFlags;
import org.netbeans.modules.parsing.impl.Utilities;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.spi.EmbeddingProvider;
import org.netbeans.modules.parsing.spi.IndexingAwareParserResultTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.netbeans.modules.parsing.spi.TaskIndexingMode;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public class TaskProcessor {
    private static final String COMPAT_MODE = "org.netbeans.modules.parsing.impl.TaskProcessor.compatMode";
    private static final Logger LOGGER = Logger.getLogger(TaskProcessor.class.getName());
    private static final int SLOW_CANCEL_LIMIT = 50;
    private static final PriorityBlockingQueue<Request> requests = new PriorityBlockingQueue(10, new RequestPriorityComparator());
    private static final Map<Source, Collection<Request>> finishedRequests = new WeakHashMap<Source, Collection<Request>>();
    private static final Map<Source, Collection<Request>> waitingRequests = new WeakHashMap<Source, Collection<Request>>();
    private static final Collection<RemovedTask> toRemove = new LinkedList<RemovedTask>();
    static final RequestProcessor WORKER = new RequestProcessor(String.format("Editor Parsing Loop (%s)", System.getProperty("netbeans.buildnumber")), 1, false, false);
    private static final CurrentRequestReference currentRequest = new CurrentRequestReference();
    public static final Object INTERNAL_LOCK = new InternalLock();
    private static final ReentrantLock parserLock = new ReentrantLock(true);
    private static int lockCount = 0;
    private static final Pattern excludedTasks;
    private static final Pattern includedTasks;
    private static final Set<StackTraceElement> warnedAboutRunInEQ;
    static volatile boolean SAMPLING_ENABLED;
    private static final int DEFAULT_START_SAMPLING_AFTER = 500;
    private static final Sampler sampler;
    private static final RequestProcessor SAMPLING_RP;
    private static final RequestProcessor.Task SAMPLING_TASK;
    private static final AtomicReference<Request> rst;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void runUserTask(final Mutex.ExceptionAction<Void> task, Collection<Source> sources) throws ParseException {
        StackTraceElement stackTraceElement;
        Parameters.notNull((CharSequence)"task", task);
        if (sources.size() == 1) {
            SourceAccessor.getINSTANCE().assignListeners(sources.iterator().next());
        }
        boolean a = false;
        if (!$assertionsDisabled) {
            a = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (a && SwingUtilities.isEventDispatchThread() && (stackTraceElement = Util.findCaller(Thread.currentThread().getStackTrace(), TaskProcessor.class, ParserManager.class, "org.netbeans.api.java.source.JavaSource", "org.netbeans.modules.j2ee.metadata.model.api.support.annotation.AnnotationModelHelper")) != null && warnedAboutRunInEQ.add(stackTraceElement)) {
            LOGGER.log(Level.WARNING, "ParserManager.parse called in AWT event thread by: {0}", stackTraceElement);
        }
        Request request = currentRequest.cancel(new CancelStrategy(Parser.CancelReason.USER_TASK){

            @Override
            public boolean apply(@NonNull Request request) {
                return true;
            }
        });
        try {
            RepositoryUpdater.getDefault().suspend();
            try {
                parserLock.lock();
                try {
                    if (lockCount < 1) {
                        for (Source source : sources) {
                            SourceAccessor.getINSTANCE().invalidate(source, false);
                        }
                    }
                    ++lockCount;
                    Utilities.runPriorityIO(new Callable<Void>(){

                        @Override
                        public Void call() throws Exception {
                            task.run();
                            return null;
                        }
                    });
                }
                catch (Exception e) {
                    ParseException ioe = new ParseException();
                    ioe.initCause(e);
                    throw ioe;
                }
                finally {
                    --lockCount;
                    parserLock.unlock();
                }
            }
            finally {
                RepositoryUpdater.getDefault().resume();
            }
        }
        finally {
            currentRequest.cancelCompleted(request);
        }
    }

    public static void addPhaseCompletionTasks(@NonNull Collection<Pair<SchedulerTask, Class<? extends Scheduler>>> tasks, @NonNull SourceCache cache, boolean bridge) {
        Collection<? extends Request> rqs = TaskProcessor.toRequests(tasks, cache, bridge);
        if (TaskProcessor.handleAddRequests(cache.getSource(), rqs)) {
            TaskProcessor.cancelLowPriorityTask(rqs);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void removePhaseCompletionTasks(Collection<? extends SchedulerTask> tasks, Source source) {
        Parameters.notNull((CharSequence)"task", tasks);
        Parameters.notNull((CharSequence)"source", (Object)source);
        Object object = INTERNAL_LOCK;
        synchronized (object) {
            boolean wakeUp = false;
            Collection<Request> frqs = finishedRequests.get(source);
            Collection<Request> wrqs = waitingRequests.get(source);
            for (SchedulerTask task : tasks) {
                Request rq;
                Iterator<Request> it;
                boolean found = false;
                String taskClassName = task.getClass().getName();
                if (excludedTasks != null && excludedTasks.matcher(taskClassName).matches() && (includedTasks == null || !includedTasks.matcher(taskClassName).matches())) continue;
                if (frqs != null) {
                    it = frqs.iterator();
                    while (it.hasNext()) {
                        rq = it.next();
                        if (rq.task != task || rq.cache == null || rq.cache.getSource() != source) continue;
                        it.remove();
                        found = true;
                    }
                    if (frqs.isEmpty()) {
                        finishedRequests.remove(source);
                        frqs = null;
                    }
                }
                it = requests.iterator();
                while (it.hasNext()) {
                    rq = it.next();
                    if (rq.task != task || rq.cache == null || rq.cache.getSource() != source) continue;
                    it.remove();
                    found = true;
                }
                if (wrqs != null) {
                    it = wrqs.iterator();
                    while (it.hasNext()) {
                        rq = it.next();
                        if (rq.task != task || rq.cache == null || rq.cache.getSource() != source) continue;
                        it.remove();
                        found = true;
                    }
                    if (wrqs.isEmpty()) {
                        waitingRequests.remove(source);
                        wrqs = null;
                    }
                }
                if (!found) {
                    toRemove.add(new RemovedTask(source, task));
                    wakeUp = true;
                }
                SourceAccessor.getINSTANCE().taskRemoved(source);
            }
            if (wakeUp) {
                requests.add(Request.NONE);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void rescheduleTasks(final Collection<SchedulerTask> tasks, Source source, Class<? extends Scheduler> schedulerType) {
        Parameters.notNull((CharSequence)"task", tasks);
        Parameters.notNull((CharSequence)"source", (Object)source);
        Request request = currentRequest.cancel(new CancelStrategy(Parser.CancelReason.PARSER_RESULT_TASK){

            @Override
            public boolean apply(@NonNull Request request) {
                return tasks.contains(request.task);
            }
        });
        try {
            Object object = INTERNAL_LOCK;
            synchronized (object) {
                Collection<Request> cr = finishedRequests.get(source);
                if (cr != null) {
                    for (SchedulerTask task : tasks) {
                        if (request != null && request.task == task) continue;
                        ArrayList<Request> aRequests = new ArrayList<Request>();
                        Iterator<Request> it = cr.iterator();
                        while (it.hasNext()) {
                            Request fr = it.next();
                            if (task != fr.task) continue;
                            it.remove();
                            assert (fr.reschedule == ReschedulePolicy.ON_CHANGE);
                            fr.schedulerType = schedulerType;
                            aRequests.add(fr);
                            if (!cr.isEmpty()) break;
                            finishedRequests.remove(source);
                            break;
                        }
                        requests.addAll(aRequests);
                    }
                }
            }
        }
        finally {
            if (request != null) {
                currentRequest.cancelCompleted(request);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void updatePhaseCompletionTask(@NonNull Collection<Pair<SchedulerTask, Class<? extends Scheduler>>> add, @NonNull Collection<SchedulerTask> remove, @NonNull Source source, @NonNull SourceCache cache) {
        Parameters.notNull((CharSequence)"add", add);
        Parameters.notNull((CharSequence)"remove", remove);
        Parameters.notNull((CharSequence)"source", (Object)source);
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        if (add.isEmpty() && remove.isEmpty()) {
            return;
        }
        Collection<? extends Request> rqs = TaskProcessor.toRequests(add, cache, false);
        Object object = INTERNAL_LOCK;
        synchronized (object) {
            TaskProcessor.removePhaseCompletionTasks(remove, source);
            TaskProcessor.handleAddRequests(source, rqs);
        }
        TaskProcessor.cancelLowPriorityTask(rqs);
    }

    public static Request resetState(Source source, boolean mayInterruptParser, boolean sync) {
        assert (source != null);
        Request r = currentRequest.cancel(new CancelStrategy(Parser.CancelReason.SOURCE_MODIFICATION_EVENT, Request.DUMMY, mayInterruptParser){

            @Override
            public boolean apply(@NonNull Request request) {
                return true;
            }
        });
        if (sync && r != null) {
            Request oldR = rst.getAndSet(r);
            assert (oldR == null);
        }
        return r;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void resetStateImpl(Source source) {
        Request r = rst.getAndSet(null);
        currentRequest.cancelCompleted(r);
        if (source != null) {
            Object object = INTERNAL_LOCK;
            synchronized (object) {
                Collection<Request> cr;
                boolean reschedule = SourceAccessor.getINSTANCE().testAndCleanFlags(source, SourceFlags.RESCHEDULE_FINISHED_TASKS, EnumSet.of(SourceFlags.RESCHEDULE_FINISHED_TASKS, SourceFlags.CHANGE_EXPECTED));
                if (reschedule && (cr = finishedRequests.remove(source)) != null && cr.size() > 0) {
                    for (Request toAdd : cr) {
                        assert (toAdd.reschedule == ReschedulePolicy.ON_CHANGE);
                        requests.add(toAdd);
                    }
                }
                if ((cr = waitingRequests.remove(source)) != null && cr.size() > 0) {
                    for (Request toAdd : cr) {
                        requests.add(toAdd);
                    }
                }
            }
        }
    }

    public static boolean isCancelled(@NonNull SchedulerTask task) {
        assert (task != null);
        return currentRequest.isCancelled(task);
    }

    static void acquireParserLock() {
        RepositoryUpdater.getDefault().suspend();
        parserLock.lock();
    }

    static void releaseParserLock() {
        parserLock.unlock();
        RepositoryUpdater.getDefault().resume();
    }

    static boolean holdsParserLock() {
        return parserLock.isHeldByCurrentThread();
    }

    static void scheduleSpecialTask(final @NonNull Runnable runnable, final int priority) {
        assert (runnable != null);
        ParserResultTask<Parser.Result> task = new ParserResultTask<Parser.Result>(){

            @Override
            public int getPriority() {
                return priority;
            }

            @Override
            public Class<? extends Scheduler> getSchedulerClass() {
                return null;
            }

            @Override
            public void cancel() {
            }

            @Override
            public void run(Parser.Result result, SchedulerEvent event) {
                runnable.run();
            }
        };
        Set<Request> rqs = Collections.singleton(new Request(task, null, ReschedulePolicy.NEVER, null));
        if (TaskProcessor.handleAddRequests(null, rqs)) {
            TaskProcessor.cancelLowPriorityTask(rqs);
        }
    }

    @NonNull
    private static Collection<? extends Request> toRequests(@NonNull Collection<Pair<SchedulerTask, Class<? extends Scheduler>>> tasks, @NonNull SourceCache cache, boolean bridge) {
        Parameters.notNull((CharSequence)"task", tasks);
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        ArrayList<Request> _requests = new ArrayList<Request>();
        for (Pair<SchedulerTask, Class<? extends Scheduler>> task : tasks) {
            String taskClassName = ((SchedulerTask)task.first()).getClass().getName();
            if (excludedTasks != null && excludedTasks.matcher(taskClassName).matches() && (includedTasks == null || !includedTasks.matcher(taskClassName).matches())) continue;
            _requests.add(new Request((SchedulerTask)task.first(), cache, bridge ? ReschedulePolicy.ON_CHANGE : ReschedulePolicy.CANCELED, (Class)task.second()));
        }
        return _requests;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean handleAddRequests(@NullAllowed Source source, @NonNull Collection<? extends Request> requests) {
        Parameters.notNull((CharSequence)"requests", requests);
        if (requests.isEmpty()) {
            return false;
        }
        if (source != null) {
            SourceAccessor.getINSTANCE().assignListeners(source);
        }
        Object object = INTERNAL_LOCK;
        synchronized (object) {
            TaskProcessor.requests.addAll(requests);
        }
        return true;
    }

    private static void cancelLowPriorityTask(@NonNull Iterable<? extends Request> requests) {
        int priority = Integer.MAX_VALUE;
        for (Request r : requests) {
            priority = Math.min(priority, r.task.getPriority());
        }
        final int pf = priority;
        Request request = currentRequest.cancel(new CancelStrategy(Parser.CancelReason.PARSER_RESULT_TASK){

            @Override
            public boolean apply(Request request) {
                return pf < request.task.getPriority();
            }
        });
        currentRequest.cancelCompleted(request);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void cancelTask(@NonNull SchedulerTask task, @NonNull Parser.CancelReason reason) {
        assert (task != null);
        assert (reason != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        Utilities.setTaskCancelReason(reason);
        try {
            task.cancel();
        }
        finally {
            Utilities.setTaskCancelReason(null);
        }
    }

    static void cancelParser(@NonNull Parser parser, boolean callDeprecatedCancel, @NonNull Parser.CancelReason cancelReason, @NullAllowed SourceModificationEvent event) {
        assert (parser != null);
        assert (cancelReason != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        if (callDeprecatedCancel) {
            parser.cancel();
        }
        parser.cancel(cancelReason, event);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static <T extends Parser.Result> long callParserResultTask(@NonNull ParserResultTask<T> task, @NullAllowed T result, @NullAllowed SchedulerEvent event) {
        long now;
        long cancelTime;
        assert (task != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        assert (parserLock.isHeldByCurrentThread());
        sampler.enableSampling();
        try {
            task.run(result, event);
        }
        finally {
            now = System.currentTimeMillis();
            cancelTime = sampler.disableSampling();
        }
        return cancelTime == 0 ? 0 : now - cancelTime;
    }

    static List<Embedding> callEmbeddingProvider(@NonNull EmbeddingProvider embeddingProvider, @NonNull Snapshot snapshot) {
        assert (embeddingProvider != null);
        assert (snapshot != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        return embeddingProvider.getEmbeddings(snapshot);
    }

    public static void callUserTask(@NonNull UserTask task, @NonNull ResultIterator resultIterator) throws Exception {
        assert (task != null);
        assert (resultIterator != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        assert (parserLock.isHeldByCurrentThread());
        task.run(resultIterator);
    }

    public static void callParse(@NonNull Parser parser, @NullAllowed Snapshot snapshot, @NonNull Task task, @NullAllowed SourceModificationEvent event) throws ParseException {
        assert (parser != null);
        assert (task != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        assert (parserLock.isHeldByCurrentThread());
        parser.parse(snapshot, task, event);
    }

    public static Parser.Result callGetResult(@NonNull Parser parser, @NonNull Task task) throws ParseException {
        assert (parser != null);
        assert (task != null);
        assert (!Thread.holdsLock(INTERNAL_LOCK));
        assert (parserLock.isHeldByCurrentThread());
        return parser.getResult(task);
    }

    private static boolean holdsDocumentWriteLock(Iterable<Source> sources) {
        assert (sources != null);
        Class<AbstractDocument> docClass = AbstractDocument.class;
        try {
            Method method = docClass.getDeclaredMethod("getCurrentWriter", new Class[0]);
            method.setAccessible(true);
            Thread currentThread = Thread.currentThread();
            for (Source source : sources) {
                try {
                    Object result;
                    Document doc = source.getDocument(true);
                    if (!(doc instanceof AbstractDocument) || (result = method.invoke(doc, new Object[0])) != currentThread) continue;
                    return true;
                }
                catch (Exception e) {
                    Exceptions.printStackTrace((Throwable)e);
                    continue;
                }
            }
        }
        catch (NoSuchMethodException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        return false;
    }

    private static boolean shouldCall(@NonNull Source source, @NullAllowed SchedulerTask task, boolean checkScan) {
        boolean sourceInvalid = SourceAccessor.getINSTANCE().testFlag(source, SourceFlags.INVALID);
        boolean scanAffinity = true;
        if (checkScan) {
            boolean scanInProgress = IndexingManager.getDefault().isIndexing();
            boolean canRunDuringScan = task instanceof IndexingAwareParserResultTask && ((IndexingAwareParserResultTask)task).getIndexingMode() == TaskIndexingMode.ALLOWED_DURING_SCAN;
            boolean compatMode = "true".equals(System.getProperty("org.netbeans.modules.parsing.impl.TaskProcessor.compatMode"));
            scanAffinity = !scanInProgress || canRunDuringScan || compatMode;
        }
        boolean taskCancelled = currentRequest.isCancelled(task);
        return !sourceInvalid && scanAffinity && !taskCancelled;
    }

    static /* synthetic */ PriorityBlockingQueue access$1000() {
        return requests;
    }

    static /* synthetic */ CurrentRequestReference access$1100() {
        return currentRequest;
    }

    static /* synthetic */ Logger access$1200() {
        return LOGGER;
    }

    static /* synthetic */ int access$1508() {
        return lockCount++;
    }

    static /* synthetic */ boolean access$1600(Source x0, SchedulerTask x1, boolean x2) {
        return TaskProcessor.shouldCall(x0, x1, x2);
    }

    static /* synthetic */ int access$1510() {
        return lockCount--;
    }

    static /* synthetic */ Map access$1700() {
        return waitingRequests;
    }

    static /* synthetic */ Map access$1800() {
        return finishedRequests;
    }

    static {
        warnedAboutRunInEQ = new HashSet<StackTraceElement>();
        SAMPLING_ENABLED = false;
        sampler = new Sampler();
        SAMPLING_RP = new RequestProcessor(TaskProcessor.class.getName() + "-sampling");
        SAMPLING_TASK = SAMPLING_RP.create((Runnable)sampler);
        WORKER.submit((Runnable)new CompilationJob());
        Pattern _excludedTasks = null;
        try {
            String excludedValue = System.getProperty("org.netbeans.modules.parsing.impl.Source.excludedTasks");
            if (excludedValue != null) {
                _excludedTasks = Pattern.compile(excludedValue);
            }
        }
        catch (PatternSyntaxException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        excludedTasks = _excludedTasks;
        Pattern _includedTasks = null;
        try {
            String includedValue = System.getProperty("org.netbeans.modules.parsing.impl.Source.includedTasks");
            if (includedValue != null) {
                _includedTasks = Pattern.compile(includedValue);
            }
        }
        catch (PatternSyntaxException e) {
            Exceptions.printStackTrace((Throwable)e);
        }
        includedTasks = _includedTasks;
        rst = new AtomicReference();
    }

    private static class Sampler
    implements Runnable {
        private boolean samplingEnabled;
        private SelfProfile profiler;
        private long cancelTime;

        private Sampler() {
        }

        synchronized void schedule() {
            this.cancelTime = System.currentTimeMillis();
            if (this.samplingEnabled) {
                SAMPLING_TASK.schedule(Integer.getInteger("org.netbeans.modules.parsing.api.taskcancel.slowness.start", 500).intValue());
            }
        }

        synchronized void enableSampling() {
            this.cancelTime = 0;
            boolean ae = false;
            if (!$assertionsDisabled) {
                ae = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            if (ae && TaskProcessor.SAMPLING_ENABLED) {
                this.samplingEnabled = true;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        synchronized long disableSampling() {
            try {
                this.samplingEnabled = false;
                SAMPLING_TASK.cancel();
            }
            finally {
                if (this.profiler != null) {
                    this.profiler.stop();
                    this.profiler = null;
                }
            }
            return this.cancelTime;
        }

        @Override
        public synchronized void run() {
            if (!this.samplingEnabled) {
                return;
            }
            assert (this.profiler == null);
            this.profiler = new SelfProfile(System.currentTimeMillis());
        }
    }

    static final class RemovedTask
    extends WeakReference<Source>
    implements Runnable {
        private final SchedulerTask task;

        public RemovedTask(@NonNull Source src, @NonNull SchedulerTask task) {
            super(src, org.openide.util.Utilities.activeReferenceQueue());
            Parameters.notNull((CharSequence)"src", (Object)src);
            Parameters.notNull((CharSequence)"task", (Object)task);
            this.task = task;
        }

        public boolean equals(Object other) {
            if (!(other instanceof RemovedTask)) {
                return false;
            }
            RemovedTask otherRt = (RemovedTask)other;
            Source thisSrc = (Source)this.get();
            Source otherSrc = (Source)otherRt.get();
            return (thisSrc == null ? otherSrc == null : thisSrc.equals(otherSrc)) && this.task.equals(otherRt.task);
        }

        public int hashCode() {
            return this.task.hashCode();
        }

        public String toString() {
            return String.format("RemovedTask[%s, %s]", this.get(), this.task);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Object object = TaskProcessor.INTERNAL_LOCK;
            synchronized (object) {
                Iterator it = toRemove.iterator();
                while (it.hasNext()) {
                    RemovedTask rt = (RemovedTask)it.next();
                    if (rt != this) continue;
                    it.remove();
                    break;
                }
            }
        }
    }

    private static final class CurrentRequestReference {
        private Request reference;
        private Request canceledReference;
        private Parser activeParser;
        private SchedulerTask canceled;
        private static final Object CRR_LOCK = new CRRLock();

        private CurrentRequestReference() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        boolean setCurrentTask(Request reference) throws InterruptedException {
            boolean result = false;
            assert (!parserLock.isHeldByCurrentThread());
            assert (reference == null || reference.cache == null || !Thread.holdsLock(TaskProcessor.INTERNAL_LOCK));
            Object object = CRR_LOCK;
            synchronized (object) {
                while (this.canceledReference != null) {
                    CRR_LOCK.wait();
                }
                result = this.canceled != null;
                this.canceled = null;
                this.activeParser = null;
                this.reference = reference;
            }
            return result;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void clearCurrentTask() {
            Object object = CRR_LOCK;
            synchronized (object) {
                this.reference = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void setCurrentParser(Parser parser) {
            Object object = CRR_LOCK;
            synchronized (object) {
                this.activeParser = parser;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Request cancel(@NonNull CancelStrategy cancelStrategy) {
            Request request;
            block17 : {
                request = null;
                Parser parser = null;
                if (!TaskProcessor.WORKER.isRequestProcessorThread()) {
                    Object object = CRR_LOCK;
                    synchronized (object) {
                        if (this.reference != null && cancelStrategy.apply(this.reference)) {
                            assert (this.canceledReference == null);
                            this.canceledReference = request = this.reference;
                            this.reference = null;
                            this.canceled = request.task;
                            parser = this.activeParser;
                            sampler.schedule();
                        } else if (this.canceledReference == null && cancelStrategy.getRequestToCancel() != null) {
                            this.canceledReference = request = cancelStrategy.getRequestToCancel();
                            parser = this.activeParser;
                        }
                    }
                    Parser.CancelReason cancelReason = cancelStrategy.getCancelReason();
                    try {
                        try {
                            if (parser == null) break block17;
                            if (cancelReason == Parser.CancelReason.SOURCE_MODIFICATION_EVENT) {
                                SourceCache sc;
                                Source src;
                                if (request != null && (sc = request.cache) != null && (src = sc.getSource()) != null) {
                                    TaskProcessor.cancelParser(parser, cancelStrategy.callDeprecatedParserCancel(), cancelReason, SourceAccessor.getINSTANCE().getSourceModificationEvent(src));
                                }
                                break block17;
                            }
                            TaskProcessor.cancelParser(parser, false, cancelReason, null);
                        }
                        finally {
                            if (request != null) {
                                TaskProcessor.cancelTask(request.task, cancelReason);
                            }
                        }
                    }
                    catch (Throwable t) {
                        if (t instanceof ThreadDeath) {
                            throw (ThreadDeath)t;
                        }
                        Exceptions.printStackTrace((Throwable)t);
                    }
                }
            }
            return request;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void cancelCompleted(Request request) {
            if (request != null) {
                Object object = CRR_LOCK;
                synchronized (object) {
                    assert (request == this.canceledReference);
                    this.canceledReference = null;
                    CRR_LOCK.notify();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        boolean isCancelled(@NullAllowed SchedulerTask task) {
            Object object = CRR_LOCK;
            synchronized (object) {
                return this.canceled == task;
            }
        }

        private static class CRRLock {
            private CRRLock() {
            }
        }

    }

    private static abstract class CancelStrategy {
        private final Parser.CancelReason cancelReason;
        private final Request cancelReplace;
        private final boolean callDeprecatedParserCancel;

        CancelStrategy(@NonNull Parser.CancelReason cancelReason) {
            this(cancelReason, null, false);
        }

        CancelStrategy(@NonNull Parser.CancelReason cancelReason, @NullAllowed Request cancelReplace, boolean callDeprecatedParserCancel) {
            Parameters.notNull((CharSequence)"cancelReason", (Object)((Object)cancelReason));
            this.cancelReason = cancelReason;
            this.cancelReplace = cancelReplace;
            this.callDeprecatedParserCancel = callDeprecatedParserCancel;
        }

        @NonNull
        public final Parser.CancelReason getCancelReason() {
            return this.cancelReason;
        }

        @CheckForNull
        public final Request getRequestToCancel() {
            return this.cancelReplace;
        }

        public final boolean callDeprecatedParserCancel() {
            return this.callDeprecatedParserCancel;
        }

        public abstract boolean apply(@NonNull Request var1);
    }

    private static class RequestPriorityComparator
    implements Comparator<Request> {
        private RequestPriorityComparator() {
        }

        @Override
        public int compare(Request r1, Request r2) {
            assert (r1 != null && r2 != null);
            return r1.task.getPriority() - r2.task.getPriority();
        }
    }

    public static class Request {
        static final Request DUMMY = new Request(){

            @Override
            public String toString() {
                return "DUMMY";
            }
        };
        static final Request NONE = new Request(){

            @Override
            public String toString() {
                return "NONE";
            }
        };
        private final SchedulerTask task;
        private final SourceCache cache;
        private final ReschedulePolicy reschedule;
        private Class<? extends Scheduler> schedulerType;

        private Request(SchedulerTask task, SourceCache cache, ReschedulePolicy reschedule, Class<? extends Scheduler> schedulerType) {
            assert (task != null);
            assert (reschedule != null);
            this.task = task;
            this.cache = cache;
            this.reschedule = reschedule;
            this.schedulerType = schedulerType;
        }

        private Request() {
            this(new ParserResultTask(){

                @Override
                public int getPriority() {
                    return 0;
                }

                @Override
                public Class<? extends Scheduler> getSchedulerClass() {
                    return null;
                }

                @Override
                public void cancel() {
                }

                public void run(Parser.Result result, SchedulerEvent event) {
                }
            }, null, ReschedulePolicy.NEVER, null);
        }

        public String toString() {
            if (this.reschedule != ReschedulePolicy.NEVER) {
                Object[] arrobject = new Object[3];
                arrobject[0] = System.identityHashCode(this);
                arrobject[1] = this.task == null ? null : this.task.toString();
                arrobject[2] = this.cache == null ? null : this.cache.toString();
                return String.format("Periodic request %d to perform: %s on: %s", arrobject);
            }
            Object[] arrobject = new Object[3];
            arrobject[0] = System.identityHashCode(this);
            arrobject[1] = this.task == null ? null : this.task.toString();
            arrobject[2] = this.cache == null ? null : this.cache.toString();
            return String.format("One time request %d to perform: %s on: %s", arrobject);
        }

        public int hashCode() {
            return this.task == null ? 0 : this.task.getPriority();
        }

        public boolean equals(Object other) {
            if (other instanceof Request) {
                Request otherRequest = (Request)other;
                return this.reschedule == otherRequest.reschedule && (this.cache == null ? otherRequest.cache == null : this.cache.equals(otherRequest.cache)) && (this.task == null ? otherRequest.task == null : this.task.equals(otherRequest.task));
            }
            return false;
        }

        static /* synthetic */ Class access$800(Request x0) {
            return x0.schedulerType;
        }

    }

    private static enum ReschedulePolicy {
        NEVER,
        CANCELED,
        ON_CHANGE;
        

        private ReschedulePolicy() {
        }
    }

    private static class CompilationJob
    implements Runnable {
        private CompilationJob() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         * Converted monitor instructions to comments
         * Lifted jumps to return sites
         */
        @Override
        public void run() {
            try {
                do {
                    try {}
                    catch (Throwable e) {
                        if (e instanceof InterruptedException) {
                            throw (InterruptedException)e;
                        }
                        if (e instanceof ThreadDeath) {
                            throw (ThreadDeath)e;
                        }
                        Exceptions.printStackTrace((Throwable)e);
                        continue;
                    }
                    break;
                    break;
                } while (true);
            }
            catch (InterruptedException ie) {
                Exceptions.printStackTrace((Throwable)ie);
                return;
            }
            do lbl-1000: // 12 sources:
            {
                if ((r = (Request)TaskProcessor.access$1000().take()) != null && r != Request.NONE) {
                    TaskProcessor.access$1100().setCurrentTask(r);
                    TaskProcessor.access$1200().log(Level.FINE, "Set current request to: {0}", r);
                    try {
                        block79 : {
                            sourceCache = Request.access$600(r);
                            if (sourceCache == null) {
                                if (!CompilationJob.$assertionsDisabled && !(Request.access$500(r) instanceof ParserResultTask)) {
                                    throw new AssertionError((Object)"Illegal request: EmbeddingProvider has to be bound to Source");
                                }
                                RepositoryUpdater.getDefault().suspend();
                                try {
                                    TaskProcessor.access$1300().lock();
                                    try {
                                        try {
                                            TaskProcessor.access$1200().log(Level.FINE, "Running Special Task: {0}", r);
                                            TaskProcessor.callParserResultTask((ParserResultTask)Request.access$500(r), null, null);
                                        }
                                        finally {
                                            TaskProcessor.access$1100().clearCurrentTask();
                                            cancelled = TaskProcessor.access$1000().contains(r);
                                            if (cancelled) ** GOTO lbl-1000
                                        }
                                    }
                                    catch (RuntimeException re) {
                                        Exceptions.printStackTrace((Throwable)re);
                                    }
                                    finally {
                                        TaskProcessor.access$1300().unlock();
                                    }
                                }
                                finally {
                                    RepositoryUpdater.getDefault().resume();
                                }
                            }
                            source = sourceCache.getSnapshot().getSource();
                            if (!CompilationJob.$assertionsDisabled && source == null) {
                                throw new AssertionError();
                            }
                            reschedule = false;
                            validFlags = 0;
                            var6_11 = TaskProcessor.INTERNAL_LOCK;
                            // MONITORENTER : var6_11
                            if (TaskProcessor.access$1400().contains(new RemovedTask(source, Request.access$500(r)))) {
                                validFlags = 1;
                            } else if (SourceAccessor.getINSTANCE().testFlag(source, SourceFlags.CHANGE_EXPECTED)) {
                                validFlags = 2;
                            }
                            // MONITOREXIT : var6_11
                            RepositoryUpdater.getDefault().suspend();
                            try {
                                if (validFlags != 0) break block79;
                                snapshot = null;
                                id = new long[]{-1};
                                if (SourceAccessor.getINSTANCE().testFlag(source, SourceFlags.INVALID)) {
                                    snapshot = sourceCache.createSnapshot(id);
                                }
                                TaskProcessor.access$1300().lock();
                                try {
                                    if (SourceAccessor.getINSTANCE().invalidate(source, id[0], (Snapshot)snapshot)) {
                                        TaskProcessor.access$1508();
                                        try {
                                            if (Request.access$500(r) instanceof EmbeddingProvider) {
                                                if (TaskProcessor.access$1600(source, Request.access$500(r), false)) {
                                                    sourceCache.refresh((EmbeddingProvider)Request.access$500(r), Request.access$800(r));
                                                }
                                                break block79;
                                            }
                                            TaskProcessor.access$1100().setCurrentParser(sourceCache.getParser());
                                            currentResult = sourceCache.getResult(Request.access$500(r));
                                            if (currentResult == null) break block79;
                                            try {
                                                if (!TaskProcessor.access$1600(source, Request.access$500(r), true)) break block79;
                                                try {
                                                    startTime = System.currentTimeMillis();
                                                    cancelTime = 0;
                                                    if (Request.access$500(r) instanceof ParserResultTask) {
                                                        TaskProcessor.access$1200().log(Level.FINE, "Running Task: {0}", r);
                                                        parserResultTask = (ParserResultTask)Request.access$500(r);
                                                        schedulerEvent = SourceAccessor.getINSTANCE().getSchedulerEvent(source, parserResultTask.getSchedulerClass());
                                                        cancelTime = TaskProcessor.callParserResultTask(parserResultTask, currentResult, schedulerEvent);
                                                    } else if (!CompilationJob.$assertionsDisabled) {
                                                        throw new AssertionError((Object)String.format("Unknown task type: %s : %s", new Object[]{Request.access$500(r), Request.access$500(r).getClass()}));
                                                    }
                                                    if (TaskProcessor.access$1200().isLoggable(Level.FINEST)) {
                                                        TaskProcessor.access$1200().log(Level.FINEST, "Executed task: {0} : {1} in {2} ms.", new Object[]{Request.access$500(r), Request.access$500(r).getClass(), System.currentTimeMillis() - startTime});
                                                    }
                                                    if (cancelTime > 50) {
                                                        TaskProcessor.access$1200().log(Level.INFO, "Task: {0} : {1} ignored cancel for {2} ms.", new Object[]{Request.access$500(r), Request.access$500(r).getClass(), cancelTime});
                                                    }
                                                    break block79;
                                                }
                                                catch (Exception re) {
                                                    Exceptions.printStackTrace((Throwable)re);
                                                }
                                                break block79;
                                            }
                                            finally {
                                                ParserAccessor.getINSTANCE().invalidate(currentResult);
                                            }
                                        }
                                        finally {
                                            TaskProcessor.access$1510();
                                        }
                                    }
                                    reschedule = true;
                                }
                                finally {
                                    TaskProcessor.access$1300().unlock();
                                }
                            }
                            finally {
                                RepositoryUpdater.getDefault().resume();
                            }
                        }
                        if (Request.access$700(r) != ReschedulePolicy.NEVER) {
                            reschedule |= TaskProcessor.access$1100().setCurrentTask(null);
                            snapshot = TaskProcessor.INTERNAL_LOCK;
                            // MONITORENTER : snapshot
                            if (!TaskProcessor.access$1400().contains(new RemovedTask(source, Request.access$500(r)))) {
                                if (validFlags == 2) {
                                    if (SourceAccessor.getINSTANCE().testFlag(source, SourceFlags.CHANGE_EXPECTED)) {
                                        rc = (Collection)TaskProcessor.access$1700().get(source);
                                        if (rc == null) {
                                            rc = new LinkedList<E>();
                                            TaskProcessor.access$1700().put(source, rc);
                                        }
                                        rc.add((Request)r);
                                        TaskProcessor.access$1200().log(Level.FINE, "Waiting Task: {0}", r);
                                    } else {
                                        TaskProcessor.access$1000().add(r);
                                        TaskProcessor.access$1200().log(Level.FINE, "Rescheduling Waiting Task: {0}", r);
                                    }
                                } else if (reschedule || SourceAccessor.getINSTANCE().testFlag(source, SourceFlags.INVALID)) {
                                    TaskProcessor.access$1000().add(r);
                                    TaskProcessor.access$1200().log(Level.FINE, "Rescheduling Canceled Task: {0}", r);
                                } else if (Request.access$700(r) == ReschedulePolicy.ON_CHANGE) {
                                    rc = (Collection)TaskProcessor.access$1800().get(Request.access$600(r).getSnapshot().getSource());
                                    if (rc == null) {
                                        rc = new LinkedList<E>();
                                        TaskProcessor.access$1800().put(Request.access$600(r).getSnapshot().getSource(), rc);
                                    }
                                    rc.add(r);
                                    TaskProcessor.access$1200().log(Level.FINE, "Finished ON_CHANGE Task: {0}", r);
                                } else {
                                    TaskProcessor.access$1200().log(Level.FINE, "Finished  CANCELED Task: {0}", r);
                                }
                            } else {
                                TaskProcessor.access$1200().log(Level.FINE, "Removing Task: {0}", r);
                            }
                            TaskProcessor.access$1400().clear();
                            // MONITOREXIT : snapshot
                        }
                        snapshot = TaskProcessor.INTERNAL_LOCK;
                        // MONITORENTER : snapshot
                        if (validFlags == 2 && !TaskProcessor.access$1400().contains(new RemovedTask(source, Request.access$500(r)))) {
                            if (SourceAccessor.getINSTANCE().testFlag(source, SourceFlags.CHANGE_EXPECTED)) {
                                rc = (LinkedList<Request>)TaskProcessor.access$1700().get(source);
                                if (rc == null) {
                                    rc = new LinkedList<Request>();
                                    TaskProcessor.access$1700().put(source, rc);
                                }
                                rc.add(r);
                                TaskProcessor.access$1200().log(Level.FINE, "Waiting NEVER Task: {0}", r.toString());
                            } else {
                                TaskProcessor.access$1000().add(r);
                                TaskProcessor.access$1200().log(Level.FINE, "Rescheduling Waiting NEVER Task: {0}", r.toString());
                            }
                        } else {
                            TaskProcessor.access$1200().log(Level.FINE, "Finished NEVER task: {0}", r.toString());
                        }
                        TaskProcessor.access$1400().clear();
                        // MONITOREXIT : snapshot
                        SourceAccessor.getINSTANCE().taskRemoved(source);
                    }
                    finally {
                        TaskProcessor.access$1100().setCurrentTask(null);
                    }
                    continue;
                }
                if (r == null) continue;
                sourceCache = TaskProcessor.INTERNAL_LOCK;
                // MONITORENTER : sourceCache
                TaskProcessor.access$1400().clear();
                // MONITOREXIT : sourceCache
            } while (true);
        }
    }

    private static class InternalLock {
        private InternalLock() {
        }
    }

}

