/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.parsing.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.spi.EmbeddingProvider;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.openide.filesystems.FileObject;

public final class DialogBindingEmbeddingProvider
extends EmbeddingProvider {
    private static final Logger LOG = Logger.getLogger(DialogBindingEmbeddingProvider.class.getName());

    @Override
    public List<Embedding> getEmbeddings(Snapshot snapshot) {
        Document doc = snapshot.getSource().getDocument(true);
        try {
            LanguagePath path = LanguagePath.get((Language)((Language)MimeLookup.getLookup((String)snapshot.getMimeType()).lookup(Language.class)));
            InputAttributes attributes = (InputAttributes)doc.getProperty(InputAttributes.class);
            Document baseDoc = (Document)attributes.getValue(path, (Object)"dialogBinding.document");
            FileObject baseFile = (FileObject)attributes.getValue(path, (Object)"dialogBinding.fileObject");
            int offset = (Integer)attributes.getValue(path, (Object)"dialogBinding.offset");
            int line = (Integer)attributes.getValue(path, (Object)"dialogBinding.line");
            int column = (Integer)attributes.getValue(path, (Object)"dialogBinding.column");
            int length = (Integer)attributes.getValue(path, (Object)"dialogBinding.length");
            Source base = baseDoc != null ? Source.create(baseDoc) : (baseFile != null ? Source.create(baseFile) : null);
            if (base == null) {
                return Collections.emptyList();
            }
            Snapshot baseSnapshot = SourceAccessor.getINSTANCE().getCache(base).getSnapshot();
            if (offset == -1) {
                int nextLso;
                int lso = SourceAccessor.getINSTANCE().getLineStartOffset(baseSnapshot, line);
                if (lso + column < (nextLso = SourceAccessor.getINSTANCE().getLineStartOffset(baseSnapshot, line + 1))) {
                    offset = lso + column;
                } else {
                    offset = nextLso - 1;
                    length = 0;
                    LOG.log(Level.INFO, "Column={0} not on the line={1}; dialog's content will be bound to the line's boundary", new Object[]{column, line});
                }
            }
            String baseMimeType = base.getMimeType();
            CharSequence part1 = baseSnapshot.getText().subSequence(0, offset);
            CharSequence part2 = snapshot.getText();
            CharSequence part3 = baseSnapshot.getText().subSequence(offset + length, baseSnapshot.getText().length());
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "\nsnapshot={0}\nbaseSnapshot={1}\ndoc={2}\nfile={3}\noffset={4}\nline={5}\ncolumn={6}\nlength={7}\npart1={8}\npart2={9}\npart3={10}\n", new Object[]{snapshot, baseSnapshot, baseDoc, baseFile, offset, line, column, length, part1.toString(), part2.toString(), part3.toString()});
            }
            ArrayList<Embedding> ret = new ArrayList<Embedding>(3);
            ret.add(snapshot.create(part1, baseMimeType));
            ret.add(snapshot.create(0, snapshot.getText().length(), baseMimeType));
            ret.add(snapshot.create(part3, baseMimeType));
            return Collections.singletonList(Embedding.create(ret));
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return Collections.emptyList();
        }
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public void cancel() {
    }

    public static final class Factory
    extends TaskFactory {
        public Collection<SchedulerTask> create(Snapshot snapshot) {
            return Collections.singletonList(new DialogBindingEmbeddingProvider());
        }
    }

}

