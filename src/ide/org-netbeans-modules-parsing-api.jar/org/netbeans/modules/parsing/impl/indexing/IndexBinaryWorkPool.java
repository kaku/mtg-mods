/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.net.URL;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.RequestProcessor;

class IndexBinaryWorkPool {
    private static final int MIN_PROC = 4;
    private static final int DEFAULT_PROC_COUNT = 2;
    private static final boolean PAR_DISABLED = Boolean.getBoolean("IndexBinaryWorkPool.sequential");
    private static final int PROC_COUNT = Integer.getInteger("IndexBinaryWorkPool.proc.count", 2);
    private static final Logger LOG = Logger.getLogger(IndexBinaryWorkPool.class.getName());
    private final Function<URL, Boolean> fnc;
    private final Callable<Boolean> cancel;
    private final Collection<? extends URL> binaries;

    IndexBinaryWorkPool(@NonNull Function<URL, Boolean> fnc, @NonNull Callable<Boolean> cancel, @NonNull Collection<? extends URL> binaries) {
        assert (fnc != null);
        assert (cancel != null);
        assert (binaries != null);
        this.fnc = fnc;
        this.cancel = cancel;
        this.binaries = binaries;
    }

    Pair<Boolean, Collection<? extends URL>> execute() {
        Strategy strategy = IndexBinaryWorkPool.getStrategy(this.binaries.size());
        assert (strategy != null);
        return strategy.execute(this.fnc, this.cancel, this.binaries);
    }

    @NonNull
    private static Strategy getStrategy(int binariesCount) {
        int procCount = Runtime.getRuntime().availableProcessors();
        boolean supportsPar = procCount >= 4;
        LOG.log(Level.FINER, "Proc Count: {0} Binaries Count: {1} Concurrent worker disabled: {2}", new Object[]{procCount, binariesCount, PAR_DISABLED});
        if (!PAR_DISABLED && supportsPar && binariesCount >= 2) {
            LOG.log(Level.FINE, "Using concurrent strategy, {0} workers", PROC_COUNT);
            return new ConcurrentStrategy();
        }
        LOG.fine("Using sequential strategy");
        return new SequentialStrategy();
    }

    static /* synthetic */ int access$100() {
        return PROC_COUNT;
    }

    private static class Task
    implements Callable<URL> {
        private final URL binary;
        private final Function<URL, Boolean> performer;
        private final Callable<Boolean> cancel;

        private Task(@NonNull URL binary, @NonNull Function<URL, Boolean> performer, @NonNull Callable<Boolean> cancel) {
            this.binary = binary;
            this.performer = performer;
            this.cancel = cancel;
        }

        @Override
        public URL call() throws Exception {
            return this.cancel.call() != false ? null : (this.performer.apply(this.binary) != false ? this.binary : null);
        }
    }

    private static class ConcurrentStrategy
    implements Strategy {
        private static final RequestProcessor RP = new RequestProcessor(ConcurrentStrategy.class.getName(), IndexBinaryWorkPool.access$100(), false, false);

        private ConcurrentStrategy() {
        }

        @NonNull
        @Override
        public Pair<Boolean, Collection<? extends URL>> execute(@NonNull Function<URL, Boolean> fnc, @NonNull Callable<Boolean> cancel, @NonNull Collection<? extends URL> binaries) {
            void success2;
            ExecutorCompletionService cs = new ExecutorCompletionService((Executor)RP);
            int submitted = 0;
            for (URL binary : binaries) {
                cs.submit(new Task(binary, fnc, cancel));
                ++submitted;
            }
            ArrayDeque<URL> result = new ArrayDeque<URL>();
            boolean i = false;
            while (++i < submitted) {
                try {
                    Future becomeURL = cs.take();
                    URL url = (URL)becomeURL.get();
                    if (url == null) continue;
                    result.add(url);
                    continue;
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            try {
                boolean success2 = cancel.call() == false;
            }
            catch (Exception e) {
                Exceptions.printStackTrace((Throwable)e);
                boolean success2 = false;
            }
            LOG.log(Level.FINER, "Canceled: {0}", success2 == false);
            return Pair.of((Object)((boolean)success2), result);
        }
    }

    private static class SequentialStrategy
    implements Strategy {
        private SequentialStrategy() {
        }

        @NonNull
        @Override
        public Pair<Boolean, Collection<? extends URL>> execute(@NonNull Function<URL, Boolean> fnc, @NonNull Callable<Boolean> cancel, @NonNull Collection<? extends URL> binaries) {
            ArrayDeque<URL> result;
            boolean success;
            result = new ArrayDeque<URL>(binaries.size());
            success = true;
            try {
                for (URL binary : binaries) {
                    if (cancel.call().booleanValue()) {
                        success = false;
                    } else {
                        if (fnc.apply(binary).booleanValue()) {
                            result.add(binary);
                            continue;
                        }
                        success = false;
                    }
                    break;
                }
            }
            catch (Exception ce) {
                success = false;
            }
            LOG.log(Level.FINER, "Canceled: {0}", !success);
            return Pair.of((Object)success, result);
        }
    }

    private static interface Strategy {
        @NonNull
        public Pair<Boolean, Collection<? extends URL>> execute(@NonNull Function<URL, Boolean> var1, @NonNull Callable<Boolean> var2, @NonNull Collection<? extends URL> var3);
    }

    static interface Function<P, R> {
        public R apply(P var1);
    }

}

