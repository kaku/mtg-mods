/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.sampler.Sampler
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.sampler.Sampler;
import org.openide.util.Exceptions;

final class SelfProfile {
    private static final Logger LOG = Logger.getLogger(SelfProfile.class.getName());
    private final Sampler profiler;
    private final long time;
    private volatile boolean profiling;

    SelfProfile(long when) {
        this.time = when;
        this.profiler = Sampler.createSampler((String)"taskcancel");
        this.profiling = true;
        LOG.finest("STARTED");
        if (this.profiler != null) {
            this.profiler.start();
            LOG.log(Level.FINE, "Profiling started {0} at {1}", new Object[]{this.profiler, this.time});
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final synchronized void stop() {
        if (!this.profiling) {
            return;
        }
        try {
            this.stopImpl();
        }
        catch (Exception ex) {
            LOG.log(Level.INFO, "Cannot stop profiling", ex);
        }
        finally {
            this.profiling = false;
        }
    }

    private void stopImpl() throws Exception {
        long now = System.currentTimeMillis();
        long delta = now - this.time;
        LOG.log(Level.FINE, "Profiling stopped at {0}", now);
        int report = Integer.getInteger("org.netbeans.modules.parsing.api.taskcancel.slowness.report", 1000);
        if (delta < (long)report) {
            LOG.finest("CANCEL");
            if (this.profiler != null) {
                this.profiler.cancel();
                LOG.log(Level.FINE, "Cancel profiling of {0}. Profiling {1}. Time {2} ms.", new Object[]{this.profiler, this.profiling, delta});
            }
            return;
        }
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(out);
            LOG.finest("LOGGED");
            if (this.profiler != null) {
                this.profiler.stopAndWriteTo(dos);
                LOG.log(Level.FINE, "Obtaining snapshot for {0} ms.", delta);
            }
            dos.close();
            if (dos.size() > 0) {
                Object[] params = new Object[]{out.toByteArray(), delta, "ParserResultTask-cancel"};
                Logger.getLogger("org.netbeans.ui.performance").log(Level.CONFIG, "Slowness detected", params);
                LOG.log(Level.FINE, "Snapshot sent to UI logger. Size {0} bytes.", dos.size());
            } else {
                LOG.log(Level.WARNING, "No snapshot taken");
            }
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }
}

