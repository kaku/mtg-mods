/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.StatusDisplayer$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.io.File;
import java.io.PrintStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.parsing.impl.indexing.RepositoryUpdater;
import org.netbeans.modules.parsing.impl.indexing.ScanCancelledException;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class LogContext {
    private static final Logger TEST_LOGGER = Logger.getLogger(RepositoryUpdater.class.getName() + ".tests");
    private static final RequestProcessor RP = new RequestProcessor("Thread dump shooter", 1);
    private static final int SECOND_DUMP_DELAY = 5000;
    private static int serial;
    private static volatile boolean closing;
    private String culpritIndexer;
    private String blockingClass;
    private Map<String, String[]> threadTraces;
    private Map<String, String> threadStatuses;
    private static final Pattern CONCURRENT_PATTERN;
    private static final Pattern PARSING_PATTERN;
    private static final String STACK_CONCURRENT_PREFIXES = "\\s+sun.*|\\s+java.util.concurrent.*|\\s+java.lang.*|\\s+org.openide.*Mutex.*";
    private static final String STACK_PARSING_PREFIXES = "\\s+org.netbeans.modules.parsing.impl";
    private volatile boolean updaterThreadAlive = false;
    private static final long EXEC_TRESHOLD;
    private static ThreadLocal<RootInfo> currentlyIndexedRoot;
    private static ThreadLocal<LogContext> currentLogContext;
    private int mySerial;
    private long storeTime;
    private final long timestamp;
    private long executed;
    private final EventType eventType;
    private final String message;
    private final StackTraceElement[] stackTrace;
    private LogContext predecessor;
    private final LogContext parent;
    private Queue<LogContext> absorbed;
    private String threadDump;
    private String secondDump;
    private Map<URL, Set<String>> reindexInitiators = Collections.emptyMap();
    private List<String> indexersAdded = Collections.emptyList();
    private Set<String> filePathsChanged = Collections.emptySet();
    private Set<String> classPathsChanged = Collections.emptySet();
    private Set<URL> rootsChanged = Collections.emptySet();
    private Set<URL> filesChanged = Collections.emptySet();
    private Set<URI> fileObjsChanged = Collections.emptySet();
    private Map<URL, RootInfo> scannedSourceRoots = new LinkedHashMap<URL, RootInfo>();
    private long crawlerTime;
    private long totalScanningTime;
    private long timeCutOff;
    private long finished;
    private Map<Thread, RootInfo> allCurrentRoots = new HashMap<Thread, RootInfo>();
    private Map<Thread, RootInfo> frozenCurrentRoots = Collections.emptyMap();
    private URL root;
    private boolean frozen;
    private Map<String, Long> totalIndexerTime = new HashMap<String, Long>();
    private long crawlerStart;
    private Callable<byte[]> profileDataSource;
    private static final Logger LOG;
    private static final String LOG_MESSAGE = "SCAN_CANCELLED";
    private static final String LOG_MESSAGE_EARLY = "SCAN_CANCELLED_EARLY";
    private static final String LOG_EXCEEDS_RATE = "SCAN_EXCEEDS_RATE {0}";
    private static final Stats STATS;
    private static final Logger BACKDOOR_LOG;

    public static void notifyClosing() {
        closing = true;
    }

    public static LogContext create(@NonNull EventType eventType, @NullAllowed String message) {
        return LogContext.create(eventType, message, null);
    }

    public static LogContext create(@NonNull EventType eventType, @NullAllowed String message, @NullAllowed LogContext parent) {
        return new LogContext(eventType, Thread.currentThread().getStackTrace(), message, parent);
    }

    @NonNull
    public static LogContext createAndAbsorb(@NonNull LogContext prototype) {
        LogContext ctx = new LogContext(prototype.eventType, prototype.stackTrace, String.format("Replacement of LogContext: [type: %s, message: %s]", new Object[]{prototype.eventType, prototype.message}), null);
        ctx.absorb(prototype);
        return ctx;
    }

    public String toString() {
        StringBuilder msg = new StringBuilder();
        this.createLogMessage(msg, new HashSet<LogContext>(), 0);
        return msg.toString();
    }

    private String createThreadDump() {
        StringBuilder sb = new StringBuilder();
        Map<Thread, StackTraceElement[]> allTraces = Thread.getAllStackTraces();
        for (Thread t : allTraces.keySet()) {
            StackTraceElement[] elems;
            sb.append(String.format("Thread id %d, \"%s\" (%s):\n", new Object[]{t.getId(), t.getName(), t.getState()}));
            for (StackTraceElement l : elems = allTraces.get(t)) {
                sb.append("\t").append(l).append("\n");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    private synchronized void checkConsistency() {
        long total = 0;
        for (RootInfo ri : this.scannedSourceRoots.values()) {
            total += ri.spent;
        }
        if (total != this.totalScanningTime) {
            System.err.println("total scanning time mismatch");
        }
    }

    private synchronized void freeze() {
        this.timeCutOff = System.currentTimeMillis();
        this.frozenCurrentRoots = new HashMap<Thread, RootInfo>(this.allCurrentRoots);
        for (RootInfo ri : this.frozenCurrentRoots.values()) {
            ri.finishCurrentIndexer(this.timeCutOff);
            this.finishScannedRoot(ri.url);
        }
        this.frozen = true;
        this.checkConsistency();
    }

    void log() {
        if (this.canLogScanCancel()) {
            this.log(true, true);
        } else {
            if (System.getProperty(RepositoryUpdater.PROP_SAMPLING) == null) {
                System.setProperty(RepositoryUpdater.PROP_SAMPLING, "oneshot");
            }
            LogRecord r = new LogRecord(Level.INFO, "SCAN_CANCELLED_EARLY");
            r.setResourceBundle(NbBundle.getBundle(LogContext.class));
            r.setResourceBundleName(LogContext.class.getPackage().getName() + ".Bundle");
            r.setLoggerName(LOG.getName());
            LOG.log(r);
            RequestProcessor.getDefault().post(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    LogContext logContext = LogContext.this;
                    synchronized (logContext) {
                        if (LogContext.this.finished == 0) {
                            LogContext.this.log(true, true);
                        }
                    }
                }
            }, (int)EXEC_TRESHOLD);
        }
    }

    private void findCulpritIndexer() {
        long secondToMax = 0;
        long max = 0;
        String candidate = null;
        for (Map.Entry<String, Long> check : this.totalIndexerTime.entrySet()) {
            if (check.getValue() <= max) continue;
            secondToMax = max;
            max = check.getValue();
            candidate = check.getKey();
        }
        if (candidate == null) {
            return;
        }
        if (max >= this.totalScanningTime / 3 && (double)max > (double)secondToMax * 1.3) {
            this.culpritIndexer = candidate;
        }
    }

    private void buildThreadTraces(String dumpString) {
        String[] threads = dumpString.split("Thread id ");
        this.threadTraces = new HashMap<String, String[]>();
        this.threadStatuses = new HashMap<String, String>();
        for (String t : threads) {
            int quote;
            int nextQuote;
            String[] lines = t.split("\n");
            if (lines.length == 0 || !lines[0].contains("\"") || (quote = lines[0].indexOf(34)) == -1 || (nextQuote = lines[0].indexOf(34, quote + 1)) == -1) continue;
            String tName = lines[0].substring(quote + 1, nextQuote);
            this.threadTraces.put(tName, lines);
            int paren = lines[0].lastIndexOf(40);
            if (paren == -1) continue;
            int rparen = lines[0].lastIndexOf(41);
            String state = rparen == -1 ? lines[0].substring(paren + 1) : lines[0].substring(paren + 1, rparen);
            this.threadStatuses.put(tName, state);
        }
    }

    private int analyzeStacktraces() {
        int dot;
        int line;
        if (this.threadDump == null) {
            return 0;
        }
        this.buildThreadTraces(this.threadDump);
        HashMap<String, String[]> blockedThreads = new HashMap<String, String[]>();
        for (Map.Entry<String, String> stEntry : this.threadStatuses.entrySet()) {
            String st = stEntry.getValue();
            if (!st.contains("BLOCKED") && !st.contains("PARKED") && !st.contains("WAITING")) continue;
            blockedThreads.put(stEntry.getKey(), this.threadTraces.get(stEntry.getKey()));
        }
        if (!blockedThreads.containsKey("RepositoryUpdater.worker")) {
            return 0;
        }
        if (this.updaterThreadAlive) {
            return 1;
        }
        String[] repositoryStack = (String[])blockedThreads.get("RepositoryUpdater.worker");
        for (line = 1; line < repositoryStack.length && CONCURRENT_PATTERN.matcher(repositoryStack[line]).find(); ++line) {
        }
        if (line >= repositoryStack.length) {
            return 2;
        }
        String qual = repositoryStack[line];
        int paren = qual.indexOf(40);
        if (paren == -1) {
            paren = qual.length();
        }
        if ((dot = (qual = qual.substring(0, paren)).lastIndexOf(46)) == -1) {
            return 2;
        }
        qual = qual.substring(0, dot);
        dot = qual.lastIndexOf(36);
        if (dot > -1) {
            qual = qual.substring(0, dot);
        }
        for (Map.Entry t : blockedThreads.entrySet()) {
            if (t.getValue() == repositoryStack) continue;
            String[] lines = (String[])t.getValue();
            for (int i = 1; i < lines.length; ++i) {
                if (!lines[i].startsWith(qual)) continue;
                this.blockingClass = qual.trim();
                return 2;
            }
        }
        this.blockingClass = qual.trim();
        return 2;
    }

    private ScanCancelledException createException() {
        ScanCancelledException nested;
        if (this.analyzeStacktraces() == 2) {
            String msg = "RepositoryUpdater is blocked by " + (this.blockingClass != null ? this.blockingClass : " <unknown>");
            return new ScanCancelledException(msg, this.blockingClass, this.stackTrace);
        }
        this.findCulpritIndexer();
        if (this.culpritIndexer != null) {
            return new ScanCancelledException("Slow scanning in " + this.culpritIndexer, this.culpritIndexer, this.stackTrace);
        }
        if ((double)this.crawlerTime >= (double)this.totalScanningTime * 0.6) {
            return new ScanCancelledException("Slow crawling", "crawler", this.stackTrace);
        }
        long delay = (this.getExecutedTime() - this.getScheduledTime()) / 1000;
        if (delay > 300 && this.predecessor != null && (nested = this.predecessor.createException()) != null) {
            return new ScanCancelledException("Execution delayed by: " + nested.getMessage(), null, this.stackTrace);
        }
        return null;
    }

    boolean canLogScanCancel() {
        return this.executed <= 0 || System.currentTimeMillis() - this.executed >= EXEC_TRESHOLD;
    }

    void log(boolean cancel, boolean logAbsorbed) {
        this.freeze();
        final LogRecord r = new LogRecord(Level.INFO, cancel ? "SCAN_CANCELLED" : "SCAN_EXCEEDS_RATE {0}");
        if (!logAbsorbed) {
            this.absorbed = null;
        }
        r.setParameters(new Object[]{this, null});
        r.setResourceBundle(NbBundle.getBundle(LogContext.class));
        r.setResourceBundleName(LogContext.class.getPackage().getName() + ".Bundle");
        r.setLoggerName(LOG.getName());
        if (cancel) {
            final StatusDisplayer.Message msg = StatusDisplayer.getDefault().setStatusText("Please wait while the scan cancel report is being produced", 700);
            this.threadDump = this.createThreadDump();
            this.updaterThreadAlive = false;
            RP.post(new Runnable(){

                @Override
                public void run() {
                    LogContext.this.secondDump = LogContext.this.createThreadDump();
                    ScanCancelledException e = LogContext.this.createException();
                    if (e == null) {
                        e = new ScanCancelledException("Scanning cancelled", null, LogContext.this.stackTrace);
                    }
                    byte[] profileData = null;
                    if (LogContext.this.profileDataSource != null) {
                        try {
                            profileData = (byte[])LogContext.this.profileDataSource.call();
                        }
                        catch (Exception ex) {
                            // empty catch block
                        }
                    }
                    r.setParameters(new Object[]{LogContext.this, LogContext.this.totalScanningTime, profileData, e.getLocation()});
                    r.setThrown(e);
                    if ("oneshot".equals(System.getProperty(RepositoryUpdater.PROP_SAMPLING))) {
                        System.getProperties().remove(RepositoryUpdater.PROP_SAMPLING);
                    }
                    msg.clear(0);
                    LOG.log(r);
                }
            }, 5000, 10);
        } else {
            LOG.log(r);
        }
    }

    synchronized void absorb(@NonNull LogContext other) {
        Parameters.notNull((CharSequence)"other", (Object)other);
        if (other.executed == 0) {
            if (other.predecessor != null) {
                this.absorb(other.predecessor);
            }
            return;
        }
        if (this.absorbed == null) {
            this.absorbed = new ArrayDeque<LogContext>();
        }
        this.absorbed.add(other);
    }

    synchronized void recordExecuted() {
        this.executed = System.currentTimeMillis();
        if (!TEST_LOGGER.isLoggable(Level.FINEST)) {
            STATS.record(this);
        }
    }

    void recordFinished() {
        this.finished = System.currentTimeMillis();
        this.freeze();
    }

    void setPredecessor(LogContext pred) {
        this.predecessor = pred;
    }

    long getScheduledTime() {
        return this.timestamp;
    }

    long getExecutedTime() {
        return this.executed;
    }

    long getFinishedTime() {
        return this.finished;
    }

    @NonNull
    StackTraceElement[] getCaller() {
        return Arrays.copyOf(this.stackTrace, this.stackTrace.length);
    }

    public synchronized void noteRootScanning(URL currentRoot, boolean crawling) {
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        RootInfo ri = this.allCurrentRoots.get(Thread.currentThread());
        assert (ri == null);
        ri = new RootInfo(currentRoot, System.currentTimeMillis());
        this.allCurrentRoots.put(Thread.currentThread(), ri);
        if (crawling) {
            ri.crawlerStart = ri.startTime;
        }
        currentlyIndexedRoot.set(ri);
        currentLogContext.set(this);
    }

    public synchronized void startCrawler() {
        this.crawlerStart = System.currentTimeMillis();
    }

    public synchronized void reportCrawlerProgress(int resCount, int allResCount) {
        long t = System.currentTimeMillis();
        this.updaterThreadAlive = true;
        RootInfo ri = this.allCurrentRoots.get(Thread.currentThread());
        if (ri == null) {
            LOG.log(Level.WARNING, "No root specified for crawler run", new Throwable());
            return;
        }
        ri.crawlerTime = t - this.crawlerStart;
        if (resCount != -1) {
            ri.resCount = resCount;
        }
        if (allResCount != -1) {
            ri.allResCount = allResCount;
        }
    }

    public synchronized void addCrawlerTime(long time, int resCount, int allResCount) {
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        this.crawlerTime += time;
        RootInfo ri = this.allCurrentRoots.get(Thread.currentThread());
        if (ri == null) {
            LOG.log(Level.WARNING, "No root specified for crawler run", new Throwable());
            return;
        }
        RootInfo.access$1414(ri, time);
        ri.crawlerStart = -1;
        if (resCount != -1) {
            ri.resCount = resCount;
        }
        if (allResCount != -1) {
            ri.allResCount = allResCount;
        }
        this.checkConsistency();
    }

    public synchronized void addStoreTime(long time) {
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        this.storeTime += time;
    }

    public synchronized void finishScannedRoot(URL scannedRoot) {
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        RootInfo ri = this.allCurrentRoots.get(Thread.currentThread());
        if (ri == null || !scannedRoot.equals(ri.url)) {
            return;
        }
        long time = System.currentTimeMillis();
        long diff = time - ri.startTime;
        this.totalScanningTime += diff;
        RootInfo.access$014(ri, diff);
        if (ri.crawlerStart >= 0) {
            ri.crawlerTime = time - ri.crawlerStart;
            ri.crawlerStart = -1;
        }
        this.crawlerTime += ri.crawlerTime;
        this.allCurrentRoots.remove(Thread.currentThread());
        currentlyIndexedRoot.remove();
        currentLogContext.remove();
        RootInfo ri2 = this.scannedSourceRoots.get(ri.url);
        if (ri2 == null) {
            ri2 = new RootInfo(ri.url, ri.startTime);
            this.scannedSourceRoots.put(ri.url, ri2);
        }
        ri2.merge(ri);
        this.checkConsistency();
    }

    public void setProfileSource(Callable<byte[]> source) {
        this.profileDataSource = source;
    }

    public synchronized void startIndexer(String fName) {
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        RootInfo ri = this.allCurrentRoots.get(Thread.currentThread());
        if (ri == null) {
            LOG.log(Level.WARNING, "Unreported root for running indexer: " + fName, new Throwable());
        } else {
            ri.startIndexer(fName);
        }
    }

    public synchronized void finishIndexer(String fName) {
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        RootInfo ri = this.allCurrentRoots.get(Thread.currentThread());
        if (ri == null) {
            LOG.log(Level.WARNING, "Unreported root for running indexer: " + fName, new Throwable());
        } else {
            long addTime = ri.finishIndexer(fName);
            Long t = this.totalIndexerTime.get(fName);
            if (t == null) {
                t = 0;
            }
            this.totalIndexerTime.put(fName, t + addTime);
        }
    }

    public synchronized void addIndexerTime(String fName, long addTime) {
        RootInfo ri;
        this.updaterThreadAlive = true;
        if (this.frozen) {
            return;
        }
        Long t = this.totalIndexerTime.get(fName);
        if (t == null) {
            t = 0;
        }
        if ((ri = this.allCurrentRoots.get(Thread.currentThread())) == null) {
            LOG.log(Level.WARNING, "Unreported root for running indexer: " + fName, new Throwable());
        } else {
            if (ri.indexerName != null) {
                addTime = ri.finishIndexer(fName);
            }
            this.totalIndexerTime.put(fName, t + addTime);
        }
    }

    public synchronized LogContext withRoot(URL root) {
        this.root = root;
        return this;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public LogContext addPaths(Collection<? extends ClassPath> paths) {
        if (paths == null || paths.isEmpty()) {
            return this;
        }
        HashSet<String> toAdd = new HashSet<String>();
        for (ClassPath cp : paths) {
            toAdd.add(cp.toString());
        }
        LogContext i$ = this;
        synchronized (i$) {
            if (this.classPathsChanged.isEmpty()) {
                this.classPathsChanged = new HashSet<String>(paths.size());
            }
            this.classPathsChanged.addAll(toAdd);
        }
        return this;
    }

    public synchronized LogContext addFilePaths(Collection<String> paths) {
        if (paths == null || paths.isEmpty()) {
            return this;
        }
        if (this.filePathsChanged.isEmpty()) {
            this.filePathsChanged = new HashSet<String>(paths.size());
        }
        this.filePathsChanged.addAll(paths);
        return this;
    }

    public synchronized LogContext addRoots(Iterable<? extends URL> roots) {
        if (roots == null) {
            return this;
        }
        Iterator<? extends URL> it = roots.iterator();
        if (!it.hasNext()) {
            return this;
        }
        if (this.rootsChanged.isEmpty()) {
            this.rootsChanged = new HashSet<URL>(11);
        }
        while (it.hasNext()) {
            this.rootsChanged.add(it.next());
        }
        return this;
    }

    public synchronized LogContext addFileObjects(Collection<FileObject> files) {
        if (files == null || files.isEmpty()) {
            return this;
        }
        if (this.fileObjsChanged.isEmpty()) {
            this.fileObjsChanged = new HashSet<URI>(files.size());
        }
        for (FileObject file : files) {
            this.fileObjsChanged.add(file.toURI());
        }
        return this;
    }

    public synchronized LogContext addFiles(Collection<? extends URL> files) {
        if (files == null || files.isEmpty()) {
            return this;
        }
        if (this.filesChanged.isEmpty()) {
            this.filesChanged = new HashSet<URL>(files.size());
        }
        this.filesChanged.addAll(files);
        return this;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private LogContext(@NonNull EventType eventType, @NonNull StackTraceElement[] stackTrace, @NullAllowed String message, @NullAllowed LogContext parent) {
        Parameters.notNull((CharSequence)"eventType", (Object)((Object)eventType));
        Parameters.notNull((CharSequence)"stackTrace", (Object)stackTrace);
        this.eventType = eventType;
        this.stackTrace = stackTrace;
        this.message = message;
        this.parent = parent;
        this.timestamp = System.currentTimeMillis();
        Class<LogContext> class_ = LogContext.class;
        synchronized (LogContext.class) {
            this.mySerial = serial++;
            // ** MonitorExit[var5_5] (shouldn't be in output)
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.FINER, "Event type: {0} from: {1}", new Object[]{eventType, Arrays.toString(stackTrace)});
            }
            return;
        }
    }

    private synchronized void createLogMessage(@NonNull StringBuilder sb, Set<LogContext> reported, int depth) {
        sb.append("ID: ").append(this.mySerial).append(", Type:").append((Object)this.eventType);
        if (reported.contains(this)) {
            sb.append(" -- see above\n");
            return;
        }
        if (depth > 5) {
            sb.append("-- too deep nesting");
            return;
        }
        reported.add(this);
        if (this.message != null) {
            sb.append(" Description:").append(this.message);
        }
        sb.append("\nTime scheduled: ").append(new Date(this.timestamp));
        if (this.executed > 0) {
            sb.append("\nTime executed: ").append(new Date(this.executed));
            if (this.finished > 0) {
                sb.append("\nTime finished: ").append(new Date(this.finished));
            }
        } else {
            sb.append("\nNOT executed");
        }
        sb.append("\nScanned roots: ").append(this.scannedSourceRoots.values().toString().replaceAll(",", "\n\t")).append("\n, total time: ").append(this.totalScanningTime);
        sb.append("\nCurrent root(s): ").append(this.frozenCurrentRoots.values().toString().replaceAll(",", "\n\t"));
        sb.append("\nCurrent indexer(s): ");
        for (RootInfo ri : this.frozenCurrentRoots.values()) {
            sb.append("\n\t").append(ri.url);
            ArrayList indexerNames = new ArrayList(ri.rootIndexerTime.keySet());
            Collections.sort(indexerNames);
            for (String s : indexerNames) {
                long l = (Long)ri.rootIndexerTime.get(s);
                sb.append("\n\t\t").append(s).append(": ").append(l);
            }
        }
        sb.append("\nTime spent in indexers:");
        ArrayList<String> iNames = new ArrayList<String>(this.totalIndexerTime.keySet());
        Collections.sort(iNames);
        for (String s : iNames) {
            long l = this.totalIndexerTime.get(s);
            sb.append("\n\t").append(s).append(": ").append(l);
        }
        sb.append("\nTime spent in indexers, in individual roots:");
        for (Map.Entry rootEn : this.scannedSourceRoots.entrySet()) {
            sb.append("\n\t").append(rootEn.getKey());
            RootInfo ri2 = (RootInfo)rootEn.getValue();
            ArrayList indexerNames = new ArrayList(ri2.rootIndexerTime.keySet());
            Collections.sort(indexerNames);
            for (String s2 : indexerNames) {
                long l = (Long)ri2.rootIndexerTime.get(s2);
                sb.append("\n\t\t").append(s2).append(": ").append(l);
            }
        }
        sb.append("\nTime in index store: " + this.storeTime);
        sb.append("\nTime crawling: " + this.crawlerTime);
        if (!this.reindexInitiators.isEmpty()) {
            sb.append("\nReindexing demanded by indexers:\n");
            for (URL u : this.reindexInitiators.keySet()) {
                ArrayList indexers = new ArrayList(this.reindexInitiators.get(u));
                Collections.sort(indexers);
                sb.append("\t").append(u).append(": ").append(indexers).append("\n");
            }
        }
        if (!this.indexersAdded.isEmpty()) {
            sb.append("\nIndexers added: " + this.indexersAdded);
        }
        sb.append("\nStacktrace:\n");
        for (StackTraceElement se : this.stackTrace) {
            sb.append('\t').append(se).append('\n');
        }
        if (this.root != null) {
            sb.append("On root: ").append(this.root).append("\n");
        }
        if (!this.rootsChanged.isEmpty()) {
            sb.append("Changed CP roots: ").append(this.rootsChanged).append("\n");
        }
        if (!this.classPathsChanged.isEmpty()) {
            sb.append("Changed ClassPaths:").append(this.classPathsChanged).append("\n");
        }
        if (!this.filesChanged.isEmpty()) {
            sb.append("Changed files(URL): ").append(this.filesChanged.toString().replace(",", "\n\t")).append("\n");
        }
        if (!this.fileObjsChanged.isEmpty()) {
            sb.append("Changed files(FO): ");
            for (URI uri : this.fileObjsChanged) {
                String name;
                try {
                    File f = Utilities.toFile((URI)uri);
                    name = f.getAbsolutePath();
                }
                catch (IllegalArgumentException iae) {
                    name = uri.toString();
                }
                sb.append(name).append("\n\t");
            }
            sb.append("\n");
        }
        if (!this.filePathsChanged.isEmpty()) {
            sb.append("Changed files(Str): ").append(this.filePathsChanged.toString().replace(",", "\n\t")).append("\n");
        }
        if (this.parent != null) {
            sb.append("Parent {");
            this.parent.createLogMessage(sb, reported, depth + 1);
            sb.append("}\n");
        }
        if (this.threadDump != null) {
            sb.append("Thread dump:\n").append(this.threadDump).append("\n");
        }
        if (this.secondDump != null) {
            sb.append("Thread dump #2 (after ").append(5).append(" seconds):\n").append(this.secondDump).append("\n");
        }
        if (this.predecessor != null) {
            sb.append("Predecessor: {");
            this.predecessor.createLogMessage(sb, reported, depth + 1);
            sb.append("}\n");
        }
        if (this.absorbed != null) {
            sb.append("Absorbed {");
            for (LogContext a : this.absorbed) {
                a.createLogMessage(sb, reported, depth + 1);
            }
            sb.append("}\n");
        }
    }

    private static long fromMinutes(int mins) {
        return mins * 60 * 1000;
    }

    synchronized void reindexForced(URL root, String indexerName) {
        Set<String> inits;
        if (this.reindexInitiators.isEmpty()) {
            this.reindexInitiators = new HashMap<URL, Set<String>>();
        }
        if ((inits = this.reindexInitiators.get(root)) == null) {
            inits = new HashSet<String>();
            this.reindexInitiators.put(root, inits);
        }
        inits.add(indexerName);
    }

    synchronized void newIndexerSeen(String s) {
        if (this.indexersAdded.isEmpty()) {
            this.indexersAdded = new ArrayList<String>();
        }
        this.indexersAdded.add(s);
    }

    static {
        CONCURRENT_PATTERN = Pattern.compile("\\s+sun.*|\\s+java.util.concurrent.*|\\s+java.lang.*|\\s+org.openide.*Mutex.*");
        PARSING_PATTERN = Pattern.compile("\\s+org.netbeans.modules.parsing.impl");
        EXEC_TRESHOLD = Integer.getInteger(LogContext.class.getName() + ".cancelTreshold", 180) * 1000;
        currentlyIndexedRoot = new ThreadLocal();
        currentLogContext = new ThreadLocal();
        LOG = Logger.getLogger(LogContext.class.getName());
        STATS = new Stats();
        BACKDOOR_LOG = Logger.getLogger(LogContext.class.getName() + ".backdoor");
        BACKDOOR_LOG.addHandler(new LH());
        BACKDOOR_LOG.setUseParentHandlers(false);
    }

    private static class LH
    extends Handler {
        private LH() {
        }

        @Override
        public void publish(LogRecord record) {
            String msg = record.getMessage();
            if (msg.equals("INDEXER_START")) {
                String indexerName = (String)record.getParameters()[0];
                LogContext lcx = (LogContext)currentLogContext.get();
                if (lcx != null) {
                    lcx.startIndexer(indexerName);
                }
            } else if (msg.equals("INDEXER_END")) {
                String indexerName = (String)record.getParameters()[0];
                LogContext lcx = (LogContext)currentLogContext.get();
                if (lcx != null) {
                    lcx.finishIndexer(indexerName);
                }
            }
            record.setLevel(Level.OFF);
        }

        @Override
        public void flush() {
        }

        @Override
        public void close() throws SecurityException {
        }
    }

    static class Stats {
        private Map<EventType, RingTimeBuffer> history = new HashMap<EventType, RingTimeBuffer>(7);
        private LinkedHashMap<URL, Map<EventType, RingTimeBuffer>> rootHistory = new LinkedHashMap(9, 0.7f, true);

        Stats() {
        }

        public synchronized void record(LogContext ctx) {
            EventType type = ctx.eventType;
            if (ctx.root != null && (type == EventType.INDEXER || type == EventType.MANAGER)) {
                this.recordIndexer(type, ctx.root, ctx);
                return;
            }
            this.recordRegular(type, ctx);
        }

        private void expireRoots() {
            long l = System.currentTimeMillis();
            Iterator<Map<EventType, RingTimeBuffer>> mapIt = this.rootHistory.values().iterator();
            while (mapIt.hasNext()) {
                Map<EventType, RingTimeBuffer> map = mapIt.next();
                Iterator<Map.Entry<EventType, RingTimeBuffer>> it = map.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<EventType, RingTimeBuffer> entry = it.next();
                    EventType et = entry.getKey();
                    RingTimeBuffer rb = entry.getValue();
                    long limit = l - LogContext.fromMinutes(et.getMinutes());
                    if (rb.lastTime >= limit) continue;
                    it.remove();
                }
                if (!map.isEmpty()) break;
                mapIt.remove();
            }
        }

        private void recordIndexer(EventType et, URL root, LogContext ctx) {
            this.expireRoots();
            Map<EventType, RingTimeBuffer> map = this.rootHistory.remove(root);
            if (map == null) {
                map = new EnumMap<EventType, RingTimeBuffer>(EventType.class);
            }
            this.rootHistory.put(root, map);
            RingTimeBuffer existing = map.get((Object)et);
            if (existing == null) {
                existing = new RingTimeBuffer(et.getMinutes() * 2);
                map.put(et, existing);
            }
            existing.mark(ctx);
        }

        private void recordRegular(EventType type, LogContext ctx) {
            RingTimeBuffer buf = this.history.get((Object)type);
            if (buf == null) {
                buf = new RingTimeBuffer(type.getMinutes() * 2);
                this.history.put(type, buf);
            }
            buf.mark(ctx);
        }
    }

    private static class RingTimeBuffer {
        private static final int INITIAL_RINGBUFFER_SIZE = 20;
        private int historyLimit;
        private long[] times = new long[20];
        private LogContext[] contexts = new LogContext[20];
        private int start;
        private int limit;
        private int reportedEnd = -1;
        private long lastTime;

        public RingTimeBuffer(int historyLimit) {
            this.historyLimit = historyLimit;
        }

        private void updateStart(long now) {
            long from = now - LogContext.fromMinutes(this.historyLimit);
            while (!this.isEmpty() && this.times[this.start] < from) {
                this.contexts[this.start] = null;
                if (this.reportedEnd == this.start) {
                    this.reportedEnd = -1;
                }
                this.start = this.inc(this.start);
            }
        }

        private void ensureSpaceAvailable() {
            if (!this.isEmpty() && this.gapSize() == 0) {
                int l;
                long[] times2 = new long[this.times.length * 2];
                LogContext[] contexts2 = new LogContext[this.times.length * 2];
                if (this.limit >= this.start) {
                    System.arraycopy(this.times, this.start, times2, 0, this.limit - this.start);
                    System.arraycopy(this.contexts, this.start, contexts2, 0, this.limit - this.start);
                    l = this.limit - this.start;
                } else {
                    System.arraycopy(this.times, this.start, times2, 0, this.times.length - this.start);
                    System.arraycopy(this.times, 0, times2, this.times.length - this.start, this.limit);
                    System.arraycopy(this.contexts, this.start, contexts2, 0, this.times.length - this.start);
                    System.arraycopy(this.contexts, 0, contexts2, this.times.length - this.start, this.limit);
                    l = this.limit + (this.times.length - this.start);
                }
                this.limit = l;
                this.start = 0;
                this.times = times2;
                this.contexts = contexts2;
            }
        }

        public void mark(LogContext ctx) {
            long l = System.currentTimeMillis();
            this.updateStart(l);
            this.ensureSpaceAvailable();
            this.times[this.limit] = l;
            this.contexts[this.limit] = ctx;
            this.limit = this.inc(this.limit);
            EventType type = ctx.eventType;
            this.checkAndReport(l, type.getMinutes(), type.getTreshold());
            this.lastTime = l;
        }

        private int inc(int i) {
            return (i + 1) % this.ringSize();
        }

        private int ringSize() {
            return this.times.length;
        }

        private boolean isEmpty() {
            return this.start == this.limit;
        }

        private int gapSize() {
            if (this.start > this.limit) {
                return this.start - this.limit;
            }
            return this.start + this.ringSize() - this.limit;
        }

        private int dataSize(int start, int end) {
            if (start < end) {
                return end - start;
            }
            return end + this.ringSize() - start;
        }

        private Pair<Integer, Integer> findHigherRate(long minTime, int minutes, int treshold) {
            int s = this.reportedEnd == -1 ? this.start : this.reportedEnd;
            int l = -1;
            while (s != this.limit && this.times[s] < minTime) {
                if ((s = this.inc(s)) != l) continue;
                l = -1;
            }
            long minDiff = LogContext.fromMinutes(minutes);
            do {
                if (s == this.limit) {
                    return null;
                }
                if (l == -1) {
                    l = s;
                }
                long t = this.times[s];
                while (l != this.limit && this.times[l] - t < minDiff) {
                    l = this.inc(l);
                }
                if (this.dataSize(s, l) > treshold) {
                    return Pair.of((Object)s, (Object)l);
                }
                s = this.inc(s);
            } while (l != this.limit);
            return null;
        }

        void checkAndReport(long now, int minutes, int treshold) {
            long minTime = now - LogContext.fromMinutes(this.historyLimit);
            Pair<Integer, Integer> found = this.findHigherRate(minTime, minutes, treshold);
            if (found == null) {
                return;
            }
            if (closing || RepositoryUpdater.getDefault().getState() == RepositoryUpdater.State.STOPPED) {
                return;
            }
            LOG.log(Level.WARNING, "Excessive indexing rate detected: " + this.dataSize((Integer)found.first(), (Integer)found.second()) + " in " + minutes + "mins, treshold is " + treshold + ". Dumping suspicious contexts");
            int index = (Integer)found.first();
            while (index != (Integer)found.second()) {
                this.contexts[index].log(false, false);
                index = (index + 1) % this.times.length;
            }
            LOG.log(Level.WARNING, "=== End excessive indexing");
            this.reportedEnd = index;
        }
    }

    private class RootInfo {
        private URL url;
        private long startTime;
        private long spent;
        private Map<String, Long> rootIndexerTime;
        private long indexerStartTime;
        private String indexerName;
        private int count;
        private long crawlerTime;
        private int resCount;
        private int allResCount;
        private LinkedList<Object> pastIndexers;
        private long crawlerStart;

        public RootInfo(URL url, long startTime) {
            this.rootIndexerTime = new HashMap<String, Long>();
            this.resCount = -1;
            this.allResCount = -1;
            this.pastIndexers = null;
            this.crawlerStart = -1;
            this.url = url;
            this.startTime = startTime;
        }

        public String toString() {
            long time = this.spent == 0 ? LogContext.this.timeCutOff - this.startTime : this.spent;
            String s = "< root = " + this.url.toString() + "; spent = " + time + "; crawler = " + this.crawlerTime + "; res = " + this.resCount + "; allRes = " + this.allResCount;
            if (this.indexerName != null) {
                s = s + "; indexer: " + this.indexerName;
            }
            return s + ">";
        }

        public void merge(RootInfo ri) {
            if (this == ri) {
                return;
            }
            if (!this.url.equals(ri.url)) {
                throw new IllegalArgumentException();
            }
            this.spent += ri.spent;
            this.crawlerTime += ri.crawlerTime;
            if (ri.resCount > -1) {
                this.resCount = ri.resCount;
            }
            if (ri.allResCount > -1) {
                this.allResCount = ri.allResCount;
            }
            for (String id : ri.rootIndexerTime.keySet()) {
                Long spent = ri.rootIndexerTime.get(id);
                Long my = this.rootIndexerTime.get(id);
                my = my == null ? spent : Long.valueOf(my + spent);
                this.rootIndexerTime.put(id, my);
            }
        }

        void startIndexer(String indexerName) {
            if (this.indexerStartTime != 0) {
                if (this.pastIndexers == null) {
                    this.pastIndexers = new LinkedList();
                }
                this.pastIndexers.add(this.indexerStartTime);
                this.pastIndexers.add(this.indexerName);
            }
            this.indexerStartTime = System.currentTimeMillis();
            this.indexerName = indexerName;
        }

        long finishCurrentIndexer(long now) {
            if (this.indexerStartTime == 0) {
                return 0;
            }
            long time = now - this.indexerStartTime;
            Long t = this.rootIndexerTime.get(this.indexerName);
            if (t == null) {
                t = 0;
            }
            t = t + time;
            this.rootIndexerTime.put(this.indexerName, t);
            if (this.pastIndexers != null && !this.pastIndexers.isEmpty()) {
                this.indexerName = (String)this.pastIndexers.removeLast();
                this.indexerStartTime = (Long)this.pastIndexers.removeLast();
            } else {
                this.indexerStartTime = 0;
            }
            return time;
        }

        long finishIndexer(String indexerName) {
            if (LogContext.this.frozen) {
                return 0;
            }
            if (this.indexerStartTime == 0 || indexerName == null) {
                return 0;
            }
            if (!indexerName.equals(this.indexerName)) {
                boolean ok = false;
                if (this.pastIndexers != null) {
                    for (int i = 1; i < this.pastIndexers.size(); i += 2) {
                        if (!indexerName.equals(this.pastIndexers.get(i))) continue;
                        long t = System.currentTimeMillis();
                        while (this.pastIndexers.size() > i) {
                            this.finishCurrentIndexer(t);
                        }
                        ok = true;
                    }
                }
                if (!ok) {
                    LOG.log(Level.WARNING, "Mismatch in indexer: " + indexerName + ", current: " + indexerName + ", past: " + this.pastIndexers, new Throwable());
                    if (this.pastIndexers != null) {
                        this.pastIndexers.clear();
                    }
                    this.indexerStartTime = 0;
                    this.indexerName = null;
                    return 0;
                }
            }
            long l = this.finishCurrentIndexer(System.currentTimeMillis());
            if (this.indexerStartTime == 0) {
                this.indexerName = null;
            }
            return l;
        }

        static /* synthetic */ long access$1414(RootInfo x0, long x1) {
            return x0.crawlerTime += x1;
        }

        static /* synthetic */ long access$014(RootInfo x0, long x1) {
            return x0.spent += x1;
        }
    }

    public static enum EventType {
        PATH(1, 10),
        FILE(2, 20),
        INDEXER(2, 5),
        MANAGER(1, 10),
        UI(1, 4);
        
        private int treshold;
        private int minutes;

        private EventType(int minutes, int treshold) {
            String prefix = EventType.class.getName() + "." + this.name();
            Integer m = Integer.getInteger(prefix + ".minutes", minutes);
            Integer t = Integer.getInteger(prefix + ".treshold", treshold);
            this.minutes = m;
            this.treshold = t;
        }

        public int getTreshold() {
            return this.treshold;
        }

        public int getMinutes() {
            return this.minutes;
        }
    }

}

