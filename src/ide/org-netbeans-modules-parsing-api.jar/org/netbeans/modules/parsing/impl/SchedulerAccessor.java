/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.impl;

import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.util.Exceptions;

public abstract class SchedulerAccessor {
    private static volatile SchedulerAccessor INSTANCE;

    public static synchronized SchedulerAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName("org.netbeans.modules.parsing.api.Source", true, SourceAccessor.class.getClassLoader());
                assert (INSTANCE != null);
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return INSTANCE;
    }

    public static void set(SchedulerAccessor instance) {
        assert (instance != null);
        INSTANCE = instance;
    }

    public abstract SchedulerEvent createSchedulerEvent(Scheduler var1, SourceModificationEvent var2);
}

