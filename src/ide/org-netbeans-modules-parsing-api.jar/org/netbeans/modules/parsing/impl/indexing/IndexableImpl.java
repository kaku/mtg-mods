/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.net.URL;

public interface IndexableImpl {
    public String getRelativePath();

    public URL getURL();

    public String getMimeType();

    public boolean isTypeOf(String var1);
}

