/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndex2$Transactional
 *  org.netbeans.modules.parsing.lucene.support.DocumentIndexCache
 *  org.netbeans.modules.parsing.lucene.support.IndexManager
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing.lucene;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.parsing.impl.indexing.ClusteredIndexables;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex2;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.IndexManager;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public final class DocumentBasedIndexManager {
    private static DocumentBasedIndexManager instance;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Map<URL, Pair<DocumentIndex2.Transactional, DocumentIndexCache>> indexes = new HashMap<URL, Pair<DocumentIndex2.Transactional, DocumentIndexCache>>();
    private boolean closed;

    private DocumentBasedIndexManager() {
    }

    public static synchronized DocumentBasedIndexManager getDefault() {
        if (instance == null) {
            instance = new DocumentBasedIndexManager();
        }
        return instance;
    }

    @CheckForNull
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    public synchronized DocumentIndex2.Transactional getIndex(URL root, Mode mode) throws IOException {
        assert (root != null);
        assert (PathRegistry.noHostPart(root));
        if (this.closed) {
            return null;
        }
        Pair li = this.indexes.get(root);
        if (li == null) {
            try {
                switch (mode) {
                    case CREATE: {
                        File file = Utilities.toFile((URI)root.toURI());
                        file.mkdir();
                        ClusteredIndexables.AttachableDocumentIndexCache cache = ClusteredIndexables.createDocumentIndexCache();
                        DocumentIndex2.Transactional index = (DocumentIndex2.Transactional)IndexManager.createTransactionalDocumentIndex((File)file, (DocumentIndexCache)cache);
                        li = Pair.of((Object)index, (Object)cache);
                        this.indexes.put(root, (Pair)li);
                        break;
                    }
                    case IF_EXIST: {
                        String[] children;
                        File file = Utilities.toFile((URI)root.toURI());
                        if (!file.isDirectory() || (children = file.list()) == null || children.length <= 0) break;
                        ClusteredIndexables.AttachableDocumentIndexCache cache = ClusteredIndexables.createDocumentIndexCache();
                        DocumentIndex2.Transactional index = (DocumentIndex2.Transactional)IndexManager.createTransactionalDocumentIndex((File)file, (DocumentIndexCache)cache);
                        li = Pair.of((Object)index, (Object)cache);
                        this.indexes.put(root, (Pair)li);
                        break;
                    }
                }
            }
            catch (URISyntaxException e) {
                throw new IOException(e);
            }
        }
        return li == null ? null : (DocumentIndex2.Transactional)li.first();
    }

    @CheckForNull
    public synchronized DocumentIndexCache getCache(@NonNull URL root) {
        Pair<DocumentIndex2.Transactional, DocumentIndexCache> entry = this.indexes.get(root);
        return entry == null ? null : (DocumentIndexCache)entry.second();
    }

    @CheckForNull
    public synchronized DocumentIndex2.Transactional getIndex(@NonNull DocumentIndexCache cache) {
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        for (Pair<DocumentIndex2.Transactional, DocumentIndexCache> e : this.indexes.values()) {
            if (!cache.equals(e.second())) continue;
            return (DocumentIndex2.Transactional)e.first();
        }
        return null;
    }

    public synchronized void close() {
        if (this.closed) {
            return;
        }
        this.closed = true;
        for (Pair<DocumentIndex2.Transactional, DocumentIndexCache> index : this.indexes.values()) {
            try {
                ((DocumentIndex2.Transactional)index.first()).close();
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
    }

    public static enum Mode {
        OPENED,
        CREATE,
        IF_EXIST;
        

        private Mode() {
        }
    }

}

