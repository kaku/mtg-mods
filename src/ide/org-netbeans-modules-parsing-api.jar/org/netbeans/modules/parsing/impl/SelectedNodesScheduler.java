/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.parsing.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.indexing.Util;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.windows.TopComponent;

public class SelectedNodesScheduler
extends Scheduler {
    private RequestProcessor requestProcessor;

    public SelectedNodesScheduler() {
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)new AListener());
        this.refresh();
    }

    private void refresh() {
        if (this.requestProcessor == null) {
            this.requestProcessor = new RequestProcessor("SelectedNodesScheduler");
        }
        this.requestProcessor.post(new Runnable(){

            @Override
            public void run() {
                FileObject fileObject;
                DataObject dataObject;
                Source source;
                Node[] nodes = TopComponent.getRegistry().getActivatedNodes();
                if (nodes.length == 1 && (dataObject = (DataObject)nodes[0].getLookup().lookup(DataObject.class)) != null && dataObject.isValid() && (fileObject = dataObject.getPrimaryFile()).isValid() && Util.canBeParsed(fileObject.getMIMEType()) && (source = Source.create(fileObject)) != null) {
                    SelectedNodesScheduler.this.schedule(source, new SchedulerEvent(SelectedNodesScheduler.this){});
                    return;
                }
                SelectedNodesScheduler.this.schedule(null, null);
            }

        });
    }

    public String toString() {
        return "SelectedNodesScheduller";
    }

    @Override
    protected SchedulerEvent createSchedulerEvent(SourceModificationEvent event) {
        if (event.getModifiedSource() == this.getSource()) {
            return new SchedulerEvent(this){};
        }
        return null;
    }

    private class AListener
    implements PropertyChangeListener {
        private AListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName() == null || evt.getPropertyName().equals("activatedNodes")) {
                SelectedNodesScheduler.this.refresh();
            }
        }
    }

}

