/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;

public final class DefaultPathRecognizer
extends PathRecognizer {
    private static final Logger LOG = Logger.getLogger(DefaultPathRecognizer.class.getName());
    private final Set<String> sourcePathIds;
    private final Set<String> libraryPathIds;
    private final Set<String> binaryLibraryPathIds;
    private final Set<String> mimeTypes;

    @Override
    public Set<String> getSourcePathIds() {
        return this.sourcePathIds;
    }

    @Override
    public Set<String> getBinaryLibraryPathIds() {
        return this.binaryLibraryPathIds;
    }

    @Override
    public Set<String> getLibraryPathIds() {
        return this.libraryPathIds;
    }

    @Override
    public Set<String> getMimeTypes() {
        return this.mimeTypes;
    }

    public static PathRecognizer createInstance(Map fileAttributes) {
        Set<String> sourcePathIds = DefaultPathRecognizer.readIdsAttribute(fileAttributes, "sourcePathIds");
        Set<String> libraryPathIds = DefaultPathRecognizer.readIdsAttribute(fileAttributes, "libraryPathIds");
        Set<String> binaryLibraryPathIds = DefaultPathRecognizer.readIdsAttribute(fileAttributes, "binaryLibraryPathIds");
        HashSet<String> mimeTypes = new HashSet<String>();
        Object mts = fileAttributes.get("mimeTypes");
        if (mts instanceof String) {
            String[] arr;
            for (String mt : arr = ((String)mts).split(",")) {
                if ((mt = mt.trim()).length() > 0 && MimePath.validate((CharSequence)mt)) {
                    mimeTypes.add(mt);
                    continue;
                }
                LOG.log(Level.WARNING, "Invalid mimetype {0}, ignoring.", mt);
            }
        }
        return new DefaultPathRecognizer(sourcePathIds, libraryPathIds, binaryLibraryPathIds, Collections.unmodifiableSet(mimeTypes));
    }

    public String toString() {
        return Object.super.toString() + "[sourcePathIds=" + this.sourcePathIds + ", libraryPathIds=" + this.libraryPathIds + ", binaryLibraryPathIds=" + this.binaryLibraryPathIds + ", mimeTypes=" + this.mimeTypes;
    }

    private DefaultPathRecognizer(Set<String> sourcePathIds, Set<String> libraryPathIds, Set<String> binaryLibraryPathIds, Set<String> mimeTypes) {
        this.sourcePathIds = sourcePathIds;
        this.libraryPathIds = libraryPathIds;
        this.binaryLibraryPathIds = binaryLibraryPathIds;
        this.mimeTypes = mimeTypes;
    }

    private static Set<String> readIdsAttribute(Map fileAttributes, String attributeName) {
        Set ids;
        ids = new HashSet();
        Object attributeValue = fileAttributes.get(attributeName);
        if (attributeValue instanceof String) {
            String[] varr;
            for (String v : varr = ((String)attributeValue).split(",")) {
                if ((v = v.trim()).equals("ANY")) {
                    ids = null;
                    break;
                }
                if (v.length() > 0) {
                    ids.add(v);
                    continue;
                }
                LOG.log(Level.WARNING, "Empty IDs are not alowed in {0} attribute, ignoring.", attributeName);
            }
        } else {
            if (attributeValue != null) {
                Object[] arrobject = new Object[3];
                arrobject[0] = attributeName;
                arrobject[1] = attributeValue;
                arrobject[2] = attributeValue == null ? null : attributeValue.getClass();
                LOG.log(Level.WARNING, "Invalid {0} attribute value, expecting java.lang.String, but got {1}, {2}", arrobject);
            }
            ids = Collections.emptySet();
        }
        return ids == null ? null : Collections.unmodifiableSet(ids);
    }
}

