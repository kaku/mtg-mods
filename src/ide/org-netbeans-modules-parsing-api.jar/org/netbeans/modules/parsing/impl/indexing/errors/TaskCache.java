/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.spi.tasklist.Task
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.impl.indexing.errors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;
import org.netbeans.modules.parsing.impl.indexing.errors.ErrorAnnotator;
import org.netbeans.modules.parsing.impl.indexing.errors.Settings;
import org.netbeans.modules.parsing.impl.indexing.errors.TaskProvider;
import org.netbeans.modules.parsing.impl.indexing.errors.Utilities;
import org.netbeans.modules.parsing.spi.indexing.ErrorsCache;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.spi.tasklist.Task;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;

public class TaskCache {
    private static final String ERR_EXT = "err";
    private static final String WARN_EXT = "warn";
    private static final String RELOCATION_FILE = "relocation.properties";
    private static final int VERSION = 1;
    private static final Logger LOG = Logger.getLogger(TaskCache.class.getName());
    private static TaskCache theInstance;
    private ThreadLocal<TransactionContext> q = new ThreadLocal();

    private TaskCache() {
    }

    public static TaskCache getDefault() {
        if (null == theInstance) {
            theInstance = new TaskCache();
        }
        return theInstance;
    }

    private String getTaskType(ErrorsCache.ErrorKind k) {
        switch (k) {
            case ERROR: 
            case ERROR_NO_BADGE: {
                return "nb-tasklist-error";
            }
            case WARNING: {
                return "nb-tasklist-warning";
            }
        }
        return null;
    }

    public List<Task> getErrors(FileObject file) {
        LinkedList<Task> result = new LinkedList<Task>();
        result.addAll(this.getErrors(file, "err"));
        result.addAll(this.getErrors(file, "warn"));
        return result;
    }

    private List<Task> getErrors(FileObject file, String ext) {
        LOG.log(Level.FINE, "getErrors, file={0}, ext={1}", new Object[]{FileUtil.getFileDisplayName((FileObject)file), ext});
        try {
            File input = this.computePersistentFile(file, ext);
            LOG.log(Level.FINE, "getErrors, error file={0}", input == null ? "null" : input.getAbsolutePath());
            if (input == null || !input.canRead()) {
                return Collections.emptyList();
            }
            input.getParentFile().mkdirs();
            return this.loadErrors(input, file);
        }
        catch (IOException e) {
            LOG.log(Level.FINE, null, e);
            return Collections.emptyList();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <T> boolean dumpErrors(File output, Iterable<? extends T> errors, ErrorsCache.Convertor<T> convertor, boolean interestedInReturnValue) throws IOException {
        if (errors.iterator().hasNext()) {
            boolean existed;
            existed = interestedInReturnValue && output.exists();
            output.getParentFile().mkdirs();
            PrintWriter pw = new PrintWriter(new OutputStreamWriter((OutputStream)new FileOutputStream(output), "UTF-8"));
            try {
                for (T err : errors) {
                    pw.print(convertor.getKind(err).name());
                    pw.print(':');
                    pw.print(convertor.getLineNumber(err));
                    pw.print(':');
                    String description = convertor.getMessage(err);
                    if (description == null || description.length() <= 0) continue;
                    description = description.replaceAll("\\\\", "\\\\\\\\");
                    description = description.replaceAll("\n", "\\\\n");
                    description = description.replaceAll(":", "\\\\d");
                    pw.println(description);
                }
            }
            finally {
                pw.close();
            }
            return !existed;
        }
        return output.delete();
    }

    private <T> void separate(Iterable<? extends T> input, ErrorsCache.Convertor<T> convertor, List<T> errors, List<T> notErrors) {
        for (T err : input) {
            if (convertor.getKind(err) == ErrorsCache.ErrorKind.ERROR) {
                errors.add(err);
                continue;
            }
            notErrors.add(err);
        }
    }

    public <T> void dumpErrors(final URL root, final Indexable i, final Iterable<? extends T> errors, final ErrorsCache.Convertor<T> convertor) {
        try {
            this.refreshTransaction(new Mutex.ExceptionAction<Void>(){

                public Void run() throws Exception {
                    TaskCache.this.dumpErrors((TransactionContext)TaskCache.this.q.get(), root, i, errors, convertor);
                    return null;
                }
            });
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private <T> void dumpErrors(TransactionContext c, URL root, Indexable i, Iterable<? extends T> errors, ErrorsCache.Convertor<T> convertor) throws IOException {
        assert (PathRegistry.noHostPart(root));
        File[] output = this.computePersistentFile(root, i);
        LinkedList trueErrors = new LinkedList();
        LinkedList notErrors = new LinkedList();
        this.separate(errors, convertor, trueErrors, notErrors);
        boolean modified = this.dumpErrors(output[0], trueErrors, convertor, true);
        this.dumpErrors(output[1], notErrors, convertor, false);
        URL currentFile = i.getURL();
        c.toRefresh.add(currentFile);
        if (modified) {
            Project p;
            currentFile = new URL(currentFile, ".");
            c.toRefresh.add(currentFile);
            String relativePath = i.getRelativePath();
            for (int depth = relativePath.split((String)"/").length - 1; depth > 0; --depth) {
                currentFile = new URL(currentFile, "..");
                c.toRefresh.add(currentFile);
            }
            FileObject rootFO = URLMapper.findFileObject((URL)root);
            if (rootFO != null && (p = FileOwnerQuery.getOwner((FileObject)rootFO)) != null) {
                FileObject projectDirectory = p.getProjectDirectory();
                if (FileUtil.isParentOf((FileObject)projectDirectory, (FileObject)rootFO)) {
                    for (FileObject currentFO = rootFO; currentFO != null && currentFO != projectDirectory; currentFO = currentFO.getParent()) {
                        c.toRefresh.add(currentFO.toURL());
                    }
                }
                c.toRefresh.add(projectDirectory.toURL());
            }
        }
        c.rootsToRefresh.add(root);
    }

    private List<Task> loadErrors(File input, FileObject file) throws IOException {
        String line;
        LinkedList<Task> result = new LinkedList<Task>();
        BufferedReader pw = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(input), "UTF-8"));
        while ((line = pw.readLine()) != null) {
            String[] parts = line.split(":");
            if (parts.length != 3) continue;
            ErrorsCache.ErrorKind kind = null;
            try {
                kind = ErrorsCache.ErrorKind.valueOf(parts[0]);
            }
            catch (IllegalArgumentException iae) {
                LOG.log(Level.FINE, "Invalid ErrorKind: {0}", line);
            }
            if (kind == null) continue;
            int lineNumber = Integer.parseInt(parts[1]);
            String message = parts[2];
            message = message.replaceAll("\\\\d", ":");
            message = message.replaceAll("\\\\n", " ");
            message = message.replaceAll("\\\\\\\\", "\\\\");
            String severity = this.getTaskType(kind);
            if (null == severity) continue;
            Task err = Task.create((FileObject)file, (String)severity, (String)message, (int)lineNumber);
            result.add(err);
        }
        pw.close();
        return result;
    }

    public List<URL> getAllFilesWithRecord(URL root) throws IOException {
        return this.getAllFilesWithRecord(root, false);
    }

    private List<URL> getAllFilesWithRecord(URL root, boolean onlyErrors) throws IOException {
        try {
            LinkedList<URL> result = new LinkedList<URL>();
            if (FileUtil.getArchiveFile((URL)root) != null) {
                return result;
            }
            URI rootURI = root.toURI();
            File cacheRoot = TaskCache.getCacheRoot(root);
            URI cacheRootURI = org.openide.util.Utilities.toURI((File)cacheRoot);
            LinkedList<File> todo = new LinkedList<File>();
            todo.add(cacheRoot);
            while (!todo.isEmpty()) {
                File f = (File)todo.poll();
                assert (f != null);
                if (f.isFile()) {
                    String relative;
                    if (f.getName().endsWith("err")) {
                        relative = cacheRootURI.relativize(org.openide.util.Utilities.toURI((File)f)).getRawPath();
                        relative = relative.replaceAll(".err$", "");
                        result.add(rootURI.resolve(relative).toURL());
                    }
                    if (onlyErrors || !f.getName().endsWith("warn")) continue;
                    relative = cacheRootURI.relativize(org.openide.util.Utilities.toURI((File)f)).getRawPath();
                    relative = relative.replaceAll(".warn$", "");
                    result.add(rootURI.resolve(relative).toURL());
                    continue;
                }
                File[] files = f.listFiles();
                if (files == null) continue;
                for (File children : files) {
                    todo.offer(children);
                }
            }
            return result;
        }
        catch (URISyntaxException e) {
            throw new IOException(e);
        }
    }

    public List<URL> getAllFilesInError(URL root) throws IOException {
        return this.getAllFilesWithRecord(root, true);
    }

    public boolean isInError(FileObject file, boolean recursive) {
        LOG.log(Level.FINE, "file={0}, recursive={1}", new Object[]{file, recursive});
        if (file.isData()) {
            return !this.getErrors(file, "err").isEmpty();
        }
        try {
            ClassPath cp = Utilities.getSourceClassPathFor(file);
            if (cp == null) {
                return false;
            }
            FileObject root = cp.findOwnerRoot(file);
            if (root == null) {
                LOG.log(Level.FINE, "file={0} does not have a root on its own source classpath", (Object)file);
                return false;
            }
            String resourceName = cp.getResourceName(file, File.separatorChar, true);
            File cacheRoot = TaskCache.getCacheRoot(root.toURL(), true);
            if (cacheRoot == null) {
                return false;
            }
            File folder = new File(cacheRoot, resourceName);
            return this.folderContainsErrors(folder, recursive);
        }
        catch (IOException e) {
            LOG.log(Level.WARNING, null, e);
            return false;
        }
    }

    private boolean folderContainsErrors(File folder, boolean recursively) throws IOException {
        File[] errors = folder.listFiles(new FilenameFilter(){

            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".err") || name.endsWith(".err.rel");
            }
        });
        if (errors == null) {
            return false;
        }
        if (errors.length > 0) {
            return true;
        }
        if (!recursively) {
            return false;
        }
        File[] children = folder.listFiles();
        if (children == null) {
            return false;
        }
        for (File c : children) {
            if (!c.isDirectory() || !this.folderContainsErrors(c, recursively)) continue;
            return true;
        }
        return false;
    }

    private File[] computePersistentFile(URL root, Indexable i) throws IOException {
        String resourceName = i.getRelativePath();
        File cacheRoot = TaskCache.getCacheRoot(root);
        File errorCacheFile = TaskCache.computePersistentFile(cacheRoot, resourceName, "err");
        File warningCacheFile = TaskCache.computePersistentFile(cacheRoot, resourceName, "warn");
        return new File[]{errorCacheFile, warningCacheFile};
    }

    private File computePersistentFile(FileObject file, String extension) throws IOException {
        ClassPath cp = Utilities.getSourceClassPathFor(file);
        if (cp == null) {
            return null;
        }
        FileObject root = cp.findOwnerRoot(file);
        if (root == null) {
            LOG.log(Level.FINE, "file={0} does not have a root on its own source classpath", (Object)file);
            return null;
        }
        String resourceName = cp.getResourceName(file, File.separatorChar, true);
        File cacheRoot = TaskCache.getCacheRoot(root.toURL());
        File cacheFile = TaskCache.computePersistentFile(cacheRoot, resourceName, extension);
        return cacheFile;
    }

    @NonNull
    private static File computePersistentFile(@NonNull File cacheRoot, @NonNull String resourceName, @NonNull String extension) {
        File candidate = new File(cacheRoot, resourceName + "." + extension);
        String nameComponent = candidate.getName();
        if (TaskCache.requiresRelocation(nameComponent)) {
            Properties relocation = TaskCache.loadRelocation(cacheRoot);
            String relName = relocation.getProperty(resourceName);
            if (relName == null) {
                relName = TaskCache.computeFreeName(relocation);
                relocation.setProperty(resourceName, relName);
                TaskCache.storeRelocation(cacheRoot, relocation);
            }
            candidate = new File(candidate.getParentFile(), String.format("%s.%s.rel", relName, extension));
        }
        return candidate;
    }

    private static boolean requiresRelocation(@NonNull String nameComponent) {
        return nameComponent.length() > 255;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    private static Properties loadRelocation(@NonNull File cacheRoot) {
        Properties result = new Properties();
        File relocationFile = new File(cacheRoot, "relocation.properties");
        if (relocationFile.canRead()) {
            try {
                FileInputStream in = new FileInputStream(relocationFile);
                try {
                    result.load(in);
                }
                finally {
                    in.close();
                }
            }
            catch (IOException ioe) {
                Exceptions.printStackTrace((Throwable)ioe);
            }
        }
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void storeRelocation(@NonNull File cacheRoot, @NonNull Properties relocation) {
        File relocationFile = new File(cacheRoot, "relocation.properties");
        try {
            FileOutputStream out = new FileOutputStream(relocationFile);
            try {
                relocation.store(out, null);
            }
            finally {
                out.close();
            }
        }
        catch (IOException ioe) {
            Exceptions.printStackTrace((Throwable)ioe);
        }
    }

    @NonNull
    private static String computeFreeName(@NonNull Properties props) {
        int lastUsed = 0;
        for (Object value : props.values()) {
            int current = Integer.parseInt((String)value);
            if (lastUsed >= current) continue;
            lastUsed = current;
        }
        return Integer.toString(lastUsed + 1);
    }

    public <T> T refreshTransaction(Mutex.ExceptionAction<T> a) throws IOException {
        TransactionContext c = this.q.get();
        if (c == null) {
            c = new TransactionContext();
            this.q.set(c);
        }
        c.depth++;
        try {
            Object object = a.run();
            return (T)object;
        }
        catch (IOException ioe) {
            throw ioe;
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
        finally {
            if (--c.depth == 0) {
                TaskCache.doRefresh(c);
                this.q.set(null);
            }
        }
    }

    private static void doRefresh(TransactionContext c) {
        ErrorAnnotator an;
        if (Settings.isBadgesEnabled() && !c.toRefresh.isEmpty() && (an = ErrorAnnotator.getAnnotator()) != null) {
            an.updateInError(c.toRefresh);
        }
        for (URL root : c.rootsToRefresh) {
            FileObject rootFO = URLMapper.findFileObject((URL)root);
            if (rootFO == null) continue;
            TaskProvider.refresh(rootFO);
        }
    }

    private static File getCacheRoot(URL root) throws IOException {
        return TaskCache.getCacheRoot(root, false);
    }

    private static File getCacheRoot(URL root, boolean onlyIfExists) throws IOException {
        FileObject dataFolder = CacheFolder.getDataFolder(root, onlyIfExists);
        if (dataFolder == null) {
            return null;
        }
        File cache = FileUtil.toFile((FileObject)FileUtil.createFolder((FileObject)dataFolder, (String)"errors/1"));
        return cache;
    }

    private static final class TransactionContext {
        private int depth;
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private Set<URL> toRefresh = new HashSet<URL>();
        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
        private Set<URL> rootsToRefresh = new HashSet<URL>();

        private TransactionContext() {
        }
    }

}

