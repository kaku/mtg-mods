/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.java.classpath.ClassPath
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.util.EventObject;
import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.parsing.impl.indexing.EventKind;
import org.netbeans.modules.parsing.impl.indexing.LogContext;
import org.netbeans.modules.parsing.impl.indexing.PathKind;
import org.netbeans.modules.parsing.impl.indexing.PathRegistry;

public final class PathRegistryEvent
extends EventObject {
    private final Iterable<? extends Change> changes;
    private final LogContext logCtx;

    public PathRegistryEvent(@NonNull PathRegistry regs, @NonNull Iterable<? extends Change> changes, @NullAllowed LogContext logCtx) {
        super(regs);
        assert (changes != null);
        this.changes = changes;
        this.logCtx = logCtx;
    }

    @NonNull
    public Iterable<? extends Change> getChanges() {
        return this.changes;
    }

    @CheckForNull
    public LogContext getLogContext() {
        return this.logCtx;
    }

    public static final class Change {
        private final EventKind eventKind;
        private final PathKind pathKind;
        private final Set<? extends ClassPath> pahs;
        private final String pathId;

        public Change(EventKind eventKind, PathKind pathKind, String pathId, Set<? extends ClassPath> paths) {
            assert (eventKind != null);
            this.pahs = paths;
            this.eventKind = eventKind;
            this.pathKind = pathKind;
            this.pathId = pathId;
        }

        public Set<? extends ClassPath> getAffectedPaths() {
            return this.pahs;
        }

        public EventKind getEventKind() {
            return this.eventKind;
        }

        public PathKind getPathKind() {
            return this.pathKind;
        }

        public String getPathId() {
            return this.pathId;
        }
    }

}

