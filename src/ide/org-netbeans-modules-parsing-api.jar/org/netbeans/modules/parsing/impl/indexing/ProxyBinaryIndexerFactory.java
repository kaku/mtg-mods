/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.impl.indexing;

import java.net.URL;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.parsing.impl.indexing.ArchiveTimeStamps;
import org.netbeans.modules.parsing.impl.indexing.SPIAccessor;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.BinaryIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.ConstrainedBinaryIndexer;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public final class ProxyBinaryIndexerFactory
extends BinaryIndexerFactory {
    private static final String ATTR_DELEGATE = "delegate";
    private static final String ATTR_INDEXER_NAME = "name";
    private static final String ATTR_INDEXER_VERSION = "version";
    private static final String ATTR_REQUIRED_RES = "requiredResource";
    private static final String ATTR_REQUIRED_MIME = "mimeType";
    private static final String ATTR_NAME_PATTERN = "namePattern";
    private static final String PROP_CHECKED = "ProxyBinaryIndexerFactory_checked";
    private static final String PROP_MATCHED_FILES = "ProxyBinaryIndexerFactory_matchedFiles";
    private static final String MIME_UNKNOWN = "content/unknown";
    private static final int USED = 1;
    private final Map<String, Object> params;
    private final String indexerName;
    private final int indexerVersion;
    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part")
    private final Set<URL> activeRoots;

    public ProxyBinaryIndexerFactory(@NonNull Map<String, Object> params) {
        Parameters.notNull((CharSequence)"params", params);
        this.params = params;
        this.indexerName = (String)params.get("name");
        Parameters.notNull((CharSequence)"indexerName", (Object)this.indexerName);
        Integer iv = (Integer)params.get("version");
        Parameters.notNull((CharSequence)"version", (Object)iv);
        this.indexerVersion = iv;
        this.activeRoots = new HashSet<URL>();
    }

    @Override
    public String getIndexerName() {
        return this.indexerName;
    }

    @Override
    public int getIndexVersion() {
        return this.indexerVersion;
    }

    @Override
    public BinaryIndexer createIndexer() {
        return new Indexer();
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part. Already verified by PathRegistry and RepositoryUpdater.")
    @Override
    public void rootsRemoved(Iterable<? extends URL> removedRoots) {
        HashSet<URL> filtered = new HashSet<URL>();
        for (URL removedRoot : removedRoots) {
            if (!this.activeRoots.remove(removedRoot)) continue;
            filtered.add(removedRoot);
        }
        if (!filtered.isEmpty()) {
            SPIAccessor.getInstance().rootsRemoved(this.getDelegate(), Collections.unmodifiableSet(filtered));
        }
    }

    @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part. Already verified by PathRegistry and RepositoryUpdater.")
    @Override
    public boolean scanStarted(Context context) {
        if (this.supports(context) != null) {
            return SPIAccessor.getInstance().scanStarted(this.getDelegate(), context);
        }
        boolean vote = true;
        if (ProxyBinaryIndexerFactory.isUpToDate(context) && ArchiveTimeStamps.getIndexerState(context) == 1) {
            vote = SPIAccessor.getInstance().scanStarted(this.getDelegate(), context);
            if (!vote) {
                SPIAccessor.getInstance().putProperty(context, "ProxyBinaryIndexerFactory_checked", Boolean.FALSE);
            } else {
                this.activeRoots.add(context.getRootURI());
            }
        }
        return vote;
    }

    @Override
    public void scanFinished(Context context) {
        if (this.supports(context) != null) {
            SPIAccessor.getInstance().scanFinished(this.getDelegate(), context);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    private Map<String, ? extends Iterable<? extends FileObject>> supports(@NonNull Context ctx) {
        FileObject root = ctx.getRoot();
        if (root == null) {
            return null;
        }
        if (Boolean.TRUE == SPIAccessor.getInstance().getProperty(ctx, "ProxyBinaryIndexerFactory_checked")) {
            return (Map)SPIAccessor.getInstance().getProperty(ctx, "ProxyBinaryIndexerFactory_matchedFiles");
        }
        HashMap<String, ArrayDeque<FileObject>> matchedFiles = new HashMap<String, ArrayDeque<FileObject>>();
        String s = (String)this.params.get("requiredResource");
        String[] requiredResources = s == null ? null : s.split(",");
        s = (String)this.params.get("mimeType");
        String[] mimeTypes = s == null ? null : s.split(",");
        s = (String)this.params.get("namePattern");
        Pattern pattern = s == null ? null : Pattern.compile(s);
        try {
            Object mimeTypesSet;
            if (ProxyBinaryIndexerFactory.isUpToDate(ctx)) {
                Map<String, ? extends Iterable<? extends FileObject>> map = null;
                return map;
            }
            if (requiredResources != null) {
                boolean found = false;
                for (String r : requiredResources) {
                    if (root.getFileObject(r) == null) continue;
                    found = true;
                    break;
                }
                if (!found) {
                    String[] arr$ = null;
                    return arr$;
                }
            }
            if (mimeTypes != null || pattern != null) {
                FileObject f;
                Enumeration fos = root.getChildren(true);
                Object object = mimeTypesSet = mimeTypes == null ? null : new HashSet<String>(Arrays.asList(mimeTypes));
                while (fos.hasMoreElements()) {
                    String name;
                    f = (FileObject)fos.nextElement();
                    if (pattern != null && !pattern.matcher(name = f.getNameExt()).matches()) continue;
                    String mt = "content/unknown";
                    if (mimeTypes != null && !mimeTypesSet.contains(mt = f.getMIMEType(mimeTypes))) continue;
                    ArrayDeque<FileObject> q = (ArrayDeque<FileObject>)matchedFiles.get(mt);
                    if (q == null) {
                        q = new ArrayDeque<FileObject>();
                        matchedFiles.put(mt, q);
                    }
                    q.offer(f);
                }
                if (matchedFiles.isEmpty()) {
                    f = null;
                    return f;
                }
            }
            Map res = Collections.unmodifiableMap(matchedFiles);
            SPIAccessor.getInstance().putProperty(ctx, "ProxyBinaryIndexerFactory_matchedFiles", res);
            mimeTypesSet = res;
            return mimeTypesSet;
        }
        finally {
            SPIAccessor.getInstance().putProperty(ctx, "ProxyBinaryIndexerFactory_checked", Boolean.TRUE);
        }
    }

    @NonNull
    private ConstrainedBinaryIndexer getDelegate() {
        Object delegate = this.params.get("delegate");
        if (!(delegate instanceof ConstrainedBinaryIndexer)) {
            throw new IllegalArgumentException(String.format("Invalid indexer %s registered as %s %d", delegate, this.indexerName, this.indexerVersion));
        }
        return (ConstrainedBinaryIndexer)delegate;
    }

    private static boolean isUpToDate(@NonNull Context ctx) {
        return !ctx.isAllFilesIndexing();
    }

    private final class Indexer
    extends BinaryIndexer {
        private Indexer() {
        }

        @SuppressWarnings(value={"DMI_COLLECTION_OF_URLS"}, justification="URLs have never host part. Already verified by PathRegistry and RepositoryUpdater.")
        @Override
        protected void index(@NonNull Context context) {
            Map matchedFiles = ProxyBinaryIndexerFactory.this.supports(context);
            if (matchedFiles != null) {
                ProxyBinaryIndexerFactory.this.activeRoots.add(context.getRootURI());
                SPIAccessor.getInstance().index(ProxyBinaryIndexerFactory.this.getDelegate(), matchedFiles, context);
                ArchiveTimeStamps.setIndexerState(context, 1);
            }
        }
    }

}

