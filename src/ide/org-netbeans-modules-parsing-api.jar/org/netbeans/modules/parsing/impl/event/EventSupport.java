/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenHierarchyEvent
 *  org.netbeans.api.lexer.TokenHierarchyListener
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.impl.event;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.EnumSet;
import java.util.EventListener;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.impl.SourceAccessor;
import org.netbeans.modules.parsing.impl.SourceCache;
import org.netbeans.modules.parsing.impl.SourceFlags;
import org.netbeans.modules.parsing.impl.TaskProcessor;
import org.netbeans.modules.parsing.impl.indexing.IndexingManagerAccessor;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class EventSupport {
    private static final Logger LOGGER = Logger.getLogger(EventSupport.class.getName());
    private static final RequestProcessor RP = new RequestProcessor("parsing-event-collector", 1, false, false);
    private static final int DEFAULT_REPARSE_DELAY = 500;
    private static final int IMMEDIATE_REPARSE_DELAY = 10;
    private static int reparseDelay = 500;
    private static int immediateReparseDelay = 10;
    private final Source source;
    private volatile boolean initialized;
    private DocListener docListener;
    private FileChangeListener fileChangeListener;
    private DataObjectListener dobjListener;
    private ChangeListener parserListener;
    private final RequestProcessor.Task resetTask;
    private static final EditorRegistryListener editorRegistryListener = new EditorRegistryListener();

    public EventSupport(Source source) {
        this.resetTask = RP.create(new Runnable(){

            @Override
            public void run() {
                EventSupport.this.resetStateImpl();
            }
        });
        assert (source != null);
        this.source = source;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void init() {
        if (this.initialized) {
            return;
        }
        Parser parser = SourceAccessor.getINSTANCE().getCache(this.source).getParser();
        Object object = TaskProcessor.INTERNAL_LOCK;
        synchronized (object) {
            if (!this.initialized) {
                FileObject fo = this.source.getFileObject();
                if (fo != null) {
                    try {
                        this.fileChangeListener = new FileChangeListenerImpl();
                        fo.addFileChangeListener(FileUtil.weakFileChangeListener((FileChangeListener)this.fileChangeListener, (Object)fo));
                        DataObject dObj = DataObject.find((FileObject)fo);
                        this.assignDocumentListener(dObj);
                        this.dobjListener = new DataObjectListener(dObj);
                        this.parserListener = new ParserListener();
                        if (parser != null) {
                            parser.addChangeListener(this.parserListener);
                        }
                    }
                    catch (DataObjectNotFoundException e) {
                        LOGGER.log(Level.WARNING, "Ignoring events non existent file: {0}", FileUtil.getFileDisplayName((FileObject)fo));
                    }
                } else {
                    Document doc = this.source.getDocument(false);
                    if (doc != null) {
                        this.docListener = new DocListener(this, doc);
                        this.parserListener = new ParserListener();
                        if (parser != null) {
                            parser.addChangeListener(this.parserListener);
                        }
                    }
                }
                this.initialized = true;
            }
        }
    }

    public void resetState(boolean invalidate, boolean mimeChanged, int startOffset, int endOffset, boolean fast) {
        EnumSet<SourceFlags> flags = EnumSet.of(SourceFlags.CHANGE_EXPECTED);
        if (invalidate) {
            flags.add(SourceFlags.INVALID);
            flags.add(SourceFlags.RESCHEDULE_FINISHED_TASKS);
        }
        SourceAccessor.getINSTANCE().setSourceModification(this.source, invalidate, startOffset, endOffset);
        SourceAccessor.getINSTANCE().setFlags(this.source, flags);
        if (mimeChanged) {
            SourceAccessor.getINSTANCE().mimeTypeMayChanged(this.source);
        }
        TaskProcessor.resetState(this.source, invalidate, true);
        if (!k24.get()) {
            this.resetTask.schedule(EventSupport.getReparseDelay(fast));
        }
    }

    public static void releaseCompletionCondition() {
        if (!IndexingManagerAccessor.getInstance().requiresReleaseOfCompletionLock() || !IndexingManagerAccessor.getInstance().isCalledFromRefreshIndexAndWait()) {
            throw new IllegalStateException();
        }
        boolean wask24 = k24.getAndSet(false);
        if (wask24) {
            TaskProcessor.resetStateImpl(null);
        }
    }

    public static void setReparseDelays(int standardReparseDelay, int fastReparseDelay) throws IllegalArgumentException {
        if (standardReparseDelay < fastReparseDelay) {
            throw new IllegalArgumentException(String.format("Fast reparse delay %d > standatd reparse delay %d", fastReparseDelay, standardReparseDelay));
        }
        immediateReparseDelay = fastReparseDelay;
        reparseDelay = standardReparseDelay;
    }

    public static int getReparseDelay(boolean fast) {
        return fast ? immediateReparseDelay : reparseDelay;
    }

    private void resetStateImpl() {
        if (!k24.get()) {
            boolean reschedule = SourceAccessor.getINSTANCE().testFlag(this.source, SourceFlags.RESCHEDULE_FINISHED_TASKS);
            if (reschedule) {
                SourceAccessor.getINSTANCE().getCache(this.source).sourceModified();
            }
            assert (this.source != null);
            TaskProcessor.resetStateImpl(this.source);
        }
    }

    private void assignDocumentListener(DataObject od) {
        EditorCookie.Observable ec = (EditorCookie.Observable)od.getCookie(EditorCookie.Observable.class);
        if (ec != null) {
            this.docListener = new DocListener(this, ec);
        }
    }

    public static class EditorRegistryListener
    implements CaretListener,
    PropertyChangeListener {
        private static final AtomicBoolean k24 = new AtomicBoolean();
        private Reference<JTextComponent> lastEditorRef;

        private EditorRegistryListener() {
            EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    EditorRegistryListener.this.editorRegistryChanged();
                }
            });
            this.editorRegistryChanged();
        }

        private void editorRegistryChanged() {
            JTextComponent lastEditor;
            JTextComponent editor = EditorRegistry.lastFocusedComponent();
            JTextComponent jTextComponent = lastEditor = this.lastEditorRef == null ? null : this.lastEditorRef.get();
            if (lastEditor != editor && (editor == null || editor.getClientProperty("AsTextField") == null)) {
                JTextComponent focused;
                if (lastEditor != null) {
                    lastEditor.removeCaretListener(this);
                    lastEditor.removePropertyChangeListener(this);
                    k24.set(false);
                }
                this.lastEditorRef = new WeakReference<JTextComponent>(editor);
                if (editor != null) {
                    editor.addCaretListener(this);
                    editor.addPropertyChangeListener(this);
                }
                if ((focused = EditorRegistry.focusedComponent()) != null) {
                    Source source;
                    Document doc = editor.getDocument();
                    String mimeType = DocumentUtilities.getMimeType((Document)doc);
                    if (doc != null && mimeType != null && (source = Source.create(doc)) != null) {
                        SourceAccessor.getINSTANCE().getEventSupport(source).resetState(true, false, -1, -1, true);
                    }
                }
            }
        }

        @Override
        public void caretUpdate(CaretEvent event) {
            JTextComponent lastEditor;
            JTextComponent jTextComponent = lastEditor = this.lastEditorRef == null ? null : this.lastEditorRef.get();
            if (lastEditor != null) {
                Source source;
                Document doc = lastEditor.getDocument();
                String mimeType = DocumentUtilities.getMimeType((Document)doc);
                if (doc != null && mimeType != null && (source = Source.create(doc)) != null) {
                    SourceAccessor.getINSTANCE().getEventSupport(source).resetState(false, false, -1, -1, false);
                }
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propName = evt.getPropertyName();
            if ("completion-active".equals(propName)) {
                String mimeType;
                Document doc;
                Source source = null;
                JTextComponent lastEditor = this.lastEditorRef == null ? null : this.lastEditorRef.get();
                Document document = doc = lastEditor == null ? null : lastEditor.getDocument();
                if (doc != null && (mimeType = DocumentUtilities.getMimeType((Document)doc)) != null) {
                    source = Source.create(doc);
                }
                if (source != null) {
                    this.handleCompletionActive(source, evt.getNewValue());
                }
            }
        }

        private void handleCompletionActive(@NonNull Source source, @NullAllowed Object rawValue) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "completion-active={0} for {1}", new Object[]{rawValue, source});
            }
            if (rawValue instanceof Boolean && ((Boolean)rawValue).booleanValue()) {
                TaskProcessor.resetState(source, false, true);
                k24.set(true);
            } else {
                EventSupport support = SourceAccessor.getINSTANCE().getEventSupport(source);
                k24.set(false);
                support.resetTask.schedule(0);
            }
        }

    }

    private final class DataObjectListener
    implements PropertyChangeListener {
        private DataObject dobj;
        private final FileObject fobj;
        private PropertyChangeListener wlistener;

        public DataObjectListener(DataObject dobj) {
            this.dobj = dobj;
            this.fobj = dobj.getPrimaryFile();
            this.wlistener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)dobj);
            this.dobj.addPropertyChangeListener(this.wlistener);
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            DataObject invalidDO = (DataObject)pce.getSource();
            if (invalidDO != this.dobj) {
                return;
            }
            String propName = pce.getPropertyName();
            if ("valid".equals(propName)) {
                this.handleInvalidDataObject(invalidDO);
            } else if (pce.getPropertyName() == null && !this.dobj.isValid()) {
                this.handleInvalidDataObject(invalidDO);
            }
        }

        private void handleInvalidDataObject(final DataObject invalidDO) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    DataObjectListener.this.handleInvalidDataObjectImpl(invalidDO);
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void handleInvalidDataObjectImpl(DataObject invalidDO) {
            invalidDO.removePropertyChangeListener(this.wlistener);
            if (this.fobj.isValid()) {
                try {
                    DataObject dobjNew = DataObject.find((FileObject)this.fobj);
                    DataObjectListener dataObjectListener = this;
                    synchronized (dataObjectListener) {
                        if (dobjNew == this.dobj) {
                            return;
                        }
                        this.dobj = dobjNew;
                        this.dobj.addPropertyChangeListener(this.wlistener);
                    }
                    EventSupport.this.assignDocumentListener(dobjNew);
                    EventSupport.this.resetState(true, false, -1, -1, false);
                }
                catch (DataObjectNotFoundException e) {
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }

    }

    private class FileChangeListenerImpl
    extends FileChangeAdapter {
        private FileChangeListenerImpl() {
        }

        public void fileChanged(FileEvent fe) {
            EventSupport.this.resetState(true, false, -1, -1, false);
        }

        public void fileRenamed(FileRenameEvent fe) {
            String newExt;
            String oldExt = fe.getExt();
            EventSupport.this.resetState(true, !Objects.equals(oldExt, newExt = fe.getFile().getExt()), -1, -1, false);
        }
    }

    private class ParserListener
    implements ChangeListener {
        private ParserListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            EventSupport.this.resetState(true, false, -1, -1, false);
        }
    }

    private class DocListener
    implements PropertyChangeListener,
    DocumentListener,
    TokenHierarchyListener {
        private final EditorCookie.Observable ec;
        private DocumentListener docListener;
        private TokenHierarchyListener thListener;
        final /* synthetic */ EventSupport this$0;

        public DocListener(EventSupport eventSupport, EditorCookie.Observable ec) {
            this.this$0 = eventSupport;
            assert (ec != null);
            this.ec = ec;
            this.ec.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.ec));
            Document doc = eventSupport.source.getDocument(false);
            if (doc != null) {
                this.assignDocumentListener(doc);
            }
        }

        public DocListener(EventSupport eventSupport, Document doc) {
            this.this$0 = eventSupport;
            assert (doc != null);
            this.ec = null;
            this.assignDocumentListener(doc);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("document".equals(evt.getPropertyName())) {
                Document doc;
                Object old = evt.getOldValue();
                if (old instanceof Document && this.docListener != null) {
                    doc = (Document)old;
                    TokenHierarchy th = TokenHierarchy.get((Document)doc);
                    th.removeTokenHierarchyListener(this.thListener);
                    doc.removeDocumentListener(this.docListener);
                    this.thListener = null;
                    this.docListener = null;
                }
                if ((doc = this.this$0.source.getDocument(false)) != null) {
                    this.assignDocumentListener(doc);
                    this.this$0.resetState(true, false, -1, -1, false);
                }
            }
        }

        private void assignDocumentListener(Document doc) {
            TokenHierarchy th = TokenHierarchy.get((Document)doc);
            this.thListener = (TokenHierarchyListener)WeakListeners.create(TokenHierarchyListener.class, (EventListener)this, (Object)th);
            th.addTokenHierarchyListener(this.thListener);
            this.docListener = (DocumentListener)WeakListeners.create(DocumentListener.class, (EventListener)this, (Object)doc);
            doc.addDocumentListener(this.docListener);
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            TokenHierarchy th = TokenHierarchy.get((Document)e.getDocument());
            if (th.isActive()) {
                return;
            }
            this.this$0.resetState(true, false, e.getOffset(), e.getOffset() + e.getLength(), false);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            TokenHierarchy th = TokenHierarchy.get((Document)e.getDocument());
            if (th.isActive()) {
                return;
            }
            this.this$0.resetState(true, false, e.getOffset(), e.getOffset(), false);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        public void tokenHierarchyChanged(TokenHierarchyEvent evt) {
            this.this$0.resetState(true, false, evt.affectedStartOffset(), evt.affectedEndOffset(), false);
        }
    }

}

