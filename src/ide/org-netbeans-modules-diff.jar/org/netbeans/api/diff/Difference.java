/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.diff;

import java.io.Serializable;

public class Difference
implements Serializable {
    public static final int DELETE = 0;
    public static final int ADD = 1;
    public static final int CHANGE = 2;
    private int type = 0;
    private int firstStart = 0;
    private int firstEnd = 0;
    private int secondStart = 0;
    private int secondEnd = 0;
    private Part[] firstLineDiffs;
    private Part[] secondLineDiffs;
    private String firstText;
    private String secondText;
    private static final long serialVersionUID = 7638201981188907148L;

    public Difference(int type, int firstStart, int firstEnd, int secondStart, int secondEnd) {
        this(type, firstStart, firstEnd, secondStart, secondEnd, null, null, null, null);
    }

    public Difference(int type, int firstStart, int firstEnd, int secondStart, int secondEnd, String firstText, String secondText) {
        this(type, firstStart, firstEnd, secondStart, secondEnd, firstText, secondText, null, null);
    }

    public Difference(int type, int firstStart, int firstEnd, int secondStart, int secondEnd, String firstText, String secondText, Part[] firstLineDiffs, Part[] secondLineDiffs) {
        if (type > 2 || type < 0) {
            throw new IllegalArgumentException("Bad Difference type = " + type);
        }
        this.type = type;
        this.firstStart = firstStart;
        this.firstEnd = firstEnd;
        this.secondStart = secondStart;
        this.secondEnd = secondEnd;
        this.firstText = firstText;
        this.secondText = secondText;
        this.firstLineDiffs = firstLineDiffs;
        this.secondLineDiffs = secondLineDiffs;
    }

    public int getType() {
        return this.type;
    }

    public int getFirstStart() {
        return this.firstStart;
    }

    public int getFirstEnd() {
        return this.firstEnd;
    }

    public int getSecondStart() {
        return this.secondStart;
    }

    public int getSecondEnd() {
        return this.secondEnd;
    }

    public Part[] getFirstLineDiffs() {
        return this.firstLineDiffs;
    }

    public Part[] getSecondLineDiffs() {
        return this.secondLineDiffs;
    }

    public String getFirstText() {
        return this.firstText;
    }

    public String getSecondText() {
        return this.secondText;
    }

    public String toString() {
        return "Difference(" + (this.type == 1 ? "ADD" : (this.type == 0 ? "DELETE" : "CHANGE")) + ", " + this.firstStart + ", " + this.firstEnd + ", " + this.secondStart + ", " + this.secondEnd + ")";
    }

    public static final class Part
    implements Serializable {
        private int type;
        private int line;
        private int pos1;
        private int pos2;
        private String text;
        private static final long serialVersionUID = 7638201981188907149L;

        public Part(int type, int line, int pos1, int pos2) {
            if (type > 2 || type < 0) {
                throw new IllegalArgumentException("Bad Difference type = " + type);
            }
            this.type = type;
            this.line = line;
            this.pos1 = pos1;
            this.pos2 = pos2;
        }

        public int getType() {
            return this.type;
        }

        public int getLine() {
            return this.line;
        }

        public int getStartPosition() {
            return this.pos1;
        }

        public int getEndPosition() {
            return this.pos2;
        }
    }

}

