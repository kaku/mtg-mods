/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.diff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.netbeans.modules.diff.PatchAction;
import org.netbeans.modules.diff.builtin.Patch;

public class PatchUtils {
    private PatchUtils() {
    }

    public static void applyPatch(File patch, File context) throws IOException {
        PatchAction.performPatch(patch, context);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isPatch(File patch) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(patch)));
        try {
            boolean bl = Patch.parse(reader).length > 0;
            return bl;
        }
        finally {
            reader.close();
        }
    }
}

