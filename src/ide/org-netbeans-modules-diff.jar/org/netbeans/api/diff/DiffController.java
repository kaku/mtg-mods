/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.api.diff;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import javax.swing.JComponent;
import org.netbeans.api.diff.Diff;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.spi.diff.DiffControllerImpl;
import org.netbeans.spi.diff.DiffControllerProvider;
import org.openide.util.Lookup;

public final class DiffController {
    public static final String PROP_DIFFERENCES = "(void) differencesChanged";
    private final DiffControllerImpl impl;

    public static DiffController create(StreamSource base, StreamSource modified) throws IOException {
        DiffControllerProvider provider = (DiffControllerProvider)Lookup.getDefault().lookup(DiffControllerProvider.class);
        if (provider != null) {
            return new DiffController(provider.createDiffController(base, modified));
        }
        DiffView view = Diff.getDefault().createDiff(base, modified);
        return new DiffController(new DiffControllerViewBridge(view));
    }

    public static DiffController createEnhanced(StreamSource base, StreamSource modified) throws IOException {
        DiffControllerProvider provider = (DiffControllerProvider)Lookup.getDefault().lookup(DiffControllerProvider.class);
        if (provider != null) {
            return new DiffController(provider.createEnhancedDiffController(base, modified));
        }
        DiffView view = Diff.getDefault().createDiff(base, modified);
        return new DiffController(new DiffControllerViewBridge(view));
    }

    private DiffController(DiffControllerImpl impl) {
        this.impl = impl;
    }

    public void setLocation(DiffPane pane, LocationType type, int location) {
        this.impl.setLocation(pane, type, location);
    }

    public JComponent getJComponent() {
        return this.impl.getJComponent();
    }

    public int getDifferenceCount() {
        return this.impl.getDifferenceCount();
    }

    public int getDifferenceIndex() {
        return this.impl.getDifferenceIndex();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.impl.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.impl.removePropertyChangeListener(listener);
    }

    private static class DiffControllerViewBridge
    extends DiffControllerImpl {
        private final DiffView view;

        DiffControllerViewBridge(DiffView view) {
            this.view = view;
        }

        @Override
        public void setLocation(DiffPane pane, LocationType type, int location) {
            if (type == LocationType.DifferenceIndex) {
                this.view.setCurrentDifference(location);
            }
        }

        @Override
        public JComponent getJComponent() {
            return (JComponent)this.view.getComponent();
        }

        @Override
        public int getDifferenceCount() {
            return this.view.getDifferenceCount();
        }
    }

    public static enum LocationType {
        LineNumber,
        DifferenceIndex;
        

        private LocationType() {
        }
    }

    public static enum DiffPane {
        Base,
        Modified;
        

        private DiffPane() {
        }
    }

}

