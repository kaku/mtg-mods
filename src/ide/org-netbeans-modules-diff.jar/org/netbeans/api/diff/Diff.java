/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.api.diff;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import javax.swing.JToolBar;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.StreamSource;
import org.openide.util.Lookup;

public abstract class Diff {
    public static Diff getDefault() {
        return (Diff)Lookup.getDefault().lookup(Diff.class);
    }

    public static Collection<? extends Diff> getAll() {
        return Lookup.getDefault().lookup(new Lookup.Template(Diff.class)).allInstances();
    }

    public abstract Component createDiff(String var1, String var2, Reader var3, String var4, String var5, Reader var6, String var7) throws IOException;

    public DiffView createDiff(StreamSource s1, StreamSource s2) throws IOException {
        final Component c = this.createDiff(s1.getName(), s1.getTitle(), s1.createReader(), s2.getName(), s2.getTitle(), s2.createReader(), s1.getMIMEType());
        return new DiffView(){

            @Override
            public Component getComponent() {
                return c;
            }

            @Override
            public int getDifferenceCount() {
                return 0;
            }

            @Override
            public boolean canSetCurrentDifference() {
                return false;
            }

            @Override
            public void setCurrentDifference(int diffNo) throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }

            @Override
            public int getCurrentDifference() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }

            @Override
            public JToolBar getToolBar() {
                return null;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener l) {
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener l) {
            }
        };
    }

}

