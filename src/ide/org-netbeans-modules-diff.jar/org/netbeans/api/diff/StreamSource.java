/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.io.ReaderInputStream
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.api.diff;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.io.ReaderInputStream;
import org.openide.util.lookup.Lookups;

public abstract class StreamSource {
    public abstract String getName();

    public abstract String getTitle();

    public abstract String getMIMEType();

    public boolean isEditable() {
        return false;
    }

    public Lookup getLookup() {
        return Lookups.fixed((Object[])new Object[0]);
    }

    public abstract Reader createReader() throws IOException;

    public abstract Writer createWriter(Difference[] var1) throws IOException;

    public void close() {
    }

    public static StreamSource createSource(String name, String title, String MIMEType, Reader r) {
        return new Impl(name, title, MIMEType, r);
    }

    public static StreamSource createSource(String name, String title, String MIMEType, File file) {
        return new Impl(name, title, MIMEType, file);
    }

    private static void copyStreamsCloseAll(Writer writer, Reader reader) throws IOException {
        int n;
        char[] buffer = new char[4096];
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        writer.close();
        reader.close();
    }

    private static class Impl
    extends StreamSource {
        private String name;
        private String title;
        private String MIMEType;
        private Reader r;
        private File readerSource;
        private Writer w;
        private File file;
        private Charset encoding;

        Impl(String name, String title, String MIMEType, Reader r) {
            this.name = name;
            this.title = title;
            this.MIMEType = MIMEType;
            this.r = r;
            this.readerSource = null;
            this.w = null;
            this.file = null;
            if (r instanceof InputStreamReader) {
                try {
                    this.encoding = Charset.forName(((InputStreamReader)r).getEncoding());
                }
                catch (UnsupportedCharsetException e) {
                    // empty catch block
                }
            }
        }

        Impl(String name, String title, String MIMEType, File file) {
            this.name = name;
            this.title = title;
            this.MIMEType = MIMEType;
            this.readerSource = null;
            this.w = null;
            this.file = file;
            this.encoding = FileEncodingQuery.getEncoding((FileObject)FileUtil.toFileObject((File)file));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private File createReaderSource(Reader r) throws IOException {
            File tmp = null;
            tmp = FileUtil.normalizeFile((File)File.createTempFile("sss", "tmp"));
            tmp.deleteOnExit();
            tmp.createNewFile();
            InputStream in = null;
            OutputStream out = null;
            try {
                if (this.encoding == null) {
                    in = new ReaderInputStream(r);
                } else {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    StreamSource.copyStreamsCloseAll(new OutputStreamWriter((OutputStream)baos, this.encoding), r);
                    in = new ByteArrayInputStream(baos.toByteArray());
                }
                out = new FileOutputStream(tmp);
                FileUtil.copy((InputStream)in, (OutputStream)out);
            }
            finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
            return tmp;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getTitle() {
            return this.title;
        }

        @Override
        public String getMIMEType() {
            return this.MIMEType;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public Reader createReader() throws IOException {
            if (this.file != null) {
                return new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(this.file), this.encoding));
            }
            Impl impl = this;
            synchronized (impl) {
                if (this.r != null) {
                    this.readerSource = this.createReaderSource(this.r);
                    this.r = null;
                }
            }
            if (this.encoding == null) {
                return new BufferedReader(new FileReader(this.readerSource));
            }
            return new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(this.readerSource), this.encoding));
        }

        @Override
        public Writer createWriter(Difference[] conflicts) throws IOException {
            if (conflicts != null && conflicts.length > 0) {
                return null;
            }
            if (this.file != null) {
                if (this.encoding == null) {
                    return new BufferedWriter(new FileWriter(this.file));
                }
                return new BufferedWriter(new OutputStreamWriter((OutputStream)new FileOutputStream(this.file), this.encoding));
            }
            return this.w;
        }
    }

}

