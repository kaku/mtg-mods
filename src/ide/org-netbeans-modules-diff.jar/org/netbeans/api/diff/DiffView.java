/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.diff;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import javax.swing.JToolBar;

public interface DiffView {
    public static final String PROP_DIFF_COUNT = "diffCount";

    public Component getComponent();

    public int getDifferenceCount();

    public boolean canSetCurrentDifference();

    public void setCurrentDifference(int var1) throws UnsupportedOperationException;

    public int getCurrentDifference() throws UnsupportedOperationException;

    public JToolBar getToolBar();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

