/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.diff;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.api.diff.DiffController;

public abstract class DiffControllerImpl {
    protected final PropertyChangeSupport support;
    private int differenceIndex;

    protected DiffControllerImpl() {
        this.support = new PropertyChangeSupport(this);
        this.differenceIndex = -1;
    }

    public void setLocation(DiffController.DiffPane pane, DiffController.LocationType type, int location) {
    }

    public abstract JComponent getJComponent();

    public abstract int getDifferenceCount();

    public final int getDifferenceIndex() {
        return this.differenceIndex;
    }

    protected final void setDifferenceIndex(int idx) {
        assert (idx >= -1);
        if (idx == this.differenceIndex) {
            return;
        }
        this.differenceIndex = idx;
        this.support.firePropertyChange("(void) differencesChanged", null, null);
    }

    public final void addPropertyChangeListener(PropertyChangeListener listener) {
        this.support.addPropertyChangeListener(listener);
    }

    public final void removePropertyChangeListener(PropertyChangeListener listener) {
        this.support.removePropertyChangeListener(listener);
    }
}

