/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.diff;

import java.awt.Component;
import java.io.IOException;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;

public abstract class MergeVisualizer {
    public abstract Component createView(Difference[] var1, StreamSource var2, StreamSource var3, StreamSource var4) throws IOException;
}

