/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.diff;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import javax.swing.JToolBar;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;

public abstract class DiffVisualizer {
    public abstract Component createView(Difference[] var1, String var2, String var3, Reader var4, String var5, String var6, Reader var7, String var8) throws IOException;

    public DiffView createDiff(Difference[] diffs, StreamSource s1, StreamSource s2) throws IOException {
        final Component c = this.createView(diffs, s1.getName(), s1.getTitle(), s1.createReader(), s2.getName(), s2.getTitle(), s2.createReader(), s1.getMIMEType());
        final int n = diffs.length;
        return new DiffView(){

            @Override
            public Component getComponent() {
                return c;
            }

            @Override
            public int getDifferenceCount() {
                return n;
            }

            @Override
            public boolean canSetCurrentDifference() {
                return false;
            }

            @Override
            public void setCurrentDifference(int diffNo) throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }

            @Override
            public int getCurrentDifference() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }

            @Override
            public JToolBar getToolBar() {
                return null;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener l) {
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener l) {
            }
        };
    }

}

