/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.diff;

import java.io.IOException;
import java.io.Reader;
import org.netbeans.api.diff.Difference;

public abstract class DiffProvider {
    public abstract Difference[] computeDiff(Reader var1, Reader var2) throws IOException;
}

