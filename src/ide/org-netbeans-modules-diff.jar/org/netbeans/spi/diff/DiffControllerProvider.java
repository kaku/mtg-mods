/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.diff;

import java.io.IOException;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.spi.diff.DiffControllerImpl;

public abstract class DiffControllerProvider {
    public abstract DiffControllerImpl createDiffController(StreamSource var1, StreamSource var2) throws IOException;

    public DiffControllerImpl createEnhancedDiffController(StreamSource base, StreamSource modified) throws IOException {
        return this.createDiffController(base, modified);
    }
}

