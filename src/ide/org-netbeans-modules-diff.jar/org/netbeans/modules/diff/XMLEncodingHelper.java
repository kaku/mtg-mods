/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.diff;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.openide.ErrorManager;

final class XMLEncodingHelper {
    private static final int EXPECTED_PROLOG_LENGTH = 1000;

    XMLEncodingHelper() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String detectEncoding(InputStream in) throws IOException {
        if (!in.markSupported()) {
            ErrorManager.getDefault().log("XMLEncodingHelper got unmarkable stream: " + in.getClass());
            return null;
        }
        try {
            String enc;
            in.mark(1000);
            byte[] bytes = new byte[1000];
            for (int i = 0; i < bytes.length; ++i) {
                try {
                    int datum = in.read();
                    if (datum == -1) break;
                    bytes[i] = (byte)datum;
                    continue;
                }
                catch (EOFException ex) {
                    // empty catch block
                }
            }
            if ((enc = XMLEncodingHelper.autoDetectEncoding(bytes)) == null) {
                String ex = null;
                return ex;
            }
            if ((enc = XMLEncodingHelper.detectDeclaredEncoding(bytes, enc)) == null) {
                String ex = null;
                return ex;
            }
            String ex = enc;
            return ex;
        }
        finally {
            in.reset();
        }
    }

    static String autoDetectEncoding(byte[] buf) throws IOException {
        if (buf.length >= 4) {
            switch (buf[0]) {
                case 0: {
                    if (buf[1] != 60 || buf[2] != 0 || buf[3] != 63) break;
                    return "UnicodeBigUnmarked";
                }
                case 60: {
                    switch (buf[1]) {
                        case 0: {
                            if (buf[2] != 63 || buf[3] != 0) break;
                            return "UnicodeLittleUnmarked";
                        }
                        case 63: {
                            if (buf[2] != 120 || buf[3] != 109) break;
                            return "UTF8";
                        }
                    }
                    break;
                }
                case 76: {
                    if (buf[1] != 111 || buf[2] != -89 || buf[3] != -108) break;
                    return "Cp037";
                }
                case -2: {
                    if (buf[1] != -1 || buf[2] == 0 && buf[3] == 0) break;
                    return "UnicodeBig";
                }
                case -1: {
                    if (buf[1] != -2 || buf[2] == 0 && buf[3] == 0) break;
                    return "UnicodeLittle";
                }
                case -17: {
                    if (buf[1] != -69 || buf[2] != -65) break;
                    return "UTF8";
                }
            }
        }
        return null;
    }

    static String detectDeclaredEncoding(byte[] data, String baseEncoding) throws IOException {
        int i;
        String s;
        int iend;
        StringBuffer buf = new StringBuffer();
        char delimiter = '\"';
        InputStreamReader r = new InputStreamReader((InputStream)new ByteArrayInputStream(data), baseEncoding);
        try {
            int c = r.read();
            while (c != -1) {
                buf.append((char)c);
                c = r.read();
            }
        }
        catch (IOException ex) {
            // empty catch block
        }
        iend = (iend = (s = buf.toString()).indexOf("?>")) == -1 ? s.length() : iend;
        int iestart = s.indexOf("encoding");
        if (iestart == -1 || iestart > iend) {
            return null;
        }
        char[] chars = s.toCharArray();
        for (i = iestart; i < iend && chars[i] != '='; ++i) {
        }
        while (i < iend) {
            if (chars[i] == '\'' || chars[i] == '\"') {
                delimiter = chars[i];
                break;
            }
            ++i;
        }
        int ivalstart = ++i;
        while (i < iend) {
            if (chars[i] == delimiter) {
                return new String(chars, ivalstart, i - ivalstart);
            }
            ++i;
        }
        return null;
    }
}

