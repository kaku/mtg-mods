/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.project.Project
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.UndoRedo
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Cancellable
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.diff;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import org.netbeans.api.diff.Diff;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.Project;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.EditorBufferSelectorPanel;
import org.netbeans.modules.diff.builtin.DefaultDiff;
import org.netbeans.modules.diff.builtin.SingleDiffPanel;
import org.netbeans.modules.diff.options.AccessibleJFileChooser;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.UndoRedo;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Node;
import org.openide.util.Cancellable;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class DiffAction
extends NodeAction {
    private static boolean diffAvailable = true;

    public DiffAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new DiffActionImpl(actionContext);
    }

    public String getName() {
        return NbBundle.getMessage(DiffAction.class, (String)"CTL_DiffActionName");
    }

    static FileObject getFileFromNode(Node node) {
        FileObject fo = (FileObject)node.getLookup().lookup(FileObject.class);
        if (fo == null) {
            Project p = (Project)node.getLookup().lookup(Project.class);
            if (p != null) {
                return p.getProjectDirectory();
            }
            DataObject dobj = (DataObject)node.getCookie(DataObject.class);
            if (dobj instanceof DataShadow) {
                dobj = ((DataShadow)dobj).getOriginal();
            }
            if (dobj != null) {
                fo = dobj.getPrimaryFile();
            }
        }
        return fo;
    }

    public boolean enable(Node[] nodes) {
        FileObject fo1;
        if (!diffAvailable) {
            return false;
        }
        if (nodes.length == 2) {
            FileObject fo12 = DiffAction.getFileFromNode(nodes[0]);
            FileObject fo2 = DiffAction.getFileFromNode(nodes[1]);
            if (fo12 != null && fo2 != null && fo12.isData() && fo2.isData()) {
                return true;
            }
        } else if (nodes.length == 1 && (fo1 = DiffAction.getFileFromNode(nodes[0])) != null && fo1.isData()) {
            return true;
        }
        return false;
    }

    protected boolean asynchronous() {
        return false;
    }

    public void performAction(Node[] nodes) {
        FileObject fo2;
        ArrayList<FileObject> fos = new ArrayList<FileObject>();
        for (int i = 0; i < nodes.length; ++i) {
            FileObject fo = DiffAction.getFileFromNode(nodes[i]);
            if (fo == null) continue;
            fos.add(fo);
        }
        if (fos.size() < 1) {
            return;
        }
        final FileObject fo1 = (FileObject)fos.get(0);
        if (fos.size() > 1) {
            fo2 = (FileObject)fos.get(1);
        } else {
            fo2 = this.promptForFileobject(fo1);
            if (fo2 == null) {
                return;
            }
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DiffAction.performAction(fo1, fo2, null);
            }
        });
    }

    private FileObject promptForFileobject(FileObject peer) {
        String path = DiffModuleConfig.getDefault().getPreferences().get("diffToLatestFolder", peer.getParent().getPath());
        File latestPath = FileUtil.normalizeFile((File)new File(path));
        AccessibleJFileChooser fileChooser = new AccessibleJFileChooser(NbBundle.getMessage(DiffAction.class, (String)"ACSD_BrowseDiffToFile"), latestPath);
        fileChooser.setDialogTitle(NbBundle.getMessage(DiffAction.class, (String)"DiffTo_BrowseFile_Title", (Object)peer.getName()));
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(0);
        EditorBufferSelectorPanel editorSelector = new EditorBufferSelectorPanel(fileChooser, peer);
        fileChooser.setAccessory(editorSelector);
        int result = fileChooser.showDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(DiffAction.class, (String)"DiffTo_BrowseFile_OK"));
        if (result != 0) {
            return null;
        }
        File f = fileChooser.getSelectedFile();
        if (f != null) {
            File file = f.getAbsoluteFile();
            DiffModuleConfig.getDefault().getPreferences().put("diffToLatestFolder", file.getParent());
            return FileUtil.toFileObject((File)f);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void performAction(FileObject fo1, FileObject fo2, FileObject type) {
        Diff diff = Diff.getDefault();
        if (diff == null) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DiffAction.class, (String)"MSG_NoDiffVisualizer")));
            diffAvailable = false;
            return;
        }
        SingleDiffPanel sdp = null;
        try {
            final Thread victim = Thread.currentThread();
            Cancellable killer = new Cancellable(){

                public boolean cancel() {
                    victim.interrupt();
                    return true;
                }
            };
            String name = NbBundle.getMessage(DiffAction.class, (String)"BK0001");
            ProgressHandle ph = ProgressHandleFactory.createHandle((String)name, (Cancellable)killer);
            try {
                ph.start();
                sdp = new SingleDiffPanel(fo1, fo2, type);
            }
            finally {
                ph.finish();
            }
        }
        catch (IOException ioex) {
            ErrorManager.getDefault().notify((Throwable)ioex);
            return;
        }
        if (sdp != null) {
            final SingleDiffPanel fsdp = sdp;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    DefaultDiff.DiffTopComponent dtc = new DefaultDiff.DiffTopComponent(fsdp){

                        protected void componentActivated() {
                            super.componentActivated();
                            fsdp.requestActive();
                        }

                        public UndoRedo getUndoRedo() {
                            return fsdp.getUndoRedo();
                        }
                    };
                    fsdp.putClientProperty(TopComponent.class, (Object)dtc);
                    fsdp.activateNodes();
                    dtc.open();
                    dtc.requestActive();
                }

            });
        }
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(DiffAction.class);
    }

    private class DiffActionImpl
    extends AbstractAction {
        private final Node[] nodes;

        private DiffActionImpl(Lookup context) {
            Collection nodez = context.lookup(new Lookup.Template(Node.class)).allInstances();
            this.nodes = nodez.toArray((T[])new Node[nodez.size()]);
            if (this.nodes.length == 1) {
                this.putValue("Name", NbBundle.getMessage(DiffAction.class, (String)"CTL_DiffToActionName"));
            } else {
                this.putValue("Name", DiffAction.this.getName());
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            DiffAction.this.performAction(this.nodes);
        }

        @Override
        public boolean isEnabled() {
            return DiffAction.this.enable(this.nodes);
        }
    }

}

