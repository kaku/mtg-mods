/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileUtil
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.modules.diff;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.diff.DiffAction;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.Utils;
import org.netbeans.modules.diff.builtin.ContextualPatch;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileUtil;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

public class PatchAction
extends NodeAction {
    private static final String PREF_RECENT_PATCH_PATH = "patch.recentPatchDir";

    public PatchAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public String getName() {
        return NbBundle.getMessage(PatchAction.class, (String)"CTL_PatchActionName");
    }

    public boolean enable(Node[] nodes) {
        FileObject fo;
        if (nodes.length == 1 && (fo = DiffAction.getFileFromNode(nodes[0])) != null) {
            try {
                return fo.getURL().getProtocol().equals("file");
            }
            catch (FileStateInvalidException fsiex) {
                return false;
            }
        }
        return false;
    }

    protected boolean asynchronous() {
        return false;
    }

    public void performAction(Node[] nodes) {
        final FileObject fo = DiffAction.getFileFromNode(nodes[0]);
        if (fo != null) {
            final File patch = this.getPatchFor(fo);
            if (patch == null) {
                return;
            }
            Utils.postParallel(new Runnable(){

                @Override
                public void run() {
                    PatchAction.performPatch(patch, FileUtil.toFile((FileObject)fo));
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean performPatch(File patch, File file) throws MissingResourceException {
        List<ContextualPatch.PatchReport> report;
        ProgressHandle ph = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(PatchAction.class, (String)"MSG_AplyingPatch", (Object[])new Object[]{patch.getName()}));
        report = null;
        try {
            ph.start();
            ContextualPatch cp = ContextualPatch.create(patch, file);
            try {
                report = cp.patch(false, ph);
            }
            catch (Exception ioex) {
                ErrorManager.getDefault().annotate((Throwable)ioex, NbBundle.getMessage(PatchAction.class, (String)"EXC_PatchParsingFailed", (Object)ioex.getLocalizedMessage()));
                ErrorManager.getDefault().notify(1, (Throwable)ioex);
                ErrorManager.getDefault().notify(256, (Throwable)ioex);
                boolean bl = false;
                ph.finish();
                return bl;
            }
        }
        finally {
            ph.finish();
        }
        return PatchAction.displayPatchReport(report, patch);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean displayPatchReport(List<ContextualPatch.PatchReport> report, final File patchFile) {
        ArrayList<ContextualPatch.PatchReport> failed;
        ArrayList<ContextualPatch.PatchReport> successful;
        block23 : {
            successful = new ArrayList<ContextualPatch.PatchReport>();
            failed = new ArrayList<ContextualPatch.PatchReport>();
            for (ContextualPatch.PatchReport patchReport : report) {
                switch (patchReport.getStatus()) {
                    case Patched: {
                        successful.add(patchReport);
                        break;
                    }
                    case Failure: 
                    case Missing: {
                        failed.add(patchReport);
                    }
                }
            }
            InputOutput log = IOProvider.getDefault().getIO("Patch Report", false);
            OutputWriter ow = log.getOut();
            if (log.isClosed()) {
                try {
                    ow.reset();
                }
                catch (IOException ex) {
                    // empty catch block
                }
                log.select();
            }
            try {
                ow.print(DateFormat.getDateTimeInstance().format(new Date()));
                ow.println("  ===========================================================================");
                ow.print(NbBundle.getMessage(PatchAction.class, (String)"MSG_PatchAction.output.patchFile"));
                try {
                    ow.println(patchFile.getAbsolutePath(), new OutputListener(){

                        public void outputLineSelected(OutputEvent ev) {
                        }

                        public void outputLineAction(OutputEvent ev) {
                            Utils.openFile(patchFile);
                        }

                        public void outputLineCleared(OutputEvent ev) {
                        }
                    });
                }
                catch (IOException ex) {
                    ow.println(patchFile.getAbsolutePath());
                }
                ow.println("--- Successfully Patched ---");
                if (successful.size() > 0) {
                    for (ContextualPatch.PatchReport patchReport2 : successful) {
                        ow.println(patchReport2.getFile().getAbsolutePath());
                    }
                } else {
                    ow.println("<none>");
                }
                ow.println("--- Failed ---");
                if (failed.size() > 0) {
                    for (ContextualPatch.PatchReport patchReport2 : failed) {
                        ow.print(patchReport2.getFile().getAbsolutePath());
                        ow.print(" (");
                        ow.print(patchReport2.getFailure().getLocalizedMessage());
                        ow.println(" )");
                    }
                    break block23;
                }
                ow.println("<none>");
            }
            finally {
                ow.close();
            }
        }
        if (successful.size() > 0) {
            ArrayList<FileObject> binaries = new ArrayList<FileObject>();
            ArrayList<FileObject> appliedFiles = new ArrayList<FileObject>();
            HashMap<FileObject, FileObject> backups = new HashMap<FileObject, FileObject>();
            for (ContextualPatch.PatchReport patchReport : successful) {
                FileObject fo = FileUtil.toFileObject((File)patchReport.getFile());
                FileObject backup = FileUtil.toFileObject((File)patchReport.getOriginalBackupFile());
                if (patchReport.isBinary()) {
                    binaries.add(fo);
                }
                appliedFiles.add(fo);
                backups.put(fo, backup);
            }
            String message = failed.size() > 0 ? NbBundle.getMessage(PatchAction.class, (String)"MSG_PatchAppliedPartially") : NbBundle.getMessage(PatchAction.class, (String)"MSG_PatchAppliedSuccessfully");
            Object notifyResult = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)message, 0));
            if (NotifyDescriptor.YES_OPTION.equals(notifyResult)) {
                PatchAction.showDiffs(appliedFiles, binaries, backups);
                PatchAction.removeBackups(appliedFiles, backups, true);
            } else {
                PatchAction.removeBackups(appliedFiles, backups, false);
            }
            return failed.isEmpty();
        }
        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(PatchAction.class, (String)"MSG_WrongPatch")));
        return false;
    }

    private File getPatchFor(FileObject fo) {
        JFileChooser chooser = new JFileChooser();
        String patchDirPath = DiffModuleConfig.getDefault().getPreferences().get("patch.recentPatchDir", System.getProperty("user.home"));
        File patchDir = new File(patchDirPath);
        while (!patchDir.isDirectory()) {
            if ((patchDir = patchDir.getParentFile()) != null) continue;
            patchDir = new File(System.getProperty("user.home"));
            break;
        }
        FileUtil.preventFileChooserSymlinkTraversal((JFileChooser)chooser, (File)patchDir);
        chooser.setFileSelectionMode(0);
        String title = NbBundle.getMessage(PatchAction.class, (String)(fo.isData() ? "TITLE_SelectPatchForFile" : "TITLE_SelectPatchForFolder"), (Object)fo.getNameExt());
        chooser.setDialogTitle(title);
        FileFilter patchFilter = new FileFilter(){

            @Override
            public boolean accept(File f) {
                return f.getName().endsWith("diff") || f.getName().endsWith("patch") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return NbBundle.getMessage(PatchAction.class, (String)"CTL_PatchDialog_FileFilter");
            }
        };
        chooser.addChoosableFileFilter(patchFilter);
        chooser.setFileFilter(patchFilter);
        chooser.setApproveButtonText(NbBundle.getMessage(PatchAction.class, (String)"BTN_Patch"));
        chooser.setApproveButtonMnemonic(NbBundle.getMessage(PatchAction.class, (String)"BTN_Patch_mnc").charAt(0));
        chooser.setApproveButtonToolTipText(NbBundle.getMessage(PatchAction.class, (String)"BTN_Patch_tooltip"));
        HelpCtx ctx = new HelpCtx(PatchAction.class.getName());
        DialogDescriptor descriptor = new DialogDescriptor((Object)chooser, title, true, new Object[0], (Object)null, 0, ctx, null);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(descriptor);
        dialog.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(PatchAction.class, (String)"ACSD_PatchDialog"));
        ChooserListener listener = new ChooserListener(dialog, chooser);
        chooser.addActionListener(listener);
        dialog.setVisible(true);
        File selectedFile = listener.getFile();
        if (selectedFile != null) {
            DiffModuleConfig.getDefault().getPreferences().put("patch.recentPatchDir", selectedFile.getParentFile().getAbsolutePath());
        }
        return selectedFile;
    }

    private static void showDiffs(List<FileObject> files, List<FileObject> binaries, Map<FileObject, FileObject> backups) {
        for (int i = 0; i < files.size(); ++i) {
            FileObject file = files.get(i);
            FileObject backup = backups.get((Object)file);
            if (binaries.contains((Object)file)) continue;
            if (backup == null) {
                try {
                    backup = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)File.createTempFile("diff-empty-backup", "")));
                }
                catch (IOException e) {
                    // empty catch block
                }
            }
            DiffAction.performAction(backup, file, file);
        }
    }

    private static void removeBackups(List<FileObject> files, Map<FileObject, FileObject> backups, boolean onExit) {
        StringBuffer filenames = new StringBuffer();
        StringBuffer exceptions = new StringBuffer();
        for (int i = 0; i < files.size(); ++i) {
            FileObject targetFileObject = files.get(i);
            FileObject backup = backups.get((Object)targetFileObject);
            if (targetFileObject != null && targetFileObject.getSize() == 0 && backup != null && backup.isValid() && backup.getSize() > 0) {
                if (onExit) {
                    PatchAction.deleteOnExit(targetFileObject);
                } else {
                    try {
                        targetFileObject.delete();
                    }
                    catch (IOException e) {
                        ErrorManager err = ErrorManager.getDefault();
                        err.annotate((Throwable)e, "Patch can not delete file, skipping...");
                        err.notify(1, (Throwable)e);
                    }
                }
            }
            if (backup == null || !backup.isValid()) continue;
            if (onExit) {
                PatchAction.deleteOnExit(backup);
                continue;
            }
            try {
                backup.delete();
                continue;
            }
            catch (IOException ex) {
                filenames.append(FileUtil.getFileDisplayName((FileObject)backup));
                filenames.append('\n');
                exceptions.append(ex.getLocalizedMessage());
                exceptions.append('\n');
            }
        }
        if (filenames.length() > 0) {
            ErrorManager.getDefault().notify(ErrorManager.getDefault().annotate((Throwable)new IOException(), NbBundle.getMessage(PatchAction.class, (String)"EXC_CannotRemoveBackup", (Object)filenames, (Object)exceptions)));
        }
    }

    private static void deleteOnExit(FileObject fo) {
        File file = FileUtil.toFile((FileObject)fo);
        if (file != null) {
            file.deleteOnExit();
        }
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(PatchAction.class);
    }

    class ChooserListener
    implements ActionListener {
        private Dialog dialog;
        private JFileChooser chooser;
        private File file;

        public ChooserListener(Dialog dialog, JFileChooser chooser) {
            this.file = null;
            this.dialog = dialog;
            this.chooser = chooser;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            if (command == "ApproveSelection") {
                if (this.dialog != null) {
                    this.file = this.chooser.getSelectedFile();
                    this.dialog.setVisible(false);
                }
            } else if (this.dialog != null) {
                this.file = null;
                this.dialog.setVisible(false);
                this.dialog.dispose();
            }
        }

        public File getFile() {
            return this.file;
        }
    }

}

