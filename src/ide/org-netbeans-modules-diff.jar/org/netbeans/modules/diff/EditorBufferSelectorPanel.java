/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.diff;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

class EditorBufferSelectorPanel
extends JPanel
implements ListSelectionListener {
    private final JFileChooser fileChooser;
    private final FileObject peer;
    private JList elementsList;
    private JLabel jLabel1;
    private JPanel jPanel1;

    public EditorBufferSelectorPanel(JFileChooser fileChooser, FileObject peer) {
        this.fileChooser = fileChooser;
        this.peer = peer;
        this.initComponents();
        this.initEditorDocuments();
    }

    private void initEditorDocuments() {
        this.elementsList = new JList(){

            @Override
            public String getToolTipText(MouseEvent event) {
                int index = this.locationToIndex(event.getPoint());
                if (index != -1) {
                    EditorListElement element = (EditorListElement)EditorBufferSelectorPanel.this.elementsList.getModel().getElementAt(index);
                    return element.fileObject.getPath();
                }
                return null;
            }
        };
        ArrayList<EditorListElement> elements = new ArrayList<EditorListElement>();
        WindowManager wm = WindowManager.getDefault();
        Set modes = wm.getModes();
        for (Mode mode : modes) {
            TopComponent[] tcs;
            if (!wm.isEditorMode(mode)) continue;
            for (TopComponent tc : tcs = mode.getTopComponents()) {
                DataObject dobj;
                Lookup lukap = tc.getLookup();
                FileObject fo = (FileObject)lukap.lookup(FileObject.class);
                if (fo == null && (dobj = (DataObject)lukap.lookup(DataObject.class)) != null) {
                    fo = dobj.getPrimaryFile();
                }
                if (fo == null || fo == this.peer) continue;
                if (tc.getHtmlDisplayName() != null) {
                    elements.add(new EditorListElement(fo, tc.getHtmlDisplayName(), true));
                    continue;
                }
                elements.add(new EditorListElement(fo, tc.getName(), false));
            }
        }
        this.elementsList.setListData(elements.toArray(new EditorListElement[elements.size()]));
        this.elementsList.setSelectionMode(0);
        this.elementsList.addListSelectionListener(this);
        this.elementsList.setCellRenderer(new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (isSelected && value instanceof EditorListElement && ((EditorListElement)value).isHtml()) {
                    value = this.stripHtml(((EditorListElement)value).toString());
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }

            private String stripHtml(String htmlText) {
                if (null == htmlText) {
                    return null;
                }
                String res = htmlText.replaceAll("<[^>]*>", "");
                res = res.replaceAll("&nbsp;", " ");
                res = res.trim();
                return res;
            }
        });
        JScrollPane sp = new JScrollPane(this.elementsList);
        this.jPanel1.add(sp);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        EditorListElement element = (EditorListElement)this.elementsList.getSelectedValue();
        if (element != null) {
            File file = FileUtil.toFile((FileObject)element.fileObject);
            this.fileChooser.setSelectedFile(file);
        } else {
            File file = new File("");
            this.fileChooser.setSelectedFile(file);
        }
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jPanel1 = new JPanel();
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(EditorBufferSelectorPanel.class, (String)"EditorBufferSelectorPanel.jLabel1.text"));
        this.jPanel1.setLayout(new BorderLayout());
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, 216, 32767).addComponent(this.jLabel1)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -1, 162, 32767).addContainerGap()));
    }

    private class EditorListElement {
        FileObject fileObject;
        String displayName;
        private final boolean html;

        EditorListElement(FileObject tc, String displayName, boolean isHtml) {
            this.fileObject = tc;
            this.displayName = displayName;
            this.html = isHtml;
        }

        public String toString() {
            return this.displayName;
        }

        public boolean isHtml() {
            return this.html;
        }
    }

}

