/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.cmdline;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.diff.cmdline.CmdlineDiffProvider;
import org.openide.ErrorManager;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class CmdlineDiffProviderBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(CmdlineDiffProvider.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] desc;
        try {
            PropertyDescriptor diffCommand = new PropertyDescriptor("diffCommand", CmdlineDiffProvider.class);
            diffCommand.setDisplayName(NbBundle.getMessage(CmdlineDiffProviderBeanInfo.class, (String)"PROP_diffCmd"));
            diffCommand.setShortDescription(NbBundle.getMessage(CmdlineDiffProviderBeanInfo.class, (String)"HINT_diffCmd"));
            desc = new PropertyDescriptor[]{diffCommand};
        }
        catch (IntrospectionException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
            desc = null;
        }
        return desc;
    }

    @Override
    public int getDefaultPropertyIndex() {
        return 0;
    }

    @Override
    public Image getIcon(int iconKind) {
        switch (iconKind) {
            case 1: {
                return ImageUtilities.loadImage((String)"org/netbeans/modules/diff/diffSettingsIcon.gif", (boolean)true);
            }
        }
        return null;
    }
}

