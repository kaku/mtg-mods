/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.diff.cmdline;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.api.diff.Difference;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class CmdlineDiffProvider
extends DiffProvider
implements Serializable {
    public static final String DIFF_REGEXP = "(^[0-9]+(,[0-9]+|)[d][0-9]+$)|(^[0-9]+(,[0-9]+|)[c][0-9]+(,[0-9]+|)$)|(^[0-9]+[a][0-9]+(,[0-9]+|)$)";
    private static final int BUFF_LENGTH = 1024;
    private String diffCmd;
    private transient Pattern pattern;
    static final long serialVersionUID = 4101521743158176210L;

    public CmdlineDiffProvider(String diffCmd) {
        this.diffCmd = diffCmd;
        try {
            this.pattern = Pattern.compile("(^[0-9]+(,[0-9]+|)[d][0-9]+$)|(^[0-9]+(,[0-9]+|)[c][0-9]+(,[0-9]+|)$)|(^[0-9]+[a][0-9]+(,[0-9]+|)$)");
        }
        catch (PatternSyntaxException resex) {
            // empty catch block
        }
    }

    public CmdlineDiffProvider() {
        this("diff {0} {1}");
    }

    public static CmdlineDiffProvider createDefault() {
        return new CmdlineDiffProvider();
    }

    public void setDiffCommand(String diffCmd) {
        this.diffCmd = diffCmd;
    }

    public String getDiffCommand() {
        return this.diffCmd;
    }

    private static boolean checkEmpty(String str, String element) {
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }

    public String getDisplayName() {
        return NbBundle.getMessage(CmdlineDiffProvider.class, (String)"displayName");
    }

    public String getShortDescription() {
        return NbBundle.getMessage(CmdlineDiffProvider.class, (String)"shortDescription");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Difference[] computeDiff(Reader r1, Reader r2) throws IOException {
        File f1 = null;
        File f2 = null;
        try {
            int length;
            f1 = FileUtil.normalizeFile((File)File.createTempFile("TempDiff".intern(), null));
            f2 = FileUtil.normalizeFile((File)File.createTempFile("TempDiff".intern(), null));
            FileWriter fw1 = new FileWriter(f1);
            FileWriter fw2 = new FileWriter(f2);
            char[] buffer = new char[1024];
            while ((length = r1.read(buffer)) > 0) {
                fw1.write(buffer, 0, length);
            }
            while ((length = r2.read(buffer)) > 0) {
                fw2.write(buffer, 0, length);
            }
            r1.close();
            r2.close();
            fw1.close();
            fw2.close();
            Difference[] arrdifference = this.createDiff(f1, f2);
            return arrdifference;
        }
        finally {
            if (f1 != null) {
                f1.delete();
            }
            if (f2 != null) {
                f2.delete();
            }
        }
    }

    public Difference[] computeDiff(FileObject fo1, FileObject fo2) throws IOException {
        File f1 = FileUtil.toFile((FileObject)fo1);
        File f2 = FileUtil.toFile((FileObject)fo2);
        if (f1 != null && f2 != null) {
            return this.createDiff(f1, f2);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Difference[] createDiff(File f1, File f2) throws IOException {
        String secondPath;
        String firstPath;
        final StringBuffer firstText = new StringBuffer();
        final StringBuffer secondText = new StringBuffer();
        if (this.pattern == null) {
            try {
                this.pattern = Pattern.compile("(^[0-9]+(,[0-9]+|)[d][0-9]+$)|(^[0-9]+(,[0-9]+|)[c][0-9]+(,[0-9]+|)$)|(^[0-9]+[a][0-9]+(,[0-9]+|)$)");
            }
            catch (PatternSyntaxException resex) {
                throw (IOException)ErrorManager.getDefault().annotate((Throwable)new IOException(), resex.getLocalizedMessage());
            }
        }
        this.diffCmd = this.diffCmd.replace("\"{0}\"", "{0}").replace("\"{1}\"", "{1}");
        if (Utilities.isWindows()) {
            firstPath = "\"" + f1.getAbsolutePath() + "\"";
            secondPath = "\"" + f2.getAbsolutePath() + "\"";
        } else {
            firstPath = f1.getAbsolutePath();
            secondPath = f2.getAbsolutePath();
        }
        final String cmd = MessageFormat.format(this.diffCmd, firstPath, secondPath);
        final Process[] p = new Process[1];
        final Object[] ret = new Object[1];
        Runnable cancellableProcessWrapper = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    int length;
                    ErrorManager.getDefault().log("#69616 CDP: executing: " + cmd);
                    Process[] arrprocess = p;
                    synchronized (arrprocess) {
                        p[0] = Runtime.getRuntime().exec(cmd);
                    }
                    InputStreamReader stdout = new InputStreamReader(p[0].getInputStream());
                    char[] buffer = new char[1024];
                    StringBuffer outBuffer = new StringBuffer();
                    ArrayList<Difference> differences = new ArrayList<Difference>();
                    while ((length = stdout.read(buffer)) > 0) {
                        for (int i = 0; i < length; ++i) {
                            if (buffer[i] == '\n') {
                                CmdlineDiffProvider.outputLine(outBuffer.toString(), CmdlineDiffProvider.this.pattern, differences, firstText, secondText);
                                outBuffer.delete(0, outBuffer.length());
                                continue;
                            }
                            if (buffer[i] == '\r') continue;
                            outBuffer.append(buffer[i]);
                        }
                    }
                    if (outBuffer.length() > 0) {
                        CmdlineDiffProvider.outputLine(outBuffer.toString(), CmdlineDiffProvider.this.pattern, differences, firstText, secondText);
                    }
                    CmdlineDiffProvider.setTextOnLastDifference(differences, firstText, secondText);
                    ret[0] = differences.toArray(new Difference[differences.size()]);
                }
                catch (IOException ioex) {
                    ret[0] = (IOException)ErrorManager.getDefault().annotate((Throwable)ioex, NbBundle.getMessage(CmdlineDiffProvider.class, (String)"runtimeError", (Object)cmd));
                }
            }
        };
        Thread t = new Thread(cancellableProcessWrapper, "Diff.exec()");
        t.start();
        try {
            t.join();
            Object[] arrobject = ret;
            synchronized (arrobject) {
                if (ret[0] instanceof IOException) {
                    throw (IOException)ret[0];
                }
                return (Difference[])ret[0];
            }
        }
        catch (InterruptedException e) {
            Process process = p[0];
            synchronized (process) {
                p[0].destroy();
            }
            throw new InterruptedIOException();
        }
    }

    public static void setTextOnLastDifference(List<Difference> differences, StringBuffer firstText, StringBuffer secondText) {
        if (differences.size() > 0) {
            String t2;
            String t1 = firstText.toString();
            if (t1.length() == 0) {
                t1 = null;
            }
            if ((t2 = secondText.toString()).length() == 0) {
                t2 = null;
            }
            Difference d = differences.remove(differences.size() - 1);
            differences.add(new Difference(d.getType(), d.getFirstStart(), d.getFirstEnd(), d.getSecondStart(), d.getSecondEnd(), t1, t2));
            firstText.delete(0, firstText.length());
            secondText.delete(0, secondText.length());
        }
    }

    public static void outputLine(String elements, Pattern pattern, List<Difference> differences, StringBuffer firstText, StringBuffer secondText) {
        int index = 0;
        int commaIndex = 0;
        int n1 = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        if (!pattern.matcher(elements).matches()) {
            if (elements.startsWith("< ")) {
                firstText.append(elements.substring(2) + "\n");
            }
            if (elements.startsWith("> ")) {
                secondText.append(elements.substring(2) + "\n");
            }
            return;
        }
        CmdlineDiffProvider.setTextOnLastDifference(differences, firstText, secondText);
        index = elements.indexOf(97);
        if (index >= 0) {
            try {
                n1 = Integer.parseInt(elements.substring(0, index));
                commaIndex = elements.indexOf(44, ++index);
                if (commaIndex < 0) {
                    String nStr = elements.substring(index, elements.length());
                    if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                        return;
                    }
                    n4 = n3 = Integer.parseInt(nStr);
                } else {
                    String nStr = elements.substring(index, commaIndex);
                    if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                        return;
                    }
                    n3 = Integer.parseInt(nStr);
                    nStr = elements.substring(commaIndex + 1, elements.length());
                    n4 = nStr == null || nStr.length() == 0 ? n3 : Integer.parseInt(nStr);
                }
            }
            catch (NumberFormatException e) {
                return;
            }
            differences.add(new Difference(1, n1, 0, n3, n4));
        } else {
            index = elements.indexOf(100);
            if (index >= 0) {
                commaIndex = elements.lastIndexOf(44, index);
                try {
                    String nStr;
                    if (commaIndex < 0) {
                        n2 = n1 = Integer.parseInt(elements.substring(0, index));
                    } else {
                        nStr = elements.substring(0, commaIndex);
                        if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                            return;
                        }
                        n1 = Integer.parseInt(nStr);
                        nStr = elements.substring(commaIndex + 1, index);
                        if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                            return;
                        }
                        n2 = Integer.parseInt(nStr);
                    }
                    nStr = elements.substring(index + 1, elements.length());
                    if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                        return;
                    }
                    n3 = Integer.parseInt(nStr);
                }
                catch (NumberFormatException e) {
                    return;
                }
                differences.add(new Difference(0, n1, n2, n3, 0));
            } else {
                index = elements.indexOf(99);
                if (index >= 0) {
                    commaIndex = elements.lastIndexOf(44, index);
                    try {
                        String nStr;
                        if (commaIndex < 0) {
                            n2 = n1 = Integer.parseInt(elements.substring(0, index));
                        } else {
                            nStr = elements.substring(0, commaIndex);
                            if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                                return;
                            }
                            n1 = Integer.parseInt(nStr);
                            nStr = elements.substring(commaIndex + 1, index);
                            if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                                return;
                            }
                            n2 = Integer.parseInt(nStr);
                        }
                        commaIndex = elements.indexOf(44, ++index);
                        if (commaIndex < 0) {
                            nStr = elements.substring(index, elements.length());
                            if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                                return;
                            }
                            n4 = n3 = Integer.parseInt(nStr);
                        } else {
                            nStr = elements.substring(index, commaIndex);
                            if (CmdlineDiffProvider.checkEmpty(nStr, elements)) {
                                return;
                            }
                            n3 = Integer.parseInt(nStr);
                            nStr = elements.substring(commaIndex + 1, elements.length());
                            n4 = nStr == null || nStr.length() == 0 ? n3 : Integer.parseInt(nStr);
                        }
                    }
                    catch (NumberFormatException e) {
                        return;
                    }
                    differences.add(new Difference(2, n1, n2, n3, n4));
                }
            }
        }
    }

}

