/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 *  org.openide.windows.Workspace
 */
package org.netbeans.modules.diff.builtin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import javax.accessibility.AccessibleContext;
import org.netbeans.api.diff.Diff;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.diff.builtin.DiffPresenter;
import org.netbeans.modules.diff.builtin.visualizer.DiffViewImpl;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.spi.diff.DiffProvider;
import org.netbeans.spi.diff.DiffVisualizer;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.Workspace;

public class DefaultDiff
extends Diff
implements Serializable {
    private boolean showDiffSelector = true;

    public String getDisplayName() {
        return NbBundle.getMessage(DefaultDiff.class, (String)"DefaultDiff.displayName");
    }

    @Override
    public Component createDiff(String name1, String title1, Reader r1, String name2, String title2, Reader r2, String MIMEType) throws IOException {
        DiffInfo diffInfo = new DiffInfo(name1, name2, title1, title2, MIMEType, this.showDiffSelector, false, r1, r2);
        DiffProvider provider = (DiffProvider)Lookup.getDefault().lookup(DiffProvider.class);
        try {
            Difference[] diffs = provider.computeDiff(diffInfo.createFirstReader(), diffInfo.createSecondReader());
            diffInfo.setInitialDiffs(diffs);
        }
        catch (IOException ioex) {
            // empty catch block
        }
        DiffPresenter diffPanel = new DiffPresenter(diffInfo);
        DiffTopComponent tp = new DiffTopComponent(diffPanel);
        diffInfo.setPresentingComponent(tp);
        diffPanel.setProvider(provider);
        diffPanel.setVisualizer((DiffVisualizer)Lookup.getDefault().lookup(DiffVisualizer.class));
        return tp;
    }

    @Override
    public DiffView createDiff(StreamSource s1, StreamSource s2) throws IOException {
        if (System.getProperty("netbeans.experimental.diff55") != null) {
            return new DiffViewImpl(s1, s2);
        }
        return new EditableDiffView(s1, s2);
    }

    public boolean isShowDiffSelector() {
        return this.showDiffSelector;
    }

    public void setShowDiffSelector(boolean showDiffSelector) {
        this.showDiffSelector = showDiffSelector;
    }

    private static void cpStream(Reader in, Writer out) throws IOException {
        int n;
        char[] buff = new char[1024];
        while ((n = in.read(buff)) > 0) {
            out.write(buff, 0, n);
        }
        in.close();
        out.close();
    }

    public static class DiffTopComponent
    extends TopComponent {
        public DiffTopComponent(Component c) {
            this.setLayout((LayoutManager)new BorderLayout());
            this.add(c, (Object)"Center");
            this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffTopComponent.class, (String)"ACSN_Diff_Top_Component"));
            this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffTopComponent.class, (String)"ACSD_Diff_Top_Component"));
            this.setName(c.getName());
        }

        public int getPersistenceType() {
            return 2;
        }

        protected String preferredID() {
            return "DiffTopComponent";
        }

        public void open(Workspace workspace) {
            super.open(workspace);
            this.requestActive();
        }
    }

    private static class DiffInfo
    extends DiffPresenter.Info {
        private String buffer1;
        private String buffer2;
        private Difference[] diffs;

        public DiffInfo(String name1, String name2, String title1, String title2, String mimeType, boolean chooseProviders, boolean chooseVisualizers, Reader r1, Reader r2) throws IOException {
            super(name1, name2, title1, title2, mimeType, chooseProviders, chooseVisualizers);
            StringWriter out1 = new StringWriter();
            StringWriter out2 = new StringWriter();
            DefaultDiff.cpStream(r1, out1);
            DefaultDiff.cpStream(r2, out2);
            this.buffer1 = out1.toString();
            this.buffer2 = out2.toString();
        }

        @Override
        public Reader createFirstReader() {
            return new StringReader(this.buffer1);
        }

        @Override
        public Reader createSecondReader() {
            return new StringReader(this.buffer2);
        }

        void setInitialDiffs(Difference[] diffs) {
            this.diffs = diffs;
        }

        @Override
        public Difference[] getInitialDifferences() {
            Difference[] diffs = this.diffs;
            this.diffs = null;
            return diffs;
        }
    }

}

