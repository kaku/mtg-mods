/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MarkBlockChain
 *  org.netbeans.editor.Utilities
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import org.netbeans.api.diff.Difference;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.diff.builtin.visualizer.editable.DecoratedEditorPane;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.modules.diff.builtin.visualizer.editable.LineNumbersActionsBar;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

class DiffViewManager
implements ChangeListener {
    private final EditableDiffView master;
    private final DiffContentPanel leftContentPanel;
    private final DiffContentPanel rightContentPanel;
    private boolean myScrollEvent;
    private int cachedDiffSerial;
    private DecoratedDifference[] decorationsCached = new DecoratedDifference[0];
    private HighLight[] secondHilitesCached = new HighLight[0];
    private HighLight[] firstHilitesCached = new HighLight[0];
    private final ScrollMapCached scrollMap;
    private final RequestProcessor.Task highlightComputeTask;
    private Dimension leftScrollBarPrefSize;
    private final Boolean[] smartScrollDisabled;
    private int rightHeightCached;
    private int leftHeightCached;

    public DiffViewManager(EditableDiffView master) {
        this.scrollMap = new ScrollMapCached();
        this.smartScrollDisabled = new Boolean[]{Boolean.FALSE};
        this.master = master;
        this.leftContentPanel = master.getEditorPane1();
        this.rightContentPanel = master.getEditorPane2();
        this.highlightComputeTask = new RequestProcessor("DiffViewHighlightsComputer", 1, true, false).create((Runnable)new HighlightsComputeTask());
    }

    void init() {
        this.initScrolling();
    }

    private void initScrolling() {
        this.rightContentPanel.getScrollPane().setVerticalScrollBarPolicy(20);
        this.leftContentPanel.getScrollPane().setVerticalScrollBarPolicy(20);
        this.leftContentPanel.getScrollPane().getVerticalScrollBar().getModel().addChangeListener(this);
        this.rightContentPanel.getScrollPane().getVerticalScrollBar().getModel().addChangeListener(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runWithSmartScrollingDisabled(Runnable runnable) {
        Boolean[] arrboolean = this.smartScrollDisabled;
        synchronized (arrboolean) {
            this.smartScrollDisabled[0] = true;
        }
        try {
            runnable.run();
        }
        catch (Exception e) {
            try {
                Logger.getLogger(DiffViewManager.class.getName()).log(Level.SEVERE, "", e);
            }
            catch (Throwable var4_5) {
                SwingUtilities.invokeLater(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        Boolean[] arrboolean = DiffViewManager.this.smartScrollDisabled;
                        synchronized (arrboolean) {
                            DiffViewManager.access$200((DiffViewManager)DiffViewManager.this)[0] = false;
                        }
                    }
                });
                throw var4_5;
            }
            SwingUtilities.invokeLater(new );
        }
        SwingUtilities.invokeLater(new );
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.rightContentPanel.getScrollPane().getVerticalScrollBar().isVisible()) {
            Dimension d = this.leftContentPanel.getScrollPane().getVerticalScrollBar().getSize();
            if (d.getHeight() > 0.0 && d.getWidth() > 0.0) {
                this.leftScrollBarPrefSize = d;
            }
            this.leftContentPanel.getScrollPane().getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
            this.leftContentPanel.getScrollPane().getVerticalScrollBar().setSize(new Dimension(0, 0));
            this.leftContentPanel.getScrollPane().getVerticalScrollBar().repaint();
        } else if (this.leftScrollBarPrefSize != null) {
            this.leftContentPanel.getScrollPane().getVerticalScrollBar().setPreferredSize(this.leftScrollBarPrefSize);
            this.leftContentPanel.getScrollPane().getVerticalScrollBar().setSize(this.leftScrollBarPrefSize);
            this.leftContentPanel.getScrollPane().getVerticalScrollBar().repaint();
        }
        JScrollBar leftScrollBar = this.leftContentPanel.getScrollPane().getVerticalScrollBar();
        JScrollBar rightScrollBar = this.rightContentPanel.getScrollPane().getVerticalScrollBar();
        boolean repaint = true;
        if (e.getSource() == this.leftContentPanel.getScrollPane().getVerticalScrollBar().getModel()) {
            int value = leftScrollBar.getValue();
            this.leftContentPanel.getActionsScrollPane().getVerticalScrollBar().setValue(value);
            if (this.myScrollEvent) {
                return;
            }
            this.myScrollEvent = true;
        } else {
            boolean doSmartScroll;
            int value = rightScrollBar.getValue();
            boolean valueChanged = value != this.rightContentPanel.getActionsScrollPane().getVerticalScrollBar().getValue();
            this.rightContentPanel.getActionsScrollPane().getVerticalScrollBar().setValue(value);
            if (this.myScrollEvent) {
                return;
            }
            this.myScrollEvent = true;
            Boolean[] arrboolean = this.smartScrollDisabled;
            synchronized (arrboolean) {
                doSmartScroll = this.smartScrollDisabled[0] == false;
            }
            if (doSmartScroll && valueChanged) {
                final Rectangle prevVisRect = this.rightContentPanel.getScrollPane().getViewport().getViewRect();
                repaint = false;
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffViewManager.this.smartScroll(true);
                        Rectangle visRect = DiffViewManager.this.rightContentPanel.getScrollPane().getViewport().getViewRect();
                        Boolean down = null;
                        if (visRect.y > prevVisRect.y) {
                            down = true;
                        } else if (visRect.y < prevVisRect.y) {
                            down = false;
                        }
                        DiffViewManager.this.master.updateCurrentDifference(down);
                        DiffViewManager.this.master.getMyDivider().repaint();
                    }
                });
            }
        }
        if (repaint) {
            this.master.getMyDivider().repaint();
        }
        this.myScrollEvent = false;
    }

    void scroll(boolean checkFileEdge) {
        this.myScrollEvent = true;
        this.smartScroll(checkFileEdge);
        this.master.getMyDivider().repaint();
        this.myScrollEvent = false;
    }

    EditableDiffView getMaster() {
        return this.master;
    }

    private void updateDifferences() {
        assert (EventQueue.isDispatchThread());
        int mds = this.master.getDiffSerial();
        int currentLeftHeight = this.getHeight(this.leftContentPanel.getEditorPane());
        int currentRightHeight = this.getHeight(this.rightContentPanel.getEditorPane());
        if (mds <= this.cachedDiffSerial && currentRightHeight == this.rightHeightCached && currentLeftHeight == this.leftHeightCached) {
            return;
        }
        this.rightHeightCached = currentRightHeight;
        this.leftHeightCached = currentLeftHeight;
        this.cachedDiffSerial = mds;
        this.computeDecorations();
        this.master.getEditorPane1().getLinesActions().repaint();
        this.master.getEditorPane2().getLinesActions().repaint();
        this.secondHilitesCached = new HighLight[0];
        this.firstHilitesCached = this.secondHilitesCached;
        this.highlightComputeTask.cancel();
        this.highlightComputeTask.schedule(0);
    }

    public DecoratedDifference[] getDecorations() {
        if (EventQueue.isDispatchThread()) {
            this.updateDifferences();
        }
        return this.decorationsCached;
    }

    public HighLight[] getSecondHighlights() {
        return this.secondHilitesCached;
    }

    public HighLight[] getFirstHighlights() {
        return this.firstHilitesCached;
    }

    private void computeFirstHighlights() {
        DecoratedDifference[] decorations;
        ArrayList<HighLight> hilites = new ArrayList<HighLight>();
        Document doc = this.leftContentPanel.getEditorPane().getDocument();
        for (DecoratedDifference dd : decorations = this.decorationsCached) {
            if (Thread.interrupted()) {
                return;
            }
            Difference diff = dd.getDiff();
            if (dd.getBottomLeft() == -1) continue;
            int start = DiffViewManager.getRowStartFromLineOffset(doc, diff.getFirstStart() > 0 ? diff.getFirstStart() - 1 : 0);
            if (this.isOneLineChange(diff)) {
                CorrectRowTokenizer firstSt = new CorrectRowTokenizer(diff.getFirstText());
                CorrectRowTokenizer secondSt = new CorrectRowTokenizer(diff.getSecondText());
                for (int i = diff.getSecondStart(); i <= diff.getSecondEnd(); ++i) {
                    String firstRow = firstSt.nextToken();
                    String secondRow = secondSt.nextToken();
                    List<HighLight> rowhilites = this.computeFirstRowHilites(start, firstRow, secondRow);
                    hilites.addAll(rowhilites);
                    start += firstRow.length() + 1;
                }
                continue;
            }
            int end = DiffViewManager.getRowStartFromLineOffset(doc, diff.getFirstEnd());
            if (end == -1) {
                end = doc.getLength();
            }
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setBackground(attrs, this.master.getColor(diff));
            attrs.addAttribute("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", Boolean.TRUE);
            hilites.add(new HighLight(start, end, attrs));
        }
        this.firstHilitesCached = hilites.toArray(new HighLight[hilites.size()]);
    }

    static int getRowStartFromLineOffset(Document doc, int lineIndex) {
        if (doc instanceof BaseDocument) {
            return Utilities.getRowStartFromLineOffset((BaseDocument)((BaseDocument)doc), (int)lineIndex);
        }
        Element element = doc.getDefaultRootElement();
        Element line = element.getElement(lineIndex);
        return line.getStartOffset();
    }

    private void computeSecondHighlights() {
        DecoratedDifference[] decorations;
        ArrayList<HighLight> hilites = new ArrayList<HighLight>();
        Document doc = this.rightContentPanel.getEditorPane().getDocument();
        for (DecoratedDifference dd : decorations = this.decorationsCached) {
            if (Thread.interrupted()) {
                return;
            }
            Difference diff = dd.getDiff();
            if (dd.getBottomRight() == -1) continue;
            int start = DiffViewManager.getRowStartFromLineOffset(doc, diff.getSecondStart() > 0 ? diff.getSecondStart() - 1 : 0);
            if (this.isOneLineChange(diff)) {
                CorrectRowTokenizer firstSt = new CorrectRowTokenizer(diff.getFirstText());
                CorrectRowTokenizer secondSt = new CorrectRowTokenizer(diff.getSecondText());
                for (int i = diff.getSecondStart(); i <= diff.getSecondEnd(); ++i) {
                    try {
                        String firstRow = firstSt.nextToken();
                        String secondRow = secondSt.nextToken();
                        List<HighLight> rowhilites = this.computeSecondRowHilites(start, firstRow, secondRow);
                        hilites.addAll(rowhilites);
                        start += secondRow.length() + 1;
                        continue;
                    }
                    catch (Exception e) {
                        // empty catch block
                    }
                }
                continue;
            }
            int end = DiffViewManager.getRowStartFromLineOffset(doc, diff.getSecondEnd());
            if (end == -1) {
                end = doc.getLength();
            }
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setBackground(attrs, this.master.getColor(diff));
            attrs.addAttribute("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", Boolean.TRUE);
            hilites.add(new HighLight(start, end, attrs));
        }
        this.secondHilitesCached = hilites.toArray(new HighLight[hilites.size()]);
    }

    private List<HighLight> computeFirstRowHilites(int rowStart, String left, String right) {
        Difference[] diffs;
        ArrayList<HighLight> hilites = new ArrayList<HighLight>(4);
        String leftRows = this.wordsToRows(left);
        String rightRows = this.wordsToRows(right);
        DiffProvider diffprovider = (DiffProvider)Lookup.getDefault().lookup(DiffProvider.class);
        if (diffprovider == null) {
            return hilites;
        }
        try {
            diffs = diffprovider.computeDiff(new StringReader(leftRows), new StringReader(rightRows));
        }
        catch (IOException e) {
            return hilites;
        }
        for (Difference diff : diffs) {
            if (diff.getType() == 1) continue;
            int start = this.rowOffset(leftRows, diff.getFirstStart());
            int end = this.rowOffset(leftRows, diff.getFirstEnd() + 1);
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setBackground(attrs, this.master.getColor(diff));
            hilites.add(new HighLight(rowStart + start, rowStart + end, attrs));
        }
        return hilites;
    }

    private List<HighLight> computeSecondRowHilites(int rowStart, String left, String right) {
        Difference[] diffs;
        ArrayList<HighLight> hilites = new ArrayList<HighLight>(4);
        String leftRows = this.wordsToRows(left);
        String rightRows = this.wordsToRows(right);
        DiffProvider diffprovider = (DiffProvider)Lookup.getDefault().lookup(DiffProvider.class);
        if (diffprovider == null) {
            return hilites;
        }
        try {
            diffs = diffprovider.computeDiff(new StringReader(leftRows), new StringReader(rightRows));
        }
        catch (IOException e) {
            return hilites;
        }
        for (Difference diff : diffs) {
            if (diff.getType() == 0) continue;
            int start = this.rowOffset(rightRows, diff.getSecondStart());
            int end = this.rowOffset(rightRows, diff.getSecondEnd() + 1);
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setBackground(attrs, this.master.getColor(diff));
            hilites.add(new HighLight(rowStart + start, rowStart + end, attrs));
        }
        return hilites;
    }

    private int rowOffset(String row, int rowIndex) {
        if (rowIndex == 1) {
            return 0;
        }
        int newLines = 0;
        for (int i = 0; i < row.length(); ++i) {
            char c = row.charAt(i);
            if (c != '\n') continue;
            ++newLines;
            if (--rowIndex != 1) continue;
            return i + 1 - newLines;
        }
        return row.length();
    }

    private String wordsToRows(String s) {
        StringBuilder sb = new StringBuilder(s.length() * 2);
        StringTokenizer st = new StringTokenizer(s, " \t\n[]{};:'\",.<>/?-=_+\\|~!@#$%^&*()", true);
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (token.length() == 0) continue;
            sb.append(token);
            sb.append('\n');
        }
        return sb.toString();
    }

    private boolean isOneLineChange(Difference diff) {
        return diff.getType() == 2 && diff.getFirstEnd() - diff.getFirstStart() == diff.getSecondEnd() - diff.getSecondStart();
    }

    private void computeDecorations() {
        Document document = this.master.getEditorPane2().getEditorPane().getDocument();
        View rootLeftView = Utilities.getDocumentView((JTextComponent)this.leftContentPanel.getEditorPane());
        View rootRightView = Utilities.getDocumentView((JTextComponent)this.rightContentPanel.getEditorPane());
        if (rootLeftView == null || rootRightView == null) {
            return;
        }
        Difference[] diffs = this.master.getDifferences();
        DecoratedDifference[] decorations = new DecoratedDifference[diffs.length];
        for (int i = 0; i < diffs.length; ++i) {
            Rectangle rightEndRect;
            Difference difference = diffs[i];
            DecoratedDifference dd = new DecoratedDifference(difference, this.canRollback(document, difference));
            Rectangle leftStartRect = this.getRectForView(this.leftContentPanel.getEditorPane(), rootLeftView, difference.getFirstStart() - 1, false);
            Rectangle leftEndRect = difference.getType() == 1 ? this.getRectForView(this.leftContentPanel.getEditorPane(), rootLeftView, difference.getFirstStart(), false) : this.getRectForView(this.leftContentPanel.getEditorPane(), rootLeftView, difference.getFirstEnd() - 1, true);
            Rectangle rightStartRect = this.getRectForView(this.rightContentPanel.getEditorPane(), rootRightView, difference.getSecondStart() - 1, false);
            Rectangle rectangle = rightEndRect = difference.getType() == 0 ? this.getRectForView(this.rightContentPanel.getEditorPane(), rootRightView, difference.getSecondStart(), false) : this.getRectForView(this.rightContentPanel.getEditorPane(), rootRightView, difference.getSecondEnd() - 1, true);
            if (leftStartRect == null || leftEndRect == null || rightStartRect == null || rightEndRect == null) {
                decorations = new DecoratedDifference[]{};
                break;
            }
            if (difference.getType() == 1) {
                dd.topRight = rightStartRect.y;
                dd.bottomRight = rightEndRect.y + rightEndRect.height;
                dd.topLeft = leftEndRect.y == 0 ? leftStartRect.y + leftStartRect.height : leftEndRect.y;
                dd.floodFill = true;
            } else if (difference.getType() == 0) {
                dd.topLeft = leftStartRect.y;
                dd.bottomLeft = leftEndRect.y + leftEndRect.height;
                dd.topRight = rightEndRect.y == 0 ? rightStartRect.y + rightStartRect.height : rightEndRect.y;
                dd.floodFill = true;
            } else {
                dd.topRight = rightStartRect.y;
                dd.bottomRight = rightEndRect.y + rightEndRect.height;
                dd.topLeft = leftStartRect.y;
                dd.bottomLeft = leftEndRect.y + leftEndRect.height;
                dd.floodFill = true;
            }
            decorations[i] = dd;
        }
        this.decorationsCached = decorations;
    }

    private Rectangle getRectForView(final JTextComponent comp, final View rootView, final int lineNumber, final boolean endOffset) {
        final Rectangle[] rect = new Rectangle[1];
        Utilities.runViewHierarchyTransaction((JTextComponent)comp, (boolean)true, (Runnable)new Runnable(){

            @Override
            public void run() {
                if (lineNumber == -1 || lineNumber >= rootView.getViewCount()) {
                    rect[0] = new Rectangle();
                    return;
                }
                View view = rootView.getView(lineNumber);
                try {
                    rect[0] = view == null ? null : comp.modelToView(endOffset ? view.getEndOffset() - 1 : view.getStartOffset());
                }
                catch (BadLocationException ex) {
                    // empty catch block
                }
            }
        });
        return rect[0];
    }

    private boolean canRollback(Document doc, Difference diff) {
        int end;
        int start;
        if (!(doc instanceof GuardedDocument)) {
            return true;
        }
        GuardedDocument document = (GuardedDocument)doc;
        if (diff.getType() == 0) {
            start = end = Utilities.getRowStartFromLineOffset((BaseDocument)document, (int)diff.getSecondStart());
        } else {
            start = Utilities.getRowStartFromLineOffset((BaseDocument)document, (int)(diff.getSecondStart() > 0 ? diff.getSecondStart() - 1 : 0));
            end = Utilities.getRowStartFromLineOffset((BaseDocument)document, (int)diff.getSecondEnd());
        }
        MarkBlockChain mbc = document.getGuardedBlockChain();
        return (mbc.compareBlock(start, end) & 1) == 0;
    }

    private void smartScroll(boolean checkFileEdge) {
        DiffContentPanel rightPane = this.master.getEditorPane2();
        DiffContentPanel leftPane = this.master.getEditorPane1();
        int[] map = this.scrollMap.getScrollMap(rightPane.getEditorPane().getSize().height, this.master.getDiffSerial());
        int rightOffet = rightPane.getScrollPane().getVerticalScrollBar().getValue();
        if (checkFileEdge && rightOffet == 0) {
            leftPane.getScrollPane().getVerticalScrollBar().setValue(0);
        } else {
            int halfScreen = rightPane.getScrollPane().getVerticalScrollBar().getVisibleAmount() / 2;
            if (checkFileEdge && (rightOffet += halfScreen) + halfScreen >= rightPane.getScrollPane().getVerticalScrollBar().getMaximum()) {
                rightOffet = map.length - 1;
            }
            if (rightOffet >= map.length) {
                return;
            }
            leftPane.getScrollPane().getVerticalScrollBar().setValue(map[rightOffet] - leftPane.getScrollPane().getVerticalScrollBar().getVisibleAmount() / 2);
        }
    }

    double getScrollFactor() {
        BoundedRangeModel m1 = this.leftContentPanel.getScrollPane().getVerticalScrollBar().getModel();
        BoundedRangeModel m2 = this.rightContentPanel.getScrollPane().getVerticalScrollBar().getModel();
        return ((double)m1.getMaximum() - (double)m1.getExtent()) / (double)(m2.getMaximum() - m2.getExtent());
    }

    void editorPainting(DecoratedEditorPane decoratedEditorPane) {
        if (!decoratedEditorPane.isFirst()) {
            JComponent mydivider = this.master.getMyDivider();
            mydivider.paint(mydivider.getGraphics());
        }
    }

    private int getHeight(final DecoratedEditorPane editorPane) {
        final int[] height = new int[1];
        editorPane.getDocument().render(new Runnable(){

            @Override
            public void run() {
                try {
                    Rectangle rec = editorPane.modelToView(editorPane.getDocument().getLength());
                    if (rec != null) {
                        height[0] = (int)(rec.getY() + rec.getHeight());
                    }
                }
                catch (BadLocationException ex) {
                    // empty catch block
                }
            }
        });
        return height[0];
    }

    private class HighlightsComputeTask
    implements Runnable {
        private int diffSerial;

        private HighlightsComputeTask() {
        }

        @Override
        public void run() {
            this.diffSerial = DiffViewManager.this.cachedDiffSerial;
            DiffViewManager.this.computeSecondHighlights();
            if (this.diffSerial != DiffViewManager.this.cachedDiffSerial) {
                return;
            }
            DiffViewManager.this.computeFirstHighlights();
            if (this.diffSerial == DiffViewManager.this.cachedDiffSerial) {
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffViewManager.this.master.getEditorPane1().fireHilitingChanged();
                        DiffViewManager.this.master.getEditorPane2().fireHilitingChanged();
                    }
                });
            }
        }

    }

    private class ScrollMapCached {
        private int rightPanelHeightCached;
        private int[] scrollMapCached;
        private int diffSerialCached;

        private ScrollMapCached() {
        }

        public synchronized int[] getScrollMap(int rightPanelHeight, int diffSerial) {
            if (rightPanelHeight != this.rightPanelHeightCached || this.diffSerialCached != diffSerial || this.scrollMapCached == null) {
                this.diffSerialCached = diffSerial;
                this.rightPanelHeightCached = rightPanelHeight;
                this.scrollMapCached = this.compute();
            }
            return this.scrollMapCached;
        }

        private int[] compute() {
            int[] scrollMap = new int[this.rightPanelHeightCached];
            if (this.rightPanelHeightCached == 0) {
                return scrollMap;
            }
            EditorUI editorUI = Utilities.getEditorUI((JTextComponent)DiffViewManager.this.leftContentPanel.getEditorPane());
            if (editorUI == null) {
                return scrollMap;
            }
            View rootLeftView = Utilities.getDocumentView((JTextComponent)DiffViewManager.this.leftContentPanel.getEditorPane());
            View rootRightView = Utilities.getDocumentView((JTextComponent)DiffViewManager.this.rightContentPanel.getEditorPane());
            if (rootLeftView == null || rootRightView == null) {
                return scrollMap;
            }
            DecoratedDifference[] diffs = DiffViewManager.this.getDecorations();
            scrollMap[0] = 0;
            scrollMap[this.rightPanelHeightCached - 1] = DiffViewManager.access$500((DiffViewManager)DiffViewManager.this).getEditorPane1().getEditorPane().getSize().height;
            int lastOffset = 0;
            boolean lastDelete = false;
            for (int i = 0; i < diffs.length; ++i) {
                DecoratedDifference ddiff = diffs[i];
                int topLeft = ddiff.getTopLeft();
                int topRight = ddiff.getTopRight();
                int bottomLeft = ddiff.getBottomLeft();
                int bottomRight = ddiff.getBottomRight();
                if (topRight >= scrollMap.length || bottomRight >= scrollMap.length) {
                    Logger.getLogger(DiffViewManager.class.getName()).log(Level.FINE, "Skipping temporary diff highlights");
                    break;
                }
                scrollMap[topRight] = topLeft;
                if (bottomLeft == -1) {
                    bottomLeft = topLeft;
                }
                if (bottomRight == -1) {
                    scrollMap[topRight] = topLeft;
                    this.interpolate(scrollMap, lastOffset, topRight);
                    lastOffset = Math.max(lastOffset, lastDelete ? (topRight + lastOffset) / 2 : topRight - 150);
                    int newLastOffset = this.rightPanelHeightCached - 1;
                    if (i < diffs.length - 1) {
                        newLastOffset = diffs[i + 1].topRight;
                        scrollMap[newLastOffset] = diffs[i + 1].topLeft;
                    }
                    scrollMap[topRight] = bottomLeft;
                    this.interpolate(scrollMap, topRight, newLastOffset);
                    newLastOffset = Math.min((topRight + newLastOffset) / 2, topRight + 150);
                    this.interpolate(scrollMap, lastOffset, newLastOffset);
                    lastOffset = newLastOffset;
                    lastDelete = true;
                    continue;
                }
                scrollMap[bottomRight] = bottomLeft;
                this.interpolate(scrollMap, lastOffset, topRight);
                this.interpolate(scrollMap, topRight, bottomRight);
                lastOffset = bottomRight;
                lastDelete = false;
            }
            this.interpolate(scrollMap, lastOffset, this.rightPanelHeightCached - 1);
            return scrollMap;
        }

        private void interpolate(int[] scrollMap, int start, int end) {
            if (end > start) {
                int rightHeight = end - start;
                int leftHeight = scrollMap[end] - scrollMap[start];
                for (int pos = 1; pos < end - start; ++pos) {
                    scrollMap[pos + start] = leftHeight * pos / rightHeight + scrollMap[start];
                }
            }
        }
    }

    private static class CorrectRowTokenizer {
        private final String s;
        private int idx;

        public CorrectRowTokenizer(String s) {
            this.s = s;
        }

        public String nextToken() {
            String token = null;
            for (int end = this.idx; end < this.s.length(); ++end) {
                if (this.s.charAt(end) != '\n') continue;
                token = this.s.substring(this.idx, end);
                this.idx = end + 1;
                break;
            }
            return token;
        }
    }

    public static class HighLight {
        private final int startOffset;
        private final int endOffset;
        private final AttributeSet attrs;

        public HighLight(int startOffset, int endOffset, AttributeSet attrs) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.attrs = attrs;
        }

        public int getStartOffset() {
            return this.startOffset;
        }

        public int getEndOffset() {
            return this.endOffset;
        }

        public AttributeSet getAttrs() {
            return this.attrs;
        }
    }

    public static class DecoratedDifference {
        private final Difference diff;
        private final boolean canRollback;
        private int topLeft;
        private int bottomLeft = -1;
        private int topRight;
        private int bottomRight = -1;
        private boolean floodFill;

        public DecoratedDifference(Difference difference, boolean canRollback) {
            this.diff = difference;
            this.canRollback = canRollback;
        }

        public boolean canRollback() {
            return this.canRollback;
        }

        public Difference getDiff() {
            return this.diff;
        }

        public int getTopLeft() {
            return this.topLeft;
        }

        public int getBottomLeft() {
            return this.bottomLeft;
        }

        public int getTopRight() {
            return this.topRight;
        }

        public int getBottomRight() {
            return this.bottomRight;
        }

        public boolean isFloodFill() {
            return this.floodFill;
        }
    }

    public static class DifferencePosition {
        private Difference diff;
        private boolean isStart;

        public DifferencePosition(Difference diff, boolean start) {
            this.diff = diff;
            this.isStart = start;
        }

        public Difference getDiff() {
            return this.diff;
        }

        public boolean isStart() {
            return this.isStart;
        }
    }

}

