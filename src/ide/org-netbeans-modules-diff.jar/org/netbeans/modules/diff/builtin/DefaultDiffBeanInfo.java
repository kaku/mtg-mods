/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.diff.builtin.DefaultDiff;
import org.openide.ErrorManager;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class DefaultDiffBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(DefaultDiff.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] desc;
        try {
            PropertyDescriptor showDiffSelector = new PropertyDescriptor("showDiffSelector", DefaultDiff.class);
            showDiffSelector.setDisplayName(NbBundle.getMessage(DefaultDiffBeanInfo.class, (String)"PROP_showDiffSelector"));
            showDiffSelector.setShortDescription(NbBundle.getMessage(DefaultDiffBeanInfo.class, (String)"HINT_showDiffSelector"));
            desc = new PropertyDescriptor[]{showDiffSelector};
        }
        catch (IntrospectionException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
            desc = null;
        }
        return desc;
    }

    @Override
    public int getDefaultPropertyIndex() {
        return 0;
    }

    @Override
    public Image getIcon(int iconKind) {
        switch (iconKind) {
            case 1: {
                return ImageUtilities.loadImage((String)"org/netbeans/modules/diff/diffSettingsIcon.gif", (boolean)true);
            }
        }
        return null;
    }
}

