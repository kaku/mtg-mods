/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.spi.diff.DiffVisualizer;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class EditableDiffVisualizer
extends DiffVisualizer {
    public String getDisplayName() {
        return NbBundle.getMessage(EditableDiffVisualizer.class, (String)"CTL_EditableDiffVisualizer_Name");
    }

    public String getShortDescription() {
        return NbBundle.getMessage(EditableDiffVisualizer.class, (String)"CTL_EditableDiffVisualizer_Desc");
    }

    @Override
    public Component createView(Difference[] diffs, String name1, String title1, Reader r1, String name2, String title2, Reader r2, String MIMEType) throws IOException {
        return this.createDiff(diffs, StreamSource.createSource(name1, title1, MIMEType, r1), StreamSource.createSource(name2, title2, MIMEType, r2)).getComponent();
    }

    @Override
    public DiffView createDiff(Difference[] diffs, StreamSource s1, StreamSource s2) throws IOException {
        EDVManager manager = new EDVManager(s1, s2);
        manager.init();
        return manager.getView();
    }

    private static class EDVManager
    implements PropertyChangeListener {
        private final StreamSource s1;
        private final StreamSource s2;
        private EditableDiffView view;
        private Action nextAction;
        private Action prevAction;

        public EDVManager(StreamSource s1, StreamSource s2) {
            this.s1 = s1;
            this.s2 = s2;
        }

        public EditableDiffView getView() {
            return this.view;
        }

        public void init() throws IOException {
            this.view = new EditableDiffView(this.s1, this.s2);
            this.view.addPropertyChangeListener(this);
            JComponent component = this.view.getComponent();
            JToolBar toolbar = new JToolBar();
            toolbar.setBorder(BorderFactory.createEmptyBorder());
            toolbar.setRollover(true);
            this.nextAction = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    EDVManager.this.onNext();
                }
            };
            this.nextAction.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/diff/builtin/visualizer/editable/diff-next.png", (boolean)false));
            JButton nextButton = new JButton(this.nextAction);
            nextButton.setMargin(new Insets(2, 2, 2, 2));
            toolbar.add(nextButton);
            this.prevAction = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    EDVManager.this.onPrev();
                }
            };
            this.prevAction.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/diff/builtin/visualizer/editable/diff-prev.png", (boolean)false));
            JButton prevButton = new JButton(this.prevAction);
            prevButton.setMargin(new Insets(2, 2, 2, 2));
            toolbar.add(prevButton);
            component.putClientProperty("DiffPresenter.toolbarPanel", toolbar);
            component.getActionMap().put("jumpNext", this.nextAction);
            component.getActionMap().put("jumpPrev", this.prevAction);
            this.refreshComponents();
        }

        private void onPrev() {
            int idx = this.view.getCurrentDifference();
            if (idx > 0) {
                this.view.setCurrentDifference(idx - 1);
            }
        }

        private void onNext() {
            int idx = this.view.getCurrentDifference();
            if (idx < this.view.getDifferenceCount() - 1) {
                this.view.setCurrentDifference(idx + 1);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.refreshComponents();
        }

        private void refreshComponents() {
            this.nextAction.setEnabled(this.view.getCurrentDifference() < this.view.getDifferenceCount() - 1);
            this.prevAction.setEnabled(this.view.getCurrentDifference() > 0);
        }

    }

}

