/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.cmdline.CmdlineDiffProvider;

public class Patch
extends Reader {
    private static final int CONTEXT_DIFF = 0;
    private static final int NORMAL_DIFF = 1;
    private static final int UNIFIED_DIFF = 2;
    private Difference[] diffs;
    private PushbackReader source;
    private int currDiff = 0;
    private int line = 1;
    private String newLine = null;
    private StringBuffer buff = new StringBuffer();
    private static final String CONTEXT_MARK1B = "*** ";
    private static final String CONTEXT_MARK2B = "--- ";
    private static final String CONTEXT_MARK_DELIMETER = ",";
    private static final String DIFFERENCE_DELIMETER = "***************";
    private static final String LINE_PREP_ADD = "+ ";
    private static final String LINE_PREP_REMOVE = "- ";
    private static final String LINE_PREP_CHANGE = "! ";
    private static final String UNIFIED_MARK = "@@";
    private static final String UNIFIED_MARK1 = "--- ";
    private static final String LINE_PREP_UNIF_ADD = "+";
    private static final String LINE_PREP_UNIF_REMOVE = "-";

    private Patch(Difference[] diffs, Reader source) {
        this.diffs = diffs;
        this.source = new PushbackReader(new BufferedReader(source), 1);
    }

    public static Reader apply(Difference[] diffs, Reader source) {
        return new Patch(diffs, source);
    }

    public static FileDifferences[] parse(Reader source) throws IOException {
        ArrayList<FileDifferences> fileDifferences = new ArrayList<FileDifferences>();
        SinglePatchReader patchReader = new SinglePatchReader(source);
        int[] diffType = new int[1];
        String[] fileName = new String[2];
        while (patchReader.hasNextPatch(diffType, fileName)) {
            Difference[] diffs = null;
            switch (diffType[0]) {
                case 0: {
                    diffs = Patch.parseContextDiff(patchReader);
                    break;
                }
                case 2: {
                    diffs = Patch.parseUnifiedDiff(patchReader);
                    break;
                }
                case 1: {
                    diffs = Patch.parseNormalDiff(patchReader);
                }
            }
            if (diffs == null) continue;
            fileDifferences.add(new FileDifferences(fileName[0], fileName[1], diffs));
        }
        return fileDifferences.toArray(new FileDifferences[fileDifferences.size()]);
    }

    @Override
    public int read(char[] cbuf, int off, int length) throws IOException {
        int ret;
        if (this.buff.length() < length) {
            this.doRetrieve(length - this.buff.length());
        }
        if ((ret = Math.min(this.buff.length(), length)) == 0) {
            return -1;
        }
        String retStr = this.buff.substring(0, ret);
        char[] retChars = retStr.toCharArray();
        System.arraycopy(retChars, 0, cbuf, off, ret);
        this.buff.delete(0, ret);
        return ret;
    }

    @Override
    public void close() throws IOException {
        if (this.currDiff < this.diffs.length) {
            throw new IOException("There are " + (this.diffs.length - this.currDiff) + " pending hunks!");
        }
        this.source.close();
    }

    private void doRetrieve(int length) throws IOException {
        int size = 0;
        while (size < length) {
            if (this.currDiff < this.diffs.length && (1 == this.diffs[this.currDiff].getType() && this.line == this.diffs[this.currDiff].getFirstStart() + 1 || 1 != this.diffs[this.currDiff].getType() && this.line == this.diffs[this.currDiff].getFirstStart())) {
                if (this.compareText(this.source, this.diffs[this.currDiff].getFirstText())) {
                    String text = Patch.convertNewLines(this.diffs[this.currDiff].getSecondText(), this.newLine);
                    this.buff.append(text);
                    ++this.currDiff;
                } else {
                    throw new IOException("Patch not applicable.");
                }
            }
            StringBuffer newLineBuffer = null;
            if (this.newLine == null) {
                newLineBuffer = new StringBuffer();
            }
            String lineStr = Patch.readLine(this.source, newLineBuffer);
            if (newLineBuffer != null) {
                this.newLine = newLineBuffer.toString();
            }
            if (lineStr == null) break;
            this.buff.append(lineStr);
            this.buff.append(this.newLine);
            ++this.line;
        }
    }

    private static String readLine(PushbackReader r, StringBuffer nl) throws IOException {
        StringBuffer line = new StringBuffer();
        int ic = r.read();
        if (ic == -1) {
            return null;
        }
        char c = (char)ic;
        while (c != '\n' && c != '\r') {
            line.append(c);
            ic = r.read();
            if (ic == -1) break;
            c = (char)ic;
        }
        if (nl != null) {
            nl.append(c);
        }
        if (c == '\r') {
            try {
                ic = r.read();
                if (ic != -1) {
                    c = (char)ic;
                    if (c != '\n') {
                        r.unread(c);
                    } else if (nl != null) {
                        nl.append(c);
                    }
                }
            }
            catch (IOException ioex) {
                // empty catch block
            }
        }
        return line.toString();
    }

    private static String convertNewLines(String text, String newLine) {
        if (text == null) {
            return "";
        }
        if (newLine == null) {
            return text;
        }
        StringBuffer newText = new StringBuffer();
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c == '\n') {
                newText.append(newLine);
                continue;
            }
            if (c == '\r') {
                if (i + 1 >= text.length() || text.charAt(i + 1) != '\n') continue;
                ++i;
                newText.append(newLine);
                continue;
            }
            newText.append(c);
        }
        return newText.toString();
    }

    private boolean compareText(PushbackReader source, String text) throws IOException {
        int n;
        if (text == null || text.length() == 0) {
            return true;
        }
        text = this.adjustTextNL(text);
        char[] chars = new char[text.length()];
        int pos = 0;
        String readStr = "";
        do {
            if ((n = source.read(chars, 0, chars.length - pos)) > 0) {
                pos += n;
                readStr = readStr + new String(chars, 0, n);
            }
            if (readStr.endsWith("\r")) {
                try {
                    char c = (char)source.read();
                    if (c != '\n') {
                        source.unread(c);
                    } else {
                        readStr = readStr + c;
                    }
                }
                catch (IOException ioex) {
                    // empty catch block
                }
            }
            readStr = this.adjustTextNL(readStr);
            pos = readStr.length();
        } while (n > 0 && pos < chars.length);
        readStr.getChars(0, readStr.length(), chars, 0);
        this.line += Patch.numChars('\n', chars);
        return readStr.equals(text);
    }

    private String adjustTextNL(String text) {
        text = text.replace("\r\n", "\n");
        text = text.replace("\n\r", "\n");
        text = text.replace("\r", "\n");
        return text;
    }

    private static int numChars(char c, char[] chars) {
        int n = 0;
        for (int i = 0; i < chars.length; ++i) {
            if (chars[i] != c) continue;
            ++n;
        }
        return n;
    }

    private static Difference[] parseContextDiff(Reader in) throws IOException {
        BufferedReader br = new BufferedReader(in);
        ArrayList<Difference> diffs = new ArrayList<Difference>();
        String line = null;
        do {
            if (line == null || !"***************".equals(line)) {
                while ((line = br.readLine()) != null && !"***************".equals(line)) {
                }
            }
            int[] firstInterval = new int[2];
            line = br.readLine();
            if (line == null || !line.startsWith("*** ")) continue;
            try {
                Patch.readNums(line, "*** ".length(), firstInterval);
            }
            catch (NumberFormatException nfex) {
                throw new IOException(nfex.getLocalizedMessage());
            }
            ArrayList<Object> firstChanges = new ArrayList<Object>();
            line = Patch.fillChanges(firstInterval, br, "--- ", firstChanges);
            int[] secondInterval = new int[2];
            if (line == null || !line.startsWith("--- ")) continue;
            try {
                Patch.readNums(line, "--- ".length(), secondInterval);
            }
            catch (NumberFormatException nfex) {
                throw new IOException(nfex.getLocalizedMessage());
            }
            ArrayList<Object> secondChanges = new ArrayList<Object>();
            line = Patch.fillChanges(secondInterval, br, "***************", secondChanges);
            if (!Patch.changesCountInvariant(firstChanges, secondChanges)) {
                throw new IOException("Diff file format error. Number of new and old file changes in one hunk must be same!");
            }
            Patch.mergeChanges(firstInterval, secondInterval, firstChanges, secondChanges, diffs);
        } while (line != null);
        return diffs.toArray(new Difference[diffs.size()]);
    }

    private static boolean changesCountInvariant(List<Object> changes1, List<Object> changes2) {
        int i1 = 0;
        Iterator<Object> it = changes1.iterator();
        while (it.hasNext()) {
            int[] ints = (int[])it.next();
            if (ints[2] == 2) {
                ++i1;
            }
            String skip = (String)it.next();
        }
        int i2 = 0;
        it = changes2.iterator();
        while (it.hasNext()) {
            int[] ints = (int[])it.next();
            if (ints[2] == 2) {
                ++i2;
            }
            String skip = (String)it.next();
        }
        return i1 == i2;
    }

    private static void readNums(String str, int off, int[] values) throws NumberFormatException {
        int end = str.indexOf(",", off);
        if (end <= 0) {
            throw new NumberFormatException("Missing comma.");
        }
        values[0] = Integer.parseInt(str.substring(off, end).trim());
        off = end + 1;
        end = str.indexOf(32, off);
        if (end <= 0) {
            throw new NumberFormatException("Missing final space.");
        }
        values[1] = Integer.parseInt(str.substring(off, end).trim());
    }

    private static String fillChanges(int[] interval, BufferedReader br, String untilStartsWith, List<Object> changes) throws IOException {
        String line = br.readLine();
        for (int pos = interval[0]; pos <= interval[1] && line != null && !line.startsWith(untilStartsWith); ++pos) {
            StringBuffer changeText;
            int[] changeInterval;
            if (line.startsWith("+ ")) {
                changeInterval = new int[3];
                changeInterval[0] = pos;
                changeInterval[2] = 1;
                changeText = new StringBuffer();
                changeText.append(line.substring("+ ".length()));
                changeText.append('\n');
                while ((line = br.readLine()) != null && line.startsWith("+ ")) {
                    changeText.append(line.substring("+ ".length()));
                    changeText.append('\n');
                    ++pos;
                }
                changeInterval[1] = pos;
                changes.add(changeInterval);
                changes.add(changeText.toString());
                continue;
            }
            if (line.startsWith("- ")) {
                changeInterval = new int[3];
                changeInterval[0] = pos;
                changeInterval[2] = 0;
                changeText = new StringBuffer();
                changeText.append(line.substring("- ".length()));
                changeText.append('\n');
                while ((line = br.readLine()) != null && line.startsWith("- ")) {
                    changeText.append(line.substring("- ".length()));
                    changeText.append('\n');
                    ++pos;
                }
                changeInterval[1] = pos;
                changes.add(changeInterval);
                changes.add(changeText.toString());
                continue;
            }
            if (line.startsWith("! ")) {
                changeInterval = new int[3];
                changeInterval[0] = pos;
                changeInterval[2] = 2;
                changeText = new StringBuffer();
                changeText.append(line.substring("! ".length()));
                changeText.append('\n');
                while ((line = br.readLine()) != null && line.startsWith("! ")) {
                    changeText.append(line.substring("! ".length()));
                    changeText.append('\n');
                    ++pos;
                }
                changeInterval[1] = pos;
                changes.add(changeInterval);
                changes.add(changeText.toString());
                continue;
            }
            line = br.readLine();
        }
        return line;
    }

    private static void mergeChanges(int[] firstInterval, int[] secondInterval, List firstChanges, List secondChanges, List<Difference> diffs) {
        int n1 = firstChanges.size();
        int n2 = secondChanges.size();
        int firstToSecondIntervalShift = secondInterval[0] - firstInterval[0];
        int p2 = 0;
        int p1 = 0;
        while (p1 < n1 || p2 < n2) {
            int[] interval1;
            int[] interval;
            int[] interval2;
            boolean isAddRemove = true;
            while (isAddRemove && p1 < n1) {
                interval = (int[])firstChanges.get(p1);
                if (p2 < n2 && interval[0] + firstToSecondIntervalShift > (interval2 = (int[])secondChanges.get(p2))[0]) break;
                isAddRemove = interval[2] == 1 || interval[2] == 0;
                if (!isAddRemove) continue;
                if (interval[2] == 1) {
                    diffs.add(new Difference(interval[2], interval[0] - 1, 0, interval[0] + firstToSecondIntervalShift, interval[1] + firstToSecondIntervalShift, (String)firstChanges.get(p1 + 1), ""));
                    firstToSecondIntervalShift += interval[1] - interval[0] + 1;
                } else {
                    diffs.add(new Difference(interval[2], interval[0], interval[1], interval[0] + firstToSecondIntervalShift - 1, 0, (String)firstChanges.get(p1 + 1), ""));
                    firstToSecondIntervalShift -= interval[1] - interval[0] + 1;
                }
                p1 += 2;
            }
            isAddRemove = true;
            while (isAddRemove && p2 < n2) {
                interval = (int[])secondChanges.get(p2);
                isAddRemove = interval[2] == 1 || interval[2] == 0;
                if (!isAddRemove) continue;
                if (interval[2] == 1) {
                    diffs.add(new Difference(interval[2], interval[0] - firstToSecondIntervalShift - 1, 0, interval[0], interval[1], "", (String)secondChanges.get(p2 + 1)));
                    firstToSecondIntervalShift += interval[1] - interval[0] + 1;
                } else {
                    diffs.add(new Difference(interval[2], interval[0] - firstToSecondIntervalShift, interval[1] - firstToSecondIntervalShift, interval[0] - 1, 0, "", (String)secondChanges.get(p2 + 1)));
                    firstToSecondIntervalShift -= interval[1] - interval[0] + 1;
                }
                p2 += 2;
            }
            if (p1 >= n1 || p2 >= n2 || (interval1 = (int[])firstChanges.get(p1))[2] != 2) continue;
            interval2 = (int[])secondChanges.get(p2);
            diffs.add(new Difference(interval1[2], interval1[0], interval1[1], interval2[0], interval2[1], (String)firstChanges.get(p1 + 1), (String)secondChanges.get(p2 + 1)));
            p1 += 2;
            p2 += 2;
            firstToSecondIntervalShift += interval2[1] - interval2[0] - (interval1[1] - interval1[0]);
        }
    }

    private static Difference[] parseUnifiedDiff(Reader in) throws IOException {
        BufferedReader br = new BufferedReader(in);
        ArrayList<Difference> diffs = new ArrayList<Difference>();
        String line = null;
        do {
            if (!(line != null && line.startsWith("@@") && line.length() > "@@".length() && line.endsWith("@@") || (line = br.readLine()) == null)) {
                continue;
            }
            if (line != null) {
                int[] intervals = new int[4];
                try {
                    Patch.readUnifiedNums(line, "@@".length(), intervals);
                }
                catch (NumberFormatException nfex) {
                    IOException ioex = new IOException("Can not parse: " + line);
                    ioex.initCause(nfex);
                    throw ioex;
                }
                line = Patch.fillUnidifChanges(intervals, br, diffs);
            }
            if (line == null) break;
        } while (true);
        return diffs.toArray(new Difference[diffs.size()]);
    }

    private static void readUnifiedNums(String str, int off, int[] values) throws NumberFormatException {
        while (str.charAt(off) == ' ' || str.charAt(off) == '-') {
            ++off;
        }
        int end = str.indexOf(",", off);
        if (end <= 0) {
            throw new NumberFormatException("Missing comma.");
        }
        values[0] = Integer.parseInt(str.substring(off, end).trim());
        off = end + 1;
        end = str.indexOf(32, off);
        if (end <= 0) {
            throw new NumberFormatException("Missing middle space.");
        }
        values[1] = Integer.parseInt(str.substring(off, end).trim());
        off = end + 1;
        while (str.charAt(off) == ' ' || str.charAt(off) == '+') {
            ++off;
        }
        end = str.indexOf(",", off);
        if (end <= 0) {
            throw new NumberFormatException("Missing second comma.");
        }
        values[2] = Integer.parseInt(str.substring(off, end).trim());
        off = end + 1;
        end = str.indexOf(32, off);
        if (end <= 0) {
            throw new NumberFormatException("Missing final space.");
        }
        values[3] = Integer.parseInt(str.substring(off, end).trim());
        int[] arrn = values;
        arrn[1] = arrn[1] + (values[0] - 1);
        int[] arrn2 = values;
        arrn2[3] = arrn2[3] + (values[2] - 1);
    }

    private static String fillUnidifChanges(int[] interval, BufferedReader br, List<Difference> diffs) throws IOException {
        String line = br.readLine();
        int pos1 = interval[0];
        int pos2 = interval[2];
        while (line != null && pos1 <= interval[1] && pos2 <= interval[3]) {
            StringBuffer changeText;
            int begin;
            Difference diff;
            Difference previousDiff;
            if (line.startsWith("+")) {
                begin = pos2;
                changeText = new StringBuffer();
                changeText.append(line.substring("+".length()));
                changeText.append('\n');
                do {
                    line = br.readLine();
                    ++pos2;
                    if (!line.startsWith("+")) break;
                    changeText.append(line.substring("+".length()));
                    changeText.append('\n');
                } while (true);
                diff = null;
                if (diffs.size() > 0 && 0 == (previousDiff = diffs.get(diffs.size() - 1)).getType() && previousDiff.getFirstEnd() == pos1 - 1) {
                    diff = new Difference(2, previousDiff.getFirstStart(), previousDiff.getFirstEnd(), begin, pos2 - 1, previousDiff.getFirstText(), changeText.toString());
                    diffs.remove(diffs.size() - 1);
                }
                if (diff == null) {
                    diff = new Difference(1, pos1 - 1, 0, begin, pos2 - 1, null, changeText.toString());
                }
                diffs.add(diff);
                continue;
            }
            if (line.startsWith("-")) {
                begin = pos1;
                changeText = new StringBuffer();
                changeText.append(line.substring("-".length()));
                changeText.append('\n');
                do {
                    line = br.readLine();
                    ++pos1;
                    if (!line.startsWith("-")) break;
                    changeText.append(line.substring("-".length()));
                    changeText.append('\n');
                } while (true);
                diff = null;
                if (diffs.size() > 0 && 1 == (previousDiff = diffs.get(diffs.size() - 1)).getType() && previousDiff.getSecondEnd() == pos2 - 1) {
                    diff = new Difference(2, begin, pos1 - 1, previousDiff.getFirstStart(), previousDiff.getFirstEnd(), changeText.toString(), previousDiff.getFirstText());
                    diffs.remove(diffs.size() - 1);
                }
                if (diff == null) {
                    diff = new Difference(0, begin, pos1 - 1, pos2 - 1, 0, changeText.toString(), null);
                }
                diffs.add(diff);
                continue;
            }
            line = br.readLine();
            ++pos1;
            ++pos2;
        }
        return line;
    }

    private static Difference[] parseNormalDiff(Reader in) throws IOException {
        String line;
        Pattern normRegexp;
        try {
            normRegexp = Pattern.compile("(^[0-9]+(,[0-9]+|)[d][0-9]+$)|(^[0-9]+(,[0-9]+|)[c][0-9]+(,[0-9]+|)$)|(^[0-9]+[a][0-9]+(,[0-9]+|)$)");
        }
        catch (PatternSyntaxException rsex) {
            normRegexp = null;
        }
        StringBuffer firstText = new StringBuffer();
        StringBuffer secondText = new StringBuffer();
        BufferedReader br = new BufferedReader(in);
        ArrayList<Difference> diffs = new ArrayList<Difference>();
        while ((line = br.readLine()) != null) {
            CmdlineDiffProvider.outputLine(line, normRegexp, diffs, firstText, secondText);
        }
        CmdlineDiffProvider.setTextOnLastDifference(diffs, firstText, secondText);
        return diffs.toArray(new Difference[diffs.size()]);
    }

    public static class FileDifferences {
        private String fileName;
        private String indexName;
        private Difference[] diffs;

        public FileDifferences(String fileName, String indexName, Difference[] diffs) {
            this.fileName = fileName;
            this.diffs = diffs;
            this.indexName = indexName;
        }

        public final String getFileName() {
            return this.fileName;
        }

        public final String getIndexName() {
            return this.indexName;
        }

        public final Difference[] getDifferences() {
            return this.diffs;
        }
    }

    private static class SinglePatchReader
    extends Reader {
        private static final int BUFF_SIZE = 512;
        private PushbackReader in;
        private char[] buffer = new char[512];
        private int buffLength = 0;
        private int buffPos = 0;
        private boolean isAtEndOfPatch = false;
        private static final String FILE_INDEX = "Index: ";

        public SinglePatchReader(Reader in) {
            this.in = new PushbackReader(in, 512);
        }

        @Override
        public int read(char[] values, int offset, int length) throws IOException {
            int totRead = 0;
            while (length > 0) {
                int buffCopyLength;
                if (length < this.buffLength) {
                    buffCopyLength = length;
                    length = 0;
                } else if (this.buffLength > 0) {
                    buffCopyLength = this.buffLength;
                    length -= this.buffLength;
                } else if (this.isAtEndOfPatch) {
                    length = 0;
                    buffCopyLength = -1;
                } else {
                    this.buffLength = this.readTillEndOfPatch(this.buffer);
                    this.buffPos = 0;
                    if (this.buffLength <= 0) {
                        buffCopyLength = -1;
                    } else {
                        buffCopyLength = Math.min(length, this.buffLength);
                        length -= buffCopyLength;
                    }
                }
                if (buffCopyLength > 0) {
                    System.arraycopy(this.buffer, this.buffPos, values, offset, buffCopyLength);
                    offset += buffCopyLength;
                    this.buffLength -= buffCopyLength;
                    this.buffPos += buffCopyLength;
                    totRead += buffCopyLength;
                    continue;
                }
                length = 0;
            }
            if (totRead == 0) {
                totRead = -1;
            }
            return totRead;
        }

        private int readTillEndOfPatch(char[] buffer) throws IOException {
            int length = this.in.read(buffer);
            String input = new String(buffer);
            int end = 0;
            if (input.startsWith("Index: ") || (end = input.indexOf("\nIndex: ")) >= 0) {
                this.isAtEndOfPatch = true;
            } else {
                end = input.lastIndexOf(10);
                if (end >= 0) {
                    ++end;
                }
            }
            if (end >= 0 && end < length) {
                this.in.unread(buffer, end, length - end);
                length = end;
            }
            if (end == 0) {
                length = -1;
            }
            return length;
        }

        @Override
        public void close() throws IOException {
        }

        private boolean hasNextPatch(int[] diffType, String[] fileName) throws IOException {
            Pattern normRegexp;
            int length;
            this.isAtEndOfPatch = false;
            PushbackReader patchSource = this.in;
            char[] buff = new char["***************".length()];
            boolean contextBeginDetected = false;
            try {
                normRegexp = Pattern.compile("(^[0-9]+(,[0-9]+|)[d][0-9]+$)|(^[0-9]+(,[0-9]+|)[c][0-9]+(,[0-9]+|)$)|(^[0-9]+[a][0-9]+(,[0-9]+|)$)");
            }
            catch (PatternSyntaxException rsex) {
                normRegexp = null;
            }
            while ((length = patchSource.read(buff)) > 0) {
                int r;
                char c;
                StringBuffer name;
                String input = new String(buff, 0, length);
                int nln = input.indexOf(10);
                int nlr = input.indexOf(13);
                int nl = nln < 0 ? nlr : nln;
                if (nl >= 0) {
                    input = nln > 0 && nln == nlr + 1 ? input.substring(0, nl - 1) : input.substring(0, nl);
                    if (nl + 1 < length) {
                        patchSource.unread(buff, nl + 1, length - (nl + 1));
                        length = nl + 1;
                    }
                }
                if (input.equals("***************")) {
                    diffType[0] = 0;
                    patchSource.unread(buff, 0, length);
                    return true;
                }
                if (input.startsWith("@@ ")) {
                    diffType[0] = 2;
                    patchSource.unread(buff, 0, length);
                    return true;
                }
                if (input.startsWith("Index: ")) {
                    name = new StringBuffer(input.substring("Index: ".length()));
                    if (nl < 0) {
                        int r2;
                        char c2;
                        while ((c2 = (char)(r2 = patchSource.read())) != '\n' && r2 != -1 && r2 != 13) {
                            name.append(c2);
                        }
                    }
                    fileName[1] = name.toString();
                    continue;
                }
                if (input.startsWith("*** ") || !contextBeginDetected && input.startsWith("--- ")) {
                    if (input.startsWith("*** ")) {
                        contextBeginDetected = true;
                        name = new StringBuffer(input.substring("*** ".length()));
                    } else {
                        name = new StringBuffer(input.substring("--- ".length()));
                    }
                    String sname = name.toString();
                    int spaceIndex = sname.indexOf(9);
                    if (spaceIndex > 0) {
                        name = name.delete(spaceIndex, name.length());
                    }
                    if (nl < 0) {
                        int r3 = 0;
                        char c3 = '\u0000';
                        if (spaceIndex < 0) {
                            do {
                                r3 = patchSource.read();
                                char c4 = (char)r3;
                                c3 = c4;
                                if (c4 == '\n' || c3 == 13 || c3 == 9 || r3 == -1) break;
                                name.append(c3);
                            } while (true);
                        }
                        if (c3 != '\n' && c3 != '\r' && r3 != -1) {
                            char c5;
                            do {
                                r3 = patchSource.read();
                                c5 = (char)r3;
                                c3 = c5;
                            } while (c5 != '\n' && c3 != '\r' && r3 != -1);
                        }
                        if (c3 == '\r' && (r3 = patchSource.read()) != -1 && (c3 = (char)((char)r3)) != '\n') {
                            patchSource.unread(c3);
                        }
                    }
                    fileName[0] = name.toString();
                    continue;
                }
                if (normRegexp != null && normRegexp.matcher(input).matches()) {
                    diffType[0] = 1;
                    patchSource.unread(buff, 0, length);
                    return true;
                }
                if (nl >= 0) continue;
                while ((c = (char)(r = patchSource.read())) != '\n' && c != '\r' && r != -1) {
                }
                if (c != '\r' || (r = patchSource.read()) == -1 || (c = (char)r) == '\n') continue;
                patchSource.unread(c);
            }
            return false;
        }
    }

}

