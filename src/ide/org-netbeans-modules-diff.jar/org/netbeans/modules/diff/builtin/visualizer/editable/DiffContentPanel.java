/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeEvent
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeListener
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.AWTKeyStroke;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.text.AttributeSet;
import javax.swing.text.EditorKit;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.visualizer.editable.DecoratedEditorPane;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffViewManager;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.modules.diff.builtin.visualizer.editable.LineNumbersActionsBar;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

class DiffContentPanel
extends JPanel
implements HighlightsContainer,
Lookup.Provider {
    private final EditableDiffView master;
    private final boolean isFirst;
    private final DecoratedEditorPane editorPane;
    private JScrollPane scrollPane;
    private final LineNumbersActionsBar linesActions;
    private final JScrollPane actionsScrollPane;
    private Difference[] currentDiff;
    private final List<HighlightsChangeListener> listeners = new ArrayList<HighlightsChangeListener>(1);

    public DiffContentPanel(EditableDiffView master, boolean isFirst) {
        this.master = master;
        this.isFirst = isFirst;
        this.setLayout(new BorderLayout());
        this.editorPane = new DecoratedEditorPane(this);
        this.editorPane.setEditable(false);
        this.scrollPane = new JScrollPane(this.editorPane);
        this.add(this.scrollPane);
        this.linesActions = new LineNumbersActionsBar(this, master.isActionsEnabled());
        this.actionsScrollPane = new JScrollPane(this.linesActions);
        this.actionsScrollPane.setHorizontalScrollBarPolicy(31);
        this.actionsScrollPane.setVerticalScrollBarPolicy(21);
        this.actionsScrollPane.setBorder(null);
        this.add((Component)this.actionsScrollPane, isFirst ? "After" : "Before");
        this.editorPane.putClientProperty("org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel", this);
        if (!isFirst) {
            this.editorPane.setFocusTraversalKeys(0, Collections.EMPTY_SET);
            this.editorPane.setFocusTraversalKeys(1, Collections.EMPTY_SET);
            this.editorPane.setFocusTraversalKeys(2, Collections.singleton(KeyStroke.getAWTKeyStroke(27, 64)));
            this.editorPane.putClientProperty("errorStripeOnly", Boolean.TRUE);
            this.editorPane.putClientProperty("code-folding-enable", false);
        }
    }

    void initActions() {
        ActionMap paneMap = this.editorPane.getActionMap();
        ActionMap am = this.getActionMap();
        am.setParent(paneMap);
        paneMap.put("cut-to-clipboard", this.getAction("cut-to-clipboard"));
        paneMap.put("copy-to-clipboard", this.getAction("copy-to-clipboard"));
        paneMap.put("delete", this.getAction("delete-next"));
        paneMap.put("paste-from-clipboard", this.getAction("paste-from-clipboard"));
    }

    private Action getAction(String key) {
        if (key == null) {
            return null;
        }
        EditorKit kit = this.editorPane.getEditorKit();
        if (kit == null) {
            return null;
        }
        Action[] actions = kit.getActions();
        for (int i = 0; i < actions.length; ++i) {
            if (!key.equals(actions[i].getValue("Name"))) continue;
            return actions[i];
        }
        return null;
    }

    LineNumbersActionsBar getLinesActions() {
        return this.linesActions;
    }

    public JScrollPane getActionsScrollPane() {
        return this.actionsScrollPane;
    }

    public JScrollPane getScrollPane() {
        return this.scrollPane;
    }

    public Difference[] getCurrentDiff() {
        return this.currentDiff;
    }

    public boolean isFirst() {
        return this.isFirst;
    }

    public void setCurrentDiff(Difference[] currentDiff) {
        this.currentDiff = currentDiff;
        this.editorPane.setDifferences(currentDiff);
        this.linesActions.onDiffSetChanged();
        this.fireHilitingChanged();
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        Container parent = this.getParent();
        if (parent instanceof JViewport && parent.getWidth() > d.width) {
            d = new Dimension(parent.getWidth(), d.height);
        }
        return d;
    }

    public DecoratedEditorPane getEditorPane() {
        return this.editorPane;
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        return this.editorPane.getAccessibleContext();
    }

    public EditableDiffView getMaster() {
        return this.master;
    }

    HighlightsContainer getHighlightsContainer() {
        return this;
    }

    public HighlightsSequence getHighlights(int start, int end) {
        return new DiffHighlightsSequence(start, end);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addHighlightsChangeListener(HighlightsChangeListener listener) {
        List<HighlightsChangeListener> list = this.listeners;
        synchronized (list) {
            this.listeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeHighlightsChangeListener(HighlightsChangeListener listener) {
        List<HighlightsChangeListener> list = this.listeners;
        synchronized (list) {
            this.listeners.remove((Object)listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fireHilitingChanged() {
        List<HighlightsChangeListener> list = this.listeners;
        synchronized (list) {
            for (HighlightsChangeListener listener : this.listeners) {
                listener.highlightChanged(new HighlightsChangeEvent((HighlightsContainer)this, 0, Integer.MAX_VALUE));
            }
        }
    }

    void onUISettingsChanged() {
        this.editorPane.repaint();
        this.linesActions.onUISettingsChanged();
        this.actionsScrollPane.revalidate();
        this.actionsScrollPane.repaint();
        this.revalidate();
        this.repaint();
    }

    public void setCustomEditor(JComponent c) {
        this.remove(this.scrollPane);
        Container viewPort = this.editorPane.getParent();
        if (viewPort instanceof JViewport && (viewPort = viewPort.getParent()) instanceof JScrollPane) {
            this.scrollPane = (JScrollPane)viewPort;
            this.add(c);
            c.setFocusTraversalKeysEnabled(false);
            c.setFocusTraversalPolicyProvider(true);
        }
    }

    public Lookup getLookup() {
        return Lookups.singleton((Object)this.getActionMap());
    }

    private class DiffHighlightsSequence
    implements HighlightsSequence {
        private final int endOffset;
        private final int startOffset;
        private int currentHiliteIndex;
        private DiffViewManager.HighLight[] hilites;

        public DiffHighlightsSequence(int start, int end) {
            this.currentHiliteIndex = -1;
            this.startOffset = start;
            this.endOffset = end;
            this.lookupHilites();
        }

        private void lookupHilites() {
            DiffViewManager.HighLight[] allHilites;
            ArrayList<DiffViewManager.HighLight> list = new ArrayList<DiffViewManager.HighLight>();
            for (DiffViewManager.HighLight hilite : allHilites = DiffContentPanel.this.isFirst ? DiffContentPanel.this.master.getManager().getFirstHighlights() : DiffContentPanel.this.master.getManager().getSecondHighlights()) {
                if (hilite.getEndOffset() < this.startOffset) continue;
                if (hilite.getStartOffset() > this.endOffset) break;
                list.add(hilite);
            }
            this.hilites = list.toArray(new DiffViewManager.HighLight[list.size()]);
        }

        public boolean moveNext() {
            if (this.currentHiliteIndex >= this.hilites.length - 1) {
                return false;
            }
            ++this.currentHiliteIndex;
            return true;
        }

        public int getStartOffset() {
            return Math.max(this.hilites[this.currentHiliteIndex].getStartOffset(), this.startOffset);
        }

        public int getEndOffset() {
            return Math.min(this.hilites[this.currentHiliteIndex].getEndOffset(), this.endOffset);
        }

        public AttributeSet getAttributes() {
            return this.hilites[this.currentHiliteIndex].getAttrs();
        }
    }

}

