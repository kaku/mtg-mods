/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.Hunk;
import org.netbeans.modules.diff.builtin.visualizer.TextDiffVisualizer;

final class UnifiedDiff {
    private final TextDiffVisualizer.TextDiffInfo diffInfo;
    private BufferedReader baseReader;
    private BufferedReader modifiedReader;
    private final String newline;
    private boolean baseEndsWithNewline;
    private boolean modifiedEndsWithNewline;
    private int currentBaseLine;
    private int currentModifiedLine;

    public UnifiedDiff(TextDiffVisualizer.TextDiffInfo diffInfo) {
        this.diffInfo = diffInfo;
        this.currentBaseLine = 1;
        this.currentModifiedLine = 1;
        this.newline = System.getProperty("line.separator");
    }

    public String computeDiff() throws IOException {
        this.baseReader = this.checkEndingNewline(this.diffInfo.createFirstReader(), true);
        this.modifiedReader = this.checkEndingNewline(this.diffInfo.createSecondReader(), false);
        StringBuilder buffer = new StringBuilder();
        buffer.append("--- ");
        buffer.append(this.diffInfo.getName1());
        buffer.append(this.newline);
        buffer.append("+++ ");
        buffer.append(this.diffInfo.getName2());
        buffer.append(this.newline);
        Difference[] diffs = this.diffInfo.getDifferences();
        int currentDifference = 0;
        while (currentDifference < diffs.length) {
            int lastDifference = this.getLastIndex(currentDifference);
            Hunk hunk = this.computeHunk(currentDifference, lastDifference);
            this.dumpHunk(buffer, hunk);
            currentDifference = lastDifference;
        }
        return buffer.toString();
    }

    private BufferedReader checkEndingNewline(Reader reader, boolean isBase) throws IOException {
        char endingChar;
        StringWriter sw = new StringWriter();
        UnifiedDiff.copyStreamsCloseAll(sw, reader);
        String s = sw.toString();
        char c = endingChar = s.length() == 0 ? '\u0000' : s.charAt(s.length() - 1);
        if (isBase) {
            this.baseEndsWithNewline = endingChar == '\n' || endingChar == '\r';
        } else {
            this.modifiedEndsWithNewline = endingChar == '\n' || endingChar == '\r';
        }
        return new BufferedReader(new StringReader(s));
    }

    private Hunk computeHunk(int firstDifference, int lastDifference) throws IOException {
        int skipLines;
        Hunk hunk = new Hunk();
        Difference firstDiff = this.diffInfo.getDifferences()[firstDifference];
        int contextLines = this.diffInfo.getContextNumLines();
        if (firstDiff.getType() == 1) {
            if (contextLines > firstDiff.getFirstStart()) {
                contextLines = firstDiff.getFirstStart();
            }
            skipLines = firstDiff.getFirstStart() - contextLines - this.currentBaseLine + 1;
        } else {
            if (contextLines >= firstDiff.getFirstStart()) {
                contextLines = firstDiff.getFirstStart() - 1;
            }
            skipLines = firstDiff.getFirstStart() - contextLines - this.currentBaseLine;
        }
        while (skipLines-- > 0) {
            this.readLine(this.baseReader);
            this.readLine(this.modifiedReader);
        }
        hunk.baseStart = this.currentBaseLine;
        hunk.modifiedStart = this.currentModifiedLine;
        for (int i = firstDifference; i < lastDifference; ++i) {
            Difference diff;
            int n;
            this.writeContextLines(hunk, diff.getFirstStart() - this.currentBaseLine + ((diff = this.diffInfo.getDifferences()[i]).getType() == 1 ? 1 : 0));
            if (diff.getFirstEnd() > 0) {
                n = diff.getFirstEnd() - diff.getFirstStart() + 1;
                this.outputLines(hunk, this.baseReader, "-", n);
                hunk.baseCount += n;
                if (!this.baseEndsWithNewline && i == this.diffInfo.getDifferences().length - 1 && diff.getFirstEnd() == this.currentBaseLine - 1) {
                    hunk.lines.add("\\ No newline at end of file");
                }
            }
            if (diff.getSecondEnd() <= 0) continue;
            n = diff.getSecondEnd() - diff.getSecondStart() + 1;
            this.outputLines(hunk, this.modifiedReader, "+", n);
            hunk.modifiedCount += n;
            if (this.modifiedEndsWithNewline || i != this.diffInfo.getDifferences().length - 1 || diff.getSecondEnd() != this.currentModifiedLine - 1) continue;
            hunk.lines.add("\\ No newline at end of file");
        }
        this.writeContextLines(hunk, this.diffInfo.getContextNumLines());
        if (hunk.modifiedCount == 0) {
            hunk.modifiedStart = 0;
        }
        if (hunk.baseCount == 0) {
            hunk.baseStart = 0;
        }
        return hunk;
    }

    private void writeContextLines(Hunk hunk, int count) throws IOException {
        while (count-- > 0) {
            String line = this.readLine(this.baseReader);
            if (line == null) {
                return;
            }
            hunk.lines.add(" " + line);
            this.readLine(this.modifiedReader);
            ++hunk.baseCount;
            ++hunk.modifiedCount;
        }
    }

    private String readLine(BufferedReader reader) throws IOException {
        String s = reader.readLine();
        if (s != null) {
            if (reader == this.baseReader) {
                ++this.currentBaseLine;
            }
            if (reader == this.modifiedReader) {
                ++this.currentModifiedLine;
            }
        }
        return s;
    }

    private void outputLines(Hunk hunk, BufferedReader reader, String mode, int n) throws IOException {
        while (n-- > 0) {
            String line = this.readLine(reader);
            hunk.lines.add(mode + line);
        }
    }

    private int getLastIndex(int firstIndex) {
        int contextLines = this.diffInfo.getContextNumLines() * 2;
        Difference[] diffs = this.diffInfo.getDifferences();
        ++firstIndex;
        while (firstIndex < diffs.length) {
            int curStart;
            Difference prevDiff = diffs[firstIndex - 1];
            Difference currentDiff = diffs[firstIndex];
            int prevEnd = 1 + (prevDiff.getType() == 1 ? prevDiff.getFirstStart() : prevDiff.getFirstEnd());
            int n = curStart = currentDiff.getType() == 1 ? currentDiff.getFirstStart() + 1 : currentDiff.getFirstStart();
            if (curStart - prevEnd > contextLines) break;
            ++firstIndex;
        }
        return firstIndex;
    }

    private void dumpHunk(StringBuilder buffer, Hunk hunk) {
        buffer.append("@@ -");
        buffer.append(Integer.toString(hunk.baseStart));
        if (hunk.baseCount != 1) {
            buffer.append(",");
            buffer.append(Integer.toString(hunk.baseCount));
        }
        buffer.append(" +");
        buffer.append(Integer.toString(hunk.modifiedStart));
        if (hunk.modifiedCount != 1) {
            buffer.append(",");
            buffer.append(Integer.toString(hunk.modifiedCount));
        }
        buffer.append(" @@");
        buffer.append(this.newline);
        for (String line : hunk.lines) {
            buffer.append(line);
            buffer.append(this.newline);
        }
    }

    private static void copyStreamsCloseAll(Writer writer, Reader reader) throws IOException {
        int n;
        char[] buffer = new char[4096];
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        writer.close();
        reader.close();
    }
}

