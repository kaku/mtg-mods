/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.diff.builtin.visualizer.GraphicalDiffVisualizer;
import org.openide.ErrorManager;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class GraphicalDiffVisualizerBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(GraphicalDiffVisualizer.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] desc;
        try {
            PropertyDescriptor colorAdded = new PropertyDescriptor("colorAdded", GraphicalDiffVisualizer.class);
            colorAdded.setDisplayName(NbBundle.getMessage(GraphicalDiffVisualizerBeanInfo.class, (String)"PROP_colorAdded"));
            colorAdded.setShortDescription(NbBundle.getMessage(GraphicalDiffVisualizerBeanInfo.class, (String)"HINT_colorAdded"));
            PropertyDescriptor colorMissing = new PropertyDescriptor("colorMissing", GraphicalDiffVisualizer.class);
            colorMissing.setDisplayName(NbBundle.getMessage(GraphicalDiffVisualizerBeanInfo.class, (String)"PROP_colorMissing"));
            colorMissing.setShortDescription(NbBundle.getMessage(GraphicalDiffVisualizerBeanInfo.class, (String)"HINT_colorMissing"));
            PropertyDescriptor colorChanged = new PropertyDescriptor("colorChanged", GraphicalDiffVisualizer.class);
            colorChanged.setDisplayName(NbBundle.getMessage(GraphicalDiffVisualizerBeanInfo.class, (String)"PROP_colorChanged"));
            colorChanged.setShortDescription(NbBundle.getMessage(GraphicalDiffVisualizerBeanInfo.class, (String)"HINT_colorChanged"));
            desc = new PropertyDescriptor[]{colorAdded, colorMissing, colorChanged};
        }
        catch (IntrospectionException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
            desc = null;
        }
        return desc;
    }

    @Override
    public int getDefaultPropertyIndex() {
        return 0;
    }

    @Override
    public Image getIcon(int iconKind) {
        switch (iconKind) {
            case 1: {
                return ImageUtilities.loadImage((String)"org/netbeans/modules/diff/diffSettingsIcon.gif", (boolean)true);
            }
        }
        return null;
    }
}

