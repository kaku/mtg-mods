/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.StatusDisplayer
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.Utils;
import org.netbeans.modules.diff.builtin.visualizer.TextDiffVisualizer;
import org.netbeans.modules.diff.options.AccessibleJFileChooser;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ExportPatch {
    private static final FileFilter unifiedFilter = new FileFilter(){

        @Override
        public boolean accept(File f) {
            return f.getName().endsWith("diff") || f.getName().endsWith("patch") || f.isDirectory();
        }

        @Override
        public String getDescription() {
            return NbBundle.getMessage(ExportPatch.class, (String)"FileFilter_Unified");
        }
    };
    private static final FileFilter normalFilter = new FileFilter(){

        @Override
        public boolean accept(File f) {
            return f.getName().endsWith("diff") || f.getName().endsWith("patch") || f.isDirectory();
        }

        @Override
        public String getDescription() {
            return NbBundle.getMessage(ExportPatch.class, (String)"FileFilter_Normal");
        }
    };

    public static void exportPatch(final StreamSource[] base, final StreamSource[] modified) {
        final AccessibleJFileChooser chooser = new AccessibleJFileChooser(NbBundle.getMessage(ExportPatch.class, (String)"ACSD_Export"));
        chooser.setDialogTitle(NbBundle.getMessage(ExportPatch.class, (String)"CTL_Export_Title"));
        chooser.setMultiSelectionEnabled(false);
        FileFilter[] old = chooser.getChoosableFileFilters();
        for (int i = 0; i < old.length; ++i) {
            FileFilter fileFilter = old[i];
            chooser.removeChoosableFileFilter(fileFilter);
        }
        chooser.setCurrentDirectory(new File(DiffModuleConfig.getDefault().getPreferences().get("ExportDiff.saveFolder", System.getProperty("user.home"))));
        chooser.addChoosableFileFilter(normalFilter);
        chooser.addChoosableFileFilter(unifiedFilter);
        chooser.setDialogType(1);
        chooser.setApproveButtonMnemonic(NbBundle.getMessage(ExportPatch.class, (String)"MNE_Export_ExportAction").charAt(0));
        chooser.setApproveButtonText(NbBundle.getMessage(ExportPatch.class, (String)"CTL_Export_ExportAction"));
        DialogDescriptor dd = new DialogDescriptor((Object)chooser, NbBundle.getMessage(ExportPatch.class, (String)"CTL_Export_Title"));
        dd.setOptions(new Object[0]);
        final Dialog dialog = DialogDisplayer.getDefault().createDialog(dd);
        chooser.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String state = e.getActionCommand();
                if (state.equals("ApproveSelection")) {
                    File destination = chooser.getSelectedFile();
                    String name = destination.getName();
                    boolean requiredExt = false;
                    final FileFilter selectedFileFilter = chooser.getFileFilter();
                    requiredExt |= name.endsWith(".diff");
                    if (!(requiredExt |= name.endsWith(".patch"))) {
                        File parent = destination.getParentFile();
                        destination = new File(parent, name + ".patch");
                    }
                    if (destination.exists()) {
                        NotifyDescriptor.Confirmation nd = new NotifyDescriptor.Confirmation((Object)NbBundle.getMessage(ExportPatch.class, (String)"BK3005", (Object)destination.getAbsolutePath()));
                        nd.setOptionType(0);
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)nd);
                        if (!nd.getValue().equals(NotifyDescriptor.OK_OPTION)) {
                            return;
                        }
                    }
                    DiffModuleConfig.getDefault().getPreferences().put("ExportDiff.saveFolder", destination.getParent());
                    final File out = destination;
                    Utils.postParallel(new Runnable(){

                        @Override
                        public void run() {
                            ExportPatch.exportDiff(base, modified, out, selectedFileFilter);
                        }
                    });
                }
                dialog.dispose();
            }

        });
        dialog.setVisible(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void exportDiff(StreamSource[] base, StreamSource[] modified, File destination, FileFilter format) {
        boolean success = false;
        OutputStream out = null;
        int exportedFiles = 0;
        try {
            String sep = System.getProperty("line.separator");
            out = new BufferedOutputStream(new FileOutputStream(destination));
            out.write(("# This patch file was generated by NetBeans IDE" + sep).getBytes("utf8"));
            out.write(("# This patch can be applied using context Tools: Apply Diff Patch action on respective folder." + sep).getBytes("utf8"));
            out.write(("# It uses platform neutral UTF-8 encoding." + sep).getBytes("utf8"));
            out.write(("# Above lines and this line are ignored by the patching process." + sep).getBytes("utf8"));
            for (int i = 0; i < base.length; ++i) {
                ExportPatch.exportDiff(base[i], modified[i], out, format);
                ++exportedFiles;
            }
            success = true;
        }
        catch (IOException ex) {
            ErrorManager.getDefault().annotate((Throwable)ex, NbBundle.getMessage(ExportPatch.class, (String)"BK3003"));
            ErrorManager.getDefault().notify(1, (Throwable)ex);
            ErrorManager.getDefault().notify(256, (Throwable)ex);
        }
        finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                }
                catch (IOException alreadyClsoed) {}
            }
            if (success) {
                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(ExportPatch.class, (String)"BK3004", (Object)new Integer(exportedFiles)));
                if (exportedFiles == 0) {
                    destination.delete();
                } else {
                    ExportPatch.openFile(destination);
                }
            } else {
                destination.delete();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void exportDiff(StreamSource base, StreamSource modified, OutputStream out, FileFilter format) throws IOException {
        Difference[] differences;
        DiffProvider diff = (DiffProvider)Lookup.getDefault().lookup(DiffProvider.class);
        Reader r1 = null;
        Reader r2 = null;
        try {
            r1 = base.createReader();
            if (r1 == null) {
                r1 = new StringReader("");
            }
            if ((r2 = modified.createReader()) == null) {
                r2 = new StringReader("");
            }
            differences = diff.computeDiff(r1, r2);
        }
        finally {
            if (r1 != null) {
                try {
                    r1.close();
                }
                catch (Exception e) {}
            }
            if (r2 != null) {
                try {
                    r2.close();
                }
                catch (Exception e) {}
            }
        }
        try {
            int i;
            r1 = base.createReader();
            if (r1 == null) {
                r1 = new StringReader("");
            }
            if ((r2 = modified.createReader()) == null) {
                r2 = new StringReader("");
            }
            TextDiffVisualizer.TextDiffInfo info = new TextDiffVisualizer.TextDiffInfo(base.getTitle(), modified.getTitle(), null, null, r1, r2, differences);
            info.setContextMode(true, 3);
            String diffText = format == unifiedFilter ? TextDiffVisualizer.differenceToUnifiedDiffText(info) : TextDiffVisualizer.differenceToNormalDiffText(info);
            ByteArrayInputStream is = new ByteArrayInputStream(diffText.getBytes("utf8"));
            while ((i = is.read()) != -1) {
                out.write(i);
            }
        }
        finally {
            if (r1 != null) {
                try {
                    r1.close();
                }
                catch (Exception e) {}
            }
            if (r2 != null) {
                try {
                    r2.close();
                }
                catch (Exception e) {}
            }
        }
    }

    private static void openFile(File file) {
        FileObject fo = FileUtil.toFileObject((File)file);
        if (fo != null) {
            try {
                DataObject dao = DataObject.find((FileObject)fo);
                OpenCookie oc = (OpenCookie)dao.getCookie(OpenCookie.class);
                if (oc != null) {
                    oc.open();
                }
            }
            catch (DataObjectNotFoundException e) {
                // empty catch block
            }
        }
    }

    private ExportPatch() {
    }

}

