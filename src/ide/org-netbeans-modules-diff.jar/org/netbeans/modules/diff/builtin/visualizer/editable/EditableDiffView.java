/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldHierarchyEvent
 *  org.netbeans.api.editor.fold.FoldHierarchyListener
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.errorstripe.privatespi.Mark
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.CookieSet$Factory
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.text.NbDocument$CustomEditor
 *  org.openide.util.Cancellable
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.UserQuestionException
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.TextUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import org.netbeans.api.diff.DiffController;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.builtin.visualizer.TextDiffVisualizer;
import org.netbeans.modules.diff.builtin.visualizer.editable.DecoratedEditorPane;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffMark;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffSplitPaneDivider;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffViewManager;
import org.netbeans.modules.diff.builtin.visualizer.editable.NoContentPanel;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.spi.diff.DiffControllerImpl;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.UndoRedo;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.Cancellable;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.UserQuestionException;
import org.openide.util.WeakListeners;

public class EditableDiffView
extends DiffControllerImpl
implements DiffView,
DocumentListener,
AncestorListener,
PropertyChangeListener,
PreferenceChangeListener,
ChangeListener {
    private static final int INITIAL_DIVIDER_SIZE = 32;
    private static final String CONTENT_TYPE_PLAIN = "text/plain";
    private Stroke boldStroke = new BasicStroke(3.0f);
    private Color colorMissing;
    private Color colorAdded;
    private Color colorChanged;
    private Color colorLines = Color.DARK_GRAY;
    private Color COLOR_READONLY_BG = new Color(255, 200, 200);
    private final Difference[] NO_DIFFERENCES = new Difference[0];
    private DiffContentPanel jEditorPane1;
    private DiffContentPanel jEditorPane2;
    private JEditorPane textualEditorPane;
    private boolean secondSourceAvailable;
    private boolean firstSourceAvailable;
    private boolean firstSourceUnsupportedTextUI;
    private boolean secondSourceUnsupportedTextUI;
    private final boolean binaryDiff;
    private JViewport jViewport2;
    final JLabel fileLabel1 = new JLabel();
    final JLabel fileLabel2 = new JLabel();
    final JPanel filePanel1 = new JPanel();
    final JPanel filePanel2 = new JPanel();
    final JPanel textualPanel = new JPanel();
    final JTabbedPane jTabbedPane;
    final JComponent view;
    final JSplitPane jSplitPane1 = new JSplitPane();
    private int diffSerial;
    private Difference[] diffs = this.NO_DIFFERENCES;
    private boolean ignoredUpdateEvents;
    private int horizontalScroll1ChangedValue = -1;
    private int horizontalScroll2ChangedValue = -1;
    private RequestProcessor.Task refreshDiffTask;
    private DiffViewManager manager;
    private boolean actionsEnabled;
    private DiffSplitPaneUI spui;
    private Document baseDocument;
    private Document modifiedDocument;
    private Boolean skipFile;
    private EditorCookie.Observable editableCookie;
    private Document editableDocument;
    private UndoRedo.Manager editorUndoRedo;
    private EditableDiffMarkProvider diffMarkprovider;
    private Integer askedLineLocation;
    private static final String PROP_SMART_SCROLLING_DISABLED = "diff.smartScrollDisabled";
    private static final RequestProcessor rp = new RequestProcessor("EditableDiffViewRP", 10);
    private static final Logger LOG = Logger.getLogger(EditableDiffView.class.getName());
    private static final String CONTENT_TYPE_DIFF = "text/x-diff";
    private final JPanel searchContainer;
    private static final String PROP_SEARCH_CONTAINER = "diff.search.container";
    private final Object DIFFING_LOCK = new Object();
    WeakHashMap<JEditorPane, FoldHierarchyListener> hieararchyListeners = new WeakHashMap(2);
    WeakHashMap<JEditorPane, PropertyChangeListener> propertyChangeListeners = new WeakHashMap(2);
    private TextualDiffRefreshTask textualRefreshTask;

    public EditableDiffView(StreamSource ss1, StreamSource ss2) {
        this(ss1, ss2, false);
    }

    public EditableDiffView(final StreamSource ss1, final StreamSource ss2, boolean enhancedView) {
        String title2;
        this.refreshDiffTask = rp.create((Runnable)new RefreshDiffTask());
        this.initColors();
        String title1 = ss1.getTitle();
        if (title1 == null) {
            title1 = NbBundle.getMessage(EditableDiffView.class, (String)"CTL_DiffPanel_NoTitle");
        }
        if ((title2 = ss2.getTitle()) == null) {
            title2 = NbBundle.getMessage(EditableDiffView.class, (String)"CTL_DiffPanel_NoTitle");
        }
        String mimeType1 = ss1.getMIMEType();
        String mimeType2 = ss2.getMIMEType();
        if (mimeType1 == null) {
            mimeType1 = mimeType2;
        }
        if (mimeType2 == null) {
            mimeType2 = mimeType1;
        }
        this.binaryDiff = mimeType1 == null || mimeType2 == null || mimeType1.equals("application/octet-stream") || mimeType2.equals("application/octet-stream");
        this.actionsEnabled = ss2.isEditable();
        this.diffMarkprovider = new EditableDiffMarkProvider();
        this.view = new JPanel(new BorderLayout(0, 0));
        this.searchContainer = new JPanel();
        this.searchContainer.setLayout(new BoxLayout(this.searchContainer, 1));
        this.view.add((Component)this.searchContainer, "Last");
        if (enhancedView) {
            this.jTabbedPane = new JTabbedPane(1);
            this.view.add((Component)this.jTabbedPane, "Center");
        } else {
            this.jTabbedPane = null;
            this.view.add((Component)this.jSplitPane1, "Center");
        }
        this.initComponents();
        if (!this.binaryDiff) {
            this.jEditorPane2.getEditorPane().putClientProperty("org.netbeans.modules.diff.builtin.visualizer.editable.MarkProvider", (Object)this.diffMarkprovider);
        }
        this.jSplitPane1.setName(NbBundle.getMessage(EditableDiffView.class, (String)"DiffComponent.title", (Object)ss1.getName(), (Object)ss2.getName()));
        this.spui = new DiffSplitPaneUI(this.jSplitPane1);
        this.jSplitPane1.setUI(this.spui);
        this.jSplitPane1.setResizeWeight(0.5);
        this.jSplitPane1.setDividerSize(32);
        this.jSplitPane1.putClientProperty("PersistenceType", "Never");
        this.jSplitPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_DiffPanelA11yName"));
        this.jSplitPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_DiffPanelA11yDesc"));
        this.view.setName(NbBundle.getMessage(EditableDiffView.class, (String)"DiffComponent.title", (Object)ss1.getName(), (Object)ss2.getName()));
        this.view.putClientProperty("PersistenceType", "Never");
        this.view.getAccessibleContext().setAccessibleName(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_DiffPanelA11yName"));
        this.view.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_DiffPanelA11yDesc"));
        this.initializeTabPane(ss1, ss2);
        this.setSourceTitle(this.fileLabel1, title1);
        this.setSourceTitle(this.fileLabel2, title2);
        final String f1 = mimeType1;
        final String f2 = mimeType2;
        boolean canceled = Thread.interrupted();
        try {
            Runnable awtTask = new Runnable(){

                @Override
                public void run() {
                    EditorKit editorKit;
                    NoContentPanel ncp;
                    Color borderColor = UIManager.getColor("scrollpane_border");
                    if (borderColor == null) {
                        borderColor = UIManager.getColor("controlShadow");
                    }
                    EditableDiffView.this.jSplitPane1.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                    EditableDiffView.this.view.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                    if (EditableDiffView.this.binaryDiff) {
                        EditableDiffView.this.adjustPreferredSizes();
                        return;
                    }
                    EditableDiffView.this.jEditorPane1.getScrollPane().setBorder(null);
                    EditableDiffView.this.jEditorPane2.getScrollPane().setBorder(null);
                    EditableDiffView.this.jEditorPane1.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                    EditableDiffView.this.jEditorPane2.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                    try {
                        editorKit = CloneableEditorSupport.getEditorKit((String)f1);
                    }
                    catch (IllegalArgumentException ex) {
                        LOG.log(Level.INFO, ss1.toString(), ex);
                        editorKit = CloneableEditorSupport.getEditorKit((String)"text/plain");
                    }
                    EditableDiffView.this.jEditorPane1.getEditorPane().setEditorKit(editorKit);
                    EditableDiffView.this.repairTextUI(EditableDiffView.this.jEditorPane1.getEditorPane());
                    EditableDiffView.this.jEditorPane1.getEditorPane().putClientProperty("usedByCloneableEditor", Boolean.TRUE);
                    try {
                        editorKit = CloneableEditorSupport.getEditorKit((String)f2);
                    }
                    catch (IllegalArgumentException ex) {
                        LOG.log(Level.INFO, ss2.toString(), ex);
                        editorKit = CloneableEditorSupport.getEditorKit((String)"text/plain");
                    }
                    EditableDiffView.this.jEditorPane2.getEditorPane().setEditorKit(editorKit);
                    EditableDiffView.this.repairTextUI(EditableDiffView.this.jEditorPane2.getEditorPane());
                    EditableDiffView.this.jEditorPane2.getEditorPane().putClientProperty("usedByCloneableEditor", Boolean.TRUE);
                    try {
                        EditableDiffView.this.setSource1(ss1);
                    }
                    catch (IOException ioex) {
                        Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, "Diff source 1 unavailable", ioex);
                    }
                    try {
                        EditableDiffView.this.setSource2(ss2);
                    }
                    catch (IOException ioex) {
                        Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, "Diff source 2 unavailable", ioex);
                    }
                    if (EditableDiffView.this.jTabbedPane != null) {
                        EditableDiffView.this.textualEditorPane.setEditorKit(CloneableEditorSupport.getEditorKit((String)"text/x-diff"));
                        EditableDiffView.this.repairTextUI(EditableDiffView.this.textualEditorPane);
                        EditableDiffView.this.setTextualContent();
                    }
                    if (!EditableDiffView.this.secondSourceAvailable) {
                        EditableDiffView.this.filePanel2.remove(EditableDiffView.this.jEditorPane2);
                        ncp = new NoContentPanel(NbBundle.getMessage(EditableDiffView.class, (String)(EditableDiffView.this.secondSourceUnsupportedTextUI ? "CTL_DiffPanel_UnsupportedTextUI" : "CTL_DiffPanel_NoContent")));
                        ncp.setPreferredSize(new Dimension(EditableDiffView.access$200((EditableDiffView)EditableDiffView.this).getPreferredSize().width, ncp.getPreferredSize().height));
                        EditableDiffView.this.filePanel2.add(ncp);
                        EditableDiffView.this.actionsEnabled = false;
                    }
                    if (!EditableDiffView.this.firstSourceAvailable) {
                        EditableDiffView.this.filePanel1.remove(EditableDiffView.this.jEditorPane1);
                        ncp = new NoContentPanel(NbBundle.getMessage(EditableDiffView.class, (String)(EditableDiffView.this.firstSourceUnsupportedTextUI ? "CTL_DiffPanel_UnsupportedTextUI" : "CTL_DiffPanel_NoContent")));
                        ncp.setPreferredSize(new Dimension(EditableDiffView.access$300((EditableDiffView)EditableDiffView.this).getPreferredSize().width, ncp.getPreferredSize().height));
                        EditableDiffView.this.filePanel1.add(ncp);
                        EditableDiffView.this.actionsEnabled = false;
                    }
                    EditableDiffView.this.adjustPreferredSizes();
                    DecoratedEditorPane leftEditor = EditableDiffView.this.jEditorPane1.getEditorPane();
                    DecoratedEditorPane rightEditor = EditableDiffView.this.jEditorPane2.getEditorPane();
                    if (rightEditor.isEditable()) {
                        EditableDiffView.this.setBackgroundColorForNonEditable(leftEditor, rightEditor);
                    }
                    if ((rightEditor.getBackground().getRGB() & 16777215) == 0) {
                        EditableDiffView.this.colorLines = Color.WHITE;
                    }
                }
            };
            if (SwingUtilities.isEventDispatchThread()) {
                awtTask.run();
            } else {
                SwingUtilities.invokeAndWait(awtTask);
            }
        }
        catch (InterruptedException e) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.FINE, ".colorLines:" + this.colorLines + ", .jviewPort2:" + this.jViewport2 + ", editableDocument:" + this.editableDocument + ", editableCookie:" + (Object)this.editableCookie + ", editorUndoRedo:" + (Object)this.editorUndoRedo, e);
            canceled = true;
        }
        catch (InvocationTargetException e) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.SEVERE, ".colorLines:" + this.colorLines + ", .jviewPort2:" + this.jViewport2 + ", editableDocument:" + this.editableDocument + ", editableCookie:" + (Object)this.editableCookie + ", editorUndoRedo:" + (Object)this.editorUndoRedo, e);
        }
        if (this.binaryDiff) {
            return;
        }
        this.view.addAncestorListener(this);
        this.manager = new DiffViewManager(this);
        this.manager.init();
        if (canceled) {
            Thread.currentThread().interrupt();
        } else {
            this.refreshDiff(100);
        }
    }

    private void initializeTabPane(StreamSource ss1, StreamSource ss2) {
        if (this.jTabbedPane != null) {
            this.jTabbedPane.addTab(NbBundle.getMessage(EditableDiffView.class, (String)"EditableDiffView.viewGraphical.title"), this.jSplitPane1);
            this.jTabbedPane.addTab(NbBundle.getMessage(EditableDiffView.class, (String)"EditableDiffView.viewTextual.title"), this.textualPanel);
            this.jTabbedPane.addChangeListener(this);
        }
    }

    private void setBackgroundColorForNonEditable(JTextComponent leftEditor, JTextComponent rightEditor) {
        int editableBgColor;
        Object bgColorObj;
        AttributeSet attrSet;
        FontColorSettings fontColorSettings;
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)leftEditor);
        if (mimeType == null) {
            mimeType = "text/plain";
        }
        Color bgColor = null;
        Lookup lookup = MimeLookup.getLookup((String)mimeType);
        if (lookup != null && (fontColorSettings = (FontColorSettings)lookup.lookup(FontColorSettings.class)) != null && (attrSet = fontColorSettings.getFontColors("guarded")) != null && (bgColorObj = attrSet.getAttribute(StyleConstants.Background)) instanceof Color) {
            bgColor = (Color)bgColorObj;
        }
        if (bgColor == null && (editableBgColor = rightEditor.getBackground().getRGB() & 16777215) == 16777215 && System.getProperty("netbeans.experimental.diff.ReadonlyBg") == null) {
            bgColor = this.COLOR_READONLY_BG;
        }
        if (bgColor != null) {
            leftEditor.setBackground(bgColor);
        }
    }

    private void adjustPreferredSizes() {
        Dimension pf1 = this.fileLabel1.getPreferredSize();
        Dimension pf2 = this.fileLabel2.getPreferredSize();
        if (pf1.width > pf2.width) {
            this.fileLabel2.setPreferredSize(new Dimension(pf1.width, pf2.height));
        } else {
            this.fileLabel1.setPreferredSize(new Dimension(pf2.width, pf1.height));
        }
    }

    @Override
    public void setLocation(final DiffController.DiffPane pane, DiffController.LocationType type, final int location) {
        if (type == DiffController.LocationType.DifferenceIndex) {
            this.manager.runWithSmartScrollingDisabled(new Runnable(){

                @Override
                public void run() {
                    EditableDiffView.this.setDifferenceImpl(location);
                }
            });
        } else {
            this.askedLineLocation = location;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Runnable withDisabledSmartScroll = null;
                    if (pane == DiffController.DiffPane.Base) {
                        if (Boolean.TRUE.equals(EditableDiffView.this.getJComponent().getClientProperty("diff.smartScrollDisabled"))) {
                            withDisabledSmartScroll = new Runnable(){

                                @Override
                                public void run() {
                                    EditableDiffView.this.setBaseLineNumberImpl(location, false);
                                }
                            };
                        } else {
                            EditableDiffView.this.setBaseLineNumberImpl(location, true);
                        }
                    } else {
                        withDisabledSmartScroll = new Runnable(){

                            @Override
                            public void run() {
                                EditableDiffView.this.setModifiedLineNumberImpl(location);
                            }
                        };
                    }
                    if (withDisabledSmartScroll != null) {
                        EditableDiffView.this.manager.runWithSmartScrollingDisabled(withDisabledSmartScroll);
                    }
                }

            });
        }
    }

    private void setModifiedLineNumberImpl(int line) {
        this.initGlobalSizes();
        try {
            int lineOffset;
            EditorUI editorUI = Utilities.getEditorUI((JTextComponent)this.jEditorPane2.getEditorPane());
            if (editorUI == null) {
                return;
            }
            int off2 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane2.getEditorPane().getDocument()), (int)line);
            this.jEditorPane2.getEditorPane().setCaretPosition(off2);
            int offset = this.jEditorPane2.getScrollPane().getViewport().getViewRect().height / 2 + 1;
            View rootView = Utilities.getDocumentView((JTextComponent)this.jEditorPane2.getEditorPane());
            Rectangle rec = this.jEditorPane2.getEditorPane().modelToView(rootView.getView(line).getEndOffset() - 1);
            if (rec == null) {
                int lineHeight = editorUI.getLineHeight();
                lineOffset = lineHeight * line - offset;
            } else {
                lineOffset = rec.y - offset;
            }
            JScrollBar rightScrollBar = this.jEditorPane2.getScrollPane().getVerticalScrollBar();
            rightScrollBar.setValue(lineOffset);
        }
        catch (IndexOutOfBoundsException ex) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, null, ex);
        }
        catch (BadLocationException ex) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, null, ex);
        }
    }

    private void setBaseLineNumberImpl(int line, boolean updateRightPanel) {
        this.initGlobalSizes();
        try {
            Rectangle rec;
            int lineOffset;
            EditorUI editorUI = Utilities.getEditorUI((JTextComponent)this.jEditorPane1.getEditorPane());
            if (editorUI == null) {
                return;
            }
            int offset = this.jEditorPane1.getScrollPane().getViewport().getViewRect().height / 2 + 1;
            View rootView = Utilities.getDocumentView((JTextComponent)this.jEditorPane1.getEditorPane());
            View lineView = rootView != null ? rootView.getView(line) : null;
            Rectangle rectangle = rec = lineView != null ? this.jEditorPane1.getEditorPane().modelToView(lineView.getEndOffset() - 1) : null;
            if (rec == null) {
                int lineHeight = editorUI.getLineHeight();
                lineOffset = lineHeight * line - offset;
            } else {
                lineOffset = rec.y - offset;
            }
            int off1 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane1.getEditorPane().getDocument()), (int)line);
            this.jEditorPane1.getEditorPane().setCaretPosition(off1);
            JScrollBar leftScrollBar = this.jEditorPane1.getScrollPane().getVerticalScrollBar();
            leftScrollBar.setValue(lineOffset);
            if (updateRightPanel) {
                JScrollBar rightScrollBar = this.jEditorPane2.getScrollPane().getVerticalScrollBar();
                rightScrollBar.setValue(lineOffset);
                this.updateCurrentDifference(null);
            }
        }
        catch (IndexOutOfBoundsException ex) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, null, ex);
        }
        catch (BadLocationException ex) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, null, ex);
        }
    }

    private void setDifferenceImpl(int location) {
        if (location < -1 || location >= this.diffs.length) {
            throw new IllegalArgumentException("Illegal difference number: " + location);
        }
        if (location != -1) {
            this.setDifferenceIndex(location);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    EditableDiffView.this.ignoredUpdateEvents = true;
                    EditableDiffView.this.showCurrentDifference();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            EditableDiffView.this.ignoredUpdateEvents = false;
                        }
                    });
                }

            });
        }
    }

    @Override
    public JComponent getJComponent() {
        return this.view;
    }

    public boolean isActionsEnabled() {
        return this.actionsEnabled;
    }

    private void initColors() {
        this.colorMissing = DiffModuleConfig.getDefault().getDeletedColor();
        this.colorAdded = DiffModuleConfig.getDefault().getAddedColor();
        this.colorChanged = DiffModuleConfig.getDefault().getChangedColor();
    }

    private void addDocumentListeners() {
        if (this.baseDocument != null) {
            this.baseDocument.addDocumentListener(this);
        }
        if (this.modifiedDocument != null) {
            this.modifiedDocument.addDocumentListener(this);
        }
    }

    private void removeDocumentListeners() {
        if (this.baseDocument != null) {
            this.baseDocument.removeDocumentListener(this);
        }
        if (this.modifiedDocument != null) {
            this.modifiedDocument.removeDocumentListener(this);
        }
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        DiffModuleConfig.getDefault().getPreferences().addPreferenceChangeListener(this);
        this.expandFolds();
        this.initGlobalSizes();
        this.addChangeListeners();
        this.addDocumentListeners();
        this.refreshDiff(50);
        if (this.editableCookie == null) {
            return;
        }
        this.refreshEditableDocument();
        this.editableCookie.addPropertyChangeListener((PropertyChangeListener)this);
    }

    private void refreshEditableDocument() {
        StyledDocument doc = null;
        try {
            doc = this.editableCookie.openDocument();
        }
        catch (IOException e) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, "Getting new Document from EditorCookie", e);
            return;
        }
        this.editableDocument.removeDocumentListener(this);
        if (doc != this.editableDocument) {
            this.editableDocument = doc;
            this.jEditorPane2.getEditorPane().setDocument(this.editableDocument);
            this.refreshDiff(20);
        }
        this.editableDocument.addDocumentListener(this);
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
        DiffModuleConfig.getDefault().getPreferences().removePreferenceChangeListener(this);
        this.removeDocumentListeners();
        if (this.editableCookie != null) {
            this.editableCookie.removePropertyChangeListener((PropertyChangeListener)this);
        }
    }

    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        this.initColors();
        this.diffChanged();
        this.refreshDiff(20);
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.refreshDiff(50);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.refreshDiff(50);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.refreshDiff(50);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.jTabbedPane == e.getSource()) {
            if (this.jTabbedPane.getSelectedComponent() == this.jSplitPane1) {
                this.updateCurrentDifference(null);
            } else {
                this.setDifferenceIndex(-1);
            }
        }
    }

    Color getColor(Difference ad) {
        if (ad.getType() == 1) {
            return this.colorAdded;
        }
        if (ad.getType() == 2) {
            return this.colorChanged;
        }
        return this.colorMissing;
    }

    JComponent getMyDivider() {
        return this.spui.splitPaneDivider.getDivider();
    }

    DiffContentPanel getEditorPane1() {
        return this.jEditorPane1;
    }

    DiffContentPanel getEditorPane2() {
        return this.jEditorPane2;
    }

    DiffViewManager getManager() {
        return this.manager;
    }

    Difference[] getDifferences() {
        return this.diffs;
    }

    private void replace(final StyledDocument doc, final int start, final int length, final String text) {
        NbDocument.runAtomic((StyledDocument)doc, (Runnable)new Runnable(){

            @Override
            public void run() {
                try {
                    doc.remove(start, length);
                    doc.insertString(start, text, null);
                }
                catch (BadLocationException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            }
        });
    }

    void rollback(Difference diff) {
        StyledDocument document = (StyledDocument)this.getEditorPane2().getEditorPane().getDocument();
        if (diff == null) {
            Document src = this.getEditorPane1().getEditorPane().getDocument();
            try {
                this.replace(document, 0, document.getLength(), src.getText(0, src.getLength()));
            }
            catch (BadLocationException e) {
                ErrorManager.getDefault().notify((Throwable)e);
            }
            return;
        }
        try {
            if (diff.getType() == 1) {
                int start = DiffViewManager.getRowStartFromLineOffset(document, diff.getSecondStart() - 1);
                int end = DiffViewManager.getRowStartFromLineOffset(document, diff.getSecondEnd());
                if (end == -1) {
                    end = document.getLength();
                }
                document.remove(start, end - start);
            } else if (diff.getType() == 0) {
                int start = DiffViewManager.getRowStartFromLineOffset(document, diff.getSecondStart());
                String addedText = diff.getFirstText();
                if (start == -1) {
                    start = document.getLength();
                    addedText = EditableDiffView.switchLineEndings(addedText);
                }
                document.insertString(start, addedText, null);
            } else {
                int start = DiffViewManager.getRowStartFromLineOffset(document, diff.getSecondStart() - 1);
                int end = DiffViewManager.getRowStartFromLineOffset(document, diff.getSecondEnd());
                if (end == -1) {
                    end = document.getLength();
                }
                this.replace(document, start, end - start, diff.getFirstText());
            }
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    private static String switchLineEndings(String addedText) {
        StringBuilder sb = new StringBuilder(addedText);
        sb.insert(0, '\n');
        if (sb.charAt(sb.length() - 1) == '\n') {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    Stroke getBoldStroke() {
        return this.boldStroke;
    }

    public boolean requestFocusInWindow() {
        return this.jEditorPane1.requestFocusInWindow();
    }

    @Override
    public JComponent getComponent() {
        return this.view;
    }

    @Override
    public int getDifferenceCount() {
        int retval = this.diffs.length;
        if (this.jTabbedPane != null && this.jTabbedPane.getSelectedComponent() == this.textualPanel) {
            retval = 0;
        }
        return retval;
    }

    @Override
    public boolean canSetCurrentDifference() {
        return this.jTabbedPane == null || this.jTabbedPane.getSelectedComponent() != this.textualPanel;
    }

    @Override
    public void setCurrentDifference(int diffNo) throws UnsupportedOperationException {
        this.setLocation(null, DiffController.LocationType.DifferenceIndex, diffNo);
    }

    @Override
    public int getCurrentDifference() {
        int retval = this.getDifferenceIndex();
        if (!this.canSetCurrentDifference()) {
            retval = -1;
        }
        return retval;
    }

    private int computeCurrentDifference(Boolean down) {
        if (this.manager == null || this.jViewport2 == null) {
            return 0;
        }
        Rectangle viewRect = this.jViewport2.getViewRect();
        int bottom = viewRect.y + viewRect.height * 2 / 3;
        DiffViewManager.DecoratedDifference[] ddiffs = this.manager.getDecorations();
        if (Boolean.FALSE.equals(down)) {
            if (viewRect.y != 0) {
                int up = viewRect.y + viewRect.height * 1 / 3;
                for (int i = ddiffs.length - 1; i >= 0; --i) {
                    int startLine = ddiffs[i].getTopRight();
                    int endLine = ddiffs[i].getBottomRight();
                    if (startLine >= up || endLine >= up) continue;
                    return Math.min(ddiffs.length - 1, i + 1);
                }
            }
            return ddiffs.length == 0 ? -1 : 0;
        }
        for (int i = 0; i < ddiffs.length; ++i) {
            int startLine = ddiffs[i].getTopRight();
            int endLine = ddiffs[i].getBottomRight();
            if (startLine <= bottom || endLine <= bottom && endLine != -1) continue;
            return Math.max(0, i - 1);
        }
        return ddiffs.length - 1;
    }

    void updateCurrentDifference(Boolean down) {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.ignoredUpdateEvents) {
            return;
        }
        int cd = this.computeCurrentDifference(down);
        this.setDifferenceIndex(cd);
    }

    @Override
    public JToolBar getToolBar() {
        return null;
    }

    private void showCurrentDifference() {
        int index = this.getDifferenceIndex();
        if (index < 0 || index >= this.diffs.length || index >= this.manager.getDecorations().length) {
            return;
        }
        final Difference diff = this.diffs[index];
        this.initGlobalSizes();
        try {
            final StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getEditorPane().getDocument();
            final StyledDocument doc2 = (StyledDocument)this.jEditorPane2.getEditorPane().getDocument();
            final int offCurrent = this.jEditorPane2.getEditorPane().getCaretPosition();
            doc1.render(new Runnable(){

                @Override
                public void run() {
                    int offFirstStart = NbDocument.findLineOffset((StyledDocument)doc1, (int)(diff.getFirstStart() > 0 ? diff.getFirstStart() - 1 : 0));
                    EditableDiffView.this.jEditorPane1.getEditorPane().setCaretPosition(offFirstStart);
                }
            });
            doc2.render(new Runnable(){

                @Override
                public void run() {
                    int offSecondEnd;
                    int offSecondStart = NbDocument.findLineOffset((StyledDocument)doc2, (int)(diff.getSecondStart() > 0 ? diff.getSecondStart() - 1 : 0));
                    if (diff.getSecondEnd() > diff.getSecondStart()) {
                        offSecondEnd = NbDocument.findLineOffset((StyledDocument)doc2, (int)(diff.getSecondEnd() > 0 ? diff.getSecondEnd() - 1 : 0));
                    } else {
                        int lastLine = NbDocument.findLineNumber((StyledDocument)doc2, (int)doc2.getLength()) + 1;
                        offSecondEnd = diff.getSecondStart() < lastLine ? NbDocument.findLineOffset((StyledDocument)doc2, (int)diff.getSecondStart()) : doc2.getLength();
                    }
                    if (offCurrent < offSecondStart || offCurrent > offSecondEnd) {
                        EditableDiffView.this.jEditorPane2.getEditorPane().setCaretPosition(offSecondStart);
                    }
                }
            });
            DiffViewManager.DecoratedDifference ddiff = this.manager.getDecorations()[index];
            int offset = this.jEditorPane2.getScrollPane().getViewport().getViewRect().height / 2 + 1;
            this.jEditorPane2.getScrollPane().getVerticalScrollBar().setValue(ddiff.getTopRight() - offset);
        }
        catch (IllegalArgumentException | IndexOutOfBoundsException ex) {
            Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, null, ex);
        }
        this.manager.scroll(index == this.diffs.length - 1 || index == 0);
    }

    private void initComponents() {
        this.fileLabel1.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.fileLabel1.setHorizontalAlignment(0);
        this.filePanel1.setLayout(new BorderLayout());
        this.filePanel1.add((Component)this.fileLabel1, "First");
        this.fileLabel2.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.fileLabel2.setHorizontalAlignment(0);
        this.filePanel2.setLayout(new BorderLayout());
        this.filePanel2.add((Component)this.fileLabel2, "First");
        this.textualPanel.setLayout(new BorderLayout());
        if (this.binaryDiff) {
            NoContentPanel ncp1 = new NoContentPanel(NbBundle.getMessage(EditableDiffView.class, (String)"CTL_DiffPanel_BinaryFile"));
            this.fileLabel1.setLabelFor(ncp1);
            this.filePanel1.add(ncp1);
            NoContentPanel ncp2 = new NoContentPanel(NbBundle.getMessage(EditableDiffView.class, (String)"CTL_DiffPanel_BinaryFile"));
            this.fileLabel2.setLabelFor(ncp2);
            this.filePanel2.add(ncp2);
        } else {
            this.jEditorPane1 = new DiffContentPanel(this, true);
            this.jEditorPane2 = new DiffContentPanel(this, false);
            this.jEditorPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_EditorPane1A11yName"));
            this.jEditorPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_EditorPane1A11yDescr"));
            this.jEditorPane2.getAccessibleContext().setAccessibleName(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_EditorPane2A11yName"));
            this.jEditorPane2.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_EditorPane2A11yDescr"));
            this.fileLabel1.setLabelFor(this.jEditorPane1);
            this.filePanel1.add(this.jEditorPane1);
            this.fileLabel2.setLabelFor(this.jEditorPane2);
            this.filePanel2.add(this.jEditorPane2);
            this.textualEditorPane = new JEditorPane(){
                private int fontHeight;
                private int charWidth;

                @Override
                public void setFont(Font font) {
                    super.setFont(font);
                    FontMetrics metrics = this.getFontMetrics(font);
                    this.charWidth = metrics.charWidth('m');
                    this.fontHeight = metrics.getHeight();
                }

                @Override
                public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
                    if (this.fontHeight == -1) {
                        return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
                    }
                    switch (orientation) {
                        case 1: {
                            return this.fontHeight;
                        }
                        case 0: {
                            return this.charWidth;
                        }
                    }
                    throw new IllegalArgumentException("Invalid orientation: " + orientation);
                }
            };
            this.textualEditorPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_EditorPane1A11yName"));
            this.textualEditorPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(EditableDiffView.class, (String)"ACS_EditorPane1A11yDescr"));
            this.textualPanel.add(new JScrollPane(this.textualEditorPane));
            this.textualEditorPane.putClientProperty("diff.search.container", this.searchContainer);
            this.jEditorPane1.getEditorPane().putClientProperty("diff.search.container", this.searchContainer);
            this.jEditorPane2.getEditorPane().putClientProperty("diff.search.container", this.searchContainer);
        }
        this.jSplitPane1.setLeftComponent(this.filePanel1);
        this.jSplitPane1.setRightComponent(this.filePanel2);
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            Color color = UIManager.getColor("NbExplorerView.background");
            this.jSplitPane1.setBackground(color);
            this.filePanel1.setBackground(color);
            this.filePanel2.setBackground(color);
            this.view.setBackground(color);
            this.view.setOpaque(true);
        }
    }

    private void expandFolds(JEditorPane pane) {
        final FoldHierarchy fh = FoldHierarchy.get((JTextComponent)pane);
        FoldUtilities.expandAll((FoldHierarchy)fh);
        FoldHierarchyListener list = new FoldHierarchyListener(){

            public void foldHierarchyChanged(FoldHierarchyEvent evt) {
                FoldUtilities.expandAll((FoldHierarchy)fh);
            }
        };
        this.hieararchyListeners.put(pane, ()list);
        fh.addFoldHierarchyListener((FoldHierarchyListener)WeakListeners.create(FoldHierarchyListener.class, (EventListener)list, (Object)fh));
    }

    private void expandFolds() {
        this.expandFolds(this.jEditorPane1.getEditorPane());
        this.expandFolds(this.jEditorPane2.getEditorPane());
    }

    private void initGlobalSizes() {
        int value;
        int numLines2;
        int totHeight;
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getEditorPane().getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane2.getEditorPane().getDocument();
        int numLines1 = NbDocument.findLineNumber((StyledDocument)doc1, (int)doc1.getEndPosition().getOffset());
        int numLines = Math.max(numLines1, numLines2 = NbDocument.findLineNumber((StyledDocument)doc2, (int)doc2.getEndPosition().getOffset()));
        if (numLines < 1) {
            numLines = 1;
        }
        if ((value = this.jEditorPane2.getSize().height) > (totHeight = this.jEditorPane1.getSize().height)) {
            totHeight = value;
        }
    }

    private void joinScrollBars() {
        final JScrollBar scrollBarH1 = this.jEditorPane1.getScrollPane().getHorizontalScrollBar();
        final JScrollBar scrollBarH2 = this.jEditorPane2.getScrollPane().getHorizontalScrollBar();
        scrollBarH1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH1.getValue();
                if (value == EditableDiffView.this.horizontalScroll1ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max1 == ext1) {
                    EditableDiffView.this.horizontalScroll2ChangedValue = 0;
                } else {
                    EditableDiffView.this.horizontalScroll2ChangedValue = (int)((long)value * (long)(max2 - ext2) / (long)(max1 - ext1));
                }
                EditableDiffView.this.horizontalScroll1ChangedValue = -1;
                scrollBarH2.setValue(EditableDiffView.this.horizontalScroll2ChangedValue);
            }
        });
        scrollBarH2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH2.getValue();
                if (value == EditableDiffView.this.horizontalScroll2ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max2 == ext2) {
                    EditableDiffView.this.horizontalScroll1ChangedValue = 0;
                } else {
                    EditableDiffView.this.horizontalScroll1ChangedValue = (int)((long)value * (long)(max1 - ext1) / (long)(max2 - ext2));
                }
                EditableDiffView.this.horizontalScroll2ChangedValue = -1;
                scrollBarH1.setValue(EditableDiffView.this.horizontalScroll1ChangedValue);
            }
        });
    }

    private void customizeEditor(JEditorPane editor) {
        Document document = editor.getDocument();
        try {
            StyledDocument doc = (StyledDocument)editor.getDocument();
        }
        catch (ClassCastException e) {
            DefaultStyledDocument doc = new DefaultStyledDocument();
            try {
                doc.insertString(0, document.getText(0, document.getLength()), null);
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
            editor.setDocument(doc);
        }
    }

    private void addChangeListeners() {
        PropertyChangeListener list = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        EditableDiffView.this.diffChanged();
                        EditableDiffView.this.initGlobalSizes();
                        EditableDiffView.this.jEditorPane1.onUISettingsChanged();
                        EditableDiffView.this.getComponent().revalidate();
                        EditableDiffView.this.getComponent().repaint();
                    }
                });
            }

        };
        this.propertyChangeListeners.put(this.jEditorPane1.getEditorPane(), ()list);
        this.jEditorPane1.getEditorPane().addPropertyChangeListener("font", WeakListeners.propertyChange((PropertyChangeListener)list, (Object)this.jEditorPane1.getEditorPane()));
        list = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        EditableDiffView.this.diffChanged();
                        EditableDiffView.this.initGlobalSizes();
                        EditableDiffView.this.jEditorPane2.onUISettingsChanged();
                        EditableDiffView.this.getComponent().revalidate();
                        EditableDiffView.this.getComponent().repaint();
                    }
                });
            }

        };
        this.propertyChangeListeners.put(this.jEditorPane2.getEditorPane(), ()list);
        this.jEditorPane2.getEditorPane().addPropertyChangeListener("font", WeakListeners.propertyChange((PropertyChangeListener)list, (Object)this.jEditorPane2.getEditorPane()));
    }

    private synchronized void diffChanged() {
        ++this.diffSerial;
    }

    private void setSource1(StreamSource ss) throws IOException {
        Document sdoc;
        Document doc;
        this.firstSourceAvailable = false;
        EditorKit kit = this.jEditorPane1.getEditorPane().getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        this.baseDocument = sdoc = this.getSourceDocument(ss);
        Document document = doc = sdoc != null ? sdoc : kit.createDefaultDocument();
        if (!Boolean.TRUE.equals(this.skipFile)) {
            if (this.jEditorPane1.getEditorPane().getUI() instanceof BaseTextUI) {
                if (sdoc == null) {
                    Reader r = ss.createReader();
                    if (r != null) {
                        this.firstSourceAvailable = true;
                        try {
                            kit.read(r, doc, 0);
                        }
                        catch (BadLocationException e) {
                            throw new IOException("Can not locate the beginning of the document.");
                        }
                        finally {
                            r.close();
                        }
                    }
                } else {
                    this.firstSourceAvailable = true;
                }
            } else {
                this.firstSourceUnsupportedTextUI = true;
            }
        }
        this.jEditorPane1.initActions();
        this.jEditorPane1.getEditorPane().setDocument(doc);
        this.customizeEditor(this.jEditorPane1.getEditorPane());
    }

    private Document getSourceDocument(StreamSource ss) {
        Document sdoc;
        block8 : {
            sdoc = null;
            FileObject fo = (FileObject)ss.getLookup().lookup(FileObject.class);
            if (fo != null) {
                try {
                    EditorCookie ec;
                    DataObject dao = DataObject.find((FileObject)fo);
                    if (dao.getPrimaryFile() != fo || (ec = (EditorCookie)dao.getCookie(EditorCookie.class)) == null) break block8;
                    try {
                        sdoc = ec.openDocument();
                    }
                    catch (UserQuestionException ex) {
                        boolean open;
                        boolean bl = open = !Boolean.TRUE.equals(this.skipFile);
                        if (this.skipFile == null) {
                            NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)ex.getLocalizedMessage(), NbBundle.getMessage(EditableDiffView.class, (String)"EditableDiffView.ConfirmOpenningTitle"), 2);
                            open = DialogDisplayer.getDefault().notify((NotifyDescriptor)desc).equals(NotifyDescriptor.OK_OPTION);
                        }
                        if (open) {
                            LOG.log(Level.INFO, "User acepted UQE: {0}", fo.getPath());
                            ex.confirmed();
                            sdoc = ec.openDocument();
                            break block8;
                        }
                        sdoc = null;
                        this.skipFile = true;
                    }
                }
                catch (Exception e) {}
            } else {
                sdoc = (Document)ss.getLookup().lookup(Document.class);
            }
        }
        return sdoc;
    }

    private void setSource2(StreamSource ss) throws IOException {
        DataObject dao;
        Component c;
        Document sdoc;
        Document doc;
        this.secondSourceAvailable = false;
        EditorKit kit = this.jEditorPane2.getEditorPane().getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        this.modifiedDocument = sdoc = this.getSourceDocument(ss);
        if (sdoc != null && ss.isEditable() && (dao = (DataObject)sdoc.getProperty("stream")) != null) {
            EditorCookie cookie;
            if (dao instanceof MultiDataObject) {
                MultiDataObject mdao = (MultiDataObject)dao;
                for (MultiDataObject.Entry entry : mdao.secondaryEntries()) {
                    StyledDocument entryDocument;
                    CookieSet.Factory factory;
                    EditorCookie ec;
                    if (!(entry instanceof CookieSet.Factory) || (entryDocument = (ec = (EditorCookie)(factory = (CookieSet.Factory)entry).createCookie(EditorCookie.class)).getDocument()) != sdoc || !(ec instanceof EditorCookie.Observable)) continue;
                    this.editableCookie = (EditorCookie.Observable)ec;
                    this.editableDocument = sdoc;
                    this.editorUndoRedo = this.getUndoRedo(ec);
                }
            }
            if (this.editableCookie == null && (cookie = (EditorCookie)dao.getCookie(EditorCookie.class)) instanceof EditorCookie.Observable) {
                this.editableCookie = (EditorCookie.Observable)cookie;
                this.editableDocument = sdoc;
                this.editorUndoRedo = this.getUndoRedo(cookie);
            }
        }
        Document document = doc = sdoc != null ? sdoc : kit.createDefaultDocument();
        if (sdoc != null || !Boolean.TRUE.equals(this.skipFile)) {
            if (this.jEditorPane2.getEditorPane().getUI() instanceof BaseTextUI) {
                if (sdoc == null) {
                    Reader r = ss.createReader();
                    if (r != null) {
                        this.secondSourceAvailable = true;
                        try {
                            kit.read(r, doc, 0);
                        }
                        catch (BadLocationException e) {
                            throw new IOException("Can not locate the beginning of the document.");
                        }
                        finally {
                            r.close();
                        }
                    }
                } else {
                    this.secondSourceAvailable = true;
                }
            } else {
                this.secondSourceUnsupportedTextUI = true;
            }
        }
        this.jEditorPane2.initActions();
        this.view.putClientProperty(UndoRedo.class, (Object)this.editorUndoRedo);
        this.jEditorPane2.getEditorPane().setDocument(doc);
        this.jEditorPane2.getEditorPane().setEditable(this.editableCookie != null);
        if (doc instanceof NbDocument.CustomEditor && (c = ((NbDocument.CustomEditor)doc).createEditor((JEditorPane)this.jEditorPane2.getEditorPane())) instanceof JComponent) {
            this.jEditorPane2.setCustomEditor((JComponent)c);
        }
        this.customizeEditor(this.jEditorPane2.getEditorPane());
        this.jViewport2 = this.jEditorPane2.getScrollPane().getViewport();
        this.joinScrollBars();
    }

    private void setTextualContent() {
        if (this.jTabbedPane.getSelectedComponent() == this.textualPanel) {
            this.countTextualDiff();
        } else {
            this.jTabbedPane.addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent e) {
                    if (EditableDiffView.this.jTabbedPane.getSelectedComponent() == EditableDiffView.this.textualPanel) {
                        EditableDiffView.this.jTabbedPane.removeChangeListener(this);
                        EditableDiffView.this.countTextualDiff();
                    }
                }
            });
        }
        this.textualEditorPane.setEditable(false);
    }

    private void countTextualDiff() {
        final EditorKit kit = this.textualEditorPane.getEditorKit();
        rp.post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = EditableDiffView.this.DIFFING_LOCK;
                synchronized (object) {
                    Document doc = kit.createDefaultDocument();
                    doc.putProperty("mimeType", "text/x-diff");
                    StyledDocument sdoc = doc instanceof StyledDocument ? (StyledDocument)doc : null;
                    EditableDiffView.this.textualRefreshTask = new TextualDiffRefreshTask(sdoc);
                    EditableDiffView.this.textualRefreshTask.refresh(EditableDiffView.this.diffs);
                }
            }
        });
    }

    private UndoRedo.Manager getUndoRedo(EditorCookie cookie) {
        try {
            Method method = CloneableEditorSupport.class.getDeclaredMethod("getUndoRedo", new Class[0]);
            method.setAccessible(true);
            return (UndoRedo.Manager)method.invoke((Object)cookie, new Object[0]);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("document".equals(evt.getPropertyName())) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    EditableDiffView.this.refreshEditableDocument();
                }
            });
        }
    }

    public void setSourceTitle(JLabel label, String title) {
        label.setText(title);
        label.setToolTipText(title);
        label.setMinimumSize(new Dimension(3, label.getMinimumSize().height));
    }

    public void setDocument1(Document doc) {
        if (doc != null) {
            this.jEditorPane1.getEditorPane().setDocument(doc);
        }
    }

    public void setDocument2(Document doc) {
        if (doc != null) {
            this.jEditorPane2.getEditorPane().setDocument(doc);
        }
    }

    private void refreshDiff(int delayMillis) {
        this.refreshDiffTask.schedule(delayMillis);
    }

    private Difference[] computeDiff() {
        if (this.editableDocument != null) {
            DataObject dao = (DataObject)this.editableDocument.getProperty("stream");
            if (dao != null) {
                Set files = dao.files();
                if (files != null) {
                    for (FileObject fo : files) {
                        LOG.log(Level.FINE, "refreshing FileOBject {0}", (Object)fo);
                        fo.refresh();
                    }
                } else {
                    LOG.log(Level.FINE, "no FileObjects to refresh for {0}", (Object)dao);
                }
            } else {
                LOG.log(Level.FINE, "no DataObject to refresh");
            }
        }
        if (!this.secondSourceAvailable || !this.firstSourceAvailable) {
            return this.NO_DIFFERENCES;
        }
        Reader first = this.getReader(this.jEditorPane1.getEditorPane().getDocument());
        Reader second = this.getReader(this.jEditorPane2.getEditorPane().getDocument());
        if (first == null || second == null) {
            return this.NO_DIFFERENCES;
        }
        DiffProvider diff = DiffModuleConfig.getDefault().getDefaultDiffProvider();
        try {
            return diff.computeDiff(first, second);
        }
        catch (IOException e) {
            Difference[] diffs = this.NO_DIFFERENCES;
            return diffs;
        }
    }

    private Reader getReader(final Document doc) {
        final Reader[] reader = new Reader[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                try {
                    reader[0] = new StringReader(doc.getText(0, doc.getLength()));
                }
                catch (BadLocationException ex) {
                    LOG.log(Level.INFO, null, ex);
                }
            }
        });
        return reader[0];
    }

    private void repairTextUI(JEditorPane pane) {
        TextUI ui = pane.getUI();
        if (!(ui instanceof BaseTextUI)) {
            pane.setEditorKit(CloneableEditorSupport.getEditorKit((String)"text/plain"));
        }
    }

    private void refreshDividerSize() {
        Font font = this.jSplitPane1.getFont();
        if (font == null) {
            return;
        }
        FontMetrics fm = this.jSplitPane1.getFontMetrics(this.jSplitPane1.getFont());
        String maxDiffNumber = Integer.toString(Math.max(1, this.diffs.length));
        int neededWidth = fm.stringWidth(maxDiffNumber + " /" + maxDiffNumber);
        this.jSplitPane1.setDividerSize(Math.max(neededWidth, 32));
    }

    synchronized int getDiffSerial() {
        return this.diffSerial;
    }

    static Difference getFirstDifference(Difference[] diff, int line) {
        if (line < 0) {
            return null;
        }
        for (int i = 0; i < diff.length; ++i) {
            Difference difference = diff[i];
            if (line < difference.getFirstStart()) {
                return null;
            }
            if (difference.getType() == 1 && line == difference.getFirstStart()) {
                return difference;
            }
            if (line > difference.getFirstEnd()) continue;
            return difference;
        }
        return null;
    }

    static Difference getSecondDifference(Difference[] diff, int line) {
        if (line < 0) {
            return null;
        }
        for (int i = 0; i < diff.length; ++i) {
            Difference difference = diff[i];
            if (line < difference.getSecondStart()) {
                return null;
            }
            if (difference.getType() == 0 && line == difference.getSecondStart()) {
                return difference;
            }
            if (line > difference.getSecondEnd()) continue;
            return difference;
        }
        return null;
    }

    Color getColorLines() {
        return this.colorLines;
    }

    private class EditableDiffMarkProvider
    extends MarkProvider {
        private List<Mark> marks;

        public EditableDiffMarkProvider() {
            this.marks = this.getMarksForDifferences();
        }

        public List<Mark> getMarks() {
            return this.marks;
        }

        void refresh() {
            List<Mark> oldMarks = this.marks;
            this.marks = this.getMarksForDifferences();
            this.firePropertyChange("marks", oldMarks, this.marks);
        }

        private List<Mark> getMarksForDifferences() {
            if (EditableDiffView.this.diffs == null) {
                return Collections.emptyList();
            }
            ArrayList<Mark> retMarks = new ArrayList<Mark>(EditableDiffView.this.diffs.length);
            for (int i = 0; i < EditableDiffView.this.diffs.length; ++i) {
                Difference difference = EditableDiffView.this.diffs[i];
                retMarks.add(new DiffMark(difference, EditableDiffView.this.getColor(difference)));
            }
            return retMarks;
        }
    }

    public class RefreshDiffTask
    implements Runnable {
        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Object object = EditableDiffView.this.DIFFING_LOCK;
            synchronized (object) {
                final Difference[] differences = EditableDiffView.this.computeDiff();
                if (EditableDiffView.this.textualRefreshTask != null) {
                    EditableDiffView.this.textualRefreshTask.refresh(differences);
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        EditableDiffView.this.diffs = differences;
                        if (EditableDiffView.this.diffs != EditableDiffView.this.NO_DIFFERENCES) {
                            EditableDiffView.this.diffChanged();
                        }
                        if (EditableDiffView.this.getDifferenceIndex() >= EditableDiffView.this.diffs.length) {
                            EditableDiffView.this.updateCurrentDifference(null);
                        }
                        EditableDiffView.this.support.firePropertyChange("(void) differencesChanged", null, null);
                        EditableDiffView.this.jEditorPane1.setCurrentDiff(EditableDiffView.this.diffs);
                        EditableDiffView.this.jEditorPane2.setCurrentDiff(EditableDiffView.this.diffs);
                        EditableDiffView.this.refreshDividerSize();
                        EditableDiffView.this.view.repaint();
                        EditableDiffView.this.diffMarkprovider.refresh();
                        if (EditableDiffView.this.diffs.length > 0 && !Boolean.TRUE.equals(EditableDiffView.this.getJComponent().getClientProperty("diff.smartScrollDisabled"))) {
                            if (EditableDiffView.this.askedLineLocation != null) {
                                EditableDiffView.this.setLocation(DiffController.DiffPane.Base, DiffController.LocationType.LineNumber, EditableDiffView.this.askedLineLocation);
                            } else if (EditableDiffView.this.getCurrentDifference() == -1) {
                                EditableDiffView.this.setCurrentDifference(0);
                            }
                        }
                    }
                });
            }
        }

    }

    private class TextualDiffRefreshTask
    implements Cancellable {
        final StyledDocument out;
        private boolean canceled;

        public TextualDiffRefreshTask(StyledDocument out) {
            this.out = out;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void refresh(Difference[] differences) {
            this.canceled = false;
            TextualDiffRefreshTask textualDiffRefreshTask = this;
            synchronized (textualDiffRefreshTask) {
                boolean docReady = false;
                if (this.out != null) {
                    try {
                        this.exportDiff(differences);
                        docReady = true;
                    }
                    catch (IOException ex) {
                        Logger.getLogger(EditableDiffView.class.getName()).log(Level.INFO, null, ex);
                    }
                }
                if (this.isCanceled()) {
                    return;
                }
                final boolean textualDiffReady = docReady;
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (textualDiffReady) {
                            EditableDiffView.this.textualEditorPane.setDocument(TextualDiffRefreshTask.this.out);
                            EditableDiffView.this.textualEditorPane.setCaretPosition(0);
                        } else {
                            EditableDiffView.this.textualPanel.remove(EditableDiffView.this.textualEditorPane);
                            NoContentPanel ncp = new NoContentPanel(NbBundle.getMessage(EditableDiffView.class, (String)"CTL_DiffPanel_NoContent"));
                            EditableDiffView.this.textualPanel.add(ncp);
                        }
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        private void exportDiff(Difference[] differences) throws IOException {
            Reader r1 = null;
            Reader r2 = null;
            if (differences == null) {
                differences = EditableDiffView.this.computeDiff();
            }
            try {
                r1 = EditableDiffView.this.getReader(EditableDiffView.this.jEditorPane1.getEditorPane().getDocument());
                if (r1 == null) {
                    r1 = new StringReader("");
                }
                if (this.isCanceled()) {
                    return;
                }
                r2 = EditableDiffView.this.getReader(EditableDiffView.this.jEditorPane2.getEditorPane().getDocument());
                if (r2 == null) {
                    r2 = new StringReader("");
                }
                if (this.isCanceled()) {
                    return;
                }
                TextDiffVisualizer.TextDiffInfo info = new TextDiffVisualizer.TextDiffInfo(EditableDiffView.this.fileLabel1.getText(), EditableDiffView.this.fileLabel2.getText(), null, null, r1, r2, differences);
                info.setContextMode(true, 3);
                final String diffText = TextDiffVisualizer.differenceToUnifiedDiffText(info);
                if (this.isCanceled()) {
                    return;
                }
                NbDocument.runAtomic((StyledDocument)this.out, (Runnable)new Runnable(){

                    @Override
                    public void run() {
                        String sep = System.getProperty("line.separator");
                        try {
                            TextualDiffRefreshTask.this.out.remove(0, TextualDiffRefreshTask.this.out.getLength());
                            TextualDiffRefreshTask.this.out.insertString(0, "# This patch file was generated by NetBeans IDE" + sep + "# It uses platform neutral UTF-8 encoding and \\n newlines." + sep + diffText, null);
                        }
                        catch (BadLocationException ex) {
                            Logger.getLogger(EditableDiffView.class.getName()).log(Level.WARNING, null, ex);
                        }
                    }
                });
                return;
            }
            finally {
                if (r1 != null) {
                    try {
                        r1.close();
                    }
                    catch (Exception e) {}
                }
                if (r2 != null) {
                    try {
                        r2.close();
                    }
                    catch (Exception e) {}
                }
            }
        }

        public boolean cancel() {
            this.canceled = true;
            return true;
        }

        boolean isCanceled() {
            return this.canceled;
        }

    }

    class DiffSplitPaneUI
    extends BasicSplitPaneUI {
        final DiffSplitPaneDivider splitPaneDivider;

        public DiffSplitPaneUI(JSplitPane splitPane) {
            this.splitPane = splitPane;
            this.splitPaneDivider = new DiffSplitPaneDivider(this, EditableDiffView.this);
        }

        @Override
        public BasicSplitPaneDivider createDefaultDivider() {
            return this.splitPaneDivider;
        }
    }

}

