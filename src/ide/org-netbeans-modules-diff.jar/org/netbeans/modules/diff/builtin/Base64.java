/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin;

import java.io.ByteArrayOutputStream;
import java.util.List;

class Base64 {
    private Base64() {
    }

    public static byte[] decode(List<String> ls) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        for (String s : ls) {
            Base64.decode(s, bos);
        }
        return bos.toByteArray();
    }

    private static void decode(String s, ByteArrayOutputStream bos) {
        int i = 0;
        int len = s.length();
        do {
            if (i < len && s.charAt(i) <= ' ') {
                ++i;
                continue;
            }
            if (i == len) break;
            int tri = (Base64.decode(s.charAt(i)) << 18) + (Base64.decode(s.charAt(i + 1)) << 12) + (Base64.decode(s.charAt(i + 2)) << 6) + Base64.decode(s.charAt(i + 3));
            bos.write(tri >> 16 & 255);
            if (s.charAt(i + 2) == '=') break;
            bos.write(tri >> 8 & 255);
            if (s.charAt(i + 3) == '=') break;
            bos.write(tri & 255);
            i += 4;
        } while (true);
    }

    private static int decode(char c) {
        if (c >= 'A' && c <= 'Z') {
            return c - 65;
        }
        if (c >= 'a' && c <= 'z') {
            return c - 97 + 26;
        }
        if (c >= '0' && c <= '9') {
            return c - 48 + 26 + 26;
        }
        switch (c) {
            case '+': {
                return 62;
            }
            case '/': {
                return 63;
            }
            case '=': {
                return 0;
            }
        }
        throw new RuntimeException("unexpected code: " + c);
    }
}

