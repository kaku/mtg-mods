/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.LifecycleManager
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.UndoRedo
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.diff.builtin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle;
import org.netbeans.api.diff.DiffController;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.api.queries.FileEncodingQuery;
import org.netbeans.modules.diff.Utils;
import org.netbeans.modules.diff.builtin.ExportPatch;
import org.openide.LifecycleManager;
import org.openide.awt.Mnemonics;
import org.openide.awt.UndoRedo;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;

public class SingleDiffPanel
extends JPanel
implements PropertyChangeListener {
    private FileObject base;
    private FileObject modified;
    private final FileObject type;
    private DiffController controller;
    private Action nextAction;
    private Action prevAction;
    private JComponent innerPanel;
    private FileChangeListener baseFCL;
    private FileChangeListener modifiedFCL;
    private JToolBar actionsToolbar;
    private JButton bExport;
    private JButton bNext;
    private JButton bOptions;
    private JButton bPrevious;
    private JButton bRefresh;
    private JButton bSwap;
    private JPanel controllerPanel;
    private JToolBar.Separator jSeparator1;

    public SingleDiffPanel(FileObject left, FileObject right, FileObject type) throws IOException {
        this.base = left;
        this.modified = right;
        this.type = type;
        this.setListeners();
        this.initComponents();
        this.initMyComponents();
        this.refreshComponents();
    }

    private void initMyComponents() throws IOException {
        this.actionsToolbar.add(Box.createHorizontalGlue(), 0);
        this.actionsToolbar.add(Box.createHorizontalGlue());
        this.nextAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                SingleDiffPanel.this.onNext();
            }
        };
        this.bNext.setAction(this.nextAction);
        this.bNext.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/diff/builtin/visualizer/editable/diff-next.png")));
        Mnemonics.setLocalizedText((AbstractButton)this.bNext, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bNext.text"));
        this.bNext.setToolTipText(NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bNext.toolTipText"));
        this.prevAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                SingleDiffPanel.this.onPrev();
            }
        };
        this.bPrevious.setAction(this.prevAction);
        this.bPrevious.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/diff/builtin/visualizer/editable/diff-prev.png")));
        Mnemonics.setLocalizedText((AbstractButton)this.bPrevious, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bPrevious.text"));
        this.bPrevious.setToolTipText(NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bPrevious.toolTipText"));
        this.getActionMap().put("jumpNext", this.nextAction);
        this.getActionMap().put("jumpPrev", this.prevAction);
        this.refreshController();
    }

    private void refreshController() throws IOException {
        if (this.controller != null) {
            this.controller.removePropertyChangeListener(this);
            this.addPropertyChangeListener(this);
        }
        this.base.refresh();
        this.modified.refresh();
        DiffStreamSource ss1 = new DiffStreamSource(this.base, this.type, false);
        DiffStreamSource ss2 = new DiffStreamSource(this.modified, this.type, true);
        this.controller = DiffController.createEnhanced(ss1, ss2);
        this.controller.addPropertyChangeListener(this);
        this.controllerPanel.removeAll();
        this.innerPanel = this.controller.getJComponent();
        this.controllerPanel.add(this.innerPanel);
        this.setName(this.innerPanel.getName());
        Container c = this.getParent();
        if (c != null) {
            c.setName(this.getName());
        }
        this.activateNodes();
        this.revalidate();
        this.repaint();
    }

    public void activateNodes() {
        TopComponent tc = (TopComponent)this.getClientProperty(TopComponent.class);
        if (tc != null) {
            Node node;
            try {
                DataObject dobj = DataObject.find((FileObject)this.modified);
                node = dobj.getNodeDelegate();
            }
            catch (DataObjectNotFoundException e) {
                node = new AbstractNode(Children.LEAF, Lookups.singleton((Object)this.modified));
            }
            tc.setActivatedNodes(new Node[]{node});
        }
    }

    public UndoRedo getUndoRedo() {
        UndoRedo undoRedo = null;
        if (this.innerPanel != null) {
            undoRedo = (UndoRedo)this.innerPanel.getClientProperty(UndoRedo.class);
        }
        if (undoRedo == null) {
            undoRedo = UndoRedo.NONE;
        }
        return undoRedo;
    }

    public void requestActive() {
        if (this.controllerPanel != null) {
            this.controllerPanel.requestFocusInWindow();
        }
    }

    private void onPrev() {
        int idx = this.controller.getDifferenceIndex();
        if (idx > 0) {
            this.controller.setLocation(DiffController.DiffPane.Modified, DiffController.LocationType.DifferenceIndex, idx - 1);
        }
    }

    private void onNext() {
        int idx = this.controller.getDifferenceIndex();
        if (idx < this.controller.getDifferenceCount() - 1) {
            this.controller.setLocation(DiffController.DiffPane.Modified, DiffController.LocationType.DifferenceIndex, idx + 1);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.refreshComponents();
    }

    private void refreshComponents() {
        this.nextAction.setEnabled(this.controller.getDifferenceIndex() < this.controller.getDifferenceCount() - 1);
        this.prevAction.setEnabled(this.controller.getDifferenceIndex() > 0);
    }

    private void setListeners() {
        FileObject baseParent = this.base.getParent();
        FileObject modifiedParent = this.modified.getParent();
        if (baseParent != null) {
            this.baseFCL = new DiffFileChangeListener();
            baseParent.addFileChangeListener((FileChangeListener)WeakListeners.create(FileChangeListener.class, (EventListener)this.baseFCL, (Object)baseParent));
        }
        if (baseParent != modifiedParent && modifiedParent != null) {
            this.modifiedFCL = new DiffFileChangeListener();
            modifiedParent.addFileChangeListener((FileChangeListener)WeakListeners.create(FileChangeListener.class, (EventListener)this.modifiedFCL, (Object)modifiedParent));
        }
    }

    private void initComponents() {
        this.actionsToolbar = new JToolBar();
        this.bNext = new JButton();
        this.bPrevious = new JButton();
        this.jSeparator1 = new JToolBar.Separator();
        this.bRefresh = new JButton();
        this.bSwap = new JButton();
        this.bExport = new JButton();
        this.bOptions = new JButton();
        this.controllerPanel = new JPanel();
        this.actionsToolbar.setFloatable(false);
        this.actionsToolbar.setRollover(true);
        Mnemonics.setLocalizedText((AbstractButton)this.bNext, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bNext.text"));
        this.bNext.setFocusable(false);
        this.bNext.setHorizontalTextPosition(0);
        this.bNext.setVerticalTextPosition(3);
        this.actionsToolbar.add(this.bNext);
        Mnemonics.setLocalizedText((AbstractButton)this.bPrevious, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bPrevious.text"));
        this.bPrevious.setFocusable(false);
        this.bPrevious.setHorizontalTextPosition(0);
        this.bPrevious.setVerticalTextPosition(3);
        this.actionsToolbar.add(this.bPrevious);
        this.actionsToolbar.add(this.jSeparator1);
        this.bRefresh.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/diff/builtin/visualizer/editable/diff-refresh.png")));
        Mnemonics.setLocalizedText((AbstractButton)this.bRefresh, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bRefresh.text"));
        this.bRefresh.setToolTipText(NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bRefresh.toolTipText"));
        this.bRefresh.setFocusable(false);
        this.bRefresh.setHorizontalTextPosition(0);
        this.bRefresh.setVerticalTextPosition(3);
        this.bRefresh.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                SingleDiffPanel.this.bRefreshActionPerformed(evt);
            }
        });
        this.actionsToolbar.add(this.bRefresh);
        Mnemonics.setLocalizedText((AbstractButton)this.bSwap, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bSwap.text"));
        this.bSwap.setFocusable(false);
        this.bSwap.setHorizontalTextPosition(0);
        this.bSwap.setVerticalTextPosition(3);
        this.bSwap.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                SingleDiffPanel.this.bSwapActionPerformed(evt);
            }
        });
        this.actionsToolbar.add(this.bSwap);
        Mnemonics.setLocalizedText((AbstractButton)this.bExport, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bExport.text"));
        this.bExport.setFocusable(false);
        this.bExport.setHorizontalTextPosition(0);
        this.bExport.setVerticalTextPosition(3);
        this.bExport.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                SingleDiffPanel.this.bExportActionPerformed(evt);
            }
        });
        this.actionsToolbar.add(this.bExport);
        Mnemonics.setLocalizedText((AbstractButton)this.bOptions, (String)NbBundle.getMessage(SingleDiffPanel.class, (String)"SingleDiffPanel.bOptions.text"));
        this.bOptions.setFocusable(false);
        this.bOptions.setHorizontalTextPosition(0);
        this.bOptions.setVerticalTextPosition(3);
        this.bOptions.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                SingleDiffPanel.this.bOptionsActionPerformed(evt);
            }
        });
        this.actionsToolbar.add(this.bOptions);
        this.controllerPanel.setLayout(new BorderLayout());
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.controllerPanel, -1, 531, 32767).addComponent(this.actionsToolbar, -1, 531, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.actionsToolbar, -2, 25, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.controllerPanel, -1, 371, 32767)));
    }

    private void bRefreshActionPerformed(ActionEvent evt) {
        LifecycleManager.getDefault().saveAll();
        try {
            this.refreshController();
        }
        catch (IOException e) {
            Logger.getLogger(SingleDiffPanel.class.getName()).log(Level.SEVERE, "", e);
        }
    }

    private void bSwapActionPerformed(ActionEvent evt) {
        LifecycleManager.getDefault().saveAll();
        FileObject temp = this.base;
        this.base = this.modified;
        this.modified = temp;
        try {
            this.refreshController();
        }
        catch (IOException e) {
            Logger.getLogger(SingleDiffPanel.class.getName()).log(Level.SEVERE, "", e);
        }
    }

    private void bExportActionPerformed(ActionEvent evt) {
        DiffStreamSource ss1 = new DiffStreamSource(this.base, this.type, false);
        DiffStreamSource ss2 = new DiffStreamSource(this.modified, this.type, true);
        ExportPatch.exportPatch(new StreamSource[]{ss1}, new StreamSource[]{ss2});
    }

    private void bOptionsActionPerformed(ActionEvent evt) {
        OptionsDisplayer.getDefault().open("Advanced/Diff");
    }

    private static class DiffStreamSource
    extends StreamSource {
        private final FileObject fileObject;
        private final FileObject type;
        private final boolean isRight;

        public DiffStreamSource(FileObject fileObject, FileObject type, boolean isRight) {
            this.fileObject = fileObject;
            this.type = type;
            this.isRight = isRight;
        }

        @Override
        public boolean isEditable() {
            return this.isRight && this.fileObject.canWrite();
        }

        @Override
        public Lookup getLookup() {
            return Lookups.fixed((Object[])new Object[]{this.fileObject});
        }

        @Override
        public String getName() {
            return this.fileObject.getName();
        }

        @Override
        public String getTitle() {
            return FileUtil.getFileDisplayName((FileObject)this.fileObject);
        }

        @Override
        public String getMIMEType() {
            if (this.type != null) {
                if (Utils.isFileContentBinary(this.type)) {
                    return null;
                }
                return this.type.getMIMEType();
            }
            if (Utils.isFileContentBinary(this.fileObject)) {
                return null;
            }
            return this.fileObject.getMIMEType();
        }

        @Override
        public Reader createReader() throws IOException {
            if (this.type != null) {
                return new InputStreamReader(this.fileObject.getInputStream(), FileEncodingQuery.getEncoding((FileObject)this.type));
            }
            return new InputStreamReader(this.fileObject.getInputStream(), FileEncodingQuery.getEncoding((FileObject)this.fileObject));
        }

        @Override
        public Writer createWriter(Difference[] conflicts) throws IOException {
            return null;
        }
    }

    private class DiffFileChangeListener
    extends FileChangeAdapter {
        private DiffFileChangeListener() {
        }

        public void fileRenamed(FileRenameEvent fe) {
            if (fe.getFile() == SingleDiffPanel.this.base || fe.getFile() == SingleDiffPanel.this.modified) {
                this.refreshFiles();
            }
        }

        private void refreshFiles() {
            EventQueue.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        SingleDiffPanel.this.refreshController();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(SingleDiffPanel.class.getName()).log(Level.SEVERE, "", ex);
                    }
                }
            });
        }

    }

}

