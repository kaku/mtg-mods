/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;

class NoContentPanel
extends JPanel {
    private JLabel label = new JLabel();

    public NoContentPanel(String text) {
        this();
        this.label.setText(text);
    }

    public NoContentPanel() {
        this.setBackground(UIManager.getColor("TextArea.background"));
        this.setLayout(new BorderLayout());
        this.label.setHorizontalAlignment(0);
        this.add((Component)this.label, "Center");
        this.label.setEnabled(false);
        this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    }

    public void setLabel(String text) {
        this.label.setText(text);
    }
}

