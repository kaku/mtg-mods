/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.Rectangle;
import org.netbeans.api.diff.Difference;

class HotSpot {
    private Rectangle rect;
    private Difference diff;

    public HotSpot(Rectangle rect, Difference diff) {
        this.rect = rect;
        this.diff = diff;
    }

    public Rectangle getRect() {
        return this.rect;
    }

    public Difference getDiff() {
        return this.diff;
    }
}

