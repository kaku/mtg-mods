/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin;

import java.io.IOException;

public final class PatchException
extends IOException {
    public PatchException(String msg) {
        super(msg);
    }
}

