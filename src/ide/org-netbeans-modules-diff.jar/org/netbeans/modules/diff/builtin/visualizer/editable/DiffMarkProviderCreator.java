/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider
 *  org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator;

public class DiffMarkProviderCreator
implements MarkProviderCreator {
    static final String MARK_PROVIDER_KEY = "org.netbeans.modules.diff.builtin.visualizer.editable.MarkProvider";

    public MarkProvider createMarkProvider(JTextComponent component) {
        return (MarkProvider)component.getClientProperty("org.netbeans.modules.diff.builtin.visualizer.editable.MarkProvider");
    }
}

