/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin;

import java.util.ArrayList;
import java.util.List;

public final class Hunk {
    public static final String ENDING_NEWLINE = "\\ No newline at end of file";
    public int baseStart;
    public int baseCount;
    public int modifiedStart;
    public int modifiedCount;
    public List<String> lines = new ArrayList<String>();
}

