/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.EditorCookie$Observable
 *  org.openide.cookies.OpenCookie
 *  org.openide.cookies.PrintCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.text.CloneableEditor
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Env
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableOpenSupport$Env
 *  org.openide.windows.CloneableTopComponent
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.VetoableChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Date;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.visualizer.TextDiffVisualizer;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.cookies.PrintCookie;
import org.openide.filesystems.FileObject;
import org.openide.text.CloneableEditor;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;

public class TextDiffEditorSupport
extends CloneableEditorSupport
implements EditorCookie.Observable,
OpenCookie,
PrintCookie,
CloseCookie {
    private final TextDiffVisualizer.TextDiffInfo diff;

    TextDiffEditorSupport(TextDiffVisualizer.TextDiffInfo diff) {
        super((CloneableEditorSupport.Env)new Env(diff));
        this.diff = diff;
    }

    public final FileObject getFileObject() {
        return null;
    }

    protected String messageOpening() {
        return NbBundle.getMessage(TextDiffEditorSupport.class, (String)"CTL_ObjectOpen", (Object)this.diff.getName());
    }

    protected String messageOpened() {
        return NbBundle.getMessage(TextDiffEditorSupport.class, (String)"CTL_ObjectOpened", (Object)this.diff.getName());
    }

    protected String messageSave() {
        return "";
    }

    protected String messageName() {
        return this.diff.getName();
    }

    protected String messageToolTip() {
        return this.diff.getTitle();
    }

    protected void initializeCloneableEditor(CloneableEditor editor) {
        editor.setIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/diff/diffSettingsIcon.gif", (boolean)true));
    }

    protected CloneableEditor createCloneableEditor() {
        return new DiffCloneableEditor(this);
    }

    protected StyledDocument createStyledDocument(EditorKit kit) {
        StyledDocument doc = super.createStyledDocument(kit);
        doc.putProperty("title", this.diff.getName());
        return doc;
    }

    CloneableTopComponent createCloneableTopComponentForMe() {
        return this.createCloneableTopComponent();
    }

    public static class DiffCloneableEditor
    extends CloneableEditor {
        private boolean componentShowingCalled = false;

        DiffCloneableEditor(CloneableEditorSupport support) {
            super(support);
        }

        public void addNotify() {
            this.componentShowing();
            super.addNotify();
        }

        protected void componentShowing() {
            if (!this.componentShowingCalled) {
                super.componentShowing();
                this.componentShowingCalled = true;
            }
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx(TextDiffEditorSupport.class);
        }
    }

    public static class Env
    implements CloneableOpenSupport.Env,
    CloneableEditorSupport.Env,
    Serializable {
        static final long serialVersionUID = -2945098431098324441L;
        private transient TextDiffVisualizer.TextDiffInfo diff;

        public Env(TextDiffVisualizer.TextDiffInfo diff) {
            this.diff = diff;
        }

        public InputStream inputStream() throws IOException {
            if (this.diff.isContextMode()) {
                String diffText = TextDiffVisualizer.differenceToUnifiedDiffText(this.diff);
                return new ByteArrayInputStream(diffText.getBytes("utf8"));
            }
            return TextDiffVisualizer.differenceToLineDiffText(this.diff.getDifferences());
        }

        public OutputStream outputStream() throws IOException {
            throw new IOException("No output to a file diff supported.");
        }

        public String getMimeType() {
            return "text/plain";
        }

        public void markModified() throws IOException {
            throw new IOException("The file revision can not be modified.");
        }

        public void unmarkModified() {
        }

        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        }

        public boolean isModified() {
            return false;
        }

        public Date getTime() {
            return new Date(System.currentTimeMillis());
        }

        public void removeVetoableChangeListener(VetoableChangeListener vetoableChangeListener) {
        }

        public boolean isValid() {
            return true;
        }

        public void addVetoableChangeListener(VetoableChangeListener vetoableChangeListener) {
        }

        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        }

        public CloneableOpenSupport findCloneableOpenSupport() {
            return this.diff.getOpenSupport();
        }
    }

}

