/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import javax.swing.text.JTextComponent;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

public class DiffHighlightsLayerFactory
implements HighlightsLayerFactory {
    static final String HIGHLITING_LAYER_ID = "org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel";

    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        DiffContentPanel master = (DiffContentPanel)context.getComponent().getClientProperty("org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel");
        if (master == null) {
            return null;
        }
        HighlightsLayer[] layers = new HighlightsLayer[]{HighlightsLayer.create((String)"org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel", (ZOrder)ZOrder.DEFAULT_RACK, (boolean)true, (HighlightsContainer)master.getHighlightsContainer())};
        return layers;
    }
}

