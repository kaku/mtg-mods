/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.ErrorManager
 *  org.openide.awt.Mnemonics
 *  org.openide.explorer.propertysheet.DefaultPropertyModel
 *  org.openide.explorer.propertysheet.PropertyModel
 *  org.openide.explorer.propertysheet.PropertyPanel
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.nodes.Node
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.diff.builtin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.Reader;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.spi.diff.DiffProvider;
import org.netbeans.spi.diff.DiffVisualizer;
import org.openide.ErrorManager;
import org.openide.awt.Mnemonics;
import org.openide.explorer.propertysheet.DefaultPropertyModel;
import org.openide.explorer.propertysheet.PropertyModel;
import org.openide.explorer.propertysheet.PropertyPanel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.loaders.InstanceDataObject;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.windows.TopComponent;

public class DiffPresenter
extends JPanel {
    public static final String PROP_PROVIDER = "provider";
    public static final String PROP_VISUALIZER = "visualizer";
    public static final String PROP_TOOLBAR = "DiffPresenter.toolbarPanel";
    private Info diffInfo;
    private DiffProvider defaultProvider;
    private DiffVisualizer defaultVisualizer;
    private JComponent progressPanel;
    private final RequestProcessor diffRP = new RequestProcessor("Diff", 1, true);
    private RequestProcessor.Task computationTask;
    private boolean added;
    JPanel jPanel1;
    JLabel providerLabel;
    JPanel servicesPanel;
    JPanel toolbarPanel;
    JLabel visualizerLabel;
    JPanel visualizerPanel;

    public DiffPresenter() {
        this.computationTask = this.diffRP.post(new Runnable(){

            @Override
            public void run() {
            }
        });
        String label = NbBundle.getMessage(DiffPresenter.class, (String)"diff.prog");
        ProgressHandle progress = ProgressHandleFactory.createHandle((String)label);
        this.progressPanel = ProgressHandleFactory.createProgressComponent((ProgressHandle)progress);
        this.add(this.progressPanel);
        progress.start();
    }

    public DiffPresenter(Info diffInfo) {
        this.computationTask = this.diffRP.post(new );
        this.initWithDiffInfo(diffInfo);
    }

    public final void initWithDiffInfo(Info diffInfo) {
        assert (this.diffInfo == null);
        this.diffInfo = diffInfo;
        if (this.progressPanel != null) {
            this.remove(this.progressPanel);
        }
        this.initComponents();
        this.initMyComponents();
        this.providerLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPresenter.class, (String)"ACS_ProviderA11yDesc"));
        this.visualizerLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPresenter.class, (String)"ACS_VisualizerA11yDesc"));
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.toolbarPanel = new JPanel();
        this.servicesPanel = new JPanel();
        this.providerLabel = new JLabel();
        this.visualizerLabel = new JLabel();
        this.visualizerPanel = new JPanel();
        this.setLayout(new GridBagLayout());
        this.toolbarPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.toolbarPanel.setLayout(new FlowLayout(1, 0, 0));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        this.add((Component)this.toolbarPanel, gridBagConstraints);
        this.servicesPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.providerLabel, (String)NbBundle.getMessage(DiffPresenter.class, (String)"LBL_Provider"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        this.servicesPanel.add((Component)this.providerLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.visualizerLabel, (String)NbBundle.getMessage(DiffPresenter.class, (String)"LBL_Visualizer"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        this.servicesPanel.add((Component)this.visualizerLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.servicesPanel, gridBagConstraints);
        this.visualizerPanel.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        this.visualizerPanel.setLayout(new BorderLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.visualizerPanel, gridBagConstraints);
    }

    private void initMyComponents() {
        PropertyDescriptor pd;
        DefaultPropertyModel model;
        PropertyPanel panel;
        GridBagConstraints gridBagConstraints;
        FileObject services = FileUtil.getConfigFile((String)"Services");
        DataFolder df = DataFolder.findFolder((FileObject)services);
        PropertyEditor editor = PropertyEditorManager.findEditor(Object.class);
        if (this.diffInfo.isChooseProviders() && editor != null) {
            try {
                pd = new PropertyDescriptor("provider", this.getClass());
            }
            catch (IntrospectionException intrex) {
                return;
            }
            pd.setPropertyEditorClass(editor.getClass());
            pd.setValue("superClass", DiffProvider.class);
            pd.setValue("suppressCustomEditor", Boolean.TRUE);
            FileObject providersFO = services.getFileObject("DiffProviders");
            try {
                DataObject providersDO = DataObject.find((FileObject)providersFO);
                Node providersNode = providersDO.getNodeDelegate();
                pd.setValue("node", (Object)providersNode);
            }
            catch (DataObjectNotFoundException donfex) {
                // empty catch block
            }
            pd.setValue("helpID", "org.netbeans.modules.diff.DiffPresenter.providers");
            model = new DefaultPropertyModel((Object)this, pd);
            panel = new PropertyPanel((PropertyModel)model, 4);
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(0, 0, 0, 15);
            gridBagConstraints.anchor = 17;
            if (!this.diffInfo.isChooseVisualizers()) {
                gridBagConstraints.weightx = 1.0;
            }
            gridBagConstraints.gridx = 1;
            this.servicesPanel.add((Component)panel, gridBagConstraints);
            this.providerLabel.setLabelFor((Component)panel);
            panel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffPresenter.class, (String)"ACS_ProviderPropertyPanelA11yName"));
            panel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPresenter.class, (String)"ACS_ProviderPropertyPanelA11yDesc"));
        }
        if (this.diffInfo.isChooseVisualizers() && editor != null) {
            try {
                pd = new PropertyDescriptor("visualizer", this.getClass());
            }
            catch (IntrospectionException intrex) {
                return;
            }
            pd.setPropertyEditorClass(editor.getClass());
            pd.setValue("superClass", DiffVisualizer.class);
            pd.setValue("suppressCustomEditor", Boolean.TRUE);
            FileObject visualizersFO = services.getFileObject("DiffVisualizers");
            try {
                DataObject visualizersDO = DataObject.find((FileObject)visualizersFO);
                Node visualizersNode = visualizersDO.getNodeDelegate();
                pd.setValue("node", (Object)visualizersNode);
            }
            catch (DataObjectNotFoundException donfex) {
                // empty catch block
            }
            pd.setValue("helpID", "org.netbeans.modules.diff.DiffPresenter.visualizers");
            model = new DefaultPropertyModel((Object)this, pd);
            panel = new PropertyPanel((PropertyModel)model, 4);
            panel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffPresenter.class, (String)"ACS_VisualizerPropertyPanelA11yName"));
            panel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPresenter.class, (String)"ACS_VisualizerPropertyPanelA11yDesc"));
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(0, 0, 0, 5);
            gridBagConstraints.anchor = 17;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.gridx = 3;
            this.servicesPanel.add((Component)panel, gridBagConstraints);
            this.visualizerLabel.setLabelFor((Component)panel);
        }
        this.providerLabel.setVisible(this.diffInfo.isChooseProviders() && editor != null);
        this.visualizerLabel.setVisible(this.diffInfo.isChooseVisualizers() && editor != null);
        this.servicesPanel.setVisible((this.diffInfo.isChooseProviders() || this.diffInfo.isChooseVisualizers()) && editor != null);
    }

    public DiffProvider getProvider() {
        return this.defaultProvider;
    }

    public void setProvider(DiffProvider p) {
        this.defaultProvider = p;
        if (this.added) {
            this.asyncDiff(p, this.defaultVisualizer);
            DiffPresenter.setDefaultDiffService(p, "Services/DiffProviders");
        }
    }

    public DiffVisualizer getVisualizer() {
        return this.defaultVisualizer;
    }

    public void setVisualizer(DiffVisualizer v) {
        this.defaultVisualizer = v;
        if (this.added) {
            this.asyncDiff(this.defaultProvider, v);
            DiffPresenter.setDefaultDiffService(v, "Services/DiffVisualizers");
        }
    }

    private static void setDefaultDiffService(Object ds, String folder) {
        FileObject services = FileUtil.getConfigFile((String)folder);
        DataFolder df = DataFolder.findFolder((FileObject)services);
        DataObject[] children = df.getChildren();
        for (int i = 0; i < children.length; ++i) {
            InstanceDataObject ido;
            if (!(children[i] instanceof InstanceDataObject) || !(ido = (InstanceDataObject)children[i]).instanceOf(ds.getClass())) continue;
            try {
                if (!ds.equals(ido.instanceCreate())) continue;
                df.setOrder(new DataObject[]{ido});
                break;
            }
            catch (IOException ioex) {
                continue;
            }
            catch (ClassNotFoundException cnfex) {
                // empty catch block
            }
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.added = true;
        this.asyncDiff(this.defaultProvider, this.defaultVisualizer);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.computationTask.cancel();
    }

    private synchronized void asyncDiff(final DiffProvider p, final DiffVisualizer v) {
        Difference[] diffs;
        if (v == null) {
            return;
        }
        if (p != null) {
            diffs = this.diffInfo.getInitialDifferences();
            if (diffs == null) {
                JPanel panel = new JPanel();
                panel.setLayout(new BorderLayout());
                String message = NbBundle.getMessage(DiffPresenter.class, (String)"BK0001");
                JLabel label = new JLabel(message);
                label.setHorizontalAlignment(0);
                panel.add((Component)label, "Center");
                this.setVisualizer(panel);
            }
        } else {
            diffs = this.diffInfo.getDifferences();
        }
        final Difference[] fdiffs = diffs;
        Runnable computation = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    Difference[] adiffs = fdiffs;
                    String message = NbBundle.getMessage(DiffPresenter.class, (String)"BK0001");
                    ProgressHandle ph = ProgressHandleFactory.createHandle((String)message);
                    if (adiffs == null) {
                        try {
                            ph.start();
                            adiffs = p.computeDiff(DiffPresenter.this.diffInfo.createFirstReader(), DiffPresenter.this.diffInfo.createSecondReader());
                        }
                        finally {
                            ph.finish();
                        }
                    }
                    if (adiffs == null) {
                        return;
                    }
                    final Difference[] fadiffs = adiffs;
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                DiffPresenter.this.viewVisualizer(v, fadiffs);
                            }
                            catch (IOException ioex) {
                                ErrorManager.getDefault().notify(256, (Throwable)ioex);
                            }
                        }
                    });
                }
                catch (InterruptedIOException ex) {
                    ErrorManager.getDefault().notify(1, (Throwable)ex);
                }
                catch (IOException ex) {
                    ErrorManager.getDefault().notify(256, (Throwable)ex);
                }
            }

        };
        this.computationTask.cancel();
        this.computationTask = this.diffRP.post(computation);
    }

    private void viewVisualizer(DiffVisualizer v, Difference[] diffs) throws IOException {
        assert (SwingUtilities.isEventDispatchThread());
        Component c = v.createView(diffs, this.diffInfo.getName1(), this.diffInfo.getTitle1(), this.diffInfo.createFirstReader(), this.diffInfo.getName2(), this.diffInfo.getTitle2(), this.diffInfo.createSecondReader(), this.diffInfo.getMimeType());
        this.setVisualizer((JComponent)c);
        TopComponent tp = this.diffInfo.getPresentingComponent();
        if (tp != null) {
            tp.setName(c.getName());
            if (c instanceof TopComponent) {
                TopComponent vtp = (TopComponent)c;
                tp.setToolTipText(vtp.getToolTipText());
                tp.setIcon(vtp.getIcon());
            }
        }
        c.requestFocus();
    }

    private void setVisualizer(JComponent visualizer) {
        this.visualizerPanel.removeAll();
        if (visualizer != null) {
            this.toolbarPanel.removeAll();
            JComponent toolbar = (JComponent)visualizer.getClientProperty("DiffPresenter.toolbarPanel");
            if (toolbar != null) {
                this.toolbarPanel.add(toolbar);
            }
            this.visualizerPanel.add((Component)visualizer, "Center");
        }
        this.revalidate();
        this.repaint();
    }

    public static abstract class Info {
        private String name1;
        private String name2;
        private String title1;
        private String title2;
        private String mimeType;
        private boolean chooseProviders;
        private boolean chooseVisualizers;
        private TopComponent tp;

        public Info(String name1, String name2, String title1, String title2, String mimeType, boolean chooseProviders, boolean chooseVisualizers) {
            this.name1 = name1;
            this.name2 = name2;
            this.title1 = title1;
            this.title2 = title2;
            this.mimeType = mimeType;
            this.chooseProviders = chooseProviders;
            this.chooseVisualizers = chooseVisualizers;
        }

        public String getName1() {
            return this.name1;
        }

        public String getName2() {
            return this.name2;
        }

        public String getTitle1() {
            return this.title1;
        }

        public String getTitle2() {
            return this.title2;
        }

        public String getMimeType() {
            return this.mimeType;
        }

        public boolean isChooseProviders() {
            return this.chooseProviders;
        }

        public boolean isChooseVisualizers() {
            return this.chooseVisualizers;
        }

        public Difference[] getDifferences() {
            return null;
        }

        public Difference[] getInitialDifferences() {
            return null;
        }

        public abstract Reader createFirstReader() throws FileNotFoundException;

        public abstract Reader createSecondReader() throws FileNotFoundException;

        public void setPresentingComponent(TopComponent tp) {
            this.tp = tp;
        }

        public TopComponent getPresentingComponent() {
            return this.tp;
        }
    }

}

