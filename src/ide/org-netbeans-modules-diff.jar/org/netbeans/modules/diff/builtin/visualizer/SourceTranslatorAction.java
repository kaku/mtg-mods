/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.Action;

public class SourceTranslatorAction
implements Action,
PropertyChangeListener {
    final Action scrollAction;
    final Object source;
    final PropertyChangeSupport support;

    public SourceTranslatorAction(Action action, Object source) {
        this.scrollAction = action;
        this.source = source;
        this.support = new PropertyChangeSupport(action);
    }

    @Override
    public Object getValue(String key) {
        return this.scrollAction.getValue(key);
    }

    @Override
    public void putValue(String key, Object value) {
        this.scrollAction.putValue(key, value);
    }

    @Override
    public void setEnabled(boolean b) {
        this.scrollAction.setEnabled(b);
    }

    @Override
    public boolean isEnabled() {
        return this.scrollAction.isEnabled();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        if (!this.support.hasListeners(null)) {
            this.scrollAction.addPropertyChangeListener(this);
        }
        this.support.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.support.removePropertyChangeListener(listener);
        if (!this.support.hasListeners(null)) {
            this.scrollAction.removePropertyChangeListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ActionEvent event = new ActionEvent(this.source, e.getID(), e.getActionCommand(), e.getWhen(), e.getModifiers());
        this.scrollAction.actionPerformed(event);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.support.firePropertyChange(evt);
    }
}

