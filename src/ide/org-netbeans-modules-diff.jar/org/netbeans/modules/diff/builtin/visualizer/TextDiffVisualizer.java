/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.windows.CloneableOpenSupport
 *  org.openide.windows.CloneableTopComponent
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Component;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.DiffPresenter;
import org.netbeans.modules.diff.builtin.visualizer.TextDiffEditorSupport;
import org.netbeans.modules.diff.builtin.visualizer.UnifiedDiff;
import org.netbeans.spi.diff.DiffVisualizer;
import org.openide.util.NbBundle;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;

public class TextDiffVisualizer
extends DiffVisualizer
implements Serializable {
    private boolean contextMode = true;
    private int contextNumLines = 3;
    static final long serialVersionUID = -2481513747957146261L;

    public String getDisplayName() {
        return NbBundle.getMessage(TextDiffVisualizer.class, (String)"TextDiffVisualizer.displayName");
    }

    public String getShortDescription() {
        return NbBundle.getMessage(TextDiffVisualizer.class, (String)"TextDiffVisualizer.shortDescription");
    }

    public boolean isContextMode() {
        return this.contextMode;
    }

    public void setContextMode(boolean contextMode) {
        this.contextMode = contextMode;
    }

    public int getContextNumLines() {
        return this.contextNumLines;
    }

    public void setContextNumLines(int contextNumLines) {
        this.contextNumLines = contextNumLines;
    }

    @Override
    public Component createView(Difference[] diffs, String name1, String title1, Reader r1, String name2, String title2, Reader r2, String MIMEType) throws IOException {
        TextDiffInfo diff = new TextDiffInfo(name1, name2, title1, title2, r1, r2, diffs);
        diff.setContextMode(this.contextMode, this.contextNumLines);
        return ((TextDiffEditorSupport)diff.getOpenSupport()).createCloneableTopComponentForMe();
    }

    static InputStream differenceToLineDiffText(Difference[] diffs) {
        StringBuffer content = new StringBuffer();
        block5 : for (int i = 0; i < diffs.length; ++i) {
            Difference diff = diffs[i];
            switch (diff.getType()) {
                int n4;
                int n1;
                int n3;
                int n2;
                case 1: {
                    n3 = diff.getSecondStart();
                    n4 = diff.getSecondEnd();
                    if (n3 == n4) {
                        content.append("" + diff.getFirstStart() + "a" + n3 + "\n");
                    } else {
                        content.append("" + diff.getFirstStart() + "a" + n3 + "," + n4 + "\n");
                    }
                    TextDiffVisualizer.appendText(content, "> ", diff.getSecondText());
                    continue block5;
                }
                case 0: {
                    n1 = diff.getFirstStart();
                    n2 = diff.getFirstEnd();
                    if (n1 == n2) {
                        content.append("" + n1 + "d" + diff.getSecondStart() + "\n");
                    } else {
                        content.append("" + n1 + "," + n2 + "d" + diff.getSecondStart() + "\n");
                    }
                    TextDiffVisualizer.appendText(content, "< ", diff.getFirstText());
                    continue block5;
                }
                case 2: {
                    n1 = diff.getFirstStart();
                    n2 = diff.getFirstEnd();
                    n3 = diff.getSecondStart();
                    n4 = diff.getSecondEnd();
                    if (n1 == n2 && n3 == n4) {
                        content.append("" + n1 + "c" + n3 + "\n");
                    } else if (n1 == n2) {
                        content.append("" + n1 + "c" + n3 + "," + n4 + "\n");
                    } else if (n3 == n4) {
                        content.append("" + n1 + "," + n2 + "c" + n3 + "\n");
                    } else {
                        content.append("" + n1 + "," + n2 + "c" + n3 + "," + n4 + "\n");
                    }
                    TextDiffVisualizer.appendText(content, "< ", diff.getFirstText());
                    content.append("---\n");
                    TextDiffVisualizer.appendText(content, "> ", diff.getSecondText());
                }
            }
        }
        return new ByteArrayInputStream(content.toString().getBytes());
    }

    private static void appendText(StringBuffer buff, String prefix, String text) {
        int endLine;
        if (text == null) {
            return;
        }
        int startLine = 0;
        do {
            if ((endLine = text.indexOf(10, startLine)) < 0) {
                endLine = text.length();
            }
            buff.append(prefix + text.substring(startLine, endLine) + "\n");
        } while ((startLine = endLine + 1) < text.length());
    }

    public static String differenceToUnifiedDiffText(TextDiffInfo diffInfo) throws IOException {
        UnifiedDiff ud = new UnifiedDiff(diffInfo);
        return ud.computeDiff();
    }

    public static String differenceToNormalDiffText(TextDiffInfo diffInfo) throws IOException {
        InputStream is = TextDiffVisualizer.differenceToLineDiffText(diffInfo.diffs);
        StringWriter sw = new StringWriter();
        TextDiffVisualizer.copyStreamsCloseAll(sw, new InputStreamReader(is));
        return sw.toString();
    }

    private static void copyStreamsCloseAll(Writer writer, Reader reader) throws IOException {
        int n;
        char[] buffer = new char[4096];
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        writer.close();
        reader.close();
    }

    public static class TextDiffInfo
    extends DiffPresenter.Info {
        private Reader r1;
        private Reader r2;
        private Difference[] diffs;
        private CloneableOpenSupport openSupport;
        private boolean contextMode;
        private int contextNumLines;

        public TextDiffInfo(String name1, String name2, String title1, String title2, Reader r1, Reader r2, Difference[] diffs) {
            super(name1, name2, title1, title2, null, false, false);
            this.r1 = r1;
            this.r2 = r2;
            this.diffs = diffs;
        }

        public String getName() {
            String componentName = this.getName1();
            String name2 = this.getName2();
            if (name2 != null && name2.length() > 0) {
                componentName = componentName + " <> " + name2;
            }
            return componentName;
        }

        public String getTitle() {
            return this.getTitle1() + " <> " + this.getTitle2();
        }

        @Override
        public Reader createFirstReader() {
            return this.r1;
        }

        @Override
        public Reader createSecondReader() {
            return this.r2;
        }

        @Override
        public Difference[] getDifferences() {
            return this.diffs;
        }

        public CloneableOpenSupport getOpenSupport() {
            if (this.openSupport == null) {
                this.openSupport = new TextDiffEditorSupport(this);
            }
            return this.openSupport;
        }

        public void setContextMode(boolean contextMode, int contextNumLines) {
            this.contextMode = contextMode;
            this.contextNumLines = contextNumLines;
        }

        public boolean isContextMode() {
            return this.contextMode;
        }

        public int getContextNumLines() {
            return this.contextNumLines;
        }
    }

}

