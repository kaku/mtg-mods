/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.openide.ErrorManager
 *  org.openide.actions.CopyAction
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.ActionPerformer
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.modules.diff.builtin.visualizer.DEditorPane;
import org.netbeans.modules.diff.builtin.visualizer.LinesComponent;
import org.openide.ErrorManager;
import org.openide.actions.CopyAction;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.ActionPerformer;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.actions.SystemAction;

public class DiffPanel
extends JPanel
implements CaretListener {
    private int totalHeight = 0;
    private int totalLines = 0;
    private int horizontalScroll1ChangedValue = -1;
    private int horizontalScroll2ChangedValue = -1;
    private LinesComponent linesComp1;
    private LinesComponent linesComp2;
    static final long serialVersionUID = 3683458237532937983L;
    private Hashtable<JEditorPane, Hashtable<Object, Action>> kitActions;
    private PropertyChangeListener copyL;
    private PropertyChangeListener copyP;
    private JViewport jViewport1;
    private JViewport jViewport2;
    final JPanel commandPanel = new JPanel();
    final JPanel editorPanel = new JPanel();
    final JLabel fileLabel1 = new JLabel();
    final JLabel fileLabel2 = new JLabel();
    final JPanel filePanel1 = new JPanel();
    final JPanel filePanel2 = new JPanel();
    final DEditorPane jEditorPane1 = new DEditorPane();
    final DEditorPane jEditorPane2 = new DEditorPane();
    final JScrollPane jScrollPane1 = new JScrollPane();
    final JScrollPane jScrollPane2 = new JScrollPane();
    final JSplitPane jSplitPane1 = new JSplitPane();
    final JButton nextButton = new JButton();
    final JButton prevButton = new JButton();

    public DiffPanel() {
        this.initComponents();
        this.aquaBackgroundWorkaround();
        this.commandPanel.remove(this.prevButton);
        this.commandPanel.remove(this.nextButton);
        JPanel toolbar = new JPanel(new FlowLayout(4, 5, 0));
        toolbar.setBorder(BorderFactory.createEmptyBorder());
        toolbar.add(this.prevButton);
        toolbar.add(this.nextButton);
        this.remove(this.commandPanel);
        this.putClientProperty("DiffPresenter.toolbarPanel", toolbar);
        this.setName(NbBundle.getMessage(DiffPanel.class, (String)"DiffComponent.title"));
        this.initActions();
        this.jSplitPane1.setResizeWeight(0.5);
        this.putClientProperty("PersistenceType", "Never");
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffPanel.class, (String)"ACS_DiffPanelA11yName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPanel.class, (String)"ACS_DiffPanelA11yDesc"));
        this.jEditorPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffPanel.class, (String)"ACS_EditorPane1A11yName"));
        this.jEditorPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPanel.class, (String)"ACS_EditorPane1A11yDescr"));
        this.jEditorPane2.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffPanel.class, (String)"ACS_EditorPane2A11yName"));
        this.jEditorPane2.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffPanel.class, (String)"ACS_EditorPane2A11yDescr"));
    }

    private void aquaBackgroundWorkaround() {
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            Color color = UIManager.getColor("NbExplorerView.background");
            this.setBackground(color);
            this.filePanel1.setBackground(color);
            this.filePanel2.setBackground(color);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.jEditorPane1.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        this.jEditorPane2.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        JComponent parent = (JComponent)this.getParent();
        ButtonAction nextAction = new ButtonAction(this.nextButton);
        ButtonAction prevAction = new ButtonAction(this.prevButton);
        parent.getActionMap().put("jumpNext", nextAction);
        parent.getActionMap().put("jumpPrev", prevAction);
    }

    private void initComponents() {
        this.setLayout(new GridBagLayout());
        this.commandPanel.setLayout(new GridBagLayout());
        this.prevButton.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/diff/builtin/visualizer/prev.gif", (boolean)true));
        this.prevButton.setToolTipText(NbBundle.getBundle(DiffPanel.class).getString("DiffComponent.prevButton.toolTipText"));
        this.prevButton.setMargin(new Insets(0, 0, 0, 0));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = -1;
        gridBagConstraints.anchor = 17;
        this.commandPanel.add((Component)this.prevButton, gridBagConstraints);
        this.nextButton.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/diff/builtin/visualizer/next.gif", (boolean)true));
        this.nextButton.setToolTipText(NbBundle.getBundle(DiffPanel.class).getString("DiffComponent.nextButton.toolTipText"));
        this.nextButton.setMargin(new Insets(0, 0, 0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = -1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 2, 0, 0);
        this.commandPanel.add((Component)this.nextButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this.commandPanel, gridBagConstraints);
        this.editorPanel.setLayout(new GridBagLayout());
        this.editorPanel.setPreferredSize(new Dimension(700, 600));
        this.jSplitPane1.setDividerSize(4);
        this.filePanel1.setLayout(new GridBagLayout());
        this.jEditorPane1.addCaretListener(this);
        this.jScrollPane1.setViewportView(this.jEditorPane1);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.filePanel1.add((Component)this.jScrollPane1, gridBagConstraints);
        this.fileLabel1.setText("jLabel1");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.filePanel1.add((Component)this.fileLabel1, gridBagConstraints);
        this.jSplitPane1.setLeftComponent(this.filePanel1);
        this.filePanel2.setLayout(new GridBagLayout());
        this.jEditorPane2.addCaretListener(this);
        this.jScrollPane2.setViewportView(this.jEditorPane2);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.filePanel2.add((Component)this.jScrollPane2, gridBagConstraints);
        this.fileLabel2.setText("jLabel2");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.filePanel2.add((Component)this.fileLabel2, gridBagConstraints);
        this.jSplitPane1.setRightComponent(this.filePanel2);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.editorPanel.add((Component)this.jSplitPane1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.editorPanel, gridBagConstraints);
    }

    @Override
    public void caretUpdate(CaretEvent evt) {
        if (evt.getSource() == this.jEditorPane1) {
            this.jEditorPane1CaretUpdate(evt);
        } else if (evt.getSource() == this.jEditorPane2) {
            this.jEditorPane2CaretUpdate(evt);
        }
    }

    private void jEditorPane1CaretUpdate(CaretEvent evt) {
    }

    private void jEditorPane2CaretUpdate(CaretEvent evt) {
    }

    public void setCurrentLine(int line, int diffLength) {
        if (line > 0) {
            this.showLine(line, diffLength);
        }
    }

    public void addPrevLineButtonListener(ActionListener listener) {
        this.prevButton.addActionListener(listener);
    }

    public void addNextLineButtonListener(ActionListener listener) {
        this.nextButton.addActionListener(listener);
    }

    private void jScrollBar1AdjustmentValueChanged(AdjustmentEvent evt) {
    }

    private void closeButtonActionPerformed(ActionEvent evt) {
        this.exitForm(null);
    }

    private void exitForm(WindowEvent evt) {
    }

    private void initActions() {
        this.jEditorPane1.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                DiffPanel.this.editorActivated(DiffPanel.this.jEditorPane1);
            }

            @Override
            public void focusLost(FocusEvent e) {
                DiffPanel.this.editorDeactivated(DiffPanel.this.jEditorPane1);
            }
        });
        this.jEditorPane2.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                DiffPanel.this.editorActivated(DiffPanel.this.jEditorPane2);
            }

            @Override
            public void focusLost(FocusEvent e) {
                DiffPanel.this.editorDeactivated(DiffPanel.this.jEditorPane2);
            }
        });
    }

    private Action getAction(String s, JEditorPane editor) {
        Hashtable actions;
        if (this.kitActions == null) {
            this.kitActions = new Hashtable();
        }
        if ((actions = this.kitActions.get(editor)) == null) {
            EditorKit kit = editor.getEditorKit();
            if (kit == null) {
                return null;
            }
            Action[] a = kit.getActions();
            actions = new Hashtable(a.length);
            int k = a.length;
            for (int i = 0; i < k; ++i) {
                actions.put(a[i].getValue("Name"), a[i]);
            }
            this.kitActions.put(editor, actions);
        }
        return actions.get(s);
    }

    private void editorActivated(final JEditorPane editor) {
        final Action copy = this.getAction("copy-to-clipboard", editor);
        if (copy != null) {
            final CallbackSystemAction sysCopy = (CallbackSystemAction)SystemAction.get(CopyAction.class);
            final ActionPerformer perf = new ActionPerformer(){

                public void performAction(SystemAction action) {
                    copy.actionPerformed(new ActionEvent(editor, 0, ""));
                }
            };
            sysCopy.setActionPerformer(copy.isEnabled() ? perf : null);
            PropertyChangeListener copyListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("enabled".equals(evt.getPropertyName())) {
                        if (((Boolean)evt.getNewValue()).booleanValue()) {
                            sysCopy.setActionPerformer(perf);
                        } else if (sysCopy.getActionPerformer() == perf) {
                            sysCopy.setActionPerformer(null);
                        }
                    }
                }
            };
            copy.addPropertyChangeListener(copyListener);
            if (editor.equals(this.jEditorPane1)) {
                this.copyL = copyListener;
            } else {
                this.copyP = copyListener;
            }
        }
    }

    private void editorDeactivated(JEditorPane editor) {
        Action copy = this.getAction("copy-to-clipboard", editor);
        PropertyChangeListener copyListener = editor.equals(this.jEditorPane1) ? this.copyL : this.copyP;
        if (copy != null) {
            copy.removePropertyChangeListener(copyListener);
        }
    }

    public void open() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DiffPanel.this.jSplitPane1.setDividerLocation(0.5);
                DiffPanel.this.openPostProcess();
            }
        });
    }

    protected void openPostProcess() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DiffPanel.this.expandFolds();
                DiffPanel.this.initGlobalSizes();
                DiffPanel.this.addChangeListeners();
            }
        });
    }

    private void expandFolds() {
        FoldHierarchy fh = FoldHierarchy.get((JTextComponent)this.jEditorPane1);
        FoldUtilities.expandAll((FoldHierarchy)fh);
        fh = FoldHierarchy.get((JTextComponent)this.jEditorPane2);
        FoldUtilities.expandAll((FoldHierarchy)fh);
    }

    private void initGlobalSizes() {
        int numLines2;
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane2.getDocument();
        int numLines1 = NbDocument.findLineNumber((StyledDocument)doc1, (int)doc1.getEndPosition().getOffset());
        int numLines = Math.max(numLines1, numLines2 = NbDocument.findLineNumber((StyledDocument)doc2, (int)doc2.getEndPosition().getOffset()));
        if (numLines < 1) {
            numLines = 1;
        }
        this.totalLines = numLines;
        int value = this.jEditorPane2.getSize().height;
        int totHeight = this.jEditorPane1.getSize().height;
        if (value > totHeight) {
            totHeight = value;
        }
        this.totalHeight = totHeight;
    }

    private void showLine(int line, int diffLength) {
        this.linesComp1.setActiveLine(line);
        this.linesComp2.setActiveLine(line);
        this.linesComp2.repaint();
        this.linesComp1.repaint();
        int padding = 5;
        if (line <= 5) {
            padding = line / 2;
        }
        int viewHeight = this.jViewport1.getExtentSize().height;
        this.initGlobalSizes();
        Point p1 = this.jViewport1.getViewPosition();
        Point p2 = this.jViewport2.getViewPosition();
        int ypos = this.totalHeight * (line - padding - 1) / (this.totalLines + 1);
        try {
            int off1 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane1.getDocument()), (int)(line - 1));
            int off2 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane2.getDocument()), (int)(line - 1));
            this.jEditorPane1.setCaretPosition(off1);
            this.jEditorPane2.setCaretPosition(off2);
        }
        catch (IndexOutOfBoundsException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
        if (ypos < p1.y || ypos + (diffLength + padding) * this.totalHeight / this.totalLines > p1.y + viewHeight) {
            p1.y = ypos;
            this.jViewport1.setViewPosition(p1);
        }
    }

    private void joinScrollBars() {
        final JScrollBar scrollBarH1 = this.jScrollPane1.getHorizontalScrollBar();
        final JScrollBar scrollBarV1 = this.jScrollPane1.getVerticalScrollBar();
        final JScrollBar scrollBarH2 = this.jScrollPane2.getHorizontalScrollBar();
        final JScrollBar scrollBarV2 = this.jScrollPane2.getVerticalScrollBar();
        scrollBarV1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarV1.getValue();
                int oldValue = scrollBarV2.getValue();
                if (oldValue != value) {
                    scrollBarV2.setValue(value);
                }
            }
        });
        this.jScrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollBarV2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarV2.getValue();
                int oldValue = scrollBarV1.getValue();
                if (oldValue != value) {
                    scrollBarV1.setValue(value);
                }
            }
        });
        scrollBarH1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH1.getValue();
                if (value == DiffPanel.this.horizontalScroll1ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max1 == ext1) {
                    DiffPanel.this.horizontalScroll2ChangedValue = 0;
                } else {
                    DiffPanel.this.horizontalScroll2ChangedValue = value * (max2 - ext2) / (max1 - ext1);
                }
                DiffPanel.this.horizontalScroll1ChangedValue = -1;
                scrollBarH2.setValue(DiffPanel.this.horizontalScroll2ChangedValue);
            }
        });
        scrollBarH2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH2.getValue();
                if (value == DiffPanel.this.horizontalScroll2ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max2 == ext2) {
                    DiffPanel.this.horizontalScroll1ChangedValue = 0;
                } else {
                    DiffPanel.this.horizontalScroll1ChangedValue = value * (max1 - ext1) / (max2 - ext2);
                }
                DiffPanel.this.horizontalScroll2ChangedValue = -1;
                scrollBarH1.setValue(DiffPanel.this.horizontalScroll1ChangedValue);
            }
        });
        this.jSplitPane1.setDividerLocation(0.5);
    }

    private String strCharacters(char c, int num) {
        StringBuffer s = new StringBuffer();
        while (num-- > 0) {
            s.append(c);
        }
        return s.toString();
    }

    private void customizeEditor(JEditorPane editor) {
        StyledDocument doc;
        EditorKit kit = editor.getEditorKit();
        Document document = editor.getDocument();
        try {
            doc = (StyledDocument)editor.getDocument();
        }
        catch (ClassCastException e) {
            doc = new DefaultStyledDocument();
            try {
                doc.insertString(0, document.getText(0, document.getLength()), null);
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
            editor.setDocument(doc);
        }
        int lastOffset = doc.getEndPosition().getOffset();
        int numLines = NbDocument.findLineNumber((StyledDocument)doc, (int)lastOffset);
        int numLength = Integer.toString(numLines).length();
        for (int line = 0; line <= numLines; ++line) {
            int offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
            String lineStr = Integer.toString(line + 1);
            if (lineStr.length() >= numLength) continue;
            lineStr = this.strCharacters(' ', numLength - lineStr.length()) + lineStr;
        }
    }

    private void addChangeListeners() {
        this.jEditorPane1.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffPanel.this.initGlobalSizes();
                        DiffPanel.this.linesComp1.repaint();
                    }
                });
            }

        });
        this.jEditorPane2.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffPanel.this.initGlobalSizes();
                        DiffPanel.this.linesComp2.repaint();
                    }
                });
            }

        });
    }

    public void setSource1(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane1.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane1.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane1);
        this.jEditorPane1.setDocument(doc);
        this.jEditorPane1.setEditable(false);
        this.customizeEditor(this.jEditorPane1);
        this.linesComp1 = new LinesComponent(this.jEditorPane1);
        this.jScrollPane1.setRowHeaderView(this.linesComp1);
        this.jViewport1 = this.jScrollPane1.getViewport();
    }

    public void setSource2(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane2.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane2.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane2);
        this.jEditorPane2.setDocument(doc);
        this.jEditorPane2.setEditable(false);
        this.customizeEditor(this.jEditorPane2);
        this.linesComp2 = new LinesComponent(this.jEditorPane2);
        this.jScrollPane2.setRowHeaderView(this.linesComp2);
        this.jViewport2 = this.jScrollPane2.getViewport();
        this.joinScrollBars();
    }

    public void setSource1Title(String title) {
        this.fileLabel1.setText(title);
        this.fileLabel1.setMinimumSize(new Dimension(3, this.fileLabel1.getMinimumSize().height));
    }

    public void setSource2Title(String title) {
        this.fileLabel2.setText(title);
        this.fileLabel2.setMinimumSize(new Dimension(3, this.fileLabel2.getMinimumSize().height));
    }

    public void setMimeType1(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane1.setEditorKit(kit);
    }

    public void setMimeType2(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane2.setEditorKit(kit);
    }

    public void setDocument1(Document doc) {
        if (doc != null) {
            this.jEditorPane1.setDocument(doc);
        }
    }

    public void setDocument2(Document doc) {
        if (doc != null) {
            this.jEditorPane2.setDocument(doc);
        }
    }

    String getDocumentText1() {
        return this.jEditorPane1.getText();
    }

    String getDocumentText2() {
        return this.jEditorPane2.getText();
    }

    private void setHighlight(StyledDocument doc, int line1, int line2, Color color) {
        for (int line = line1 - 1; line < line2; ++line) {
            if (line < 0) continue;
            try {
                int offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
                if (offset < 0) continue;
                Style s = doc.getLogicalStyle(offset);
                if (s == null) {
                    s = doc.addStyle("diff-style(" + color + "):1500", null);
                }
                s.addAttribute(StyleConstants.ColorConstants.Background, color);
                doc.setLogicalStyle(offset, s);
                continue;
            }
            catch (IndexOutOfBoundsException ex) {
                ErrorManager.getDefault().annotate((Throwable)ex, "#67631 reappreared. Please reopen with details.");
                ErrorManager.getDefault().notify((Throwable)ex);
            }
        }
    }

    private void unhighlight(StyledDocument doc) {
        int endOffset = doc.getEndPosition().getOffset();
        int endLine = NbDocument.findLineNumber((StyledDocument)doc, (int)endOffset);
        Style s = doc.addStyle("diff-style(white):1500", null);
        s.addAttribute(StyleConstants.ColorConstants.Background, Color.white);
        for (int line = 0; line <= endLine; ++line) {
            int offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
            doc.setLogicalStyle(offset, s);
        }
    }

    public void unhighlightAll() {
        this.unhighlight((StyledDocument)this.jEditorPane1.getDocument());
        this.unhighlight((StyledDocument)this.jEditorPane2.getDocument());
    }

    public void highlightRegion1(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane1.getDocument();
        this.setHighlight(doc, line1, line2, color);
    }

    public void highlightRegion2(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane2.getDocument();
        this.setHighlight(doc, line1, line2, color);
    }

    private void addEmptyLines(StyledDocument doc, int line, int numLines) {
        int lastOffset = doc.getEndPosition().getOffset();
        int totLines = NbDocument.findLineNumber((StyledDocument)doc, (int)lastOffset);
        int offset = lastOffset - 1;
        if (line <= totLines) {
            offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
        }
        String insStr = this.strCharacters('\n', numLines);
        try {
            doc.insertString(offset, insStr, null);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    public void addEmptyLines1(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane1.getDocument();
        this.addEmptyLines(doc, line, numLines);
        this.linesComp1.addEmptyLines(line, numLines);
    }

    public void addEmptyLines2(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane2.getDocument();
        this.addEmptyLines(doc, line, numLines);
        this.linesComp2.addEmptyLines(line, numLines);
    }

    private static class ButtonAction
    extends AbstractAction {
        final JButton button;

        public ButtonAction(JButton button) {
            this.button = button;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.button.doClick();
        }
    }

}

