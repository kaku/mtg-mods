/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.builtin.provider.HuntDiff;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.util.NbBundle;

public class BuiltInDiffProvider
extends DiffProvider
implements Serializable {
    private Options options = DiffModuleConfig.getDefault().getOptions();
    static final long serialVersionUID = 1;

    public String getDisplayName() {
        return NbBundle.getMessage(BuiltInDiffProvider.class, (String)"BuiltInDiffProvider.displayName");
    }

    public String getShortDescription() {
        return NbBundle.getMessage(BuiltInDiffProvider.class, (String)"BuiltInDiffProvider.shortDescription");
    }

    @Override
    public Difference[] computeDiff(Reader r1, Reader r2) throws IOException {
        if (this.options == null) {
            this.options = DiffModuleConfig.getDefault().getOptions();
        }
        return HuntDiff.diff(this.getLines(r1), this.getLines(r2), this.options);
    }

    private String[] getLines(Reader r) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(r);
        ArrayList<String> lines = new ArrayList<String>();
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
        return lines.toArray(new String[lines.size()]);
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public static class Options {
        public boolean ignoreLeadingAndtrailingWhitespace;
        public boolean ignoreInnerWhitespace;
        public boolean ignoreCase;
    }

}

