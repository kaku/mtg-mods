/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.diff.builtin.visualizer.TextDiffVisualizer;
import org.openide.ErrorManager;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class TextDiffVisualizerBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(TextDiffVisualizer.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] desc;
        try {
            PropertyDescriptor contextMode = new PropertyDescriptor("contextMode", TextDiffVisualizer.class);
            contextMode.setDisplayName(NbBundle.getMessage(TextDiffVisualizerBeanInfo.class, (String)"PROP_contextMode"));
            contextMode.setShortDescription(NbBundle.getMessage(TextDiffVisualizerBeanInfo.class, (String)"HINT_contextMode"));
            PropertyDescriptor contextNumLines = new PropertyDescriptor("contextNumLines", TextDiffVisualizer.class);
            contextNumLines.setDisplayName(NbBundle.getMessage(TextDiffVisualizerBeanInfo.class, (String)"PROP_contextNumLines"));
            contextNumLines.setShortDescription(NbBundle.getMessage(TextDiffVisualizerBeanInfo.class, (String)"HINT_contextNumLines"));
            desc = new PropertyDescriptor[]{contextMode, contextNumLines};
        }
        catch (IntrospectionException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
            desc = null;
        }
        return desc;
    }

    @Override
    public int getDefaultPropertyIndex() {
        return 0;
    }

    @Override
    public Image getIcon(int iconKind) {
        switch (iconKind) {
            case 1: {
                return ImageUtilities.loadImage((String)"org/netbeans/modules/diff/diffSettingsIcon.gif", (boolean)true);
            }
        }
        return null;
    }
}

