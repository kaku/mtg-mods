/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.errorstripe.privatespi.Mark
 *  org.netbeans.modules.editor.errorstripe.privatespi.Status
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.Color;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;

final class DiffMark
implements Mark {
    private final int[] span;
    private final Color color;
    private final String desc;

    public DiffMark(Difference difference, Color color) {
        if (difference.getType() == 0) {
            int start = difference.getSecondStart() - 1;
            if (start < 0) {
                start = 0;
            }
            this.span = new int[]{start, start};
        } else {
            this.span = new int[]{difference.getSecondStart() - 1, difference.getSecondEnd() - 1};
        }
        this.color = color;
        this.desc = "";
    }

    public int getType() {
        return 1;
    }

    public Status getStatus() {
        return Status.STATUS_OK;
    }

    public int getPriority() {
        return Integer.MIN_VALUE;
    }

    public Color getEnhancedColor() {
        return this.color;
    }

    public int[] getAssignedLines() {
        return this.span;
    }

    public String getShortDescription() {
        return this.desc;
    }
}

