/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.openide.ErrorManager
 *  org.openide.actions.CopyAction
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.ActionPerformer
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import org.netbeans.api.diff.DiffView;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.builtin.visualizer.DEditorPane;
import org.netbeans.modules.diff.builtin.visualizer.LinesComponent;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.ErrorManager;
import org.openide.actions.CopyAction;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;
import org.openide.util.actions.ActionPerformer;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.actions.SystemAction;

public class DiffViewImpl
extends JPanel
implements DiffView,
CaretListener {
    private Difference[] diffs = null;
    private int[][] diffShifts;
    private Color colorMissing;
    private Color colorAdded;
    private Color colorChanged;
    private int currentDiffLine = -1;
    private int totalHeight = 0;
    private int totalLines = 0;
    private int horizontalScroll1ChangedValue = -1;
    private int horizontalScroll2ChangedValue = -1;
    private LinesComponent linesComp1;
    private LinesComponent linesComp2;
    private String source1;
    private String source2;
    private int onLayoutLine;
    private int onLayoutLength;
    private Hashtable<JEditorPane, Hashtable<Object, Action>> kitActions;
    private PropertyChangeListener copyL;
    private PropertyChangeListener copyP;
    private JViewport jViewport1;
    private JViewport jViewport2;
    final JLabel fileLabel1 = new JLabel();
    final JLabel fileLabel2 = new JLabel();
    final JPanel filePanel1 = new JPanel();
    final JPanel filePanel2 = new JPanel();
    final DEditorPane jEditorPane1 = new DEditorPane();
    final DEditorPane jEditorPane2 = new DEditorPane();
    final JScrollPane jScrollPane1 = new JScrollPane();
    final JScrollPane jScrollPane2 = new JScrollPane();
    final JSplitPane jSplitPane1 = new JSplitPane();

    public DiffViewImpl() {
    }

    private static void copyStreamsCloseAll(Writer out, Reader in) throws IOException {
        int n;
        char[] buff = new char[4096];
        while ((n = in.read(buff)) > 0) {
            out.write(buff, 0, n);
        }
        in.close();
        out.close();
    }

    public DiffViewImpl(StreamSource ss1, StreamSource ss2) throws IOException {
        this.colorMissing = DiffModuleConfig.getDefault().getDeletedColor();
        this.colorAdded = DiffModuleConfig.getDefault().getAddedColor();
        this.colorChanged = DiffModuleConfig.getDefault().getChangedColor();
        Reader r1 = ss1.createReader();
        Reader r2 = ss2.createReader();
        String title1 = ss1.getTitle();
        String title2 = ss2.getTitle();
        String mimeType1 = ss1.getMIMEType();
        String mimeType2 = ss2.getMIMEType();
        if (mimeType1 == null) {
            mimeType1 = mimeType2;
        }
        if (mimeType2 == null) {
            mimeType2 = mimeType1;
        }
        this.saveSources(r1, r2);
        this.initComponents();
        this.aquaBackgroundWorkaround();
        this.setName(NbBundle.getMessage(DiffViewImpl.class, (String)"DiffComponent.title"));
        this.initActions();
        this.jSplitPane1.setResizeWeight(0.5);
        this.putClientProperty("PersistenceType", "Never");
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffViewImpl.class, (String)"ACS_DiffPanelA11yName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffViewImpl.class, (String)"ACS_DiffPanelA11yDesc"));
        this.jEditorPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffViewImpl.class, (String)"ACS_EditorPane1A11yName"));
        this.jEditorPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffViewImpl.class, (String)"ACS_EditorPane1A11yDescr"));
        this.jEditorPane2.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DiffViewImpl.class, (String)"ACS_EditorPane2A11yName"));
        this.jEditorPane2.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DiffViewImpl.class, (String)"ACS_EditorPane2A11yDescr"));
        if (this.source1 == null) {
            this.jEditorPane1.setVisible(false);
        }
        if (this.source2 == null) {
            this.jEditorPane2.setVisible(false);
        }
        if (r1 != null && r2 != null) {
            DiffProvider provider = DiffModuleConfig.getDefault().getDefaultDiffProvider();
            this.diffs = provider.computeDiff(new StringReader(this.source1), new StringReader(this.source2));
        } else {
            this.diffs = new Difference[0];
        }
        this.diffShifts = new int[this.diffs.length][2];
        this.setSource1Title(title1);
        this.setSource2Title(title2);
        final String f1 = mimeType1;
        final String f2 = mimeType2;
        try {
            Runnable awtTask = new Runnable(){

                @Override
                public void run() {
                    DiffViewImpl.this.setMimeType1(f1);
                    DiffViewImpl.this.setMimeType2(f2);
                    try {
                        if (DiffViewImpl.this.source1 != null) {
                            DiffViewImpl.this.setSource1(new StringReader(DiffViewImpl.this.source1));
                        }
                        if (DiffViewImpl.this.source2 != null) {
                            DiffViewImpl.this.setSource2(new StringReader(DiffViewImpl.this.source2));
                        }
                    }
                    catch (IOException ioex) {
                        ErrorManager.getDefault().notify((Throwable)ioex);
                    }
                    DiffViewImpl.this.insertEmptyLines(true);
                    DiffViewImpl.this.setDiffHighlight(true);
                    DiffViewImpl.this.insertEmptyLinesNotReported();
                    Color borderColor = UIManager.getColor("scrollpane_border");
                    if (borderColor == null) {
                        borderColor = UIManager.getColor("controlShadow");
                    }
                    DiffViewImpl.this.jScrollPane1.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                    DiffViewImpl.this.jScrollPane2.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                    DiffViewImpl.this.jSplitPane1.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, borderColor));
                }
            };
            if (SwingUtilities.isEventDispatchThread()) {
                awtTask.run();
            } else {
                SwingUtilities.invokeAndWait(awtTask);
            }
        }
        catch (InterruptedException e) {
            ErrorManager err = ErrorManager.getDefault();
            err.notify((Throwable)e);
        }
        catch (InvocationTargetException e) {
            ErrorManager err = ErrorManager.getDefault();
            err.notify((Throwable)e);
        }
    }

    private void aquaBackgroundWorkaround() {
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            Color color = UIManager.getColor("NbExplorerView.background");
            this.setBackground(color);
            this.filePanel1.setBackground(color);
            this.filePanel2.setBackground(color);
        }
    }

    private void saveSources(Reader r1, Reader r2) throws IOException {
        StringWriter sw;
        if (r1 != null) {
            sw = new StringWriter();
            DiffViewImpl.copyStreamsCloseAll(sw, r1);
            this.source1 = sw.toString();
        }
        if (r2 != null) {
            sw = new StringWriter();
            DiffViewImpl.copyStreamsCloseAll(sw, r2);
            this.source2 = sw.toString();
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        return this.jEditorPane1.requestFocusInWindow();
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public int getDifferenceCount() {
        return this.diffs.length;
    }

    @Override
    public boolean canSetCurrentDifference() {
        return true;
    }

    @Override
    public void setCurrentDifference(int diffNo) throws UnsupportedOperationException {
        if (diffNo < -1 || diffNo >= this.diffs.length) {
            throw new IllegalArgumentException("Illegal difference number: " + diffNo);
        }
        if (diffNo == -1) {
            if (this.linesComp1 != null) {
                this.linesComp1.setActiveLine(-1);
                this.linesComp1.repaint();
            }
            if (this.linesComp2 != null) {
                this.linesComp2.setActiveLine(-1);
                this.linesComp2.repaint();
            }
        } else {
            this.currentDiffLine = diffNo;
            this.showCurrentLine();
        }
    }

    @Override
    public int getCurrentDifference() throws UnsupportedOperationException {
        int candidate = this.currentDiffLine;
        if (this.jViewport1 != null) {
            int viewHeight = this.jViewport1.getViewSize().height;
            this.initGlobalSizes();
            Point p1 = this.jViewport1.getViewPosition();
            int HALFLINE_CEILING = 2;
            float firstPct = (float)p1.y / (float)viewHeight;
            int firstVisibleLine = (int)(firstPct * (float)this.totalLines) + HALFLINE_CEILING;
            float lastPct = (float)(this.jViewport1.getHeight() + p1.y) / (float)viewHeight;
            int lastVisibleLine = (int)(lastPct * (float)this.totalLines) - HALFLINE_CEILING;
            for (int i = 0; i < this.diffs.length; ++i) {
                int startLine = this.diffShifts[i][0] + this.diffs[i].getFirstStart();
                int endLine = this.diffShifts[i][0] + this.diffs[i].getFirstEnd();
                if ((firstVisibleLine >= startLine || startLine >= lastVisibleLine) && (firstVisibleLine >= endLine || endLine >= lastVisibleLine)) continue;
                if (i == this.currentDiffLine) {
                    return this.currentDiffLine;
                }
                candidate = i;
            }
        }
        return candidate;
    }

    @Override
    public JToolBar getToolBar() {
        return null;
    }

    private void showCurrentLine() {
        Difference diff = this.diffs[this.currentDiffLine];
        int line = diff.getFirstStart() + this.diffShifts[this.currentDiffLine][0];
        if (diff.getType() == 1) {
            ++line;
        }
        int lf1 = diff.getFirstEnd() - diff.getFirstStart() + 1;
        int lf2 = diff.getSecondEnd() - diff.getSecondStart() + 1;
        int length = Math.max(lf1, lf2);
        this.setCurrentLine(line, length);
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        this.jSplitPane1.setDividerSize(4);
        this.filePanel1.setLayout(new GridBagLayout());
        this.jEditorPane1.addCaretListener(this);
        this.jScrollPane1.setViewportView(this.jEditorPane1);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.filePanel1.add((Component)this.jScrollPane1, gridBagConstraints);
        this.fileLabel1.setHorizontalAlignment(0);
        this.fileLabel1.setLabelFor(this.jEditorPane1);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.filePanel1.add((Component)this.fileLabel1, gridBagConstraints);
        this.jSplitPane1.setLeftComponent(this.filePanel1);
        this.filePanel2.setLayout(new GridBagLayout());
        this.jEditorPane2.addCaretListener(this);
        this.jScrollPane2.setViewportView(this.jEditorPane2);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.filePanel2.add((Component)this.jScrollPane2, gridBagConstraints);
        this.fileLabel2.setHorizontalAlignment(0);
        this.fileLabel2.setLabelFor(this.jEditorPane2);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.filePanel2.add((Component)this.fileLabel2, gridBagConstraints);
        this.jSplitPane1.setRightComponent(this.filePanel2);
        this.add((Component)this.jSplitPane1, "Center");
    }

    @Override
    public void caretUpdate(CaretEvent evt) {
        if (evt.getSource() == this.jEditorPane1) {
            this.jEditorPane1CaretUpdate(evt);
        } else if (evt.getSource() == this.jEditorPane2) {
            this.jEditorPane2CaretUpdate(evt);
        }
    }

    private void jEditorPane1CaretUpdate(CaretEvent evt) {
    }

    private void jEditorPane2CaretUpdate(CaretEvent evt) {
    }

    public void setCurrentLine(int line, int diffLength) {
        if (line > 0) {
            this.showLine(line, diffLength);
        }
        this.onLayoutLine = line;
        this.onLayoutLength = diffLength;
    }

    private void initActions() {
        this.jEditorPane1.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                DiffViewImpl.this.editorActivated(DiffViewImpl.this.jEditorPane1);
            }

            @Override
            public void focusLost(FocusEvent e) {
                DiffViewImpl.this.editorDeactivated(DiffViewImpl.this.jEditorPane1);
            }
        });
        this.jEditorPane2.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                DiffViewImpl.this.editorActivated(DiffViewImpl.this.jEditorPane2);
            }

            @Override
            public void focusLost(FocusEvent e) {
                DiffViewImpl.this.editorDeactivated(DiffViewImpl.this.jEditorPane2);
            }
        });
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.jEditorPane1.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        this.jEditorPane2.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        ArrayList<Action> actions = new ArrayList<Action>(2);
        actions.add(this.getActionMap().get("jumpNext"));
        actions.add(this.getActionMap().get("jumpPrev"));
        this.jEditorPane1.setPopupActions(actions);
        this.jEditorPane2.setPopupActions(actions);
        this.expandFolds();
        this.initGlobalSizes();
        this.addChangeListeners();
    }

    @Override
    public void doLayout() {
        super.doLayout();
        this.setCurrentLine(this.onLayoutLine, this.onLayoutLength);
        this.onLayoutLine = 0;
    }

    private Action getAction(String s, JEditorPane editor) {
        Hashtable actions;
        if (this.kitActions == null) {
            this.kitActions = new Hashtable();
        }
        if ((actions = this.kitActions.get(editor)) == null) {
            EditorKit kit = editor.getEditorKit();
            if (kit == null) {
                return null;
            }
            Action[] a = kit.getActions();
            actions = new Hashtable(a.length);
            int k = a.length;
            for (int i = 0; i < k; ++i) {
                actions.put(a[i].getValue("Name"), a[i]);
            }
            this.kitActions.put(editor, actions);
        }
        return actions.get(s);
    }

    private void editorActivated(final JEditorPane editor) {
        final Action copy = this.getAction("copy-to-clipboard", editor);
        if (copy != null) {
            final CallbackSystemAction sysCopy = (CallbackSystemAction)SystemAction.get(CopyAction.class);
            final ActionPerformer perf = new ActionPerformer(){

                public void performAction(SystemAction action) {
                    copy.actionPerformed(new ActionEvent(editor, 0, ""));
                }
            };
            sysCopy.setActionPerformer(copy.isEnabled() ? perf : null);
            PropertyChangeListener copyListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("enabled".equals(evt.getPropertyName())) {
                        if (((Boolean)evt.getNewValue()).booleanValue()) {
                            sysCopy.setActionPerformer(perf);
                        } else if (sysCopy.getActionPerformer() == perf) {
                            sysCopy.setActionPerformer(null);
                        }
                    }
                }
            };
            copy.addPropertyChangeListener(copyListener);
            if (editor.equals(this.jEditorPane1)) {
                this.copyL = copyListener;
            } else {
                this.copyP = copyListener;
            }
        }
    }

    private void editorDeactivated(JEditorPane editor) {
        Action copy = this.getAction("copy-to-clipboard", editor);
        PropertyChangeListener copyListener = editor.equals(this.jEditorPane1) ? this.copyL : this.copyP;
        if (copy != null) {
            copy.removePropertyChangeListener(copyListener);
        }
    }

    private void expandFolds() {
        FoldHierarchy fh = FoldHierarchy.get((JTextComponent)this.jEditorPane1);
        FoldUtilities.expandAll((FoldHierarchy)fh);
        fh = FoldHierarchy.get((JTextComponent)this.jEditorPane2);
        FoldUtilities.expandAll((FoldHierarchy)fh);
    }

    private void initGlobalSizes() {
        int numLines2;
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane2.getDocument();
        int numLines1 = NbDocument.findLineNumber((StyledDocument)doc1, (int)doc1.getEndPosition().getOffset());
        int numLines = Math.max(numLines1, numLines2 = NbDocument.findLineNumber((StyledDocument)doc2, (int)doc2.getEndPosition().getOffset()));
        if (numLines < 1) {
            numLines = 1;
        }
        this.totalLines = numLines;
        int value = this.jEditorPane2.getSize().height;
        int totHeight = this.jEditorPane1.getSize().height;
        if (value > totHeight) {
            totHeight = value;
        }
        this.totalHeight = totHeight;
    }

    private void showLine(int line, int diffLength) {
        this.linesComp1.setActiveLine(line);
        this.linesComp2.setActiveLine(line);
        this.linesComp2.repaint();
        this.linesComp1.repaint();
        int padding = 5;
        if (line <= 5) {
            padding = line / 2;
        }
        int viewHeight = this.jViewport1.getExtentSize().height;
        this.initGlobalSizes();
        Point p1 = this.jViewport1.getViewPosition();
        Point p2 = this.jViewport2.getViewPosition();
        int ypos = this.totalHeight * (line - padding - 1) / (this.totalLines + 1);
        try {
            int off1 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane1.getDocument()), (int)(line - 1));
            int off2 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane2.getDocument()), (int)(line - 1));
            this.jEditorPane1.setCaretPosition(off1);
            this.jEditorPane2.setCaretPosition(off2);
        }
        catch (IndexOutOfBoundsException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
        if (ypos < p1.y || ypos + (diffLength + padding) * this.totalHeight / this.totalLines > p1.y + viewHeight) {
            p1.y = ypos;
            this.jViewport1.setViewPosition(p1);
        }
    }

    private void joinScrollBars() {
        final JScrollBar scrollBarH1 = this.jScrollPane1.getHorizontalScrollBar();
        final JScrollBar scrollBarV1 = this.jScrollPane1.getVerticalScrollBar();
        final JScrollBar scrollBarH2 = this.jScrollPane2.getHorizontalScrollBar();
        final JScrollBar scrollBarV2 = this.jScrollPane2.getVerticalScrollBar();
        scrollBarV1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarV1.getValue();
                int oldValue = scrollBarV2.getValue();
                if (oldValue != value) {
                    scrollBarV2.setValue(value);
                }
            }
        });
        this.jScrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollBarV2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarV2.getValue();
                int oldValue = scrollBarV1.getValue();
                if (oldValue != value) {
                    scrollBarV1.setValue(value);
                }
            }
        });
        scrollBarH1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH1.getValue();
                if (value == DiffViewImpl.this.horizontalScroll1ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max1 == ext1) {
                    DiffViewImpl.this.horizontalScroll2ChangedValue = 0;
                } else {
                    DiffViewImpl.this.horizontalScroll2ChangedValue = value * (max2 - ext2) / (max1 - ext1);
                }
                DiffViewImpl.this.horizontalScroll1ChangedValue = -1;
                scrollBarH2.setValue(DiffViewImpl.this.horizontalScroll2ChangedValue);
            }
        });
        scrollBarH2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH2.getValue();
                if (value == DiffViewImpl.this.horizontalScroll2ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max2 == ext2) {
                    DiffViewImpl.this.horizontalScroll1ChangedValue = 0;
                } else {
                    DiffViewImpl.this.horizontalScroll1ChangedValue = value * (max1 - ext1) / (max2 - ext2);
                }
                DiffViewImpl.this.horizontalScroll2ChangedValue = -1;
                scrollBarH1.setValue(DiffViewImpl.this.horizontalScroll1ChangedValue);
            }
        });
        this.jSplitPane1.setDividerLocation(0.5);
    }

    private String strCharacters(char c, int num) {
        StringBuffer s = new StringBuffer();
        while (num-- > 0) {
            s.append(c);
        }
        return s.toString();
    }

    private void customizeEditor(JEditorPane editor) {
        Document document = editor.getDocument();
        try {
            StyledDocument doc = (StyledDocument)editor.getDocument();
        }
        catch (ClassCastException e) {
            DefaultStyledDocument doc = new DefaultStyledDocument();
            try {
                doc.insertString(0, document.getText(0, document.getLength()), null);
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
            editor.setDocument(doc);
        }
    }

    private void addChangeListeners() {
        this.jEditorPane1.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffViewImpl.this.initGlobalSizes();
                        DiffViewImpl.this.linesComp1.repaint();
                    }
                });
            }

        });
        this.jEditorPane2.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffViewImpl.this.initGlobalSizes();
                        DiffViewImpl.this.linesComp2.repaint();
                    }
                });
            }

        });
    }

    public void setSource1(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane1.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane1.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane1);
        this.jEditorPane1.setDocument(doc);
        this.jEditorPane1.setEditable(false);
        this.customizeEditor(this.jEditorPane1);
        this.linesComp1 = new LinesComponent(this.jEditorPane1);
        this.jScrollPane1.setRowHeaderView(this.linesComp1);
        this.jViewport1 = this.jScrollPane1.getViewport();
    }

    public void setSource2(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane2.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane2.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane2);
        this.jEditorPane2.setDocument(doc);
        this.jEditorPane2.setEditable(false);
        this.customizeEditor(this.jEditorPane2);
        this.linesComp2 = new LinesComponent(this.jEditorPane2);
        this.jScrollPane2.setRowHeaderView(this.linesComp2);
        this.jViewport2 = this.jScrollPane2.getViewport();
        this.joinScrollBars();
    }

    public void setSource1Title(String title) {
        this.fileLabel1.setText(title);
        this.fileLabel1.setMinimumSize(new Dimension(3, this.fileLabel1.getMinimumSize().height));
    }

    public void setSource2Title(String title) {
        this.fileLabel2.setText(title);
        this.fileLabel2.setMinimumSize(new Dimension(3, this.fileLabel2.getMinimumSize().height));
    }

    public void setMimeType1(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane1.setEditorKit(kit);
    }

    public void setMimeType2(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane2.setEditorKit(kit);
    }

    public void setDocument1(Document doc) {
        if (doc != null) {
            this.jEditorPane1.setDocument(doc);
        }
    }

    public void setDocument2(Document doc) {
        if (doc != null) {
            this.jEditorPane2.setDocument(doc);
        }
    }

    String getDocumentText1() {
        return this.jEditorPane1.getText();
    }

    String getDocumentText2() {
        return this.jEditorPane2.getText();
    }

    private void setHighlight(StyledDocument doc, int line1, int line2, Color color) {
        for (int line = line1 - 1; line < line2; ++line) {
            int offset;
            if (line < 0 || (offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line)) < 0) continue;
            Style s = doc.getLogicalStyle(offset);
            if (s == null) {
                s = doc.addStyle("diff-style(" + color + "):1500", null);
            }
            s.addAttribute(StyleConstants.ColorConstants.Background, color);
            doc.setLogicalStyle(offset, s);
        }
    }

    private void unhighlight(StyledDocument doc) {
        int endOffset = doc.getEndPosition().getOffset();
        int endLine = NbDocument.findLineNumber((StyledDocument)doc, (int)endOffset);
        Style s = doc.addStyle("diff-style(white):1500", null);
        s.addAttribute(StyleConstants.ColorConstants.Background, Color.white);
        for (int line = 0; line <= endLine; ++line) {
            int offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
            doc.setLogicalStyle(offset, s);
        }
    }

    public void unhighlightAll() {
        this.unhighlight((StyledDocument)this.jEditorPane1.getDocument());
        this.unhighlight((StyledDocument)this.jEditorPane2.getDocument());
    }

    public void highlightRegion1(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane1.getDocument();
        this.setHighlight(doc, line1, line2, color);
    }

    public void highlightRegion2(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane2.getDocument();
        this.setHighlight(doc, line1, line2, color);
    }

    private void addEmptyLines(StyledDocument doc, int line, int numLines) {
        int lastOffset = doc.getEndPosition().getOffset();
        int totLines = NbDocument.findLineNumber((StyledDocument)doc, (int)lastOffset);
        int offset = lastOffset - 1;
        if (line <= totLines) {
            offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
        }
        String insStr = this.strCharacters('\n', numLines);
        try {
            doc.insertString(offset, insStr, null);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    public void addEmptyLines1(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane1.getDocument();
        this.addEmptyLines(doc, line, numLines);
        this.linesComp1.addEmptyLines(line, numLines);
    }

    public void addEmptyLines2(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane2.getDocument();
        this.addEmptyLines(doc, line, numLines);
        this.linesComp2.addEmptyLines(line, numLines);
    }

    private void insertEmptyLines(boolean updateActionLines) {
        int n = this.diffs.length;
        block5 : for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            if (updateActionLines && i < n - 1) {
                this.diffShifts[i + 1][0] = this.diffShifts[i][0];
                this.diffShifts[i + 1][1] = this.diffShifts[i][1];
            }
            switch (action.getType()) {
                case 0: {
                    this.addEmptyLines2(n3, n2 - n1 + 1);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[1] = arrn[1] + (n2 - n1 + 1);
                    continue block5;
                }
                case 1: {
                    this.addEmptyLines1(n1, n4 - n3 + 1);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[0] = arrn[0] + (n4 - n3 + 1);
                    continue block5;
                }
                case 2: {
                    int r1 = n2 - n1;
                    int r2 = n4 - n3;
                    if (r1 < r2) {
                        this.addEmptyLines1(n2, r2 - r1);
                        if (!updateActionLines || i >= n - 1) continue block5;
                        int[] arrn = this.diffShifts[i + 1];
                        arrn[0] = arrn[0] + (r2 - r1);
                        continue block5;
                    }
                    if (r1 <= r2) continue block5;
                    this.addEmptyLines2(n4, r1 - r2);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[1] = arrn[1] + (r1 - r2);
                }
            }
        }
    }

    private void setDiffHighlight(boolean set) {
        int n = this.diffs.length;
        block5 : for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            switch (action.getType()) {
                case 0: {
                    if (set) {
                        this.highlightRegion1(n1, n2, this.colorMissing);
                        continue block5;
                    }
                    this.highlightRegion1(n1, n2, Color.white);
                    continue block5;
                }
                case 1: {
                    if (set) {
                        this.highlightRegion2(n3, n4, this.colorAdded);
                        continue block5;
                    }
                    this.highlightRegion2(n3, n4, Color.white);
                    continue block5;
                }
                case 2: {
                    if (set) {
                        this.highlightRegion1(n1, n2, this.colorChanged);
                        this.highlightRegion2(n3, n4, this.colorChanged);
                        continue block5;
                    }
                    this.highlightRegion1(n1, n2, Color.white);
                    this.highlightRegion2(n3, n4, Color.white);
                }
            }
        }
    }

    private void insertEmptyLinesNotReported() {
        String docText1 = this.getDocumentText1();
        String docText2 = this.getDocumentText2();
        int[] begin1 = new int[]{0};
        int[] end1 = new int[]{-1};
        int[] begin2 = new int[]{0};
        int[] end2 = new int[]{-1};
        int n1 = docText1.length();
        int n2 = docText2.length();
        int lineNumber = 1;
        int diffIndex = 0;
        do {
            int emptyLines2;
            int emptyLines1;
            boolean addMissingLine;
            int lastBegin1 = begin1[0];
            int lastBegin2 = begin2[0];
            String line1 = DiffViewImpl.readLine(begin1, end1, docText1);
            String line2 = DiffViewImpl.readLine(begin2, end2, docText2);
            if (line1.length() == 0 && line2.length() > 0) {
                if ((diffIndex = DiffViewImpl.findDiffForLine(lineNumber, diffIndex, this.diffs, this.diffShifts)) >= this.diffs.length || !DiffViewImpl.isLineInDiff(lineNumber, this.diffs[diffIndex], this.diffShifts[diffIndex])) {
                    if (line2.trim().length() == 0) {
                        emptyLines1 = DiffViewImpl.numEmptyLines(begin1[0], docText1, diffIndex < this.diffs.length ? this.diffs[diffIndex].getFirstStart() : -1);
                        emptyLines2 = DiffViewImpl.numEmptyLines(begin2[0], docText2, diffIndex < this.diffs.length ? this.diffs[diffIndex].getSecondStart() : -1);
                        addMissingLine = emptyLines1 > emptyLines2;
                    } else {
                        addMissingLine = true;
                    }
                    if (addMissingLine) {
                        this.addEmptyLines2(lineNumber - 1, 1);
                        this.shiftDiffs(false, lineNumber);
                        begin2[0] = lastBegin2;
                        end2[0] = lastBegin2 - 1;
                    }
                }
            } else if (!(line2.length() != 0 || line1.length() <= 0 || (diffIndex = DiffViewImpl.findDiffForLine(lineNumber, diffIndex, this.diffs, this.diffShifts)) < this.diffs.length && DiffViewImpl.isLineInDiff(lineNumber, this.diffs[diffIndex], this.diffShifts[diffIndex]))) {
                if (line1.trim().length() == 0) {
                    emptyLines1 = DiffViewImpl.numEmptyLines(begin1[0], docText1, diffIndex < this.diffs.length ? this.diffs[diffIndex].getFirstStart() : -1);
                    emptyLines2 = DiffViewImpl.numEmptyLines(begin2[0], docText2, diffIndex < this.diffs.length ? this.diffs[diffIndex].getSecondStart() : -1);
                    addMissingLine = emptyLines2 > emptyLines1;
                } else {
                    addMissingLine = true;
                }
                if (addMissingLine) {
                    this.addEmptyLines1(lineNumber - 1, 1);
                    this.shiftDiffs(true, lineNumber);
                    begin1[0] = lastBegin1;
                    end1[0] = lastBegin1 - 1;
                }
            }
            ++lineNumber;
        } while (begin1[0] < n1 && begin2[0] < n2);
    }

    private void shiftDiffs(boolean inFirstDoc, int fromLine) {
        int n = this.diffs.length;
        for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            if (inFirstDoc) {
                if (action.getFirstStart() + this.diffShifts[i][0] < fromLine) continue;
                int[] arrn = this.diffShifts[i];
                arrn[0] = arrn[0] + 1;
                continue;
            }
            if (action.getSecondStart() + this.diffShifts[i][1] < fromLine) continue;
            int[] arrn = this.diffShifts[i];
            arrn[1] = arrn[1] + 1;
        }
    }

    private static int numEmptyLines(int beginLine, String text, int endLine) {
        String line;
        if (endLine >= 0 && endLine <= beginLine) {
            return 0;
        }
        int numLines = 0;
        int[] begin = new int[]{beginLine};
        int[] end = new int[]{0};
        while ((line = DiffViewImpl.readLine(begin, end, text)).trim().length() <= 0 && (endLine < 0 || beginLine + ++numLines < endLine) && begin[0] < text.length()) {
        }
        return numLines;
    }

    private static int findDiffForLine(int lineNumber, int diffIndex, Difference[] diffs, int[][] diffShifts) {
        while (diffIndex < diffs.length && diffs[diffIndex].getFirstEnd() + diffShifts[diffIndex][0] < lineNumber && diffs[diffIndex].getSecondEnd() + diffShifts[diffIndex][1] < lineNumber) {
            ++diffIndex;
        }
        return diffIndex;
    }

    private static boolean isLineInDiff(int lineNumber, Difference diff, int[] diffShifts) {
        int l1 = diff.getFirstStart() + diffShifts[0];
        int l2 = diff.getFirstEnd() + diffShifts[0];
        int l3 = diff.getSecondStart() + diffShifts[1];
        int l4 = diff.getSecondEnd() + diffShifts[1];
        return l1 <= lineNumber && l2 >= l1 && l2 >= lineNumber || l3 <= lineNumber && l4 >= l3 && l4 >= lineNumber;
    }

    private static String readLine(int[] begin, int[] end, String text) {
        int n = text.length();
        for (int i = begin[0]; i < n; ++i) {
            char c = text.charAt(i);
            if (c != '\n' && c != '\r') continue;
            end[0] = i;
            break;
        }
        if (end[0] < begin[0]) {
            end[0] = n;
        }
        String line = text.substring(begin[0], end[0]);
        begin[0] = end[0] + 1;
        if (begin[0] < n && text.charAt(end[0]) == '\r' && text.charAt(begin[0]) == '\n') {
            int[] arrn = begin;
            arrn[0] = arrn[0] + 1;
        }
        return line;
    }

}

