/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.openide.ErrorManager
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.diff.Utils;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.openide.ErrorManager;
import org.openide.util.RequestProcessor;

class DecoratedEditorPane
extends JEditorPane
implements PropertyChangeListener {
    private Difference[] currentDiff;
    private DiffContentPanel master;
    private final RequestProcessor.Task repaintTask;
    private static final RequestProcessor FONT_RP = new RequestProcessor("DiffFontLoadingRP", 1);
    private int fontHeight = -1;
    private int charWidth;

    public DecoratedEditorPane(DiffContentPanel master) {
        this.repaintTask = Utils.createParallelTask(new RepaintPaneTask());
        this.setBorder(null);
        this.master = master;
        master.getMaster().addPropertyChangeListener(this);
    }

    public boolean isFirst() {
        return this.master.isFirst();
    }

    public DiffContentPanel getMaster() {
        return this.master;
    }

    void setDifferences(Difference[] diff) {
        this.currentDiff = diff;
        this.repaint();
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        this.setFontHeightWidth(this.getFont());
    }

    private void setFontHeightWidth(final Font font) {
        FONT_RP.post(new Runnable(){

            @Override
            public void run() {
                FontMetrics metrics = DecoratedEditorPane.this.getFontMetrics(font);
                DecoratedEditorPane.this.charWidth = metrics.charWidth('m');
                DecoratedEditorPane.this.fontHeight = metrics.getHeight();
            }
        });
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        if (this.fontHeight == -1) {
            return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
        }
        switch (orientation) {
            case 1: {
                return this.fontHeight;
            }
            case 0: {
                return this.charWidth;
            }
        }
        throw new IllegalArgumentException("Invalid orientation: " + orientation);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void paintComponent(Graphics gr) {
        block20 : {
            super.paintComponent(gr);
            if (this.currentDiff == null) {
                return;
            }
            EditorUI editorUI = Utilities.getEditorUI((JTextComponent)this);
            if (editorUI == null) {
                return;
            }
            Graphics2D g = (Graphics2D)gr.create();
            Rectangle clip = g.getClipBounds();
            Stroke cs = g.getStroke();
            --clip.y;
            ++clip.height;
            FoldHierarchy foldHierarchy = FoldHierarchy.get((JTextComponent)editorUI.getComponent());
            JTextComponent component = editorUI.getComponent();
            if (component == null) {
                return;
            }
            View rootView = Utilities.getDocumentView((JTextComponent)component);
            if (rootView == null) {
                return;
            }
            BaseTextUI textUI = (BaseTextUI)component.getUI();
            AbstractDocument doc = (AbstractDocument)component.getDocument();
            doc.readLock();
            try {
                foldHierarchy.lock();
                try {
                    int startPos = textUI.getPosFromY(clip.y);
                    int startViewIndex = rootView.getViewIndex(startPos, Position.Bias.Forward);
                    int rootViewCount = rootView.getViewCount();
                    if (startViewIndex < 0 || startViewIndex >= rootViewCount) break block20;
                    Rectangle rec = textUI.modelToView(component, rootView.getView(startViewIndex).getStartOffset());
                    int y = rec == null ? 0 : rec.y;
                    int clipEndY = clip.y + clip.height;
                    Element rootElem = textUI.getRootView(component).getElement();
                    View view = rootView.getView(startViewIndex);
                    int line = rootElem.getElementIndex(view.getStartOffset());
                    ++line;
                    int curDif = this.master.getMaster().getCurrentDifference();
                    g.setColor(this.master.getMaster().getColorLines());
                    for (int i = startViewIndex; i < rootViewCount; ++i) {
                        view = rootView.getView(i);
                        line = rootElem.getElementIndex(view.getStartOffset());
                        Difference ad = this.master.isFirst() ? EditableDiffView.getFirstDifference(this.currentDiff, line) : EditableDiffView.getSecondDifference(this.currentDiff, ++line);
                        Rectangle rec1 = component.modelToView(view.getStartOffset());
                        Rectangle rec2 = component.modelToView(view.getEndOffset() - 1);
                        if (rec1 != null && rec2 != null) {
                            y = (int)rec1.getY();
                            int height = (int)(rec2.getY() + rec2.getHeight() - rec1.getY());
                            if (ad != null) {
                                g.setStroke(curDif >= 0 && curDif < this.currentDiff.length && this.currentDiff[curDif] == ad ? this.master.getMaster().getBoldStroke() : cs);
                                int yy = y + height;
                                if (ad.getType() == (this.master.isFirst() ? 1 : 0)) {
                                    g.drawLine(0, yy, this.getWidth(), yy);
                                    ad = null;
                                } else {
                                    if ((this.master.isFirst() ? ad.getFirstStart() : ad.getSecondStart()) == line) {
                                        g.drawLine(0, y, this.getWidth(), y);
                                    }
                                    if ((this.master.isFirst() ? ad.getFirstEnd() : ad.getSecondEnd()) == line) {
                                        g.drawLine(0, yy, this.getWidth(), yy);
                                    }
                                }
                            }
                            if ((y += height) < clipEndY) {
                                continue;
                            }
                        }
                        break;
                    }
                }
                finally {
                    foldHierarchy.unlock();
                }
            }
            catch (BadLocationException ble) {
                ErrorManager.getDefault().notify((Throwable)ble);
            }
            finally {
                doc.readUnlock();
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.repaintTask.schedule(150);
    }

    private class RepaintPaneTask
    implements Runnable {
        private RepaintPaneTask() {
        }

        @Override
        public void run() {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    DecoratedEditorPane.this.repaint();
                }
            });
        }

    }

}

