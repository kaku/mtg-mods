/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin;

import java.io.IOException;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.spi.diff.DiffControllerImpl;
import org.netbeans.spi.diff.DiffControllerProvider;

public class DefaultDiffControllerProvider
extends DiffControllerProvider {
    @Override
    public DiffControllerImpl createDiffController(StreamSource base, StreamSource modified) throws IOException {
        return new EditableDiffView(base, modified);
    }

    @Override
    public DiffControllerImpl createEnhancedDiffController(StreamSource base, StreamSource modified) throws IOException {
        return new EditableDiffView(base, modified, true);
    }
}

