/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.List;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class DEditorPane
extends JEditorPane {
    private List popupActions;

    @Override
    protected void processMouseEvent(MouseEvent e) {
        if (!e.isPopupTrigger()) {
            super.processMouseEvent(e);
        } else if (this.popupActions != null) {
            JPopupMenu popup = new JPopupMenu();
            Iterator it = this.popupActions.iterator();
            int actions = 0;
            while (it.hasNext()) {
                Action action = (Action)it.next();
                if (action == null) continue;
                popup.add(action);
                ++actions;
            }
            if (actions > 0) {
                popup.show(e.getComponent(), e.getX() - 7, e.getY() - 10);
            }
        }
    }

    public void setPopupActions(List actions) {
        this.popupActions = actions;
    }
}

