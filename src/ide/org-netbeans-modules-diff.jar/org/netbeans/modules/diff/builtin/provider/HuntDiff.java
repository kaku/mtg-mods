/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.builtin.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.provider.BuiltInDiffProvider;

class HuntDiff {
    private static final Pattern spaces = Pattern.compile("(\\s+)");

    private HuntDiff() {
    }

    public static Difference[] diff(String[] lines1, String[] lines2, BuiltInDiffProvider.Options options) {
        int[] J = HuntDiff.prepareIndex(lines1, lines2, options);
        List<Difference2> differences = HuntDiff.getDifferences(J, lines1, lines2);
        HuntDiff.cleanup(differences);
        Difference2[] tempDiffs = differences.toArray(new Difference2[differences.size()]);
        Difference[] diffs = new Difference[tempDiffs.length];
        for (int i = 0; i < tempDiffs.length; ++i) {
            Difference2 tempDiff = tempDiffs[i];
            tempDiffs[i] = null;
            diffs[i] = new Difference(tempDiff.getType(), tempDiff.getFirstStart(), tempDiff.getFirstEnd(), tempDiff.getSecondStart(), tempDiff.getSecondEnd(), tempDiff.getFirstText(), tempDiff.getSecondText());
        }
        return diffs;
    }

    private static int[] prepareIndex(String[] lines1, String[] lines2, BuiltInDiffProvider.Options options) {
        int m = lines1.length;
        int n = lines2.length;
        if (options.ignoreCase || options.ignoreInnerWhitespace || options.ignoreLeadingAndtrailingWhitespace) {
            lines1 = HuntDiff.copy(lines1);
            lines2 = HuntDiff.copy(lines2);
            HuntDiff.applyDiffOptions(lines1, lines2, options);
        }
        Line[] l2s = new Line[n + 1];
        for (int i = 1; i <= n; ++i) {
            l2s[i] = new Line(i, lines2[i - 1]);
        }
        Arrays.sort(l2s, 1, n + 1, new Comparator<Line>(){

            @Override
            public int compare(Line l1, Line l2) {
                return l1.line.compareTo(l2.line);
            }
        });
        int[] equvalenceLines = new int[n + 1];
        boolean[] equivalence = new boolean[n + 1];
        for (int i2 = 1; i2 <= n; ++i2) {
            Line l = l2s[i2];
            equvalenceLines[i2] = l.lineNo;
            equivalence[i2] = i2 == n || !l.line.equals(l2s[i2 + 1].line);
        }
        equvalenceLines[0] = 0;
        equivalence[0] = true;
        int[] equivalenceAssoc = new int[m + 1];
        for (int i3 = 1; i3 <= m; ++i3) {
            equivalenceAssoc[i3] = HuntDiff.findAssoc(lines1[i3 - 1], l2s, equivalence);
        }
        l2s = null;
        Candidate[] K = new Candidate[java.lang.Math.min(m, n) + 2];
        K[0] = new Candidate(0, 0, null);
        K[1] = new Candidate(m + 1, n + 1, null);
        int k = 0;
        for (int i4 = 1; i4 <= m; ++i4) {
            if (equivalenceAssoc[i4] == 0) continue;
            k = HuntDiff.merge(K, k, i4, equvalenceLines, equivalence, equivalenceAssoc[i4]);
        }
        int[] J = new int[m + 2];
        Candidate c = K[k];
        while (c != null) {
            J[Candidate.access$000((Candidate)c)] = c.b;
            c = c.c;
        }
        return J;
    }

    private static String[] copy(String[] strings) {
        String[] copy = new String[strings.length];
        for (int i = 0; i < strings.length; ++i) {
            copy[i] = strings[i];
        }
        return copy;
    }

    private static void applyDiffOptions(String[] lines1, String[] lines2, BuiltInDiffProvider.Options options) {
        int i;
        if (options.ignoreLeadingAndtrailingWhitespace && options.ignoreInnerWhitespace) {
            for (i = 0; i < lines1.length; ++i) {
                lines1[i] = spaces.matcher(lines1[i]).replaceAll("");
            }
            for (i = 0; i < lines2.length; ++i) {
                lines2[i] = spaces.matcher(lines2[i]).replaceAll("");
            }
        } else if (options.ignoreLeadingAndtrailingWhitespace) {
            for (i = 0; i < lines1.length; ++i) {
                lines1[i] = lines1[i].trim();
            }
            for (i = 0; i < lines2.length; ++i) {
                lines2[i] = lines2[i].trim();
            }
        } else if (options.ignoreInnerWhitespace) {
            for (i = 0; i < lines1.length; ++i) {
                HuntDiff.replaceInnerSpaces(lines1, i);
            }
            for (i = 0; i < lines2.length; ++i) {
                HuntDiff.replaceInnerSpaces(lines2, i);
            }
        }
        if (options.ignoreCase) {
            for (i = 0; i < lines1.length; ++i) {
                lines1[i] = lines1[i].toUpperCase();
            }
            for (i = 0; i < lines2.length; ++i) {
                lines2[i] = lines2[i].toUpperCase();
            }
        }
    }

    private static void replaceInnerSpaces(String[] strings, int idx) {
        Matcher m = spaces.matcher(strings[idx]);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            if (m.start() == 0 || m.end() == strings[idx].length()) {
                m.appendReplacement(sb, "$1");
                continue;
            }
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        strings[idx] = sb.toString();
    }

    private static int findAssoc(String line1, Line[] l2s, boolean[] equivalence) {
        int idx = HuntDiff.binarySearch(l2s, line1, 1, l2s.length - 1);
        if (idx < 1) {
            return 0;
        }
        int lastGoodIdx = 0;
        while (idx >= 1 && l2s[idx].line.equals(line1)) {
            if (equivalence[idx - 1]) {
                lastGoodIdx = idx;
            }
            --idx;
        }
        return lastGoodIdx;
    }

    private static int binarySearch(Line[] L, String key, int low, int high) {
        while (low <= high) {
            int mid = low + high >> 1;
            String midVal = L[mid].line;
            int comparison = midVal.compareTo(key);
            if (comparison < 0) {
                low = mid + 1;
                continue;
            }
            if (comparison > 0) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    private static int binarySearch(Candidate[] K, int key, int low, int high) {
        while (low <= high) {
            int mid = low + high >> 1;
            int midVal = K[mid].b;
            if (midVal < key) {
                low = mid + 1;
                continue;
            }
            if (midVal > key) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    private static int merge(Candidate[] K, int k, int i, int[] equvalenceLines, boolean[] equivalence, int p) {
        int r = 0;
        Candidate c = K[0];
        do {
            int j;
            int s;
            if ((s = HuntDiff.binarySearch(K, j = equvalenceLines[p], r, k)) >= 0) {
                s = k + 1;
            } else if ((s = - s - 2) < r || s > k) {
                s = k + 1;
            }
            if (s <= k) {
                if (K[s + 1].b > j) {
                    Candidate newc = new Candidate(i, j, K[s]);
                    K[r] = c;
                    r = s + 1;
                    c = newc;
                }
                if (s == k) {
                    K[k + 2] = K[k + 1];
                    ++k;
                    break;
                }
            }
            if (equivalence[p]) break;
            ++p;
        } while (true);
        K[r] = c;
        return k;
    }

    private static List<Difference2> getDifferences(int[] J, String[] lines1, String[] lines2) {
        ArrayList<String> addedLines;
        int end2;
        ArrayList<Difference2> differences = new ArrayList<Difference2>();
        int n = lines1.length;
        int m = lines2.length;
        int start1 = 1;
        int start2 = 1;
        do {
            if (start1 <= n && J[start1] == start2) {
                ++start1;
                ++start2;
                continue;
            }
            if (start1 > n) break;
            if (J[start1] < start2) {
                int end1;
                ArrayList<String> deletedLines = new ArrayList<String>();
                deletedLines.add(lines1[start1 - 1]);
                for (end1 = start1 + 1; end1 <= n && J[end1] < start2; ++end1) {
                    String line = lines1[end1 - 1];
                    deletedLines.add(line);
                }
                differences.add(new Difference2(0, start1, end1 - 1, start2 - 1, 0, deletedLines.toArray(new String[deletedLines.size()]), null));
                start1 = end1;
            } else {
                end2 = J[start1];
                addedLines = new ArrayList();
                for (int i = start2; i < end2; ++i) {
                    String line = lines2[i - 1];
                    addedLines.add(line);
                }
                differences.add(new Difference2(1, start1 - 1, 0, start2, end2 - 1, null, addedLines.toArray(new String[addedLines.size()])));
                start2 = end2;
            }
            if (start1 > n) break;
        } while (true);
        if (start2 <= m) {
            addedLines = new ArrayList<String>();
            addedLines.add(lines2[start2 - 1]);
            for (end2 = start2 + 1; end2 <= m; ++end2) {
                String line = lines2[end2 - 1];
                addedLines.add(line);
            }
            differences.add(new Difference2(1, n, 0, start2, m, null, addedLines.toArray(new String[addedLines.size()])));
        }
        return differences;
    }

    private static void cleanup(List<Difference2> diffs) {
        Difference2 last = null;
        for (int i = 0; i < diffs.size(); ++i) {
            Difference2 diff = diffs.get(i);
            if (last != null && (diff.getType() == 1 && last.getType() == 0 || diff.getType() == 0 && last.getType() == 1)) {
                Difference2 add;
                Difference2 del;
                if (1 == diff.getType()) {
                    add = diff;
                    del = last;
                } else {
                    add = last;
                    del = diff;
                }
                int d1f1l1 = add.getFirstStart() - (del.getFirstEnd() - del.getFirstStart());
                int d2f1l1 = del.getFirstStart();
                if (d1f1l1 == d2f1l1) {
                    Difference2 newDiff = new Difference2(2, d1f1l1, del.getFirstEnd(), add.getSecondStart(), add.getSecondEnd(), del.getFirstLines(), add.getSecondLines());
                    diffs.set(i - 1, newDiff);
                    diffs.remove(i);
                    --i;
                    diff = newDiff;
                }
            }
            last = diff;
        }
    }

    private static String mergeLines(String[] lines) {
        int capacity = lines.length;
        for (String line : lines) {
            capacity += line.length();
        }
        StringBuilder sb = new StringBuilder(capacity);
        for (int i = 0; i < lines.length; ++i) {
            String line2 = lines[i];
            lines[i] = null;
            sb.append(line2).append('\n');
        }
        return sb.toString();
    }

    private static class Difference2
    extends Difference {
        private final String[] firstLines;
        private final String[] secondLines;
        private String leftText;
        private String rightText;

        public Difference2(int type, int firstStart, int firstEnd, int secondStart, int secondEnd, String[] lines1, String[] lines2) {
            super(type, firstStart, firstEnd, secondStart, secondEnd);
            this.firstLines = lines1;
            this.secondLines = lines2;
        }

        public String[] getFirstLines() {
            return this.firstLines;
        }

        public String[] getSecondLines() {
            return this.secondLines;
        }

        @Override
        public String getFirstText() {
            if (this.leftText == null && this.firstLines != null) {
                this.leftText = HuntDiff.mergeLines(this.firstLines);
            }
            return this.leftText;
        }

        @Override
        public String getSecondText() {
            if (this.rightText == null && this.secondLines != null) {
                this.rightText = HuntDiff.mergeLines(this.secondLines);
            }
            return this.rightText;
        }
    }

    private static class Candidate {
        private int a;
        private int b;
        private Candidate c;

        public Candidate(int a, int b, Candidate c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        static /* synthetic */ int access$000(Candidate x0) {
            return x0.a;
        }
    }

    private static class Line {
        public int lineNo;
        public String line;
        public int hash;

        public Line(int lineNo, String line) {
            this.lineNo = lineNo;
            this.line = line;
            this.hash = line.hashCode();
        }
    }

}

