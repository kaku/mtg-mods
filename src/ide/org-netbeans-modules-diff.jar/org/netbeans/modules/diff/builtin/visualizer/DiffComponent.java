/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 *  org.openide.windows.Workspace
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;
import java.io.Reader;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.visualizer.DiffPanel;
import org.openide.ErrorManager;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.Workspace;

public class DiffComponent
extends TopComponent {
    public static final Color COLOR_MISSING = new Color(255, 160, 180);
    public static final Color COLOR_ADDED = new Color(180, 255, 180);
    public static final Color COLOR_CHANGED = new Color(160, 200, 255);
    private Difference[] diffs = null;
    private int[][] diffShifts;
    private DiffPanel diffPanel = null;
    private Color colorMissing = COLOR_MISSING;
    private Color colorAdded = COLOR_ADDED;
    private Color colorChanged = COLOR_CHANGED;
    private int currentDiffLine = -1;
    private boolean diffSetSuccess = true;
    static final long serialVersionUID = 3683458237532937983L;

    public DiffComponent() {
        this.putClientProperty((Object)"PersistenceType", (Object)"Never");
    }

    public DiffComponent(Difference[] diffs, String mainTitle, String mimeType, String sourceName1, String sourceName2, String title1, String title2, Reader r1, Reader r2) {
        this(diffs, mainTitle, mimeType, sourceName1, sourceName2, title1, title2, r1, r2, null);
    }

    public DiffComponent(final Difference[] diffs, String mainTitle, String mimeType, String sourceName1, String sourceName2, String title1, String title2, Reader r1, Reader r2, Color[] colors) {
        this.diffs = diffs;
        this.diffShifts = new int[diffs.length][2];
        this.setLayout((LayoutManager)new BorderLayout());
        this.diffPanel = new DiffPanel();
        this.putClientProperty((Object)"DiffPresenter.toolbarPanel", this.diffPanel.getClientProperty("DiffPresenter.toolbarPanel"));
        this.diffPanel.addPrevLineButtonListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                if (diffs.length == 0) {
                    return;
                }
                DiffComponent.this.currentDiffLine--;
                if (DiffComponent.this.currentDiffLine < 0) {
                    DiffComponent.this.currentDiffLine = diffs.length - 1;
                }
                DiffComponent.this.showCurrentLine();
            }
        });
        this.diffPanel.addNextLineButtonListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                if (diffs.length == 0) {
                    return;
                }
                DiffComponent.this.currentDiffLine++;
                if (DiffComponent.this.currentDiffLine >= diffs.length) {
                    DiffComponent.this.currentDiffLine = 0;
                }
                DiffComponent.this.showCurrentLine();
            }
        });
        this.add((Component)this.diffPanel, (Object)"Center");
        if (colors != null && colors.length >= 3) {
            this.colorMissing = colors[0];
            this.colorAdded = colors[1];
            this.colorChanged = colors[2];
        }
        if (mainTitle == null) {
            this.setName(NbBundle.getBundle(DiffComponent.class).getString("DiffComponent.title"));
        } else {
            this.setName(mainTitle);
        }
        this.setIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/diff/diffSettingsIcon.gif", (boolean)true));
        this.initContent(mimeType, sourceName1, sourceName2, title1, title2, r1, r2);
        this.putClientProperty((Object)"PersistenceType", (Object)"Never");
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(DiffComponent.class);
    }

    private void showCurrentLine() {
        if (this.currentDiffLine >= this.diffs.length) {
            return;
        }
        Difference diff = this.diffs[this.currentDiffLine];
        int line = diff.getFirstStart() + this.diffShifts[this.currentDiffLine][0];
        if (diff.getType() == 1) {
            ++line;
        }
        int lf1 = diff.getFirstEnd() - diff.getFirstStart() + 1;
        int lf2 = diff.getSecondEnd() - diff.getSecondStart() + 1;
        int length = Math.max(lf1, lf2);
        this.diffPanel.setCurrentLine(line, length);
    }

    private void initContent(String mimeType, String sourceName1, String sourceName2, String title1, String title2, Reader r1, Reader r2) {
        this.setMimeType1(mimeType);
        this.setMimeType2(mimeType);
        try {
            this.setSource1(r1);
            this.setSource2(r2);
        }
        catch (IOException ioex) {
            ErrorManager.getDefault().notify((Throwable)ioex);
        }
        this.setSource1Title(title1);
        this.setSource2Title(title2);
        this.insertEmptyLines(true);
        this.setDiffHighlight(true);
        this.insertEmptyLinesNotReported();
    }

    @Deprecated
    public void open(Workspace workspace) {
        super.open(workspace);
        this.diffPanel.open();
        if (this.currentDiffLine < 0) {
            this.currentDiffLine = 0;
            this.showCurrentLine();
        }
    }

    @Deprecated
    public void requestFocus() {
        super.requestFocus();
        this.diffPanel.requestFocus();
    }

    @Deprecated
    public boolean requestFocusInWindow() {
        super.requestFocusInWindow();
        return this.diffPanel.requestFocusInWindow();
    }

    public void addNotify() {
        super.addNotify();
        if (this.currentDiffLine < 0) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    DiffComponent.this.diffPanel.open();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            Toolkit.getDefaultToolkit().sync();
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    if (DiffComponent.this.currentDiffLine < 0) {
                                        DiffComponent.this.currentDiffLine = 0;
                                        DiffComponent.this.showCurrentLine();
                                    }
                                }
                            });
                        }

                    });
                }

            });
        }
    }

    @Deprecated
    public boolean canClose(Workspace workspace, boolean last) {
        boolean can = super.canClose(workspace, last);
        if (last && can) {
            this.exitForm(null);
        }
        return can;
    }

    public void setSource1(Reader r) throws IOException {
        this.diffPanel.setSource1(r);
    }

    public void setSource2(Reader r) throws IOException {
        this.diffPanel.setSource2(r);
    }

    public void setSource1Title(String title) {
        this.diffPanel.setSource1Title(title);
    }

    public void setSource2Title(String title) {
        this.diffPanel.setSource2Title(title);
    }

    public void setMimeType1(String mime) {
        this.diffPanel.setMimeType1(mime);
    }

    public void setMimeType2(String mime) {
        this.diffPanel.setMimeType2(mime);
    }

    public void setDocument1(Document doc) {
        this.diffPanel.setDocument1(doc);
    }

    public void setDocument2(Document doc) {
        this.diffPanel.setDocument2(doc);
    }

    public void unhighlightAll() {
        this.diffPanel.unhighlightAll();
    }

    public void highlightRegion1(int line1, int line2, Color color) {
        this.diffPanel.highlightRegion1(line1, line2, color);
    }

    public void highlightRegion2(int line1, int line2, Color color) {
        this.diffPanel.highlightRegion2(line1, line2, color);
    }

    public void addEmptyLines1(int line, int numLines) {
        this.diffPanel.addEmptyLines1(line, numLines);
    }

    public void addEmptyLines2(int line, int numLines) {
        this.diffPanel.addEmptyLines2(line, numLines);
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
        out.writeObject(this.diffs);
    }

    private void insertEmptyLines(boolean updateActionLines) {
        int n = this.diffs.length;
        block5 : for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            if (updateActionLines && i < n - 1) {
                this.diffShifts[i + 1][0] = this.diffShifts[i][0];
                this.diffShifts[i + 1][1] = this.diffShifts[i][1];
            }
            switch (action.getType()) {
                case 0: {
                    this.addEmptyLines2(n3, n2 - n1 + 1);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[1] = arrn[1] + (n2 - n1 + 1);
                    continue block5;
                }
                case 1: {
                    this.addEmptyLines1(n1, n4 - n3 + 1);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[0] = arrn[0] + (n4 - n3 + 1);
                    continue block5;
                }
                case 2: {
                    int r1 = n2 - n1;
                    int r2 = n4 - n3;
                    if (r1 < r2) {
                        this.addEmptyLines1(n2, r2 - r1);
                        if (!updateActionLines || i >= n - 1) continue block5;
                        int[] arrn = this.diffShifts[i + 1];
                        arrn[0] = arrn[0] + (r2 - r1);
                        continue block5;
                    }
                    if (r1 <= r2) continue block5;
                    this.addEmptyLines2(n4, r1 - r2);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[1] = arrn[1] + (r1 - r2);
                }
            }
        }
    }

    private void setDiffHighlight(boolean set) {
        int n = this.diffs.length;
        block5 : for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            switch (action.getType()) {
                case 0: {
                    if (set) {
                        this.highlightRegion1(n1, n2, this.colorMissing);
                        continue block5;
                    }
                    this.highlightRegion1(n1, n2, Color.white);
                    continue block5;
                }
                case 1: {
                    if (set) {
                        this.highlightRegion2(n3, n4, this.colorAdded);
                        continue block5;
                    }
                    this.highlightRegion2(n3, n4, Color.white);
                    continue block5;
                }
                case 2: {
                    if (set) {
                        this.highlightRegion1(n1, n2, this.colorChanged);
                        this.highlightRegion2(n3, n4, this.colorChanged);
                        continue block5;
                    }
                    this.highlightRegion1(n1, n2, Color.white);
                    this.highlightRegion2(n3, n4, Color.white);
                }
            }
        }
    }

    private static String readLine(int[] begin, int[] end, String text) {
        int n = text.length();
        for (int i = begin[0]; i < n; ++i) {
            char c = text.charAt(i);
            if (c != '\n' && c != '\r') continue;
            end[0] = i;
            break;
        }
        if (end[0] < begin[0]) {
            end[0] = n;
        }
        String line = text.substring(begin[0], end[0]);
        begin[0] = end[0] + 1;
        if (begin[0] < n && text.charAt(end[0]) == '\r' && text.charAt(begin[0]) == '\n') {
            int[] arrn = begin;
            arrn[0] = arrn[0] + 1;
        }
        return line;
    }

    private static int findDiffForLine(int lineNumber, int diffIndex, Difference[] diffs, int[][] diffShifts) {
        while (diffIndex < diffs.length && diffs[diffIndex].getFirstEnd() + diffShifts[diffIndex][0] < lineNumber && diffs[diffIndex].getSecondEnd() + diffShifts[diffIndex][1] < lineNumber) {
            ++diffIndex;
        }
        return diffIndex;
    }

    private static boolean isLineInDiff(int lineNumber, Difference diff, int[] diffShifts) {
        int l1 = diff.getFirstStart() + diffShifts[0];
        int l2 = diff.getFirstEnd() + diffShifts[0];
        int l3 = diff.getSecondStart() + diffShifts[1];
        int l4 = diff.getSecondEnd() + diffShifts[1];
        return l1 <= lineNumber && l2 >= l1 && l2 >= lineNumber || l3 <= lineNumber && l4 >= l3 && l4 >= lineNumber;
    }

    private static int numEmptyLines(int beginLine, String text, int endLine) {
        String line;
        if (endLine >= 0 && endLine <= beginLine) {
            return 0;
        }
        int numLines = 0;
        int[] begin = new int[]{beginLine};
        int[] end = new int[]{0};
        while ((line = DiffComponent.readLine(begin, end, text)).trim().length() <= 0 && (endLine < 0 || beginLine + ++numLines < endLine) && begin[0] < text.length()) {
        }
        return numLines;
    }

    private void insertEmptyLinesNotReported() {
        String docText1 = this.diffPanel.getDocumentText1();
        String docText2 = this.diffPanel.getDocumentText2();
        int[] begin1 = new int[]{0};
        int[] end1 = new int[]{-1};
        int[] begin2 = new int[]{0};
        int[] end2 = new int[]{-1};
        int n1 = docText1.length();
        int n2 = docText2.length();
        int lineNumber = 1;
        int diffIndex = 0;
        do {
            int emptyLines2;
            int emptyLines1;
            boolean addMissingLine;
            int lastBegin1 = begin1[0];
            int lastBegin2 = begin2[0];
            String line1 = DiffComponent.readLine(begin1, end1, docText1);
            String line2 = DiffComponent.readLine(begin2, end2, docText2);
            if (line1.length() == 0 && line2.length() > 0) {
                if ((diffIndex = DiffComponent.findDiffForLine(lineNumber, diffIndex, this.diffs, this.diffShifts)) >= this.diffs.length || !DiffComponent.isLineInDiff(lineNumber, this.diffs[diffIndex], this.diffShifts[diffIndex])) {
                    if (line2.trim().length() == 0) {
                        emptyLines1 = DiffComponent.numEmptyLines(begin1[0], docText1, diffIndex < this.diffs.length ? this.diffs[diffIndex].getFirstStart() : -1);
                        emptyLines2 = DiffComponent.numEmptyLines(begin2[0], docText2, diffIndex < this.diffs.length ? this.diffs[diffIndex].getSecondStart() : -1);
                        addMissingLine = emptyLines1 > emptyLines2;
                    } else {
                        addMissingLine = true;
                    }
                    if (addMissingLine) {
                        this.addEmptyLines2(lineNumber - 1, 1);
                        this.shiftDiffs(false, lineNumber);
                        begin2[0] = lastBegin2;
                        end2[0] = lastBegin2 - 1;
                    }
                }
            } else if (!(line2.length() != 0 || line1.length() <= 0 || (diffIndex = DiffComponent.findDiffForLine(lineNumber, diffIndex, this.diffs, this.diffShifts)) < this.diffs.length && DiffComponent.isLineInDiff(lineNumber, this.diffs[diffIndex], this.diffShifts[diffIndex]))) {
                if (line1.trim().length() == 0) {
                    emptyLines1 = DiffComponent.numEmptyLines(begin1[0], docText1, diffIndex < this.diffs.length ? this.diffs[diffIndex].getFirstStart() : -1);
                    emptyLines2 = DiffComponent.numEmptyLines(begin2[0], docText2, diffIndex < this.diffs.length ? this.diffs[diffIndex].getSecondStart() : -1);
                    addMissingLine = emptyLines2 > emptyLines1;
                } else {
                    addMissingLine = true;
                }
                if (addMissingLine) {
                    this.addEmptyLines1(lineNumber - 1, 1);
                    this.shiftDiffs(true, lineNumber);
                    begin1[0] = lastBegin1;
                    end1[0] = lastBegin1 - 1;
                }
            }
            ++lineNumber;
        } while (begin1[0] < n1 && begin2[0] < n2);
    }

    private void shiftDiffs(boolean inFirstDoc, int fromLine) {
        int n = this.diffs.length;
        for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            if (inFirstDoc) {
                if (action.getFirstStart() + this.diffShifts[i][0] < fromLine) continue;
                int[] arrn = this.diffShifts[i];
                arrn[0] = arrn[0] + 1;
                continue;
            }
            if (action.getSecondStart() + this.diffShifts[i][1] < fromLine) continue;
            int[] arrn = this.diffShifts[i];
            arrn[1] = arrn[1] + 1;
        }
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
        Object obj = in.readObject();
        this.diffs = (Difference[])obj;
        this.diffPanel = new DiffPanel();
    }

    private Object readResolve() throws ObjectStreamException {
        if (this.diffSetSuccess) {
            return this;
        }
        return null;
    }

    protected Object writeReplace() throws ObjectStreamException {
        this.exitForm(null);
        return null;
    }

    private void exitForm(WindowEvent evt) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DiffComponent.this.diffPanel = null;
                DiffComponent.this.diffs = null;
                DiffComponent.this.removeAll();
            }
        });
    }

}

