/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.GeneralPath;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.accessibility.Accessible;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffViewManager;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.modules.diff.builtin.visualizer.editable.HotSpot;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class DiffSplitPaneDivider
extends BasicSplitPaneDivider
implements MouseMotionListener,
MouseListener,
Accessible {
    private final Image insertAllImage = Utilities.loadImage((String)"org/netbeans/modules/diff/builtin/visualizer/editable/move_all.png");
    private final Image insertAllActiveImage = Utilities.loadImage((String)"org/netbeans/modules/diff/builtin/visualizer/editable/move_all_active.png");
    private final int actionIconsHeight;
    private final int actionIconsWidth;
    private final Point POINT_ZERO;
    private final EditableDiffView master;
    private Point lastMousePosition;
    private HotSpot lastHotSpot;
    private List<HotSpot> hotspots;
    private DiffSplitDivider mydivider;
    private final Color fontColor;

    DiffSplitPaneDivider(BasicSplitPaneUI splitPaneUI, EditableDiffView master) {
        super(splitPaneUI);
        this.lastMousePosition = this.POINT_ZERO = new Point(0, 0);
        this.lastHotSpot = null;
        this.hotspots = new ArrayList<HotSpot>(0);
        this.master = master;
        this.fontColor = new JLabel().getForeground();
        this.actionIconsHeight = this.insertAllImage.getHeight(this);
        this.actionIconsWidth = this.insertAllImage.getWidth(this);
        this.setBorder(null);
        this.setLayout(new BorderLayout());
        this.mydivider = new DiffSplitDivider();
        this.add(this.mydivider);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        HotSpot spot;
        if (!e.isPopupTrigger() && (spot = this.getHotspotAt(e.getPoint())) != null) {
            this.performAction();
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.lastMousePosition = this.POINT_ZERO;
        if (this.lastHotSpot != null) {
            this.mydivider.repaint(this.lastHotSpot.getRect());
        }
        this.lastHotSpot = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point p;
        this.lastMousePosition = p = e.getPoint();
        HotSpot spot = this.getHotspotAt(p);
        if (this.lastHotSpot != spot) {
            this.mydivider.repaint(this.lastHotSpot == null ? spot.getRect() : this.lastHotSpot.getRect());
        }
        this.lastHotSpot = spot;
        this.setCursor(spot != null ? Cursor.getPredefinedCursor(12) : Cursor.getPredefinedCursor(11));
        if (spot != null) {
            ToolTipManager.sharedInstance().mouseMoved(new MouseEvent(this.mydivider, 0, 0, 0, spot.getRect().x + 5, spot.getRect().y + 5, 0, false));
        } else {
            ToolTipManager.sharedInstance().mouseMoved(new MouseEvent(this.mydivider, 0, 0, 0, 0, 0, 0, false));
            this.mydivider.repaint();
        }
    }

    private void performAction() {
        this.master.rollback(null);
    }

    @Override
    public void setBorder(Border border) {
        super.setBorder(BorderFactory.createEmptyBorder());
    }

    DiffSplitDivider getDivider() {
        return this.mydivider;
    }

    private HotSpot getHotspotAt(Point p) {
        for (HotSpot hotspot : this.hotspots) {
            if (!hotspot.getRect().contains(p)) continue;
            return hotspot;
        }
        return null;
    }

    private class DiffSplitDivider
    extends JPanel {
        private Map renderingHints;

        public DiffSplitDivider() {
            this.setBackground(UIManager.getColor("SplitPane.background"));
            this.setOpaque(true);
            this.renderingHints = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                this.setBackground(UIManager.getColor("NbExplorerView.background"));
            }
        }

        @Override
        public String getToolTipText(MouseEvent event) {
            Point p = event.getPoint();
            HotSpot spot = DiffSplitPaneDivider.this.getHotspotAt(p);
            if (spot == null) {
                return null;
            }
            return NbBundle.getMessage(DiffSplitDivider.class, (String)"TT_DiffPanel_MoveAll");
        }

        @Override
        protected void paintComponent(Graphics gr) {
            Graphics2D g = (Graphics2D)gr.create();
            Rectangle clip = g.getClipBounds();
            Stroke cs = g.getStroke();
            g.setColor(this.getBackground());
            g.fillRect(clip.x, clip.y, clip.width, clip.height);
            if (DiffSplitPaneDivider.this.master.getEditorPane1() == null) {
                g.dispose();
                return;
            }
            Rectangle rightView = DiffSplitPaneDivider.this.master.getEditorPane2().getScrollPane().getViewport().getViewRect();
            Rectangle leftView = DiffSplitPaneDivider.this.master.getEditorPane1().getScrollPane().getViewport().getViewRect();
            int editorsOffset = DiffSplitPaneDivider.access$100((DiffSplitPaneDivider)DiffSplitPaneDivider.this).getEditorPane2().getLocation().y + DiffSplitPaneDivider.access$100((DiffSplitPaneDivider)DiffSplitPaneDivider.this).getEditorPane2().getInsets().top;
            int rightOffset = - rightView.y + editorsOffset;
            int leftOffset = - leftView.y + editorsOffset;
            if (this.renderingHints != null) {
                g.addRenderingHints(this.renderingHints);
            }
            String diffInfo = "" + (DiffSplitPaneDivider.this.master.getCurrentDifference() + 1) + "/" + DiffSplitPaneDivider.this.master.getDifferenceCount();
            int width = g.getFontMetrics().stringWidth(diffInfo);
            g.setColor(DiffSplitPaneDivider.this.fontColor);
            g.drawString(diffInfo, (this.getWidth() - width) / 2, g.getFontMetrics().getHeight());
            if (clip.y < editorsOffset) {
                g.setClip(clip.x, editorsOffset, clip.width, clip.height);
            }
            int rightY = this.getWidth() - 1;
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(0, clip.y, 0, clip.height);
            g.drawLine(rightY, clip.y, rightY, clip.height);
            int curDif = DiffSplitPaneDivider.this.master.getCurrentDifference();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            DiffViewManager.DecoratedDifference[] decoratedDiffs = DiffSplitPaneDivider.this.master.getManager().getDecorations();
            int idx = 0;
            boolean everythingEditable = true;
            for (DiffViewManager.DecoratedDifference dd : decoratedDiffs) {
                everythingEditable &= dd.canRollback();
                g.setColor(DiffSplitPaneDivider.this.master.getColor(dd.getDiff()));
                g.setStroke(curDif == idx++ ? DiffSplitPaneDivider.this.master.getBoldStroke() : cs);
                if (dd.getBottomLeft() == -1) {
                    this.paintMatcher(g, DiffSplitPaneDivider.this.master.getColor(dd.getDiff()), 0, rightY, dd.getTopLeft() + leftOffset, dd.getTopRight() + rightOffset, dd.getBottomRight() + rightOffset, dd.getTopLeft() + leftOffset);
                    continue;
                }
                if (dd.getBottomRight() == -1) {
                    this.paintMatcher(g, DiffSplitPaneDivider.this.master.getColor(dd.getDiff()), 0, rightY, dd.getTopLeft() + leftOffset, dd.getTopRight() + rightOffset, dd.getTopRight() + rightOffset, dd.getBottomLeft() + leftOffset);
                    continue;
                }
                this.paintMatcher(g, DiffSplitPaneDivider.this.master.getColor(dd.getDiff()), 0, rightY, dd.getTopLeft() + leftOffset, dd.getTopRight() + rightOffset, dd.getBottomRight() + rightOffset, dd.getBottomLeft() + leftOffset);
            }
            if (DiffSplitPaneDivider.this.master.isActionsEnabled() && everythingEditable) {
                ArrayList<HotSpot> newActionIcons = new ArrayList<HotSpot>();
                Rectangle hotSpot = new Rectangle((this.getWidth() - DiffSplitPaneDivider.this.actionIconsWidth) / 2, editorsOffset, DiffSplitPaneDivider.this.actionIconsWidth, DiffSplitPaneDivider.this.actionIconsHeight);
                if (hotSpot.contains(DiffSplitPaneDivider.this.lastMousePosition)) {
                    g.drawImage(DiffSplitPaneDivider.this.insertAllActiveImage, hotSpot.x, hotSpot.y, this);
                } else {
                    g.drawImage(DiffSplitPaneDivider.this.insertAllImage, hotSpot.x, hotSpot.y, this);
                }
                newActionIcons.add(new HotSpot(hotSpot, null));
                DiffSplitPaneDivider.this.hotspots = newActionIcons;
            }
            g.dispose();
        }

        private void paintMatcher(Graphics2D g, Color fillClr, int leftX, int rightX, int upL, int upR, int doR, int doL) {
            int bottomY;
            int topY = Math.min(upL, upR);
            if (!g.hitClip(leftX, topY, rightX - leftX, (bottomY = Math.max(doL, doR)) - topY)) {
                return;
            }
            CubicCurve2D.Float upper = new CubicCurve2D.Float(leftX, upL, (float)(rightX - leftX) * 0.3f, upL, (float)(rightX - leftX) * 0.7f, upR, rightX, upR);
            CubicCurve2D.Float bottom = new CubicCurve2D.Float(rightX, doR, (float)(rightX - leftX) * 0.7f, doR, (float)(rightX - leftX) * 0.3f, doL, leftX, doL);
            GeneralPath path = new GeneralPath();
            path.append(upper, false);
            path.append(bottom, true);
            path.closePath();
            g.setColor(fillClr);
            g.fill(path);
            g.setColor(DiffSplitPaneDivider.this.master.getColorLines());
            g.draw(upper);
            g.draw(bottom);
        }
    }

}

