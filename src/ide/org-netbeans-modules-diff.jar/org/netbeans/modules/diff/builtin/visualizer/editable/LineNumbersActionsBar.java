/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.diff.builtin.visualizer.editable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.diff.builtin.visualizer.editable.DecoratedEditorPane;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffContentPanel;
import org.netbeans.modules.diff.builtin.visualizer.editable.DiffViewManager;
import org.netbeans.modules.diff.builtin.visualizer.editable.EditableDiffView;
import org.netbeans.modules.diff.builtin.visualizer.editable.HotSpot;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;

class LineNumbersActionsBar
extends JPanel
implements Scrollable,
MouseMotionListener,
MouseListener,
PropertyChangeListener {
    private static final int ACTIONS_BAR_WIDTH = 16;
    private static final int LINES_BORDER_WIDTH = 4;
    private static final Point POINT_ZERO = new Point(0, 0);
    private final Image insertImage = org.openide.util.Utilities.loadImage((String)"org/netbeans/modules/diff/builtin/visualizer/editable/insert.png");
    private final Image removeImage = org.openide.util.Utilities.loadImage((String)"org/netbeans/modules/diff/builtin/visualizer/editable/remove.png");
    private final Image insertActiveImage = org.openide.util.Utilities.loadImage((String)"org/netbeans/modules/diff/builtin/visualizer/editable/insert_active.png");
    private final Image removeActiveImage = org.openide.util.Utilities.loadImage((String)"org/netbeans/modules/diff/builtin/visualizer/editable/remove_active.png");
    private final DiffContentPanel master;
    private final boolean actionsEnabled;
    private final int actionIconsHeight;
    private final int actionIconsWidth;
    private final String lineNumberPadding = "        ";
    private int linesWidth;
    private int actionsWidth;
    private Color linesColor;
    private int linesCount;
    private int maxNumberCount;
    private Point lastMousePosition = POINT_ZERO;
    private HotSpot lastHotSpot = null;
    private List<HotSpot> hotspots = new ArrayList<HotSpot>(0);
    private int oldLinesWidth;

    public LineNumbersActionsBar(DiffContentPanel master, boolean actionsEnabled) {
        this.master = master;
        this.actionsEnabled = actionsEnabled;
        this.actionsWidth = actionsEnabled ? 16 : 0;
        this.actionIconsHeight = this.insertImage.getHeight(this);
        this.actionIconsWidth = this.insertImage.getWidth(this);
        this.setOpaque(true);
        this.setToolTipText("");
        master.getMaster().addPropertyChangeListener(this);
        this.addMouseMotionListener(this);
        this.addMouseListener(this);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.initUI();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.repaint();
    }

    private Font getLinesFont() {
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)this.master.getEditorPane());
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        Coloring col = Coloring.fromAttributeSet((AttributeSet)fcs.getFontColors("line-number"));
        Font font = col.getFont();
        if (font == null) {
            font = Coloring.fromAttributeSet((AttributeSet)fcs.getFontColors("default")).getFont();
        }
        return font;
    }

    private void initUI() {
        Color bg;
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)this.master.getEditorPane());
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        AttributeSet attrs = fcs.getFontColors("line-number");
        AttributeSet defAttrs = fcs.getFontColors("default");
        this.linesColor = (Color)attrs.getAttribute(StyleConstants.Foreground);
        if (this.linesColor == null) {
            this.linesColor = (Color)defAttrs.getAttribute(StyleConstants.Foreground);
        }
        if ((bg = (Color)attrs.getAttribute(StyleConstants.Background)) == null) {
            bg = (Color)defAttrs.getAttribute(StyleConstants.Background);
        }
        this.setBackground(bg);
        this.updateStateOnDocumentChange();
    }

    private HotSpot getHotspotAt(Point p) {
        for (HotSpot hotspot : this.hotspots) {
            if (!hotspot.getRect().contains(p)) continue;
            return hotspot;
        }
        return null;
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        Point p = event.getPoint();
        HotSpot spot = this.getHotspotAt(p);
        if (spot == null) {
            return null;
        }
        Difference diff = spot.getDiff();
        if (diff.getType() == 1) {
            return NbBundle.getMessage(LineNumbersActionsBar.class, (String)"TT_DiffPanel_Remove");
        }
        if (diff.getType() == 2) {
            return NbBundle.getMessage(LineNumbersActionsBar.class, (String)"TT_DiffPanel_Replace");
        }
        return NbBundle.getMessage(LineNumbersActionsBar.class, (String)"TT_DiffPanel_Insert");
    }

    private void performAction(HotSpot spot) {
        this.master.getMaster().rollback(spot.getDiff());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        HotSpot spot;
        if (!e.isPopupTrigger() && (spot = this.getHotspotAt(e.getPoint())) != null) {
            this.performAction(spot);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.lastMousePosition = POINT_ZERO;
        if (this.lastHotSpot != null) {
            this.repaint(this.lastHotSpot.getRect());
        }
        this.lastHotSpot = null;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point p;
        this.lastMousePosition = p = e.getPoint();
        HotSpot spot = this.getHotspotAt(p);
        if (this.lastHotSpot != spot) {
            this.repaint(this.lastHotSpot == null ? spot.getRect() : this.lastHotSpot.getRect());
        }
        this.lastHotSpot = spot;
        this.setCursor(spot != null ? Cursor.getPredefinedCursor(12) : Cursor.getDefaultCursor());
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    void onUISettingsChanged() {
        this.initUI();
        this.updateStateOnDocumentChange();
        this.repaint();
    }

    private void updateStateOnDocumentChange() {
        assert (SwingUtilities.isEventDispatchThread());
        StyledDocument doc = (StyledDocument)this.master.getEditorPane().getDocument();
        int lastOffset = doc.getEndPosition().getOffset();
        this.linesCount = NbDocument.findLineNumber((StyledDocument)doc, (int)lastOffset);
        Graphics g = this.getGraphics();
        if (g != null) {
            this.checkLinesWidth(g);
        }
        this.maxNumberCount = this.getNumberCount(this.linesCount);
        this.revalidate();
    }

    private boolean checkLinesWidth(Graphics g) {
        FontMetrics fm = g.getFontMetrics(this.getLinesFont());
        Rectangle2D rect = fm.getStringBounds(Integer.toString(this.linesCount), g);
        this.linesWidth = (int)rect.getWidth() + 8;
        if (this.linesWidth != this.oldLinesWidth) {
            this.oldLinesWidth = this.linesWidth;
            this.revalidate();
            this.repaint();
            return true;
        }
        return false;
    }

    private int getNumberCount(int n) {
        int nc = 0;
        while (n > 0) {
            n /= 10;
            ++nc;
        }
        return nc;
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        Dimension dim = this.master.getEditorPane().getPreferredScrollableViewportSize();
        return new Dimension(this.getBarWidth(), dim.height);
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return this.master.getEditorPane().getScrollableUnitIncrement(visibleRect, orientation, direction);
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return this.master.getEditorPane().getScrollableBlockIncrement(visibleRect, orientation, direction);
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.getBarWidth(), 536870911);
    }

    private int getBarWidth() {
        return this.actionsWidth + this.linesWidth;
    }

    public void onDiffSetChanged() {
        this.updateStateOnDocumentChange();
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics gr) {
        final Graphics2D g = (Graphics2D)gr;
        final Rectangle clip = g.getClipBounds();
        Stroke cs = g.getStroke();
        if (this.checkLinesWidth(gr)) {
            return;
        }
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)this.master.getEditorPane());
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        Map renderingHints = (Map)fcs.getFontColors("default").getAttribute(EditorStyleConstants.RenderingHints);
        if (!renderingHints.isEmpty()) {
            g.addRenderingHints(renderingHints);
        }
        EditorUI editorUI = Utilities.getEditorUI((JTextComponent)this.master.getEditorPane());
        final int lineHeight = editorUI.getLineHeight();
        g.setColor(this.getBackground());
        g.fillRect(clip.x, clip.y, clip.width, clip.height);
        g.setColor(Color.LIGHT_GRAY);
        int x = this.master.isFirst() ? 0 : this.getBarWidth() - 1;
        g.drawLine(x, clip.y, x, clip.y + clip.height - 1);
        DiffViewManager.DecoratedDifference[] diffs = this.master.getMaster().getManager().getDecorations();
        int actionsYOffset = (lineHeight - this.actionIconsHeight) / 2;
        int offset = this.linesWidth;
        int currentDifference = this.master.getMaster().getCurrentDifference();
        ArrayList<HotSpot> newActionIcons = new ArrayList<HotSpot>();
        int idx = 0;
        for (DiffViewManager.DecoratedDifference dd : diffs) {
            int bottom = this.master.isFirst() ? dd.getBottomLeft() : dd.getBottomRight();
            int top = this.master.isFirst() ? dd.getTopLeft() : dd.getTopRight();
            g.setColor(this.master.getMaster().getColorLines());
            g.setStroke(currentDifference == idx ? this.master.getMaster().getBoldStroke() : cs);
            g.drawLine(0, top, clip.width, top);
            if (bottom != -1) {
                g.drawLine(0, bottom, clip.width, bottom);
            }
            if (this.actionsEnabled && dd.canRollback() && (this.master.isFirst() && dd.getDiff().getType() != 1 || !this.master.isFirst() && dd.getDiff().getType() == 1)) {
                Image activeImage = this.master.isFirst() ? this.insertActiveImage : this.removeActiveImage;
                Image image = this.master.isFirst() ? this.insertImage : this.removeImage;
                Rectangle hotSpot = new Rectangle((this.master.isFirst() ? 0 : offset) + 1, top + actionsYOffset, this.actionIconsWidth, this.actionIconsHeight);
                if (hotSpot.contains(this.lastMousePosition) || idx == currentDifference) {
                    g.drawImage(activeImage, hotSpot.x, hotSpot.y, this);
                } else {
                    g.drawImage(image, hotSpot.x, hotSpot.y, this);
                }
                newActionIcons.add(new HotSpot(hotSpot, dd.getDiff()));
            }
            ++idx;
        }
        this.hotspots = newActionIcons;
        final int linesXOffset = (this.master.isFirst() ? this.actionsWidth : 0) + 4;
        g.setFont(this.getLinesFont());
        g.setColor(this.linesColor);
        Utilities.runViewHierarchyTransaction((JTextComponent)this.master.getEditorPane(), (boolean)true, (Runnable)new Runnable(){

            @Override
            public void run() {
                try {
                    View view;
                    int localLineHeight = lineHeight;
                    View rootView = Utilities.getDocumentView((JTextComponent)LineNumbersActionsBar.this.master.getEditorPane());
                    if (rootView == null) {
                        return;
                    }
                    int lineNumber = Utilities.getLineOffset((BaseDocument)((BaseDocument)LineNumbersActionsBar.this.master.getEditorPane().getDocument()), (int)LineNumbersActionsBar.this.master.getEditorPane().viewToModel(new Point(clip.x, clip.y)));
                    if (lineNumber > 0) {
                        --lineNumber;
                    }
                    if ((view = rootView.getView(lineNumber)) == null) {
                        return;
                    }
                    Rectangle rec = LineNumbersActionsBar.this.master.getEditorPane().modelToView(view.getStartOffset());
                    if (rec == null) {
                        return;
                    }
                    int linesDrawn = clip.height / localLineHeight + 4;
                    int docLines = Utilities.getRowCount((BaseDocument)((BaseDocument)LineNumbersActionsBar.this.master.getEditorPane().getDocument()));
                    if (lineNumber + linesDrawn > docLines) {
                        linesDrawn = docLines - lineNumber;
                    }
                    for (int i = 0; i < linesDrawn && (view = rootView.getView(lineNumber)) != null; ++i) {
                        Rectangle rec1 = LineNumbersActionsBar.this.master.getEditorPane().modelToView(view.getStartOffset());
                        Rectangle rec2 = LineNumbersActionsBar.this.master.getEditorPane().modelToView(view.getEndOffset() - 1);
                        if (rec1 != null && rec2 != null) {
                            int yOffset = rec1.y + rec1.height - rec1.height / 4;
                            localLineHeight = (int)(rec2.getY() + rec2.getHeight() - rec1.getY());
                            g.drawString(LineNumbersActionsBar.this.formatLineNumber(++lineNumber), linesXOffset, yOffset);
                            continue;
                        }
                        break;
                    }
                }
                catch (BadLocationException ex) {
                    // empty catch block
                }
            }
        });
    }

    private String formatLineNumber(int lineNumber) {
        String strNumber = Integer.toString(lineNumber);
        int nc = this.getNumberCount(lineNumber);
        if (nc < this.maxNumberCount) {
            StringBuilder sb = new StringBuilder(10);
            sb.append("        ", 0, this.maxNumberCount - nc);
            sb.append(strNumber);
            return sb.toString();
        }
        return strNumber;
    }

}

