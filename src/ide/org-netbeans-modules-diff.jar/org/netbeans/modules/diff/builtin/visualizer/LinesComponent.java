/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.FontMetricsCache
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.FontMetricsCache;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class LinesComponent
extends JComponent
implements Accessible,
PropertyChangeListener {
    private JEditorPane editorPane;
    private EditorUI editorUI;
    private Color backgroundColor;
    private Color foreColor;
    private Font font;
    private boolean init = false;
    private int numberWidth;
    private boolean showLineNumbers = true;
    private static final int ENLARGE_GUTTER_HEIGHT = 300;
    private int highestLineNumber = 0;
    private Insets lineNumberMargin;
    private int lineNumberDigitWidth;
    private LinkedList<String> linesList;
    private int activeLine = -1;
    private static final long serialVersionUID = -4861542695772182147L;

    public LinesComponent(JEditorPane pane) {
        this.editorPane = pane;
        this.font = this.editorPane.getFont();
        this.foreColor = this.editorPane.getForeground();
        this.backgroundColor = this.editorPane.getBackground();
        this.setLineNumberDigitWidth(10);
        this.setLineNumberMargin(new Insets(2, 2, 2, 4));
        this.editorUI = Utilities.getEditorUI((JTextComponent)this.editorPane);
        this.editorUI.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.editorUI));
        this.init();
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PANEL;
                }
            };
        }
        return this.accessibleContext;
    }

    protected void init() {
        this.createLines();
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(LinesComponent.class, (String)"ACSN_Lines_Component"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(LinesComponent.class, (String)"ACSD_Lines_Component"));
    }

    private void createLines() {
        this.linesList = new LinkedList();
        StyledDocument doc = (StyledDocument)this.editorPane.getDocument();
        int lastOffset = doc.getEndPosition().getOffset();
        int lineCnt = NbDocument.findLineNumber((StyledDocument)doc, (int)lastOffset);
        for (int i = 0; i < lineCnt; ++i) {
            this.linesList.add(Integer.toString(i + 1));
        }
    }

    public void addEmptyLines(int line, int count) {
        boolean appending = line > this.linesList.size();
        for (int i = 0; i < count; ++i) {
            if (appending) {
                this.linesList.add("");
                continue;
            }
            this.linesList.add(line, "");
        }
    }

    public void insertNumbers(int line, int startNum, int count) {
        boolean appending;
        boolean bl = appending = line >= this.linesList.size();
        if (appending) {
            int i = 0;
            while (i < count) {
                this.linesList.add(Integer.toString(startNum));
                ++i;
                ++startNum;
            }
        } else {
            int toAdd = Math.max(line + count - this.linesList.size(), 0);
            int i = 0;
            while (i < (count -= toAdd)) {
                this.linesList.set(line, Integer.toString(startNum));
                ++i;
                ++startNum;
                ++line;
            }
            i = 0;
            while (i < toAdd) {
                this.linesList.add(Integer.toString(startNum));
                ++i;
                ++startNum;
            }
        }
    }

    public void removeNumbers(int line, int count) {
        boolean appending;
        boolean bl = appending = line >= this.linesList.size();
        if (appending) {
            for (int i = 0; i < count; ++i) {
                this.linesList.add("");
            }
        } else {
            int toAdd = Math.max(line + count - this.linesList.size(), 0);
            int i = 0;
            while (i < (count -= toAdd)) {
                this.linesList.set(line, "");
                ++i;
                ++line;
            }
            for (i = 0; i < toAdd; ++i) {
                this.linesList.add("");
            }
        }
    }

    public void shrink(int numLines) {
        while (this.linesList.size() > numLines) {
            this.linesList.remove(numLines);
        }
    }

    private void updateState(Graphics g) {
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)this.editorPane);
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        Coloring col = Coloring.fromAttributeSet((AttributeSet)AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{fcs.getFontColors("line-number"), fcs.getFontColors("default")}));
        this.foreColor = col.getForeColor();
        this.backgroundColor = col.getBackColor();
        this.font = col.getFont();
        FontMetrics fm = g.getFontMetrics(this.font);
        this.showLineNumbers = true;
        this.init = true;
        if (this.highestLineNumber <= this.getLineCount()) {
            this.highestLineNumber = this.getLineCount();
        }
        int maxWidth = 1;
        char[] digit = new char[1];
        for (int i = 0; i <= 9; ++i) {
            digit[0] = (char)(48 + i);
            maxWidth = Math.max(maxWidth, fm.charsWidth(digit, 0, 1));
        }
        this.setLineNumberDigitWidth(maxWidth);
        this.resize();
    }

    protected void resize() {
        Dimension dim = new Dimension();
        dim.width = this.getWidthDimension();
        dim.height = this.getHeightDimension();
        dim.height += 300 * this.editorUI.getLineHeight();
        this.numberWidth = this.getLineNumberWidth();
        this.setPreferredSize(dim);
        this.revalidate();
    }

    protected int getLineCount() {
        return this.linesList.size();
    }

    protected int getDigitCount(int number) {
        return Integer.toString(number).length();
    }

    protected int getLineNumberWidth() {
        int newWidth = 0;
        Insets insets = this.getLineNumberMargin();
        if (insets != null) {
            newWidth += insets.left + insets.right;
        }
        return newWidth += (this.getDigitCount(this.highestLineNumber) + 1) * this.getLineNumberDigitWidth();
    }

    protected int getWidthDimension() {
        int newWidth = 0;
        if (this.showLineNumbers) {
            newWidth += this.getLineNumberWidth();
        }
        return newWidth;
    }

    protected int getHeightDimension() {
        View rootView = Utilities.getDocumentView((JTextComponent)this.editorPane);
        int height = this.highestLineNumber * this.editorUI.getLineHeight();
        if (rootView != null) {
            try {
                int lineCount = rootView.getViewCount();
                if (lineCount > 0) {
                    Rectangle rec = this.editorPane.modelToView(rootView.getView(lineCount - 1).getEndOffset() - 1);
                    height = rec.y + rec.height;
                }
            }
            catch (BadLocationException ex) {
                // empty catch block
            }
        }
        return height;
    }

    @Override
    public void paintComponent(Graphics g) {
        int pos;
        View rootView;
        int line;
        super.paintComponent(g);
        if (!this.init) {
            this.updateState(g);
        }
        Rectangle drawHere = g.getClipBounds();
        g.setColor(this.backgroundColor);
        g.fillRect(drawHere.x, drawHere.y, drawHere.width, drawHere.height);
        g.setFont(this.font);
        g.setColor(this.foreColor);
        FontMetrics fm = FontMetricsCache.getFontMetrics((Font)this.font, (Component)this);
        int rightMargin = 0;
        Insets margin = this.getLineNumberMargin();
        if (margin != null) {
            rightMargin = margin.right;
        }
        if ((line = (rootView = Utilities.getDocumentView((JTextComponent)this.editorPane)).getViewIndex(pos = this.editorPane.viewToModel(new Point(0, drawHere.y)), Position.Bias.Forward)) > 0) {
            --line;
        }
        try {
            Rectangle rec = this.editorPane.modelToView(rootView.getView(line).getStartOffset());
            if (rec == null) {
                return;
            }
            int y = rec.y;
            int lineHeight = this.editorUI.getLineHeight();
            int lineAscent = this.editorUI.getLineAscent();
            int lineCount = rootView.getViewCount();
            while (line < lineCount && y + lineHeight / 2 <= drawHere.y + drawHere.height) {
                View view = rootView.getView(line);
                Rectangle rec1 = this.editorPane.modelToView(view.getStartOffset());
                Rectangle rec2 = this.editorPane.modelToView(view.getEndOffset() - 1);
                if (rec1 != null && rec2 != null) {
                    y = (int)rec1.getY();
                    if (this.showLineNumbers) {
                        String lineStr = null;
                        if (line < this.linesList.size()) {
                            lineStr = this.linesList.get(line);
                        }
                        if (lineStr == null) {
                            lineStr = "";
                        }
                        String activeSymbol = "*";
                        int lineNumberWidth = fm.stringWidth(lineStr);
                        if (line == this.activeLine - 1) {
                            lineStr = lineStr + activeSymbol;
                        }
                        int activeSymbolWidth = fm.stringWidth(activeSymbol);
                        g.drawString(lineStr, this.numberWidth - (lineNumberWidth += activeSymbolWidth) - rightMargin, y + lineAscent);
                    }
                    y += (int)(rec2.getY() + rec2.getHeight() - rec1.getY());
                    ++line;
                    continue;
                }
                break;
            }
        }
        catch (BadLocationException ex) {
            return;
        }
    }

    public void changedAll() {
        if (!this.init) {
            return;
        }
        this.repaint();
        this.checkSize();
    }

    protected void checkSize() {
        int count = this.getLineCount();
        if (count > this.highestLineNumber) {
            this.highestLineNumber = count;
        }
        Dimension dim = this.getPreferredSize();
        if (this.getWidthDimension() > dim.width || this.getHeightDimension() > dim.height) {
            this.resize();
        }
    }

    public Insets getLineNumberMargin() {
        return this.lineNumberMargin;
    }

    public void setLineNumberMargin(Insets lineNumberMargin) {
        this.lineNumberMargin = lineNumberMargin;
    }

    public int getLineNumberDigitWidth() {
        return this.lineNumberDigitWidth;
    }

    public void setLineNumberDigitWidth(int lineNumberDigitWidth) {
        this.lineNumberDigitWidth = lineNumberDigitWidth;
    }

    public int getActiveLine() {
        return this.activeLine;
    }

    public void setActiveLine(int activeLine) {
        this.activeLine = activeLine;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.init = false;
        this.repaint();
    }

}

