/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.builtin.visualizer;

import java.awt.Color;
import java.awt.Component;
import java.io.Reader;
import java.io.Serializable;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.visualizer.DiffComponent;
import org.netbeans.spi.diff.DiffVisualizer;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;

public class GraphicalDiffVisualizer
extends DiffVisualizer
implements Serializable {
    private Color colorAdded = DiffComponent.COLOR_ADDED;
    private Color colorMissing = DiffComponent.COLOR_MISSING;
    private Color colorChanged = DiffComponent.COLOR_CHANGED;
    static final long serialVersionUID = -1135210647457196211L;

    public String getDisplayName() {
        return NbBundle.getMessage(GraphicalDiffVisualizer.class, (String)"GraphicalDiffVisualizer.displayName");
    }

    public String getShortDescription() {
        return NbBundle.getMessage(GraphicalDiffVisualizer.class, (String)"GraphicalDiffVisualizer.shortDescription");
    }

    @Override
    public Component createView(Difference[] diffs, String name1, String title1, Reader r1, String name2, String title2, Reader r2, String MIMEType) {
        if (diffs.length == 0) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(GraphicalDiffVisualizer.class, (String)"MSG_NoDifference", (Object)name1, (Object)name2)));
        }
        String componentName = name1;
        if (name2 != null && name2.length() > 0) {
            componentName = NbBundle.getMessage(GraphicalDiffVisualizer.class, (String)"MSG_TwoFilesDiffTitle", (Object)componentName, (Object)name2);
        }
        DiffComponent diff = new DiffComponent(diffs, componentName, MIMEType, name1, name2, title1, title2, r1, r2, new Color[]{this.colorMissing, this.colorAdded, this.colorChanged});
        return diff;
    }

    public Color getColorAdded() {
        return this.colorAdded;
    }

    public void setColorAdded(Color colorAdded) {
        this.colorAdded = colorAdded;
    }

    public Color getColorMissing() {
        return this.colorMissing;
    }

    public void setColorMissing(Color colorMissing) {
        this.colorMissing = colorMissing;
    }

    public Color getColorChanged() {
        return this.colorChanged;
    }

    public void setColorChanged(Color colorChanged) {
        this.colorChanged = colorChanged;
    }
}

