/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.InstanceDataObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.diff;

import java.awt.Color;
import java.io.IOException;
import java.util.Collection;
import java.util.prefs.Preferences;
import javax.swing.UIManager;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.diff.builtin.provider.BuiltInDiffProvider;
import org.netbeans.modules.diff.cmdline.CmdlineDiffProvider;
import org.netbeans.spi.diff.DiffProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.InstanceDataObject;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class DiffModuleConfig {
    public static final String PREF_EXTERNAL_DIFF_COMMAND = "externalDiffCommand";
    private static final String PREF_IGNORE_LEADINGTRAILING_WHITESPACE = "ignoreWhitespace";
    private static final String PREF_IGNORE_INNER_WHITESPACE = "ignoreInnerWhitespace";
    private static final String PREF_IGNORE_CASE = "ignoreCase";
    private static final String PREF_USE_INTERNAL_DIFF = "useInternalDiff";
    private static final String PREF_ADDED_COLOR = "addedColor";
    private static final String PREF_CHANGED_COLOR = "changedColor";
    private static final String PREF_DELETED_COLOR = "deletedColor";
    private static final String PREF_MERGE_UNRESOLVED_COLOR = "merge.unresolvedColor";
    private static final String PREF_MERGE_APPLIED_COLOR = "merge.appliedColor";
    private static final String PREF_MERGE_NOTAPPLIED_COLOR = "merge.notappliedColor";
    private static final String PREF_SIDEBAR_DELETED_COLOR = "sidebar.deletedColor";
    private static final String PREF_SIDEBAR_CHANGED_COLOR = "sidebar.changedColor";
    private static final DiffModuleConfig INSTANCE = new DiffModuleConfig();
    private final Color defaultAddedColor;
    private final Color defaultChangedColor;
    private final Color defaultDeletedColor;
    private final Color defaultAppliedColor;
    private final Color defaultNotAppliedColor;
    private final Color defaultUnresolvedColor;
    private final Color defaultSidebarDeletedColor;
    private final Color defaultSidebarChangedColor;

    public static DiffModuleConfig getDefault() {
        return INSTANCE;
    }

    private DiffModuleConfig() {
        Color c = UIManager.getColor("nb.diff.added.color");
        if (null == c) {
            c = new Color(180, 255, 180);
        }
        this.defaultAddedColor = c;
        c = UIManager.getColor("nb.diff.changed.color");
        if (null == c) {
            c = new Color(160, 200, 255);
        }
        this.defaultChangedColor = c;
        c = UIManager.getColor("nb.diff.deleted.color");
        if (null == c) {
            c = new Color(255, 160, 180);
        }
        this.defaultDeletedColor = c;
        c = UIManager.getColor("nb.diff.applied.color");
        if (null == c) {
            c = new Color(180, 255, 180);
        }
        this.defaultAppliedColor = c;
        c = UIManager.getColor("nb.diff.notapplied.color");
        if (null == c) {
            c = new Color(160, 200, 255);
        }
        this.defaultNotAppliedColor = c;
        c = UIManager.getColor("nb.diff.unresolved.color");
        if (null == c) {
            c = new Color(255, 160, 180);
        }
        this.defaultUnresolvedColor = c;
        c = UIManager.getColor("nb.diff.sidebar.deleted.color");
        if (null == c) {
            c = new Color(255, 225, 232);
        }
        this.defaultSidebarDeletedColor = c;
        c = UIManager.getColor("nb.diff.sidebar.changed.color");
        if (null == c) {
            c = new Color(233, 241, 255);
        }
        this.defaultSidebarChangedColor = c;
    }

    public Color getAddedColor() {
        return this.getColor("addedColor", this.defaultAddedColor);
    }

    public Color getDefaultAddedColor() {
        return this.defaultAddedColor;
    }

    public Color getChangedColor() {
        return this.getColor("changedColor", this.defaultChangedColor);
    }

    public Color getDefaultChangedColor() {
        return this.defaultChangedColor;
    }

    public Color getDeletedColor() {
        return this.getColor("deletedColor", this.defaultDeletedColor);
    }

    public Color getDefaultDeletedColor() {
        return this.defaultDeletedColor;
    }

    public Color getAppliedColor() {
        return this.getColor("merge.appliedColor", this.defaultAppliedColor);
    }

    public Color getDefaultAppliedColor() {
        return this.defaultAppliedColor;
    }

    public Color getNotAppliedColor() {
        return this.getColor("merge.notappliedColor", this.defaultNotAppliedColor);
    }

    public Color getDefaultNotAppliedColor() {
        return this.defaultNotAppliedColor;
    }

    public Color getUnresolvedColor() {
        return this.getColor("merge.unresolvedColor", this.defaultUnresolvedColor);
    }

    public Color getDefaultUnresolvedColor() {
        return this.defaultUnresolvedColor;
    }

    public Color getSidebarDeletedColor() {
        return this.getColor("sidebar.deletedColor", this.defaultSidebarDeletedColor);
    }

    public Color getDefaultSidebarDeletedColor() {
        return this.defaultSidebarDeletedColor;
    }

    public Color getSidebarChangedColor() {
        return this.getColor("sidebar.changedColor", this.defaultSidebarChangedColor);
    }

    public Color getDefaultSidebarChangedColor() {
        return this.defaultSidebarChangedColor;
    }

    public void setChangedColor(Color color) {
        this.putColor("changedColor", this.defaultChangedColor.equals(color) ? null : color);
    }

    public void setAddedColor(Color color) {
        this.putColor("addedColor", this.defaultAddedColor.equals(color) ? null : color);
    }

    public void setDeletedColor(Color color) {
        this.putColor("deletedColor", this.defaultDeletedColor.equals(color) ? null : color);
    }

    public void setNotAppliedColor(Color color) {
        this.putColor("merge.notappliedColor", this.defaultNotAppliedColor.equals(color) ? null : color);
    }

    public void setAppliedColor(Color color) {
        this.putColor("merge.appliedColor", this.defaultAppliedColor.equals(color) ? null : color);
    }

    public void setUnresolvedColor(Color color) {
        this.putColor("merge.unresolvedColor", this.defaultUnresolvedColor.equals(color) ? null : color);
    }

    public void setSidebarDeletedColor(Color color) {
        this.putColor("sidebar.deletedColor", this.defaultSidebarDeletedColor.equals(color) ? null : color);
    }

    public void setSidebarChangedColor(Color color) {
        this.putColor("sidebar.changedColor", this.defaultSidebarChangedColor.equals(color) ? null : color);
    }

    private void putColor(String key, Color color) {
        if (color == null) {
            this.getPreferences().remove(key);
        } else {
            this.getPreferences().putInt(key, color.getRGB());
        }
    }

    private Color getColor(String key, Color defaultColor) {
        int rgb = this.getPreferences().getInt(key, defaultColor.getRGB());
        return new Color(rgb);
    }

    public DiffProvider getDefaultDiffProvider() {
        Collection providers = Lookup.getDefault().lookup(new Lookup.Template(DiffProvider.class)).allInstances();
        for (DiffProvider p : providers) {
            if (this.isUseInteralDiff()) {
                if (!(p instanceof BuiltInDiffProvider)) continue;
                ((BuiltInDiffProvider)p).setOptions(this.getOptions());
                return p;
            }
            if (!(p instanceof CmdlineDiffProvider)) continue;
            ((CmdlineDiffProvider)p).setDiffCommand(this.getDiffCommand());
            return p;
        }
        return null;
    }

    private void setDefaultProvider(DiffProvider ds) {
        FileObject services = FileUtil.getConfigFile((String)"Services/DiffProviders");
        DataFolder df = DataFolder.findFolder((FileObject)services);
        DataObject[] children = df.getChildren();
        for (int i = 0; i < children.length; ++i) {
            InstanceDataObject ido;
            if (!(children[i] instanceof InstanceDataObject) || !(ido = (InstanceDataObject)children[i]).instanceOf(ds.getClass())) continue;
            try {
                if (!ds.equals(ido.instanceCreate())) continue;
                df.setOrder(new DataObject[]{ido});
                break;
            }
            catch (IOException ioex) {
                continue;
            }
            catch (ClassNotFoundException cnfex) {
                // empty catch block
            }
        }
    }

    private String getDiffCommand() {
        return this.getPreferences().get("externalDiffCommand", "diff {0} {1}");
    }

    public void setOptions(BuiltInDiffProvider.Options options) {
        this.getPreferences().putBoolean("ignoreWhitespace", options.ignoreLeadingAndtrailingWhitespace);
        this.getPreferences().putBoolean("ignoreInnerWhitespace", options.ignoreInnerWhitespace);
        this.getPreferences().putBoolean("ignoreCase", options.ignoreCase);
        this.getBuiltinProvider().setOptions(options);
    }

    public BuiltInDiffProvider.Options getOptions() {
        BuiltInDiffProvider.Options options = new BuiltInDiffProvider.Options();
        options.ignoreLeadingAndtrailingWhitespace = this.getPreferences().getBoolean("ignoreWhitespace", true);
        options.ignoreInnerWhitespace = this.getPreferences().getBoolean("ignoreInnerWhitespace", false);
        options.ignoreCase = this.getPreferences().getBoolean("ignoreCase", false);
        return options;
    }

    private BuiltInDiffProvider getBuiltinProvider() {
        Collection diffs = Lookup.getDefault().lookupAll(DiffProvider.class);
        for (DiffProvider diff : diffs) {
            if (!(diff instanceof BuiltInDiffProvider)) continue;
            return (BuiltInDiffProvider)diff;
        }
        throw new IllegalStateException("No builtin diff provider");
    }

    public void setUseInteralDiff(boolean useInternal) {
        this.getPreferences().putBoolean("useInternalDiff", useInternal);
        Collection diffs = Lookup.getDefault().lookupAll(DiffProvider.class);
        if (useInternal) {
            this.setDefaultProvider(this.getBuiltinProvider());
        } else {
            for (DiffProvider diff : diffs) {
                if (!(diff instanceof CmdlineDiffProvider)) continue;
                this.setDefaultProvider(diff);
                break;
            }
        }
    }

    public boolean isUseInteralDiff() {
        return this.getPreferences().getBoolean("useInternalDiff", true);
    }

    public Preferences getPreferences() {
        return NbPreferences.forModule(DiffModuleConfig.class);
    }

    public int getSpacesPerTabFor(String mimeType) {
        int spacesPerTab = 1;
        Preferences pref = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        if (pref != null) {
            spacesPerTab = pref.getInt("tab-size", 1);
        }
        return spacesPerTab;
    }
}

