/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.cookies.EditCookie
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.diff;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Method;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.diff.XMLEncodingHelper;
import org.openide.ErrorManager;
import org.openide.cookies.EditCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Lookup;

public class EncodedReaderFactory {
    private static final String CHAR_SET_ATTRIBUTE = "Content-Encoding";
    private static EncodedReaderFactory factory;

    private EncodedReaderFactory() {
    }

    public static synchronized EncodedReaderFactory getDefault() {
        if (factory == null) {
            factory = new EncodedReaderFactory();
        }
        return factory;
    }

    public Reader getReader(File file, String mimeType) throws FileNotFoundException {
        return this.getReader(file, mimeType, this.getEncoding(file));
    }

    public Reader getReader(File file, String mimeType, String encoding) throws FileNotFoundException {
        String ext;
        if (encoding != null) {
            try {
                return new InputStreamReader((InputStream)new FileInputStream(file), encoding);
            }
            catch (UnsupportedEncodingException ueex) {
                ErrorManager.getDefault().notify(1, (Throwable)ueex);
            }
        }
        Reader r = null;
        String name = file.getName();
        int endingIndex = name.lastIndexOf(46);
        String string = ext = endingIndex >= 0 && endingIndex < name.length() - 1 ? name.substring(endingIndex + 1) : "";
        if (!"java".equalsIgnoreCase(ext)) {
            try {
                file = FileUtil.normalizeFile((File)file);
                FileObject fo = FileUtil.toFileObject((File)file);
                if (fo != null) {
                    r = this.getReaderFromEditorSupport(fo, fo);
                }
            }
            catch (IllegalArgumentException iaex) {
                ErrorManager.getDefault().notify(1, (Throwable)iaex);
            }
            if (r == null) {
                r = this.getReaderFromKit(file, null, mimeType);
            }
        }
        if (r == null) {
            r = new InputStreamReader(new FileInputStream(file));
        }
        return r;
    }

    public Reader getReader(FileObject fo, String encoding) throws FileNotFoundException {
        return this.getReader(fo, encoding, fo.getExt());
    }

    public Reader getReader(FileObject fo, String encoding, String secondFileExt) throws FileNotFoundException {
        return this.getReader(fo, encoding, fo, secondFileExt);
    }

    public Reader getReader(FileObject fo, String encoding, FileObject type) throws FileNotFoundException {
        return this.getReader(fo, encoding, type, type.getExt());
    }

    private Reader getReader(FileObject fo, String encoding, FileObject type, String secondFileExt) throws FileNotFoundException {
        if (encoding != null) {
            try {
                return new InputStreamReader(fo.getInputStream(), encoding);
            }
            catch (UnsupportedEncodingException ueex) {
                ErrorManager.getDefault().notify(1, (Throwable)ueex);
            }
        }
        Reader r = null;
        String ext = type.getExt();
        if (!("java".equalsIgnoreCase(ext) && ext.equals(secondFileExt) || (r = this.getReaderFromEditorSupport(fo, type)) != null)) {
            r = this.getReaderFromKit(null, fo, type.getMIMEType());
        }
        if (r == null) {
            r = new InputStreamReader(fo.getInputStream());
        }
        return r;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Reader getReaderFromEditorSupport(FileObject fo, FileObject type) throws FileNotFoundException {
        DataObject dobj;
        try {
            dobj = DataObject.find((FileObject)type);
        }
        catch (DataObjectNotFoundException donfex) {
            return null;
        }
        if (!type.equals((Object)dobj.getPrimaryFile())) {
            return null;
        }
        EditCookie edit = (EditCookie)dobj.getCookie(EditCookie.class);
        CloneableEditorSupport editorSupport = null;
        if (edit instanceof CloneableEditorSupport) {
            editorSupport = (CloneableEditorSupport)edit;
        }
        if (editorSupport == null) {
            return null;
        }
        try {
            Method createKitMethod = EncodedReaderFactory.getDeclaredMethod(editorSupport.getClass(), "createEditorKit", new Class[0]);
            createKitMethod.setAccessible(true);
            EditorKit kit = (EditorKit)createKitMethod.invoke((Object)editorSupport, new Object[0]);
            Method createStyledDocumentMethod = EncodedReaderFactory.getDeclaredMethod(editorSupport.getClass(), "createStyledDocument", new Class[]{EditorKit.class});
            createStyledDocumentMethod.setAccessible(true);
            StyledDocument doc = (StyledDocument)createStyledDocumentMethod.invoke((Object)editorSupport, kit);
            Method loadFromStreamToKitMethod = EncodedReaderFactory.getDeclaredMethod(editorSupport.getClass(), "loadFromStreamToKit", new Class[]{StyledDocument.class, InputStream.class, EditorKit.class});
            loadFromStreamToKitMethod.setAccessible(true);
            InputStream in = fo.getInputStream();
            try {
                loadFromStreamToKitMethod.invoke((Object)editorSupport, doc, in, kit);
            }
            finally {
                try {
                    in.close();
                }
                catch (IOException ioex) {}
            }
            String text = doc.getText(0, doc.getLength());
            doc = null;
            return new StringReader(text);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Writer getWriterFromEditorSupport(FileObject fo, FileLock lock) throws FileNotFoundException {
        DataObject dobj;
        try {
            dobj = DataObject.find((FileObject)fo);
        }
        catch (DataObjectNotFoundException donfex) {
            return null;
        }
        if (!fo.equals((Object)dobj.getPrimaryFile())) {
            return null;
        }
        EditCookie edit = (EditCookie)dobj.getCookie(EditCookie.class);
        CloneableEditorSupport editorSupport = edit instanceof CloneableEditorSupport ? (CloneableEditorSupport)edit : null;
        if (editorSupport == null) {
            return null;
        }
        try {
            Method createKitMethod = EncodedReaderFactory.getDeclaredMethod(editorSupport.getClass(), "createEditorKit", new Class[0]);
            createKitMethod.setAccessible(true);
            EditorKit kit = (EditorKit)createKitMethod.invoke((Object)editorSupport, new Object[0]);
            Method createStyledDocumentMethod = EncodedReaderFactory.getDeclaredMethod(editorSupport.getClass(), "createStyledDocument", new Class[]{EditorKit.class});
            createStyledDocumentMethod.setAccessible(true);
            StyledDocument doc = (StyledDocument)createStyledDocumentMethod.invoke((Object)editorSupport, kit);
            Method saveFromKitToStreamMethod = EncodedReaderFactory.getDeclaredMethod(editorSupport.getClass(), "saveFromKitToStream", new Class[]{StyledDocument.class, EditorKit.class, OutputStream.class});
            saveFromKitToStreamMethod.setAccessible(true);
            return new DocWriter(doc, fo, lock, null, kit, editorSupport, saveFromKitToStreamMethod);
        }
        catch (Exception ex) {
            ErrorManager.getDefault().notify(1, (Throwable)ex);
            return null;
        }
    }

    private static Method getDeclaredMethod(Class<?> objClass, String name, Class[] args) throws NoSuchMethodException, SecurityException {
        try {
            return objClass.getDeclaredMethod(name, args);
        }
        catch (NoSuchMethodException nsmex) {
            Class superClass = objClass.getSuperclass();
            if (superClass != null) {
                return EncodedReaderFactory.getDeclaredMethod(superClass, name, args);
            }
            throw nsmex;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Reader getReaderFromKit(File file, FileObject fo, String mimeType) throws FileNotFoundException {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mimeType);
        if (kit.getContentType().equalsIgnoreCase("text/plain") && "text/x-dtd".equalsIgnoreCase(mimeType)) {
            kit = CloneableEditorSupport.getEditorKit((String)"text/xml");
        }
        if (kit != null) {
            Document doc = kit.createDefaultDocument();
            InputStream stream = null;
            try {
                stream = file != null ? new FileInputStream(file) : fo.getInputStream();
                kit.read(stream, doc, 0);
                String text = doc.getText(0, doc.getLength());
                doc = null;
                StringReader stringReader = new StringReader(text);
                return stringReader;
            }
            catch (IOException ioex) {
                FileNotFoundException fnfex = file != null ? new FileNotFoundException("Can not read file " + file.getAbsolutePath()) : new FileNotFoundException("Can not read file " + (Object)fo);
                fnfex.initCause(ioex);
                throw fnfex;
            }
            catch (BadLocationException blex) {
                ErrorManager.getDefault().notify((Throwable)blex);
            }
            finally {
                if (stream != null) {
                    try {
                        stream.close();
                    }
                    catch (IOException e) {}
                }
            }
        }
        return null;
    }

    private Writer getWriterFromKit(File file, FileObject fo, FileLock lock, String mimeType) throws FileNotFoundException {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mimeType);
        if (kit.getContentType().equalsIgnoreCase("text/plain") && "text/x-dtd".equalsIgnoreCase(mimeType)) {
            kit = CloneableEditorSupport.getEditorKit((String)"text/xml");
        }
        if (kit != null) {
            Document doc = kit.createDefaultDocument();
            return new DocWriter(doc, fo, lock, file, kit, null, null);
        }
        return null;
    }

    public Writer getWriter(File file, String mimeType) throws FileNotFoundException {
        return this.getWriter(file, mimeType, this.getEncoding(file));
    }

    public Writer getWriter(File file, String mimeType, String encoding) throws FileNotFoundException {
        String ext;
        if (encoding != null) {
            try {
                return new OutputStreamWriter((OutputStream)new FileOutputStream(file), encoding);
            }
            catch (UnsupportedEncodingException ueex) {
                ErrorManager.getDefault().notify(1, (Throwable)ueex);
            }
        }
        Writer w = null;
        String name = file.getName();
        int endingIndex = name.lastIndexOf(46);
        String string = ext = endingIndex >= 0 && endingIndex < name.length() - 1 ? name.substring(endingIndex + 1) : "";
        if (!"java".equalsIgnoreCase(ext)) {
            block10 : {
                try {
                    FileLock lock;
                    file = FileUtil.normalizeFile((File)file);
                    FileObject fo = FileUtil.toFileObject((File)file);
                    if (fo == null) break block10;
                    try {
                        lock = fo.lock();
                    }
                    catch (IOException ioex) {
                        FileNotFoundException fnfex = new FileNotFoundException(ioex.getLocalizedMessage());
                        fnfex.initCause(ioex);
                        throw fnfex;
                    }
                    w = this.getWriterFromEditorSupport(fo, lock);
                }
                catch (IllegalArgumentException iaex) {
                    ErrorManager.getDefault().notify(1, (Throwable)iaex);
                }
            }
            if (w == null) {
                w = this.getWriterFromKit(file, null, null, mimeType);
            }
        }
        if (w == null) {
            w = new OutputStreamWriter(new FileOutputStream(file));
        }
        return w;
    }

    public Writer getWriter(FileObject fo, FileLock lock, String encoding) throws IOException {
        if (lock == null) {
            lock = fo.lock();
        }
        if (encoding != null) {
            try {
                return new OutputStreamWriter(fo.getOutputStream(lock), encoding);
            }
            catch (UnsupportedEncodingException ueex) {
                ErrorManager.getDefault().notify(1, (Throwable)ueex);
            }
        }
        Writer w = null;
        String ext = fo.getExt();
        if (!"java".equalsIgnoreCase(ext) && (w = this.getWriterFromEditorSupport(fo, lock)) == null) {
            w = this.getWriterFromKit(null, fo, lock, fo.getMIMEType());
        }
        if (w == null) {
            w = new OutputStreamWriter(fo.getOutputStream(lock));
        }
        return w;
    }

    public String getEncoding(File file) {
        return this.getEncoding(FileUtil.toFileObject((File)FileUtil.normalizeFile((File)file)));
    }

    public static String decodeName(FileObject fo) {
        String name;
        int hashOffset;
        String ret = fo.getNameExt();
        if (fo.getParent() != null && fo.getParent().getPath().endsWith("CVS" + File.separator + "RevisionCache") && (hashOffset = (name = fo.getName()).lastIndexOf("#")) != 1) {
            ret = name.substring(0, hashOffset);
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getEncoding(FileObject fo) {
        String name = EncodedReaderFactory.decodeName(fo).toLowerCase();
        if (name.endsWith(".properties")) {
            return EncodedReaderFactory.findPropertiesEncoding();
        }
        if (name.endsWith(".form")) {
            return "utf8";
        }
        Object encoding = null;
        if (fo != null) {
            if (name.endsWith(".java")) {
                encoding = EncodedReaderFactory.findJavaEncoding(fo);
            }
            if (encoding == null) {
                encoding = fo.getAttribute("Content-Encoding");
            }
        }
        if (name.endsWith(".xml") || name.endsWith(".dtd") || name.endsWith(".xsd") || name.endsWith(".xsl")) {
            BufferedInputStream in = null;
            try {
                in = new BufferedInputStream(fo.getInputStream(), 2048);
                encoding = XMLEncodingHelper.detectEncoding(in);
            }
            catch (IOException e) {
                ErrorManager err = ErrorManager.getDefault();
                err.annotate((Throwable)e, "Can not detect encoding for: " + fo.getPath());
                err.notify(1, (Throwable)e);
            }
            finally {
                if (in != null) {
                    try {
                        in.close();
                    }
                    catch (IOException e) {}
                }
            }
        }
        if (encoding != null) {
            return encoding.toString();
        }
        return null;
    }

    private static String findJavaEncoding(FileObject fo) {
        ClassLoader systemClassLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        Method org_netbeans_modules_java_Util_getFileEncoding = null;
        try {
            Class c = systemClassLoader.loadClass("org.netbeans.modules.java.Util");
            org_netbeans_modules_java_Util_getFileEncoding = c.getMethod("getFileEncoding", FileObject.class);
        }
        catch (Exception e) {
            // empty catch block
        }
        if (org_netbeans_modules_java_Util_getFileEncoding != null) {
            try {
                String encoding = (String)org_netbeans_modules_java_Util_getFileEncoding.invoke(null, new Object[]{fo});
                return encoding;
            }
            catch (Exception e) {
                ErrorManager.getDefault().notify(1, (Throwable)e);
            }
        }
        return null;
    }

    private static String findPropertiesEncoding() {
        return "ISO-8859-1";
    }

    private static class DocWriter
    extends Writer {
        private Document doc;
        private FileObject fo;
        private FileLock foLock;
        private File file;
        private EditorKit kit;
        private CloneableEditorSupport editorSupport;
        private Method saveFromKitToStreamMethod;
        private boolean closed;

        public DocWriter(Document doc, FileObject fo, FileLock foLock, File file, EditorKit kit, CloneableEditorSupport editorSupport, Method saveFromKitToStreamMethod) {
            this.doc = doc;
            this.fo = fo;
            this.foLock = foLock;
            this.file = file;
            this.kit = kit;
            this.editorSupport = editorSupport;
            this.saveFromKitToStreamMethod = saveFromKitToStreamMethod;
        }

        @Override
        public void write(int c) throws IOException {
            try {
                this.doc.insertString(this.doc.getLength(), Character.toString((char)c), null);
            }
            catch (BadLocationException blex) {
                IOException ioex = new IOException(blex.getLocalizedMessage());
                ioex.initCause(blex);
                throw ioex;
            }
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            if (off < 0 || off > cbuf.length || len < 0 || off + len > cbuf.length || off + len < 0) {
                throw new IndexOutOfBoundsException();
            }
            if (len == 0) {
                return;
            }
            try {
                this.doc.insertString(this.doc.getLength(), new String(cbuf, off, len), null);
            }
            catch (BadLocationException blex) {
                IOException ioex = new IOException(blex.getLocalizedMessage());
                ioex.initCause(blex);
                throw ioex;
            }
        }

        @Override
        public void write(String str) throws IOException {
            try {
                this.doc.insertString(this.doc.getLength(), str, null);
            }
            catch (BadLocationException blex) {
                IOException ioex = new IOException(blex.getLocalizedMessage());
                ioex.initCause(blex);
                throw ioex;
            }
        }

        @Override
        public void flush() throws IOException {
        }

        @Override
        public void close() throws IOException {
            if (this.closed) {
                return;
            }
            if (this.saveFromKitToStreamMethod != null) {
                OutputStream out = this.fo.getOutputStream(this.foLock);
                try {
                    this.saveFromKitToStreamMethod.invoke((Object)this.editorSupport, this.doc, this.kit, out);
                }
                catch (Exception e) {
                    IOException ioex = new IOException(e.getLocalizedMessage());
                    ioex.initCause(e);
                    throw ioex;
                }
                finally {
                    try {
                        out.close();
                    }
                    catch (IOException ioex) {}
                    this.foLock.releaseLock();
                }
            }
            OutputStream out = this.file != null ? new FileOutputStream(this.file) : this.fo.getOutputStream(this.foLock);
            try {
                this.kit.write(out, this.doc, 0, this.doc.getLength());
            }
            catch (BadLocationException blex) {
                IOException ioex = new IOException(blex.getLocalizedMessage());
                ioex.initCause(blex);
                throw ioex;
            }
            finally {
                out.close();
            }
            this.closed = true;
        }
    }

}

