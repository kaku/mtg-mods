/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.diff.options;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.Utils;
import org.netbeans.modules.diff.builtin.provider.BuiltInDiffProvider;
import org.netbeans.modules.diff.options.AccessibleJFileChooser;
import org.netbeans.modules.diff.options.DiffOptionsController;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileUtil;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

class DiffOptionsPanel
extends JPanel
implements ActionListener,
DocumentListener {
    private boolean isChanged;
    private final RequestProcessor.Task t;
    private JButton browseCommand;
    private ButtonGroup buttonGroup1;
    private JTextField externalCommand;
    private JRadioButton externalDiff;
    private JCheckBox ignoreAllWhitespace;
    private JCheckBox ignoreCase;
    private JCheckBox ignoreWhitespace;
    private JRadioButton internalDiff;
    private JLabel jLabel1;
    private JLabel lblWarningCommand;
    private String cmd;

    public DiffOptionsPanel() {
        this.t = Utils.createParallelTask(new CheckCommand(true));
        this.initComponents();
        this.internalDiff.addActionListener(this);
        this.externalDiff.addActionListener(this);
        this.ignoreWhitespace.addActionListener(this);
        this.ignoreAllWhitespace.addActionListener(this);
        this.ignoreCase.addActionListener(this);
        this.externalCommand.getDocument().addDocumentListener(this);
        this.refreshComponents();
    }

    void refreshComponents() {
        this.ignoreWhitespace.setEnabled(this.internalDiff.isSelected());
        this.ignoreAllWhitespace.setEnabled(this.internalDiff.isSelected());
        this.ignoreCase.setEnabled(this.internalDiff.isSelected());
        this.jLabel1.setEnabled(this.externalDiff.isSelected());
        this.externalCommand.setEnabled(this.externalDiff.isSelected());
        this.browseCommand.setEnabled(this.externalDiff.isSelected());
        this.checkExternalCommand();
    }

    public JTextField getExternalCommand() {
        return this.externalCommand;
    }

    public JRadioButton getExternalDiff() {
        return this.externalDiff;
    }

    public JCheckBox getIgnoreWhitespace() {
        return this.ignoreWhitespace;
    }

    public JCheckBox getIgnoreInnerWhitespace() {
        return this.ignoreAllWhitespace;
    }

    public JCheckBox getIgnoreCase() {
        return this.ignoreCase;
    }

    public JRadioButton getInternalDiff() {
        return this.internalDiff;
    }

    public void setChanged(boolean changed) {
        this.isChanged = changed;
    }

    public boolean isChanged() {
        return this.isChanged;
    }

    private void fireChanged() {
        boolean useInteralDiff = DiffModuleConfig.getDefault().isUseInteralDiff();
        if (this.internalDiff.isSelected() != useInteralDiff) {
            this.isChanged = true;
            return;
        }
        if (this.externalDiff.isSelected() == useInteralDiff) {
            this.isChanged = true;
            return;
        }
        if (this.ignoreWhitespace.isSelected() != DiffModuleConfig.getDefault().getOptions().ignoreLeadingAndtrailingWhitespace) {
            this.isChanged = true;
            return;
        }
        if (this.ignoreAllWhitespace.isSelected() != DiffModuleConfig.getDefault().getOptions().ignoreInnerWhitespace) {
            this.isChanged = true;
            return;
        }
        if (this.ignoreCase.isSelected() != DiffModuleConfig.getDefault().getOptions().ignoreCase) {
            this.isChanged = true;
            return;
        }
        if (!this.externalCommand.getText().equals(DiffModuleConfig.getDefault().getPreferences().get("externalDiffCommand", "diff {0} {1}"))) {
            this.checkExternalCommand();
            this.isChanged = true;
            return;
        }
        this.isChanged = false;
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.internalDiff = new JRadioButton();
        this.ignoreWhitespace = new JCheckBox();
        this.ignoreAllWhitespace = new JCheckBox();
        this.ignoreCase = new JCheckBox();
        this.externalDiff = new JRadioButton();
        this.jLabel1 = new JLabel();
        this.externalCommand = new JTextField();
        this.browseCommand = new JButton();
        this.lblWarningCommand = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 5));
        this.buttonGroup1.add(this.internalDiff);
        Mnemonics.setLocalizedText((AbstractButton)this.internalDiff, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"jRadioButton1.text"));
        this.internalDiff.setMargin(new Insets(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.ignoreWhitespace, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"jCheckBox1.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.ignoreAllWhitespace, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"DiffOptionsPanel.ignoreAllWhitespace.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.ignoreCase, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"DiffOptionsPanel.ignoreCase.text"));
        this.buttonGroup1.add(this.externalDiff);
        Mnemonics.setLocalizedText((AbstractButton)this.externalDiff, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"jRadioButton2.text"));
        this.externalDiff.setMargin(new Insets(0, 0, 0, 0));
        this.jLabel1.setLabelFor(this.externalCommand);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"jLabel1.text"));
        this.externalCommand.setText(NbBundle.getMessage(DiffOptionsPanel.class, (String)"jTextField1.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.browseCommand, (String)NbBundle.getMessage(DiffOptionsPanel.class, (String)"jButton1.text"));
        this.browseCommand.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                DiffOptionsPanel.this.browseCommandActionPerformed(evt);
            }
        });
        this.lblWarningCommand.setForeground(UIManager.getDefaults().getColor("nb.errorForeground"));
        Mnemonics.setLocalizedText((JLabel)this.lblWarningCommand, (String)" ");
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.internalDiff).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel1).addComponent(this.externalDiff))).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.ignoreWhitespace).addComponent(this.ignoreAllWhitespace).addComponent(this.ignoreCase).addComponent(this.lblWarningCommand).addGroup(layout.createSequentialGroup().addComponent(this.externalCommand, -1, 408, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.browseCommand))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.internalDiff).addComponent(this.ignoreWhitespace)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.ignoreAllWhitespace).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.ignoreCase).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.externalDiff).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.browseCommand).addComponent(this.externalCommand, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblWarningCommand).addContainerGap(-1, 32767)));
    }

    private void browseCommandActionPerformed(ActionEvent evt) {
        String execPath = this.externalCommand.getText();
        File oldFile = FileUtil.normalizeFile((File)new File(execPath));
        AccessibleJFileChooser fileChooser = new AccessibleJFileChooser(NbBundle.getMessage(DiffOptionsPanel.class, (String)"ACSD_BrowseFolder"), oldFile);
        fileChooser.setDialogTitle(NbBundle.getMessage(DiffOptionsPanel.class, (String)"BrowseFolder_Title"));
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(0);
        fileChooser.showDialog(this, NbBundle.getMessage(DiffOptionsPanel.class, (String)"BrowseFolder_OK"));
        File f = fileChooser.getSelectedFile();
        if (f != null) {
            this.externalCommand.setText(f.getAbsolutePath() + " {0} {1}");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.fireChanged();
        this.refreshComponents();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.fireChanged();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.fireChanged();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.fireChanged();
    }

    void checkExternalCommand() {
        if (this.getExternalDiff().isSelected()) {
            String cmd = this.getExternalCommand().getText();
            this.checkExternalCommand(cmd);
        } else {
            Mutex.EVENT.readAccess(new Runnable(){

                @Override
                public void run() {
                    DiffOptionsPanel.this.lblWarningCommand.setText(" ");
                }
            });
        }
    }

    private void checkExternalCommand(String cmd) {
        this.cmd = cmd;
        boolean inAwt = EventQueue.isDispatchThread();
        if (inAwt) {
            this.t.schedule(250);
        } else {
            new CheckCommand(false).run();
        }
    }

    private final class CheckCommand
    implements Runnable {
        private final boolean inPanel;

        private CheckCommand(boolean notifyInPanel) {
            this.inPanel = notifyInPanel;
        }

        @Override
        public void run() {
            String toCheck = DiffOptionsPanel.this.cmd;
            try {
                Process p = Runtime.getRuntime().exec(toCheck);
                p.destroy();
                EventQueue.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DiffOptionsPanel.this.lblWarningCommand.setText(" ");
                    }
                });
            }
            catch (IOException e) {
                if (this.inPanel) {
                    EventQueue.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            DiffOptionsPanel.this.lblWarningCommand.setText(NbBundle.getMessage(DiffOptionsController.class, (String)"MSG_InvalidDiffCommand"));
                        }
                    });
                }
                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(DiffOptionsController.class, (String)"MSG_InvalidDiffCommand"), 2));
            }
        }

    }

}

