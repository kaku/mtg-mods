/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.diff.options;

import java.awt.Component;
import java.awt.HeadlessException;
import java.io.File;
import javax.accessibility.AccessibleContext;
import javax.swing.JDialog;
import javax.swing.JFileChooser;

public class AccessibleJFileChooser
extends JFileChooser {
    private final String acsd;

    public AccessibleJFileChooser(String acsd) {
        this.acsd = acsd;
    }

    public AccessibleJFileChooser(String acsd, File currentDirectory) {
        super(currentDirectory);
        this.acsd = acsd;
    }

    @Override
    protected JDialog createDialog(Component parent) throws HeadlessException {
        JDialog dialog = super.createDialog(parent);
        dialog.getAccessibleContext().setAccessibleDescription(this.acsd);
        return dialog;
    }
}

