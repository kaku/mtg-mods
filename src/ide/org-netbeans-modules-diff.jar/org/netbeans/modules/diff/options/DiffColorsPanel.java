/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.modules.options.colors.ColorModel
 *  org.netbeans.modules.options.colors.spi.FontsColorsController
 *  org.openide.awt.ColorComboBox
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.diff.options;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.options.CategoryRenderer;
import org.netbeans.modules.diff.options.DiffOptionsPanel;
import org.netbeans.modules.options.colors.ColorModel;
import org.netbeans.modules.options.colors.spi.FontsColorsController;
import org.openide.awt.ColorComboBox;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class DiffColorsPanel
extends JPanel
implements ActionListener,
FontsColorsController {
    private static final String ATTR_NAME_ADDED = "added";
    private static final String ATTR_NAME_DELETED = "deleted";
    private static final String ATTR_NAME_CHANGED = "changed";
    private static final String ATTR_NAME_MERGE_UNRESOLVED = "merge.unresolved";
    private static final String ATTR_NAME_MERGE_APPLIED = "merge.applied";
    private static final String ATTR_NAME_MERGE_NOTAPPLIED = "merge.notapplied";
    private static final String ATTR_NAME_SIDEBAR_DELETED = "sidebar.deleted";
    private static final String ATTR_NAME_SIDEBAR_CHANGED = "sidebar.changed";
    private static final String DEFAULT_BACKGROUND = "default.background";
    private boolean listen;
    private List<AttributeSet> categories;
    private boolean changed;
    private JButton btnResetToDefaults;
    private ColorComboBox cbBackground;
    private JLabel jLabel1;
    private JLabel jLabel3;
    private JScrollPane jScrollPane1;
    private JList lCategories;

    public DiffColorsPanel() {
        this.initComponents();
        this.setName(DiffColorsPanel.loc("LBL_DiffOptions_Tab"));
        this.lCategories.setSelectionMode(0);
        this.lCategories.setVisibleRowCount(6);
        this.lCategories.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!DiffColorsPanel.this.listen) {
                    return;
                }
                DiffColorsPanel.this.refreshUI();
            }
        });
        this.lCategories.setCellRenderer(new CategoryRenderer());
        this.cbBackground.addActionListener((ActionListener)this);
        this.btnResetToDefaults.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (!this.listen) {
            return;
        }
        if (evt.getSource() == this.btnResetToDefaults) {
            this.resetToDefaults();
            this.refreshUI();
        } else {
            this.updateData();
        }
        this.fireChanged();
    }

    public void update(ColorModel colorModel) {
        this.listen = false;
        int index = this.lCategories.getSelectedIndex();
        this.lCategories.setListData(new Vector<AttributeSet>(this.getCategories()));
        if (index >= 0 && index < this.lCategories.getModel().getSize()) {
            this.lCategories.setSelectedIndex(index);
        } else {
            this.lCategories.setSelectedIndex(0);
        }
        this.refreshUI();
        this.listen = true;
        this.changed = false;
    }

    public void cancel() {
        this.changed = false;
        this.categories = null;
    }

    public void applyChanges() {
        List<AttributeSet> colors = this.getCategories();
        for (AttributeSet color : colors) {
            if ("added".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setAddedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if ("changed".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setChangedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if ("deleted".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setDeletedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if ("merge.applied".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setAppliedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if ("merge.notapplied".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setNotAppliedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if ("merge.unresolved".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setUnresolvedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if ("sidebar.deleted".equals(color.getAttribute(StyleConstants.NameAttribute))) {
                DiffModuleConfig.getDefault().setSidebarDeletedColor((Color)color.getAttribute(StyleConstants.Background));
            }
            if (!"sidebar.changed".equals(color.getAttribute(StyleConstants.NameAttribute))) continue;
            DiffModuleConfig.getDefault().setSidebarChangedColor((Color)color.getAttribute(StyleConstants.Background));
        }
        this.changed = false;
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void setCurrentProfile(String currentProfile) {
        this.refreshUI();
    }

    public void deleteProfile(String profile) {
    }

    public JComponent getComponent() {
        return this;
    }

    Collection<AttributeSet> getHighlightings() {
        return this.getCategories();
    }

    private static String loc(String key) {
        return NbBundle.getMessage(DiffColorsPanel.class, (String)key);
    }

    private void fireChanged() {
        List<AttributeSet> colors = this.getCategories();
        boolean isChanged = false;
        for (AttributeSet color : colors) {
            isChanged |= this.fireChanged(color);
        }
        this.changed = isChanged;
    }

    private boolean fireChanged(AttributeSet color) {
        Color c = (Color)color.getAttribute(StyleConstants.Background);
        if ("added".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getAddedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultAddedColor());
        }
        if ("changed".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getChangedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultChangedColor());
        }
        if ("deleted".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getDeletedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultDeletedColor());
        }
        if ("merge.applied".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getAppliedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultAppliedColor());
        }
        if ("merge.notapplied".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getNotAppliedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultNotAppliedColor());
        }
        if ("merge.unresolved".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getUnresolvedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultUnresolvedColor());
        }
        if ("sidebar.deleted".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getSidebarDeletedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultSidebarDeletedColor());
        }
        if ("sidebar.changed".equals(color.getAttribute(StyleConstants.NameAttribute))) {
            return !DiffModuleConfig.getDefault().getSidebarChangedColor().equals(c != null ? c : DiffModuleConfig.getDefault().getDefaultSidebarChangedColor());
        }
        return false;
    }

    private void updateData() {
        int index = this.lCategories.getSelectedIndex();
        if (index < 0) {
            return;
        }
        List<AttributeSet> categories = this.getCategories();
        AttributeSet category = categories.get(this.lCategories.getSelectedIndex());
        SimpleAttributeSet c = new SimpleAttributeSet(category);
        Color color = this.cbBackground.getSelectedColor();
        if (color != null) {
            c.addAttribute(StyleConstants.Background, color);
        } else {
            c.removeAttribute(StyleConstants.Background);
        }
        categories.set(index, c);
    }

    private void refreshUI() {
        int index = this.lCategories.getSelectedIndex();
        if (index < 0) {
            this.cbBackground.setEnabled(false);
            return;
        }
        this.cbBackground.setEnabled(true);
        List<AttributeSet> categories = this.getCategories();
        AttributeSet category = categories.get(index);
        this.listen = false;
        this.cbBackground.setSelectedColor((Color)category.getAttribute(StyleConstants.Background));
        this.listen = true;
    }

    private void resetToDefaults() {
        List<AttributeSet> categories = this.getCategories();
        ListIterator<AttributeSet> it = categories.listIterator();
        while (it.hasNext()) {
            AttributeSet category = it.next();
            SimpleAttributeSet c = new SimpleAttributeSet(category);
            if (category.getAttribute("default.background").equals(c.getAttribute(StyleConstants.Background))) continue;
            c.addAttribute(StyleConstants.Background, category.getAttribute("default.background"));
            it.set(c);
        }
    }

    private List<AttributeSet> getCategories() {
        if (this.categories == null) {
            this.categories = this.getDiffHighlights();
        }
        return this.categories;
    }

    private List<AttributeSet> getDiffHighlights() {
        ArrayList<AttributeSet> attrs = new ArrayList<AttributeSet>();
        SimpleAttributeSet sas = null;
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getAddedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "added");
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultAddedColor());
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_AddedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getDeletedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "deleted");
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultDeletedColor());
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_DeletedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getChangedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "changed");
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultChangedColor());
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_ChangedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getAppliedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "merge.applied");
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultAppliedColor());
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_AppliedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getNotAppliedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "merge.notapplied");
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultNotAppliedColor());
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_NotAppliedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getUnresolvedColor());
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultUnresolvedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "merge.unresolved");
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_UnresolvedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getSidebarDeletedColor());
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultSidebarDeletedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "sidebar.deleted");
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_SidebarDeletedColor"));
        attrs.add(sas);
        sas = new SimpleAttributeSet();
        StyleConstants.setBackground(sas, DiffModuleConfig.getDefault().getSidebarChangedColor());
        sas.addAttribute("default.background", DiffModuleConfig.getDefault().getDefaultSidebarChangedColor());
        sas.addAttribute(StyleConstants.NameAttribute, "sidebar.changed");
        sas.addAttribute(EditorStyleConstants.DisplayName, NbBundle.getMessage(DiffOptionsPanel.class, (String)"LBL_SidebarChangedColor"));
        attrs.add(sas);
        return attrs;
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.lCategories = new JList();
        this.jLabel3 = new JLabel();
        this.cbBackground = new ColorComboBox();
        this.btnResetToDefaults = new JButton();
        this.jLabel1.setLabelFor(this.lCategories);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(DiffColorsPanel.class, (String)"DiffColorsPanel.jLabel1.text"));
        this.lCategories.setModel(new DefaultListModel());
        this.jScrollPane1.setViewportView(this.lCategories);
        this.jLabel3.setLabelFor((Component)this.cbBackground);
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(DiffColorsPanel.class, (String)"DiffColorsPanel.jLabel3.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.btnResetToDefaults, (String)NbBundle.getMessage(DiffColorsPanel.class, (String)"DiffColorsPanel.btnResetToDefaults.text"));
        this.btnResetToDefaults.setToolTipText(NbBundle.getMessage(DiffColorsPanel.class, (String)"DiffColorsPanel.btnResetToDefaults.TTtext"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.jScrollPane1, -2, -1, -2).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(20, 20, 20).addComponent(this.jLabel3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent((Component)this.cbBackground, -2, -1, -2)).addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addComponent(this.btnResetToDefaults)))).addComponent(this.jLabel1)).addContainerGap(-1, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent((Component)this.cbBackground, -2, -1, -2)).addGap(18, 18, 18).addComponent(this.btnResetToDefaults).addGap(0, 0, 32767)).addComponent(this.jScrollPane1, -1, 382, 32767)).addContainerGap()));
    }

}

