/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.diff.options;

import java.beans.PropertyChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.diff.builtin.provider.BuiltInDiffProvider;
import org.netbeans.modules.diff.options.DiffOptionsPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class DiffOptionsController
extends OptionsPanelController {
    public static final String OPTIONS_SUBPATH = "Diff";
    private DiffOptionsPanel panel;

    public void update() {
        this.panel.getInternalDiff().setSelected(DiffModuleConfig.getDefault().isUseInteralDiff());
        this.panel.getExternalDiff().setSelected(!DiffModuleConfig.getDefault().isUseInteralDiff());
        this.panel.getIgnoreWhitespace().setSelected(DiffModuleConfig.getDefault().getOptions().ignoreLeadingAndtrailingWhitespace);
        this.panel.getIgnoreInnerWhitespace().setSelected(DiffModuleConfig.getDefault().getOptions().ignoreInnerWhitespace);
        this.panel.getIgnoreCase().setSelected(DiffModuleConfig.getDefault().getOptions().ignoreCase);
        this.panel.getExternalCommand().setText(DiffModuleConfig.getDefault().getPreferences().get("externalDiffCommand", "diff {0} {1}"));
        this.panel.refreshComponents();
        this.panel.setChanged(false);
    }

    public void applyChanges() {
        this.panel.checkExternalCommand();
        DiffModuleConfig.getDefault().setUseInteralDiff(this.panel.getInternalDiff().isSelected());
        BuiltInDiffProvider.Options options = new BuiltInDiffProvider.Options();
        options.ignoreLeadingAndtrailingWhitespace = this.panel.getIgnoreWhitespace().isSelected();
        options.ignoreInnerWhitespace = this.panel.getIgnoreInnerWhitespace().isSelected();
        options.ignoreCase = this.panel.getIgnoreCase().isSelected();
        DiffModuleConfig.getDefault().setOptions(options);
        DiffModuleConfig.getDefault().getPreferences().put("externalDiffCommand", this.panel.getExternalCommand().getText());
        this.panel.setChanged(false);
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.panel.isChanged();
    }

    public JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            this.panel = new DiffOptionsPanel();
        }
        return this.panel;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.modules.diff.options.DiffOptionsController");
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
    }
}

