/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.diff;

import java.io.File;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;

public class Utils {
    private static RequestProcessor parallelRP;

    public static RequestProcessor.Task createParallelTask(Runnable runnable) {
        RequestProcessor rp = Utils.getParallelRequestProcessor();
        return rp.create(runnable);
    }

    public static RequestProcessor.Task postParallel(Runnable runnable) {
        RequestProcessor rp = Utils.getParallelRequestProcessor();
        return rp.post(runnable);
    }

    public static void openFile(File file) {
        FileObject fo = FileUtil.toFileObject((File)file);
        if (fo != null) {
            try {
                DataObject dao = DataObject.find((FileObject)fo);
                OpenCookie oc = (OpenCookie)dao.getCookie(OpenCookie.class);
                if (oc != null) {
                    oc.open();
                }
            }
            catch (DataObjectNotFoundException e) {
                // empty catch block
            }
        }
    }

    private static RequestProcessor getParallelRequestProcessor() {
        if (parallelRP == null) {
            parallelRP = new RequestProcessor("Diff.ParallelTasks", 5);
        }
        return parallelRP;
    }

    public static boolean isFileContentBinary(FileObject fo) {
        if (fo == null) {
            return false;
        }
        try {
            DataObject dao = DataObject.find((FileObject)fo);
            return dao.getCookie(EditorCookie.class) == null;
        }
        catch (DataObjectNotFoundException e) {
            return false;
        }
    }

    private Utils() {
    }
}

