/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.actions.CopyAction
 *  org.openide.actions.SaveAction
 *  org.openide.awt.Mnemonics
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.ActionPerformer
 *  org.openide.util.actions.CallbackSystemAction
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.merge.builtin.visualizer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Position;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import org.netbeans.api.diff.Difference;
import org.netbeans.modules.diff.builtin.visualizer.LinesComponent;
import org.netbeans.modules.merge.builtin.visualizer.CloseMergeViewAction;
import org.netbeans.modules.merge.builtin.visualizer.MergePane;
import org.openide.ErrorManager;
import org.openide.actions.CopyAction;
import org.openide.actions.SaveAction;
import org.openide.awt.Mnemonics;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.actions.ActionPerformer;
import org.openide.util.actions.CallbackSystemAction;
import org.openide.util.actions.SystemAction;

public class MergePanel
extends JPanel
implements ActionListener,
CaretListener {
    public static final String ACTION_FIRST_CONFLICT = "firstConflict";
    public static final String ACTION_LAST_CONFLICT = "lastConflict";
    public static final String ACTION_PREVIOUS_CONFLICT = "previousConflict";
    public static final String ACTION_NEXT_CONFLICT = "nextConflict";
    public static final String ACTION_ACCEPT_RIGHT = "acceptRight";
    public static final String ACTION_ACCEPT_LEFT_RIGHT = "acceptLeftRight";
    public static final String ACTION_ACCEPT_LEFT = "acceptLeft";
    public static final String ACTION_ACCEPT_RIGHT_LEFT = "acceptRightLeft";
    public static final String PROP_CAN_BE_SAVED = "canBeSaved";
    public static final String PROP_CAN_NOT_BE_SAVED = "canNotBeSaved";
    private int totalHeight = 0;
    private int totalLines = 0;
    private int horizontalScroll1ChangedValue = -1;
    private int horizontalScroll2ChangedValue = -1;
    private int horizontalScroll3ChangedValue = -1;
    private int verticalScroll1ChangedValue = -1;
    private int verticalScroll3ChangedValue = -1;
    private LinesComponent linesComp1;
    private LinesComponent linesComp2;
    private LinesComponent linesComp3;
    private int[] resultLineNumbers;
    private int numConflicts;
    private int numUnresolvedConflicts;
    private int currentConflictPos;
    private final List<Integer> resolvedLeftConflictsLineNumbers = new ArrayList<Integer>();
    private final List<Integer> resolvedRightConflictsLineNumbers = new ArrayList<Integer>();
    private final List<Integer> resolvedLeftRightConflictsLineNumbers = new ArrayList<Integer>();
    private final List<Integer> resolvedRightLeftConflictsLineNumbers = new ArrayList<Integer>();
    private ArrayList<ActionListener> controlListeners = new ArrayList();
    private SystemAction[] systemActions = new SystemAction[]{SaveAction.get(SaveAction.class), null, CloseMergeViewAction.get(CloseMergeViewAction.class)};
    static final long serialVersionUID = 3683458237532937983L;
    private static final String PLAIN_TEXT_MIME = "text/plain";
    private Difference[] conflicts;
    private Hashtable<JEditorPane, Hashtable<Object, Action>> kitActions;
    private PropertyChangeListener copyL;
    private PropertyChangeListener copyP;
    private static final int EXTRA_CAPACITY = 5;
    private JViewport jViewport1;
    private JViewport jViewport2;
    private JButton acceptAndNextLeftButton;
    private JButton acceptAndNextRightButton;
    private JButton acceptLeftButton;
    private JButton acceptLeftRightButton;
    private JButton acceptRightButton;
    private JButton acceptRightLeftButton;
    final JPanel commandPanel = new JPanel();
    final JSplitPane diffSplitPane = new JSplitPane();
    final JPanel editorPanel = new JPanel();
    final JLabel fileLabel1 = new JLabel();
    final JLabel fileLabel2 = new JLabel();
    final JPanel filePanel1 = new JPanel();
    final JPanel filePanel2 = new JPanel();
    final JButton firstConflictButton = new JButton();
    final JEditorPane jEditorPane1 = new MergePane();
    final JEditorPane jEditorPane2 = new MergePane();
    final JEditorPane jEditorPane3 = new MergePane();
    final JScrollPane jScrollPane1 = new JScrollPane();
    final JScrollPane jScrollPane2 = new JScrollPane();
    final JButton lastConflictButton = new JButton();
    final JPanel leftCommandPanel = new JPanel();
    final JSplitPane mergeSplitPane = new JSplitPane();
    final JButton nextConflictButton = new JButton();
    final JButton prevConflictButton = new JButton();
    final JLabel resultLabel = new JLabel();
    final JPanel resultPanel = new JPanel();
    final JScrollPane resultScrollPane = new JScrollPane();
    final JPanel rightCommandPanel = new JPanel();
    final JLabel statusLabel = new JLabel();

    public MergePanel() {
        this.initComponents();
        this.firstConflictButton.setVisible(false);
        this.lastConflictButton.setVisible(false);
        this.prevConflictButton.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/diff/builtin/visualizer/prev.gif", (boolean)true));
        this.nextConflictButton.setIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/diff/builtin/visualizer/next.gif", (boolean)true));
        this.initActions();
        this.diffSplitPane.setResizeWeight(0.5);
        this.mergeSplitPane.setResizeWeight(0.5);
        this.putClientProperty("PersistenceType", "Never");
        this.jEditorPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(MergePanel.class, (String)"ACS_EditorPane1A11yName"));
        this.jEditorPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MergePanel.class, (String)"ACS_EditorPane1A11yDescr"));
        this.jEditorPane1.putClientProperty("org.netbeans.modules.merge.builtin.visualizer.MergePanel", this.jEditorPane1);
        this.jEditorPane2.getAccessibleContext().setAccessibleName(NbBundle.getMessage(MergePanel.class, (String)"ACS_EditorPane2A11yName"));
        this.jEditorPane2.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MergePanel.class, (String)"ACS_EditorPane2A11yDescr"));
        this.jEditorPane2.putClientProperty("org.netbeans.modules.merge.builtin.visualizer.MergePanel", this.jEditorPane2);
        this.jEditorPane3.getAccessibleContext().setAccessibleName(NbBundle.getMessage(MergePanel.class, (String)"ACS_EditorPane3A11yName"));
        this.jEditorPane3.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MergePanel.class, (String)"ACS_EditorPane3A11yDescr"));
        this.jEditorPane3.putClientProperty("org.netbeans.modules.merge.builtin.visualizer.MergePanel", this.jEditorPane3);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.jEditorPane1.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        this.jEditorPane2.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        this.jEditorPane3.putClientProperty("HighlightsLayerExcludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.CaretRowHighlighting$");
        this.jEditorPane3.getDocument().addDocumentListener((MergePane)this.jEditorPane3);
    }

    @Override
    public void removeNotify() {
        this.jEditorPane3.getDocument().removeDocumentListener((MergePane)this.jEditorPane3);
        super.removeNotify();
    }

    private void initComponents() {
        this.acceptLeftButton = new JButton();
        this.acceptLeftRightButton = new JButton();
        this.acceptAndNextLeftButton = new JButton();
        this.acceptRightButton = new JButton();
        this.acceptRightLeftButton = new JButton();
        this.acceptAndNextRightButton = new JButton();
        this.setLayout(new GridBagLayout());
        this.commandPanel.setLayout(new GridBagLayout());
        this.firstConflictButton.setPreferredSize(new Dimension(24, 24));
        this.firstConflictButton.addActionListener(this);
        this.commandPanel.add((Component)this.firstConflictButton, new GridBagConstraints());
        this.prevConflictButton.setToolTipText(NbBundle.getMessage(MergePanel.class, (String)"MergePanel.prevButton.toolTipText"));
        this.prevConflictButton.setMargin(new Insets(1, 1, 0, 1));
        this.prevConflictButton.setMaximumSize(new Dimension(24, 24));
        this.prevConflictButton.setMinimumSize(new Dimension(24, 24));
        this.prevConflictButton.setPreferredSize(new Dimension(24, 24));
        this.prevConflictButton.addActionListener(this);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 2, 0, 1);
        this.commandPanel.add((Component)this.prevConflictButton, gridBagConstraints);
        this.nextConflictButton.setToolTipText(NbBundle.getMessage(MergePanel.class, (String)"MergePanel.nextButton.toolTipText"));
        this.nextConflictButton.setMargin(new Insets(1, 1, 0, 1));
        this.nextConflictButton.setMaximumSize(new Dimension(24, 24));
        this.nextConflictButton.setMinimumSize(new Dimension(24, 24));
        this.nextConflictButton.setPreferredSize(new Dimension(24, 24));
        this.nextConflictButton.addActionListener(this);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 1, 0, 1);
        this.commandPanel.add((Component)this.nextConflictButton, gridBagConstraints);
        this.lastConflictButton.setPreferredSize(new Dimension(24, 24));
        this.lastConflictButton.addActionListener(this);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 1, 0, 2);
        this.commandPanel.add((Component)this.lastConflictButton, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.statusLabel, (String)"jLabel1");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 4, 0, 1);
        this.commandPanel.add((Component)this.statusLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this.commandPanel, gridBagConstraints);
        this.editorPanel.setPreferredSize(new Dimension(700, 600));
        this.editorPanel.setLayout(new GridBagLayout());
        this.mergeSplitPane.setDividerSize(4);
        this.mergeSplitPane.setOrientation(0);
        this.diffSplitPane.setDividerSize(4);
        this.filePanel1.setLayout(new GridBagLayout());
        this.leftCommandPanel.setLayout(new BoxLayout(this.leftCommandPanel, 2));
        Mnemonics.setLocalizedText((AbstractButton)this.acceptLeftButton, (String)NbBundle.getMessage(MergePanel.class, (String)"MergePanel.acceptLeftButton.text"));
        this.acceptLeftButton.setToolTipText(NbBundle.getBundle(MergePanel.class).getString("ACS_MergePanel.acceptLeftButton.textA11yDesc"));
        this.acceptLeftButton.addActionListener(this);
        this.leftCommandPanel.add(this.acceptLeftButton);
        Mnemonics.setLocalizedText((AbstractButton)this.acceptLeftRightButton, (String)NbBundle.getMessage(MergePanel.class, (String)"MergePanel.acceptLeftRightButton.text"));
        this.acceptLeftRightButton.setToolTipText(NbBundle.getBundle(MergePanel.class).getString("MergePanel.acceptLeftRightButton.TTtext"));
        this.acceptLeftRightButton.setActionCommand(NbBundle.getMessage(MergePanel.class, (String)"MergePanel.declineLeftButton.text"));
        this.acceptLeftRightButton.addActionListener(this);
        this.leftCommandPanel.add(this.acceptLeftRightButton);
        Mnemonics.setLocalizedText((AbstractButton)this.acceptAndNextLeftButton, (String)NbBundle.getMessage(MergePanel.class, (String)"MergePanel.acceptAndNextLeftButton"));
        this.acceptAndNextLeftButton.setToolTipText(NbBundle.getBundle(MergePanel.class).getString("ACS_MergePanel.acceptAndNextLeftButtonA11yDesc"));
        this.acceptAndNextLeftButton.addActionListener(this);
        this.leftCommandPanel.add(this.acceptAndNextLeftButton);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        this.filePanel1.add((Component)this.leftCommandPanel, gridBagConstraints);
        this.jEditorPane1.addCaretListener(this);
        this.jScrollPane1.setViewportView(this.jEditorPane1);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.filePanel1.add((Component)this.jScrollPane1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.fileLabel1, (String)"jLabel1");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.filePanel1.add((Component)this.fileLabel1, gridBagConstraints);
        this.diffSplitPane.setLeftComponent(this.filePanel1);
        this.filePanel2.setLayout(new GridBagLayout());
        this.rightCommandPanel.setLayout(new BoxLayout(this.rightCommandPanel, 2));
        Mnemonics.setLocalizedText((AbstractButton)this.acceptRightButton, (String)NbBundle.getMessage(MergePanel.class, (String)"MergePanel.acceptRightButton.text"));
        this.acceptRightButton.setToolTipText(NbBundle.getBundle(MergePanel.class).getString("ACS_MergePanel.acceptRightButton.textA11yDesc"));
        this.acceptRightButton.addActionListener(this);
        this.rightCommandPanel.add(this.acceptRightButton);
        Mnemonics.setLocalizedText((AbstractButton)this.acceptRightLeftButton, (String)NbBundle.getMessage(MergePanel.class, (String)"MergePanel.acceptRightLeftButton.text"));
        this.acceptRightLeftButton.setToolTipText(NbBundle.getBundle(MergePanel.class).getString("MergePanel.acceptRightLeftButton.TTtext"));
        this.acceptRightLeftButton.setActionCommand(NbBundle.getMessage(MergePanel.class, (String)"MergePanel.declineLeftButton.text"));
        this.acceptRightLeftButton.addActionListener(this);
        this.rightCommandPanel.add(this.acceptRightLeftButton);
        Mnemonics.setLocalizedText((AbstractButton)this.acceptAndNextRightButton, (String)NbBundle.getMessage(MergePanel.class, (String)"MergePanel.acceptAndNextRightButton"));
        this.acceptAndNextRightButton.setToolTipText(NbBundle.getBundle(MergePanel.class).getString("ACS_MergePanel.acceptAndNextRightButtonA11yDesc"));
        this.acceptAndNextRightButton.addActionListener(this);
        this.rightCommandPanel.add(this.acceptAndNextRightButton);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        this.filePanel2.add((Component)this.rightCommandPanel, gridBagConstraints);
        this.jEditorPane2.addCaretListener(this);
        this.jScrollPane2.setViewportView(this.jEditorPane2);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.filePanel2.add((Component)this.jScrollPane2, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.fileLabel2, (String)"jLabel2");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.filePanel2.add((Component)this.fileLabel2, gridBagConstraints);
        this.diffSplitPane.setRightComponent(this.filePanel2);
        this.mergeSplitPane.setLeftComponent(this.diffSplitPane);
        this.resultPanel.setLayout(new GridBagLayout());
        this.resultScrollPane.setViewportView(this.jEditorPane3);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.resultPanel.add((Component)this.resultScrollPane, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.resultLabel, (String)"jLabel1");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        this.resultPanel.add((Component)this.resultLabel, gridBagConstraints);
        this.mergeSplitPane.setRightComponent(this.resultPanel);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.editorPanel.add((Component)this.mergeSplitPane, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.editorPanel, gridBagConstraints);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() == this.firstConflictButton) {
            this.firstConflictButtonActionPerformed(evt);
        } else if (evt.getSource() == this.prevConflictButton) {
            this.prevConflictButtonActionPerformed(evt);
        } else if (evt.getSource() == this.nextConflictButton) {
            this.nextConflictButtonActionPerformed(evt);
        } else if (evt.getSource() == this.lastConflictButton) {
            this.lastConflictButtonActionPerformed(evt);
        } else if (evt.getSource() == this.acceptLeftButton) {
            this.acceptLeftButtonActionPerformed(evt);
        } else if (evt.getSource() == this.acceptLeftRightButton) {
            this.acceptLeftRightButtonActionPerformed(evt);
        } else if (evt.getSource() == this.acceptAndNextLeftButton) {
            this.acceptAndNextLeftButtonActionPerformed(evt);
        } else if (evt.getSource() == this.acceptRightButton) {
            this.acceptRightButtonActionPerformed(evt);
        } else if (evt.getSource() == this.acceptRightLeftButton) {
            this.acceptRightLeftButtonActionPerformed(evt);
        } else if (evt.getSource() == this.acceptAndNextRightButton) {
            this.acceptAndNextRightButtonActionPerformed(evt);
        }
    }

    @Override
    public void caretUpdate(CaretEvent evt) {
        if (evt.getSource() == this.jEditorPane1) {
            this.jEditorPane1CaretUpdate(evt);
        } else if (evt.getSource() == this.jEditorPane2) {
            this.jEditorPane2CaretUpdate(evt);
        }
    }

    private void firstConflictButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("firstConflict");
    }

    private void prevConflictButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("previousConflict");
    }

    private void nextConflictButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("nextConflict");
    }

    private void lastConflictButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("lastConflict");
    }

    private void acceptRightButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("acceptRight");
    }

    private void acceptAndNextRightButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("acceptRight");
        this.fireControlActionCommand("nextConflict");
    }

    private void acceptAndNextLeftButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("acceptLeft");
        this.fireControlActionCommand("nextConflict");
    }

    private void acceptLeftButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("acceptLeft");
    }

    private void jEditorPane1CaretUpdate(CaretEvent evt) {
    }

    private void jEditorPane2CaretUpdate(CaretEvent evt) {
    }

    public void setConflicts(Difference[] diffs) {
        this.conflicts = diffs;
        this.numUnresolvedConflicts = this.numConflicts = diffs.length;
    }

    public int getNumUnresolvedConflicts() {
        return this.numUnresolvedConflicts;
    }

    public void setCurrentLine(final int line, final int diffLength, final int conflictPos, final int resultLine) {
        if (line > 0) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    MergePanel.this.showLine12(line, diffLength);
                    MergePanel.this.showLine3(resultLine, diffLength);
                    if (conflictPos >= 0) {
                        MergePanel.this.currentConflictPos = conflictPos;
                    }
                    MergePanel.this.updateStatusLine();
                    MergePanel.this.updateAcceptButtons(line);
                }
            });
        }
    }

    public void setNeedsSaveState(boolean needsSave) {
        this.firePropertyChange(needsSave ? "canBeSaved" : "canNotBeSaved", null, null);
    }

    public synchronized void addControlActionListener(ActionListener listener) {
        this.controlListeners.add(listener);
    }

    public synchronized void removeControlActionListener(ActionListener listener) {
        this.controlListeners.remove(listener);
    }

    private void updateStatusLine() {
        this.statusLabel.setText(NbBundle.getMessage(MergePanel.class, (String)"MergePanel.statusLine", (Object)Integer.toString(this.currentConflictPos + 1), (Object)Integer.toString(this.numConflicts), (Object)Integer.toString(this.numUnresolvedConflicts)));
    }

    private void updateAcceptButtons(int linePos) {
        Integer conflictPos = new Integer(linePos);
        boolean left = this.resolvedLeftConflictsLineNumbers.contains(conflictPos);
        boolean right = this.resolvedRightConflictsLineNumbers.contains(conflictPos);
        boolean leftRight = this.resolvedLeftRightConflictsLineNumbers.contains(conflictPos);
        boolean rightLeft = this.resolvedRightLeftConflictsLineNumbers.contains(conflictPos);
        this.acceptLeftButton.setEnabled(!left);
        this.acceptLeftRightButton.setEnabled(this.conflicts[this.currentConflictPos].getType() == 2 && !leftRight);
        this.acceptAndNextLeftButton.setEnabled(!left);
        this.acceptRightButton.setEnabled(!right);
        this.acceptRightLeftButton.setEnabled(this.conflicts[this.currentConflictPos].getType() == 2 && !rightLeft);
        this.acceptAndNextRightButton.setEnabled(!right);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireControlActionCommand(String command) {
        ArrayList<ActionListener> listeners;
        MergePanel mergePanel = this;
        synchronized (mergePanel) {
            listeners = new ArrayList<ActionListener>(this.controlListeners);
        }
        ActionEvent evt = new ActionEvent(this, 0, command);
        for (ActionListener l : listeners) {
            l.actionPerformed(evt);
        }
    }

    private void jScrollBar1AdjustmentValueChanged(AdjustmentEvent evt) {
    }

    private void closeButtonActionPerformed(ActionEvent evt) {
        this.exitForm(null);
    }

    private void exitForm(WindowEvent evt) {
    }

    private void acceptLeftRightButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("acceptLeftRight");
    }

    private void acceptRightLeftButtonActionPerformed(ActionEvent evt) {
        this.fireControlActionCommand("acceptRightLeft");
    }

    public void setSystemActions(SystemAction[] actions) {
        this.systemActions = actions;
    }

    public SystemAction[] getSystemActions() {
        return this.systemActions;
    }

    private void initActions() {
        this.jEditorPane1.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                MergePanel.this.editorActivated(MergePanel.this.jEditorPane1);
            }

            @Override
            public void focusLost(FocusEvent e) {
                MergePanel.this.editorDeactivated(MergePanel.this.jEditorPane1);
            }
        });
        this.jEditorPane2.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                MergePanel.this.editorActivated(MergePanel.this.jEditorPane2);
            }

            @Override
            public void focusLost(FocusEvent e) {
                MergePanel.this.editorDeactivated(MergePanel.this.jEditorPane2);
            }
        });
        this.jEditorPane3.addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent e) {
                MergePanel.this.editorActivated(MergePanel.this.jEditorPane3);
            }

            @Override
            public void focusLost(FocusEvent e) {
                MergePanel.this.editorDeactivated(MergePanel.this.jEditorPane3);
            }
        });
    }

    private Action getAction(String s, JEditorPane editor) {
        Hashtable actions;
        if (this.kitActions == null) {
            this.kitActions = new Hashtable();
        }
        if ((actions = this.kitActions.get(editor)) == null) {
            EditorKit kit = editor.getEditorKit();
            if (kit == null) {
                return null;
            }
            Action[] a = kit.getActions();
            actions = new Hashtable(a.length);
            int k = a.length;
            for (int i = 0; i < k; ++i) {
                actions.put(a[i].getValue("Name"), a[i]);
            }
            this.kitActions.put(editor, actions);
        }
        return actions.get(s);
    }

    private void editorActivated(final JEditorPane editor) {
        final Action copy = this.getAction("copy-to-clipboard", editor);
        if (copy != null) {
            final CallbackSystemAction sysCopy = (CallbackSystemAction)SystemAction.get(CopyAction.class);
            final ActionPerformer perf = new ActionPerformer(){

                public void performAction(SystemAction action) {
                    copy.actionPerformed(new ActionEvent(editor, 0, ""));
                }
            };
            sysCopy.setActionPerformer(copy.isEnabled() ? perf : null);
            PropertyChangeListener copyListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("enabled".equals(evt.getPropertyName())) {
                        if (((Boolean)evt.getNewValue()).booleanValue()) {
                            sysCopy.setActionPerformer(perf);
                        } else if (sysCopy.getActionPerformer() == perf) {
                            sysCopy.setActionPerformer(null);
                        }
                    }
                }
            };
            copy.addPropertyChangeListener(copyListener);
            if (editor.equals(this.jEditorPane1)) {
                this.copyL = copyListener;
            } else {
                this.copyP = copyListener;
            }
        }
    }

    private void editorDeactivated(JEditorPane editor) {
        Action copy = this.getAction("copy-to-clipboard", editor);
        PropertyChangeListener copyListener = editor.equals(this.jEditorPane1) ? this.copyL : this.copyP;
        if (copy != null) {
            copy.removePropertyChangeListener(copyListener);
        }
    }

    public void open() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                MergePanel.this.diffSplitPane.setDividerLocation(0.5);
                MergePanel.this.mergeSplitPane.setDividerLocation(0.5);
                MergePanel.this.openPostProcess();
            }
        });
    }

    protected void openPostProcess() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                MergePanel.this.initGlobalSizes();
                MergePanel.this.addChangeListeners();
            }
        });
    }

    private void initGlobalSizes() {
        int numLines2;
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane2.getDocument();
        int numLines1 = NbDocument.findLineNumber((StyledDocument)doc1, (int)doc1.getEndPosition().getOffset());
        int numLines = Math.max(numLines1, numLines2 = NbDocument.findLineNumber((StyledDocument)doc2, (int)doc2.getEndPosition().getOffset()));
        if (numLines < 1) {
            numLines = 1;
        }
        this.totalLines = numLines;
        int value = this.jEditorPane2.getSize().height;
        int totHeight = this.jEditorPane1.getSize().height;
        if (value > totHeight) {
            totHeight = value;
        }
        this.totalHeight = totHeight;
    }

    private void showLine12(int line, int diffLength) {
        assert (SwingUtilities.isEventDispatchThread());
        this.linesComp1.setActiveLine(line);
        this.linesComp2.setActiveLine(line);
        this.linesComp1.repaint();
        this.linesComp2.repaint();
        int padding = 5;
        if (line <= 5) {
            padding = line / 2;
        }
        int viewHeight = this.jViewport1.getExtentSize().height;
        this.initGlobalSizes();
        Point p1 = this.jViewport1.getViewPosition();
        Point p2 = this.jViewport2.getViewPosition();
        int ypos = this.totalHeight * (line - padding - 1) / (this.totalLines + 1);
        int viewSize = this.jViewport1.getViewRect().y;
        if (ypos < p1.y || ypos + (diffLength + padding) * this.totalHeight / this.totalLines > p1.y + viewHeight) {
            p1.y = ypos;
            p2.y = ypos;
            this.setViewPosition(p1, p2);
        }
        try {
            int off1 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane1.getDocument()), (int)line);
            this.jEditorPane1.setCaretPosition(off1);
        }
        catch (IndexOutOfBoundsException ex) {
            // empty catch block
        }
        try {
            int off2 = NbDocument.findLineOffset((StyledDocument)((StyledDocument)this.jEditorPane2.getDocument()), (int)line);
            this.jEditorPane2.setCaretPosition(off2);
        }
        catch (IndexOutOfBoundsException ex) {
            // empty catch block
        }
    }

    private void showLine3(int line, int diffLength) {
        this.linesComp3.setActiveLine(line);
        this.linesComp3.repaint();
    }

    private void setViewPosition(Point p1, Point p2) {
        assert (SwingUtilities.isEventDispatchThread());
        this.jViewport1.setViewPosition(p1);
        this.jViewport1.repaint(this.jViewport1.getViewRect());
        this.jViewport2.setViewPosition(p2);
        this.jViewport2.repaint(this.jViewport2.getViewRect());
    }

    private void joinScrollBars() {
        final JScrollBar scrollBarH1 = this.jScrollPane1.getHorizontalScrollBar();
        final JScrollBar scrollBarV1 = this.jScrollPane1.getVerticalScrollBar();
        final JScrollBar scrollBarH2 = this.jScrollPane2.getHorizontalScrollBar();
        final JScrollBar scrollBarV2 = this.jScrollPane2.getVerticalScrollBar();
        final JScrollBar scrollBarH3 = this.resultScrollPane.getHorizontalScrollBar();
        final JScrollBar scrollBarV3 = this.resultScrollPane.getVerticalScrollBar();
        scrollBarV1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarV1.getValue();
                int oldValue = scrollBarV2.getValue();
                if (oldValue != value) {
                    scrollBarV2.setValue(value);
                }
                if (value == MergePanel.this.verticalScroll1ChangedValue) {
                    return;
                }
                int max1 = scrollBarV1.getMaximum();
                int max2 = scrollBarV3.getMaximum();
                int ext1 = scrollBarV1.getModel().getExtent();
                int ext2 = scrollBarV3.getModel().getExtent();
                if (max1 == ext1) {
                    MergePanel.this.verticalScroll3ChangedValue = 0;
                } else {
                    MergePanel.this.verticalScroll3ChangedValue = value * (max2 - ext2) / (max1 - ext1);
                }
                MergePanel.this.verticalScroll1ChangedValue = -1;
                scrollBarV3.setValue(MergePanel.this.verticalScroll3ChangedValue);
            }
        });
        this.jScrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollBarV2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarV2.getValue();
                int oldValue = scrollBarV1.getValue();
                if (oldValue != value) {
                    scrollBarV1.setValue(value);
                }
            }
        });
        scrollBarH1.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH1.getValue();
                if (value == MergePanel.this.horizontalScroll1ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                if (max1 == ext1) {
                    MergePanel.this.horizontalScroll2ChangedValue = 0;
                } else {
                    MergePanel.this.horizontalScroll2ChangedValue = value * (max2 - ext2) / (max1 - ext1);
                }
                MergePanel.this.horizontalScroll1ChangedValue = -1;
                scrollBarH2.setValue(MergePanel.this.horizontalScroll2ChangedValue);
            }
        });
        scrollBarH2.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH2.getValue();
                if (value == MergePanel.this.horizontalScroll2ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int max3 = scrollBarH3.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                int ext3 = scrollBarH3.getModel().getExtent();
                if (max2 == ext2) {
                    MergePanel.this.horizontalScroll1ChangedValue = 0;
                    MergePanel.this.horizontalScroll3ChangedValue = 0;
                } else {
                    MergePanel.this.horizontalScroll1ChangedValue = value * (max1 - ext1) / (max2 - ext2);
                    MergePanel.this.horizontalScroll3ChangedValue = value * (max3 - ext3) / (max2 - ext2);
                }
                MergePanel.this.horizontalScroll2ChangedValue = -1;
                scrollBarH1.setValue(MergePanel.this.horizontalScroll1ChangedValue);
                scrollBarH3.setValue(MergePanel.this.horizontalScroll3ChangedValue);
            }
        });
        scrollBarH3.getModel().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = scrollBarH3.getValue();
                if (value == MergePanel.this.horizontalScroll3ChangedValue) {
                    return;
                }
                int max1 = scrollBarH1.getMaximum();
                int max2 = scrollBarH2.getMaximum();
                int max3 = scrollBarH3.getMaximum();
                int ext1 = scrollBarH1.getModel().getExtent();
                int ext2 = scrollBarH2.getModel().getExtent();
                int ext3 = scrollBarH3.getModel().getExtent();
                if (max3 == ext3) {
                    MergePanel.this.horizontalScroll1ChangedValue = 0;
                    MergePanel.this.horizontalScroll2ChangedValue = 0;
                } else {
                    MergePanel.this.horizontalScroll1ChangedValue = value * (max1 - ext1) / (max3 - ext3);
                    MergePanel.this.horizontalScroll2ChangedValue = value * (max2 - ext2) / (max3 - ext3);
                }
                MergePanel.this.horizontalScroll3ChangedValue = -1;
                scrollBarH1.setValue(MergePanel.this.horizontalScroll1ChangedValue);
                scrollBarH2.setValue(MergePanel.this.horizontalScroll2ChangedValue);
            }
        });
        this.diffSplitPane.setDividerLocation(0.5);
        this.mergeSplitPane.setDividerLocation(0.5);
    }

    private String strCharacters(char c, int num) {
        StringBuffer s = new StringBuffer();
        while (num-- > 0) {
            s.append(c);
        }
        return s.toString();
    }

    private void customizeEditor(JEditorPane editor) {
        EditorKit kit = editor.getEditorKit();
        Document document = editor.getDocument();
        try {
            StyledDocument doc = (StyledDocument)editor.getDocument();
        }
        catch (ClassCastException e) {
            DefaultStyledDocument doc = new DefaultStyledDocument();
            try {
                doc.insertString(0, document.getText(0, document.getLength()), null);
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
            editor.setDocument(doc);
        }
    }

    private void addChangeListeners() {
        this.jEditorPane1.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        MergePanel.this.initGlobalSizes();
                        MergePanel.this.linesComp1.changedAll();
                    }
                });
            }

        });
        this.jEditorPane2.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        MergePanel.this.initGlobalSizes();
                        MergePanel.this.linesComp2.changedAll();
                    }
                });
            }

        });
        this.jEditorPane3.addPropertyChangeListener("font", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        MergePanel.this.initGlobalSizes();
                        MergePanel.this.linesComp3.changedAll();
                    }
                });
            }

        });
    }

    public void setSource1(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane1.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane1.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane1);
        this.jEditorPane1.putClientProperty("code-folding-enable", false);
        this.jEditorPane1.setDocument(doc);
        this.jEditorPane1.setEditable(false);
        this.customizeEditor(this.jEditorPane1);
        this.linesComp1 = new LinesComponent(this.jEditorPane1);
        this.jScrollPane1.setRowHeaderView(this.linesComp1);
        this.jViewport1 = this.jScrollPane1.getViewport();
    }

    public void setSource2(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane2.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane2.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane2);
        this.jEditorPane2.putClientProperty("code-folding-enable", false);
        this.jEditorPane2.setDocument(doc);
        this.jEditorPane2.setEditable(false);
        this.customizeEditor(this.jEditorPane2);
        this.linesComp2 = new LinesComponent(this.jEditorPane2);
        this.jScrollPane2.setRowHeaderView(this.linesComp2);
        this.jViewport2 = this.jScrollPane2.getViewport();
        this.joinScrollBars();
    }

    public void setResultSource(Reader r) throws IOException {
        EditorKit kit = this.jEditorPane3.getEditorKit();
        if (kit == null) {
            throw new IOException("Missing Editor Kit");
        }
        Document doc = kit.createDefaultDocument();
        if (!(doc instanceof StyledDocument)) {
            doc = new DefaultStyledDocument(new StyleContext());
            kit = new StyledEditorKit();
            this.jEditorPane3.setEditorKit(kit);
        }
        try {
            kit.read(r, doc, 0);
        }
        catch (BadLocationException e) {
            throw new IOException("Can not locate the beginning of the document.");
        }
        finally {
            r.close();
        }
        kit.install(this.jEditorPane3);
        this.jEditorPane3.putClientProperty("code-folding-enable", false);
        this.jEditorPane3.setDocument(doc);
        this.jEditorPane3.setEditable(false);
        this.customizeEditor(this.jEditorPane3);
        this.linesComp3 = new LinesComponent(this.jEditorPane3);
        this.resultScrollPane.setRowHeaderView(this.linesComp3);
        this.resultLineNumbers = new int[1];
        this.assureResultLineNumbersLength(NbDocument.findLineNumber((StyledDocument)((StyledDocument)doc), (int)doc.getEndPosition().getOffset()) + 1);
        int i = 0;
        while (i < this.resultLineNumbers.length) {
            this.resultLineNumbers[i] = i++;
        }
    }

    private void assureResultLineNumbersLength(int length) {
        if (length > this.resultLineNumbers.length) {
            int[] newrln = new int[length + 5];
            System.arraycopy(this.resultLineNumbers, 0, newrln, 0, this.resultLineNumbers.length);
            this.resultLineNumbers = newrln;
        }
    }

    public void copySource1ToResult(int line1, int line2, int line3) {
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane3.getDocument();
        try {
            this.copy(doc1, line1, line2, doc2, line3);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    public void copySource2ToResult(int line1, int line2, int line3) {
        StyledDocument doc1 = (StyledDocument)this.jEditorPane2.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane3.getDocument();
        try {
            this.copy(doc1, line1, line2, doc2, line3);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    private void copy(StyledDocument doc1, int line1, int line2, StyledDocument doc2, int line3) throws BadLocationException {
        int offset2;
        int offset1 = NbDocument.findLineOffset((StyledDocument)doc1, (int)(line1 - 1));
        int n = offset2 = line2 >= 0 ? NbDocument.findLineOffset((StyledDocument)doc1, (int)line2) : doc1.getLength() - 1;
        if (offset1 >= offset2) {
            return;
        }
        int offset3 = NbDocument.findLineOffset((StyledDocument)doc2, (int)(line3 - 1));
        int length = offset2 - offset1;
        if (line2 < 0) {
            ++length;
        }
        String text = doc1.getText(offset1, length);
        doc2.insertString(offset3, text, null);
        if (line2 < 0) {
            line2 = NbDocument.findLineNumber((StyledDocument)doc1, (int)doc1.getLength());
        }
        int numLines = line2 - line1 + 1;
        this.assureResultLineNumbersLength(line3 + numLines);
        if (this.resultLineNumbers[line3] == 0 && line3 > 0) {
            this.resultLineNumbers[line3] = this.resultLineNumbers[line3 - 1] + 1;
        }
        int resultLine = this.resultLineNumbers[line3];
        this.linesComp3.insertNumbers(line3 - 1, resultLine, numLines);
        this.linesComp3.changedAll();
        for (int i = 0; i < numLines; ++i) {
            this.resultLineNumbers[line3 + i] = resultLine + i;
        }
    }

    public void replaceSource1InResult(int line1, int line2, int line3, int line4) {
        Integer conflictLine = new Integer(line1 > 0 ? line1 : 1);
        if (this.resolvedLeftConflictsLineNumbers.contains(conflictLine)) {
            return;
        }
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane3.getDocument();
        try {
            this.replace(doc1, line1, line2, doc2, line3, line4);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        if (this.resolvedRightConflictsLineNumbers.contains(conflictLine)) {
            this.resolvedRightConflictsLineNumbers.remove(conflictLine);
        } else if (this.resolvedLeftRightConflictsLineNumbers.contains(conflictLine)) {
            this.resolvedLeftRightConflictsLineNumbers.remove(conflictLine);
        } else if (this.resolvedRightLeftConflictsLineNumbers.contains(conflictLine)) {
            this.resolvedRightLeftConflictsLineNumbers.remove(conflictLine);
        } else {
            --this.numUnresolvedConflicts;
            this.updateStatusLine();
        }
        this.resolvedLeftConflictsLineNumbers.add(conflictLine);
        this.updateAcceptButtons(line1);
    }

    public void replaceSource2InResult(int line1, int line2, int line3, int line4) {
        Integer conflictLine = new Integer(line1 > 0 ? line1 : 1);
        if (this.resolvedRightConflictsLineNumbers.contains(conflictLine)) {
            return;
        }
        StyledDocument doc1 = (StyledDocument)this.jEditorPane2.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane3.getDocument();
        try {
            this.replace(doc1, line1, line2, doc2, line3, line4);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        if (this.resolvedLeftConflictsLineNumbers.contains(conflictLine)) {
            this.resolvedLeftConflictsLineNumbers.remove(conflictLine);
        } else if (this.resolvedLeftRightConflictsLineNumbers.contains(conflictLine)) {
            this.resolvedLeftRightConflictsLineNumbers.remove(conflictLine);
        } else if (this.resolvedRightLeftConflictsLineNumbers.contains(conflictLine)) {
            this.resolvedRightLeftConflictsLineNumbers.remove(conflictLine);
        } else {
            --this.numUnresolvedConflicts;
            this.updateStatusLine();
        }
        this.resolvedRightConflictsLineNumbers.add(conflictLine);
        this.updateAcceptButtons(line1);
    }

    public void replaceBothInResult(int line1, int line2, int line3, int line4, int line5, int line6, boolean right) {
        Integer conflictLine1 = line1 > 0 ? line1 : 1;
        Integer conflictLine2 = line3 > 0 ? line3 : 1;
        StyledDocument doc1 = (StyledDocument)this.jEditorPane1.getDocument();
        StyledDocument doc2 = (StyledDocument)this.jEditorPane2.getDocument();
        StyledDocument target = (StyledDocument)this.jEditorPane3.getDocument();
        try {
            if (right) {
                this.replace(doc2, line3, line4, doc1, line1, line2, target, line5, line6);
            } else {
                this.replace(doc1, line1, line2, doc2, line3, line4, target, line5, line6);
            }
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
        if (this.resolvedLeftConflictsLineNumbers.contains(conflictLine1)) {
            this.resolvedLeftConflictsLineNumbers.remove(conflictLine1);
        } else if (this.resolvedRightConflictsLineNumbers.contains(conflictLine2)) {
            this.resolvedRightConflictsLineNumbers.remove(conflictLine2);
        } else if (right && this.resolvedLeftRightConflictsLineNumbers.contains(conflictLine1)) {
            this.resolvedLeftRightConflictsLineNumbers.remove(conflictLine1);
        } else if (!right && this.resolvedRightLeftConflictsLineNumbers.contains(conflictLine2)) {
            this.resolvedRightLeftConflictsLineNumbers.remove(conflictLine2);
        } else {
            --this.numUnresolvedConflicts;
            this.updateStatusLine();
        }
        if (right) {
            this.resolvedRightLeftConflictsLineNumbers.add(conflictLine2);
        } else {
            this.resolvedLeftRightConflictsLineNumbers.add(conflictLine1);
        }
        this.updateAcceptButtons(line1);
    }

    private void replace(StyledDocument doc1, int line1, int line2, StyledDocument doc2, int line3, int line4) throws BadLocationException {
        int offset1 = line1 > 0 ? NbDocument.findLineOffset((StyledDocument)doc1, (int)(line1 - 1)) : 0;
        int offset2 = line2 >= 0 ? NbDocument.findLineOffset((StyledDocument)doc1, (int)line2) : doc1.getLength() - 1;
        int offset3 = line3 > 0 ? NbDocument.findLineOffset((StyledDocument)doc2, (int)(line3 - 1)) : 0;
        int offset4 = line4 >= 0 ? NbDocument.findLineOffset((StyledDocument)doc2, (int)line4) : doc2.getLength() - 1;
        int length = offset2 - offset1;
        if (line2 < 0) {
            ++length;
        }
        String text = doc1.getText(offset1, length);
        doc2.remove(offset3, offset4 - offset3);
        doc2.insertString(offset3, text, null);
        this.assureResultLineNumbersLength(line4);
        int physicalLineDiff = line2 - line1 - (line4 - line3);
        if (physicalLineDiff > 0) {
            System.arraycopy(this.resultLineNumbers, line4 + 1, this.resultLineNumbers, line4 + physicalLineDiff + 1, this.resultLineNumbers.length - line4 - physicalLineDiff - 1);
        }
        int lineDiff = this.resultLineNumbers[line3] <= this.resultLineNumbers[line3 - 1] ? line2 - line1 + 1 : line2 - line1 - (line4 - line3);
        int n = this.resultLineNumbers[line3 - 1];
        for (int i = line3; i <= line4 + physicalLineDiff; ++i) {
            this.resultLineNumbers[i] = ++n;
        }
        this.linesComp3.insertNumbers(line3 - 1, this.resultLineNumbers[line3], line2 - line1 + 1);
        this.linesComp3.changedAll();
        if (physicalLineDiff < 0) {
            System.arraycopy(this.resultLineNumbers, line4 + 1, this.resultLineNumbers, line4 + physicalLineDiff + 1, this.resultLineNumbers.length - line4 - 1);
        }
        this.adjustLineNumbers(line4 + physicalLineDiff + 1, lineDiff);
        int line = -1;
        for (int i2 = 0; i2 < this.resultLineNumbers.length; ++i2) {
            if (this.resultLineNumbers[i2] < line) {
                this.resultLineNumbers[i2] = line;
            }
            line = this.resultLineNumbers[i2];
        }
    }

    private void replace(StyledDocument doc1, int line1, int line2, StyledDocument doc2, int line3, int line4, StyledDocument target, int targetStart, int targetEnd) throws BadLocationException {
        int offset1 = line1 > 0 ? NbDocument.findLineOffset((StyledDocument)doc1, (int)(line1 - 1)) : 0;
        int offset2 = line2 >= 0 ? NbDocument.findLineOffset((StyledDocument)doc1, (int)line2) : doc1.getLength() - 1;
        int offset3 = line3 > 0 ? NbDocument.findLineOffset((StyledDocument)doc2, (int)(line3 - 1)) : 0;
        int offset4 = line4 >= 0 ? NbDocument.findLineOffset((StyledDocument)doc2, (int)line4) : doc2.getLength() - 1;
        int offset5 = targetStart > 0 ? NbDocument.findLineOffset((StyledDocument)target, (int)(targetStart - 1)) : 0;
        int offset6 = targetEnd >= 0 ? NbDocument.findLineOffset((StyledDocument)target, (int)targetEnd) : target.getLength() - 1;
        int length = offset4 - offset3;
        if (line4 < 0) {
            ++length;
        }
        String text = doc2.getText(offset3, length);
        target.remove(offset5, offset6 - offset5);
        target.insertString(offset5, text, null);
        length = offset2 - offset1;
        if (line2 < 0) {
            ++length;
        }
        text = doc1.getText(offset1, length);
        target.insertString(offset5, text, null);
        this.assureResultLineNumbersLength(targetEnd);
        int physicalLineDiff = line2 - line1 + line4 - line3 + 1 - (targetEnd - targetStart);
        if (physicalLineDiff > 0) {
            System.arraycopy(this.resultLineNumbers, targetEnd + 1, this.resultLineNumbers, targetEnd + physicalLineDiff + 1, this.resultLineNumbers.length - targetEnd - physicalLineDiff - 1);
        }
        int lineDiff = this.resultLineNumbers[targetStart] <= this.resultLineNumbers[targetStart - 1] ? line2 - line1 + 1 + line4 - line3 + 1 : physicalLineDiff;
        int n = this.resultLineNumbers[targetStart - 1];
        for (int i = targetStart; i <= targetEnd + physicalLineDiff; ++i) {
            this.resultLineNumbers[i] = ++n;
        }
        this.linesComp3.insertNumbers(targetStart - 1, this.resultLineNumbers[targetStart], line2 - line1 + 1 + line4 - line3 + 1);
        this.linesComp3.changedAll();
        if (physicalLineDiff < 0) {
            System.arraycopy(this.resultLineNumbers, targetEnd + 1, this.resultLineNumbers, targetEnd + physicalLineDiff + 1, this.resultLineNumbers.length - targetEnd - 1);
        }
        this.adjustLineNumbers(targetEnd + physicalLineDiff + 1, lineDiff);
        int line = -1;
        for (int i2 = 0; i2 < this.resultLineNumbers.length; ++i2) {
            if (this.resultLineNumbers[i2] < line) {
                this.resultLineNumbers[i2] = line;
            }
            line = this.resultLineNumbers[i2];
        }
    }

    private void adjustLineNumbers(int startLine, int shift) {
        int end;
        for (end = this.resultLineNumbers.length; end > 0 && this.resultLineNumbers[end - 1] == 0; --end) {
        }
        int startSetLine = -1;
        int endSetLine = -1;
        for (int i = startLine; i < end; ++i) {
            int[] arrn = this.resultLineNumbers;
            int n = i;
            arrn[n] = arrn[n] + shift;
            if (this.resultLineNumbers[i] <= this.resultLineNumbers[i - 1]) {
                if (startSetLine > 0) {
                    this.linesComp3.insertNumbers(startSetLine - 1, this.resultLineNumbers[startSetLine], i - startSetLine);
                    this.linesComp3.changedAll();
                    startSetLine = -1;
                }
                if (endSetLine >= 0) continue;
                endSetLine = i;
                continue;
            }
            if (endSetLine > 0) {
                this.linesComp3.removeNumbers(endSetLine - 1, i - endSetLine);
                this.linesComp3.changedAll();
                endSetLine = -1;
            }
            if (startSetLine >= 0) continue;
            startSetLine = i;
        }
        if (startSetLine > 0) {
            this.linesComp3.insertNumbers(startSetLine - 1, this.resultLineNumbers[startSetLine], end - startSetLine);
            this.linesComp3.shrink(end - 1);
            this.linesComp3.changedAll();
        }
        if (endSetLine > 0) {
            this.linesComp3.removeNumbers(endSetLine - 1, end - endSetLine);
            this.linesComp3.shrink(end - 1);
            this.linesComp3.changedAll();
        }
    }

    public void setSource1Title(String title) {
        this.fileLabel1.setText(title);
    }

    public void setSource2Title(String title) {
        this.fileLabel2.setText(title);
    }

    public void setResultSourceTitle(String title) {
        this.resultLabel.setText(title);
    }

    public void setStatusLabel(String status) {
        this.statusLabel.setText(status);
    }

    public void setMimeType1(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane1.setEditorKit(kit);
    }

    public void setMimeType2(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane2.setEditorKit(kit);
    }

    public void setMimeType3(String mime) {
        EditorKit kit = CloneableEditorSupport.getEditorKit((String)mime);
        this.jEditorPane3.setEditorKit(kit);
    }

    public void setResultDocument(Document doc) {
        if (doc != null) {
            this.jEditorPane3.setDocument(doc);
            this.jEditorPane3.setEditable(false);
            this.linesComp3 = new LinesComponent(this.jEditorPane3);
            this.resultScrollPane.setRowHeaderView(this.linesComp3);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void writeResult(Writer w, boolean stripLastNewline) throws IOException {
        block13 : {
            int end;
            for (end = this.resultLineNumbers.length; end > 0 && this.resultLineNumbers[end - 1] == 0; --end) {
            }
            int startSetLine = -1;
            StyledDocument doc = (StyledDocument)this.jEditorPane3.getDocument();
            try {
                for (int i = 1; i < end; ++i) {
                    if (this.resultLineNumbers[i] <= this.resultLineNumbers[i - 1]) {
                        if (startSetLine <= 0) continue;
                        try {
                            int offsetStart = NbDocument.findLineOffset((StyledDocument)doc, (int)(startSetLine - 1));
                            int offsetEnd = NbDocument.findLineOffset((StyledDocument)doc, (int)(i - 1));
                            try {
                                this.writeText(w, doc.getText(offsetStart, offsetEnd - offsetStart));
                            }
                            catch (BadLocationException blex) {
                                throw new IOException(blex.getLocalizedMessage());
                            }
                            startSetLine = -1;
                        }
                        catch (IndexOutOfBoundsException ex) {
                            Logger.getLogger(MergePanel.class.getName()).log(Level.SEVERE, "Invalid position " + startSetLine + "[" + i + "]: " + Arrays.toString(this.resultLineNumbers), ex);
                        }
                        continue;
                    }
                    if (startSetLine >= 0) continue;
                    startSetLine = i;
                }
                if (startSetLine <= 0) break block13;
                int offsetStart = NbDocument.findLineOffset((StyledDocument)doc, (int)(startSetLine - 1));
                int offsetEnd = doc.getLength();
                try {
                    String text = doc.getText(offsetStart, offsetEnd - offsetStart);
                    if (stripLastNewline && text.endsWith("\n")) {
                        text = text.substring(0, text.length() - 1);
                    }
                    this.writeText(w, text);
                }
                catch (BadLocationException blex) {
                    throw new IOException(blex.getLocalizedMessage());
                }
            }
            finally {
                w.close();
            }
        }
    }

    private void writeText(Writer w, String text) throws IOException {
        text = text.replaceAll("\n", System.getProperty("line.separator"));
        w.write(text);
    }

    public void highlightRegion1(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane1.getDocument();
        ((MergePane)this.jEditorPane1).addHighlight(doc, line1, line2, color);
    }

    public void highlightRegion2(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane2.getDocument();
        ((MergePane)this.jEditorPane2).addHighlight(doc, line1, line2, color);
    }

    public void highlightRegion3(int line1, int line2, Color color) {
        StyledDocument doc = (StyledDocument)this.jEditorPane3.getDocument();
        ((MergePane)this.jEditorPane3).addHighlight(doc, line1, line2, color);
    }

    public void unhighlightRegion3(int line1, int line2) {
        StyledDocument doc = (StyledDocument)this.jEditorPane3.getDocument();
        ((MergePane)this.jEditorPane3).removeHighlight(doc, line1, line2);
    }

    private void addEmptyLines(StyledDocument doc, int line, int numLines) {
        int lastOffset = doc.getEndPosition().getOffset();
        int totLines = NbDocument.findLineNumber((StyledDocument)doc, (int)lastOffset);
        int offset = lastOffset;
        if (line <= totLines) {
            offset = NbDocument.findLineOffset((StyledDocument)doc, (int)line);
        } else {
            offset = lastOffset - 1;
            Logger logger = Logger.getLogger(MergePanel.class.getName());
            logger.log(Level.WARNING, "line({0}) > totLines({1}): final offset({2})", new Object[]{line, totLines, offset});
            logger.log(Level.INFO, null, new Exception());
        }
        String insStr = this.strCharacters('\n', numLines);
        try {
            doc.insertString(offset, insStr, null);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    public void addEmptyLines1(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane1.getDocument();
        this.addEmptyLines(doc, line, numLines);
        this.linesComp1.addEmptyLines(line, numLines);
    }

    public void addEmptyLines2(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane2.getDocument();
        this.addEmptyLines(doc, line, numLines);
        this.linesComp2.addEmptyLines(line, numLines);
    }

    public void addEmptyLines3(int line, int numLines) {
        StyledDocument doc = (StyledDocument)this.jEditorPane3.getDocument();
        this.addEmptyLines(doc, line - 1, numLines);
        this.linesComp3.addEmptyLines(line - 1, numLines);
        this.assureResultLineNumbersLength(line + numLines);
        if (this.resultLineNumbers[line] == 0 && line > 0) {
            this.resultLineNumbers[line] = this.resultLineNumbers[line - 1];
        }
        int resultLine = this.resultLineNumbers[line];
        for (int i = 1; i < numLines; ++i) {
            this.resultLineNumbers[line + i] = resultLine;
        }
    }

}

