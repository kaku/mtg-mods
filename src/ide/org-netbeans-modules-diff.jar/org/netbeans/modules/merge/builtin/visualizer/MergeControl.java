/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.merge.builtin.visualizer;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.merge.builtin.visualizer.MergePanel;
import org.openide.ErrorManager;
import org.openide.util.NbBundle;

public class MergeControl
implements ActionListener,
VetoableChangeListener {
    private Color colorUnresolvedConflict;
    private Color colorResolvedConflict;
    private Color colorOtherConflict;
    private MergePanel panel;
    private Difference[] diffs;
    private int[][] diffShifts;
    private int currentDiffLine = 0;
    private int[] resultDiffLocations;
    private final Map<Difference, AcceptKind> resolvedConflicts = new HashMap<Difference, AcceptKind>();
    private StreamSource resultSource;
    private boolean firstNewlineIsFake;
    private boolean secondNewlineIsFake;

    public MergeControl(MergePanel panel) {
        this.panel = panel;
    }

    public void initialize(Difference[] diffs, StreamSource source1, StreamSource source2, StreamSource result, Color colorUnresolvedConflict, Color colorResolvedConflict, Color colorOtherConflict) {
        if (diffs.length > 0) {
            Difference ld = diffs[diffs.length - 1];
            if (!ld.getFirstText().endsWith("\n")) {
                this.firstNewlineIsFake = true;
            }
            if (!ld.getSecondText().endsWith("\n")) {
                this.secondNewlineIsFake = true;
            }
            if (this.firstNewlineIsFake || this.secondNewlineIsFake) {
                diffs[diffs.length - 1] = new Difference(ld.getType(), ld.getFirstStart(), ld.getFirstEnd(), ld.getSecondStart(), ld.getSecondEnd(), ld.getFirstText() + (this.firstNewlineIsFake ? "\n" : ""), ld.getSecondText() + (this.secondNewlineIsFake ? "\n" : ""));
            }
        }
        this.diffs = diffs;
        this.diffShifts = new int[diffs.length][2];
        this.resultDiffLocations = new int[diffs.length];
        for (Difference diff : diffs) {
            this.resolvedConflicts.put(diff, AcceptKind.NONE);
        }
        this.panel.setMimeType1(source1.getMIMEType());
        this.panel.setMimeType2(source2.getMIMEType());
        this.panel.setMimeType3(result.getMIMEType());
        this.panel.setSource1Title(source1.getTitle());
        this.panel.setSource2Title(source2.getTitle());
        this.panel.setResultSourceTitle(result.getTitle());
        this.panel.setName(source1.getName());
        try {
            this.panel.setSource1(source1.createReader());
            this.panel.setSource2(source2.createReader());
            this.panel.setResultSource(new StringReader(""));
        }
        catch (IOException ioex) {
            ErrorManager.getDefault().notify((Throwable)ioex);
        }
        this.colorUnresolvedConflict = colorUnresolvedConflict;
        this.colorResolvedConflict = colorResolvedConflict;
        this.colorOtherConflict = colorOtherConflict;
        this.insertEmptyLines(true);
        this.setDiffHighlight(true);
        this.copyToResult();
        this.panel.setConflicts(diffs);
        this.panel.addControlActionListener(this);
        this.showCurrentLine();
        this.resultSource = result;
    }

    private void insertEmptyLines(boolean updateActionLines) {
        int n = this.diffs.length;
        block5 : for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            if (updateActionLines && i < n - 1) {
                this.diffShifts[i + 1][0] = this.diffShifts[i][0];
                this.diffShifts[i + 1][1] = this.diffShifts[i][1];
            }
            switch (action.getType()) {
                case 0: {
                    this.panel.addEmptyLines2(n3, n2 - n1 + 1);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[1] = arrn[1] + (n2 - n1 + 1);
                    continue block5;
                }
                case 1: {
                    this.panel.addEmptyLines1(n1, n4 - n3 + 1);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[0] = arrn[0] + (n4 - n3 + 1);
                    continue block5;
                }
                case 2: {
                    int r1 = n2 - n1;
                    int r2 = n4 - n3;
                    if (r1 < r2) {
                        this.panel.addEmptyLines1(n2, r2 - r1);
                        if (!updateActionLines || i >= n - 1) continue block5;
                        int[] arrn = this.diffShifts[i + 1];
                        arrn[0] = arrn[0] + (r2 - r1);
                        continue block5;
                    }
                    if (r1 <= r2) continue block5;
                    this.panel.addEmptyLines2(n4, r1 - r2);
                    if (!updateActionLines || i >= n - 1) continue block5;
                    int[] arrn = this.diffShifts[i + 1];
                    arrn[1] = arrn[1] + (r1 - r2);
                }
            }
        }
    }

    private void setDiffHighlight(boolean set) {
        int n = this.diffs.length;
        block5 : for (int i = 0; i < n; ++i) {
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            switch (action.getType()) {
                case 0: {
                    if (set) {
                        this.panel.highlightRegion1(n1, n2, this.colorUnresolvedConflict);
                        continue block5;
                    }
                    this.panel.highlightRegion1(n1, n2, Color.white);
                    continue block5;
                }
                case 1: {
                    if (set) {
                        this.panel.highlightRegion2(n3, n4, this.colorUnresolvedConflict);
                        continue block5;
                    }
                    this.panel.highlightRegion2(n3, n4, Color.white);
                    continue block5;
                }
                case 2: {
                    if (set) {
                        this.panel.highlightRegion1(n1, n2, this.colorUnresolvedConflict);
                        this.panel.highlightRegion2(n3, n4, this.colorUnresolvedConflict);
                        continue block5;
                    }
                    this.panel.highlightRegion1(n1, n2, Color.white);
                    this.panel.highlightRegion2(n3, n4, Color.white);
                }
            }
        }
    }

    private void copyToResult() {
        int n = this.diffs.length;
        int line1 = 1;
        int line3 = 1;
        for (int i = 0; i < n; ++i) {
            int endcopy;
            Difference action = this.diffs[i];
            int n1 = action.getFirstStart() + this.diffShifts[i][0];
            int n2 = action.getFirstEnd() + this.diffShifts[i][0];
            int n3 = action.getSecondStart() + this.diffShifts[i][1];
            int n4 = action.getSecondEnd() + this.diffShifts[i][1];
            int n5 = endcopy = action.getType() != 1 ? n1 - 1 : n1;
            if (endcopy >= line1) {
                this.panel.copySource1ToResult(line1, endcopy, line3);
                line3 += endcopy + 1 - line1;
            }
            int length = Math.max(n2 - n1, 0) + Math.max(n4 - n3, 0);
            this.panel.addEmptyLines3(line3, length + 1);
            this.panel.highlightRegion3(line3, line3 + length, this.colorUnresolvedConflict);
            this.resultDiffLocations[i] = line3;
            line3 += length + 1;
            line1 = Math.max(n2, n4) + 1;
        }
        this.panel.copySource1ToResult(line1, -1, line3);
    }

    private void showCurrentLine() {
        Difference diff = this.diffs[this.currentDiffLine];
        int line = diff.getFirstStart() + this.diffShifts[this.currentDiffLine][0];
        if (diff.getType() == 1) {
            ++line;
        }
        int lf1 = diff.getFirstEnd() - diff.getFirstStart() + 1;
        int lf2 = diff.getSecondEnd() - diff.getSecondStart() + 1;
        int length = Math.max(lf1, lf2);
        this.panel.setCurrentLine(line, length, this.currentDiffLine, this.resultDiffLocations[this.currentDiffLine]);
    }

    private void doResolveConflict(boolean right, int conflNum) {
        int shift;
        int line3;
        int line4;
        int line2;
        int line1;
        Difference diff = this.diffs[conflNum];
        int[] shifts = this.diffShifts[conflNum];
        if (diff.getType() == 1) {
            line1 = diff.getFirstStart() + shifts[0] + 1;
            line2 = line1 - 1;
        } else {
            line1 = diff.getFirstStart() + shifts[0];
            line2 = diff.getFirstEnd() + shifts[0];
        }
        if (diff.getType() == 0) {
            line3 = diff.getSecondStart() + shifts[1] + 1;
            line4 = line3 - 1;
        } else {
            line3 = diff.getSecondStart() + shifts[1];
            line4 = diff.getSecondEnd() + shifts[1];
        }
        int rlength = 0;
        AcceptKind acceptedAs = this.resolvedConflicts.get(diff);
        switch (acceptedAs) {
            case NONE: {
                rlength = Math.max(line2 - line1, 0) + Math.max(line4 - line3, 0);
                break;
            }
            case LEFT: {
                rlength = line2 - line1;
                break;
            }
            case RIGHT: {
                rlength = line4 - line3;
                break;
            }
            case LEFT_RIGHT: 
            case RIGHT_LEFT: {
                rlength = line2 - line1 + line4 - line3 + 1;
            }
        }
        if (right) {
            this.panel.replaceSource2InResult(line3, Math.max(line4, 0), this.resultDiffLocations[conflNum], this.resultDiffLocations[conflNum] + rlength);
            shift = rlength - (line4 - line3);
            this.panel.highlightRegion1(line1, Math.max(line2, 0), this.colorOtherConflict);
            this.panel.highlightRegion2(line3, Math.max(line4, 0), this.colorResolvedConflict);
        } else {
            this.panel.replaceSource1InResult(line1, Math.max(line2, 0), this.resultDiffLocations[conflNum], this.resultDiffLocations[conflNum] + rlength);
            shift = rlength - (line2 - line1);
            this.panel.highlightRegion1(line1, Math.max(line2, 0), this.colorResolvedConflict);
            this.panel.highlightRegion2(line3, Math.max(line4, 0), this.colorOtherConflict);
        }
        if (right && line4 >= line3 || !right && line2 >= line1) {
            this.panel.highlightRegion3(this.resultDiffLocations[conflNum], this.resultDiffLocations[conflNum] + rlength - shift, this.colorResolvedConflict);
        } else {
            this.panel.unhighlightRegion3(this.resultDiffLocations[conflNum], this.resultDiffLocations[conflNum]);
        }
        int i = conflNum + 1;
        while (i < this.diffs.length) {
            int[] arrn = this.resultDiffLocations;
            int n = i++;
            arrn[n] = arrn[n] - shift;
        }
        this.resolvedConflicts.put(diff, right ? AcceptKind.RIGHT : AcceptKind.LEFT);
        this.panel.setNeedsSaveState(true);
    }

    private void doAcceptBoth(boolean right, int conflNum) {
        int line3;
        int line4;
        int line2;
        int line1;
        Difference diff = this.diffs[conflNum];
        int[] shifts = this.diffShifts[conflNum];
        if (diff.getType() == 1) {
            line1 = diff.getFirstStart() + shifts[0] + 1;
            line2 = line1 - 1;
        } else {
            line1 = diff.getFirstStart() + shifts[0];
            line2 = diff.getFirstEnd() + shifts[0];
        }
        if (diff.getType() == 0) {
            line3 = diff.getSecondStart() + shifts[1] + 1;
            line4 = line3 - 1;
        } else {
            line3 = diff.getSecondStart() + shifts[1];
            line4 = diff.getSecondEnd() + shifts[1];
        }
        int rlength = 0;
        AcceptKind acceptedAs = this.resolvedConflicts.get(diff);
        switch (acceptedAs) {
            case NONE: {
                rlength = Math.max(line2 - line1, 0) + Math.max(line4 - line3, 0);
                break;
            }
            case LEFT: {
                rlength = line2 - line1;
                break;
            }
            case RIGHT: {
                rlength = line4 - line3;
                break;
            }
            case LEFT_RIGHT: 
            case RIGHT_LEFT: {
                rlength = line2 - line1 + line4 - line3 + 1;
            }
        }
        this.panel.replaceBothInResult(line1, Math.max(line2, 0), line3, Math.max(line4, 0), this.resultDiffLocations[conflNum], this.resultDiffLocations[conflNum] + rlength, right);
        int shift = rlength - (line2 - line1 + line4 - line3 + 1);
        this.panel.highlightRegion1(line1, Math.max(line2, 0), this.colorResolvedConflict);
        this.panel.highlightRegion2(line3, Math.max(line4, 0), this.colorResolvedConflict);
        this.panel.highlightRegion3(this.resultDiffLocations[conflNum], this.resultDiffLocations[conflNum] + rlength - shift, this.colorResolvedConflict);
        int i = conflNum + 1;
        while (i < this.diffs.length) {
            int[] arrn = this.resultDiffLocations;
            int n = i++;
            arrn[n] = arrn[n] - shift;
        }
        this.resolvedConflicts.put(diff, right ? AcceptKind.RIGHT_LEFT : AcceptKind.LEFT_RIGHT);
        this.panel.setNeedsSaveState(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String actionCommand = actionEvent.getActionCommand();
        if ("firstConflict".equals(actionCommand)) {
            this.currentDiffLine = 0;
            this.showCurrentLine();
        } else if ("lastConflict".equals(actionCommand)) {
            this.currentDiffLine = this.diffs.length - 1;
            this.showCurrentLine();
        } else if ("previousConflict".equals(actionCommand)) {
            --this.currentDiffLine;
            if (this.currentDiffLine < 0) {
                this.currentDiffLine = this.diffs.length - 1;
            }
            this.showCurrentLine();
        } else if ("nextConflict".equals(actionCommand)) {
            ++this.currentDiffLine;
            if (this.currentDiffLine >= this.diffs.length) {
                this.currentDiffLine = 0;
            }
            this.showCurrentLine();
        } else if ("acceptRight".equals(actionCommand)) {
            this.doResolveConflict(true, this.currentDiffLine);
        } else if ("acceptLeft".equals(actionCommand)) {
            this.doResolveConflict(false, this.currentDiffLine);
        } else if ("acceptRightLeft".equals(actionCommand)) {
            this.doAcceptBoth(true, this.currentDiffLine);
        } else if ("acceptLeftRight".equals(actionCommand)) {
            this.doAcceptBoth(false, this.currentDiffLine);
        }
    }

    @Override
    public void vetoableChange(PropertyChangeEvent propertyChangeEvent) throws PropertyVetoException {
        MergePanel panel;
        if ("panelSave".equals(propertyChangeEvent.getPropertyName()) && this.panel == (panel = (MergePanel)propertyChangeEvent.getNewValue())) {
            ArrayList<Difference> unresolvedConflicts = new ArrayList<Difference>();
            int diffLocationShift = 0;
            for (int i = 0; i < this.diffs.length; ++i) {
                if (this.resolvedConflicts.get(this.diffs[i]) != AcceptKind.NONE) continue;
                int diffLocation = this.resultDiffLocations[i] - diffLocationShift;
                Difference conflict = new Difference(this.diffs[i].getType(), diffLocation, diffLocation + this.diffs[i].getFirstEnd() - this.diffs[i].getFirstStart(), diffLocation, diffLocation + this.diffs[i].getSecondEnd() - this.diffs[i].getSecondStart(), this.diffs[i].getFirstText(), this.diffs[i].getSecondText());
                unresolvedConflicts.add(conflict);
                diffLocationShift += Math.max(this.diffs[i].getFirstEnd() - this.diffs[i].getFirstStart() + 1, this.diffs[i].getSecondEnd() - this.diffs[i].getSecondStart() + 1);
            }
            try {
                panel.writeResult(this.resultSource.createWriter(unresolvedConflicts.toArray(new Difference[unresolvedConflicts.size()])), this.firstNewlineIsFake | this.secondNewlineIsFake);
                panel.setNeedsSaveState(false);
            }
            catch (IOException ioex) {
                PropertyVetoException pvex = new PropertyVetoException(NbBundle.getMessage(MergeControl.class, (String)"MergeControl.failedToSave", (Object)ioex.getLocalizedMessage()), propertyChangeEvent);
                pvex.initCause(ioex);
                throw pvex;
            }
        }
        if ("panelClosing".equals(propertyChangeEvent.getPropertyName()) && this.panel == (panel = (MergePanel)propertyChangeEvent.getNewValue())) {
            this.resultSource.close();
        }
        if ("allPanelsClosed".equals(propertyChangeEvent.getPropertyName()) || "allPanelsCancelled".equals(propertyChangeEvent.getPropertyName())) {
            this.resultSource.close();
        }
    }

    static enum AcceptKind {
        LEFT,
        RIGHT,
        LEFT_RIGHT,
        RIGHT_LEFT,
        NONE;
        

        private AcceptKind() {
        }
    }

}

