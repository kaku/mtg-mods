/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeEvent
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeListener
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.merge.builtin.visualizer;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.diff.builtin.visualizer.DEditorPane;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.openide.text.NbDocument;

public final class MergePane
extends DEditorPane
implements HighlightsContainer,
DocumentListener {
    private final List<HighLight> highlights = new LinkedList<HighLight>();
    private final List<HighlightsChangeListener> listeners = new ArrayList<HighlightsChangeListener>(1);

    public HighlightsSequence getHighlights(int start, int end) {
        return new MergeHighlightsSequence(start, end, this.getHighlights());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addHighlightsChangeListener(HighlightsChangeListener listener) {
        List<HighlightsChangeListener> list = this.listeners;
        synchronized (list) {
            this.listeners.add(listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeHighlightsChangeListener(HighlightsChangeListener listener) {
        List<HighlightsChangeListener> list = this.listeners;
        synchronized (list) {
            this.listeners.remove((Object)listener);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        int offset = e.getOffset();
        int length = e.getLength();
        this.moveRegions(offset, length);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        int offset = e.getOffset();
        int length = e.getLength();
        this.moveRegions(offset, - length);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void addHighlight(StyledDocument doc, int line1, int line2, Color color) {
        if (line1 > 0) {
            --line1;
        }
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setBackground(attrs, color);
        attrs.addAttribute("org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", Boolean.TRUE);
        int startOffset = MergePane.getRowStartFromLineOffset(doc, line1);
        int endOffset = MergePane.getRowStartFromLineOffset(doc, line2);
        List<HighLight> list = this.highlights;
        synchronized (list) {
            ListIterator<HighLight> it = this.highlights.listIterator();
            HighLight toAdd = new HighLight(startOffset, endOffset, attrs);
            while (it.hasNext()) {
                HighLight highlight = it.next();
                if (highlight.contains(startOffset)) {
                    it.remove();
                    break;
                }
                if (!highlight.precedes(startOffset)) continue;
                it.previous();
                break;
            }
            it.add(toAdd);
        }
        this.fireHilitingChanged();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeHighlight(StyledDocument doc, int line1, int line2) {
        if (line1 > 0) {
            --line1;
        }
        int startOffset = MergePane.getRowStartFromLineOffset(doc, line1);
        List<HighLight> list = this.highlights;
        synchronized (list) {
            ListIterator<HighLight> it = this.highlights.listIterator();
            while (it.hasNext()) {
                HighLight highlight = it.next();
                if (!highlight.contains(startOffset)) continue;
                it.remove();
                break;
            }
        }
        this.fireHilitingChanged();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private HighLight[] getHighlights() {
        List<HighLight> list = this.highlights;
        synchronized (list) {
            return this.highlights.toArray(new HighLight[this.highlights.size()]);
        }
    }

    static int getRowStartFromLineOffset(Document doc, int lineIndex) {
        int offset;
        if (doc instanceof StyledDocument) {
            offset = NbDocument.findLineOffset((StyledDocument)((StyledDocument)doc), (int)lineIndex);
        } else {
            Element element = doc.getDefaultRootElement();
            Element line = element.getElement(lineIndex);
            offset = line.getStartOffset();
        }
        return offset;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fireHilitingChanged() {
        List<HighlightsChangeListener> list = this.listeners;
        synchronized (list) {
            for (HighlightsChangeListener listener : this.listeners) {
                listener.highlightChanged(new HighlightsChangeEvent((HighlightsContainer)this, 0, Integer.MAX_VALUE));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void moveRegions(int offset, int displacement) {
        List<HighLight> list = this.highlights;
        synchronized (list) {
            ListIterator<HighLight> it = this.highlights.listIterator();
            while (it.hasNext()) {
                HighLight highlight = it.next();
                if (highlight.startOffset <= offset) continue;
                highlight.move(displacement);
            }
        }
        this.fireHilitingChanged();
    }

    private static class MergeHighlightsSequence
    implements HighlightsSequence {
        private final int endOffset;
        private final int startOffset;
        private int currentHiliteIndex = -1;
        private HighLight[] highlights;

        public MergeHighlightsSequence(int start, int end, HighLight[] allHilites) {
            this.startOffset = start;
            this.endOffset = end;
            LinkedList<HighLight> list = new LinkedList<HighLight>();
            for (HighLight hilite : allHilites) {
                if (hilite.getEndOffset() < this.startOffset) continue;
                if (hilite.getStartOffset() > this.endOffset) break;
                list.add(hilite);
            }
            this.highlights = list.toArray(new HighLight[list.size()]);
        }

        public boolean moveNext() {
            if (this.currentHiliteIndex >= this.highlights.length - 1) {
                return false;
            }
            ++this.currentHiliteIndex;
            return true;
        }

        public int getStartOffset() {
            return Math.max(this.highlights[this.currentHiliteIndex].getStartOffset(), this.startOffset);
        }

        public int getEndOffset() {
            return Math.min(this.highlights[this.currentHiliteIndex].getEndOffset(), this.endOffset);
        }

        public AttributeSet getAttributes() {
            return this.highlights[this.currentHiliteIndex].getAttrs();
        }
    }

    private static class HighLight {
        private int startOffset;
        private int endOffset;
        private final AttributeSet attrs;

        public HighLight(int startOffset, int endOffset, AttributeSet attrs) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.attrs = attrs;
        }

        public int getStartOffset() {
            return this.startOffset;
        }

        public int getEndOffset() {
            return this.endOffset;
        }

        public AttributeSet getAttrs() {
            return this.attrs;
        }

        private boolean contains(int position) {
            return this.startOffset <= position && this.endOffset >= position;
        }

        private boolean precedes(int position) {
            return this.startOffset > position;
        }

        private void move(int displacement) {
            this.startOffset += displacement;
            this.endOffset += displacement;
        }
    }

}

