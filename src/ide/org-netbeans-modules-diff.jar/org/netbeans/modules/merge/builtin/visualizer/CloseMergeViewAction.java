/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.cookies.CloseCookie
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CookieAction
 */
package org.netbeans.modules.merge.builtin.visualizer;

import org.openide.cookies.CloseCookie;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;

public class CloseMergeViewAction
extends CookieAction {
    private static final long serialVersionUID = 2746214508313015932L;

    protected Class[] cookieClasses() {
        return new Class[]{CloseCookie.class};
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(CloseMergeViewAction.class);
    }

    public String getName() {
        return NbBundle.getMessage(CloseMergeViewAction.class, (String)"CloseAction");
    }

    protected int mode() {
        return 8;
    }

    protected boolean asynchronous() {
        return false;
    }

    protected void performAction(Node[] node) {
        if (node.length == 0) {
            return;
        }
        CloseCookie cc = (CloseCookie)node[0].getCookie(CloseCookie.class);
        if (cc != null) {
            cc.close();
        }
    }
}

