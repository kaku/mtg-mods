/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.merge.builtin.visualizer;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.merge.builtin.visualizer.GraphicalMergeVisualizer;
import org.openide.ErrorManager;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class GraphicalMergeVisualizerBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(GraphicalMergeVisualizer.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] desc;
        try {
            PropertyDescriptor colorUnresolvedConflict = new PropertyDescriptor("colorUnresolvedConflict", GraphicalMergeVisualizer.class);
            colorUnresolvedConflict.setDisplayName(NbBundle.getMessage(GraphicalMergeVisualizerBeanInfo.class, (String)"PROP_colorUnresolvedConflict"));
            colorUnresolvedConflict.setShortDescription(NbBundle.getMessage(GraphicalMergeVisualizerBeanInfo.class, (String)"HINT_colorUnresolvedConflict"));
            PropertyDescriptor colorResolvedConflict = new PropertyDescriptor("colorResolvedConflict", GraphicalMergeVisualizer.class);
            colorResolvedConflict.setDisplayName(NbBundle.getMessage(GraphicalMergeVisualizerBeanInfo.class, (String)"PROP_colorResolvedConflict"));
            colorResolvedConflict.setShortDescription(NbBundle.getMessage(GraphicalMergeVisualizerBeanInfo.class, (String)"HINT_colorResolvedConflict"));
            PropertyDescriptor colorOtherConflict = new PropertyDescriptor("colorOtherConflict", GraphicalMergeVisualizer.class);
            colorOtherConflict.setDisplayName(NbBundle.getMessage(GraphicalMergeVisualizerBeanInfo.class, (String)"PROP_colorOtherConflict"));
            colorOtherConflict.setShortDescription(NbBundle.getMessage(GraphicalMergeVisualizerBeanInfo.class, (String)"HINT_colorOtherConflict"));
            desc = new PropertyDescriptor[]{colorUnresolvedConflict, colorResolvedConflict, colorOtherConflict};
        }
        catch (IntrospectionException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
            desc = null;
        }
        return desc;
    }

    @Override
    public int getDefaultPropertyIndex() {
        return 0;
    }

    @Override
    public Image getIcon(int iconKind) {
        switch (iconKind) {
            case 1: {
                return ImageUtilities.loadImage((String)"org/netbeans/modules/merge/builtin/visualizer/mergeModeIcon.gif", (boolean)true);
            }
        }
        return null;
    }
}

