/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 */
package org.netbeans.modules.merge.builtin.visualizer;

import javax.swing.text.JTextComponent;
import org.netbeans.modules.merge.builtin.visualizer.MergePane;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

public class MergeHighlightsLayerFactory
implements HighlightsLayerFactory {
    static final String HIGHLITING_LAYER_ID = "org.netbeans.modules.merge.builtin.visualizer.MergePanel";

    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        MergePane master = (MergePane)context.getComponent().getClientProperty("org.netbeans.modules.merge.builtin.visualizer.MergePanel");
        if (master == null) {
            return null;
        }
        HighlightsLayer[] layers = new HighlightsLayer[]{HighlightsLayer.create((String)"org.netbeans.modules.merge.builtin.visualizer.MergePanel", (ZOrder)ZOrder.DEFAULT_RACK, (boolean)true, (HighlightsContainer)master)};
        return layers;
    }
}

