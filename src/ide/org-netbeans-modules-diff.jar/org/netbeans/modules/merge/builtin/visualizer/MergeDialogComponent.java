/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.actions.FileSystemAction
 *  org.openide.awt.JPopupMenuPlus
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.UserQuestionException
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 *  org.openide.windows.Workspace
 */
package org.netbeans.modules.merge.builtin.visualizer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TabbedPaneUI;
import org.netbeans.modules.merge.builtin.visualizer.MergePanel;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.actions.FileSystemAction;
import org.openide.awt.JPopupMenuPlus;
import org.openide.awt.Mnemonics;
import org.openide.awt.MouseUtils;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.SaveCookie;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.UserQuestionException;
import org.openide.util.WeakListeners;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.openide.windows.Workspace;

public class MergeDialogComponent
extends TopComponent
implements ChangeListener {
    public static final String PROP_PANEL_CLOSING = "panelClosing";
    public static final String PROP_ALL_CLOSED = "allPanelsClosed";
    public static final String PROP_ALL_CANCELLED = "allPanelsCancelled";
    public static final String PROP_PANEL_SAVE = "panelSave";
    private Map<MergePanel, MergeNode> nodesForPanels = new HashMap<MergePanel, MergeNode>();
    private boolean internallyClosing;
    private JPanel buttonsPanel;
    private JButton cancelButton;
    private JButton helpButton;
    private JTabbedPane mergeTabbedPane;
    private JButton okButton;

    public MergeDialogComponent() {
        this.initComponents();
        this.initListeners();
        this.putClientProperty((Object)"PersistenceType", (Object)"Never");
        this.setName(NbBundle.getMessage(MergeDialogComponent.class, (String)"MergeDialogComponent.title"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(MergeDialogComponent.class, (String)"ACSN_Merge_Dialog_Component"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MergeDialogComponent.class, (String)"ACSD_Merge_Dialog_Component"));
        this.mergeTabbedPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(MergeDialogComponent.class, (String)"ACSN_Merge_Tabbed_Pane"));
        this.mergeTabbedPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(MergeDialogComponent.class, (String)"ACSD_Merge_Tabbed_Pane"));
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("org.netbeans.modules.merge.builtin.visualizer.MergeDialogComponent");
    }

    private void initComponents() {
        this.mergeTabbedPane = new JTabbedPane();
        this.buttonsPanel = new JPanel();
        this.okButton = new JButton();
        this.cancelButton = new JButton();
        this.helpButton = new JButton();
        FormListener formListener = new FormListener();
        this.setLayout((LayoutManager)new GridBagLayout());
        this.mergeTabbedPane.setTabPlacement(3);
        this.mergeTabbedPane.setPreferredSize(new Dimension(600, 600));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.mergeTabbedPane, (Object)gridBagConstraints);
        this.buttonsPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.okButton, (String)NbBundle.getMessage(MergeDialogComponent.class, (String)"BTN_OK"));
        this.okButton.setToolTipText(NbBundle.getBundle(MergeDialogComponent.class).getString("ACS_BTN_OKA11yDesc"));
        this.okButton.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 13;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 5, 0, 0);
        this.buttonsPanel.add((Component)this.okButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.cancelButton, (String)NbBundle.getMessage(MergeDialogComponent.class, (String)"BTN_Cancel"));
        this.cancelButton.setToolTipText(NbBundle.getBundle(MergeDialogComponent.class).getString("ACS_BTN_CancelA11yDesc"));
        this.cancelButton.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(0, 5, 0, 0);
        this.buttonsPanel.add((Component)this.cancelButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.helpButton, (String)NbBundle.getMessage(MergeDialogComponent.class, (String)"BTN_Help"));
        this.helpButton.setToolTipText(NbBundle.getBundle(MergeDialogComponent.class).getString("ACS_BTN_HelpA11yDesc"));
        this.helpButton.addActionListener(formListener);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(0, 5, 0, 0);
        this.buttonsPanel.add((Component)this.helpButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 12, 11, 11);
        this.add((Component)this.buttonsPanel, (Object)gridBagConstraints);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void okButtonActionPerformed(ActionEvent evt) {
        Component[] panels;
        if (!this.beforeClose()) {
            return;
        }
        MergeDialogComponent mergeDialogComponent = this;
        synchronized (mergeDialogComponent) {
            panels = this.mergeTabbedPane.getComponents();
        }
        try {
            this.internallyClosing = true;
            for (int i = 0; i < panels.length; ++i) {
                MergePanel panel = (MergePanel)panels[i];
                try {
                    this.fireVetoableChange("panelClosing", (Object)null, (Object)panel);
                }
                catch (PropertyVetoException pvex) {
                    this.internallyClosing = false;
                    return;
                }
                this.removeMergePanel(panel);
            }
        }
        finally {
            this.internallyClosing = false;
        }
    }

    private void helpButtonActionPerformed(ActionEvent evt) {
        this.getHelpCtx().display();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void cancelButtonActionPerformed(ActionEvent evt) {
        MergeDialogComponent mergeDialogComponent = this;
        synchronized (mergeDialogComponent) {
            try {
                this.fireVetoableChange("allPanelsCancelled", (Object)null, (Object)null);
            }
            catch (PropertyVetoException pvex) {
                // empty catch block
            }
            try {
                this.internallyClosing = true;
                this.close();
            }
            finally {
                this.internallyClosing = false;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void componentClosed() {
        Component[] panels;
        MergeDialogComponent mergeDialogComponent = this;
        synchronized (mergeDialogComponent) {
            try {
                this.fireVetoableChange("allPanelsCancelled", (Object)null, (Object)null);
            }
            catch (PropertyVetoException pvex) {
                // empty catch block
            }
            panels = this.mergeTabbedPane.getComponents();
        }
        for (int i = 0; i < panels.length; ++i) {
            MergePanel panel = (MergePanel)panels[i];
            this.removeMergePanel(panel);
        }
    }

    public boolean canClose() {
        return super.canClose() && (this.internallyClosing || this.beforeClose());
    }

    protected void componentOpened() {
        this.refreshName();
        this.requestActive();
    }

    public Dimension getPreferredSize() {
        Rectangle bounds = WindowManager.getDefault().getCurrentWorkspace().getBounds();
        return new Dimension(bounds.width / 2, (int)((double)bounds.height / 1.25));
    }

    public int getPersistenceType() {
        return 2;
    }

    public void addNotify() {
        super.addNotify();
        JRootPane root = this.getRootPane();
        if (root != null) {
            root.setDefaultButton(this.okButton);
        }
    }

    private void initListeners() {
        this.mergeTabbedPane.addMouseListener((MouseListener)((Object)new PopupMenuImpl()));
        this.mergeTabbedPane.addChangeListener(this);
    }

    public synchronized void addMergePanel(MergePanel panel) {
        this.mergeTabbedPane.addTab(panel.getName(), panel);
        MergeNode node = new MergeNode(panel);
        this.nodesForPanels.put(panel, node);
        this.mergeTabbedPane.setSelectedComponent(panel);
        this.setActivatedNodes(new Node[]{node});
    }

    public synchronized void removeMergePanel(MergePanel panel) {
        this.mergeTabbedPane.remove(panel);
        this.nodesForPanels.remove(panel);
        if (this.mergeTabbedPane.getTabCount() == 0) {
            try {
                this.fireVetoableChange("allPanelsClosed", (Object)null, (Object)null);
            }
            catch (PropertyVetoException pvex) {
                return;
            }
            this.close();
        }
    }

    public MergePanel getSelectedMergePanel() {
        Component selected = this.mergeTabbedPane.getSelectedComponent();
        if (selected == null || !(selected instanceof MergePanel)) {
            return null;
        }
        return (MergePanel)selected;
    }

    private static JPopupMenu createPopupMenu(MergePanel panel) {
        JPopupMenuPlus popup = new JPopupMenuPlus();
        SystemAction[] actions = panel.getSystemActions();
        for (int i = 0; i < actions.length; ++i) {
            if (actions[i] == null) {
                popup.addSeparator();
                continue;
            }
            if (actions[i] instanceof CallableSystemAction) {
                popup.add(((CallableSystemAction)actions[i]).getPopupPresenter());
                continue;
            }
            if (!(actions[i] instanceof FileSystemAction)) continue;
            popup.add(((FileSystemAction)actions[i]).getPopupPresenter());
        }
        return popup;
    }

    private static void showPopupMenu(JPopupMenu popup, Point p, Component comp) {
        SwingUtilities.convertPointToScreen(p, comp);
        Dimension popupSize = popup.getPreferredSize();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (p.x + popupSize.width > screenSize.width) {
            p.x = screenSize.width - popupSize.width;
        }
        if (p.y + popupSize.height > screenSize.height) {
            p.y = screenSize.height - popupSize.height;
        }
        SwingUtilities.convertPointFromScreen(p, comp);
        popup.show(comp, p.x, p.y);
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        Node node;
        MergePanel panel = (MergePanel)this.mergeTabbedPane.getSelectedComponent();
        if (panel != null && (node = (Node)this.nodesForPanels.get(panel)) != null) {
            this.setActivatedNodes(new Node[]{node});
        }
    }

    private void refreshName() {
        Mutex.EVENT.readAccess(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Component[] panels;
                String name = NbBundle.getMessage(MergeDialogComponent.class, (String)"MergeDialogComponent.title");
                MergeDialogComponent mergeDialogComponent = MergeDialogComponent.this;
                synchronized (mergeDialogComponent) {
                    panels = MergeDialogComponent.this.mergeTabbedPane.getComponents();
                }
                for (int i = 0; i < panels.length; ++i) {
                    MergePanel panel = (MergePanel)panels[i];
                    MergeNode node = (MergeNode)MergeDialogComponent.this.nodesForPanels.get(panel);
                    if (node.getLookup().lookup(SaveCookie.class) == null) continue;
                    name = "<html><b>" + name + "*</b></html>";
                    break;
                }
                MergeDialogComponent.this.setName(name);
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean beforeClose() {
        Component[] panels;
        MergeDialogComponent mergeDialogComponent = this;
        synchronized (mergeDialogComponent) {
            panels = this.mergeTabbedPane.getComponents();
        }
        boolean warning = false;
        ArrayList<String> unsavedPanelNames = new ArrayList<String>();
        ArrayList<SaveCookie> saveCookies = new ArrayList<SaveCookie>();
        for (int i = 0; i < panels.length; ++i) {
            SaveCookie sc;
            MergeNode node;
            MergePanel panel = (MergePanel)panels[i];
            if (panel.getNumUnresolvedConflicts() > 0 && !warning) {
                warning = true;
            }
            if ((sc = (SaveCookie)(node = this.nodesForPanels.get(panel)).getLookup().lookup(SaveCookie.class)) == null) continue;
            unsavedPanelNames.add(panel.getName());
            saveCookies.add(sc);
        }
        Object ret = unsavedPanelNames.size() == 1 ? DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)(warning ? NbBundle.getMessage(MergeDialogComponent.class, (String)"SaveFileWarningQuestion", unsavedPanelNames.get(0)) : NbBundle.getMessage(MergeDialogComponent.class, (String)"SaveFileQuestion", unsavedPanelNames.get(0))), 1)) : (unsavedPanelNames.size() > 1 ? DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)(warning ? NbBundle.getMessage(MergeDialogComponent.class, (String)"SaveFilesWarningQuestion", (Object)new Integer(unsavedPanelNames.size())) : NbBundle.getMessage(MergeDialogComponent.class, (String)"SaveFilesQuestion", (Object)new Integer(unsavedPanelNames.size()))), 1)) : (warning ? DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)NbBundle.getMessage(MergeDialogComponent.class, (String)"WarningQuestion", (Object)new Integer(unsavedPanelNames.size())), 2)) : NotifyDescriptor.YES_OPTION));
        if (!NotifyDescriptor.YES_OPTION.equals(ret) && !NotifyDescriptor.NO_OPTION.equals(ret)) {
            return false;
        }
        if (NotifyDescriptor.YES_OPTION.equals(ret) || NotifyDescriptor.OK_OPTION.equals(ret)) {
            for (SaveCookie sc : saveCookies) {
                IOException ioException = null;
                try {
                    sc.save();
                }
                catch (UserQuestionException uqex) {
                    Object status = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)uqex.getLocalizedMessage()));
                    if (status == NotifyDescriptor.OK_OPTION || status == NotifyDescriptor.YES_OPTION) {
                        boolean success;
                        try {
                            uqex.confirmed();
                            success = true;
                        }
                        catch (IOException ioex) {
                            success = false;
                            ioException = ioex;
                        }
                        if (success) {
                            try {
                                sc.save();
                            }
                            catch (IOException ioex) {
                                ioException = ioex;
                            }
                        }
                    }
                    if (status != NotifyDescriptor.NO_OPTION) {
                        return false;
                    }
                }
                catch (IOException ioEx) {
                    ioException = ioEx;
                }
                if (ioException == null) continue;
                ErrorManager.getDefault().notify((Throwable)ioException);
                return false;
            }
        }
        return true;
    }

    private class MergeNode
    extends AbstractNode
    implements PropertyChangeListener {
        private Reference<MergePanel> mergePanelRef;
        private final SaveCookieImpl saveCookie;

        public MergeNode(MergePanel panel) {
            super(Children.LEAF);
            panel.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)panel));
            this.mergePanelRef = new WeakReference<MergePanel>(panel);
            this.getCookieSet().add((Node.Cookie)new CloseCookieImpl());
            this.saveCookie = new SaveCookieImpl();
        }

        private void activateSave() {
            this.getCookieSet().add((Node.Cookie)this.saveCookie);
        }

        private void deactivateSave() {
            this.getCookieSet().remove((Node.Cookie)this.saveCookie);
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("canBeSaved".equals(propertyChangeEvent.getPropertyName())) {
                this.activateSave();
            } else if ("canNotBeSaved".equals(propertyChangeEvent.getPropertyName())) {
                this.deactivateSave();
            }
            MergeDialogComponent.this.refreshName();
        }

        private class CloseCookieImpl
        implements CloseCookie {
            public boolean close() {
                try {
                    MergeDialogComponent.this.fireVetoableChange("panelClosing", null, MergeNode.this.mergePanelRef.get());
                }
                catch (PropertyVetoException vetoEx) {
                    return false;
                }
                MergeDialogComponent.this.removeMergePanel((MergePanel)MergeNode.this.mergePanelRef.get());
                return true;
            }
        }

        private class SaveCookieImpl
        implements SaveCookie {
            private SaveCookieImpl() {
            }

            public void save() throws IOException {
                try {
                    MergeDialogComponent.this.fireVetoableChange("panelSave", null, MergeNode.this.mergePanelRef.get());
                }
                catch (PropertyVetoException vetoEx) {
                    Throwable cause = vetoEx.getCause();
                    if (cause instanceof IOException) {
                        throw (IOException)cause;
                    }
                    throw new IOException(vetoEx.getLocalizedMessage());
                }
            }
        }

    }

    private class PopupMenuImpl
    extends MouseUtils.PopupMouseAdapter {
        protected void showPopup(MouseEvent mouseEvent) {
            TabbedPaneUI tabUI = MergeDialogComponent.this.mergeTabbedPane.getUI();
            int clickTab = tabUI.tabForCoordinate(MergeDialogComponent.this.mergeTabbedPane, mouseEvent.getX(), mouseEvent.getY());
            MergePanel panel = MergeDialogComponent.this.getSelectedMergePanel();
            if (panel == null) {
                return;
            }
            if (clickTab != -1) {
                MergeDialogComponent.showPopupMenu(MergeDialogComponent.createPopupMenu(panel), mouseEvent.getPoint(), MergeDialogComponent.this.mergeTabbedPane);
            }
        }
    }

    private class FormListener
    implements ActionListener {
        FormListener() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == MergeDialogComponent.this.okButton) {
                MergeDialogComponent.this.okButtonActionPerformed(evt);
            } else if (evt.getSource() == MergeDialogComponent.this.cancelButton) {
                MergeDialogComponent.this.cancelButtonActionPerformed(evt);
            } else if (evt.getSource() == MergeDialogComponent.this.helpButton) {
                MergeDialogComponent.this.helpButtonActionPerformed(evt);
            }
        }
    }

}

