/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.merge.builtin.visualizer;

import java.awt.Color;
import java.awt.Component;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.Set;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.diff.DiffModuleConfig;
import org.netbeans.modules.merge.builtin.visualizer.MergeControl;
import org.netbeans.modules.merge.builtin.visualizer.MergeDialogComponent;
import org.netbeans.modules.merge.builtin.visualizer.MergePanel;
import org.netbeans.spi.diff.MergeVisualizer;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;

public class GraphicalMergeVisualizer
extends MergeVisualizer
implements Serializable {
    private Color colorUnresolvedConflict = DiffModuleConfig.getDefault().getUnresolvedColor();
    private Color colorResolvedConflict = DiffModuleConfig.getDefault().getAppliedColor();
    private Color colorOtherConflict = DiffModuleConfig.getDefault().getNotAppliedColor();
    private MergeDialogComponent merge = null;
    static final long serialVersionUID = -2175410667258166512L;

    public String getDisplayName() {
        return NbBundle.getMessage(GraphicalMergeVisualizer.class, (String)"GraphicalMergeVisualizer.displayName");
    }

    public String getShortDescription() {
        return NbBundle.getMessage(GraphicalMergeVisualizer.class, (String)"GraphicalMergeVisualizer.shortDescription");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Component createView(Difference[] diffs, StreamSource source1, StreamSource source2, StreamSource result) throws IOException {
        GraphicalMergeVisualizer graphicalMergeVisualizer = this;
        synchronized (graphicalMergeVisualizer) {
            if (this.merge == null) {
                Set opened = TopComponent.getRegistry().getOpened();
                for (Object component : opened) {
                    if (!(component instanceof MergeDialogComponent)) continue;
                    this.merge = (MergeDialogComponent)component;
                    break;
                }
                if (this.merge == null) {
                    this.merge = new MergeDialogComponent();
                }
            }
        }
        if (!this.merge.isOpened()) {
            this.merge.open();
        }
        MergePanel panel = new MergePanel();
        MergeControl control = new MergeControl(panel);
        control.initialize(diffs, source1, source2, result, this.colorUnresolvedConflict, this.colorResolvedConflict, this.colorOtherConflict);
        this.merge.addVetoableChangeListener(WeakListeners.vetoableChange((VetoableChangeListener)control, (Object)this.merge));
        this.merge.addMergePanel(panel);
        return this.merge;
    }

    public Color getColorUnresolvedConflict() {
        return this.colorUnresolvedConflict;
    }

    public void setColorUnresolvedConflict(Color colorUnresolvedConflict) {
        this.colorUnresolvedConflict = colorUnresolvedConflict;
    }

    public Color getColorResolvedConflict() {
        return this.colorResolvedConflict;
    }

    public void setColorResolvedConflict(Color colorResolvedConflict) {
        this.colorResolvedConflict = colorResolvedConflict;
    }

    public Color getColorOtherConflict() {
        return this.colorOtherConflict;
    }

    public void setColorOtherConflict(Color colorOtherConflict) {
        this.colorOtherConflict = colorOtherConflict;
    }
}

