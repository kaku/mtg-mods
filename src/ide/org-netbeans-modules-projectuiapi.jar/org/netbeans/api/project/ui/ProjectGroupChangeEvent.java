/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.api.project.ui;

import java.util.EventObject;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.api.project.ui.ProjectGroup;

public final class ProjectGroupChangeEvent
extends EventObject {
    private final ProjectGroup newGroup;
    private final ProjectGroup oldGroup;

    public ProjectGroupChangeEvent(@NullAllowed ProjectGroup o, @NullAllowed ProjectGroup n) {
        super(OpenProjects.getDefault());
        this.oldGroup = o;
        this.newGroup = n;
    }

    @CheckForNull
    public ProjectGroup getNewGroup() {
        return this.newGroup;
    }

    @CheckForNull
    public ProjectGroup getOldGroup() {
        return this.oldGroup;
    }
}

