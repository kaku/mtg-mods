/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.project.ui;

import java.util.Collection;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.modules.project.uiapi.BrokenReferencesImplementation;
import org.netbeans.spi.project.ui.ProjectProblemsProvider;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public class ProjectProblems {
    private ProjectProblems() {
        throw new IllegalStateException();
    }

    public static boolean isBroken(@NonNull Project project) {
        Parameters.notNull((CharSequence)"project", (Object)project);
        ProjectProblemsProvider provider = (ProjectProblemsProvider)project.getLookup().lookup(ProjectProblemsProvider.class);
        return provider != null && !provider.getProblems().isEmpty();
    }

    public static void showAlert(@NonNull Project project) {
        Parameters.notNull((CharSequence)"project", (Object)project);
        BrokenReferencesImplementation impl = (BrokenReferencesImplementation)Lookup.getDefault().lookup(BrokenReferencesImplementation.class);
        if (impl != null) {
            impl.showAlert(project);
        }
    }

    public static void showCustomizer(@NonNull Project project) {
        Parameters.notNull((CharSequence)"project", (Object)project);
        BrokenReferencesImplementation impl = (BrokenReferencesImplementation)Lookup.getDefault().lookup(BrokenReferencesImplementation.class);
        if (impl != null) {
            impl.showCustomizer(project);
        }
    }
}

