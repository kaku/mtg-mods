/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.project.ui;

import java.util.EventListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.ui.ProjectGroupChangeEvent;

public interface ProjectGroupChangeListener
extends EventListener {
    public void projectGroupChanging(@NonNull ProjectGroupChangeEvent var1);

    public void projectGroupChanged(@NonNull ProjectGroupChangeEvent var1);
}

