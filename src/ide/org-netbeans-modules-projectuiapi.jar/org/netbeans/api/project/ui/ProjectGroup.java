/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.project.ui;

import java.util.prefs.Preferences;
import org.netbeans.modules.project.uiapi.Utilities;

public final class ProjectGroup {
    private final Preferences prefs;
    private final String name;

    ProjectGroup(String name, Preferences prefs) {
        this.name = name;
        this.prefs = prefs;
    }

    public String getName() {
        return this.name;
    }

    public Preferences preferencesForPackage(Class clazz) {
        return this.prefs.node(clazz.getPackage().getName().replace(".", "/"));
    }

    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ProjectGroup other = (ProjectGroup)obj;
        if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    static {
        AccessorImpl impl = new AccessorImpl();
        impl.assign();
    }

    static class AccessorImpl
    extends Utilities.ProjectGroupAccessor {
        AccessorImpl() {
        }

        public void assign() {
            if (Utilities.ACCESSOR == null) {
                Utilities.ACCESSOR = this;
            }
        }

        @Override
        public ProjectGroup createGroup(String name, Preferences prefs) {
            return new ProjectGroup(name, prefs);
        }
    }

}

