/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 */
package org.netbeans.api.project.ui;

import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjectsListener;
import org.netbeans.api.project.ui.ProjectGroup;
import org.netbeans.api.project.ui.ProjectGroupChangeListener;
import org.netbeans.modules.project.uiapi.OpenProjectsTrampoline;
import org.netbeans.modules.project.uiapi.Utilities;

public final class OpenProjects {
    public static final String PROPERTY_OPEN_PROJECTS = "openProjects";
    public static final String PROPERTY_MAIN_PROJECT = "MainProject";
    private static OpenProjects INSTANCE = new OpenProjects();
    private static final Logger LOG = Logger.getLogger(OpenProjects.class.getName());
    private OpenProjectsTrampoline trampoline = Utilities.getOpenProjectsTrampoline();

    private OpenProjects() {
        this.addPropertyChangeListener(new OpenProjectsListener());
    }

    public static OpenProjects getDefault() {
        return INSTANCE;
    }

    public Project[] getOpenProjects() {
        return this.trampoline.getOpenProjectsAPI();
    }

    public Future<Project[]> openProjects() {
        return this.trampoline.openProjectsAPI();
    }

    public void open(Project[] projects, boolean openSubprojects) {
        if (Arrays.asList(projects).contains(null)) {
            throw new NullPointerException();
        }
        this.trampoline.openAPI(projects, openSubprojects, false);
    }

    public void open(Project[] projects, boolean openSubprojects, boolean showProgress) {
        if (Arrays.asList(projects).contains(null)) {
            throw new NullPointerException();
        }
        this.trampoline.openAPI(projects, openSubprojects, showProgress);
    }

    public boolean isProjectOpen(Project p) {
        if (p == null) {
            return false;
        }
        for (Project real : this.getOpenProjects()) {
            if (p.equals((Object)real) || real.equals((Object)p)) {
                LOG.log(Level.FINE, "isProjectOpen => true for {0} @{1} ~ real @{2}", new Object[]{p, p.hashCode(), real.hashCode()});
                return true;
            }
            LOG.log(Level.FINER, "distinct from {0} @{1}", new Object[]{real, real.hashCode()});
        }
        LOG.log(Level.FINE, "isProjectOpen => false for {0} @{1}", new Object[]{p, p.hashCode()});
        return false;
    }

    public void close(Project[] projects) {
        this.trampoline.closeAPI(projects);
    }

    public Project getMainProject() {
        return this.trampoline.getMainProject();
    }

    public void setMainProject(Project project) {
        this.trampoline.setMainProject(project);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.trampoline.addPropertyChangeListenerAPI(listener, this);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.trampoline.removePropertyChangeListenerAPI(listener);
    }

    @CheckForNull
    public ProjectGroup getActiveProjectGroup() {
        return this.trampoline.getActiveProjectGroupAPI();
    }

    public void addProjectGroupChangeListener(@NonNull ProjectGroupChangeListener listener) {
        this.trampoline.addProjectGroupChangeListenerAPI(listener);
    }

    public void removeProjectGroupChangeListener(@NonNull ProjectGroupChangeListener listener) {
        this.trampoline.removeProjectGroupChangeListenerAPI(listener);
    }
}

