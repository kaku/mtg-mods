/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.openide.windows.TopComponentGroup
 *  org.openide.windows.WindowManager
 */
package org.netbeans.api.project.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowManager;

class OpenProjectsListener
implements PropertyChangeListener {
    OpenProjectsListener() {
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("openProjects".equals(evt.getPropertyName())) {
            this.openCloseWindowGroup();
        }
    }

    private void openCloseWindowGroup() {
        final Project[] projects = OpenProjects.getDefault().getOpenProjects();
        Runnable r = new Runnable(){

            @Override
            public void run() {
                boolean show;
                TopComponentGroup taskListGroup;
                TopComponentGroup projectsGroup = WindowManager.getDefault().findTopComponentGroup("OpenedProjects");
                if (null == projectsGroup) {
                    Logger.getLogger(OpenProjectsListener.class.getName()).log(Level.FINE, "OpenedProjects TopComponent Group not found.");
                }
                if (null == (taskListGroup = WindowManager.getDefault().findTopComponentGroup("TaskList"))) {
                    Logger.getLogger(OpenProjectsListener.class.getName()).log(Level.FINE, "TaskList TopComponent Group not found.");
                }
                boolean bl = show = projects.length > 0;
                if (show) {
                    if (null != projectsGroup) {
                        projectsGroup.open();
                    }
                    if (null != taskListGroup && OpenProjectsListener.this.supportsTaskList(projects)) {
                        taskListGroup.open();
                    }
                } else {
                    if (null != projectsGroup) {
                        projectsGroup.close();
                    }
                    if (null != taskListGroup) {
                        taskListGroup.close();
                    }
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }

    private boolean supportsTaskList(Project[] projects) {
        boolean res = false;
        for (Project p : projects) {
            if (p.getClass().getName().equals("org.netbeans.modules.cnd.makeproject.MakeProject")) continue;
            res = true;
            break;
        }
        return res;
    }

}

