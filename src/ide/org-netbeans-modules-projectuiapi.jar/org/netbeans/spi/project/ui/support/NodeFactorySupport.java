/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectManager
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.spi.project.ui.support;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.Lookups;

public class NodeFactorySupport {
    private static RequestProcessor RP = new RequestProcessor(NodeFactorySupport.class.getName());
    private static NodeListKeyWrapper LOADING_KEY = new NodeListKeyWrapper(null, null);

    private NodeFactorySupport() {
    }

    public static Children createCompositeChildren(Project project, String folderPath) {
        return new DelegateChildren(project, folderPath);
    }

    public static /* varargs */ NodeList<?> fixedNodeList(Node ... nodes) {
        return new FixedNodeList(nodes);
    }

    static Node createWaitNode() {
        AbstractNode n = new AbstractNode(Children.LEAF);
        n.setIconBaseWithExtension("org/openide/nodes/wait.gif");
        n.setDisplayName(NbBundle.getMessage(ChildFactory.class, (String)"LBL_WAIT"));
        return n;
    }

    private static class NodeListKeyWrapper {
        NodeList nodeList;
        Object object;

        NodeListKeyWrapper(Object obj, NodeList list) {
            this.nodeList = list;
            this.object = obj;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            NodeListKeyWrapper other = (NodeListKeyWrapper)obj;
            if (!(this.nodeList == other.nodeList || this.nodeList != null && this.nodeList.equals(other.nodeList))) {
                return false;
            }
            if (!(this.object == other.object || this.object != null && this.object.equals(other.object))) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 3;
            hash = 67 * hash + (this.nodeList != null ? this.nodeList.hashCode() : 0);
            hash = 67 * hash + (this.object != null ? this.object.hashCode() : 0);
            return hash;
        }
    }

    static class DelegateChildren
    extends Children.Keys<NodeListKeyWrapper>
    implements LookupListener,
    ChangeListener {
        private String folderPath;
        private Project project;
        private List<NodeList<?>> nodeLists = new ArrayList();
        private List<NodeFactory> factories = new ArrayList<NodeFactory>();
        private Lookup.Result<NodeFactory> result;
        private final HashMap<NodeList<?>, List<NodeListKeyWrapper>> keys;
        private RequestProcessor.Task task;

        public DelegateChildren(Project proj, String path) {
            this.folderPath = path;
            this.project = proj;
            this.keys = new HashMap();
        }

        protected Lookup createLookup() {
            return Lookups.forPath((String)this.folderPath);
        }

        protected Node[] createNodes(NodeListKeyWrapper key) {
            if (key == LOADING_KEY) {
                return new Node[]{NodeFactorySupport.createWaitNode()};
            }
            Node nd = key.nodeList.node(key.object);
            if (nd != null) {
                return new Node[]{nd};
            }
            return new Node[0];
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Collection<NodeListKeyWrapper> createKeys() {
            ArrayList<NodeListKeyWrapper> col = new ArrayList<NodeListKeyWrapper>();
            assert (!Thread.holdsLock(this.keys));
            DelegateChildren delegateChildren = this;
            synchronized (delegateChildren) {
                for (NodeList lst : this.nodeLists) {
                    List<NodeListKeyWrapper> x;
                    HashMap hashMap = this.keys;
                    synchronized (hashMap) {
                        x = this.keys.get(lst);
                    }
                    if (x == null) continue;
                    col.addAll(x);
                }
            }
            return col;
        }

        protected void addNotify() {
            Children.Keys.super.addNotify();
            this.setKeys(Collections.singleton(LOADING_KEY));
            this.task = RP.post(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    DelegateChildren delegateChildren = DelegateChildren.this;
                    synchronized (delegateChildren) {
                        DelegateChildren.this.result = DelegateChildren.this.createLookup().lookupResult(NodeFactory.class);
                        for (NodeFactory factory : DelegateChildren.this.result.allInstances()) {
                            NodeList lst = factory.createNodes(DelegateChildren.this.project);
                            assert (lst != null);
                            lst.addNotify();
                            List objects = lst.keys();
                            HashMap hashMap = DelegateChildren.this.keys;
                            synchronized (hashMap) {
                                DelegateChildren.this.nodeLists.add(lst);
                                DelegateChildren.this.addKeys(lst, objects);
                            }
                            lst.addChangeListener(DelegateChildren.this);
                            DelegateChildren.this.factories.add(factory);
                        }
                        DelegateChildren.this.result.addLookupListener((LookupListener)DelegateChildren.this);
                    }
                    DelegateChildren.this.setKeys(DelegateChildren.this.createKeys());
                    DelegateChildren.this.task = null;
                }
            });
        }

        public Node[] getNodes(boolean optimalResult) {
            Node[] ns = Children.Keys.super.getNodes(optimalResult);
            RequestProcessor.Task _task = this.task;
            if (optimalResult && _task != null) {
                _task.waitFinished();
                ns = Children.Keys.super.getNodes(optimalResult);
            }
            return ns;
        }

        public int getNodesCount(boolean optimalResult) {
            int cnt = Children.Keys.super.getNodesCount(optimalResult);
            RequestProcessor.Task _task = this.task;
            if (optimalResult && _task != null) {
                _task.waitFinished();
                cnt = Children.Keys.super.getNodesCount(optimalResult);
            }
            return cnt;
        }

        public Node findChild(String name) {
            if (name != null) {
                this.getNodes(true);
            }
            return Children.Keys.super.findChild(name);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void removeNotify() {
            Children.Keys.super.removeNotify();
            this.setKeys(Collections.emptySet());
            DelegateChildren delegateChildren = this;
            synchronized (delegateChildren) {
                for (NodeList elem : this.nodeLists) {
                    elem.removeChangeListener(this);
                    elem.removeNotify();
                }
                HashMap i$ = this.keys;
                synchronized (i$) {
                    this.keys.clear();
                    this.nodeLists.clear();
                }
                this.factories.clear();
                if (this.result != null) {
                    this.result.removeLookupListener((LookupListener)this);
                    this.result = null;
                }
            }
        }

        @Override
        public void stateChanged(final ChangeEvent e) {
            Runnable action = new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    NodeList list = (NodeList)e.getSource();
                    List objects = list.keys();
                    HashMap hashMap = DelegateChildren.this.keys;
                    synchronized (hashMap) {
                        DelegateChildren.this.removeKeys(list);
                        DelegateChildren.this.addKeys(list, objects);
                    }
                    Collection ks = DelegateChildren.this.createKeys();
                    NodeFactorySupport.createWaitNode();
                    EventQueue.invokeLater(new RunnableImpl(DelegateChildren.this, ks));
                }
            };
            if (ProjectManager.mutex().isReadAccess() || ProjectManager.mutex().isWriteAccess()) {
                action.run();
            } else {
                RP.post(action);
            }
        }

        private void addKeys(NodeList list, List objects) {
            assert (Thread.holdsLock(this.keys));
            ArrayList<NodeListKeyWrapper> wrps = new ArrayList<NodeListKeyWrapper>();
            for (Object key : objects) {
                wrps.add(new NodeListKeyWrapper(key, list));
            }
            this.keys.put(list, wrps);
        }

        private void removeKeys(NodeList list) {
            assert (Thread.holdsLock(this.keys));
            this.keys.remove(list);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            int index = 0;
            DelegateChildren delegateChildren = this;
            synchronized (delegateChildren) {
                Lookup.Result res = (Lookup.Result)ev.getSource();
                for (Object _factory : res.allInstances()) {
                    Object lst;
                    NodeFactory factory = (NodeFactory)_factory;
                    if (!this.factories.contains(factory)) {
                        this.factories.add(index, factory);
                        lst = factory.createNodes(this.project);
                        assert (lst != null);
                        List objects = lst.keys();
                        HashMap hashMap = this.keys;
                        synchronized (hashMap) {
                            this.nodeLists.add(index, (Object)lst);
                            this.addKeys((NodeList)lst, objects);
                        }
                        lst.addNotify();
                        lst.addChangeListener(this);
                    } else {
                        while (!factory.equals(this.factories.get(index))) {
                            this.factories.remove(index);
                            lst = this.keys;
                            synchronized (lst) {
                                NodeList lst2 = this.nodeLists.remove(index);
                                this.removeKeys(lst2);
                                lst2.removeNotify();
                                lst2.removeChangeListener(this);
                                continue;
                            }
                        }
                    }
                    ++index;
                }
            }
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    DelegateChildren.this.setKeys(DelegateChildren.this.createKeys());
                }
            });
        }

        private static class RunnableImpl
        implements Runnable {
            private Collection<NodeListKeyWrapper> ks;
            private DelegateChildren ch;

            public RunnableImpl(DelegateChildren aThis, Collection<NodeListKeyWrapper> ks) {
                this.ks = ks;
                this.ch = aThis;
            }

            @Override
            public void run() {
                this.ch.setKeys(this.ks);
                this.ch = null;
                this.ks = null;
            }
        }

    }

    private static class FixedNodeList
    implements NodeList<Node> {
        private List<Node> nodes;

        /* varargs */ FixedNodeList(Node ... nds) {
            this.nodes = Arrays.asList(nds);
        }

        @Override
        public List<Node> keys() {
            return this.nodes;
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }

        @Override
        public void addNotify() {
        }

        @Override
        public void removeNotify() {
        }

        @Override
        public Node node(Node key) {
            return key;
        }
    }

}

