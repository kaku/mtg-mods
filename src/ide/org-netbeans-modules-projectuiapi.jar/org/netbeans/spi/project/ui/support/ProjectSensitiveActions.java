/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 */
package org.netbeans.spi.project.ui.support;

import javax.swing.Action;
import javax.swing.Icon;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.ProjectActionPerformer;
import org.openide.util.ContextAwareAction;

public class ProjectSensitiveActions {
    private ProjectSensitiveActions() {
    }

    public static Action projectCommandAction(String command, String namePattern, Icon icon) {
        return Utilities.getActionsFactory().projectCommandAction(command, namePattern, icon);
    }

    public static Action projectSensitiveAction(ProjectActionPerformer performer, String namePattern, Icon icon) {
        return Utilities.getActionsFactory().projectSensitiveAction(performer, namePattern, icon);
    }
}

