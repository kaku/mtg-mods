/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.netbeans.spi.project.ui;

import org.netbeans.spi.project.ui.PathFinder;
import org.openide.nodes.Node;

public interface LogicalViewProvider
extends PathFinder {
    public Node createLogicalView();
}

