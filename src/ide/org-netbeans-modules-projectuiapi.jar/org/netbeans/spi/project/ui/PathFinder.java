/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.netbeans.spi.project.ui;

import org.openide.nodes.Node;

public interface PathFinder {
    public Node findPath(Node var1, Object var2);
}

