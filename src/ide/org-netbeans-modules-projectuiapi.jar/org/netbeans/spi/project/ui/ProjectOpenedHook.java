/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project.ui;

import org.netbeans.modules.project.uiapi.ProjectOpenedTrampoline;

public abstract class ProjectOpenedHook {
    protected ProjectOpenedHook() {
    }

    protected abstract void projectOpened();

    protected abstract void projectClosed();

    static {
        ProjectOpenedTrampoline.DEFAULT = new ProjectOpenedTrampoline(){

            @Override
            public void projectOpened(ProjectOpenedHook hook) {
                hook.projectOpened();
            }

            @Override
            public void projectClosed(ProjectOpenedHook hook) {
                hook.projectClosed();
            }
        };
    }

}

