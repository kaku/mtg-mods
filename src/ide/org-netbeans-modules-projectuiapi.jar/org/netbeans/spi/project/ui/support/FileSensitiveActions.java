/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.spi.project.ui.support;

import javax.swing.Action;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.FileActionPerformer;

public class FileSensitiveActions {
    private FileSensitiveActions() {
    }

    public static Action fileCommandAction(@NonNull String command, @NonNull String namePattern, @NullAllowed Icon icon) {
        return Utilities.getActionsFactory().fileCommandAction(command, namePattern, icon);
    }

    public static Action fileSensitiveAction(@NonNull FileActionPerformer performer, @NonNull String namePattern, @NullAllowed Icon icon) {
        return Utilities.getActionsFactory().fileSensitiveAction(performer, namePattern, icon);
    }
}

