/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 */
package org.netbeans.spi.project.ui.support;

import org.netbeans.api.project.Project;

public interface ProjectActionPerformer {
    public boolean enable(Project var1);

    public void perform(Project var1);
}

