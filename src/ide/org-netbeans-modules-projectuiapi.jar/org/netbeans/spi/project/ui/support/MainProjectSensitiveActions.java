/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project.ui.support;

import javax.swing.Action;
import javax.swing.Icon;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.ProjectActionPerformer;

public class MainProjectSensitiveActions {
    private MainProjectSensitiveActions() {
    }

    public static Action mainProjectCommandAction(String command, String namePattern, Icon icon) {
        return Utilities.getActionsFactory().mainProjectCommandAction(command, namePattern, icon);
    }

    public static Action mainProjectSensitiveAction(ProjectActionPerformer performer, String namePattern, Icon icon) {
        return Utilities.getActionsFactory().mainProjectSensitiveAction(performer, namePattern, icon);
    }
}

