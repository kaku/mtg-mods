/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.netbeans.spi.project.ui.support;

import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.nodes.Node;

public interface NodeList<K> {
    public List<K> keys();

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public Node node(K var1);

    public void addNotify();

    public void removeNotify();
}

