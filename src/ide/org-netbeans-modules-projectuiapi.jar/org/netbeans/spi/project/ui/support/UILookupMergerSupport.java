/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.spi.project.LookupMerger
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.spi.project.ui.support;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.project.uiapi.ProjectOpenedTrampoline;
import org.netbeans.spi.project.LookupMerger;
import org.netbeans.spi.project.ui.PrivilegedTemplates;
import org.netbeans.spi.project.ui.ProjectOpenedHook;
import org.netbeans.spi.project.ui.ProjectProblemsProvider;
import org.netbeans.spi.project.ui.RecommendedTemplates;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public final class UILookupMergerSupport {
    private UILookupMergerSupport() {
    }

    public static LookupMerger<RecommendedTemplates> createRecommendedTemplatesMerger() {
        return new RecommendedMerger();
    }

    public static LookupMerger<PrivilegedTemplates> createPrivilegedTemplatesMerger() {
        return new PrivilegedMerger();
    }

    public static LookupMerger<ProjectOpenedHook> createProjectOpenHookMerger(ProjectOpenedHook defaultInstance) {
        return new OpenMerger(defaultInstance);
    }

    public static LookupMerger<ProjectProblemsProvider> createProjectProblemsProviderMerger() {
        return new ProjectProblemsProviderMerger();
    }

    private static class ProjectProblemsProviderImpl
    implements ProjectProblemsProvider,
    PropertyChangeListener {
        private final Lookup lkp;
        private final PropertyChangeSupport support;
        private Iterable<? extends Reference<ProjectProblemsProvider>> providers;

        ProjectProblemsProviderImpl(@NonNull Lookup lkp) {
            Parameters.notNull((CharSequence)"lkp", (Object)lkp);
            this.lkp = lkp;
            this.support = new PropertyChangeSupport(this);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            this.support.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            Parameters.notNull((CharSequence)"listener", (Object)listener);
            this.support.removePropertyChangeListener(listener);
        }

        @Override
        public Collection<? extends ProjectProblemsProvider.ProjectProblem> getProblems() {
            LinkedHashSet<? extends ProjectProblemsProvider.ProjectProblem> problems = new LinkedHashSet<ProjectProblemsProvider.ProjectProblem>();
            for (Reference<ProjectProblemsProvider> providerRef : this.getProviders()) {
                ProjectProblemsProvider provider = providerRef.get();
                if (provider == null) continue;
                problems.addAll(provider.getProblems());
            }
            return Collections.unmodifiableCollection(problems);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("problems".equals(evt.getPropertyName())) {
                this.support.firePropertyChange("problems", null, null);
            }
        }

        private synchronized Iterable<? extends Reference<ProjectProblemsProvider>> getProviders() {
            if (this.providers == null) {
                LinkedHashSet<? extends Reference<ProjectProblemsProvider>> newProviders = new LinkedHashSet<Reference<ProjectProblemsProvider>>();
                for (ProjectProblemsProvider provider : this.lkp.lookupAll(ProjectProblemsProvider.class)) {
                    provider.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)provider));
                    newProviders.add(new WeakReference<ProjectProblemsProvider>(provider));
                }
                this.providers = newProviders;
            }
            return this.providers;
        }
    }

    private static class OpenHookImpl
    extends ProjectOpenedHook {
        private final ProjectOpenedHook defaultInstance;
        private final Lookup lkp;

        OpenHookImpl(ProjectOpenedHook def, Lookup lkp) {
            this.defaultInstance = def;
            this.lkp = lkp;
        }

        @Override
        protected void projectOpened() {
            if (this.defaultInstance != null) {
                ProjectOpenedTrampoline.DEFAULT.projectOpened(this.defaultInstance);
            }
            for (ProjectOpenedHook poh : this.lkp.lookupAll(ProjectOpenedHook.class)) {
                if (poh == this.defaultInstance || poh == this) continue;
                ProjectOpenedTrampoline.DEFAULT.projectOpened(poh);
            }
        }

        @Override
        protected void projectClosed() {
            if (this.defaultInstance != null) {
                ProjectOpenedTrampoline.DEFAULT.projectClosed(this.defaultInstance);
            }
            for (ProjectOpenedHook poh : this.lkp.lookupAll(ProjectOpenedHook.class)) {
                if (poh == this.defaultInstance || poh == this) continue;
                ProjectOpenedTrampoline.DEFAULT.projectClosed(poh);
            }
        }
    }

    private static class RecommendedTemplatesImpl
    implements RecommendedTemplates {
        private final Lookup lkp;

        public RecommendedTemplatesImpl(Lookup lkp) {
            this.lkp = lkp;
        }

        @Override
        public String[] getRecommendedTypes() {
            LinkedHashSet<String> templates = new LinkedHashSet<String>();
            for (RecommendedTemplates pt : this.lkp.lookupAll(RecommendedTemplates.class)) {
                String[] temp = pt.getRecommendedTypes();
                if (temp == null) {
                    throw new IllegalStateException(pt.getClass().getName() + " returns null from getRecommendedTemplates() method.");
                }
                templates.addAll(Arrays.asList(temp));
            }
            return templates.toArray(new String[templates.size()]);
        }
    }

    private static class PrivilegedTemplatesImpl
    implements PrivilegedTemplates {
        private final Lookup lkp;

        public PrivilegedTemplatesImpl(Lookup lkp) {
            this.lkp = lkp;
        }

        @Override
        public String[] getPrivilegedTemplates() {
            LinkedHashSet<String> templates = new LinkedHashSet<String>();
            for (PrivilegedTemplates pt : this.lkp.lookupAll(PrivilegedTemplates.class)) {
                String[] temp = pt.getPrivilegedTemplates();
                if (temp == null) {
                    throw new IllegalStateException(pt.getClass().getName() + " returns null from getPrivilegedTemplates() method.");
                }
                templates.addAll(Arrays.asList(temp));
            }
            return templates.toArray(new String[templates.size()]);
        }
    }

    private static class ProjectProblemsProviderMerger
    implements LookupMerger<ProjectProblemsProvider> {
        private ProjectProblemsProviderMerger() {
        }

        public Class<ProjectProblemsProvider> getMergeableClass() {
            return ProjectProblemsProvider.class;
        }

        public ProjectProblemsProvider merge(Lookup lookup) {
            return new ProjectProblemsProviderImpl(lookup);
        }
    }

    private static class OpenMerger
    implements LookupMerger<ProjectOpenedHook> {
        private final ProjectOpenedHook defaultInstance;

        OpenMerger(ProjectOpenedHook def) {
            this.defaultInstance = def;
        }

        public Class<ProjectOpenedHook> getMergeableClass() {
            return ProjectOpenedHook.class;
        }

        public ProjectOpenedHook merge(Lookup lookup) {
            return new OpenHookImpl(this.defaultInstance, lookup);
        }
    }

    private static class RecommendedMerger
    implements LookupMerger<RecommendedTemplates> {
        private RecommendedMerger() {
        }

        public Class<RecommendedTemplates> getMergeableClass() {
            return RecommendedTemplates.class;
        }

        public RecommendedTemplates merge(Lookup lookup) {
            return new RecommendedTemplatesImpl(lookup);
        }
    }

    private static class PrivilegedMerger
    implements LookupMerger<PrivilegedTemplates> {
        private PrivilegedMerger() {
        }

        public Class<PrivilegedTemplates> getMergeableClass() {
            return PrivilegedTemplates.class;
        }

        public PrivilegedTemplates merge(Lookup lookup) {
            return new PrivilegedTemplatesImpl(lookup);
        }
    }

}

