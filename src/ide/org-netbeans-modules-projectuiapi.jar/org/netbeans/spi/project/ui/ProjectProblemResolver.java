/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project.ui;

import java.util.concurrent.Future;
import org.netbeans.spi.project.ui.ProjectProblemsProvider;

public interface ProjectProblemResolver {
    public Future<ProjectProblemsProvider.Result> resolve();

    public boolean equals(Object var1);

    public int hashCode();
}

