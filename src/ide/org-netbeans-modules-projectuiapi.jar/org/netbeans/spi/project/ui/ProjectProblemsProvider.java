/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.project.ui;

import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.spi.project.ui.ProjectProblemResolver;
import org.openide.util.Parameters;

public interface ProjectProblemsProvider {
    public static final String PROP_PROBLEMS = "problems";

    public void addPropertyChangeListener(@NonNull PropertyChangeListener var1);

    public void removePropertyChangeListener(@NonNull PropertyChangeListener var1);

    @NonNull
    public Collection<? extends ProjectProblem> getProblems();

    public static final class ProjectProblem {
        private final Severity severity;
        private final String displayName;
        private final String description;
        private final ProjectProblemResolver resolver;

        private ProjectProblem(@NonNull Severity severity, @NonNull String displayName, @NonNull String description, @NullAllowed ProjectProblemResolver resolver) {
            Parameters.notNull((CharSequence)"severity", (Object)((Object)severity));
            Parameters.notNull((CharSequence)"displayName", (Object)displayName);
            Parameters.notNull((CharSequence)"description", (Object)description);
            this.severity = severity;
            this.displayName = displayName;
            this.description = description;
            this.resolver = resolver;
        }

        @NonNull
        public Severity getSeverity() {
            return this.severity;
        }

        @NonNull
        public String getDisplayName() {
            return this.displayName;
        }

        @NonNull
        public String getDescription() {
            return this.description;
        }

        public boolean isResolvable() {
            return this.resolver != null;
        }

        public Future<Result> resolve() {
            if (this.resolver == null) {
                FutureTask<Result> toRet = new FutureTask<Result>(new Runnable(){

                    @Override
                    public void run() {
                    }
                }, Result.create(Status.UNRESOLVED));
                toRet.run();
                return toRet;
            }
            return this.resolver.resolve();
        }

        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if (!(other instanceof ProjectProblem)) {
                return false;
            }
            ProjectProblem otherProblem = (ProjectProblem)other;
            return this.displayName.equals(otherProblem.displayName) && this.description.equals(otherProblem.description) && (this.resolver != null ? this.resolver.equals(otherProblem.resolver) : otherProblem.resolver == null);
        }

        public int hashCode() {
            int result = 17;
            result = 31 * result + this.displayName.hashCode();
            result = 31 * result + this.description.hashCode();
            result = 31 * result + (this.resolver != null ? this.resolver.hashCode() : 0);
            return result;
        }

        public String toString() {
            return String.format("Project Problem: %s, resolvable by: %s", this.displayName, this.resolver);
        }

        @NonNull
        public static ProjectProblem createError(@NonNull String displayName, @NonNull String description, @NonNull ProjectProblemResolver resolver) {
            return new ProjectProblem(Severity.ERROR, displayName, description, resolver);
        }

        @NonNull
        public static ProjectProblem createError(@NonNull String displayName, @NonNull String description) {
            return new ProjectProblem(Severity.ERROR, displayName, description, null);
        }

        @NonNull
        public static ProjectProblem createWarning(@NonNull String displayName, @NonNull String description, @NonNull ProjectProblemResolver resolver) {
            return new ProjectProblem(Severity.WARNING, displayName, description, resolver);
        }

        @NonNull
        public static ProjectProblem createWarning(@NonNull String displayName, @NonNull String description) {
            return new ProjectProblem(Severity.WARNING, displayName, description, null);
        }

    }

    public static final class Result {
        private final Status status;
        private final String message;

        private Result(@NonNull Status status, @NullAllowed String message) {
            this.status = status;
            this.message = message;
        }

        public boolean isResolved() {
            return this.status != Status.UNRESOLVED;
        }

        @NonNull
        public Status getStatus() {
            return this.status;
        }

        @CheckForNull
        public String getMessage() {
            return this.message;
        }

        public static Result create(@NonNull Status status) {
            Parameters.notNull((CharSequence)"status", (Object)((Object)status));
            return new Result(status, null);
        }

        public static Result create(@NonNull Status status, @NonNull String message) {
            Parameters.notNull((CharSequence)"status", (Object)((Object)status));
            Parameters.notNull((CharSequence)"message", (Object)message);
            return new Result(status, message);
        }
    }

    public static enum Severity {
        ERROR,
        WARNING;
        

        private Severity() {
        }
    }

    public static enum Status {
        RESOLVED,
        RESOLVED_WITH_WARNING,
        UNRESOLVED;
        

        private Status() {
        }
    }

}

