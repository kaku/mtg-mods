/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.SourceGroup
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.TemplateWizard
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.project.ui.templates.support;

import java.io.IOException;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.modules.project.uiapi.Utilities;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.TemplateWizard;
import org.openide.util.Parameters;

public class Templates {
    private static final String SET_AS_MAIN = "setAsMain";

    private Templates() {
    }

    public static Project getProject(WizardDescriptor wizardDescriptor) {
        assert (wizardDescriptor != null);
        Project p = (Project)wizardDescriptor.getProperty("project");
        if (p != null) {
            return p;
        }
        FileObject target = Templates.getTargetFolder(wizardDescriptor);
        if (target != null) {
            return FileOwnerQuery.getOwner((FileObject)target);
        }
        return null;
    }

    public static FileObject getTemplate(WizardDescriptor wizardDescriptor) {
        DataObject template;
        if (wizardDescriptor == null) {
            throw new IllegalArgumentException("Cannot pass a null wizardDescriptor");
        }
        if (wizardDescriptor instanceof TemplateWizard && (template = ((TemplateWizard)wizardDescriptor).getTemplate()) != null) {
            return template.getPrimaryFile();
        }
        return (FileObject)wizardDescriptor.getProperty("targetTemplate");
    }

    public static FileObject getTargetFolder(WizardDescriptor wizardDescriptor) {
        if (wizardDescriptor instanceof TemplateWizard) {
            try {
                return ((TemplateWizard)wizardDescriptor).getTargetFolder().getPrimaryFile();
            }
            catch (IOException e) {
                return null;
            }
        }
        return (FileObject)wizardDescriptor.getProperty("targetFolder");
    }

    public static FileObject getExistingSourcesFolder(WizardDescriptor wizardDescriptor) {
        return (FileObject)wizardDescriptor.getProperty("existingSourcesFolder");
    }

    public static void setTargetFolder(WizardDescriptor wizardDescriptor, FileObject folder) {
        if (wizardDescriptor instanceof TemplateWizard) {
            if (folder == null) {
                ((TemplateWizard)wizardDescriptor).setTargetFolder(null);
            } else {
                DataFolder dataFolder = DataFolder.findFolder((FileObject)folder);
                ((TemplateWizard)wizardDescriptor).setTargetFolder(dataFolder);
            }
        } else {
            wizardDescriptor.putProperty("targetFolder", (Object)folder);
        }
    }

    public static String getTargetName(WizardDescriptor wizardDescriptor) {
        if (wizardDescriptor instanceof TemplateWizard) {
            return ((TemplateWizard)wizardDescriptor).getTargetName();
        }
        return (String)wizardDescriptor.getProperty("targetName");
    }

    public static void setTargetName(WizardDescriptor wizardDescriptor, String targetName) {
        if (wizardDescriptor instanceof TemplateWizard) {
            ((TemplateWizard)wizardDescriptor).setTargetName(targetName);
        } else {
            wizardDescriptor.putProperty("targetName", (Object)targetName);
        }
    }

    public static boolean getDefinesMainProject(WizardDescriptor wizardDescriptor) {
        return Boolean.TRUE.equals(wizardDescriptor.getProperty("setAsMain"));
    }

    @Deprecated
    public static void setDefinesMainProject(WizardDescriptor wizardDescriptor, boolean definesMainProject) {
        wizardDescriptor.putProperty("setAsMain", (Object)definesMainProject);
    }

    @Deprecated
    public static WizardDescriptor.Panel<WizardDescriptor> createSimpleTargetChooser(Project project, SourceGroup[] folders) {
        return Templates.buildSimpleTargetChooser(project, folders).create();
    }

    @Deprecated
    public static WizardDescriptor.Panel<WizardDescriptor> createSimpleTargetChooser(Project project, SourceGroup[] folders, WizardDescriptor.Panel<WizardDescriptor> bottomPanel) {
        return Templates.buildSimpleTargetChooser(project, folders).bottomPanel(bottomPanel).create();
    }

    public static SimpleTargetChooserBuilder buildSimpleTargetChooser(@NullAllowed Project project, @NonNull SourceGroup[] folders) {
        Parameters.notNull((CharSequence)"folders", (Object)folders);
        return new SimpleTargetChooserBuilder(project, folders);
    }

    public static final class SimpleTargetChooserBuilder {
        @NullAllowed
        final Project project;
        @NonNull
        final SourceGroup[] folders;
        WizardDescriptor.Panel<WizardDescriptor> bottomPanel;
        boolean freeFileExtension;

        SimpleTargetChooserBuilder(@NullAllowed Project project, @NonNull SourceGroup[] folders) {
            this.project = project;
            this.folders = folders;
        }

        public SimpleTargetChooserBuilder bottomPanel(WizardDescriptor.Panel<WizardDescriptor> bottomPanel) {
            this.bottomPanel = bottomPanel;
            return this;
        }

        public SimpleTargetChooserBuilder freeFileExtension() {
            this.freeFileExtension = true;
            return this;
        }

        public WizardDescriptor.Panel<WizardDescriptor> create() {
            return Utilities.getProjectChooserFactory().createSimpleTargetChooser(this.project, this.folders, this.bottomPanel, this.freeFileExtension);
        }
    }

}

