/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 */
package org.netbeans.spi.project.ui.support;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeList;

public interface NodeFactory {
    public NodeList<?> createNodes(Project var1);

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE, ElementType.METHOD})
    public static @interface Registration {
        public String[] projectType();

        public int position() default Integer.MAX_VALUE;
    }

}

