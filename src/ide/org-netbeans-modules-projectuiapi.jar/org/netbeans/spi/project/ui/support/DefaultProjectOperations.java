/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 *  org.netbeans.spi.project.support.ProjectOperations
 */
package org.netbeans.spi.project.ui.support;

import org.netbeans.api.project.Project;
import org.netbeans.modules.project.uiapi.DefaultProjectOperationsImplementation;
import org.netbeans.spi.project.support.ProjectOperations;

public final class DefaultProjectOperations {
    private DefaultProjectOperations() {
    }

    public static void performDefaultDeleteOperation(Project p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException("Project is null");
        }
        if (!ProjectOperations.isDeleteOperationSupported((Project)p)) {
            throw new IllegalArgumentException("Attempt to delete project that does not support deletion.");
        }
        DefaultProjectOperationsImplementation.deleteProject(p);
    }

    public static void performDefaultCopyOperation(Project p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException("Project is null");
        }
        if (!ProjectOperations.isCopyOperationSupported((Project)p)) {
            throw new IllegalArgumentException("Attempt to copy project that does not support copy.");
        }
        DefaultProjectOperationsImplementation.copyProject(p);
    }

    public static void performDefaultMoveOperation(Project p) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException("Project is null");
        }
        if (!ProjectOperations.isMoveOperationSupported((Project)p)) {
            throw new IllegalArgumentException("Attempt to move project that does not support move.");
        }
        DefaultProjectOperationsImplementation.moveProject(p);
    }

    public static void performDefaultRenameOperation(Project p, String newName) throws IllegalArgumentException {
        if (p == null) {
            throw new IllegalArgumentException("Project is null");
        }
        if (!ProjectOperations.isMoveOperationSupported((Project)p)) {
            throw new IllegalArgumentException("Attempt to rename project that does not support move.");
        }
        DefaultProjectOperationsImplementation.renameProject(p, newName);
    }
}

