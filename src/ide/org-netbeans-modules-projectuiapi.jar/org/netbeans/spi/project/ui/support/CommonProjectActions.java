/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.spi.project.ui.support;

import java.util.List;
import javax.swing.Action;
import org.openide.util.Utilities;

public class CommonProjectActions {
    public static final String EXISTING_SOURCES_FOLDER = "existingSourcesFolder";
    public static final String PROJECT_PARENT_FOLDER = "projdir";

    private CommonProjectActions() {
    }

    public static Action setAsMainProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().setAsMainProjectAction();
    }

    public static Action customizeProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().customizeProjectAction();
    }

    public static Action openSubprojectsAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().openSubprojectsAction();
    }

    public static Action closeProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().closeProjectAction();
    }

    public static Action newFileAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().newFileAction();
    }

    public static Action deleteProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().deleteProjectAction();
    }

    public static Action copyProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().copyProjectAction();
    }

    public static Action moveProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().moveProjectAction();
    }

    public static Action renameProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().renameProjectAction();
    }

    public static Action newProjectAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().newProjectAction();
    }

    public static Action setProjectConfigurationAction() {
        return org.netbeans.modules.project.uiapi.Utilities.getActionsFactory().setProjectConfigurationAction();
    }

    public static Action[] forType(String projectType) {
        List actions = Utilities.actionsForPath((String)("Projects/" + projectType + "/Actions"));
        return actions.toArray(new Action[actions.size()]);
    }
}

