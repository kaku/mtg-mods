/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.project.ui.support;

import java.io.File;
import javax.swing.JFileChooser;
import org.netbeans.modules.project.uiapi.Utilities;

public class ProjectChooser {
    private ProjectChooser() {
    }

    public static File getProjectsFolder() {
        return Utilities.getProjectChooserFactory().getProjectsFolder();
    }

    public static void setProjectsFolder(File folder) {
        if (folder == null || !folder.isDirectory()) {
            throw new IllegalArgumentException("Parameter must be a valid folder.");
        }
        Utilities.getProjectChooserFactory().setProjectsFolder(folder);
    }

    public static JFileChooser projectChooser() {
        return Utilities.getProjectChooserFactory().createProjectChooser();
    }
}

