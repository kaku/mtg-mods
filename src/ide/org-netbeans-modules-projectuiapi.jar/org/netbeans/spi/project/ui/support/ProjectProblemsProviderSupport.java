/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.project.ui.support;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.spi.project.ui.ProjectProblemsProvider;
import org.openide.util.Parameters;

public final class ProjectProblemsProviderSupport {
    private final PropertyChangeSupport propertyChangeSupport;
    private final Object problemsLock = new Object();
    private Collection<ProjectProblemsProvider.ProjectProblem> problems;
    private long changeId;

    public ProjectProblemsProviderSupport(@NonNull Object source) {
        Parameters.notNull((CharSequence)"source", (Object)source);
        this.propertyChangeSupport = new PropertyChangeSupport(source);
    }

    public void addPropertyChangeListener(@NonNull PropertyChangeListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(@NonNull PropertyChangeListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    public Collection<? extends ProjectProblemsProvider.ProjectProblem> getProblems(@NonNull ProblemsCollector problemsCollector) {
        long currentChangeId;
        Collection<ProjectProblemsProvider.ProjectProblem> currentProblems;
        Parameters.notNull((CharSequence)"problemsCollector", (Object)problemsCollector);
        Object object = this.problemsLock;
        synchronized (object) {
            currentProblems = this.problems;
            currentChangeId = this.changeId;
        }
        if (currentProblems != null) {
            return Collections.unmodifiableCollection(currentProblems);
        }
        currentProblems = new ArrayList<ProjectProblemsProvider.ProjectProblem>();
        currentProblems.addAll(problemsCollector.collectProblems());
        if (currentProblems.isEmpty()) {
            currentProblems = Collections.emptySet();
        }
        object = this.problemsLock;
        synchronized (object) {
            if (currentChangeId == this.changeId) {
                this.problems = currentProblems;
            } else if (this.problems != null) {
                currentProblems = this.problems;
            }
        }
        assert (currentProblems != null);
        return Collections.unmodifiableCollection(currentProblems);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void fireProblemsChange() {
        Object object = this.problemsLock;
        synchronized (object) {
            this.problems = null;
            ++this.changeId;
        }
        this.propertyChangeSupport.firePropertyChange("problems", null, null);
    }

    public static interface ProblemsCollector {
        @NonNull
        public Collection<? extends ProjectProblemsProvider.ProjectProblem> collectProblems();
    }

}

