/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project.ui.support;

import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;

public interface FileActionPerformer {
    public boolean enable(FileObject var1);

    public void perform(@NonNull FileObject var1);
}

