/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.project.ui.support;

import javax.swing.event.ChangeListener;
import org.netbeans.modules.project.uiapi.BuildExecutionSupportImplementation;
import org.netbeans.modules.project.uiapi.Utilities;
import org.openide.filesystems.FileObject;

public final class BuildExecutionSupport {
    private BuildExecutionSupport() {
    }

    public static void registerRunningItem(Item item) {
        Utilities.getBuildExecutionSupportImplementation().registerRunningItem(item);
    }

    public static void registerFinishedItem(Item item) {
        Utilities.getBuildExecutionSupportImplementation().registerFinishedItem(item);
    }

    public static Item getLastFinishedItem() {
        return Utilities.getBuildExecutionSupportImplementation().getLastItem();
    }

    public static void addChangeListener(ChangeListener listener) {
        BuildExecutionSupportImplementation besi = Utilities.getBuildExecutionSupportImplementation();
        besi.addChangeListener(listener);
    }

    public static void removeChangeListener(ChangeListener listener) {
        BuildExecutionSupportImplementation besi = Utilities.getBuildExecutionSupportImplementation();
        besi.removeChangeListener(listener);
    }

    public static interface ActionItem
    extends Item {
        public String getAction();

        public FileObject getProjectDirectory();
    }

    public static interface Item {
        public String getDisplayName();

        public void repeatExecution();

        public boolean isRunning();

        public void stopRunning();
    }

}

