/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$Status
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.project.ui.support;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.IllegalCharsetNameException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.project.uiapi.CategoryChangeSupport;
import org.netbeans.modules.project.uiapi.CategoryModel;
import org.netbeans.modules.project.uiapi.CategoryView;
import org.netbeans.modules.project.uiapi.CustomizerDialog;
import org.netbeans.modules.project.uiapi.CustomizerPane;
import org.netbeans.modules.project.uiapi.Utilities;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class ProjectCustomizer {
    private static final Logger LOG = Logger.getLogger(ProjectCustomizer.class.getName());

    private ProjectCustomizer() {
    }

    public static Dialog createCustomizerDialog(Category[] categories, CategoryComponentProvider componentProvider, String preselectedCategory, @NonNull ActionListener okOptionListener, HelpCtx helpCtx) {
        return ProjectCustomizer.createCustomizerDialog(categories, componentProvider, preselectedCategory, okOptionListener, null, helpCtx);
    }

    public static Dialog createCustomizerDialog(Category[] categories, CategoryComponentProvider componentProvider, String preselectedCategory, @NonNull ActionListener okOptionListener, @NullAllowed ActionListener storeListener, HelpCtx helpCtx) {
        Parameters.notNull((CharSequence)"okOptionListener", (Object)okOptionListener);
        CustomizerPane innerPane = ProjectCustomizer.createCustomizerPane(categories, componentProvider, preselectedCategory);
        Dialog dialog = CustomizerDialog.createDialog(okOptionListener, storeListener, innerPane, helpCtx, categories, componentProvider);
        return dialog;
    }

    public static Dialog createCustomizerDialog(String folderPath, Lookup context, String preselectedCategory, @NonNull ActionListener okOptionListener, HelpCtx helpCtx) {
        return ProjectCustomizer.createCustomizerDialog(folderPath, context, preselectedCategory, okOptionListener, null, helpCtx);
    }

    public static Dialog createCustomizerDialog(String folderPath, Lookup context, String preselectedCategory, @NonNull ActionListener okOptionListener, @NullAllowed ActionListener storeListener, HelpCtx helpCtx) {
        FileObject root = FileUtil.getConfigFile((String)folderPath);
        if (root == null) {
            throw new IllegalArgumentException("The designated path " + folderPath + " doesn't exist. Cannot create customizer.");
        }
        DataFolder def = DataFolder.findFolder((FileObject)root);
        assert (def != null);
        DelegateCategoryProvider prov = new DelegateCategoryProvider(def, context);
        Category[] categories = prov.getSubCategories();
        if (categories.length == 0) {
            return new JDialog((Frame)null, "<broken>");
        }
        return ProjectCustomizer.createCustomizerDialog(categories, prov, preselectedCategory, okOptionListener, storeListener, helpCtx);
    }

    private static CustomizerPane createCustomizerPane(Category[] categories, CategoryComponentProvider componentProvider, String preselectedCategory) {
        Category c;
        CategoryChangeSupport changeSupport = new CategoryChangeSupport();
        ProjectCustomizer.registerCategoryChangeSupport(changeSupport, categories);
        CategoryModel categoryModel = new CategoryModel(categories);
        CategoryView categoryView = new CategoryView(categoryModel);
        CustomizerPane customizerPane = new CustomizerPane(categoryView, categoryModel, componentProvider);
        if (preselectedCategory == null) {
            preselectedCategory = categories[0].getName();
        }
        if ((c = categoryModel.getCategory(preselectedCategory)) != null) {
            categoryModel.setCurrentCategory(c);
        }
        return customizerPane;
    }

    private static void registerCategoryChangeSupport(CategoryChangeSupport changeSupport, Category[] categories) {
        for (int i = 0; i < categories.length; ++i) {
            Utilities.putCategoryChangeSupport(categories[i], changeSupport);
            Category[] subCategories = categories[i].getSubcategories();
            if (subCategories == null) continue;
            ProjectCustomizer.registerCategoryChangeSupport(changeSupport, subCategories);
        }
    }

    public static ListCellRenderer encodingRenderer() {
        return new EncodingRenderer();
    }

    public static ComboBoxModel encodingModel(String initialCharset) {
        return new EncodingModel(initialCharset);
    }

    private static final class EncodingModel
    extends DefaultComboBoxModel {
        EncodingModel(String originalEncoding) {
            Charset defEnc = null;
            for (Charset c : Charset.availableCharsets().values()) {
                if (c.name().equals(originalEncoding)) {
                    defEnc = c;
                } else if (c.aliases().contains(originalEncoding)) {
                    defEnc = c;
                }
                this.addElement(c);
            }
            if (originalEncoding != null && defEnc == null) {
                try {
                    defEnc = new UnknownCharset(originalEncoding);
                    this.addElement(defEnc);
                }
                catch (IllegalCharsetNameException e) {
                    LOG.log(Level.INFO, "IllegalCharsetName: {0}", originalEncoding);
                }
            }
            if (defEnc == null) {
                defEnc = Charset.defaultCharset();
            }
            this.setSelectedItem(defEnc);
        }

        private static final class UnknownCharset
        extends Charset {
            UnknownCharset(String name) {
                super(name, new String[0]);
            }

            @Override
            public boolean contains(Charset c) {
                return false;
            }

            @Override
            public CharsetDecoder newDecoder() {
                throw new UnsupportedOperationException();
            }

            @Override
            public CharsetEncoder newEncoder() {
                throw new UnsupportedOperationException();
            }
        }

    }

    private static final class EncodingRenderer
    extends DefaultListCellRenderer {
        EncodingRenderer() {
            this.setName("ComboBox.listRenderer");
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean isLeadSelection) {
            if (value instanceof Charset) {
                value = ((Charset)value).displayName();
            }
            return super.getListCellRendererComponent(list, value, index, isSelected, isLeadSelection);
        }
    }

    static class DelegateCategoryProvider
    implements CategoryComponentProvider,
    CompositeCategoryProvider,
    Lookup.Provider {
        private static final String SELF = "Self";
        private final Lookup context;
        private final Map<Category, CompositeCategoryProvider> category2provider;
        private final DataFolder folder;
        private final CompositeCategoryProvider selfProvider;

        public DelegateCategoryProvider(DataFolder folder, Lookup context) {
            this(folder, context, new HashMap<Category, CompositeCategoryProvider>());
        }

        private DelegateCategoryProvider(DataFolder folder, Lookup context, Map<Category, CompositeCategoryProvider> cat2Provider) {
            this(folder, context, cat2Provider, null);
        }

        private DelegateCategoryProvider(DataFolder folder, Lookup context, Map<Category, CompositeCategoryProvider> cat2Provider, CompositeCategoryProvider sProv) {
            this.context = context;
            this.folder = folder;
            this.category2provider = cat2Provider;
            this.selfProvider = sProv;
        }

        @Override
        public JComponent create(Category category) {
            CompositeCategoryProvider prov = this.category2provider.get(category);
            assert (prov != null);
            return prov.createComponent(category, this.context);
        }

        Category[] getSubCategories() {
            return this.readCategories(this.folder);
        }

        Category[] readCategories(DataFolder folder) {
            ArrayList<Category> toRet = new ArrayList<Category>();
            for (DataObject dob : folder.getChildren()) {
                if (dob instanceof DataFolder) {
                    DataObject[] subDobs;
                    CompositeCategoryProvider sProvider = null;
                    for (DataObject subDob : subDobs = ((DataFolder)dob).getChildren()) {
                        if (!subDob.getName().equals("Self")) continue;
                        InstanceCookie cookie = (InstanceCookie)subDob.getLookup().lookup(InstanceCookie.class);
                        try {
                            if (cookie == null || !CompositeCategoryProvider.class.isAssignableFrom(cookie.instanceClass())) continue;
                            sProvider = (CompositeCategoryProvider)cookie.instanceCreate();
                            continue;
                        }
                        catch (IOException x) {
                            LOG.log(Level.WARNING, "Could not load " + (Object)subDob, x);
                            continue;
                        }
                        catch (ClassNotFoundException x) {
                            LOG.log(Level.WARNING, "Could not load " + (Object)subDob, x);
                        }
                    }
                    DelegateCategoryProvider prov = sProvider != null ? new DelegateCategoryProvider((DataFolder)dob, this.context, this.category2provider, sProvider) : new DelegateCategoryProvider((DataFolder)dob, this.context, this.category2provider);
                    Category cat = prov.createCategory(this.context);
                    toRet.add(cat);
                    this.category2provider.put(cat, prov);
                }
                if (dob.getName().equals("Self")) continue;
                InstanceCookie cook = (InstanceCookie)dob.getLookup().lookup(InstanceCookie.class);
                try {
                    Category cat;
                    CompositeCategoryProvider provider;
                    if (cook == null || !CompositeCategoryProvider.class.isAssignableFrom(cook.instanceClass()) || (provider = (CompositeCategoryProvider)cook.instanceCreate()) == null || (cat = provider.createCategory(this.context)) == null) continue;
                    toRet.add(cat);
                    this.category2provider.put(cat, provider);
                    this.includeSubcats(cat.getSubcategories(), provider);
                    continue;
                }
                catch (IOException x) {
                    LOG.log(Level.WARNING, "Could not load " + (Object)dob, x);
                    continue;
                }
                catch (ClassNotFoundException x) {
                    LOG.log(Level.WARNING, "Could not load " + (Object)dob, x);
                }
            }
            return toRet.toArray(new Category[toRet.size()]);
        }

        private void includeSubcats(Category[] cats, CompositeCategoryProvider provider) {
            if (cats != null) {
                for (Category cat : cats) {
                    this.category2provider.put(cat, provider);
                    this.includeSubcats(cat.getSubcategories(), provider);
                }
            }
        }

        @Override
        public Category createCategory(Lookup context) {
            FileObject fo = this.folder.getPrimaryFile();
            String dn = fo.getNameExt();
            try {
                dn = fo.getFileSystem().getStatus().annotateName(fo.getNameExt(), Collections.singleton(fo));
            }
            catch (FileStateInvalidException ex) {
                LOG.log(Level.WARNING, "Cannot retrieve display name for folder " + fo.getPath(), (Throwable)ex);
            }
            return Category.create(this.folder.getName(), dn, null, this.getSubCategories());
        }

        @Override
        public JComponent createComponent(Category category, Lookup context) {
            if (this.selfProvider != null) {
                return this.selfProvider.createComponent(category, context);
            }
            return new JPanel();
        }

        public Lookup getLookup() {
            return this.context;
        }
    }

    public static final class Category {
        private final String name;
        private final String displayName;
        private final Image icon;
        private final Category[] subcategories;
        private boolean valid;
        private String errorMessage;
        private ActionListener okListener;
        private ActionListener storeListener;
        private ActionListener closeListener;

        private Category(String name, String displayName, Image icon, Category[] subcategories) {
            this.name = name;
            this.displayName = displayName;
            this.icon = icon;
            this.subcategories = subcategories;
            this.valid = true;
        }

        public static /* varargs */ Category create(String name, String displayName, Image icon, Category ... subcategories) {
            return new Category(name, displayName, icon, subcategories);
        }

        public String getName() {
            return this.name;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public Image getIcon() {
            return this.icon;
        }

        public Category[] getSubcategories() {
            return this.subcategories;
        }

        public String getErrorMessage() {
            return this.errorMessage;
        }

        public boolean isValid() {
            return this.valid;
        }

        public void setValid(boolean valid) {
            if (this.valid != valid) {
                this.valid = valid;
                Utilities.getCategoryChangeSupport(this).firePropertyChange("isCategoryValid", !valid, valid);
            }
        }

        public void setErrorMessage(String message) {
            if (message == null) {
                message = "";
            }
            if (!message.equals(this.errorMessage)) {
                String oldMessage = this.errorMessage;
                this.errorMessage = message;
                Utilities.getCategoryChangeSupport(this).firePropertyChange("categoryErrorMessage", oldMessage, message);
            }
        }

        public void setOkButtonListener(ActionListener okButtonListener) {
            this.okListener = okButtonListener;
        }

        public ActionListener getOkButtonListener() {
            return this.okListener;
        }

        public void setStoreListener(ActionListener listener) {
            this.storeListener = listener;
        }

        public ActionListener getStoreListener() {
            return this.storeListener;
        }

        public void setCloseListener(ActionListener listener) {
            this.closeListener = listener;
        }

        public ActionListener getCloseListener() {
            return this.closeListener;
        }
    }

    public static interface CompositeCategoryProvider {
        public Category createCategory(Lookup var1);

        public JComponent createComponent(Category var1, Lookup var2);

        @Target(value={ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
        @Retention(value=RetentionPolicy.SOURCE)
        public static @interface Registrations {
            public Registration[] value();
        }

        @Target(value={ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
        @Retention(value=RetentionPolicy.SOURCE)
        public static @interface Registration {
            public String projectType();

            public String category() default "";

            public String categoryLabel() default "";

            public int position() default Integer.MAX_VALUE;
        }

    }

    public static interface CategoryComponentProvider {
        public JComponent create(Category var1);
    }

}

