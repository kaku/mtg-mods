/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.project.uiapi;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.util.NbBundle;

public class SavingProjectDataPanel
extends JPanel {
    JComponent component;
    private JLabel savingDataLabel;

    public SavingProjectDataPanel(JComponent component) {
        this.component = component;
        this.initComponents();
    }

    private void initComponents() {
        this.savingDataLabel = new JLabel();
        this.setPreferredSize(new Dimension(450, 50));
        this.setLayout(new GridBagLayout());
        this.savingDataLabel.setText(NbBundle.getMessage(SavingProjectDataPanel.class, (String)"LBL_savingDataLabel"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(8, 8, 0, 8);
        this.add((Component)this.savingDataLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 8, 8, 8);
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.component, gridBagConstraints);
    }
}

