/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.project.uiapi;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.project.uiapi.DefaultProjectOperationsImplementation;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.NbBundle;

public class DefaultProjectRenamePanel
extends JPanel
implements DocumentListener,
DefaultProjectOperationsImplementation.InvalidablePanel {
    private Project project;
    private final ChangeSupport changeSupport;
    private ProgressHandle handle;
    private JComponent progressComponent;
    private ProgressBar progressBar;
    private JCheckBox alsoRenameFolder;
    private JLabel errorMessage;
    private JLabel jLabel1;
    private JLabel jLabel3;
    private JLabel jLabel5;
    private JPanel jPanel4;
    private JPanel progress;
    private JPanel progressImpl;
    private JTextField projectFolder;
    private JLabel projectFolderLabel;
    private JTextField projectName;

    public DefaultProjectRenamePanel(ProgressHandle handle, Project project, String name) {
        this.changeSupport = new ChangeSupport((Object)this);
        this.project = project;
        this.handle = handle;
        if (name == null) {
            name = ProjectUtils.getInformation((Project)project).getDisplayName();
        }
        this.initComponents();
        this.projectName.setText(name);
        this.projectName.getDocument().addDocumentListener(this);
        this.updateProjectFolder();
        this.validateDialog();
        if (Boolean.getBoolean("org.netbeans.modules.project.uiapi.DefaultProjectOperations.showProgress")) {
            ((CardLayout)this.progress.getLayout()).show(this.progress, "progress");
        }
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        this.changeSupport.addChangeListener(l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        this.changeSupport.removeChangeListener(l);
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.projectFolderLabel = new JLabel();
        this.projectName = new JTextField();
        this.projectFolder = new JTextField();
        this.alsoRenameFolder = new JCheckBox();
        this.jLabel3 = new JLabel();
        this.errorMessage = new JLabel();
        this.progress = new JPanel();
        this.jPanel4 = new JPanel();
        this.progressImpl = new JPanel();
        this.jLabel5 = new JLabel();
        this.setMinimumSize(new Dimension(225, 250));
        this.setPreferredSize(new Dimension(542, 250));
        this.setLayout(new GridBagLayout());
        this.jLabel1.setLabelFor(this.projectName);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"LBL_Project_Name"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 0, 0, 12);
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.projectFolderLabel.setLabelFor(this.projectFolder);
        Mnemonics.setLocalizedText((JLabel)this.projectFolderLabel, (String)NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"LBL_Project_Folder"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 18, 0, 12);
        this.add((Component)this.projectFolderLabel, gridBagConstraints);
        this.projectName.setColumns(30);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.projectName, gridBagConstraints);
        this.projectName.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSN_Project_Name", (Object[])new Object[0]));
        this.projectName.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSD_Project_Name", (Object[])new Object[0]));
        this.projectFolder.setEditable(false);
        this.projectFolder.setColumns(30);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(6, 0, 0, 0);
        this.add((Component)this.projectFolder, gridBagConstraints);
        this.projectFolder.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSN_Project_Folder", (Object[])new Object[0]));
        this.projectFolder.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSD_Project_Folder", (Object[])new Object[0]));
        Mnemonics.setLocalizedText((AbstractButton)this.alsoRenameFolder, (String)NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"LBL_Also_Rename_Project_Folder"));
        this.alsoRenameFolder.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                DefaultProjectRenamePanel.this.alsoRenameFolderActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.alsoRenameFolder, gridBagConstraints);
        this.alsoRenameFolder.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSN_Also_Rename_Project_Folder", (Object[])new Object[0]));
        this.alsoRenameFolder.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSD_Also_Rename_Project_Folder", (Object[])new Object[0]));
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"LBL_Rename_Dialog_Text", (Object[])new Object[]{ProjectUtils.getInformation((Project)this.project).getDisplayName()}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 17;
        this.add((Component)this.jLabel3, gridBagConstraints);
        this.errorMessage.setForeground(UIManager.getColor("nb.errorForeground"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.errorMessage, gridBagConstraints);
        this.progress.setLayout(new CardLayout());
        this.progress.add((Component)this.jPanel4, "not-progress");
        this.progressComponent = ProgressHandleFactory.createProgressComponent((ProgressHandle)this.handle);
        this.progressImpl.add(this.progressComponent);
        this.progressImpl.setMinimumSize(new Dimension(121, 17));
        this.progressImpl.setPreferredSize(new Dimension(121, 17));
        this.progressImpl.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel5, (String)NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"LBL_Renaming_Project", (Object[])new Object[0]));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        this.progressImpl.add((Component)this.jLabel5, gridBagConstraints);
        this.progress.add((Component)this.progressImpl, "progress");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.progress, gridBagConstraints);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectRenamePanel.class, (String)"ACSD_Project_Rename", (Object[])new Object[0]));
    }

    private void alsoRenameFolderActionPerformed(ActionEvent evt) {
        this.updateProjectFolder();
        this.validateDialog();
    }

    public String getNewName() {
        return this.projectName.getText();
    }

    public boolean getRenameProjectFolder() {
        return this.alsoRenameFolder.isSelected();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.updateProjectFolder();
        this.validateDialog();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.updateProjectFolder();
        this.validateDialog();
    }

    private void updateProjectFolder() {
        File location = FileUtil.toFile((FileObject)this.project.getProjectDirectory().getParent());
        File projectFolderFile = this.alsoRenameFolder.isSelected() ? new File(location, this.projectName.getText()) : new File(location, this.project.getProjectDirectory().getNameExt());
        this.projectFolder.setText(projectFolderFile.getAbsolutePath());
    }

    @Override
    public boolean isPanelValid() {
        return " ".equals(this.errorMessage.getText());
    }

    private void validateDialog() {
        String newError = this.computeError();
        boolean changed = false;
        String currentError = this.errorMessage.getText();
        newError = newError != null ? newError : " ";
        changed = !currentError.equals(newError);
        this.errorMessage.setText(newError);
        if (changed) {
            this.changeSupport.fireChange();
        }
    }

    private String computeError() {
        File location = FileUtil.toFile((FileObject)this.project.getProjectDirectory().getParent());
        return DefaultProjectOperationsImplementation.computeError(location, this.projectName.getText(), !this.getRenameProjectFolder());
    }

    @Override
    public void showProgress() {
        this.projectFolder.setEnabled(false);
        this.projectName.setEnabled(false);
        this.alsoRenameFolder.setEnabled(false);
        this.progress.setVisible(true);
        ((CardLayout)this.progress.getLayout()).last(this.progress);
    }

    protected void addProgressBar() {
        this.progressBar = ProgressBar.create(this.progressComponent);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        this.progressImpl.add((Component)this.progressBar, gridBagConstraints);
        this.progressImpl.repaint();
        this.progressImpl.revalidate();
    }

    protected void removeProgressBar() {
        this.progressImpl.remove(this.progressBar);
    }

    private static class ProgressBar
    extends JPanel {
        private JLabel label;

        private static ProgressBar create(JComponent progress) {
            ProgressBar instance = new ProgressBar();
            instance.setLayout(new BorderLayout());
            instance.label = new JLabel(" ");
            instance.label.setBorder(new EmptyBorder(0, 0, 2, 0));
            instance.add((Component)instance.label, "North");
            instance.add((Component)progress, "Center");
            return instance;
        }

        public void setString(String value) {
            this.label.setText(value);
        }

        private ProgressBar() {
        }
    }

}

