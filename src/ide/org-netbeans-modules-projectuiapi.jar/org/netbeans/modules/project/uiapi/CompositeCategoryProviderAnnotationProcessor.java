/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.project.uiapi;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class CompositeCategoryProviderAnnotationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(ProjectCustomizer.CompositeCategoryProvider.Registration.class.getCanonicalName(), ProjectCustomizer.CompositeCategoryProvider.Registrations.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(ProjectCustomizer.CompositeCategoryProvider.Registration.class)) {
            ProjectCustomizer.CompositeCategoryProvider.Registration r = e2.getAnnotation(ProjectCustomizer.CompositeCategoryProvider.Registration.class);
            if (r == null) continue;
            this.handle(e2, r);
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(ProjectCustomizer.CompositeCategoryProvider.Registrations.class)) {
            ProjectCustomizer.CompositeCategoryProvider.Registrations rr = e.getAnnotation(ProjectCustomizer.CompositeCategoryProvider.Registrations.class);
            if (rr == null) continue;
            for (ProjectCustomizer.CompositeCategoryProvider.Registration r : rr.value()) {
                this.handle(e, r);
            }
        }
        return true;
    }

    private void handle(Element e, ProjectCustomizer.CompositeCategoryProvider.Registration r) throws LayerGenerationException {
        boolean addsFolder;
        String path = "Projects/" + r.projectType() + "/Customizer";
        if (r.category().length() > 0) {
            path = path + "/" + r.category();
        }
        boolean bl = addsFolder = r.categoryLabel().length() > 0;
        if (addsFolder) {
            this.handleFolder(path, e, r);
        }
        if (e.getKind() == ElementKind.PACKAGE) {
            if (!addsFolder) {
                throw new LayerGenerationException("Must specify categoryLabel", e, this.processingEnv, (Annotation)r);
            }
        } else {
            LayerBuilder.File f = this.layer(new Element[]{e}).instanceFile(path, addsFolder ? "Self" : null, ProjectCustomizer.CompositeCategoryProvider.class, (Annotation)r, null);
            f.position(addsFolder ? 0 : r.position());
            f.write();
        }
    }

    private void handleFolder(String path, Element e, ProjectCustomizer.CompositeCategoryProvider.Registration r) throws LayerGenerationException {
        if (r.category().length() == 0) {
            throw new LayerGenerationException("Must specify category", e, this.processingEnv, (Annotation)r);
        }
        this.layer(new Element[]{e}).folder(path).bundlevalue("displayName", r.categoryLabel(), (Annotation)r, "categoryLabel").position(r.position()).write();
    }
}

