/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.project.Project
 */
package org.netbeans.modules.project.uiapi;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.project.Project;

public interface BrokenReferencesImplementation {
    public void showAlert(@NonNull Project var1);

    public void showCustomizer(@NonNull Project var1);
}

