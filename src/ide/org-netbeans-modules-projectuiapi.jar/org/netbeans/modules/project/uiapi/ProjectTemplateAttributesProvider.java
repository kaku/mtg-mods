/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.queries.FileEncodingQuery
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.CreateFromTemplateAttributesProvider
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.NbCollections
 */
package org.netbeans.modules.project.uiapi;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.CreateFromTemplateAttributesProvider;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.NbCollections;

public final class ProjectTemplateAttributesProvider
implements CreateFromTemplateAttributesProvider {
    private static final String ATTR_PROJECT = "project";
    private static final String ATTR_LICENSE = "license";
    private static final String ATTR_LICENSE_PATH = "licensePath";
    private static final String ATTR_ENCODING = "encoding";

    public Map<String, ? extends Object> attributesFor(DataObject template, DataFolder target, String name) {
        Project prj = FileOwnerQuery.getOwner((FileObject)target.getPrimaryFile());
        HashMap all = null;
        if (prj != null) {
            for (CreateFromTemplateAttributesProvider attrs : prj.getLookup().lookupAll(CreateFromTemplateAttributesProvider.class)) {
                Map m = attrs.attributesFor(template, target, name);
                if (m == null) continue;
                if (all == null) {
                    all = new HashMap();
                }
                all.putAll(m);
            }
        }
        return ProjectTemplateAttributesProvider.checkProjectAttrs(all, target.getPrimaryFile());
    }

    static Map<String, ? extends Object> checkProjectAttrs(Map<String, Object> m, FileObject parent) {
        Object prjAttrObj;
        Object object = prjAttrObj = m != null ? m.get("project") : null;
        if (prjAttrObj instanceof Map) {
            Map prjAttrs = NbCollections.checkedMapByFilter((Map)((Map)prjAttrObj), String.class, Object.class, (boolean)false);
            HashMap<String, Object> newPrjAttrs = new HashMap<String, Object>(prjAttrs);
            m.put("project", newPrjAttrs);
            ProjectTemplateAttributesProvider.ensureProjectAttrs(newPrjAttrs, parent);
            return m;
        }
        if (prjAttrObj != null) {
            return m;
        }
        HashMap<String, Object> projectMap = new HashMap<String, Object>();
        ProjectTemplateAttributesProvider.ensureProjectAttrs(projectMap, parent);
        if (m != null) {
            m.put("project", projectMap);
            return m;
        }
        return Collections.singletonMap("project", projectMap);
    }

    private static void ensureProjectAttrs(Map<String, Object> map, FileObject parent) {
        String url;
        if (map.get("license") == null) {
            map.put("license", "default");
        }
        if (map.get("licensePath") == null) {
            map.put("licensePath", "Templates/Licenses/license-" + map.get("license").toString() + ".txt");
        }
        if (FileUtil.getConfigFile((String)(url = map.get("licensePath").toString())) == null) {
            try {
                URI uri = URI.create(url);
                map.put("licensePath", new URI("file", "", uri.getPath(), null).toString());
            }
            catch (Exception malformedURLException) {}
        } else {
            map.put("licensePath", "/" + url);
        }
        if (map.get("encoding") == null) {
            Charset charset = FileEncodingQuery.getEncoding((FileObject)parent);
            String encoding = charset != null ? charset.name() : "UTF-8";
            map.put("encoding", encoding);
        }
    }
}

