/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.project.uiapi;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.project.uiapi.DefaultProjectOperationsImplementation;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

final class DefaultProjectDeletePanel
extends JPanel
implements DefaultProjectOperationsImplementation.InvalidablePanel {
    private String projectDisplaName;
    private String projectFolder;
    private boolean enableCheckbox;
    private ProgressHandle handle;
    private JComponent progressComponent;
    private ProgressBar progressBar;
    private JCheckBox deleteSourcesCheckBox;
    private JLabel jLabel5;
    private JPanel jPanel4;
    private JPanel progress;
    private JPanel progressImpl;
    private JLabel questionLabel;
    private JTextArea warningText;

    public DefaultProjectDeletePanel(ProgressHandle handle, String projectDisplaName, String projectFolder, boolean enableCheckbox) {
        this.projectDisplaName = projectDisplaName;
        this.projectFolder = projectFolder;
        this.enableCheckbox = enableCheckbox;
        this.handle = handle;
        this.initComponents();
        if (Boolean.getBoolean("org.netbeans.modules.project.uiapi.DefaultProjectOperations.showProgress")) {
            ((CardLayout)this.progress.getLayout()).show(this.progress, "progress");
        }
    }

    private void initComponents() {
        this.warningText = new JTextArea();
        this.deleteSourcesCheckBox = new JCheckBox();
        this.progress = new JPanel();
        this.jPanel4 = new JPanel();
        this.progressImpl = new JPanel();
        this.jLabel5 = new JLabel();
        this.questionLabel = new JLabel();
        this.setLayout(new GridBagLayout());
        this.warningText.setEditable(false);
        this.warningText.setFont(UIManager.getFont("Label.font"));
        this.warningText.setLineWrap(true);
        this.warningText.setText(NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"LBL_Pre_Delete_Warning", (Object[])new Object[]{this.projectDisplaName}));
        this.warningText.setWrapStyleWord(true);
        this.warningText.setDisabledTextColor(UIManager.getColor("Label.foreground"));
        this.warningText.setOpaque(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 12, 0, 12);
        this.add((Component)this.warningText, gridBagConstraints);
        this.warningText.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"ASCN_Pre_Delete_Warning", (Object[])new Object[0]));
        this.warningText.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"ACSD_Pre_Delete_Warning", (Object[])new Object[]{this.projectDisplaName}));
        Mnemonics.setLocalizedText((AbstractButton)this.deleteSourcesCheckBox, (String)NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"LBL_Delete_Also_Sources", (Object[])new Object[]{this.projectFolder}));
        this.deleteSourcesCheckBox.setEnabled(this.enableCheckbox);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 12, 0, 12);
        this.add((Component)this.deleteSourcesCheckBox, gridBagConstraints);
        this.deleteSourcesCheckBox.getAccessibleContext().setAccessibleName(NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"ACSN_Delete_Also_Sources", (Object[])new Object[]{this.projectFolder}));
        this.deleteSourcesCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"ACSD_Delete_Also_Sources", (Object[])new Object[0]));
        this.progress.setLayout(new CardLayout());
        this.progress.add((Component)this.jPanel4, "not-progress");
        this.progressComponent = ProgressHandleFactory.createProgressComponent((ProgressHandle)this.handle);
        this.progressImpl.add(this.progressComponent);
        this.progressImpl.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel5, (String)NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"LBL_Deleting_Project", (Object[])new Object[0]));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        this.progressImpl.add((Component)this.jLabel5, gridBagConstraints);
        this.progress.add((Component)this.progressImpl, "progress");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 0.8;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.progress, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.questionLabel, (String)NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"LBL_Pre_Delete_Question"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(10, 12, 0, 0);
        this.add((Component)this.questionLabel, gridBagConstraints);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectDeletePanel.class, (String)"ACSD_Delete_Panel", (Object[])new Object[0]));
    }

    public boolean isDeleteSources() {
        return this.deleteSourcesCheckBox.isSelected();
    }

    void setDeleteSources(boolean value) {
        this.deleteSourcesCheckBox.setSelected(value);
    }

    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }

    @Override
    public void showProgress() {
        this.deleteSourcesCheckBox.setEnabled(false);
        ((CardLayout)this.progress.getLayout()).last(this.progress);
    }

    @Override
    public boolean isPanelValid() {
        return true;
    }

    protected void addProgressBar() {
        this.progressBar = ProgressBar.create(this.progressComponent);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        this.progressImpl.add((Component)this.progressBar, gridBagConstraints);
    }

    protected void removeProgressBar() {
        this.progressImpl.remove(this.progressBar);
    }

    private static class ProgressBar
    extends JPanel {
        private JLabel label;

        private static ProgressBar create(JComponent progress) {
            ProgressBar instance = new ProgressBar();
            instance.setLayout(new BorderLayout());
            instance.label = new JLabel(" ");
            instance.label.setBorder(new EmptyBorder(0, 0, 2, 0));
            instance.add((Component)instance.label, "North");
            instance.add((Component)progress, "Center");
            return instance;
        }

        public void setString(String value) {
            this.label.setText(value);
        }

        private ProgressBar() {
        }
    }

}

