/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 */
package org.netbeans.modules.project.uiapi;

import javax.swing.Action;
import javax.swing.Icon;
import org.netbeans.spi.project.ui.support.FileActionPerformer;
import org.netbeans.spi.project.ui.support.ProjectActionPerformer;
import org.openide.util.ContextAwareAction;

public interface ActionsFactory {
    public Action setAsMainProjectAction();

    public Action customizeProjectAction();

    public Action openSubprojectsAction();

    public Action closeProjectAction();

    public Action newFileAction();

    public Action deleteProjectAction();

    public Action copyProjectAction();

    public Action moveProjectAction();

    public Action newProjectAction();

    public ContextAwareAction projectCommandAction(String var1, String var2, Icon var3);

    public Action projectSensitiveAction(ProjectActionPerformer var1, String var2, Icon var3);

    public Action mainProjectCommandAction(String var1, String var2, Icon var3);

    public Action mainProjectSensitiveAction(ProjectActionPerformer var1, String var2, Icon var3);

    public Action fileCommandAction(String var1, String var2, Icon var3);

    public Action fileSensitiveAction(FileActionPerformer var1, String var2, Icon var3);

    public Action renameProjectAction();

    public Action setProjectConfigurationAction();
}

