/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.SourceGroup
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 */
package org.netbeans.modules.project.uiapi;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.ui.ProjectGroup;
import org.netbeans.api.project.ui.ProjectGroupChangeListener;
import org.netbeans.modules.project.uiapi.ActionsFactory;
import org.netbeans.modules.project.uiapi.BuildExecutionSupportImplementation;
import org.netbeans.modules.project.uiapi.CategoryChangeSupport;
import org.netbeans.modules.project.uiapi.OpenProjectsTrampoline;
import org.netbeans.modules.project.uiapi.ProjectChooserFactory;
import org.netbeans.spi.project.ui.support.BuildExecutionSupport;
import org.netbeans.spi.project.ui.support.FileActionPerformer;
import org.netbeans.spi.project.ui.support.ProjectActionPerformer;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileUtil;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public class Utilities {
    private static final Logger LOG = Logger.getLogger(Utilities.class.getName());
    private static final Map<ProjectCustomizer.Category, CategoryChangeSupport> CATEGORIES = new HashMap<ProjectCustomizer.Category, CategoryChangeSupport>();
    @SuppressWarnings(value={"MS_SHOULD_BE_FINAL"})
    public static ProjectGroupAccessor ACCESSOR = null;

    private Utilities() {
    }

    public static ActionsFactory getActionsFactory() {
        ActionsFactory instance = (ActionsFactory)Lookup.getDefault().lookup(ActionsFactory.class);
        return instance != null ? instance : new ActionsFactory(){

            @Override
            public Action setAsMainProjectAction() {
                return new Dummy("setAsMainProject");
            }

            @Override
            public Action customizeProjectAction() {
                return new Dummy("customizeProject");
            }

            @Override
            public Action openSubprojectsAction() {
                return new Dummy("openSubprojects");
            }

            @Override
            public Action closeProjectAction() {
                return new Dummy("closeProject");
            }

            @Override
            public Action newFileAction() {
                return new Dummy("newFile");
            }

            @Override
            public Action deleteProjectAction() {
                return new Dummy("deleteProject");
            }

            @Override
            public Action copyProjectAction() {
                return new Dummy("copyProject");
            }

            @Override
            public Action moveProjectAction() {
                return new Dummy("moveProject");
            }

            @Override
            public Action newProjectAction() {
                return new Dummy("newProject");
            }

            @Override
            public Action renameProjectAction() {
                return new Dummy("renameProject");
            }

            @Override
            public Action setProjectConfigurationAction() {
                return new Dummy("setProjectConfiguration");
            }

            @Override
            public ContextAwareAction projectCommandAction(String command, String namePattern, Icon icon) {
                return new Dummy("projectCommand:" + command);
            }

            @Override
            public Action projectSensitiveAction(ProjectActionPerformer performer, String name, Icon icon) {
                return new Dummy("projectSensitive");
            }

            @Override
            public Action mainProjectCommandAction(String command, String name, Icon icon) {
                return new Dummy("mainProjectCommand:" + command);
            }

            @Override
            public Action mainProjectSensitiveAction(ProjectActionPerformer performer, String name, Icon icon) {
                return new Dummy("mainProjectSensitive");
            }

            @Override
            public Action fileCommandAction(String command, String name, Icon icon) {
                return new Dummy("fileCommand:" + command);
            }

            @Override
            public Action fileSensitiveAction(FileActionPerformer performer, String name, Icon icon) {
                return new Dummy("fileCommand");
            }

            class Dummy
            extends AbstractAction
            implements ContextAwareAction {
                Dummy(String label) {
                    super(label);
                }

                @Override
                public boolean isEnabled() {
                    return false;
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    assert (false);
                }

                public Action createContextAwareInstance(Lookup actionContext) {
                    return this;
                }
            }

        };
    }

    public static BuildExecutionSupportImplementation getBuildExecutionSupportImplementation() {
        BuildExecutionSupportImplementation instance = (BuildExecutionSupportImplementation)Lookup.getDefault().lookup(BuildExecutionSupportImplementation.class);
        return instance != null ? instance : new BuildExecutionSupportImplementation(){

            @Override
            public void registerFinishedItem(BuildExecutionSupport.Item item) {
            }

            @Override
            public void registerRunningItem(BuildExecutionSupport.Item item) {
            }

            @Override
            public void addChangeListener(ChangeListener listener) {
            }

            @Override
            public void removeChangeListener(ChangeListener listener) {
            }

            @Override
            public BuildExecutionSupport.Item getLastItem() {
                return null;
            }

            @Override
            public List<BuildExecutionSupport.Item> getRunningItems() {
                return Collections.emptyList();
            }
        };
    }

    public static ProjectChooserFactory getProjectChooserFactory() {
        ProjectChooserFactory instance = (ProjectChooserFactory)Lookup.getDefault().lookup(ProjectChooserFactory.class);
        return instance != null ? instance : new ProjectChooserFactory(){
            File projectsFolder;

            @Override
            public File getProjectsFolder() {
                return this.projectsFolder != null ? this.projectsFolder : FileUtil.normalizeFile((File)new File(System.getProperty("java.io.tmpdir", "")));
            }

            @Override
            public void setProjectsFolder(File file) {
                this.projectsFolder = file;
            }

            @Override
            public JFileChooser createProjectChooser() {
                JFileChooser jfc = new JFileChooser();
                jfc.setFileSelectionMode(1);
                return jfc;
            }

            @Override
            public WizardDescriptor.Panel<WizardDescriptor> createSimpleTargetChooser(Project project, SourceGroup[] folders, WizardDescriptor.Panel<WizardDescriptor> bottomPanel, boolean freeFileExtension) {
                return new WizardDescriptor.Panel<WizardDescriptor>(){

                    public Component getComponent() {
                        return new JPanel();
                    }

                    public HelpCtx getHelp() {
                        return null;
                    }

                    public void readSettings(WizardDescriptor settings) {
                    }

                    public void storeSettings(WizardDescriptor settings) {
                    }

                    public boolean isValid() {
                        return false;
                    }

                    public void addChangeListener(ChangeListener l) {
                    }

                    public void removeChangeListener(ChangeListener l) {
                    }
                };
            }

        };
    }

    public static OpenProjectsTrampoline getOpenProjectsTrampoline() {
        OpenProjectsTrampoline instance = (OpenProjectsTrampoline)Lookup.getDefault().lookup(OpenProjectsTrampoline.class);
        return instance != null ? instance : new OpenProjectsTrampoline(){
            final PropertyChangeSupport pcs;
            final Collection<Project> open;
            Project main;

            @Override
            public Project[] getOpenProjectsAPI() {
                return this.open.toArray((T[])new Project[this.open.size()]);
            }

            @Override
            public void openAPI(Project[] projects, boolean openRequiredProjects, boolean showProgress) {
                this.open.addAll(Arrays.asList(projects));
                this.pcs.firePropertyChange("openProjects", null, null);
            }

            @Override
            public void closeAPI(Project[] projects) {
                this.open.removeAll(Arrays.asList(projects));
                this.pcs.firePropertyChange("openProjects", null, null);
            }

            @Override
            public Future<Project[]> openProjectsAPI() {
                return RequestProcessor.getDefault().submit((Callable)new Callable<Project[]>(){

                    @Override
                    public Project[] call() {
                        return 4.this.getOpenProjectsAPI();
                    }
                });
            }

            @Override
            public Project getMainProject() {
                return this.main;
            }

            @Override
            public void setMainProject(Project project) {
                this.main = project;
                this.pcs.firePropertyChange("MainProject", null, null);
            }

            @Override
            public void addPropertyChangeListenerAPI(PropertyChangeListener listener, Object source) {
                this.pcs.addPropertyChangeListener(listener);
            }

            @Override
            public void removePropertyChangeListenerAPI(PropertyChangeListener listener) {
                this.pcs.removePropertyChangeListener(listener);
            }

            @Override
            public void addProjectGroupChangeListenerAPI(ProjectGroupChangeListener listener) {
            }

            @Override
            public void removeProjectGroupChangeListenerAPI(ProjectGroupChangeListener listener) {
            }

            @Override
            public ProjectGroup getActiveProjectGroupAPI() {
                return null;
            }

        };
    }

    public static CategoryChangeSupport getCategoryChangeSupport(ProjectCustomizer.Category category) {
        CategoryChangeSupport cw = CATEGORIES.get(category);
        return cw == null ? CategoryChangeSupport.NULL_INSTANCE : cw;
    }

    public static void putCategoryChangeSupport(ProjectCustomizer.Category category, CategoryChangeSupport wrapper) {
        CATEGORIES.put(category, wrapper);
    }

    public static void removeCategoryChangeSupport(ProjectCustomizer.Category category) {
        CATEGORIES.remove(category);
    }

    static {
        Class<ProjectGroup> c = ProjectGroup.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            LOG.log(Level.SEVERE, "very wrong, very wrong, yes indeed", ex);
        }
    }

    public static abstract class ProjectGroupAccessor {
        public abstract ProjectGroup createGroup(String var1, Preferences var2);
    }

}

