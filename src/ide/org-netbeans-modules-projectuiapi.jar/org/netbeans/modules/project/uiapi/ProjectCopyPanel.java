/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.spi.project.support.ProjectOperations
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.project.uiapi;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.project.uiapi.DefaultProjectOperationsImplementation;
import org.netbeans.spi.project.support.ProjectOperations;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.NbBundle;

public class ProjectCopyPanel
extends JPanel
implements DocumentListener,
DefaultProjectOperationsImplementation.InvalidablePanel {
    private Project project;
    private boolean isMove;
    private boolean invalid;
    private final ChangeSupport changeSupport;
    private ProgressHandle handle;
    private JButton browse;
    private JLabel errorMessage;
    private JLabel extSourcesWarning;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JLabel nameLabel;
    private JPanel progress;
    private JPanel progressImpl;
    private JTextField projectFolder;
    private JTextField projectLocation;
    private JTextField projectName;
    private JTextArea warningTextArea;
    private String lastComputedName;

    public ProjectCopyPanel(ProgressHandle handle, Project project, boolean isMove) {
        this.changeSupport = new ChangeSupport((Object)this);
        this.project = project;
        this.isMove = isMove;
        this.handle = handle;
        this.initComponents();
        this.setProject();
        this.projectName.getDocument().addDocumentListener(this);
        this.projectLocation.getDocument().addDocumentListener(this);
        if (isMove) {
            this.nameLabel.setVisible(false);
            this.projectName.setVisible(false);
            this.warningTextArea.setVisible(false);
        }
        if (Boolean.getBoolean("org.netbeans.modules.project.uiapi.DefaultProjectOperations.showProgress")) {
            ((CardLayout)this.progress.getLayout()).show(this.progress, "progress");
        }
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        this.changeSupport.addChangeListener(l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        this.changeSupport.removeChangeListener(l);
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.projectLocation = new JTextField();
        this.nameLabel = new JLabel();
        this.projectName = new JTextField();
        this.browse = new JButton();
        this.jLabel4 = new JLabel();
        this.projectFolder = new JTextField();
        this.extSourcesWarning = new JLabel();
        this.errorMessage = new JLabel();
        this.progress = new JPanel();
        this.jPanel4 = new JPanel();
        this.progressImpl = new JPanel();
        this.jLabel5 = new JLabel();
        this.jPanel3 = new JPanel();
        this.warningTextArea = new JTextArea();
        this.setLayout(new GridBagLayout());
        Object[] arrobject = new Object[2];
        arrobject[0] = new Integer(this.isMove ? 1 : 0);
        arrobject[1] = ProjectUtils.getInformation((Project)this.project).getDisplayName();
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_Copy_Move_Dialog_Text", (Object[])arrobject));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.jLabel2.setLabelFor(this.projectLocation);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_Project_Location"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 0, 0, 12);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this.projectLocation.setColumns(30);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 0, 5);
        this.add((Component)this.projectLocation, gridBagConstraints);
        this.projectLocation.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSN_Project_Location", (Object[])new Object[0]));
        this.projectLocation.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSD_Project_Location", (Object[])new Object[0]));
        this.nameLabel.setLabelFor(this.projectName);
        Mnemonics.setLocalizedText((JLabel)this.nameLabel, (String)NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_Project_Name"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 0, 0, 12);
        this.add((Component)this.nameLabel, gridBagConstraints);
        this.projectName.setColumns(30);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 5);
        this.add((Component)this.projectName, gridBagConstraints);
        this.projectName.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSN_Project_Name", (Object[])new Object[0]));
        this.projectName.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSD_Project_Name", (Object[])new Object[0]));
        Mnemonics.setLocalizedText((AbstractButton)this.browse, (String)NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_Browse", (Object[])new Object[0]));
        this.browse.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProjectCopyPanel.this.browseActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new Insets(6, 0, 0, 0);
        this.add((Component)this.browse, gridBagConstraints);
        this.browse.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSD_Browse", (Object[])new Object[0]));
        this.jLabel4.setLabelFor(this.projectFolder);
        Mnemonics.setLocalizedText((JLabel)this.jLabel4, (String)NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_Project_Folder"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 0, 0, 12);
        this.add((Component)this.jLabel4, gridBagConstraints);
        this.projectFolder.setColumns(30);
        this.projectFolder.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(6, 0, 0, 5);
        this.add((Component)this.projectFolder, gridBagConstraints);
        this.projectFolder.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSN_Project_Folder", (Object[])new Object[0]));
        this.projectFolder.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSD_Project_Folder", (Object[])new Object[0]));
        this.extSourcesWarning.setForeground(UIManager.getColor("nb.errorForeground"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.extSourcesWarning, gridBagConstraints);
        this.errorMessage.setForeground(UIManager.getColor("nb.errorForeground"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.errorMessage, gridBagConstraints);
        this.progress.setLayout(new CardLayout());
        this.progress.add((Component)this.jPanel4, "not-progress");
        this.progressImpl.setLayout(new GridBagLayout());
        Object[] arrobject2 = new Object[1];
        arrobject2[0] = this.isMove ? new Integer(1) : new Integer(0);
        Mnemonics.setLocalizedText((JLabel)this.jLabel5, (String)NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_Copying_Moving", (Object[])arrobject2));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        this.progressImpl.add((Component)this.jLabel5, gridBagConstraints);
        this.jPanel3.add(ProgressHandleFactory.createProgressComponent((ProgressHandle)this.handle));
        this.jPanel3.setLayout(new BorderLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.progressImpl.add((Component)this.jPanel3, gridBagConstraints);
        this.progress.add((Component)this.progressImpl, "progress");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new Insets(8, 0, 0, 0);
        this.add((Component)this.progress, gridBagConstraints);
        this.warningTextArea.setColumns(20);
        this.warningTextArea.setEditable(false);
        this.warningTextArea.setForeground(UIManager.getColor("nb.errorForeground"));
        this.warningTextArea.setLineWrap(true);
        this.warningTextArea.setRows(5);
        this.warningTextArea.setText(NbBundle.getMessage(ProjectCopyPanel.class, (String)"VCSWarningMessage"));
        this.warningTextArea.setWrapStyleWord(true);
        this.warningTextArea.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(8, 0, 0, 0);
        this.add((Component)this.warningTextArea, gridBagConstraints);
        Object[] arrobject3 = new Object[1];
        arrobject3[0] = new Integer(this.isMove ? 1 : 0);
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProjectCopyPanel.class, (String)"ACSD_Copy_Move_Panel", (Object[])arrobject3));
    }

    private void browseActionPerformed(ActionEvent evt) {
        String dlgTitle = NbBundle.getMessage(ProjectCopyPanel.class, (String)"TITLE_BrowseProjectLocation");
        String okText = NbBundle.getMessage(ProjectCopyPanel.class, (String)"LBL_BrowseProjectLocation_OK_Button");
        File dir = new FileChooserBuilder(ProjectCopyPanel.class).setDefaultWorkingDirectory(new File(this.projectLocation.getText())).setDirectoriesOnly(true).setTitle(dlgTitle).setApproveText(okText).showOpenDialog();
        if (dir != null) {
            this.projectLocation.setText(dir.getAbsolutePath());
        }
    }

    private String computeValidProjectName(String projectLocation, String projectNamePrefix) {
        File location = new File(projectLocation);
        if (!location.exists()) {
            this.lastComputedName = projectNamePrefix;
            return projectNamePrefix;
        }
        int num = 1;
        String projectName = projectNamePrefix;
        if (new File(location, projectName).exists()) {
            while (new File(location, projectName = projectNamePrefix + "_" + num).exists()) {
                ++num;
            }
        }
        this.lastComputedName = projectName;
        return projectName;
    }

    private void setProject() {
        FileObject parent = this.project.getProjectDirectory().getParent();
        File parentFile = FileUtil.toFile((FileObject)parent);
        String parentPath = parentFile != null ? parentFile.getAbsolutePath() : System.getProperty("java.io.tmpdir");
        this.projectLocation.setText(parentPath);
        if (this.isMove) {
            this.projectName.setText(ProjectUtils.getInformation((Project)this.project).getName());
        } else {
            this.projectName.setText(this.computeValidProjectName(parentPath, ProjectUtils.getInformation((Project)this.project).getName()));
        }
        this.updateProjectFolder();
        this.validateDialog();
        if (this.hasExternalSources() && !this.isMove) {
            this.extSourcesWarning.setText(NbBundle.getMessage(ProjectCopyPanel.class, (String)"WRN_External_Sources"));
            this.invalid = true;
        }
    }

    private boolean hasExternalSources() {
        FileObject projectDir = this.project.getProjectDirectory();
        for (FileObject file : ProjectOperations.getDataFiles((Project)this.project)) {
            if (FileUtil.isParentOf((FileObject)projectDir, (FileObject)file) || projectDir.equals((Object)file)) continue;
            return true;
        }
        return false;
    }

    public String getNewName() {
        return this.projectName.getText();
    }

    public String getProjectFolderName() {
        return this.project.getProjectDirectory().getNameExt();
    }

    public File getNewDirectory() {
        return new File(this.projectLocation.getText());
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        if (e.getDocument().equals(this.projectLocation.getDocument()) && this.lastComputedName != null && this.lastComputedName.equals(this.projectName.getText())) {
            this.projectName.setText(this.computeValidProjectName(new File(this.projectLocation.getText()).getAbsolutePath(), ProjectUtils.getInformation((Project)this.project).getName()));
        }
        this.updateProjectFolder();
        this.validateDialog();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (e.getDocument().equals(this.projectLocation.getDocument()) && this.lastComputedName != null && this.lastComputedName.equals(this.projectName.getText())) {
            this.projectName.setText(this.computeValidProjectName(new File(this.projectLocation.getText()).getAbsolutePath(), ProjectUtils.getInformation((Project)this.project).getName()));
        }
        this.updateProjectFolder();
        this.validateDialog();
    }

    private void updateProjectFolder() {
        File location;
        File projectFolderFile = location = new File(this.projectLocation.getText());
        projectFolderFile = this.isMove ? new File(location, this.project.getProjectDirectory().getNameExt()) : new File(location, this.projectName.getText());
        this.projectFolder.setText(projectFolderFile.getAbsolutePath());
    }

    @Override
    public boolean isPanelValid() {
        return " ".equals(this.errorMessage.getText()) && !this.invalid;
    }

    private void validateDialog() {
        if (this.invalid) {
            return;
        }
        String newError = this.computeError();
        boolean changed = false;
        String currentError = this.errorMessage.getText();
        newError = newError != null ? newError : " ";
        changed = !currentError.equals(newError);
        this.errorMessage.setText(newError);
        if (changed) {
            this.changeSupport.fireChange();
        }
    }

    private String computeError() {
        File location = new File(this.projectLocation.getText());
        return DefaultProjectOperationsImplementation.computeError(location, this.projectName.getText(), this.projectFolder.getText(), false);
    }

    @Override
    public void showProgress() {
        this.projectFolder.setEnabled(false);
        this.projectLocation.setEnabled(false);
        this.projectName.setEnabled(false);
        this.browse.setEnabled(false);
        ((CardLayout)this.progress.getLayout()).show(this.progress, "progress");
    }

}

