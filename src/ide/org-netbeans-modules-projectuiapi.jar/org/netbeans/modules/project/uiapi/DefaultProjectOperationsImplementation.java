/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectManager
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.queries.SharabilityQuery
 *  org.netbeans.api.queries.SharabilityQuery$Sharability
 *  org.netbeans.api.queries.VisibilityQuery
 *  org.netbeans.spi.project.MoveOperationImplementation
 *  org.netbeans.spi.project.MoveOrRenameOperationImplementation
 *  org.netbeans.spi.project.support.ProjectOperations
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.project.uiapi;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.api.queries.SharabilityQuery;
import org.netbeans.api.queries.VisibilityQuery;
import org.netbeans.modules.project.uiapi.DefaultProjectDeletePanel;
import org.netbeans.modules.project.uiapi.DefaultProjectRenamePanel;
import org.netbeans.modules.project.uiapi.ProjectCopyPanel;
import org.netbeans.spi.project.MoveOperationImplementation;
import org.netbeans.spi.project.MoveOrRenameOperationImplementation;
import org.netbeans.spi.project.support.ProjectOperations;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public final class DefaultProjectOperationsImplementation {
    private static final Logger LOG = Logger.getLogger(DefaultProjectOperationsImplementation.class.getName());
    private static final double NOTIFY_WORK = 0.1;
    private static final double FIND_PROJECT_WORK = 0.1;
    static final int MAX_WORK = 100;

    private DefaultProjectOperationsImplementation() {
    }

    private static String getDisplayName(Project project) {
        return ProjectUtils.getInformation((Project)project).getDisplayName();
    }

    private static void performDelete(Project project, List<FileObject> toDelete, ProgressHandle handle) throws Exception {
        try {
            handle.start(toDelete.size() + 1);
            int done = 0;
            handle.progress(NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Progress_Cleaning_Project"));
            ProjectOperations.notifyDeleting((Project)project);
            handle.progress(++done);
            for (FileObject f : toDelete) {
                handle.progress(NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Progress_Deleting_File", (Object)FileUtil.getFileDisplayName((FileObject)f)));
                if (f != null && f.isValid()) {
                    f.delete();
                }
                handle.progress(++done);
            }
            FileObject projectFolder = project.getProjectDirectory();
            projectFolder.refresh();
            if (!projectFolder.isValid()) {
                LOG.log(Level.WARNING, "invalid project folder: {0}", (Object)projectFolder);
            } else if (projectFolder.getChildren().length == 0) {
                projectFolder.delete();
            } else {
                LOG.log(Level.WARNING, "project folder {0} was not empty: {1}", new Object[]{projectFolder, Arrays.asList(projectFolder.getChildren())});
            }
            handle.finish();
            ProjectOperations.notifyDeleted((Project)project);
        }
        catch (Exception e) {
            String displayName = DefaultProjectOperationsImplementation.getDisplayName(project);
            String message = NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Project_cannot_be_deleted.", (Object)displayName);
            Exceptions.attachLocalizedMessage((Throwable)e, (String)message);
            throw e;
        }
    }

    public static void deleteProject(Project project) {
        DefaultProjectOperationsImplementation.deleteProject(project, new GUIUserInputHandler());
    }

    static void deleteProject(final Project project, UserInputHandler handler) {
        String displayName = DefaultProjectOperationsImplementation.getDisplayName(project);
        FileObject projectFolder = project.getProjectDirectory();
        LOG.log(Level.FINE, "delete started: {0}", displayName);
        final List metadataFiles = ProjectOperations.getMetadataFiles((Project)project);
        List dataFiles = ProjectOperations.getDataFiles((Project)project);
        final ArrayList allFiles = new ArrayList();
        allFiles.addAll(metadataFiles);
        allFiles.addAll(dataFiles);
        Iterator i = allFiles.iterator();
        while (i.hasNext()) {
            FileObject f = (FileObject)i.next();
            if (FileUtil.isParentOf((FileObject)projectFolder, (FileObject)f) || projectFolder.equals((Object)f)) continue;
            i.remove();
        }
        final ProgressHandle handle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Delete_Project_Caption"));
        final DefaultProjectDeletePanel deletePanel = new DefaultProjectDeletePanel(handle, displayName, FileUtil.getFileDisplayName((FileObject)projectFolder), !dataFiles.isEmpty());
        String caption = NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Delete_Project_Caption");
        handler.showConfirmationDialog(deletePanel, project, caption, "Yes_Button", "No_Button", true, new Executor(){

            @Override
            public void execute() throws Exception {
                deletePanel.addProgressBar();
                DefaultProjectOperationsImplementation.close(project);
                if (deletePanel.isDeleteSources()) {
                    try {
                        DefaultProjectOperationsImplementation.performDelete(project, allFiles, handle);
                    }
                    catch (IOException x) {
                        LOG.log(Level.WARNING, null, x);
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                    }
                    catch (Exception x) {
                        LOG.log(Level.WARNING, null, x);
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                    }
                } else {
                    try {
                        DefaultProjectOperationsImplementation.performDelete(project, metadataFiles, handle);
                    }
                    catch (IOException x) {
                        LOG.log(Level.WARNING, null, x);
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                    }
                    catch (Exception x) {
                        LOG.log(Level.WARNING, null, x);
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                    }
                }
                deletePanel.removeProgressBar();
            }
        });
        LOG.log(Level.FINE, "delete done: {0}", displayName);
    }

    public static void copyProject(final Project project) {
        final ProgressHandle handle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Copy_Project_Handle"));
        final ProjectCopyPanel panel = new ProjectCopyPanel(handle, project, false);
        handle.start(100);
        DefaultProjectOperationsImplementation.showConfirmationDialog(panel, project, NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Copy_Project_Caption"), "Copy_Button", null, false, new Executor(){

            @Override
            public void execute() throws Exception {
                final String nueName = panel.getNewName();
                File newTarget = FileUtil.normalizeFile((File)panel.getNewDirectory());
                FileObject newTargetFO = FileUtil.toFileObject((File)newTarget);
                if (newTargetFO == null) {
                    newTargetFO = DefaultProjectOperationsImplementation.createFolder(newTarget.getParentFile(), newTarget.getName());
                }
                final FileObject newTgtFO = newTargetFO;
                project.getProjectDirectory().getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

                    public void run() throws IOException {
                        try {
                            DefaultProjectOperationsImplementation.doCopyProject(handle, project, nueName, newTgtFO);
                        }
                        catch (IOException x) {
                            LOG.log(Level.WARNING, null, x);
                            NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                        }
                        catch (Exception x) {
                            LOG.log(Level.WARNING, null, x);
                            NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                        }
                    }
                });
            }

        });
    }

    static void doCopyProject(ProgressHandle handle, Project project, String nueName, FileObject newTarget) throws Exception {
        try {
            int totalWork = 100;
            double currentWorkDone = 0.0;
            handle.progress((int)currentWorkDone);
            ProjectOperations.notifyCopying((Project)project);
            currentWorkDone = (double)totalWork * 0.1;
            handle.progress((int)currentWorkDone);
            FileObject target = newTarget.createFolder(nueName);
            FileObject projectDirectory = project.getProjectDirectory();
            ArrayList<FileObject> toCopyList = new ArrayList<FileObject>();
            for (FileObject child : projectDirectory.getChildren()) {
                if (!child.isValid()) continue;
                toCopyList.add(child);
            }
            double workPerFileAndOperation = (double)totalWork * 0.7000000000000001 / (double)toCopyList.size();
            for (FileObject toCopy : toCopyList) {
                DefaultProjectOperationsImplementation.doCopy(project, toCopy, target);
                int lastWorkDone = (int)currentWorkDone;
                if (lastWorkDone >= (int)(currentWorkDone += workPerFileAndOperation)) continue;
                handle.progress((int)currentWorkDone);
            }
            ProjectManager.getDefault().clearNonProjectCache();
            Project nue = ProjectManager.getDefault().findProject(target);
            assert (nue != null);
            handle.progress((int)(currentWorkDone += (double)totalWork * 0.1));
            ProjectOperations.notifyCopied((Project)project, (Project)nue, (File)FileUtil.toFile((FileObject)project.getProjectDirectory()), (String)nueName);
            handle.progress((int)(currentWorkDone += (double)totalWork * 0.1));
            ProjectManager.getDefault().saveProject(nue);
            DefaultProjectOperationsImplementation.open(nue, false);
            handle.progress(totalWork);
            handle.finish();
        }
        catch (Exception e) {
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Cannot_Move", (Object)e.getLocalizedMessage()));
            throw e;
        }
    }

    public static void moveProject(final Project project) {
        final ProgressHandle handle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Move_Project_Handle"));
        final ProjectCopyPanel panel = new ProjectCopyPanel(handle, project, true);
        handle.start(100);
        DefaultProjectOperationsImplementation.showConfirmationDialog(panel, project, NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Move_Project_Caption"), "Move_Button", null, false, new Executor(){

            @Override
            public void execute() throws Exception {
                String nueFolderName = panel.getProjectFolderName();
                String nueProjectName = panel.getNewName();
                File newTarget = FileUtil.normalizeFile((File)panel.getNewDirectory());
                FileObject newTargetFO = FileUtil.toFileObject((File)newTarget);
                if (newTargetFO == null) {
                    newTargetFO = DefaultProjectOperationsImplementation.createFolder(newTarget.getParentFile(), newTarget.getName());
                }
                FileObject newTgtFO = newTargetFO;
                try {
                    DefaultProjectOperationsImplementation.doMoveProject(handle, project, nueFolderName, nueProjectName, newTgtFO, "ERR_Cannot_Move");
                }
                catch (IOException x) {
                    LOG.log(Level.WARNING, null, x);
                    NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                    DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                }
                catch (Exception x) {
                    LOG.log(Level.WARNING, null, x);
                    NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                    DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                }
            }
        });
    }

    public static void renameProject(Project project) {
        DefaultProjectOperationsImplementation.renameProject(project, null);
    }

    public static void renameProject(final Project project, String nueName) {
        final ProgressHandle handle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Rename_Project_Handle"));
        final DefaultProjectRenamePanel panel = new DefaultProjectRenamePanel(handle, project, nueName);
        handle.start(100);
        DefaultProjectOperationsImplementation.showConfirmationDialog(panel, project, NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Rename_Project_Caption"), "Rename_Button", null, false, new Executor(){

            @Override
            public void execute() throws Exception {
                final String nueName = panel.getNewName();
                panel.addProgressBar();
                if (panel.getRenameProjectFolder()) {
                    try {
                        DefaultProjectOperationsImplementation.doMoveProject(handle, project, nueName, nueName, project.getProjectDirectory().getParent(), "ERR_Cannot_Rename");
                    }
                    catch (IOException x) {
                        LOG.log(Level.WARNING, null, x);
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                    }
                    catch (Exception x) {
                        LOG.log(Level.WARNING, null, x);
                        NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                        DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                    }
                } else {
                    project.getProjectDirectory().getFileSystem().runAtomicAction(new FileSystem.AtomicAction(){

                        public void run() throws IOException {
                            try {
                                DefaultProjectOperationsImplementation.doRenameProject(handle, project, nueName);
                            }
                            catch (IOException x) {
                                LOG.log(Level.WARNING, null, x);
                                NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                            }
                            catch (Exception x) {
                                LOG.log(Level.WARNING, null, x);
                                NotifyDescriptor.Message nd = new NotifyDescriptor.Message((Object)x.getLocalizedMessage(), 0);
                                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                            }
                        }
                    });
                }
                panel.removeProgressBar();
            }

        });
    }

    private static void doRenameProject(ProgressHandle handle, Project project, String nueName) throws Exception {
        Collection operations = project.getLookup().lookupAll(MoveOperationImplementation.class);
        for (MoveOperationImplementation o : operations) {
            if (o instanceof MoveOrRenameOperationImplementation) continue;
            Logger.getLogger(DefaultProjectOperationsImplementation.class.getName()).log(Level.WARNING, "{0} should implement MoveOrRenameOperationImplementation", o.getClass().getName());
            DefaultProjectOperationsImplementation.doRenameProjectOld(handle, project, nueName, operations);
            return;
        }
        try {
            handle.switchToDeterminate(4);
            int currentWorkDone = 0;
            handle.progress(++currentWorkDone);
            for (MoveOperationImplementation o22 : operations) {
                ((MoveOrRenameOperationImplementation)o22).notifyRenaming();
            }
            handle.progress(++currentWorkDone);
            for (MoveOperationImplementation o22 : operations) {
                ((MoveOrRenameOperationImplementation)o22).notifyRenamed(nueName);
            }
            handle.progress(++currentWorkDone);
            ProjectManager.getDefault().saveProject(project);
            handle.progress(++currentWorkDone);
            handle.finish();
        }
        catch (Exception e) {
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Cannot_Rename", (Object)e.getLocalizedMessage()));
            throw e;
        }
    }

    private static void doRenameProjectOld(ProgressHandle handle, Project project, String nueName, Collection<? extends MoveOperationImplementation> operations) throws Exception {
        boolean originalOK = true;
        Project main = OpenProjects.getDefault().getMainProject();
        boolean wasMain = main != null && project.getProjectDirectory().equals((Object)main.getProjectDirectory());
        Project nue = null;
        try {
            handle.switchToIndeterminate();
            handle.switchToDeterminate(5);
            int currentWorkDone = 0;
            FileObject projectDirectory = project.getProjectDirectory();
            File projectDirectoryFile = FileUtil.toFile((FileObject)project.getProjectDirectory());
            DefaultProjectOperationsImplementation.close(project);
            handle.progress(++currentWorkDone);
            for (MoveOperationImplementation o22 : operations) {
                o22.notifyMoving();
            }
            handle.progress(++currentWorkDone);
            for (MoveOperationImplementation o22 : operations) {
                o22.notifyMoved(null, projectDirectoryFile, nueName);
            }
            handle.progress(++currentWorkDone);
            ProjectManager.getDefault().clearNonProjectCache();
            nue = ProjectManager.getDefault().findProject(projectDirectory);
            assert (nue != null);
            originalOK = false;
            handle.progress(++currentWorkDone);
            operations = nue.getLookup().lookupAll(MoveOperationImplementation.class);
            for (MoveOperationImplementation o22 : operations) {
                o22.notifyMoved(project, projectDirectoryFile, nueName);
            }
            ProjectManager.getDefault().saveProject(project);
            ProjectManager.getDefault().saveProject(nue);
            DefaultProjectOperationsImplementation.open(nue, wasMain);
            handle.progress(++currentWorkDone);
            handle.finish();
        }
        catch (Exception e) {
            if (originalOK) {
                DefaultProjectOperationsImplementation.open(project, wasMain);
            } else {
                assert (nue != null);
                DefaultProjectOperationsImplementation.open(nue, wasMain);
            }
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Cannot_Rename", (Object)e.getLocalizedMessage()));
            throw e;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void doMoveProject(ProgressHandle handle, Project project, String nueFolderName, String nueProjectName, FileObject newTarget, String errorKey) throws Exception {
        boolean originalOK = true;
        Project main = OpenProjects.getDefault().getMainProject();
        boolean wasMain = main != null && project.getProjectDirectory().equals((Object)main.getProjectDirectory());
        FileObject target = null;
        try {
            int totalWork = 100;
            double currentWorkDone = 0.0;
            handle.progress((int)currentWorkDone);
            ProjectOperations.notifyMoving((Project)project);
            DefaultProjectOperationsImplementation.close(project);
            currentWorkDone = (double)totalWork * 0.1;
            handle.progress((int)currentWorkDone);
            FileObject projectDirectory = project.getProjectDirectory();
            LOG.log(Level.FINE, "doMoveProject 1/2: {0} @{1}", new Object[]{projectDirectory, project.hashCode()});
            if (LOG.isLoggable(Level.FINER)) {
                for (Project real : OpenProjects.getDefault().getOpenProjects()) {
                    LOG.log(Level.FINER, "  open project: {0} @{1}", new Object[]{real, real.hashCode()});
                }
            }
            double workPerFileAndOperation = (double)totalWork * 0.7000000000000001;
            FileLock lock = projectDirectory.lock();
            try {
                target = projectDirectory.move(lock, newTarget, nueFolderName, null);
            }
            finally {
                lock.releaseLock();
            }
            int lastWorkDone = (int)currentWorkDone;
            if (lastWorkDone < (int)(currentWorkDone += workPerFileAndOperation)) {
                handle.progress((int)currentWorkDone);
            }
            originalOK = false;
            ProjectManager.getDefault().clearNonProjectCache();
            Project nue = ProjectManager.getDefault().findProject(target);
            handle.progress((int)(currentWorkDone += (double)totalWork * 0.1));
            assert (nue != null);
            assert (nue != project);
            LOG.log(Level.FINE, "doMoveProject 2/2: {0} @{1}", new Object[]{target, nue.hashCode()});
            ProjectOperations.notifyMoved((Project)project, (Project)nue, (File)FileUtil.toFile((FileObject)project.getProjectDirectory()), (String)nueProjectName);
            handle.progress((int)(currentWorkDone += (double)totalWork * 0.1));
            ProjectManager.getDefault().saveProject(nue);
            DefaultProjectOperationsImplementation.open(nue, wasMain);
            if (LOG.isLoggable(Level.FINER)) {
                for (Project real : OpenProjects.getDefault().getOpenProjects()) {
                    LOG.log(Level.FINER, "  open project: {0} @{1}", new Object[]{real, real.hashCode()});
                }
            }
            handle.progress(totalWork);
            handle.finish();
        }
        catch (Exception e) {
            if (originalOK) {
                DefaultProjectOperationsImplementation.open(project, wasMain);
            } else {
                ProjectManager.getDefault().clearNonProjectCache();
                if (target == null) {
                    Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)errorKey, (Object)e.getLocalizedMessage()));
                    throw e;
                }
                Project nue = ProjectManager.getDefault().findProject(target);
                if (nue != null) {
                    DefaultProjectOperationsImplementation.open(nue, wasMain);
                }
            }
            Exceptions.attachLocalizedMessage((Throwable)e, (String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)errorKey, (Object)e.getLocalizedMessage()));
            throw e;
        }
    }

    private static void doCopy(Project original, FileObject from, FileObject toParent) throws IOException {
        if (!VisibilityQuery.getDefault().isVisible(from)) {
            return;
        }
        if (!original.getProjectDirectory().equals((Object)FileOwnerQuery.getOwner((FileObject)from).getProjectDirectory())) {
            return;
        }
        if (SharabilityQuery.getSharability((FileObject)from) == SharabilityQuery.Sharability.NOT_SHARABLE) {
            return;
        }
        if (from.isFolder()) {
            FileObject copy = toParent.createFolder(from.getNameExt());
            for (FileObject kid : from.getChildren()) {
                DefaultProjectOperationsImplementation.doCopy(original, kid, copy);
            }
        } else {
            assert (from.isData());
            FileObject target = FileUtil.copyFile((FileObject)from, (FileObject)toParent, (String)from.getName(), (String)from.getExt());
        }
    }

    private static FileObject createFolder(File parent, String name) throws IOException {
        FileObject path = FileUtil.toFileObject((File)parent);
        if (path != null) {
            return path.createFolder(name);
        }
        return DefaultProjectOperationsImplementation.createFolder(parent.getParentFile(), parent.getName()).createFolder(name);
    }

    private static JComponent wrapPanel(JComponent component) {
        component.setBorder(new EmptyBorder(12, 12, 12, 12));
        return component;
    }

    private static void showConfirmationDialog(final JComponent panel, Project project, String caption, String confirmButton, String cancelButton, boolean doSetMessageType, final Executor executor) {
        final JButton confirm = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)confirm, (String)NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)("LBL_" + confirmButton)));
        final JButton cancel = new JButton(cancelButton == null ? NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"LBL_Cancel_Button") : NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)("LBL_" + cancelButton)));
        confirm.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)("ACSD_" + confirmButton)));
        cancel.getAccessibleContext().setAccessibleDescription(cancelButton == null ? NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ACSD_Cancel_Button") : NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)("ACSD_" + cancelButton)));
        assert (panel instanceof InvalidablePanel);
        ((InvalidablePanel)((Object)panel)).addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                confirm.setEnabled(((InvalidablePanel)((Object)panel)).isPanelValid());
            }
        });
        confirm.setEnabled(((InvalidablePanel)((Object)panel)).isPanelValid());
        final Dialog[] dialog = new Dialog[1];
        DialogDescriptor dd = new DialogDescriptor((Object)(doSetMessageType ? panel : DefaultProjectOperationsImplementation.wrapPanel(panel)), caption, true, new Object[]{confirm, cancel}, (Object)(cancelButton != null ? cancel : confirm), 0, null, new ActionListener(){
            private boolean operationRunning;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (this.operationRunning) {
                    return;
                }
                if (dialog[0] instanceof JDialog) {
                    ((JDialog)dialog[0]).getRootPane().getInputMap(1).remove(KeyStroke.getKeyStroke(27, 0));
                    ((JDialog)dialog[0]).setDefaultCloseOperation(0);
                }
                this.operationRunning = true;
                if (e.getSource() == confirm) {
                    Container findParent;
                    confirm.setEnabled(false);
                    cancel.setEnabled(false);
                    ((InvalidablePanel)((Object)panel)).showProgress();
                    for (findParent = panel; findParent != null && !(findParent instanceof Window); findParent = findParent.getParent()) {
                    }
                    if (findParent != null) {
                        ((Window)findParent).pack();
                    }
                    RequestProcessor.getDefault().post(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            AtomicReference<Throwable> e = new AtomicReference<Throwable>();
                            try {
                                executor.execute();
                            }
                            catch (Throwable ex) {
                                block4 : {
                                    try {
                                        e.set(ex);
                                        if (!(ex instanceof ThreadDeath)) break block4;
                                        throw (ThreadDeath)ex;
                                    }
                                    catch (Throwable var3_3) {
                                        SwingUtilities.invokeLater(new Runnable(e){
                                            final /* synthetic */ AtomicReference val$e;

                                            @Override
                                            public void run() {
                                                dialog[0].setVisible(false);
                                                if (this.val$e.get() != null) {
                                                    LOG.log(Level.WARNING, null, (Throwable)this.val$e.get());
                                                }
                                            }
                                        });
                                        throw var3_3;
                                    }
                                }
                                SwingUtilities.invokeLater(new );
                            }
                            SwingUtilities.invokeLater(new );
                        }

                    });
                } else {
                    dialog[0].setVisible(false);
                }
            }

        });
        if (doSetMessageType) {
            dd.setMessageType(3);
        }
        dd.setClosingOptions(new Object[0]);
        dialog[0] = DialogDisplayer.getDefault().createDialog(dd);
        dialog[0].setVisible(true);
        dialog[0].dispose();
        dialog[0] = null;
    }

    @CheckForNull
    static String computeError(@NullAllowed File location, String projectNameText, boolean pureRename) {
        return DefaultProjectOperationsImplementation.computeError(location, projectNameText, null, pureRename);
    }

    @CheckForNull
    static String computeError(@NullAllowed File location, String projectNameText, String projectFolderText, boolean pureRename) {
        if (projectNameText.length() == 0) {
            return NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Project_Name_Must_Entered");
        }
        if (projectNameText.indexOf(47) != -1 || projectNameText.indexOf(92) != -1) {
            return NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Not_Valid_Filename", (Object)projectNameText);
        }
        if (location == null) {
            return null;
        }
        File parent = location;
        if (!location.exists()) {
            for (parent = location.getParentFile(); parent != null && !parent.exists(); parent = parent.getParentFile()) {
            }
            if (parent == null) {
                return NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Location_Does_Not_Exist");
            }
        }
        if (!parent.canWrite()) {
            return NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Location_Read_Only");
        }
        File projectFolderFile = null;
        projectFolderFile = projectFolderText == null ? new File(location, projectNameText) : new File(projectFolderText);
        if (projectFolderFile.exists() && !pureRename) {
            return NbBundle.getMessage(DefaultProjectOperationsImplementation.class, (String)"ERR_Project_Folder_Exists");
        }
        return null;
    }

    private static void close(Project prj) {
        LifecycleManager.getDefault().saveAll();
        OpenProjects.getDefault().close(new Project[]{prj});
    }

    private static void open(Project prj, boolean setAsMain) {
        OpenProjects.getDefault().open(new Project[]{prj}, false);
        if (setAsMain) {
            OpenProjects.getDefault().setMainProject(prj);
        }
    }

    public static interface InvalidablePanel {
        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);

        public boolean isPanelValid();

        public void showProgress();
    }

    static interface Executor {
        public void execute() throws Exception;
    }

    private static final class GUIUserInputHandler
    implements UserInputHandler {
        private GUIUserInputHandler() {
        }

        @Override
        public void showConfirmationDialog(JComponent panel, Project project, String caption, String confirmButton, String cancelButton, boolean doSetMessageType, Executor executor) {
            DefaultProjectOperationsImplementation.showConfirmationDialog(panel, project, caption, confirmButton, cancelButton, doSetMessageType, executor);
        }
    }

    static interface UserInputHandler {
        public void showConfirmationDialog(JComponent var1, Project var2, String var3, String var4, String var5, boolean var6, Executor var7);
    }

}

