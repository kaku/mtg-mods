/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.project.uiapi;

import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.spi.project.ui.support.BuildExecutionSupport;

public interface BuildExecutionSupportImplementation {
    public void registerFinishedItem(BuildExecutionSupport.Item var1);

    public void registerRunningItem(BuildExecutionSupport.Item var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public BuildExecutionSupport.Item getLastItem();

    public List<BuildExecutionSupport.Item> getRunningItems();
}

