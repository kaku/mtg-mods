/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.project.uiapi;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;

public class CategoryModel {
    public static final String PROP_CURRENT_CATEGORY = "propCurrentCategory";
    private ProjectCustomizer.Category[] categories;
    private ProjectCustomizer.Category currentCategory;
    private PropertyChangeSupport pcs;

    public CategoryModel(ProjectCustomizer.Category[] categories) {
        if (categories == null || categories.length == 0) {
            throw new IllegalArgumentException("Must provide at least one category");
        }
        this.categories = categories;
        this.currentCategory = categories[0];
        this.pcs = new PropertyChangeSupport(this);
    }

    public ProjectCustomizer.Category getCurrentCategory() {
        return this.currentCategory;
    }

    public ProjectCustomizer.Category getCategory(String name) {
        return CategoryModel.findCategoryByName(name, this.categories);
    }

    public void setCurrentCategory(ProjectCustomizer.Category category) {
        if (this.currentCategory != category) {
            ProjectCustomizer.Category oldValue = this.currentCategory;
            this.currentCategory = category;
            this.firePropertyChange("propCurrentCategory", oldValue, category);
        }
    }

    public ProjectCustomizer.Category[] getCategories() {
        return this.categories;
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(propertyName, l);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(propertyName, l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    private static ProjectCustomizer.Category findCategoryByName(String name, ProjectCustomizer.Category[] categories) {
        for (int i = 0; i < categories.length; ++i) {
            ProjectCustomizer.Category category;
            if (name.equals(categories[i].getName())) {
                return categories[i];
            }
            ProjectCustomizer.Category[] subcategories = categories[i].getSubcategories();
            if (subcategories == null || (category = CategoryModel.findCategoryByName(name, subcategories)) == null) continue;
            return category;
        }
        return null;
    }
}

