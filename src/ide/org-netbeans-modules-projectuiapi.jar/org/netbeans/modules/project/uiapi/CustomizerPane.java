/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.project.uiapi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.modules.project.uiapi.CategoryModel;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class CustomizerPane
extends JPanel
implements HelpCtx.Provider {
    public static final String HELP_CTX_PROPERTY = "helpCtxProperty";
    private Component currentCustomizer;
    private JPanel errorPanel;
    private JLabel errorIcon;
    private JTextArea errorMessageValue;
    private HelpCtx currentHelpCtx;
    private GridBagConstraints fillConstraints;
    private GridBagConstraints errMessConstraints = new GridBagConstraints();
    private ProjectCustomizer.CategoryComponentProvider componentProvider;
    private HashMap<ProjectCustomizer.Category, JComponent> panelCache = new HashMap();
    private static final int MAX_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height * 3 / 4;
    private static final int MAX_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width * 3 / 4;
    private JPanel categoryPanel;
    private JPanel customizerPanel;
    private JLabel jLabel1;

    public CustomizerPane(JPanel categoryView, CategoryModel categoryModel, ProjectCustomizer.CategoryComponentProvider componentProvider) {
        this.initComponents();
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CustomizerPane.class, (String)"AD_CustomizerPane"));
        this.componentProvider = componentProvider;
        this.fillConstraints = new GridBagConstraints();
        this.fillConstraints.gridwidth = 0;
        this.fillConstraints.gridheight = 1;
        this.fillConstraints.fill = 1;
        this.fillConstraints.weightx = 1.0;
        this.fillConstraints.weighty = 1.0;
        categoryModel.addPropertyChangeListener(new CategoryChangeListener());
        this.categoryPanel.add((Component)categoryView, this.fillConstraints);
        this.errorIcon = new JLabel();
        this.errorPanel = new JPanel(new BorderLayout(this.errorIcon.getIconTextGap(), 0));
        this.errorPanel.add((Component)this.errorIcon, "Before");
        this.errorIcon.setVerticalAlignment(1);
        this.errorMessageValue = new JTextArea();
        this.errorMessageValue.setLineWrap(true);
        this.errorMessageValue.setWrapStyleWord(true);
        this.errorMessageValue.setBorder(BorderFactory.createEmptyBorder());
        this.errorMessageValue.setBackground(this.customizerPanel.getBackground());
        this.errorMessageValue.setEditable(false);
        this.errorPanel.add((Component)this.errorMessageValue, "Center");
        this.errMessConstraints = new GridBagConstraints();
        this.errMessConstraints.gridx = 0;
        this.errMessConstraints.gridy = 1;
        this.errMessConstraints.gridwidth = 1;
        this.errMessConstraints.gridheight = 1;
        this.errMessConstraints.insets = new Insets(12, 0, 0, 0);
        this.errMessConstraints.fill = 2;
        this.customizerPanel.add((Component)this.errorPanel, this.errMessConstraints);
        this.setCategory(categoryModel.getCurrentCategory());
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.categoryPanel = new JPanel();
        this.customizerPanel = new JPanel();
        this.setLayout(new GridBagLayout());
        this.jLabel1.setLabelFor(this.categoryPanel);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(CustomizerPane.class, (String)"LBL_Customizer_Categories"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(8, 11, 0, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.categoryPanel.setBorder(BorderFactory.createEtchedBorder());
        this.categoryPanel.setMinimumSize(new Dimension(220, 4));
        this.categoryPanel.setPreferredSize(new Dimension(220, 4));
        this.categoryPanel.setLayout(new GridBagLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 11, 8, 11);
        this.add((Component)this.categoryPanel, gridBagConstraints);
        this.customizerPanel.setLayout(new GridBagLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 8, 11);
        this.add((Component)this.customizerPanel, gridBagConstraints);
    }

    public void clearPanelComponentCache() {
        this.panelCache.clear();
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.isPreferredSizeSet()) {
            return super.getPreferredSize();
        }
        int height = Math.max(500, this.currentCustomizer.getPreferredSize().height + 50);
        int width = Math.max(800, this.currentCustomizer.getPreferredSize().width + 240);
        Dimension dim = super.getPreferredSize();
        if (dim == null) {
            return new Dimension(width, height);
        }
        if (dim.getWidth() < (double)width || dim.getHeight() < (double)height) {
            return new Dimension(width, height);
        }
        if (dim.getWidth() > (double)MAX_WIDTH) {
            dim.width = MAX_WIDTH;
        }
        if (dim.getHeight() > (double)MAX_HEIGHT) {
            dim.height = MAX_HEIGHT;
        }
        return dim;
    }

    public HelpCtx getHelpCtx() {
        return this.currentHelpCtx;
    }

    private void setCategory(final ProjectCustomizer.Category newCategory) {
        JComponent newCustomizer;
        if (newCategory == null) {
            return;
        }
        if (this.currentCustomizer != null) {
            this.customizerPanel.remove(this.currentCustomizer);
        }
        if ((newCustomizer = this.panelCache.get(newCategory)) == null && !this.panelCache.containsKey(newCustomizer)) {
            newCustomizer = this.componentProvider.create(newCategory);
            this.panelCache.put(newCategory, newCustomizer);
        }
        if (newCustomizer != null) {
            Utilities.getCategoryChangeSupport(newCategory).addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    CustomizerPane.this.setErrorMessage(newCategory.getErrorMessage(), newCategory.isValid());
                }
            });
            this.currentCustomizer = newCustomizer;
            this.currentHelpCtx = HelpCtx.findHelp((Component)this.currentCustomizer);
            this.customizerPanel.add(this.currentCustomizer, this.fillConstraints);
            this.customizerPanel.validate();
            this.customizerPanel.repaint();
            this.setErrorMessage(newCategory.getErrorMessage(), newCategory.isValid());
            this.firePropertyChange("helpCtxProperty", null, (Object)this.getHelpCtx());
        } else {
            this.currentCustomizer = null;
        }
    }

    private void setErrorMessage(String errMessage, boolean valid) {
        this.customizerPanel.remove(this.errorPanel);
        if (errMessage != null && !errMessage.trim().isEmpty()) {
            this.errorIcon.setIcon(ImageUtilities.loadImageIcon((String)(valid ? "org/netbeans/modules/dialogs/warning.gif" : "org/netbeans/modules/dialogs/error.gif"), (boolean)true));
            this.errorMessageValue.setText(errMessage);
            this.errorMessageValue.setForeground(UIManager.getColor(valid ? "nb.warningForeground" : "nb.errorForeground"));
            this.customizerPanel.add((Component)this.errorPanel, this.errMessConstraints);
        }
        this.customizerPanel.revalidate();
        this.customizerPanel.repaint();
    }

    private class CategoryChangeListener
    implements PropertyChangeListener {
        private CategoryChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("propCurrentCategory".equals(evt.getPropertyName())) {
                ProjectCustomizer.Category newCategory = (ProjectCustomizer.Category)evt.getNewValue();
                CustomizerPane.this.setCategory(newCategory);
            }
        }
    }

}

