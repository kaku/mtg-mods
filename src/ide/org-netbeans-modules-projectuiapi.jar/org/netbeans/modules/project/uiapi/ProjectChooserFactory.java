/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.SourceGroup
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 */
package org.netbeans.modules.project.uiapi;

import java.io.File;
import javax.swing.JFileChooser;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.SourceGroup;
import org.openide.WizardDescriptor;

public interface ProjectChooserFactory {
    public static final String WIZARD_KEY_PROJECT = "project";
    public static final String WIZARD_KEY_TARGET_FOLDER = "targetFolder";
    public static final String WIZARD_KEY_TARGET_NAME = "targetName";
    public static final String WIZARD_KEY_TEMPLATE = "targetTemplate";

    public File getProjectsFolder();

    public void setProjectsFolder(File var1);

    public JFileChooser createProjectChooser();

    public WizardDescriptor.Panel<WizardDescriptor> createSimpleTargetChooser(@NullAllowed Project var1, @NonNull SourceGroup[] var2, WizardDescriptor.Panel<WizardDescriptor> var3, boolean var4);
}

