/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.Project
 */
package org.netbeans.modules.project.uiapi;

import java.beans.PropertyChangeListener;
import java.util.concurrent.Future;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.ProjectGroup;
import org.netbeans.api.project.ui.ProjectGroupChangeListener;

public interface OpenProjectsTrampoline {
    public Project[] getOpenProjectsAPI();

    public void openAPI(Project[] var1, boolean var2, boolean var3);

    public void closeAPI(Project[] var1);

    public void addPropertyChangeListenerAPI(PropertyChangeListener var1, Object var2);

    public Future<Project[]> openProjectsAPI();

    public void removePropertyChangeListenerAPI(PropertyChangeListener var1);

    public Project getMainProject();

    public void setMainProject(Project var1);

    public ProjectGroup getActiveProjectGroupAPI();

    public void addProjectGroupChangeListenerAPI(ProjectGroupChangeListener var1);

    public void removeProjectGroupChangeListenerAPI(ProjectGroupChangeListener var1);
}

