/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.BeanTreeView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.project.uiapi;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.accessibility.AccessibleContext;
import javax.swing.JPanel;
import org.netbeans.modules.project.uiapi.CategoryModel;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public class CategoryView
extends JPanel
implements ExplorerManager.Provider,
PropertyChangeListener {
    private static final String DEFAULT_CATEGORY = "org/netbeans/modules/project/uiapi/defaultCategory.gif";
    private ExplorerManager manager;
    private BeanTreeView btv;
    private CategoryModel categoryModel;

    public CategoryView(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
        this.manager = new ExplorerManager();
        this.setLayout(new BorderLayout());
        Dimension size = new Dimension(220, 4);
        this.btv = new BeanTreeView();
        this.btv.setSelectionMode(1);
        this.btv.setPopupAllowed(false);
        this.btv.setRootVisible(false);
        this.btv.setDefaultActionAllowed(false);
        this.btv.setMinimumSize(size);
        this.btv.setPreferredSize(size);
        this.btv.setMaximumSize(size);
        this.btv.setDragSource(false);
        this.add((Component)this.btv, "Center");
        this.manager.setRootContext(this.createRootNode(categoryModel));
        this.manager.addPropertyChangeListener((PropertyChangeListener)this);
        categoryModel.addPropertyChangeListener(this);
        this.btv.expandAll();
        this.selectNode(categoryModel.getCurrentCategory());
        this.btv.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CategoryView.class, (String)"AN_CatgoryView"));
        this.btv.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CategoryView.class, (String)"AD_CategoryView"));
    }

    public ExplorerManager getExplorerManager() {
        return this.manager;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.btv.expandAll();
        this.btv.requestFocusInWindow();
    }

    private void selectNode(ProjectCustomizer.Category category) {
        Node node = this.findNode(category, this.manager.getRootContext());
        if (node != null) {
            try {
                this.manager.setSelectedNodes(new Node[]{node});
            }
            catch (PropertyVetoException e) {
                // empty catch block
            }
        }
    }

    private Node findNode(ProjectCustomizer.Category category, Node node) {
        Node[] nodes;
        Children ch = node.getChildren();
        if (ch != null && ch != Children.LEAF && (nodes = ch.getNodes(true)) != null) {
            for (Node child : nodes) {
                ProjectCustomizer.Category cc = (ProjectCustomizer.Category)child.getLookup().lookup(ProjectCustomizer.Category.class);
                if (cc == category) {
                    return child;
                }
                Node n = this.findNode(category, child);
                if (n == null) continue;
                return n;
            }
        }
        return null;
    }

    private Node createRootNode(CategoryModel categoryModel) {
        ProjectCustomizer.Category rootCategory = ProjectCustomizer.Category.create("root", "root", null, categoryModel.getCategories());
        return new CategoryNode(rootCategory);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        String propertyName = evt.getPropertyName();
        if (source == this.manager && "selectedNodes".equals(propertyName)) {
            Node[] nodes = this.manager.getSelectedNodes();
            if (nodes == null || nodes.length <= 0) {
                return;
            }
            Node node = nodes[0];
            ProjectCustomizer.Category category = (ProjectCustomizer.Category)node.getLookup().lookup(ProjectCustomizer.Category.class);
            if (category != this.categoryModel.getCurrentCategory()) {
                this.categoryModel.setCurrentCategory(category);
            }
        }
        if (source == this.categoryModel && "propCurrentCategory".equals(propertyName)) {
            this.selectNode((ProjectCustomizer.Category)evt.getNewValue());
        }
    }

    private static class CategoryChildren
    extends Children.Keys<ProjectCustomizer.Category> {
        private final ProjectCustomizer.Category[] descriptions;

        public CategoryChildren(ProjectCustomizer.Category[] descriptions) {
            this.descriptions = descriptions;
        }

        public void addNotify() {
            this.setKeys(this.descriptions);
        }

        public void removeNotify() {
            this.setKeys((Object[])new ProjectCustomizer.Category[0]);
        }

        protected Node[] createNodes(ProjectCustomizer.Category c) {
            return new Node[]{new CategoryNode(c)};
        }
    }

    private static class CategoryNode
    extends AbstractNode
    implements PropertyChangeListener {
        private Image icon = ImageUtilities.loadImage((String)"org/netbeans/modules/project/uiapi/defaultCategory.gif");
        private final ProjectCustomizer.Category category;

        public CategoryNode(ProjectCustomizer.Category category) {
            super(category.getSubcategories() == null || category.getSubcategories().length == 0 ? Children.LEAF : new CategoryChildren(category.getSubcategories()), Lookups.fixed((Object[])new Object[]{category}));
            this.setName(category.getName());
            this.category = category;
            this.setDisplayName(category.getDisplayName());
            if (category.getIcon() != null) {
                this.icon = category.getIcon();
            }
            Utilities.getCategoryChangeSupport(category).addPropertyChangeListener(this);
        }

        public String getHtmlDisplayName() {
            return this.category.isValid() ? null : "<html><font color=\"!nb.errorForeground\">" + this.category.getDisplayName() + "</font></html>";
        }

        public Image getIcon(int type) {
            return this.icon;
        }

        public Image getOpenedIcon(int type) {
            return this.getIcon(type);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("isCategoryValid".equals(evt.getPropertyName())) {
                this.fireDisplayNameChange(null, null);
            }
        }
    }

}

