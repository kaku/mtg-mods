/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.project.uiapi;

import org.netbeans.spi.project.ui.ProjectOpenedHook;

public abstract class ProjectOpenedTrampoline {
    public static ProjectOpenedTrampoline DEFAULT;

    protected ProjectOpenedTrampoline() {
    }

    public abstract void projectOpened(ProjectOpenedHook var1);

    public abstract void projectClosed(ProjectOpenedHook var1);

    static {
        Class<ProjectOpenedHook> c = ProjectOpenedHook.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

