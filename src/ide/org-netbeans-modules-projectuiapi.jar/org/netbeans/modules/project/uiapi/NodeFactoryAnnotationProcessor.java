/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.project.uiapi;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class NodeFactoryAnnotationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(NodeFactory.Registration.class.getCanonicalName());
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(NodeFactory.Registration.class)) {
            NodeFactory.Registration r = e.getAnnotation(NodeFactory.Registration.class);
            if (r == null) continue;
            for (String type : r.projectType()) {
                this.layer(new Element[]{e}).instanceFile("Projects/" + type + "/Nodes", null, NodeFactory.class, (Annotation)r, null).position(r.position()).write();
            }
        }
        return true;
    }
}

