/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectManager
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.project.uiapi;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.modules.project.uiapi.CustomizerPane;
import org.netbeans.modules.project.uiapi.SavingProjectDataPanel;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.windows.WindowManager;

public class CustomizerDialog {
    private static final int OPTION_OK = 0;
    private static final int OPTION_CANCEL = 1;
    private static final String COMMAND_OK = "OK";
    private static final String COMMAND_CANCEL = "CANCEL";
    private static final int ACTION_CLOSE = 2;
    private static final String CUSTOMIZER_DIALOG_X = "CustomizerDialog.dialog.x";
    private static final String CUSTOMIZER_DIALOG_Y = "CustomizerDialog.dialog.y";
    private static final String CUSTOMIZER_DIALOG_WIDTH = "CustomizerDialog.dialog.width";
    private static final String CUSTOMIZER_DIALOG_HEIGHT = "CustomizerDialog.dialog.height";

    private CustomizerDialog() {
    }

    public static Dialog createDialog(@NonNull ActionListener okOptionListener, @NullAllowed ActionListener storeListener, final CustomizerPane innerPane, HelpCtx helpCtx, final ProjectCustomizer.Category[] categories, ProjectCustomizer.CategoryComponentProvider componentProvider) {
        HelpCtx help;
        ListeningButton okButton = new ListeningButton(NbBundle.getMessage(CustomizerDialog.class, (String)"LBL_Customizer_Ok_Option"), categories);
        okButton.setEnabled(CustomizerDialog.checkValidity(categories));
        Object[] options = new JButton[]{okButton, new JButton(NbBundle.getMessage(CustomizerDialog.class, (String)"LBL_Customizer_Cancel_Option"))};
        options[0].setActionCommand("OK");
        options[1].setActionCommand("CANCEL");
        options[0].getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CustomizerDialog.class, (String)"AD_Customizer_Ok_Option"));
        options[1].getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CustomizerDialog.class, (String)"AD_Customizer_Cancel_Option"));
        OptionListener optionsListener = new OptionListener(okOptionListener, storeListener, categories, componentProvider);
        options[0].addActionListener(optionsListener);
        options[1].addActionListener(optionsListener);
        innerPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CustomizerDialog.class, (String)"AN_ProjectCustomizer"));
        innerPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CustomizerDialog.class, (String)"AD_ProjectCustomizer"));
        if (helpCtx == null) {
            helpCtx = HelpCtx.DEFAULT_HELP;
        }
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)innerPane, NbBundle.getMessage(CustomizerDialog.class, (String)"LBL_Customizer_Title"), false, options, (Object)options[0], 0, helpCtx, null);
        innerPane.addPropertyChangeListener(new HelpCtxChangeListener(dialogDescriptor, helpCtx));
        if (innerPane instanceof HelpCtx.Provider && !(help = innerPane.getHelpCtx()).equals((Object)HelpCtx.DEFAULT_HELP)) {
            dialogDescriptor.setHelpCtx(help);
        }
        dialogDescriptor.setClosingOptions(new Object[]{options[0], options[1]});
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        Preferences prefs = NbPreferences.forModule(CustomizerDialog.class);
        int dialogX = prefs.getInt("CustomizerDialog.dialog.x", 0);
        int dialogY = prefs.getInt("CustomizerDialog.dialog.y", 0);
        int dialogWidth = prefs.getInt("CustomizerDialog.dialog.width", 0);
        int dialogHeight = prefs.getInt("CustomizerDialog.dialog.height", 0);
        if (dialogWidth != 0 && dialogHeight != 0) {
            int maxHeight;
            GraphicsConfiguration gf = WindowManager.getDefault().getMainWindow().getGraphicsConfiguration();
            Rectangle gbounds = gf.getBounds();
            int maxWidth = gbounds.width;
            if (dialogWidth > maxWidth) {
                dialogWidth = maxWidth * 3 / 4;
            }
            if (dialogHeight > (maxHeight = gbounds.height)) {
                dialogHeight = maxHeight * 3 / 4;
            }
            int minx = gbounds.x;
            int maxx = minx + gbounds.width;
            int miny = gbounds.y;
            int maxy = miny + gbounds.height;
            dialog.setBounds(dialogX, dialogY, dialogWidth, dialogHeight);
            if (dialogX < minx || dialogX > maxx || dialogY < miny || dialogY > maxy) {
                dialog.setLocationRelativeTo(WindowManager.getDefault().getMainWindow());
            }
        }
        dialog.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosed(WindowEvent e) {
                Preferences prefs = NbPreferences.forModule(CustomizerDialog.class);
                prefs.putInt("CustomizerDialog.dialog.x", e.getWindow().getX());
                prefs.putInt("CustomizerDialog.dialog.y", e.getWindow().getY());
                prefs.putInt("CustomizerDialog.dialog.width", e.getWindow().getWidth());
                prefs.putInt("CustomizerDialog.dialog.height", e.getWindow().getHeight());
                innerPane.clearPanelComponentCache();
                LinkedList<ProjectCustomizer.Category> queue = new LinkedList<ProjectCustomizer.Category>(Arrays.asList(categories));
                while (!queue.isEmpty()) {
                    ProjectCustomizer.Category category = queue.remove(0);
                    Utilities.removeCategoryChangeSupport(category);
                    ActionListener listener = category.getCloseListener();
                    if (listener != null) {
                        listener.actionPerformed(new ActionEvent(this, 2, e.paramString()));
                    }
                    if (category.getSubcategories() == null) continue;
                    queue.addAll(Arrays.asList(category.getSubcategories()));
                }
            }
        });
        return dialog;
    }

    private static boolean checkValidity(ProjectCustomizer.Category[] categories) {
        for (ProjectCustomizer.Category c : categories) {
            if (!c.isValid()) {
                return false;
            }
            ProjectCustomizer.Category[] subCategories = c.getSubcategories();
            if (subCategories == null || CustomizerDialog.checkValidity(subCategories)) continue;
            return false;
        }
        return true;
    }

    private static class ListeningButton
    extends JButton
    implements PropertyChangeListener {
        private ProjectCustomizer.Category[] categories;

        public ListeningButton(String label, ProjectCustomizer.Category[] categories) {
            super(label);
            this.categories = categories;
            for (ProjectCustomizer.Category c : categories) {
                Utilities.getCategoryChangeSupport(c).addPropertyChangeListener(this);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName() == "isCategoryValid") {
                boolean valid = (Boolean)evt.getNewValue();
                this.setEnabled(valid && CustomizerDialog.checkValidity(this.categories));
            }
        }
    }

    private static class HelpCtxChangeListener
    implements PropertyChangeListener {
        DialogDescriptor dialogDescriptor;
        HelpCtx defaultHelpCtx;

        HelpCtxChangeListener(DialogDescriptor dialogDescriptor, HelpCtx defaultHelpCtx) {
            this.dialogDescriptor = dialogDescriptor;
            this.defaultHelpCtx = defaultHelpCtx;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("helpCtxProperty".equals(evt.getPropertyName())) {
                HelpCtx newHelp = (HelpCtx)evt.getNewValue();
                this.dialogDescriptor.setHelpCtx(newHelp == null || newHelp == HelpCtx.DEFAULT_HELP ? this.defaultHelpCtx : newHelp);
            }
        }
    }

    private static class OptionListener
    implements ActionListener {
        @NonNull
        private ActionListener okOptionListener;
        @NullAllowed
        private ActionListener storeListener;
        private ProjectCustomizer.Category[] categories;
        private Lookup.Provider prov;

        OptionListener(@NonNull ActionListener okOptionListener, @NullAllowed ActionListener storeListener, ProjectCustomizer.Category[] categs, ProjectCustomizer.CategoryComponentProvider componentProvider) {
            this.okOptionListener = okOptionListener;
            this.storeListener = storeListener;
            this.categories = categs;
            if (componentProvider instanceof Lookup.Provider) {
                this.prov = (Lookup.Provider)componentProvider;
            }
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            String command = e.getActionCommand();
            if ("OK".equals(command)) {
                ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Object>(){

                    public Object run() {
                        OptionListener.this.okOptionListener.actionPerformed(e);
                        OptionListener.this.actionPerformed(e, OptionListener.this.categories);
                        return null;
                    }
                });
                final ProgressHandle handle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(CustomizerDialog.class, (String)"LBL_Saving_Project_data_progress"));
                JComponent component = ProgressHandleFactory.createProgressComponent((ProgressHandle)handle);
                Frame mainWindow = WindowManager.getDefault().getMainWindow();
                final JDialog dialog = new JDialog(mainWindow, NbBundle.getMessage(CustomizerDialog.class, (String)"LBL_Saving_Project_data"), true);
                SavingProjectDataPanel panel = new SavingProjectDataPanel(component);
                dialog.getContentPane().add(panel);
                dialog.setDefaultCloseOperation(0);
                dialog.pack();
                Rectangle bounds = mainWindow.getBounds();
                int middleX = bounds.x + bounds.width / 2;
                int middleY = bounds.y + bounds.height / 2;
                Dimension size = dialog.getPreferredSize();
                dialog.setBounds(middleX - size.width / 2, middleY - size.height / 2, size.width, size.height);
                RequestProcessor.getDefault().post(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        try {}
                        catch (Throwable var1_1) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    dialog.setVisible(false);
                                    dialog.dispose();
                                }
                            });
                            throw var1_1;
                        }
                        ProjectManager.mutex().writeAccess((Mutex.Action)new Mutex.Action<Object>(){

                            public Object run() {
                                FileUtil.runAtomicAction((Runnable)new Runnable(){

                                    @Override
                                    public void run() {
                                        handle.start();
                                        if (OptionListener.this.storeListener != null) {
                                            OptionListener.this.storeListener.actionPerformed(e);
                                        }
                                        OptionListener.this.storePerformed(e, OptionListener.this.categories);
                                        OptionListener.this.saveModifiedProject();
                                    }
                                });
                                return null;
                            }

                        });
                        SwingUtilities.invokeLater(new );
                    }

                });
                dialog.setVisible(true);
            }
        }

        private void actionPerformed(ActionEvent e, ProjectCustomizer.Category[] categs) {
            for (ProjectCustomizer.Category category : categs) {
                ActionListener list = category.getOkButtonListener();
                if (list != null) {
                    list.actionPerformed(e);
                }
                if (category.getSubcategories() == null) continue;
                this.actionPerformed(e, category.getSubcategories());
            }
        }

        private void storePerformed(ActionEvent e, ProjectCustomizer.Category[] categories) {
            for (ProjectCustomizer.Category category : categories) {
                ActionListener listener = category.getStoreListener();
                if (listener != null) {
                    listener.actionPerformed(e);
                }
                if (category.getSubcategories() == null) continue;
                this.storePerformed(e, category.getSubcategories());
            }
        }

        private void saveModifiedProject() {
            if (this.prov != null) {
                Project prj = (Project)this.prov.getLookup().lookup(Project.class);
                if (ProjectManager.getDefault().isModified(prj)) {
                    try {
                        ProjectManager.getDefault().saveProject(prj);
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    catch (IllegalArgumentException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
        }

    }

}

