/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.project.uiapi;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class CategoryChangeSupport {
    public static final CategoryChangeSupport NULL_INSTANCE = new CategoryChangeSupport(){

        @Override
        public void firePropertyChange(String pn, Object o, Object n) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener l) {
        }

        @Override
        void addPropertyChangeListener(PropertyChangeListener l) {
        }
    };
    private PropertyChangeSupport changeSupport;
    public static final String VALID_PROPERTY = "isCategoryValid";
    public static final String ERROR_MESSAGE_PROPERTY = "categoryErrorMessage";

    synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return;
        }
        if (this.changeSupport == null) {
            this.changeSupport = new PropertyChangeSupport(this);
        }
        this.changeSupport.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (listener == null || this.changeSupport == null) {
            return;
        }
        this.changeSupport.removePropertyChangeListener(listener);
    }

    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (this.changeSupport == null || oldValue != null && newValue != null && oldValue.equals(newValue)) {
            return;
        }
        this.changeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

}

