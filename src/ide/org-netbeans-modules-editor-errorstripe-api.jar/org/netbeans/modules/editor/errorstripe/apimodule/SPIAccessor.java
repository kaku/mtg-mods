/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.errorstripe.apimodule;

import java.beans.PropertyChangeListener;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider;

public abstract class SPIAccessor {
    public static SPIAccessor DEFAULT;

    public static final SPIAccessor getDefault() {
        return DEFAULT;
    }

    public abstract void addPropertyChangeListener(UpToDateStatusProvider var1, PropertyChangeListener var2);

    public abstract void removePropertyChangeListener(UpToDateStatusProvider var1, PropertyChangeListener var2);
}

