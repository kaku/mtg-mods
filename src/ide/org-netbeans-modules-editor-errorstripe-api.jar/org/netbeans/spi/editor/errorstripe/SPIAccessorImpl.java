/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.errorstripe;

import java.beans.PropertyChangeListener;
import org.netbeans.modules.editor.errorstripe.apimodule.SPIAccessor;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider;

class SPIAccessorImpl
extends SPIAccessor {
    @Override
    public void removePropertyChangeListener(UpToDateStatusProvider provider, PropertyChangeListener l) {
        provider.removePropertyChangeListener(l);
    }

    @Override
    public void addPropertyChangeListener(UpToDateStatusProvider provider, PropertyChangeListener l) {
        provider.addPropertyChangeListener(l);
    }
}

