/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.errorstripe;

public final class UpToDateStatus
implements Comparable {
    private static final int UP_TO_DATE_OK_VALUE = 0;
    private static final int UP_TO_DATE_PROCESSING_VALUE = 1;
    private static final int UP_TO_DATE_DIRTY_VALUE = 2;
    public static final UpToDateStatus UP_TO_DATE_OK = new UpToDateStatus(0);
    public static final UpToDateStatus UP_TO_DATE_PROCESSING = new UpToDateStatus(1);
    public static final UpToDateStatus UP_TO_DATE_DIRTY = new UpToDateStatus(2);
    private int status;
    private static final String[] statusNames = new String[]{"OK", "PROCESSING", "DIRTY"};

    private UpToDateStatus(int status) {
        this.status = status;
    }

    private int getStatus() {
        return this.status;
    }

    public int compareTo(Object o) {
        UpToDateStatus remote = (UpToDateStatus)o;
        return this.status - remote.status;
    }

    public int hashCode() {
        return 73 ^ this.status;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof UpToDateStatus)) {
            return false;
        }
        return this.compareTo(obj) == 0;
    }

    public String toString() {
        return statusNames[this.getStatus()];
    }
}

