/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.errorstripe;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.netbeans.modules.editor.errorstripe.apimodule.SPIAccessor;
import org.netbeans.spi.editor.errorstripe.SPIAccessorImpl;
import org.netbeans.spi.editor.errorstripe.UpToDateStatus;

public abstract class UpToDateStatusProvider {
    public static final String PROP_UP_TO_DATE = "upToDate";
    private PropertyChangeSupport pcs;

    public UpToDateStatusProvider() {
        this.pcs = new PropertyChangeSupport(this);
    }

    public abstract UpToDateStatus getUpToDate();

    final void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    final void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String name, Object old, Object nue) {
        this.pcs.firePropertyChange(name, old, nue);
    }

    static {
        SPIAccessor.DEFAULT = new SPIAccessorImpl();
    }
}

