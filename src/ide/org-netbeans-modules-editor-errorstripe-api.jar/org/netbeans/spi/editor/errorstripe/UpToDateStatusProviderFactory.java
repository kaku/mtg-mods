/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor.errorstripe;

import javax.swing.text.Document;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="UpToDateStatusProvider")
public interface UpToDateStatusProviderFactory {
    public UpToDateStatusProvider createUpToDateStatusProvider(Document var1);
}

