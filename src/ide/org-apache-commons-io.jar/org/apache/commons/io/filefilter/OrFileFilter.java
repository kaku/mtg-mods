/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.ConditionalFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;

public class OrFileFilter
extends AbstractFileFilter
implements ConditionalFileFilter,
Serializable {
    private List fileFilters;

    public OrFileFilter() {
        this.fileFilters = new ArrayList();
    }

    public OrFileFilter(List fileFilters) {
        this.fileFilters = fileFilters == null ? new ArrayList() : new ArrayList(fileFilters);
    }

    public OrFileFilter(IOFileFilter filter1, IOFileFilter filter2) {
        if (filter1 == null || filter2 == null) {
            throw new IllegalArgumentException("The filters must not be null");
        }
        this.fileFilters = new ArrayList();
        this.addFileFilter(filter1);
        this.addFileFilter(filter2);
    }

    public void addFileFilter(IOFileFilter ioFileFilter) {
        this.fileFilters.add(ioFileFilter);
    }

    public List getFileFilters() {
        return Collections.unmodifiableList(this.fileFilters);
    }

    public boolean removeFileFilter(IOFileFilter ioFileFilter) {
        return this.fileFilters.remove(ioFileFilter);
    }

    public void setFileFilters(List fileFilters) {
        this.fileFilters = fileFilters;
    }

    public boolean accept(File file) {
        Iterator iter = this.fileFilters.iterator();
        while (iter.hasNext()) {
            IOFileFilter fileFilter = (IOFileFilter)iter.next();
            if (!fileFilter.accept(file)) continue;
            return true;
        }
        return false;
    }

    public boolean accept(File file, String name) {
        Iterator iter = this.fileFilters.iterator();
        while (iter.hasNext()) {
            IOFileFilter fileFilter = (IOFileFilter)iter.next();
            if (!fileFilter.accept(file, name)) continue;
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(super.toString());
        buffer.append("(");
        if (this.fileFilters != null) {
            for (int i = 0; i < this.fileFilters.size(); ++i) {
                Object filter;
                if (i > 0) {
                    buffer.append(",");
                }
                buffer.append((filter = this.fileFilters.get(i)) == null ? "null" : filter.toString());
            }
        }
        buffer.append(")");
        return buffer.toString();
    }
}

