/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.ConditionalFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;

public class AndFileFilter
extends AbstractFileFilter
implements ConditionalFileFilter,
Serializable {
    private List fileFilters;

    public AndFileFilter() {
        this.fileFilters = new ArrayList();
    }

    public AndFileFilter(List fileFilters) {
        this.fileFilters = fileFilters == null ? new ArrayList() : new ArrayList(fileFilters);
    }

    public AndFileFilter(IOFileFilter filter1, IOFileFilter filter2) {
        if (filter1 == null || filter2 == null) {
            throw new IllegalArgumentException("The filters must not be null");
        }
        this.fileFilters = new ArrayList();
        this.addFileFilter(filter1);
        this.addFileFilter(filter2);
    }

    public void addFileFilter(IOFileFilter ioFileFilter) {
        this.fileFilters.add(ioFileFilter);
    }

    public List getFileFilters() {
        return Collections.unmodifiableList(this.fileFilters);
    }

    public boolean removeFileFilter(IOFileFilter ioFileFilter) {
        return this.fileFilters.remove(ioFileFilter);
    }

    public void setFileFilters(List fileFilters) {
        this.fileFilters = new ArrayList(fileFilters);
    }

    public boolean accept(File file) {
        if (this.fileFilters.size() == 0) {
            return false;
        }
        Iterator iter = this.fileFilters.iterator();
        while (iter.hasNext()) {
            IOFileFilter fileFilter = (IOFileFilter)iter.next();
            if (fileFilter.accept(file)) continue;
            return false;
        }
        return true;
    }

    public boolean accept(File file, String name) {
        if (this.fileFilters.size() == 0) {
            return false;
        }
        Iterator iter = this.fileFilters.iterator();
        while (iter.hasNext()) {
            IOFileFilter fileFilter = (IOFileFilter)iter.next();
            if (fileFilter.accept(file, name)) continue;
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(super.toString());
        buffer.append("(");
        if (this.fileFilters != null) {
            for (int i = 0; i < this.fileFilters.size(); ++i) {
                Object filter;
                if (i > 0) {
                    buffer.append(",");
                }
                buffer.append((filter = this.fileFilters.get(i)) == null ? "null" : filter.toString());
            }
        }
        buffer.append(")");
        return buffer.toString();
    }
}

