/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import org.apache.commons.io.filefilter.IOFileFilter;

public class TrueFileFilter
implements IOFileFilter,
Serializable {
    public static final IOFileFilter TRUE;
    public static final IOFileFilter INSTANCE;

    protected TrueFileFilter() {
    }

    public boolean accept(File file) {
        return true;
    }

    public boolean accept(File dir, String name) {
        return true;
    }

    static {
        INSTANCE = TrueFileFilter.TRUE = new TrueFileFilter();
    }
}

