/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

public interface IOFileFilter
extends FileFilter,
FilenameFilter {
    public boolean accept(File var1);

    public boolean accept(File var1, String var2);
}

