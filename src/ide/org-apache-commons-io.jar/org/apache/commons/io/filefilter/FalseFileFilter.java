/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import org.apache.commons.io.filefilter.IOFileFilter;

public class FalseFileFilter
implements IOFileFilter,
Serializable {
    public static final IOFileFilter FALSE;
    public static final IOFileFilter INSTANCE;

    protected FalseFileFilter() {
    }

    public boolean accept(File file) {
        return false;
    }

    public boolean accept(File dir, String name) {
        return false;
    }

    static {
        INSTANCE = FalseFileFilter.FALSE = new FalseFileFilter();
    }
}

