/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

public class FileSystemUtils {
    private static final FileSystemUtils INSTANCE = new FileSystemUtils();
    private static final int INIT_PROBLEM = -1;
    private static final int OTHER = 0;
    private static final int WINDOWS = 1;
    private static final int UNIX = 2;
    private static final int POSIX_UNIX = 3;
    private static final int OS;

    public static long freeSpace(String path) throws IOException {
        return INSTANCE.freeSpaceOS(path, OS, false);
    }

    public static long freeSpaceKb(String path) throws IOException {
        return INSTANCE.freeSpaceOS(path, OS, true);
    }

    long freeSpaceOS(String path, int os, boolean kb) throws IOException {
        if (path == null) {
            throw new IllegalArgumentException("Path must not be empty");
        }
        switch (os) {
            case 1: {
                return kb ? this.freeSpaceWindows(path) / 1024 : this.freeSpaceWindows(path);
            }
            case 2: {
                return this.freeSpaceUnix(path, kb, false);
            }
            case 3: {
                return this.freeSpaceUnix(path, kb, true);
            }
            case 0: {
                throw new IllegalStateException("Unsupported operating system");
            }
        }
        throw new IllegalStateException("Exception caught when determining operating system");
    }

    long freeSpaceWindows(String path) throws IOException {
        if ((path = FilenameUtils.normalize(path)).length() > 2 && path.charAt(1) == ':') {
            path = path.substring(0, 2);
        }
        String[] cmdAttribs = new String[]{"cmd.exe", "/C", "dir /-c " + path};
        List lines = this.performCommand(cmdAttribs, Integer.MAX_VALUE);
        for (int i = lines.size() - 1; i >= 0; --i) {
            String line = (String)lines.get(i);
            if (line.length() <= 0) continue;
            return this.parseDir(line, path);
        }
        throw new IOException("Command line 'dir /-c' did not return any info for path '" + path + "'");
    }

    long parseDir(String line, String path) throws IOException {
        char c;
        int j;
        int bytesStart = 0;
        int bytesEnd = 0;
        for (j = line.length() - 1; j >= 0; --j) {
            c = line.charAt(j);
            if (!Character.isDigit(c)) continue;
            bytesEnd = j + 1;
            break;
        }
        while (j >= 0) {
            c = line.charAt(j);
            if (!Character.isDigit(c) && c != ',' && c != '.') {
                bytesStart = j + 1;
                break;
            }
            --j;
        }
        if (j < 0) {
            throw new IOException("Command line 'dir /-c' did not return valid info for path '" + path + "'");
        }
        StringBuffer buf = new StringBuffer(line.substring(bytesStart, bytesEnd));
        for (int k = 0; k < buf.length(); ++k) {
            if (buf.charAt(k) != ',' && buf.charAt(k) != '.') continue;
            buf.deleteCharAt(k--);
        }
        return this.parseBytes(buf.toString(), path);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    long freeSpaceUnix(String path, boolean kb, boolean posix) throws IOException {
        String[] arrstring;
        if (path.length() == 0) {
            throw new IllegalArgumentException("Path must not be empty");
        }
        path = FilenameUtils.normalize(path);
        String flags = "-";
        if (kb) {
            flags = flags + "k";
        }
        if (posix) {
            flags = flags + "P";
        }
        if (flags.length() > 1) {
            String[] arrstring2 = new String[3];
            arrstring2[0] = "df";
            arrstring2[1] = flags;
            arrstring = arrstring2;
            arrstring2[2] = path;
        } else {
            String[] arrstring3 = new String[2];
            arrstring3[0] = "df";
            arrstring = arrstring3;
            arrstring3[1] = path;
        }
        String[] cmdAttribs = arrstring;
        List lines = this.performCommand(cmdAttribs, 3);
        if (lines.size() < 2) {
            throw new IOException("Command line 'df' did not return info as expected for path '" + path + "'- response was " + lines);
        }
        String line2 = (String)lines.get(1);
        StringTokenizer tok = new StringTokenizer(line2, " ");
        if (tok.countTokens() < 4) {
            if (tok.countTokens() != 1 || lines.size() < 3) throw new IOException("Command line 'df' did not return data as expected for path '" + path + "'- check path is valid");
            String line3 = (String)lines.get(2);
            tok = new StringTokenizer(line3, " ");
        } else {
            tok.nextToken();
        }
        tok.nextToken();
        tok.nextToken();
        String freeSpace = tok.nextToken();
        return this.parseBytes(freeSpace, path);
    }

    long parseBytes(String freeSpace, String path) throws IOException {
        try {
            long bytes = Long.parseLong(freeSpace);
            if (bytes < 0) {
                throw new IOException("Command line 'df' did not find free space in response for path '" + path + "'- check path is valid");
            }
            return bytes;
        }
        catch (NumberFormatException ex) {
            throw new IOException("Command line 'df' did not return numeric data as expected for path '" + path + "'- check path is valid");
        }
    }

    List performCommand(String[] cmdAttribs, int max) throws IOException {
        ArrayList<String> lines = new ArrayList<String>(20);
        Process proc = null;
        InputStream in = null;
        OutputStream out = null;
        InputStream err = null;
        BufferedReader inr = null;
        try {
            proc = this.openProcess(cmdAttribs);
            in = proc.getInputStream();
            out = proc.getOutputStream();
            err = proc.getErrorStream();
            inr = new BufferedReader(new InputStreamReader(in));
            String line = inr.readLine();
            while (line != null && lines.size() < max) {
                line = line.toLowerCase().trim();
                lines.add(line);
                line = inr.readLine();
            }
            proc.waitFor();
            if (proc.exitValue() != 0) {
                throw new IOException("Command line returned OS error code '" + proc.exitValue() + "' for command " + Arrays.asList(cmdAttribs));
            }
            if (lines.size() == 0) {
                throw new IOException("Command line did not return any info for command " + Arrays.asList(cmdAttribs));
            }
            ArrayList<String> arrayList = lines;
            return arrayList;
        }
        catch (InterruptedException ex) {
            throw new IOException("Command line threw an InterruptedException '" + ex.getMessage() + "' for command " + Arrays.asList(cmdAttribs));
        }
        finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(err);
            IOUtils.closeQuietly(inr);
            if (proc != null) {
                proc.destroy();
            }
        }
    }

    Process openProcess(String[] cmdAttribs) throws IOException {
        return Runtime.getRuntime().exec(cmdAttribs);
    }

    static {
        int os = 0;
        try {
            String osName = System.getProperty("os.name");
            if (osName == null) {
                throw new IOException("os.name not found");
            }
            os = (osName = osName.toLowerCase()).indexOf("windows") != -1 ? 1 : (osName.indexOf("linux") != -1 || osName.indexOf("sun os") != -1 || osName.indexOf("sunos") != -1 || osName.indexOf("solaris") != -1 || osName.indexOf("mpe/ix") != -1 || osName.indexOf("freebsd") != -1 || osName.indexOf("irix") != -1 || osName.indexOf("digital unix") != -1 || osName.indexOf("unix") != -1 || osName.indexOf("mac os x") != -1 ? 2 : (osName.indexOf("hp-ux") != -1 || osName.indexOf("aix") != -1 ? 3 : 0));
        }
        catch (Exception ex) {
            os = -1;
        }
        OS = os;
    }
}

