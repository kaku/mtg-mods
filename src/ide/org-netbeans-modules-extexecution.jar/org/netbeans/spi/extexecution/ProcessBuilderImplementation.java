/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.spi.extexecution;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;

public interface ProcessBuilderImplementation {
    @NonNull
    public Process createProcess(@NonNull String var1, @NullAllowed String var2, @NonNull List<String> var3, @NonNull List<String> var4, @NonNull Map<String, String> var5, boolean var6) throws IOException;
}

