/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.extexecution.destroy;

import java.util.Map;

public interface ProcessDestroyPerformer {
    public void destroy(Process var1, Map<String, String> var2);
}

