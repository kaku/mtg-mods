/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.extexecution.open;

import java.net.URL;
import org.netbeans.api.annotations.common.NonNull;

public interface HttpOpenHandler {
    public void open(@NonNull URL var1);
}

