/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.extexecution.open;

import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;

public interface FileOpenHandler {
    public void open(@NonNull FileObject var1, int var2);
}

