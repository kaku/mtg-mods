/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.extexecution.open;

import org.netbeans.api.annotations.common.NonNull;

public interface OptionOpenHandler {
    public void open(@NonNull String var1);
}

