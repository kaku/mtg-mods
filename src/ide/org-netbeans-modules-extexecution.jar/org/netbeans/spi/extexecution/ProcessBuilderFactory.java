/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.extexecution;

import org.netbeans.api.extexecution.ProcessBuilder;
import org.netbeans.modules.extexecution.ProcessBuilderAccessor;
import org.netbeans.spi.extexecution.ProcessBuilderImplementation;

public class ProcessBuilderFactory {
    private ProcessBuilderFactory() {
    }

    public static ProcessBuilder createProcessBuilder(ProcessBuilderImplementation impl, String description) {
        return ProcessBuilderAccessor.getDefault().createProcessBuilder(impl, description);
    }
}

