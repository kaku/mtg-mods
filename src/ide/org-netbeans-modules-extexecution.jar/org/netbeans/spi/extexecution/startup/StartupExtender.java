/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.extexecution.startup;

import java.util.Map;
import org.netbeans.modules.extexecution.startup.ProxyStartupExtender;
import org.netbeans.spi.extexecution.startup.StartupExtenderImplementation;

final class StartupExtender {
    static StartupExtenderImplementation createProxy(Map<String, ?> map) {
        return new ProxyStartupExtender(map);
    }

    private StartupExtender() {
    }
}

