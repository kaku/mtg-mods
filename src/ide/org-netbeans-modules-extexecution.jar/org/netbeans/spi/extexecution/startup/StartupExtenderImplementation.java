/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.extexecution.startup;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.extexecution.startup.StartupExtender;
import org.openide.util.Lookup;

public interface StartupExtenderImplementation {
    @NonNull
    public List<String> getArguments(@NonNull Lookup var1, @NonNull StartupExtender.StartMode var2);

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE, ElementType.METHOD})
    public static @interface Registration {
        public String displayName();

        public StartupExtender.StartMode[] startMode();

        public int position() default Integer.MAX_VALUE;
    }

}

