/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.windows.InputOutput
 */
package org.netbeans.modules.extexecution;

import java.awt.event.ActionEvent;
import java.util.concurrent.Future;
import javax.swing.AbstractAction;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.extexecution.ExecutionDescriptor;
import org.netbeans.api.extexecution.ExecutionService;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.windows.InputOutput;

public final class RerunAction
extends AbstractAction
implements ChangeListener {
    private InputOutput parent;
    private ExecutionService service;
    private ExecutionDescriptor.RerunCondition condition;
    private ChangeListener listener;

    public RerunAction() {
        this.setEnabled(false);
        this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/extexecution/resources/rerun.png", (boolean)true));
        this.putValue("ShortDescription", NbBundle.getMessage(RerunAction.class, (String)"Rerun"));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setParent(InputOutput parent) {
        RerunAction rerunAction = this;
        synchronized (rerunAction) {
            this.parent = parent;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setExecutionService(ExecutionService service) {
        RerunAction rerunAction = this;
        synchronized (rerunAction) {
            this.service = service;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setRerunCondition(ExecutionDescriptor.RerunCondition condition) {
        RerunAction rerunAction = this;
        synchronized (rerunAction) {
            if (this.condition != null) {
                this.condition.removeChangeListener(this.listener);
            }
            this.condition = condition;
            if (this.condition != null) {
                this.listener = WeakListeners.change((ChangeListener)this, (Object)this.condition);
                this.condition.addChangeListener(this.listener);
            }
        }
        this.stateChanged(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        InputOutput required;
        ExecutionService actionService;
        this.setEnabled(false);
        RerunAction rerunAction = this;
        synchronized (rerunAction) {
            actionService = this.service;
            required = this.parent;
        }
        if (actionService != null) {
            Accessor.getDefault().run(actionService, required);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        Boolean value = null;
        RerunAction rerunAction = this;
        synchronized (rerunAction) {
            if (this.condition != null) {
                value = this.condition.isRerunPossible();
            }
        }
        if (value != null) {
            this.firePropertyChange("enabled", null, value);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean isEnabled() {
        RerunAction rerunAction = this;
        synchronized (rerunAction) {
            return super.isEnabled() && (this.condition == null || this.condition.isRerunPossible());
        }
    }

    public static abstract class Accessor {
        private static volatile Accessor accessor;

        public static void setDefault(Accessor accessor) {
            if (Accessor.accessor != null) {
                throw new IllegalStateException("Already initialized accessor");
            }
            Accessor.accessor = accessor;
        }

        public static Accessor getDefault() {
            block4 : {
                if (accessor != null) {
                    return accessor;
                }
                Class<ExecutionService> c = ExecutionService.class;
                try {
                    Class.forName(c.getName(), true, c.getClassLoader());
                }
                catch (ClassNotFoundException ex) {
                    if ($assertionsDisabled) break block4;
                    throw new AssertionError(ex);
                }
            }
            assert (accessor != null);
            return accessor;
        }

        public abstract Future<Integer> run(ExecutionService var1, InputOutput var2);
    }

}

