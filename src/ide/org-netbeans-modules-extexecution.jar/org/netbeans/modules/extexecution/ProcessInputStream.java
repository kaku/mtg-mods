/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution;

import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ProcessInputStream
extends FilterInputStream {
    private static final Logger LOGGER = Logger.getLogger(ProcessInputStream.class.getName());
    private final Process process;
    private byte[] buffer;
    private int position;
    private boolean closed;
    private boolean exhausted;

    public ProcessInputStream(Process process, InputStream in) {
        super(in);
        this.process = process;
    }

    @Override
    public synchronized int available() throws IOException {
        if (this.buffer != null && this.position < this.buffer.length) {
            return this.buffer.length - this.position;
        }
        if (this.closed) {
            if (!this.exhausted) {
                this.exhausted = true;
                return 0;
            }
            throw new IOException("Already closed stream");
        }
        return super.available();
    }

    @Override
    public synchronized void close() throws IOException {
        if (!this.closed) {
            this.close(false);
        }
    }

    @Override
    public void mark(int readlimit) {
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public synchronized int read() throws IOException {
        if (this.buffer != null && this.position < this.buffer.length) {
            return this.buffer[this.position++];
        }
        if (this.closed) {
            if (!this.exhausted) {
                this.exhausted = true;
                return -1;
            }
            throw new IOException("Already closed stream");
        }
        return super.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) throws IOException {
        if (this.buffer != null) {
            int available = this.buffer.length - this.position;
            int size = Math.min(len, available);
            System.arraycopy(this.buffer, this.position, b, off, size);
            this.position += size;
            return size;
        }
        if (this.closed) {
            if (!this.exhausted) {
                this.exhausted = true;
                return -1;
            }
            throw new IOException("Already closed stream");
        }
        return super.read(b, off, len);
    }

    @Override
    public void reset() throws IOException {
    }

    @Override
    public long skip(long n) throws IOException {
        return 0;
    }

    public synchronized void close(boolean drain) throws IOException {
        this.closed = true;
        if (drain) {
            LOGGER.log(Level.FINE, "Draining process stream");
            boolean running = false;
            try {
                this.process.exitValue();
            }
            catch (IllegalThreadStateException ex) {
                running = true;
            }
            if (running) {
                LOGGER.log(Level.FINE, "Process is still running");
            }
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            try {
                if (running) {
                    while (super.available() > 0) {
                        os.write(super.read());
                    }
                } else {
                    int read;
                    while ((read = super.read()) >= 0) {
                        os.write(read);
                    }
                }
            }
            catch (IOException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
            this.buffer = os.toByteArray();
            LOGGER.log(Level.FINE, "Read {0} bytes from stream", this.buffer.length);
        }
        super.close();
    }
}

