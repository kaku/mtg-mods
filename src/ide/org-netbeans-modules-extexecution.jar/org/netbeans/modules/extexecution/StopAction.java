/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.extexecution;

import java.awt.event.ActionEvent;
import java.util.concurrent.Future;
import javax.swing.AbstractAction;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class StopAction
extends AbstractAction {
    private Future<?> task;

    public StopAction() {
        this.setEnabled(false);
        this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/extexecution/resources/stop.png", (boolean)true));
        this.putValue("ShortDescription", NbBundle.getMessage(StopAction.class, (String)"Stop"));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setTask(Future<?> task) {
        StopAction stopAction = this;
        synchronized (stopAction) {
            this.task = task;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Future actionTask;
        this.setEnabled(false);
        StopAction stopAction = this;
        synchronized (stopAction) {
            actionTask = this.task;
        }
        if (actionTask != null) {
            actionTask.cancel(true);
        }
    }
}

