/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import org.netbeans.api.extexecution.ExternalProcessSupport;

public class WrapperProcess
extends Process {
    public static final String KEY_UUID = "NB_EXEC_EXTEXECUTION_PROCESS_UUID";
    private final String uuid;
    private final Process del;

    public WrapperProcess(Process delegate, String uuid) {
        this.del = delegate;
        this.uuid = uuid;
    }

    @Override
    public OutputStream getOutputStream() {
        return this.del.getOutputStream();
    }

    @Override
    public InputStream getInputStream() {
        return this.del.getInputStream();
    }

    @Override
    public InputStream getErrorStream() {
        return this.del.getErrorStream();
    }

    @Override
    public int waitFor() throws InterruptedException {
        return this.del.waitFor();
    }

    @Override
    public int exitValue() {
        return this.del.exitValue();
    }

    @Override
    public void destroy() {
        ExternalProcessSupport.destroy(this.del, Collections.singletonMap("NB_EXEC_EXTEXECUTION_PROCESS_UUID", this.uuid));
    }
}

