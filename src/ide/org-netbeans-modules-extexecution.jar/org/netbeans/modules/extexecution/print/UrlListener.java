/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 */
package org.netbeans.modules.extexecution.print;

import java.net.URL;
import org.netbeans.spi.extexecution.open.HttpOpenHandler;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;

public class UrlListener
implements OutputListener {
    private final URL url;
    private final HttpOpenHandler handler;

    public UrlListener(URL url, HttpOpenHandler handler) {
        this.url = url;
        this.handler = handler;
    }

    public void outputLineAction(OutputEvent ev) {
        this.handler.open(this.url);
    }

    public void outputLineCleared(OutputEvent ev) {
    }

    public void outputLineSelected(OutputEvent ev) {
    }
}

