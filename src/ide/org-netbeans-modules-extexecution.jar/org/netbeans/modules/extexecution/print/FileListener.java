/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 */
package org.netbeans.modules.extexecution.print;

import java.io.File;
import java.util.logging.Logger;
import org.netbeans.api.extexecution.print.LineConvertors;
import org.netbeans.spi.extexecution.open.FileOpenHandler;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;

public class FileListener
implements OutputListener {
    private static final Logger LOGGER = Logger.getLogger(FileListener.class.getName());
    private final String file;
    private final int lineno;
    private final LineConvertors.FileLocator fileLocator;
    private final FileOpenHandler handler;

    public FileListener(String file, int line, LineConvertors.FileLocator fileLocator, FileOpenHandler handler) {
        if (line < 0) {
            line = 0;
        }
        this.file = file;
        this.lineno = line;
        this.fileLocator = fileLocator;
        this.handler = handler;
    }

    public void outputLineSelected(OutputEvent ev) {
    }

    public void outputLineAction(OutputEvent ev) {
        FileObject fo = this.findFile(this.file);
        if (fo != null) {
            this.handler.open(fo, this.lineno);
        }
    }

    public void outputLineCleared(OutputEvent ev) {
    }

    private FileObject findFile(String path) {
        FileObject fo;
        if (this.fileLocator != null && (fo = this.fileLocator.find(path)) != null) {
            return fo;
        }
        File realFile = new File(path);
        if (realFile.isFile()) {
            return FileUtil.toFileObject((File)FileUtil.normalizeFile((File)realFile));
        }
        LOGGER.warning("Cannot resolve file for \"" + path + "\" path.");
        return null;
    }
}

