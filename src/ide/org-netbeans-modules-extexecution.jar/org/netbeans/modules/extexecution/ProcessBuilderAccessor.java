/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution;

import org.netbeans.api.extexecution.ProcessBuilder;
import org.netbeans.spi.extexecution.ProcessBuilderImplementation;

public abstract class ProcessBuilderAccessor {
    private static volatile ProcessBuilderAccessor DEFAULT;

    public static ProcessBuilderAccessor getDefault() {
        block3 : {
            ProcessBuilderAccessor a = DEFAULT;
            if (a != null) {
                return a;
            }
            Class<ProcessBuilder> c = ProcessBuilder.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError(ex);
            }
        }
        return DEFAULT;
    }

    public static void setDefault(ProcessBuilderAccessor accessor) {
        if (DEFAULT != null) {
            throw new IllegalStateException();
        }
        DEFAULT = accessor;
    }

    public abstract ProcessBuilder createProcessBuilder(ProcessBuilderImplementation var1, String var2);
}

