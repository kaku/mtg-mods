/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.extexecution.startup;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.extexecution.startup.StartupExtender;
import org.netbeans.spi.extexecution.startup.StartupExtenderImplementation;
import org.openide.util.Lookup;

public class ProxyStartupExtender
implements StartupExtenderImplementation {
    private final Map<String, ?> attributes;
    private final Set<StartupExtender.StartMode> startMode;
    private StartupExtenderImplementation delegate;

    public ProxyStartupExtender(Map<String, ?> attributes) {
        this.attributes = attributes;
        String startModeValue = (String)attributes.get("startMode");
        this.startMode = EnumSet.noneOf(StartupExtender.StartMode.class);
        if (startModeValue != null) {
            for (String value : startModeValue.split(",")) {
                this.startMode.add(StartupExtender.StartMode.valueOf(value));
            }
        }
    }

    @Override
    public List<String> getArguments(Lookup context, StartupExtender.StartMode mode) {
        if (this.startMode.contains((Object)mode)) {
            return this.getDelegate().getArguments(context, mode);
        }
        return Collections.emptyList();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private StartupExtenderImplementation getDelegate() {
        ProxyStartupExtender proxyStartupExtender = this;
        synchronized (proxyStartupExtender) {
            if (this.delegate != null) {
                return this.delegate;
            }
        }
        StartupExtenderImplementation provider = (StartupExtenderImplementation)this.attributes.get("delegate");
        if (provider == null) {
            throw new IllegalStateException("Delegate must not be null");
        }
        ProxyStartupExtender proxyStartupExtender2 = this;
        synchronized (proxyStartupExtender2) {
            if (this.delegate == null) {
                this.delegate = provider;
            }
            return this.delegate;
        }
    }
}

