/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.extexecution.startup;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import org.netbeans.api.extexecution.startup.StartupExtender;
import org.netbeans.spi.extexecution.startup.StartupExtenderImplementation;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedAnnotationTypes(value={"org.netbeans.spi.extexecution.startup.StartupExtenderImplementation.Registration"})
@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class StartupExtenderRegistrationProcessor
extends LayerGeneratingProcessor {
    public static final String PATH = "StartupExtender";
    public static final String DELEGATE_ATTRIBUTE = "delegate";
    public static final String START_MODE_ATTRIBUTE = "startMode";

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element element : roundEnv.getElementsAnnotatedWith(StartupExtenderImplementation.Registration.class)) {
            StartupExtenderImplementation.Registration annotation = element.getAnnotation(StartupExtenderImplementation.Registration.class);
            if (annotation == null) continue;
            StringBuilder builder = new StringBuilder();
            for (StartupExtender.StartMode mode : annotation.startMode()) {
                builder.append(mode.name()).append(",");
            }
            if (builder.length() > 0) {
                builder.setLength(builder.length() - 1);
            }
            LayerBuilder.File f = this.layer(new Element[]{element}).instanceFile("StartupExtender", null).instanceAttribute("delegate", StartupExtenderImplementation.class, (Annotation)annotation, null).stringvalue("startMode", builder.toString()).bundlevalue("displayName", element.getAnnotation(StartupExtenderImplementation.Registration.class).displayName()).methodvalue("instanceCreate", "org.netbeans.spi.extexecution.startup.StartupExtender", "createProxy").position(element.getAnnotation(StartupExtenderImplementation.Registration.class).position());
            f.write();
        }
        return true;
    }
}

