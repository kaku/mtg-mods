/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 */
package org.netbeans.modules.extexecution;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.modules.extexecution.OptionsAction;
import org.netbeans.modules.extexecution.RerunAction;
import org.netbeans.modules.extexecution.StopAction;
import org.netbeans.spi.extexecution.open.OptionOpenHandler;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

public final class InputOutputManager {
    private static final Logger LOGGER = Logger.getLogger(InputOutputManager.class.getName());
    private static final Map<InputOutput, InputOutputData> AVAILABLE = new WeakHashMap<InputOutput, InputOutputData>();
    private static final Set<String> ACTIVE_DISPLAY_NAMES = new HashSet<String>();

    private InputOutputManager() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void addInputOutput(InputOutputData data) {
        Class<InputOutputManager> class_ = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            AVAILABLE.put(data.inputOutput, data);
            ACTIVE_DISPLAY_NAMES.remove(data.displayName);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static InputOutputData getInputOutput(String name, boolean actions, String optionsPath) {
        InputOutputData result = null;
        TreeSet<InputOutputData> candidates = new TreeSet<InputOutputData>(DISPLAY_NAME_COMPARATOR);
        Class<InputOutputManager> class_ = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            Iterator<Map.Entry<InputOutput, InputOutputData>> it = AVAILABLE.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<InputOutput, InputOutputData> entry = it.next();
                InputOutput free = entry.getKey();
                InputOutputData data = entry.getValue();
                if (free.isClosed()) {
                    it.remove();
                    continue;
                }
                if (InputOutputManager.isAppropriateName(name, data.displayName) && (actions && data.rerunAction != null && data.stopAction != null || !actions && data.rerunAction == null && data.stopAction == null) && (optionsPath != null && data.optionsAction != null && data.optionsAction.getOptionsPath().equals(optionsPath) || optionsPath == null && data.optionsAction == null)) {
                    candidates.add(data);
                }
                LOGGER.log(Level.FINEST, "InputOutputManager pool: {0}", data.getDisplayName());
            }
            if (!candidates.isEmpty()) {
                result = (InputOutputData)candidates.first();
                AVAILABLE.remove((Object)result.inputOutput);
                ACTIVE_DISPLAY_NAMES.add(result.displayName);
            }
            // ** MonitorExit[var5_5] (shouldn't be in output)
            return result;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static InputOutputData getInputOutput(InputOutput inputOutput) {
        InputOutputData result = null;
        Class<InputOutputManager> class_ = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            Iterator<Map.Entry<InputOutput, InputOutputData>> it = AVAILABLE.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<InputOutput, InputOutputData> entry = it.next();
                InputOutput free = entry.getKey();
                InputOutputData data = entry.getValue();
                if (free.isClosed()) {
                    it.remove();
                    continue;
                }
                if (free.equals((Object)inputOutput)) {
                    result = data;
                    ACTIVE_DISPLAY_NAMES.add(result.displayName);
                    it.remove();
                }
                LOGGER.log(Level.FINEST, "InputOutputManager pool: {0}", data.getDisplayName());
            }
            // ** MonitorExit[var2_2] (shouldn't be in output)
            return result;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static InputOutputData createInputOutput(String originalDisplayName, boolean controlActions, String optionsPath) {
        Class<InputOutputManager> class_ = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            InputOutput io;
            String displayName = InputOutputManager.getNonActiveDisplayName(originalDisplayName);
            StopAction stopAction = null;
            RerunAction rerunAction = null;
            OptionsAction optionsAction = null;
            if (controlActions) {
                stopAction = new StopAction();
                rerunAction = new RerunAction();
                if (optionsPath != null) {
                    OptionOpenHandler handler = (OptionOpenHandler)Lookup.getDefault().lookup(OptionOpenHandler.class);
                    if (handler != null) {
                        optionsAction = new OptionsAction(handler, optionsPath);
                        io = IOProvider.getDefault().getIO(displayName, new Action[]{rerunAction, stopAction, optionsAction});
                    } else {
                        LOGGER.log(Level.WARNING, "No available OptionsOpenHandler so no Options button");
                        io = IOProvider.getDefault().getIO(displayName, new Action[]{rerunAction, stopAction});
                    }
                } else {
                    io = IOProvider.getDefault().getIO(displayName, new Action[]{rerunAction, stopAction});
                }
                rerunAction.setParent(io);
            } else if (optionsPath != null) {
                OptionOpenHandler handler = (OptionOpenHandler)Lookup.getDefault().lookup(OptionOpenHandler.class);
                if (handler != null) {
                    optionsAction = new OptionsAction(handler, optionsPath);
                    io = IOProvider.getDefault().getIO(displayName, new Action[]{optionsAction});
                } else {
                    LOGGER.log(Level.WARNING, "No available OptionsOpenHandler so no Options button");
                    io = IOProvider.getDefault().getIO(displayName, true);
                }
            } else {
                io = IOProvider.getDefault().getIO(displayName, true);
            }
            ACTIVE_DISPLAY_NAMES.add(displayName);
            // ** MonitorExit[var3_3] (shouldn't be in output)
            return new InputOutputData(io, displayName, stopAction, rerunAction, optionsAction);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void clear() {
        Class<InputOutputManager> class_ = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            AVAILABLE.clear();
            ACTIVE_DISPLAY_NAMES.clear();
            // ** MonitorExit[var0] (shouldn't be in output)
            return;
        }
    }

    private static boolean isAppropriateName(String base, String toMatch) {
        if (!toMatch.startsWith(base)) {
            return false;
        }
        return toMatch.substring(base.length()).matches("^(\\ #[0-9]+)?$");
    }

    private static String getNonActiveDisplayName(String displayNameBase) {
        String nonActiveDN = displayNameBase;
        if (ACTIVE_DISPLAY_NAMES.contains(nonActiveDN)) {
            String testdn;
            int i = 2;
            while (ACTIVE_DISPLAY_NAMES.contains(testdn = NbBundle.getMessage(InputOutputManager.class, (String)"Uniquified", (Object)nonActiveDN, (Object)i++))) {
            }
            nonActiveDN = testdn;
        }
        assert (!ACTIVE_DISPLAY_NAMES.contains(nonActiveDN));
        return nonActiveDN;
    }

    public static final class InputOutputData {
        private static final Comparator<InputOutputData> DISPLAY_NAME_COMPARATOR = new Comparator<InputOutputData>(){

            @Override
            public int compare(InputOutputData o1, InputOutputData o2) {
                return o1.displayName.compareTo(o2.displayName);
            }
        };
        private final InputOutput inputOutput;
        private final String displayName;
        private final StopAction stopAction;
        private final RerunAction rerunAction;
        private final OptionsAction optionsAction;

        public InputOutputData(InputOutput inputOutput, String displayName, StopAction stopAction, RerunAction rerunAction, OptionsAction optionsAction) {
            this.displayName = displayName;
            this.stopAction = stopAction;
            this.rerunAction = rerunAction;
            this.inputOutput = inputOutput;
            this.optionsAction = optionsAction;
        }

        public InputOutput getInputOutput() {
            return this.inputOutput;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public RerunAction getRerunAction() {
            return this.rerunAction;
        }

        public StopAction getStopAction() {
            return this.stopAction;
        }

        public OptionsAction getOptionsAction() {
            return this.optionsAction;
        }

    }

}

