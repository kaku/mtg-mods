/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.InputReader;

public class DefaultInputReader
implements InputReader {
    private static final Logger LOGGER = Logger.getLogger(DefaultInputReader.class.getName());
    private static final int BUFFER_SIZE = 512;
    private final Reader reader;
    private final char[] buffer;
    private final boolean greedy;
    private boolean closed;

    public DefaultInputReader(Reader reader, boolean greedy) {
        assert (reader != null);
        this.reader = new BufferedReader(reader);
        this.greedy = greedy;
        this.buffer = new char[greedy ? 1024 : 512];
    }

    @Override
    public int readInput(InputProcessor inputProcessor) throws IOException {
        if (this.closed) {
            throw new IllegalStateException("Already closed reader");
        }
        if (!this.reader.ready()) {
            return 0;
        }
        int fetched = 0;
        StringBuilder builder = new StringBuilder();
        do {
            int size;
            if ((size = this.reader.read(this.buffer)) <= 0) continue;
            builder.append(this.buffer, 0, size);
            fetched += size;
        } while (this.reader.ready() && this.greedy);
        if (inputProcessor != null && fetched > 0) {
            inputProcessor.processInput(builder.toString().toCharArray());
        }
        return fetched;
    }

    @Override
    public void close() throws IOException {
        this.closed = true;
        this.reader.close();
        LOGGER.log(Level.FINEST, "Reader closed");
    }
}

