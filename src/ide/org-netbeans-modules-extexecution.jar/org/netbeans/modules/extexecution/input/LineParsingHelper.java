/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution.input;

import java.nio.CharBuffer;
import java.util.ArrayList;

public final class LineParsingHelper {
    private String trailingLine;

    public String[] parse(char[] buffer) {
        return this.parse(buffer, 0, buffer.length);
    }

    public String[] parse(char[] buffer, int offset, int limit) {
        return this.parse(CharBuffer.wrap(buffer, offset, limit));
    }

    public String[] parse(CharSequence input) {
        String lines = this.trailingLine != null ? this.trailingLine : "";
        lines = lines + input.toString();
        int tlLength = this.trailingLine != null ? this.trailingLine.length() : 0;
        int start = 0;
        ArrayList<String> ret = new ArrayList<String>();
        int length = input.length();
        for (int i = 0; i < length; ++i) {
            String line;
            char c = input.charAt(i);
            if (c == '\r' && (i + 1 == length || input.charAt(i + 1) != '\n') || c == '\n') {
                line = lines.substring(start, tlLength + i);
                start = tlLength + (i + 1);
                ret.add(line);
                continue;
            }
            if (c != '\r' || i + 1 >= length || input.charAt(i + 1) != '\n') continue;
            line = lines.substring(start, tlLength + i);
            start = tlLength + (++i + 1);
            ret.add(line);
        }
        this.trailingLine = start < lines.length() ? lines.substring(start) : null;
        return ret.toArray(new String[ret.size()]);
    }

    public String getTrailingLine(boolean flush) {
        String line = this.trailingLine;
        if (flush) {
            this.trailingLine = null;
        }
        return "".equals(line) ? null : line;
    }
}

