/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution.input;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.InputReader;
import org.netbeans.api.extexecution.input.InputReaders;

public class FileInputReader
implements InputReader {
    private static final Logger LOGGER = Logger.getLogger(FileInputReader.class.getName());
    private static final int BUFFER_SIZE = 512;
    private final InputReaders.FileInput.Provider fileProvider;
    private final char[] buffer = new char[512];
    private InputReaders.FileInput currentFile;
    private Reader reader;
    private long fileLength;
    private boolean closed;

    public FileInputReader(InputReaders.FileInput.Provider fileProvider) {
        assert (fileProvider != null);
        this.fileProvider = fileProvider;
    }

    @Override
    public int readInput(InputProcessor inputProcessor) {
        int fetched;
        block12 : {
            if (this.closed) {
                throw new IllegalStateException("Already closed reader");
            }
            fetched = 0;
            try {
                InputReaders.FileInput file = this.fileProvider.getFileInput();
                if (this.currentFile != file && (this.currentFile == null || !this.currentFile.equals(file)) || this.fileLength > this.currentFile.getFile().length() || this.reader == null) {
                    if (this.reader != null) {
                        this.reader.close();
                    }
                    this.currentFile = file;
                    if (this.currentFile != null && this.currentFile.getFile().exists() && this.currentFile.getFile().canRead()) {
                        this.reader = new BufferedReader(new InputStreamReader((InputStream)new FileInputStream(this.currentFile.getFile()), this.currentFile.getCharset()));
                    }
                    if (this.fileLength > 0) {
                        inputProcessor.reset();
                    }
                    this.fileLength = 0;
                }
                if (this.reader == null) {
                    return fetched;
                }
                int size = this.reader.read(this.buffer);
                if (size > 0) {
                    this.fileLength += (long)size;
                    fetched += size;
                    if (inputProcessor != null) {
                        char[] toProcess = new char[size];
                        System.arraycopy(this.buffer, 0, toProcess, 0, size);
                        inputProcessor.processInput(toProcess);
                    }
                }
            }
            catch (Exception ex) {
                LOGGER.log(Level.INFO, null, ex);
                if (this.reader == null) break block12;
                try {
                    this.reader.close();
                }
                catch (IOException iex) {
                    LOGGER.log(Level.FINE, null, ex);
                }
            }
        }
        return fetched;
    }

    @Override
    public void close() throws IOException {
        this.closed = true;
        if (this.reader != null) {
            this.reader.close();
            this.reader = null;
        }
    }
}

