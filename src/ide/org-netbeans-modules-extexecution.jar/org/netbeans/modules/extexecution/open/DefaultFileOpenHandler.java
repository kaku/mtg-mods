/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.extexecution.open;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.spi.extexecution.open.FileOpenHandler;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class DefaultFileOpenHandler
implements FileOpenHandler {
    private static final Logger LOGGER = Logger.getLogger(DefaultFileOpenHandler.class.getName());

    @Override
    public void open(FileObject file, int line) {
        File realFile = FileUtil.toFile((FileObject)file);
        if (realFile != null) {
            try {
                Desktop.getDesktop().edit(realFile);
            }
            catch (IOException ex) {
                LOGGER.log(Level.INFO, null, ex);
            }
        }
    }
}

