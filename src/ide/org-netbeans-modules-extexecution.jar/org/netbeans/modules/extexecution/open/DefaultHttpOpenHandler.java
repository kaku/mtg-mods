/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extexecution.open;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.spi.extexecution.open.HttpOpenHandler;

public class DefaultHttpOpenHandler
implements HttpOpenHandler {
    private static final Logger LOGGER = Logger.getLogger(DefaultHttpOpenHandler.class.getName());

    @Override
    public void open(URL url) {
        try {
            Desktop.getDesktop().browse(url.toURI());
        }
        catch (URISyntaxException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
        catch (IOException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
    }
}

