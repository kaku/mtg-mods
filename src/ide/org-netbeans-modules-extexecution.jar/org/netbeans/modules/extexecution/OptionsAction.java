/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.extexecution;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.spi.extexecution.open.OptionOpenHandler;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class OptionsAction
extends AbstractAction {
    private final OptionOpenHandler handler;
    private final String optionsPath;

    public OptionsAction(OptionOpenHandler handler, String optionsPath) {
        this.setEnabled(true);
        this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/extexecution/resources/options.png", (boolean)true));
        this.putValue("ShortDescription", NbBundle.getMessage(OptionsAction.class, (String)"Options"));
        assert (handler != null);
        this.handler = handler;
        this.optionsPath = optionsPath;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.handler.open(this.optionsPath);
    }

    public String getOptionsPath() {
        return this.optionsPath;
    }
}

