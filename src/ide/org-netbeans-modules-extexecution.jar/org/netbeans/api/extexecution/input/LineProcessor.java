/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.extexecution.input;

import java.io.Closeable;
import org.netbeans.api.annotations.common.NonNull;

public interface LineProcessor
extends Closeable {
    public void processLine(@NonNull String var1);

    public void reset();

    @Override
    public void close();
}

