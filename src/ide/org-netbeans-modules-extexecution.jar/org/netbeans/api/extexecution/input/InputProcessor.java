/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.extexecution.input;

import java.io.Closeable;
import java.io.IOException;
import org.netbeans.api.annotations.common.NonNull;

public interface InputProcessor
extends Closeable {
    public void processInput(@NonNull char[] var1) throws IOException;

    public void reset() throws IOException;

    @Override
    public void close() throws IOException;
}

