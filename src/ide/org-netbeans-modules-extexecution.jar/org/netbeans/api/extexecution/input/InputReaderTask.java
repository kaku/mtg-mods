/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Cancellable
 *  org.openide.util.Parameters
 */
package org.netbeans.api.extexecution.input;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.InputReader;
import org.openide.util.Cancellable;
import org.openide.util.Parameters;

public final class InputReaderTask
implements Runnable,
Cancellable {
    private static final Logger LOGGER = Logger.getLogger(InputReaderTask.class.getName());
    private static final int MIN_DELAY = 50;
    private static final int MAX_DELAY = 300;
    private static final int DELAY_INCREMENT = 50;
    private final InputReader inputReader;
    private final InputProcessor inputProcessor;
    private final boolean draining;
    private boolean cancelled;
    private boolean running;

    private InputReaderTask(InputReader inputReader, InputProcessor inputProcessor, boolean draining) {
        this.inputReader = inputReader;
        this.inputProcessor = inputProcessor;
        this.draining = draining;
    }

    @NonNull
    public static InputReaderTask newTask(@NonNull InputReader reader, @NullAllowed InputProcessor processor) {
        Parameters.notNull((CharSequence)"reader", (Object)reader);
        return new InputReaderTask(reader, processor, false);
    }

    @NonNull
    public static InputReaderTask newDrainingTask(@NonNull InputReader reader, @NullAllowed InputProcessor processor) {
        Parameters.notNull((CharSequence)"reader", (Object)reader);
        return new InputReaderTask(reader, processor, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public void run() {
        InputReaderTask inputReaderTask = this;
        synchronized (inputReaderTask) {
            if (this.running) {
                throw new IllegalStateException("Already running task");
            }
            this.running = true;
        }
        boolean interrupted = false;
        try {
            long delay = 50;
            int emptyReads = 0;
            do {
                InputReaderTask inputReaderTask2 = this;
                synchronized (inputReaderTask2) {
                    if (Thread.currentThread().isInterrupted() || this.cancelled) {
                        interrupted = Thread.interrupted();
                        break;
                    }
                }
                int count = this.inputReader.readInput(this.inputProcessor);
                if (count > 0) {
                    delay = 50;
                    emptyReads = 0;
                } else if (emptyReads > 6) {
                    emptyReads = 0;
                    delay = Math.min(delay + 50, 300);
                } else {
                    ++emptyReads;
                }
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.FINEST, "Task {0} sleeping for {1} ms", new Object[]{Thread.currentThread().getName(), delay});
                }
                try {
                    Thread.sleep(delay);
                    continue;
                }
                catch (InterruptedException e) {
                    interrupted = true;
                    break;
                }
            } while (true);
            InputReaderTask count = this;
            synchronized (count) {
                if (!Thread.currentThread().isInterrupted() && !this.cancelled) return;
                {
                    interrupted = Thread.interrupted();
                }
                return;
            }
        }
        catch (Exception ex) {
            LOGGER.log(Level.FINE, null, ex);
            return;
        }
        finally {
            if (this.draining) {
                try {
                    while (this.inputReader.readInput(this.inputProcessor) > 0) {
                        LOGGER.log(Level.FINE, "Draining the rest of the reader");
                    }
                }
                catch (IOException ex) {
                    LOGGER.log(Level.FINE, null, ex);
                }
            }
            try {
                if (this.inputProcessor != null) {
                    this.inputProcessor.close();
                }
                this.inputReader.close();
            }
            catch (IOException ex) {
                LOGGER.log(Level.INFO, null, ex);
            }
            finally {
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean cancel() {
        InputReaderTask inputReaderTask = this;
        synchronized (inputReaderTask) {
            if (this.cancelled) {
                return false;
            }
            this.cancelled = true;
            return true;
        }
    }
}

