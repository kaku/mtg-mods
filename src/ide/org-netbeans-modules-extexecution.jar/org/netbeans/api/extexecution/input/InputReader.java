/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.api.extexecution.input;

import java.io.Closeable;
import java.io.IOException;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.input.InputProcessor;

public interface InputReader
extends Closeable {
    public int readInput(@NullAllowed InputProcessor var1) throws IOException;

    @Override
    public void close() throws IOException;
}

