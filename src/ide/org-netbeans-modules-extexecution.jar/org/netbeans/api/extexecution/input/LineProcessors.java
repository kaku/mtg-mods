/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.api.extexecution.input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.input.LineProcessor;
import org.netbeans.api.extexecution.print.ConvertedLine;
import org.netbeans.api.extexecution.print.LineConvertor;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

public final class LineProcessors {
    private static final Logger LOGGER = Logger.getLogger(LineProcessors.class.getName());

    private LineProcessors() {
    }

    @NonNull
    public static /* varargs */ LineProcessor proxy(@NonNull LineProcessor ... processors) {
        return new ProxyLineProcessor(processors);
    }

    @NonNull
    public static LineProcessor printing(@NonNull OutputWriter out, boolean resetEnabled) {
        return LineProcessors.printing(out, null, resetEnabled);
    }

    @NonNull
    public static LineProcessor printing(@NonNull OutputWriter out, @NullAllowed LineConvertor convertor, boolean resetEnabled) {
        return new PrintingLineProcessor(out, convertor, resetEnabled);
    }

    @NonNull
    public static LineProcessor patternWaiting(@NonNull Pattern pattern, @NonNull CountDownLatch latch) {
        return new WaitingLineProcessor(pattern, latch);
    }

    private static class WaitingLineProcessor
    implements LineProcessor {
        private final Pattern pattern;
        private final CountDownLatch latch;
        private boolean processed;
        private boolean closed;

        public WaitingLineProcessor(Pattern pattern, CountDownLatch latch) {
            assert (pattern != null);
            assert (latch != null);
            this.pattern = pattern;
            this.latch = latch;
        }

        @Override
        public synchronized void processLine(String line) {
            assert (line != null);
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            if (!this.processed && this.pattern.matcher(line).matches()) {
                this.latch.countDown();
                this.processed = true;
            }
        }

        @Override
        public synchronized void reset() {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
        }

        @Override
        public synchronized void close() {
            this.closed = true;
        }
    }

    private static class PrintingLineProcessor
    implements LineProcessor {
        private final OutputWriter out;
        private final LineConvertor convertor;
        private final boolean resetEnabled;
        private boolean closed;

        public PrintingLineProcessor(OutputWriter out, LineConvertor convertor, boolean resetEnabled) {
            assert (out != null);
            this.out = out;
            this.convertor = convertor;
            this.resetEnabled = resetEnabled;
        }

        @Override
        public void processLine(String line) {
            assert (line != null);
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            LOGGER.log(Level.FINEST, line);
            if (this.convertor != null) {
                List<ConvertedLine> convertedLines = this.convertor.convert(line);
                if (convertedLines != null) {
                    for (ConvertedLine converted : convertedLines) {
                        if (converted.getListener() == null) {
                            this.out.println(converted.getText());
                            continue;
                        }
                        try {
                            this.out.println(converted.getText(), converted.getListener());
                        }
                        catch (IOException ex) {
                            LOGGER.log(Level.INFO, null, ex);
                            this.out.println(converted.getText());
                        }
                    }
                } else {
                    this.out.println(line);
                }
            } else {
                this.out.println(line);
            }
            this.out.flush();
        }

        @Override
        public void reset() {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            if (!this.resetEnabled) {
                return;
            }
            try {
                this.out.reset();
            }
            catch (IOException ex) {
                LOGGER.log(Level.INFO, null, ex);
            }
        }

        @Override
        public void close() {
            this.closed = true;
            this.out.flush();
            this.out.close();
        }
    }

    private static class ProxyLineProcessor
    implements LineProcessor {
        private final List<LineProcessor> processors = new ArrayList<LineProcessor>();
        private boolean closed;

        public /* varargs */ ProxyLineProcessor(LineProcessor ... processors) {
            for (LineProcessor processor : processors) {
                if (processor == null) continue;
                this.processors.add(processor);
            }
        }

        @Override
        public void processLine(String line) {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            for (LineProcessor processor : this.processors) {
                processor.processLine(line);
            }
        }

        @Override
        public void reset() {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            for (LineProcessor processor : this.processors) {
                processor.reset();
            }
        }

        @Override
        public void close() {
            this.closed = true;
            for (LineProcessor processor : this.processors) {
                processor.close();
            }
        }
    }

}

