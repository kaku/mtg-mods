/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 *  org.openide.windows.OutputListener
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.api.extexecution.input;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.LineProcessor;
import org.netbeans.api.extexecution.print.ConvertedLine;
import org.netbeans.api.extexecution.print.LineConvertor;
import org.netbeans.modules.extexecution.input.LineParsingHelper;
import org.openide.util.Parameters;
import org.openide.windows.OutputListener;
import org.openide.windows.OutputWriter;

public final class InputProcessors {
    private static final Logger LOGGER = Logger.getLogger(InputProcessors.class.getName());

    private InputProcessors() {
    }

    @NonNull
    public static InputProcessor bridge(@NonNull LineProcessor lineProcessor) {
        return new Bridge(lineProcessor);
    }

    @NonNull
    public static /* varargs */ InputProcessor proxy(@NonNull InputProcessor ... processors) {
        return new ProxyInputProcessor(processors);
    }

    @NonNull
    public static InputProcessor copying(@NonNull Writer writer) {
        return new CopyingInputProcessor(writer);
    }

    @NonNull
    public static InputProcessor printing(@NonNull OutputWriter out, boolean resetEnabled) {
        return InputProcessors.printing(out, null, resetEnabled);
    }

    @NonNull
    public static InputProcessor printing(@NonNull OutputWriter out, @NullAllowed LineConvertor convertor, boolean resetEnabled) {
        return new PrintingInputProcessor(out, convertor, resetEnabled);
    }

    @NonNull
    public static InputProcessor ansiStripping(@NonNull InputProcessor delegate) {
        return new AnsiStrippingInputProcessor(delegate);
    }

    private static class AnsiStrippingInputProcessor
    implements InputProcessor {
        private final InputProcessor delegate;
        private boolean closed;

        public AnsiStrippingInputProcessor(InputProcessor delegate) {
            this.delegate = delegate;
        }

        @Override
        public void processInput(char[] chars) throws IOException {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            String sequence = new String(chars);
            if (AnsiStrippingInputProcessor.containsAnsiColors(sequence)) {
                sequence = AnsiStrippingInputProcessor.stripAnsiColors(sequence);
            }
            this.delegate.processInput(sequence.toCharArray());
        }

        @Override
        public void reset() throws IOException {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            this.delegate.reset();
        }

        @Override
        public void close() throws IOException {
            this.closed = true;
            this.delegate.close();
        }

        private static boolean containsAnsiColors(String sequence) {
            return sequence.indexOf("\u001b[") != -1;
        }

        private static String stripAnsiColors(String sequence) {
            StringBuilder sb = new StringBuilder(sequence.length());
            int index = 0;
            int max = sequence.length();
            block0 : while (index < max) {
                int n;
                int nextEscape = sequence.indexOf("\u001b[", index);
                if (nextEscape == -1) {
                    nextEscape = sequence.length();
                }
                int n2 = n = nextEscape == -1 ? max : nextEscape;
                while (index < n) {
                    sb.append(sequence.charAt(index));
                    ++index;
                }
                if (nextEscape == -1) continue;
                while (index < max) {
                    char c = sequence.charAt(index);
                    if (c == 'm') {
                        ++index;
                        continue block0;
                    }
                    ++index;
                }
            }
            return sb.toString();
        }
    }

    private static class CopyingInputProcessor
    implements InputProcessor {
        private final Writer writer;
        private boolean closed;

        public CopyingInputProcessor(Writer writer) {
            this.writer = writer;
        }

        @Override
        public void processInput(char[] chars) throws IOException {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            LOGGER.log(Level.FINEST, Arrays.toString(chars));
            this.writer.write(chars);
            this.writer.flush();
        }

        @Override
        public void reset() {
        }

        @Override
        public void close() throws IOException {
            this.closed = true;
            this.writer.close();
        }
    }

    private static class PrintingInputProcessor
    implements InputProcessor {
        private final OutputWriter out;
        private final LineConvertor convertor;
        private final boolean resetEnabled;
        private final LineParsingHelper helper = new LineParsingHelper();
        private boolean closed;

        public PrintingInputProcessor(OutputWriter out, LineConvertor convertor, boolean resetEnabled) {
            assert (out != null);
            this.out = out;
            this.convertor = convertor;
            this.resetEnabled = resetEnabled;
        }

        @Override
        public void processInput(char[] chars) {
            String[] lines;
            assert (chars != null);
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            for (String line : lines = this.helper.parse(chars)) {
                LOGGER.log(Level.FINEST, "{0}\\n", line);
                this.convert(line);
                this.out.flush();
            }
            String line2 = this.helper.getTrailingLine(true);
            if (line2 != null) {
                LOGGER.log(Level.FINEST, line2);
                this.out.print(line2);
                this.out.flush();
            }
        }

        @Override
        public void reset() throws IOException {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            if (!this.resetEnabled) {
                return;
            }
            this.out.reset();
        }

        @Override
        public void close() throws IOException {
            this.closed = true;
            this.out.close();
        }

        private void convert(String line) {
            if (this.convertor == null) {
                this.out.println(line);
                return;
            }
            List<ConvertedLine> convertedLines = this.convertor.convert(line);
            if (convertedLines == null) {
                this.out.println(line);
                return;
            }
            for (ConvertedLine converted : convertedLines) {
                if (converted.getListener() == null) {
                    this.out.println(converted.getText());
                    continue;
                }
                try {
                    this.out.println(converted.getText(), converted.getListener());
                }
                catch (IOException ex) {
                    LOGGER.log(Level.INFO, null, ex);
                    this.out.println(converted.getText());
                }
            }
        }
    }

    private static class ProxyInputProcessor
    implements InputProcessor {
        private final List<InputProcessor> processors = new ArrayList<InputProcessor>();
        private boolean closed;

        public /* varargs */ ProxyInputProcessor(InputProcessor ... processors) {
            for (InputProcessor processor : processors) {
                if (processor == null) continue;
                this.processors.add(processor);
            }
        }

        @Override
        public void processInput(char[] chars) throws IOException {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            for (InputProcessor processor : this.processors) {
                processor.processInput(chars);
            }
        }

        @Override
        public void reset() throws IOException {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            for (InputProcessor processor : this.processors) {
                processor.reset();
            }
        }

        @Override
        public void close() throws IOException {
            this.closed = true;
            for (InputProcessor processor : this.processors) {
                processor.close();
            }
        }
    }

    private static class Bridge
    implements InputProcessor {
        private final LineProcessor lineProcessor;
        private final LineParsingHelper helper = new LineParsingHelper();
        private boolean closed;

        public Bridge(LineProcessor lineProcessor) {
            Parameters.notNull((CharSequence)"lineProcessor", (Object)lineProcessor);
            this.lineProcessor = lineProcessor;
        }

        @Override
        public final void processInput(char[] chars) {
            String[] lines;
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            for (String line : lines = this.helper.parse(chars)) {
                this.lineProcessor.processLine(line);
            }
        }

        @Override
        public final void reset() {
            if (this.closed) {
                throw new IllegalStateException("Already closed processor");
            }
            this.flush();
            this.lineProcessor.reset();
        }

        @Override
        public final void close() {
            this.closed = true;
            this.flush();
            this.lineProcessor.close();
        }

        private void flush() {
            String line = this.helper.getTrailingLine(true);
            if (line != null) {
                this.lineProcessor.processLine(line);
            }
        }
    }

}

