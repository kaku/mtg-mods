/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.api.extexecution.input;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.extexecution.input.InputReader;
import org.netbeans.modules.extexecution.input.DefaultInputReader;
import org.netbeans.modules.extexecution.input.FileInputReader;
import org.openide.util.Parameters;

public final class InputReaders {
    private InputReaders() {
    }

    @NonNull
    public static InputReader forReader(@NonNull Reader reader) {
        return new DefaultInputReader(reader, true);
    }

    @NonNull
    public static InputReader forStream(@NonNull InputStream stream, @NonNull Charset charset) {
        Parameters.notNull((CharSequence)"stream", (Object)stream);
        return InputReaders.forReader(new InputStreamReader(stream, charset));
    }

    @NonNull
    public static InputReader forFile(@NonNull File file, @NonNull Charset charset) {
        Parameters.notNull((CharSequence)"file", (Object)file);
        Parameters.notNull((CharSequence)"charset", (Object)charset);
        final FileInput fileInput = new FileInput(file, charset);
        return InputReaders.forFileInputProvider(new FileInput.Provider(){

            @Override
            public FileInput getFileInput() {
                return fileInput;
            }
        });
    }

    @NonNull
    public static InputReader forFileInputProvider(@NonNull FileInput.Provider fileProvider) {
        Parameters.notNull((CharSequence)"fileProvider", (Object)fileProvider);
        return new FileInputReader(fileProvider);
    }

    public static final class FileInput {
        private final File file;
        private final Charset charset;

        public FileInput(@NonNull File file, @NonNull Charset charset) {
            Parameters.notNull((CharSequence)"file", (Object)file);
            Parameters.notNull((CharSequence)"charset", (Object)charset);
            this.file = file;
            this.charset = charset;
        }

        @NonNull
        public Charset getCharset() {
            return this.charset;
        }

        @NonNull
        public File getFile() {
            return this.file;
        }

        public static interface Provider {
            @CheckForNull
            public FileInput getFileInput();
        }

    }

}

