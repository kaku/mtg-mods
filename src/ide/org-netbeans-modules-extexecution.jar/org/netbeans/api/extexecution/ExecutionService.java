/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.util.Cancellable
 *  org.openide.util.Mutex
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 */
package org.netbeans.api.extexecution;

import java.awt.event.ActionEvent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.extexecution.ExecutionDescriptor;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.InputProcessors;
import org.netbeans.api.extexecution.input.InputReaderTask;
import org.netbeans.api.extexecution.input.InputReaders;
import org.netbeans.api.extexecution.input.LineProcessors;
import org.netbeans.api.extexecution.print.LineConvertor;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.extexecution.InputOutputManager;
import org.netbeans.modules.extexecution.OptionsAction;
import org.netbeans.modules.extexecution.ProcessInputStream;
import org.netbeans.modules.extexecution.RerunAction;
import org.netbeans.modules.extexecution.StopAction;
import org.openide.util.Cancellable;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

public final class ExecutionService {
    private static final Logger LOGGER = Logger.getLogger(ExecutionService.class.getName());
    private static final Set<Process> RUNNING_PROCESSES = new HashSet<Process>();
    private static final int EXECUTOR_SHUTDOWN_SLICE = 1000;
    private static final ExecutorService EXECUTOR_SERVICE = new RequestProcessor(ExecutionService.class.getName(), Integer.MAX_VALUE);
    private final Callable<Process> processCreator;
    private final ExecutionDescriptor descriptor;
    private final String originalDisplayName;

    private ExecutionService(Callable<Process> processCreator, String displayName, ExecutionDescriptor descriptor) {
        this.processCreator = processCreator;
        this.originalDisplayName = displayName;
        this.descriptor = descriptor;
    }

    @NonNull
    public static ExecutionService newService(@NonNull Callable<Process> processCreator, @NonNull ExecutionDescriptor descriptor, @NonNull String displayName) {
        return new ExecutionService(processCreator, displayName, descriptor);
    }

    @NonNull
    public Future<Integer> run() {
        return this.run(null);
    }

    private Future<Integer> run(InputOutput required) {
        class ExecutedHolder {
            private boolean executed;

            ExecutedHolder() {
                this.executed = false;
            }
        }
        final InputOutputManager.InputOutputData ioData = this.getInputOutput(required);
        String displayName = ioData.getDisplayName();
        ProgressCancellable cancellable = this.descriptor.isControllable() ? new ProgressCancellable() : null;
        final ProgressHandle handle = this.createProgressHandle(ioData.getInputOutput(), displayName, cancellable);
        InputOutput io = ioData.getInputOutput();
        final OutputWriter out = io.getOut();
        final OutputWriter err = io.getErr();
        final Reader in = io.getIn();
        final CountDownLatch finishedLatch = new CountDownLatch(1);
        final ExecutedHolder executed = new ExecutedHolder();
        Callable<Integer> callable = new Callable<Integer>(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             * Loose catch block
             * Enabled aggressive block sorting
             * Enabled unnecessary exception pruning
             * Enabled aggressive exception aggregation
             * Converted monitor instructions to comments
             * Lifted jumps to return sites
             */
            @Override
            public Integer call() throws Exception {
                ArrayList<InputReaderTask> tasks;
                Integer ret;
                ExecutorService executor;
                ProcessInputStream outStream;
                Process process;
                boolean interrupted;
                ProcessInputStream errStream;
                block113 : {
                    Object pre;
                    block111 : {
                        Set set;
                        block112 : {
                            block109 : {
                                Integer n;
                                block110 : {
                                    interrupted = false;
                                    process = null;
                                    ret = null;
                                    executor = null;
                                    outStream = null;
                                    errStream = null;
                                    tasks = new ArrayList<InputReaderTask>();
                                    executed.executed = true;
                                    pre = ExecutionService.this.descriptor.getPreExecution();
                                    if (pre != null) {
                                        pre.run();
                                    }
                                    if (!Thread.currentThread().isInterrupted()) break block109;
                                    n = null;
                                    if (!(interrupted |= Thread.interrupted())) {
                                        if (outStream != null) {
                                            outStream.close(true);
                                        }
                                        if (errStream != null) {
                                            errStream.close(true);
                                        }
                                    }
                                    if (process == null) break block110;
                                    process.destroy();
                                    Set set2 = RUNNING_PROCESSES;
                                    // MONITORENTER : set2
                                    RUNNING_PROCESSES.remove(process);
                                    // MONITOREXIT : set2
                                    try {
                                        ret = process.exitValue();
                                    }
                                    catch (IllegalThreadStateException ex) {
                                        LOGGER.log(Level.FINE, "Process not yet exited", ex);
                                    }
                                }
                                try {
                                    ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                                    Runnable post = ExecutionService.this.descriptor.getPostExecution();
                                    if (post == null) return n;
                                    post.run();
                                    return n;
                                }
                                finally {
                                    finishedLatch.countDown();
                                    if (interrupted) {
                                        Thread.currentThread().interrupt();
                                    }
                                }
                                catch (Throwable t) {
                                    try {
                                        LOGGER.log(Level.INFO, null, t);
                                        throw new WrappedException(t);
                                    }
                                    catch (Throwable var13_26) {
                                        try {
                                            ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                                            Runnable post = ExecutionService.this.descriptor.getPostExecution();
                                            if (post == null) throw var13_26;
                                            post.run();
                                            throw var13_26;
                                        }
                                        finally {
                                            finishedLatch.countDown();
                                            if (interrupted) {
                                                Thread.currentThread().interrupt();
                                            }
                                        }
                                    }
                                }
                            }
                            process = (Process)ExecutionService.this.processCreator.call();
                            set = RUNNING_PROCESSES;
                            // MONITORENTER : set
                            RUNNING_PROCESSES.add(process);
                            // MONITOREXIT : set
                            if (!Thread.currentThread().isInterrupted()) break block111;
                            set = null;
                            if (!(interrupted |= Thread.interrupted())) {
                                if (outStream != null) {
                                    outStream.close(true);
                                }
                                if (errStream != null) {
                                    errStream.close(true);
                                }
                            }
                            if (process == null) break block112;
                            process.destroy();
                            Set t = RUNNING_PROCESSES;
                            // MONITORENTER : t
                            RUNNING_PROCESSES.remove(process);
                            // MONITOREXIT : t
                            try {
                                ret = process.exitValue();
                            }
                            catch (IllegalThreadStateException ex) {
                                LOGGER.log(Level.FINE, "Process not yet exited", ex);
                            }
                        }
                        try {
                            ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                            Runnable post = ExecutionService.this.descriptor.getPostExecution();
                            if (post == null) return set;
                            post.run();
                            return set;
                        }
                        finally {
                            finishedLatch.countDown();
                            if (interrupted) {
                                Thread.currentThread().interrupt();
                            }
                        }
                        catch (Throwable t) {
                            try {
                                LOGGER.log(Level.INFO, null, t);
                                throw new WrappedException(t);
                            }
                            catch (Throwable var19_30) {
                                try {
                                    ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                                    Runnable post = ExecutionService.this.descriptor.getPostExecution();
                                    if (post == null) throw var19_30;
                                    post.run();
                                    throw var19_30;
                                }
                                finally {
                                    finishedLatch.countDown();
                                    if (interrupted) {
                                        Thread.currentThread().interrupt();
                                    }
                                }
                            }
                        }
                    }
                    outStream = new ProcessInputStream(process, process.getInputStream());
                    errStream = new ProcessInputStream(process, process.getErrorStream());
                    executor = Executors.newFixedThreadPool(ExecutionService.this.descriptor.isInputVisible() ? 3 : 2);
                    Charset charset = ExecutionService.this.descriptor.getCharset();
                    if (charset == null) {
                        charset = Charset.defaultCharset();
                    }
                    tasks.add(InputReaderTask.newDrainingTask(InputReaders.forStream(new BufferedInputStream(outStream), charset), ExecutionService.this.createOutProcessor(out)));
                    tasks.add(InputReaderTask.newDrainingTask(InputReaders.forStream(new BufferedInputStream(errStream), charset), ExecutionService.this.createErrProcessor(err)));
                    if (ExecutionService.this.descriptor.isInputVisible()) {
                        tasks.add(InputReaderTask.newTask(InputReaders.forReader(in), ExecutionService.this.createInProcessor(process.getOutputStream())));
                    }
                    for (InputReaderTask task : tasks) {
                        executor.submit(task);
                    }
                    process.waitFor();
                    if (!(interrupted |= Thread.interrupted())) {
                        if (outStream != null) {
                            outStream.close(true);
                        }
                        if (errStream != null) {
                            errStream.close(true);
                        }
                    }
                    if (process == null) break block113;
                    process.destroy();
                    pre = RUNNING_PROCESSES;
                    // MONITORENTER : pre
                    RUNNING_PROCESSES.remove(process);
                    // MONITOREXIT : pre
                    try {
                        ret = process.exitValue();
                    }
                    catch (IllegalThreadStateException ex) {
                        LOGGER.log(Level.FINE, "Process not yet exited", ex);
                    }
                }
                try {
                    ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                    Runnable post = ExecutionService.this.descriptor.getPostExecution();
                    if (post == null) return ret;
                    post.run();
                    return ret;
                }
                finally {
                    finishedLatch.countDown();
                    if (interrupted) {
                        Thread.currentThread().interrupt();
                    }
                }
                catch (Throwable t) {
                    try {
                        LOGGER.log(Level.INFO, null, t);
                        throw new WrappedException(t);
                    }
                    catch (Throwable var24_35) {
                        try {
                            ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                            Runnable post = ExecutionService.this.descriptor.getPostExecution();
                            if (post == null) throw var24_35;
                            post.run();
                            throw var24_35;
                        }
                        finally {
                            finishedLatch.countDown();
                            if (interrupted) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    }
                }
                catch (InterruptedException ex2222222) {
                    block114 : {
                        LOGGER.log(Level.FINE, null, ex2222222);
                        interrupted = true;
                        {
                            catch (Throwable throwable) {
                                block115 : {
                                    if (!(interrupted |= Thread.interrupted())) {
                                        if (outStream != null) {
                                            outStream.close(true);
                                        }
                                        if (errStream != null) {
                                            errStream.close(true);
                                        }
                                    }
                                    if (process == null) break block115;
                                    process.destroy();
                                    Set set = RUNNING_PROCESSES;
                                    // MONITORENTER : set
                                    RUNNING_PROCESSES.remove(process);
                                    // MONITOREXIT : set
                                    try {
                                        ret = process.exitValue();
                                    }
                                    catch (IllegalThreadStateException ex) {
                                        LOGGER.log(Level.FINE, "Process not yet exited", ex);
                                    }
                                }
                                try {
                                    ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                                    Runnable post = ExecutionService.this.descriptor.getPostExecution();
                                    if (post == null) throw throwable;
                                    post.run();
                                    throw throwable;
                                }
                                finally {
                                    finishedLatch.countDown();
                                    if (interrupted) {
                                        Thread.currentThread().interrupt();
                                    }
                                }
                                catch (Throwable t4) {
                                    try {
                                        LOGGER.log(Level.INFO, null, t4);
                                        throw new WrappedException(t4);
                                    }
                                    catch (Throwable var36_47) {
                                        try {
                                            ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                                            Runnable post = ExecutionService.this.descriptor.getPostExecution();
                                            if (post == null) throw var36_47;
                                            post.run();
                                            throw var36_47;
                                        }
                                        finally {
                                            finishedLatch.countDown();
                                            if (interrupted) {
                                                Thread.currentThread().interrupt();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!(interrupted |= Thread.interrupted())) {
                            if (outStream != null) {
                                outStream.close(true);
                            }
                            if (errStream != null) {
                                errStream.close(true);
                            }
                        }
                        if (process == null) break block114;
                        process.destroy();
                        Set ex2222222 = RUNNING_PROCESSES;
                        // MONITORENTER : ex2222222
                        RUNNING_PROCESSES.remove(process);
                        // MONITOREXIT : ex2222222
                        try {
                            ret = process.exitValue();
                        }
                        catch (IllegalThreadStateException ex) {
                            LOGGER.log(Level.FINE, "Process not yet exited", ex);
                        }
                    }
                    try {
                        ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                        Runnable post = ExecutionService.this.descriptor.getPostExecution();
                        if (post == null) return ret;
                        post.run();
                        return ret;
                    }
                    finally {
                        finishedLatch.countDown();
                        if (interrupted) {
                            Thread.currentThread().interrupt();
                        }
                    }
                    catch (Throwable t2) {
                        try {
                            LOGGER.log(Level.INFO, null, t2);
                            throw new WrappedException(t2);
                        }
                        catch (Throwable var29_39) {
                            try {
                                ExecutionService.this.cleanup(tasks, executor, handle, ioData, ioData.getInputOutput() != ExecutionService.this.descriptor.getInputOutput(), ExecutionService.this.descriptor.isFrontWindowOnError() && ret != null && ret != 0);
                                Runnable post = ExecutionService.this.descriptor.getPostExecution();
                                if (post == null) throw var29_39;
                                post.run();
                                throw var29_39;
                            }
                            finally {
                                finishedLatch.countDown();
                                if (interrupted) {
                                    Thread.currentThread().interrupt();
                                }
                            }
                        }
                    }
                    catch (Throwable t3) {
                        LOGGER.log(Level.INFO, null, t3);
                        throw new WrappedException(t3);
                    }
                }
            }
        };
        final FutureTask<Integer> current = new FutureTask<Integer>((Callable)callable){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             * Enabled force condition propagation
             * Lifted jumps to return sites
             */
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                boolean ret = FutureTask.super.cancel(mayInterruptIfRunning);
                if (executed.executed) return ret;
                ExecutionService.this.cleanup(handle, ioData, false);
                Class<InputOutputManager> class_ = InputOutputManager.class;
                synchronized (InputOutputManager.class) {
                    if (ioData.getInputOutput() == ExecutionService.this.descriptor.getInputOutput()) return ret;
                    {
                        InputOutputManager.addInputOutput(ioData);
                    }
                    // ** MonitorExit[var3_3] (shouldn't be in output)
                    return ret;
                }
            }

            @Override
            protected void setException(Throwable t) {
                if (t instanceof WrappedException) {
                    FutureTask.super.setException(((WrappedException)t).getCause());
                } else {
                    FutureTask.super.setException(t);
                }
            }
        };
        final StopAction workingStopAction = ioData.getStopAction();
        final RerunAction workingRerunAction = ioData.getRerunAction();
        Mutex.EVENT.readAccess(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                if (workingStopAction != null) {
                    StopAction stopAction = workingStopAction;
                    synchronized (stopAction) {
                        workingStopAction.setTask(current);
                        workingStopAction.setEnabled(true);
                    }
                }
                if (workingRerunAction != null) {
                    RerunAction rerunAction = workingRerunAction;
                    synchronized (rerunAction) {
                        workingRerunAction.setExecutionService(ExecutionService.this);
                        workingRerunAction.setRerunCondition(ExecutionService.this.descriptor.getRerunCondition());
                        workingRerunAction.setEnabled(false);
                    }
                }
            }
        });
        if (cancellable != null) {
            cancellable.setTask(current);
        }
        EXECUTOR_SERVICE.execute(current);
        return current;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private InputOutputManager.InputOutputData getInputOutput(InputOutput required) {
        InputOutputManager.InputOutputData result = null;
        Class<InputOutputManager> class_ = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            InputOutput io = this.descriptor.getInputOutput();
            if (io != null) {
                result = new InputOutputManager.InputOutputData(io, this.originalDisplayName, null, null, null);
            }
            if (result == null) {
                result = InputOutputManager.getInputOutput(required);
            }
            if (result == null) {
                result = InputOutputManager.getInputOutput(this.originalDisplayName, this.descriptor.isControllable(), this.descriptor.getOptionsPath());
            }
            if (result == null) {
                result = InputOutputManager.createInputOutput(this.originalDisplayName, this.descriptor.isControllable(), this.descriptor.getOptionsPath());
            }
            this.configureInputOutput(result.getInputOutput());
            // ** MonitorExit[var3_3] (shouldn't be in output)
            return result;
        }
    }

    private void configureInputOutput(InputOutput inputOutput) {
        if (inputOutput == InputOutput.NULL) {
            return;
        }
        if (this.descriptor.getInputOutput() == null || !this.descriptor.noReset()) {
            try {
                inputOutput.getOut().reset();
            }
            catch (IOException exc) {
                LOGGER.log(Level.INFO, null, exc);
            }
            inputOutput.setErrSeparated(false);
        }
        if (this.descriptor.isFrontWindow()) {
            inputOutput.select();
        }
        inputOutput.setInputVisible(this.descriptor.isInputVisible());
    }

    private ProgressHandle createProgressHandle(InputOutput inputOutput, String displayName, Cancellable cancellable) {
        if (!this.descriptor.showProgress() && !this.descriptor.showSuspended()) {
            return null;
        }
        ProgressHandle handle = ProgressHandleFactory.createHandle((String)displayName, (Cancellable)cancellable, (Action)new ProgressAction(inputOutput));
        handle.setInitialDelay(0);
        handle.start();
        handle.switchToIndeterminate();
        if (this.descriptor.showSuspended()) {
            handle.suspend(NbBundle.getMessage(ExecutionService.class, (String)"Running"));
        }
        return handle;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void cleanup(List<InputReaderTask> tasks, final ExecutorService processingExecutor, ProgressHandle progressHandle, InputOutputManager.InputOutputData inputOutputData, boolean managed, boolean show) {
        boolean interrupted = false;
        if (processingExecutor != null) {
            try {
                AccessController.doPrivileged(new PrivilegedAction<Void>(){

                    @Override
                    public Void run() {
                        processingExecutor.shutdown();
                        return null;
                    }
                });
                for (Cancellable cancellable : tasks) {
                    cancellable.cancel();
                }
                while (!processingExecutor.awaitTermination(1000, TimeUnit.MILLISECONDS)) {
                    LOGGER.log(Level.INFO, "Awaiting processing finish");
                }
            }
            catch (InterruptedException ex) {
                interrupted = true;
            }
        }
        this.cleanup(progressHandle, inputOutputData, show);
        Class<InputOutputManager> ex = InputOutputManager.class;
        synchronized (InputOutputManager.class) {
            if (managed) {
                InputOutputManager.addInputOutput(inputOutputData);
            }
            // ** MonitorExit[ex] (shouldn't be in output)
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
            return;
        }
    }

    private void cleanup(final ProgressHandle progressHandle, final InputOutputManager.InputOutputData inputOutputData, final boolean show) {
        Runnable ui = new Runnable(){

            @Override
            public void run() {
                if (show) {
                    inputOutputData.getInputOutput().select();
                }
                if (inputOutputData.getStopAction() != null) {
                    inputOutputData.getStopAction().setEnabled(false);
                }
                if (inputOutputData.getRerunAction() != null) {
                    inputOutputData.getRerunAction().setEnabled(true);
                }
                if (progressHandle != null) {
                    progressHandle.finish();
                }
            }
        };
        Mutex.EVENT.readAccess(ui);
    }

    private InputProcessor createOutProcessor(OutputWriter writer) {
        ExecutionDescriptor.LineConvertorFactory convertorFactory = this.descriptor.getOutConvertorFactory();
        InputProcessor outProcessor = null;
        outProcessor = this.descriptor.isOutLineBased() ? InputProcessors.bridge(LineProcessors.printing(writer, convertorFactory != null ? convertorFactory.newLineConvertor() : null, true)) : InputProcessors.printing(writer, convertorFactory != null ? convertorFactory.newLineConvertor() : null, true);
        ExecutionDescriptor.InputProcessorFactory descriptorOutFactory = this.descriptor.getOutProcessorFactory();
        if (descriptorOutFactory != null) {
            outProcessor = descriptorOutFactory.newInputProcessor(outProcessor);
        }
        return outProcessor;
    }

    private InputProcessor createErrProcessor(OutputWriter writer) {
        ExecutionDescriptor.LineConvertorFactory convertorFactory = this.descriptor.getErrConvertorFactory();
        InputProcessor errProcessor = null;
        errProcessor = this.descriptor.isErrLineBased() ? InputProcessors.bridge(LineProcessors.printing(writer, convertorFactory != null ? convertorFactory.newLineConvertor() : null, false)) : InputProcessors.printing(writer, convertorFactory != null ? convertorFactory.newLineConvertor() : null, false);
        ExecutionDescriptor.InputProcessorFactory descriptorErrFactory = this.descriptor.getErrProcessorFactory();
        if (descriptorErrFactory != null) {
            errProcessor = descriptorErrFactory.newInputProcessor(errProcessor);
        }
        return errProcessor;
    }

    private InputProcessor createInProcessor(OutputStream os) {
        return InputProcessors.copying(new OutputStreamWriter(os));
    }

    static {
        RerunAction.Accessor.setDefault(new RerunAction.Accessor(){

            @Override
            public Future<Integer> run(ExecutionService service, InputOutput required) {
                return service.run(required);
            }
        });
        Runtime.getRuntime().addShutdownHook(new Thread(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                EXECUTOR_SERVICE.shutdown();
                Set set = RUNNING_PROCESSES;
                synchronized (set) {
                    for (Process process : RUNNING_PROCESSES) {
                        process.destroy();
                    }
                }
            }
        });
    }

    private static class WrappedException
    extends Exception {
        public WrappedException(Throwable cause) {
            super(cause);
        }
    }

    private static class ProgressAction
    extends AbstractAction {
        private final InputOutput io;

        public ProgressAction(InputOutput io) {
            this.io = io;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.io.select();
        }
    }

    private static class ProgressCancellable
    implements Cancellable {
        private Future<Integer> task;

        public synchronized void setTask(Future<Integer> task) {
            this.task = task;
        }

        public synchronized boolean cancel() {
            if (this.task != null) {
                this.task.cancel(true);
            }
            return true;
        }
    }

}

