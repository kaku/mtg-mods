/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.extexecution.startup;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String StartMode_Debug() {
        return NbBundle.getMessage(Bundle.class, (String)"StartMode_Debug");
    }

    static String StartMode_Normal() {
        return NbBundle.getMessage(Bundle.class, (String)"StartMode_Normal");
    }

    static String StartMode_Profile() {
        return NbBundle.getMessage(Bundle.class, (String)"StartMode_Profile");
    }

    static String StartMode_Test_Debug() {
        return NbBundle.getMessage(Bundle.class, (String)"StartMode_Test_Debug");
    }

    static String StartMode_Test_Normal() {
        return NbBundle.getMessage(Bundle.class, (String)"StartMode_Test_Normal");
    }

    static String StartMode_Test_Profile() {
        return NbBundle.getMessage(Bundle.class, (String)"StartMode_Test_Profile");
    }

    private void Bundle() {
    }
}

