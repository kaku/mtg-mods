/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Parameters
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.api.extexecution.startup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.extexecution.startup.Bundle;
import org.netbeans.spi.extexecution.startup.StartupExtenderImplementation;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.lookup.Lookups;

public final class StartupExtender {
    private static final Logger LOG = Logger.getLogger(StartupExtender.class.getName());
    private final String description;
    private final List<String> arguments;

    private StartupExtender(String description, List<String> arguments) {
        this.description = description;
        this.arguments = arguments;
    }

    @NonNull
    public static List<StartupExtender> getExtenders(@NonNull Lookup context, @NonNull StartMode mode) {
        Parameters.notNull((CharSequence)"context", (Object)context);
        Parameters.notNull((CharSequence)"mode", (Object)((Object)mode));
        LOG.log(Level.FINE, "getExtenders: context={0} mode={1}", new Object[]{context, mode});
        Lookup lkp = Lookups.forPath((String)"StartupExtender");
        ArrayList<StartupExtender> res = new ArrayList<StartupExtender>();
        for (Lookup.Item item : lkp.lookupResult(StartupExtenderImplementation.class).allItems()) {
            StartupExtender extender = new StartupExtender(item.getDisplayName(), ((StartupExtenderImplementation)item.getInstance()).getArguments(context, mode));
            LOG.log(Level.FINE, " {0} => {1}", new Object[]{extender.description, extender.getArguments()});
            res.add(extender);
        }
        return res;
    }

    @NonNull
    public String getDescription() {
        return this.description;
    }

    @NonNull
    public List<String> getArguments() {
        return this.arguments;
    }

    public static enum StartMode {
        NORMAL(Bundle.StartMode_Normal()),
        DEBUG(Bundle.StartMode_Debug()),
        PROFILE(Bundle.StartMode_Profile()),
        TEST_NORMAL(Bundle.StartMode_Test_Normal()),
        TEST_DEBUG(Bundle.StartMode_Test_Debug()),
        TEST_PROFILE(Bundle.StartMode_Test_Profile());
        
        private final String mode;

        private StartMode(String mode) {
            this.mode = mode;
        }

        public String toString() {
            return this.mode;
        }
    }

}

