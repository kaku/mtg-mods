/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckReturnValue
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.NbPreferences
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.api.extexecution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.CheckReturnValue;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.extexecution.WrapperProcess;
import org.openide.util.NbPreferences;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

public final class ExternalProcessBuilder
implements Callable<Process> {
    private static final Logger LOGGER = Logger.getLogger(ExternalProcessBuilder.class.getName());
    private static final Pattern ESCAPED_PATTERN = Pattern.compile("\".*\"");
    private static final String USE_PROXY_AUTHENTICATION = "useProxyAuthentication";
    private static final String PROXY_AUTHENTICATION_USERNAME = "proxyAuthenticationUsername";
    private static final String PROXY_AUTHENTICATION_PASSWORD = "proxyAuthenticationPassword";
    private final String executable;
    private final File workingDirectory;
    private final boolean redirectErrorStream;
    private final List<String> arguments = new ArrayList<String>();
    private final List<File> paths = new ArrayList<File>();
    private final Map<String, String> envVariables = new HashMap<String, String>();

    public ExternalProcessBuilder(@NonNull String executable) {
        this(new BuilderData(executable));
    }

    private ExternalProcessBuilder(BuilderData builder) {
        this.executable = builder.executable;
        this.workingDirectory = builder.workingDirectory;
        this.redirectErrorStream = builder.redirectErrorStream;
        this.arguments.addAll(builder.arguments);
        this.paths.addAll(builder.paths);
        this.envVariables.putAll(builder.envVariables);
    }

    @NonNull
    @CheckReturnValue
    public ExternalProcessBuilder workingDirectory(@NonNull File workingDirectory) {
        Parameters.notNull((CharSequence)"workingDirectory", (Object)workingDirectory);
        BuilderData builder = new BuilderData(this);
        return new ExternalProcessBuilder(builder.workingDirectory(workingDirectory));
    }

    @NonNull
    @CheckReturnValue
    public ExternalProcessBuilder redirectErrorStream(boolean redirectErrorStream) {
        BuilderData builder = new BuilderData(this);
        return new ExternalProcessBuilder(builder.redirectErrorStream(redirectErrorStream));
    }

    @NonNull
    @CheckReturnValue
    public ExternalProcessBuilder prependPath(@NonNull File path) {
        Parameters.notNull((CharSequence)"path", (Object)path);
        BuilderData builder = new BuilderData(this);
        return new ExternalProcessBuilder(builder.prependPath(path));
    }

    @NonNull
    @CheckReturnValue
    public ExternalProcessBuilder addArgument(@NonNull String argument) {
        Parameters.notNull((CharSequence)"argument", (Object)argument);
        BuilderData builder = new BuilderData(this);
        return new ExternalProcessBuilder(builder.addArgument(argument));
    }

    @NonNull
    @CheckReturnValue
    public ExternalProcessBuilder addEnvironmentVariable(@NonNull String name, @NonNull String value) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        Parameters.notNull((CharSequence)"value", (Object)value);
        BuilderData builder = new BuilderData(this);
        return new ExternalProcessBuilder(builder.addEnvironmentVariable(name, value));
    }

    @NonNull
    @Override
    public Process call() throws IOException {
        ArrayList<String> commandList = new ArrayList<String>();
        if (Utilities.isWindows() && !ESCAPED_PATTERN.matcher(this.executable).matches()) {
            commandList.add(ExternalProcessBuilder.escapeString(this.executable));
        } else {
            commandList.add(this.executable);
        }
        List<String> args = this.buildArguments();
        commandList.addAll(args);
        ProcessBuilder pb = new ProcessBuilder(commandList.toArray(new String[commandList.size()]));
        if (this.workingDirectory != null) {
            pb.directory(this.workingDirectory);
        }
        Map<String, String> pbEnv = pb.environment();
        Map<String, String> env = this.buildEnvironment(pbEnv);
        pbEnv.putAll(env);
        String uuid = UUID.randomUUID().toString();
        pbEnv.put("NB_EXEC_EXTEXECUTION_PROCESS_UUID", uuid);
        this.adjustProxy(pb);
        pb.redirectErrorStream(this.redirectErrorStream);
        this.logProcess(Level.FINE, pb);
        WrapperProcess wp = new WrapperProcess(pb.start(), uuid);
        return wp;
    }

    private void logProcess(Level level, ProcessBuilder pb) {
        if (!LOGGER.isLoggable(level)) {
            return;
        }
        File dir = pb.directory();
        String basedir = dir == null ? "" : "(basedir: " + dir.getAbsolutePath() + ") ";
        StringBuilder command = new StringBuilder();
        Iterator<String> it = pb.command().iterator();
        while (it.hasNext()) {
            command.append(it.next());
            if (!it.hasNext()) continue;
            command.append(' ');
        }
        LOGGER.log(level, "Running: " + basedir + '\"' + command.toString() + '\"');
        LOGGER.log(level, "Environment: " + pb.environment());
    }

    Map<String, String> buildEnvironment(Map<String, String> original) {
        String currentPath;
        HashMap<String, String> ret = new HashMap<String, String>(original);
        ret.putAll(this.envVariables);
        String pathName = "PATH";
        if (Utilities.isWindows()) {
            pathName = "Path";
            for (String key : ret.keySet()) {
                if (!"PATH".equals(key.toUpperCase(Locale.ENGLISH))) continue;
                pathName = key;
                break;
            }
        }
        if ((currentPath = ret.get(pathName)) == null) {
            currentPath = "";
        }
        for (File path : this.paths) {
            currentPath = path.getAbsolutePath().replace(" ", "\\ ") + File.pathSeparator + currentPath;
        }
        if (!"".equals(currentPath.trim())) {
            ret.put(pathName, currentPath);
        }
        return ret;
    }

    List<String> buildArguments() {
        if (!Utilities.isWindows()) {
            return new ArrayList<String>(this.arguments);
        }
        ArrayList<String> result = new ArrayList<String>(this.arguments.size());
        for (String arg : this.arguments) {
            if (arg != null && !ESCAPED_PATTERN.matcher(arg).matches()) {
                result.add(ExternalProcessBuilder.escapeString(arg));
                continue;
            }
            result.add(arg);
        }
        return result;
    }

    private static String escapeString(String s) {
        if (s.length() == 0) {
            return "\"\"";
        }
        StringBuilder sb = new StringBuilder();
        boolean hasSpace = false;
        int slen = s.length();
        for (int i = 0; i < slen; ++i) {
            char c = s.charAt(i);
            if (Character.isWhitespace(c)) {
                hasSpace = true;
                sb.append(c);
                continue;
            }
            sb.append(c);
        }
        if (hasSpace) {
            sb.insert(0, '\"');
            sb.append('\"');
        }
        return sb.toString();
    }

    private void adjustProxy(ProcessBuilder pb) {
        Map<String, String> env;
        String proxy = ExternalProcessBuilder.getNetBeansHttpProxy();
        if (proxy != null && (env = pb.environment()).get("HTTP_PROXY") == null && env.get("http_proxy") == null) {
            env.put("HTTP_PROXY", proxy);
            env.put("http_proxy", proxy);
        }
    }

    private static String getNetBeansHttpProxy() {
        int port;
        String host = System.getProperty("http.proxyHost");
        if (host == null) {
            return null;
        }
        String portHttp = System.getProperty("http.proxyPort");
        try {
            port = Integer.parseInt(portHttp);
        }
        catch (NumberFormatException e) {
            port = 8080;
        }
        Preferences prefs = NbPreferences.root().node("org/netbeans/core");
        boolean useAuth = prefs.getBoolean("useProxyAuthentication", false);
        String auth = "";
        if (useAuth) {
            auth = prefs.get("proxyAuthenticationUsername", "") + ":" + prefs.get("proxyAuthenticationPassword", "") + '@';
        }
        if (host.indexOf(58) == -1) {
            host = "http://" + auth + host;
        }
        return host + ":" + port;
    }

    private static class BuilderData {
        private final String executable;
        private File workingDirectory;
        private boolean redirectErrorStream;
        private List<String> arguments = new ArrayList<String>();
        private List<File> paths = new ArrayList<File>();
        private Map<String, String> envVariables = new HashMap<String, String>();

        public BuilderData(String executable) {
            this.executable = executable;
        }

        public BuilderData(ExternalProcessBuilder builder) {
            this.executable = builder.executable;
            this.workingDirectory = builder.workingDirectory;
            this.redirectErrorStream = builder.redirectErrorStream;
            this.arguments.addAll(builder.arguments);
            this.paths.addAll(builder.paths);
            this.envVariables.putAll(builder.envVariables);
        }

        public BuilderData workingDirectory(File workingDirectory) {
            assert (workingDirectory != null);
            this.workingDirectory = workingDirectory;
            return this;
        }

        public BuilderData redirectErrorStream(boolean redirectErrorStream) {
            this.redirectErrorStream = redirectErrorStream;
            return this;
        }

        public BuilderData prependPath(File path) {
            assert (path != null);
            this.paths.add(path);
            return this;
        }

        public BuilderData addArgument(String argument) {
            assert (argument != null);
            this.arguments.add(argument);
            return this;
        }

        public BuilderData addEnvironmentVariable(String name, String value) {
            assert (name != null);
            assert (value != null);
            this.envVariables.put(name, value);
            return this;
        }
    }

}

