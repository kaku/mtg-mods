/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 */
package org.netbeans.api.extexecution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.ExternalProcessBuilder;
import org.netbeans.modules.extexecution.ProcessBuilderAccessor;
import org.netbeans.spi.extexecution.ProcessBuilderImplementation;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;

public final class ProcessBuilder
implements Callable<Process> {
    private final ProcessBuilderImplementation implementation;
    private final String description;
    private String executable;
    private String workingDirectory;
    private List<String> arguments = new ArrayList<String>();
    private List<String> paths = new ArrayList<String>();
    private Map<String, String> envVariables = new HashMap<String, String>();
    private boolean redirectErrorStream;

    private ProcessBuilder(ProcessBuilderImplementation implementation, String description) {
        this.implementation = implementation;
        this.description = description;
    }

    public static ProcessBuilder getLocal() {
        return new ProcessBuilder(new LocalProcessFactory(), NbBundle.getMessage(ProcessBuilder.class, (String)"LocalProcessBuilder"));
    }

    @NonNull
    public String getDescription() {
        return this.description;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setExecutable(@NonNull String executable) {
        Parameters.notNull((CharSequence)"executable", (Object)executable);
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            this.executable = executable;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setWorkingDirectory(@NullAllowed String workingDirectory) {
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            this.workingDirectory = workingDirectory;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setArguments(@NonNull List<String> arguments) {
        Parameters.notNull((CharSequence)"arguments", arguments);
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            this.arguments.clear();
            this.arguments.addAll(arguments);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setEnvironmentVariables(@NonNull Map<String, String> envVariables) {
        Parameters.notNull((CharSequence)"envVariables", envVariables);
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            this.envVariables.clear();
            this.envVariables.putAll(envVariables);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setPaths(@NonNull List<String> paths) {
        Parameters.notNull((CharSequence)"paths", paths);
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            this.paths.clear();
            this.paths.addAll(paths);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setRedirectErrorStream(boolean redirectErrorStream) {
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            this.redirectErrorStream = redirectErrorStream;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    @Override
    public Process call() throws IOException {
        String currentExecutable = null;
        String currentWorkingDirectory = null;
        ArrayList<String> currentArguments = new ArrayList<String>();
        ArrayList<String> currentPaths = new ArrayList<String>();
        HashMap<String, String> currentEnvVariables = new HashMap<String, String>();
        boolean currentRedirectErrorStream = false;
        ProcessBuilder processBuilder = this;
        synchronized (processBuilder) {
            currentExecutable = this.executable;
            currentWorkingDirectory = this.workingDirectory;
            currentArguments.addAll(this.arguments);
            currentPaths.addAll(this.paths);
            currentEnvVariables.putAll(this.envVariables);
            currentRedirectErrorStream = this.redirectErrorStream;
        }
        if (currentExecutable == null) {
            throw new IllegalStateException("The executable has not been configured");
        }
        return this.implementation.createProcess(currentExecutable, currentWorkingDirectory, currentArguments, currentPaths, currentEnvVariables, currentRedirectErrorStream);
    }

    static {
        ProcessBuilderAccessor.setDefault(new ProcessBuilderAccessor(){

            @Override
            public ProcessBuilder createProcessBuilder(ProcessBuilderImplementation impl, String description) {
                return new ProcessBuilder(impl, description);
            }
        });
    }

    private static class LocalProcessFactory
    implements ProcessBuilderImplementation {
        private LocalProcessFactory() {
        }

        @Override
        public Process createProcess(String executable, String workingDirectory, List<String> arguments, List<String> paths, Map<String, String> environment, boolean redirectErrorStream) throws IOException {
            ExternalProcessBuilder builder = new ExternalProcessBuilder(executable);
            if (workingDirectory != null) {
                builder = builder.workingDirectory(new File(workingDirectory));
            }
            for (String argument : arguments) {
                builder = builder.addArgument(argument);
            }
            for (String path : paths) {
                builder = builder.prependPath(new File(path));
            }
            for (Map.Entry entry : environment.entrySet()) {
                builder = builder.addEnvironmentVariable((String)entry.getKey(), (String)entry.getValue());
            }
            builder = builder.redirectErrorStream(redirectErrorStream);
            return builder.call();
        }
    }

}

