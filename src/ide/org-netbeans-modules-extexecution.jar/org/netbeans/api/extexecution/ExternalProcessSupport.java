/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.extexecution;

import java.util.Map;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.extexecution.WrapperProcess;
import org.netbeans.spi.extexecution.destroy.ProcessDestroyPerformer;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class ExternalProcessSupport {
    private ExternalProcessSupport() {
    }

    public static void destroy(@NonNull Process process, @NonNull Map<String, String> env) {
        Parameters.notNull((CharSequence)"process", (Object)process);
        Parameters.notNull((CharSequence)"env", env);
        if (process instanceof WrapperProcess) {
            process.destroy();
            return;
        }
        ProcessDestroyPerformer pdp = (ProcessDestroyPerformer)Lookup.getDefault().lookup(ProcessDestroyPerformer.class);
        if (pdp != null) {
            pdp.destroy(process, env);
        } else {
            process.destroy();
        }
    }
}

