/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckReturnValue
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.windows.InputOutput
 */
package org.netbeans.api.extexecution;

import java.nio.charset.Charset;
import javax.swing.event.ChangeListener;
import org.netbeans.api.annotations.common.CheckReturnValue;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.print.LineConvertor;
import org.openide.windows.InputOutput;

public final class ExecutionDescriptor {
    private final Runnable preExecution;
    private final Runnable postExecution;
    private final boolean suspend;
    private final boolean progress;
    private final boolean front;
    private final boolean input;
    private final boolean controllable;
    private final boolean noReset;
    private final boolean outLineBased;
    private final boolean errLineBased;
    private final boolean frontWindowOnError;
    private final LineConvertorFactory outConvertorFactory;
    private final LineConvertorFactory errConvertorFactory;
    private final InputProcessorFactory outProcessorFactory;
    private final InputProcessorFactory errProcessorFactory;
    private final InputOutput inputOutput;
    private final RerunCondition rerunCondition;
    private final String optionsPath;
    private final Charset charset;

    public ExecutionDescriptor() {
        this(new DescriptorData());
    }

    private ExecutionDescriptor(DescriptorData data) {
        this.preExecution = data.preExecution;
        this.postExecution = data.postExecution;
        this.suspend = data.suspend;
        this.progress = data.progress;
        this.front = data.front;
        this.input = data.input;
        this.controllable = data.controllable;
        this.outLineBased = data.outLineBased;
        this.errLineBased = data.errLineBased;
        this.frontWindowOnError = data.frontWindowOnError;
        this.outConvertorFactory = data.outConvertorFactory;
        this.errConvertorFactory = data.errConvertorFactory;
        this.outProcessorFactory = data.outProcessorFactory;
        this.errProcessorFactory = data.errProcessorFactory;
        this.inputOutput = data.inputOutput;
        this.rerunCondition = data.rerunCondition;
        this.optionsPath = data.optionsPath;
        this.charset = data.charset;
        this.noReset = data.noReset;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor inputOutput(@NullAllowed InputOutput io) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.inputOutput(io));
    }

    InputOutput getInputOutput() {
        return this.inputOutput;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor controllable(boolean controllable) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.controllable(controllable));
    }

    boolean isControllable() {
        return this.controllable;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor frontWindow(boolean frontWindow) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.frontWindow(frontWindow));
    }

    boolean isFrontWindow() {
        return this.front;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor inputVisible(boolean inputVisible) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.inputVisible(inputVisible));
    }

    boolean isInputVisible() {
        return this.input;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor showProgress(boolean showProgress) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.showProgress(showProgress));
    }

    boolean showProgress() {
        return this.progress;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor showSuspended(boolean showSuspended) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.showSuspended(showSuspended));
    }

    boolean showSuspended() {
        return this.suspend;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor noReset(boolean noReset) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.noReset(noReset));
    }

    boolean noReset() {
        return this.noReset;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor outLineBased(boolean outLineBased) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.outLineBased(outLineBased));
    }

    boolean isOutLineBased() {
        return this.outLineBased;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor errLineBased(boolean errLineBased) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.errLineBased(errLineBased));
    }

    boolean isErrLineBased() {
        return this.errLineBased;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor frontWindowOnError(boolean frontWindowOnError) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.frontWindowOnError(frontWindowOnError));
    }

    boolean isFrontWindowOnError() {
        return this.frontWindowOnError;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor outProcessorFactory(@NullAllowed InputProcessorFactory outProcessorFactory) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.outProcessorFactory(outProcessorFactory));
    }

    InputProcessorFactory getOutProcessorFactory() {
        return this.outProcessorFactory;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor errProcessorFactory(@NullAllowed InputProcessorFactory errProcessorFactory) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.errProcessorFactory(errProcessorFactory));
    }

    InputProcessorFactory getErrProcessorFactory() {
        return this.errProcessorFactory;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor outConvertorFactory(@NullAllowed LineConvertorFactory convertorFactory) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.outConvertorFactory(convertorFactory));
    }

    LineConvertorFactory getOutConvertorFactory() {
        return this.outConvertorFactory;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor errConvertorFactory(@NullAllowed LineConvertorFactory convertorFactory) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.errConvertorFactory(convertorFactory));
    }

    LineConvertorFactory getErrConvertorFactory() {
        return this.errConvertorFactory;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor preExecution(@NullAllowed Runnable preExecution) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.preExecution(preExecution));
    }

    Runnable getPreExecution() {
        return this.preExecution;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor postExecution(@NullAllowed Runnable postExecution) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.postExecution(postExecution));
    }

    Runnable getPostExecution() {
        return this.postExecution;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor rerunCondition(@NullAllowed RerunCondition rerunCondition) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.rerunCondition(rerunCondition));
    }

    RerunCondition getRerunCondition() {
        return this.rerunCondition;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor optionsPath(@NullAllowed String optionsPath) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.optionsPath(optionsPath));
    }

    String getOptionsPath() {
        return this.optionsPath;
    }

    @NonNull
    @CheckReturnValue
    public ExecutionDescriptor charset(@NullAllowed Charset charset) {
        DescriptorData data = new DescriptorData(this);
        return new ExecutionDescriptor(data.charset(charset));
    }

    Charset getCharset() {
        return this.charset;
    }

    private static final class DescriptorData {
        private Runnable preExecution;
        private Runnable postExecution;
        private boolean suspend;
        private boolean progress;
        private boolean front;
        private boolean input;
        private boolean controllable;
        private boolean noReset;
        private boolean outLineBased;
        private boolean errLineBased;
        private boolean frontWindowOnError;
        private LineConvertorFactory outConvertorFactory;
        private LineConvertorFactory errConvertorFactory;
        private InputProcessorFactory outProcessorFactory;
        private InputProcessorFactory errProcessorFactory;
        private InputOutput inputOutput;
        private RerunCondition rerunCondition;
        private String optionsPath;
        private Charset charset;

        public DescriptorData() {
        }

        public DescriptorData(ExecutionDescriptor descriptor) {
            this.preExecution = descriptor.preExecution;
            this.postExecution = descriptor.postExecution;
            this.suspend = descriptor.suspend;
            this.progress = descriptor.progress;
            this.front = descriptor.front;
            this.input = descriptor.input;
            this.controllable = descriptor.controllable;
            this.outLineBased = descriptor.outLineBased;
            this.errLineBased = descriptor.errLineBased;
            this.frontWindowOnError = descriptor.frontWindowOnError;
            this.outConvertorFactory = descriptor.outConvertorFactory;
            this.errConvertorFactory = descriptor.errConvertorFactory;
            this.outProcessorFactory = descriptor.outProcessorFactory;
            this.errProcessorFactory = descriptor.errProcessorFactory;
            this.inputOutput = descriptor.inputOutput;
            this.rerunCondition = descriptor.rerunCondition;
            this.optionsPath = descriptor.optionsPath;
            this.charset = descriptor.charset;
            this.noReset = descriptor.noReset;
        }

        public DescriptorData inputOutput(InputOutput io) {
            this.inputOutput = io;
            return this;
        }

        public DescriptorData controllable(boolean controllable) {
            this.controllable = controllable;
            return this;
        }

        public DescriptorData frontWindow(boolean frontWindow) {
            this.front = frontWindow;
            return this;
        }

        public DescriptorData inputVisible(boolean inputVisible) {
            this.input = inputVisible;
            return this;
        }

        public DescriptorData showProgress(boolean showProgress) {
            this.progress = showProgress;
            return this;
        }

        public DescriptorData showSuspended(boolean showSuspended) {
            this.suspend = showSuspended;
            return this;
        }

        public DescriptorData noReset(boolean noReset) {
            this.noReset = noReset;
            return this;
        }

        public DescriptorData outLineBased(boolean outLineBased) {
            this.outLineBased = outLineBased;
            return this;
        }

        public DescriptorData errLineBased(boolean errLineBased) {
            this.errLineBased = errLineBased;
            return this;
        }

        public DescriptorData frontWindowOnError(boolean frontWindowOnError) {
            this.frontWindowOnError = frontWindowOnError;
            return this;
        }

        public DescriptorData outProcessorFactory(InputProcessorFactory outProcessorFactory) {
            this.outProcessorFactory = outProcessorFactory;
            return this;
        }

        public DescriptorData errProcessorFactory(InputProcessorFactory errProcessorFactory) {
            this.errProcessorFactory = errProcessorFactory;
            return this;
        }

        public DescriptorData outConvertorFactory(LineConvertorFactory convertorFactory) {
            this.outConvertorFactory = convertorFactory;
            return this;
        }

        public DescriptorData errConvertorFactory(LineConvertorFactory convertorFactory) {
            this.errConvertorFactory = convertorFactory;
            return this;
        }

        public DescriptorData preExecution(Runnable preExecution) {
            this.preExecution = preExecution;
            return this;
        }

        public DescriptorData postExecution(Runnable postExecution) {
            this.postExecution = postExecution;
            return this;
        }

        public DescriptorData rerunCondition(RerunCondition rerunCondition) {
            this.rerunCondition = rerunCondition;
            return this;
        }

        public DescriptorData optionsPath(String optionsPath) {
            this.optionsPath = optionsPath;
            return this;
        }

        public DescriptorData charset(Charset charset) {
            this.charset = charset;
            return this;
        }
    }

    public static interface LineConvertorFactory {
        @NonNull
        public LineConvertor newLineConvertor();
    }

    public static interface InputProcessorFactory {
        @NonNull
        public InputProcessor newInputProcessor(@NonNull InputProcessor var1);
    }

    public static interface RerunCondition {
        public void addChangeListener(@NonNull ChangeListener var1);

        public void removeChangeListener(@NonNull ChangeListener var1);

        public boolean isRerunPossible();
    }

}

