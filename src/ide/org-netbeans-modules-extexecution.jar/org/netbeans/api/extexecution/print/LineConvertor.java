/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.api.extexecution.print;

import java.util.List;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.extexecution.print.ConvertedLine;

public interface LineConvertor {
    @CheckForNull
    public List<ConvertedLine> convert(@NonNull String var1);
}

