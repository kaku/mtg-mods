/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.api.extexecution.print;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.extexecution.print.ConvertedLine;
import org.netbeans.api.extexecution.print.LineConvertor;
import org.netbeans.modules.extexecution.open.DefaultFileOpenHandler;
import org.netbeans.modules.extexecution.open.DefaultHttpOpenHandler;
import org.netbeans.modules.extexecution.print.FileListener;
import org.netbeans.modules.extexecution.print.UrlListener;
import org.netbeans.spi.extexecution.open.FileOpenHandler;
import org.netbeans.spi.extexecution.open.HttpOpenHandler;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class LineConvertors {
    private static final Logger LOGGER = Logger.getLogger(LineConvertors.class.getName());
    private static final DefaultFileOpenHandler DEFAULT_FILE_HANDLER = new DefaultFileOpenHandler();
    private static final DefaultHttpOpenHandler DEFAULT_HTTP_HANDLER = new DefaultHttpOpenHandler();

    private LineConvertors() {
    }

    @NonNull
    public static /* varargs */ LineConvertor proxy(@NonNull LineConvertor ... convertors) {
        return new ProxyLineConvertor(convertors);
    }

    @NonNull
    public static LineConvertor filePattern(@NullAllowed FileLocator fileLocator, @NonNull Pattern linePattern, @NullAllowed Pattern filePattern, int fileGroup, int lineGroup) {
        Parameters.notNull((CharSequence)"linePattern", (Object)linePattern);
        if (fileGroup < 0) {
            throw new IllegalArgumentException("File goup must be non negative: " + fileGroup);
        }
        return new FilePatternConvertor(fileLocator, linePattern, filePattern, fileGroup, lineGroup);
    }

    @NonNull
    public static LineConvertor httpUrl() {
        return new HttpUrlConvertor();
    }

    private static class HttpUrlConvertor
    implements LineConvertor {
        private final Pattern pattern = Pattern.compile(".*(((http)|(https))://\\S+)(\\s.*|$)");
        private final HttpOpenHandler handler;

        public HttpUrlConvertor() {
            HttpOpenHandler candidate = (HttpOpenHandler)Lookup.getDefault().lookup(HttpOpenHandler.class);
            this.handler = candidate != null ? candidate : DEFAULT_HTTP_HANDLER;
        }

        @Override
        public List<ConvertedLine> convert(String line) {
            Matcher matcher = this.pattern.matcher(line);
            if (matcher.matches()) {
                String stringUrl = matcher.group(1);
                try {
                    URL url = new URL(stringUrl);
                    return Collections.singletonList(ConvertedLine.forText(line, new UrlListener(url, this.handler)));
                }
                catch (MalformedURLException ex) {
                    // empty catch block
                }
            }
            return null;
        }
    }

    private static class FilePatternConvertor
    implements LineConvertor {
        private final FileOpenHandler handler;
        private final FileLocator locator;
        private final Pattern linePattern;
        private final Pattern filePattern;
        private final int fileGroup;
        private final int lineGroup;

        public FilePatternConvertor(FileLocator locator, Pattern linePattern, Pattern filePattern) {
            this(locator, linePattern, filePattern, 1, 2);
        }

        public FilePatternConvertor(FileLocator locator, Pattern linePattern, Pattern filePattern, int fileGroup, int lineGroup) {
            FileOpenHandler candidate = (FileOpenHandler)Lookup.getDefault().lookup(FileOpenHandler.class);
            this.handler = candidate != null ? candidate : DEFAULT_FILE_HANDLER;
            this.locator = locator;
            this.linePattern = linePattern;
            this.fileGroup = fileGroup;
            this.lineGroup = lineGroup;
            this.filePattern = filePattern;
        }

        @Override
        public List<ConvertedLine> convert(String line) {
            if (line.length() > 400) {
                return null;
            }
            Matcher match = this.linePattern.matcher(line);
            if (match.matches()) {
                String file = null;
                int lineno = -1;
                if (this.fileGroup >= 0) {
                    file = match.group(this.fileGroup);
                    if (file.startsWith("\"")) {
                        file = file.substring(1);
                    }
                    if (file.startsWith("./")) {
                        file = file.substring(2);
                    }
                    if (this.filePattern != null && !this.filePattern.matcher(file).matches()) {
                        return null;
                    }
                }
                if (this.lineGroup >= 0) {
                    String linenoStr = match.group(this.lineGroup);
                    try {
                        lineno = Integer.parseInt(linenoStr);
                    }
                    catch (NumberFormatException nfe) {
                        LOGGER.log(Level.INFO, null, nfe);
                        lineno = 0;
                    }
                }
                return Collections.singletonList(ConvertedLine.forText(line, new FileListener(file, lineno, this.locator, this.handler)));
            }
            return null;
        }
    }

    private static class ProxyLineConvertor
    implements LineConvertor {
        private final List<LineConvertor> convertors = new ArrayList<LineConvertor>();

        public /* varargs */ ProxyLineConvertor(LineConvertor ... convertors) {
            for (LineConvertor convertor : convertors) {
                if (convertor == null) continue;
                this.convertors.add(convertor);
            }
        }

        @Override
        public List<ConvertedLine> convert(String line) {
            for (LineConvertor convertor : this.convertors) {
                List<ConvertedLine> converted = convertor.convert(line);
                if (converted == null) continue;
                return converted;
            }
            return null;
        }
    }

    public static interface FileLocator {
        @CheckForNull
        public FileObject find(@NonNull String var1);
    }

}

