/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 *  org.openide.windows.OutputListener
 */
package org.netbeans.api.extexecution.print;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.openide.util.Parameters;
import org.openide.windows.OutputListener;

public final class ConvertedLine {
    private final String text;
    private final OutputListener listener;

    private ConvertedLine(String text, OutputListener listener) {
        assert (text != null);
        this.text = text;
        this.listener = listener;
    }

    @NonNull
    public static ConvertedLine forText(@NonNull String text, @NullAllowed OutputListener listener) {
        Parameters.notNull((CharSequence)"text", (Object)text);
        return new ConvertedLine(text, listener);
    }

    @NonNull
    public String getText() {
        return this.text;
    }

    @CheckForNull
    public OutputListener getListener() {
        return this.listener;
    }
}

