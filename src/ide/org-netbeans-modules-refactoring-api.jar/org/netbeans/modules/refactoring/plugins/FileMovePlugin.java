/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.PositionBounds
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.plugins;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.logging.Logger;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.MoveRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.plugins.FileHandlingFactory;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.PositionBounds;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class FileMovePlugin
implements RefactoringPlugin {
    private MoveRefactoring refactoring;

    public FileMovePlugin(MoveRefactoring refactoring) {
        this.refactoring = refactoring;
    }

    @Override
    public Problem preCheck() {
        return null;
    }

    @Override
    public Problem prepare(RefactoringElementsBag elements) {
        URL targetUrl = (URL)this.refactoring.getTarget().lookup(URL.class);
        if (targetUrl != null) {
            for (FileObject o : this.refactoring.getRefactoringSource().lookupAll(FileObject.class)) {
                elements.addFileChange(this.refactoring, new MoveFile(o, elements));
            }
        }
        return null;
    }

    @Override
    public Problem fastCheckParameters() {
        return null;
    }

    @Override
    public Problem checkParameters() {
        return null;
    }

    @Override
    public void cancelRequest() {
    }

    private class MoveFile
    extends SimpleRefactoringElementImplementation {
        private FileObject fo;
        DataFolder sourceFolder;
        DataObject source;

        public MoveFile(FileObject fo, RefactoringElementsBag session) {
            this.fo = fo;
        }

        @Override
        public String getText() {
            return NbBundle.getMessage(FileMovePlugin.class, (String)"TXT_MoveFile", (Object)this.fo.getNameExt());
        }

        @Override
        public String getDisplayText() {
            return this.getText();
        }

        @Override
        public void performChange() {
            try {
                FileObject target = FileHandlingFactory.getOrCreateFolder((URL)FileMovePlugin.this.refactoring.getTarget().lookup(URL.class));
                DataFolder targetFolder = DataFolder.findFolder((FileObject)target);
                if (!this.fo.isValid()) {
                    String path = FileUtil.getFileDisplayName((FileObject)this.fo);
                    Logger.getLogger(FileMovePlugin.class.getName()).fine("Invalid FileObject " + path + "trying to recreate...");
                    this.fo = FileUtil.toFileObject((File)FileUtil.toFile((FileObject)this.fo));
                    if (this.fo == null) {
                        Logger.getLogger(FileMovePlugin.class.getName()).severe("Invalid FileObject " + path + "\n. File not found.");
                        return;
                    }
                }
                this.source = DataObject.find((FileObject)this.fo);
                this.sourceFolder = this.source.getFolder();
                this.source.move(targetFolder);
            }
            catch (DataObjectNotFoundException ex) {
                ex.printStackTrace();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void undoChange() {
            try {
                this.source.move(this.sourceFolder);
            }
            catch (DataObjectNotFoundException ex) {
                ex.printStackTrace();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public Lookup getLookup() {
            return Lookup.EMPTY;
        }

        @Override
        public FileObject getParentFile() {
            return this.fo;
        }

        @Override
        public PositionBounds getPosition() {
            return null;
        }
    }

}

