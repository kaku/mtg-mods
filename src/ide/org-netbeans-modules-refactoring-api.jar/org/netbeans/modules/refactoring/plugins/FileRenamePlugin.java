/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.PositionBounds
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.plugins;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.RenameRefactoring;
import org.netbeans.modules.refactoring.api.impl.CannotUndoRefactoring;
import org.netbeans.modules.refactoring.plugins.Bundle;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.PositionBounds;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class FileRenamePlugin
implements RefactoringPlugin {
    private RenameRefactoring refactoring;

    public FileRenamePlugin(RenameRefactoring refactoring) {
        this.refactoring = refactoring;
    }

    @Override
    public Problem preCheck() {
        return null;
    }

    @Override
    public Problem prepare(RefactoringElementsBag elements) {
        elements.addFileChange(this.refactoring, new RenameFile((FileObject)this.refactoring.getRefactoringSource().lookup(FileObject.class), elements));
        return null;
    }

    @Override
    public Problem fastCheckParameters() {
        return null;
    }

    @Override
    public Problem checkParameters() {
        return null;
    }

    @Override
    public void cancelRequest() {
    }

    private class RenameFile
    extends SimpleRefactoringElementImplementation {
        private FileObject fo;
        private String oldName;

        public RenameFile(FileObject fo, RefactoringElementsBag bag) {
            this.fo = fo;
        }

        @Override
        public String getText() {
            return this.fo.isFolder() ? Bundle.TXT_RenameFolder(this.fo.getNameExt()) : Bundle.TXT_RenameFile(this.fo.getNameExt());
        }

        @Override
        public String getDisplayText() {
            return this.getText();
        }

        @Override
        public void performChange() {
            try {
                this.oldName = this.fo.getName();
                DataObject.find((FileObject)this.fo).rename(FileRenamePlugin.this.refactoring.getNewName());
            }
            catch (DataObjectNotFoundException ex) {
                throw new IllegalStateException((Throwable)ex);
            }
            catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }

        @Override
        public void undoChange() {
            try {
                if (!this.fo.isValid()) {
                    throw new CannotUndoRefactoring(Collections.singleton(this.fo.getPath()));
                }
                DataObject.find((FileObject)this.fo).rename(this.oldName);
            }
            catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        @Override
        public Lookup getLookup() {
            return Lookup.EMPTY;
        }

        @Override
        public FileObject getParentFile() {
            return this.fo;
        }

        @Override
        public PositionBounds getPosition() {
            return null;
        }
    }

}

