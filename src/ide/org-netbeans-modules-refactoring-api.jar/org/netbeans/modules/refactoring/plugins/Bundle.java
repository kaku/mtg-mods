/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.plugins;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String TXT_RenameFile(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"TXT_RenameFile", (Object)arg0);
    }

    static String TXT_RenameFolder(Object arg0) {
        return NbBundle.getMessage(Bundle.class, (String)"TXT_RenameFolder", (Object)arg0);
    }

    private void Bundle() {
    }
}

