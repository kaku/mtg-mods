/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.text.PositionBounds
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.plugins;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import org.netbeans.modules.refactoring.api.Context;
import org.netbeans.modules.refactoring.plugins.FileHandlingFactory;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.text.PositionBounds;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class CopyFile
extends SimpleRefactoringElementImplementation {
    private FileObject fo;
    private DataObject newOne;
    private final URL target;
    private final String newName;
    private final Context context;

    public CopyFile(FileObject fo, URL target, String newName, Context context) {
        this.fo = fo;
        this.target = target;
        this.newName = newName;
        this.context = context;
    }

    @Override
    public String getText() {
        return NbBundle.getMessage(CopyFile.class, (String)"TXT_CopyFile", (Object)this.fo.getNameExt());
    }

    @Override
    public String getDisplayText() {
        return this.getText();
    }

    @Override
    public void performChange() {
        try {
            FileObject targetFo = FileHandlingFactory.getOrCreateFolder(this.target);
            FileObject Fo = this.fo;
            DataObject dob = DataObject.find((FileObject)Fo);
            this.newOne = dob.copy(DataFolder.findFolder((FileObject)targetFo));
            if (this.newName != null) {
                this.newOne.rename(this.newName);
            }
            FileObject[] newFiles = this.context.lookup(FileObject[].class);
            FileObject newFile = this.newOne.getPrimaryFile();
            newFile.setAttribute("originalFile", (Object)this.fo.getNameExt());
            if (newFiles == null) {
                newFiles = new FileObject[]{newFile};
            } else {
                newFiles = Arrays.copyOf(newFiles, newFiles.length + 1);
                newFiles[newFiles.length - 1] = newFile;
            }
            this.context.add(newFiles);
            this.context.add((Object)newFile);
        }
        catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void undoChange() {
        try {
            if (this.newOne != null) {
                this.newOne.delete();
            }
        }
        catch (IOException ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
    }

    @Override
    public Lookup getLookup() {
        return Lookup.EMPTY;
    }

    @Override
    public FileObject getParentFile() {
        return this.fo;
    }

    @Override
    public PositionBounds getPosition() {
        return null;
    }
}

