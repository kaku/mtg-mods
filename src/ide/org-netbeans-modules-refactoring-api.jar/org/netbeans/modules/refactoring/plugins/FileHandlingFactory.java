/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.fileinfo.NonRecursiveFolder
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.plugins;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.CopyRefactoring;
import org.netbeans.modules.refactoring.api.MoveRefactoring;
import org.netbeans.modules.refactoring.api.RenameRefactoring;
import org.netbeans.modules.refactoring.api.SafeDeleteRefactoring;
import org.netbeans.modules.refactoring.api.SingleCopyRefactoring;
import org.netbeans.modules.refactoring.plugins.FileDeletePlugin;
import org.netbeans.modules.refactoring.plugins.FileMovePlugin;
import org.netbeans.modules.refactoring.plugins.FileRenamePlugin;
import org.netbeans.modules.refactoring.plugins.FilesCopyPlugin;
import org.netbeans.modules.refactoring.plugins.PackageDeleteRefactoringPlugin;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.RefactoringPluginFactory;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Lookup;

public class FileHandlingFactory
implements RefactoringPluginFactory {
    @Override
    public RefactoringPlugin createInstance(AbstractRefactoring refactoring) {
        Lookup look = refactoring.getRefactoringSource();
        Collection o = look.lookupAll(FileObject.class);
        NonRecursiveFolder folder = (NonRecursiveFolder)look.lookup(NonRecursiveFolder.class);
        if (refactoring instanceof RenameRefactoring) {
            if (!o.isEmpty()) {
                return new FileRenamePlugin((RenameRefactoring)refactoring);
            }
        } else if (refactoring instanceof MoveRefactoring) {
            if (!o.isEmpty()) {
                return new FileMovePlugin((MoveRefactoring)refactoring);
            }
        } else if (refactoring instanceof SafeDeleteRefactoring) {
            if (folder != null) {
                return new PackageDeleteRefactoringPlugin((SafeDeleteRefactoring)refactoring);
            }
            if (!o.isEmpty()) {
                FileObject fObj = (FileObject)o.iterator().next();
                if (fObj.isFolder()) {
                    return new PackageDeleteRefactoringPlugin((SafeDeleteRefactoring)refactoring);
                }
                return new FileDeletePlugin((SafeDeleteRefactoring)refactoring);
            }
        } else if ((refactoring instanceof SingleCopyRefactoring || refactoring instanceof CopyRefactoring) && !o.isEmpty()) {
            return new FilesCopyPlugin(refactoring);
        }
        return null;
    }

    static FileObject getOrCreateFolder(URL url) throws IOException {
        try {
            FileObject result = URLMapper.findFileObject((URL)url);
            if (result != null) {
                return result;
            }
            File f = new File(url.toURI());
            result = FileUtil.createFolder((File)f);
            return result;
        }
        catch (URISyntaxException ex) {
            throw (IOException)new IOException().initCause(ex);
        }
    }
}

