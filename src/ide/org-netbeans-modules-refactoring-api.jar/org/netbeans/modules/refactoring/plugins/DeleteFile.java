/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.PositionBounds
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.refactoring.plugins;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.refactoring.api.impl.CannotUndoRefactoring;
import org.netbeans.modules.refactoring.plugins.FileDeletePlugin;
import org.netbeans.modules.refactoring.spi.BackupFacility;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.PositionBounds;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class DeleteFile
extends SimpleRefactoringElementImplementation {
    private final URL res;
    private final String filename;
    private final RefactoringElementsBag session;
    private BackupFacility.Handle id;

    public DeleteFile(FileObject fo, RefactoringElementsBag session) {
        this.res = fo.toURL();
        this.filename = fo.getNameExt();
        this.session = session;
    }

    @Override
    public String getText() {
        return NbBundle.getMessage(FileDeletePlugin.class, (String)"TXT_DeleteFile", (Object)this.filename);
    }

    @Override
    public String getDisplayText() {
        return this.getText();
    }

    @Override
    public void performChange() {
        try {
            FileObject fo = URLMapper.findFileObject((URL)this.res);
            if (fo == null) {
                throw new IOException(this.res.toString());
            }
            this.id = BackupFacility.getDefault().backup(fo);
            DataObject.find((FileObject)fo).delete();
        }
        catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public void undoChange() {
        try {
            try {
                File f = Utilities.toFile((URI)this.res.toURI());
                if (f.exists()) {
                    throw new CannotUndoRefactoring(Collections.singleton(f.getPath()));
                }
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            this.id.restore();
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    @Override
    public Lookup getLookup() {
        return Lookup.EMPTY;
    }

    @Override
    public FileObject getParentFile() {
        return URLMapper.findFileObject((URL)this.res);
    }

    @Override
    public PositionBounds getPosition() {
        return null;
    }
}

