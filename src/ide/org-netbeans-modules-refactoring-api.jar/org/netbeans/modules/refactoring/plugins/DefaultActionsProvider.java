/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.plugins;

import java.util.Collection;
import org.netbeans.modules.refactoring.spi.impl.SafeDeleteUI;
import org.netbeans.modules.refactoring.spi.ui.ActionsImplementationProvider;
import org.netbeans.modules.refactoring.spi.ui.UI;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class DefaultActionsProvider
extends ActionsImplementationProvider {
    @Override
    public boolean canDelete(Lookup lookup) {
        return false;
    }

    @Override
    public void doDelete(Lookup lookup) {
        Collection nodes = lookup.lookupAll(Node.class);
        FileObject[] fobs = new FileObject[nodes.size()];
        int i = 0;
        for (Node node : nodes) {
            DataObject dob = (DataObject)node.getCookie(DataObject.class);
            if (dob == null) continue;
            fobs[i++] = dob.getPrimaryFile();
        }
        UI.openRefactoringUI(new SafeDeleteUI<FileObject>((T[])fobs));
    }
}

