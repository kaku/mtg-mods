/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.plugins;

import java.net.URL;
import java.util.Collection;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Context;
import org.netbeans.modules.refactoring.api.CopyRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.SingleCopyRefactoring;
import org.netbeans.modules.refactoring.plugins.CopyFile;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class FilesCopyPlugin
implements RefactoringPlugin {
    private AbstractRefactoring refactoring;

    public FilesCopyPlugin(AbstractRefactoring refactoring) {
        this.refactoring = refactoring;
    }

    @Override
    public Problem preCheck() {
        return null;
    }

    @Override
    public Problem prepare(RefactoringElementsBag elements) {
        SingleCopyRefactoring scr;
        URL target = null;
        String newName = null;
        if (this.refactoring instanceof SingleCopyRefactoring) {
            scr = (SingleCopyRefactoring)this.refactoring;
            target = (URL)scr.getTarget().lookup(URL.class);
            newName = scr.getNewName();
        } else if (this.refactoring instanceof CopyRefactoring) {
            scr = (CopyRefactoring)this.refactoring;
            target = (URL)scr.getTarget().lookup(URL.class);
        }
        Collection fileObjects = this.refactoring.getRefactoringSource().lookupAll(FileObject.class);
        for (FileObject fileObject : fileObjects) {
            elements.add(this.refactoring, new CopyFile(fileObject, target, newName, this.refactoring.getContext()));
        }
        return null;
    }

    @Override
    public Problem fastCheckParameters() {
        return null;
    }

    @Override
    public Problem checkParameters() {
        return null;
    }

    @Override
    public void cancelRequest() {
    }
}

