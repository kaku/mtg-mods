/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.plugins;

import java.util.Map;
import java.util.WeakHashMap;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.plugins.RefactoringTreeElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElementFactoryImplementation;

public class TreeElementFactoryImpl
implements TreeElementFactoryImplementation {
    private Map<Object, TreeElement> map = new WeakHashMap<Object, TreeElement>();

    @Override
    public TreeElement getTreeElement(Object o) {
        TreeElement result = this.map.get(o);
        if (result != null) {
            return result;
        }
        if (o instanceof RefactoringElement) {
            result = new RefactoringTreeElement((RefactoringElement)o);
        }
        if (result != null) {
            this.map.put(o, result);
        }
        return result;
    }

    @Override
    public void cleanUp() {
        this.map.clear();
    }
}

