/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.fileinfo.NonRecursiveFolder
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.api.project.SourceGroup
 *  org.netbeans.api.project.Sources
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.text.PositionBounds
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.plugins;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.SafeDeleteRefactoring;
import org.netbeans.modules.refactoring.plugins.DeleteFile;
import org.netbeans.modules.refactoring.plugins.FileDeletePlugin;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.text.PositionBounds;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class PackageDeleteRefactoringPlugin
implements RefactoringPlugin {
    private final SafeDeleteRefactoring refactoring;
    static final String JAVA_EXTENSION = "java";

    public PackageDeleteRefactoringPlugin(SafeDeleteRefactoring safeDeleteRefactoring) {
        this.refactoring = safeDeleteRefactoring;
    }

    @Override
    public Problem prepare(RefactoringElementsBag refactoringElements) {
        Lookup lkp = this.refactoring.getRefactoringSource();
        NonRecursiveFolder folder = (NonRecursiveFolder)lkp.lookup(NonRecursiveFolder.class);
        if (folder != null) {
            return this.preparePackageDelete(folder, refactoringElements);
        }
        FileObject fileObject = (FileObject)lkp.lookup(FileObject.class);
        if (fileObject != null && fileObject.isFolder()) {
            return this.prepareFolderDelete(fileObject, refactoringElements);
        }
        return null;
    }

    @Override
    public Problem preCheck() {
        return null;
    }

    @Override
    public Problem checkParameters() {
        return null;
    }

    @Override
    public Problem fastCheckParameters() {
        return null;
    }

    @Override
    public void cancelRequest() {
    }

    private Problem prepareFolderDelete(FileObject fileObject, RefactoringElementsBag refactoringElements) {
        this.addDataFilesInFolder(fileObject, refactoringElements);
        refactoringElements.addFileChange(this.refactoring, new FolderDeleteElem(fileObject));
        return null;
    }

    private Problem preparePackageDelete(NonRecursiveFolder folder, RefactoringElementsBag refactoringElements) {
        DataFolder dataFolder = DataFolder.findFolder((FileObject)folder.getFolder());
        DataObject[] children = dataFolder.getChildren();
        boolean empty = true;
        for (int i = 0; children != null && i < children.length; ++i) {
            FileObject fileObject = children[i].getPrimaryFile();
            if (!fileObject.isFolder()) {
                refactoringElements.addFileChange(this.refactoring, new DeleteFile(fileObject, refactoringElements));
                continue;
            }
            empty = false;
        }
        if (empty) {
            refactoringElements.addFileChange(this.refactoring, new PackageDeleteElem(folder));
        }
        return null;
    }

    private void addDataFilesInFolder(FileObject folderFileObject, RefactoringElementsBag refactoringElements) {
        for (FileObject childFileObject : folderFileObject.getChildren()) {
            if (!childFileObject.isFolder()) {
                refactoringElements.addFileChange(this.refactoring, new DeleteFile(childFileObject, refactoringElements));
                continue;
            }
            if (!childFileObject.isFolder()) continue;
            this.addDataFilesInFolder(childFileObject, refactoringElements);
        }
    }

    private static void createNewFolder(File f) throws IOException {
        if (!f.exists()) {
            File parent = f.getParentFile();
            if (parent != null) {
                PackageDeleteRefactoringPlugin.createNewFolder(parent);
            }
            f.mkdir();
        }
    }

    private static class PackageDeleteElem
    extends SimpleRefactoringElementImplementation {
        private final URL res;
        private final NonRecursiveFolder folder;
        private File dir;
        private SourceGroup srcGroup;

        private PackageDeleteElem(NonRecursiveFolder folder) {
            this.folder = folder;
            this.dir = FileUtil.toFile((FileObject)folder.getFolder());
            try {
                this.res = folder.getFolder().getURL();
            }
            catch (FileStateInvalidException fileStateInvalidException) {
                throw new IllegalStateException((Throwable)fileStateInvalidException);
            }
            this.srcGroup = this.getSourceGroup(folder.getFolder(), "java");
            if (this.srcGroup == null) {
                this.srcGroup = this.getSourceGroup(folder.getFolder(), "generic");
            }
        }

        @Override
        public void performChange() {
            FileObject root = this.srcGroup.getRootFolder();
            FileObject parent = this.folder.getFolder().getParent();
            this.dir = FileUtil.toFile((FileObject)this.folder.getFolder());
            try {
                this.folder.getFolder().delete();
                while (!parent.equals((Object)root) && parent.getChildren().length == 0) {
                    FileObject newParent = parent.getParent();
                    parent.delete();
                    parent = newParent;
                }
            }
            catch (IOException ioException) {
                ErrorManager.getDefault().notify((Throwable)ioException);
            }
        }

        @Override
        public void undoChange() {
            try {
                PackageDeleteRefactoringPlugin.createNewFolder(this.dir);
            }
            catch (IOException ioException) {
                ErrorManager.getDefault().notify((Throwable)ioException);
            }
        }

        @Override
        public String getText() {
            return NbBundle.getMessage(FileDeletePlugin.class, (String)"TXT_DeletePackage", (Object)this.dir.getName());
        }

        @Override
        public String getDisplayText() {
            return this.getText();
        }

        @Override
        public Lookup getLookup() {
            return Lookup.EMPTY;
        }

        @Override
        public FileObject getParentFile() {
            return URLMapper.findFileObject((URL)this.res);
        }

        @Override
        public PositionBounds getPosition() {
            return null;
        }

        private SourceGroup getSourceGroup(FileObject file, String type) {
            SourceGroup[] javagroups;
            Project prj = FileOwnerQuery.getOwner((FileObject)file);
            if (prj == null) {
                return null;
            }
            Sources src = ProjectUtils.getSources((Project)prj);
            for (SourceGroup javaSourceGroup : javagroups = src.getSourceGroups(type)) {
                if (!javaSourceGroup.getRootFolder().equals((Object)file) && !FileUtil.isParentOf((FileObject)javaSourceGroup.getRootFolder(), (FileObject)file)) continue;
                return javaSourceGroup;
            }
            return null;
        }
    }

    private static class FolderDeleteElem
    extends SimpleRefactoringElementImplementation {
        private final FileObject dirFileObject;
        private File dir;

        private FolderDeleteElem(FileObject folder) {
            this.dirFileObject = folder;
            this.dir = FileUtil.toFile((FileObject)this.dirFileObject);
        }

        @Override
        public void performChange() {
            try {
                this.dirFileObject.delete();
            }
            catch (IOException ioException) {
                ErrorManager.getDefault().notify((Throwable)ioException);
            }
        }

        @Override
        public void undoChange() {
            try {
                PackageDeleteRefactoringPlugin.createNewFolder(this.dir);
            }
            catch (IOException ioException) {
                ErrorManager.getDefault().notify((Throwable)ioException);
            }
        }

        @Override
        public String getText() {
            return NbBundle.getMessage(FileDeletePlugin.class, (String)"TXT_DeleteFolder", (Object)this.dirFileObject.getNameExt());
        }

        @Override
        public String getDisplayText() {
            return this.getText();
        }

        @Override
        public Lookup getLookup() {
            return Lookup.EMPTY;
        }

        @Override
        public FileObject getParentFile() {
            try {
                return URLMapper.findFileObject((URL)this.dirFileObject.getURL());
            }
            catch (FileStateInvalidException ex) {
                ErrorManager.getDefault().notify((Throwable)ex);
                throw new IllegalStateException((Throwable)ex);
            }
        }

        @Override
        public PositionBounds getPosition() {
            return null;
        }
    }

}

