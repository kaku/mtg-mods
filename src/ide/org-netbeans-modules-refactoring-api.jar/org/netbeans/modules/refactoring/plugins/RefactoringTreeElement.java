/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.plugins;

import javax.swing.Icon;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElementFactory;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public class RefactoringTreeElement
implements TreeElement {
    RefactoringElement element;
    private final Icon icon;

    RefactoringTreeElement(RefactoringElement element) {
        this.element = element;
        this.icon = (Icon)element.getLookup().lookup(Icon.class);
    }

    @Override
    public TreeElement getParent(boolean isLogical) {
        Object composite;
        if (isLogical && (composite = this.element.getLookup().lookup(Object.class)) != null) {
            return TreeElementFactory.getTreeElement(composite);
        }
        return TreeElementFactory.getTreeElement((Object)this.element.getParentFile());
    }

    @Override
    public Icon getIcon() {
        return this.icon;
    }

    @Override
    public String getText(boolean isLogical) {
        return this.element.getDisplayText();
    }

    @Override
    public Object getUserObject() {
        return this.element;
    }
}

