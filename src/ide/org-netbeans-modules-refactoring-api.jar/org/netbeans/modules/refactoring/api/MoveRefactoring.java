/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class MoveRefactoring
extends AbstractRefactoring {
    private Lookup target;

    public MoveRefactoring(@NonNull Lookup objectsToMove) {
        super(objectsToMove);
    }

    public void setTarget(@NonNull Lookup target) {
        Parameters.notNull((CharSequence)"target", (Object)target);
        this.target = target;
    }

    @CheckForNull
    public Lookup getTarget() {
        return this.target;
    }
}

