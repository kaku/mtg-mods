/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.editor.Utilities
 *  org.openide.LifecycleManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import java.io.IOException;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.api.impl.ProgressSupport;
import org.netbeans.modules.refactoring.api.impl.SPIAccessor;
import org.netbeans.modules.refactoring.spi.ProgressProvider;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.Transaction;
import org.netbeans.modules.refactoring.spi.impl.UndoManager;
import org.netbeans.modules.refactoring.spi.impl.UndoableWrapper;
import org.openide.LifecycleManager;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.Parameters;

public final class RefactoringSession {
    private final ArrayList<RefactoringElementImplementation> internalList = new ArrayList();
    private final RefactoringElementsBag bag;
    private final Collection<RefactoringElement> refactoringElements;
    private final String description;
    private ProgressSupport progressSupport;
    private UndoManager undoManager = UndoManager.getDefault();
    boolean realcommit = true;
    private AtomicBoolean finished = new AtomicBoolean(false);
    private static final int COMMITSTEPS = 100;

    private RefactoringSession(String description) {
        this.bag = SPIAccessor.DEFAULT.createBag(this, this.internalList);
        this.description = description;
        this.refactoringElements = new ElementsCollection();
    }

    @NonNull
    public static RefactoringSession create(@NonNull String description) {
        Parameters.notNull((CharSequence)"description", (Object)description);
        return new RefactoringSession(description);
    }

    @CheckForNull
    public Problem doRefactoring(final boolean saveAfterDone) {
        return (Problem)Utilities.runWithOnSaveTasksDisabled((Mutex.Action)new Mutex.Action<Problem>(){

            public Problem run() {
                return RefactoringSession.this.reallyDoRefactoring(saveAfterDone);
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Problem reallyDoRefactoring(boolean saveAfterDone) {
        long time = System.currentTimeMillis();
        Iterator<RefactoringElementImplementation> it = this.internalList.iterator();
        ArrayList<Transaction> commits = SPIAccessor.DEFAULT.getCommits(this.bag);
        float progressStep = 100.0f / (float)this.internalList.size();
        float current = 0.0f;
        this.fireProgressListenerStart(0, 100 + commits.size() * 100 + 1);
        ProgressL progressListener = new ProgressL(commits, 100);
        if (this.realcommit) {
            this.undoManager.transactionStarted();
            this.undoManager.setUndoDescription(this.description);
        }
        try {
            try {
                while (it.hasNext()) {
                    RefactoringElementImplementation element = it.next();
                    if (element.isEnabled() && element.getStatus() != 2 && element.getStatus() != 3) {
                        element.performChange();
                    }
                    this.fireProgressListenerStep((int)(current += progressStep));
                }
            }
            finally {
                for (Transaction commit : commits) {
                    SPIAccessor.DEFAULT.check(commit, false);
                }
                UndoableWrapper wrapper = (UndoableWrapper)MimeLookup.getLookup((String)"").lookup(UndoableWrapper.class);
                for (Transaction commit22 : commits) {
                    ProgressProvider progressProvider;
                    if (wrapper != null) {
                        this.setWrappers(commit22, wrapper);
                    }
                    if (commit22 instanceof ProgressProvider) {
                        progressProvider = (ProgressProvider)((Object)commit22);
                        progressProvider.addProgressListener(progressListener);
                    }
                    try {
                        commit22.commit();
                    }
                    finally {
                        if (commit22 instanceof ProgressProvider) {
                            progressProvider = (ProgressProvider)((Object)commit22);
                            progressProvider.removeProgressListener(progressListener);
                        }
                    }
                    if (wrapper == null) continue;
                    this.unsetWrappers(commit22, wrapper);
                }
                if (wrapper != null) {
                    wrapper.close();
                }
                for (Transaction commit22 : commits) {
                    SPIAccessor.DEFAULT.sum(commit22);
                }
            }
            if (saveAfterDone) {
                LifecycleManager.getDefault().saveAll();
                for (DataObject dob : DataObject.getRegistry().getModified()) {
                    SaveCookie cookie = (SaveCookie)dob.getCookie(SaveCookie.class);
                    try {
                        cookie.save();
                        continue;
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
            for (RefactoringElementImplementation fileChange : SPIAccessor.DEFAULT.getFileChanges(this.bag)) {
                if (!fileChange.isEnabled()) continue;
                fileChange.performChange();
            }
            this.fireProgressListenerStep();
        }
        finally {
            this.fireProgressListenerStop();
            if (this.realcommit) {
                this.undoManager.addItem(this);
                this.undoManager.transactionEnded(false, this);
                this.realcommit = false;
            }
        }
        Logger timer = Logger.getLogger("TIMER.RefactoringSession");
        if (timer.isLoggable(Level.FINE)) {
            time = System.currentTimeMillis() - time;
            timer.log(Level.FINE, "refactoringSession.doRefactoring", new Object[]{this.description, this, time});
        }
        return null;
    }

    @CheckForNull
    public Problem undoRefactoring(final boolean saveAfterDone) {
        return (Problem)Utilities.runWithOnSaveTasksDisabled((Mutex.Action)new Mutex.Action<Problem>(){

            public Problem run() {
                return RefactoringSession.this.reallyUndoRefactoring(saveAfterDone);
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Problem reallyUndoRefactoring(boolean saveAfterDone) {
        try {
            ListIterator<RefactoringElementImplementation> it = this.internalList.listIterator(this.internalList.size());
            this.fireProgressListenerStart(0, this.internalList.size() + 1);
            ArrayList<RefactoringElementImplementation> fileChanges = SPIAccessor.DEFAULT.getFileChanges(this.bag);
            ArrayList<Transaction> commits = SPIAccessor.DEFAULT.getCommits(this.bag);
            ListIterator<RefactoringElementImplementation> fileChangeIterator = fileChanges.listIterator(fileChanges.size());
            while (fileChangeIterator.hasPrevious()) {
                RefactoringElementImplementation f = fileChangeIterator.previous();
                if (!f.isEnabled()) continue;
                f.undoChange();
            }
            for (Transaction commit : SPIAccessor.DEFAULT.getCommits(this.bag)) {
                SPIAccessor.DEFAULT.check(commit, true);
            }
            UndoableWrapper wrapper = (UndoableWrapper)MimeLookup.getLookup((String)"").lookup(UndoableWrapper.class);
            ListIterator<Transaction> commitIterator = commits.listIterator(commits.size());
            while (commitIterator.hasPrevious()) {
                Transaction commit2 = commitIterator.previous();
                this.setWrappers(commit2, wrapper);
                commit2.rollback();
                this.unsetWrappers(commit2, wrapper);
            }
            wrapper.close();
            for (Transaction commit2 : SPIAccessor.DEFAULT.getCommits(this.bag)) {
                SPIAccessor.DEFAULT.sum(commit2);
            }
            while (it.hasPrevious()) {
                this.fireProgressListenerStep();
                RefactoringElementImplementation element = it.previous();
                if (!element.isEnabled() || element.getStatus() == 2 || element.getStatus() == 3) continue;
                element.undoChange();
            }
            if (saveAfterDone) {
                LifecycleManager.getDefault().saveAll();
            }
            this.fireProgressListenerStep();
        }
        finally {
            this.fireProgressListenerStop();
        }
        return null;
    }

    @NonNull
    public Collection<RefactoringElement> getRefactoringElements() {
        return this.refactoringElements;
    }

    public void finished() {
        this.finished.set(true);
    }

    boolean isFinished() {
        return this.finished.get();
    }

    public synchronized void addProgressListener(@NonNull ProgressListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        if (this.progressSupport == null) {
            this.progressSupport = new ProgressSupport();
        }
        this.progressSupport.addProgressListener(listener);
    }

    public synchronized void removeProgressListener(@NonNull ProgressListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        if (this.progressSupport != null) {
            this.progressSupport.removeProgressListener(listener);
        }
    }

    RefactoringElementsBag getElementsBag() {
        return this.bag;
    }

    private void fireProgressListenerStart(int type, int count) {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStart(this, type, count);
        }
    }

    private void fireProgressListenerStep() {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStep(this);
        }
    }

    private void fireProgressListenerStep(int count) {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStep(this, count);
        }
    }

    private void fireProgressListenerStop() {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStop(this);
        }
    }

    private void setWrappers(Transaction commit, UndoableWrapper wrap) {
        wrap.setActive(true, this);
    }

    private void unsetWrappers(Transaction commit, UndoableWrapper wrap) {
        wrap.setActive(false, null);
    }

    private Document getDocument(FileObject f) {
        try {
            DataObject dob = DataObject.find((FileObject)f);
            EditorCookie cookie = (EditorCookie)dob.getLookup().lookup(EditorCookie.class);
            if (cookie == null) {
                return null;
            }
            return cookie.getDocument();
        }
        catch (DataObjectNotFoundException ex) {
            return null;
        }
    }

    private class ElementsCollection
    extends AbstractCollection<RefactoringElement> {
        private ElementsCollection() {
        }

        @Override
        public Iterator<RefactoringElement> iterator() {
            return new Iterator(){
                private final Iterator<RefactoringElementImplementation> inner2;
                private int index;

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }

                public RefactoringElement next() {
                    if (this.index < RefactoringSession.this.internalList.size()) {
                        return new RefactoringElement((RefactoringElementImplementation)RefactoringSession.this.internalList.get(this.index++));
                    }
                    return new RefactoringElement(this.inner2.next());
                }

                @Override
                public boolean hasNext() {
                    boolean hasNext;
                    boolean bl = hasNext = this.index < RefactoringSession.this.internalList.size();
                    if (hasNext) {
                        return hasNext;
                    }
                    while (!RefactoringSession.this.finished.get()) {
                        try {
                            Thread.sleep(50);
                        }
                        catch (InterruptedException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                        if (!(hasNext = this.index < RefactoringSession.this.internalList.size())) continue;
                        return hasNext;
                    }
                    return this.index < RefactoringSession.this.internalList.size() || this.inner2.hasNext();
                }
            };
        }

        @Override
        public int size() {
            return RefactoringSession.this.internalList.size() + SPIAccessor.DEFAULT.getFileChanges(RefactoringSession.this.bag).size();
        }

    }

    private class ProgressL
    implements ProgressListener {
        private float progressStep;
        private float current;
        private final ArrayList<Transaction> commits;
        private final int start;

        ProgressL(ArrayList<Transaction> commits, int start) {
            this.commits = commits;
            this.start = start;
        }

        @Override
        public void start(ProgressEvent event) {
            this.progressStep = 100.0f / (float)event.getCount();
            this.current = this.start + this.commits.indexOf(event.getSource()) * 100;
            RefactoringSession.this.fireProgressListenerStep((int)this.current);
        }

        @Override
        public void step(ProgressEvent event) {
            this.current += this.progressStep;
            RefactoringSession.this.fireProgressListenerStep((int)this.current);
        }

        @Override
        public void stop(ProgressEvent event) {
        }
    }

}

