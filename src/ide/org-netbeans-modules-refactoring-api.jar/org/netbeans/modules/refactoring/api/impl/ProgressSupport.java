/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.api.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;

public final class ProgressSupport {
    private final List<ProgressListener> progressListenerList = new ArrayList<ProgressListener>();
    private int counter;
    private boolean deterministic;

    public boolean isEmpty() {
        return this.progressListenerList.isEmpty();
    }

    public synchronized void addProgressListener(ProgressListener listener) {
        this.progressListenerList.add(listener);
    }

    public synchronized void removeProgressListener(ProgressListener listener) {
        this.progressListenerList.remove(listener);
    }

    public void fireProgressListenerStart(Object source, int type, int count) {
        ProgressListener[] listeners;
        this.counter = -1;
        this.deterministic = count > 0;
        ProgressEvent event = new ProgressEvent(source, 1, type, count);
        for (ProgressListener listener : listeners = this.getListenersCopy()) {
            try {
                listener.start(event);
                continue;
            }
            catch (RuntimeException e) {
                this.log(e);
            }
        }
    }

    public void fireProgressListenerStart(int type, int count) {
        this.fireProgressListenerStart(this, type, count);
    }

    public void fireProgressListenerStep(Object source, int count) {
        ProgressListener[] listeners;
        if (this.deterministic) {
            if (count < 0) {
                this.deterministic = false;
            }
            this.counter = count;
        } else if (count > 0) {
            this.deterministic = true;
            this.counter = -1;
        } else {
            this.counter = count;
        }
        ProgressEvent event = new ProgressEvent(source, 2, 0, count);
        for (ProgressListener listener : listeners = this.getListenersCopy()) {
            try {
                listener.step(event);
                continue;
            }
            catch (RuntimeException e) {
                this.log(e);
            }
        }
    }

    public void fireProgressListenerStep(Object source) {
        if (this.deterministic) {
            ++this.counter;
        }
        this.fireProgressListenerStep(source, this.counter);
    }

    public void fireProgressListenerStop(Object source) {
        ProgressListener[] listeners;
        ProgressEvent event = new ProgressEvent(source, 4);
        for (ProgressListener listener : listeners = this.getListenersCopy()) {
            try {
                listener.stop(event);
                continue;
            }
            catch (RuntimeException e) {
                this.log(e);
            }
        }
    }

    public void fireProgressListenerStop() {
        this.fireProgressListenerStop(this);
    }

    private synchronized ProgressListener[] getListenersCopy() {
        return this.progressListenerList.toArray(new ProgressListener[this.progressListenerList.size()]);
    }

    private void log(Exception e) {
        Logger.getLogger(ProgressSupport.class.getName()).log(Level.INFO, e.getMessage(), e);
    }
}

