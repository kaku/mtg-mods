/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.api.impl;

import java.util.Collection;
import javax.swing.undo.CannotUndoException;

public class CannotUndoRefactoring
extends CannotUndoException {
    private Collection<String> files;

    public CannotUndoRefactoring(Collection<String> checkChecksum) {
        this.files = checkChecksum;
    }

    @Override
    public String getMessage() {
        StringBuilder b = new StringBuilder("Cannot Undo.\nFollowing files were modified:\n");
        for (String f : this.files) {
            b.append(f);
            b.append('\n');
        }
        return b.toString();
    }

    public Collection<String> getFiles() {
        return this.files;
    }
}

