/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ui.OpenProjects
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.api.impl;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.modules.refactoring.spi.impl.CopyAction;
import org.netbeans.modules.refactoring.spi.ui.ActionsImplementationProvider;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class ActionsImplementationFactory {
    private static final Logger LOG = Logger.getLogger(ActionsImplementationFactory.class.getName());
    private static final Lookup.Result<ActionsImplementationProvider> implementations = Lookup.getDefault().lookupResult(ActionsImplementationProvider.class);

    private ActionsImplementationFactory() {
    }

    public static boolean canRename(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            if (!rafi.canRename(lookup)) continue;
            return true;
        }
        return false;
    }

    public static void doRename(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            boolean canRename = rafi.canRename(lookup);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(String.format("canRename: %s, %s", rafi, canRename));
            }
            if (!canRename) continue;
            rafi.doRename(lookup);
            return;
        }
        ActionsImplementationFactory.notifyOutOfContext("LBL_RenameRefactoring", lookup);
    }

    public static boolean canFindUsages(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            if (!rafi.canFindUsages(lookup)) continue;
            return true;
        }
        return false;
    }

    public static void doFindUsages(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            boolean canFindUsages = rafi.canFindUsages(lookup);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(String.format("canFindUsages: %s, %s", rafi, canFindUsages));
            }
            if (!canFindUsages) continue;
            rafi.doFindUsages(lookup);
            return;
        }
        ActionsImplementationFactory.notifyOutOfContext("LBL_FindUsagesRefactoring", lookup);
    }

    public static boolean canDelete(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            if (!rafi.canDelete(lookup)) continue;
            return true;
        }
        return false;
    }

    public static void doDelete(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            boolean canDelete = rafi.canDelete(lookup);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(String.format("canDelete: %s, %s", rafi, canDelete));
            }
            if (!canDelete) continue;
            rafi.doDelete(lookup);
            return;
        }
        ActionsImplementationFactory.notifyOutOfContext("LBL_SafeDeleteRefactoring", lookup);
    }

    public static void doMove(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            boolean canMove = rafi.canMove(lookup);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(String.format("canMove: %s, %s", rafi, canMove));
            }
            if (!canMove) continue;
            rafi.doMove(lookup);
            return;
        }
        ActionsImplementationFactory.notifyOutOfContext("LBL_MoveRefactoring", lookup);
    }

    public static boolean canMove(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            if (!rafi.canMove(lookup)) continue;
            return true;
        }
        return false;
    }

    public static void doCopy(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            boolean canCopy = rafi.canCopy(lookup);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(String.format("canCopy: %s, %s", rafi, canCopy));
            }
            if (!canCopy) continue;
            rafi.doCopy(lookup);
            return;
        }
        ActionsImplementationFactory.notifyOutOfContext("LBL_CopyRefactoring", lookup);
    }

    public static boolean canCopy(Lookup lookup) {
        for (ActionsImplementationProvider rafi : implementations.allInstances()) {
            if (!rafi.canCopy(lookup)) continue;
            return true;
        }
        return false;
    }

    private static void notifyOutOfContext(String refactoringNameKey, Lookup context) {
        for (Node node : context.lookupAll(Node.class)) {
            for (FileObject file : node.getLookup().lookupAll(FileObject.class)) {
                if (ActionsImplementationFactory.isFileInOpenProject(file)) continue;
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(CopyAction.class, (String)"ERR_ProjectNotOpened", (Object)file.getNameExt())));
                return;
            }
        }
        String refactoringName = NbBundle.getMessage(CopyAction.class, (String)refactoringNameKey);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(CopyAction.class, (String)"MSG_CantApplyRefactoring", (Object)refactoringName)));
    }

    private static boolean isFileInOpenProject(FileObject file) {
        assert (file != null);
        Project p = FileOwnerQuery.getOwner((FileObject)file);
        if (p == null) {
            return false;
        }
        return ActionsImplementationFactory.isOpenProject(p);
    }

    private static boolean isOpenProject(Project p) {
        return OpenProjects.getDefault().isProjectOpen(p);
    }
}

