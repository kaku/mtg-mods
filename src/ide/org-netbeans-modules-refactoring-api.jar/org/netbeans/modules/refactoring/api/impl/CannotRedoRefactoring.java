/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.api.impl;

import java.util.Collection;
import javax.swing.undo.CannotRedoException;

public class CannotRedoRefactoring
extends CannotRedoException {
    private Collection<String> files;

    public CannotRedoRefactoring(Collection<String> checkChecksum) {
        this.files = checkChecksum;
    }

    public Collection<String> getFiles() {
        return this.files;
    }

    @Override
    public String getMessage() {
        StringBuilder b = new StringBuilder("Cannot Redo.\nFollowing files were modified:\n");
        for (String f : this.files) {
            b.append(f);
            b.append('\n');
        }
        return b.toString();
    }
}

