/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.text.PositionBounds
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.spi.FiltersManager;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.openide.filesystems.FileObject;
import org.openide.text.PositionBounds;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class RefactoringElement {
    public static final int NORMAL = 0;
    public static final int WARNING = 1;
    public static final int GUARDED = 2;
    public static final int READ_ONLY = 3;
    final RefactoringElementImplementation impl;

    RefactoringElement(RefactoringElementImplementation impl) {
        Parameters.notNull((CharSequence)"impl", (Object)impl);
        this.impl = impl;
    }

    @NonNull
    public String getText() {
        return this.impl.getText();
    }

    @NonNull
    public String getDisplayText() {
        return this.impl.getDisplayText();
    }

    public boolean isEnabled() {
        return this.impl.isEnabled();
    }

    public void setEnabled(boolean enabled) {
        this.impl.setEnabled(enabled);
    }

    @NonNull
    public Lookup getLookup() {
        return this.impl.getLookup();
    }

    public FileObject getParentFile() {
        return this.impl.getParentFile();
    }

    public PositionBounds getPosition() {
        return this.impl.getPosition();
    }

    public int getStatus() {
        return this.impl.getStatus();
    }

    public void showPreview() {
        this.impl.showPreview();
    }

    public void openInEditor() {
        this.impl.openInEditor();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        RefactoringElement other = (RefactoringElement)obj;
        if (!(this.impl == other.impl || this.impl != null && this.impl.equals(other.impl))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.impl != null ? this.impl.hashCode() : 0);
        return hash;
    }

    public boolean include(FiltersManager filtersManager) {
        if (this.impl instanceof FiltersManager.Filterable) {
            FiltersManager.Filterable filterable = (FiltersManager.Filterable)((Object)this.impl);
            return filterable.filter(filtersManager);
        }
        return true;
    }
}

