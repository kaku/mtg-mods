/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class SingleCopyRefactoring
extends AbstractRefactoring {
    private Lookup target;
    private String newName;

    public SingleCopyRefactoring(@NonNull Lookup objectToCopy) {
        super(objectToCopy);
    }

    public void setTarget(@NonNull Lookup target) {
        Parameters.notNull((CharSequence)"target", (Object)target);
        this.target = target;
    }

    @CheckForNull
    public Lookup getTarget() {
        return this.target;
    }

    @CheckForNull
    public String getNewName() {
        return this.newName;
    }

    public void setNewName(@NonNull String newName) {
        Parameters.notNull((CharSequence)"newName", (Object)newName);
        this.newName = newName;
    }
}

