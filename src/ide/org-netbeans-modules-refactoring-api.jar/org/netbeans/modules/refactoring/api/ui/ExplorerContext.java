/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package org.netbeans.modules.refactoring.api.ui;

import java.awt.datatransfer.Transferable;
import org.openide.nodes.Node;

public final class ExplorerContext {
    private Node targetNode;
    private Transferable transferable;
    private String newName;
    private boolean isDelete;

    public Node getTargetNode() {
        return this.targetNode;
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isDelete() {
        return this.isDelete;
    }

    public void setTargetNode(Node targetNode) {
        this.targetNode = targetNode;
    }

    public Transferable getTransferable() {
        return this.transferable;
    }

    public void setTransferable(Transferable transferable) {
        this.transferable = transferable;
    }

    public String getNewName() {
        return this.newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}

