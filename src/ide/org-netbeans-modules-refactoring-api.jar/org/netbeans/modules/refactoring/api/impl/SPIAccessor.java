/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.api.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.Transaction;

public abstract class SPIAccessor {
    public static SPIAccessor DEFAULT;

    public abstract RefactoringElementsBag createBag(RefactoringSession var1, List var2);

    public abstract Collection getReadOnlyFiles(RefactoringElementsBag var1);

    public abstract ArrayList<Transaction> getCommits(RefactoringElementsBag var1);

    public abstract ArrayList<RefactoringElementImplementation> getFileChanges(RefactoringElementsBag var1);

    public abstract String getNewFileContent(SimpleRefactoringElementImplementation var1);

    public abstract boolean hasChangesInGuardedBlocks(RefactoringElementsBag var1);

    public abstract boolean hasChangesInReadOnlyFiles(RefactoringElementsBag var1);

    public abstract void check(Transaction var1, boolean var2);

    public abstract void sum(Transaction var1);

    static {
        Class<RefactoringElementsBag> c = RefactoringElementsBag.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

