/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Cancellable
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import javax.swing.Action;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.spi.ProblemDetailsImplementation;
import org.openide.util.Cancellable;
import org.openide.util.Parameters;

public final class ProblemDetails {
    private ProblemDetailsImplementation pdi;

    ProblemDetails(ProblemDetailsImplementation pdi) {
        this.pdi = pdi;
    }

    public void showDetails(@NonNull Action rerunRefactoringAction, @NonNull Cancellable parent) {
        Parameters.notNull((CharSequence)"rerunRefactoringAction", (Object)rerunRefactoringAction);
        Parameters.notNull((CharSequence)"parent", (Object)parent);
        this.pdi.showDetails(rerunRefactoringAction, parent);
    }

    @NonNull
    public String getDetailsHint() {
        return this.pdi.getDetailsHint();
    }
}

