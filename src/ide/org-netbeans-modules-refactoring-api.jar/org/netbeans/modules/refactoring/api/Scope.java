/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.fileinfo.NonRecursiveFolder
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.refactoring.api;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.openide.filesystems.FileObject;

public final class Scope {
    private final Set<FileObject> sourceRoots = new HashSet<FileObject>();
    private final Set<NonRecursiveFolder> folders = new HashSet<NonRecursiveFolder>();
    private final Set<FileObject> files = new HashSet<FileObject>();

    private Scope() {
    }

    @NonNull
    public Set<FileObject> getFiles() {
        return Collections.unmodifiableSet(this.files);
    }

    @NonNull
    public Set<NonRecursiveFolder> getFolders() {
        return Collections.unmodifiableSet(this.folders);
    }

    @NonNull
    public Set<FileObject> getSourceRoots() {
        return Collections.unmodifiableSet(this.sourceRoots);
    }

    @NonNull
    public static Scope create(@NullAllowed Collection<FileObject> sourceRoots, @NullAllowed Collection<NonRecursiveFolder> folders, @NullAllowed Collection<FileObject> files) {
        Scope scope = new Scope();
        if (files != null) {
            scope.files.addAll(files);
        }
        if (folders != null) {
            scope.folders.addAll(folders);
        }
        if (sourceRoots != null) {
            scope.sourceRoots.addAll(sourceRoots);
        }
        return scope;
    }
}

