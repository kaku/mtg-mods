/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import java.util.EventObject;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Parameters;

public final class ProgressEvent
extends EventObject {
    public static final int START = 1;
    public static final int STEP = 2;
    public static final int STOP = 4;
    private final int eventId;
    private final int operationType;
    private final int count;

    public ProgressEvent(@NonNull Object source, int eventId) {
        this(source, eventId, 0, 0);
    }

    public ProgressEvent(@NonNull Object source, int eventId, int operationType, int count) {
        super(source);
        Parameters.notNull((CharSequence)"source", (Object)source);
        this.eventId = eventId;
        this.operationType = operationType;
        this.count = count;
    }

    public int getEventId() {
        return this.eventId;
    }

    public int getOperationType() {
        return this.operationType;
    }

    public int getCount() {
        return this.count;
    }
}

