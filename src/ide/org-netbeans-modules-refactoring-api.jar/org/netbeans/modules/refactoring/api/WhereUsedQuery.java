/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import java.util.Hashtable;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class WhereUsedQuery
extends AbstractRefactoring {
    public static final String SEARCH_IN_COMMENTS = "SEARCH_IN_COMMENTS";
    public static final String FIND_REFERENCES = "FIND_REFERENCES";
    private Hashtable hash = new Hashtable();

    public WhereUsedQuery(@NonNull Lookup lookup) {
        super(lookup);
        this.putValue("FIND_REFERENCES", true);
    }

    public final void setRefactoringSource(@NonNull Lookup lookup) {
        Parameters.notNull((CharSequence)"lookup", (Object)lookup);
        this.refactoringSource = lookup;
    }

    public final boolean getBooleanValue(@NonNull Object key) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        Object o = this.hash.get(key);
        if (o instanceof Boolean) {
            return (Boolean)o;
        }
        return false;
    }

    public final void putValue(@NonNull Object key, Object value) {
        Parameters.notNull((CharSequence)"key", (Object)key);
        this.hash.put(key, value);
    }
}

