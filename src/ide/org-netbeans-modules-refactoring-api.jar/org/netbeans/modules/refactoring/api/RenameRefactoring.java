/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.openide.util.Lookup;
import org.openide.util.Parameters;

public final class RenameRefactoring
extends AbstractRefactoring {
    private String newName = null;
    private boolean searchInComments;

    public RenameRefactoring(@NonNull Lookup item) {
        super(item);
    }

    @CheckForNull
    public String getNewName() {
        return this.newName;
    }

    public void setNewName(@NonNull String newName) {
        Parameters.notNull((CharSequence)"newName", (Object)newName);
        this.newName = newName;
    }

    public boolean isSearchInComments() {
        return this.searchInComments;
    }

    public void setSearchInComments(boolean searchInComments) {
        this.searchInComments = searchInComments;
    }
}

