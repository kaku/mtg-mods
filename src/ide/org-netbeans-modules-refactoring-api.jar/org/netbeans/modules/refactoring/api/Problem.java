/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.ProblemDetails;
import org.openide.util.Parameters;

public final class Problem {
    private final boolean fatal;
    private final String message;
    private Problem next = null;
    private ProblemDetails details;

    public Problem(boolean fatal, @NonNull String message) {
        this(fatal, message, null);
    }

    public Problem(boolean fatal, @NonNull String message, ProblemDetails details) {
        Parameters.notNull((CharSequence)"message", (Object)message);
        this.fatal = fatal;
        this.message = message;
        this.details = details;
    }

    public boolean isFatal() {
        return this.fatal;
    }

    @NonNull
    public String getMessage() {
        return this.message;
    }

    @CheckForNull
    public Problem getNext() {
        return this.next;
    }

    public void setNext(@NonNull Problem next) throws IllegalStateException {
        Parameters.notNull((CharSequence)"next", (Object)next);
        if (this.next != null) {
            throw new IllegalStateException("Cannot change \"next\" property of Problem.");
        }
        this.next = next;
    }

    @CheckForNull
    public ProblemDetails getDetails() {
        return this.details;
    }
}

