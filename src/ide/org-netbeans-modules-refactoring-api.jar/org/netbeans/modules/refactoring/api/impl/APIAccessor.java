/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.api.impl;

import java.util.Collection;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProblemDetails;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.GuardedBlockHandler;
import org.netbeans.modules.refactoring.spi.ProblemDetailsImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.ui.FiltersDescription;

public abstract class APIAccessor {
    public static APIAccessor DEFAULT;

    public abstract Collection<GuardedBlockHandler> getGBHandlers(AbstractRefactoring var1);

    public abstract Problem chainProblems(Problem var1, Problem var2);

    public abstract ProblemDetails createProblemDetails(ProblemDetailsImplementation var1);

    public abstract boolean isCommit(RefactoringSession var1);

    public abstract RefactoringElementImplementation getRefactoringElementImplementation(RefactoringElement var1);

    public abstract boolean hasPluginsWithProgress(AbstractRefactoring var1);

    public abstract boolean hasChangesInGuardedBlocks(RefactoringSession var1);

    public abstract boolean hasChangesInReadOnlyFiles(RefactoringSession var1);

    public abstract FiltersDescription getFiltersDescription(AbstractRefactoring var1);

    public abstract boolean isFinished(RefactoringSession var1);

    static {
        Class<AbstractRefactoring> c = AbstractRefactoring.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

