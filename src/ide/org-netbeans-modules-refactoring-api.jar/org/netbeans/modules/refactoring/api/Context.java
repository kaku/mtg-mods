/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.Parameters
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class Context
extends Lookup {
    private InstanceContent instanceContent;
    private AbstractLookup delegate;

    Context(InstanceContent instanceContent) {
        this.delegate = new AbstractLookup((AbstractLookup.Content)instanceContent);
        this.instanceContent = instanceContent;
    }

    public void add(@NonNull Object value) {
        Parameters.notNull((CharSequence)"value", (Object)value);
        this.remove(value.getClass());
        this.instanceContent.add(value);
    }

    public void remove(@NonNull Class<?> clazz) {
        Parameters.notNull((CharSequence)"clazz", clazz);
        Object old = this.lookup(clazz);
        if (old != null) {
            this.instanceContent.remove(old);
        }
    }

    public <T> T lookup(Class<T> clazz) {
        return (T)this.delegate.lookup(clazz);
    }

    public <T> Lookup.Result<T> lookup(Lookup.Template<T> template) {
        return this.delegate.lookup(template);
    }
}

