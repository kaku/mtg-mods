/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.queries.SharabilityQuery
 *  org.netbeans.api.queries.SharabilityQuery$Sharability
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.modules.ModuleInfo
 *  org.openide.modules.Modules
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 *  org.openide.util.Parameters
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.refactoring.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.queries.SharabilityQuery;
import org.netbeans.modules.refactoring.api.AccessorImpl;
import org.netbeans.modules.refactoring.api.Context;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.WhereUsedQuery;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.api.impl.ProgressSupport;
import org.netbeans.modules.refactoring.api.impl.SPIAccessor;
import org.netbeans.modules.refactoring.spi.GuardedBlockHandler;
import org.netbeans.modules.refactoring.spi.GuardedBlockHandlerFactory;
import org.netbeans.modules.refactoring.spi.ProgressProvider;
import org.netbeans.modules.refactoring.spi.ReadOnlyFilesHandler;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;
import org.netbeans.modules.refactoring.spi.RefactoringPluginFactory;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.netbeans.modules.refactoring.spi.impl.Util;
import org.netbeans.modules.refactoring.spi.ui.FiltersDescription;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.modules.ModuleInfo;
import org.openide.modules.Modules;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Parameters;
import org.openide.util.lookup.InstanceContent;

public abstract class AbstractRefactoring {
    public static final int INIT = 0;
    public static final int PRE_CHECK = 1;
    public static final int PARAMETERS_CHECK = 2;
    public static final int PREPARE = 3;
    private int currentState = 0;
    private static final int PLUGIN_STEPS = 30;
    private ArrayList plugins;
    private FiltersDescription filtersDescription;
    ArrayList pluginsWithProgress;
    private ArrayList gbHandlers;
    private ProgressL progressListener;
    private ProgressSupport progressSupport;
    Lookup refactoringSource;
    private Context scope;
    private final AtomicBoolean cancel;

    protected AbstractRefactoring(Lookup refactoringSource) {
        this.progressListener = new ProgressL();
        this.cancel = new AtomicBoolean();
        Parameters.notNull((CharSequence)"refactoringSource", (Object)refactoringSource);
        this.refactoringSource = refactoringSource;
    }

    private Collection getPlugins() {
        if (this.plugins == null) {
            this.plugins = new ArrayList();
            this.filtersDescription = new FiltersDescription();
            Lookup.Result result = Lookup.getDefault().lookup(new Lookup.Template(RefactoringPluginFactory.class));
            for (RefactoringPluginFactory factory : result.allInstances()) {
                RefactoringPlugin plugin = factory.createInstance(this);
                if (plugin == null) continue;
                RefactoringPlugin callerPlugin = this.getContext().lookup(RefactoringPlugin.class);
                AbstractRefactoring caller = this.getContext().lookup(AbstractRefactoring.class);
                if (caller != null && !factory.getClass().getClassLoader().equals(callerPlugin.getClass().getClassLoader()) && factory.createInstance(caller) != null) continue;
                this.plugins.add(plugin);
                if (!(plugin instanceof FiltersDescription.Provider)) continue;
                FiltersDescription.Provider prov = (FiltersDescription.Provider)((Object)plugin);
                prov.addFilters(this.filtersDescription);
            }
        }
        return this.plugins;
    }

    FiltersDescription getFiltersDescription() {
        return this.filtersDescription;
    }

    Collection getGBHandlers() {
        if (this.gbHandlers == null) {
            this.gbHandlers = new ArrayList();
            Lookup.Result result = Lookup.getDefault().lookup(new Lookup.Template(GuardedBlockHandlerFactory.class));
            Iterator it = result.allInstances().iterator();
            while (it.hasNext()) {
                GuardedBlockHandler handler = ((GuardedBlockHandlerFactory)it.next()).createInstance(this);
                if (handler == null) continue;
                this.gbHandlers.add(handler);
            }
        }
        return this.gbHandlers;
    }

    @CheckForNull
    public final Problem preCheck() {
        this.currentState = 1;
        return this.pluginsPreCheck(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @CheckForNull
    public final Problem prepare(@NonNull RefactoringSession session) {
        try {
            Parameters.notNull((CharSequence)"session", (Object)session);
            long time = System.currentTimeMillis();
            Problem p = null;
            boolean checkCalled = false;
            if (this.currentState < 2) {
                p = this.checkParameters();
                checkCalled = true;
            }
            if (p != null && p.isFatal()) {
                Problem problem = p;
                return problem;
            }
            p = this.pluginsPrepare(checkCalled ? p : null, session);
            Logger timer = Logger.getLogger("TIMER.RefactoringPrepare");
            if (timer.isLoggable(Level.FINE)) {
                time = System.currentTimeMillis() - time;
                timer.log(Level.FINE, "refactoring.prepare", new Object[]{this, time});
            }
            Problem problem = p;
            return problem;
        }
        finally {
            session.finished();
        }
    }

    @CheckForNull
    public final Problem checkParameters() {
        Problem p = this.fastCheckParameters();
        if (p != null && p.isFatal()) {
            return p;
        }
        this.currentState = 2;
        return this.pluginsCheckParams(p);
    }

    @CheckForNull
    public final Problem fastCheckParameters() {
        Problem p = null;
        if (this.currentState < 1) {
            p = this.preCheck();
        }
        if (p != null && p.isFatal()) {
            return p;
        }
        return this.pluginsFastCheckParams(p);
    }

    public final synchronized void addProgressListener(@NonNull ProgressListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        if (this.progressSupport == null) {
            this.progressSupport = new ProgressSupport();
        }
        this.progressSupport.addProgressListener(listener);
        if (this.pluginsWithProgress == null) {
            this.pluginsWithProgress = new ArrayList();
            for (RefactoringPlugin plugin : this.getPlugins()) {
                if (!(plugin instanceof ProgressProvider)) continue;
                ((ProgressProvider)((Object)plugin)).addProgressListener(this.progressListener);
                this.pluginsWithProgress.add(plugin);
            }
        }
    }

    public final synchronized void removeProgressListener(@NonNull ProgressListener listener) {
        Parameters.notNull((CharSequence)"listener", (Object)listener);
        if (this.progressSupport != null) {
            this.progressSupport.removeProgressListener(listener);
        }
        if (this.pluginsWithProgress != null && this.progressSupport != null && this.progressSupport.isEmpty()) {
            for (ProgressProvider plugin : this.pluginsWithProgress) {
                plugin.removeProgressListener(this.progressListener);
            }
            this.pluginsWithProgress.clear();
            this.pluginsWithProgress = null;
        }
    }

    @NonNull
    public final Context getContext() {
        if (this.scope == null) {
            this.scope = new Context(new InstanceContent());
        }
        return this.scope;
    }

    @NonNull
    public final Lookup getRefactoringSource() {
        return this.refactoringSource;
    }

    public final void cancelRequest() {
        this.cancel.set(true);
        for (RefactoringPlugin plugin : this.getPlugins()) {
            plugin.cancelRequest();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Problem pluginsPreCheck(Problem problem) {
        try {
            Object plugin;
            this.cancel.set(false);
            this.progressListener.start();
            Iterator pIt = this.getPlugins().iterator();
            while (pIt.hasNext()) {
                if (this.cancel.get()) {
                    Problem problem2 = null;
                    return problem2;
                }
                plugin = (RefactoringPlugin)pIt.next();
                try {
                    problem = AbstractRefactoring.chainProblems(plugin.preCheck(), problem);
                }
                catch (Throwable t2) {
                    problem = this.createProblemAndLog(problem, t2, plugin.getClass());
                }
                if (problem == null || !problem.isFatal()) continue;
                Problem t2 = problem;
                return t2;
            }
            plugin = problem;
            return plugin;
        }
        finally {
            this.progressListener.stop();
        }
    }

    private String getModuleName(Class<?> c) {
        ModuleInfo info = Modules.getDefault().ownerOf(c);
        return info != null ? info.getDisplayName() : "Unknown";
    }

    private String createMessage(Class c, Throwable t) {
        return NbBundle.getMessage(RefactoringPanel.class, (String)"ERR_ExceptionInModule", (Object)this.getModuleName(c), (Object)t.toString());
    }

    private Problem createProblemAndLog(Problem p, Throwable t, Class source) {
        Problem newProblem;
        Throwable cause = t.getCause();
        if (cause != null && (cause.getClass().getName().equals("org.netbeans.api.java.source.JavaSource$InsufficientMemoryException") || cause.getCause() != null && cause.getCause().getClass().getName().equals("org.netbeans.api.java.source.JavaSource$InsufficientMemoryException"))) {
            newProblem = new Problem(true, NbBundle.getMessage(Util.class, (String)"ERR_OutOfMemory"));
            Logger.global.log(Level.INFO, "There is not enough memory to complete this task.", t);
        } else {
            newProblem = new Problem(false, this.createMessage(source, t));
            Exceptions.printStackTrace((Throwable)t);
        }
        return AbstractRefactoring.chainProblems(newProblem, p);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Problem pluginsPrepare(Problem problem, RefactoringSession session) {
        try {
            this.progressListener.start();
            Problem problem2 = this.pluginsPrepare2(problem, session);
            return problem2;
        }
        finally {
            this.progressListener.stop();
        }
    }

    private Problem pluginsPrepare2(Problem problem, RefactoringSession session) {
        ReadOnlyFilesHandler handler;
        RefactoringElementsBag elements = session.getElementsBag();
        Iterator pIt = this.getPlugins().iterator();
        while (pIt.hasNext()) {
            if (this.cancel.get()) {
                return null;
            }
            RefactoringPlugin plugin = (RefactoringPlugin)pIt.next();
            try {
                problem = AbstractRefactoring.chainProblems(plugin.prepare(elements), problem);
            }
            catch (Throwable t) {
                problem = this.createProblemAndLog(problem, t, plugin.getClass());
            }
            if (problem != null && problem.isFatal()) {
                return problem;
            }
            if (!(plugin instanceof FiltersDescription.Provider)) continue;
            FiltersDescription.Provider provider = (FiltersDescription.Provider)((Object)plugin);
            provider.enableFilters(this.filtersDescription);
        }
        if (!(this instanceof WhereUsedQuery) && (handler = this.getROHandler()) != null) {
            Collection files = SPIAccessor.DEFAULT.getReadOnlyFiles(elements);
            HashSet<FileObject> allFiles = new HashSet<FileObject>();
            for (FileObject f : files) {
                try {
                    DataObject dob = DataObject.find((FileObject)f);
                    for (FileObject file : dob.files()) {
                        if (SharabilityQuery.getSharability((FileObject)file) != SharabilityQuery.Sharability.SHARABLE) continue;
                        allFiles.add(file);
                    }
                    continue;
                }
                catch (DataObjectNotFoundException e) {
                    allFiles.add(f);
                    continue;
                }
            }
            problem = AbstractRefactoring.chainProblems(handler.createProblem(session, allFiles), problem);
        }
        return problem;
    }

    private ReadOnlyFilesHandler getROHandler() {
        Lookup.Result result = Lookup.getDefault().lookup(new Lookup.Template(ReadOnlyFilesHandler.class));
        List handlers = (List)result.allInstances();
        if (handlers.isEmpty()) {
            return null;
        }
        if (handlers.size() > 1) {
            ErrorManager.getDefault().log(16, "Multiple instances of ReadOnlyFilesHandler found in Lookup; only using first one: " + handlers);
        }
        return (ReadOnlyFilesHandler)handlers.get(0);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Problem pluginsCheckParams(Problem problem) {
        try {
            Object plugin;
            this.progressListener.start();
            Iterator pIt = this.getPlugins().iterator();
            while (pIt.hasNext()) {
                if (this.cancel.get()) {
                    Problem problem2 = null;
                    return problem2;
                }
                plugin = (RefactoringPlugin)pIt.next();
                try {
                    problem = AbstractRefactoring.chainProblems(plugin.checkParameters(), problem);
                }
                catch (Throwable t2) {
                    problem = this.createProblemAndLog(problem, t2, plugin.getClass());
                }
                if (problem == null || !problem.isFatal()) continue;
                Problem t2 = problem;
                return t2;
            }
            plugin = problem;
            return plugin;
        }
        finally {
            this.progressListener.stop();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Problem pluginsFastCheckParams(Problem problem) {
        try {
            Object plugin;
            this.progressListener.start();
            Iterator pIt = this.getPlugins().iterator();
            while (pIt.hasNext()) {
                if (this.cancel.get()) {
                    Problem problem2 = null;
                    return problem2;
                }
                plugin = (RefactoringPlugin)pIt.next();
                try {
                    problem = AbstractRefactoring.chainProblems(plugin.fastCheckParameters(), problem);
                }
                catch (Throwable t2) {
                    problem = this.createProblemAndLog(problem, t2, plugin.getClass());
                }
                if (problem == null || !problem.isFatal()) continue;
                Problem t2 = problem;
                return t2;
            }
            plugin = problem;
            return plugin;
        }
        finally {
            this.progressListener.stop();
        }
    }

    static Problem chainProblems(Problem p, Problem p1) {
        if (p == null) {
            return p1;
        }
        if (p1 == null) {
            return p;
        }
        Problem problem = p;
        while (problem.getNext() != null) {
            problem = problem.getNext();
        }
        problem.setNext(p1);
        return p;
    }

    static {
        APIAccessor.DEFAULT = new AccessorImpl();
    }

    private class ProgressL
    implements ProgressListener {
        private float progressStep;
        private float current;
        private boolean startCalledByPlugin;

        private ProgressL() {
            this.startCalledByPlugin = false;
        }

        @Override
        public void start(ProgressEvent event) {
            this.progressStep = 30.0f / (float)event.getCount();
            this.startCalledByPlugin |= true;
            if (AbstractRefactoring.this.pluginsWithProgress.indexOf(event.getSource()) == 0) {
                this.current = 0.0f;
                if (event.getCount() == -1) {
                    this.fireProgressListenerStart(event.getOperationType(), -1);
                    this.progressStep = -1.0f;
                } else {
                    this.fireProgressListenerStart(event.getOperationType(), 30 * AbstractRefactoring.this.pluginsWithProgress.size());
                }
            } else {
                this.current = AbstractRefactoring.this.pluginsWithProgress.indexOf(event.getSource()) * 30;
                this.fireProgressListenerStep((int)this.current);
            }
        }

        @Override
        public void step(ProgressEvent event) {
            if (this.progressStep < 0.0f) {
                int size = event.getCount();
                if (size > 0) {
                    this.progressStep = 30.0f / (float)size;
                    this.fireProgressListenerStep(30 * AbstractRefactoring.this.pluginsWithProgress.size());
                } else {
                    this.fireProgressListenerStep();
                }
            } else {
                this.current += this.progressStep;
                this.fireProgressListenerStep((int)this.current);
            }
        }

        @Override
        public void stop(ProgressEvent event) {
        }

        void start() {
            this.startCalledByPlugin = false;
        }

        void stop() {
            if (this.startCalledByPlugin) {
                this.fireProgressListenerStop();
            }
        }

        private void fireProgressListenerStart(int type, int count) {
            if (AbstractRefactoring.this.progressSupport != null) {
                AbstractRefactoring.this.progressSupport.fireProgressListenerStart(this, type, count);
            }
        }

        private void fireProgressListenerStep() {
            if (AbstractRefactoring.this.progressSupport != null) {
                AbstractRefactoring.this.progressSupport.fireProgressListenerStep(this);
            }
        }

        private void fireProgressListenerStep(int count) {
            if (AbstractRefactoring.this.progressSupport != null) {
                AbstractRefactoring.this.progressSupport.fireProgressListenerStep(this, count);
            }
        }

        private void fireProgressListenerStop() {
            if (AbstractRefactoring.this.progressSupport != null) {
                AbstractRefactoring.this.progressSupport.fireProgressListenerStop(this);
            }
        }
    }

}

