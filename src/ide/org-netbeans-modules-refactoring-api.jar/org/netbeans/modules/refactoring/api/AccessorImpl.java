/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.api;

import java.util.ArrayList;
import java.util.Collection;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProblemDetails;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.api.impl.SPIAccessor;
import org.netbeans.modules.refactoring.spi.GuardedBlockHandler;
import org.netbeans.modules.refactoring.spi.ProblemDetailsImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.ui.FiltersDescription;

final class AccessorImpl
extends APIAccessor {
    AccessorImpl() {
    }

    @Override
    public Collection<GuardedBlockHandler> getGBHandlers(AbstractRefactoring refactoring) {
        assert (refactoring != null);
        return refactoring.getGBHandlers();
    }

    @Override
    public boolean hasPluginsWithProgress(AbstractRefactoring refactoring) {
        return refactoring.pluginsWithProgress != null && !refactoring.pluginsWithProgress.isEmpty();
    }

    @Override
    public Problem chainProblems(Problem p, Problem p1) {
        return AbstractRefactoring.chainProblems(p, p1);
    }

    @Override
    public ProblemDetails createProblemDetails(ProblemDetailsImplementation pdi) {
        assert (pdi != null);
        return new ProblemDetails(pdi);
    }

    @Override
    public boolean isCommit(RefactoringSession session) {
        return session.realcommit;
    }

    @Override
    public RefactoringElementImplementation getRefactoringElementImplementation(RefactoringElement el) {
        return el.impl;
    }

    @Override
    public boolean hasChangesInGuardedBlocks(RefactoringSession session) {
        return SPIAccessor.DEFAULT.hasChangesInGuardedBlocks(session.getElementsBag());
    }

    @Override
    public boolean hasChangesInReadOnlyFiles(RefactoringSession session) {
        return SPIAccessor.DEFAULT.hasChangesInReadOnlyFiles(session.getElementsBag());
    }

    @Override
    public FiltersDescription getFiltersDescription(AbstractRefactoring refactoring) {
        return refactoring.getFiltersDescription();
    }

    @Override
    public boolean isFinished(RefactoringSession session) {
        return session.isFinished();
    }
}

