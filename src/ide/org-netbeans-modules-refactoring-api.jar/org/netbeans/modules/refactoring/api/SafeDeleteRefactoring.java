/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.api;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.openide.util.Lookup;

public final class SafeDeleteRefactoring
extends AbstractRefactoring {
    private boolean checkInComments;

    public SafeDeleteRefactoring(@NonNull Lookup namedElements) {
        super(namedElements);
    }

    public boolean isCheckInComments() {
        return this.checkInComments;
    }

    public void setCheckInComments(boolean checkInComments) {
        this.checkInComments = checkInComments;
    }
}

