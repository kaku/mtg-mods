/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ContextAwareAction
 */
package org.netbeans.modules.refactoring.api.ui;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.netbeans.modules.refactoring.spi.impl.CopyAction;
import org.netbeans.modules.refactoring.spi.impl.MoveAction;
import org.netbeans.modules.refactoring.spi.impl.RSMDataObjectAction;
import org.netbeans.modules.refactoring.spi.impl.RSMEditorAction;
import org.netbeans.modules.refactoring.spi.impl.RenameAction;
import org.netbeans.modules.refactoring.spi.impl.SafeDeleteAction;
import org.netbeans.modules.refactoring.spi.impl.WhereUsedAction;
import org.openide.util.ContextAwareAction;

public final class RefactoringActionsFactory {
    public static final ActionEvent DEFAULT_EVENT = new ActionEvent(new Object(), 0, null){

        @Override
        public void setSource(Object newSource) {
            throw new UnsupportedOperationException();
        }
    };

    private RefactoringActionsFactory() {
    }

    public static ContextAwareAction renameAction() {
        return (ContextAwareAction)RenameAction.findObject(RenameAction.class, (boolean)true);
    }

    public static ContextAwareAction moveAction() {
        return (ContextAwareAction)MoveAction.findObject(MoveAction.class, (boolean)true);
    }

    public static ContextAwareAction safeDeleteAction() {
        return (ContextAwareAction)SafeDeleteAction.findObject(SafeDeleteAction.class, (boolean)true);
    }

    public static ContextAwareAction copyAction() {
        return (ContextAwareAction)SafeDeleteAction.findObject(CopyAction.class, (boolean)true);
    }

    public static ContextAwareAction whereUsedAction() {
        return (ContextAwareAction)WhereUsedAction.findObject(WhereUsedAction.class, (boolean)true);
    }

    public static Action editorSubmenuAction() {
        return (Action)RSMEditorAction.findObject(RSMEditorAction.class, (boolean)true);
    }

    public static ContextAwareAction popupSubmenuAction() {
        return (ContextAwareAction)RSMDataObjectAction.findObject(RSMDataObjectAction.class, (boolean)true);
    }

}

