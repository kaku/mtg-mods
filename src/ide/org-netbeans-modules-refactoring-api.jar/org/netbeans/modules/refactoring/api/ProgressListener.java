/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.refactoring.api;

import java.util.EventListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.ProgressEvent;

public interface ProgressListener
extends EventListener {
    public void start(@NonNull ProgressEvent var1);

    public void step(@NonNull ProgressEvent var1);

    public void stop(@NonNull ProgressEvent var1);
}

