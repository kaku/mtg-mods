/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.FolderRenameHandler
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.refactoring.impl;

import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.modules.refactoring.api.impl.ActionsImplementationFactory;
import org.netbeans.modules.refactoring.api.ui.ExplorerContext;
import org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.FolderRenameHandler;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class FolderRenameHandlerImpl
implements FolderRenameHandler {
    public void handleRename(DataFolder folder, String newName) {
        InstanceContent ic = new InstanceContent();
        ic.add((Object)folder.getNodeDelegate());
        ExplorerContext d = new ExplorerContext();
        d.setNewName(newName);
        ic.add((Object)d);
        AbstractLookup l = new AbstractLookup((AbstractLookup.Content)ic);
        if (ActionsImplementationFactory.canRename((Lookup)l)) {
            SwingUtilities.invokeLater(new Runnable((Lookup)l){
                final /* synthetic */ Lookup val$l;

                @Override
                public void run() {
                    Action a = RefactoringActionsFactory.renameAction().createContextAwareInstance(this.val$l);
                    a.actionPerformed(RefactoringActionsFactory.DEFAULT_EVENT);
                }
            });
        } else {
            FileObject fo = folder.getPrimaryFile();
            try {
                folder.rename(newName);
            }
            catch (IOException ioe) {
                ErrorManager.getDefault().notify((Throwable)ioe);
            }
        }
    }

}

