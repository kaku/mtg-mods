/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.impl;

import org.netbeans.modules.refactoring.api.impl.ActionsImplementationFactory;
import org.openide.util.Lookup;

public class RefactoringWarmUpTask
implements Runnable {
    @Override
    public void run() {
        ActionsImplementationFactory.canRename(Lookup.EMPTY);
    }
}

