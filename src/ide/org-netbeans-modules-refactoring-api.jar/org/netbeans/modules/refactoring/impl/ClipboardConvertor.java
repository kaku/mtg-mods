/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeTransfer
 *  org.openide.nodes.NodeTransfer$Paste
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.datatransfer.ExClipboard
 *  org.openide.util.datatransfer.ExClipboard$Convertor
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package org.netbeans.modules.refactoring.impl;

import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.modules.refactoring.api.impl.ActionsImplementationFactory;
import org.netbeans.modules.refactoring.api.ui.ExplorerContext;
import org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory;
import org.netbeans.modules.refactoring.spi.impl.Util;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.datatransfer.ExClipboard;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class ClipboardConvertor
implements ExClipboard.Convertor {
    public Transferable convert(Transferable t) {
        InstanceContent ic;
        ExplorerContext td;
        ExTransferable tr;
        Node[] nodes = NodeTransfer.nodes((Transferable)t, (int)4);
        if (nodes != null && nodes.length > 0) {
            ic = new InstanceContent();
            for (Node n : nodes) {
                ic.add((Object)n);
            }
            td = new ExplorerContext();
            ic.add((Object)td);
            AbstractLookup l = new AbstractLookup((AbstractLookup.Content)ic);
            if (ActionsImplementationFactory.canMove((Lookup)l)) {
                Action move = RefactoringActionsFactory.moveAction().createContextAwareInstance((Lookup)l);
                tr = ExTransferable.create((Transferable)t);
                tr.put(NodeTransfer.createPaste((NodeTransfer.Paste)new RefactoringPaste(t, ic, move, td)));
                return tr;
            }
        }
        if ((nodes = NodeTransfer.nodes((Transferable)t, (int)1)) != null && nodes.length > 0) {
            ic = new InstanceContent();
            for (Node n : nodes) {
                ic.add((Object)n);
            }
            td = new ExplorerContext();
            ic.add((Object)td);
            AbstractLookup l = new AbstractLookup((AbstractLookup.Content)ic);
            if (ActionsImplementationFactory.canCopy((Lookup)l)) {
                Action copy = RefactoringActionsFactory.copyAction().createContextAwareInstance((Lookup)l);
                tr = ExTransferable.create((Transferable)t);
                tr.put(NodeTransfer.createPaste((NodeTransfer.Paste)new RefactoringPaste(t, ic, copy, td)));
                return tr;
            }
        }
        return t;
    }

    private class RefactoringPaste
    implements NodeTransfer.Paste {
        private Transferable delegate;
        private InstanceContent ic;
        private Action refactor;
        private ExplorerContext d;

        RefactoringPaste(Transferable t, InstanceContent ic, Action refactor, ExplorerContext d) {
            this.delegate = t;
            this.ic = ic;
            this.refactor = refactor;
            this.d = d;
        }

        public PasteType[] types(Node target) {
            RefactoringPasteType refactoringPaste = new RefactoringPasteType(this.delegate, target);
            if (refactoringPaste.canHandle()) {
                return new PasteType[]{refactoringPaste};
            }
            return new PasteType[0];
        }

        private class RefactoringPasteType
        extends PasteType {
            RefactoringPasteType(Transferable orig, Node target) {
                RefactoringPaste.this.d.setTargetNode(target);
                RefactoringPaste.this.d.setTransferable(orig);
            }

            public boolean canHandle() {
                if (RefactoringPaste.this.refactor == null) {
                    return false;
                }
                return (Boolean)RefactoringPaste.this.refactor.getValue("applicable");
            }

            public Transferable paste() throws IOException {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (RefactoringPaste.this.refactor != null) {
                            RefactoringPaste.this.refactor.actionPerformed(null);
                        }
                    }
                });
                return null;
            }

            public String getName() {
                return NbBundle.getMessage(Util.class, (String)"Actions/Refactoring") + " " + RefactoringPaste.this.refactor.getValue("Name");
            }

        }

    }

}

