/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.impl;

import javax.swing.Action;
import javax.swing.JMenu;
import org.netbeans.modules.refactoring.spi.impl.RefactoringSubMenuAction;

public class RefactoringMenu
extends JMenu {
    public RefactoringMenu() {
        super(new RefactoringSubMenuAction(true));
    }
}

