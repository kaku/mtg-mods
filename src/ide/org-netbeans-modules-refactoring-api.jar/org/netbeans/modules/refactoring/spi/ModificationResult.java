/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.refactoring.spi;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import org.openide.filesystems.FileObject;

public interface ModificationResult {
    public String getResultingSource(FileObject var1) throws IOException, IllegalArgumentException;

    public Collection<? extends FileObject> getModifiedFileObjects();

    public Collection<? extends File> getNewFiles();

    public void commit() throws IOException;
}

