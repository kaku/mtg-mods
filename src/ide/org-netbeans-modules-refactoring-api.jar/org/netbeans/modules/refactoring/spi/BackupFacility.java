/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;

@Deprecated
public abstract class BackupFacility {
    private static BackupFacility defaultInstance;

    private BackupFacility() {
    }

    public /* varargs */ abstract Handle backup(FileObject ... var1) throws IOException;

    public final Handle backup(Collection<? extends FileObject> fileObjects) throws IOException {
        return this.backup(fileObjects.toArray((T[])new FileObject[fileObjects.size()]));
    }

    public abstract void clear();

    public static BackupFacility getDefault() {
        BackupFacility instance = (BackupFacility)Lookup.getDefault().lookup(BackupFacility.class);
        return instance != null ? instance : BackupFacility.getDefaultInstance();
    }

    private static synchronized BackupFacility getDefaultInstance() {
        if (defaultInstance == null) {
            defaultInstance = new DefaultImpl();
        }
        return defaultInstance;
    }

    private static class DefaultImpl
    extends BackupFacility {
        private long currentId = 0;
        private Map<Long, BackupEntry> map = new HashMap<Long, BackupEntry>();

        private DefaultImpl() {
            super();
        }

        @Override
        public /* varargs */ Handle backup(FileObject ... file) throws IOException {
            ArrayList<Long> list = new ArrayList<Long>();
            for (FileObject f : file) {
                list.add(this.backup(f));
            }
            return new DefaultHandle(this, list);
        }

        public long backup(FileObject file) throws IOException {
            try {
                BackupEntry entry = new BackupEntry();
                entry.file = File.createTempFile("nbbackup", null);
                this.copy(file, entry.file);
                entry.path = file.getURL().toURI();
                this.map.put(this.currentId, entry);
                entry.file.deleteOnExit();
                return this.currentId++;
            }
            catch (URISyntaxException ex) {
                throw (IOException)new IOException(file.toString()).initCause(ex);
            }
        }

        void restore(long id) throws IOException {
            BackupEntry entry = this.map.get(id);
            if (entry == null) {
                throw new IllegalArgumentException("Backup with id " + id + "does not exist");
            }
            File backup = File.createTempFile("nbbackup", null);
            backup.deleteOnExit();
            File f = new File(entry.path);
            if (this.createNewFile(f)) {
                backup.createNewFile();
                this.copy(f, backup);
            }
            FileObject fileObj = FileUtil.toFileObject((File)f);
            this.copy(entry.file, fileObj);
            entry.file.delete();
            if (backup.exists()) {
                entry.file = backup;
            } else {
                this.map.remove(id);
            }
        }

        private boolean createNewFile(File f) throws IOException {
            if (f.exists()) {
                return true;
            }
            File parent = f.getParentFile();
            if (parent != null) {
                this.createNewFolder(parent);
            }
            FileUtil.createData((File)f);
            return false;
        }

        private void createNewFolder(File f) throws IOException {
            if (!f.exists()) {
                File parent = f.getParentFile();
                if (parent != null) {
                    this.createNewFolder(parent);
                }
                FileUtil.createFolder((File)f);
            }
        }

        private void copy(FileObject a, File b) throws IOException {
            InputStream fs = a.getInputStream();
            FileOutputStream fo = new FileOutputStream(b);
            this.copy(fs, fo);
        }

        private void copy(File a, File b) throws IOException {
            FileInputStream fs = new FileInputStream(a);
            FileOutputStream fo = new FileOutputStream(b);
            this.copy(fs, fo);
        }

        private void copy(File a, FileObject b) throws IOException {
            FileInputStream fs = new FileInputStream(a);
            OutputStream fo = b.getOutputStream();
            this.copy(fs, fo);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void copy(InputStream is, OutputStream os) throws IOException {
            try {
                FileUtil.copy((InputStream)is, (OutputStream)os);
            }
            finally {
                is.close();
                os.close();
            }
        }

        @Override
        public void clear() {
            for (BackupEntry entry : this.map.values()) {
                entry.file.delete();
            }
            this.map.clear();
        }

        private class BackupEntry {
            private File file;
            private URI path;

            private BackupEntry() {
            }
        }

    }

    private static class DefaultHandle
    implements Handle {
        List<Long> handle;
        DefaultImpl instance;

        private DefaultHandle(DefaultImpl instance, List<Long> handles) {
            this.handle = handles;
            this.instance = instance;
        }

        @Override
        public void restore() throws IOException {
            Iterator<Long> i$ = this.handle.iterator();
            while (i$.hasNext()) {
                long l = i$.next();
                this.instance.restore(l);
            }
        }
    }

    public static interface Handle {
        public void restore() throws IOException;
    }

}

