/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import javax.swing.Icon;
import org.netbeans.modules.refactoring.api.impl.ActionsImplementationFactory;
import org.netbeans.modules.refactoring.spi.impl.RefactoringGlobalAction;
import org.netbeans.modules.refactoring.spi.impl.RenameAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class CopyAction
extends RefactoringGlobalAction {
    public CopyAction() {
        super(NbBundle.getMessage(RenameAction.class, (String)"LBL_CopyAction"), null);
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    @Override
    public final void performAction(Lookup context) {
        ActionsImplementationFactory.doCopy(context);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    @Override
    protected boolean enable(Lookup context) {
        return true;
    }

    @Override
    protected boolean applicable(Lookup context) {
        return ActionsImplementationFactory.canCopy(context);
    }
}

