/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.spi.ui.ExpandableTreeElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public final class CheckNode
extends DefaultMutableTreeNode {
    public static final int SINGLE_SELECTION = 0;
    public static final int DIG_IN_SELECTION = 4;
    private int selectionMode;
    private boolean isSelected = true;
    private boolean isQuery;
    private String nodeLabel;
    private Icon icon;
    private boolean disabled = false;
    private boolean needsRefresh = false;
    private static Icon found = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/found_item_orange.png", (boolean)false);
    private String tooltip;
    private static final RequestProcessor WORKER = new RequestProcessor(CheckNode.class.getName(), 1, true, false);
    private boolean childrenFilled;

    public CheckNode(Object userObject, String nodeLabel, Icon icon, boolean isQuery) {
        super(userObject, !(userObject instanceof RefactoringElement) || userObject instanceof ExpandableTreeElement);
        this.setSelectionMode(4);
        this.isQuery = isQuery;
        this.nodeLabel = nodeLabel;
        this.icon = icon;
        if (userObject instanceof TreeElement && ((TreeElement)userObject).getUserObject() instanceof RefactoringElement) {
            RefactoringElement ree = (RefactoringElement)((TreeElement)userObject).getUserObject();
            int s = ree.getStatus();
            PositionBounds bounds = this.getPosition();
            if (isQuery && bounds != null) {
                int line = 0;
                try {
                    line = bounds.getBegin().getLine() + 1;
                }
                catch (IOException ioe) {
                    // empty catch block
                }
                this.nodeLabel = "<font color='!controlShadow'>" + this.getLineString(line, 4) + "</font>" + nodeLabel;
                if (this.icon == null) {
                    this.icon = found;
                }
            }
            if (s == 2 || s == 3) {
                this.isSelected = false;
                this.disabled = true;
                this.nodeLabel = "[<font color=#CC0000>" + NbBundle.getMessage(CheckNode.class, (String)(s == 2 ? "LBL_InGuardedBlock" : "LBL_InReadOnlyFile")) + "</font>]" + this.nodeLabel;
            }
        }
        if (userObject instanceof ExpandableTreeElement) {
            this.add(new CheckNode("Please wait", "Please wait...", null, isQuery));
        }
    }

    String getLabel() {
        return this.nodeLabel;
    }

    void setNodeLabel(String nodeLabel) {
        this.nodeLabel = nodeLabel;
    }

    Icon getIcon() {
        return this.icon;
    }

    public void setDisabled() {
        this.disabled = true;
        this.isSelected = false;
        this.removeAllChildren();
    }

    boolean isDisabled() {
        return this.disabled;
    }

    void setNeedsRefresh() {
        this.needsRefresh = true;
        this.setDisabled();
    }

    boolean needsRefresh() {
        return this.needsRefresh;
    }

    public void setSelectionMode(int mode) {
        this.selectionMode = mode;
    }

    public int getSelectionMode() {
        return this.selectionMode;
    }

    public void setSelected(boolean isSelected) {
        Object ob;
        this.isSelected = isSelected;
        if (this.userObject instanceof TreeElement && (ob = ((TreeElement)this.userObject).getUserObject()) instanceof RefactoringElement) {
            ((RefactoringElement)ob).setEnabled(isSelected);
        }
        if (this.selectionMode == 4 && this.children != null) {
            Enumeration e = this.children.elements();
            while (e.hasMoreElements()) {
                CheckNode node = (CheckNode)e.nextElement();
                node.setSelected(isSelected);
            }
        }
    }

    public boolean isSelected() {
        Object ob;
        if (this.userObject instanceof TreeElement && (ob = ((TreeElement)this.userObject).getUserObject()) instanceof RefactoringElement) {
            return ((RefactoringElement)ob).isEnabled() && ((RefactoringElement)ob).getStatus() != 2 && ((RefactoringElement)ob).getStatus() != 3;
        }
        return this.isSelected;
    }

    public PositionBounds getPosition() {
        Object re;
        if (this.userObject instanceof TreeElement && (re = ((TreeElement)this.userObject).getUserObject()) instanceof RefactoringElement) {
            return ((RefactoringElement)re).getPosition();
        }
        return null;
    }

    public String getToolTip() {
        Object re;
        if (this.tooltip == null && this.userObject instanceof TreeElement && (re = ((TreeElement)this.userObject).getUserObject()) instanceof RefactoringElement) {
            RefactoringElement ree = (RefactoringElement)re;
            PositionBounds bounds = this.getPosition();
            FileObject file = ree.getParentFile();
            if (bounds != null && file != null) {
                int line;
                try {
                    line = bounds.getBegin().getLine() + 1;
                }
                catch (IOException ioe) {
                    return null;
                }
                this.tooltip = FileUtil.getFileDisplayName((FileObject)file) + ':' + line;
            }
        }
        return this.tooltip;
    }

    private String getLineString(int line, int size) {
        String l = Integer.toString(line);
        int length = size - l.length();
        for (int i = 0; i < length * 2; ++i) {
            l = "&nbsp;" + l;
        }
        return l + ":&nbsp;&nbsp;";
    }

    public synchronized void ensureChildrenFilled(final DefaultTreeModel model) {
        if (!this.childrenFilled) {
            this.childrenFilled = true;
            if (this.userObject instanceof ExpandableTreeElement) {
                WORKER.post(new Runnable(){

                    @Override
                    public void run() {
                        final ArrayList<TreeElement> subelements = new ArrayList<TreeElement>();
                        for (TreeElement el : (ExpandableTreeElement)CheckNode.this.userObject) {
                            subelements.add(el);
                        }
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                for (TreeElement el : subelements) {
                                    CheckNode.this.insert(new CheckNode(el, el.getText(true), el.getIcon(), CheckNode.this.isQuery), CheckNode.this.getChildCount() - 1);
                                }
                                int[] added = new int[CheckNode.this.getChildCount() - 1];
                                int i = 0;
                                while (i < added.length) {
                                    added[i] = i++;
                                }
                                model.nodesWereInserted(CheckNode.this, added);
                                int childCount = CheckNode.this.getChildCount();
                                TreeNode last = CheckNode.this.getChildAt(childCount - 1);
                                int index = model.getIndexOfChild(CheckNode.this, last);
                                CheckNode.this.remove(index);
                                model.nodesWereRemoved(CheckNode.this, new int[]{index}, new Object[]{last});
                            }
                        });
                    }

                });
            }
        }
    }

}

