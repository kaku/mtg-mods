/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;

public interface RefactoringPlugin {
    public Problem preCheck();

    public Problem checkParameters();

    public Problem fastCheckParameters();

    public void cancelRequest();

    public Problem prepare(RefactoringElementsBag var1);
}

