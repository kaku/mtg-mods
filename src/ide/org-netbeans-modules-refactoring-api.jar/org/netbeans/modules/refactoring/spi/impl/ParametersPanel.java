/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Cancellable
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Context;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProblemDetails;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.api.ui.RefactoringActionsFactory;
import org.netbeans.modules.refactoring.spi.impl.ErrorPanel;
import org.netbeans.modules.refactoring.spi.impl.ProblemComponent;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.netbeans.modules.refactoring.spi.impl.TooltipLabel;
import org.netbeans.modules.refactoring.spi.ui.CustomRefactoringPanel;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUI;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUIBypass;
import org.netbeans.modules.refactoring.spi.ui.UI;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.Cancellable;
import org.openide.util.ContextAwareAction;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

public class ParametersPanel
extends JPanel
implements ProgressListener,
ChangeListener {
    public static final String JUMP_TO_FIRST_OCCURENCE = "JUMP_TO_FIRST_OCCURENCE";
    private static final String PREF_OPEN_NEW_TAB = "PREF_OPEN_NEW_TAB";
    private static final Logger LOGGER = Logger.getLogger(ParametersPanel.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(ParametersPanel.class.getName(), 1, false, false);
    private final Object RESULT_LOCK = new Object();
    private RefactoringSession result;
    private final RefactoringUI rui;
    private final JPanel customPanel;
    private final CustomRefactoringPanel customComponent;
    private transient JDialog dialog = null;
    private ArrayList components = null;
    private Problem problem;
    private ErrorPanel errorPanel;
    private final int PRE_CHECK = 0;
    private final int INPUT_PARAMETERS = 1;
    private final int POST_CHECK = 2;
    private final int CHECK_PARAMETERS = 3;
    private transient int currentState = 1;
    private boolean cancelRequest = false;
    private boolean canceledDialog;
    private boolean inspect = false;
    private JButton back;
    private JPanel buttonsPanel;
    public JButton cancel;
    private JPanel containerPanel;
    private JPanel controlsPanel;
    private JButton help;
    private JPanel innerPanel;
    private JLabel label;
    private JButton next;
    private JCheckBox openInNewTab;
    private JLabel pleaseWait;
    private JButton previewButton;
    private JPanel progressPanel;
    private ProblemDetails currentProblemAction;
    private ProgressBar progressBar;
    private ProgressHandle progressHandle;
    private boolean isIndeterminate;

    public void setPreviewEnabled(boolean enabled) {
        RefactoringPanel.checkEventThread();
        this.next.setEnabled(enabled && !this.isPreviewRequired());
    }

    public ParametersPanel(RefactoringUI rui) {
        RefactoringPanel.checkEventThread();
        this.rui = rui;
        this.initComponents();
        HelpCtx helpCtx = this.getHelpCtx();
        this.help.setEnabled(helpCtx != null && helpCtx != HelpCtx.DEFAULT_HELP);
        this.innerPanel.setBorder(null);
        this.label.setText(" ");
        this.customComponent = rui.getPanel(this);
        this.customPanel = this.customComponent != null ? (JPanel)this.customComponent.getComponent() : null;
        this.errorPanel = new ErrorPanel(rui);
        this.back.setVisible(!rui.isQuery());
        this.previewButton.setVisible(!rui.isQuery());
        this.openInNewTab.setVisible(rui.isQuery());
        this.setButtonsEnabled(false);
        if (rui.isQuery()) {
            Preferences prefs = NbPreferences.forModule(RefactoringPanel.class);
            this.openInNewTab.setSelected(prefs.getBoolean("PREF_OPEN_NEW_TAB", true));
        }
        Mnemonics.setLocalizedText((AbstractButton)this.next, (String)NbBundle.getMessage(ParametersPanel.class, (String)(rui.isQuery() ? "CTL_Find" : "CTL_Finish")));
        this.inspect = "org.netbeans.modules.java.hints.spiimpl.refactoring.InspectAndRefactorUI".equals(rui.getClass().getName());
        this.next.setVisible(!this.isPreviewRequired());
        this.validate();
        Dimension preferredSize = this.progressPanel.getPreferredSize();
        this.progressPanel.setPreferredSize(preferredSize);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension cpDim = new Dimension(0, 0);
        Dimension ppDim = this.progressPanel.getPreferredSize();
        Dimension epDim = new Dimension(0, 0);
        if (this.customPanel != null) {
            cpDim = this.customPanel.getPreferredSize();
        }
        if (this.errorPanel != null) {
            epDim = this.errorPanel.getPreferredSize();
        }
        Dimension dimension = new Dimension(Math.max(Math.max(cpDim.width, ppDim.width), epDim.width), Math.max(cpDim.height, epDim.height) + ppDim.height);
        return dimension;
    }

    private void initComponents() {
        this.progressPanel = new JPanel();
        this.controlsPanel = new JPanel();
        this.buttonsPanel = new JPanel();
        this.back = new JButton();
        this.previewButton = new JButton();
        this.next = new JButton();
        this.cancel = new JButton();
        this.help = new JButton();
        this.openInNewTab = new JCheckBox();
        this.innerPanel = new JPanel();
        this.label = new TooltipLabel();
        this.containerPanel = new JPanel();
        this.pleaseWait = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        ResourceBundle bundle = ResourceBundle.getBundle("org/netbeans/modules/refactoring/spi/impl/Bundle");
        this.setName(bundle.getString("LBL_FindUsagesDialog"));
        this.setLayout(new BorderLayout());
        this.progressPanel.setBorder(BorderFactory.createEmptyBorder(0, 12, 0, 11));
        this.progressPanel.setOpaque(false);
        this.progressPanel.setLayout(new BorderLayout());
        this.controlsPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        this.controlsPanel.setLayout(new BorderLayout());
        this.buttonsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.back, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("CTL_Back"));
        this.back.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ParametersPanel.this.backActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.previewButton, (String)NbBundle.getMessage(ParametersPanel.class, (String)"CTL_PreviewAll"));
        this.previewButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ParametersPanel.this.preview(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.next, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("CTL_Finish"));
        this.next.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ParametersPanel.this.refactor(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cancel, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("CTL_Cancel"));
        this.cancel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ParametersPanel.this.cancelActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.help, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("CTL_Help"));
        this.help.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ParametersPanel.this.helpActionPerformed(evt);
            }
        });
        GroupLayout buttonsPanelLayout = new GroupLayout(this.buttonsPanel);
        this.buttonsPanel.setLayout(buttonsPanelLayout);
        buttonsPanelLayout.setHorizontalGroup(buttonsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(buttonsPanelLayout.createSequentialGroup().addComponent(this.back, -2, 96, -2).addGap(4, 4, 4).addComponent(this.previewButton, -2, 96, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.next).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cancel, -2, 96, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.help, -2, 96, -2).addContainerGap()));
        buttonsPanelLayout.linkSize(0, this.back, this.cancel, this.help, this.next, this.previewButton);
        buttonsPanelLayout.setVerticalGroup(buttonsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.back).addGroup(buttonsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.previewButton).addComponent(this.next).addComponent(this.cancel).addComponent(this.help)));
        this.back.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_Back"));
        this.previewButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ParametersPanel.class, (String)"ParametersPanel.previewButton.AccessibleContext.accessibleDescription"));
        this.next.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_finish"));
        this.cancel.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_cancel"));
        this.help.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_help"));
        this.controlsPanel.add((Component)this.buttonsPanel, "East");
        Mnemonics.setLocalizedText((AbstractButton)this.openInNewTab, (String)NbBundle.getMessage(ParametersPanel.class, (String)"ParametersPanel.openInNewTab.text"));
        this.openInNewTab.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.openInNewTab.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent evt) {
                ParametersPanel.this.openInNewTabStateChanged(evt);
            }
        });
        this.controlsPanel.add((Component)this.openInNewTab, "West");
        this.progressPanel.add((Component)this.controlsPanel, "South");
        this.innerPanel.setBorder(BorderFactory.createLineBorder(new Color(128, 128, 128)));
        this.innerPanel.setLayout(new BorderLayout());
        this.label.setHorizontalAlignment(4);
        this.innerPanel.add((Component)this.label, "West");
        this.progressPanel.add((Component)this.innerPanel, "North");
        this.add((Component)this.progressPanel, "South");
        this.containerPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.containerPanel.setLayout(new BorderLayout());
        this.pleaseWait.setHorizontalAlignment(0);
        Mnemonics.setLocalizedText((JLabel)this.pleaseWait, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("LBL_PleaseWait"));
        this.containerPanel.add((Component)this.pleaseWait, "Center");
        this.pleaseWait.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ParametersPanel.class, (String)"LBL_PleaseWait"));
        this.add((Component)this.containerPanel, "Center");
        this.getAccessibleContext().setAccessibleName(bundle.getString("LBL_FindUsagesDialog"));
        this.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_FindUsagesDialog"));
    }

    private void preview(ActionEvent evt) {
        this.refactor(true);
    }

    private void helpActionPerformed(ActionEvent evt) {
        HelpCtx ctx = this.getHelpCtx();
        if (ctx != null) {
            ctx.display();
        }
    }

    private void backActionPerformed(ActionEvent evt) {
        this.placeCustomPanel();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void cancelActionPerformed(ActionEvent evt) {
        ParametersPanel parametersPanel = this;
        synchronized (parametersPanel) {
            this.canceledDialog = true;
            if (evt != null && evt.getSource() instanceof Cancellable) {
                this.putResult(null);
                if (this.dialog != null) {
                    this.setPanelEnabled(true);
                    this.dialog.setVisible(false);
                }
            } else {
                this.rui.getRefactoring().cancelRequest();
                this.putResult(null);
                if (this.dialog != null) {
                    this.dialog.setVisible(false);
                }
                this.cancelRequest = true;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void refactor(final boolean previewAll) {
        LOGGER.log(Level.FINEST, "refactor - currentState={0}", this.currentState);
        if (this.currentState == 0 && this.rui.hasParameters()) {
            LOGGER.finest("refactor - PRE_CHECK");
            this.placeCustomPanel();
            return;
        }
        if (this.currentState == 0 && !this.rui.hasParameters()) {
            RefactoringSession session = this.putResult(RefactoringSession.create(this.rui.getName()));
            try {
                this.rui.getRefactoring().prepare(session);
                return;
            }
            finally {
                this.setVisibleLater(false);
            }
        }
        if (this.currentState == 2 && previewAll && this.currentProblemAction != null) {
            LOGGER.finest("refactor - POST_CHECK - problems");
            Cancellable doCloseParent = new Cancellable(){

                public boolean cancel() {
                    ParametersPanel.this.cancelActionPerformed(new ActionEvent(this, 0, null));
                    return true;
                }
            };
            this.currentProblemAction.showDetails(new ProblemComponent.CallbackAction(this.rui), doCloseParent);
            return;
        }
        this.setPanelEnabled(false);
        this.cancel.setEnabled(true);
        this.openInNewTab.setVisible(false);
        this.next.setVisible(false);
        RequestProcessor rp = new RequestProcessor();
        final int inputState = this.currentState;
        if (this.currentState != 0 && this.currentState != 2) {
            if (this.rui instanceof RefactoringUIBypass && ((RefactoringUIBypass)((Object)this.rui)).isRefactoringBypassRequired()) {
                LOGGER.finest("refactor - bypass");
                try {
                    ((RefactoringUIBypass)((Object)this.rui)).doRefactoringBypass();
                }
                catch (IOException ioe) {
                    this.currentState = 2;
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            String message = ioe.getMessage();
                            message = message != null ? message : "";
                            ParametersPanel.this.placeErrorPanel(new Problem(true, message));
                        }
                    });
                }
                finally {
                    if (inputState == this.currentState) {
                        this.result = null;
                        this.setVisibleLater(false);
                    }
                    return;
                }
            }
            if (this.currentState != 2 && this.currentState != 3) {
                this.putResult(RefactoringSession.create(this.rui.getName()));
                rp.post((Runnable)new Prepare());
            } else if (this.currentState == 3) {
                rp.post((Runnable)new Prepare());
            }
        }
        LOGGER.finest("refactor - asynchronously");
        rp.post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                block11 : {
                    LOGGER.log(Level.FINEST, "refactor - inputState={0}, currentState={1}", new Object[]{inputState, ParametersPanel.this.currentState});
                    if (inputState == ParametersPanel.this.currentState) {
                        final RefactoringSession session = ParametersPanel.this.getResult();
                        if (session != null && !previewAll && ParametersPanel.this.currentState != 2 && (APIAccessor.DEFAULT.hasChangesInGuardedBlocks(session) || APIAccessor.DEFAULT.hasChangesInReadOnlyFiles(session))) {
                            ParametersPanel.this.currentState = 2;
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    ParametersPanel.this.placeErrorPanel(new Problem(false, NbBundle.getMessage(ParametersPanel.class, (String)(APIAccessor.DEFAULT.hasChangesInReadOnlyFiles(session) ? "LBL_CannotRefactorReadOnlyFile" : "LBL_CannotRefactorGuardedBlock"))));
                                }
                            });
                            return;
                        }
                        try {
                            if (previewAll || session == null) break block11;
                            if (!ParametersPanel.this.rui.isQuery() && session.getRefactoringElements().isEmpty()) {
                                DialogDescriptor nd = new DialogDescriptor((Object)NbBundle.getMessage(ParametersPanel.class, (String)"MSG_NoPatternsFound"), ParametersPanel.this.rui.getName(), true, new Object[]{DialogDescriptor.OK_OPTION}, DialogDescriptor.OK_OPTION, 0, ParametersPanel.this.rui.getHelpCtx(), null);
                                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                                break block11;
                            }
                            session.addProgressListener(ParametersPanel.this);
                            try {
                                session.doRefactoring(true);
                            }
                            finally {
                                session.removeProgressListener(ParametersPanel.this);
                            }
                        }
                        finally {
                            if (!previewAll) {
                                ParametersPanel.this.putResult(null);
                            }
                            if (inputState == ParametersPanel.this.currentState) {
                                ParametersPanel.this.setVisibleLater(false);
                            }
                        }
                    }
                }
            }

        });
    }

    private void refactor(ActionEvent evt) {
        this.refactor(this.rui.isQuery());
    }

    private void openInNewTabStateChanged(ChangeEvent evt) {
        Preferences prefs = NbPreferences.forModule(RefactoringPanel.class);
        prefs.putBoolean("PREF_OPEN_NEW_TAB", this.openInNewTab.isSelected());
    }

    private void setVisibleLater(final boolean visible) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (ParametersPanel.this.dialog != null) {
                    ParametersPanel.this.dialog.setVisible(visible);
                }
            }
        });
    }

    private void setPanelEnabled(boolean enabled) {
        RefactoringPanel.checkEventThread();
        this.setButtonsEnabled(enabled);
        if (enabled) {
            if (this.components == null) {
                return;
            }
            Iterator it = this.components.iterator();
            while (it.hasNext()) {
                ((Component)it.next()).setEnabled(true);
            }
            this.components = null;
        } else {
            if (this.components != null) {
                return;
            }
            this.components = new ArrayList();
            this.disableComponents(this.customPanel);
        }
    }

    private void disableComponents(Container c) {
        if (c == null) {
            assert (this.customPanel == null);
            return;
        }
        RefactoringPanel.checkEventThread();
        Component[] children = c.getComponents();
        for (int i = 0; i < children.length; ++i) {
            if (children[i].isEnabled()) {
                children[i].setEnabled(false);
                this.components.add(children[i]);
            }
            if (!(children[i] instanceof Container)) continue;
            this.disableComponents((Container)children[i]);
        }
    }

    public synchronized RefactoringSession showDialog() {
        HelpCtx helpCtx;
        RefactoringPanel.checkEventThread();
        this.putClientProperty("JUMP_TO_FIRST_OCCURENCE", false);
        if (this.rui != null) {
            this.rui.getRefactoring().addProgressListener(this);
            this.openInNewTab.setVisible(this.rui.isQuery());
            this.next.setVisible(true);
        }
        String title = this.customPanel != null && this.customPanel.getName() != null && !"".equals(this.customPanel.getName()) ? this.customPanel.getName() : this.rui.getName();
        DialogDescriptor descriptor = new DialogDescriptor((Object)this, title, true, new Object[0], (Object)null, 0, null, null);
        this.dialog = (JDialog)DialogDisplayer.getDefault().createDialog(descriptor);
        if (this.customPanel != null) {
            this.dialog.getAccessibleContext().setAccessibleName(this.rui.getName());
            this.dialog.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ParametersPanel.class, (String)"ACSD_FindUsagesDialog"));
        }
        helpCtx = (helpCtx = this.rui.getHelpCtx()) == null ? HelpCtx.DEFAULT_HELP : helpCtx;
        HelpCtx.setHelpIDString((JComponent)this.dialog.getRootPane(), (String)helpCtx.getHelpID());
        this.setOkCancelShortcuts();
        RequestProcessor.Task task = RP.post(new Runnable(){

            @Override
            public void run() {
                try {
                    if (!ParametersPanel.this.rui.isQuery()) {
                        LifecycleManager.getDefault().saveAll();
                    }
                    ParametersPanel.this.problem = ParametersPanel.this.rui.getRefactoring().preCheck();
                }
                catch (RuntimeException e) {
                    ParametersPanel.this.setVisibleLater(false);
                    throw e;
                }
                if (ParametersPanel.this.problem != null) {
                    ParametersPanel.this.currentState = 0;
                    if (!ParametersPanel.this.problem.isFatal() && ParametersPanel.this.rui.hasParameters()) {
                        ParametersPanel.this.customComponent.initialize();
                    }
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (ParametersPanel.this.dialog != null) {
                                ParametersPanel.this.placeErrorPanel(ParametersPanel.this.problem);
                                ParametersPanel.this.dialog.setVisible(true);
                            }
                        }
                    });
                } else {
                    if (ParametersPanel.this.customPanel != null) {
                        ParametersPanel.this.customComponent.initialize();
                    }
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            ParametersPanel.this.placeCustomPanel();
                        }
                    });
                    if (!ParametersPanel.this.rui.hasParameters()) {
                        ParametersPanel.this.currentState = 2;
                        RefactoringSession session = ParametersPanel.this.putResult(RefactoringSession.create(ParametersPanel.this.rui.getName()));
                        Problem problem = null;
                        try {
                            problem = ParametersPanel.this.rui.getRefactoring().prepare(session);
                        }
                        catch (Throwable t) {
                            ParametersPanel.this.setVisibleLater(false);
                        }
                        if (problem != null) {
                            ParametersPanel.this.back.setEnabled(false);
                            ParametersPanel.this.placeErrorPanel(problem);
                        } else {
                            ParametersPanel.this.setVisibleLater(false);
                        }
                    }
                }
            }

        });
        if (this.customComponent != null || this.rui.hasParameters() || APIAccessor.DEFAULT.hasPluginsWithProgress(this.rui.getRefactoring())) {
            this.dialog.pack();
            this.dialog.setVisible(true);
        }
        this.dialog.dispose();
        this.dialog = null;
        descriptor.setMessage((Object)"");
        if (this.rui != null) {
            this.stop(null);
            this.rui.getRefactoring().removeProgressListener(this);
        }
        if (!this.cancelRequest) {
            task.waitFinished();
        }
        RefactoringSession temp = this.getResult();
        this.putResult(null);
        return temp;
    }

    private void setOkCancelShortcuts() {
        this.canceledDialog = false;
        KeyStroke cancelKS = KeyStroke.getKeyStroke(27, 0);
        String cancelActionKey = "cancel";
        this.getRootPane().getInputMap(1).put(cancelKS, cancelActionKey);
        AbstractAction cancelAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent ev) {
                if (ParametersPanel.this.cancel.isEnabled()) {
                    ParametersPanel.this.cancelActionPerformed(ev);
                }
            }
        };
        this.getRootPane().getActionMap().put(cancelActionKey, cancelAction);
        this.dialog.addWindowListener(new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent evt) {
                if (ParametersPanel.this.cancel.isEnabled()) {
                    ParametersPanel.this.cancelActionPerformed(null);
                }
            }
        });
        if (this.rui.isQuery()) {
            ContextAwareAction whereUsedAction = RefactoringActionsFactory.whereUsedAction();
            KeyStroke OKKS = (KeyStroke)whereUsedAction.getValue("AcceleratorKey");
            String OKActionKey = "OK";
            this.getRootPane().getInputMap(1).put(OKKS, OKActionKey);
            AbstractAction OKAction = new AbstractAction(){

                @Override
                public void actionPerformed(ActionEvent ae) {
                    ParametersPanel.this.putClientProperty("JUMP_TO_FIRST_OCCURENCE", true);
                    ParametersPanel.this.refactor(null);
                }
            };
            this.getRootPane().getActionMap().put(OKActionKey, OKAction);
        }
    }

    boolean isCanceledDialog() {
        return this.canceledDialog;
    }

    private ProblemDetails getDetails(Problem problem) {
        if (problem.getNext() == null) {
            return problem.getDetails();
        }
        return null;
    }

    private void placeErrorPanel(Problem problem) {
        if (this.dialog == null) {
            return;
        }
        this.containerPanel.removeAll();
        this.errorPanel = new ErrorPanel(problem, this.rui);
        this.errorPanel.setBorder(new EmptyBorder(new Insets(12, 12, 11, 11)));
        this.containerPanel.add((Component)this.errorPanel, "Center");
        this.next.setEnabled(!problem.isFatal() && !this.isPreviewRequired());
        this.dialog.getRootPane().setDefaultButton(this.isPreviewRequired() ? this.previewButton : this.next);
        this.next.setVisible(true);
        if (this.currentState == 0) {
            Mnemonics.setLocalizedText((AbstractButton)this.next, (String)NbBundle.getMessage(ParametersPanel.class, (String)"CTL_Next"));
            this.back.setVisible(false);
            if (!this.rui.hasParameters()) {
                this.next.setVisible(false);
                this.previewButton.setVisible(true);
                this.previewButton.setEnabled(true);
            } else {
                this.next.setVisible(true);
                this.previewButton.setVisible(false);
                this.previewButton.setEnabled(false);
            }
        } else {
            ProblemDetails details = this.getDetails(problem);
            if (details != null) {
                Mnemonics.setLocalizedText((AbstractButton)this.previewButton, (String)details.getDetailsHint());
                this.previewButton.setVisible(true);
                this.previewButton.setEnabled(true);
                this.currentProblemAction = details;
            }
            this.back.setVisible(true);
            this.back.setEnabled(true);
            this.dialog.getRootPane().setDefaultButton(this.back);
        }
        this.cancel.setEnabled(true);
        this.previewButton.setEnabled(!problem.isFatal());
        if (this.progressHandle != null) {
            this.stop(new ProgressEvent(this, 4));
        }
        ((BorderLayout)this.getLayout()).invalidateLayout(this);
        this.dialog.pack();
        if (this.next.isEnabled() && this.next.isVisible()) {
            this.next.requestFocusInWindow();
        } else {
            this.cancel.requestFocusInWindow();
        }
        this.dialog.repaint();
    }

    private void placeCustomPanel() {
        if (this.dialog == null) {
            return;
        }
        if (this.customPanel == null) {
            return;
        }
        Mnemonics.setLocalizedText((AbstractButton)this.next, (String)NbBundle.getMessage(ParametersPanel.class, (String)(this.rui.isQuery() ? "CTL_Find" : "CTL_Finish")));
        Mnemonics.setLocalizedText((AbstractButton)this.previewButton, (String)NbBundle.getMessage(ParametersPanel.class, (String)(this.inspect ? "CTL_Inspect" : "CTL_PreviewAll")));
        this.customPanel.setBorder(new EmptyBorder(new Insets(12, 12, 11, 11)));
        this.containerPanel.removeAll();
        this.containerPanel.add((Component)this.customPanel, "Center");
        this.back.setVisible(false);
        this.next.setVisible(!this.isPreviewRequired());
        this.previewButton.setVisible(!this.rui.isQuery());
        this.next.setEnabled(!this.isPreviewRequired());
        this.currentState = 1;
        this.setPanelEnabled(true);
        this.cancel.setEnabled(true);
        this.dialog.getRootPane().setDefaultButton(this.isPreviewRequired() ? this.previewButton : this.next);
        this.setOKorRefactor();
        ((BorderLayout)this.getLayout()).invalidateLayout(this);
        this.stop(new ProgressEvent(this, 4));
        this.dialog.pack();
        if (!this.customPanel.requestFocusInWindow()) {
            if (this.previewButton.isEnabled() && this.previewButton.isVisible()) {
                this.previewButton.requestFocusInWindow();
            } else {
                this.next.requestFocusInWindow();
            }
        }
        this.dialog.repaint();
    }

    private boolean isPreviewRequired() {
        UI.Constants b = this.rui.getRefactoring().getContext().lookup(UI.Constants.class);
        return b != null && b == UI.Constants.REQUEST_PREVIEW;
    }

    @Override
    public void start(final ProgressEvent event) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                String text;
                if (ParametersPanel.this.dialog == null) {
                    return;
                }
                if (ParametersPanel.this.progressBar != null && ParametersPanel.this.progressBar.isVisible()) {
                    LOGGER.log(Level.INFO, "{0} called start multiple times", event.getSource());
                    ParametersPanel.this.stop(event);
                }
                ParametersPanel.this.progressPanel.remove(ParametersPanel.this.innerPanel);
                ParametersPanel.this.progressBar = ProgressBar.create(ParametersPanel.this.progressHandle = ProgressHandleFactory.createHandle((String)""));
                if (event.getCount() == -1) {
                    ParametersPanel.this.isIndeterminate = true;
                    ParametersPanel.this.progressHandle.start();
                    ParametersPanel.this.progressHandle.switchToIndeterminate();
                } else {
                    ParametersPanel.this.isIndeterminate = false;
                    ParametersPanel.this.progressHandle.start(event.getCount());
                }
                switch (event.getOperationType()) {
                    case 2: {
                        text = NbBundle.getMessage(ParametersPanel.class, (String)"LBL_ParametersCheck");
                        break;
                    }
                    case 3: {
                        text = NbBundle.getMessage(ParametersPanel.class, (String)"LBL_Prepare");
                        break;
                    }
                    case 1: {
                        text = NbBundle.getMessage(ParametersPanel.class, (String)"LBL_PreCheck");
                        break;
                    }
                    default: {
                        text = NbBundle.getMessage(ParametersPanel.class, (String)"LBL_Usages");
                    }
                }
                ParametersPanel.this.progressBar.setString(text);
                ParametersPanel.this.progressPanel.add((Component)ParametersPanel.this.progressBar, "North");
                ParametersPanel.this.progressPanel.setPreferredSize(null);
                if (ParametersPanel.this.dialog != null) {
                    ParametersPanel.this.dialog.validate();
                }
                ParametersPanel.this.setButtonsEnabled(false);
            }
        });
    }

    @Override
    public void step(final ProgressEvent event) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    if (ParametersPanel.this.progressHandle == null) {
                        return;
                    }
                    if (ParametersPanel.this.isIndeterminate && event.getCount() > 0) {
                        ParametersPanel.this.progressHandle.switchToDeterminate(event.getCount());
                        ParametersPanel.this.isIndeterminate = false;
                    } else {
                        ParametersPanel.this.progressHandle.progress(ParametersPanel.this.isIndeterminate ? -2 : event.getCount());
                    }
                }
                catch (Throwable e) {
                    ErrorManager.getDefault().notify(1, e);
                }
            }
        });
    }

    @Override
    public void stop(ProgressEvent event) {
        Runnable run = new Runnable(){

            @Override
            public void run() {
                if (ParametersPanel.this.progressHandle == null) {
                    return;
                }
                ParametersPanel.this.progressHandle.finish();
                ParametersPanel.this.progressPanel.remove(ParametersPanel.this.progressBar);
                ParametersPanel.this.progressPanel.add((Component)ParametersPanel.this.innerPanel, "Center");
                ParametersPanel.this.progressHandle = null;
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            run.run();
        } else {
            SwingUtilities.invokeLater(run);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.rui instanceof RefactoringUIBypass && ((RefactoringUIBypass)((Object)this.rui)).isRefactoringBypassRequired()) {
            this.showProblem(null);
        } else {
            this.showProblem(this.rui.checkParameters());
        }
        this.setOKorRefactor();
    }

    private void setOKorRefactor() {
        if (this.rui instanceof RefactoringUIBypass) {
            if (((RefactoringUIBypass)((Object)this.rui)).isRefactoringBypassRequired()) {
                this.next.setText(NbBundle.getMessage(DialogDisplayer.class, (String)"CTL_OK"));
                this.previewButton.setVisible(false);
            } else {
                Mnemonics.setLocalizedText((AbstractButton)this.next, (String)NbBundle.getMessage(ParametersPanel.class, (String)(this.rui.isQuery() ? "CTL_Find" : "CTL_Finish")));
                this.previewButton.setVisible(true);
            }
        }
    }

    private void showProblem(Problem problem) {
        if (problem == null) {
            this.label.setText(" ");
            this.innerPanel.setBorder(null);
            this.next.setEnabled(!this.isPreviewRequired());
            this.previewButton.setEnabled(true);
            return;
        }
        this.innerPanel.setBorder(new LineBorder(Color.gray));
        this.progressPanel.setVisible(true);
        if (problem.isFatal()) {
            this.displayError(problem.getMessage());
        } else {
            this.displayWarning(problem.getMessage());
        }
    }

    private void displayError(String error) {
        this.next.setEnabled(false);
        this.previewButton.setEnabled(false);
        this.label.setText("<html><font color=\"red\">" + NbBundle.getMessage(ParametersPanel.class, (String)"LBL_Error") + ": </font>" + error + "</html>");
    }

    private void displayWarning(String warning) {
        this.next.setEnabled(!this.isPreviewRequired());
        this.previewButton.setEnabled(true);
        this.label.setText("<html><font color=\"red\">" + NbBundle.getMessage(ParametersPanel.class, (String)"LBL_Warning") + ": </font>" + warning + "</html>");
    }

    boolean isCreateNewTab() {
        return this.openInNewTab.isSelected();
    }

    private void setButtonsEnabled(boolean enabled) {
        this.next.setEnabled(enabled && !this.isPreviewRequired());
        this.back.setEnabled(enabled);
        this.previewButton.setEnabled(enabled);
    }

    public HelpCtx getHelpCtx() {
        return this.rui.getHelpCtx();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private RefactoringSession getResult() {
        Object object = this.RESULT_LOCK;
        synchronized (object) {
            return this.result;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private RefactoringSession putResult(RefactoringSession session) {
        Object object = this.RESULT_LOCK;
        synchronized (object) {
            this.result = session;
        }
        return session;
    }

    private static class ProgressBar
    extends JPanel {
        private JLabel label;

        private static ProgressBar create(ProgressHandle handle) {
            ProgressBar instance = new ProgressBar();
            instance.setLayout(new BorderLayout());
            instance.label = new JLabel(" ");
            instance.label.setBorder(new EmptyBorder(0, 0, 2, 0));
            instance.add((Component)instance.label, "North");
            JComponent progress = ProgressHandleFactory.createProgressComponent((ProgressHandle)handle);
            instance.add((Component)progress, "Center");
            return instance;
        }

        public void setString(String value) {
            this.label.setText(value);
        }

        private ProgressBar() {
        }
    }

    private class Prepare
    implements Runnable {
        private Prepare() {
        }

        @Override
        public void run() {
            if (ParametersPanel.this.currentState != 2 && ParametersPanel.this.currentState != 3) {
                ParametersPanel.this.problem = ParametersPanel.this.rui.setParameters();
                if (ParametersPanel.this.problem != null && ParametersPanel.this.currentState != 2) {
                    ParametersPanel.this.currentState = 3;
                    try {
                        SwingUtilities.invokeAndWait(new Runnable(){

                            @Override
                            public void run() {
                                ParametersPanel.this.placeErrorPanel(ParametersPanel.this.problem);
                            }
                        });
                    }
                    catch (Exception ie) {
                        throw new RuntimeException(ie);
                    }
                    return;
                }
            }
            try {
                final RefactoringSession refactoringSession = ParametersPanel.this.getResult();
                if (refactoringSession != null) {
                    ParametersPanel.this.problem = null;
                    if (ParametersPanel.this.rui.isQuery()) {
                        RP.post(new Runnable(){

                            @Override
                            public void run() {
                                ParametersPanel.this.problem = ParametersPanel.this.rui.getRefactoring().prepare(refactoringSession);
                            }
                        });
                    } else {
                        ParametersPanel.this.problem = ParametersPanel.this.rui.getRefactoring().prepare(refactoringSession);
                    }
                }
            }
            catch (RuntimeException e) {
                ParametersPanel.this.setVisibleLater(false);
                throw e;
            }
            if (ParametersPanel.this.problem != null) {
                ParametersPanel.this.currentState = 2;
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        ParametersPanel.this.placeErrorPanel(ParametersPanel.this.problem);
                    }
                });
            }
        }

    }

}

