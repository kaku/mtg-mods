/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.diff.DiffController
 *  org.netbeans.api.diff.DiffController$DiffPane
 *  org.netbeans.api.diff.DiffController$LocationType
 *  org.netbeans.api.diff.Difference
 *  org.netbeans.api.diff.StreamSource
 *  org.netbeans.spi.editor.guards.GuardedEditorSupport
 *  org.netbeans.spi.editor.guards.GuardedSectionsFactory
 *  org.netbeans.spi.editor.guards.GuardedSectionsProvider
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.diff.DiffController;
import org.netbeans.api.diff.Difference;
import org.netbeans.api.diff.StreamSource;
import org.netbeans.modules.refactoring.api.impl.SPIAccessor;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanelContainer;
import org.netbeans.modules.refactoring.spi.ui.UI;
import org.netbeans.spi.editor.guards.GuardedEditorSupport;
import org.netbeans.spi.editor.guards.GuardedSectionsFactory;
import org.netbeans.spi.editor.guards.GuardedSectionsProvider;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public class PreviewManager {
    private static PreviewManager manager;
    private WeakHashMap<RefactoringPanel, HashMap<FileObject, Pair>> map = new WeakHashMap();

    private PreviewManager() {
    }

    public static PreviewManager getDefault() {
        if (manager == null) {
            manager = new PreviewManager();
        }
        return manager;
    }

    public void clean(RefactoringPanel panel) {
        this.map.remove(panel);
    }

    private Pair getPair(SimpleRefactoringElementImplementation element) {
        Pair pair;
        RefactoringPanel current = RefactoringPanelContainer.getRefactoringComponent().getCurrentPanel();
        HashMap m = this.map.get(current);
        if (m != null && (pair = m.get((Object)element.getParentFile())) != null) {
            return pair;
        }
        try {
            NewDiffSource nds = new NewDiffSource(element);
            DiffController diffView = DiffController.create((StreamSource)new OldDiffSource(element), (StreamSource)nds);
            if (m == null) {
                m = new HashMap();
                this.map.put(current, m);
            }
            Pair p = new Pair(diffView, nds);
            m.put(element.getParentFile(), p);
            return p;
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public void refresh(SimpleRefactoringElementImplementation element) {
        try {
            String newText = SPIAccessor.DEFAULT.getNewFileContent(element);
            if (newText == null) {
                UI.setComponentForRefactoringPreview(null);
                return;
            }
            Pair p = this.getPair(element);
            UI.setComponentForRefactoringPreview(p.dc.getJComponent());
            p.source.setNewText(newText);
            if (element.getPosition() != null) {
                p.dc.setLocation(DiffController.DiffPane.Base, DiffController.LocationType.LineNumber, element.getPosition().getBegin().getLine());
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private class NewDiffSource
    extends StreamSource {
        private SimpleRefactoringElementImplementation element;
        private Document internal;

        NewDiffSource(SimpleRefactoringElementImplementation refactElemImpl) {
            this.element = refactElemImpl;
        }

        public String getName() {
            return NbBundle.getMessage(PreviewManager.class, (String)"LBL_ProposedRefactoring");
        }

        public String getTitle() {
            if (this.element.getParentFile().isFolder()) {
                return NbBundle.getMessage(PreviewManager.class, (String)"LBL_NewFile");
            }
            return NbBundle.getMessage(PreviewManager.class, (String)"LBL_Refactored", (Object)this.element.getParentFile().getNameExt());
        }

        public String getMIMEType() {
            if (this.element.getParentFile().isFolder()) {
                return "text/x-java";
            }
            return this.element.getParentFile().getMIMEType();
        }

        public Reader createReader() throws IOException {
            return null;
        }

        public Writer createWriter(Difference[] conflicts) throws IOException {
            return null;
        }

        private Document getDocument() {
            if (this.internal == null) {
                this.internal = CloneableEditorSupport.getEditorKit((String)this.getMIMEType()).createDefaultDocument();
            }
            return this.internal;
        }

        public Lookup getLookup() {
            return Lookups.singleton((Object)this.getDocument());
        }

        private boolean isOriginalDocumentLoaded() {
            try {
                EditorCookie ec;
                FileObject fo = this.element.getParentFile();
                DataObject dObj = DataObject.find((FileObject)fo);
                EditorCookie editorCookie = ec = dObj != null ? (EditorCookie)dObj.getCookie(EditorCookie.class) : null;
                if (ec != null) {
                    StyledDocument doc = ec.getDocument();
                    return doc != null;
                }
            }
            catch (DataObjectNotFoundException ex) {
                // empty catch block
            }
            return false;
        }

        public void setNewText(String newText) {
            block5 : {
                try {
                    if (!this.isOriginalDocumentLoaded() && GuardedSectionsFactory.find((String)this.getMIMEType()) != null) {
                        GuardedSectionsFactory guardedSectionsFactory = GuardedSectionsFactory.find((String)this.getMIMEType());
                        GuardedSectionsProvider guardedProvider = guardedSectionsFactory.create(new GuardedEditorSupport(){

                            public StyledDocument getDocument() {
                                return (StyledDocument)NewDiffSource.this.getDocument();
                            }
                        });
                        Reader reader = guardedProvider.createGuardedReader((InputStream)new ByteArrayInputStream(newText.getBytes()), Charset.defaultCharset());
                        char[] buf = new char[newText.length()];
                        try {
                            reader.read(buf);
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                        this.internal.remove(0, this.internal.getLength());
                        this.internal.insertString(0, new String(buf), null);
                        break block5;
                    }
                    this.internal.remove(0, this.internal.getLength());
                    this.internal.insertString(0, newText, null);
                }
                catch (BadLocationException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        }

    }

    private class OldDiffSource
    extends StreamSource {
        private FileObject file;

        OldDiffSource(SimpleRefactoringElementImplementation r) {
            this.file = r.getParentFile();
        }

        public String getName() {
            if (this.file.isFolder()) {
                return NbBundle.getMessage(PreviewManager.class, (String)"LBL_FileDoesNotExist");
            }
            return this.file.getName();
        }

        public String getTitle() {
            if (this.file.isFolder()) {
                return NbBundle.getMessage(PreviewManager.class, (String)"LBL_FileDoesNotExist");
            }
            return this.file.getNameExt();
        }

        public String getMIMEType() {
            return this.file.getMIMEType();
        }

        public Reader createReader() throws IOException {
            return null;
        }

        public Writer createWriter(Difference[] conflicts) throws IOException {
            return null;
        }

        public Lookup getLookup() {
            return Lookups.singleton((Object)this.file);
        }
    }

    private class Pair {
        DiffController dc;
        NewDiffSource source;

        Pair(DiffController dc, NewDiffSource source) {
            this.dc = dc;
            this.source = source;
        }
    }

}

