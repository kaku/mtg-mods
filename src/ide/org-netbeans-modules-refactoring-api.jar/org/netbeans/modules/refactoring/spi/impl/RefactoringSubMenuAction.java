/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.text.TextAction;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public final class RefactoringSubMenuAction
extends TextAction
implements Presenter.Menu,
Presenter.Popup {
    private static final Logger LOG = Logger.getLogger(RefactoringSubMenuAction.class.getName());
    private final boolean showIcons;

    public static RefactoringSubMenuAction create(FileObject o) {
        return new RefactoringSubMenuAction(true);
    }

    public static JMenu createMenu() {
        RefactoringSubMenuAction action = new RefactoringSubMenuAction(true);
        return (JMenu)action.getMenuPresenter();
    }

    RefactoringSubMenuAction(boolean showIcons) {
        super(NbBundle.getMessage(RefactoringSubMenuAction.class, (String)"LBL_Action"));
        this.showIcons = showIcons;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    public JMenuItem getMenuPresenter() {
        return new SubMenu();
    }

    public JMenuItem getPopupPresenter() {
        return this.getMenuPresenter();
    }

    public boolean equals(Object o) {
        return o instanceof RefactoringSubMenuAction;
    }

    public int hashCode() {
        return 1;
    }

    private final class SubMenu
    extends JMenu {
        private boolean createMenuLazily;
        private boolean wasSeparator;
        private boolean shouldAddSeparator;

        public SubMenu() {
            super((String)RefactoringSubMenuAction.this.getValue("Name"));
            this.createMenuLazily = true;
            if (RefactoringSubMenuAction.this.showIcons) {
                this.setMnemonic(NbBundle.getMessage(RefactoringSubMenuAction.class, (String)"LBL_ActionMnemonic").charAt(0));
            }
        }

        @Override
        public JPopupMenu getPopupMenu() {
            if (this.createMenuLazily) {
                this.createMenuItems();
                this.createMenuLazily = false;
            }
            return super.getPopupMenu();
        }

        private void createMenuItems() {
            DataFolder df;
            this.removeAll();
            FileObject fo = FileUtil.getConfigFile((String)"Menu/Refactoring");
            DataFolder dataFolder = df = fo == null ? null : DataFolder.findFolder((FileObject)fo);
            if (df != null) {
                this.wasSeparator = true;
                this.shouldAddSeparator = false;
                DataObject[] actionObjects = df.getChildren();
                for (int i = 0; i < actionObjects.length; ++i) {
                    Object instance;
                    InstanceCookie ic = (InstanceCookie)actionObjects[i].getCookie(InstanceCookie.class);
                    if (ic == null) continue;
                    try {
                        instance = ic.instanceCreate();
                    }
                    catch (IOException e) {
                        LOG.log(Level.WARNING, actionObjects[i].toString(), e);
                        continue;
                    }
                    catch (ClassNotFoundException e) {
                        LOG.log(Level.WARNING, actionObjects[i].toString(), e);
                        continue;
                    }
                    if (instance instanceof Presenter.Popup) {
                        JMenuItem temp = ((Presenter.Popup)instance).getPopupPresenter();
                        if (temp instanceof DynamicMenuContent) {
                            for (JComponent presenter : ((DynamicMenuContent)temp).getMenuPresenters()) {
                                this.addPresenter(presenter);
                            }
                            continue;
                        }
                        this.addPresenter(temp);
                        continue;
                    }
                    if (instance instanceof Action) {
                        JMenuItem mi = new JMenuItem();
                        Actions.connect((JMenuItem)mi, (Action)((Action)instance), (boolean)true);
                        this.addPresenter(mi);
                        continue;
                    }
                    if (!(instance instanceof JSeparator)) continue;
                    this.addPresenter((JSeparator)instance);
                }
            }
        }

        private void addPresenter(JComponent presenter) {
            boolean isSeparator;
            if (!RefactoringSubMenuAction.this.showIcons && presenter instanceof AbstractButton) {
                ((AbstractButton)presenter).setIcon(null);
            }
            boolean bl = isSeparator = presenter == null || presenter instanceof JSeparator;
            if (isSeparator) {
                if (!this.wasSeparator) {
                    this.shouldAddSeparator = true;
                    this.wasSeparator = true;
                }
            } else {
                if (this.shouldAddSeparator) {
                    this.addSeparator();
                    this.shouldAddSeparator = false;
                }
                this.add(presenter);
                this.wasSeparator = false;
            }
        }
    }

}

