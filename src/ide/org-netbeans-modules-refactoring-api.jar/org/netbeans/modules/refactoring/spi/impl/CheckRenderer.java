/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;
import org.netbeans.modules.refactoring.spi.impl.CheckNode;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.openide.awt.HtmlRenderer;
import org.openide.util.NbBundle;

public class CheckRenderer
extends JPanel
implements TreeCellRenderer {
    protected JCheckBox check;
    protected HtmlRenderer.Renderer renderer = HtmlRenderer.createRenderer();
    private CheckNode renderedNode;
    private static Dimension checkDim;
    static Rectangle checkBounds;
    private Component stringDisplayer = new JLabel(" ");
    private boolean outerCallGetToolTipText = true;

    public CheckRenderer(boolean isQuery, Color background) {
        this.setLayout(null);
        if (isQuery) {
            this.check = null;
        } else {
            this.check = new JCheckBox();
            this.add(this.check);
            this.check.setBackground(background);
            this.check.setPreferredSize(checkDim);
        }
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        CheckNode node;
        this.renderedNode = node = (CheckNode)value;
        this.stringDisplayer = this.renderer.getTreeCellRendererComponent(tree, (Object)this.getNodeText(node), isSelected, expanded, leaf, row, hasFocus);
        this.renderer.setIcon(node.getIcon());
        this.stringDisplayer.setEnabled(!node.isDisabled());
        if (this.stringDisplayer.getBackground() == null) {
            this.stringDisplayer.setBackground(tree.getBackground());
        }
        if (this.stringDisplayer.getForeground() == null) {
            this.stringDisplayer.setForeground(tree.getForeground());
        }
        if (this.check != null) {
            this.check.setSelected(node.isSelected());
            this.check.setEnabled(!node.isDisabled());
        }
        return this;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolTipText() {
        if (this.outerCallGetToolTipText) {
            try {
                this.outerCallGetToolTipText = false;
                this.setToolTipText(this.renderedNode.getToolTip());
            }
            finally {
                this.outerCallGetToolTipText = true;
            }
        }
        return super.getToolTipText();
    }

    @Override
    public void paintComponent(Graphics g) {
        Dimension d_check = this.check == null ? new Dimension(0, 0) : this.check.getSize();
        Dimension d_label = this.stringDisplayer == null ? new Dimension(0, 0) : this.stringDisplayer.getPreferredSize();
        boolean y_check = false;
        int y_label = 0;
        if (d_check.height >= d_label.height) {
            y_label = (d_check.height - d_label.height) / 2;
        }
        if (this.check != null) {
            this.check.setBounds(0, 0, d_check.width, d_check.height);
            this.check.paint(g);
        }
        if (this.stringDisplayer != null) {
            int y = y_label - 2;
            this.stringDisplayer.setBounds(d_check.width, y, d_label.width, this.getHeight() - 1);
            g.translate(d_check.width, y_label);
            this.stringDisplayer.paint(g);
            g.translate(- d_check.width, - y_label);
        }
    }

    private String getNodeText(CheckNode node) {
        int i;
        String nodeLabel = node.getLabel() == null ? NbBundle.getMessage(CheckRenderer.class, (String)"LBL_NotAvailable") : node.getLabel();
        nodeLabel = "<html>" + nodeLabel;
        if (node.needsRefresh()) {
            nodeLabel = nodeLabel + " - " + NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_NeedsRefresh");
        }
        if ((i = (nodeLabel = nodeLabel + "</html>").indexOf("<br>")) != -1) {
            return nodeLabel.substring(0, i) + "</html>";
        }
        return nodeLabel;
    }

    @Override
    public Dimension getPreferredSize() {
        if (this.stringDisplayer != null) {
            this.stringDisplayer.setFont(this.getFont());
        }
        Dimension d_check = this.check == null ? null : this.check.getPreferredSize();
        d_check = d_check == null ? new Dimension(0, CheckRenderer.checkDim.height) : d_check;
        Dimension d_label = this.stringDisplayer == null ? null : this.stringDisplayer.getPreferredSize();
        d_label = d_label == null ? new Dimension(0, 0) : d_label;
        return new Dimension(d_check.width + d_label.width, d_check.height < d_label.height ? d_label.height : d_check.height);
    }

    @Override
    public void doLayout() {
        Dimension d_check = this.check == null ? new Dimension(0, 0) : this.check.getPreferredSize();
        Dimension d_label = this.stringDisplayer == null ? new Dimension(0, 0) : this.stringDisplayer.getPreferredSize();
        int y_check = 0;
        int y_label = 0;
        if (d_check.height < d_label.height) {
            y_check = (d_label.height - d_check.height) / 2;
        } else {
            y_label = (d_check.height - d_label.height) / 2;
        }
        if (this.check != null) {
            this.check.setLocation(0, y_check);
            this.check.setBounds(0, y_check, d_check.width, d_check.height);
            if (checkBounds == null) {
                checkBounds = this.check.getBounds();
            }
        }
    }

    public static Rectangle getCheckBoxRectangle() {
        return (Rectangle)checkBounds.clone();
    }

    static {
        Dimension old = new JCheckBox().getPreferredSize();
        checkDim = new Dimension(old.width, old.height - 5);
    }
}

