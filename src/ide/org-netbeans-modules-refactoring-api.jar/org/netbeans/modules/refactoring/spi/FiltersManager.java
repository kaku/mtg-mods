/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

public abstract class FiltersManager {
    public abstract boolean isSelected(String var1);

    public static interface Filterable {
        public boolean filter(FiltersManager var1);
    }

}

