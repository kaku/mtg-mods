/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.project.SourceGroup
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.ToolbarWithOverflow
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.spi.FiltersManager;
import org.netbeans.modules.refactoring.spi.impl.CheckNode;
import org.netbeans.modules.refactoring.spi.impl.CheckNodeListener;
import org.netbeans.modules.refactoring.spi.impl.CheckRenderer;
import org.netbeans.modules.refactoring.spi.impl.FiltersManagerImpl;
import org.netbeans.modules.refactoring.spi.impl.ParametersPanel;
import org.netbeans.modules.refactoring.spi.impl.PreviewManager;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanelContainer;
import org.netbeans.modules.refactoring.spi.ui.ExpandableTreeElement;
import org.netbeans.modules.refactoring.spi.ui.FiltersDescription;
import org.netbeans.modules.refactoring.spi.ui.RefactoringCustomUI;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUI;
import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElementFactory;
import org.netbeans.modules.refactoring.spi.ui.TreeElementFactoryImplementation;
import org.netbeans.modules.refactoring.spi.ui.UI;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.awt.ToolbarWithOverflow;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public class RefactoringPanel
extends JPanel
implements FiltersManagerImpl.FilterChangeListener {
    private static final RequestProcessor RP = new RequestProcessor(RefactoringPanel.class.getName(), 1, false, false);
    private transient JTree tree = null;
    private transient JButton refreshButton = null;
    private transient JToggleButton expandButton = null;
    private transient JButton refactorButton = null;
    private transient JButton cancelButton = null;
    private transient ButtonL buttonListener = null;
    private transient JButton rerunButton = null;
    private final RefactoringUI ui;
    private final boolean isQuery;
    private transient boolean isVisible = false;
    private transient RefactoringSession session = null;
    private transient ParametersPanel parametersPanel = null;
    private transient JScrollPane scrollPane = null;
    private transient JPanel southPanel;
    public JSplitPane splitPane;
    private JPanel left;
    private Action callback = null;
    private static final int MAX_ROWS = 50;
    private transient JToggleButton logicalViewButton = null;
    private transient JToggleButton physicalViewButton = null;
    private transient JToggleButton customViewButton = null;
    private JButton stopButton;
    private transient ProgressListener progressListener;
    private transient ProgressListener fuListener;
    private transient JButton prevMatch = null;
    private transient JButton nextMatch = null;
    private WeakReference<TopComponent> refCallerTC;
    private boolean inited = false;
    private Component customComponent;
    private AtomicBoolean cancelRequest = new AtomicBoolean();
    private FiltersManagerImpl filtersManager;
    private JComponent filterBar;
    private JPanel toolbars;
    static Image PACKAGE_BADGE = ImageUtilities.loadImage((String)"org/netbeans/spi/java/project/support/ui/packageBadge.gif");
    private static final byte LOGICAL = 0;
    private static final byte PHYSICAL = 1;
    private static final byte GRAPHICAL = 2;
    private static final String PREF_VIEW_TYPE = "PREF_VIEW_TYPE";
    private byte currentView;
    RequestProcessor rp;
    private Map<FileObject, Long> timeStamps;
    private int location;

    public RefactoringPanel(RefactoringUI ui) {
        this(ui, null);
    }

    public RefactoringPanel(RefactoringUI ui, TopComponent caller) {
        this.currentView = this.getPrefViewType();
        this.rp = new RequestProcessor();
        this.timeStamps = new HashMap<FileObject, Long>();
        if (caller != null) {
            this.refCallerTC = new WeakReference<TopComponent>(caller);
        }
        this.ui = ui;
        this.isQuery = ui.isQuery();
        if (this.isQuery) {
            this.fuListener = new FUListener();
            ui.getRefactoring().addProgressListener(this.fuListener);
        }
        this.refresh(true);
    }

    public RefactoringPanel(RefactoringUI ui, RefactoringSession session, Action callback) {
        this.currentView = this.getPrefViewType();
        this.rp = new RequestProcessor();
        this.timeStamps = new HashMap<FileObject, Long>();
        this.session = session;
        this.ui = ui;
        this.isQuery = ui.isQuery();
        this.callback = callback;
        if (this.isQuery) {
            this.fuListener = new FUListener();
            ui.getRefactoring().addProgressListener(this.fuListener);
        }
        this.initialize();
        this.updateFilters();
        this.refresh(false);
    }

    public static void checkEventThread() {
        if (!SwingUtilities.isEventDispatchThread()) {
            ErrorManager.getDefault().notify(1, (Throwable)new IllegalStateException("This must happen in event thread!"));
        }
    }

    private void initialize() {
        JToolBar toolbar;
        GridBagConstraints c;
        if (this.inited) {
            return;
        }
        RefactoringPanel.checkEventThread();
        this.setFocusCycleRoot(true);
        this.splitPane = new JSplitPane(1);
        this.left = new JPanel();
        this.splitPane.setLeftComponent(this.left);
        this.left.setLayout(new BorderLayout());
        this.setLayout(new BorderLayout());
        this.add((Component)this.splitPane, "Center");
        if (!this.isQuery) {
            this.splitPane.setRightComponent(new JLabel(NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_Preview_not_Available"), 0));
        } else {
            this.splitPane.setDividerSize(0);
        }
        this.splitPane.setBorder(null);
        JButton[] buttons = this.getButtons();
        this.southPanel = new JPanel(new GridBagLayout());
        for (int i = 0; i < buttons.length; ++i) {
            c = new GridBagConstraints();
            c.gridy = 0;
            c.insets = new Insets(5, 5, 5, 0);
            this.southPanel.add((Component)buttons[i], c);
        }
        JPanel pp = new JPanel(new BorderLayout());
        c = new GridBagConstraints();
        c.gridy = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.weightx = 1.0;
        c.fill = 2;
        this.southPanel.add((Component)pp, c);
        if (!this.isQuery || this.callback != null) {
            this.left.add((Component)this.southPanel, "South");
        }
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.southPanel.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
        if ((toolbar = this.getToolBar()) != null) {
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                toolbar.setBackground(UIManager.getColor("NbExplorerView.background"));
            }
            this.toolbars = new JPanel(new BorderLayout());
            this.toolbars.add((Component)toolbar, "West");
            this.left.add((Component)this.toolbars, "West");
        }
        this.validate();
        this.inited = true;
    }

    private JToolBar getToolBar() {
        RefactoringPanel.checkEventThread();
        this.refreshButton = new JButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/refresh.png", (boolean)false));
        Dimension dim = new Dimension(24, 24);
        this.refreshButton.setMaximumSize(dim);
        this.refreshButton.setMinimumSize(dim);
        this.refreshButton.setPreferredSize(dim);
        this.refreshButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_refresh"));
        this.refreshButton.setBorderPainted(false);
        this.refreshButton.addActionListener(this.getButtonListener());
        this.expandButton = new JToggleButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/expandTree.png", (boolean)false));
        this.expandButton.setSelectedIcon(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/colapseTree.png", (boolean)false));
        this.expandButton.setMaximumSize(dim);
        this.expandButton.setMinimumSize(dim);
        this.expandButton.setPreferredSize(dim);
        this.expandButton.setSelected(true);
        this.expandButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_expandAll"));
        this.expandButton.setBorderPainted(false);
        this.expandButton.addActionListener(this.getButtonListener());
        this.logicalViewButton = new JToggleButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/logical_view.png", (boolean)false));
        this.logicalViewButton.setMaximumSize(dim);
        this.logicalViewButton.setMinimumSize(dim);
        this.logicalViewButton.setPreferredSize(dim);
        this.logicalViewButton.setSelected(this.currentView == 0);
        this.logicalViewButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_logicalView"));
        this.logicalViewButton.setBorderPainted(false);
        this.logicalViewButton.addActionListener(this.getButtonListener());
        this.physicalViewButton = new JToggleButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/file_view.png", (boolean)false));
        this.physicalViewButton.setMaximumSize(dim);
        this.physicalViewButton.setMinimumSize(dim);
        this.physicalViewButton.setPreferredSize(dim);
        this.physicalViewButton.setSelected(this.currentView == 1);
        this.physicalViewButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_physicalView"));
        this.physicalViewButton.setBorderPainted(false);
        this.physicalViewButton.addActionListener(this.getButtonListener());
        if (!Utilities.isMac()) {
            this.refreshButton.setMnemonic(NbBundle.getMessage(RefactoringPanel.class, (String)"MNEM_refresh").charAt(0));
            this.expandButton.setMnemonic(NbBundle.getMessage(RefactoringPanel.class, (String)"MNEM_expandAll").charAt(0));
            this.logicalViewButton.setMnemonic(NbBundle.getMessage(RefactoringPanel.class, (String)"MNEM_logicalView").charAt(0));
            this.physicalViewButton.setMnemonic(NbBundle.getMessage(RefactoringPanel.class, (String)"MNEM_physicalView").charAt(0));
        }
        if (this.ui instanceof RefactoringCustomUI) {
            this.customViewButton = new JToggleButton(((RefactoringCustomUI)((Object)this.ui)).getCustomIcon());
            this.customViewButton.setMaximumSize(dim);
            this.customViewButton.setMinimumSize(dim);
            this.customViewButton.setPreferredSize(dim);
            this.customViewButton.setSelected(this.currentView == 2);
            this.customViewButton.setToolTipText(((RefactoringCustomUI)((Object)this.ui)).getCustomToolTip());
            this.customViewButton.setBorderPainted(false);
            this.customViewButton.addActionListener(this.getButtonListener());
        }
        this.nextMatch = new JButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/nextmatch.png", (boolean)false));
        this.nextMatch.setMaximumSize(dim);
        this.nextMatch.setMinimumSize(dim);
        this.nextMatch.setPreferredSize(dim);
        this.nextMatch.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_nextMatch"));
        this.nextMatch.setBorderPainted(false);
        this.nextMatch.addActionListener(this.getButtonListener());
        this.prevMatch = new JButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/prevmatch.png", (boolean)false));
        this.prevMatch.setMaximumSize(dim);
        this.prevMatch.setMinimumSize(dim);
        this.prevMatch.setPreferredSize(dim);
        this.prevMatch.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_prevMatch"));
        this.prevMatch.setBorderPainted(false);
        this.prevMatch.addActionListener(this.getButtonListener());
        this.stopButton = new JButton(ImageUtilities.loadImageIcon((String)"org/netbeans/modules/refactoring/api/resources/stop.png", (boolean)false));
        this.stopButton.setMaximumSize(dim);
        this.stopButton.setMinimumSize(dim);
        this.stopButton.setPreferredSize(dim);
        this.stopButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_stop"));
        this.stopButton.setBorderPainted(false);
        this.stopButton.addActionListener(this.getButtonListener());
        ToolbarWithOverflow toolbar = new ToolbarWithOverflow(1);
        toolbar.setFloatable(false);
        toolbar.add(this.refreshButton);
        toolbar.add(this.stopButton);
        toolbar.add(this.prevMatch);
        toolbar.add(this.nextMatch);
        toolbar.add(this.expandButton);
        toolbar.add(this.logicalViewButton);
        toolbar.add(this.physicalViewButton);
        if (this.ui instanceof RefactoringCustomUI) {
            toolbar.add(this.customViewButton);
        }
        return toolbar;
    }

    private JButton[] getButtons() {
        RefactoringPanel.checkEventThread();
        if (this.isQuery) {
            this.refactorButton = null;
            if (this.callback == null) {
                return new JButton[0];
            }
            this.rerunButton = new JButton((String)this.callback.getValue("Name"));
            this.rerunButton.addActionListener(this.getButtonListener());
            return new JButton[]{this.rerunButton};
        }
        this.refactorButton = new JButton();
        Mnemonics.setLocalizedText((AbstractButton)this.refactorButton, (String)NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_DoRefactor"));
        this.refactorButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_DoRefactor"));
        this.refactorButton.addActionListener(this.getButtonListener());
        this.cancelButton = new JButton(NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_CancelRefactor"));
        Mnemonics.setLocalizedText((AbstractButton)this.cancelButton, (String)NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_CancelRefactor"));
        this.cancelButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_CancelRefactor"));
        this.cancelButton.addActionListener(this.getButtonListener());
        return new JButton[]{this.refactorButton, this.cancelButton};
    }

    void switchToLogicalView() {
        this.logicalViewButton.setSelected(true);
        if (this.currentView == 0) {
            return;
        }
        this.currentView = 0;
        this.physicalViewButton.setSelected(false);
        if (this.customViewButton != null) {
            this.customViewButton.setSelected(false);
            this.prevMatch.setEnabled(true);
            this.nextMatch.setEnabled(true);
            this.expandButton.setEnabled(true);
        }
        this.storePrefViewType();
        this.refresh(false);
    }

    void switchToPhysicalView() {
        this.physicalViewButton.setSelected(true);
        if (this.currentView == 1) {
            return;
        }
        this.currentView = 1;
        this.logicalViewButton.setSelected(false);
        if (this.customViewButton != null) {
            this.customViewButton.setSelected(false);
            this.prevMatch.setEnabled(true);
            this.nextMatch.setEnabled(true);
            this.expandButton.setEnabled(true);
        }
        this.storePrefViewType();
        this.refresh(false);
    }

    void switchToCustomView() {
        this.customViewButton.setSelected(true);
        if (this.currentView == 2) {
            return;
        }
        this.currentView = 2;
        this.logicalViewButton.setSelected(false);
        this.physicalViewButton.setSelected(false);
        this.prevMatch.setEnabled(false);
        this.nextMatch.setEnabled(false);
        this.expandButton.setEnabled(false);
        this.refresh(false);
    }

    private CheckNode createNode(TreeElement representedObject, Map<Object, CheckNode> nodes, CheckNode root) {
        boolean isLogical = this.currentView == 0;
        CheckNode node = null;
        node = representedObject instanceof SourceGroup ? nodes.get((Object)((SourceGroup)representedObject).getRootFolder()) : nodes.get(representedObject);
        if (node != null) {
            return node;
        }
        TreeElement parent = representedObject.getParent(isLogical);
        String displayName = representedObject.getText(isLogical);
        Icon icon = representedObject.getIcon();
        node = new CheckNode(representedObject, displayName, icon, this.isQuery);
        final CheckNode parentNode = parent == null ? root : this.createNode(parent, nodes, root);
        parentNode.add(node);
        if (this.isQuery) {
            final int childCount = parentNode.getChildCount();
            try {
                SwingUtilities.invokeAndWait(new Runnable(){

                    @Override
                    public void run() {
                        if (RefactoringPanel.this.tree != null) {
                            ((DefaultTreeModel)RefactoringPanel.this.tree.getModel()).nodesWereInserted(parentNode, new int[]{childCount - 1});
                            RefactoringPanel.this.tree.expandPath(new TreePath(parentNode.getPath()));
                        }
                    }
                });
            }
            catch (InterruptedException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (InvocationTargetException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if (representedObject instanceof SourceGroup) {
            nodes.put((Object)((SourceGroup)representedObject).getRootFolder(), node);
        } else {
            nodes.put(representedObject, node);
        }
        return node;
    }

    private void refactor() {
        RefactoringPanel.checkEventThread();
        if (!this.checkTimeStamps()) {
            if (JOptionPane.showConfirmDialog(this, NbBundle.getMessage(RefactoringPanel.class, (String)"MSG_ConfirmRefresh"), NbBundle.getMessage(RefactoringPanel.class, (String)"MSG_FileModified"), 2) == 0) {
                this.refresh(true);
                return;
            }
            return;
        }
        this.disableComponents();
        this.progressListener = new ProgressL();
        RP.post(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {}
                catch (Throwable var1_1) {
                    RefactoringPanel.this.session.removeProgressListener(RefactoringPanel.this.progressListener);
                    RefactoringPanel.this.progressListener.stop(null);
                    RefactoringPanel.this.progressListener = null;
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            RefactoringPanel.this.close();
                        }
                    });
                    throw var1_1;
                }
                RefactoringPanel.this.session.addProgressListener(RefactoringPanel.this.progressListener);
                RefactoringPanel.this.session.doRefactoring(true);
                RefactoringPanel.this.session.removeProgressListener(RefactoringPanel.this.progressListener);
                RefactoringPanel.this.progressListener.stop(null);
                RefactoringPanel.this.progressListener = null;
                SwingUtilities.invokeLater(new );
            }

        });
    }

    private int cancel() {
        RefactoringPanel.checkEventThread();
        this.close();
        return 0;
    }

    void close() {
        Action action;
        if (this.isQuery) {
            RefactoringPanelContainer.getUsagesComponent().removePanel(this);
        } else {
            RefactoringPanelContainer.getRefactoringComponent().removePanel(this);
        }
        if (this.isVisible && (action = (Action)FileUtil.getConfigObject((String)"Actions/Window/org-netbeans-core-windows-actions-SwitchToRecentDocumentAction.instance", Action.class)) != null) {
            action.actionPerformed(new ActionEvent(this, 1001, null));
        }
        this.closeNotify();
    }

    private ButtonL getButtonListener() {
        if (this.buttonListener == null) {
            this.buttonListener = new ButtonL();
        }
        return this.buttonListener;
    }

    public void expandAll() {
        RefactoringPanel.checkEventThread();
        Cursor old = this.getCursor();
        this.expandButton.setEnabled(false);
        this.setCursor(Cursor.getPredefinedCursor(3));
        for (int row = 0; row < this.tree.getRowCount(); ++row) {
            this.tree.expandRow(row);
        }
        this.setCursor(old);
        this.expandButton.setEnabled(true);
        this.expandButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_collapseAll"));
    }

    public void collapseAll() {
        RefactoringPanel.checkEventThread();
        this.expandButton.setEnabled(false);
        Cursor old = this.getCursor();
        this.setCursor(Cursor.getPredefinedCursor(3));
        for (int row = this.tree.getRowCount() - 1; row > 0; --row) {
            this.tree.collapseRow(row);
        }
        this.setCursor(old);
        this.expandButton.setEnabled(true);
        this.expandButton.setToolTipText(NbBundle.getMessage(RefactoringPanel.class, (String)"HINT_expandAll"));
    }

    private void refresh(final boolean showParametersPanel) {
        RefactoringPanel.checkEventThread();
        boolean scanning = IndexingManager.getDefault().isIndexing();
        if (showParametersPanel) {
            RefactoringSession tempSession;
            if (this.parametersPanel == null) {
                this.parametersPanel = new ParametersPanel(this.ui);
            }
            if ((tempSession = this.parametersPanel.showDialog()) == null) {
                if (!this.parametersPanel.isCanceledDialog()) {
                    this.close();
                }
                return;
            }
            if (tempSession.getRefactoringElements().isEmpty() && !scanning && !this.isQuery) {
                DialogDescriptor nd = new DialogDescriptor((Object)NbBundle.getMessage(ParametersPanel.class, (String)"MSG_NoPatternsFound"), this.ui.getName(), true, new Object[]{DialogDescriptor.OK_OPTION}, DialogDescriptor.OK_OPTION, 0, this.ui.getHelpCtx(), null);
                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)nd);
                return;
            }
            this.session = tempSession;
        }
        final RefactoringPanelContainer cont = this.isQuery ? RefactoringPanelContainer.getUsagesComponent() : RefactoringPanelContainer.getRefactoringComponent();
        cont.makeBusy(true);
        final AtomicInteger size = new AtomicInteger();
        final AtomicBoolean sizeIsApproximate = new AtomicBoolean();
        this.initialize();
        if (showParametersPanel) {
            this.updateFilters();
        }
        this.cancelRequest.set(false);
        this.stopButton.setVisible(this.isQuery && showParametersPanel);
        this.refreshButton.setVisible(!this.isQuery || !showParametersPanel);
        this.stopButton.setEnabled(showParametersPanel);
        final String description = this.ui.getDescription();
        this.setToolTipText("<html>" + description + "</html>");
        final Collection<RefactoringElement> elements = this.session.getRefactoringElements();
        this.setName(this.ui.getName());
        if (this.ui instanceof RefactoringCustomUI) {
            if (this.customComponent == null) {
                this.customComponent = ((RefactoringCustomUI)((Object)this.ui)).getCustomComponent(elements);
            }
            this.left.remove(this.customComponent);
        }
        final ProgressHandle progressHandle = ProgressHandleFactory.createHandle((String)NbBundle.getMessage(RefactoringPanel.class, (String)(this.isQuery ? "LBL_PreparingUsagesTree" : "LBL_PreparingRefactoringTree")));
        if (this.currentView == 2) {
            assert (this.ui instanceof RefactoringCustomUI);
            assert (this.customComponent != null);
            RefactoringCustomUI cui = (RefactoringCustomUI)((Object)this.ui);
            this.left.remove(this.scrollPane);
            this.left.add(this.customComponent, "Center");
            UI.setComponentForRefactoringPreview(null);
            this.splitPane.validate();
            this.repaint();
            this.tree = null;
        } else {
            RP.post(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 * Unable to fully structure code
                 * Enabled aggressive block sorting
                 * Enabled unnecessary exception pruning
                 * Enabled aggressive exception aggregation
                 * Lifted jumps to return sites
                 */
                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            RefactoringPanel.this.setTreeControlsEnabled(false);
                        }
                    });
                    fileObjects = new HashSet<FileObject>();
                    errorsNum = 0;
                    if (!RefactoringPanel.access$600(RefactoringPanel.this)) {
                        for (RefactoringElement elem : elements) {
                            if (elem.getStatus() != 2 && elem.getStatus() != 3) continue;
                            ++errorsNum;
                        }
                    }
                    errorsDesc = this.getErrorDesc(errorsNum, RefactoringPanel.access$600(RefactoringPanel.this) != false ? size.get() : elements.size(), 0, RefactoringPanel.access$600(RefactoringPanel.this) != false && sizeIsApproximate.get() != false);
                    root = new CheckNode(RefactoringPanel.access$700(RefactoringPanel.this), description + errorsDesc.toString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", ImageUtilities.loadImageIcon((String)("org/netbeans/modules/refactoring/api/resources/" + (RefactoringPanel.access$600(RefactoringPanel.this) != false ? "findusages.png" : "refactoring.gif")), (boolean)false), RefactoringPanel.access$600(RefactoringPanel.this));
                    nodes = new HashMap<K, V>();
                    if (RefactoringPanel.access$600(RefactoringPanel.this) && showParametersPanel) {
                        RefactoringPanel.access$800(RefactoringPanel.this, root, showParametersPanel);
                    }
                    if (RefactoringPanel.access$600(RefactoringPanel.this)) {
                        if (!showParametersPanel) {
                            progressHandle.start();
                        }
                    } else {
                        progressHandle.start(elements.size() / 10);
                    }
                    i = 0;
                    hidden = 0;
                    try {
                        progressStarted = false;
                        it = elements.iterator();
                        while (it.hasNext()) {
                            e = (RefactoringElement)it.next();
                            treeElement = null;
                            if (RefactoringPanel.access$900(RefactoringPanel.this) != null || e.include(RefactoringPanel.access$1000(RefactoringPanel.this))) {
                                treeElement = TreeElementFactory.getTreeElement(e);
                                RefactoringPanel.access$1100(RefactoringPanel.this, treeElement, nodes, root);
                            } else {
                                ++hidden;
                            }
                            occurrences = i + (treeElement instanceof ExpandableTreeElement != false ? ((ExpandableTreeElement)treeElement).estimateChildCount() : 1);
                            hiddenOccurrences = hidden;
                            size.set(occurrences);
                            sizeIsApproximate.compareAndSet(false, treeElement instanceof ExpandableTreeElement);
                            if (!RefactoringPanel.access$600(RefactoringPanel.this) || !showParametersPanel) ** GOTO lbl46
                            if (!RefactoringPanel.access$1200(RefactoringPanel.this).get()) {
                                finished = RefactoringPanel.access$300(RefactoringPanel.this) != null ? APIAccessor.DEFAULT.isFinished(RefactoringPanel.access$300(RefactoringPanel.this)) : true;
                                v0 = last = it.hasNext() == false;
                                if (!progressStarted && (finished || last)) {
                                    progressStarted = true;
                                    progressHandle.start();
                                }
                                if (occurrences % 10 == 0 && !finished || last) {
                                    SwingUtilities.invokeLater(new Runnable(){

                                        @Override
                                        public void run() {
                                            if (RefactoringPanel.this.tree != null) {
                                                root.setNodeLabel(description + 3.this.getErrorDesc(0, occurrences, hiddenOccurrences, RefactoringPanel.this.isQuery && sizeIsApproximate.get()));
                                                if (last) {
                                                    RefactoringPanel.this.tree.repaint();
                                                }
                                            }
                                        }
                                    });
                                }
lbl46: // 4 sources:
                                fileObjects.add(e.getParentFile());
                                if (!RefactoringPanel.access$600(RefactoringPanel.this) && i % 10 == 0) {
                                    progressHandle.progress(i / 10);
                                }
                                ++i;
                                continue;
                            }
                            break;
                        }
                        RefactoringPanel.access$1400(RefactoringPanel.this, fileObjects);
                    }
                    catch (RuntimeException t) {
                        try {
                            RefactoringPanel.access$1500(RefactoringPanel.this);
                            throw t;
                            catch (Error e) {
                                RefactoringPanel.access$1500(RefactoringPanel.this);
                                throw e;
                            }
                        }
                        catch (Throwable var17_19) {
                            progressHandle.finish();
                            cont.makeBusy(false);
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    RefactoringPanel.this.setTreeControlsEnabled(true);
                                    RefactoringPanel.this.stopButton.setEnabled(false);
                                    RefactoringPanel.this.stopButton.setVisible(false);
                                    RefactoringPanel.this.refreshButton.setVisible(true);
                                    if (showParametersPanel) {
                                        RefactoringPanel.this.updateFilters();
                                    }
                                }
                            });
                            throw var17_19;
                        }
                    }
                    progressHandle.finish();
                    cont.makeBusy(false);
                    SwingUtilities.invokeLater(new );
                    if (!RefactoringPanel.access$600(RefactoringPanel.this)) ** GOTO lbl75
                    if (showParametersPanel != false) return;
lbl75: // 2 sources:
                    root.setNodeLabel(description + this.getErrorDesc(errorsNum, RefactoringPanel.access$600(RefactoringPanel.this) != false ? size.get() : elements.size(), hidden, RefactoringPanel.access$600(RefactoringPanel.this) != false && sizeIsApproximate.get() != false).toString());
                    RefactoringPanel.access$1900(RefactoringPanel.this, root, showParametersPanel, elements.size());
                }

                private StringBuffer getErrorDesc(int errorsNum, int occurencesNum, int hiddenNum, boolean occurencesNumApproximate) throws MissingResourceException {
                    StringBuffer errorsDesc = new StringBuffer();
                    errorsDesc.append(" [");
                    errorsDesc.append(occurencesNumApproximate ? NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_OccurencesApproximate", (Object)occurencesNum) : NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_Occurences", (Object)occurencesNum));
                    if (errorsNum > 0) {
                        errorsDesc.append(',');
                        errorsDesc.append(' ');
                        errorsDesc.append("<font color=#CC0000>").append(errorsNum);
                        errorsDesc.append(' ');
                        errorsDesc.append(errorsNum == 1 ? NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_Error") : NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_Errors"));
                        errorsDesc.append("</font>");
                    }
                    if (hiddenNum > 0) {
                        errorsDesc.append(',');
                        errorsDesc.append(' ');
                        errorsDesc.append("<font color=#CC0000>").append(hiddenNum);
                        errorsDesc.append(' ');
                        errorsDesc.append(NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_Hidden"));
                        errorsDesc.append("</font>");
                    }
                    errorsDesc.append(']');
                    return errorsDesc;
                }

            });
        }
        if (!this.isVisible) {
            cont.open();
            cont.requestActive();
            if (this.isQuery && this.parametersPanel != null && !this.parametersPanel.isCreateNewTab()) {
                cont.removePanel(null);
            }
            cont.addPanel(this);
            this.isVisible = true;
        }
        if (!this.isQuery) {
            this.setRefactoringEnabled(false, true);
        }
    }

    private void setTreeControlsEnabled(boolean b) {
        this.expandButton.setEnabled(b);
        this.logicalViewButton.setEnabled(b);
        this.physicalViewButton.setEnabled(b);
        if (this.customViewButton != null) {
            this.customViewButton.setEnabled(b);
        }
    }

    private void setupTree(final CheckNode root, final boolean showParametersPanel, final int size) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                RefactoringPanel.this.createTree(root);
                if (showParametersPanel) {
                    RefactoringPanel.this.splitPane.setDividerLocation(0.3);
                    if (size < 50) {
                        RefactoringPanel.this.expandAll();
                        if (!RefactoringPanel.this.isQuery) {
                            RefactoringPanel.this.selectNextUsage();
                        }
                    } else {
                        RefactoringPanel.this.expandButton.setSelected(false);
                    }
                } else if (RefactoringPanel.this.expandButton.isSelected()) {
                    RefactoringPanel.this.expandAll();
                    if (!RefactoringPanel.this.isQuery) {
                        RefactoringPanel.this.selectNextUsage();
                    }
                } else {
                    RefactoringPanel.this.expandButton.setSelected(false);
                }
                RefactoringPanel.this.tree.setSelectionRow(0);
                RefactoringPanel.this.setRefactoringEnabled(true, true);
                if (RefactoringPanel.this.parametersPanel != null && ((Boolean)RefactoringPanel.this.parametersPanel.getClientProperty("JUMP_TO_FIRST_OCCURENCE")).booleanValue()) {
                    RefactoringPanel.this.selectNextUsage();
                }
            }
        });
    }

    private void storeTimeStamps(Set<FileObject> fileObjects) {
        this.timeStamps.clear();
        for (FileObject fo : fileObjects) {
            this.timeStamps.put(fo, fo.lastModified().getTime());
        }
    }

    private boolean checkTimeStamps() {
        Set<FileObject> modified = this.getModifiedFileObjects();
        for (Map.Entry<FileObject, Long> entry : this.timeStamps.entrySet()) {
            if (modified.contains((Object)entry.getKey())) {
                return false;
            }
            if (!entry.getKey().isValid()) {
                return false;
            }
            if (entry.getKey().lastModified().getTime() == entry.getValue().longValue()) continue;
            return false;
        }
        return true;
    }

    private Set<FileObject> getModifiedFileObjects() {
        HashSet<FileObject> result = new HashSet<FileObject>();
        for (DataObject dob : DataObject.getRegistry().getModified()) {
            result.add(dob.getPrimaryFile());
        }
        return result;
    }

    private void createTree(TreeNode root) throws MissingResourceException {
        if (this.tree == null) {
            this.tree = new JTree(root);
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                this.tree.setBackground(UIManager.getColor("NbExplorerView.background"));
            }
            ToolTipManager.sharedInstance().registerComponent(this.tree);
            this.tree.setCellRenderer(new CheckRenderer(this.isQuery, this.tree.getBackground()));
            String s = NbBundle.getMessage(RefactoringPanel.class, (String)"ACSD_usagesTree");
            this.tree.getAccessibleContext().setAccessibleDescription(s);
            this.tree.getAccessibleContext().setAccessibleName(s);
            CheckNodeListener l = new CheckNodeListener(this.isQuery);
            this.tree.addMouseListener(l);
            this.tree.addKeyListener(l);
            this.tree.setToggleClickCount(0);
            this.tree.setTransferHandler(new TransferHandlerImpl());
            this.scrollPane = new JScrollPane(this.tree);
            this.scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, UIManager.getDefaults().getColor("Separator.background")), BorderFactory.createMatteBorder(0, 1, 1, 1, UIManager.getDefaults().getColor("Separator.foreground"))));
            this.left.add((Component)this.scrollPane, "Center");
            this.validate();
        } else {
            this.tree.setModel(new DefaultTreeModel(root));
        }
        this.tree.setRowHeight((int)((CheckRenderer)this.tree.getCellRenderer()).getPreferredSize().getHeight());
        this.tree.addTreeWillExpandListener(new TreeWillExpandListener(){

            @Override
            public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
                Object last = event.getPath().getLastPathComponent();
                if (last instanceof CheckNode) {
                    ((CheckNode)last).ensureChildrenFilled((DefaultTreeModel)RefactoringPanel.this.tree.getModel());
                }
            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
            }
        });
    }

    private void setupInstantTree(final CheckNode root, boolean showParametersPanel) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                RefactoringPanel.this.createTree(root);
                RefactoringPanel.this.tree.setSelectionRow(0);
                if (RefactoringPanel.this.refactorButton != null) {
                    RefactoringPanel.this.refactorButton.requestFocusInWindow();
                } else if (RefactoringPanel.this.tree != null) {
                    RefactoringPanel.this.tree.requestFocusInWindow();
                }
            }
        });
    }

    void setRefactoringEnabled(boolean enabled, boolean isRefreshing) {
        RefactoringPanel.checkEventThread();
        if (this.tree != null) {
            if (!enabled) {
                CheckNode c = (CheckNode)this.tree.getModel().getRoot();
                if (!isRefreshing) {
                    c.setNeedsRefresh();
                } else {
                    c.setDisabled();
                }
                this.tree.setModel(new DefaultTreeModel(c, false));
            }
            this.tree.setEnabled(enabled);
            if (this.refactorButton != null) {
                this.refactorButton.setEnabled(enabled);
            }
        }
        if (this.refactorButton != null) {
            this.refactorButton.requestFocusInWindow();
        } else if (this.tree != null) {
            this.tree.requestFocusInWindow();
        }
    }

    private void disableComponent(JComponent jc) {
        if (jc != null) {
            jc.setEnabled(false);
        }
    }

    private void disableComponents() {
        this.disableComponent(this.cancelButton);
        this.disableComponent(this.expandButton);
        this.disableComponent(this.filterBar);
        this.disableComponent(this.logicalViewButton);
        this.disableComponent(this.nextMatch);
        this.disableComponent(this.physicalViewButton);
        this.disableComponent(this.prevMatch);
        this.disableComponent(this.refactorButton);
        this.disableComponent(this.refreshButton);
        this.disableComponent(this.rerunButton);
        this.disableComponent(this.stopButton);
        this.disableComponent(this.tree);
    }

    void selectNextUsage() {
        CheckNodeListener.selectNextPrev(true, this.isQuery, this.tree);
    }

    void selectPrevUsage() {
        CheckNodeListener.selectNextPrev(false, this.isQuery, this.tree);
    }

    public void storeDividerLocation() {
        if (this.splitPane.getRightComponent() != null) {
            this.location = this.splitPane.getDividerLocation();
        }
    }

    public void restoreDeviderLocation() {
        if (this.splitPane.getRightComponent() != null) {
            this.splitPane.setDividerLocation(this.location);
        }
    }

    private byte getPrefViewType() {
        Preferences prefs = NbPreferences.forModule(RefactoringPanel.class);
        return (byte)prefs.getInt("PREF_VIEW_TYPE", 1);
    }

    private void storePrefViewType() {
        assert (this.currentView != 2);
        Preferences prefs = NbPreferences.forModule(RefactoringPanel.class);
        prefs.putInt("PREF_VIEW_TYPE", this.currentView);
    }

    private void updateFilters() {
        FiltersDescription desc;
        if (!this.ui.isQuery() || this.callback != null) {
            if (this.filterBar != null) {
                this.toolbars.remove(this.filterBar);
                this.filterBar = null;
                this.filtersManager = null;
            }
            return;
        }
        if (this.filtersManager != null) {
            this.toolbars.remove(this.filterBar);
            this.filterBar = null;
            this.filtersManager = null;
        }
        this.filtersManager = FiltersManagerImpl.create((desc = APIAccessor.DEFAULT.getFiltersDescription(this.ui.getRefactoring())) == null ? new FiltersDescription() : desc);
        this.filterBar = this.filtersManager.getComponent(new JToggleButton[0]);
        this.toolbars.add((Component)this.filterBar, "East");
        this.filtersManager.hookChangeListener(this);
        this.toolbars.validate();
    }

    @Override
    public void filterStateChanged(ChangeEvent e) {
        this.refresh(false);
    }

    private void stopSearch() {
        if (this.isVisible) {
            this.stopButton.setEnabled(false);
            this.stopButton.setVisible(false);
            this.refreshButton.setVisible(true);
        }
        this.cancelRequest.set(true);
        this.ui.getRefactoring().cancelRequest();
    }

    private static String normalize(String input) {
        int size = input.length();
        char[] c = new char[size];
        input.getChars(0, size, c, 0);
        boolean wb = false;
        int pos = 0;
        char[] nc = new char[size];
        for (int i = 0; i < size; ++i) {
            if (Character.isWhitespace(c[i])) {
                if (wb) continue;
                nc[pos++] = 32;
                wb = true;
                continue;
            }
            nc[pos++] = c[i];
            wb = false;
        }
        return new String(nc, 0, pos);
    }

    protected void closeNotify() {
        if (this.fuListener != null) {
            this.stopSearch();
            this.ui.getRefactoring().removeProgressListener(this.fuListener);
            this.fuListener.stop(null);
            this.fuListener = null;
        }
        this.timeStamps.clear();
        if (this.tree != null) {
            ToolTipManager.sharedInstance().unregisterComponent(this.tree);
            this.scrollPane.getViewport().remove(this.tree);
        }
        if (this.scrollPane != null) {
            this.scrollPane.setViewport(null);
        }
        this.cleanupTreeElements();
        PreviewManager.getDefault().clean(this);
        this.tree = null;
        this.session = null;
        this.parametersPanel = null;
    }

    private void cleanupTreeElements() {
        for (TreeElementFactoryImplementation tefi : Lookup.getDefault().lookupAll(TreeElementFactoryImplementation.class)) {
            tefi.cleanUp();
        }
    }

    static /* synthetic */ RefactoringUI access$700(RefactoringPanel x0) {
        return x0.ui;
    }

    static /* synthetic */ void access$800(RefactoringPanel x0, CheckNode x1, boolean x2) {
        x0.setupInstantTree(x1, x2);
    }

    static /* synthetic */ FiltersManagerImpl access$1000(RefactoringPanel x0) {
        return x0.filtersManager;
    }

    static /* synthetic */ CheckNode access$1100(RefactoringPanel x0, TreeElement x1, Map x2, CheckNode x3) {
        return x0.createNode(x1, x2, x3);
    }

    static /* synthetic */ AtomicBoolean access$1200(RefactoringPanel x0) {
        return x0.cancelRequest;
    }

    static /* synthetic */ void access$1400(RefactoringPanel x0, Set x1) {
        x0.storeTimeStamps(x1);
    }

    static /* synthetic */ void access$1500(RefactoringPanel x0) {
        x0.cleanupTreeElements();
    }

    static /* synthetic */ void access$1900(RefactoringPanel x0, CheckNode x1, boolean x2, int x3) {
        x0.setupTree(x1, x2, x3);
    }

    private static class Html2Text
    extends HTMLEditorKit.ParserCallback {
        StringBuffer s;

        public void parse(Reader in) throws IOException {
            this.s = new StringBuffer();
            ParserDelegator delegator = new ParserDelegator();
            delegator.parse(in, this, Boolean.TRUE);
        }

        @Override
        public void handleText(char[] text, int pos) {
            this.s.append(text);
        }

        public String getText() {
            return this.s.toString();
        }
    }

    private static class ResultTransferable
    implements Transferable {
        private static DataFlavor[] stringFlavors;
        private static DataFlavor[] plainFlavors;
        private static DataFlavor[] htmlFlavors;
        protected String plainData;
        protected String htmlData;

        public ResultTransferable(String plainData, String htmlData) {
            this.plainData = plainData;
            this.htmlData = htmlData;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            int nHtml = this.isHtmlSupported() ? htmlFlavors.length : 0;
            int nPlain = this.isPlainSupported() ? plainFlavors.length : 0;
            int nString = this.isPlainSupported() ? stringFlavors.length : 0;
            int nFlavors = nHtml + nPlain + nString;
            DataFlavor[] flavors = new DataFlavor[nFlavors];
            int nDone = 0;
            if (nHtml > 0) {
                System.arraycopy(htmlFlavors, 0, flavors, nDone, nHtml);
                nDone += nHtml;
            }
            if (nPlain > 0) {
                System.arraycopy(plainFlavors, 0, flavors, nDone, nPlain);
                nDone += nPlain;
            }
            if (nString > 0) {
                System.arraycopy(stringFlavors, 0, flavors, nDone, nString);
                nDone += nString;
            }
            return flavors;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            DataFlavor[] flavors = this.getTransferDataFlavors();
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (this.isHtmlFlavor(flavor)) {
                String html = this.getHtmlData();
                String string = html = html == null ? "" : html;
                if (String.class.equals(flavor.getRepresentationClass())) {
                    return html;
                }
                if (Reader.class.equals(flavor.getRepresentationClass())) {
                    return new StringReader(html);
                }
                if (InputStream.class.equals(flavor.getRepresentationClass())) {
                    return new StringBufferInputStream(html);
                }
            } else if (this.isPlainFlavor(flavor)) {
                String data = this.getPlainData();
                String string = data = data == null ? "" : data;
                if (String.class.equals(flavor.getRepresentationClass())) {
                    return data;
                }
                if (Reader.class.equals(flavor.getRepresentationClass())) {
                    return new StringReader(data);
                }
                if (InputStream.class.equals(flavor.getRepresentationClass())) {
                    return new StringBufferInputStream(data);
                }
            } else if (this.isStringFlavor(flavor)) {
                String data = this.getPlainData();
                data = data == null ? "" : data;
                return data;
            }
            throw new UnsupportedFlavorException(flavor);
        }

        protected boolean isPlainFlavor(DataFlavor flavor) {
            DataFlavor[] flavors = plainFlavors;
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        protected boolean isPlainSupported() {
            return this.plainData != null;
        }

        protected String getPlainData() {
            return this.plainData;
        }

        protected boolean isStringFlavor(DataFlavor flavor) {
            DataFlavor[] flavors = stringFlavors;
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        protected boolean isHtmlFlavor(DataFlavor flavor) {
            DataFlavor[] flavors = htmlFlavors;
            for (int i = 0; i < flavors.length; ++i) {
                if (!flavors[i].equals(flavor)) continue;
                return true;
            }
            return false;
        }

        protected boolean isHtmlSupported() {
            return this.htmlData != null;
        }

        protected String getHtmlData() {
            return this.htmlData;
        }

        static {
            block2 : {
                try {
                    htmlFlavors = new DataFlavor[3];
                    ResultTransferable.htmlFlavors[0] = new DataFlavor("text/html;class=java.lang.String");
                    ResultTransferable.htmlFlavors[1] = new DataFlavor("text/html;class=java.io.Reader");
                    ResultTransferable.htmlFlavors[2] = new DataFlavor("text/html;charset=unicode;class=java.io.InputStream");
                    plainFlavors = new DataFlavor[3];
                    ResultTransferable.plainFlavors[0] = new DataFlavor("text/plain;class=java.lang.String");
                    ResultTransferable.plainFlavors[1] = new DataFlavor("text/plain;class=java.io.Reader");
                    ResultTransferable.plainFlavors[2] = new DataFlavor("text/plain;charset=unicode;class=java.io.InputStream");
                    stringFlavors = new DataFlavor[2];
                    ResultTransferable.stringFlavors[0] = new DataFlavor("application/x-java-jvm-local-objectref;class=java.lang.String");
                    ResultTransferable.stringFlavors[1] = DataFlavor.stringFlavor;
                }
                catch (ClassNotFoundException cle) {
                    if ($assertionsDisabled) break block2;
                    throw new AssertionError(cle);
                }
            }
        }
    }

    private static class TransferHandlerImpl
    extends TransferHandler {
        private TransferHandlerImpl() {
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            if (c instanceof JTree) {
                JTree tree = (JTree)c;
                TreePath[] paths = tree.getSelectionPaths();
                if (paths == null || paths.length == 0) {
                    return null;
                }
                Html2Text html2Text = new Html2Text();
                StringBuilder plain = new StringBuilder();
                StringBuilder html = new StringBuilder("<html><ul>");
                int depth = 1;
                for (TreePath path : paths) {
                    String label;
                    block8 : {
                        while (depth < path.getPathCount()) {
                            html.append("<ul>");
                            ++depth;
                        }
                        while (depth > path.getPathCount()) {
                            html.append("</ul>");
                            --depth;
                        }
                        Object o = path.getLastPathComponent();
                        if (!(o instanceof CheckNode)) continue;
                        CheckNode node = (CheckNode)o;
                        label = node.getLabel();
                        try {
                            html2Text.parse(new StringReader(label));
                        }
                        catch (IOException ex) {
                            if ($assertionsDisabled) break block8;
                            throw new AssertionError(ex);
                        }
                    }
                    plain.append(html2Text.getText());
                    plain.append("\n");
                    html.append("<li>");
                    html.append(label);
                    html.append("</li>");
                }
                while (depth > 1) {
                    html.append("</ul>");
                    --depth;
                }
                html.append("</ul></html>");
                return new ResultTransferable(plain.toString(), html.toString());
            }
            return null;
        }

        @Override
        public int getSourceActions(JComponent c) {
            return 1;
        }
    }

    private class FUListener
    implements ProgressListener,
    Cancellable {
        private ProgressHandle handle;
        private boolean isIndeterminate;

        private FUListener() {
        }

        @Override
        public void start(ProgressEvent event) {
            this.handle = ProgressHandleFactory.createHandle((String)this.getMessage(event), (Cancellable)this);
            if (event.getCount() == -1) {
                this.handle.start();
                this.handle.switchToIndeterminate();
                this.isIndeterminate = true;
            } else {
                this.handle.start(event.getCount());
                this.isIndeterminate = false;
            }
        }

        @Override
        public void step(ProgressEvent event) {
            if (this.handle == null) {
                return;
            }
            if (this.isIndeterminate && event.getCount() > 0) {
                this.handle.switchToDeterminate(event.getCount());
                this.handle.setDisplayName(this.getMessage(event));
                this.isIndeterminate = false;
            } else {
                this.handle.progress(this.isIndeterminate ? -2 : event.getCount());
            }
        }

        @Override
        public void stop(ProgressEvent event) {
            if (this.handle != null) {
                this.handle.finish();
            }
        }

        private String getMessage(ProgressEvent event) {
            switch (event.getOperationType()) {
                case 2: {
                    return NbBundle.getMessage(ParametersPanel.class, (String)"LBL_ParametersCheck");
                }
                case 3: {
                    return NbBundle.getMessage(ParametersPanel.class, (String)"LBL_Prepare");
                }
                case 1: {
                    return NbBundle.getMessage(ParametersPanel.class, (String)"LBL_PreCheck");
                }
            }
            return NbBundle.getMessage(ParametersPanel.class, (String)"LBL_Usages");
        }

        public boolean cancel() {
            RefactoringPanel.this.stopSearch();
            return true;
        }
    }

    private static class ProgressL
    implements ProgressListener {
        private final ProgressHandle handle;
        private final Dialog d;

        public ProgressL() {
            String lab = NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_RefactorProgressLabel");
            this.handle = ProgressHandleFactory.createHandle((String)lab);
            JComponent progress = ProgressHandleFactory.createProgressComponent((ProgressHandle)this.handle);
            JPanel component = new JPanel();
            component.setLayout(new BorderLayout());
            component.setBorder(new EmptyBorder(12, 12, 11, 11));
            JLabel label = new JLabel(lab);
            label.setBorder(new EmptyBorder(0, 0, 6, 0));
            component.add((Component)label, "North");
            component.add((Component)progress, "Center");
            DialogDescriptor desc = new DialogDescriptor((Object)component, NbBundle.getMessage(RefactoringPanel.class, (String)"LBL_RefactoringInProgress"), true, new Object[0], (Object)null, 0, null, null);
            desc.setLeaf(true);
            this.d = DialogDisplayer.getDefault().createDialog(desc);
            ((JDialog)this.d).setDefaultCloseOperation(0);
        }

        @Override
        public void start(final ProgressEvent event) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ProgressL.this.handle.start(event.getCount());
                    ProgressL.this.d.setVisible(true);
                }
            });
        }

        @Override
        public void step(final ProgressEvent event) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        ProgressL.this.handle.progress(event.getCount());
                    }
                    catch (Throwable e) {
                        ErrorManager.getDefault().notify(1, e);
                    }
                }
            });
        }

        @Override
        public void stop(final ProgressEvent event) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (event != null) {
                        ProgressL.this.handle.finish();
                    }
                    ProgressL.this.d.setVisible(false);
                }
            });
        }

    }

    private class ButtonL
    implements ActionListener {
        private ButtonL() {
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            Object o = event.getSource();
            if (o == RefactoringPanel.this.cancelButton) {
                RefactoringPanel.this.cancel();
            } else if (o == RefactoringPanel.this.refactorButton) {
                RefactoringPanel.this.refactor();
            } else if (o == RefactoringPanel.this.rerunButton) {
                RefactoringPanel.this.close();
                RefactoringPanel.this.callback.actionPerformed(event);
            } else if (o == RefactoringPanel.this.expandButton && RefactoringPanel.this.tree != null) {
                if (RefactoringPanel.this.expandButton.isSelected()) {
                    RefactoringPanel.this.expandAll();
                } else {
                    RefactoringPanel.this.collapseAll();
                }
            } else if (o == RefactoringPanel.this.refreshButton) {
                if (RefactoringPanel.this.callback != null) {
                    RefactoringPanel.this.close();
                    RefactoringPanel.this.callback.actionPerformed(event);
                } else {
                    RefactoringPanel.this.refresh(true);
                }
            } else if (o == RefactoringPanel.this.physicalViewButton) {
                RefactoringPanel.this.switchToPhysicalView();
            } else if (o == RefactoringPanel.this.logicalViewButton) {
                RefactoringPanel.this.switchToLogicalView();
            } else if (o == RefactoringPanel.this.customViewButton) {
                RefactoringPanel.this.switchToCustomView();
            } else if (o == RefactoringPanel.this.nextMatch) {
                RefactoringPanel.this.selectNextUsage();
            } else if (o == RefactoringPanel.this.prevMatch) {
                RefactoringPanel.this.selectPrevUsage();
            } else if (o == RefactoringPanel.this.stopButton) {
                RefactoringPanel.this.stopSearch();
            }
        }
    }

}

