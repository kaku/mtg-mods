/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.TYPE})
@Retention(value=RetentionPolicy.SOURCE)
public @interface ScopeReference {
    public String path();

    public String id() default "";
}

