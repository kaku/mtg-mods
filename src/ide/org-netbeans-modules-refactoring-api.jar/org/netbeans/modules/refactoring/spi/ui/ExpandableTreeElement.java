/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package org.netbeans.modules.refactoring.spi.ui;

import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.openide.util.Cancellable;

public interface ExpandableTreeElement
extends TreeElement,
Iterable<TreeElement>,
Cancellable {
    public int estimateChildCount();
}

