/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Cancellable
 */
package org.netbeans.modules.refactoring.spi;

import javax.swing.Action;
import org.openide.util.Cancellable;

public interface ProblemDetailsImplementation {
    public void showDetails(Action var1, Cancellable var2);

    public String getDetailsHint();
}

