/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Cancellable
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProblemDetails;
import org.netbeans.modules.refactoring.spi.impl.ErrorPanel;
import org.netbeans.modules.refactoring.spi.impl.ParametersPanel;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUI;
import org.openide.awt.Mnemonics;
import org.openide.util.Cancellable;
import org.openide.util.NbBundle;

public class ProblemComponent
extends JPanel {
    private Problem problem;
    private ProblemDetails details;
    private RefactoringUI ui;
    private static double buttonWidth;
    private JLabel icon;
    private JTextArea problemDescription;
    private JButton showDetails;
    private static final float ALTERNATE_ROW_DARKER_FACTOR = 0.96f;

    public ProblemComponent(Problem problem, RefactoringUI ui, boolean single) {
        this.initComponents();
        this.ui = ui;
        this.icon.setIcon(problem.isFatal() ? ErrorPanel.getFatalErrorIcon() : ErrorPanel.getNonfatalErrorIcon());
        this.problemDescription.setText(problem.getMessage());
        this.problem = problem;
        this.details = problem.getDetails();
        if (!single && this.details != null) {
            Mnemonics.setLocalizedText((AbstractButton)this.showDetails, (String)this.details.getDetailsHint());
            this.showDetails.setPreferredSize(new Dimension((int)buttonWidth, (int)this.showDetails.getMinimumSize().getHeight()));
        } else {
            this.showDetails.setVisible(false);
        }
        this.validate();
    }

    static void initButtonSize(Problem problem) {
        buttonWidth = -1.0;
        while (problem != null) {
            ProblemDetails pdi = problem.getDetails();
            if (pdi != null) {
                buttonWidth = Math.max(new JButton(pdi.getDetailsHint()).getMinimumSize().getWidth(), buttonWidth);
            }
            problem = problem.getNext();
        }
    }

    public void setLightBackground() {
        this.problemDescription.setBackground(this.getBackground());
        this.icon.setBackground(this.getBackground());
    }

    public void setDarkBackground() {
        Color bgColor = ProblemComponent.getDarker(this.getBackground());
        this.setBackground(bgColor);
        this.problemDescription.setBackground(bgColor);
        this.icon.setBackground(bgColor);
    }

    private void initComponents() {
        this.icon = new JLabel();
        this.problemDescription = new JTextArea();
        this.showDetails = new JButton();
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.icon.setBackground(UIManager.getDefaults().getColor("TextArea.background"));
        this.icon.setBorder(BorderFactory.createEmptyBorder(1, 6, 1, 6));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        this.add((Component)this.icon, gridBagConstraints);
        this.problemDescription.setEditable(false);
        this.problemDescription.setLineWrap(true);
        this.problemDescription.setWrapStyleWord(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.problemDescription, gridBagConstraints);
        this.problemDescription.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ProblemComponent.class, (String)"ACSD_ProblemDescriptionName"));
        this.problemDescription.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(ProblemComponent.class, (String)"ACSD_ProblemDescriptionDescription"));
        this.showDetails.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProblemComponent.this.showDetailsActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        this.add((Component)this.showDetails, gridBagConstraints);
        this.showDetails.getAccessibleContext().setAccessibleName(this.showDetails.getText());
        this.showDetails.getAccessibleContext().setAccessibleDescription(this.showDetails.getText());
    }

    private void showDetailsActionPerformed(ActionEvent evt) {
        Container c = this;
        while (!(c instanceof ParametersPanel)) {
            c = c.getParent();
        }
        final ParametersPanel parametersPanel = (ParametersPanel)c;
        Cancellable doCloseParent = new Cancellable(){

            public boolean cancel() {
                parametersPanel.cancel.doClick();
                return true;
            }
        };
        ProblemDetails details = this.problem.getDetails();
        if (details != null) {
            details.showDetails(new CallbackAction(this.ui), doCloseParent);
        }
    }

    private static Color getDarker(Color c) {
        if (c.equals(Color.WHITE)) {
            return new Color(244, 244, 244);
        }
        return ProblemComponent.getSafeColor((int)((float)c.getRed() * 0.96f), (int)((float)c.getGreen() * 0.96f), (int)((float)c.getBlue() * 0.96f));
    }

    private static Color getSafeColor(int red, int green, int blue) {
        red = Math.max(red, 0);
        red = Math.min(red, 255);
        green = Math.max(green, 0);
        green = Math.min(green, 255);
        blue = Math.max(blue, 0);
        blue = Math.min(blue, 255);
        return new Color(red, green, blue);
    }

    static class CallbackAction
    extends AbstractAction {
        RefactoringUI ui;

        public CallbackAction(RefactoringUI ui) {
            super(MessageFormat.format(NbBundle.getMessage(ProblemComponent.class, (String)"LBL_Rerun"), ui.getName()));
            this.ui = ui;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            new RefactoringPanel(this.ui).setVisible(true);
        }
    }

}

