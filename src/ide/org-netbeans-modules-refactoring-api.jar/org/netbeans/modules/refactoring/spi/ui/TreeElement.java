/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.ui;

import javax.swing.Icon;

public interface TreeElement {
    public TreeElement getParent(boolean var1);

    public Icon getIcon();

    public String getText(boolean var1);

    public Object getUserObject();
}

