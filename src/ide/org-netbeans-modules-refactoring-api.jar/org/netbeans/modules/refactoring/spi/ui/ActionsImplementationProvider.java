/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi.ui;

import org.openide.util.Lookup;

public abstract class ActionsImplementationProvider {
    public boolean canRename(Lookup lookup) {
        return false;
    }

    public void doRename(Lookup lookup) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    public boolean canFindUsages(Lookup lookup) {
        return false;
    }

    public void doFindUsages(Lookup lookup) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    public boolean canDelete(Lookup lookup) {
        return false;
    }

    public void doDelete(Lookup lookup) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    public boolean canMove(Lookup lookup) {
        return false;
    }

    public void doMove(Lookup lookup) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    public boolean canCopy(Lookup lookup) {
        return false;
    }

    public void doCopy(Lookup lookup) {
        throw new UnsupportedOperationException("Not implemented!");
    }
}

