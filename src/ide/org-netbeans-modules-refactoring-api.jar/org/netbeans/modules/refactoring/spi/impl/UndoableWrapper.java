/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.spi.editor.document.UndoableEditWrapper
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.impl.UndoManager;
import org.netbeans.spi.editor.document.UndoableEditWrapper;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class UndoableWrapper
implements UndoableEditWrapper {
    private AtomicBoolean active = new AtomicBoolean();
    private Map<BaseDocument, UndoableEditDelegate> docToFirst = new HashMap<BaseDocument, UndoableEditDelegate>();
    private RefactoringSession session;

    public UndoableEdit wrap(UndoableEdit ed, Document doc) {
        if (!this.active.get()) {
            return ed;
        }
        Object stream = doc.getProperty("stream");
        DataObject dob = null;
        if (stream != null && stream instanceof DataObject) {
            dob = (DataObject)stream;
        } else if (stream != null && stream instanceof FileObject) {
            FileObject fileObject = (FileObject)stream;
            try {
                dob = DataObject.find((FileObject)fileObject);
            }
            catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        if (dob == null) {
            return ed;
        }
        UndoableEditDelegate current = new UndoableEditDelegate(ed, dob, this.session);
        UndoableEditDelegate first = this.docToFirst.get(doc);
        if (first == null) {
            this.docToFirst.put((BaseDocument)doc, current);
        }
        return current;
    }

    public void close() {
        for (UndoableEditDelegate first : this.docToFirst.values()) {
            first.end();
        }
        this.docToFirst.clear();
    }

    public void setActive(boolean b, RefactoringSession session) {
        this.session = session;
        this.active.set(b);
    }

    public class UndoableEditDelegate
    implements UndoableEdit {
        private UndoManager undoManager;
        private CloneableEditorSupport ces;
        private UndoableEdit delegate;
        private CompoundEdit inner;
        private RefactoringSession session;

        private UndoableEditDelegate(UndoableEdit ed, DataObject dob, RefactoringSession session) {
            this.undoManager = UndoManager.getDefault();
            this.ces = (CloneableEditorSupport)dob.getLookup().lookup(CloneableEditorSupport.class);
            this.inner = new CompoundEdit();
            this.inner.addEdit(ed);
            this.delegate = ed;
            this.session = session;
        }

        @Override
        public void undo() throws CannotUndoException {
            JTextComponent focusedComponent = EditorRegistry.focusedComponent();
            if (focusedComponent != null && focusedComponent.getDocument() == this.ces.getDocument()) {
                this.undoManager.undo(this.session);
            }
            this.inner.undo();
        }

        @Override
        public boolean canUndo() {
            return this.inner.canUndo();
        }

        @Override
        public void redo() throws CannotRedoException {
            JTextComponent focusedComponent = EditorRegistry.focusedComponent();
            if (focusedComponent != null && focusedComponent.getDocument() == this.ces.getDocument()) {
                this.undoManager.redo(this.session);
            }
            this.inner.redo();
        }

        @Override
        public boolean canRedo() {
            return this.inner.canRedo();
        }

        @Override
        public void die() {
            this.inner.die();
        }

        @Override
        public boolean addEdit(UndoableEdit ue) {
            if (ue instanceof List) {
                boolean refatoringEditOnly;
                List listEdit = (List)((Object)ue);
                UndoableEdit topEdit = (UndoableEdit)listEdit.get(listEdit.size() - 1);
                boolean bl = refatoringEditOnly = listEdit.size() == 2;
                if (refatoringEditOnly && topEdit instanceof UndoableEditDelegate) {
                    this.inner.addEdit((UndoableEdit)listEdit.get(0));
                    return true;
                }
                return false;
            }
            return false;
        }

        public UndoableEdit unwrap() {
            return this.delegate;
        }

        @Override
        public boolean replaceEdit(UndoableEdit ue) {
            return this.inner.replaceEdit(ue);
        }

        @Override
        public boolean isSignificant() {
            return this.inner.isSignificant();
        }

        @Override
        public String getPresentationName() {
            return this.undoManager.getUndoDescription(this.session);
        }

        @Override
        public String getUndoPresentationName() {
            return this.undoManager.getUndoDescription(this.session);
        }

        @Override
        public String getRedoPresentationName() {
            return this.undoManager.getRedoDescription(this.session);
        }

        private void end() {
            this.inner.end();
        }
    }

}

