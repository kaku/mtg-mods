/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.ui;

import org.netbeans.modules.refactoring.spi.ui.TreeElement;

public interface TreeElementFactoryImplementation {
    public TreeElement getTreeElement(Object var1);

    public void cleanUp();
}

