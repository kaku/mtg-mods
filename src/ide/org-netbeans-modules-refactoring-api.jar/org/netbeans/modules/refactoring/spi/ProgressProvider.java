/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.modules.refactoring.api.ProgressListener;

public interface ProgressProvider {
    public void addProgressListener(ProgressListener var1);

    public void removeProgressListener(ProgressListener var1);
}

