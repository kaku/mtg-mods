/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.modules.refactoring.spi.impl.FindUsagesOpenAction;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanelContainer;
import org.openide.util.NbBundle;

public class RefactoringPreviewOpenAction
extends AbstractAction {
    public RefactoringPreviewOpenAction() {
        String name = NbBundle.getMessage(FindUsagesOpenAction.class, (String)"LBL_RefactoringWindow");
        this.putValue("Name", name);
        this.putValue("iconBase", "org/netbeans/modules/refactoring/api/resources/refactoringpreview.png");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RefactoringPanelContainer resultView = RefactoringPanelContainer.getRefactoringComponent();
        resultView.open();
        resultView.requestActive();
    }
}

