/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.awt.TabbedPaneFactory
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.openide.awt.MouseUtils;
import org.openide.awt.TabbedPaneFactory;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class RefactoringPanelContainer
extends TopComponent {
    private static RefactoringPanelContainer usages = null;
    private static RefactoringPanelContainer refactorings = null;
    private transient boolean isVisible = false;
    private JPopupMenu pop;
    private PopupListener listener;
    private CloseListener closeL;
    private boolean isRefactoring;
    private static Image REFACTORING_BADGE = ImageUtilities.loadImage((String)"org/netbeans/modules/refactoring/api/resources/refactoringpreview.png");
    private static Image USAGES_BADGE = ImageUtilities.loadImage((String)"org/netbeans/modules/refactoring/api/resources/findusages.png");

    private RefactoringPanelContainer() {
        this("", false);
    }

    private RefactoringPanelContainer(String name, boolean isRefactoring) {
        this.setName(name);
        this.setToolTipText(name);
        this.setFocusable(true);
        this.setLayout((LayoutManager)new BorderLayout());
        this.setMinimumSize(new Dimension(1, 1));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"ACSD_usagesPanel"));
        this.pop = new JPopupMenu();
        this.pop.add(new Close());
        this.pop.add(new CloseAll());
        this.pop.add(new CloseAllButCurrent());
        this.listener = new PopupListener();
        this.closeL = new CloseListener();
        this.isRefactoring = isRefactoring;
        this.setFocusCycleRoot(true);
        JLabel label = new JLabel(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"LBL_NoUsages"));
        label.setHorizontalAlignment(0);
        label.setEnabled(false);
        this.add((Component)label, (Object)"Center");
        this.initActions();
    }

    void addPanel(JPanel panel) {
        RefactoringPanel.checkEventThread();
        if (this.getComponentCount() == 0) {
            this.add((Component)panel, (Object)"Center");
        } else {
            Component comp = this.getComponent(0);
            if (comp instanceof JTabbedPane) {
                ((JTabbedPane)comp).addTab(panel.getName(), null, panel, panel.getToolTipText());
                ((JTabbedPane)comp).setSelectedComponent(panel);
                comp.validate();
            } else if (comp instanceof JLabel) {
                this.remove(comp);
                this.add((Component)panel, (Object)"Center");
            } else {
                this.remove(comp);
                JTabbedPane pane = TabbedPaneFactory.createCloseButtonTabbedPane();
                pane.addMouseListener((MouseListener)((Object)this.listener));
                pane.addPropertyChangeListener(this.closeL);
                this.add((Component)pane, (Object)"Center");
                pane.addTab(comp.getName(), null, comp, ((JPanel)comp).getToolTipText());
                pane.addTab(panel.getName(), null, panel, panel.getToolTipText());
                pane.setSelectedComponent(panel);
                pane.validate();
            }
        }
        if (!this.isVisible) {
            this.isVisible = true;
            this.open();
        }
        this.validate();
        this.requestActive();
    }

    void removePanel(JPanel panel) {
        Component comp;
        RefactoringPanel.checkEventThread();
        Component component = comp = this.getComponentCount() > 0 ? this.getComponent(0) : null;
        if (comp instanceof JTabbedPane) {
            JTabbedPane tabs = (JTabbedPane)comp;
            if (panel == null) {
                panel = (JPanel)tabs.getSelectedComponent();
            }
            tabs.remove(panel);
            if (tabs.getTabCount() == 1) {
                Component c = tabs.getComponentAt(0);
                tabs.removeMouseListener((MouseListener)((Object)this.listener));
                tabs.removePropertyChangeListener(this.closeL);
                this.remove((Component)tabs);
                this.add(c, (Object)"Center");
            }
            this.validate();
        } else {
            if (comp != null) {
                this.remove(comp);
            }
            this.isVisible = false;
            this.close();
        }
    }

    void closeAllButCurrent() {
        Component comp = this.getComponent(0);
        if (comp instanceof JTabbedPane) {
            int i;
            JTabbedPane tabs = (JTabbedPane)comp;
            Component current = tabs.getSelectedComponent();
            int tabCount = tabs.getTabCount();
            Component[] c = new Component[tabCount - 1];
            int j = 0;
            for (i = 0; i < tabCount; ++i) {
                Component tab = tabs.getComponentAt(i);
                if (tab == current) continue;
                c[j++] = tab;
            }
            for (i = 0; i < c.length; ++i) {
                ((RefactoringPanel)c[i]).close();
            }
        }
    }

    public static synchronized RefactoringPanelContainer getUsagesComponent() {
        if (usages == null && (RefactoringPanelContainer.usages = (RefactoringPanelContainer)WindowManager.getDefault().findTopComponent("find-usages")) == null) {
            usages = RefactoringPanelContainer.createUsagesComponent();
        }
        return usages;
    }

    public static synchronized RefactoringPanelContainer getRefactoringComponent() {
        if (refactorings == null && (RefactoringPanelContainer.refactorings = (RefactoringPanelContainer)WindowManager.getDefault().findTopComponent("refactoring-preview")) == null) {
            refactorings = RefactoringPanelContainer.createRefactoringComponent();
        }
        return refactorings;
    }

    public static synchronized RefactoringPanelContainer createRefactoringComponent() {
        if (refactorings == null) {
            refactorings = new RefactoringPanelContainer(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"LBL_Refactoring"), true);
        }
        return refactorings;
    }

    public static synchronized RefactoringPanelContainer createUsagesComponent() {
        if (usages == null) {
            usages = new RefactoringPanelContainer(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"LBL_Usages"), false);
        }
        return usages;
    }

    protected void componentClosed() {
        this.isVisible = false;
        if (this.getComponentCount() == 0) {
            return;
        }
        Component comp = this.getComponent(0);
        if (comp instanceof JTabbedPane) {
            int i;
            JTabbedPane pane = (JTabbedPane)comp;
            Component[] c = new Component[pane.getTabCount()];
            for (i = 0; i < c.length; ++i) {
                c[i] = pane.getComponentAt(i);
            }
            for (i = 0; i < c.length; ++i) {
                ((RefactoringPanel)c[i]).close();
            }
        } else if (comp instanceof RefactoringPanel) {
            ((RefactoringPanel)comp).close();
        }
    }

    protected String preferredID() {
        return "RefactoringPanel";
    }

    public int getPersistenceType() {
        return 0;
    }

    private void initActions() {
        ActionMap map = this.getActionMap();
        map.put("jumpNext", new PrevNextAction(false));
        map.put("jumpPrev", new PrevNextAction(true));
    }

    public RefactoringPanel getCurrentPanel() {
        if (this.getComponentCount() > 0) {
            Component comp = this.getComponent(0);
            if (comp instanceof JTabbedPane) {
                JTabbedPane tabs = (JTabbedPane)comp;
                return (RefactoringPanel)tabs.getSelectedComponent();
            }
            if (comp instanceof RefactoringPanel) {
                return (RefactoringPanel)comp;
            }
        }
        return null;
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(RefactoringPanelContainer.class.getName() + (this.isRefactoring ? ".refactoring-preview" : ".find-usages"));
    }

    public Image getIcon() {
        if (this.isRefactoring) {
            return REFACTORING_BADGE;
        }
        return USAGES_BADGE;
    }

    private class CloseAllButCurrent
    extends AbstractAction {
        public CloseAllButCurrent() {
            super(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"LBL_CloseAllButCurrent"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            RefactoringPanelContainer.this.closeAllButCurrent();
        }
    }

    private final class CloseAll
    extends AbstractAction {
        public CloseAll() {
            super(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"LBL_CloseAll"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            RefactoringPanelContainer.this.close();
        }
    }

    private class Close
    extends AbstractAction {
        public Close() {
            super(NbBundle.getMessage(RefactoringPanelContainer.class, (String)"LBL_CloseWindow"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            RefactoringPanelContainer.this.removePanel(null);
        }
    }

    private class PopupListener
    extends MouseUtils.PopupMouseAdapter {
        private PopupListener() {
        }

        protected void showPopup(MouseEvent e) {
            RefactoringPanelContainer.this.pop.show((Component)((Object)RefactoringPanelContainer.this), e.getX(), e.getY());
        }
    }

    private class CloseListener
    implements PropertyChangeListener {
        private CloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("close".equals(evt.getPropertyName())) {
                RefactoringPanelContainer.this.removePanel((JPanel)evt.getNewValue());
            }
        }
    }

    private final class PrevNextAction
    extends AbstractAction {
        private boolean prev;

        public PrevNextAction(boolean prev) {
            this.prev = prev;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            RefactoringPanel panel = RefactoringPanelContainer.this.getCurrentPanel();
            if (panel != null) {
                if (this.prev) {
                    panel.selectPrevUsage();
                } else {
                    panel.selectNextUsage();
                }
            }
        }
    }

}

