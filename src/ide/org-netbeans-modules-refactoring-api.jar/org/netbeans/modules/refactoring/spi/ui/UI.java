/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.awt.Component;
import javax.swing.Action;
import javax.swing.JSplitPane;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanel;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanelContainer;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUI;
import org.openide.windows.TopComponent;

public final class UI {
    private UI() {
    }

    public static void openRefactoringUI(RefactoringUI ui) {
        new RefactoringPanel(ui);
    }

    public static void openRefactoringUI(RefactoringUI ui, TopComponent callerTC) {
        new RefactoringPanel(ui, callerTC);
    }

    public static void openRefactoringUI(RefactoringUI ui, RefactoringSession callerTC, Action callback) {
        new RefactoringPanel(ui, callerTC, callback).setVisible(true);
    }

    public static boolean setComponentForRefactoringPreview(Component component) {
        TopComponent activated = TopComponent.getRegistry().getActivated();
        RefactoringPanel refactoringPanel = null;
        if (activated instanceof RefactoringPanelContainer) {
            RefactoringPanelContainer panel = (RefactoringPanelContainer)activated;
            refactoringPanel = panel.getCurrentPanel();
        }
        if (refactoringPanel == null) {
            refactoringPanel = RefactoringPanelContainer.getRefactoringComponent().getCurrentPanel();
        }
        if (refactoringPanel == null) {
            refactoringPanel = RefactoringPanelContainer.getUsagesComponent().getCurrentPanel();
        }
        if (refactoringPanel == null) {
            return false;
        }
        if (component == null && refactoringPanel.splitPane.getRightComponent() == null) {
            return false;
        }
        refactoringPanel.storeDividerLocation();
        refactoringPanel.splitPane.setRightComponent(component);
        refactoringPanel.restoreDeviderLocation();
        return true;
    }

    public static enum Constants {
        REQUEST_PREVIEW;
        

        private Constants() {
        }
    }

}

