/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.awt.Component;
import java.util.Collection;
import javax.swing.Icon;
import org.netbeans.modules.refactoring.api.RefactoringElement;

public interface RefactoringCustomUI {
    public Component getCustomComponent(Collection<RefactoringElement> var1);

    public Icon getCustomIcon();

    public String getCustomToolTip();
}

