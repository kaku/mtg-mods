/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.modules.refactoring.api.ProblemDetails;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.spi.ProblemDetailsImplementation;

public class ProblemDetailsFactory {
    private ProblemDetailsFactory() {
    }

    public static ProblemDetails createProblemDetails(ProblemDetailsImplementation pdi) {
        return APIAccessor.DEFAULT.createProblemDetails(pdi);
    }
}

