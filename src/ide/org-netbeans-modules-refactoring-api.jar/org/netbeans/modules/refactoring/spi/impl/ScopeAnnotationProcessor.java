/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import org.netbeans.modules.refactoring.spi.ui.ScopeProvider;
import org.netbeans.modules.refactoring.spi.ui.ScopeReference;
import org.netbeans.modules.refactoring.spi.ui.ScopeReferences;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
@SupportedAnnotationTypes(value={"org.netbeans.modules.refactoring.spi.ui.ScopeDescription", "org.netbeans.modules.refactoring.spi.ui.ScopeReference", "org.netbeans.modules.refactoring.spi.ui.ScopeReferences"})
public class ScopeAnnotationProcessor
extends LayerGeneratingProcessor {
    private static final String ERR_SUPER_TYPE = "The class must extend org.netbeans.modules.refactoring.api.AbstractAnnotatedRefactoring";
    private static final String ERR_ID_NEEDED = "This annotation needs to be used together with ScopeDescription, or you need to specify the id.";

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (!roundEnv.processingOver()) {
            this.generateTypeList(roundEnv);
        }
        return false;
    }

    private void generateTypeList(RoundEnvironment roundEnv) throws LayerGenerationException {
        TypeElement scopeRegistration = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.refactoring.spi.ui.ScopeProvider.Registration");
        TypeElement scopeReference = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.refactoring.spi.ui.ScopeReference");
        TypeElement scopeReferences = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.refactoring.spi.ui.ScopeReferences");
        TypeMirror customProvider = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.refactoring.spi.ui.ScopeProvider.CustomScopeProvider").asType();
        TypeMirror provider = this.processingEnv.getElementUtils().getTypeElement("org.netbeans.modules.refactoring.spi.ui.ScopeProvider").asType();
        if (scopeRegistration == null || scopeReference == null || scopeReferences == null) {
            return;
        }
        Types typeUtils = this.processingEnv.getTypeUtils();
        for (Element annotated22 : roundEnv.getElementsAnnotatedWith(scopeRegistration)) {
            boolean custom;
            if (typeUtils.isSubtype(annotated22.asType(), customProvider)) {
                custom = true;
            } else if (typeUtils.isSubtype(annotated22.asType(), provider)) {
                custom = false;
            } else {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "The class must extend org.netbeans.modules.refactoring.api.AbstractAnnotatedRefactoring", annotated22);
                continue;
            }
            ScopeProvider.Registration ar = annotated22.getAnnotation(ScopeProvider.Registration.class);
            LayerBuilder builder = this.layer(new Element[]{annotated22});
            LayerBuilder.File f = builder.file("Scopes/ScopeDescriptions/" + ar.id() + ".instance");
            f.bundlevalue("displayName", ar.displayName(), (Annotation)ar, "displayName");
            f.stringvalue("id", ar.id());
            f.stringvalue("iconBase", ar.iconBase());
            f.intvalue("position", ar.position());
            f.newvalue("delegate", this.getFQN((TypeElement)annotated22));
            f.stringvalue("instanceClass", "org.netbeans.modules.refactoring.spi.ui.ScopeProvider");
            if (custom) {
                f.methodvalue("instanceCreate", "org.netbeans.modules.refactoring.spi.impl.DelegatingCustomScopeProvider", "create");
            } else {
                f.methodvalue("instanceCreate", "org.netbeans.modules.refactoring.spi.impl.DelegatingScopeProvider", "create");
            }
            f.write();
        }
        for (Element annotated : roundEnv.getElementsAnnotatedWith(ScopeReferences.class)) {
            LayerBuilder builder = this.layer(new Element[]{annotated});
            ScopeReferences refs = annotated.getAnnotation(ScopeReferences.class);
            if (refs == null) continue;
            for (ScopeReference ar : refs.value()) {
                this.processReference(ar, annotated, builder);
            }
        }
        for (Element annotated2 : roundEnv.getElementsAnnotatedWith(scopeReference)) {
            ScopeReference ar = annotated2.getAnnotation(ScopeReference.class);
            this.processReference(ar, annotated2, this.layer(new Element[]{annotated2}));
        }
    }

    private String getFQN(TypeElement clazz) {
        return this.processingEnv.getElementUtils().getBinaryName(clazz).toString();
    }

    private void processReference(ScopeReference ar, Element annotated, LayerBuilder builder) {
        String id = ar.id();
        if (id.isEmpty()) {
            ScopeProvider.Registration desc = annotated.getAnnotation(ScopeProvider.Registration.class);
            if (desc == null) {
                this.processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "This annotation needs to be used together with ScopeDescription, or you need to specify the id.", annotated);
            } else {
                id = desc.id();
            }
        }
        LayerBuilder.File f = builder.file("Scopes/" + ar.path() + "/" + id + ".shadow");
        f.stringvalue("originalFile", "Scopes/ScopeDescriptions/" + id + ".instance");
        f.write();
    }
}

