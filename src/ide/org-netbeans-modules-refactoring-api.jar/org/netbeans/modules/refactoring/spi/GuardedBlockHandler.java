/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import java.util.Collection;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.Transaction;

public interface GuardedBlockHandler {
    public Problem handleChange(RefactoringElementImplementation var1, Collection<RefactoringElementImplementation> var2, Collection<Transaction> var3);
}

