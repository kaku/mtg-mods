/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import org.netbeans.modules.refactoring.spi.impl.RefactoringSubMenuAction;
import org.openide.util.HelpCtx;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;

public class RSMEditorAction
extends SystemAction
implements Presenter.Menu,
Presenter.Popup {
    private final RefactoringSubMenuAction action = new RefactoringSubMenuAction(false);

    public void actionPerformed(ActionEvent ev) {
    }

    public String getName() {
        return (String)this.action.getValue("Name");
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public JMenuItem getMenuPresenter() {
        return this.action.getMenuPresenter();
    }

    public JMenuItem getPopupPresenter() {
        return this.action.getPopupPresenter();
    }
}

