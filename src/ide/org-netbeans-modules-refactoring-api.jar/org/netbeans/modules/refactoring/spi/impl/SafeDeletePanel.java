/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import org.netbeans.modules.refactoring.spi.ui.CustomRefactoringPanel;

public class SafeDeletePanel
extends JPanel
implements CustomRefactoringPanel {
    public SafeDeletePanel() {
        this.initComponents();
    }

    private void initComponents() {
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    @Override
    public void initialize() {
    }

    @Override
    public Component getComponent() {
        return this;
    }
}

