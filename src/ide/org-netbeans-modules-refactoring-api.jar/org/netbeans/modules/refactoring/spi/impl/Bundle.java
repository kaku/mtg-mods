/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String MSG_ConfirmDeleteObject(Object name) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ConfirmDeleteObject", (Object)name);
    }

    static String MSG_ConfirmDeleteObjectTitle() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ConfirmDeleteObjectTitle");
    }

    static String MSG_ConfirmDeleteObjects(Object number_of_objects) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ConfirmDeleteObjects", (Object)number_of_objects);
    }

    static String MSG_ConfirmDeleteObjectsTitle() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_ConfirmDeleteObjectsTitle");
    }

    private void Bundle() {
    }
}

