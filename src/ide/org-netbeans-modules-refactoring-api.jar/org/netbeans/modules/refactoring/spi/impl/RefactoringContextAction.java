/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.refactoring.spi.impl.RefactoringContextActionsProvider;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

public final class RefactoringContextAction
extends AbstractAction
implements ContextAwareAction,
Presenter.Menu,
Presenter.Popup {
    private final Lookup context;

    public RefactoringContextAction() {
        this(null);
    }

    public RefactoringContextAction(Lookup context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new RefactoringContextAction(actionContext);
    }

    public JMenuItem getMenuPresenter() {
        return new InlineMenu(this.context, false);
    }

    public JMenuItem getPopupPresenter() {
        return new InlineMenu(this.context, true);
    }

    private static final class InlineMenu
    extends JMenuItem
    implements DynamicMenuContent {
        private static final JComponent[] EMPTY_CONTENT = new JComponent[0];
        private Lookup context;
        private final boolean popup;

        public InlineMenu(Lookup context, boolean popup) {
            this.context = context;
            this.popup = popup;
        }

        public JComponent[] getMenuPresenters() {
            return this.createMenuItems();
        }

        public JComponent[] synchMenuPresenters(JComponent[] items) {
            JComponent[] comps = new JComponent[1];
            for (JComponent item : items) {
                if (item instanceof Actions.MenuItem) {
                    comps[0] = item;
                    ((Actions.MenuItem)item).synchMenuPresenters(comps);
                    continue;
                }
                if (!(item instanceof JMenu)) continue;
                JMenu jMenu = (JMenu)item;
                for (Component subItem : jMenu.getMenuComponents()) {
                    if (!(subItem instanceof Actions.MenuItem)) continue;
                    comps[0] = (JComponent)subItem;
                    ((Actions.MenuItem)subItem).synchMenuPresenters(comps);
                }
            }
            return this.createMenuItems();
        }

        private JComponent[] createMenuItems() {
            RefactoringContextActionsProvider actionProvider;
            this.resolveContext();
            MimePath mpath = this.resolveMIMEType();
            RefactoringContextActionsProvider refactoringContextActionsProvider = actionProvider = mpath != null ? (RefactoringContextActionsProvider)MimeLookup.getLookup((MimePath)mpath).lookup(RefactoringContextActionsProvider.class) : null;
            if (actionProvider != null) {
                return actionProvider.getMenuItems(this.popup, this.context);
            }
            return EMPTY_CONTENT;
        }

        private void resolveContext() {
            if (this.context == null) {
                this.context = Utilities.actionsGlobalContext();
            }
        }

        private MimePath resolveMIMEType() {
            MimePath mpath = (MimePath)this.context.lookup(MimePath.class);
            if (mpath != null) {
                return mpath;
            }
            FileObject fobj = (FileObject)this.context.lookup(FileObject.class);
            if (fobj != null) {
                return MimePath.parse((String)fobj.getMIMEType());
            }
            DataObject dobj = (DataObject)this.context.lookup(DataObject.class);
            return dobj == null ? null : MimePath.parse((String)dobj.getPrimaryFile().getMIMEType());
        }
    }

}

