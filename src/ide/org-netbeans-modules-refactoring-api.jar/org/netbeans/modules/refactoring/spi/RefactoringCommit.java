/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.impl.CannotRedoRefactoring;
import org.netbeans.modules.refactoring.api.impl.CannotUndoRefactoring;
import org.netbeans.modules.refactoring.api.impl.ProgressSupport;
import org.netbeans.modules.refactoring.spi.BackupFacility2;
import org.netbeans.modules.refactoring.spi.ModificationResult;
import org.netbeans.modules.refactoring.spi.ProgressProvider;
import org.netbeans.modules.refactoring.spi.Transaction;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public final class RefactoringCommit
implements Transaction,
ProgressProvider {
    private static final Logger LOG = Logger.getLogger(RefactoringCommit.class.getName());
    private ProgressSupport progressSupport;
    List<BackupFacility2.Handle> ids = new ArrayList<BackupFacility2.Handle>();
    private boolean commited = false;
    Collection<? extends ModificationResult> results;
    private boolean newFilesStored = false;

    @NonNull
    Collection<? extends FileObject> getModifiedFiles() {
        ArrayList<? extends FileObject> result = new ArrayList<FileObject>();
        for (ModificationResult modification : this.results) {
            result.addAll(modification.getModifiedFileObjects());
        }
        return result;
    }

    public RefactoringCommit(Collection<? extends ModificationResult> results) {
        this.results = results;
    }

    void check(boolean undo) {
        if (!this.commited) {
            return;
        }
        for (BackupFacility2.Handle id : this.ids) {
            try {
                Collection<String> checkChecksum = id.checkChecksum(undo);
                if (checkChecksum.isEmpty()) continue;
                throw undo ? new CannotUndoRefactoring(checkChecksum) : new CannotRedoRefactoring(checkChecksum);
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                continue;
            }
        }
    }

    void sum() {
        if (!this.commited) {
            return;
        }
        for (BackupFacility2.Handle id : this.ids) {
            try {
                id.storeChecksum();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    @Override
    public void commit() {
        this.fireProgressListenerStart(1, this.results.size());
        try {
            if (this.commited) {
                for (BackupFacility2.Handle id : this.ids) {
                    try {
                        id.restore();
                        continue;
                    }
                    catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            } else {
                this.commited = true;
                for (ModificationResult result : this.results) {
                    BackupFacility2.Handle backupid = BackupFacility2.getDefault().backup(result.getModifiedFileObjects());
                    this.ids.add(backupid);
                    BackupFacility2.Handle backupid2 = null;
                    if (!result.getNewFiles().isEmpty()) {
                        backupid2 = BackupFacility2.getDefault().backup(result.getNewFiles().toArray(new File[result.getNewFiles().size()]));
                        this.ids.add(backupid2);
                    }
                    result.commit();
                    backupid.storeChecksum();
                    if (backupid2 != null) {
                        backupid2.storeChecksum();
                    }
                    RefactoringCommit.openNewFiles(result.getNewFiles());
                    this.fireProgressListenerStep();
                }
            }
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        this.fireProgressListenerStop();
    }

    @Override
    public void rollback() {
        for (BackupFacility2.Handle id : this.ids) {
            try {
                id.restore();
                continue;
            }
            catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private static void openNewFiles(Collection<? extends File> newFiles) {
        if (newFiles == null) {
            return;
        }
        for (File file : newFiles) {
            FileObject fo = FileUtil.toFileObject((File)file);
            if (fo == null) continue;
            try {
                DataObject dobj = DataObject.find((FileObject)fo);
                EditorCookie editor = (EditorCookie)dobj.getLookup().lookup(EditorCookie.class);
                if (editor == null) continue;
                editor.open();
            }
            catch (DataObjectNotFoundException ex) {
                LOG.log(Level.INFO, ex.getMessage(), (Throwable)ex);
            }
        }
    }

    @Override
    public synchronized void addProgressListener(ProgressListener listener) {
        if (this.progressSupport == null) {
            this.progressSupport = new ProgressSupport();
        }
        this.progressSupport.addProgressListener(listener);
    }

    @Override
    public synchronized void removeProgressListener(ProgressListener listener) {
        if (this.progressSupport != null) {
            this.progressSupport.removeProgressListener(listener);
        }
    }

    private void fireProgressListenerStart(int type, int count) {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStart(this, type, count);
        }
    }

    private void fireProgressListenerStep() {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStep(this);
        }
    }

    private void fireProgressListenerStop() {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStop(this);
        }
    }
}

