/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.refactoring.api.Scope;
import org.openide.util.Lookup;

public abstract class ScopeProvider {
    public abstract boolean initialize(@NonNull Lookup var1, @NonNull AtomicBoolean var2);

    @CheckForNull
    public abstract Scope getScope();

    @CheckForNull
    public Icon getIcon() {
        return null;
    }

    @CheckForNull
    public String getDetail() {
        return null;
    }

    @Target(value={ElementType.TYPE})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface Registration {
        public String id();

        public int position() default Integer.MAX_VALUE;

        public String displayName();

        public String iconBase() default "";
    }

    public static abstract class CustomScopeProvider
    extends ScopeProvider {
        public abstract void setScope(@NullAllowed Scope var1);

        public abstract boolean showCustomizer();
    }

}

