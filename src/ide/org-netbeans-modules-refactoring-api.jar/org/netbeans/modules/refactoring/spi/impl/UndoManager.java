/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbBundle
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Frame;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.ProgressEvent;
import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.BackupFacility;
import org.openide.util.ChangeSupport;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

public final class UndoManager {
    private LinkedHashMap<RefactoringSession, LinkedList<UndoItem>> undoList;
    private LinkedHashMap<RefactoringSession, LinkedList<UndoItem>> redoList;
    private final ChangeSupport changeSupport;
    private boolean wasUndo;
    private boolean wasRedo;
    private boolean transactionStart;
    private IdentityHashMap<LinkedList, String> descriptionMap;
    private String description;
    private ProgressListener progress;
    private static UndoManager instance;
    public boolean autoConfirm;
    private int stepCounter;

    public void setAutoConfirm(boolean autoConfirm) {
        this.autoConfirm = autoConfirm;
    }

    public static synchronized UndoManager getDefault() {
        if (instance == null) {
            instance = new UndoManager();
        }
        return instance;
    }

    private UndoManager() {
        this.changeSupport = new ChangeSupport((Object)this);
        this.wasUndo = false;
        this.wasRedo = false;
        this.autoConfirm = false;
        this.stepCounter = 0;
        this.undoList = new LinkedHashMap();
        this.redoList = new LinkedHashMap();
        this.descriptionMap = new IdentityHashMap();
    }

    private UndoManager(ProgressListener progress) {
        this();
        this.progress = progress;
    }

    public void setUndoDescription(String desc) {
        this.description = desc;
    }

    public String getUndoDescription(RefactoringSession refactoringSession) {
        RefactoringSession session;
        LinkedList<UndoItem> undoitems;
        if (refactoringSession == null) {
            refactoringSession = this.getLastUndo();
        }
        if ((undoitems = this.undoList.get(session = refactoringSession)) == null) {
            return null;
        }
        return this.descriptionMap.get(undoitems);
    }

    public String getRedoDescription(RefactoringSession refactoringSession) {
        LinkedList<UndoItem> redoitems;
        RefactoringSession session;
        if (refactoringSession == null) {
            refactoringSession = this.getLastUndo();
        }
        if ((redoitems = this.redoList.get(session = refactoringSession)) == null) {
            return null;
        }
        return this.descriptionMap.get(redoitems);
    }

    public void transactionStarted() {
        this.transactionStart = true;
    }

    public void transactionEnded(boolean fail, RefactoringSession session) {
        this.description = null;
        if ((!fail || this.undoList.isEmpty()) && this.isUndoAvailable(session) && this.getUndoDescription(session) == null) {
            this.descriptionMap.remove(this.undoList.remove(session));
        }
        this.fireChange();
    }

    public void undo(RefactoringSession refactoringSession) {
        RefactoringSession session;
        LinkedList<UndoItem> undoitems;
        if (refactoringSession == null) {
            refactoringSession = this.getLastUndo();
        }
        if ((undoitems = this.undoList.get(session = refactoringSession)) != null) {
            final LinkedList<UndoItem> undo = undoitems;
            if (!this.autoConfirm && JOptionPane.showConfirmDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(UndoManager.class, (String)"MSG_ReallyUndo", (Object)this.getUndoDescription(session)), NbBundle.getMessage(UndoManager.class, (String)"MSG_ConfirmUndo"), 0) != 0) {
                throw new CannotUndoException();
            }
            Runnable run = new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    boolean fail = true;
                    try {
                        UndoManager.this.transactionStarted();
                        UndoManager.this.wasUndo = true;
                        UndoManager.this.fireProgressListenerStart(0, undo.size());
                        Iterator undoIterator = undo.iterator();
                        UndoManager.this.redoList.put(session, new LinkedList());
                        UndoManager.this.descriptionMap.put(UndoManager.this.redoList.get(session), UndoManager.this.descriptionMap.remove(undo));
                        try {
                            while (undoIterator.hasNext()) {
                                UndoManager.this.fireProgressListenerStep();
                                UndoItem item = (UndoItem)undoIterator.next();
                                item.undo();
                                if (!(item instanceof SessionUndoItem)) continue;
                                SessionUndoItem sessionUndoItem = (SessionUndoItem)item;
                                UndoManager.this.addItem(item, sessionUndoItem.change);
                            }
                        }
                        catch (CannotUndoException e) {
                            UndoManager.this.descriptionMap.put(undo, UndoManager.this.descriptionMap.get(UndoManager.this.redoList.get(session)));
                            UndoManager.this.descriptionMap.remove(UndoManager.this.redoList.get(session));
                            UndoManager.this.redoList.remove(session);
                            throw e;
                        }
                        UndoManager.this.undoList.remove(session);
                        fail = false;
                    }
                    finally {
                        try {
                            UndoManager.this.wasUndo = false;
                            UndoManager.this.transactionEnded(fail, session);
                        }
                        finally {
                            UndoManager.this.fireProgressListenerStop();
                            UndoManager.this.fireChange();
                        }
                    }
                }
            };
            run.run();
        }
    }

    public void redo(RefactoringSession refactoringSession) {
        LinkedList<UndoItem> redoitems;
        RefactoringSession session;
        if (refactoringSession == null) {
            refactoringSession = this.getLastRedo();
        }
        if ((redoitems = this.redoList.get(session = refactoringSession)) != null) {
            final LinkedList<UndoItem> redo = redoitems;
            if (!this.autoConfirm && JOptionPane.showConfirmDialog(WindowManager.getDefault().getMainWindow(), NbBundle.getMessage(UndoManager.class, (String)"MSG_ReallyRedo", (Object)this.getRedoDescription(session)), NbBundle.getMessage(UndoManager.class, (String)"MSG_ConfirmRedo"), 0) != 0) {
                throw new CannotRedoException();
            }
            Runnable run = new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    boolean fail = true;
                    try {
                        UndoManager.this.transactionStarted();
                        UndoManager.this.wasRedo = true;
                        UndoManager.this.fireProgressListenerStart(1, redo.size());
                        Iterator redoIterator = redo.iterator();
                        UndoManager.this.description = (String)UndoManager.this.descriptionMap.remove(redo);
                        try {
                            while (redoIterator.hasNext()) {
                                UndoManager.this.fireProgressListenerStep();
                                UndoItem item = (UndoItem)redoIterator.next();
                                item.redo();
                                if (!(item instanceof SessionUndoItem)) continue;
                                UndoManager.this.addItem(item, session);
                            }
                        }
                        catch (CannotRedoException ex) {
                            UndoManager.this.descriptionMap.put(redo, UndoManager.this.description);
                            throw ex;
                        }
                        UndoManager.this.redoList.remove(session);
                        fail = false;
                    }
                    finally {
                        try {
                            UndoManager.this.wasRedo = false;
                            UndoManager.this.transactionEnded(fail, session);
                        }
                        finally {
                            UndoManager.this.fireProgressListenerStop();
                            UndoManager.this.fireChange();
                        }
                    }
                }
            };
            run.run();
        }
    }

    public void clear() {
        this.undoList.clear();
        this.redoList.clear();
        this.descriptionMap.clear();
        BackupFacility.getDefault().clear();
        this.fireChange();
    }

    public void addItem(RefactoringSession session) {
        this.addItem(new SessionUndoItem(session), session);
    }

    private void addItem(UndoItem item, RefactoringSession session) {
        if (this.wasUndo) {
            LinkedList<UndoItem> redo = this.redoList.get(session);
            redo.addFirst(item);
        } else {
            if (this.transactionStart) {
                this.undoList.put(session, new LinkedList());
                this.descriptionMap.put(this.undoList.get(session), this.description);
                this.transactionStart = false;
            }
            LinkedList<UndoItem> undo = this.undoList.get(session);
            undo.addFirst(item);
        }
        if (!this.wasUndo && !this.wasRedo) {
            this.redoList.clear();
        }
    }

    public boolean isUndoAvailable(RefactoringSession session) {
        return this.undoList.containsKey(session);
    }

    public boolean isRedoAvailable(RefactoringSession session) {
        return this.redoList.containsKey(session);
    }

    public boolean isUndoAvailable() {
        return !this.undoList.isEmpty();
    }

    public boolean isRedoAvailable() {
        return !this.redoList.isEmpty();
    }

    public void addChangeListener(ChangeListener cl) {
        this.changeSupport.addChangeListener(cl);
    }

    public void removeChangeListener(ChangeListener cl) {
        this.changeSupport.removeChangeListener(cl);
    }

    private void fireChange() {
        this.changeSupport.fireChange();
    }

    private void fireProgressListenerStart(int type, int count) {
        this.stepCounter = 0;
        if (this.progress == null) {
            return;
        }
        this.progress.start(new ProgressEvent(this, 1, type, count));
    }

    private void fireProgressListenerStep() {
        if (this.progress == null) {
            return;
        }
        this.progress.step(new ProgressEvent(this, 2, 0, ++this.stepCounter));
    }

    private void fireProgressListenerStop() {
        if (this.progress == null) {
            return;
        }
        this.progress.stop(new ProgressEvent(this, 4));
    }

    private RefactoringSession getLastUndo() {
        RefactoringSession session2 = null;
        if (!this.undoList.isEmpty()) {
            for (RefactoringSession session2 : this.undoList.keySet()) {
            }
        }
        return session2;
    }

    private RefactoringSession getLastRedo() {
        RefactoringSession session2 = null;
        if (!this.redoList.isEmpty()) {
            for (RefactoringSession session2 : this.redoList.keySet()) {
            }
        }
        return session2;
    }

    private final class SessionUndoItem
    implements UndoItem {
        private RefactoringSession change;

        public SessionUndoItem(RefactoringSession change) {
            this.change = change;
        }

        @Override
        public void undo() {
            this.change.undoRefactoring(true);
        }

        @Override
        public void redo() {
            this.change.doRefactoring(true);
        }
    }

    private static interface UndoItem {
        public void undo();

        public void redo();
    }

}

