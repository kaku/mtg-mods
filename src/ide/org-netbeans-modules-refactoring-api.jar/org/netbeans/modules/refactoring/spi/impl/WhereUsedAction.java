/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import javax.swing.Icon;
import org.netbeans.modules.refactoring.api.impl.ActionsImplementationFactory;
import org.netbeans.modules.refactoring.spi.impl.RefactoringGlobalAction;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class WhereUsedAction
extends RefactoringGlobalAction {
    public WhereUsedAction() {
        super(NbBundle.getMessage(WhereUsedAction.class, (String)"LBL_WhereUsedAction"), null);
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    @Override
    public final void performAction(Lookup context) {
        ActionsImplementationFactory.doFindUsages(context);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    @Override
    protected boolean enable(Lookup context) {
        return ActionsImplementationFactory.canFindUsages(context);
    }

    @Override
    protected boolean applicable(Lookup context) {
        return ActionsImplementationFactory.canFindUsages(context);
    }
}

