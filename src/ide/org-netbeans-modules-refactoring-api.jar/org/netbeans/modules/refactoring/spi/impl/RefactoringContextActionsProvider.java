/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.awt.AcceleratorBinding
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Component;
import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.awt.AcceleratorBinding;
import org.openide.awt.Actions;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.actions.Presenter;

@MimeLocation(subfolderName="RefactoringActions", instanceProviderClass=RefactoringContextActionsProvider.class)
public final class RefactoringContextActionsProvider
implements InstanceProvider<RefactoringContextActionsProvider> {
    private static final Logger LOG = Logger.getLogger(RefactoringContextActionsProvider.class.getName());
    private final List<FileObject> fileObjectList;
    private JComponent[] menuItems;

    public RefactoringContextActionsProvider() {
        this.fileObjectList = Collections.emptyList();
    }

    public RefactoringContextActionsProvider(List<FileObject> fileObjectList) {
        this.fileObjectList = fileObjectList;
    }

    public RefactoringContextActionsProvider createInstance(List<FileObject> fileObjectList) {
        return new RefactoringContextActionsProvider(fileObjectList);
    }

    public JComponent[] getMenuItems(boolean reset) {
        return this.getMenuItems(reset, null);
    }

    public JComponent[] getMenuItems(boolean reset, Lookup context) {
        assert (EventQueue.isDispatchThread());
        if (this.menuItems == null || reset) {
            List<JComponent> l = this.createMenuItems(context);
            this.menuItems = l.toArray(new JComponent[l.size()]);
        }
        return this.menuItems;
    }

    private List<JComponent> createMenuItems(Lookup context) {
        if (this.fileObjectList.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<JComponent> result = new ArrayList<JComponent>(this.fileObjectList.size() + 1);
        result.addAll(this.retrieveMenuItems(this.fileObjectList, context));
        if (!result.isEmpty()) {
            if (result.get(0) instanceof JSeparator) {
                result.set(0, null);
            } else {
                result.add(0, null);
            }
        }
        return result;
    }

    private static void resolveInstance(Object instance, List<JComponent> result) throws IOException {
        if (instance instanceof Presenter.Popup) {
            JMenuItem temp = ((Presenter.Popup)instance).getPopupPresenter();
            result.add(temp);
        } else if (instance instanceof JSeparator) {
            result.add(null);
        } else if (instance instanceof JComponent) {
            result.add((JComponent)instance);
        } else if (instance instanceof Action) {
            Actions.MenuItem mi = new Actions.MenuItem((Action)instance, true);
            result.add((JComponent)mi);
        } else {
            throw new IOException(String.format("Unsupported instance: %s, class: %s", instance, instance.getClass()));
        }
    }

    private List<JComponent> retrieveMenuItems(List<FileObject> files, Lookup context) {
        LinkedList<JComponent> result = new LinkedList<JComponent>();
        for (FileObject fo : files) {
            try {
                DataFolder dobj;
                if (fo.isFolder()) {
                    dobj = DataFolder.findFolder((FileObject)fo);
                    List children = Arrays.asList(fo.getChildren());
                    children = FileUtil.getOrder(children, (boolean)false);
                    String displayName = (String)fo.getAttribute("displayName");
                    JMenu subMenu = new JMenu(displayName != null ? displayName : dobj.getName());
                    for (JComponent jComponent : this.retrieveMenuItems(children, context)) {
                        subMenu.add(jComponent);
                    }
                    result.add(subMenu);
                    continue;
                }
                dobj = DataObject.find((FileObject)fo);
                InstanceCookie ic = (InstanceCookie)dobj.getLookup().lookup(InstanceCookie.class);
                if (ic == null) continue;
                Object instance = ic.instanceCreate();
                if (instance instanceof Action) {
                    AcceleratorBinding.setAccelerator((Action)((Action)instance), (FileObject)fo);
                }
                if (instance instanceof ContextAwareAction) {
                    instance = ((ContextAwareAction)instance).createContextAwareInstance(context);
                }
                RefactoringContextActionsProvider.resolveInstance(instance, result);
            }
            catch (DataObjectNotFoundException ex) {
                LOG.log(Level.WARNING, fo.toString(), (Throwable)ex);
            }
            catch (IOException ex) {
                LOG.log(Level.WARNING, fo.toString(), ex);
            }
            catch (ClassNotFoundException ex) {
                LOG.log(Level.WARNING, fo.toString(), ex);
            }
        }
        return result;
    }
}

