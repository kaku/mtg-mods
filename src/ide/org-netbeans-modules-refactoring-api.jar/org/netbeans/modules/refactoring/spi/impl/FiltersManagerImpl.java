/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ToolbarWithOverflow
 *  org.openide.util.Mutex
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import org.netbeans.modules.refactoring.spi.FiltersManager;
import org.netbeans.modules.refactoring.spi.ui.FiltersDescription;
import org.openide.awt.ToolbarWithOverflow;
import org.openide.util.Mutex;

public final class FiltersManagerImpl
extends FiltersManager {
    private FiltersComponent comp;

    static FiltersManagerImpl create(FiltersDescription descr) {
        return new FiltersManagerImpl(descr);
    }

    @Override
    public boolean isSelected(String filterName) {
        return this.comp.isSelected(filterName);
    }

    public void setSelected(String filterName, boolean value) {
        this.comp.setFilterSelected(filterName, value);
    }

    public /* varargs */ JToolBar getComponent(JToggleButton ... additionalButtons) {
        if (additionalButtons != null && additionalButtons.length > 0) {
            this.comp.addAdditionalButtons(additionalButtons);
        }
        return this.comp.toolbar;
    }

    public FiltersDescription getDescription() {
        return this.comp.getDescription();
    }

    public void hookChangeListener(FilterChangeListener l) {
        this.comp.hookFilterChangeListener(l);
    }

    private FiltersManagerImpl(FiltersDescription descr) {
        this.comp = new FiltersComponent(descr);
    }

    private class FiltersComponent
    implements ActionListener {
        private List<JToggleButton> toggles;
        private final FiltersDescription filtersDesc;
        private final Object L_LOCK;
        private FilterChangeListener clientL;
        private final Object STATES_LOCK;
        private Map<String, Boolean> filterStates;
        private JToolBar toolbar;

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isSelected(String filterName) {
            Boolean result;
            Object object = this.STATES_LOCK;
            synchronized (object) {
                if (this.filterStates == null) {
                    int index = this.filterIndexForName(filterName);
                    if (index < 0) {
                        return false;
                    }
                    return this.filtersDesc.isEnabled(index) && this.filtersDesc.isSelected(index);
                }
                result = this.filterStates.get(filterName);
            }
            if (result == null) {
                throw new IllegalArgumentException("Filter " + filterName + " not found.");
            }
            return result;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void setFilterSelected(String filterName, boolean value) {
            assert (SwingUtilities.isEventDispatchThread());
            int index = this.filterIndexForName(filterName);
            if (index < 0) {
                throw new IllegalArgumentException("Filter " + filterName + " not found.");
            }
            this.toggles.get(index).setSelected(value);
            Object object = this.STATES_LOCK;
            synchronized (object) {
                this.filterStates.put(filterName, value);
            }
            this.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void hookFilterChangeListener(FilterChangeListener l) {
            Object object = this.L_LOCK;
            synchronized (object) {
                this.clientL = l;
            }
        }

        public FiltersDescription getDescription() {
            return this.filtersDesc;
        }

        FiltersComponent(FiltersDescription descr) {
            this.L_LOCK = new Object();
            this.STATES_LOCK = new Object();
            this.filtersDesc = descr;
            Mutex.EVENT.readAccess(new Runnable(FiltersManagerImpl.this){
                final /* synthetic */ FiltersManagerImpl val$this$0;

                @Override
                public void run() {
                    FiltersComponent.this.initPanel();
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void initPanel() {
            this.toolbar = new ToolbarWithOverflow(1);
            this.toolbar.setFloatable(false);
            this.toolbar.setRollover(true);
            this.toolbar.setBorderPainted(true);
            this.toolbar.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, UIManager.getDefaults().getColor("Separator.foreground")), BorderFactory.createEmptyBorder(0, 1, 0, 0)));
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                this.toolbar.setBackground(UIManager.getColor("NbExplorerView.background"));
            }
            int filterCount = this.filtersDesc.getFilterCount();
            this.toggles = new ArrayList<JToggleButton>(filterCount);
            HashMap<String, Boolean> fStates = new HashMap<String, Boolean>(filterCount * 2);
            for (int i = 0; i < filterCount; ++i) {
                JToggleButton toggleButton = this.createToggle(fStates, i);
                this.toggles.add(toggleButton);
            }
            for (int i22 = 0; i22 < this.toggles.size(); ++i22) {
                JToggleButton curToggle = this.toggles.get(i22);
                curToggle.addActionListener(this);
                this.toolbar.add(curToggle);
            }
            Object i22 = this.STATES_LOCK;
            synchronized (i22) {
                this.filterStates = fStates;
            }
        }

        private void addAdditionalButtons(JToggleButton[] sortButtons) {
            Dimension space = new Dimension(3, 0);
            for (JToggleButton button : sortButtons) {
                Dimension size = new Dimension(21, 21);
                button.setMaximumSize(size);
                button.setMinimumSize(size);
                button.setPreferredSize(size);
                this.toolbar.addSeparator(space);
                this.toolbar.add(button);
            }
        }

        private JToggleButton createToggle(Map<String, Boolean> fStates, int index) {
            boolean isSelected = this.filtersDesc.isSelected(index);
            boolean enabled = this.filtersDesc.isEnabled(index);
            Icon icon = this.filtersDesc.getIcon(index);
            JToggleButton result = new JToggleButton(icon, enabled && isSelected);
            Dimension size = new Dimension(21, 21);
            result.setMaximumSize(size);
            result.setMinimumSize(size);
            result.setPreferredSize(size);
            if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
                result.setBorderPainted(true);
            } else {
                result.setBorderPainted(false);
            }
            result.setToolTipText(this.filtersDesc.getTooltip(index));
            result.setEnabled(enabled);
            fStates.put(this.filtersDesc.getKey(index), isSelected);
            return result;
        }

        private int filterIndexForName(String filterName) {
            int filterCount = this.filtersDesc.getFilterCount();
            for (int i = 0; i < filterCount; ++i) {
                String curName = this.filtersDesc.getKey(i);
                if (!filterName.equals(curName)) continue;
                return i;
            }
            return -1;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            JToggleButton toggle = (JToggleButton)e.getSource();
            int index = this.toggles.indexOf(e.getSource());
            Object object = this.STATES_LOCK;
            synchronized (object) {
                this.filterStates.put(this.filtersDesc.getKey(index), toggle.isSelected());
            }
            this.fireChange();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void fireChange() {
            FilterChangeListener lCopy;
            Object object = this.L_LOCK;
            synchronized (object) {
                if (this.clientL == null) {
                    return;
                }
                lCopy = this.clientL;
            }
            lCopy.filterStateChanged(new ChangeEvent(FiltersManagerImpl.this));
        }

    }

    public static interface FilterChangeListener {
        public void filterStateChanged(ChangeEvent var1);
    }

}

