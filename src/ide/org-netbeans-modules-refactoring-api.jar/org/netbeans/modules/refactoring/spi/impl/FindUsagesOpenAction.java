/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.modules.refactoring.spi.impl.RefactoringPanelContainer;
import org.openide.util.NbBundle;

public class FindUsagesOpenAction
extends AbstractAction {
    public FindUsagesOpenAction() {
        String name = NbBundle.getMessage(FindUsagesOpenAction.class, (String)"LBL_UsagesWindow");
        this.putValue("Name", name);
        this.putValue("iconBase", "org/netbeans/modules/refactoring/api/resources/findusages.png");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RefactoringPanelContainer resultView = RefactoringPanelContainer.getUsagesComponent();
        resultView.open();
        resultView.requestActive();
    }
}

