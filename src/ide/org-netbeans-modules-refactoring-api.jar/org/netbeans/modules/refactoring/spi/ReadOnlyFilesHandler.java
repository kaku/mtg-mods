/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import java.util.Collection;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.RefactoringSession;

public interface ReadOnlyFilesHandler {
    public Problem createProblem(RefactoringSession var1, Collection var2);
}

