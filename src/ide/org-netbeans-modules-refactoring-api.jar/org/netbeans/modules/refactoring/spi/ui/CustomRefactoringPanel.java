/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.awt.Component;

public interface CustomRefactoringPanel {
    public void initialize();

    public Component getComponent();
}

