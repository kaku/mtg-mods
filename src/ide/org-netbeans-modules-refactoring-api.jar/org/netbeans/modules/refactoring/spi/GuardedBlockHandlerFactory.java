/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.spi.GuardedBlockHandler;

public interface GuardedBlockHandlerFactory {
    public GuardedBlockHandler createInstance(AbstractRefactoring var1);
}

