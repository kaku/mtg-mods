/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.spi.RefactoringPlugin;

public interface RefactoringPluginFactory {
    public RefactoringPlugin createInstance(AbstractRefactoring var1);
}

