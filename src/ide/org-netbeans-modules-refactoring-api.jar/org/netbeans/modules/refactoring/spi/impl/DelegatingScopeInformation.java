/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.Icon;
import org.netbeans.modules.refactoring.api.Scope;
import org.openide.util.Lookup;

public interface DelegatingScopeInformation {
    public String getDisplayName();

    public int getPosition();

    public String getId();

    public String getDetail();

    public Icon getIcon();

    public boolean initialize(Lookup var1, AtomicBoolean var2);

    public Scope getScope();
}

