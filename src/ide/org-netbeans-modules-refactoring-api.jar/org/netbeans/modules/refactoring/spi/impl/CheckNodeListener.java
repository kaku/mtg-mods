/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Openable
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.api.actions.Openable;
import org.netbeans.modules.refactoring.api.RefactoringElement;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.impl.CheckNode;
import org.netbeans.modules.refactoring.spi.impl.CheckRenderer;
import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class CheckNodeListener
implements MouseListener,
KeyListener {
    private final boolean isQuery;

    public CheckNodeListener(boolean isQuery) {
        this.isQuery = isQuery;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int y;
        JTree tree = (JTree)e.getSource();
        Point p = e.getPoint();
        int x = e.getX();
        int row = tree.getRowForLocation(x, y = e.getY());
        TreePath path = tree.getPathForRow(row);
        if (path != null) {
            CheckNode node = (CheckNode)path.getLastPathComponent();
            if (this.isQuery) {
                Object o;
                if (e.getClickCount() == 2) {
                    Object o2 = node.getUserObject();
                    if (o2 instanceof Openable) {
                        ((Openable)o2).open();
                    } else if (o2 instanceof TreeElement) {
                        if ((o2 = ((TreeElement)o2).getUserObject()) instanceof RefactoringElement || o2 instanceof FileObject || o2 instanceof Openable) {
                            if (!CheckNodeListener.findInSource(node)) {
                                if (tree.isCollapsed(row)) {
                                    tree.expandRow(row);
                                } else {
                                    tree.collapseRow(row);
                                }
                            }
                        } else if (tree.isCollapsed(row)) {
                            tree.expandRow(row);
                        } else {
                            tree.collapseRow(row);
                        }
                    } else if (tree.isCollapsed(row)) {
                        tree.expandRow(row);
                    } else {
                        tree.collapseRow(row);
                    }
                } else if (e.getClickCount() == 1 && (o = node.getUserObject()) instanceof TreeElement && (o = ((TreeElement)o).getUserObject()) instanceof RefactoringElement) {
                    CheckNodeListener.openDiff(node);
                }
            } else {
                Object o;
                Rectangle chRect = CheckRenderer.getCheckBoxRectangle();
                Rectangle rowRect = tree.getPathBounds(path);
                chRect.setLocation(chRect.x + rowRect.x, chRect.y + rowRect.y);
                if (e.getClickCount() == 1 && chRect.contains(p) && !node.isDisabled()) {
                    boolean isSelected = !node.isSelected();
                    node.setSelected(isSelected);
                    Object o3 = node.getUserObject();
                    if (o3 instanceof TreeElement && (o3 = ((TreeElement)o3).getUserObject()) instanceof RefactoringElement) {
                        CheckNodeListener.openDiff(node);
                    }
                    ((DefaultTreeModel)tree.getModel()).nodeChanged(node);
                    if (row == 0) {
                        tree.revalidate();
                    }
                    tree.repaint();
                } else if (e.getClickCount() == 2 && !chRect.contains(p)) {
                    Object o4 = node.getUserObject();
                    if (o4 instanceof TreeElement) {
                        if ((o4 = ((TreeElement)o4).getUserObject()) instanceof RefactoringElement || o4 instanceof FileObject) {
                            CheckNodeListener.findInSource(node);
                        }
                    } else if (o4 instanceof Openable) {
                        ((Openable)o4).open();
                    } else if (tree.isCollapsed(row)) {
                        tree.expandRow(row);
                    } else {
                        tree.collapseRow(row);
                    }
                } else if (e.getClickCount() == 1 && !chRect.contains(p) && (o = node.getUserObject()) instanceof TreeElement && (o = ((TreeElement)o).getUserObject()) instanceof RefactoringElement) {
                    CheckNodeListener.openDiff(node);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (keyCode == 38 || keyCode == 40) {
            Object o;
            CheckNode node;
            JTree tree = (JTree)e.getSource();
            int row = tree.getSelectionRows()[0];
            TreePath path = tree.getSelectionPath();
            if (path != null && (o = (node = (CheckNode)path.getLastPathComponent()).getUserObject()) instanceof TreeElement && (o = ((TreeElement)o).getUserObject()) instanceof RefactoringElement) {
                CheckNodeListener.openDiff(node);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent event) {
        int y;
        int x;
        int row;
        JTree tree = (JTree)event.getSource();
        TreePath path = tree.getPathForRow(row = tree.getRowForLocation(x = event.getX(), y = event.getY()));
        if (path == null) {
            return;
        }
        CheckNode node = (CheckNode)path.getLastPathComponent();
        if (!SwingUtilities.isRightMouseButton(event)) {
            return;
        }
        Object o = node.getUserObject();
        if (!(o instanceof TreeElement)) {
            return;
        }
        if ((o = ((TreeElement)o).getUserObject()) instanceof RefactoringElement) {
            this.showPopup(((RefactoringElement)o).getLookup().lookupAll(Action.class), tree, x, y);
        }
    }

    private void showPopup(Collection<? extends Action> actions, Component c, int x, int y) {
        if (actions.isEmpty()) {
            return;
        }
        JPopupMenu menu = new JPopupMenu();
        for (Action a : actions) {
            menu.add(a);
        }
        menu.show(c, x, y);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        TreePath path;
        JTree tree;
        int keyCode = e.getKeyCode();
        if (keyCode == 32) {
            JTree tree2 = (JTree)e.getSource();
            TreePath path2 = tree2.getSelectionPath();
            if (path2 != null) {
                CheckNode node;
                node.setSelected(!(node = (CheckNode)path2.getLastPathComponent()).isSelected());
                tree2.repaint();
                e.consume();
            }
        } else if (keyCode == 10 && (path = (tree = (JTree)e.getSource()).getSelectionPath()) != null) {
            CheckNode node = (CheckNode)path.getLastPathComponent();
            CheckNodeListener.findInSource(node);
        }
    }

    static boolean findInSource(CheckNode node) {
        Object o = node.getUserObject();
        if (o instanceof TreeElement) {
            if (o instanceof Openable) {
                ((Openable)o).open();
                return true;
            }
            if ((o = ((TreeElement)o).getUserObject()) instanceof RefactoringElement) {
                APIAccessor.DEFAULT.getRefactoringElementImplementation((RefactoringElement)o).openInEditor();
                return true;
            }
            if (o instanceof Openable) {
                ((Openable)o).open();
                return true;
            }
            if (o instanceof FileObject) {
                try {
                    OpenCookie oc = (OpenCookie)DataObject.find((FileObject)((FileObject)o)).getCookie(OpenCookie.class);
                    if (oc != null) {
                        oc.open();
                        return true;
                    }
                }
                catch (DataObjectNotFoundException ex) {
                    // empty catch block
                }
            }
        }
        return false;
    }

    static void openDiff(CheckNode node) {
        Object o = node.getUserObject();
        if (o instanceof TreeElement && (o = ((TreeElement)o).getUserObject()) instanceof RefactoringElement) {
            APIAccessor.DEFAULT.getRefactoringElementImplementation((RefactoringElement)o).showPreview();
        }
    }

    static void selectNextPrev(boolean next, boolean isQuery, JTree tree) {
        CheckNode node;
        int[] rows = tree.getSelectionRows();
        int newRow = rows == null || rows.length == 0 ? 0 : rows[0];
        int maxcount = tree.getRowCount();
        do {
            TreePath path;
            if (next) {
                if (++newRow >= maxcount) {
                    newRow = 0;
                }
            } else if (--newRow < 0) {
                newRow = maxcount - 1;
            }
            if ((node = (CheckNode)(path = tree.getPathForRow(newRow)).getLastPathComponent()).isLeaf()) continue;
            tree.expandRow(newRow);
            maxcount = tree.getRowCount();
        } while (!node.isLeaf());
        tree.setSelectionRow(newRow);
        tree.scrollRowToVisible(newRow);
        if (isQuery) {
            CheckNodeListener.findInSource(node);
        } else {
            CheckNodeListener.openDiff(node);
        }
    }
}

