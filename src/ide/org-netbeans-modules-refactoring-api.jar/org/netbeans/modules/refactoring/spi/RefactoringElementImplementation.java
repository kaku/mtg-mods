/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.filesystems.FileObject
 *  org.openide.text.PositionBounds
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.api.annotations.common.NonNull;
import org.openide.filesystems.FileObject;
import org.openide.text.PositionBounds;
import org.openide.util.Lookup;

public interface RefactoringElementImplementation {
    public static final int NORMAL = 0;
    public static final int WARNING = 1;
    public static final int GUARDED = 2;
    public static final int READ_ONLY = 3;

    public String getText();

    public String getDisplayText();

    public boolean isEnabled();

    public void setEnabled(boolean var1);

    public void performChange();

    public void undoChange();

    public Lookup getLookup();

    @NonNull
    public FileObject getParentFile();

    public PositionBounds getPosition();

    public int getStatus();

    public void setStatus(int var1);

    public void openInEditor();

    public void showPreview();
}

