/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.fileinfo.NonRecursiveFolder
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.plaf.UIResource;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.modules.refactoring.api.Scope;
import org.netbeans.modules.refactoring.spi.impl.DelegatingCustomScopeProvider;
import org.netbeans.modules.refactoring.spi.impl.DelegatingScopeInformation;
import org.netbeans.modules.refactoring.spi.ui.ScopeProvider;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class ScopePanel
extends JPanel {
    private static final String ELLIPSIS = "\u2026";
    private static final int SCOPE_COMBOBOX_COLUMNS = 14;
    private final String id;
    private final Preferences preferences;
    private final String preferencesKey;
    private ArrayList<DelegatingScopeInformation> scopes;
    private JButton btnCustomScope;
    private JComboBox scopeCombobox;

    @Deprecated
    public ScopePanel() {
        this(null, null, null);
    }

    public ScopePanel(String id, Preferences preferences, String preferencesKey) {
        this.id = id;
        this.preferences = preferences;
        this.preferencesKey = preferencesKey;
        this.scopes = new ArrayList();
        this.initComponents();
    }

    public boolean initialize(Lookup context, AtomicBoolean cancel) {
        this.scopes.clear();
        Collection scopeProviders = Lookups.forPath((String)("Scopes/" + this.id)).lookupAll(ScopeProvider.class);
        final AtomicBoolean customizable = new AtomicBoolean();
        for (ScopeProvider provider : scopeProviders) {
            if (!provider.initialize(context, new AtomicBoolean())) continue;
            this.scopes.add((DelegatingScopeInformation)((Object)provider));
            if (!(provider instanceof ScopeProvider.CustomScopeProvider)) continue;
            customizable.set(true);
        }
        Collections.sort(this.scopes, new Comparator<DelegatingScopeInformation>(){

            @Override
            public int compare(DelegatingScopeInformation o1, DelegatingScopeInformation o2) {
                return o1.getPosition() - o2.getPosition();
            }
        });
        if (!this.scopes.isEmpty()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    int defaultItem;
                    ScopePanel.this.scopeCombobox.setModel(new DefaultComboBoxModel<ScopeProvider>(ScopePanel.this.scopes.toArray(new ScopeProvider[ScopePanel.this.scopes.size()])));
                    ScopePanel.this.btnCustomScope.setVisible(customizable.get());
                    String preselectId = ScopePanel.this.preferences.get(ScopePanel.this.preferencesKey, null);
                    if ((preselectId == null || ScopePanel.isNumeric(preselectId)) && (defaultItem = Integer.valueOf(ScopePanel.this.preferences.getInt(ScopePanel.this.preferencesKey, -1)).intValue()) != -1) {
                        switch (defaultItem) {
                            case 0: {
                                preselectId = "all-projects";
                                break;
                            }
                            case 1: {
                                preselectId = "current-project";
                                break;
                            }
                            case 2: {
                                preselectId = "current-package";
                                break;
                            }
                            case 3: {
                                preselectId = "current-file";
                                break;
                            }
                            case 4: {
                                preselectId = "custom-scope";
                            }
                        }
                    }
                    if (preselectId != null) {
                        ScopePanel.this.selectScopeById(preselectId);
                    } else {
                        ScopePanel.this.selectPreferredScope();
                    }
                }
            });
        }
        return !this.scopes.isEmpty();
    }

    private static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        }
        catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @CheckForNull
    public Scope getSelectedScope() {
        ScopeProvider selectedScope = (ScopeProvider)this.scopeCombobox.getSelectedItem();
        return selectedScope != null ? selectedScope.getScope() : null;
    }

    public void selectScopeById(@NonNull String id) {
        ComboBoxModel m = this.scopeCombobox.getModel();
        for (int i = 0; i < m.getSize(); ++i) {
            Scope s;
            DelegatingScopeInformation sd = (DelegatingScopeInformation)m.getElementAt(i);
            if (!sd.getId().equals(id)) continue;
            if (sd instanceof ScopeProvider.CustomScopeProvider && (s = sd.getScope()) != null && s.getFiles().isEmpty() && s.getFolders().isEmpty() && s.getSourceRoots().isEmpty()) {
                this.selectPreferredScope();
                return;
            }
            this.scopeCombobox.setSelectedItem(sd);
            return;
        }
    }

    private void initComponents() {
        this.btnCustomScope = new JButton();
        this.scopeCombobox = new JComboBox();
        this.btnCustomScope.setAction(new ScopeAction(this.scopeCombobox));
        Mnemonics.setLocalizedText((AbstractButton)this.btnCustomScope, (String)"...");
        this.scopeCombobox.setRenderer(new ScopeDescriptionRenderer());
        ((JTextField)this.scopeCombobox.getEditor().getEditorComponent()).setColumns(14);
        this.scopeCombobox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                ScopePanel.this.scopeComboboxActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.scopeCombobox, 0, 343, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.btnCustomScope)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnCustomScope).addComponent(this.scopeCombobox, -2, -1, -2)));
    }

    private void scopeComboboxActionPerformed(ActionEvent evt) {
        Object selectedItem = this.scopeCombobox.getSelectedItem();
        if (selectedItem instanceof DelegatingScopeInformation) {
            DelegatingScopeInformation scopeInfo = (DelegatingScopeInformation)selectedItem;
            this.preferences.put(this.preferencesKey, scopeInfo.getId());
        } else {
            this.preferences.remove(this.preferencesKey);
        }
    }

    private void selectPreferredScope() {
        ComboBoxModel m = this.scopeCombobox.getModel();
        for (int i = 0; i < m.getSize(); ++i) {
            DelegatingScopeInformation sd = (DelegatingScopeInformation)m.getElementAt(i);
            if (sd.getPosition() < 0) continue;
            this.scopeCombobox.setSelectedItem(sd);
            return;
        }
    }

    private static class ScopeDescriptionRenderer
    extends JLabel
    implements ListCellRenderer,
    UIResource {
        public ScopeDescriptionRenderer() {
            this.setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            this.setName("ComboBox.listRenderer");
            DelegatingScopeInformation scopeDescription = null;
            if (value instanceof DelegatingScopeInformation) {
                scopeDescription = (DelegatingScopeInformation)value;
            }
            if (scopeDescription != null) {
                String detail = scopeDescription.getDetail();
                String displayName = scopeDescription.getDisplayName();
                this.setText(detail == null ? displayName : displayName + " (" + detail + ")");
                this.setIcon(scopeDescription.getIcon());
            }
            if (isSelected) {
                this.setBackground(list.getSelectionBackground());
                this.setForeground(list.getSelectionForeground());
            } else {
                this.setBackground(list.getBackground());
                this.setForeground(list.getForeground());
            }
            return this;
        }

        @Override
        public String getName() {
            String name = super.getName();
            return name == null ? "ComboBox.renderer" : name;
        }
    }

    private class ScopeAction
    extends AbstractAction {
        private final JComboBox scopeCombobox;

        private ScopeAction(JComboBox scopeCombobox) {
            this.scopeCombobox = scopeCombobox;
            this.putValue("Name", "\u2026");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            ComboBoxModel m = this.scopeCombobox.getModel();
            ScopeProvider selectedScope = (ScopeProvider)this.scopeCombobox.getSelectedItem();
            Scope scope = selectedScope.getScope();
            if (selectedScope instanceof DelegatingCustomScopeProvider) {
                this.showCustomizer((DelegatingCustomScopeProvider)selectedScope, scope);
            } else {
                for (int i = 0; i < m.getSize(); ++i) {
                    ScopeProvider sd = (ScopeProvider)m.getElementAt(i);
                    if (!(sd instanceof DelegatingCustomScopeProvider)) continue;
                    this.showCustomizer((DelegatingCustomScopeProvider)sd, scope);
                    break;
                }
            }
        }

        private void showCustomizer(DelegatingCustomScopeProvider csd, Scope scope) {
            csd.setScope(scope);
            if (csd.showCustomizer()) {
                ScopePanel.this.selectScopeById(csd.getId());
            }
        }
    }

}

