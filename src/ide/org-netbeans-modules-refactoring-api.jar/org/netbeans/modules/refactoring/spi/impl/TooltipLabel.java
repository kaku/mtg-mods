/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JToolTip;
import javax.swing.border.Border;

public class TooltipLabel
extends JLabel {
    public TooltipLabel() {
        this.setToolTipText(" ");
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        String text;
        FontMetrics metrix = this.getFontMetrics(this.getFont());
        int textWidth = metrix.stringWidth((text = this.getText()).replaceAll("<[^>]*>", ""));
        return textWidth > this.getParent().getSize().width ? text : null;
    }

    @Override
    public Point getToolTipLocation(MouseEvent event) {
        return new Point(-3, 0);
    }

    @Override
    public JToolTip createToolTip() {
        JToolTip tooltp = new JToolTip();
        tooltp.setBackground(SystemColor.control);
        tooltp.setFont(this.getFont());
        tooltp.setOpaque(true);
        tooltp.setComponent(this);
        tooltp.setBorder(null);
        return tooltp;
    }
}

