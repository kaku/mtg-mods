/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.util.ChangeSupport
 */
package org.netbeans.modules.refactoring.spi.impl;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.spi.impl.UndoManager;
import org.openide.awt.UndoRedo;
import org.openide.util.ChangeSupport;

public class RefactoringUndoRedo
implements UndoRedo {
    private UndoManager manager = UndoManager.getDefault();
    private ChangeSupport pcs;

    public RefactoringUndoRedo() {
        this.pcs = new ChangeSupport((Object)this);
        this.manager.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                RefactoringUndoRedo.this.pcs.fireChange();
            }
        });
    }

    public boolean canUndo() {
        return this.manager.isUndoAvailable();
    }

    public boolean canRedo() {
        return this.manager.isRedoAvailable();
    }

    public void undo() throws CannotUndoException {
        this.manager.undo(null);
    }

    public void redo() throws CannotRedoException {
        this.manager.redo(null);
    }

    public void addChangeListener(ChangeListener l) {
        this.pcs.addChangeListener(l);
    }

    public void removeChangeListener(ChangeListener l) {
        this.pcs.removeChangeListener(l);
    }

    public String getUndoPresentationName() {
        return this.manager.getUndoDescription(null);
    }

    public String getRedoPresentationName() {
        return this.manager.getRedoDescription(null);
    }

}

