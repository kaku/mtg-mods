/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

public interface Transaction {
    public void commit();

    public void rollback();
}

