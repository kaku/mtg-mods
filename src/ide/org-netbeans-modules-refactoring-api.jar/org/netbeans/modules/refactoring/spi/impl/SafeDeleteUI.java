/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.SafeDeleteRefactoring;
import org.netbeans.modules.refactoring.spi.impl.SafeDeletePanel;
import org.netbeans.modules.refactoring.spi.ui.CustomRefactoringPanel;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUI;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public class SafeDeleteUI<T>
implements RefactoringUI {
    private final T[] elementsToDelete;
    private final SafeDeleteRefactoring refactoring;
    private SafeDeletePanel panel;
    private ResourceBundle bundle;

    public SafeDeleteUI(T[] selectedElements) {
        this.elementsToDelete = selectedElements;
        this.refactoring = new SafeDeleteRefactoring(Lookups.fixed((Object[])this.elementsToDelete));
    }

    @Override
    public Problem checkParameters() {
        return this.refactoring.fastCheckParameters();
    }

    @Override
    public String getDescription() {
        return NbBundle.getMessage(SafeDeleteUI.class, (String)"LBL_SafeDel");
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx(SafeDeleteUI.class.getName());
    }

    @Override
    public String getName() {
        return NbBundle.getMessage(SafeDeleteUI.class, (String)"LBL_SafeDel");
    }

    @Override
    public CustomRefactoringPanel getPanel(ChangeListener parent) {
        if (this.panel == null) {
            this.panel = new SafeDeletePanel();
        }
        return this.panel;
    }

    @Override
    public AbstractRefactoring getRefactoring() {
        return this.refactoring;
    }

    @Override
    public boolean hasParameters() {
        return false;
    }

    @Override
    public boolean isQuery() {
        return false;
    }

    @Override
    public Problem setParameters() {
        return this.refactoring.checkParameters();
    }

    private String getString(String key) {
        if (this.bundle == null) {
            this.bundle = NbBundle.getBundle(SafeDeleteUI.class);
        }
        return this.bundle.getString(key);
    }

    private String getString(String key, Object value) {
        return new MessageFormat(this.getString(key)).format(new Object[]{value});
    }
}

