/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.guards.GuardedSection
 *  org.netbeans.api.editor.guards.GuardedSectionManager
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.UserQuestionException
 */
package org.netbeans.modules.refactoring.spi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.api.editor.guards.GuardedSectionManager;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.SingleCopyRefactoring;
import org.netbeans.modules.refactoring.api.WhereUsedQuery;
import org.netbeans.modules.refactoring.api.impl.APIAccessor;
import org.netbeans.modules.refactoring.api.impl.SPIAccessor;
import org.netbeans.modules.refactoring.spi.AccessorImpl;
import org.netbeans.modules.refactoring.spi.GuardedBlockHandler;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.Transaction;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.UserQuestionException;

public final class RefactoringElementsBag {
    ArrayList<Transaction> commits;
    ArrayList<RefactoringElementImplementation> fileChanges;
    boolean hasGuarded = false;
    boolean hasReadOnly = false;
    private final List<RefactoringElementImplementation> delegate;
    private final RefactoringSession session;
    private Collection<FileObject> readOnlyFiles = new HashSet<FileObject>();

    RefactoringElementsBag(RefactoringSession session, List<RefactoringElementImplementation> delegate) {
        this.session = session;
        this.delegate = delegate;
        this.commits = new ArrayList();
        this.fileChanges = new ArrayList();
    }

    public Problem add(AbstractRefactoring refactoring, RefactoringElementImplementation el) {
        Problem p = null;
        if (el == null) {
            throw new NullPointerException();
        }
        if (this.isReadOnly(el) && !(refactoring instanceof WhereUsedQuery) && !(refactoring instanceof SingleCopyRefactoring)) {
            FileObject file = el.getParentFile();
            this.readOnlyFiles.add(file);
            el.setEnabled(false);
            el.setStatus(3);
            this.hasReadOnly = true;
            this.delegate.add(el);
        } else if (!(refactoring instanceof WhereUsedQuery) && this.isGuarded(el)) {
            ArrayList<RefactoringElementImplementation> proposedChanges = new ArrayList<RefactoringElementImplementation>();
            ArrayList<Transaction> transactions = new ArrayList<Transaction>();
            for (GuardedBlockHandler gbHandler : APIAccessor.DEFAULT.getGBHandlers(refactoring)) {
                el.setEnabled(false);
                p = APIAccessor.DEFAULT.chainProblems(gbHandler.handleChange(el, proposedChanges, transactions), p);
                if (p != null && p.isFatal()) {
                    return p;
                }
                this.delegate.addAll(proposedChanges);
                for (Transaction transaction : transactions) {
                    this.registerTransaction(transaction);
                }
                if (proposedChanges.isEmpty() && transactions.isEmpty()) continue;
                return p;
            }
            el.setEnabled(false);
            el.setStatus(2);
            this.hasGuarded = true;
            this.delegate.add(el);
        } else {
            this.delegate.add(el);
        }
        return p;
    }

    public Problem addAll(AbstractRefactoring refactoring, Collection<RefactoringElementImplementation> elements) {
        Problem p = null;
        for (RefactoringElementImplementation rei : elements) {
            if ((p = APIAccessor.DEFAULT.chainProblems(p, this.add(refactoring, rei))) == null || !p.isFatal()) continue;
            return p;
        }
        return p;
    }

    public RefactoringSession getSession() {
        return this.session;
    }

    Collection<FileObject> getReadOnlyFiles() {
        return this.readOnlyFiles;
    }

    public void registerTransaction(Transaction commit) {
        if (APIAccessor.DEFAULT.isCommit(this.session) && !this.commits.contains(commit)) {
            this.commits.add(commit);
        }
    }

    public Problem addFileChange(AbstractRefactoring refactoring, RefactoringElementImplementation el) {
        if (APIAccessor.DEFAULT.isCommit(this.session)) {
            this.fileChanges.add(el);
        }
        return null;
    }

    private boolean isReadOnly(RefactoringElementImplementation rei) {
        FileObject fileObject = rei.getParentFile();
        if (fileObject == null) {
            throw new NullPointerException("null parent file: " + rei.getClass().getName());
        }
        return !rei.getParentFile().canWrite();
    }

    private boolean isGuarded(RefactoringElementImplementation el) {
        if (el.getPosition() == null) {
            return false;
        }
        try {
            GuardedSectionManager manager;
            DataObject dob = DataObject.find((FileObject)el.getParentFile());
            EditorCookie e = (EditorCookie)dob.getCookie(EditorCookie.class);
            if (e != null && (manager = GuardedSectionManager.getInstance((StyledDocument)RefactoringElementsBag.openDocument(e))) != null) {
                Position elementStart = el.getPosition().getBegin().getPosition();
                Position elementEnd = el.getPosition().getEnd().getPosition();
                for (GuardedSection section : manager.getGuardedSections()) {
                    if (!section.contains(elementStart, true) && !section.contains(elementEnd, true)) continue;
                    return true;
                }
            }
        }
        catch (DataObjectNotFoundException ex) {
            Logger.getLogger("global").log(Level.SEVERE, ex.getMessage(), (Throwable)ex);
        }
        catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }

    private static StyledDocument openDocument(EditorCookie ec) throws IOException {
        StyledDocument doc;
        try {
            doc = ec.openDocument();
        }
        catch (UserQuestionException ex) {
            if (ex.getMessage().startsWith("The file is too big.") || ex.getMessage().contains("cannot be safely opened with encoding")) {
                ex.confirmed();
                doc = ec.openDocument();
            }
            throw ex;
        }
        return doc;
    }

    static {
        SPIAccessor.DEFAULT = new AccessorImpl();
    }
}

