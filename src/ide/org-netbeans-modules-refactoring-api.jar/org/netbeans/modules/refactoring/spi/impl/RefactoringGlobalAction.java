/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.Actions$MenuItem
 *  org.openide.cookies.EditorCookie
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.CloneableEditorSupport$Pane
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import org.openide.awt.Actions;
import org.openide.cookies.EditorCookie;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.TopComponent;

public abstract class RefactoringGlobalAction
extends NodeAction {
    public RefactoringGlobalAction(String name, Icon icon) {
        this.setName(name);
        this.setIcon(icon);
    }

    public final String getName() {
        return (String)this.getValue("Name");
    }

    protected void setName(String name) {
        this.putValue("Name", (Object)name);
    }

    protected void setMnemonic(char m) {
        this.putValue("MnemonicKey", (Object)new Integer(m));
    }

    private static String trim(String arg) {
        arg = arg.replace("&", "");
        return arg.replace("...", "");
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected Lookup getLookup(Node[] n) {
        EditorCookie tc;
        InstanceContent ic = new InstanceContent();
        for (Node node : n) {
            ic.add((Object)node);
        }
        if (n.length > 0 && (tc = RefactoringGlobalAction.getTextComponent(n[0])) != null) {
            ic.add((Object)tc);
        }
        return new AbstractLookup((AbstractLookup.Content)ic);
    }

    protected static EditorCookie getTextComponent(Node n) {
        TopComponent activetc;
        EditorCookie ec;
        DataObject dobj = (DataObject)n.getCookie(DataObject.class);
        if (dobj != null && (ec = (EditorCookie)dobj.getCookie(EditorCookie.class)) != null && (activetc = TopComponent.getRegistry().getActivated()) instanceof CloneableEditorSupport.Pane) {
            return ec;
        }
        return null;
    }

    public abstract void performAction(Lookup var1);

    protected abstract boolean enable(Lookup var1);

    protected abstract boolean applicable(Lookup var1);

    public final void performAction(Node[] activatedNodes) {
        this.performAction(this.getLookup(activatedNodes));
    }

    protected boolean enable(Node[] activatedNodes) {
        return this.enable(this.getLookup(activatedNodes));
    }

    public Action createContextAwareInstance(Lookup actionContext) {
        return new ContextAction(actionContext);
    }

    public class ContextAction
    implements Action,
    Presenter.Menu,
    Presenter.Popup,
    Presenter.Toolbar {
        Lookup context;

        public ContextAction(Lookup context) {
            this.context = context;
        }

        @Override
        public Object getValue(String arg0) {
            if ("applicable".equals(arg0)) {
                return RefactoringGlobalAction.this.applicable(this.context);
            }
            return RefactoringGlobalAction.this.getValue(arg0);
        }

        @Override
        public void putValue(String arg0, Object arg1) {
            RefactoringGlobalAction.this.putValue(arg0, arg1);
        }

        @Override
        public void setEnabled(boolean arg0) {
            RefactoringGlobalAction.this.setEnabled(arg0);
        }

        @Override
        public boolean isEnabled() {
            return RefactoringGlobalAction.this.enable(this.context);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener arg0) {
            RefactoringGlobalAction.this.addPropertyChangeListener(arg0);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener arg0) {
            RefactoringGlobalAction.this.removePropertyChangeListener(arg0);
        }

        @Override
        public void actionPerformed(ActionEvent arg0) {
            RefactoringGlobalAction.this.performAction(this.context);
        }

        public JMenuItem getMenuPresenter() {
            if (this.isMethodOverridden(RefactoringGlobalAction.this, "getMenuPresenter")) {
                return RefactoringGlobalAction.this.getMenuPresenter();
            }
            return new Actions.MenuItem((Action)this, true);
        }

        public JMenuItem getPopupPresenter() {
            if (this.isMethodOverridden(RefactoringGlobalAction.this, "getPopupPresenter")) {
                return RefactoringGlobalAction.this.getPopupPresenter();
            }
            return new Actions.MenuItem((Action)this, false);
        }

        public Component getToolbarPresenter() {
            if (this.isMethodOverridden(RefactoringGlobalAction.this, "getToolbarPresenter")) {
                return RefactoringGlobalAction.this.getToolbarPresenter();
            }
            JButton button = new JButton();
            Actions.connect((AbstractButton)button, (Action)this);
            return button;
        }

        private boolean isMethodOverridden(NodeAction d, String name) {
            try {
                Method m = d.getClass().getMethod(name, new Class[0]);
                return m.getDeclaringClass() != CallableSystemAction.class;
            }
            catch (NoSuchMethodException ex) {
                ex.printStackTrace();
                throw new IllegalStateException("Error searching for method " + name + " in " + (Object)d);
            }
        }
    }

}

