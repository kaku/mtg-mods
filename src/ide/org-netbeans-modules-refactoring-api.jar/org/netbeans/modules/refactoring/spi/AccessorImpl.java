/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.refactoring.spi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.modules.refactoring.api.RefactoringSession;
import org.netbeans.modules.refactoring.api.impl.SPIAccessor;
import org.netbeans.modules.refactoring.spi.RefactoringCommit;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.RefactoringElementsBag;
import org.netbeans.modules.refactoring.spi.SimpleRefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.Transaction;
import org.openide.filesystems.FileObject;

final class AccessorImpl
extends SPIAccessor {
    AccessorImpl() {
    }

    @Override
    public RefactoringElementsBag createBag(RefactoringSession session, List delegate) {
        assert (session != null && delegate != null);
        return new RefactoringElementsBag(session, delegate);
    }

    @Override
    public Collection getReadOnlyFiles(RefactoringElementsBag bag) {
        return bag.getReadOnlyFiles();
    }

    @Override
    public ArrayList<Transaction> getCommits(RefactoringElementsBag bag) {
        return bag.commits;
    }

    @Override
    public ArrayList<RefactoringElementImplementation> getFileChanges(RefactoringElementsBag bag) {
        return bag.fileChanges;
    }

    @Override
    public String getNewFileContent(SimpleRefactoringElementImplementation impl) {
        return impl.getNewFileContent();
    }

    @Override
    public boolean hasChangesInGuardedBlocks(RefactoringElementsBag bag) {
        return bag.hasGuarded;
    }

    @Override
    public boolean hasChangesInReadOnlyFiles(RefactoringElementsBag bag) {
        return bag.hasReadOnly;
    }

    @Override
    public void check(Transaction commit, boolean undo) {
        if (commit instanceof RefactoringCommit) {
            ((RefactoringCommit)commit).check(undo);
        }
    }

    @Override
    public void sum(Transaction commit) {
        if (commit instanceof RefactoringCommit) {
            ((RefactoringCommit)commit).sum();
        }
    }
}

