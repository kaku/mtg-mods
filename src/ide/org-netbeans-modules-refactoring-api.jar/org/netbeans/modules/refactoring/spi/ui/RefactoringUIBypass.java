/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.io.IOException;

public interface RefactoringUIBypass {
    public boolean isRefactoringBypassRequired();

    public void doRefactoringBypass() throws IOException;
}

