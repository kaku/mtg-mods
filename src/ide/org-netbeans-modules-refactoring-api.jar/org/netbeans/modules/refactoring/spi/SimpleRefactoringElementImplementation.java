/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.JumpList
 *  org.openide.DialogDisplayer
 *  org.openide.ErrorManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.PositionBounds
 *  org.openide.text.PositionRef
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package org.netbeans.modules.refactoring.spi;

import java.awt.Container;
import javax.swing.JEditorPane;
import org.netbeans.editor.JumpList;
import org.netbeans.modules.refactoring.spi.RefactoringElementImplementation;
import org.netbeans.modules.refactoring.spi.impl.ParametersPanel;
import org.netbeans.modules.refactoring.spi.impl.PreviewManager;
import org.openide.DialogDisplayer;
import org.openide.ErrorManager;
import org.openide.NotifyDescriptor;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.PositionBounds;
import org.openide.text.PositionRef;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public abstract class SimpleRefactoringElementImplementation
implements RefactoringElementImplementation {
    private boolean enabled = true;
    private int status = 0;

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public void openInEditor() {
        PositionBounds bounds = this.getPosition();
        if (bounds == null) {
            return;
        }
        PositionRef beginPos = bounds.getBegin();
        CloneableEditorSupport editSupp = beginPos.getCloneableEditorSupport();
        editSupp.edit();
        JEditorPane[] panes = editSupp.getOpenedPanes();
        if (panes != null) {
            JumpList.checkAddEntry();
            try {
                panes[0].setCaretPosition(bounds.getEnd().getOffset());
                panes[0].moveCaretPosition(beginPos.getOffset());
            }
            catch (IllegalArgumentException iae) {
                ErrorManager.getDefault().notify(1, (Throwable)iae);
            }
            SimpleRefactoringElementImplementation.getTopComponent(panes[0]).requestActive();
        } else {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(ParametersPanel.class, (String)"ERR_ErrorOpeningEditor")));
        }
    }

    @Override
    public void showPreview() {
        PreviewManager manager = PreviewManager.getDefault();
        manager.refresh(this);
    }

    protected String getNewFileContent() {
        return null;
    }

    private static final TopComponent getTopComponent(Container temp) {
        while (!(temp instanceof TopComponent)) {
            temp = temp.getParent();
        }
        return (TopComponent)temp;
    }

    @Override
    public void undoChange() {
    }
}

