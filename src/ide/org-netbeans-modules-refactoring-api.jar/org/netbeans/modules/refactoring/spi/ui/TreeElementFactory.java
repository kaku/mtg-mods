/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.util.Collection;
import org.netbeans.modules.refactoring.spi.ui.TreeElement;
import org.netbeans.modules.refactoring.spi.ui.TreeElementFactoryImplementation;
import org.openide.util.Lookup;

public final class TreeElementFactory {
    private static final Lookup.Result<TreeElementFactoryImplementation> implementations = Lookup.getDefault().lookup(new Lookup.Template(TreeElementFactoryImplementation.class));

    private TreeElementFactory() {
    }

    public static TreeElement getTreeElement(Object object) {
        if (object == null) {
            throw new IllegalArgumentException();
        }
        for (TreeElementFactoryImplementation fac : implementations.allInstances()) {
            TreeElement result = fac.getTreeElement(object);
            if (result == null) continue;
            return result;
        }
        return null;
    }
}

