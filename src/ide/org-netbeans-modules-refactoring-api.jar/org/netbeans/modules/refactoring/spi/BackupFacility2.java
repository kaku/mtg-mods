/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.editor.BaseDocument
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Manager
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.text.NbDocument
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.StyledDocument;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.refactoring.spi.impl.UndoableWrapper;
import org.openide.awt.UndoRedo;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

abstract class BackupFacility2 {
    private static final Logger LOG = Logger.getLogger("org.netbeans.modules.refactoring.Undo");
    private static BackupFacility2 defaultInstance;

    private BackupFacility2() {
    }

    public /* varargs */ abstract Handle backup(FileObject ... var1) throws IOException;

    public /* varargs */ abstract Handle backup(File ... var1) throws IOException;

    public final Handle backup(Collection<? extends FileObject> fileObjects) throws IOException {
        return this.backup(fileObjects.toArray((T[])new FileObject[fileObjects.size()]));
    }

    public abstract void clear();

    public static BackupFacility2 getDefault() {
        BackupFacility2 instance = (BackupFacility2)Lookup.getDefault().lookup(BackupFacility2.class);
        return instance != null ? instance : BackupFacility2.getDefaultInstance();
    }

    private static synchronized BackupFacility2 getDefaultInstance() {
        if (defaultInstance == null) {
            defaultInstance = new DefaultImpl();
        }
        return defaultInstance;
    }

    private static class DefaultImpl
    extends BackupFacility2 {
        private long currentId = 0;
        private Map<Long, BackupEntry> map = new HashMap<Long, BackupEntry>();
        private static Field undoRedo;

        private String MD5toString(byte[] digest) {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < digest.length; ++i) {
                b.append(Integer.toHexString(255 & digest[i]));
            }
            return b.toString();
        }

        private void storeChecksum(long l) throws IOException {
            FileObject fo;
            BaseDocument doc;
            CloneableEditorSupport ces;
            BackupEntry backup = this.map.get(l);
            if (backup.orig == null) {
                backup.orig = FileUtil.toFileObject((File)backup.origFile);
                backup.origFile = null;
            }
            if (!(fo = backup.orig).isValid()) {
                backup.checkSum = new byte[16];
                Arrays.fill(backup.checkSum, 0);
                return;
            }
            DataObject dob = DataObject.find((FileObject)fo);
            if (dob != null && (doc = (BaseDocument)(ces = (CloneableEditorSupport)dob.getLookup().lookup(CloneableEditorSupport.class)).getDocument()) != null && doc.isAtomicLock()) {
                return;
            }
            LOG.log(Level.FINE, "Storing MD5 for {0}", (Object)backup.orig);
            backup.checkSum = this.getMD5(this.getInputStream(backup.orig));
            LOG.log(Level.FINE, "MD5 is: {0}", this.MD5toString(backup.checkSum));
        }

        private String checkChecksum(long l, boolean undo) {
            try {
                BackupEntry backup = this.map.get(l);
                FileObject fo = backup.orig;
                if (!fo.isValid()) {
                    return null;
                }
                DataObject dob = DataObject.find((FileObject)fo);
                if (dob != null) {
                    CloneableEditorSupport ces = (CloneableEditorSupport)dob.getLookup().lookup(CloneableEditorSupport.class);
                    BaseDocument doc = (BaseDocument)ces.getDocument();
                    if (doc != null && doc.isAtomicLock()) {
                        return null;
                    }
                    EditorCookie editor = (EditorCookie)dob.getLookup().lookup(EditorCookie.class);
                    if (editor != null && doc != null && editor.isModified()) {
                        UndoableWrapper.UndoableEditDelegate edit;
                        UndoableWrapper.UndoableEditDelegate undoableEditDelegate = edit = undo ? (UndoableWrapper.UndoableEditDelegate)NbDocument.getEditToBeUndoneOfType((EditorCookie)editor, UndoableWrapper.UndoableEditDelegate.class) : (UndoableWrapper.UndoableEditDelegate)NbDocument.getEditToBeRedoneOfType((EditorCookie)editor, UndoableWrapper.UndoableEditDelegate.class);
                        if (edit == null) {
                            LOG.fine("Editor Undo Different");
                            return backup.orig.getPath();
                        }
                    }
                }
                try {
                    LOG.log(Level.FINE, "Checking MD5 for {0}", (Object)backup.orig);
                    byte[] ts = this.getMD5(this.getInputStream(backup.orig));
                    if (!Arrays.equals(backup.checkSum, ts)) {
                        LOG.fine("MD5 check failed");
                        return backup.orig.getPath();
                    }
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            return null;
        }

        private InputStream getInputStream(FileObject fo) throws IOException {
            DataObject dob = DataObject.find((FileObject)fo);
            CloneableEditorSupport ces = (CloneableEditorSupport)dob.getLookup().lookup(CloneableEditorSupport.class);
            if (ces != null && ces.isModified()) {
                LOG.fine("Editor Input Stream");
                return ces.getInputStream();
            }
            LOG.fine("File Input Stream");
            return fo.getInputStream();
        }

        private DefaultImpl() {
            super();
        }

        @Override
        public /* varargs */ Handle backup(FileObject ... file) throws IOException {
            ArrayList<Long> list = new ArrayList<Long>();
            for (FileObject f : file) {
                list.add(this.backup(f));
            }
            return new DefaultHandle(this, list);
        }

        @Override
        public /* varargs */ Handle backup(File ... files) throws IOException {
            ArrayList<Long> list = new ArrayList<Long>();
            for (File f : files) {
                list.add(this.backup(f));
            }
            return new DefaultHandle(this, list);
        }

        public long backup(FileObject file) throws IOException {
            BackupEntry entry = new BackupEntry();
            entry.file = File.createTempFile("nbbackup", null);
            this.copy(file, entry.file);
            entry.orig = file;
            this.map.put(this.currentId, entry);
            entry.file.deleteOnExit();
            return this.currentId++;
        }

        public long backup(File file) throws IOException {
            BackupEntry entry = new BackupEntry();
            entry.file = File.createTempFile("nbbackup", null);
            entry.exists = file.exists();
            if (entry.exists) {
                FileObject fo = FileUtil.toFileObject((File)file);
                entry.orig = fo;
            } else {
                entry.origFile = file;
            }
            this.map.put(this.currentId, entry);
            entry.file.deleteOnExit();
            if (entry.exists) {
                this.copy(file, entry.file);
            }
            return this.currentId++;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private byte[] getMD5(InputStream is) throws IOException {
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                try {
                    is = new DigestInputStream(is, md);
                    DefaultImpl.readFully(is, -1, true);
                }
                finally {
                    is.close();
                }
                return md.digest();
            }
            catch (NoSuchAlgorithmException ex) {
                throw new IOException(ex);
            }
        }

        private static byte[] readFully(InputStream is, int length, boolean readAll) throws IOException {
            int cc;
            byte[] output = new byte[]{};
            if (length == -1) {
                length = Integer.MAX_VALUE;
            }
            for (int pos = 0; pos < length; pos += cc) {
                int bytesToRead;
                if (pos >= output.length) {
                    bytesToRead = Math.min(length - pos, output.length + 1024);
                    if (output.length < pos + bytesToRead) {
                        output = Arrays.copyOf(output, pos + bytesToRead);
                    }
                } else {
                    bytesToRead = output.length - pos;
                }
                if ((cc = is.read(output, pos, bytesToRead)) >= 0) continue;
                if (readAll && length != Integer.MAX_VALUE) {
                    throw new EOFException("Detect premature EOF");
                }
                if (output.length == pos) break;
                output = Arrays.copyOf(output, pos);
                break;
            }
            return output;
        }

        void restore(long id) throws IOException {
            BackupEntry entry = this.map.get(id);
            if (entry == null) {
                throw new IllegalArgumentException("Backup with id " + id + "does not exist");
            }
            File backup = File.createTempFile("nbbackup", null);
            backup.deleteOnExit();
            boolean exists = false;
            FileObject fo = entry.orig;
            if (!fo.isValid()) {
                FileObject file = FileUtil.toFileObject((File)FileUtil.toFile((FileObject)fo));
                FileObject fileObject = fo = file == null ? fo : file;
            }
            if (exists = fo.isValid()) {
                backup.createNewFile();
                this.copy(fo, backup);
            } else {
                fo = this.createNewFile(fo);
            }
            if (entry.exists) {
                if (!this.tryUndoOrRedo(fo, entry)) {
                    this.copy(entry.file, fo);
                }
            } else {
                fo.delete();
            }
            entry.exists = exists;
            entry.file.delete();
            if (backup.exists()) {
                entry.file = backup;
            } else {
                this.map.remove(id);
            }
        }

        private boolean tryUndoOrRedo(@NonNull FileObject fileObj, final @NonNull BackupEntry entry) throws DataObjectNotFoundException {
            DataObject dob = DataObject.find((FileObject)fileObj);
            if (dob != null) {
                CloneableEditorSupport ces = (CloneableEditorSupport)dob.getLookup().lookup(CloneableEditorSupport.class);
                if (ces == null) {
                    return false;
                }
                try {
                    final UndoRedo.Manager manager = (UndoRedo.Manager)undoRedo.get((Object)ces);
                    BaseDocument doc = (BaseDocument)ces.getDocument();
                    if (doc == null) {
                        return false;
                    }
                    if (doc.isAtomicLock() || fileObj.isLocked()) {
                        if (entry.isUndo()) {
                            entry.setUndo(false);
                        } else {
                            entry.setUndo(true);
                        }
                    } else if (entry.isUndo() && manager.canUndo() || !entry.isUndo() && manager.canRedo()) {
                        doc.runAtomic(new Runnable(){

                            @Override
                            public void run() {
                                if (entry.isUndo()) {
                                    manager.undo();
                                    entry.setUndo(false);
                                } else {
                                    manager.redo();
                                    entry.setUndo(true);
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                    return true;
                }
                catch (IllegalAccessException | IllegalArgumentException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            return false;
        }

        private FileObject createNewFile(FileObject fo) throws IOException {
            if (fo.isValid()) {
                return fo;
            }
            File file = FileUtil.toFile((FileObject)fo);
            if (file != null && file.exists()) {
                return FileUtil.toFileObject((File)file);
            }
            FileObject parent = fo.getParent();
            if (parent != null) {
                this.createNewFolder(parent);
            }
            return FileUtil.createData((FileObject)parent, (String)fo.getNameExt());
        }

        private void createNewFolder(FileObject fo) throws IOException {
            FileObject parent;
            if (!fo.isValid() && (parent = fo.getParent()) != null) {
                this.createNewFolder(parent);
                FileUtil.createFolder((FileObject)parent, (String)fo.getNameExt());
            }
        }

        private void copy(FileObject a, File b) throws IOException {
            InputStream fs = a.getInputStream();
            FileOutputStream fo = new FileOutputStream(b);
            this.copy(fs, fo);
        }

        private void copy(File a, File b) throws IOException {
            FileInputStream fs = new FileInputStream(a);
            FileOutputStream fo = new FileOutputStream(b);
            this.copy(fs, fo);
        }

        private void copy(File a, FileObject b) throws IOException {
            FileInputStream fs = new FileInputStream(a);
            OutputStream fo = b.getOutputStream();
            this.copy(fs, fo);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void copy(InputStream is, OutputStream os) throws IOException {
            try {
                FileUtil.copy((InputStream)is, (OutputStream)os);
            }
            finally {
                is.close();
                os.close();
            }
        }

        @Override
        public void clear() {
            for (BackupEntry entry : this.map.values()) {
                entry.file.delete();
            }
            this.map.clear();
        }

        static {
            try {
                undoRedo = CloneableEditorSupport.class.getDeclaredField("undoRedo");
                undoRedo.setAccessible(true);
            }
            catch (NoSuchFieldException | SecurityException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

        private class BackupEntry {
            private File file;
            private FileObject orig;
            private File origFile;
            private byte[] checkSum;
            private boolean undo;
            private boolean exists;

            public BackupEntry() {
                this.undo = true;
                this.exists = true;
            }

            public boolean isUndo() {
                return this.undo;
            }

            public void setUndo(boolean undo) {
                this.undo = undo;
            }
        }

    }

    private static class DefaultHandle
    implements Handle {
        private List<Long> handle;
        private DefaultImpl instance;

        private DefaultHandle(DefaultImpl instance, List<Long> handles) {
            this.handle = handles;
            this.instance = instance;
        }

        @Override
        public void restore() throws IOException {
            Iterator<Long> i$ = this.handle.iterator();
            while (i$.hasNext()) {
                long l = i$.next();
                this.instance.restore(l);
            }
        }

        @Override
        public void storeChecksum() throws IOException {
            Iterator<Long> i$ = this.handle.iterator();
            while (i$.hasNext()) {
                long l = i$.next();
                this.instance.storeChecksum(l);
            }
        }

        @Override
        public Collection<String> checkChecksum(boolean undo) throws IOException {
            LinkedList<String> result = new LinkedList<String>();
            Iterator<Long> i$ = this.handle.iterator();
            while (i$.hasNext()) {
                long l = i$.next();
                String checkChecksum = this.instance.checkChecksum(l, undo);
                if (checkChecksum == null) continue;
                result.add(checkChecksum);
            }
            return result;
        }
    }

    public static interface Handle {
        public void restore() throws IOException;

        public void storeChecksum() throws IOException;

        public Collection<String> checkChecksum(boolean var1) throws IOException;
    }

}

