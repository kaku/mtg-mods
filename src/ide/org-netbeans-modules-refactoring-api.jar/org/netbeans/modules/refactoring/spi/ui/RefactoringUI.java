/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 */
package org.netbeans.modules.refactoring.spi.ui;

import javax.swing.event.ChangeListener;
import org.netbeans.modules.refactoring.api.AbstractRefactoring;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.spi.ui.CustomRefactoringPanel;
import org.openide.util.HelpCtx;

public interface RefactoringUI {
    public String getName();

    public String getDescription();

    public boolean isQuery();

    public CustomRefactoringPanel getPanel(ChangeListener var1);

    public Problem setParameters();

    public Problem checkParameters();

    public boolean hasParameters();

    public AbstractRefactoring getRefactoring();

    public HelpCtx getHelpCtx();
}

