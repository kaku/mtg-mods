/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.modules.refactoring.api.Problem;
import org.netbeans.modules.refactoring.spi.impl.ProblemComponent;
import org.netbeans.modules.refactoring.spi.ui.RefactoringUI;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class ErrorPanel
extends JPanel {
    private static ImageIcon fatalImage = null;
    private static ImageIcon nonFatalImage = null;
    private RefactoringUI ui;
    private JLabel errorLabel;
    private JPanel errors;
    private JPanel explanationPanel;
    private JLabel fatalError;
    private JTextArea headLine;
    private JPanel listPanel;
    private JScrollPane listScrollPane;
    private JLabel nonFatalError;

    public ErrorPanel(RefactoringUI ui) {
        this.ui = ui;
        this.initComponents();
        this.headLine.setBackground(UIManager.getDefaults().getColor("Panel.background"));
        this.setPreferredSize(new Dimension(510, 200));
    }

    public ErrorPanel(Problem problem, RefactoringUI ui) {
        this(ui);
        this.setProblems(problem);
    }

    public void setProblems(Problem problem) {
        GridBagConstraints gridBagConstraints;
        boolean single;
        this.errors.removeAll();
        int i = 0;
        ProblemComponent.initButtonSize(problem);
        boolean bl = single = problem.getNext() == null;
        while (problem != null) {
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = i++;
            gridBagConstraints.fill = 2;
            gridBagConstraints.anchor = 11;
            gridBagConstraints.weightx = 1.0;
            ProblemComponent c = new ProblemComponent(problem, this.ui, single);
            this.errors.add((Component)c, gridBagConstraints);
            problem = problem.getNext();
            if (i % 2 == 1) {
                c.setLightBackground();
                continue;
            }
            c.setDarkBackground();
        }
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = i;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        JPanel jp = new JPanel();
        this.errors.add((Component)jp, gridBagConstraints);
    }

    static ImageIcon getFatalErrorIcon() {
        if (fatalImage == null) {
            fatalImage = new ImageIcon(ErrorPanel.class.getResource("/org/netbeans/modules/refactoring/api/resources/error.png"));
        }
        return fatalImage;
    }

    static ImageIcon getNonfatalErrorIcon() {
        if (nonFatalImage == null) {
            nonFatalImage = new ImageIcon(ErrorPanel.class.getResource("/org/netbeans/modules/refactoring/api/resources/warning.png"));
        }
        return nonFatalImage;
    }

    private void initComponents() {
        this.listPanel = new JPanel();
        this.errorLabel = new JLabel();
        this.listScrollPane = new JScrollPane();
        this.errors = new JPanel();
        this.explanationPanel = new JPanel();
        this.fatalError = new JLabel();
        this.nonFatalError = new JLabel();
        this.headLine = new JTextArea();
        this.setLayout(new BorderLayout());
        this.listPanel.setLayout(new BorderLayout());
        this.errorLabel.setLabelFor(this.errors);
        Mnemonics.setLocalizedText((JLabel)this.errorLabel, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("LBL_ErrorsList"));
        this.listPanel.add((Component)this.errorLabel, "North");
        this.listScrollPane.setHorizontalScrollBarPolicy(31);
        this.errors.setLayout(new GridBagLayout());
        this.listScrollPane.setViewportView(this.errors);
        this.listPanel.add((Component)this.listScrollPane, "Center");
        this.add((Component)this.listPanel, "Center");
        this.explanationPanel.setLayout(new FlowLayout(0));
        this.fatalError.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/refactoring/api/resources/error.png")));
        Mnemonics.setLocalizedText((JLabel)this.fatalError, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("LBL_FatalError"));
        this.fatalError.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 20));
        this.explanationPanel.add(this.fatalError);
        this.nonFatalError.setIcon(new ImageIcon(this.getClass().getResource("/org/netbeans/modules/refactoring/api/resources/warning.png")));
        Mnemonics.setLocalizedText((JLabel)this.nonFatalError, (String)NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("LBL_NonFatalError"));
        this.explanationPanel.add(this.nonFatalError);
        this.add((Component)this.explanationPanel, "South");
        this.headLine.setBackground(UIManager.getDefaults().getColor("Panel.background"));
        this.headLine.setEditable(false);
        this.headLine.setFont(this.errorLabel.getFont());
        this.headLine.setLineWrap(true);
        this.headLine.setText(NbBundle.getBundle((String)"org/netbeans/modules/refactoring/spi/impl/Bundle").getString("LBL_ErrorPanelDescription"));
        this.headLine.setWrapStyleWord(true);
        this.headLine.setBorder(BorderFactory.createEmptyBorder(1, 1, 10, 1));
        this.add((Component)this.headLine, "North");
        this.headLine.getAccessibleContext().setAccessibleName("null");
        this.headLine.getAccessibleContext().setAccessibleDescription("null");
        this.getAccessibleContext().setAccessibleName(NbBundle.getBundle(ErrorPanel.class).getString("ACSD_ErrorPanelName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(ErrorPanel.class).getString("ACSD_ErrorPanelDescription"));
    }
}

