/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileStateInvalidException
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.io.File;
import java.util.LinkedHashSet;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileStateInvalidException;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;

public class Util {
    private static FileSystem[] fileSystems;

    private static FileSystem[] getFileSystems() {
        if (fileSystems != null) {
            return fileSystems;
        }
        File[] roots = File.listRoots();
        LinkedHashSet<FileSystem> allRoots = new LinkedHashSet<FileSystem>();
        assert (roots != null && roots.length > 0);
        for (int i = 0; i < roots.length; ++i) {
            File root = roots[i];
            FileObject random = FileUtil.toFileObject((File)root);
            if (random == null) continue;
            try {
                FileSystem fs = random.getFileSystem();
                allRoots.add(fs);
                continue;
            }
            catch (FileStateInvalidException e) {
                throw new AssertionError((Object)e);
            }
        }
        FileSystem[] retVal = new FileSystem[allRoots.size()];
        allRoots.toArray((T[])retVal);
        assert (retVal.length > 0);
        fileSystems = retVal;
        return fileSystems;
    }

    public static void addFileSystemsListener(FileChangeListener l) {
        FileSystem[] fileSystems = Util.getFileSystems();
        for (int i = 0; i < fileSystems.length; ++i) {
            fileSystems[i].addFileChangeListener(l);
        }
    }

    public static void removeFileSystemsListener(FileChangeListener l) {
        FileSystem[] fileSystems = Util.getFileSystems();
        for (int i = 0; i < fileSystems.length; ++i) {
            fileSystems[i].removeFileChangeListener(l);
        }
    }
}

