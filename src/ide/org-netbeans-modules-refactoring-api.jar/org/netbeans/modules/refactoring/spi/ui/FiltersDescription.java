/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.refactoring.spi.ui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import org.netbeans.api.annotations.common.NonNull;

public final class FiltersDescription {
    private List<FilterItem> filters = new ArrayList<FilterItem>();

    public void addFilter(@NonNull String key, @NonNull String tooltip, boolean selected, @NonNull Icon icon) {
        FilterItem newItem = new FilterItem(key, tooltip, selected, icon);
        this.filters.add(newItem);
    }

    public int getFilterCount() {
        return this.filters.size();
    }

    @NonNull
    public String getKey(int index) {
        return this.filters.get((int)index).key;
    }

    @NonNull
    public String getTooltip(int index) {
        return this.filters.get((int)index).tooltip;
    }

    @NonNull
    public Icon getIcon(int index) {
        return this.filters.get((int)index).icon;
    }

    public boolean isSelected(int index) {
        return this.filters.get((int)index).selected;
    }

    public void setSelected(int index, boolean selected) {
        this.filters.get((int)index).selected = selected;
    }

    public void enable(int index) {
        this.filters.get((int)index).enabled = true;
    }

    public void enable(@NonNull String key) {
        for (FilterItem filterItem : this.filters) {
            if (!filterItem.key.contentEquals(key)) continue;
            filterItem.enabled = true;
            break;
        }
    }

    public boolean isEnabled(int index) {
        return this.filters.get((int)index).enabled;
    }

    public static interface Provider {
        public void addFilters(FiltersDescription var1);

        public void enableFilters(FiltersDescription var1);
    }

    private static class FilterItem {
        String key;
        String tooltip;
        Icon icon;
        boolean selected;
        boolean enabled;

        FilterItem(String key, String tooltip, boolean selected, Icon icon) {
            this.key = key;
            this.tooltip = tooltip;
            this.icon = icon;
            this.selected = selected;
            this.enabled = false;
        }
    }

}

