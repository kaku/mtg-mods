/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.explorer.ExtendedDelete
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileSystem$AtomicAction
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import org.netbeans.modules.refactoring.api.impl.ActionsImplementationFactory;
import org.netbeans.modules.refactoring.api.ui.ExplorerContext;
import org.netbeans.modules.refactoring.spi.impl.RefactoringGlobalAction;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExtendedDelete;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class SafeDeleteAction
extends RefactoringGlobalAction
implements ExtendedDelete {
    private static final Logger LOGGER = Logger.getLogger(SafeDeleteAction.class.getName());
    private boolean regularDelete = false;

    public SafeDeleteAction() {
        super(NbBundle.getMessage(SafeDeleteAction.class, (String)"LBL_SafeDel_Action"), null);
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    @Override
    public final void performAction(Lookup context) {
        ActionsImplementationFactory.doDelete(context);
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "SafeDeleteAction.performAction", new Exception());
        }
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    @Override
    protected boolean enable(Lookup context) {
        return true;
    }

    @Override
    protected Lookup getLookup(Node[] n) {
        Lookup l = super.getLookup(n);
        if (this.regularDelete) {
            ExplorerContext con = (ExplorerContext)l.lookup(ExplorerContext.class);
            if (con != null) {
                con.setDelete(true);
            } else {
                con = new ExplorerContext();
                con.setDelete(true);
                return new ProxyLookup(new Lookup[]{l, Lookups.singleton((Object)con)});
            }
        }
        return l;
    }

    public boolean delete(final Node[] nodes) {
        if (nodes.length < 2 && ActionsImplementationFactory.canDelete(this.getLookup(nodes))) {
            if (EventQueue.isDispatchThread()) {
                this.regularDelete = true;
                this.performAction(nodes);
                this.regularDelete = false;
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        SafeDeleteAction.this.regularDelete = true;
                        SafeDeleteAction.this.performAction(nodes);
                        SafeDeleteAction.this.regularDelete = false;
                    }
                });
            }
            return true;
        }
        boolean delete = true;
        for (int i = 0; i < nodes.length; ++i) {
            if (nodes[i].getLookup().lookup(DataObject.class) != null) continue;
            delete = false;
            break;
        }
        if (delete) {
            if (this.doConfirm(nodes)) {
                try {
                    FileUtil.runAtomicAction((FileSystem.AtomicAction)new FileSystem.AtomicAction(){

                        public void run() throws IOException {
                            for (int i = 0; i < nodes.length; ++i) {
                                try {
                                    nodes[i].destroy();
                                    continue;
                                }
                                catch (IOException ioe) {
                                    Exceptions.printStackTrace((Throwable)ioe);
                                }
                            }
                        }
                    });
                }
                catch (IOException ioe) {
                    Exceptions.printStackTrace((Throwable)ioe);
                }
            }
            return true;
        }
        return false;
    }

    private boolean doConfirm(Node[] sel) {
        String message;
        String title;
        boolean customDelete = true;
        for (int i = 0; i < sel.length; ++i) {
            if (Boolean.TRUE.equals(sel[i].getValue("customDelete"))) continue;
            customDelete = false;
            break;
        }
        if (customDelete) {
            return true;
        }
        if (sel.length == 1) {
            message = NbBundle.getMessage(SafeDeleteAction.class, (String)"MSG_ConfirmDeleteObject", (Object)sel[0].getDisplayName());
            title = NbBundle.getMessage(SafeDeleteAction.class, (String)"MSG_ConfirmDeleteObjectTitle");
        } else {
            message = NbBundle.getMessage(SafeDeleteAction.class, (String)"MSG_ConfirmDeleteObjects", (Object)sel.length);
            title = NbBundle.getMessage(SafeDeleteAction.class, (String)"MSG_ConfirmDeleteObjectsTitle");
        }
        NotifyDescriptor.Confirmation desc = new NotifyDescriptor.Confirmation((Object)message, title, 0);
        return NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)desc));
    }

    @Override
    protected boolean applicable(Lookup context) {
        return ActionsImplementationFactory.canDelete(context);
    }

}

