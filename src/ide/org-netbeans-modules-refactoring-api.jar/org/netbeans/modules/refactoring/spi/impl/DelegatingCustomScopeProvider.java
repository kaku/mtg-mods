/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.refactoring.spi.impl;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.Icon;
import org.netbeans.modules.refactoring.api.Scope;
import org.netbeans.modules.refactoring.spi.impl.DelegatingScopeInformation;
import org.netbeans.modules.refactoring.spi.ui.ScopeProvider;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

public class DelegatingCustomScopeProvider
extends ScopeProvider.CustomScopeProvider
implements DelegatingScopeInformation {
    private final Map<?, ?> map;
    private final String id;
    private final String displayName;
    private final int position;
    private Icon icon;
    private ScopeProvider.CustomScopeProvider delegate;

    public static DelegatingCustomScopeProvider create(Map<?, ?> map) {
        return new DelegatingCustomScopeProvider(map);
    }

    public DelegatingCustomScopeProvider(ScopeProvider.CustomScopeProvider delegate, String id, String displayName, int position, Icon image) {
        this.icon = image;
        this.id = id;
        this.displayName = displayName;
        this.position = position;
        this.delegate = delegate;
        this.map = null;
    }

    private DelegatingCustomScopeProvider(Map<?, ?> map) {
        this.map = map;
        String path = (String)map.get("iconBase");
        this.icon = path != null && !path.equals("") ? ImageUtilities.loadImageIcon((String)path, (boolean)false) : null;
        this.id = (String)map.get("id");
        this.displayName = (String)map.get("displayName");
        this.position = (Integer)map.get("position");
    }

    public ScopeProvider.CustomScopeProvider getDelegate() {
        if (this.delegate == null) {
            assert (this.map != null);
            this.delegate = (ScopeProvider.CustomScopeProvider)this.map.get("delegate");
        }
        return this.delegate;
    }

    @Override
    public boolean initialize(Lookup context, AtomicBoolean cancel) {
        ScopeProvider.CustomScopeProvider d = this.getDelegate();
        return d != null ? Boolean.valueOf(d.initialize(context, cancel)) : null;
    }

    @Override
    public Scope getScope() {
        ScopeProvider.CustomScopeProvider d = this.getDelegate();
        return d != null ? d.getScope() : null;
    }

    @Override
    public Icon getIcon() {
        Icon delegateIcon = null;
        ScopeProvider.CustomScopeProvider d = this.getDelegate();
        if (d != null) {
            delegateIcon = d.getIcon();
        }
        return delegateIcon == null ? this.icon : delegateIcon;
    }

    @Override
    public String getDisplayName() {
        String detail = null;
        ScopeProvider.CustomScopeProvider d = this.getDelegate();
        if (d != null) {
            detail = d.getDetail();
        }
        return detail == null ? this.displayName : this.displayName + " (" + detail + ")";
    }

    @Override
    public void setScope(Scope currentScope) {
        ScopeProvider.CustomScopeProvider d = this.getDelegate();
        if (d != null) {
            d.setScope(currentScope);
        }
    }

    @Override
    public boolean showCustomizer() {
        ScopeProvider.CustomScopeProvider d = this.getDelegate();
        return d != null ? Boolean.valueOf(d.showCustomizer()) : null;
    }

    @Override
    public int getPosition() {
        return this.position;
    }

    @Override
    public String getId() {
        return this.id;
    }
}

