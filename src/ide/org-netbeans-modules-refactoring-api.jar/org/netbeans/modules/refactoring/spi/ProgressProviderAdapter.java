/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.refactoring.spi;

import org.netbeans.modules.refactoring.api.ProgressListener;
import org.netbeans.modules.refactoring.api.impl.ProgressSupport;
import org.netbeans.modules.refactoring.spi.ProgressProvider;

public class ProgressProviderAdapter
implements ProgressProvider {
    private ProgressSupport progressSupport;

    protected ProgressProviderAdapter() {
    }

    @Override
    public synchronized void addProgressListener(ProgressListener listener) {
        if (this.progressSupport == null) {
            this.progressSupport = new ProgressSupport();
        }
        this.progressSupport.addProgressListener(listener);
    }

    @Override
    public synchronized void removeProgressListener(ProgressListener listener) {
        if (this.progressSupport != null) {
            this.progressSupport.removeProgressListener(listener);
        }
    }

    protected final void fireProgressListenerStart(int type, int count) {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStart(this, type, count);
        }
    }

    protected final void fireProgressListenerStep() {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStep(this);
        }
    }

    protected final void fireProgressListenerStep(int count) {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStep(this, count);
        }
    }

    protected final void fireProgressListenerStop() {
        if (this.progressSupport != null) {
            this.progressSupport.fireProgressListenerStop(this);
        }
    }
}

