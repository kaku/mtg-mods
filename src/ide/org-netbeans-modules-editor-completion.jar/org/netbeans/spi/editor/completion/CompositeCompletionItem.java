/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion;

import java.util.List;
import org.netbeans.spi.editor.completion.CompletionItem;

public interface CompositeCompletionItem
extends CompletionItem {
    public List<? extends CompletionItem> getSubItems();
}

