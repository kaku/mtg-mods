/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion.support;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;

public abstract class AsyncCompletionQuery {
    private AsyncCompletionTask task;

    protected void preQueryUpdate(JTextComponent component) {
    }

    protected abstract void query(CompletionResultSet var1, Document var2, int var3);

    protected boolean canFilter(JTextComponent component) {
        return false;
    }

    protected void filter(CompletionResultSet resultSet) {
        throw new IllegalStateException("Filtering not supported");
    }

    protected void prepareQuery(JTextComponent component) {
    }

    public final boolean isTaskCancelled() {
        assert (this.task != null);
        return this.task.isCancelled();
    }

    final void initTask(AsyncCompletionTask task) {
        assert (task != null);
        assert (this.task == null);
        this.task = task;
    }
}

