/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor.completion;

import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="CompletionProviders")
public interface CompletionProvider {
    public static final int COMPLETION_QUERY_TYPE = 1;
    public static final int DOCUMENTATION_QUERY_TYPE = 2;
    public static final int TOOLTIP_QUERY_TYPE = 4;
    public static final int COMPLETION_ALL_QUERY_TYPE = 9;

    public CompletionTask createTask(int var1, JTextComponent var2);

    public int getAutoQueryTypes(JTextComponent var1, String var2);
}

