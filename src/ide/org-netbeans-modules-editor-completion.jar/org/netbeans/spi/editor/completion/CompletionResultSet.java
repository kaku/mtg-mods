/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion;

import java.util.Collection;
import javax.swing.JToolTip;
import org.netbeans.modules.editor.completion.CompletionResultSetImpl;
import org.netbeans.modules.editor.completion.CompletionSpiPackageAccessor;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;

public final class CompletionResultSet {
    public static final int PRIORITY_SORT_TYPE = 0;
    public static final int TEXT_SORT_TYPE = 1;
    private CompletionResultSetImpl impl;

    CompletionResultSet(CompletionResultSetImpl impl) {
        this.impl = impl;
        impl.setResultSet(this);
    }

    @Deprecated
    public void setTitle(String title) {
        this.impl.setTitle(title);
    }

    public void setAnchorOffset(int anchorOffset) {
        this.impl.setAnchorOffset(anchorOffset);
    }

    public boolean addItem(CompletionItem item) {
        return this.impl.addItem(item);
    }

    public boolean addAllItems(Collection<? extends CompletionItem> items) {
        return this.impl.addAllItems(items);
    }

    public void estimateItems(int estimatedItemCount, int estimatedItemWidth) {
        this.impl.estimateItems(estimatedItemCount, estimatedItemWidth);
    }

    public void setHasAdditionalItems(boolean value) {
        this.impl.setHasAdditionalItems(value);
    }

    public void setHasAdditionalItemsText(String text) {
        this.impl.setHasAdditionalItemsText(text);
    }

    public void setDocumentation(CompletionDocumentation documentation) {
        this.impl.setDocumentation(documentation);
    }

    public void setToolTip(JToolTip toolTip) {
        this.impl.setToolTip(toolTip);
    }

    public void finish() {
        this.impl.finish();
    }

    public boolean isFinished() {
        return this.impl.isFinished();
    }

    public int getSortType() {
        return this.impl.getSortType();
    }

    public void setWaitText(String waitText) {
        this.impl.setWaitText(waitText);
    }

    static {
        CompletionSpiPackageAccessor.register(new SpiAccessor());
    }

    private static final class SpiAccessor
    extends CompletionSpiPackageAccessor {
        private SpiAccessor() {
        }

        @Override
        public CompletionResultSet createCompletionResultSet(CompletionResultSetImpl impl) {
            return new CompletionResultSet(impl);
        }
    }

}

