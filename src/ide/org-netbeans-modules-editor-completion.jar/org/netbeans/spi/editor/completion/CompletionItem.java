/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.completion.CompletionTask;

public interface CompletionItem {
    public void defaultAction(JTextComponent var1);

    public void processKeyEvent(KeyEvent var1);

    public int getPreferredWidth(Graphics var1, Font var2);

    public void render(Graphics var1, Font var2, Color var3, Color var4, int var5, int var6, boolean var7);

    public CompletionTask createDocumentationTask();

    public CompletionTask createToolTipTask();

    public boolean instantSubstitution(JTextComponent var1);

    public int getSortPriority();

    public CharSequence getSortText();

    public CharSequence getInsertPrefix();
}

