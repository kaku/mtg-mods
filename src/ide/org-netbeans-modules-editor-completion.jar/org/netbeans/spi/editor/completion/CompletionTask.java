/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion;

import org.netbeans.spi.editor.completion.CompletionResultSet;

public interface CompletionTask {
    public void query(CompletionResultSet var1);

    public void refresh(CompletionResultSet var1);

    public void cancel();
}

