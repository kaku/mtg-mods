/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.spi.editor.completion.support;

import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.openide.util.RequestProcessor;

public final class AsyncCompletionTask
implements CompletionTask,
Runnable {
    private static final RequestProcessor RP = new RequestProcessor("Code Completion", 10, false, false);
    private final AsyncCompletionQuery query;
    private final JTextComponent component;
    private Document doc;
    private int queryCaretOffset;
    private CompletionResultSet queryResultSet;
    private CompletionResultSet refreshResultSet;
    private RequestProcessor.Task rpTask;
    private boolean cancelled;
    private boolean queryInvoked;

    public AsyncCompletionTask(AsyncCompletionQuery query, JTextComponent component) {
        assert (query != null);
        this.query = query;
        this.component = component;
        query.initTask(this);
    }

    public AsyncCompletionTask(AsyncCompletionQuery query) {
        this(query, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void query(CompletionResultSet resultSet) {
        assert (resultSet != null);
        assert (SwingUtilities.isEventDispatchThread());
        this.doc = this.component != null ? this.component.getDocument() : null;
        this.queryInvoked = true;
        AsyncCompletionTask asyncCompletionTask = this;
        synchronized (asyncCompletionTask) {
            this.performQuery(resultSet);
        }
    }

    @Override
    public void refresh(CompletionResultSet resultSet) {
        assert (SwingUtilities.isEventDispatchThread());
        assert (!this.cancelled);
        if (this.queryInvoked) {
            assert (resultSet != null);
            this.refreshResultSet = resultSet;
            this.refreshImpl();
        } else {
            this.query.preQueryUpdate(this.component);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void cancel() {
        this.cancelled = true;
        AsyncCompletionTask asyncCompletionTask = this;
        synchronized (asyncCompletionTask) {
            if (this.rpTask != null) {
                this.rpTask.cancel();
                this.rpTask = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void performQuery(CompletionResultSet resultSet) {
        this.queryCaretOffset = this.component != null ? this.component.getSelectionStart() : -1;
        this.query.prepareQuery(this.component);
        AsyncCompletionTask asyncCompletionTask = this;
        synchronized (asyncCompletionTask) {
            this.queryResultSet = resultSet;
            this.rpTask = RP.post((Runnable)this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void refreshImpl() {
        boolean rpTaskFinished;
        CompletionResultSet resultSet;
        AsyncCompletionTask asyncCompletionTask = this;
        synchronized (asyncCompletionTask) {
            rpTaskFinished = this.rpTask == null;
            resultSet = this.refreshResultSet;
        }
        if (resultSet != null) {
            if (rpTaskFinished) {
                asyncCompletionTask = this;
                synchronized (asyncCompletionTask) {
                    this.refreshResultSet = null;
                }
                if (this.query.canFilter(this.component)) {
                    this.query.filter(resultSet);
                    assert (resultSet.isFinished());
                } else {
                    this.performQuery(resultSet);
                }
            } else if (!this.query.canFilter(this.component)) {
                // empty if block
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        CompletionResultSet resultSet = this.queryResultSet;
        if (resultSet != null) {
            this.query.query(resultSet, this.doc, this.queryCaretOffset);
            assert (resultSet.isFinished());
        }
        AsyncCompletionTask asyncCompletionTask = this;
        synchronized (asyncCompletionTask) {
            this.rpTask = null;
            this.queryResultSet = null;
            if (this.refreshResultSet != null) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        AsyncCompletionTask.this.refreshImpl();
                    }
                });
            }
        }
    }

    synchronized boolean isCancelled() {
        return this.cancelled;
    }

    public String toString() {
        return "AsyncCompletionTask: query=" + this.query;
    }

}

