/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion.support;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import javax.swing.ImageIcon;
import org.netbeans.modules.editor.completion.PatchedHtmlRenderer;

public final class CompletionUtilities {
    private static final int BEFORE_ICON_GAP = 1;
    private static final int AFTER_ICON_GAP = 4;
    private static final int ICON_HEIGHT = 16;
    private static final int ICON_WIDTH = 16;
    private static final int BEFORE_RIGHT_TEXT_GAP = 5;
    private static final int AFTER_RIGHT_TEXT_GAP = 3;

    private CompletionUtilities() {
    }

    public static int getPreferredWidth(String leftHtmlText, String rightHtmlText, Graphics g, Font defaultFont) {
        int width = 24;
        if (leftHtmlText != null && leftHtmlText.length() > 0) {
            width += (int)PatchedHtmlRenderer.renderHTML(leftHtmlText, g, 0, 0, Integer.MAX_VALUE, 0, defaultFont, Color.black, 0, false, true);
        }
        if (rightHtmlText != null && rightHtmlText.length() > 0) {
            if (leftHtmlText != null) {
                width += 5;
            }
            width += (int)PatchedHtmlRenderer.renderHTML(rightHtmlText, g, 0, 0, Integer.MAX_VALUE, 0, defaultFont, Color.black, 0, false, true);
        }
        return width;
    }

    public static void renderHtml(ImageIcon icon, String leftHtmlText, String rightHtmlText, Graphics g, Font defaultFont, Color defaultColor, int width, int height, boolean selected) {
        if (icon != null) {
            g.drawImage(icon.getImage(), 1, (height - icon.getIconHeight()) / 2, null);
        }
        int iconWidth = 1 + (icon != null ? icon.getIconWidth() : 16) + 4;
        int rightTextX = width - 3;
        FontMetrics fm = g.getFontMetrics(defaultFont);
        int textY = (height - fm.getHeight()) / 2 + fm.getHeight() - fm.getDescent();
        if (rightHtmlText != null && rightHtmlText.length() > 0) {
            int rightTextWidth = (int)PatchedHtmlRenderer.renderHTML(rightHtmlText, g, 0, 0, Integer.MAX_VALUE, 0, defaultFont, defaultColor, 0, false, true);
            rightTextX = Math.max(iconWidth, rightTextX - rightTextWidth);
            PatchedHtmlRenderer.renderHTML(rightHtmlText, g, rightTextX, textY, rightTextWidth, textY, defaultFont, defaultColor, 0, true, selected);
            rightTextX = Math.max(iconWidth, rightTextX - 5);
        }
        if (leftHtmlText != null && leftHtmlText.length() > 0 && rightTextX > iconWidth) {
            PatchedHtmlRenderer.renderHTML(leftHtmlText, g, iconWidth, textY, rightTextX - iconWidth, textY, defaultFont, defaultColor, 1, true, selected);
        }
    }
}

