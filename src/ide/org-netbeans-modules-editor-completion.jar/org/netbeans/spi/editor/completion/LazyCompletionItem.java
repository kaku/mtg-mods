/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion;

import org.netbeans.spi.editor.completion.CompletionItem;

public interface LazyCompletionItem
extends CompletionItem {
    public boolean accept();
}

