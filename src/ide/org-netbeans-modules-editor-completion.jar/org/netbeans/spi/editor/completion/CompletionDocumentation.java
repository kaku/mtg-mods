/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.completion;

import java.net.URL;
import javax.swing.Action;

public interface CompletionDocumentation {
    public String getText();

    public URL getURL();

    public CompletionDocumentation resolveLink(String var1);

    public Action getGotoSourceAction();
}

