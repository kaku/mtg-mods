/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.completion;

import org.netbeans.modules.editor.completion.CompletionImpl;

public final class Completion {
    private static final Completion singleton = new Completion();

    public static Completion get() {
        return singleton;
    }

    private Completion() {
    }

    public void showCompletion() {
        CompletionImpl.get().showCompletion();
    }

    public void hideCompletion() {
        CompletionImpl.get().hideCompletion();
    }

    public void showDocumentation() {
        CompletionImpl.get().showDocumentation();
    }

    public void hideDocumentation() {
        CompletionImpl.get().hideDocumentation();
    }

    public void showToolTip() {
        CompletionImpl.get().showToolTip();
    }

    public void hideToolTip() {
        CompletionImpl.get().hideToolTip();
    }

    public void hideAll() {
        CompletionImpl.get().hideAll();
    }

    private void repaintCompletionView() {
        CompletionImpl.get().repaintCompletionView();
    }
}

