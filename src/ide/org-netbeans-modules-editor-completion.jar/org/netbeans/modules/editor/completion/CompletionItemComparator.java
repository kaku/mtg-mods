/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.completion;

import java.util.Comparator;
import org.netbeans.spi.editor.completion.CompletionItem;

public class CompletionItemComparator
implements Comparator<CompletionItem> {
    public static final Comparator<CompletionItem> BY_PRIORITY = new CompletionItemComparator(true);
    public static final Comparator<CompletionItem> ALPHABETICAL = new CompletionItemComparator(false);
    private final boolean byPriority;

    private CompletionItemComparator(boolean byPriority) {
        this.byPriority = byPriority;
    }

    public static final Comparator<CompletionItem> get(int sortType) {
        if (sortType == 0) {
            return BY_PRIORITY;
        }
        if (sortType == 1) {
            return ALPHABETICAL;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public int compare(CompletionItem i1, CompletionItem i2) {
        if (i1 == i2) {
            return 0;
        }
        if (this.byPriority) {
            int importanceDiff = CompletionItemComparator.compareIntegers(i1.getSortPriority(), i2.getSortPriority());
            if (importanceDiff != 0) {
                return importanceDiff;
            }
            int alphabeticalDiff = CompletionItemComparator.compareText(i1.getSortText(), i2.getSortText());
            return alphabeticalDiff;
        }
        int alphabeticalDiff = CompletionItemComparator.compareText(i1.getSortText(), i2.getSortText());
        if (alphabeticalDiff != 0) {
            return alphabeticalDiff;
        }
        int importanceDiff = CompletionItemComparator.compareIntegers(i1.getSortPriority(), i2.getSortPriority());
        return importanceDiff;
    }

    private static int compareIntegers(int x, int y) {
        return x < y ? -1 : (x == y ? 0 : 1);
    }

    private static int compareText(CharSequence text1, CharSequence text2) {
        if (text1 == null) {
            text1 = "";
        }
        if (text2 == null) {
            text2 = "";
        }
        int len = Math.min(text1.length(), text2.length());
        for (int i = 0; i < len; ++i) {
            char ch2;
            char ch1 = text1.charAt(i);
            if (ch1 == (ch2 = text2.charAt(i))) continue;
            return ch1 - ch2;
        }
        return text1.length() - text2.length();
    }
}

