/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.modules.editor.completion;

import org.netbeans.modules.editor.completion.CompletionImpl;
import org.openide.modules.ModuleInstall;

public class CompletionModule
extends ModuleInstall {
    public void restored() {
        CompletionImpl.get();
    }
}

