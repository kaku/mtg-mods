/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.completion;

import java.awt.Dimension;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;

public final class CompletionSettings {
    private static final Logger LOG = Logger.getLogger(CompletionSettings.class.getName());
    private Preferences preferences = null;

    public static synchronized CompletionSettings getInstance(JTextComponent component) {
        return new CompletionSettings(component != null ? DocumentUtilities.getMimeType((JTextComponent)component) : null);
    }

    public boolean completionAutoPopup() {
        return this.preferences.getBoolean("completion-auto-popup", true);
    }

    public int completionAutoPopupDelay() {
        return this.preferences.getInt("completion-auto-popup-delay", 250);
    }

    public boolean documentationAutoPopup() {
        return this.preferences.getBoolean("javadoc-auto-popup", true);
    }

    boolean documentationPopupNextToCC() {
        return this.preferences.getBoolean("javadoc-popup-next-to-cc", false);
    }

    public int documentationAutoPopupDelay() {
        return this.preferences.getInt("javadoc-auto-popup-delay", 200);
    }

    public Dimension completionPaneMaximumSize() {
        return CompletionSettings.parseDimension(this.preferences.get("completion-pane-max-size", null), new Dimension(400, 300));
    }

    public Dimension documentationPopupPreferredSize() {
        return CompletionSettings.parseDimension(this.preferences.get("javadoc-preferred-size", null), new Dimension(500, 300));
    }

    public boolean completionInstantSubstitution() {
        return this.preferences.getBoolean("completion-instant-substitution", true);
    }

    private CompletionSettings(String mimeType) {
        this.preferences = (Preferences)(mimeType != null ? MimeLookup.getLookup((String)mimeType) : MimeLookup.getLookup((MimePath)MimePath.EMPTY)).lookup(Preferences.class);
    }

    private static Dimension parseDimension(String s, Dimension d) {
        int[] arr = new int[2];
        int i = 0;
        if (s != null) {
            StringTokenizer st = new StringTokenizer(s, ",");
            while (st.hasMoreElements()) {
                if (i > 1) {
                    return d;
                }
                try {
                    arr[i] = Integer.parseInt(st.nextToken());
                }
                catch (NumberFormatException nfe) {
                    LOG.log(Level.WARNING, null, nfe);
                    return d;
                }
                ++i;
            }
        }
        if (i != 2) {
            return d;
        }
        return new Dimension(arr[0], arr[1]);
    }
}

