/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseKit
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.completion;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.html.HTMLDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.modules.editor.completion.CompletionImpl;
import org.netbeans.modules.editor.completion.CompletionSettings;
import org.netbeans.modules.editor.completion.HTMLDocView;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class DocumentationScrollPane
extends JScrollPane {
    private static final String BACK = "org/netbeans/modules/editor/completion/resources/back.png";
    private static final String FORWARD = "org/netbeans/modules/editor/completion/resources/forward.png";
    private static final String GOTO_SOURCE = "org/netbeans/modules/editor/completion/resources/open_source_in_editor.png";
    private static final String SHOW_WEB = "org/netbeans/modules/editor/completion/resources/open_in_external_browser.png";
    private static final String JAVADOC_ESCAPE = "javadoc-escape";
    private static final String JAVADOC_BACK = "javadoc-back";
    private static final String JAVADOC_FORWARD = "javadoc-forward";
    private static final String JAVADOC_OPEN_IN_BROWSER = "javadoc-open-in-browser";
    private static final String JAVADOC_OPEN_SOURCE = "javadoc-open-source";
    private static final String COPY_TO_CLIPBOARD = "copy-to-clipboard";
    private static final int ACTION_JAVADOC_ESCAPE = 0;
    private static final int ACTION_JAVADOC_BACK = 1;
    private static final int ACTION_JAVADOC_FORWARD = 2;
    private static final int ACTION_JAVADOC_OPEN_IN_BROWSER = 3;
    private static final int ACTION_JAVADOC_OPEN_SOURCE = 4;
    private static final int ACTION_JAVADOC_COPY = 5;
    private static final RequestProcessor RP = new RequestProcessor(DocumentationScrollPane.class);
    private JButton bBack;
    private JButton bForward;
    private JButton bGoToSource;
    private JButton bShowWeb;
    private HTMLDocView view;
    private List<CompletionDocumentation> history = new ArrayList<CompletionDocumentation>(5);
    private int currentHistoryIndex = -1;
    protected CompletionDocumentation currentDocumentation = null;
    private Dimension documentationPreferredSize;
    private final JTextComponent editorComponent;

    public DocumentationScrollPane(JTextComponent editorComponent) {
        this.documentationPreferredSize = CompletionSettings.getInstance(editorComponent).documentationPopupPreferredSize();
        this.setPreferredSize(null);
        Color bgColor = new JEditorPane().getBackground();
        bgColor = new Color(Math.max(bgColor.getRed() - 8, 0), Math.max(bgColor.getGreen() - 8, 0), bgColor.getBlue());
        this.view = new HTMLDocView(bgColor);
        this.view.addHyperlinkListener(new HyperlinkAction());
        this.setViewportView(this.view);
        this.installTitleComponent();
        this.installKeybindings(editorComponent);
        this.editorComponent = editorComponent;
        this.setFocusable(true);
    }

    @Override
    public void setPreferredSize(Dimension preferredSize) {
        if (preferredSize == null) {
            preferredSize = this.documentationPreferredSize;
        }
        super.setPreferredSize(preferredSize);
    }

    public void setData(CompletionDocumentation doc) {
        this.setDocumentation(doc);
        if (doc != null && doc != CompletionImpl.PLEASE_WAIT_DOC) {
            this.addToHistory(doc);
        }
    }

    private ImageIcon resolveIcon(String res) {
        return ImageUtilities.loadImageIcon((String)res, (boolean)false);
    }

    private void installTitleComponent() {
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, UIManager.getColor("controlDkShadow")));
        toolbar.setLayout(new GridBagLayout());
        GridBagConstraints gdc = new GridBagConstraints();
        gdc.gridx = 0;
        gdc.gridy = 0;
        gdc.anchor = 17;
        ImageIcon icon = this.resolveIcon("org/netbeans/modules/editor/completion/resources/back.png");
        if (icon != null) {
            this.bBack = new BrowserButton(this, icon);
            this.bBack.addMouseListener(new MouseEventListener(this.bBack));
            this.bBack.setEnabled(false);
            this.bBack.setFocusable(false);
            this.bBack.setContentAreaFilled(false);
            this.bBack.setMargin(new Insets(0, 0, 0, 0));
            this.bBack.setToolTipText(NbBundle.getMessage(DocumentationScrollPane.class, (String)"HINT_doc_browser_back_button"));
            toolbar.add((Component)this.bBack, gdc);
        }
        gdc.gridx = 1;
        gdc.gridy = 0;
        gdc.anchor = 17;
        icon = this.resolveIcon("org/netbeans/modules/editor/completion/resources/forward.png");
        if (icon != null) {
            this.bForward = new BrowserButton(this, icon);
            this.bForward.addMouseListener(new MouseEventListener(this.bForward));
            this.bForward.setEnabled(false);
            this.bForward.setFocusable(false);
            this.bForward.setContentAreaFilled(false);
            this.bForward.setToolTipText(NbBundle.getMessage(DocumentationScrollPane.class, (String)"HINT_doc_browser_forward_button"));
            this.bForward.setMargin(new Insets(0, 0, 0, 0));
            toolbar.add((Component)this.bForward, gdc);
        }
        gdc.gridx = 2;
        gdc.gridy = 0;
        gdc.anchor = 17;
        icon = this.resolveIcon("org/netbeans/modules/editor/completion/resources/open_in_external_browser.png");
        if (icon != null) {
            this.bShowWeb = new BrowserButton(this, icon);
            this.bShowWeb.addMouseListener(new MouseEventListener(this.bShowWeb));
            this.bShowWeb.setEnabled(false);
            this.bShowWeb.setFocusable(false);
            this.bShowWeb.setContentAreaFilled(false);
            this.bShowWeb.setMargin(new Insets(0, 0, 0, 0));
            this.bShowWeb.setToolTipText(NbBundle.getMessage(DocumentationScrollPane.class, (String)"HINT_doc_browser_show_web_button"));
            toolbar.add((Component)this.bShowWeb, gdc);
        }
        gdc.gridx = 3;
        gdc.gridy = 0;
        gdc.weightx = 1.0;
        gdc.anchor = 17;
        icon = this.resolveIcon("org/netbeans/modules/editor/completion/resources/open_source_in_editor.png");
        if (icon != null) {
            this.bGoToSource = new BrowserButton(this, icon);
            this.bGoToSource.addMouseListener(new MouseEventListener(this.bGoToSource));
            this.bGoToSource.setEnabled(false);
            this.bGoToSource.setFocusable(false);
            this.bGoToSource.setContentAreaFilled(false);
            this.bGoToSource.setMargin(new Insets(0, 0, 0, 0));
            this.bGoToSource.setToolTipText(NbBundle.getMessage(DocumentationScrollPane.class, (String)"HINT_doc_browser_goto_source_button"));
            toolbar.add((Component)this.bGoToSource, gdc);
        }
        this.setColumnHeaderView(toolbar);
    }

    private synchronized void setDocumentation(CompletionDocumentation doc) {
        this.currentDocumentation = doc;
        if (this.currentDocumentation != null) {
            String text = this.currentDocumentation.getText();
            URL url = this.currentDocumentation.getURL();
            if (text != null) {
                Document document = this.view.getDocument();
                document.putProperty("stream", null);
                if (url != null && document instanceof HTMLDocument) {
                    ((HTMLDocument)document).setBase(url);
                }
                this.view.setContent(text, url != null ? url.getRef() : null);
            } else if (url != null) {
                try {
                    this.view.setPage(url);
                }
                catch (IOException ioe) {
                    StatusDisplayer.getDefault().setStatusText(ioe.toString());
                }
            }
            this.bShowWeb.setEnabled(url != null);
            this.bGoToSource.setEnabled(this.currentDocumentation.getGotoSourceAction() != null);
        }
    }

    private synchronized void addToHistory(CompletionDocumentation doc) {
        int histSize = this.history.size();
        for (int i = this.currentHistoryIndex + 1; i < histSize; ++i) {
            this.history.remove(this.history.size() - 1);
        }
        this.history.add(doc);
        this.currentHistoryIndex = this.history.size() - 1;
        if (this.currentHistoryIndex > 0) {
            this.bBack.setEnabled(true);
        }
        this.bForward.setEnabled(false);
    }

    private synchronized void backHistory() {
        if (this.currentHistoryIndex > 0) {
            --this.currentHistoryIndex;
            this.setDocumentation(this.history.get(this.currentHistoryIndex));
            if (this.currentHistoryIndex == 0) {
                this.bBack.setEnabled(false);
            }
            this.bForward.setEnabled(true);
        }
    }

    private synchronized void forwardHistory() {
        if (this.currentHistoryIndex < this.history.size() - 1) {
            ++this.currentHistoryIndex;
            this.setDocumentation(this.history.get(this.currentHistoryIndex));
            if (this.currentHistoryIndex == this.history.size() - 1) {
                this.bForward.setEnabled(false);
            }
            this.bBack.setEnabled(true);
        }
    }

    synchronized void clearHistory() {
        this.currentHistoryIndex = -1;
        this.history.clear();
        this.bBack.setEnabled(false);
        this.bForward.setEnabled(false);
    }

    private void openInExternalBrowser() {
        URL url;
        CompletionDocumentation cd = this.currentDocumentation;
        if (cd != null && (url = cd.getURL()) != null) {
            HtmlBrowser.URLDisplayer.getDefault().showURL(url);
        }
    }

    private void goToSource() {
        Action action;
        CompletionDocumentation cd = this.currentDocumentation;
        if (cd != null && (action = cd.getGotoSourceAction()) != null) {
            action.actionPerformed(new ActionEvent(cd, 0, null));
        }
    }

    private void copy() {
        Caret caret = this.view.getCaret();
        if (caret.getDot() != caret.getMark()) {
            this.view.copy();
        } else {
            this.editorComponent.copy();
        }
    }

    private KeyStroke[] findEditorKeys(String editorActionName, KeyStroke defaultKey, JTextComponent component) {
        KeyStroke[] ret = new KeyStroke[]{defaultKey};
        if (component != null) {
            KeyStroke[] keys;
            EditorKit kit;
            Action a;
            TextUI componentUI = component.getUI();
            Keymap km = component.getKeymap();
            if (componentUI != null && km != null && (kit = componentUI.getEditorKit(component)) instanceof BaseKit && (a = ((BaseKit)kit).getActionByName(editorActionName)) != null && (keys = km.getKeyStrokesForAction(a)) != null && keys.length > 0) {
                ret = keys;
            }
        }
        return ret;
    }

    private void registerKeybinding(int action, String actionName, KeyStroke stroke, String editorActionName, JTextComponent component) {
        KeyStroke[] keys = this.findEditorKeys(editorActionName, stroke, component);
        for (int i = 0; i < keys.length; ++i) {
            this.getInputMap().put(keys[i], actionName);
        }
        this.getActionMap().put(actionName, new DocPaneAction(action));
    }

    private void installKeybindings(JTextComponent component) {
        this.registerKeybinding(0, "javadoc-escape", KeyStroke.getKeyStroke(27, 0), "escape", component);
        this.registerKeybinding(1, "javadoc-back", KeyStroke.getKeyStroke(37, 8), null, component);
        this.registerKeybinding(2, "javadoc-forward", KeyStroke.getKeyStroke(39, 8), null, component);
        this.registerKeybinding(3, "javadoc-open-in-browser", KeyStroke.getKeyStroke(112, 9), null, component);
        this.registerKeybinding(4, "javadoc-open-source", KeyStroke.getKeyStroke(79, 10), null, component);
        this.registerKeybinding(5, "copy-to-clipboard", KeyStroke.getKeyStroke(67, 2), "copy-to-clipboard", component);
        this.mapWithCtrl(KeyStroke.getKeyStroke(38, 0));
        this.mapWithCtrl(KeyStroke.getKeyStroke(40, 0));
        this.mapWithCtrl(KeyStroke.getKeyStroke(33, 0));
        this.mapWithCtrl(KeyStroke.getKeyStroke(34, 0));
        this.mapWithCtrl(KeyStroke.getKeyStroke(39, 0));
        this.mapWithCtrl(KeyStroke.getKeyStroke(37, 0));
    }

    private void mapWithCtrl(KeyStroke key) {
        InputMap inputMap = this.getInputMap(1);
        Object actionKey = inputMap.get(key);
        if (actionKey != null) {
            key = KeyStroke.getKeyStroke(key.getKeyCode(), key.getModifiers() | 2);
            this.getInputMap().put(key, actionKey);
        }
    }

    private class DocPaneAction
    extends AbstractAction {
        private int action;

        private DocPaneAction(int action) {
            this.action = action;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            switch (this.action) {
                case 0: {
                    CompletionImpl.get().hideDocumentation(false);
                    break;
                }
                case 1: {
                    DocumentationScrollPane.this.backHistory();
                    break;
                }
                case 2: {
                    DocumentationScrollPane.this.forwardHistory();
                    break;
                }
                case 3: {
                    DocumentationScrollPane.this.openInExternalBrowser();
                    break;
                }
                case 4: {
                    DocumentationScrollPane.this.goToSource();
                    break;
                }
                case 5: {
                    DocumentationScrollPane.this.copy();
                }
            }
        }
    }

    private class HyperlinkAction
    implements HyperlinkListener {
        private HyperlinkAction() {
        }

        @Override
        public void hyperlinkUpdate(HyperlinkEvent e) {
            String desc;
            if (e != null && HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType()) && (desc = e.getDescription()) != null) {
                RP.post(new Runnable(){

                    @Override
                    public void run() {
                        CompletionDocumentation doc;
                        CompletionDocumentation cd = DocumentationScrollPane.this.currentDocumentation;
                        if (cd != null && (doc = cd.resolveLink(desc)) != null) {
                            EventQueue.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    DocumentationScrollPane.this.setData(doc);
                                }
                            });
                        }
                    }

                });
            }
        }

    }

    private class MouseEventListener
    extends MouseAdapter {
        private JButton button;

        MouseEventListener(JButton button) {
            this.button = button;
        }

        @Override
        public void mouseEntered(MouseEvent ev) {
            if (this.button.isEnabled()) {
                this.button.setContentAreaFilled(true);
                this.button.setBorderPainted(true);
            }
        }

        @Override
        public void mouseExited(MouseEvent ev) {
            this.button.setContentAreaFilled(false);
            this.button.setBorderPainted(false);
        }

        @Override
        public void mouseClicked(MouseEvent evt) {
            if (this.button.equals(DocumentationScrollPane.this.bBack)) {
                DocumentationScrollPane.this.backHistory();
            } else if (this.button.equals(DocumentationScrollPane.this.bForward)) {
                DocumentationScrollPane.this.forwardHistory();
            } else if (this.button.equals(DocumentationScrollPane.this.bGoToSource)) {
                DocumentationScrollPane.this.goToSource();
            } else if (this.button.equals(DocumentationScrollPane.this.bShowWeb)) {
                DocumentationScrollPane.this.openInExternalBrowser();
            }
        }
    }

    private class BrowserButton
    extends JButton {
        final /* synthetic */ DocumentationScrollPane this$0;

        public BrowserButton(DocumentationScrollPane documentationScrollPane) {
            this.this$0 = documentationScrollPane;
            this.setBorderPainted(false);
            this.setFocusPainted(false);
        }

        public BrowserButton(DocumentationScrollPane documentationScrollPane, String text) {
            this.this$0 = documentationScrollPane;
            super(text);
            this.setBorderPainted(false);
            this.setFocusPainted(false);
        }

        public BrowserButton(DocumentationScrollPane documentationScrollPane, Icon icon) {
            this.this$0 = documentationScrollPane;
            super(icon);
            this.setBorderPainted(false);
            this.setFocusPainted(false);
        }

        @Override
        public void setEnabled(boolean b) {
            super.setEnabled(b);
        }
    }

}

