/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.LocaleSupport
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.completion;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.LocaleSupport;
import org.netbeans.modules.editor.completion.CompletionImpl;
import org.netbeans.modules.editor.completion.LazyListModel;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompositeCompletionItem;
import org.netbeans.spi.editor.completion.LazyCompletionItem;
import org.openide.util.ImageUtilities;
import org.openide.util.Utilities;

public class CompletionJList
extends JList {
    private static final int DARKER_COLOR_COMPONENT = 5;
    private static final int SUB_MENU_ICON_GAP = 1;
    private static final ImageIcon subMenuIcon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/hints/resources/suggestion.gif", (boolean)false);
    private final RenderComponent renderComponent;
    private Graphics cellPreferredSizeGraphics;
    private int fixedItemHeight;
    private int maxVisibleRowCount;
    private JTextComponent editorComponent;
    private int smartIndex;
    private JLabel accessibleLabel;
    private JLabel accessibleFakeLabel;

    public CompletionJList(int maxVisibleRowCount, MouseListener mouseListener, JTextComponent editorComponent) {
        this.maxVisibleRowCount = maxVisibleRowCount;
        this.editorComponent = editorComponent;
        this.addMouseListener(mouseListener);
        this.setFont(editorComponent.getFont());
        this.setLayoutOrientation(0);
        this.fixedItemHeight = Math.max(16, this.getFontMetrics(this.getFont()).getHeight());
        this.setFixedCellHeight(this.fixedItemHeight);
        this.setModel(new Model(Collections.EMPTY_LIST));
        this.setFocusable(false);
        this.renderComponent = new RenderComponent();
        this.setSelectionMode(0);
        this.setCellRenderer(new ListCellRenderer(){
            private final ListCellRenderer defaultRenderer;

            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof CompletionItem) {
                    Color bgColor;
                    Color fgColor;
                    CompletionItem item = (CompletionItem)value;
                    CompletionJList.this.renderComponent.setItem(item);
                    CompletionJList.this.renderComponent.setSelected(isSelected);
                    CompletionJList.this.renderComponent.setSeparator(CompletionJList.this.smartIndex > 0 && CompletionJList.this.smartIndex == index);
                    if (isSelected) {
                        bgColor = list.getSelectionBackground();
                        fgColor = list.getSelectionForeground();
                    } else {
                        bgColor = list.getBackground();
                        if (index % 2 == 0) {
                            bgColor = new Color(Math.abs(bgColor.getRed() - 5), Math.abs(bgColor.getGreen() - 5), Math.abs(bgColor.getBlue() - 5));
                        }
                        fgColor = list.getForeground();
                    }
                    if (CompletionJList.this.renderComponent.getBackground() != bgColor) {
                        CompletionJList.this.renderComponent.setBackground(bgColor);
                    }
                    if (CompletionJList.this.renderComponent.getForeground() != fgColor) {
                        CompletionJList.this.renderComponent.setForeground(fgColor);
                    }
                    return CompletionJList.this.renderComponent;
                }
                return this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
        this.getAccessibleContext().setAccessibleName(LocaleSupport.getString((String)"ACSN_CompletionView"));
        this.getAccessibleContext().setAccessibleDescription(LocaleSupport.getString((String)"ACSD_CompletionView"));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        Map renderingHints;
        Map value = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
        Map map = renderingHints = value instanceof Map ? value : null;
        if (renderingHints != null && g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D)g;
            RenderingHints oldHints = g2d.getRenderingHints();
            g2d.addRenderingHints(renderingHints);
            try {
                super.paint(g2d);
            }
            finally {
                g2d.setRenderingHints(oldHints);
            }
        }
        super.paint(g);
    }

    void setData(List data) {
        this.smartIndex = -1;
        if (data != null) {
            int itemCount = data.size();
            ListCellRenderer renderer = this.getCellRenderer();
            int width = 0;
            int maxWidth = this.getParent().getParent().getMaximumSize().width;
            boolean stop = false;
            for (int index = 0; index < itemCount; ++index) {
                Component c;
                Object value = data.get(index);
                if (value instanceof LazyCompletionItem) {
                    maxWidth = (int)((double)Utilities.getUsableScreenBounds().width * 0.4);
                }
                if ((c = renderer.getListCellRendererComponent(this, value, index, false, false)) != null) {
                    Dimension cellSize = c.getPreferredSize();
                    if (cellSize.width > width && (width = cellSize.width) >= maxWidth) {
                        stop = true;
                    }
                }
                if (this.smartIndex < 0 && value instanceof CompletionItem && ((CompletionItem)value).getSortPriority() >= 0) {
                    this.smartIndex = index;
                }
                if (stop && this.smartIndex >= 0) break;
            }
            this.setFixedCellWidth(width);
            LazyListModel lm = LazyListModel.create(new Model(data), CompletionImpl.filter, 1.0, LocaleSupport.getString((String)"completion-please-wait"));
            this.setModel(lm);
            if (itemCount > 0) {
                this.setSelectedIndex(0);
            }
            int visibleRowCount = Math.min(itemCount, this.maxVisibleRowCount);
            this.setVisibleRowCount(visibleRowCount);
        }
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (this.isVisible()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    CompletionJList.this.updateAccessible();
                }
            });
        } else {
            AccessibleContext editorAC = this.editorComponent.getAccessibleContext();
            if (this.accessibleLabel != null) {
                editorAC.firePropertyChange("AccessibleActiveDescendant", this.accessibleLabel, null);
                editorAC.firePropertyChange("AccessibleChild", this.accessibleLabel, null);
            }
            if (this.accessibleFakeLabel != null) {
                editorAC.firePropertyChange("AccessibleChild", this.accessibleFakeLabel, null);
            }
        }
    }

    @Override
    public void setSelectedIndex(int index) {
        super.setSelectedIndex(index);
        if (this.isVisible()) {
            this.updateAccessible();
        }
    }

    private void updateAccessible() {
        AccessibleContext editorAC = this.editorComponent.getAccessibleContext();
        if (this.accessibleFakeLabel == null) {
            this.accessibleFakeLabel = new JLabel("");
            editorAC.firePropertyChange("AccessibleChild", null, this.accessibleFakeLabel);
        }
        JLabel orig = this.accessibleLabel;
        editorAC.firePropertyChange("AccessibleActiveDescendant", this.accessibleLabel, this.accessibleFakeLabel);
        Object selectedValue = this.getSelectedValue();
        String accName = selectedValue instanceof Accessible ? ((Accessible)selectedValue).getAccessibleContext().getAccessibleName() : selectedValue.toString();
        this.accessibleLabel = new JLabel(LocaleSupport.getString((String)"ACSN_CompletionView_SelectedItem") + accName);
        editorAC.firePropertyChange("AccessibleChild", null, this.accessibleLabel);
        editorAC.firePropertyChange("AccessibleActiveDescendant", this.accessibleFakeLabel, this.accessibleLabel);
        if (orig != null) {
            editorAC.firePropertyChange("AccessibleChild", orig, null);
        }
    }

    public void up() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx;
            for (idx = (this.getSelectedIndex() - 1 + size) % size; idx > 0 && this.getModel().getElementAt(idx) == null; --idx) {
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
        }
    }

    public void down() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx;
            for (idx = (this.getSelectedIndex() + 1) % size; idx < size && this.getModel().getElementAt(idx) == null; ++idx) {
            }
            if (idx == size) {
                idx = 0;
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
        }
    }

    public void pageUp() {
        if (this.getModel().getSize() > 0) {
            int idx;
            int pageSize = Math.max(this.getLastVisibleIndex() - this.getFirstVisibleIndex(), 0);
            for (idx = Math.max((int)(this.getSelectedIndex() - pageSize), (int)0); idx > 0 && this.getModel().getElementAt(idx) == null; --idx) {
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
        }
    }

    public void pageDown() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx;
            int pageSize = Math.max(this.getLastVisibleIndex() - this.getFirstVisibleIndex(), 0);
            for (idx = Math.min((int)(this.getSelectedIndex() + pageSize), (int)(size - 1)); idx < size && this.getModel().getElementAt(idx) == null; ++idx) {
            }
            if (idx == size) {
                for (idx = Math.min((int)(this.getSelectedIndex() + pageSize), (int)(size - 1)); idx > 0 && this.getModel().getElementAt(idx) == null; --idx) {
                }
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
        }
    }

    public void begin() {
        if (this.getModel().getSize() > 0) {
            this.setSelectedIndex(0);
            this.ensureIndexIsVisible(0);
        }
    }

    public void end() {
        int size = this.getModel().getSize();
        if (size > 0) {
            int idx;
            for (idx = size - 1; idx > 0 && this.getModel().getElementAt(idx) == null; --idx) {
            }
            this.setSelectedIndex(idx);
            this.ensureIndexIsVisible(idx);
        }
    }

    public static int arrowSpan() {
        return 1 + subMenuIcon.getIconWidth() + 1;
    }

    private final class RenderComponent
    extends JComponent {
        private CompletionItem item;
        private boolean selected;
        private boolean separator;

        private RenderComponent() {
        }

        void setItem(CompletionItem item) {
            this.item = item;
        }

        void setSelected(boolean selected) {
            this.selected = selected;
        }

        void setSeparator(boolean separator) {
            this.separator = separator;
        }

        @Override
        public void paintComponent(Graphics g) {
            JViewport parent = (JViewport)CompletionJList.this.getParent();
            int itemRenderWidth = parent != null ? parent.getWidth() : this.getWidth();
            Color bgColor = this.getBackground();
            Color fgColor = this.getForeground();
            int height = this.getHeight();
            g.setColor(bgColor);
            g.fillRect(0, 0, itemRenderWidth, height);
            g.setColor(fgColor);
            this.item.render(g, CompletionJList.this.getFont(), this.getForeground(), bgColor, itemRenderWidth, this.getHeight(), this.selected);
            if (this.selected && this.item instanceof CompositeCompletionItem && !((CompositeCompletionItem)this.item).getSubItems().isEmpty()) {
                g.drawImage(subMenuIcon.getImage(), itemRenderWidth - subMenuIcon.getIconWidth() - 1, (height - subMenuIcon.getIconHeight()) / 2, null);
            }
            if (this.separator) {
                g.setColor(Color.gray);
                g.drawLine(0, 0, itemRenderWidth, 0);
                g.setColor(fgColor);
            }
        }

        @Override
        public Dimension getPreferredSize() {
            if (CompletionJList.this.cellPreferredSizeGraphics == null) {
                CompletionJList.this.cellPreferredSizeGraphics = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(1, 1).getGraphics();
                assert (CompletionJList.this.cellPreferredSizeGraphics != null);
            }
            return new Dimension(this.item.getPreferredWidth(CompletionJList.this.cellPreferredSizeGraphics, CompletionJList.this.getFont()), CompletionJList.this.fixedItemHeight);
        }
    }

    private final class Model
    extends AbstractListModel {
        List data;

        public Model(List data) {
            this.data = data;
        }

        @Override
        public int getSize() {
            return this.data.size();
        }

        @Override
        public Object getElementAt(int index) {
            return index >= 0 && index < this.data.size() ? this.data.get(index) : null;
        }
    }

}

