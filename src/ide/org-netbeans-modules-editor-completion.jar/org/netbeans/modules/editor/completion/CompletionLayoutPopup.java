/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.completion;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.completion.CompletionLayout;
import org.openide.util.Utilities;

abstract class CompletionLayoutPopup {
    static final double COMPL_COVERAGE = 0.4;
    static final double MAX_COMPL_COVERAGE = 0.9;
    private CompletionLayout layout;
    private Popup popup;
    private Rectangle popupBounds;
    private JComponent contentComponent;
    private int anchorOffset;
    private Rectangle anchorOffsetBounds;
    private boolean displayAboveCaret;
    private boolean preferDisplayAboveCaret;
    private boolean showRetainedPreferredSize;

    CompletionLayoutPopup() {
    }

    public final boolean isVisible() {
        return this.popup != null;
    }

    public final boolean isActive() {
        return this.contentComponent != null;
    }

    public final void hide() {
        if (this.isVisible()) {
            this.popup.hide();
            this.popup = null;
            this.popupBounds = null;
            this.contentComponent = null;
            this.anchorOffset = -1;
        }
    }

    public final boolean isDisplayAboveCaret() {
        return this.displayAboveCaret;
    }

    public final Rectangle getPopupBounds() {
        return this.popupBounds;
    }

    final void setLayout(CompletionLayout layout) {
        assert (layout != null);
        this.layout = layout;
    }

    final void setPreferDisplayAboveCaret(boolean preferDisplayAboveCaret) {
        this.preferDisplayAboveCaret = preferDisplayAboveCaret;
    }

    final void setContentComponent(JComponent contentComponent) {
        assert (contentComponent != null);
        this.contentComponent = contentComponent;
    }

    final void setAnchorOffset(int anchorOffset) {
        this.anchorOffset = anchorOffset;
        this.anchorOffsetBounds = null;
    }

    final void setLocation(Point location) {
        this.anchorOffset = -1;
        this.anchorOffsetBounds = new Rectangle(location);
    }

    final int getAnchorOffset() {
        JTextComponent editorComponent;
        int offset = this.anchorOffset;
        if (offset == -1 && (editorComponent = this.getEditorComponent()) != null) {
            offset = editorComponent.getSelectionStart();
        }
        return offset;
    }

    final JComponent getContentComponent() {
        return this.contentComponent;
    }

    final Dimension getPreferredSize() {
        JComponent comp = this.getContentComponent();
        if (comp == null) {
            return new Dimension(0, 0);
        }
        int screenWidth = Utilities.getUsableScreenBounds().width;
        Dimension maxSize = new Dimension((int)((double)screenWidth * 0.9), comp.getMaximumSize().height);
        this.setMaxSize(comp, maxSize);
        return comp.getPreferredSize();
    }

    private final void setMaxSize(JComponent comp, Dimension maxSize) {
        if (comp instanceof JPanel) {
            comp.getComponent(0).setMaximumSize(maxSize);
        } else {
            comp.setMaximumSize(maxSize);
        }
    }

    final void resetPreferredSize() {
        JComponent comp = this.getContentComponent();
        if (comp == null) {
            return;
        }
        comp.setPreferredSize(null);
    }

    final boolean isShowRetainedPreferredSize() {
        return this.showRetainedPreferredSize;
    }

    final CompletionLayout getLayout() {
        return this.layout;
    }

    final JTextComponent getEditorComponent() {
        return this.layout.getEditorComponent();
    }

    protected int getAnchorHorizontalShift() {
        return 0;
    }

    final Rectangle getAnchorOffsetBounds() {
        JTextComponent editorComponent = this.getEditorComponent();
        if (editorComponent == null) {
            return new Rectangle();
        }
        if (this.anchorOffsetBounds == null) {
            int anchorOffset = this.getAnchorOffset();
            try {
                this.anchorOffsetBounds = editorComponent.modelToView(anchorOffset);
                if (this.anchorOffsetBounds != null) {
                    this.anchorOffsetBounds.x -= this.getAnchorHorizontalShift();
                } else {
                    this.anchorOffsetBounds = new Rectangle();
                }
            }
            catch (BadLocationException e) {
                this.anchorOffsetBounds = new Rectangle();
            }
            Point anchorOffsetPoint = this.anchorOffsetBounds.getLocation();
            SwingUtilities.convertPointToScreen(anchorOffsetPoint, editorComponent);
            this.anchorOffsetBounds.setLocation(anchorOffsetPoint);
        }
        return this.anchorOffsetBounds;
    }

    final Popup getPopup() {
        return this.popup;
    }

    private Rectangle findPopupBounds(Rectangle occupiedBounds, boolean aboveOccupiedBounds) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        Dimension prefSize = this.getPreferredSize();
        Rectangle popupBounds = new Rectangle();
        popupBounds.x = Math.min(occupiedBounds.x, screen.x + screen.width - prefSize.width);
        popupBounds.x = Math.max(popupBounds.x, screen.x);
        popupBounds.width = Math.min(prefSize.width, screen.width);
        if (aboveOccupiedBounds) {
            popupBounds.height = Math.min(prefSize.height, occupiedBounds.y - screen.y - 1);
            popupBounds.y = occupiedBounds.y - 1 - popupBounds.height;
        } else {
            popupBounds.y = occupiedBounds.y + occupiedBounds.height + 1;
            popupBounds.height = Math.min(prefSize.height, screen.y + screen.height - popupBounds.y);
        }
        return popupBounds;
    }

    private void show(Rectangle popupBounds, boolean displayAboveCaret) {
        if (this.popup != null) {
            this.popup.hide();
            this.popup = null;
        }
        Dimension origPrefSize = this.getPreferredSize();
        Dimension newPrefSize = popupBounds.getSize();
        JComponent contComp = this.getContentComponent();
        if (contComp == null) {
            return;
        }
        contComp.setPreferredSize(newPrefSize);
        this.showRetainedPreferredSize = newPrefSize.equals(origPrefSize);
        PopupFactory factory = PopupFactory.getSharedInstance();
        JTextComponent owner = this.layout.getEditorComponent();
        if (owner != null && owner.getClientProperty("ForceHeavyweightCompletionPopup") != null) {
            owner = null;
        }
        if (displayAboveCaret && Utilities.isMac()) {
            popupBounds.y -= 10;
        }
        this.popup = factory.getPopup(owner, contComp, popupBounds.x, popupBounds.y);
        this.popup.show();
        this.popupBounds = popupBounds;
        this.displayAboveCaret = displayAboveCaret;
    }

    void showAlongAnchorBounds() {
        this.showAlongOccupiedBounds(this.getAnchorOffsetBounds());
    }

    void showAlongAnchorBounds(boolean aboveCaret) {
        this.showAlongOccupiedBounds(this.getAnchorOffsetBounds(), aboveCaret);
    }

    void showAlongOccupiedBounds(Rectangle occupiedBounds) {
        boolean aboveCaret = this.isEnoughSpace(occupiedBounds, this.preferDisplayAboveCaret) ? this.preferDisplayAboveCaret : this.isMoreSpaceAbove(occupiedBounds);
        Rectangle bounds = this.findPopupBounds(occupiedBounds, aboveCaret);
        this.show(bounds, aboveCaret);
    }

    void showAlongOrNextOccupiedBounds(Rectangle occupiedBounds, Rectangle unionBounds) {
        if (occupiedBounds != null) {
            Rectangle screen = Utilities.getUsableScreenBounds();
            Dimension prefSize = this.getPreferredSize();
            Rectangle bounds = new Rectangle();
            boolean aboveCaret = this.isEnoughSpace(occupiedBounds, this.preferDisplayAboveCaret) ? this.preferDisplayAboveCaret : false;
            boolean left = false;
            boolean right = false;
            if (occupiedBounds.x + occupiedBounds.width + prefSize.width < screen.width && occupiedBounds.y + prefSize.height < screen.height) {
                bounds.x = occupiedBounds.x + occupiedBounds.width + 1;
                right = true;
            }
            if (!right && occupiedBounds.x - prefSize.width > 0 && occupiedBounds.y + prefSize.height < screen.height) {
                bounds.x = occupiedBounds.x - prefSize.width - 1;
                left = true;
            }
            if (right || left) {
                bounds.width = prefSize.width;
                bounds.height = Math.min(prefSize.height, screen.height);
                bounds.y = aboveCaret ? occupiedBounds.y + occupiedBounds.height - prefSize.height : occupiedBounds.y;
                this.show(bounds, aboveCaret);
                return;
            }
        }
        this.showAlongOccupiedBounds(unionBounds);
    }

    void showAlongOccupiedBounds(Rectangle occupiedBounds, boolean aboveCaret) {
        Rectangle bounds = this.findPopupBounds(occupiedBounds, aboveCaret);
        this.show(bounds, aboveCaret);
    }

    boolean isMoreSpaceAbove(Rectangle bounds) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        int above = bounds.y - screen.y;
        int below = screen.y + screen.height - (bounds.y + bounds.height);
        return above > below;
    }

    boolean isEnoughSpace(Rectangle occupiedBounds) {
        return this.isEnoughSpace(occupiedBounds, this.preferDisplayAboveCaret);
    }

    boolean isEnoughSpace(Rectangle occupiedBounds, boolean aboveOccupiedBounds) {
        Rectangle screen = Utilities.getUsableScreenBounds();
        int freeHeight = aboveOccupiedBounds ? occupiedBounds.y - screen.y : screen.y + screen.height - (occupiedBounds.y + occupiedBounds.height);
        Dimension prefSize = this.getPreferredSize();
        return prefSize.height < freeHeight;
    }

    boolean isEnoughSpace(boolean aboveCaret) {
        return this.isEnoughSpace(this.getAnchorOffsetBounds(), aboveCaret);
    }

    public boolean isOverlapped(Rectangle bounds) {
        return this.isVisible() ? this.popupBounds.intersects(bounds) : false;
    }

    public boolean isOverlapped(CompletionLayoutPopup popup) {
        return popup.isVisible() ? this.isOverlapped(popup.getPopupBounds()) : false;
    }

    public Rectangle unionBounds(Rectangle bounds) {
        return this.isVisible() ? bounds.union(this.getPopupBounds()) : bounds;
    }

    public abstract void processKeyEvent(KeyEvent var1);
}

