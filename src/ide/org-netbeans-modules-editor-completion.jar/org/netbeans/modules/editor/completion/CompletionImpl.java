/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MultiKeymap
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.ErrorManager
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.completion;

import java.awt.Container;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JToolTip;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MultiKeymap;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.completion.CompletionImplProfile;
import org.netbeans.modules.editor.completion.CompletionItemComparator;
import org.netbeans.modules.editor.completion.CompletionLayout;
import org.netbeans.modules.editor.completion.CompletionResultSetImpl;
import org.netbeans.modules.editor.completion.CompletionSettings;
import org.netbeans.modules.editor.completion.LazyListModel;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.LazyCompletionItem;
import org.openide.ErrorManager;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public class CompletionImpl
extends MouseAdapter
implements DocumentListener,
CaretListener,
KeyListener,
FocusListener,
ListSelectionListener,
PropertyChangeListener,
ChangeListener {
    private static final Logger LOG = Logger.getLogger(CompletionImpl.class.getName());
    private static final boolean alphaSort = Boolean.getBoolean("org.netbeans.modules.editor.completion.alphabeticalSort");
    private static final Object CT_HANDLER_DOC_PROPERTY = "code-template-insert-handler";
    private static final Logger UI_LOG = Logger.getLogger("org.netbeans.ui.editor.completion");
    private static CompletionImpl singleton = null;
    private static final String NO_SUGGESTIONS = NbBundle.getMessage(CompletionImpl.class, (String)"completion-no-suggestions");
    private static final String PLEASE_WAIT = NbBundle.getMessage(CompletionImpl.class, (String)"completion-please-wait");
    private static final String COMPLETION_SHOW = "completion-show";
    private static final String COMPLETION_ALL_SHOW = "completion-all-show";
    private static final String DOC_SHOW = "doc-show";
    private static final String TOOLTIP_SHOW = "tooltip-show";
    private static final int PLEASE_WAIT_TIMEOUT = 750;
    private static final int PRESCAN = 25;
    static final CompletionDocumentation PLEASE_WAIT_DOC = new CompletionDocumentation(){

        @Override
        public String getText() {
            return PLEASE_WAIT;
        }

        @Override
        public URL getURL() {
            return null;
        }

        @Override
        public CompletionDocumentation resolveLink(String link) {
            return null;
        }

        @Override
        public Action getGotoSourceAction() {
            return null;
        }
    };
    static LazyListModel.Filter filter = new LazyListModel.Filter(){

        @Override
        public boolean accept(Object obj) {
            if (obj instanceof LazyCompletionItem) {
                return ((LazyCompletionItem)obj).accept();
            }
            return true;
        }

        @Override
        public void scheduleUpdate(Runnable run) {
            SwingUtilities.invokeLater(run);
        }
    };
    private WeakReference<JTextComponent> activeComponent = null;
    private WeakReference<Document> activeDocument = null;
    private InputMap inputMap;
    private ActionMap actionMap;
    private final CompletionLayout layout = new CompletionLayout();
    private CompletionProvider[] activeProviders = null;
    private HashMap<String, CompletionProvider[]> providersCache = new HashMap();
    private Result completionResult;
    private Result docResult;
    private Result toolTipResult;
    private Timer completionAutoPopupTimer;
    private Timer docAutoPopupTimer;
    private Timer pleaseWaitTimer;
    private boolean refreshedQuery = false;
    private boolean explicitQuery = false;
    private WeakReference<CompletionItem> lastSelectedItem = null;
    private int autoModEndOffset = -1;
    private boolean pleaseWaitDisplayed = false;
    private String completionShortcut = null;
    private Lookup.Result<KeyBindingSettings> kbs;
    private RequestProcessor.Task asyncWarmUpTask = null;
    private String asyncWarmUpMimeType = null;
    private static CompletionImplProfile profile;
    private final LookupListener shortcutsTracker;
    private Point lastViewPosition;

    public static CompletionImpl get() {
        if (singleton == null) {
            singleton = new CompletionImpl();
        }
        return singleton;
    }

    private CompletionImpl() {
        this.shortcutsTracker = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                Utilities.runInEventDispatchThread((Runnable)new Runnable(){

                    @Override
                    public void run() {
                        CompletionImpl.this.installKeybindings();
                    }
                });
            }

        };
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)this);
        this.completionAutoPopupTimer = new Timer(0, new ActionListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                Result localCompletionResult;
                CompletionImpl completionImpl = CompletionImpl.this;
                synchronized (completionImpl) {
                    localCompletionResult = CompletionImpl.this.completionResult;
                }
                if (localCompletionResult != null && !localCompletionResult.isQueryInvoked()) {
                    CompletionImpl.this.pleaseWaitTimer.restart();
                    CompletionImpl.this.refreshedQuery = false;
                    CompletionImpl.this.getActiveComponent().putClientProperty("completion-active", Boolean.TRUE);
                    CompletionImpl.queryResultSets(localCompletionResult.getResultSets());
                    localCompletionResult.queryInvoked();
                }
            }
        });
        this.completionAutoPopupTimer.setRepeats(false);
        this.docAutoPopupTimer = new Timer(0, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (CompletionImpl.this.lastSelectedItem == null || CompletionImpl.this.lastSelectedItem.get() != CompletionImpl.this.layout.getSelectedCompletionItem()) {
                    CompletionImpl.this.showDocumentation();
                }
            }
        });
        this.docAutoPopupTimer.setRepeats(false);
        this.pleaseWaitTimer = new Timer(750, new ActionListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                Result localCompletionResult;
                List<CompletionResultSetImpl> resultSets;
                String waitText = PLEASE_WAIT;
                boolean politeWaitText = false;
                CompletionImpl completionImpl = CompletionImpl.this;
                synchronized (completionImpl) {
                    localCompletionResult = CompletionImpl.this.completionResult;
                }
                if (localCompletionResult != null && (resultSets = localCompletionResult.getResultSets()) != null) {
                    for (CompletionResultSetImpl resultSet : resultSets) {
                        if (resultSet == null || resultSet.getWaitText() == null) continue;
                        waitText = resultSet.getWaitText();
                        politeWaitText = true;
                        break;
                    }
                }
                CompletionImpl.this.layout.showCompletion(Collections.singletonList(waitText), null, -1, CompletionImpl.this, null, null, 0);
                CompletionImpl.this.pleaseWaitDisplayed = true;
                if (!politeWaitText) {
                    long when = System.currentTimeMillis() - 750;
                    CompletionImpl.initializeProfiling(when);
                }
            }
        });
        this.pleaseWaitTimer.setRepeats(false);
        this.kbs = MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookupResult(KeyBindingSettings.class);
        this.kbs.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.shortcutsTracker, this.kbs));
    }

    private JTextComponent getActiveComponent() {
        return this.activeComponent != null ? this.activeComponent.get() : null;
    }

    private Document getActiveDocument() {
        return this.activeDocument != null ? this.activeDocument.get() : null;
    }

    int getSortType() {
        return alphaSort ? 1 : 0;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            return;
        }
        if (!DocumentUtilities.isTypingModification((DocumentEvent)e)) {
            return;
        }
        if (this.ensureActiveProviders()) {
            try {
                int modEndOffset = e.getOffset() + e.getLength();
                String typedText = e.getDocument().getText(e.getOffset(), e.getLength());
                for (int i = 0; i < this.activeProviders.length; ++i) {
                    boolean completionResultNull;
                    boolean tooltipResultNull;
                    int type = this.activeProviders[i].getAutoQueryTypes(this.getActiveComponent(), typedText);
                    CompletionImpl completionImpl = this;
                    synchronized (completionImpl) {
                        completionResultNull = this.completionResult == null;
                    }
                    if ((type & 1) != 0 && CompletionSettings.getInstance(this.getActiveComponent()).completionAutoPopup()) {
                        this.autoModEndOffset = modEndOffset;
                        if (completionResultNull) {
                            this.showCompletion(false, false, true, 1);
                        }
                    }
                    CompletionImpl completionImpl2 = this;
                    synchronized (completionImpl2) {
                        tooltipResultNull = this.toolTipResult == null;
                    }
                    if (!tooltipResultNull || (type & 4) == 0) continue;
                    this.showToolTip();
                }
            }
            catch (BadLocationException ex) {
                // empty catch block
            }
            if (this.completionAutoPopupTimer.isRunning()) {
                this.restartCompletionAutoPopupTimer();
            }
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            return;
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void caretUpdate(CaretEvent e) {
        assert (SwingUtilities.isEventDispatchThread());
        if (this.ensureActiveProviders()) {
            Result localCompletionResult;
            CompletionImpl completionImpl = this;
            synchronized (completionImpl) {
                localCompletionResult = this.completionResult;
            }
            if (!(this.autoModEndOffset < 0 || e.getDot() == this.autoModEndOffset || !this.completionAutoPopupTimer.isRunning() && localCompletionResult == null || this.layout.isCompletionVisible() && !this.pleaseWaitDisplayed)) {
                this.hideCompletion(false);
            }
            this.completionRefresh();
            this.toolTipRefresh();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        this.dispatchKeyEvent(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.dispatchKeyEvent(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        this.dispatchKeyEvent(e);
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.hideAll();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.hideAll();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        Container parent;
        boolean hide = true;
        JTextComponent component = this.getActiveComponent();
        Container container = parent = component != null ? component.getParent() : null;
        if (parent instanceof JViewport) {
            JViewport viewport = (JViewport)parent;
            Point viewPosition = viewport.getViewPosition();
            if (this.lastViewPosition != null && this.lastViewPosition.y == viewPosition.y) {
                hide = false;
            }
            this.lastViewPosition = viewPosition;
        }
        if (hide) {
            this.hideAll();
        }
    }

    public void hideAll() {
        this.hideToolTip();
        this.hideCompletion(true);
        this.hideDocumentation(true);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        assert (SwingUtilities.isEventDispatchThread());
        this.documentationCancel();
        if (this.layout.isDocumentationVisible() || CompletionSettings.getInstance(this.getActiveComponent()).documentationAutoPopup()) {
            this.restartDocumentationAutoPopupTimer();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        Document document;
        assert (SwingUtilities.isEventDispatchThread());
        boolean cancel = false;
        JTextComponent component = EditorRegistry.lastFocusedComponent();
        if (component != this.getActiveComponent()) {
            JViewport viewport;
            Container parent;
            this.initActiveProviders(component);
            JTextComponent activeJtc = this.getActiveComponent();
            if (activeJtc != null) {
                activeJtc.removeCaretListener(this);
                activeJtc.removeKeyListener(this);
                activeJtc.removeFocusListener(this);
                activeJtc.removeMouseListener(this);
                parent = activeJtc.getParent();
                if (parent instanceof JViewport) {
                    viewport = (JViewport)parent;
                    viewport.removeChangeListener(this);
                }
            }
            if (component != null) {
                component.addCaretListener(this);
                component.addKeyListener(this);
                component.addFocusListener(this);
                component.addMouseListener(this);
                parent = component.getParent();
                if (parent instanceof JViewport) {
                    viewport = (JViewport)parent;
                    viewport.addChangeListener(this);
                }
            }
            this.activeComponent = component != null ? new WeakReference<JTextComponent>(component) : null;
            this.layout.setEditorComponent(this.getActiveComponent());
            CompletionImpl.stopProfiling();
            this.installKeybindings();
            cancel = true;
        }
        Document document2 = document = component != null ? component.getDocument() : null;
        if (document != this.getActiveDocument()) {
            this.initActiveProviders(component);
            if (this.getActiveDocument() != null) {
                DocumentUtilities.removeDocumentListener((Document)this.getActiveDocument(), (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
            }
            if (document != null) {
                DocumentUtilities.addDocumentListener((Document)document, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
            }
            this.activeDocument = document != null ? new WeakReference<Document>(document) : null;
            cancel = true;
        }
        if (cancel) {
            this.completionCancel();
        }
    }

    private void initActiveProviders(JTextComponent component) {
        CompletionProvider[] arrcompletionProvider = this.activeProviders = component != null ? this.getCompletionProvidersForComponent(component, true) : null;
        if (LOG.isLoggable(Level.FINE)) {
            StringBuffer sb = new StringBuffer("Completion PROVIDERS:\n");
            if (this.activeProviders != null) {
                for (int i = 0; i < this.activeProviders.length; ++i) {
                    sb.append("providers[");
                    sb.append(i);
                    sb.append("]: ");
                    sb.append(this.activeProviders[i].getClass());
                    sb.append('\n');
                }
            }
            LOG.fine(sb.toString());
        }
    }

    private boolean ensureActiveProviders() {
        if (this.activeProviders != null) {
            return true;
        }
        JTextComponent component = this.getActiveComponent();
        CompletionProvider[] arrcompletionProvider = this.activeProviders = component != null ? this.getCompletionProvidersForComponent(component, false) : null;
        if (LOG.isLoggable(Level.FINE)) {
            StringBuffer sb = new StringBuffer("Completion PROVIDERS:\n");
            if (this.activeProviders != null) {
                for (int i = 0; i < this.activeProviders.length; ++i) {
                    sb.append("providers[");
                    sb.append(i);
                    sb.append("]: ");
                    sb.append(this.activeProviders[i].getClass());
                    sb.append('\n');
                }
            }
            LOG.fine(sb.toString());
        }
        return this.activeProviders != null;
    }

    private void restartCompletionAutoPopupTimer() {
        assert (SwingUtilities.isEventDispatchThread());
        int completionDelay = CompletionSettings.getInstance(this.getActiveComponent()).completionAutoPopupDelay();
        this.completionAutoPopupTimer.setInitialDelay(completionDelay);
        this.completionAutoPopupTimer.restart();
    }

    private void restartDocumentationAutoPopupTimer() {
        assert (SwingUtilities.isEventDispatchThread());
        int docDelay = CompletionSettings.getInstance(this.getActiveComponent()).documentationAutoPopupDelay();
        this.docAutoPopupTimer.setInitialDelay(docDelay);
        this.docAutoPopupTimer.restart();
    }

    private CompletionProvider[] getCompletionProvidersForComponent(JTextComponent component, boolean asyncWarmUp) {
        String mimeType;
        assert (SwingUtilities.isEventDispatchThread());
        if (component == null) {
            return null;
        }
        Object mimeTypeObj = component.getDocument().getProperty("mimeType");
        if (mimeTypeObj instanceof String) {
            mimeType = (String)mimeTypeObj;
        } else {
            BaseKit kit = Utilities.getKit((JTextComponent)component);
            if (kit == null) {
                return new CompletionProvider[0];
            }
            mimeType = kit.getContentType();
        }
        if (this.providersCache.containsKey(mimeType)) {
            return this.providersCache.get(mimeType);
        }
        if (this.asyncWarmUpTask != null) {
            if (asyncWarmUp && mimeType != null && mimeType.equals(this.asyncWarmUpMimeType)) {
                return null;
            }
            if (!this.asyncWarmUpTask.cancel()) {
                this.asyncWarmUpTask.waitFinished();
            }
            this.asyncWarmUpTask = null;
            this.asyncWarmUpMimeType = null;
        }
        final Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.get((String)mimeType));
        if (asyncWarmUp) {
            this.asyncWarmUpMimeType = mimeType;
            this.asyncWarmUpTask = RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    lookup.lookupAll(CompletionProvider.class);
                }
            });
            return null;
        }
        Collection col = lookup.lookupAll(CompletionProvider.class);
        int size = col.size();
        CompletionProvider[] ret = size == 0 ? null : col.toArray(new CompletionProvider[size]);
        this.providersCache.put(mimeType, ret);
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void dispatchKeyEvent(KeyEvent e) {
        Action action;
        if (e == null) {
            return;
        }
        KeyStroke ks = KeyStroke.getKeyStrokeForEvent(e);
        JTextComponent comp = this.getActiveComponent();
        boolean compEditable = comp != null && comp.isEditable();
        Document doc = comp.getDocument();
        boolean guardedPos = doc instanceof GuardedDocument && ((GuardedDocument)doc).isPosGuarded(comp.getSelectionEnd());
        Object obj = this.inputMap.get(ks);
        if (obj != null && (action = this.actionMap.get(obj)) != null) {
            if (compEditable) {
                action.actionPerformed(null);
            }
            e.consume();
            return;
        }
        if (this.layout.isCompletionVisible()) {
            block21 : {
                CompletionItem item = this.layout.getSelectedCompletionItem();
                if (item != null) {
                    CompletionImpl.sendUndoableEdit(doc, CloneableEditorSupport.BEGIN_COMMIT_GROUP);
                    try {
                        LogRecord r;
                        if (compEditable && !guardedPos) {
                            r = new LogRecord(Level.FINE, "COMPL_KEY_SELECT");
                            r.setParameters(new Object[]{Character.valueOf(e.getKeyChar()), this.layout.getSelectedIndex(), item.getClass().getSimpleName()});
                            item.processKeyEvent(e);
                            if (e.isConsumed()) {
                                CompletionImpl.uilog(r);
                                return;
                            }
                        }
                        if (e.getKeyCode() == 10 && e.getID() == 401 && (e.getModifiers() & 8) == 0) {
                            e.consume();
                            if (guardedPos) {
                                Toolkit.getDefaultToolkit().beep();
                            } else if (compEditable) {
                                if ((e.getModifiers() & 2) > 0) {
                                    this.consumeIdentifier();
                                }
                                r = new LogRecord(Level.FINE, "COMPL_KEY_SELECT_DEFAULT");
                                r.setParameters(new Object[]{Character.valueOf('\n'), this.layout.getSelectedIndex(), item.getClass().getSimpleName()});
                                item.defaultAction(this.getActiveComponent());
                                CompletionImpl.uilog(r);
                            }
                            return;
                        }
                        break block21;
                    }
                    finally {
                        CompletionImpl.sendUndoableEdit(doc, CloneableEditorSupport.END_COMMIT_GROUP);
                    }
                }
                if (e.getKeyCode() == 38 || e.getKeyCode() == 40 || e.getKeyCode() == 33 || e.getKeyCode() == 34 || e.getKeyCode() == 36 || e.getKeyCode() == 35) {
                    this.hideCompletion(false);
                }
            }
            if (e.getKeyCode() == 9 && doc.getProperty(CT_HANDLER_DOC_PROPERTY) == null) {
                e.consume();
                if (guardedPos) {
                    Toolkit.getDefaultToolkit().beep();
                } else if (compEditable && e.getID() == 401) {
                    this.insertCommonPrefix();
                }
                return;
            }
        }
        this.layout.processKeyEvent(e);
    }

    static void sendUndoableEdit(Document d, UndoableEdit ue) {
        if (d instanceof AbstractDocument) {
            UndoableEditListener[] uels = ((AbstractDocument)d).getUndoableEditListeners();
            UndoableEditEvent ev = new UndoableEditEvent(d, ue);
            for (UndoableEditListener uel : uels) {
                uel.undoableEditHappened(ev);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void completionQuery(boolean refreshedQuery, boolean delayQuery, int queryType) {
        Result newCompletionResult = new Result(this.activeProviders.length);
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            assert (this.completionResult == null);
            this.completionResult = newCompletionResult;
        }
        List<CompletionResultSetImpl> completionResultSets = newCompletionResult.getResultSets();
        for (int i = 0; i < this.activeProviders.length; ++i) {
            CompletionTask compTask = this.activeProviders[i].createTask(queryType, this.getActiveComponent());
            if (compTask == null) continue;
            CompletionResultSetImpl resultSet = new CompletionResultSetImpl(this, newCompletionResult, compTask, queryType);
            completionResultSets.add(resultSet);
        }
        if (completionResultSets.size() > 0) {
            if (delayQuery) {
                this.restartCompletionAutoPopupTimer();
            } else {
                this.pleaseWaitTimer.restart();
                this.refreshedQuery = refreshedQuery;
                this.getActiveComponent().putClientProperty("completion-active", Boolean.TRUE);
                CompletionImpl.queryResultSets(completionResultSets);
                newCompletionResult.queryInvoked();
            }
        } else {
            this.completionCancel();
            if (this.explicitQuery) {
                this.layout.showCompletion(Collections.singletonList(NO_SUGGESTIONS), null, -1, this, null, null, 0);
            }
            this.pleaseWaitDisplayed = false;
            CompletionImpl.stopProfiling();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void completionRefresh() {
        Result localCompletionResult;
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            localCompletionResult = this.completionResult;
        }
        if (localCompletionResult != null) {
            this.refreshedQuery = true;
            Result refreshResult = localCompletionResult.createRefreshResult();
            CompletionImpl completionImpl2 = this;
            synchronized (completionImpl2) {
                this.completionResult = refreshResult;
            }
            refreshResult.invokeRefresh(true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void completionCancel() {
        Result oldCompletionResult;
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            oldCompletionResult = this.completionResult;
            this.completionResult = null;
        }
        if (oldCompletionResult != null) {
            oldCompletionResult.cancel();
        }
    }

    private void consumeIdentifier() {
        int initCarPos;
        JTextComponent comp = this.getActiveComponent();
        BaseDocument doc = (BaseDocument)comp.getDocument();
        int carPos = initCarPos = comp.getCaretPosition();
        boolean nonChar = false;
        try {
            while (!nonChar) {
                char c = doc.getChars(carPos, 1)[0];
                if (!Character.isJavaIdentifierPart(c)) {
                    nonChar = true;
                }
                ++carPos;
            }
            doc.remove(initCarPos, carPos - initCarPos - 1);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void insertCommonPrefix() {
        Result localCompletionResult;
        JTextComponent c = this.getActiveComponent();
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            localCompletionResult = this.completionResult;
            if (localCompletionResult == null) {
                return;
            }
            if (!CompletionImpl.isAllResultsFinished(localCompletionResult.resultSets)) {
                Toolkit.getDefaultToolkit().beep();
                return;
            }
        }
        if (localCompletionResult != null) {
            int caretOffset;
            CharSequence commonText = null;
            int anchorOffset = -1;
            block8 : for (CompletionResultSetImpl resultSet : localCompletionResult.getResultSets()) {
                List<? extends CompletionItem> resultItems = resultSet.getItems();
                if (resultItems.size() <= 0) continue;
                if (anchorOffset >= -1) {
                    anchorOffset = anchorOffset > -1 && anchorOffset != resultSet.getAnchorOffset() ? -2 : resultSet.getAnchorOffset();
                }
                Iterator<? extends CompletionItem> itt = resultItems.iterator();
                block9 : while (itt.hasNext()) {
                    CharSequence text = itt.next().getInsertPrefix();
                    if (text == null) {
                        commonText = null;
                        break block8;
                    }
                    if (commonText == null) {
                        commonText = text;
                        continue;
                    }
                    if (text.length() < commonText.length()) {
                        commonText = commonText.subSequence(0, text.length());
                    }
                    for (int commonInd = 0; commonInd < commonText.length(); ++commonInd) {
                        if (text.charAt(commonInd) == commonText.charAt(commonInd)) continue;
                        if (commonInd == 0) {
                            commonText = null;
                            break block8;
                        }
                        commonText = commonText.subSequence(0, commonInd);
                        continue block9;
                    }
                }
            }
            if (commonText != null && anchorOffset >= 0 && (caretOffset = c.getSelectionStart()) - anchorOffset < commonText.length()) {
                Document doc = this.getActiveDocument();
                BaseDocument baseDoc = null;
                if (doc instanceof BaseDocument) {
                    baseDoc = (BaseDocument)doc;
                }
                if (baseDoc != null) {
                    baseDoc.atomicLock();
                }
                try {
                    doc.remove(anchorOffset, caretOffset - anchorOffset);
                    doc.insertString(anchorOffset, commonText.toString(), null);
                }
                catch (BadLocationException e) {}
                finally {
                    if (baseDoc != null) {
                        baseDoc.atomicUnlock();
                    }
                }
                return;
            }
            CompletionItem item = this.layout.getSelectedCompletionItem();
            if (item != null) {
                item.defaultAction(c);
            }
        }
    }

    public void showCompletion() {
        this.autoModEndOffset = -1;
        this.showCompletion(true, false, false, 1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void showCompletion(boolean explicitQuery, boolean refreshedQuery, boolean delayQuery, int queryType) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(0, explicitQuery, delayQuery, queryType));
            return;
        }
        LogRecord r = new LogRecord(Level.FINE, "COMPL_INVOCATION");
        r.setParameters(new Object[]{explicitQuery});
        CompletionImpl.uilog(r);
        this.explicitQuery = explicitQuery;
        if (this.ensureActiveProviders()) {
            this.completionAutoPopupTimer.stop();
            CompletionImpl completionImpl = this;
            synchronized (completionImpl) {
                if (explicitQuery && this.completionResult != null) {
                    CompletionResultSetImpl rSet;
                    Iterator i$ = this.completionResult.resultSets.iterator();
                    if (i$.hasNext() && (rSet = (CompletionResultSetImpl)i$.next()).getQueryType() == 9) {
                        return;
                    }
                    queryType = 9;
                }
            }
            this.completionCancel();
            this.completionQuery(refreshedQuery, delayQuery, queryType);
        }
    }

    void requestShowCompletionPane(final Result result) {
        boolean noSuggestions;
        this.pleaseWaitTimer.stop();
        CompletionImpl.stopProfiling();
        int size = 0;
        int qType = 0;
        boolean hasAdditionalItems = false;
        final StringBuilder hasAdditionalItemsText = new StringBuilder();
        List<CompletionResultSetImpl> completionResultSets = result.getResultSets();
        for (int i = completionResultSets.size() - 1; i >= 0; --i) {
            CompletionResultSetImpl resultSet = completionResultSets.get(i);
            size += resultSet.getItems().size();
            qType = resultSet.getQueryType();
            if (!resultSet.hasAdditionalItems()) continue;
            hasAdditionalItems = true;
            String s = resultSet.getHasAdditionalItemsText();
            if (s == null) continue;
            hasAdditionalItemsText.append(s);
        }
        ArrayList<? extends CompletionItem> resultItems = new ArrayList<CompletionItem>(size);
        String title = null;
        int anchorOffset = -1;
        if (size > 0) {
            for (int i2 = 0; i2 < completionResultSets.size(); ++i2) {
                CompletionResultSetImpl resultSet = completionResultSets.get(i2);
                List<? extends CompletionItem> items = resultSet.getItems();
                if (items.size() <= 0) continue;
                resultItems.addAll(items);
                if (title == null) {
                    title = resultSet.getTitle();
                }
                if (anchorOffset != -1) continue;
                anchorOffset = resultSet.getAnchorOffset();
            }
        }
        size = resultItems.size();
        final ArrayList<CompletionItem> sortedResultItems = new ArrayList<CompletionItem>(size);
        if (size > 0) {
            try {
                Collections.sort(resultItems, CompletionItemComparator.get(this.getSortType()));
            }
            catch (IllegalArgumentException iae) {
                LOG.warning("Unable to sort: " + resultItems);
            }
            int cnt = 0;
            for (int i3 = 0; i3 < size; ++i3) {
                CompletionItem item = (CompletionItem)resultItems.get(i3);
                if (cnt < 25) {
                    if (!filter.accept(item)) continue;
                    sortedResultItems.add(item);
                } else {
                    sortedResultItems.add(item);
                }
                ++cnt;
            }
        }
        boolean bl = noSuggestions = sortedResultItems.size() == 0;
        if (noSuggestions) {
            if (hasAdditionalItems && qType == 1 && !this.refreshedQuery) {
                this.showCompletion(this.explicitQuery, this.refreshedQuery, false, 9);
                return;
            }
            if (!this.explicitQuery) {
                this.hideCompletion(false);
                return;
            }
        }
        final String displayTitle = title;
        final int displayAnchorOffset = anchorOffset;
        final boolean displayAdditionalItems = hasAdditionalItems;
        Runnable requestShowRunnable = new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                JTextComponent c;
                CompletionSettings cs;
                block14 : {
                    CompletionImpl completionImpl = CompletionImpl.this;
                    synchronized (completionImpl) {
                        if (result != CompletionImpl.this.completionResult) {
                            return;
                        }
                    }
                    c = CompletionImpl.this.getActiveComponent();
                    Document doc = c.getDocument();
                    cs = CompletionSettings.getInstance(c);
                    int caretOffset = c.getSelectionStart();
                    if (sortedResultItems.size() == 1 && !CompletionImpl.this.refreshedQuery && CompletionImpl.this.explicitQuery && cs.completionInstantSubstitution() && c.isEditable() && (!(doc instanceof GuardedDocument) || !((GuardedDocument)doc).isPosGuarded(caretOffset))) {
                        try {
                            int[] block = Utilities.getIdentifierBlock((JTextComponent)c, (int)caretOffset);
                            if (block != null && block[1] != caretOffset) break block14;
                            CompletionItem item = (CompletionItem)sortedResultItems.get(0);
                            CompletionImpl.sendUndoableEdit(doc, CloneableEditorSupport.BEGIN_COMMIT_GROUP);
                            try {
                                if (item.instantSubstitution(c)) {
                                    return;
                                }
                            }
                            finally {
                                CompletionImpl.sendUndoableEdit(doc, CloneableEditorSupport.END_COMMIT_GROUP);
                            }
                        }
                        catch (BadLocationException ex) {
                            // empty catch block
                        }
                    }
                }
                int selectedIndex = CompletionImpl.this.getCompletionPreSelectionIndex(sortedResultItems);
                c.putClientProperty("completion-visible", Boolean.TRUE);
                CompletionImpl.this.layout.showCompletion(noSuggestions ? Collections.singletonList(NO_SUGGESTIONS) : sortedResultItems, displayTitle, displayAnchorOffset, CompletionImpl.this, displayAdditionalItems ? hasAdditionalItemsText.toString() : null, displayAdditionalItems ? CompletionImpl.this.completionShortcut : null, selectedIndex);
                CompletionImpl.this.pleaseWaitDisplayed = false;
                CompletionImpl.stopProfiling();
                if (cs.documentationAutoPopup()) {
                    if (noSuggestions) {
                        CompletionImpl.this.docAutoPopupTimer.stop();
                        CompletionImpl.this.documentationCancel();
                        CompletionImpl.this.layout.hideDocumentation();
                    } else {
                        CompletionImpl.this.restartDocumentationAutoPopupTimer();
                    }
                }
            }
        };
        CompletionImpl.runInAWT(requestShowRunnable);
    }

    private int getCompletionPreSelectionIndex(List<CompletionItem> items) {
        String prefix = null;
        if (this.getActiveDocument() instanceof BaseDocument) {
            BaseDocument doc = (BaseDocument)this.getActiveDocument();
            int caretOffset = this.getActiveComponent().getSelectionStart();
            try {
                int[] block = Utilities.getIdentifierBlock((BaseDocument)doc, (int)caretOffset);
                if (block != null) {
                    block[1] = caretOffset;
                    prefix = doc.getText(block);
                }
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
        }
        if (prefix != null && prefix.length() > 0) {
            int idx = 0;
            for (CompletionItem item : items) {
                CharSequence text = item.getInsertPrefix();
                if (text != null && text.toString().startsWith(prefix)) {
                    return idx;
                }
                ++idx;
            }
        }
        return 0;
    }

    public boolean hideCompletion() {
        return this.hideCompletion(true);
    }

    public boolean hideCompletion(boolean completionOnly) {
        this.completionCancel();
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(4, completionOnly));
            return false;
        }
        return this.hideCompletionPane(completionOnly);
    }

    private boolean hideCompletionPane(boolean completionOnly) {
        this.completionAutoPopupTimer.stop();
        this.pleaseWaitTimer.stop();
        CompletionImpl.stopProfiling();
        boolean hidePerformed = this.layout.hideCompletion();
        if (!this.layout.isCompletionVisible()) {
            this.pleaseWaitDisplayed = false;
            JTextComponent jtc = this.getActiveComponent();
            if (!completionOnly && hidePerformed && CompletionSettings.getInstance(jtc).documentationAutoPopup()) {
                this.hideDocumentation(true);
            }
            if (jtc != null) {
                jtc.putClientProperty("completion-visible", Boolean.FALSE);
                jtc.putClientProperty("completion-active", Boolean.FALSE);
            }
        }
        return hidePerformed;
    }

    public void showCompletionSubItems() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(1));
            return;
        }
        this.layout.showCompletionSubItems();
    }

    public void showDocumentation() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(2));
            return;
        }
        if (this.ensureActiveProviders()) {
            this.documentationCancel();
            this.layout.clearDocumentationHistory();
            this.documentationQuery();
        }
    }

    void requestShowDocumentationPane(Result result) {
        final CompletionResultSetImpl resultSet = CompletionImpl.findFirstValidResult(result.getResultSets());
        CompletionImpl.runInAWT(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                CompletionImpl completionImpl = CompletionImpl.this;
                synchronized (completionImpl) {
                    if (resultSet != null) {
                        CompletionImpl.this.layout.showDocumentation(resultSet.getDocumentation(), resultSet.getAnchorOffset());
                    } else {
                        CompletionImpl.this.documentationCancel();
                        CompletionImpl.this.layout.hideDocumentation();
                    }
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void documentationQuery() {
        Result newDocumentationResult = new Result(1);
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            assert (this.docResult == null);
            this.docResult = newDocumentationResult;
        }
        List<CompletionResultSetImpl> documentationResultSets = this.docResult.getResultSets();
        CompletionItem selectedItem = this.layout.getSelectedCompletionItem();
        if (selectedItem != null) {
            this.lastSelectedItem = new WeakReference<CompletionItem>(selectedItem);
            CompletionTask docTask = selectedItem.createDocumentationTask();
            if (docTask != null) {
                CompletionResultSetImpl resultSet = new CompletionResultSetImpl(this, newDocumentationResult, docTask, 2);
                documentationResultSets.add(resultSet);
            }
        } else {
            this.lastSelectedItem = null;
            for (int i = 0; i < this.activeProviders.length; ++i) {
                CompletionTask docTask = this.activeProviders[i].createTask(2, this.getActiveComponent());
                if (docTask == null) continue;
                CompletionResultSetImpl resultSet = new CompletionResultSetImpl(this, newDocumentationResult, docTask, 2);
                documentationResultSets.add(resultSet);
            }
        }
        if (documentationResultSets.size() > 0) {
            if (this.layout.isDocumentationVisible()) {
                this.layout.showDocumentation(PLEASE_WAIT_DOC, -1);
            }
            CompletionImpl.queryResultSets(documentationResultSets);
            newDocumentationResult.queryInvoked();
        } else {
            this.documentationCancel();
            this.layout.hideDocumentation();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void documentationCancel() {
        Result oldDocumentationResult;
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            oldDocumentationResult = this.docResult;
            this.docResult = null;
        }
        if (oldDocumentationResult != null) {
            oldDocumentationResult.cancel();
        }
    }

    public boolean hideDocumentation() {
        return this.hideDocumentation(true);
    }

    boolean hideDocumentation(boolean documentationOnly) {
        this.documentationCancel();
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(5, documentationOnly));
            return false;
        }
        return this.hideDocumentationPane(documentationOnly);
    }

    boolean hideDocumentationPane(boolean documentationOnly) {
        this.docAutoPopupTimer.stop();
        boolean hidePerformed = this.layout.hideDocumentation();
        if (!documentationOnly && hidePerformed && CompletionSettings.getInstance(this.getActiveComponent()).documentationAutoPopup()) {
            this.hideCompletion(true);
        }
        return hidePerformed;
    }

    public void showToolTip() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(3));
            return;
        }
        if (this.ensureActiveProviders()) {
            this.toolTipCancel();
            this.toolTipQuery();
        }
    }

    void requestShowToolTipPane(Result result) {
        final CompletionResultSetImpl resultSet = CompletionImpl.findFirstValidResult(result.getResultSets());
        CompletionImpl.runInAWT(new Runnable(){

            @Override
            public void run() {
                if (resultSet != null) {
                    CompletionImpl.this.layout.showToolTip(resultSet.getToolTip(), resultSet.getAnchorOffset());
                } else {
                    CompletionImpl.this.hideToolTip();
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void toolTipQuery() {
        CompletionTask toolTipTask;
        Result newToolTipResult = new Result(1);
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            assert (this.toolTipResult == null);
            this.toolTipResult = newToolTipResult;
        }
        List<CompletionResultSetImpl> toolTipResultSets = newToolTipResult.getResultSets();
        CompletionItem selectedItem = this.layout.getSelectedCompletionItem();
        if (selectedItem != null && (toolTipTask = selectedItem.createToolTipTask()) != null) {
            CompletionResultSetImpl resultSet = new CompletionResultSetImpl(this, newToolTipResult, toolTipTask, 4);
            toolTipResultSets.add(resultSet);
        } else {
            for (int i = 0; i < this.activeProviders.length; ++i) {
                toolTipTask = this.activeProviders[i].createTask(4, this.getActiveComponent());
                if (toolTipTask == null) continue;
                CompletionResultSetImpl resultSet = new CompletionResultSetImpl(this, newToolTipResult, toolTipTask, 4);
                toolTipResultSets.add(resultSet);
            }
        }
        CompletionImpl.queryResultSets(toolTipResultSets);
        newToolTipResult.queryInvoked();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void toolTipRefresh() {
        Result localToolTipResult;
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            localToolTipResult = this.toolTipResult;
        }
        if (localToolTipResult != null) {
            Result refreshResult = localToolTipResult.createRefreshResult();
            CompletionImpl completionImpl2 = this;
            synchronized (completionImpl2) {
                this.toolTipResult = refreshResult;
            }
            refreshResult.invokeRefresh(false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void toolTipCancel() {
        Result oldToolTipResult;
        CompletionImpl completionImpl = this;
        synchronized (completionImpl) {
            oldToolTipResult = this.toolTipResult;
            this.toolTipResult = null;
        }
        if (oldToolTipResult != null) {
            oldToolTipResult.cancel();
        }
    }

    public boolean hideToolTip() {
        this.toolTipCancel();
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new ParamRunnable(6));
            return false;
        }
        return this.hideToolTipPane();
    }

    boolean hideToolTipPane() {
        return this.layout.hideToolTip();
    }

    private KeyStroke[] findEditorKeys(String editorActionName) {
        if (editorActionName != null && this.getActiveComponent() != null) {
            EditorKit kit;
            Action a;
            TextUI ui = this.getActiveComponent().getUI();
            Keymap km = this.getActiveComponent().getKeymap();
            if (ui != null && km != null && (kit = ui.getEditorKit(this.getActiveComponent())) instanceof BaseKit && (a = ((BaseKit)kit).getActionByName(editorActionName)) != null) {
                KeyStroke[] keys = km.getKeyStrokesForAction(a);
                if (keys != null && keys.length > 0) {
                    return keys;
                }
                MultiKeymap km2 = ((BaseKit)kit).getKeymap();
                KeyStroke[] keys2 = km2.getKeyStrokesForAction(a);
                if (keys2 != null && keys2.length > 0) {
                    return keys2;
                }
            }
        }
        return new KeyStroke[0];
    }

    private void installKeybindings() {
        int i;
        this.actionMap = new ActionMap();
        this.inputMap = new InputMap();
        this.completionShortcut = null;
        this.kbs.allInstances();
        KeyStroke[] keys = this.findEditorKeys("completion-show");
        for (i = 0; i < keys.length; ++i) {
            this.inputMap.put(keys[i], "completion-show");
            if (this.completionShortcut != null) continue;
            this.completionShortcut = CompletionImpl.getKeyStrokeAsText(keys[i]);
        }
        this.actionMap.put("completion-show", new CompletionShowAction(1));
        keys = this.findEditorKeys("all-completion-show");
        for (i = 0; i < keys.length; ++i) {
            this.inputMap.put(keys[i], "completion-all-show");
        }
        this.actionMap.put("completion-all-show", new CompletionShowAction(9));
        keys = this.findEditorKeys("documentation-show");
        for (i = 0; i < keys.length; ++i) {
            this.inputMap.put(keys[i], "doc-show");
        }
        this.actionMap.put("doc-show", new DocShowAction());
        keys = this.findEditorKeys("tooltip-show");
        for (i = 0; i < keys.length; ++i) {
            this.inputMap.put(keys[i], "tooltip-show");
        }
        this.actionMap.put("tooltip-show", new ToolTipShowAction());
    }

    private static String getKeyStrokeAsText(KeyStroke keyStroke) {
        int modifiers = keyStroke.getModifiers();
        StringBuffer sb = new StringBuffer();
        sb.append('\'');
        if ((modifiers & 128) > 0) {
            sb.append("Ctrl+");
        }
        if ((modifiers & 512) > 0) {
            sb.append("Alt+");
        }
        if ((modifiers & 64) > 0) {
            sb.append("Shift+");
        }
        if ((modifiers & 256) > 0) {
            sb.append("Meta+");
        }
        if (keyStroke.getKeyCode() != 16 && keyStroke.getKeyCode() != 17 && keyStroke.getKeyCode() != 157 && keyStroke.getKeyCode() != 18 && keyStroke.getKeyCode() != 65406) {
            sb.append(org.openide.util.Utilities.keyToString((KeyStroke)KeyStroke.getKeyStroke(keyStroke.getKeyCode(), 0)));
        }
        sb.append('\'');
        return sb.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void finishNotify(CompletionResultSetImpl finishedResult) {
        boolean finished = false;
        switch (finishedResult.getQueryType()) {
            case 1: 
            case 9: {
                Result localResult;
                CompletionImpl completionImpl = this;
                synchronized (completionImpl) {
                    localResult = this.completionResult;
                    if (finishedResult.getResultId() == localResult) {
                        finished = CompletionImpl.isAllResultsFinished(localResult.getResultSets());
                    }
                }
                if (!finished) break;
                this.requestShowCompletionPane(localResult);
                break;
            }
            case 2: {
                Result localResult;
                CompletionImpl completionImpl = this;
                synchronized (completionImpl) {
                    localResult = this.docResult;
                    if (finishedResult.getResultId() == localResult) {
                        finished = CompletionImpl.isAllResultsFinished(localResult.getResultSets());
                    }
                }
                if (!finished) break;
                this.requestShowDocumentationPane(localResult);
                break;
            }
            case 4: {
                Result localResult;
                CompletionImpl completionImpl = this;
                synchronized (completionImpl) {
                    localResult = this.toolTipResult;
                    if (finishedResult.getResultId() == localResult) {
                        finished = CompletionImpl.isAllResultsFinished(localResult.getResultSets());
                    }
                }
                if (!finished) break;
                this.requestShowToolTipPane(localResult);
                break;
            }
            default: {
                throw new IllegalStateException();
            }
        }
    }

    private static boolean isAllResultsFinished(List<CompletionResultSetImpl> resultSets) {
        for (int i = resultSets.size() - 1; i >= 0; --i) {
            CompletionResultSetImpl result = resultSets.get(i);
            if (result.isFinished()) continue;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("CompletionTask: " + result.getTask() + " not finished yet\n");
            }
            return false;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("----- All tasks finished -----\n");
        }
        return true;
    }

    private static CompletionResultSetImpl findFirstValidResult(List<CompletionResultSetImpl> resultSets) {
        block4 : for (int i = 0; i < resultSets.size(); ++i) {
            CompletionResultSetImpl result = resultSets.get(i);
            switch (result.getQueryType()) {
                case 2: {
                    if (result.getDocumentation() == null) continue block4;
                    return result;
                }
                case 4: {
                    if (result.getToolTip() == null) continue block4;
                    return result;
                }
                default: {
                    throw new IllegalStateException();
                }
            }
        }
        return null;
    }

    private static void runInAWT(Runnable r) {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }

    CompletionLayout testGetCompletionLayout() {
        return this.layout;
    }

    void testSetActiveComponent(JTextComponent component) {
        this.activeComponent = new WeakReference<JTextComponent>(component);
    }

    public void repaintCompletionView() {
        this.layout.repaintCompletionView();
    }

    private static void queryResultSets(List<CompletionResultSetImpl> resultSets) {
        for (int i = 0; i < resultSets.size(); ++i) {
            CompletionResultSetImpl resultSet = resultSets.get(i);
            resultSet.getTask().query(resultSet.getResultSet());
        }
    }

    private static void createRefreshResultSets(List<CompletionResultSetImpl> resultSets, Result refreshResult) {
        List<CompletionResultSetImpl> refreshResultSets = refreshResult.getResultSets();
        int size = resultSets.size();
        for (int i = 0; i < size; ++i) {
            CompletionResultSetImpl result = resultSets.get(i);
            result.markInactive();
            result = new CompletionResultSetImpl(result.getCompletionImpl(), refreshResult, result.getTask(), result.getQueryType());
            refreshResultSets.add(result);
        }
    }

    private static void refreshResultSets(List<CompletionResultSetImpl> resultSets, boolean beforeQuery) {
        try {
            int size = resultSets.size();
            for (int i = 0; i < size; ++i) {
                CompletionResultSetImpl result = resultSets.get(i);
                result.getTask().refresh(beforeQuery ? null : result.getResultSet());
            }
        }
        catch (Exception ex) {
            ErrorManager.getDefault().notify((Throwable)ex);
        }
    }

    private static void cancelResultSets(List<CompletionResultSetImpl> resultSets) {
        int size = resultSets.size();
        for (int i = 0; i < size; ++i) {
            CompletionResultSetImpl result = resultSets.get(i);
            result.markInactive();
            result.getTask().cancel();
        }
    }

    public CompletionResultSetImpl createTestResultSet(CompletionTask task, int queryType) {
        return new CompletionResultSetImpl(this, "TestResult", task, queryType);
    }

    static void uilog(LogRecord rec) {
        rec.setResourceBundle(NbBundle.getBundle(CompletionImpl.class));
        rec.setResourceBundleName(CompletionImpl.class.getPackage().getName() + ".Bundle");
        rec.setLoggerName(UI_LOG.getName());
        UI_LOG.log(rec);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void initializeProfiling(long since) {
        boolean devel = false;
        if (!$assertionsDisabled) {
            devel = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (!devel) {
            return;
        }
        Class<CompletionImpl> class_ = CompletionImpl.class;
        synchronized (CompletionImpl.class) {
            CompletionImpl.stopProfiling();
            profile = new CompletionImplProfile(since);
            // ** MonitorExit[var3_2] (shouldn't be in output)
            return;
        }
    }

    private static synchronized void stopProfiling() {
        if (profile != null) {
            profile.stop();
            profile = null;
        }
    }

    final class Result {
        private final List<CompletionResultSetImpl> resultSets;
        private boolean invoked;
        private boolean cancelled;
        private boolean beforeQuery;

        Result(int resultSetsSize) {
            this.beforeQuery = true;
            this.resultSets = new ArrayList<CompletionResultSetImpl>(resultSetsSize);
        }

        List<CompletionResultSetImpl> getResultSets() {
            return this.resultSets;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void cancel() {
            boolean fin;
            Result result = this;
            synchronized (result) {
                assert (!this.cancelled);
                fin = this.invoked;
                if (!this.invoked) {
                    this.cancelled = true;
                }
            }
            if (fin) {
                CompletionImpl.cancelResultSets(this.resultSets);
            }
        }

        synchronized boolean isQueryInvoked() {
            return this.invoked;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        boolean queryInvoked() {
            boolean canc;
            Result result = this;
            synchronized (result) {
                assert (!this.invoked);
                this.invoked = true;
                canc = this.cancelled;
                this.beforeQuery = false;
            }
            if (canc) {
                CompletionImpl.cancelResultSets(this.resultSets);
            }
            return canc;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Result createRefreshResult() {
            Result result = this;
            synchronized (result) {
                if (this.cancelled) {
                    return null;
                }
                if (this.beforeQuery) {
                    return this;
                }
                assert (this.invoked);
                this.invoked = false;
            }
            CompletionImpl completionImpl = CompletionImpl.this;
            completionImpl.getClass();
            Result refreshResult = completionImpl.new Result(this.getResultSets().size());
            refreshResult.beforeQuery = this.beforeQuery;
            CompletionImpl.createRefreshResultSets(this.resultSets, refreshResult);
            return refreshResult;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void invokeRefresh(boolean docCancel) {
            CompletionImpl.refreshResultSets(this.getResultSets(), this.beforeQuery);
            if (!this.beforeQuery) {
                this.queryInvoked();
                CompletionImpl completionImpl = CompletionImpl.this;
                synchronized (completionImpl) {
                    if (CompletionImpl.this.completionResult != null && !CompletionImpl.isAllResultsFinished(CompletionImpl.this.completionResult.getResultSets())) {
                        if (docCancel) {
                            CompletionImpl.this.documentationCancel();
                        }
                        CompletionImpl.this.pleaseWaitTimer.restart();
                    }
                }
            }
        }
    }

    private final class ParamRunnable
    implements Runnable {
        private static final int SHOW_COMPLETION = 0;
        private static final int SHOW_COMPLETION_SUB_ITEMS = 1;
        private static final int SHOW_DOCUMENTATION = 2;
        private static final int SHOW_TOOL_TIP = 3;
        private static final int HIDE_COMPLETION_PANE = 4;
        private static final int HIDE_DOCUMENTATION_PANE = 5;
        private static final int HIDE_TOOL_TIP_PANE = 6;
        private final int opCode;
        private final boolean explicit;
        private final boolean delayQuery;
        private final int type;

        ParamRunnable(int opCode) {
            this(opCode, false);
        }

        ParamRunnable(int opCode, boolean explicit) {
            this(opCode, explicit, false, 1);
        }

        ParamRunnable(int opCode, boolean explicit, boolean delayQuery, int type) {
            this.opCode = opCode;
            this.explicit = explicit;
            this.delayQuery = delayQuery;
            this.type = type;
        }

        @Override
        public void run() {
            switch (this.opCode) {
                case 0: {
                    CompletionImpl.this.showCompletion(CompletionImpl.this.explicitQuery, false, this.delayQuery, this.type);
                    break;
                }
                case 1: {
                    CompletionImpl.this.showCompletion(CompletionImpl.this.explicitQuery, false, this.delayQuery, this.type);
                    break;
                }
                case 2: {
                    CompletionImpl.this.showDocumentation();
                    break;
                }
                case 3: {
                    CompletionImpl.this.showToolTip();
                    break;
                }
                case 4: {
                    CompletionImpl.this.hideCompletionPane(this.explicit);
                    break;
                }
                case 5: {
                    CompletionImpl.this.hideDocumentationPane(this.explicit);
                    break;
                }
                case 6: {
                    CompletionImpl.this.hideToolTipPane();
                    break;
                }
                default: {
                    throw new IllegalStateException();
                }
            }
        }
    }

    private final class ToolTipShowAction
    extends AbstractAction {
        private ToolTipShowAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CompletionImpl.this.showToolTip();
        }
    }

    private final class DocShowAction
    extends AbstractAction {
        private DocShowAction() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CompletionImpl.this.showDocumentation();
        }
    }

    private final class CompletionShowAction
    extends AbstractAction {
        private int queryType;

        private CompletionShowAction(int queryType) {
            this.queryType = queryType;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            CompletionImpl.this.autoModEndOffset = -1;
            CompletionImpl.this.showCompletion(true, false, false, this.queryType);
        }
    }

}

