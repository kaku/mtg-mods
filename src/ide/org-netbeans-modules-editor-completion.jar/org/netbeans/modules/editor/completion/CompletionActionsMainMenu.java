/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.MainMenuAction
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.completion;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.MainMenuAction;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public abstract class CompletionActionsMainMenu
extends MainMenuAction
implements Action {
    private AbstractAction delegate;

    public CompletionActionsMainMenu() {
        this.delegate = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        };
        this.putValue("Name", this.getActionName());
        this.setMenu();
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        this.delegate.removePropertyChangeListener(listener);
    }

    @Override
    public void putValue(String key, Object newValue) {
        this.delegate.putValue(key, newValue);
    }

    @Override
    public Object getValue(String key) {
        return this.delegate.getValue(key);
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        this.delegate.addPropertyChangeListener(listener);
    }

    @Override
    public void setEnabled(boolean newValue) {
        this.delegate.setEnabled(newValue);
    }

    @Override
    public boolean isEnabled() {
        return this.delegate.isEnabled();
    }

    protected void setMenu() {
        JMenuItem presenter;
        Action presenterAction;
        ActionMap am = this.getContextActionMap();
        Action action = null;
        if (am != null) {
            action = am.get(this.getActionName());
        }
        if ((presenterAction = (presenter = this.getMenuPresenter()).getAction()) == null) {
            presenter.setAction(this);
            presenter.setToolTipText(null);
            this.menuInitialized = false;
        } else if (!this.equals(presenterAction)) {
            presenter.setAction(this);
            presenter.setToolTipText(null);
            this.menuInitialized = false;
        }
        if (!this.menuInitialized) {
            Mnemonics.setLocalizedText((AbstractButton)presenter, (String)this.getMenuItemText());
            this.menuInitialized = true;
        }
        presenter.setEnabled(action != null);
        JTextComponent comp = Utilities.getFocusedComponent();
        if (comp != null && comp instanceof JEditorPane) {
            CompletionActionsMainMenu.addAccelerators((Action)this, (JMenuItem)presenter, (JTextComponent)comp);
        } else {
            presenter.setAccelerator(this.getDefaultAccelerator());
        }
    }

    public static final class ToolTipShow
    extends CompletionActionsMainMenu {
        protected String getMenuItemText() {
            return NbBundle.getBundle(CompletionActionsMainMenu.class).getString("tooltip-show-main_menu_item");
        }

        protected String getActionName() {
            return "tooltip-show";
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Completion.get().showToolTip();
        }
    }

    public static final class DocumentationShow
    extends CompletionActionsMainMenu {
        protected String getMenuItemText() {
            return NbBundle.getBundle(CompletionActionsMainMenu.class).getString("documentation-show-main_menu_item");
        }

        protected String getActionName() {
            return "documentation-show";
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Completion.get().showDocumentation();
        }
    }

    public static final class CompletionShow
    extends CompletionActionsMainMenu {
        protected String getMenuItemText() {
            return NbBundle.getBundle(CompletionActionsMainMenu.class).getString("completion-show-main_menu_item");
        }

        protected String getActionName() {
            return "completion-show";
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Completion.get().showCompletion();
        }
    }

}

