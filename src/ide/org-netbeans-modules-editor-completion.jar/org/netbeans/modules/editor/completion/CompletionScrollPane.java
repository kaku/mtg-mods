/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.completion;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.netbeans.modules.editor.completion.CompletionImpl;
import org.netbeans.modules.editor.completion.CompletionJList;
import org.netbeans.modules.editor.completion.CompletionSettings;
import org.netbeans.spi.editor.completion.CompletionItem;

public class CompletionScrollPane
extends JScrollPane {
    private static final String ESCAPE = "escape";
    private static final String COMPLETION_UP = "completion-up";
    private static final String COMPLETION_DOWN = "completion-down";
    private static final String COMPLETION_PGUP = "completion-pgup";
    private static final String COMPLETION_PGDN = "completion-pgdn";
    private static final String COMPLETION_BEGIN = "completion-begin";
    private static final String COMPLETION_END = "completion-end";
    private static final String COMPLETION_SUBITEMS_SHOW = "completion-subitems-show";
    private static final int ACTION_ESCAPE = 0;
    private static final int ACTION_COMPLETION_UP = 1;
    private static final int ACTION_COMPLETION_DOWN = 2;
    private static final int ACTION_COMPLETION_PGUP = 3;
    private static final int ACTION_COMPLETION_PGDN = 4;
    private static final int ACTION_COMPLETION_BEGIN = 5;
    private static final int ACTION_COMPLETION_END = 6;
    private static final int ACTION_COMPLETION_SUBITEMS_SHOW = 7;
    private final CompletionJList view;
    private List dataObj;
    private JLabel topLabel;

    public CompletionScrollPane(JTextComponent editorComponent, ListSelectionListener listSelectionListener, MouseListener mouseListener) {
        this.setHorizontalScrollBarPolicy(31);
        this.setVerticalScrollBarPolicy(20);
        this.setMaximumSize(CompletionSettings.getInstance(editorComponent).completionPaneMaximumSize());
        int maxVisibleRowCount = Math.max(2, this.getMaximumSize().height / 16 - 1);
        this.view = new CompletionJList(maxVisibleRowCount, mouseListener, editorComponent);
        if (listSelectionListener != null) {
            this.view.addListSelectionListener(listSelectionListener);
        }
        this.setViewportView(this.view);
        this.installKeybindings(editorComponent);
    }

    public void setData(List data, String title, int selectedIndex) {
        this.dataObj = data;
        this.view.setData(data);
        this.view.setSelectedIndex(selectedIndex);
        Rectangle r = this.view.getCellBounds(selectedIndex, selectedIndex);
        if (r != null) {
            this.view.scrollRectToVisible(r);
        }
        this.setTitle(title);
        this.setViewportView(this.getViewport().getView());
    }

    public CompletionItem getSelectedCompletionItem() {
        Object ret = this.view.getSelectedValue();
        return ret instanceof CompletionItem ? (CompletionItem)ret : null;
    }

    public int getSelectedIndex() {
        return this.view.getSelectedIndex();
    }

    public Point getSelectedLocation() {
        Rectangle r = this.view.getCellBounds(this.getSelectedIndex(), this.getSelectedIndex());
        Point p = new Point(r.getLocation());
        SwingUtilities.convertPointToScreen(p, this.view);
        p.x += r.width;
        return p;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension prefSize = super.getPreferredSize();
        Dimension labelSize = this.topLabel != null ? this.topLabel.getPreferredSize() : new Dimension(0, 0);
        Dimension maxSize = this.getMaximumSize();
        if (labelSize.width > prefSize.width) {
            prefSize.width = labelSize.width;
        }
        if (prefSize.width > maxSize.width) {
            prefSize.width = maxSize.width;
        }
        return prefSize;
    }

    protected CompletionJList getView() {
        return this.view;
    }

    private void setTitle(String title) {
        if (title == null) {
            if (this.topLabel != null) {
                this.setColumnHeader(null);
                this.topLabel = null;
            }
        } else if (this.topLabel != null) {
            this.topLabel.setText(title);
        } else {
            this.topLabel = new JLabel(title);
            this.topLabel.setForeground(Color.blue);
            this.topLabel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
            this.setColumnHeaderView(this.topLabel);
        }
    }

    private KeyStroke[] findEditorKeys(String editorActionName, KeyStroke defaultKey, JTextComponent component) {
        KeyStroke[] ret = new KeyStroke[]{defaultKey};
        if (component != null && editorActionName != null) {
            KeyStroke[] keys;
            Action a = component.getActionMap().get(editorActionName);
            Keymap km = component.getKeymap();
            if (a != null && km != null && (keys = km.getKeyStrokesForAction(a)) != null && keys.length > 0) {
                ret = keys;
            }
        }
        return ret;
    }

    private void registerKeybinding(int action, String actionName, KeyStroke stroke, String editorActionName, JTextComponent component) {
        KeyStroke[] keys;
        for (KeyStroke key : keys = this.findEditorKeys(editorActionName, stroke, component)) {
            this.getInputMap().put(key, actionName);
        }
        this.getActionMap().put(actionName, new CompletionPaneAction(action));
    }

    private void installKeybindings(JTextComponent component) {
        this.registerKeybinding(0, "escape", KeyStroke.getKeyStroke(27, 0), "escape", component);
        this.registerKeybinding(1, "completion-up", KeyStroke.getKeyStroke(38, 0), "caret-up", component);
        this.registerKeybinding(2, "completion-down", KeyStroke.getKeyStroke(40, 0), "caret-down", component);
        this.registerKeybinding(4, "completion-pgdn", KeyStroke.getKeyStroke(34, 0), "page-down", component);
        this.registerKeybinding(3, "completion-pgup", KeyStroke.getKeyStroke(33, 0), "page-up", component);
        this.registerKeybinding(5, "completion-begin", KeyStroke.getKeyStroke(36, 0), "caret-begin-line", component);
        this.registerKeybinding(6, "completion-end", KeyStroke.getKeyStroke(35, 0), "caret-end-line", component);
        this.registerKeybinding(7, "completion-subitems-show", KeyStroke.getKeyStroke(10, 8), null, component);
    }

    List testGetData() {
        return this.dataObj;
    }

    private class CompletionPaneAction
    extends AbstractAction {
        private final int action;

        private CompletionPaneAction(int action) {
            this.action = action;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            switch (this.action) {
                case 0: {
                    if (!CompletionImpl.get().hideCompletion(false)) break;
                    LogRecord r = new LogRecord(Level.FINE, "COMPL_CANCEL");
                    CompletionImpl.uilog(r);
                    break;
                }
                case 1: {
                    CompletionScrollPane.this.view.up();
                    break;
                }
                case 2: {
                    CompletionScrollPane.this.view.down();
                    break;
                }
                case 3: {
                    CompletionScrollPane.this.view.pageUp();
                    break;
                }
                case 4: {
                    CompletionScrollPane.this.view.pageDown();
                    break;
                }
                case 5: {
                    CompletionScrollPane.this.view.begin();
                    break;
                }
                case 6: {
                    CompletionScrollPane.this.view.end();
                    break;
                }
                case 7: {
                    CompletionImpl.get().showCompletionSubItems();
                }
            }
        }
    }

}

