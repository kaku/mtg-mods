/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.GuardedDocument
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.completion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolTip;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.modules.editor.completion.CompletionImpl;
import org.netbeans.modules.editor.completion.CompletionJList;
import org.netbeans.modules.editor.completion.CompletionLayoutPopup;
import org.netbeans.modules.editor.completion.CompletionScrollPane;
import org.netbeans.modules.editor.completion.CompletionSettings;
import org.netbeans.modules.editor.completion.DocumentationScrollPane;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompositeCompletionItem;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.NbBundle;

public final class CompletionLayout {
    public static final int COMPLETION_ITEM_HEIGHT = 16;
    private static final int COMPLETION_ANCHOR_HORIZONTAL_SHIFT = 22;
    static final int POPUP_VERTICAL_GAP = 1;
    private Reference<JTextComponent> editorComponentRef;
    private final CompletionPopup completionPopup = new CompletionPopup();
    private final DocPopup docPopup;
    private final TipPopup tipPopup;
    private LinkedList<CompletionLayoutPopup> visiblePopups;

    CompletionLayout() {
        this.completionPopup.setLayout(this);
        this.completionPopup.setPreferDisplayAboveCaret(false);
        this.docPopup = new DocPopup();
        this.docPopup.setLayout(this);
        this.docPopup.setPreferDisplayAboveCaret(false);
        this.tipPopup = new TipPopup();
        this.tipPopup.setLayout(this);
        this.tipPopup.setPreferDisplayAboveCaret(true);
        this.visiblePopups = new LinkedList();
    }

    public JTextComponent getEditorComponent() {
        return this.editorComponentRef != null ? this.editorComponentRef.get() : null;
    }

    public void setEditorComponent(JTextComponent editorComponent) {
        this.hideAll();
        this.editorComponentRef = new WeakReference<JTextComponent>(editorComponent);
    }

    private void hideAll() {
        for (CompletionLayoutPopup popup : this.visiblePopups) {
            popup.hide();
        }
        this.visiblePopups.clear();
    }

    public void showCompletion(List data, String title, int anchorOffset, ListSelectionListener listSelectionListener, String additionalItemsText, String shortcutHint, int selectedIndex) {
        this.completionPopup.show(data, title, anchorOffset, listSelectionListener, additionalItemsText, shortcutHint, selectedIndex);
        if (!this.visiblePopups.contains(this.completionPopup)) {
            this.visiblePopups.push(this.completionPopup);
        }
    }

    public void showCompletionSubItems() {
        Point p;
        List<? extends CompletionItem> subItems;
        CompletionItem item = this.getSelectedCompletionItem();
        List<? extends CompletionItem> list = subItems = item instanceof CompositeCompletionItem ? ((CompositeCompletionItem)item).getSubItems() : null;
        if (subItems != null && !subItems.isEmpty() && (p = this.getSelectedLocation()) != null) {
            CompletionPopup popup = new CompletionPopup();
            popup.setLayout(this);
            popup.show(subItems, p);
            if (!this.visiblePopups.contains(popup)) {
                this.visiblePopups.push(popup);
            }
        }
    }

    public boolean hideCompletion() {
        for (CompletionLayoutPopup popup : this.visiblePopups) {
            if (!(popup instanceof CompletionPopup) || !popup.isVisible()) continue;
            popup.hide();
            ((CompletionPopup)popup).completionScrollPane = null;
            this.visiblePopups.remove(popup);
            return true;
        }
        return false;
    }

    public boolean isCompletionVisible() {
        return this.completionPopup.isVisible();
    }

    public CompletionItem getSelectedCompletionItem() {
        for (CompletionLayoutPopup popup : this.visiblePopups) {
            if (!(popup instanceof CompletionPopup) || !popup.isVisible()) continue;
            return ((CompletionPopup)popup).getSelectedCompletionItem();
        }
        return null;
    }

    public int getSelectedIndex() {
        for (CompletionLayoutPopup popup : this.visiblePopups) {
            if (!(popup instanceof CompletionPopup) || !popup.isVisible()) continue;
            return ((CompletionPopup)popup).getSelectedIndex();
        }
        return -1;
    }

    public Point getSelectedLocation() {
        for (CompletionLayoutPopup popup : this.visiblePopups) {
            if (!(popup instanceof CompletionPopup) || !popup.isVisible()) continue;
            return ((CompletionPopup)popup).getSelectedLocation();
        }
        return null;
    }

    public void processKeyEvent(KeyEvent evt) {
        for (CompletionLayoutPopup popup : this.visiblePopups) {
            popup.processKeyEvent(evt);
            if (!evt.isConsumed()) continue;
            return;
        }
    }

    public void showDocumentation(CompletionDocumentation doc, int anchorOffset) {
        this.docPopup.show(doc, anchorOffset);
        if (!this.visiblePopups.contains(this.docPopup)) {
            this.visiblePopups.push(this.docPopup);
        }
    }

    public boolean hideDocumentation() {
        if (this.docPopup.isVisible()) {
            this.docPopup.getDocumentationScrollPane().setData(null);
            this.docPopup.clearHistory();
            this.docPopup.hide();
            this.visiblePopups.remove(this.docPopup);
            return true;
        }
        return false;
    }

    public boolean isDocumentationVisible() {
        return this.docPopup.isVisible();
    }

    public void clearDocumentationHistory() {
        this.docPopup.clearHistory();
    }

    public void showToolTip(JToolTip toolTip, int anchorOffset) {
        this.tipPopup.show(toolTip, anchorOffset);
        if (!this.visiblePopups.contains(this.tipPopup)) {
            this.visiblePopups.push(this.tipPopup);
        }
    }

    public boolean hideToolTip() {
        if (this.tipPopup.isVisible()) {
            this.tipPopup.hide();
            this.visiblePopups.remove(this.tipPopup);
            return true;
        }
        return false;
    }

    public boolean isToolTipVisible() {
        return this.tipPopup.isVisible();
    }

    void updateLayout(CompletionLayoutPopup popup) {
        popup.resetPreferredSize();
        if (popup == this.completionPopup) {
            if (this.isToolTipVisible()) {
                boolean wantAboveCaret;
                boolean bl = wantAboveCaret = !this.tipPopup.isDisplayAboveCaret();
                if (this.completionPopup.isEnoughSpace(wantAboveCaret)) {
                    this.completionPopup.showAlongAnchorBounds(wantAboveCaret);
                } else {
                    Rectangle occupiedBounds = popup.getAnchorOffsetBounds();
                    occupiedBounds = this.tipPopup.unionBounds(occupiedBounds);
                    this.completionPopup.showAlongOccupiedBounds(occupiedBounds, this.tipPopup.isDisplayAboveCaret());
                }
            } else {
                popup.showAlongAnchorBounds();
            }
            if (this.docPopup.isVisible() && (this.docPopup.isOverlapped(popup) || this.docPopup.isOverlapped(this.tipPopup) || this.docPopup.getAnchorOffset() != this.completionPopup.getAnchorOffset() || !this.docPopup.isShowRetainedPreferredSize())) {
                this.updateLayout(this.docPopup);
            }
        } else if (popup == this.docPopup) {
            if (this.isCompletionVisible()) {
                popup.setAnchorOffset(this.completionPopup.getAnchorOffset());
            }
            Rectangle occupiedBounds = popup.getAnchorOffsetBounds();
            occupiedBounds = this.tipPopup.unionBounds(this.completionPopup.unionBounds(occupiedBounds));
            if (CompletionSettings.getInstance(this.getEditorComponent()).documentationPopupNextToCC()) {
                this.docPopup.showAlongOrNextOccupiedBounds(this.completionPopup.getPopupBounds(), occupiedBounds);
            } else {
                this.docPopup.showAlongOccupiedBounds(occupiedBounds);
            }
        } else if (popup == this.tipPopup) {
            popup.showAlongAnchorBounds();
            if (this.completionPopup.isOverlapped(popup) || this.docPopup.isOverlapped(popup)) {
                this.updateLayout(this.completionPopup);
            }
        } else {
            Rectangle occupiedBounds = popup.getAnchorOffsetBounds();
            popup.showAlongOrNextOccupiedBounds(occupiedBounds, occupiedBounds);
        }
    }

    CompletionPopup testGetCompletionPopup() {
        return this.completionPopup;
    }

    void repaintCompletionView() {
        assert (EventQueue.isDispatchThread());
        CompletionScrollPane completionView = this.completionPopup.completionScrollPane;
        if (completionView != null && completionView.isVisible()) {
            completionView.repaint();
        }
    }

    private static final class TipPopup
    extends CompletionLayoutPopup {
        private TipPopup() {
        }

        protected void show(JToolTip toolTip, int anchorOffset) {
            JComponent lastComponent = null;
            if (this.isVisible()) {
                lastComponent = this.getContentComponent();
            }
            this.setContentComponent(toolTip);
            this.setAnchorOffset(anchorOffset);
            if (lastComponent != toolTip) {
                this.getLayout().updateLayout(this);
            }
        }

        @Override
        public void processKeyEvent(KeyEvent evt) {
            if (this.isVisible() && KeyStroke.getKeyStroke(27, 0).equals(KeyStroke.getKeyStrokeForEvent(evt))) {
                evt.consume();
                CompletionImpl.get().hideToolTip();
            }
        }
    }

    private static final class DocPopup
    extends CompletionLayoutPopup {
        private DocPopup() {
        }

        private DocumentationScrollPane getDocumentationScrollPane() {
            return (DocumentationScrollPane)this.getContentComponent();
        }

        protected void show(CompletionDocumentation doc, int anchorOffset) {
            JTextComponent editorComponent = this.getEditorComponent();
            if (editorComponent == null) {
                return;
            }
            if (!this.isVisible()) {
                this.setContentComponent(new DocumentationScrollPane(editorComponent));
            }
            this.getDocumentationScrollPane().setData(doc);
            if (!this.isVisible()) {
                this.setAnchorOffset(anchorOffset);
                this.getLayout().updateLayout(this);
            }
        }

        @Override
        public void processKeyEvent(KeyEvent evt) {
            Action action;
            Object actionMapKey;
            if (this.isVisible() && (actionMapKey = this.getDocumentationScrollPane().getInputMap().get(KeyStroke.getKeyStrokeForEvent(evt))) != null && (action = this.getDocumentationScrollPane().getActionMap().get(actionMapKey)) != null) {
                action.actionPerformed(new ActionEvent(this.getDocumentationScrollPane(), 0, null));
                evt.consume();
            }
        }

        public void clearHistory() {
            if (this.isVisible()) {
                this.getDocumentationScrollPane().clearHistory();
            }
        }

        @Override
        protected int getAnchorHorizontalShift() {
            return 22;
        }
    }

    private static final class CompletionPopup
    extends CompletionLayoutPopup {
        private CompletionScrollPane completionScrollPane;

        private CompletionPopup() {
        }

        public void show(List data, Point location) {
            this.show(data, null, -1, location, null, null, null, 0);
        }

        public void show(List data, String title, int anchorOffset, ListSelectionListener listSelectionListener, String additionalItemsText, String shortcutHint, int selectedIndex) {
            this.show(data, title, anchorOffset, null, listSelectionListener, additionalItemsText, shortcutHint, selectedIndex);
        }

        private void show(List data, String title, int anchorOffset, Point location, ListSelectionListener listSelectionListener, String additionalItemsText, String shortcutHint, int selectedIndex) {
            Dimension lastSize;
            JTextComponent editorComponent = this.getEditorComponent();
            if (editorComponent == null) {
                return;
            }
            int lastAnchorOffset = this.getAnchorOffset();
            if (this.isVisible() && this.getContentComponent() == this.completionScrollPane ^ shortcutHint != null) {
                lastSize = this.getContentComponent().getSize();
                this.resetPreferredSize();
            } else {
                lastSize = new Dimension(0, 0);
                this.completionScrollPane = new CompletionScrollPane(editorComponent, listSelectionListener, new MouseAdapter(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void mouseClicked(MouseEvent evt) {
                        JTextComponent c = CompletionPopup.this.getEditorComponent();
                        if (SwingUtilities.isLeftMouseButton(evt)) {
                            CompletionItem selectedItem;
                            if (CompletionPopup.access$300((CompletionPopup)CompletionPopup.this).getView().getSize().width - CompletionJList.arrowSpan() <= evt.getPoint().x && (selectedItem = CompletionPopup.this.completionScrollPane.getSelectedCompletionItem()) instanceof CompositeCompletionItem && !((CompositeCompletionItem)selectedItem).getSubItems().isEmpty()) {
                                CompletionImpl.get().showCompletionSubItems();
                                evt.consume();
                                return;
                            }
                            for (CompletionLayoutPopup popup : CompletionPopup.this.getLayout().visiblePopups) {
                                if (!(popup instanceof CompletionPopup)) continue;
                                if (popup == CompletionPopup.this) break;
                                popup.hide();
                            }
                            if (c != null && evt.getClickCount() == 2 && (selectedItem = CompletionPopup.this.completionScrollPane.getSelectedCompletionItem()) != null) {
                                Document doc = c.getDocument();
                                if (doc instanceof GuardedDocument && ((GuardedDocument)doc).isPosGuarded(c.getSelectionEnd())) {
                                    Toolkit.getDefaultToolkit().beep();
                                } else {
                                    LogRecord r = new LogRecord(Level.FINE, "COMPL_MOUSE_SELECT");
                                    r.setParameters(new Object[]{null, CompletionPopup.this.completionScrollPane.getSelectedIndex(), selectedItem.getClass().getSimpleName()});
                                    CompletionImpl.uilog(r);
                                    CompletionImpl.sendUndoableEdit(doc, CloneableEditorSupport.BEGIN_COMMIT_GROUP);
                                    try {
                                        selectedItem.defaultAction(c);
                                    }
                                    finally {
                                        CompletionImpl.sendUndoableEdit(doc, CloneableEditorSupport.END_COMMIT_GROUP);
                                    }
                                }
                            }
                        }
                    }
                });
                if (shortcutHint != null) {
                    JPanel panel = new JPanel();
                    panel.setLayout(new BorderLayout());
                    panel.add((Component)this.completionScrollPane, "Center");
                    JLabel label = new JLabel();
                    label.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.white), BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, Color.gray), BorderFactory.createEmptyBorder(2, 2, 2, 2))));
                    label.setFont(label.getFont().deriveFont((float)label.getFont().getSize() - 2.0f));
                    label.setHorizontalAlignment(4);
                    label.setText(NbBundle.getMessage(CompletionLayout.class, (String)"TXT_completion_shortcut_tips", (Object)additionalItemsText, (Object)shortcutHint));
                    panel.add((Component)label, "South");
                    this.setContentComponent(panel);
                } else {
                    this.setContentComponent(this.completionScrollPane);
                }
            }
            this.getPreferredSize();
            this.completionScrollPane.setData(data, title, selectedIndex);
            if (location != null) {
                this.setLocation(location);
            } else {
                this.setAnchorOffset(anchorOffset);
            }
            Dimension prefSize = this.getPreferredSize();
            boolean changePopupSize = this.isVisible() ? prefSize.height != lastSize.height || prefSize.width != lastSize.width || anchorOffset != lastAnchorOffset : true;
            if (changePopupSize) {
                this.getLayout().updateLayout(this);
            }
        }

        public CompletionItem getSelectedCompletionItem() {
            return this.isVisible() ? this.completionScrollPane.getSelectedCompletionItem() : null;
        }

        public int getSelectedIndex() {
            return this.isVisible() ? this.completionScrollPane.getSelectedIndex() : -1;
        }

        public Point getSelectedLocation() {
            return this.isVisible() ? this.completionScrollPane.getSelectedLocation() : null;
        }

        @Override
        public void processKeyEvent(KeyEvent evt) {
            Action action;
            Object actionMapKey;
            if (this.isVisible() && (actionMapKey = this.completionScrollPane.getInputMap().get(KeyStroke.getKeyStrokeForEvent(evt))) != null && (action = this.completionScrollPane.getActionMap().get(actionMapKey)) != null) {
                action.actionPerformed(new ActionEvent(this.completionScrollPane, 0, null));
                evt.consume();
            }
        }

        @Override
        protected int getAnchorHorizontalShift() {
            return 22;
        }

    }

}

