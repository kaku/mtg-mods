/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.sampler.Sampler
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.completion;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.sampler.Sampler;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

final class CompletionImplProfile {
    private static final Logger LOG = Logger.getLogger(CompletionImplProfile.class.getName());
    static final RequestProcessor RP = new RequestProcessor("Completion Slowness");
    private final Sampler profiler;
    private boolean profiling;
    private final long time;

    CompletionImplProfile(long when) {
        this.time = when;
        this.profiler = Sampler.createSampler((String)"completion");
        this.profiling = true;
        if (this.profiler != null) {
            this.profiler.start();
            LOG.log(Level.FINE, "Profiling started {0} at {1}", new Object[]{this.profiler, this.time});
        }
    }

    final synchronized void stop() {
        if (!this.profiling) {
            return;
        }
        final long now = System.currentTimeMillis();
        RP.post(new Runnable(){

            @Override
            public void run() {
                try {
                    CompletionImplProfile.this.stopImpl(now);
                }
                catch (Exception ex) {
                    LOG.log(Level.INFO, "Cannot stop profiling", ex);
                }
            }
        });
        this.profiling = false;
    }

    private void stopImpl(long now) throws Exception {
        long delta = now - this.time;
        LOG.log(Level.FINE, "Profiling stopped at {0}", now);
        int report = Integer.getInteger("org.netbeans.modules.editor.completion.slowness.report", 2000);
        if (delta < (long)report) {
            LOG.log(Level.FINE, "Cancel profiling of {0}. Profiling {1}. Time {2} ms.", new Object[]{this.profiler, this.profiling, delta});
            if (this.profiler != null) {
                this.profiler.cancel();
            }
            return;
        }
        try {
            LOG.log(Level.FINE, "Obtaining snapshot for {0} ms.", delta);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(out);
            if (this.profiler != null) {
                this.profiler.stopAndWriteTo(dos);
            }
            dos.close();
            if (dos.size() > 0) {
                Object[] params = new Object[]{out.toByteArray(), delta, "CodeCompletion"};
                Logger.getLogger("org.netbeans.ui.performance").log(Level.CONFIG, "Slowness detected", params);
                LOG.log(Level.FINE, "Snapshot sent to UI logger. Size {0} bytes.", dos.size());
            } else {
                LOG.log(Level.WARNING, "No snapshot taken");
            }
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

}

