/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.completion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.swing.JToolTip;
import org.netbeans.modules.editor.completion.CompletionImpl;
import org.netbeans.modules.editor.completion.CompletionSpiPackageAccessor;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;

public final class CompletionResultSetImpl {
    private static final CompletionSpiPackageAccessor spi;
    private final CompletionImpl completionImpl;
    private final Object resultId;
    private final CompletionTask task;
    private final int queryType;
    private CompletionResultSet resultSet;
    private boolean active;
    private String title;
    private String waitText;
    private int anchorOffset;
    private List<CompletionItem> items;
    private boolean hasAdditionalItems;
    private String hasAdditionalItemsText;
    private boolean finished;
    private CompletionDocumentation documentation;
    private JToolTip toolTip;
    private int estimatedItemCount;
    private int estimatedItemWidth;

    CompletionResultSetImpl(CompletionImpl completionImpl, Object resultId, CompletionTask task, int queryType) {
        assert (completionImpl != null);
        assert (resultId != null);
        assert (task != null);
        this.completionImpl = completionImpl;
        this.resultId = resultId;
        this.task = task;
        this.queryType = queryType;
        this.anchorOffset = -1;
        this.estimatedItemCount = -1;
        this.active = true;
        spi.createCompletionResultSet(this);
    }

    public synchronized CompletionResultSet getResultSet() {
        return this.resultSet;
    }

    public synchronized void setResultSet(CompletionResultSet resultSet) {
        assert (resultSet != null);
        assert (this.resultSet == null);
        this.resultSet = resultSet;
    }

    public CompletionTask getTask() {
        return this.task;
    }

    public int getQueryType() {
        return this.queryType;
    }

    public synchronized void markInactive() {
        this.active = false;
    }

    public synchronized String getTitle() {
        return this.title;
    }

    public synchronized void setTitle(String title) {
        this.checkNotFinished();
        this.title = title;
    }

    public synchronized int getAnchorOffset() {
        return this.anchorOffset;
    }

    public synchronized void setAnchorOffset(int anchorOffset) {
        this.checkNotFinished();
        this.anchorOffset = anchorOffset;
    }

    public synchronized boolean addItem(CompletionItem item) {
        assert (item != null);
        this.checkNotFinished();
        if (!this.active || (this.queryType & 1) == 0) {
            return false;
        }
        if (this.items == null) {
            int estSize = this.estimatedItemCount == -1 ? 10 : this.estimatedItemCount;
            this.items = new ArrayList<CompletionItem>(estSize);
        }
        this.items.add(item);
        return this.items.size() < 1000;
    }

    public boolean addAllItems(Collection<? extends CompletionItem> items) {
        boolean cont = true;
        Iterator<? extends CompletionItem> it = items.iterator();
        while (it.hasNext()) {
            cont = this.addItem(it.next());
        }
        return cont;
    }

    public synchronized List<? extends CompletionItem> getItems() {
        assert (this.isFinished());
        return this.items != null ? this.items : Collections.emptyList();
    }

    public synchronized void setHasAdditionalItems(boolean value) {
        this.checkNotFinished();
        if (this.queryType != 1) {
            return;
        }
        this.hasAdditionalItems = value;
    }

    public synchronized boolean hasAdditionalItems() {
        return this.hasAdditionalItems;
    }

    public synchronized void setHasAdditionalItemsText(String text) {
        this.checkNotFinished();
        if (this.queryType != 1) {
            return;
        }
        this.hasAdditionalItemsText = text;
    }

    public synchronized String getHasAdditionalItemsText() {
        return this.hasAdditionalItemsText;
    }

    public synchronized void setDocumentation(CompletionDocumentation documentation) {
        this.checkNotFinished();
        if (!this.active || this.queryType != 2) {
            return;
        }
        this.documentation = documentation;
    }

    public synchronized CompletionDocumentation getDocumentation() {
        return this.documentation;
    }

    public synchronized JToolTip getToolTip() {
        return this.toolTip;
    }

    public synchronized void setToolTip(JToolTip toolTip) {
        this.checkNotFinished();
        if (!this.active || this.queryType != 4) {
            return;
        }
        this.toolTip = toolTip;
    }

    public synchronized boolean isFinished() {
        return this.finished;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void finish() {
        CompletionResultSetImpl completionResultSetImpl = this;
        synchronized (completionResultSetImpl) {
            if (this.finished) {
                throw new IllegalStateException("finish() already called");
            }
            this.finished = true;
        }
        this.completionImpl.finishNotify(this);
    }

    public int getSortType() {
        return this.completionImpl.getSortType();
    }

    public synchronized void estimateItems(int estimatedItemCount, int estimatedItemWidth) {
        this.estimatedItemCount = estimatedItemCount;
        this.estimatedItemWidth = estimatedItemWidth;
    }

    CompletionImpl getCompletionImpl() {
        return this.completionImpl;
    }

    Object getResultId() {
        return this.resultId;
    }

    private void checkNotFinished() {
        if (this.isFinished()) {
            throw new IllegalStateException("Result set already finished");
        }
    }

    public synchronized String getWaitText() {
        return this.waitText;
    }

    public synchronized void setWaitText(String waitText) {
        this.waitText = waitText;
    }

    static {
        try {
            Class.forName(CompletionResultSet.class.getName(), true, CompletionResultSetImpl.class.getClassLoader());
        }
        catch (ClassNotFoundException ex) {
            // empty catch block
        }
        spi = CompletionSpiPackageAccessor.get();
    }
}

