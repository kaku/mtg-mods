/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.completion;

import org.netbeans.modules.editor.completion.CompletionResultSetImpl;
import org.netbeans.spi.editor.completion.CompletionResultSet;

public abstract class CompletionSpiPackageAccessor {
    private static CompletionSpiPackageAccessor INSTANCE;

    public static CompletionSpiPackageAccessor get() {
        return INSTANCE;
    }

    public static void register(CompletionSpiPackageAccessor accessor) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already registered");
        }
        INSTANCE = accessor;
    }

    public abstract CompletionResultSet createCompletionResultSet(CompletionResultSetImpl var1);
}

