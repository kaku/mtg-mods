/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.EditorUI
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.completion;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Highlighter;
import javax.swing.text.LayeredHighlighter;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.openide.util.Exceptions;

public class HTMLDocView
extends JEditorPane {
    private HTMLEditorKit htmlKit;
    private int selectionAnchor = 0;
    private Object highlight = null;

    public HTMLDocView(Color bgColor) {
        this.setEditable(false);
        this.setFocusable(true);
        this.setBackground(bgColor);
        this.setMargin(new Insets(0, 3, 3, 3));
        this.addMouseListener(new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent e) {
                HTMLDocView.this.getHighlighter().removeAllHighlights();
            }

            @Override
            public void mousePressed(MouseEvent e) {
                HTMLDocView.this.getHighlighter().removeAllHighlights();
                HTMLDocView.this.selectionAnchor = HTMLDocView.this.positionCaret(e);
                try {
                    HTMLDocView.this.highlight = HTMLDocView.this.getHighlighter().addHighlight(HTMLDocView.this.selectionAnchor, HTMLDocView.this.selectionAnchor, DefaultHighlighter.DefaultPainter);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        this.addMouseMotionListener(new MouseMotionListener(){

            @Override
            public void mouseDragged(MouseEvent e) {
                try {
                    if (HTMLDocView.this.highlight == null) {
                        HTMLDocView.this.getHighlighter().removeAllHighlights();
                        HTMLDocView.this.selectionAnchor = HTMLDocView.this.positionCaret(e);
                        HTMLDocView.this.highlight = HTMLDocView.this.getHighlighter().addHighlight(HTMLDocView.this.selectionAnchor, HTMLDocView.this.selectionAnchor, DefaultHighlighter.DefaultPainter);
                    } else if (HTMLDocView.this.selectionAnchor <= HTMLDocView.this.positionCaret(e)) {
                        HTMLDocView.this.getHighlighter().changeHighlight(HTMLDocView.this.highlight, HTMLDocView.this.selectionAnchor, HTMLDocView.this.positionCaret(e));
                    } else {
                        HTMLDocView.this.getHighlighter().changeHighlight(HTMLDocView.this.highlight, HTMLDocView.this.positionCaret(e), HTMLDocView.this.selectionAnchor);
                    }
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        this.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
    }

    private int positionCaret(MouseEvent event) {
        int positionOffset = this.viewToModel(event.getPoint());
        return positionOffset;
    }

    @Override
    public boolean isFocusable() {
        return false;
    }

    public void setContent(final String content, final String reference) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                StringReader in = new StringReader("<HTML><BODY>" + content + "</BODY></HTML>");
                try {
                    Document doc = HTMLDocView.this.getDocument();
                    doc.remove(0, doc.getLength());
                    HTMLDocView.this.getEditorKit().read(in, HTMLDocView.this.getDocument(), 0);
                    HTMLDocView.this.setCaretPosition(0);
                    if (reference != null) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                HTMLDocView.this.scrollToReference(reference);
                            }
                        });
                    } else {
                        HTMLDocView.this.scrollRectToVisible(new Rectangle(0, 0, 0, 0));
                    }
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                catch (BadLocationException ble) {
                    ble.printStackTrace();
                }
            }

        });
    }

    @Override
    protected EditorKit createDefaultEditorKit() {
        if (this.htmlKit == null) {
            this.htmlKit = new HTMLEditorKit();
            this.setEditorKit(this.htmlKit);
            if (this.htmlKit.getStyleSheet().getStyleSheets() != null) {
                this.setBodyFontInCSS();
                return this.htmlKit;
            }
            this.setBodyFontInCSS();
        }
        return this.htmlKit;
    }

    private void setBodyFontInCSS() {
        StyleSheet css = new StyleSheet();
        Font f = new EditorUI().getDefaultColoring().getFont();
        this.setFont(f);
        css.addRule("body, div { font-size: " + f.getSize() + "; font-family: " + this.getFont().getFamily() + ";}");
        css.addStyleSheet(this.htmlKit.getStyleSheet());
        this.htmlKit.setStyleSheet(css);
    }

}

