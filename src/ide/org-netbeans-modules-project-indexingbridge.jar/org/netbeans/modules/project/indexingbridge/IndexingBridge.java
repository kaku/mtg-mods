/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.project.indexingbridge;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public abstract class IndexingBridge {
    private static final Logger LOG = Logger.getLogger(IndexingBridge.class.getName());

    protected IndexingBridge() {
    }

    public final Lock protectedMode() {
        return this.protectedMode(false);
    }

    public final Lock protectedMode(boolean waitForScan) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, null, new Throwable("IndexingBridge.protectedMode"));
        }
        if (waitForScan && this instanceof Ordering) {
            try {
                ((Ordering)this).await();
            }
            catch (InterruptedException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        this.enterProtectedMode();
        return new Lock();
    }

    protected abstract void enterProtectedMode();

    protected abstract void exitProtectedMode();

    public static IndexingBridge getDefault() {
        IndexingBridge b = (IndexingBridge)Lookup.getDefault().lookup(IndexingBridge.class);
        return b != null ? b : new IndexingBridge(){

            @Override
            protected void enterProtectedMode() {
            }

            @Override
            protected void exitProtectedMode() {
            }
        };
    }

    private static final class Stack
    extends Throwable {
        Stack(String msg) {
            super(msg);
        }

        Stack(String msg, Stack prior) {
            super(msg, prior);
        }

        @Override
        public synchronized Throwable fillInStackTrace() {
            boolean asserts = false;
            if (!$assertionsDisabled) {
                asserts = true;
                if (!true) {
                    throw new AssertionError();
                }
            }
            return asserts ? super.fillInStackTrace() : this /* !! */ ;
        }
    }

    public final class Lock {
        private final Stack creationStack;
        private Stack releaseStack;

        public Lock() {
            this.creationStack = new Stack("locked here");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void release() {
            IndexingBridge indexingBridge = IndexingBridge.this;
            synchronized (indexingBridge) {
                if (this.releaseStack != null) {
                    LOG.log(Level.WARNING, null, new IllegalStateException("Attempted to release lock twice", this.releaseStack));
                    return;
                }
                this.releaseStack = new Stack("released here", this.creationStack);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, null, new Throwable("IndexingBridge.Lock.release"));
                }
            }
            IndexingBridge.this.exitProtectedMode();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void finalize() throws Throwable {
            super.finalize();
            IndexingBridge indexingBridge = IndexingBridge.this;
            synchronized (indexingBridge) {
                if (this.releaseStack != null) {
                    return;
                }
                LOG.log(Level.WARNING, "Unreleased lock", this.creationStack);
                this.releaseStack = new Stack("released here", this.creationStack);
            }
            IndexingBridge.this.exitProtectedMode();
        }
    }

    public static abstract class Ordering
    extends IndexingBridge {
        protected abstract void await() throws InterruptedException;
    }

}

