/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.spi.editor.document;

import javax.swing.text.Document;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.annotations.common.NonNull;

public interface UndoableEditWrapper {
    @NonNull
    public UndoableEdit wrap(@NonNull UndoableEdit var1, @NonNull Document var2);
}

