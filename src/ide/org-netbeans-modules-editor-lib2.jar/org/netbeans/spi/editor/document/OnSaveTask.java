/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.util.Cancellable
 */
package org.netbeans.spi.editor.document;

import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.editor.lib2.document.DocumentSpiPackageAccessor;
import org.netbeans.modules.editor.lib2.document.ModRootElement;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.util.Cancellable;

public interface OnSaveTask
extends Cancellable {
    public void performTask();

    public void runLocked(@NonNull Runnable var1);

    public boolean cancel();

    public static final class PackageAccessor
    extends DocumentSpiPackageAccessor {
        @Override
        public Context createContext(Document doc) {
            return new Context(doc);
        }

        @Override
        public void setUndoEdit(Context context, UndoableEdit undoEdit) {
            context.setUndoEdit(undoEdit);
        }

        @Override
        public void setTaskStarted(Context context, boolean taskStarted) {
            context.setTaskStarted(taskStarted);
        }
    }

    public static final class Context {
        private final Document doc;
        private UndoableEdit undoEdit;
        private boolean taskStarted;

        Context(Document doc) {
            this.doc = doc;
        }

        public Document getDocument() {
            return this.doc;
        }

        public void addUndoEdit(UndoableEdit edit) {
            if (!this.taskStarted) {
                throw new IllegalStateException("This method may only be called during OnSaveTask.performTask()");
            }
            if (this.undoEdit != null) {
                this.undoEdit.addEdit(edit);
            }
        }

        public Element getModificationsRootElement() {
            return ModRootElement.get(this.doc);
        }

        void setUndoEdit(UndoableEdit undoEdit) {
            this.undoEdit = undoEdit;
        }

        void setTaskStarted(boolean taskStarted) {
            this.taskStarted = taskStarted;
        }

        static {
            DocumentSpiPackageAccessor.register(new PackageAccessor());
        }
    }

    @MimeLocation(subfolderName="OnSave")
    public static interface Factory {
        public OnSaveTask createTask(@NonNull Context var1);
    }

}

