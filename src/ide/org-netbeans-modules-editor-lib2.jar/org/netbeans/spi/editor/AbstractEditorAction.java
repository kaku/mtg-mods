/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.netbeans.spi.editor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.lib2.actions.MacroRecording;
import org.netbeans.modules.editor.lib2.actions.PresenterUpdater;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;
import org.openide.util.actions.Presenter;

public abstract class AbstractEditorAction
extends TextAction
implements Presenter.Menu,
Presenter.Popup,
Presenter.Toolbar {
    public static final String DISPLAY_NAME_KEY = "displayName";
    public static final String MENU_TEXT_KEY = "menuText";
    public static final String POPUP_TEXT_KEY = "popupText";
    public static final String ICON_RESOURCE_KEY = "iconBase";
    public static final String NO_ICON_IN_MENU = "noIconInMenu";
    public static final String NO_KEY_BINDING = "no-keybinding";
    public static final String MULTI_ACCELERATOR_LIST_KEY = "MultiAcceleratorListKey";
    public static final String ASYNCHRONOUS_KEY = "asynchronous";
    public static final String MIME_TYPE_KEY = "mimeType";
    public static final String PREFERENCES_NODE_KEY = "preferencesNode";
    public static final String PREFERENCES_KEY_KEY = "preferencesKey";
    public static final String PREFERENCES_DEFAULT_KEY = "preferencesDefault";
    public static final String WRAPPER_ACTION_KEY = "WrapperActionKey";
    private static final Logger UILOG = Logger.getLogger("org.netbeans.ui.actions.editor");
    private static final Logger LOG = Logger.getLogger(AbstractEditorAction.class.getName());
    private static final long serialVersionUID = 1;
    private static final Map<String, Boolean> LOGGED_ACTION_NAMES = Collections.synchronizedMap(new HashMap<K, V>());
    private Map<String, ?> attrs;
    private final Map<String, Object> properties = new HashMap<String, Object>();
    private Action delegateAction;
    private PreferencesNodeAndListener preferencesNodeAndListener;
    private static final Action UNITIALIZED_ACTION = EditorActionUtilities.createEmptyAction();
    private static final Object MASK_NULL_VALUE = new Object();

    protected AbstractEditorAction(Map<String, ?> attrs) {
        super(null);
        if (attrs != null) {
            this.setAttrs(attrs);
            this.delegateAction = Boolean.TRUE.equals(attrs.get("WrapperActionKey")) ? UNITIALIZED_ACTION : null;
            this.checkPreferencesKey();
        }
    }

    protected AbstractEditorAction() {
        this(null);
    }

    protected abstract void actionPerformed(ActionEvent var1, JTextComponent var2);

    protected void valuesUpdated() {
    }

    protected boolean asynchronous() {
        return Boolean.TRUE.equals(this.getValue("asynchronous"));
    }

    protected final void resetCaretMagicPosition(JTextComponent component) {
        EditorActionUtilities.resetCaretMagicPosition(component);
    }

    public JMenuItem getMenuPresenter() {
        return PresenterUpdater.createMenuPresenter(this);
    }

    public JMenuItem getPopupPresenter() {
        return PresenterUpdater.createPopupPresenter(this);
    }

    public Component getToolbarPresenter() {
        return PresenterUpdater.createToolbarPresenter(this);
    }

    protected final String actionName() {
        return (String)this.getValue("Name");
    }

    @Override
    public final void actionPerformed(final ActionEvent evt) {
        Action dAction = this.getDelegateAction();
        if (dAction != null) {
            if (!(dAction instanceof AbstractEditorAction)) {
                this.checkTogglePreferencesValue();
            }
            dAction.actionPerformed(evt);
            return;
        }
        final JTextComponent component = this.getTextComponent(evt);
        MacroRecording.get().recordAction(this, evt, component);
        if (UILOG.isLoggable(Level.FINE)) {
            String actionName = this.actionName();
            Boolean logged = LOGGED_ACTION_NAMES.get(actionName);
            if (logged == null) {
                logged = AbstractEditorAction.isLogged(actionName);
                LOGGED_ACTION_NAMES.put(actionName, logged);
            }
            if (logged.booleanValue()) {
                LogRecord r = new LogRecord(Level.FINE, "UI_ACTION_EDITOR");
                r.setResourceBundle(NbBundle.getBundle(AbstractEditorAction.class));
                if (evt != null) {
                    r.setParameters(new Object[]{evt, evt.toString(), this, this.toString(), this.getValue("Name")});
                } else {
                    r.setParameters(new Object[]{"no-ActionEvent", "no-ActionEvent", this, this.toString(), this.getValue("Name")});
                }
                r.setLoggerName(UILOG.getName());
                UILOG.log(r);
            }
        }
        this.checkTogglePreferencesValue();
        if (this.asynchronous()) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    AbstractEditorAction.this.actionPerformed(evt, component);
                }
            });
        } else {
            this.actionPerformed(evt, component);
        }
    }

    private static boolean isLogged(String actionName) {
        return actionName != null && !"default-typed".equals(actionName) && -1 == actionName.indexOf("caret") && -1 == actionName.indexOf("delete") && -1 == actionName.indexOf("selection") && -1 == actionName.indexOf("build-tool-tip") && -1 == actionName.indexOf("build-popup-menu") && -1 == actionName.indexOf("page-up") && -1 == actionName.indexOf("page-down") && -1 == actionName.indexOf("-kit-install");
    }

    @Override
    public final Object getValue(String key) {
        Action dAction = this.delegateAction;
        if (dAction != null && dAction != UNITIALIZED_ACTION) {
            Object value = dAction.getValue(key);
            if (value == null && (value = this.getValueLocal(key)) != null) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Transfer wrapper action property: key=" + key + ", value=" + value + '\n');
                }
                dAction.putValue(key, value);
            }
            return value;
        }
        return this.getValueLocal(key);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Object getValueLocal(String key) {
        if ("enabled" == key) {
            return this.enabled;
        }
        Map<String, Object> map = this.properties;
        synchronized (map) {
            Object value = this.properties.get(key);
            if (value == null) {
                if ("instanceCreate".equals(key)) {
                    return null;
                }
                if (value == null) {
                    value = this.createValue(key);
                    if (value == null) {
                        value = MASK_NULL_VALUE;
                    }
                    this.properties.put(key, value);
                }
            }
            if (value == MASK_NULL_VALUE) {
                value = null;
            }
            return value;
        }
    }

    protected Object createValue(String key) {
        Icon value = "SmallIcon".equals(key) ? EditorActionUtilities.createSmallIcon(this) : ("SwingLargeIconKey".equals(key) ? EditorActionUtilities.createLargeIcon(this) : (this.attrs != null ? this.attrs.get(key) : null));
        return value;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final void putValue(String key, Object value) {
        Object oldValue;
        Action dAction = this.delegateAction;
        if (dAction != null && dAction != UNITIALIZED_ACTION) {
            dAction.putValue(key, value);
            return;
        }
        if (value == null && this.properties == null) {
            return;
        }
        if ("enabled" == key) {
            oldValue = this.enabled;
            this.enabled = Boolean.TRUE.equals(value);
        } else {
            Map<String, Object> map = this.properties;
            synchronized (map) {
                oldValue = this.properties.put(key, value != null ? value : MASK_NULL_VALUE);
            }
        }
        this.firePropertyChange(key, oldValue, value);
    }

    @Override
    public boolean isEnabled() {
        Action dAction = this.delegateAction;
        if (dAction != null && dAction != UNITIALIZED_ACTION) {
            return dAction.isEnabled();
        }
        return super.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        Action dAction = this.delegateAction;
        if (dAction != null && dAction != UNITIALIZED_ACTION) {
            dAction.setEnabled(enabled);
        } else {
            super.setEnabled(enabled);
        }
    }

    @Override
    public Object[] getKeys() {
        Set<String> keys = this.properties.keySet();
        Object[] keysArray = new Object[keys.size()];
        keys.toArray(keysArray);
        return keysArray;
    }

    private void setAttrs(Map<String, ?> attrs) {
        this.attrs = attrs;
    }

    private Action getDelegateAction() {
        Action dAction = this.delegateAction;
        if (dAction == UNITIALIZED_ACTION) {
            dAction = (Action)this.attrs.get("delegate");
            if (dAction == null) {
                throw new IllegalStateException("delegate is null for wrapper action");
            }
            if (dAction instanceof AbstractEditorAction) {
                AbstractEditorAction aeAction = (AbstractEditorAction)dAction;
                aeAction.setAttrs(this.attrs);
                this.transferProperties(dAction);
                aeAction.checkPreferencesKey();
                aeAction.valuesUpdated();
            } else {
                this.transferProperties(dAction);
            }
            boolean dActionEnabled = dAction.isEnabled();
            if (this.isEnabled() != dActionEnabled) {
                super.setEnabled(dActionEnabled);
            }
            dAction.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)new DelegateActionPropertyChangeListener(this), (Object)dAction));
            this.delegateAction = dAction;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Delegate action created: " + dAction + '\n');
            }
        }
        return dAction;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void transferProperties(Action dAction) {
        boolean log = LOG.isLoggable(Level.FINE);
        if (log) {
            LOG.fine("Transfer properties into " + dAction + '\n');
        }
        Map<String, Object> map = this.properties;
        synchronized (map) {
            for (Map.Entry<String, Object> entry : this.properties.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value == MASK_NULL_VALUE) continue;
                if (log) {
                    LOG.fine("    key=" + key + ", value=" + value + '\n');
                }
                dAction.putValue(key, value);
            }
        }
    }

    private void checkPreferencesKey() {
        String preferencesKey = (String)this.attrs.get("preferencesKey");
        if (preferencesKey != null) {
            this.preferencesNodeAndListener = new PreferencesNodeAndListener(preferencesKey);
        }
    }

    private void checkTogglePreferencesValue() {
        if (this.preferencesNodeAndListener != null) {
            this.preferencesNodeAndListener.togglePreferencesValue();
        }
    }

    public String toString() {
        String clsName = this.getClass().getSimpleName();
        return clsName + '@' + System.identityHashCode(this) + " mime=\"" + this.getValue("mimeType") + "\" name=\"" + this.actionName() + "\"";
    }

    private final class PreferencesNodeAndListener
    implements PreferenceChangeListener,
    PropertyChangeListener {
        final Preferences node;
        final String key;
        boolean expectedPropertyChange;

        public PreferencesNodeAndListener(String key) {
            this.key = key;
            this.node = (Preferences)AbstractEditorAction.this.getValue("preferencesNode");
            if (this.node == null) {
                throw new IllegalStateException("PREFERENCES_KEY_KEY property set but PREFERENCES_NODE_KEY not for action=" + AbstractEditorAction.this);
            }
            this.node.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.node));
            AbstractEditorAction.this.addPropertyChangeListener(this);
            AbstractEditorAction.this.putValue("SwingSelectedKey", this.preferencesValue());
        }

        private boolean preferencesValue() {
            boolean value = Boolean.TRUE.equals(AbstractEditorAction.this.getValue("preferencesDefault"));
            value = this.node.getBoolean(this.key, value);
            return value;
        }

        private void togglePreferencesValue() {
            boolean value = this.preferencesValue();
            this.setPreferencesValue(!value);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void setPreferencesValue(boolean value) {
            this.node.putBoolean(this.key, value);
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            boolean selected = this.preferencesValue();
            AbstractEditorAction.this.putValue("SwingSelectedKey", selected);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (!this.expectedPropertyChange && "SwingSelectedKey".equals(evt.getPropertyName())) {
                boolean selected = (Boolean)evt.getNewValue();
                this.setPreferencesValue(selected);
            }
        }
    }

    private static final class DelegateActionPropertyChangeListener
    implements PropertyChangeListener {
        private final AbstractEditorAction wrapper;

        DelegateActionPropertyChangeListener(AbstractEditorAction wrapper) {
            this.wrapper = wrapper;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            this.wrapper.firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
        }
    }

}

