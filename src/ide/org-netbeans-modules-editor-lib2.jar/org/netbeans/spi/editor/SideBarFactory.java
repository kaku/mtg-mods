/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor;

import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="SideBar")
public interface SideBarFactory {
    public JComponent createSideBar(JTextComponent var1);
}

