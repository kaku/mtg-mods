/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.highlighting;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public interface HighlightAttributeValue<T> {
    public T getValue(JTextComponent var1, Document var2, Object var3, int var4, int var5);
}

