/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.highlighting;

import java.util.NoSuchElementException;
import javax.swing.text.AttributeSet;

public interface HighlightsSequence {
    public static final HighlightsSequence EMPTY = new HighlightsSequence(){

        @Override
        public boolean moveNext() {
            return false;
        }

        @Override
        public int getStartOffset() {
            throw new NoSuchElementException();
        }

        @Override
        public int getEndOffset() {
            throw new NoSuchElementException();
        }

        @Override
        public AttributeSet getAttributes() {
            throw new NoSuchElementException();
        }
    };

    public boolean moveNext();

    public int getStartOffset();

    public int getEndOffset();

    public AttributeSet getAttributes();

}

