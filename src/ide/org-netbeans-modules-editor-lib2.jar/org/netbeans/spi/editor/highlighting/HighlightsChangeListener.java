/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.highlighting;

import java.util.EventListener;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;

public interface HighlightsChangeListener
extends EventListener {
    public void highlightChanged(HighlightsChangeEvent var1);
}

