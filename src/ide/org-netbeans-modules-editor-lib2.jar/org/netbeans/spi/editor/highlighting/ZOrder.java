/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.TopologicalSortException
 */
package org.netbeans.spi.editor.highlighting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.openide.util.TopologicalSortException;

public final class ZOrder {
    private static final Logger LOG = Logger.getLogger(ZOrder.class.getName());
    public static final ZOrder TOP_RACK = new ZOrder(50, 0);
    public static final ZOrder SHOW_OFF_RACK = new ZOrder(40, 0);
    public static final ZOrder DEFAULT_RACK = new ZOrder(30, 0);
    public static final ZOrder CARET_RACK = new ZOrder(20, 0);
    public static final ZOrder SYNTAX_RACK = new ZOrder(10, 0);
    public static final ZOrder BOTTOM_RACK = new ZOrder(0, 0);
    private static final Comparator<HighlightsLayer> COMPARATOR = new Comparator<HighlightsLayer>(){

        @Override
        public int compare(HighlightsLayer layerA, HighlightsLayer layerB) {
            ZOrder zOrderA = layerA.getZOrder();
            ZOrder zOrderB = layerB.getZOrder();
            if (zOrderA.rack == zOrderB.rack) {
                return zOrderA.position - zOrderB.position;
            }
            return zOrderA.rack - zOrderB.rack;
        }
    };
    private final int rack;
    private final int position;

    static HighlightsLayer[] sort(HighlightsLayer[] layers) throws TopologicalSortException {
        List<? extends HighlightsLayer> list = ZOrder.sort(Arrays.asList(layers));
        return list.toArray(new HighlightsLayer[list.size()]);
    }

    int getRack() {
        return this.rack;
    }

    static List<? extends HighlightsLayer> sort(Collection<? extends HighlightsLayer> layers) throws TopologicalSortException {
        ArrayList<? extends HighlightsLayer> sortedLayers = new ArrayList<HighlightsLayer>(layers);
        Collections.sort(sortedLayers, COMPARATOR);
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("Sorted layer Ids: ");
            for (HighlightsLayer layer : sortedLayers) {
                LOG.finest("    " + layer.getLayerTypeId());
            }
            LOG.finest("End of Sorted layer Ids: -----------------------");
        }
        return sortedLayers;
    }

    private ZOrder(int rack, int position) {
        this.rack = rack;
        this.position = position;
    }

    public ZOrder forPosition(int position) {
        return new ZOrder(this.rack, position);
    }

    public String toString() {
        String s = "Unknown_rack";
        switch (this.rack) {
            case 0: {
                s = "BOTTOM_RACK";
                break;
            }
            case 10: {
                s = "SYNTAX_RACK";
                break;
            }
            case 20: {
                s = "CARET_RACK";
                break;
            }
            case 30: {
                s = "DEFAULT_RACK";
                break;
            }
            case 40: {
                s = "SHOW_OFF_RACK";
                break;
            }
            case 50: {
                s = "TOP_RACK";
            }
        }
        return s + "(" + this.position + ")";
    }

}

