/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.highlighting;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;

public interface HighlightsLayerFactory {
    public HighlightsLayer[] createLayers(Context var1);

    public static final class Context {
        private Document document;
        private JTextComponent component;

        Context(Document document, JTextComponent component) {
            this.document = document;
            this.component = component;
        }

        public Document getDocument() {
            return this.document;
        }

        public JTextComponent getComponent() {
            return this.component;
        }
    }

}

