/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ListenerList
 */
package org.netbeans.spi.editor.highlighting.support;

import java.util.EventListener;
import java.util.List;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public abstract class AbstractHighlightsContainer
implements HighlightsContainer {
    private ListenerList<HighlightsChangeListener> listeners = new ListenerList();

    protected AbstractHighlightsContainer() {
    }

    @Override
    public abstract HighlightsSequence getHighlights(int var1, int var2);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final void addHighlightsChangeListener(HighlightsChangeListener listener) {
        ListenerList<HighlightsChangeListener> listenerList = this.listeners;
        synchronized (listenerList) {
            this.listeners.add((EventListener)listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final void removeHighlightsChangeListener(HighlightsChangeListener listener) {
        ListenerList<HighlightsChangeListener> listenerList = this.listeners;
        synchronized (listenerList) {
            this.listeners.remove((EventListener)listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void fireHighlightsChange(int changeStartOffset, int changeEndOffset) {
        List targets;
        ListenerList<HighlightsChangeListener> listenerList = this.listeners;
        synchronized (listenerList) {
            targets = this.listeners.getListeners();
        }
        if (targets.size() > 0) {
            HighlightsChangeEvent evt = new HighlightsChangeEvent(this, changeStartOffset, changeEndOffset);
            for (HighlightsChangeListener l : targets) {
                l.highlightChanged(evt);
            }
        }
    }
}

