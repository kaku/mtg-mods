/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.highlighting;

import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public interface HighlightsContainer {
    public static final String ATTR_EXTENDS_EOL = "org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL";
    public static final String ATTR_EXTENDS_EMPTY_LINE = "org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EMPTY_LINE";

    public HighlightsSequence getHighlights(int var1, int var2);

    public void addHighlightsChangeListener(HighlightsChangeListener var1);

    public void removeHighlightsChangeListener(HighlightsChangeListener var1);
}

