/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.highlighting;

import java.util.EventObject;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;

public final class HighlightsChangeEvent
extends EventObject {
    private int startOffset;
    private int endOffset;

    public HighlightsChangeEvent(HighlightsContainer source, int startOffset, int endOffset) {
        super(source);
        this.startOffset = startOffset;
        this.endOffset = endOffset;
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public int getEndOffset() {
        return this.endOffset;
    }
}

