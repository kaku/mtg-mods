/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.TopologicalSortException
 */
package org.netbeans.spi.editor.highlighting;

import java.util.Collection;
import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingSpiPackageAccessor;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsLayerAccessor;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.openide.util.TopologicalSortException;

public final class HighlightsLayer {
    private final String layerTypeId;
    private final ZOrder zOrder;
    private final boolean fixedSize;
    private HighlightsContainer container;
    private HighlightsLayerAccessor accessor;

    public static HighlightsLayer create(String layerTypeId, ZOrder zOrder, boolean fixedSize, HighlightsContainer container) {
        return new HighlightsLayer(layerTypeId, zOrder, fixedSize, container);
    }

    private HighlightsLayer(String layerTypeId, ZOrder zOrder, boolean fixedSize, HighlightsContainer container) {
        assert (layerTypeId != null);
        assert (zOrder != null);
        this.layerTypeId = layerTypeId;
        this.zOrder = zOrder;
        this.fixedSize = fixedSize;
        this.container = container;
    }

    String getLayerTypeId() {
        return this.layerTypeId;
    }

    ZOrder getZOrder() {
        return this.zOrder;
    }

    boolean isFixedSize() {
        return this.fixedSize;
    }

    HighlightsContainer getContainer() {
        return this.container;
    }

    public String toString() {
        return "HighlightsLayer@" + System.identityHashCode(this) + (this.isFixedSize() ? "(F)" : "") + ":typeId=" + this.layerTypeId;
    }

    static {
        HighlightingSpiPackageAccessor.register(new PackageAccessor());
    }

    private static final class PackageAccessor
    extends HighlightingSpiPackageAccessor {
        @Override
        public HighlightsLayerFactory.Context createFactoryContext(Document document, JTextComponent component) {
            return new HighlightsLayerFactory.Context(document, component);
        }

        @Override
        public List<? extends HighlightsLayer> sort(Collection<? extends HighlightsLayer> layers) throws TopologicalSortException {
            return ZOrder.sort(layers);
        }

        @Override
        public HighlightsLayerAccessor getHighlightsLayerAccessor(final HighlightsLayer layer) {
            if (layer.accessor == null) {
                layer.accessor = new HighlightsLayerAccessor(){

                    @Override
                    public String getLayerTypeId() {
                        return layer.getLayerTypeId();
                    }

                    @Override
                    public boolean isFixedSize() {
                        return layer.isFixedSize();
                    }

                    @Override
                    public ZOrder getZOrder() {
                        return layer.getZOrder();
                    }

                    @Override
                    public HighlightsContainer getContainer() {
                        return layer.getContainer();
                    }
                };
            }
            return layer.accessor;
        }

        @Override
        public int getZOrderRack(ZOrder zOrder) {
            return zOrder.getRack();
        }

    }

}

