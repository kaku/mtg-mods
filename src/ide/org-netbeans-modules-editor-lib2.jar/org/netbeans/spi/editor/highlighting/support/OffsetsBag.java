/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.openide.util.Utilities
 */
package org.netbeans.spi.editor.highlighting.support;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.modules.editor.lib2.highlighting.OffsetGapList;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.util.Utilities;

public final class OffsetsBag
extends AbstractHighlightsContainer {
    private static final Logger LOG = Logger.getLogger(OffsetsBag.class.getName());
    private Document document;
    private final OffsetGapList<Mark> marks;
    private final boolean mergeHighlights;
    private long version = 0;
    private DocL docListener;
    private int lastAddIndex;
    private int lastMoveNextIndex;
    private StackTraceElement[] discardCaller = null;
    private String discardThreadId = null;

    public OffsetsBag(Document document) {
        this(document, false);
    }

    public OffsetsBag(Document document, boolean mergeHighlights) {
        this.document = document;
        this.mergeHighlights = mergeHighlights;
        this.marks = new OffsetGapList(true);
        this.docListener = new DocL(this);
        this.document.addDocumentListener(this.docListener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void discard() {
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            if (this.document != null) {
                this.document.removeDocumentListener(this.docListener);
                this.marks.clear();
                ++this.version;
                this.docListener = null;
                this.document = null;
                boolean ae = false;
                if (!$assertionsDisabled) {
                    ae = true;
                    if (!true) {
                        throw new AssertionError();
                    }
                }
                if (ae) {
                    Thread t = Thread.currentThread();
                    this.discardCaller = t.getStackTrace();
                    this.discardThreadId = t.getName() + ":" + t.getId();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addHighlight(int startOffset, int endOffset, AttributeSet attributes) {
        int[] offsets;
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            offsets = this.addHighlightImpl(startOffset, endOffset, attributes);
            if (offsets != null) {
                ++this.version;
            }
        }
        if (offsets != null) {
            this.fireHighlightsChange(offsets[0], offsets[1]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addAllHighlights(HighlightsSequence bag) {
        int[] offsets;
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            offsets = this.addAllHighlightsImpl(bag);
            if (offsets != null) {
                ++this.version;
            }
        }
        if (offsets != null) {
            this.fireHighlightsChange(offsets[0], offsets[1]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setHighlights(HighlightsSequence seq) {
        if (seq instanceof Seq) {
            this.setHighlights(((Seq)seq).getBag());
            return;
        }
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            int[] clearedArea = this.clearImpl();
            int[] populatedArea = this.addAllHighlightsImpl(seq);
            if (clearedArea != null) {
                changeStart = clearedArea[0];
                changeEnd = clearedArea[1];
            }
            if (populatedArea != null) {
                if (changeStart == Integer.MAX_VALUE || changeStart > populatedArea[0]) {
                    changeStart = populatedArea[0];
                }
                if (changeEnd == Integer.MIN_VALUE || changeEnd < populatedArea[1]) {
                    changeEnd = populatedArea[1];
                }
            }
            if (changeStart < changeEnd) {
                ++this.version;
            }
        }
        if (changeStart < changeEnd) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setHighlights(OffsetsBag bag) {
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            OffsetGapList<Mark> newMarks;
            assert (this.document != null);
            int[] clearedArea = this.clearImpl();
            int[] populatedArea = null;
            OffsetGapList<Mark> offsetGapList2 = newMarks = bag.getMarks();
            synchronized (offsetGapList2) {
                Iterator i$ = newMarks.iterator();
                while (i$.hasNext()) {
                    Mark mark = (Mark)i$.next();
                    this.marks.add(this.marks.size(), new Mark(mark.getOffset(), mark.getAttributes()));
                }
                if (this.marks.size() > 0) {
                    populatedArea = new int[]{((Mark)this.marks.get(0)).getOffset(), ((Mark)this.marks.get(this.marks.size() - 1)).getOffset()};
                }
            }
            if (clearedArea != null) {
                changeStart = clearedArea[0];
                changeEnd = clearedArea[1];
            }
            if (populatedArea != null) {
                if (changeStart == Integer.MAX_VALUE || changeStart > populatedArea[0]) {
                    changeStart = populatedArea[0];
                }
                if (changeEnd == Integer.MIN_VALUE || changeEnd < populatedArea[1]) {
                    changeEnd = populatedArea[1];
                }
            }
            if (changeStart < changeEnd) {
                ++this.version;
            }
        }
        if (changeStart < changeEnd) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeHighlights(int startOffset, int endOffset, boolean clip) {
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        if (startOffset == endOffset && clip) {
            return;
        }
        assert (startOffset <= endOffset);
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            int endIdx;
            assert (this.document != null);
            if (this.marks.isEmpty()) {
                return;
            }
            int startIdx = this.indexBeforeOffset(startOffset);
            int n = startOffset == endOffset ? startIdx : (endIdx = this.indexBeforeOffset(endOffset, startIdx < 0 ? 0 : startIdx, this.marks.size() - 1));
            if (clip) {
                if (startIdx == endIdx) {
                    if (startIdx != -1 && ((Mark)this.marks.get(startIdx)).getAttributes() != null) {
                        AttributeSet original = ((Mark)this.marks.get(startIdx)).getAttributes();
                        if (((Mark)this.marks.get(startIdx)).getOffset() == startOffset) {
                            this.marks.set(startIdx, new Mark(endOffset, original));
                        } else {
                            this.marks.add(startIdx + 1, new Mark(startOffset, null));
                            this.marks.add(startIdx + 2, new Mark(endOffset, original));
                        }
                        changeStart = startOffset;
                        changeEnd = endOffset;
                    }
                    startIdx = Integer.MAX_VALUE;
                    endIdx = Integer.MIN_VALUE;
                } else {
                    assert (endIdx != -1);
                    if (((Mark)this.marks.get(endIdx)).getAttributes() != null) {
                        this.marks.set(endIdx, new Mark(endOffset, ((Mark)this.marks.get(endIdx)).getAttributes()));
                        changeEnd = endOffset;
                        --endIdx;
                    }
                    if (startIdx != -1 && ((Mark)this.marks.get(startIdx)).getAttributes() != null) {
                        if (startIdx + 1 < endIdx) {
                            this.marks.set(++startIdx, new Mark(startOffset, null));
                        } else if (((Mark)this.marks.get(startIdx)).getOffset() < startOffset) {
                            if (startIdx + 1 == endIdx) {
                                this.marks.set(++startIdx, new Mark(startOffset, null));
                            } else {
                                this.marks.add(++startIdx, new Mark(startOffset, null));
                            }
                        } else if (startIdx == 0 || ((Mark)this.marks.get(startIdx - 1)).getAttributes() == null) {
                            --startIdx;
                        } else {
                            this.marks.set(startIdx, new Mark(startOffset, null));
                        }
                        changeStart = startOffset;
                    }
                    ++startIdx;
                }
            } else {
                if (startIdx == -1 || ((Mark)this.marks.get(startIdx)).getAttributes() == null) {
                    ++startIdx;
                } else if (startIdx > 0 && ((Mark)this.marks.get(startIdx - 1)).getAttributes() != null) {
                    ((Mark)this.marks.get(startIdx)).setAttributes(null);
                    ++startIdx;
                }
                if (endIdx != -1 && ((Mark)this.marks.get(endIdx)).getAttributes() != null) {
                    if (((Mark)this.marks.get(endIdx)).getOffset() < endOffset) {
                        if (endIdx + 1 >= this.marks.size() || ((Mark)this.marks.get(endIdx + 1)).getAttributes() == null) {
                            ++endIdx;
                        }
                    } else {
                        --endIdx;
                    }
                }
            }
            if (startIdx <= endIdx) {
                if (changeStart == Integer.MAX_VALUE) {
                    changeStart = ((Mark)this.marks.get(startIdx)).getOffset();
                }
                if (changeEnd == Integer.MIN_VALUE) {
                    if (endIdx >= this.marks.size()) {
                        if (LOG.isLoggable(Level.INFO)) {
                            String logMsg = "Too high endIdx=" + endIdx + ", marks.size()=" + this.marks.size() + ", startIdx=" + startIdx + ", startOffset=" + startOffset + ", endOffset=" + endOffset + ", changeStart=" + changeStart + ", document.getLength()=" + this.document.getLength() + ", lastMark=" + this.marks.get(this.marks.size() - 1);
                            LOG.log(Level.INFO, logMsg, new Exception());
                        }
                        endIdx = this.marks.size() - 1;
                    }
                    changeEnd = ((Mark)this.marks.get(endIdx)).getOffset();
                }
                this.marks.remove(startIdx, endIdx - startIdx + 1);
            }
            if (changeStart <= changeEnd) {
                ++this.version;
            }
        }
        if (changeStart <= changeEnd) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        if (LOG.isLoggable(Level.FINE) && startOffset >= endOffset) {
            LOG.fine("startOffset must be less than endOffset: startOffset = " + startOffset + " endOffset = " + endOffset);
        }
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            if (this.document != null) {
                return new Seq(this.version, startOffset, endOffset);
            }
            return HighlightsSequence.EMPTY;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clear() {
        int[] clearedArea;
        OffsetGapList<Mark> offsetGapList = this.marks;
        synchronized (offsetGapList) {
            assert (this.document != null);
            clearedArea = this.clearImpl();
            if (clearedArea != null) {
                ++this.version;
            }
        }
        if (clearedArea != null) {
            this.fireHighlightsChange(clearedArea[0], clearedArea[1]);
        }
    }

    OffsetGapList<Mark> getMarks() {
        return this.marks;
    }

    Document getDocument() {
        return this.document;
    }

    private int[] addHighlightImpl(int startOffset, int endOffset, AttributeSet attributes) {
        if (startOffset == endOffset) {
            return null;
        }
        assert (this.document != null);
        assert (startOffset < endOffset);
        assert (attributes != null);
        if (this.document == null || startOffset >= endOffset || attributes == null) {
            return null;
        }
        if (this.mergeHighlights) {
            this.merge(startOffset, endOffset, attributes);
        } else {
            this.trim(startOffset, endOffset, attributes);
        }
        return new int[]{startOffset, endOffset};
    }

    private int findAddIndex(int offset) {
        int idx;
        int lastIdx = this.lastAddIndex;
        boolean hit = false;
        int marksSize = this.marks.size();
        if (lastIdx < marksSize) {
            if (lastIdx == -1) {
                hit = marksSize == 0 || ((Mark)this.marks.get(0)).getOffset() > offset;
            } else if (lastIdx >= 0) {
                int markOffset = ((Mark)this.marks.get(lastIdx)).getOffset();
                if (offset == markOffset) {
                    hit = lastIdx == 0 || ((Mark)this.marks.get(lastIdx - 1)).getOffset() < offset;
                } else if (offset > markOffset) {
                    if (lastIdx >= marksSize - 1 || offset < ((Mark)this.marks.get(lastIdx + 1)).getOffset()) {
                        hit = true;
                    } else if (lastIdx >= marksSize - 2 || offset < ((Mark)this.marks.get(lastIdx + 2)).getOffset()) {
                        ++lastIdx;
                        hit = true;
                    }
                }
            }
        }
        this.lastAddIndex = idx = hit ? lastIdx : this.indexBeforeOffset(offset);
        return idx;
    }

    private int findMoveNextIndex(int offset) {
        int idx;
        int lastIdx = this.lastMoveNextIndex;
        boolean hit = false;
        int marksSize = this.marks.size();
        if (lastIdx < marksSize) {
            if (lastIdx == -1) {
                hit = marksSize == 0 || ((Mark)this.marks.get(0)).getOffset() > offset;
            } else {
                int markOffset = ((Mark)this.marks.get(lastIdx)).getOffset();
                if (offset == markOffset) {
                    hit = lastIdx == 0 || ((Mark)this.marks.get(lastIdx - 1)).getOffset() < offset;
                } else if (offset > markOffset) {
                    if (lastIdx >= marksSize - 1 || offset < ((Mark)this.marks.get(lastIdx + 1)).getOffset()) {
                        hit = true;
                    } else if (lastIdx >= marksSize - 2 || offset < ((Mark)this.marks.get(lastIdx + 2)).getOffset()) {
                        ++lastIdx;
                        hit = true;
                    }
                }
            }
        }
        this.lastMoveNextIndex = idx = hit ? lastIdx : this.indexBeforeOffset(offset);
        return idx;
    }

    private void merge(int startOffset, int endOffset, AttributeSet attributes) {
        AttributeSet lastKnownAttributes = null;
        int startIdx = this.findAddIndex(startOffset);
        if (startIdx < 0) {
            startIdx = 0;
            this.marks.add(startIdx, new Mark(startOffset, attributes));
            ++this.lastAddIndex;
        } else {
            Mark mark = (Mark)this.marks.get(startIdx);
            AttributeSet markAttribs = mark.getAttributes();
            AttributeSet newAttribs = markAttribs == null ? attributes : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{attributes, markAttribs});
            lastKnownAttributes = mark.getAttributes();
            if (mark.getOffset() == startOffset) {
                mark.setAttributes(newAttribs);
            } else {
                this.marks.add(++startIdx, new Mark(startOffset, newAttribs));
                ++this.lastAddIndex;
            }
        }
        int idx = startIdx + 1;
        do {
            Mark mark;
            if (idx < this.marks.size()) {
                mark = (Mark)this.marks.get(idx);
                if (mark.getOffset() >= endOffset) {
                    if (mark.getOffset() <= endOffset) break;
                    this.marks.add(idx, new Mark(endOffset, lastKnownAttributes));
                    ++this.lastAddIndex;
                    break;
                }
            } else {
                this.marks.add(idx, new Mark(endOffset, lastKnownAttributes));
                ++this.lastAddIndex;
                break;
            }
            lastKnownAttributes = mark.getAttributes();
            mark.setAttributes(lastKnownAttributes == null ? attributes : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{attributes, lastKnownAttributes}));
            ++idx;
        } while (true);
    }

    private void trim(int startOffset, int endOffset, AttributeSet attributes) {
        int startIdx;
        int endIdx = this.indexBeforeOffset(endOffset, (startIdx = this.findAddIndex(startOffset)) < 0 ? 0 : startIdx, this.marks.size() - 1);
        if (startIdx == endIdx) {
            AttributeSet original = null;
            if (startIdx != -1 && ((Mark)this.marks.get(startIdx)).getAttributes() != null) {
                original = ((Mark)this.marks.get(startIdx)).getAttributes();
            }
            if (startIdx != -1 && ((Mark)this.marks.get(startIdx)).getOffset() == startOffset) {
                ((Mark)this.marks.get(startIdx)).setAttributes(attributes);
            } else {
                this.marks.add(++startIdx, new Mark(startOffset, attributes));
                ++this.lastAddIndex;
            }
            this.marks.add(++startIdx, new Mark(endOffset, original));
            ++this.lastAddIndex;
        } else {
            assert (endIdx != -1);
            this.marks.set(endIdx, new Mark(endOffset, ((Mark)this.marks.get(endIdx)).getAttributes()));
            --endIdx;
            if (startIdx != -1 && ((Mark)this.marks.get(startIdx)).getOffset() == startOffset) {
                ((Mark)this.marks.get(startIdx)).setAttributes(attributes);
            } else if (startIdx + 1 <= endIdx) {
                this.marks.set(++startIdx, new Mark(startOffset, attributes));
            } else {
                this.marks.add(++startIdx, new Mark(startOffset, attributes));
                ++this.lastAddIndex;
            }
            if (++startIdx <= endIdx) {
                this.marks.remove(startIdx, endIdx - startIdx + 1);
                --this.lastAddIndex;
            }
        }
    }

    private int[] addAllHighlightsImpl(HighlightsSequence sequence) {
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        while (sequence.moveNext()) {
            this.addHighlightImpl(sequence.getStartOffset(), sequence.getEndOffset(), sequence.getAttributes());
            if (changeStart == Integer.MAX_VALUE) {
                changeStart = sequence.getStartOffset();
            }
            changeEnd = sequence.getEndOffset();
        }
        if (changeStart != Integer.MAX_VALUE && changeEnd != Integer.MIN_VALUE) {
            return new int[]{changeStart, changeEnd};
        }
        return null;
    }

    private int[] clearImpl() {
        if (!this.marks.isEmpty()) {
            int changeStart = ((Mark)this.marks.get(0)).getOffset();
            int changeEnd = ((Mark)this.marks.get(this.marks.size() - 1)).getOffset();
            this.marks.clear();
            return new int[]{changeStart, changeEnd};
        }
        return null;
    }

    private int indexBeforeOffset(int offset, int low, int high) {
        int idx = this.marks.findElementIndex(offset, low, high);
        if (idx < 0) {
            idx = - idx - 2;
        }
        return idx;
    }

    private int indexBeforeOffset(int offset) {
        return this.indexBeforeOffset(offset, 0, this.marks.size() - 1);
    }

    private static String printStackTrace(StackTraceElement[] stackTrace) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement e : stackTrace) {
            sb.append(e);
            sb.append('\n');
        }
        return sb.toString();
    }

    private static final class DocL
    extends WeakReference<OffsetsBag>
    implements DocumentListener,
    Runnable {
        private Document document;

        public DocL(OffsetsBag bag) {
            super(bag, Utilities.activeReferenceQueue());
            this.document = bag.getDocument();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void insertUpdate(DocumentEvent e) {
            OffsetsBag bag = (OffsetsBag)this.get();
            if (bag != null) {
                OffsetGapList offsetGapList = bag.marks;
                synchronized (offsetGapList) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("OffsetsBag@" + Integer.toHexString(System.identityHashCode(this)) + " insertUpdate: doc=" + Integer.toHexString(System.identityHashCode(this.document)) + ", offset=" + e.getOffset() + ", insertLength=" + e.getLength() + ", docLength=" + this.document.getLength());
                    }
                    bag.marks.defaultInsertUpdate(e.getOffset(), e.getLength());
                }
            } else {
                this.run();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void removeUpdate(DocumentEvent e) {
            OffsetsBag bag = (OffsetsBag)this.get();
            if (bag != null) {
                OffsetGapList offsetGapList = bag.marks;
                synchronized (offsetGapList) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("OffsetsBag@" + Integer.toHexString(System.identityHashCode(this)) + " removeUpdate: doc=" + Integer.toHexString(System.identityHashCode(this.document)) + ", offset=" + e.getOffset() + ", removedLength=" + e.getLength() + ", docLength=" + this.document.getLength());
                    }
                    bag.marks.defaultRemoveUpdate(e.getOffset(), e.getLength());
                }
            } else {
                this.run();
            }
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        @Override
        public void run() {
            Document d = this.document;
            if (d != null) {
                d.removeDocumentListener(this);
                this.document = null;
            }
        }
    }

    private final class Seq
    implements HighlightsSequence {
        private long version;
        private int startOffset;
        private int endOffset;
        private int highlightStart;
        private int highlightEnd;
        private AttributeSet highlightAttributes;
        private int idx;

        public Seq(long version, int startOffset, int endOffset) {
            this.idx = -1;
            this.version = version;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean moveNext() {
            OffsetGapList offsetGapList = OffsetsBag.this.marks;
            synchronized (offsetGapList) {
                if (this.checkVersion()) {
                    if (this.idx == -1) {
                        this.idx = OffsetsBag.this.findMoveNextIndex(this.startOffset);
                        if (this.idx == -1 && OffsetsBag.this.marks.size() > 0) {
                            this.idx = 0;
                        }
                    } else {
                        ++this.idx;
                    }
                    int[] offsets = new int[2];
                    while (this.isIndexValid(this.idx, offsets)) {
                        if (((Mark)OffsetsBag.this.marks.get(this.idx)).getAttributes() != null) {
                            this.highlightStart = Math.max(offsets[0], this.startOffset);
                            this.highlightEnd = Math.min(offsets[1], this.endOffset);
                            this.highlightAttributes = ((Mark)OffsetsBag.this.marks.get(this.idx)).getAttributes();
                            return true;
                        }
                        ++this.idx;
                    }
                }
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getStartOffset() {
            OffsetGapList offsetGapList = OffsetsBag.this.marks;
            synchronized (offsetGapList) {
                assert (this.idx != -1);
                return this.highlightStart;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getEndOffset() {
            OffsetGapList offsetGapList = OffsetsBag.this.marks;
            synchronized (offsetGapList) {
                assert (this.idx != -1);
                return this.highlightEnd;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public AttributeSet getAttributes() {
            OffsetGapList offsetGapList = OffsetsBag.this.marks;
            synchronized (offsetGapList) {
                assert (this.idx != -1);
                return this.highlightAttributes;
            }
        }

        private boolean isIndexValid(int idx, int[] offsets) {
            return idx >= 0 && idx + 1 < OffsetsBag.this.marks.size() && (offsets[0] = ((Mark)OffsetsBag.this.marks.get(idx)).getOffset()) < this.endOffset && (offsets[1] = ((Mark)OffsetsBag.this.marks.get(idx + 1)).getOffset()) > this.startOffset;
        }

        private OffsetsBag getBag() {
            return OffsetsBag.this;
        }

        private boolean checkVersion() {
            return OffsetsBag.this.version == this.version;
        }
    }

    static final class Mark
    extends OffsetGapList.Offset {
        private AttributeSet attribs;

        public Mark(int offset, AttributeSet attribs) {
            super(offset);
            this.attribs = attribs;
        }

        public AttributeSet getAttributes() {
            return this.attribs;
        }

        public void setAttributes(AttributeSet attribs) {
            this.attribs = attribs;
        }

        public String toString() {
            return "offset=" + this.getOffset() + ", attribs=" + this.attribs;
        }
    }

}

