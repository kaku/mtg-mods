/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.spi.editor.highlighting.support;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;

public final class PositionsBag
extends AbstractHighlightsContainer {
    private static final Logger LOG = Logger.getLogger(PositionsBag.class.getName());
    private final Document document;
    private final boolean mergeHighlights;
    private final GapList<Position> marks = new GapList();
    private final GapList<AttributeSet> attributes = new GapList();
    private long version = 0;

    public PositionsBag(Document document) {
        this(document, false);
    }

    public PositionsBag(Document document, boolean mergeHighlights) {
        this.document = document;
        this.mergeHighlights = mergeHighlights;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addHighlight(Position startPosition, Position endPosition, AttributeSet attributes) {
        int[] offsets;
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            offsets = this.addHighlightImpl(startPosition, endPosition, attributes);
            if (offsets != null) {
                ++this.version;
            }
        }
        if (offsets != null) {
            this.fireHighlightsChange(offsets[0], offsets[1]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addAllHighlights(PositionsBag bag) {
        int[] offsets;
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            offsets = this.addAllHighlightsImpl(bag);
            if (offsets != null) {
                ++this.version;
            }
        }
        if (offsets != null) {
            this.fireHighlightsChange(offsets[0], offsets[1]);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setHighlights(PositionsBag bag) {
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            int[] clearedArea = this.clearImpl();
            int[] populatedArea = null;
            GapList<Position> newMarks = bag.getMarks();
            GapList<AttributeSet> newAttributes = bag.getAttributes();
            GapList<Position> gapList2 = newMarks;
            synchronized (gapList2) {
                for (Position mark : newMarks) {
                    this.marks.add(this.marks.size(), (Object)mark);
                }
                for (AttributeSet attrib : newAttributes) {
                    this.attributes.add(this.attributes.size(), (Object)attrib);
                }
                if (this.marks.size() > 0) {
                    populatedArea = new int[]{((Position)this.marks.get(0)).getOffset(), ((Position)this.marks.get(this.marks.size() - 1)).getOffset()};
                }
            }
            if (clearedArea != null) {
                changeStart = clearedArea[0];
                changeEnd = clearedArea[1];
            }
            if (populatedArea != null) {
                if (changeStart == Integer.MAX_VALUE || changeStart > populatedArea[0]) {
                    changeStart = populatedArea[0];
                }
                if (changeEnd == Integer.MIN_VALUE || changeEnd < populatedArea[1]) {
                    changeEnd = populatedArea[1];
                }
            }
            if (changeStart < changeEnd) {
                ++this.version;
            }
        }
        if (changeStart < changeEnd) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeHighlights(Position startPosition, Position endPosition, boolean clip) {
        if (!clip) {
            this.removeHighlights(startPosition.getOffset(), endPosition.getOffset());
            return;
        }
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        if (startPosition.getOffset() == endPosition.getOffset()) {
            return;
        }
        assert (startPosition.getOffset() < endPosition.getOffset());
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            int startIdx;
            if (this.marks.isEmpty()) {
                return;
            }
            int endIdx = this.indexBeforeOffset(endPosition.getOffset(), (startIdx = this.indexBeforeOffset(startPosition.getOffset())) < 0 ? 0 : startIdx, this.marks.size() - 1);
            if (startIdx == endIdx) {
                if (startIdx != -1 && this.attributes.get(startIdx) != null) {
                    AttributeSet original = (AttributeSet)this.attributes.get(startIdx);
                    if (((Position)this.marks.get(startIdx)).getOffset() == startPosition.getOffset()) {
                        this.marks.set(startIdx, (Object)endPosition);
                        this.attributes.set(startIdx, (Object)original);
                    } else {
                        this.marks.add(startIdx + 1, (Object)startPosition);
                        this.attributes.add(startIdx + 1, (Object)null);
                        this.marks.add(startIdx + 2, (Object)endPosition);
                        this.attributes.add(startIdx + 2, (Object)original);
                    }
                    changeStart = startPosition.getOffset();
                    changeEnd = endPosition.getOffset();
                }
                startIdx = Integer.MAX_VALUE;
                endIdx = Integer.MIN_VALUE;
            } else {
                assert (endIdx != -1);
                if (this.attributes.get(endIdx) != null) {
                    this.marks.set(endIdx, (Object)endPosition);
                    changeEnd = endPosition.getOffset();
                    --endIdx;
                }
                if (startIdx != -1 && this.attributes.get(startIdx) != null) {
                    if (startIdx + 1 < endIdx) {
                        this.marks.set(++startIdx, (Object)startPosition);
                        this.attributes.set(startIdx, (Object)null);
                    } else if (((Position)this.marks.get(startIdx)).getOffset() < startPosition.getOffset()) {
                        if (startIdx + 1 == endIdx) {
                            this.marks.set(++startIdx, (Object)startPosition);
                            this.attributes.set(startIdx, (Object)null);
                        } else {
                            this.marks.add(++startIdx, (Object)startPosition);
                            this.attributes.add(startIdx, (Object)null);
                        }
                    } else if (startIdx == 0 || this.attributes.get(startIdx - 1) == null) {
                        --startIdx;
                    } else {
                        this.marks.set(startIdx, (Object)startPosition);
                        this.attributes.set(startIdx, (Object)null);
                    }
                    changeStart = startPosition.getOffset();
                }
                ++startIdx;
            }
            if (startIdx <= endIdx) {
                if (changeStart == Integer.MAX_VALUE) {
                    changeStart = ((Position)this.marks.get(startIdx)).getOffset();
                }
                if (changeEnd == Integer.MIN_VALUE) {
                    changeEnd = ((Position)this.marks.get(endIdx)).getOffset();
                }
                this.marks.remove(startIdx, endIdx - startIdx + 1);
                this.attributes.remove(startIdx, endIdx - startIdx + 1);
            }
            if (changeStart < changeEnd) {
                ++this.version;
            }
        }
        if (changeStart < changeEnd) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeHighlights(int startOffset, int endOffset) {
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        assert (startOffset <= endOffset);
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            if (this.marks.isEmpty()) {
                return;
            }
            int startIdx = this.indexBeforeOffset(startOffset);
            int endIdx = this.indexBeforeOffset(endOffset, startIdx < 0 ? 0 : startIdx, this.marks.size() - 1);
            if (startIdx == -1 || this.attributes.get(startIdx) == null) {
                ++startIdx;
            } else if (startIdx > 0 && this.attributes.get(startIdx - 1) != null) {
                this.attributes.set(startIdx, (Object)null);
                ++startIdx;
            }
            if (endIdx != -1 && this.attributes.get(endIdx) != null) {
                if (((Position)this.marks.get(endIdx)).getOffset() < endOffset) {
                    if (endIdx + 1 >= this.attributes.size() || this.attributes.get(endIdx + 1) == null) {
                        ++endIdx;
                    }
                } else {
                    --endIdx;
                }
            }
            if (startIdx <= endIdx) {
                if (changeStart == Integer.MAX_VALUE) {
                    changeStart = ((Position)this.marks.get(startIdx)).getOffset();
                }
                if (changeEnd == Integer.MIN_VALUE) {
                    changeEnd = ((Position)this.marks.get(endIdx)).getOffset();
                }
                this.marks.remove(startIdx, endIdx - startIdx + 1);
                this.attributes.remove(startIdx, endIdx - startIdx + 1);
            }
            if (changeStart < changeEnd) {
                ++this.version;
            }
        }
        if (changeStart < changeEnd) {
            this.fireHighlightsChange(changeStart, changeEnd);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        if (LOG.isLoggable(Level.FINE) && startOffset >= endOffset) {
            LOG.fine("startOffset must be less than endOffset: startOffset = " + startOffset + " endOffset = " + endOffset);
        }
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            return new Seq(this.version, startOffset, endOffset);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clear() {
        int[] clearedArea;
        GapList<Position> gapList = this.marks;
        synchronized (gapList) {
            clearedArea = this.clearImpl();
            if (clearedArea != null) {
                ++this.version;
            }
        }
        if (clearedArea != null) {
            this.fireHighlightsChange(clearedArea[0], clearedArea[1]);
        }
    }

    GapList<Position> getMarks() {
        return this.marks;
    }

    GapList<AttributeSet> getAttributes() {
        return this.attributes;
    }

    private int[] addHighlightImpl(Position startPosition, Position endPosition, AttributeSet attributes) {
        if (startPosition.getOffset() == endPosition.getOffset()) {
            return null;
        }
        assert (startPosition.getOffset() < endPosition.getOffset());
        assert (attributes != null);
        if (this.mergeHighlights) {
            this.merge(startPosition, endPosition, attributes);
        } else {
            this.trim(startPosition, endPosition, attributes);
        }
        return new int[]{startPosition.getOffset(), endPosition.getOffset()};
    }

    private void merge(Position startPosition, Position endPosition, AttributeSet attrSet) {
        AttributeSet lastKnownAttributes = null;
        int startIdx = this.indexBeforeOffset(startPosition.getOffset());
        if (startIdx < 0) {
            startIdx = 0;
            this.marks.add(startIdx, (Object)startPosition);
            this.attributes.add(startIdx, (Object)attrSet);
        } else {
            Position mark = (Position)this.marks.get(startIdx);
            AttributeSet markAttribs = (AttributeSet)this.attributes.get(startIdx);
            AttributeSet newAttribs = markAttribs == null ? attrSet : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{attrSet, markAttribs});
            lastKnownAttributes = (AttributeSet)this.attributes.get(startIdx);
            if (mark.getOffset() == startPosition.getOffset()) {
                this.attributes.set(startIdx, (Object)newAttribs);
            } else {
                this.marks.add(++startIdx, (Object)startPosition);
                this.attributes.add(startIdx, (Object)newAttribs);
            }
        }
        int idx = startIdx + 1;
        do {
            if (idx < this.marks.size()) {
                Position mark = (Position)this.marks.get(idx);
                if (mark.getOffset() >= endPosition.getOffset()) {
                    if (mark.getOffset() <= endPosition.getOffset()) break;
                    this.marks.add(idx, (Object)endPosition);
                    this.attributes.add(idx, (Object)lastKnownAttributes);
                    break;
                }
            } else {
                this.marks.add(idx, (Object)endPosition);
                this.attributes.add(idx, (Object)lastKnownAttributes);
                break;
            }
            lastKnownAttributes = (AttributeSet)this.attributes.get(idx);
            this.attributes.set(idx, (Object)(lastKnownAttributes == null ? attrSet : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{attrSet, lastKnownAttributes})));
            ++idx;
        } while (true);
    }

    private void trim(Position startPosition, Position endPosition, AttributeSet attrSet) {
        int startIdx;
        int endIdx = this.indexBeforeOffset(endPosition.getOffset(), (startIdx = this.indexBeforeOffset(startPosition.getOffset())) < 0 ? 0 : startIdx, this.marks.size() - 1);
        if (startIdx == endIdx) {
            AttributeSet original = null;
            if (startIdx != -1 && this.attributes.get(startIdx) != null) {
                original = (AttributeSet)this.attributes.get(startIdx);
            }
            if (startIdx != -1 && ((Position)this.marks.get(startIdx)).getOffset() == startPosition.getOffset()) {
                this.attributes.set(startIdx, (Object)attrSet);
            } else {
                this.marks.add(++startIdx, (Object)startPosition);
                this.attributes.add(startIdx, (Object)attrSet);
            }
            this.marks.add(++startIdx, (Object)endPosition);
            this.attributes.add(startIdx, (Object)original);
        } else {
            assert (endIdx != -1);
            this.marks.set(endIdx, (Object)endPosition);
            this.attributes.set(endIdx, this.attributes.get(endIdx));
            --endIdx;
            if (startIdx != -1 && ((Position)this.marks.get(startIdx)).getOffset() == startPosition.getOffset()) {
                this.attributes.set(startIdx, (Object)attrSet);
            } else if (startIdx + 1 <= endIdx) {
                this.marks.set(++startIdx, (Object)startPosition);
                this.attributes.set(startIdx, (Object)attrSet);
            } else {
                this.marks.add(++startIdx, (Object)startPosition);
                this.attributes.add(startIdx, (Object)attrSet);
            }
            if (++startIdx <= endIdx) {
                this.marks.remove(startIdx, endIdx - startIdx + 1);
                this.attributes.remove(startIdx, endIdx - startIdx + 1);
            }
        }
    }

    private int[] addAllHighlightsImpl(PositionsBag bag) {
        int changeStart = Integer.MAX_VALUE;
        int changeEnd = Integer.MIN_VALUE;
        GapList<Position> newMarks = bag.getMarks();
        GapList<AttributeSet> newAttributes = bag.getAttributes();
        int i = 0;
        while (i + 1 < newMarks.size()) {
            Position mark1 = (Position)newMarks.get(i);
            Position mark2 = (Position)newMarks.get(i + 1);
            AttributeSet attrSet = (AttributeSet)newAttributes.get(i);
            if (attrSet != null) {
                this.addHighlightImpl(mark1, mark2, attrSet);
                if (changeStart == Integer.MAX_VALUE) {
                    changeStart = mark1.getOffset();
                }
                changeEnd = mark2.getOffset();
            }
            ++i;
        }
        if (changeStart != Integer.MAX_VALUE && changeEnd != Integer.MIN_VALUE) {
            return new int[]{changeStart, changeEnd};
        }
        return null;
    }

    private int[] clearImpl() {
        if (!this.marks.isEmpty()) {
            int changeStart = ((Position)this.marks.get(0)).getOffset();
            int changeEnd = ((Position)this.marks.get(this.marks.size() - 1)).getOffset();
            this.marks.clear();
            this.attributes.clear();
            return new int[]{changeStart, changeEnd};
        }
        return null;
    }

    private int indexBeforeOffset(int offset, int low, int high) {
        int idx = this.findElementIndex(offset, low, high);
        if (idx < 0) {
            idx = - idx - 1;
            return idx - 1;
        }
        return idx;
    }

    private int indexBeforeOffset(int offset) {
        return this.indexBeforeOffset(offset, 0, this.marks.size() - 1);
    }

    private int findElementIndex(int offset, int lowIdx, int highIdx) {
        if (lowIdx < 0 || highIdx > this.marks.size() - 1) {
            throw new IndexOutOfBoundsException("lowIdx = " + lowIdx + ", highIdx = " + highIdx + ", size = " + this.marks.size());
        }
        int low = lowIdx;
        int high = highIdx;
        while (low <= high) {
            int index = low + high >> 1;
            int elemOffset = ((Position)this.marks.get(index)).getOffset();
            if (elemOffset < offset) {
                low = index + 1;
                continue;
            }
            if (elemOffset > offset) {
                high = index - 1;
                continue;
            }
            while (index > 0) {
                if (((Position)this.marks.get(--index)).getOffset() >= offset) continue;
                ++index;
                break;
            }
            return index;
        }
        return - low + 1;
    }

    private final class Seq
    implements HighlightsSequence {
        private long version;
        private int startOffset;
        private int endOffset;
        private int highlightStart;
        private int highlightEnd;
        private AttributeSet highlightAttributes;
        private int idx;

        public Seq(long version, int startOffset, int endOffset) {
            this.idx = -1;
            this.version = version;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean moveNext() {
            GapList gapList = PositionsBag.this.marks;
            synchronized (gapList) {
                if (this.checkVersion()) {
                    if (this.idx == -1) {
                        this.idx = PositionsBag.this.indexBeforeOffset(this.startOffset);
                        if (this.idx == -1 && PositionsBag.this.marks.size() > 0) {
                            this.idx = 0;
                        }
                    } else {
                        ++this.idx;
                    }
                    while (this.isIndexValid(this.idx)) {
                        if (PositionsBag.this.attributes.get(this.idx) != null) {
                            this.highlightStart = Math.max(((Position)PositionsBag.this.marks.get(this.idx)).getOffset(), this.startOffset);
                            this.highlightEnd = Math.min(((Position)PositionsBag.this.marks.get(this.idx + 1)).getOffset(), this.endOffset);
                            this.highlightAttributes = (AttributeSet)PositionsBag.this.attributes.get(this.idx);
                            return true;
                        }
                        ++this.idx;
                    }
                }
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getStartOffset() {
            GapList gapList = PositionsBag.this.marks;
            synchronized (gapList) {
                assert (this.idx != -1);
                return this.highlightStart;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getEndOffset() {
            GapList gapList = PositionsBag.this.marks;
            synchronized (gapList) {
                assert (this.idx != -1);
                return this.highlightEnd;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public AttributeSet getAttributes() {
            GapList gapList = PositionsBag.this.marks;
            synchronized (gapList) {
                assert (this.idx != -1);
                return this.highlightAttributes;
            }
        }

        private boolean isIndexValid(int idx) {
            return idx >= 0 && idx + 1 < PositionsBag.this.marks.size() && ((Position)PositionsBag.this.marks.get(idx)).getOffset() < this.endOffset && ((Position)PositionsBag.this.marks.get(idx + 1)).getOffset() > this.startOffset;
        }

        private boolean checkVersion() {
            return PositionsBag.this.version == this.version;
        }
    }

}

