/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.editor.codegen;

import java.util.List;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.util.Lookup;

public interface CodeGenerator {
    public String getDisplayName();

    public void invoke();

    @MimeLocation(subfolderName="CodeGenerators")
    public static interface Factory {
        public List<? extends CodeGenerator> create(Lookup var1);
    }

}

