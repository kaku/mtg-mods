/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.editor.codegen;

import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.util.Lookup;

@MimeLocation(subfolderName="CodeGeneratorContextProviders")
public interface CodeGeneratorContextProvider {
    public void runTaskWithinContext(Lookup var1, Task var2);

    public static interface Task {
        public void run(Lookup var1);
    }

}

