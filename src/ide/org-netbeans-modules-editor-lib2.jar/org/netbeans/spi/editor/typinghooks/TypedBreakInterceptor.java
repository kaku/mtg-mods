/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.editor.typinghooks;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.openide.util.Parameters;

public interface TypedBreakInterceptor {
    public boolean beforeInsert(Context var1) throws BadLocationException;

    public void insert(MutableContext var1) throws BadLocationException;

    public void afterInsert(Context var1) throws BadLocationException;

    public void cancelled(Context var1);

    public static interface Factory {
        public TypedBreakInterceptor createTypedBreakInterceptor(MimePath var1);
    }

    public static final class MutableContext
    extends Context {
        private String insertionText = null;
        private int breakInsertPosition = -1;
        private int caretPosition = -1;
        private int[] reindentBlocks = null;

        public /* varargs */ void setText(String text, int breakInsertPosition, int caretPosition, int ... reindentBlocks) {
            Parameters.notNull((CharSequence)"text", (Object)text);
            if (text.indexOf(10) == -1) {
                throw new IllegalArgumentException("The text must contain a new line (\\n) character.");
            }
            if (breakInsertPosition != -1) {
                if (breakInsertPosition < 0 || breakInsertPosition >= text.length()) {
                    throw new IllegalArgumentException("The breakInsertPosition=" + breakInsertPosition + " must point in the text=<0, " + text.length() + ").");
                }
                if (text.charAt(breakInsertPosition) != '\n') {
                    throw new IllegalArgumentException("The character at breakInsertPosition=" + breakInsertPosition + " must be the new line (\\n) character.");
                }
            }
            if (caretPosition != -1 && (caretPosition < 0 || caretPosition > text.length())) {
                throw new IllegalArgumentException("The caretPosition=" + caretPosition + " must point in the text=<0, " + text.length() + ">.");
            }
            if (reindentBlocks != null && reindentBlocks.length > 0) {
                if (reindentBlocks.length % 2 != 0) {
                    throw new IllegalArgumentException("The reindentBlocks must contain even number of positions within the text: " + reindentBlocks.length);
                }
                for (int i = 0; i < reindentBlocks.length / 2; ++i) {
                    int s = reindentBlocks[2 * i];
                    if (s < 0 || s > text.length()) {
                        throw new IllegalArgumentException("The reindentBlocks[" + 2 * i + "]=" + s + " must point in the text=<0, " + text.length() + ").");
                    }
                    int e = reindentBlocks[2 * i + 1];
                    if (e < 0 || e > text.length()) {
                        throw new IllegalArgumentException("The reindentBlocks[" + (2 * i + 1) + "]=" + e + " must point in the text=<0, " + text.length() + ").");
                    }
                    if (s <= e) continue;
                    throw new IllegalArgumentException("The reindentBlocks[" + 2 * i + "]=" + s + " must be smaller than reindentBlocks[" + (2 * i + 1) + "]=" + e);
                }
            }
            this.insertionText = text;
            this.breakInsertPosition = breakInsertPosition;
            this.caretPosition = caretPosition;
            this.reindentBlocks = reindentBlocks;
        }

        MutableContext(JTextComponent component, int caretOffset, int insertBreakOffset) {
            super(component, caretOffset, insertBreakOffset);
        }

        Object[] getData() {
            Object[] arrobject;
            if (this.insertionText != null) {
                Object[] arrobject2 = new Object[4];
                arrobject2[0] = this.insertionText;
                arrobject2[1] = this.breakInsertPosition;
                arrobject2[2] = this.caretPosition;
                arrobject = arrobject2;
                arrobject2[3] = this.reindentBlocks;
            } else {
                arrobject = null;
            }
            return arrobject;
        }

        void resetData() {
            this.insertionText = null;
            this.breakInsertPosition = -1;
            this.caretPosition = -1;
            this.reindentBlocks = null;
        }
    }

    public static class Context {
        private final JTextComponent component;
        private final Document document;
        private final int caretOffset;
        private final int breakInsertOffset;

        public JTextComponent getComponent() {
            return this.component;
        }

        public Document getDocument() {
            return this.document;
        }

        public int getCaretOffset() {
            return this.caretOffset;
        }

        public int getBreakInsertOffset() {
            return this.breakInsertOffset;
        }

        private Context(JTextComponent component, int caretOffset, int breakInsertOffset) {
            this.component = component;
            this.document = component.getDocument();
            this.caretOffset = caretOffset;
            this.breakInsertOffset = breakInsertOffset;
        }
    }

}

