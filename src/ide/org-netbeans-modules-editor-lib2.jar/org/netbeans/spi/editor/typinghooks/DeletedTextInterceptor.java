/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.spi.editor.typinghooks;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;

public interface DeletedTextInterceptor {
    public boolean beforeRemove(Context var1) throws BadLocationException;

    public void remove(Context var1) throws BadLocationException;

    public void afterRemove(Context var1) throws BadLocationException;

    public void cancelled(Context var1);

    public static interface Factory {
        public DeletedTextInterceptor createDeletedTextInterceptor(MimePath var1);
    }

    public static final class Context {
        private final JTextComponent component;
        private final Document document;
        private final int offset;
        private final boolean backwardDelete;
        private final String removedText;

        public JTextComponent getComponent() {
            return this.component;
        }

        public Document getDocument() {
            return this.document;
        }

        public int getOffset() {
            return this.offset;
        }

        public boolean isBackwardDelete() {
            return this.backwardDelete;
        }

        public String getText() {
            return this.removedText;
        }

        Context(JTextComponent component, int offset, String removedText, boolean backwardDelete) {
            this.component = component;
            this.document = component.getDocument();
            this.offset = offset;
            this.backwardDelete = backwardDelete;
            this.removedText = removedText;
        }
    }

}

