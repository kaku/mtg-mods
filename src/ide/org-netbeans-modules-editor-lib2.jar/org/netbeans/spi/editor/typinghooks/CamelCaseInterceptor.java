/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.spi.editor.typinghooks;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;

public interface CamelCaseInterceptor {
    public boolean beforeChange(MutableContext var1) throws BadLocationException;

    public void change(MutableContext var1) throws BadLocationException;

    public void afterChange(MutableContext var1) throws BadLocationException;

    public void cancelled(MutableContext var1);

    public static interface Factory {
        public CamelCaseInterceptor createCamelCaseInterceptor(MimePath var1);
    }

    public static final class MutableContext {
        private final JTextComponent component;
        private final Document document;
        private final int offset;
        private final boolean backward;
        private int nextWordOffset = -1;

        public JTextComponent getComponent() {
            return this.component;
        }

        public Document getDocument() {
            return this.document;
        }

        public int getOffset() {
            return this.offset;
        }

        public boolean isBackward() {
            return this.backward;
        }

        public void setNextWordOffset(int nextWordOffset) {
            this.nextWordOffset = nextWordOffset;
        }

        MutableContext(JTextComponent component, int offset, boolean backwardDelete) {
            this.component = component;
            this.document = component.getDocument();
            this.offset = offset;
            this.backward = backwardDelete;
        }

        Object[] getData() {
            Object[] arrobject;
            if (this.nextWordOffset != -1) {
                Object[] arrobject2 = new Object[1];
                arrobject = arrobject2;
                arrobject2[0] = this.nextWordOffset;
            } else {
                arrobject = null;
            }
            return arrobject;
        }
    }

}

