/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.spi.editor.typinghooks;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.lib2.typinghooks.TypingHooksSpiAccessor;
import org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;

public interface TypedTextInterceptor {
    public boolean beforeInsert(Context var1) throws BadLocationException;

    public void insert(MutableContext var1) throws BadLocationException;

    public void afterInsert(Context var1) throws BadLocationException;

    public void cancelled(Context var1);

    public static interface Factory {
        public TypedTextInterceptor createTypedTextInterceptor(MimePath var1);
    }

    public static final class MutableContext
    extends Context {
        private String insertionText = null;
        private String replacedText = null;
        private int caretPosition = -1;

        @Override
        public String getText() {
            return this.insertionText != null ? this.insertionText : super.getText();
        }

        public void setText(String text, int caretPosition) {
            assert (text != null);
            assert (caretPosition >= 0 && caretPosition <= text.length());
            this.insertionText = text;
            this.caretPosition = caretPosition;
        }

        public String getReplacedText() {
            return this.replacedText;
        }

        private MutableContext(JTextComponent c, Position offset, String typedText, String replacedText) {
            super(c, offset, typedText);
            this.replacedText = replacedText;
        }

        static {
            TypingHooksSpiAccessor.register(new Accessor());
        }

        private static final class Accessor
        extends TypingHooksSpiAccessor {
            private Accessor() {
            }

            @Override
            public MutableContext createTtiContext(JTextComponent c, Position offset, String typedText, String replacedText) {
                return new MutableContext(c, offset, typedText, replacedText);
            }

            @Override
            public Object[] getTtiContextData(MutableContext context) {
                Object[] arrobject;
                if (context.insertionText != null) {
                    Object[] arrobject2 = new Object[2];
                    arrobject2[0] = context.insertionText;
                    arrobject = arrobject2;
                    arrobject2[1] = context.caretPosition;
                } else {
                    arrobject = null;
                }
                return arrobject;
            }

            @Override
            public void resetTtiContextData(MutableContext context) {
                context.insertionText = null;
                context.caretPosition = -1;
            }

            @Override
            public DeletedTextInterceptor.Context createDtiContext(JTextComponent c, int offset, String removedText, boolean backwardDelete) {
                return new DeletedTextInterceptor.Context(c, offset, removedText, backwardDelete);
            }

            @Override
            public CamelCaseInterceptor.MutableContext createDwiContext(JTextComponent c, int offset, boolean backwardDelete) {
                return new CamelCaseInterceptor.MutableContext(c, offset, backwardDelete);
            }

            @Override
            public TypedBreakInterceptor.MutableContext createTbiContext(JTextComponent c, int caretOffset, int insertBreakOffset) {
                return new TypedBreakInterceptor.MutableContext(c, caretOffset, insertBreakOffset);
            }

            @Override
            public Object[] getTbiContextData(TypedBreakInterceptor.MutableContext context) {
                return context.getData();
            }

            @Override
            public void resetTbiContextData(TypedBreakInterceptor.MutableContext context) {
                context.resetData();
            }

            @Override
            public Object[] getDwiContextData(CamelCaseInterceptor.MutableContext context) {
                return context.getData();
            }
        }

    }

    public static class Context {
        private final JTextComponent component;
        private final Document document;
        private final Position offset;
        private final String originallyTypedText;

        public JTextComponent getComponent() {
            return this.component;
        }

        public Document getDocument() {
            return this.document;
        }

        public int getOffset() {
            return this.offset.getOffset();
        }

        public String getText() {
            return this.originallyTypedText;
        }

        Context(JTextComponent component, Position offset, String typedText) {
            this.component = component;
            this.document = component.getDocument();
            this.offset = offset;
            this.originallyTypedText = typedText;
        }
    }

}

