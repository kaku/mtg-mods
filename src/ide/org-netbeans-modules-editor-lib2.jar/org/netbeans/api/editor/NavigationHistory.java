/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.modules.editor.lib2.URLMapper;
import org.netbeans.modules.editor.lib2.WeakPositions;

public final class NavigationHistory {
    public static final String PROP_WAYPOINTS = "NavigationHHistory.PROP_WAYPOINTS";
    private static final Logger LOG = Logger.getLogger(NavigationHistory.class.getName());
    private static final Map<String, NavigationHistory> instances = new HashMap<String, NavigationHistory>();
    private final String id;
    private final String LOCK = new String("NavigationHistory.LOCK");
    private final RingBuffer<Waypoint> waypoints = new RingBuffer<Waypoint>(new Waypoint[50]);
    private int pointer = 0;
    private List<Waypoint> sublistsCache = null;
    private final PropertyChangeSupport PCS;

    public static NavigationHistory getNavigations() {
        return NavigationHistory.get("navigation-history");
    }

    public static NavigationHistory getEdits() {
        return NavigationHistory.get("last-edit-history");
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.PCS.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.PCS.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint markWaypoint(JTextComponent comp, int offset, boolean currentPosition, boolean append) throws BadLocationException {
        assert (comp != null);
        if (comp.getClientProperty("AsTextField") != null) {
            return null;
        }
        Waypoint newWpt = null;
        String string = this.LOCK;
        synchronized (string) {
            Position pos;
            Waypoint wpt;
            Position position = pos = offset == -1 ? null : WeakPositions.get(comp.getDocument(), offset);
            if (!append) {
                while (this.waypoints.size() > this.pointer) {
                    wpt = this.waypoints.remove(this.waypoints.size() - 1);
                    wpt.dispose();
                }
            }
            if (this.waypoints.size() > 0) {
                wpt = this.waypoints.get(this.waypoints.size() - 1);
                JTextComponent wptComp = wpt.getComponent();
                int wptOffset = wpt.getOffset();
                if (wptComp != null && wptComp.equals(comp) && wptOffset == offset) {
                    newWpt = wpt;
                }
            }
            if (newWpt == null) {
                newWpt = new Waypoint(this, comp, pos);
                int rawIndex = this.waypoints.addEx(newWpt);
                newWpt.initRawIndex(rawIndex);
            }
            this.pointer = currentPosition ? this.waypoints.size() - 1 : this.waypoints.size();
            this.sublistsCache = null;
        }
        this.PCS.firePropertyChange("NavigationHHistory.PROP_WAYPOINTS", null, null);
        return newWpt;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint getCurrentWaypoint() {
        String string = this.LOCK;
        synchronized (string) {
            if (this.pointer < this.waypoints.size()) {
                return this.waypoints.get(this.pointer);
            }
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean hasPreviousWaypoints() {
        String string = this.LOCK;
        synchronized (string) {
            return this.pointer > 0;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean hasNextWaypoints() {
        String string = this.LOCK;
        synchronized (string) {
            return this.pointer + 1 < this.waypoints.size();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<Waypoint> getPreviousWaypoints() {
        String string = this.LOCK;
        synchronized (string) {
            if (this.hasPreviousWaypoints()) {
                return this.getSublistsCache().subList(0, this.pointer);
            }
            return Collections.emptyList();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<Waypoint> getNextWaypoints() {
        String string = this.LOCK;
        synchronized (string) {
            if (this.hasNextWaypoints()) {
                return this.getSublistsCache().subList(this.pointer + 1, this.waypoints.size());
            }
            return Collections.emptyList();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint navigateBack() {
        Waypoint waypoint = null;
        String string = this.LOCK;
        synchronized (string) {
            if (this.hasPreviousWaypoints()) {
                --this.pointer;
                waypoint = this.waypoints.get(this.pointer);
            }
        }
        if (waypoint != null) {
            this.PCS.firePropertyChange("NavigationHHistory.PROP_WAYPOINTS", null, null);
        }
        return waypoint;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint navigateForward() {
        Waypoint waypoint = null;
        String string = this.LOCK;
        synchronized (string) {
            if (this.hasNextWaypoints()) {
                ++this.pointer;
                waypoint = this.waypoints.get(this.pointer);
            }
        }
        if (waypoint != null) {
            this.PCS.firePropertyChange("NavigationHHistory.PROP_WAYPOINTS", null, null);
        }
        return waypoint;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint navigateTo(Waypoint waypoint) {
        assert (waypoint != null);
        String string = this.LOCK;
        synchronized (string) {
            int rawIndex = waypoint.getRawIndex();
            if (rawIndex == -1) {
                waypoint = null;
            } else {
                int wptPointer = this.waypoints.getIndex(rawIndex);
                if (this.pointer != wptPointer) {
                    this.pointer = wptPointer;
                } else {
                    waypoint = null;
                }
            }
        }
        if (waypoint != null) {
            this.PCS.firePropertyChange("NavigationHHistory.PROP_WAYPOINTS", null, null);
        }
        return waypoint;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint navigateFirst() {
        Waypoint waypoint = null;
        String string = this.LOCK;
        synchronized (string) {
            if (this.waypoints.size() > 0) {
                this.pointer = 0;
                waypoint = this.waypoints.get(this.pointer);
            }
        }
        if (waypoint != null) {
            this.PCS.firePropertyChange("NavigationHHistory.PROP_WAYPOINTS", null, null);
        }
        return waypoint;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Waypoint navigateLast() {
        Waypoint waypoint = null;
        String string = this.LOCK;
        synchronized (string) {
            if (this.waypoints.size() > 0) {
                this.pointer = this.waypoints.size() - 1;
                waypoint = this.waypoints.get(this.pointer);
            }
        }
        if (waypoint != null) {
            this.PCS.firePropertyChange("NavigationHHistory.PROP_WAYPOINTS", null, null);
        }
        return waypoint;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static NavigationHistory get(String id) {
        Map<String, NavigationHistory> map = instances;
        synchronized (map) {
            NavigationHistory nh = instances.get(id);
            if (nh == null) {
                nh = new NavigationHistory(id);
                instances.put(id, nh);
            }
            return nh;
        }
    }

    private NavigationHistory(String id) {
        this.PCS = new PropertyChangeSupport(this);
        this.id = id;
    }

    private List<Waypoint> getSublistsCache() {
        if (this.sublistsCache == null) {
            this.sublistsCache = Collections.unmodifiableList(new ArrayList<Waypoint>(this.waypoints));
        }
        return this.sublistsCache;
    }

    private static final class RingBuffer<E>
    extends AbstractList<E> {
        private final E[] buffer;
        private int head = 0;
        private int tail = 0;

        public RingBuffer(E[] buffer) {
            assert (buffer != null);
            assert (buffer.length >= 2);
            this.buffer = buffer;
        }

        @Override
        public E set(int index, E element) {
            int rawIndex = this.getRawIndex(index);
            E old = this.buffer[rawIndex];
            this.buffer[rawIndex] = element;
            return old;
        }

        @Override
        public void add(int index, E element) {
            int rawIndex = (this.head + index) % this.buffer.length;
            if (rawIndex != this.tail) {
                throw new UnsupportedOperationException("This ring buffer only allows adding to the end of the buffer.");
            }
            this.addEx(element);
        }

        public int addEx(E element) {
            int rawIndex = this.tail;
            this.buffer[rawIndex] = element;
            this.tail = (this.tail + 1) % this.buffer.length;
            if (this.tail == this.head) {
                if (this.buffer[this.head] instanceof Waypoint) {
                    ((Waypoint)this.buffer[this.head]).dispose();
                }
                this.buffer[this.head] = null;
                this.head = (this.head + 1) % this.buffer.length;
            }
            return rawIndex;
        }

        @Override
        public E remove(int index) {
            int rawIndex = this.getRawIndex(index);
            if (rawIndex == this.head) {
                this.head = (this.head + 1) % this.buffer.length;
            } else {
                int tailMinusOne = (this.tail - 1 + this.buffer.length) % this.buffer.length;
                if (rawIndex == tailMinusOne) {
                    this.tail = tailMinusOne;
                } else {
                    throw new UnsupportedOperationException("This ring buffer only allows removing at the beginning or end of the buffer.");
                }
            }
            E old = this.buffer[rawIndex];
            this.buffer[rawIndex] = null;
            return old;
        }

        @Override
        public E get(int index) {
            return this.buffer[this.getRawIndex(index)];
        }

        @Override
        public int size() {
            return (this.tail - this.head + this.buffer.length) % this.buffer.length;
        }

        private int getRawIndex(int index) {
            if (index >= 0 && index < this.size()) {
                return (this.head + index) % this.buffer.length;
            }
            throw new IndexOutOfBoundsException("Index = " + index + ", size = " + this.size());
        }

        public int getIndex(int rawIndex) {
            boolean valid;
            if (this.tail < this.head) {
                valid = rawIndex >= 0 && rawIndex < this.tail || rawIndex >= this.head && rawIndex < this.buffer.length;
            } else {
                boolean bl = valid = rawIndex >= this.head && rawIndex < this.tail;
            }
            if (valid) {
                return (rawIndex - this.head + this.buffer.length) % this.buffer.length;
            }
            throw new IndexOutOfBoundsException("Invalid raw index. RawIndex = " + rawIndex + ", head = " + this.head + ", tail = " + this.tail);
        }
    }

    public static final class Waypoint {
        private final NavigationHistory navigationHistory;
        private Reference<JTextComponent> compRef;
        private Position pos;
        private URL url;
        private int rawIndex = -2;

        private Waypoint(NavigationHistory nh, JTextComponent comp, Position pos) throws BadLocationException {
            this.navigationHistory = nh;
            this.compRef = new WeakReference<JTextComponent>(comp);
            this.pos = pos;
            this.url = URLMapper.findUrl(comp);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(this.navigationHistory.id + ": waypoint added: " + this.getUrl());
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public URL getUrl() {
            String string = this.navigationHistory.LOCK;
            synchronized (string) {
                return this.url;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public JTextComponent getComponent() {
            String string = this.navigationHistory.LOCK;
            synchronized (string) {
                return this.compRef == null ? null : this.compRef.get();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getOffset() {
            String string = this.navigationHistory.LOCK;
            synchronized (string) {
                return this.pos == null ? -1 : this.pos.getOffset();
            }
        }

        private int getRawIndex() {
            return this.rawIndex;
        }

        private void initRawIndex(int rawIndex) {
            assert (this.rawIndex == -2);
            this.rawIndex = rawIndex;
        }

        private void dispose() {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine(this.navigationHistory.id + ": waypoint disposed: " + this.getUrl());
            }
            this.rawIndex = -1;
            this.url = null;
            this.compRef = null;
            this.pos = null;
        }
    }

}

