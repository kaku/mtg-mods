/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE, ElementType.METHOD})
public @interface EditorActionRegistration {
    public String name();

    public String mimeType() default "";

    public String iconResource() default "";

    public String shortDescription() default "BY_ACTION_NAME";

    public String menuText() default "";

    public String popupText() default "";

    public String menuPath() default "";

    public int menuPosition() default Integer.MAX_VALUE;

    public String popupPath() default "";

    public int popupPosition() default Integer.MAX_VALUE;

    public int toolBarPosition() default Integer.MAX_VALUE;

    public boolean noIconInMenu() default 0;

    public boolean noKeyBinding() default 0;

    public String preferencesKey() default "";

    public boolean preferencesDefault() default 0;

    public int weight() default 0;
}

