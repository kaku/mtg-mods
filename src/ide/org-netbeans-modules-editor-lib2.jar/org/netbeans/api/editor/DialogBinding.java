/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.api.editor;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.editor.lib2.DialogBindingTokenId;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public final class DialogBinding {
    private static final Logger LOG = Logger.getLogger(DialogBinding.class.getName());

    public static void bindComponentToFile(FileObject fileObject, int offset, int length, JTextComponent component) {
        Parameters.notNull((CharSequence)"fileObject", (Object)fileObject);
        Parameters.notNull((CharSequence)"component", (Object)component);
        if (!fileObject.isValid() || !fileObject.isData()) {
            return;
        }
        if (offset < 0 || (long)offset > fileObject.getSize()) {
            throw new IllegalArgumentException("Invalid offset=" + offset + "; file.size=" + fileObject.getSize());
        }
        if (length < 0 || (long)(offset + length) > fileObject.getSize()) {
            throw new IllegalArgumentException("Invalid lenght=" + length + "; offset=" + offset + ", file.size=" + fileObject.getSize());
        }
        DialogBinding.bind(component, null, fileObject, offset, -1, -1, length, fileObject.getMIMEType());
    }

    public static void bindComponentToDocument(Document document, int offset, int length, JTextComponent component) {
        Parameters.notNull((CharSequence)"document", (Object)document);
        Parameters.notNull((CharSequence)"component", (Object)component);
        if (offset < 0 || offset > document.getLength()) {
            throw new IllegalArgumentException("Invalid offset=" + offset + "; file.size=" + document.getLength());
        }
        if (length < 0 || offset + length > document.getLength()) {
            throw new IllegalArgumentException("Invalid lenght=" + length + "; offset=" + offset + ", file.size=" + document.getLength());
        }
        DialogBinding.bind(component, document, null, offset, -1, -1, length, (String)document.getProperty("mimeType"));
    }

    public static void bindComponentToFile(FileObject fileObject, int line, int column, int length, JTextComponent component) {
        Parameters.notNull((CharSequence)"fileObject", (Object)fileObject);
        Parameters.notNull((CharSequence)"component", (Object)component);
        if (!fileObject.isValid() || !fileObject.isData()) {
            return;
        }
        if (line < 0 || column < 0) {
            throw new IllegalArgumentException("Invalid line=" + line + " or column=" + column);
        }
        DialogBinding.bind(component, null, fileObject, -1, line, column, length, fileObject.getMIMEType());
    }

    public static void bindComponentToDocument(Document document, int line, int column, int length, JTextComponent component) {
        Parameters.notNull((CharSequence)"document", (Object)document);
        Parameters.notNull((CharSequence)"component", (Object)component);
        if (line < 0 || column < 0) {
            throw new IllegalArgumentException("Invalid line=" + line + " or column=" + column);
        }
        DialogBinding.bind(component, document, null, -1, line, column, length, (String)document.getProperty("mimeType"));
    }

    private static void bind(JTextComponent component, Document document, FileObject fileObject, int offset, int line, int column, int length, String mimeType) {
        if (component instanceof JEditorPane) {
            ((JEditorPane)component).setEditorKit((EditorKit)MimeLookup.getLookup((String)mimeType).lookup(EditorKit.class));
            ((JEditorPane)component).setBackground(new JTextField().getBackground());
        }
        Document doc = component.getDocument();
        doc.putProperty("mimeType", DialogBindingTokenId.language().mimeType());
        InputAttributes inputAttributes = new InputAttributes();
        Language language = (Language)MimeLookup.getLookup((String)DialogBindingTokenId.language().mimeType()).lookup(Language.class);
        inputAttributes.setValue(language, (Object)"dialogBinding.document", (Object)document, true);
        inputAttributes.setValue(language, (Object)"dialogBinding.fileObject", (Object)fileObject, true);
        inputAttributes.setValue(language, (Object)"dialogBinding.offset", (Object)offset, true);
        inputAttributes.setValue(language, (Object)"dialogBinding.line", (Object)line, true);
        inputAttributes.setValue(language, (Object)"dialogBinding.column", (Object)column, true);
        inputAttributes.setValue(language, (Object)"dialogBinding.length", (Object)length, true);
        doc.putProperty(InputAttributes.class, (Object)inputAttributes);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "\njtc={0}\ndoc={1}\nfile={2}\noffset={3}\nline={4}\ncolumn={5}\nlength={6}\nmimeType={7}\n", new Object[]{component, document, fileObject, offset, line, column, length, mimeType});
        }
    }
}

