/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.api.editor.EditorActionRegistration;

@Retention(value=RetentionPolicy.SOURCE)
@Target(value={ElementType.TYPE, ElementType.METHOD})
public @interface EditorActionRegistrations {
    public EditorActionRegistration[] value();
}

