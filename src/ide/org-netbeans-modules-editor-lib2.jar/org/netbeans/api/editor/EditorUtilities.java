/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor;

import javax.swing.Action;
import javax.swing.text.EditorKit;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;

public final class EditorUtilities {
    private EditorUtilities() {
    }

    public static Action getAction(EditorKit editorKit, String actionName) {
        return EditorActionUtilities.getAction(editorKit, actionName);
    }
}

