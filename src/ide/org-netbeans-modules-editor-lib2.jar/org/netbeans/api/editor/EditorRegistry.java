/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.api.editor;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.EditorApiPackageAccessor;

public final class EditorRegistry {
    private static final Logger LOG;
    public static final String FOCUS_GAINED_PROPERTY = "focusGained";
    public static final String FOCUS_LOST_PROPERTY = "focusLost";
    public static final String FOCUSED_DOCUMENT_PROPERTY = "focusedDocument";
    public static final String COMPONENT_REMOVED_PROPERTY = "componentRemoved";
    public static final String LAST_FOCUSED_REMOVED_PROPERTY = "lastFocusedRemoved";
    private static final String USED_BY_CLONEABLE_EDITOR_PROPERTY = "usedByCloneableEditor";
    private static Item items;
    private static final PropertyChangeSupport pcs;
    private static Class ignoredAncestorClass;

    private EditorRegistry() {
    }

    public static synchronized JTextComponent lastFocusedComponent() {
        return EditorRegistry.firstValidComponent();
    }

    public static synchronized JTextComponent focusedComponent() {
        JTextComponent c = EditorRegistry.firstValidComponent();
        return c != null && c.isFocusOwner() ? c : null;
    }

    public static synchronized List<? extends JTextComponent> componentList() {
        List l;
        JTextComponent c = EditorRegistry.firstValidComponent();
        if (c != null) {
            l = new ArrayList();
            l.add((JTextComponent)c);
            Item item = EditorRegistry.items.next;
            while (item != null) {
                c = (JTextComponent)item.get();
                if (c != null) {
                    l.add((JTextComponent)c);
                    item = item.next;
                    continue;
                }
                item = EditorRegistry.removeFromItemList(item);
            }
        } else {
            l = Collections.emptyList();
        }
        return l;
    }

    public static void addPropertyChangeListener(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

    public static void removePropertyChangeListener(PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void register(JTextComponent c) {
        ArrayList<PropertyChangeEvent> events = new ArrayList<PropertyChangeEvent>();
        Class<EditorRegistry> class_ = EditorRegistry.class;
        synchronized (EditorRegistry.class) {
            assert (c != null);
            if (EditorRegistry.item(c) == null) {
                Item item = new Item(c);
                c.putClientProperty(Item.class, item);
                c.addFocusListener(FocusL.INSTANCE);
                c.addAncestorListener(AncestorL.INSTANCE);
                c.addPropertyChangeListener(CloneableEditorUsageL.INSTANCE);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "EditorRegistry.register(): " + EditorRegistry.dumpComponent(c) + '\n');
                }
                if (c.isFocusOwner()) {
                    EditorRegistry._focusGained(c, null, events);
                } else if (c.isDisplayable()) {
                    EditorRegistry.itemMadeDisplayable(item);
                }
            }
            // ** MonitorExit[var2_2] (shouldn't be in output)
            EditorRegistry.fireEvents(events);
            return;
        }
    }

    static synchronized void setIgnoredAncestorClass(Class ignoredAncestorClass) {
        EditorRegistry.ignoredAncestorClass = ignoredAncestorClass;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void notifyClose(JComponent c) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "EditorRegistry.notifyClose(): " + EditorRegistry.dumpComponent(c) + '\n');
        }
        ArrayList<PropertyChangeEvent> events = new ArrayList<PropertyChangeEvent>();
        Class<EditorRegistry> class_ = EditorRegistry.class;
        synchronized (EditorRegistry.class) {
            Item item = items;
            while (item != null) {
                JTextComponent textComponent = (JTextComponent)item.get();
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest(EditorRegistry.s2s(c) + " isAncestorOf " + EditorRegistry.s2s(textComponent) + " = " + (textComponent == null ? null : Boolean.valueOf(c.isAncestorOf(textComponent))) + '\n');
                    LOG.finest(EditorRegistry.s2s(c) + " isIgnoredAncestorOf " + EditorRegistry.s2s(textComponent) + " = " + (item.ignoredAncestor == null ? null : Boolean.valueOf(item.ignoredAncestor.get() == c)) + '\n');
                }
                if (textComponent == null || item.ignoreAncestorChange && (c.isAncestorOf(textComponent) || item.ignoredAncestor != null && item.ignoredAncestor.get() == c)) {
                    if (textComponent != null) {
                        EditorRegistry._focusLost(textComponent, null, events);
                        item = EditorRegistry.removeFromRegistry(item, events);
                        continue;
                    }
                    item = EditorRegistry.removeFromItemList(item);
                    continue;
                }
                item = item.next;
            }
            // ** MonitorExit[var2_2] (shouldn't be in output)
            EditorRegistry.fireEvents(events);
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void releasedByCloneableEditor(JTextComponent component) {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.fine("releasedByCloneableEditor for " + EditorRegistry.dumpComponent(component) + "\n");
        }
        ArrayList<PropertyChangeEvent> events = new ArrayList<PropertyChangeEvent>();
        Class<EditorRegistry> class_ = EditorRegistry.class;
        synchronized (EditorRegistry.class) {
            Item item = EditorRegistry.item(component);
            EditorRegistry.removeFromRegistry(item, events);
            // ** MonitorExit[var2_2] (shouldn't be in output)
            EditorRegistry.fireEvents(events);
            return;
        }
    }

    static void focusGained(JTextComponent c, Component origFocused) {
        ArrayList<PropertyChangeEvent> events = new ArrayList<PropertyChangeEvent>();
        EditorRegistry._focusGained(c, origFocused, events);
        EditorRegistry.fireEvents(events);
    }

    private static synchronized void _focusGained(JTextComponent c, Component origFocused, List<PropertyChangeEvent> events) {
        Item item = EditorRegistry.item(c);
        assert (item != null);
        EditorRegistry.removeFromItemList(item);
        EditorRegistry.addToItemListAsFirst(item);
        item.focused = true;
        c.addPropertyChangeListener(PropertyDocL.INSTANCE);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "focusGained: " + EditorRegistry.dumpComponent(c) + '\n');
            EditorRegistry.logItemListFinest();
        }
        if (c == origFocused) {
            origFocused = null;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("has equal components, using origFocused = " + origFocused);
            }
        }
        events.add(new PropertyChangeEvent(EditorRegistry.class, "focusGained", origFocused, c));
    }

    static void focusLost(JTextComponent c, Component newFocused) {
        ArrayList<PropertyChangeEvent> events = new ArrayList<PropertyChangeEvent>();
        EditorRegistry._focusLost(c, newFocused, events);
        EditorRegistry.fireEvents(events);
    }

    private static synchronized void _focusLost(JTextComponent c, Component newFocused, List<PropertyChangeEvent> events) {
        Item item = EditorRegistry.item(c);
        assert (item != null);
        if (item.focused) {
            item.focused = false;
            if (!item.ignoreAncestorChange && EditorRegistry.firstValidComponent() != c) {
                throw new IllegalStateException("Invalid ordering of focusLost()");
            }
            c.removePropertyChangeListener(PropertyDocL.INSTANCE);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "focusLost: " + EditorRegistry.dumpComponent(c) + '\n');
                EditorRegistry.logItemListFinest();
            }
            events.add(new PropertyChangeEvent(EditorRegistry.class, "focusLost", c, newFocused));
        }
    }

    static void itemMadeDisplayable(Item item) {
        EditorRegistry.addToItemListAsLast(item);
        JTextComponent c = (JTextComponent)item.get();
        if (c == null) {
            throw new IllegalStateException("Component should be non-null");
        }
        item.ignoreAncestorChange = ignoredAncestorClass != null && SwingUtilities.getAncestorOfClass(ignoredAncestorClass, c) != null;
        item.usedByCloneableEditor = Boolean.TRUE.equals(c.getClientProperty("usedByCloneableEditor"));
        item.ignoreAncestorChange |= item.usedByCloneableEditor;
        if (LOG.isLoggable(Level.FINER)) {
            LOG.fine("ancestorAdded: " + EditorRegistry.dumpComponent((JComponent)item.get()) + '\n');
            EditorRegistry.logItemListFinest();
        }
    }

    static void focusedDocumentChange(JTextComponent c, Document oldDoc, Document newDoc) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "focusedDocument: " + EditorRegistry.dumpComponent(c) + "\n    OLDDoc=" + oldDoc + "\n    NEWDoc=" + newDoc + '\n');
        }
        pcs.firePropertyChange("focusedDocument", oldDoc, newDoc);
    }

    private static JTextComponent firstValidComponent() {
        JTextComponent c = null;
        while (items != null && (c = (JTextComponent)items.get()) == null) {
            EditorRegistry.removeFromItemList(items);
        }
        return c;
    }

    static Item item(JComponent c) {
        return (Item)c.getClientProperty(Item.class);
    }

    private static void addToItemListAsLast(Item item) {
        if (item.linked) {
            return;
        }
        item.linked = true;
        if (items == null) {
            items = item;
        } else {
            Item i = items;
            while (i.next != null) {
                i = i.next;
            }
            i.next = item;
            item.previous = i;
        }
        if (LOG.isLoggable(Level.FINEST)) {
            EditorRegistry.checkItemListConsistency();
        }
    }

    private static void addToItemListAsFirst(Item item) {
        if (item.linked) {
            return;
        }
        item.linked = true;
        item.next = items;
        if (items != null) {
            EditorRegistry.items.previous = item;
        }
        items = item;
        if (LOG.isLoggable(Level.FINEST)) {
            EditorRegistry.checkItemListConsistency();
        }
    }

    private static Item removeFromItemList(Item item) {
        if (!item.linked) {
            return null;
        }
        item.linked = false;
        Item next = item.next;
        if (item.previous == null) {
            assert (items == item);
            items = next;
        } else {
            item.previous.next = next;
        }
        if (next != null) {
            next.previous = item.previous;
        }
        item.previous = null;
        item.next = null;
        if (LOG.isLoggable(Level.FINEST)) {
            EditorRegistry.checkItemListConsistency();
        }
        return next;
    }

    static Item removeFromRegistry(Item item, List<PropertyChangeEvent> events) {
        boolean lastFocused = items == item;
        JTextComponent component = (JTextComponent)item.get();
        item = EditorRegistry.removeFromItemList(item);
        if (component != null) {
            component.putClientProperty(Item.class, null);
            component.removeFocusListener(FocusL.INSTANCE);
            component.removeAncestorListener(AncestorL.INSTANCE);
            component.removePropertyChangeListener(CloneableEditorUsageL.INSTANCE);
            events.add(new PropertyChangeEvent(EditorRegistry.class, "componentRemoved", component, null));
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.fine("Component removed: " + EditorRegistry.dumpComponent(component) + '\n');
                EditorRegistry.logItemListFinest();
            }
            if (lastFocused) {
                events.add(new PropertyChangeEvent(EditorRegistry.class, "lastFocusedRemoved", component, EditorRegistry.lastFocusedComponent()));
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Fired LAST_FOCUSED_REMOVED_PROPERTY for " + EditorRegistry.dumpComponent(component) + '\n');
                }
            }
        }
        return item;
    }

    static void checkItemListConsistency() {
        Item item = items;
        Item previous = null;
        while (item != null) {
            if (!item.linked) {
                throw new IllegalStateException("item=" + item + " is in list but item.linked is false.");
            }
            if (item.previous != previous) {
                throw new IllegalStateException("Invalid previous of item=" + item);
            }
            if (item.ignoreAncestorChange && item.runningTimer != null) {
                throw new IllegalStateException("item=" + item + " has running timer.");
            }
            if (item.focused && item != items) {
                throw new IllegalStateException("Non-first component has focused flag.");
            }
            previous = item;
            item = item.next;
        }
    }

    static void fireEvents(List<PropertyChangeEvent> events) {
        for (PropertyChangeEvent e : events) {
            pcs.firePropertyChange(e);
        }
    }

    static void logItemListFinest() {
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest(EditorRegistry.dumpItemList());
        }
    }

    private static String dumpItemList() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("---------- EditorRegistry Dump START ----------\n");
        int i = 0;
        Item item = items;
        while (item != null) {
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)1);
            sb.append(' ');
            if (item.focused) {
                sb.append("Focused, ");
            }
            if (item.ignoreAncestorChange) {
                sb.append("IgnoreAncestorChange, ");
            }
            if (item.usedByCloneableEditor) {
                sb.append("UsedByCloneableEditor, ");
            }
            sb.append(EditorRegistry.dumpComponent((JComponent)item.get()));
            sb.append('\n');
            item = item.next;
            ++i;
        }
        sb.append("---------- EditorRegistry Dump END ----------\n");
        return sb.toString();
    }

    static String dumpComponent(JComponent c) {
        Document doc = null;
        Object streamDesc = null;
        if (c instanceof JTextComponent && (doc = ((JTextComponent)c).getDocument()) != null) {
            streamDesc = doc.getProperty("stream");
        }
        return "component=" + EditorRegistry.s2s(c) + "; doc=" + EditorRegistry.s2s(doc) + "; streamDesc=" + EditorRegistry.s2s(streamDesc);
    }

    static StringBuilder dumpComponentHierarchy(Component c, String indent, StringBuilder sb) {
        sb.append(indent);
        sb.append(EditorRegistry.s2s(c));
        sb.append('\n');
        if (c instanceof Container) {
            for (Component child : ((Container)c).getComponents()) {
                EditorRegistry.dumpComponentHierarchy(child, indent + "  ", sb);
            }
        }
        return sb;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    static {
        EditorApiPackageAccessor.register(new PackageAccessor());
        LOG = Logger.getLogger(EditorRegistry.class.getName());
        pcs = new PropertyChangeSupport(EditorRegistry.class);
    }

    private static final class PackageAccessor
    extends EditorApiPackageAccessor {
        private PackageAccessor() {
        }

        @Override
        public void register(JTextComponent c) {
            EditorRegistry.register(c);
        }

        @Override
        public void setIgnoredAncestorClass(Class ignoredAncestorClass) {
            EditorRegistry.setIgnoredAncestorClass(ignoredAncestorClass);
        }

        @Override
        public void notifyClose(JComponent c) {
            EditorRegistry.notifyClose(c);
        }
    }

    private static final class AncestorL
    implements AncestorListener {
        static final AncestorL INSTANCE = new AncestorL();
        private static final int BEFORE_REMOVE_DELAY = 2000;

        private AncestorL() {
        }

        @Override
        public void ancestorAdded(AncestorEvent event) {
            Item item = EditorRegistry.item(event.getComponent());
            if (item.runningTimer != null) {
                item.runningTimer.stop();
                item.runningTimer = null;
            }
            EditorRegistry.itemMadeDisplayable(item);
        }

        @Override
        public void ancestorMoved(AncestorEvent event) {
        }

        @Override
        public void ancestorRemoved(AncestorEvent event) {
            JComponent component = event.getComponent();
            Item item = EditorRegistry.item(component);
            if (LOG.isLoggable(Level.FINER)) {
                LOG.fine("ancestorRemoved for " + EditorRegistry.dumpComponent(component) + "; ignoreAncestorChange=" + item.ignoreAncestorChange + '\n');
            }
            if (!item.ignoreAncestorChange) {
                final WeakReference<JComponent> componentRef = new WeakReference<JComponent>(component);
                item.runningTimer = new Timer(2000, new ActionListener(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     * Enabled force condition propagation
                     * Lifted jumps to return sites
                     */
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JComponent c = (JComponent)componentRef.get();
                        if (c == null) return;
                        ArrayList<PropertyChangeEvent> events = new ArrayList<PropertyChangeEvent>();
                        Class<EditorRegistry> class_ = EditorRegistry.class;
                        synchronized (EditorRegistry.class) {
                            Item item = EditorRegistry.item(c);
                            item.runningTimer.stop();
                            item.runningTimer = null;
                            EditorRegistry.removeFromRegistry(item, events);
                            // ** MonitorExit[var4_4] (shouldn't be in output)
                            EditorRegistry.fireEvents(events);
                            return;
                        }
                    }
                });
                item.runningTimer.start();
            } else {
                Container c = SwingUtilities.getAncestorOfClass(ignoredAncestorClass, component);
                item.ignoredAncestor = new WeakReference<Container>(c);
            }
        }

    }

    private static final class CloneableEditorUsageL
    implements PropertyChangeListener {
        static final CloneableEditorUsageL INSTANCE = new CloneableEditorUsageL();

        private CloneableEditorUsageL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("usedByCloneableEditor".equals(evt.getPropertyName()) && Boolean.FALSE.equals(evt.getNewValue())) {
                EditorRegistry.releasedByCloneableEditor((JTextComponent)evt.getSource());
            }
        }
    }

    private static final class PropertyDocL
    implements PropertyChangeListener {
        static final PropertyDocL INSTANCE = new PropertyDocL();

        private PropertyDocL() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("document".equals(evt.getPropertyName())) {
                EditorRegistry.focusedDocumentChange((JTextComponent)evt.getSource(), (Document)evt.getOldValue(), (Document)evt.getNewValue());
            }
        }
    }

    private static final class FocusL
    implements FocusListener {
        static final FocusL INSTANCE = new FocusL();

        private FocusL() {
        }

        @Override
        public void focusGained(FocusEvent e) {
            EditorRegistry.focusGained((JTextComponent)e.getSource(), e.getOppositeComponent());
        }

        @Override
        public void focusLost(FocusEvent e) {
            EditorRegistry.focusLost((JTextComponent)e.getSource(), e.getOppositeComponent());
        }
    }

    private static final class Item
    extends WeakReference<JTextComponent> {
        boolean linked;
        boolean focused;
        Item next;
        Item previous;
        boolean ignoreAncestorChange;
        boolean usedByCloneableEditor;
        Reference<Container> ignoredAncestor;
        Timer runningTimer;

        Item(JTextComponent c) {
            super(c);
        }

        public String toString() {
            return "component=" + this.get() + ", linked=" + this.linked + ", hasPrevious=" + (this.previous != null) + ", hasNext=" + (this.next != null) + ", ignoreAncestorChange=" + this.ignoreAncestorChange + ", hasTimer=" + (this.runningTimer != null);
        }
    }

}

