/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.document;

import javax.swing.text.Document;
import org.netbeans.modules.editor.lib2.document.EditorDocumentHandler;

public final class EditorDocumentUtils {
    private EditorDocumentUtils() {
    }

    public static void runExclusive(Document doc, Runnable r) {
        EditorDocumentHandler.runExclusive(doc, r);
    }
}

