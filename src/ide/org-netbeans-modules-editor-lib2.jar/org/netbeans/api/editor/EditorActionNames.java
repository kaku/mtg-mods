/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor;

public final class EditorActionNames {
    public static final String toggleToolbar = "toggle-toolbar";
    public static final String toggleLineNumbers = "toggle-line-numbers";
    public static final String toggleNonPrintableCharacters = "toggle-non-printable-characters";
    public static final String gotoDeclaration = "goto-declaration";
    public static final String zoomTextIn = "zoom-text-in";
    public static final String zoomTextOut = "zoom-text-out";
    public static final String toggleRectangularSelection = "toggle-rectangular-selection";
    public static final String transposeLetters = "transpose-letters";
    public static final String moveCodeElementUp = "move-code-element-up";
    public static final String moveCodeElementDown = "move-code-element-down";
    public static final String removeSurroundingCode = "remove-surrounding-code";
    public static final String organizeImports = "organize-imports";
    public static final String organizeMembers = "organize-members";

    private EditorActionNames() {
    }
}

