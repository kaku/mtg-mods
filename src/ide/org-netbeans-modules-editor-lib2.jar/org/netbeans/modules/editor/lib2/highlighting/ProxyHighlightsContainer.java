/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.AttributesUtilities
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.modules.editor.lib2.highlighting.CheckedHighlightsSequence;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsSequenceEx;
import org.netbeans.modules.editor.lib2.highlighting.MultiLayerContainer;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;

public final class ProxyHighlightsContainer
extends AbstractHighlightsContainer
implements MultiLayerContainer {
    private static final Logger LOG = Logger.getLogger(ProxyHighlightsContainer.class.getName());
    private Document doc;
    private HighlightsContainer[] layers;
    private boolean[] blacklisted;
    private long version = 0;
    private final String LOCK = new String("ProxyHighlightsContainer.LOCK");
    private final LayerListener listener;

    public ProxyHighlightsContainer() {
        this(null, null);
    }

    public ProxyHighlightsContainer(Document doc, HighlightsContainer[] layers) {
        this.listener = new LayerListener(this);
        this.setLayers(doc, layers);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        assert (0 <= startOffset);
        assert (0 <= endOffset);
        assert (startOffset <= endOffset);
        String string = this.LOCK;
        synchronized (string) {
            if (this.doc == null || this.layers == null || this.layers.length == 0 || startOffset < 0 || endOffset < 0 || startOffset >= endOffset || startOffset > this.doc.getLength()) {
                return HighlightsSequence.EMPTY;
            }
            if (endOffset >= this.doc.getLength()) {
                endOffset = Integer.MAX_VALUE;
            }
            ArrayList<HighlightsSequence> seq = new ArrayList<HighlightsSequence>(this.layers.length);
            for (int i = this.layers.length - 1; i >= 0; --i) {
                if (this.blacklisted[i]) continue;
                try {
                    CheckedHighlightsSequence checked = new CheckedHighlightsSequence(this.layers[i].getHighlights(startOffset, endOffset), startOffset, endOffset);
                    if (LOG.isLoggable(Level.FINE)) {
                        checked.setContainerDebugId("PHC.Layer[" + i + "]=" + this.layers[i]);
                    }
                    seq.add(checked);
                    continue;
                }
                catch (ThreadDeath td) {
                    throw td;
                }
                catch (Throwable t) {
                    this.blacklisted[i] = true;
                    LOG.log(Level.WARNING, "The layer failed to supply highlights: " + this.layers[i], t);
                }
            }
            return new ProxySeq(this.version, seq, startOffset, endOffset);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public HighlightsContainer[] getLayers() {
        String string = this.LOCK;
        synchronized (string) {
            return this.layers;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setLayers(Document doc, HighlightsContainer[] layers) {
        Document docForEvents = null;
        String string = this.LOCK;
        synchronized (string) {
            int i;
            if (doc == null && !$assertionsDisabled && layers != null) {
                throw new AssertionError((Object)"If doc is null the layers must be null too.");
            }
            Document document = docForEvents = doc != null ? doc : this.doc;
            if (this.layers != null) {
                for (i = 0; i < this.layers.length; ++i) {
                    this.layers[i].removeHighlightsChangeListener(this.listener);
                }
            }
            this.doc = doc;
            this.layers = layers;
            this.blacklisted = layers == null ? null : new boolean[layers.length];
            this.increaseVersion();
            if (this.layers != null) {
                for (i = 0; i < this.layers.length; ++i) {
                    this.layers[i].addHighlightsChangeListener(this.listener);
                }
            }
        }
        if (docForEvents != null) {
            docForEvents.render(new Runnable(){

                @Override
                public void run() {
                    ProxyHighlightsContainer.this.fireHighlightsChange(0, Integer.MAX_VALUE);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void layerChanged(HighlightsContainer layer, final int changeStartOffset, final int changeEndOffset) {
        Document docForEvents = null;
        String string = this.LOCK;
        synchronized (string) {
            LOG.log(Level.FINE, "Container's layer changed: {0}", layer);
            this.increaseVersion();
            docForEvents = this.doc;
        }
        if (docForEvents != null) {
            docForEvents.render(new Runnable(){

                @Override
                public void run() {
                    ProxyHighlightsContainer.this.fireHighlightsChange(changeStartOffset, changeEndOffset);
                }
            });
        }
    }

    private void increaseVersion() {
        ++this.version;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("PHC@" + Integer.toHexString(System.identityHashCode(this)) + ", doc@" + Integer.toHexString(System.identityHashCode(this.doc)) + " version=" + this.version);
        }
    }

    static final class Sequence2Marks {
        private HighlightsSequence seq;
        private int startOffset;
        private int endOffset;
        private boolean hasNext = false;
        private boolean useStartOffset = true;
        private boolean finished = true;
        private int lastEndOffset = -1;
        private int previousMarkOffset = -1;
        private AttributeSet previousMarkAttributes = null;

        public Sequence2Marks(HighlightsSequence seq, int startOffset, int endOffset) {
            this.seq = seq;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        public boolean isFinished() {
            return this.finished;
        }

        public boolean moveNext() {
            if (!this.useStartOffset || this.hasNext) {
                this.previousMarkOffset = this.getMarkOffset();
                this.previousMarkAttributes = this.getMarkAttributes();
            }
            if (this.useStartOffset) {
                while ((this.hasNext = this.seq.moveNext()) && this.seq.getEndOffset() <= this.startOffset) {
                }
                if (this.hasNext && this.seq.getStartOffset() > this.endOffset) {
                    this.hasNext = false;
                }
                if (this.hasNext) {
                    if (this.lastEndOffset != -1 && this.lastEndOffset < this.seq.getStartOffset()) {
                        this.useStartOffset = false;
                    } else {
                        this.lastEndOffset = this.seq.getEndOffset();
                    }
                } else if (this.lastEndOffset != -1) {
                    this.useStartOffset = false;
                }
            } else {
                if (this.hasNext) {
                    this.lastEndOffset = this.seq.getEndOffset();
                }
                this.useStartOffset = true;
            }
            this.finished = this.useStartOffset && !this.hasNext;
            return !this.finished;
        }

        public int getMarkOffset() {
            if (this.finished) {
                throw new NoSuchElementException();
            }
            return this.useStartOffset ? Math.max(this.startOffset, this.seq.getStartOffset()) : Math.min(this.endOffset, this.lastEndOffset);
        }

        public AttributeSet getMarkAttributes() {
            if (this.finished) {
                throw new NoSuchElementException();
            }
            return this.useStartOffset ? this.seq.getAttributes() : null;
        }

        public int getPreviousMarkOffset() {
            return this.previousMarkOffset;
        }

        public AttributeSet getPreviousMarkAttributes() {
            return this.previousMarkAttributes;
        }
    }

    private final class ProxySeq
    implements HighlightsSequenceEx {
        private final Sequence2Marks[] marks;
        private int index1;
        private int index2;
        private AttributeSet compositeAttributes;
        private long version;

        public ProxySeq(long version, List<HighlightsSequence> seq, int startOffset, int endOffset) {
            this.index1 = -2;
            this.index2 = -2;
            this.compositeAttributes = null;
            this.version = version;
            this.marks = new Sequence2Marks[seq.size()];
            for (int i = 0; i < seq.size(); ++i) {
                this.marks[i] = new Sequence2Marks(seq.get(i), startOffset, endOffset);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean moveNext() {
            String string = ProxyHighlightsContainer.this.LOCK;
            synchronized (string) {
                if (this.checkVersion()) {
                    if (this.index1 == -2 && this.index2 == -2) {
                        for (Sequence2Marks m : this.marks) {
                            m.moveNext();
                        }
                        this.index2 = this.findLowest();
                    }
                    do {
                        this.index1 = this.index2;
                        if (this.index2 != -1) {
                            this.marks[this.index2].moveNext();
                            this.index2 = this.findLowest();
                        }
                        if (this.index1 == -1 || this.index2 == -1) break;
                        this.compositeAttributes = this.findAttributes();
                    } while (this.compositeAttributes == null);
                    return this.index1 != -1 && this.index2 != -1;
                }
                this.index2 = -1;
                this.index1 = -1;
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getStartOffset() {
            String string = ProxyHighlightsContainer.this.LOCK;
            synchronized (string) {
                if (this.index1 == -2 && this.index2 == -2) {
                    throw new IllegalStateException("Uninitialized sequence, call moveNext() first.");
                }
                if (this.index1 == -1 || this.index2 == -1) {
                    throw new NoSuchElementException();
                }
                return this.marks[this.index1].getPreviousMarkOffset();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getEndOffset() {
            String string = ProxyHighlightsContainer.this.LOCK;
            synchronized (string) {
                if (this.index1 == -2 && this.index2 == -2) {
                    throw new IllegalStateException("Uninitialized sequence, call moveNext() first.");
                }
                if (this.index1 == -1 || this.index2 == -1) {
                    throw new NoSuchElementException();
                }
                return this.marks[this.index2].getMarkOffset();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public AttributeSet getAttributes() {
            String string = ProxyHighlightsContainer.this.LOCK;
            synchronized (string) {
                if (this.index1 == -2 && this.index2 == -2) {
                    throw new IllegalStateException("Uninitialized sequence, call moveNext() first.");
                }
                if (this.index1 == -1 || this.index2 == -1) {
                    throw new NoSuchElementException();
                }
                return this.compositeAttributes;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean isStale() {
            String string = ProxyHighlightsContainer.this.LOCK;
            synchronized (string) {
                return !this.checkVersion();
            }
        }

        private int findLowest() {
            int lowest = Integer.MAX_VALUE;
            int idx = -1;
            for (int i = 0; i < this.marks.length; ++i) {
                int offset;
                if (this.marks[i].isFinished() || (offset = this.marks[i].getMarkOffset()) >= lowest) continue;
                lowest = offset;
                idx = i;
            }
            return idx;
        }

        private AttributeSet findAttributes() {
            ArrayList<AttributeSet> list = new ArrayList<AttributeSet>();
            for (int i = 0; i < this.marks.length; ++i) {
                if (this.marks[i].getPreviousMarkAttributes() == null) continue;
                list.add(this.marks[i].getPreviousMarkAttributes());
            }
            if (!list.isEmpty()) {
                return AttributesUtilities.createComposite((AttributeSet[])list.toArray(new AttributeSet[list.size()]));
            }
            return null;
        }

        private boolean checkVersion() {
            return this.version == ProxyHighlightsContainer.this.version;
        }
    }

    private static final class LayerListener
    implements HighlightsChangeListener {
        private WeakReference<ProxyHighlightsContainer> ref;

        public LayerListener(ProxyHighlightsContainer container) {
            this.ref = new WeakReference<ProxyHighlightsContainer>(container);
        }

        @Override
        public void highlightChanged(HighlightsChangeEvent event) {
            ProxyHighlightsContainer container = this.ref.get();
            if (container != null) {
                container.layerChanged((HighlightsContainer)event.getSource(), event.getStartOffset(), event.getEndOffset());
            }
        }
    }

}

