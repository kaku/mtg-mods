/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public final class NewlineView
extends EditorView {
    private int rawEndOffset = 1;
    private final AttributeSet attributes;

    public NewlineView(AttributeSet attributes) {
        super(null);
        this.attributes = attributes;
    }

    @Override
    public int getRawEndOffset() {
        return this.rawEndOffset;
    }

    @Override
    public void setRawEndOffset(int rawOffset) {
        this.rawEndOffset = rawOffset;
    }

    @Override
    public int getStartOffset() {
        return this.getEndOffset() - this.getLength();
    }

    @Override
    public int getEndOffset() {
        EditorView.Parent parent = (EditorView.Parent)((Object)this.getParent());
        return parent != null ? parent.getViewEndOffset(this.rawEndOffset) : this.rawEndOffset;
    }

    @Override
    public int getLength() {
        return 1;
    }

    @Override
    public AttributeSet getAttributes() {
        return this.attributes;
    }

    @Override
    public float getPreferredSpan(int axis) {
        DocumentView documentView = this.getDocumentView();
        if (axis == 0) {
            return documentView != null ? (documentView.op.isNonPrintableCharactersVisible() ? documentView.op.getNewlineCharTextLayout().getAdvance() : documentView.op.getDefaultCharWidth()) : 1.0f;
        }
        return documentView != null ? documentView.op.getDefaultRowHeight() : 1.0f;
    }

    ParagraphView getParagraphView() {
        return (ParagraphView)this.getParent();
    }

    DocumentView getDocumentView() {
        ParagraphView paragraphView = this.getParagraphView();
        return paragraphView != null ? paragraphView.getDocumentView() : null;
    }

    @Override
    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        Rectangle2D.Double mutableBounds = ViewUtils.shape2Bounds(alloc);
        mutableBounds.width = this.getPreferredSpan(0);
        return mutableBounds;
    }

    @Override
    public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        return this.getStartOffset();
    }

    @Override
    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        int retOffset;
        biasRet[0] = Position.Bias.Forward;
        int viewStartOffset = this.getStartOffset();
        switch (direction) {
            case 3: {
                if (offset == -1) {
                    retOffset = viewStartOffset;
                    break;
                }
                retOffset = -1;
                break;
            }
            case 7: {
                if (offset == -1) {
                    retOffset = viewStartOffset;
                    break;
                }
                if (offset == viewStartOffset) {
                    retOffset = -1;
                    break;
                }
                retOffset = viewStartOffset;
                break;
            }
            case 1: 
            case 5: {
                retOffset = -1;
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return retOffset;
    }

    @Override
    public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
        int viewStartOffset = this.getStartOffset();
        DocumentView docView = this.getDocumentView();
        HighlightsViewUtils.paintHiglighted(g, alloc, clipBounds, docView, this, viewStartOffset, true, null, viewStartOffset, 0, 1);
    }

    @Override
    protected String getDumpName() {
        return "NV";
    }

    public String toString() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }
}

