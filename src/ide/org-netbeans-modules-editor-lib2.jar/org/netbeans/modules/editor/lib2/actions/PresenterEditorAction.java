/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.util.WeakSet
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.netbeans.modules.editor.lib2.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.EditorUtilities;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.lib2.actions.SearchableEditorKit;
import org.openide.awt.Actions;
import org.openide.util.WeakSet;
import org.openide.util.actions.Presenter;

public final class PresenterEditorAction
extends TextAction
implements Presenter.Menu,
Presenter.Popup,
Presenter.Toolbar,
PropertyChangeListener {
    private static final String SELECTED_KEY = "SwingSelectedKey";
    private static final Logger LOG = Logger.getLogger(PresenterEditorAction.class.getName());
    private static Reference<SearchableEditorKit> activeKitRef;
    private static boolean activeKitLastFocused;
    private static final Set<PresenterEditorAction> presenterActions;
    private static final ChangeListener actionsChangeListener;
    private final String actionName;
    private JMenuItem menuPresenter;
    private JMenuItem popupPresenter;
    private Component toolBarPresenter;
    private Action globalKitAction;
    private Action activeKitAction;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static SearchableEditorKit activeKit() {
        Class<PresenterEditorAction> class_ = PresenterEditorAction.class;
        synchronized (PresenterEditorAction.class) {
            // ** MonitorExit[var0] (shouldn't be in output)
            return activeKitRef != null ? activeKitRef.get() : null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void registerAction(PresenterEditorAction action) {
        Class<PresenterEditorAction> class_ = PresenterEditorAction.class;
        synchronized (PresenterEditorAction.class) {
            presenterActions.add(action);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    private static void refreshActiveKitActions(SearchableEditorKit activeKit, boolean kitChanged) {
        for (PresenterEditorAction a : presenterActions) {
            a.refreshActiveKitAction(activeKit, kitChanged);
        }
    }

    public static Action create(Map<String, ?> attrs) {
        String actionName = (String)attrs.get("Name");
        if (actionName == null) {
            throw new IllegalArgumentException("Null Action.NAME attribute for attrs: " + attrs);
        }
        return new PresenterEditorAction(actionName);
    }

    private PresenterEditorAction(String actionName) {
        super(actionName);
        this.actionName = actionName;
        this.refreshGlobalAction(EditorActionUtilities.getGlobalActionsKit());
        this.refreshActiveKitAction(PresenterEditorAction.activeKit(), true);
        PresenterEditorAction.registerAction(this);
    }

    @Override
    public void putValue(String key, Object newValue) {
        if (this.actionName != null && "Name".equals(key)) {
            throw new IllegalArgumentException("PresenterEditorAction(\"" + this.actionName + "\"): putValue(Action.NAME,newName) prohibited.");
        }
        super.putValue(key, newValue);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        EditorKit kit;
        TextUI ui;
        JTextComponent component = this.getTextComponent(evt);
        if (component != null && (ui = component.getUI()) != null && (kit = ui.getEditorKit(component)) != null) {
            Action action = EditorUtilities.getAction(kit, this.actionName);
            if (action != null) {
                action.actionPerformed(evt);
            } else if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Action '" + this.actionName + "' not found in editor kit " + kit + '\n');
            }
        }
    }

    public JMenuItem getMenuPresenter() {
        if (this.menuPresenter == null) {
            this.menuPresenter = this.createMenuItem(false);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("getMenuPresenter() for action=" + this.actionName + " returns " + this.menuPresenter);
        }
        return this.menuPresenter;
    }

    public JMenuItem getPopupPresenter() {
        if (this.popupPresenter == null) {
            this.popupPresenter = this.createMenuItem(true);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("getPopupPresenter() for action=" + this.actionName + " returns " + this.popupPresenter);
        }
        return this.popupPresenter;
    }

    public Component getToolbarPresenter() {
        if (this.toolBarPresenter == null) {
            this.toolBarPresenter = new JButton(this);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("getToolbarPresenter() for action=" + this.actionName + " returns " + this.toolBarPresenter);
        }
        return this.toolBarPresenter;
    }

    @Override
    public Object getValue(String key) {
        Object value = super.getValue(key);
        if (value == null && !"instanceCreate".equals(key)) {
            Action action = this.activeKitAction;
            if (action != null) {
                value = action.getValue(key);
            }
            if (value == null && (action = this.globalKitAction) != null) {
                value = action.getValue(key);
            }
        }
        return value;
    }

    @Override
    public boolean isEnabled() {
        return this.activeKitAction != null ? this.activeKitAction.isEnabled() && activeKitLastFocused : false;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        if ("SwingSelectedKey".equals(propertyName)) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("propertyChange() of SELECTED_KEY for action " + this.actionName);
            }
            this.updateSelectedInPresenters();
        }
        this.firePropertyChange(propertyName, null, null);
    }

    void refreshGlobalAction(SearchableEditorKit kit) {
        Action newAction;
        Action action = newAction = kit != null ? kit.getAction(this.actionName) : null;
        if (newAction != this.globalKitAction) {
            if (this.globalKitAction != null) {
                this.globalKitAction.removePropertyChangeListener(this);
            }
            ActionPropertyRefresh propertyRefresh = new ActionPropertyRefresh();
            propertyRefresh.before();
            this.globalKitAction = newAction;
            propertyRefresh.after();
            if (this.globalKitAction != null) {
                this.globalKitAction.addPropertyChangeListener(this);
            }
        }
    }

    void refreshActiveKitAction(SearchableEditorKit kit, boolean kitChanged) {
        boolean newEnabled;
        if (kitChanged) {
            Action newAction;
            Action action = newAction = kit != null ? kit.getAction(this.actionName) : null;
            if (newAction != this.activeKitAction) {
                if (this.activeKitAction != null) {
                    this.activeKitAction.removePropertyChangeListener(this);
                }
                ActionPropertyRefresh propertyRefresh = new ActionPropertyRefresh();
                propertyRefresh.before();
                this.activeKitAction = newAction;
                propertyRefresh.after();
                if (this.activeKitAction != null) {
                    this.activeKitAction.addPropertyChangeListener(this);
                }
            }
        }
        this.firePropertyChange("enabled", !(newEnabled = this.isEnabled()), newEnabled);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("\"" + this.actionName + "\".refreshActiveKitAction(): activeKitFocused=" + activeKitLastFocused + ", newEnabled=" + newEnabled + ", kitChanged=" + kitChanged + '\n');
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.INFO, "", new Exception());
            }
        }
    }

    private void updateSelectedInPresenters() {
        if (this.isCheckBox()) {
            boolean selected = this.isSelected();
            if (this.menuPresenter instanceof JCheckBoxMenuItem) {
                ((JCheckBoxMenuItem)this.menuPresenter).setSelected(selected);
            }
            if (this.popupPresenter instanceof JCheckBoxMenuItem) {
                ((JCheckBoxMenuItem)this.popupPresenter).setSelected(selected);
            }
        }
    }

    private boolean isSelected() {
        return Boolean.TRUE.equals(this.getValue("SwingSelectedKey"));
    }

    private JMenuItem createMenuItem(boolean isPopup) {
        AbstractButton menuItem;
        if (this.isCheckBox()) {
            menuItem = new JCheckBoxMenuItem();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Create checkbox menu item for action " + this.actionName + ", selected=" + this.isSelected());
            }
            menuItem.setSelected(this.isSelected());
            menuItem.addItemListener(new ItemListener(){

                @Override
                public void itemStateChanged(ItemEvent evt) {
                    boolean actionSelected;
                    boolean checkboxSelected = ((JCheckBoxMenuItem)evt.getSource()).isSelected();
                    if (checkboxSelected != (actionSelected = PresenterEditorAction.this.isSelected())) {
                        Action action = PresenterEditorAction.this.activeKitAction;
                        if (action != null) {
                            action.putValue("SwingSelectedKey", checkboxSelected);
                        } else {
                            action = PresenterEditorAction.this.globalKitAction;
                            if (action != null) {
                                action.putValue("SwingSelectedKey", checkboxSelected);
                            }
                        }
                    }
                }
            });
        } else {
            menuItem = new JMenuItem();
        }
        Actions.connect((JMenuItem)menuItem, (Action)this, (boolean)isPopup);
        return menuItem;
    }

    private boolean isCheckBox() {
        String presenterType = (String)this.getValue("PresenterType");
        return "CheckBox".equals(presenterType);
    }

    static {
        presenterActions = new WeakSet();
        actionsChangeListener = new ChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                Class<PresenterEditorAction> class_ = PresenterEditorAction.class;
                synchronized (PresenterEditorAction.class) {
                    PresenterEditorAction.refreshActiveKitActions(PresenterEditorAction.activeKit(), true);
                    // ** MonitorExit[var2_2] (shouldn't be in output)
                    return;
                }
            }
        };
        EditorRegistry.addPropertyChangeListener(new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             * Enabled force condition propagation
             * Lifted jumps to return sites
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("focusGained".equals(evt.getPropertyName())) {
                    Object ui;
                    JTextComponent focusedTextComponent = (JTextComponent)evt.getNewValue();
                    SearchableEditorKit oldActiveKit = PresenterEditorAction.activeKit();
                    EditorKit kit = null;
                    if (focusedTextComponent != null && (ui = focusedTextComponent.getUI()) != null) {
                        kit = ui.getEditorKit(focusedTextComponent);
                    }
                    ui = PresenterEditorAction.class;
                    synchronized (PresenterEditorAction.class) {
                        SearchableEditorKit newActiveKit;
                        boolean kitChanged;
                        SearchableEditorKit searchableEditorKit = newActiveKit = kit != null ? EditorActionUtilities.getSearchableKit(kit) : null;
                        if (newActiveKit != oldActiveKit) {
                            if (oldActiveKit != null) {
                                oldActiveKit.removeActionsChangeListener(actionsChangeListener);
                            }
                            activeKitRef = newActiveKit != null ? new WeakReference<SearchableEditorKit>(newActiveKit) : null;
                            if (newActiveKit != null) {
                                newActiveKit.addActionsChangeListener(actionsChangeListener);
                            }
                            kitChanged = true;
                        } else {
                            kitChanged = false;
                        }
                        boolean focusChanged = !activeKitLastFocused;
                        activeKitLastFocused = true;
                        if (!focusChanged && !kitChanged) return;
                        {
                            PresenterEditorAction.refreshActiveKitActions(newActiveKit, kitChanged);
                        }
                        // ** MonitorExit[ui] (shouldn't be in output)
                        return;
                    }
                }
                if (!"focusLost".equals(evt.getPropertyName())) return;
                Class<PresenterEditorAction> focusedTextComponent = PresenterEditorAction.class;
                synchronized (PresenterEditorAction.class) {
                    boolean newActiveKitLastFocused = EditorRegistry.lastFocusedComponent() != null;
                    if (newActiveKitLastFocused == activeKitLastFocused) return;
                    activeKitLastFocused = newActiveKitLastFocused;
                    for (PresenterEditorAction a : presenterActions) {
                        a.refreshActiveKitAction(null, false);
                    }
                    // ** MonitorExit[focusedTextComponent] (shouldn't be in output)
                    return;
                }
            }
        });
        EditorActionUtilities.getGlobalActionsKit().addActionsChangeListener(new ChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                SearchableEditorKit globalKit = EditorActionUtilities.getGlobalActionsKit();
                Class<PresenterEditorAction> class_ = PresenterEditorAction.class;
                synchronized (PresenterEditorAction.class) {
                    for (PresenterEditorAction a : presenterActions) {
                        a.refreshGlobalAction(globalKit);
                    }
                    // ** MonitorExit[var3_3] (shouldn't be in output)
                    return;
                }
            }
        });
    }

    private final class ActionPropertyRefresh {
        private boolean checkBox;
        private boolean selected;

        ActionPropertyRefresh() {
            this.checkBox = PresenterEditorAction.this.isCheckBox();
        }

        void before() {
            if (this.checkBox) {
                this.selected = PresenterEditorAction.this.isSelected();
            }
        }

        void after() {
            if (this.checkBox && this.selected != PresenterEditorAction.this.isSelected()) {
                PresenterEditorAction.this.firePropertyChange("SwingSelectedKey", this.selected, !this.selected);
            }
        }
    }

}

