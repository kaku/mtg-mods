/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

public final class EditorPreferencesKeys {
    public static final String CARET_BLINK_RATE = "caret-blink-rate";
    @Deprecated
    public static final String CARET_COLOR_INSERT_MODE = "caret-color-insert-mode";
    @Deprecated
    public static final String CARET_COLOR_OVERWRITE_MODE = "caret-color-overwrite-mode";
    public static final String CARET_ITALIC_INSERT_MODE = "caret-italic-insert-mode";
    public static final String CARET_ITALIC_OVERWRITE_MODE = "caret-italic-overwrite-mode";
    public static final String CARET_TYPE_INSERT_MODE = "caret-type-insert-mode";
    public static final String CARET_TYPE_OVERWRITE_MODE = "caret-type-overwrite-mode";
    public static final String THICK_CARET_WIDTH = "thick-caret-width";
    public static final String CODE_FOLDING_ENABLE = "code-folding-enable";
    public static final String EXPAND_TABS = "expand-tabs";
    public static final String HYPERLINK_ACTIVATION_MODIFIERS = "hyperlink-activation-modifiers";
    public static final String INDENT_SHIFT_WIDTH = "indent-shift-width";
    public static final String LINE_HEIGHT_CORRECTION = "line-height-correction";
    public static final String LINE_NUMBER_VISIBLE = "line-number-visible";
    public static final String MARGIN = "margin";
    public static final String SCROLL_FIND_INSETS = "scroll-find-insets";
    public static final String SCROLL_JUMP_INSETS = "scroll-jump-insets";
    public static final String SPACES_PER_TAB = "spaces-per-tab";
    public static final String STATUS_BAR_CARET_DELAY = "status-bar-caret-delay";
    public static final String STATUS_BAR_VISIBLE = "status-bar-visible";
    public static final String TAB_SIZE = "tab-size";
    public static final String TEXT_LEFT_MARGIN_WIDTH = "text-left-margin-width";
    @Deprecated
    public static final String TEXT_LIMIT_LINE_COLOR = "text-limit-line-color";
    public static final String TEXT_LIMIT_LINE_VISIBLE = "text-limit-line-visible";
    public static final String TEXT_LIMIT_WIDTH = "text-limit-width";
    public static final String PAIR_CHARACTERS_COMPLETION = "pair-characters-completion";
    public static final String IDENTIFIER_ACCEPTOR = "identifier-acceptor";
    public static final String WHITESPACE_ACCEPTOR = "whitespace-acceptor";
    public static final String ABBREV_MAP = "abbrev-map";
    public static final String MACRO_MAP = "macro-map";
    public static final String ABBREV_ACTION_MAP = "abbrev-action-map";
    public static final String ABBREV_EXPAND_ACCEPTOR = "abbrev-expand-acceptor";
    public static final String ABBREV_ADD_TYPED_CHAR_ACCEPTOR = "abbrev-add-typed-char-acceptor";
    public static final String ABBREV_RESET_ACCEPTOR = "abbrev-reset-acceptor";
    public static final String PRINT_LINE_NUMBER_VISIBLE = "print-line-number-visible";
    public static final String COMPONENT_SIZE_INCREMENT = "component-size-increment";
    public static final String RENDERING_HINTS = "rendering-hints";
    public static final String KEY_BINDING_LIST = "key-bindings";
    public static final String INPUT_METHODS_ENABLED = "input-methods-enabled";
    public static final String FIND_WHAT = "find-what";
    public static final String FIND_REPLACE_WITH = "find-replace-with";
    public static final String FIND_HIGHLIGHT_SEARCH = "find-highlight-search";
    public static final String FIND_INC_SEARCH = "find-inc-search";
    public static final String FIND_INC_SEARCH_DELAY = "find-inc-search-delay";
    public static final String FIND_BACKWARD_SEARCH = "find-backward-search";
    public static final String FIND_WRAP_SEARCH = "find-wrap-search";
    public static final String FIND_MATCH_CASE = "find-match-case";
    public static final String FIND_SMART_CASE = "find-smart-case";
    public static final String FIND_WHOLE_WORDS = "find-whole-words";
    public static final String FIND_REG_EXP = "find-reg-exp";
    public static final String FIND_HISTORY = "find-history";
    public static final String FIND_HISTORY_SIZE = "find-history-size";
    public static final String FIND_BLOCK_SEARCH = "find-block-search";
    public static final String FIND_BLOCK_SEARCH_START = "find-block-search-start";
    public static final String FIND_BLOCK_SEARCH_END = "find-block-search-end";
    public static final String WORD_MATCH_SEARCH_LEN = "word-match-search-len";
    public static final String WORD_MATCH_WRAP_SEARCH = "word-match-wrap-search";
    public static final String WORD_MATCH_STATIC_WORDS = "word-match-static-words";
    public static final String WORD_MATCH_MATCH_CASE = "word-match-match-case";
    public static final String WORD_MATCH_SMART_CASE = "word-match-smart-case";
    public static final String WORD_MATCH_MATCH_ONE_CHAR = "word-match-match-one-char";
    public static final String CUSTOM_ACTION_LIST = "custom-action-list";
    public static final String KIT_INSTALL_ACTION_NAME_LIST = "kit-install-action-name-list";
    public static final String KIT_DEINSTALL_ACTION_NAME_LIST = "kit-deinstall-action-name-list";
    public static final String DOC_INSTALL_ACTION_NAME_LIST = "doc-install-action-name-list";
    public static final String HOME_KEY_COLUMN_ONE = "home-key-column-one";
    public static final String NEXT_WORD_FINDER = "next-word-finder";
    public static final String PREVIOUS_WORD_FINDER = "previous-word-finder";
    public static final String WORD_MOVE_NEWLINE_STOP = "word-move-newline-stop";
    public static final String READ_BUFFER_SIZE = "read-buffer-size";
    public static final String WRITE_BUFFER_SIZE = "write-buffer-size";
    public static final String READ_MARK_DISTANCE = "read-mark-distance";
    public static final String MARK_DISTANCE = "mark-distance";
    public static final String MAX_MARK_DISTANCE = "max-mark-distance";
    public static final String MIN_MARK_DISTANCE = "min-mark-distance";
    public static final String SYNTAX_UPDATE_BATCH_SIZE = "syntax-update-batch-size";
    public static final String LINE_BATCH_SIZE = "line-batch-size";
    public static final String IGNORE_VISUAL_CHANGES = "ignore-visual-changes";
    public static final String COLORING_NAME_LIST = "coloring-name-list";
    public static final String TOKEN_CONTEXT_LIST = "token-context-list";
    public static final String COLORING_NAME_SUFFIX = "-coloring";
    public static final String COLORING_NAME_PRINT_SUFFIX = "-print-coloring";
    public static final String DEFAULT_COLORING = "default";
    public static final String LINE_NUMBER_COLORING = "line-number";
    public static final String GUARDED_COLORING = "guarded";
    public static final String CODE_FOLDING_COLORING = "code-folding";
    public static final String CODE_FOLDING_BAR_COLORING = "code-folding-bar";
    public static final String SELECTION_COLORING = "selection";
    public static final String HIGHLIGHT_SEARCH_COLORING = "highlight-search";
    public static final String INC_SEARCH_COLORING = "inc-search";
    public static final String BLOCK_SEARCH_COLORING = "block-search";
    public static final String STATUS_BAR_COLORING = "status-bar";
    public static final String STATUS_BAR_BOLD_COLORING = "status-bar-bold";
    public static final String COMPLETION_AUTO_POPUP = "completion-auto-popup";
    public static final String COMPLETION_AUTO_POPUP_DELAY = "completion-auto-popup-delay";
    public static final String COMPLETION_CASE_SENSITIVE = "completion-case-sensitive";
    public static final String COMPLETION_INSTANT_SUBSTITUTION = "completion-instant-substitution";
    public static final String COMPLETION_NATURAL_SORT = "completion-natural-sort";
    public static final String COMPLETION_PANE_MAX_SIZE = "completion-pane-max-size";
    public static final String COMPLETION_PANE_MIN_SIZE = "completion-pane-min-size";
    public static final String HIGHLIGHT_CARET_ROW = "highlight-caret-row";
    public static final String HIGHLIGHT_MATCH_BRACE = "highlight-match-brace";
    public static final String JAVADOC_AUTO_POPUP = "javadoc-auto-popup";
    public static final String JAVADOC_AUTO_POPUP_DELAY = "javadoc-auto-popup-delay";
    @Deprecated
    public static final String JAVADOC_BG_COLOR = "javadoc-bg-color";
    public static final String JAVADOC_PREFERRED_SIZE = "javadoc-preferred-size";
    public static final String POPUP_MENU_ENABLED = "popup-menu-enabled";
    public static final String SHOW_DEPRECATED_MEMBERS = "show-deprecated-members";
    public static final String POPUP_MENU_ACTION_NAME_LIST = "popup-menu-action-name-list";
    public static final String DIALOG_POPUP_MENU_ACTION_NAME_LIST = "dialog-popup-menu-action-name-list";
    public static final String HIGHLIGHT_CARET_ROW_COLORING = "highlight-caret-row";
    public static final String HIGHLIGHT_MATCH_BRACE_COLORING = "highlight-match-brace";
    public static final String HIGHLIGHT_MATCH_BRACE_DELAY = "highlight-match-brace-delay";
    public static final String CARET_SIMPLE_MATCH_BRACE = "caret-simple-match-brace";
    public static final String COMPLETION_REFRESH_DELAY = "completion-refresh-delay";
    public static final String INDENT_HOT_CHARS_ACCEPTOR = "indent-hot-chars-acceptor";
    public static final String REINDENT_WITH_TEXT_BEFORE = "reindent-with-text-before";
    public static final String FAST_IMPORT_SELECTION = "fast-import-selection";
    public static final String FAST_IMPORT_PACKAGE = "fast-import-package";
    public static final String ALWAYS = "pd_always";
    public static final String NEVER = "pd_never";

    private EditorPreferencesKeys() {
    }
}

