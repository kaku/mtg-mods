/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.TabExpander;
import javax.swing.text.TabableView;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public final class TabView
extends EditorView
implements TabableView {
    private static final Logger LOG = Logger.getLogger(TabView.class.getName());
    private int rawEndOffset;
    private int length;
    private final AttributeSet attributes;
    private float firstTabWidth;
    private float width;

    public TabView(int length, AttributeSet attributes) {
        super(null);
        assert (length > 0);
        this.length = length;
        this.attributes = attributes;
    }

    @Override
    public float getPreferredSpan(int axis) {
        DocumentView docView = this.getDocumentView();
        return axis == 0 ? this.width : (docView != null ? docView.op.getDefaultRowHeight() : 0.0f);
    }

    @Override
    public float getTabbedSpan(float x, TabExpander e) {
        int offset = this.getStartOffset();
        int endOffset = offset + this.getLength();
        float tabX = e.nextTabStop(x, offset++);
        this.firstTabWidth = tabX - x;
        while (offset < endOffset) {
            tabX = e.nextTabStop(tabX, offset++);
        }
        this.width = tabX - x;
        return this.width;
    }

    @Override
    public float getPartialSpan(int p0, int p1) {
        return 0.0f;
    }

    @Override
    public int getRawEndOffset() {
        return this.rawEndOffset;
    }

    @Override
    public void setRawEndOffset(int rawOffset) {
        this.rawEndOffset = rawOffset;
    }

    @Override
    public int getLength() {
        return this.length;
    }

    @Override
    public int getStartOffset() {
        return this.getEndOffset() - this.getLength();
    }

    @Override
    public int getEndOffset() {
        EditorView.Parent parent = (EditorView.Parent)((Object)this.getParent());
        return parent != null ? parent.getViewEndOffset(this.rawEndOffset) : this.rawEndOffset;
    }

    @Override
    public AttributeSet getAttributes() {
        return this.attributes;
    }

    ParagraphView getParagraphView() {
        return (ParagraphView)this.getParent();
    }

    DocumentView getDocumentView() {
        ParagraphView paragraphView = this.getParagraphView();
        return paragraphView != null ? paragraphView.getDocumentView() : null;
    }

    @Override
    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        int extraTabCount;
        int startOffset = this.getStartOffset();
        Rectangle2D.Double mutableBounds = ViewUtils.shape2Bounds(alloc);
        int charIndex = offset - startOffset;
        if (charIndex == 1) {
            mutableBounds.x += (double)this.firstTabWidth;
        } else if (charIndex > 1 && (extraTabCount = this.getLength() - 1) > 0) {
            mutableBounds.x += (double)(this.firstTabWidth + (float)(charIndex - 1) * ((this.width - this.firstTabWidth) / (float)extraTabCount));
        }
        mutableBounds.width = 1.0;
        return mutableBounds;
    }

    @Override
    public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        int offset = this.getStartOffset();
        Rectangle2D.Double mutableBounds = ViewUtils.shape2Bounds(alloc);
        double cmpX = mutableBounds.x + (double)(this.firstTabWidth / 2.0f);
        if (x > cmpX) {
            int endOffset = offset + this.getLength();
            if (++offset < endOffset) {
                float tabWidth = (this.width - this.firstTabWidth) / (float)(endOffset - offset);
                cmpX += (double)(this.firstTabWidth / 2.0f + tabWidth / 2.0f);
                while (x > cmpX && offset < endOffset) {
                    cmpX += (double)tabWidth;
                    ++offset;
                }
            }
        }
        return offset;
    }

    @Override
    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        int startOffset = this.getStartOffset();
        int endOffset = startOffset + this.getLength();
        int retOffset = -1;
        switch (direction) {
            case 3: {
                biasRet[0] = Position.Bias.Forward;
                if (offset == -1) {
                    retOffset = this.getStartOffset();
                    break;
                }
                retOffset = offset + 1;
                if (retOffset < endOffset) break;
                retOffset = endOffset;
                biasRet[0] = Position.Bias.Backward;
                break;
            }
            case 7: {
                biasRet[0] = Position.Bias.Forward;
                if (offset == -1) {
                    retOffset = endOffset - 1;
                    break;
                }
                retOffset = offset - 1;
                if (retOffset >= startOffset) break;
                retOffset = -1;
                break;
            }
            case 1: 
            case 5: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return retOffset;
    }

    @Override
    public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
        int viewStartOffset = this.getStartOffset();
        DocumentView docView = this.getDocumentView();
        HighlightsViewUtils.paintHiglighted(g, alloc, clipBounds, docView, this, viewStartOffset, false, null, viewStartOffset, 0, this.getLength());
    }

    @Override
    public View breakView(int axis, int offset, float pos, float len) {
        return this;
    }

    @Override
    public View createFragment(int p0, int p1) {
        ViewUtils.checkFragmentBounds(p0, p1, this.getStartOffset(), this.getLength());
        return this;
    }

    @Override
    protected String getDumpName() {
        return "TV";
    }

    public String toString() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }
}

