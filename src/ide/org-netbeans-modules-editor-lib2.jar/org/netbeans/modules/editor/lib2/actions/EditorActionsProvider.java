/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.actions;

import java.util.List;
import javax.swing.Action;

public interface EditorActionsProvider {
    public List<Action> getActionsOnly();

    public List<Object> getAllInstances();
}

