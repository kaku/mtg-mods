/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.UIManager;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.editor.lib2.document.EditorDocumentContent;
import org.netbeans.modules.editor.lib2.document.MarkVector;

public abstract class ContentEdit
implements UndoableEdit {
    private static final int ALIVE = 1;
    private static final int HAS_BEEN_DONE = 2;
    private static final int ALIVE_AND_DONE = 3;
    final EditorDocumentContent content;
    final int offset;
    final String text;
    MarkVector.MarkUpdate[] markUpdates;
    MarkVector.MarkUpdate[] bbMarkUpdates;
    private int statusBits;

    protected ContentEdit(EditorDocumentContent content, int offset, String text) {
        this.content = content;
        this.offset = offset;
        this.text = text;
        this.statusBits = 3;
    }

    public final String getText() {
        return this.text;
    }

    public final int length() {
        return this.text.length();
    }

    @Override
    public void undo() throws CannotUndoException {
        if (!this.canUndo()) {
            throw new CannotUndoException();
        }
        this.statusBits &= -3;
    }

    @Override
    public void redo() throws CannotRedoException {
        if (!this.canRedo()) {
            throw new CannotRedoException();
        }
        this.statusBits |= 2;
    }

    @Override
    public void die() {
        this.statusBits &= -2;
    }

    @Override
    public boolean canUndo() {
        return (this.statusBits & 3) == 3;
    }

    @Override
    public boolean canRedo() {
        return (this.statusBits & 3) == 1;
    }

    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        return false;
    }

    @Override
    public boolean replaceEdit(UndoableEdit anEdit) {
        return false;
    }

    @Override
    public boolean isSignificant() {
        return true;
    }

    @Override
    public String getUndoPresentationName() {
        String name = this.getPresentationName();
        name = !"".equals(name) ? UIManager.getString("AbstractUndoableEdit.undoText") + " " + name : UIManager.getString("AbstractUndoableEdit.undoText");
        return name;
    }

    @Override
    public String getRedoPresentationName() {
        String name = this.getPresentationName();
        name = !"".equals(name) ? UIManager.getString("AbstractUndoableEdit.redoText") + " " + name : UIManager.getString("AbstractUndoableEdit.redoText");
        return name;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append(this.getPresentationName()).append(": o=").append(this.offset).append(",len=").append(this.length()).append(",hBDone=").append((this.statusBits & 2) != 0 ? "T" : "F").append(",alive=").append((this.statusBits & 1) != 0 ? "T" : "F").append(",IHC=").append(System.identityHashCode(this));
        if (this.markUpdates != null) {
            sb.append("\n  markUpdates:\n");
            MarkVector.markUpdatesToString(sb, this.markUpdates, this.markUpdates.length);
        } else {
            sb.append(",markUpdates:NONE");
        }
        if (this.bbMarkUpdates != null) {
            sb.append("\n  BBmarkUpdates:\n");
            MarkVector.markUpdatesToString(sb, this.bbMarkUpdates, this.bbMarkUpdates.length);
        } else {
            sb.append(",BBmarkUpdates:NONE");
        }
        return sb.toString();
    }

    static final class RemoveEdit
    extends ContentEdit {
        protected RemoveEdit(EditorDocumentContent content, int offset, String text) {
            super(content, offset, text);
        }

        @Override
        public String getPresentationName() {
            return "Remove:\"" + CharSequenceUtilities.debugText((CharSequence)this.getText()) + "\"";
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            this.content.insertEdit(this, "RemoveEditUndo-");
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            this.content.removeEdit(this, "RemoveEditRedo-");
        }
    }

    static final class InsertEdit
    extends ContentEdit {
        InsertEdit(EditorDocumentContent content, int offset, String text) {
            super(content, offset, text);
        }

        @Override
        public String getPresentationName() {
            return "Insert:\"" + CharSequenceUtilities.debugText((CharSequence)this.getText()) + "\"";
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            this.content.removeEdit(this, "InsertEditUndo-");
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            this.content.insertEdit(this, "InsertEditRedo-");
        }
    }

}

