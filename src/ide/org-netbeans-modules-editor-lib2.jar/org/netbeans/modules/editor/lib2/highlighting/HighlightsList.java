/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.awt.Font;
import javax.swing.text.AttributeSet;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.highlighting.CompoundAttributes;
import org.netbeans.modules.editor.lib2.highlighting.HighlightItem;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public final class HighlightsList {
    private HighlightItem[] highlightItems;
    private int startIndex;
    private int endIndex;
    private int startOffset;

    public HighlightsList(int startOffset) {
        this.highlightItems = new HighlightItem[4];
        this.startOffset = startOffset;
    }

    public HighlightsList(int startOffset, HighlightItem[] items) {
        this.highlightItems = items;
        this.endIndex = items.length;
        this.startOffset = startOffset;
    }

    public int startOffset() {
        return this.startOffset;
    }

    public int endOffset() {
        return this.endIndex - this.startIndex > 0 ? this.highlightItems[this.endIndex - 1].getEndOffset() : this.startOffset;
    }

    public int size() {
        return this.endIndex - this.startIndex;
    }

    public HighlightItem get(int index) {
        if (this.startIndex + index >= this.endIndex) {
            throw new IndexOutOfBoundsException("index=" + index + " >= size=" + this.size() + ", " + this);
        }
        return this.highlightItems[this.startIndex + index];
    }

    public void add(HighlightItem item) {
        if (this.endIndex == this.highlightItems.length) {
            if (this.startIndex == 0) {
                HighlightItem[] tmp = new HighlightItem[this.highlightItems.length << 1];
                System.arraycopy(this.highlightItems, 0, tmp, 0, this.highlightItems.length);
                this.highlightItems = tmp;
            } else {
                System.arraycopy(this.highlightItems, this.startIndex, this.highlightItems, 0, this.size());
                this.endIndex -= this.startIndex;
                this.startIndex = 0;
            }
        }
        this.highlightItems[this.endIndex++] = item;
    }

    public AttributeSet cutSameFont(Font defaultFont, int maxEndOffset, int wsEndOffset, CharSequence docText) {
        assert (maxEndOffset <= this.endOffset());
        HighlightItem item = this.get(0);
        AttributeSet firstAttrs = item.getAttributes();
        int itemEndOffset = item.getEndOffset();
        if (wsEndOffset <= itemEndOffset) {
            if (wsEndOffset == maxEndOffset) {
                if (maxEndOffset == itemEndOffset) {
                    this.cutStartItems(1);
                }
                this.startOffset = maxEndOffset;
                return firstAttrs;
            }
            int limitOffset = Math.min(maxEndOffset, itemEndOffset);
            for (int offset = wsEndOffset; offset < limitOffset; ++offset) {
                if (!Character.isWhitespace(docText.charAt(offset))) continue;
                this.startOffset = offset;
                return firstAttrs;
            }
            if (maxEndOffset > itemEndOffset && Character.isWhitespace(docText.charAt(itemEndOffset)) || maxEndOffset == itemEndOffset) {
                this.cutStartItems(1);
                this.startOffset = itemEndOffset;
                return firstAttrs;
            }
            if (maxEndOffset < itemEndOffset) {
                this.startOffset = maxEndOffset;
                return firstAttrs;
            }
        }
        Font firstFont = ViewUtils.getFont(firstAttrs, defaultFont);
        int index = 1;
        do {
            Font font;
            AttributeSet attrs;
            if (!(font = ViewUtils.getFont(attrs = (item = this.get(index)).getAttributes(), defaultFont)).equals(firstFont)) {
                if (index == 1) {
                    this.cutStartItems(1);
                    this.startOffset = itemEndOffset;
                    return firstAttrs;
                }
                return this.cutCompound(index, itemEndOffset);
            }
            int itemStartOffset = itemEndOffset;
            itemEndOffset = item.getEndOffset();
            if (wsEndOffset <= itemEndOffset) {
                if (wsEndOffset == maxEndOffset) {
                    if (maxEndOffset == itemEndOffset) {
                        return this.cutCompound(index + 1, itemEndOffset);
                    }
                    return this.cutCompoundAndPart(index, maxEndOffset, attrs);
                }
                int limitOffset = Math.min(maxEndOffset, itemEndOffset);
                for (int offset = Math.max((int)itemStartOffset, (int)wsEndOffset); offset < limitOffset; ++offset) {
                    if (!Character.isWhitespace(docText.charAt(offset))) continue;
                    return this.cutCompoundAndPart(index, offset, attrs);
                }
                if (maxEndOffset > itemEndOffset && Character.isWhitespace(docText.charAt(itemEndOffset)) || maxEndOffset == itemEndOffset) {
                    return this.cutCompound(index + 1, itemEndOffset);
                }
                if (maxEndOffset < itemEndOffset) {
                    return this.cutCompoundAndPart(index, maxEndOffset, attrs);
                }
            }
            ++index;
        } while (true);
    }

    public AttributeSet cut(int endOffset) {
        assert (endOffset <= this.endOffset());
        HighlightItem item = this.get(0);
        AttributeSet attrs = item.getAttributes();
        int itemEndOffset = item.getEndOffset();
        if (endOffset <= itemEndOffset) {
            if (endOffset == itemEndOffset) {
                this.cutStartItems(1);
            }
            this.startOffset = endOffset;
            return attrs;
        }
        int index = 1;
        do {
            if (endOffset <= (itemEndOffset = (item = this.get(index)).getEndOffset())) {
                if (endOffset == itemEndOffset) {
                    return this.cutCompound(index + 1, itemEndOffset);
                }
                return this.cutCompoundAndPart(index, endOffset, item.getAttributes());
            }
            ++index;
        } while (true);
    }

    public AttributeSet cutSingleChar() {
        HighlightItem item = this.get(0);
        ++this.startOffset;
        if (this.startOffset == item.getEndOffset()) {
            this.cutStartItems(1);
        }
        return item.getAttributes();
    }

    public void skip(int newStartOffset) {
        HighlightItem item = this.get(0);
        int itemEndOffset = item.getEndOffset();
        if (newStartOffset <= itemEndOffset) {
            if (newStartOffset == itemEndOffset) {
                this.cutStartItems(1);
            }
        } else {
            int index = 1;
            do {
                if (newStartOffset <= (itemEndOffset = (item = this.get(index)).getEndOffset())) {
                    if (newStartOffset == itemEndOffset) {
                        this.cutStartItems(index + 1);
                        break;
                    }
                    this.cutStartItems(index);
                    break;
                }
                ++index;
            } while (true);
        }
        this.startOffset = newStartOffset;
    }

    private void cutStartItems(int count) {
        this.startIndex += count;
    }

    private CompoundAttributes cutCompound(int count, int lastItemEndOffset) {
        HighlightItem[] cutItems = new HighlightItem[count];
        System.arraycopy(this.highlightItems, this.startIndex, cutItems, 0, count);
        this.cutStartItems(count);
        CompoundAttributes cAttrs = new CompoundAttributes(this.startOffset, cutItems);
        this.startOffset = lastItemEndOffset;
        return cAttrs;
    }

    private CompoundAttributes cutCompoundAndPart(int count, int cutEndOffset, AttributeSet lastAttrs) {
        HighlightItem[] cutItems = new HighlightItem[count + 1];
        cutItems[count] = new HighlightItem(cutEndOffset, lastAttrs);
        System.arraycopy(this.highlightItems, this.startIndex, cutItems, 0, count);
        this.cutStartItems(count);
        CompoundAttributes cAttrs = new CompoundAttributes(this.startOffset, cutItems);
        this.startOffset = cutEndOffset;
        return cAttrs;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("HL:<").append(this.startOffset()).append(",").append(this.endOffset()).append(">");
        sb.append(", Items#").append(this.size()).append("\n");
        int size = this.size();
        int digitCount = ArrayUtilities.digitCount((int)size);
        int lastOffset = this.startOffset;
        for (int i = 0; i < size; ++i) {
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            HighlightItem item = this.get(i);
            sb.append(item.toString(lastOffset));
            sb.append('\n');
            lastOffset = item.getEndOffset();
        }
        return sb.toString();
    }
}

