/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public interface HighlightsSequenceEx
extends HighlightsSequence {
    public boolean isStale();
}

