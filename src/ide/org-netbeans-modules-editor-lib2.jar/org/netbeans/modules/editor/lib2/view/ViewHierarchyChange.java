/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public final class ViewHierarchyChange {
    DocumentEvent documentEvent;
    int changeStartOffset = -1;
    int changeEndOffset;
    boolean changeY;
    double startY;
    double endY;
    double deltaY;

    ViewHierarchyChange() {
    }

    void addChange(int changeStartOffset, int changeEndOffset) {
        if (this.changeStartOffset == -1) {
            this.changeStartOffset = changeStartOffset;
            this.changeEndOffset = changeEndOffset;
            if (ViewHierarchyImpl.EVENT_LOG.isLoggable(Level.FINE)) {
                ViewUtils.log(ViewHierarchyImpl.EVENT_LOG, "addChange-New: Offset-range <" + changeStartOffset + "," + changeEndOffset + ">\n");
            }
        } else {
            this.changeStartOffset = Math.min(changeStartOffset, this.changeStartOffset);
            this.changeEndOffset = Math.max(changeEndOffset, this.changeEndOffset);
            if (ViewHierarchyImpl.EVENT_LOG.isLoggable(Level.FINE)) {
                ViewUtils.log(ViewHierarchyImpl.EVENT_LOG, "addChange-Merge: Offset-range <" + changeStartOffset + "," + changeEndOffset + "> => <" + this.changeStartOffset + "," + this.changeEndOffset + ">\n");
            }
        }
    }

    void addChangeY(double startY, double endY, double deltaY) {
        if (!this.changeY) {
            this.changeY = true;
            this.startY = startY;
            this.endY = endY;
            this.deltaY = deltaY;
            if (ViewHierarchyImpl.EVENT_LOG.isLoggable(Level.FINE)) {
                ViewUtils.log(ViewHierarchyImpl.EVENT_LOG, "addChangeY-New: Y:<" + startY + "," + endY + "> dY=" + deltaY + '\n');
            }
        } else {
            this.startY = Math.min(startY, this.startY);
            if (endY > this.endY) {
                this.endY = Math.max(endY -= this.deltaY, this.endY);
            }
            this.deltaY += deltaY;
            if (ViewHierarchyImpl.EVENT_LOG.isLoggable(Level.FINE)) {
                ViewUtils.log(ViewHierarchyImpl.EVENT_LOG, "addChangeY-Merge: Y:<" + startY + "," + endY + "> dY=" + deltaY + " => Y:<" + this.startY + "," + this.endY + "> dY=" + this.deltaY + '\n');
            }
        }
    }

    public DocumentEvent documentEvent() {
        return this.documentEvent;
    }

    public int changeStartOffset() {
        return this.changeStartOffset;
    }

    public int changeEndOffset() {
        return this.changeEndOffset;
    }

    public boolean isChangeY() {
        return this.changeY;
    }

    public double startY() {
        return this.startY;
    }

    public double endY() {
        return this.endY;
    }

    public double deltaY() {
        return this.deltaY;
    }

    public String toString() {
        return "<" + this.changeStartOffset + "," + this.changeEndOffset + "> " + (this.changeY ? new StringBuilder().append("Y:<").append(this.startY()).append(",").append(this.endY()).append("> dY=").append(this.deltaY()).toString() : "No-changeY");
    }
}

