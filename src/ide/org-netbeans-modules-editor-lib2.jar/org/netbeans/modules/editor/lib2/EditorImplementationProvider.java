/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

import java.util.ResourceBundle;
import javax.swing.Action;
import javax.swing.text.JTextComponent;

public interface EditorImplementationProvider {
    public ResourceBundle getResourceBundle(String var1);

    public Action[] getGlyphGutterActions(JTextComponent var1);

    public boolean activateComponent(JTextComponent var1);
}

