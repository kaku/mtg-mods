/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.lib.editor.util.ListenerList
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.EventListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewApiPackageAccessor;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyListener;
import org.openide.util.Exceptions;

public final class ViewHierarchyImpl {
    static final Logger OP_LOG = Logger.getLogger("org.netbeans.editor.view.op");
    static final Logger CHANGE_LOG = Logger.getLogger("org.netbeans.editor.view.change");
    static final Logger BUILD_LOG = Logger.getLogger("org.netbeans.editor.view.build");
    static final Logger PAINT_LOG = Logger.getLogger("org.netbeans.editor.view.paint");
    static final Logger SPAN_LOG = Logger.getLogger("org.netbeans.editor.view.span");
    static final Logger REPAINT_LOG = Logger.getLogger("org.netbeans.editor.view.repaint");
    static final Logger CHECK_LOG = Logger.getLogger("org.netbeans.editor.view.check");
    static final Logger SETTINGS_LOG = Logger.getLogger("org.netbeans.editor.view.settings");
    static final Logger EVENT_LOG = Logger.getLogger("org.netbeans.editor.view.event");
    static final Logger LOG = Logger.getLogger(ViewHierarchyImpl.class.getName());
    private final JTextComponent textComponent;
    private final ViewHierarchy viewHierarchy;
    private final ListenerList<ViewHierarchyListener> listenerList;
    private DocumentView currentDocView;

    public static synchronized ViewHierarchyImpl get(JTextComponent component) {
        ViewHierarchyImpl ViewHierarchyImpl2 = (ViewHierarchyImpl)component.getClientProperty(ViewHierarchyImpl.class);
        if (ViewHierarchyImpl2 == null) {
            ViewHierarchyImpl2 = new ViewHierarchyImpl(component);
            component.putClientProperty(ViewHierarchyImpl.class, ViewHierarchyImpl2);
        }
        return ViewHierarchyImpl2;
    }

    private ViewHierarchyImpl(JTextComponent textComponent) {
        this.textComponent = textComponent;
        this.listenerList = new ListenerList();
        this.viewHierarchy = ViewApiPackageAccessor.get().createViewHierarchy(this);
    }

    @NonNull
    public JTextComponent textComponent() {
        return this.textComponent;
    }

    @NonNull
    public ViewHierarchy viewHierarchy() {
        return this.viewHierarchy;
    }

    public synchronized void setDocumentView(DocumentView docView) {
        this.currentDocView = docView;
    }

    public synchronized DocumentView getDocumentView() {
        return this.currentDocView;
    }

    public LockedViewHierarchy lock() {
        LockedViewHierarchy lvh = ViewApiPackageAccessor.get().createLockedViewHierarchy(this);
        return lvh;
    }

    public double modelToY(DocumentView docView, int offset) {
        if (docView != null) {
            return docView.modelToYNeedsLock(offset);
        }
        return this.fallBackModelToY(offset);
    }

    private double fallBackModelToY(int offset) {
        Rectangle s;
        try {
            s = this.textComponent.modelToView(offset);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            s = null;
        }
        if (s != null) {
            return s.getBounds().y;
        }
        return 0.0;
    }

    public double[] modelToY(DocumentView docView, int[] offsets) {
        if (docView != null) {
            return docView.modelToYNeedsLock(offsets);
        }
        double[] ys = new double[offsets.length];
        for (int i = 0; i < offsets.length; ++i) {
            ys[i] = this.fallBackModelToY(offsets[i]);
        }
        return ys;
    }

    public Shape modelToView(DocumentView docView, int offset, Position.Bias bias) {
        if (docView != null) {
            return docView.modelToViewNeedsLock(offset, docView.getAllocation(), bias);
        }
        TextUI ui = this.textComponent.getUI();
        try {
            return ui != null ? ui.modelToView(this.textComponent, offset, bias) : null;
        }
        catch (BadLocationException ex) {
            return null;
        }
    }

    public int viewToModel(DocumentView docView, double x, double y, Position.Bias[] biasReturn) {
        if (docView != null) {
            return docView.viewToModelNeedsLock(x, y, docView.getAllocation(), biasReturn);
        }
        TextUI ui = this.textComponent.getUI();
        return ui != null ? ui.viewToModel(this.textComponent, new Point((int)x, (int)y), biasReturn) : 0;
    }

    public int modelToParagraphViewIndex(DocumentView docView, int offset) {
        int pViewIndex = docView != null && docView.op.isActive() ? docView.getViewIndex(offset) : -1;
        return pViewIndex;
    }

    public int yToParagraphViewIndex(DocumentView docView, double y) {
        int pViewIndex = docView != null && docView.op.isActive() ? docView.getViewIndex(y) : -1;
        return pViewIndex;
    }

    public boolean verifyParagraphViewIndexValid(DocumentView docView, int paragraphViewIndex) {
        return docView.op.isActive() && paragraphViewIndex >= 0 && paragraphViewIndex < docView.getViewCount();
    }

    public int getParagraphViewCount(DocumentView docView) {
        int pViewCount = docView != null && docView.op.isActive() ? docView.getViewCount() : -1;
        return pViewCount;
    }

    public float getDefaultRowHeight(DocumentView docView) {
        return docView.op.getDefaultRowHeight();
    }

    public float getDefaultCharWidth(DocumentView docView) {
        return docView.op.getDefaultCharWidth();
    }

    public boolean isActive(DocumentView docView) {
        return docView != null && docView.op.isActive();
    }

    public void addViewHierarchyListener(ViewHierarchyListener l) {
        this.listenerList.add((EventListener)l);
    }

    public void removeViewHierarchyListener(ViewHierarchyListener l) {
        this.listenerList.remove((EventListener)l);
    }

    void fireChange(ViewHierarchyChange change) {
        ViewHierarchyEvent evt = ViewApiPackageAccessor.get().createEvent(this.viewHierarchy, change);
        if (EVENT_LOG.isLoggable(Level.FINE)) {
            EVENT_LOG.fine("Firing event: " + evt + "\n");
        }
        for (ViewHierarchyListener l : this.listenerList.getListeners()) {
            l.viewHierarchyChanged(evt);
        }
    }

    public String toString() {
        return this.currentDocView != null ? this.currentDocView.getDumpId() : "<NULL-docView>";
    }
}

