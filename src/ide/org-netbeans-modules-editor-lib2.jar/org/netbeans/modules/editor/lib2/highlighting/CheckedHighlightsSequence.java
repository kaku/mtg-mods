/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import org.netbeans.modules.editor.lib2.highlighting.CompoundHighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public final class CheckedHighlightsSequence
implements HighlightsSequence {
    private static final Logger LOG = Logger.getLogger(CheckedHighlightsSequence.class.getName());
    private final HighlightsSequence originalSeq;
    private final int startOffset;
    private final int endOffset;
    private String containerDebugId = null;
    private int start = -1;
    private int end = -1;

    public CheckedHighlightsSequence(HighlightsSequence seq, int startOffset, int endOffset, String containerDebugId) {
        assert (seq != null);
        assert (0 <= startOffset);
        assert (0 <= endOffset);
        assert (startOffset <= endOffset);
        this.originalSeq = seq;
        this.startOffset = startOffset;
        this.endOffset = endOffset;
        if (LOG.isLoggable(Level.FINE)) {
            this.containerDebugId = containerDebugId != null ? containerDebugId : seq.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(seq));
        }
    }

    public CheckedHighlightsSequence(HighlightsSequence seq, int startOffset, int endOffset) {
        this(seq, startOffset, endOffset, null);
    }

    public void setContainerDebugId(String containerDebugId) {
        this.containerDebugId = containerDebugId;
    }

    @Override
    public boolean moveNext() {
        boolean hasNext;
        boolean retry = hasNext = this.originalSeq.moveNext();
        while (retry) {
            this.start = this.originalSeq.getStartOffset();
            this.end = this.originalSeq.getEndOffset();
            if (this.start > this.end) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, this.containerDebugId + " supplied invalid highlight " + CompoundHighlightsContainer.dumpHighlight(this.originalSeq, null) + ", requested range <" + this.startOffset + ", " + this.endOffset + ">." + " Highlight ignored.");
                }
                retry = hasNext = this.originalSeq.moveNext();
                continue;
            }
            if (this.start > this.endOffset || this.end < this.startOffset) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, this.containerDebugId + " supplied highlight " + CompoundHighlightsContainer.dumpHighlight(this.originalSeq, null) + ", which is outside of the requested range <" + this.startOffset + ", " + this.endOffset + ">." + " Highlight skipped.");
                }
                retry = hasNext = this.originalSeq.moveNext();
                continue;
            }
            if (this.originalSeq.getAttributes() == null) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, this.containerDebugId + " supplied highlight " + CompoundHighlightsContainer.dumpHighlight(this.originalSeq, null) + ", which has null attributes <" + this.startOffset + ", " + this.endOffset + ">." + " Highlight skipped.");
                }
                retry = hasNext = this.originalSeq.moveNext();
                continue;
            }
            retry = false;
        }
        if (hasNext) {
            boolean unclipped = false;
            if (this.start < this.startOffset) {
                this.start = this.startOffset;
                unclipped = true;
            }
            if (this.end > this.endOffset) {
                this.end = this.endOffset;
                unclipped = true;
            }
            if (unclipped && LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, this.containerDebugId + " supplied unclipped highlight " + CompoundHighlightsContainer.dumpHighlight(this.originalSeq, null) + ", requested range <" + this.startOffset + ", " + this.endOffset + ">." + " Highlight clipped.");
            }
        }
        return hasNext;
    }

    @Override
    public int getStartOffset() {
        return this.start;
    }

    @Override
    public int getEndOffset() {
        return this.end;
    }

    @Override
    public AttributeSet getAttributes() {
        return this.originalSeq.getAttributes();
    }
}

