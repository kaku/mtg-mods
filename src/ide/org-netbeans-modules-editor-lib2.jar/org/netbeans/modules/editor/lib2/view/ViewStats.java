/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class ViewStats {
    private static final Logger LOG = Logger.getLogger(ViewStats.class.getName());
    private static final int TEXT_LAYOUT_CREATED_OR_REUSED_THRESHOLD = 200;
    private static final int STALE_VIEW_CREATION_TIMEOUT = 10;
    private static int textLayoutCreatedCount;
    private static int textLayoutCreatedCharCount;
    private static int textLayoutReusedCount;
    private static int textLayoutReusedCharCount;
    private static int staleViewCreationCount;

    private ViewStats() {
    }

    public static void incrementTextLayoutCreated(int charCount) {
        textLayoutCreatedCharCount += charCount;
        if (LOG.isLoggable(Level.FINE) && (LOG.isLoggable(Level.FINEST) || ++textLayoutCreatedCount % 200 == 0)) {
            LOG.fine(ViewStats.stats());
        }
    }

    public static void incrementTextLayoutReused(int charCount) {
        textLayoutReusedCharCount += charCount;
        if (LOG.isLoggable(Level.FINE) && (LOG.isLoggable(Level.FINEST) || ++textLayoutReusedCount % 200 == 0)) {
            LOG.fine(ViewStats.stats());
        }
    }

    public static void incrementStaleViewCreations() {
        if (LOG.isLoggable(Level.FINE) && (LOG.isLoggable(Level.FINEST) || ++staleViewCreationCount % 10 == 0)) {
            LOG.fine(ViewStats.stats());
        }
    }

    public static String stats() {
        return "TextLayouts:\n  Created:\tcount: " + textLayoutCreatedCount + "\tchar-count: " + textLayoutCreatedCharCount + "\n  Reused:\tcount: " + textLayoutReusedCount + "\tchar-count: " + textLayoutReusedCharCount + "\nStaleCreations: " + staleViewCreationCount + "\n";
    }
}

