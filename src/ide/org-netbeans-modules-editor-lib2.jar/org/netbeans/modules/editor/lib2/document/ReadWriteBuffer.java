/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.document;

import org.netbeans.api.annotations.common.NonNull;

public final class ReadWriteBuffer {
    char[] text;
    int length;

    public ReadWriteBuffer(@NonNull char[] text, int length) {
        assert (length >= 0);
        this.text = text;
        this.length = length;
    }

    public int length() {
        return this.length;
    }

    public char[] text() {
        return this.text;
    }

    public String toString() {
        return new String(this.text, 0, this.length);
    }
}

