/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.netbeans.spi.editor.highlighting.support.PositionsBag;

public class BlockHighlighting
extends AbstractHighlightsContainer
implements HighlightsChangeListener {
    private static final Logger LOG = Logger.getLogger(BlockHighlighting.class.getName());
    private String layerId;
    private JTextComponent component;
    private Document document;
    private PositionsBag bag;

    public BlockHighlighting(String layerId, JTextComponent component) {
        this.layerId = layerId;
        this.component = component;
        this.document = component.getDocument();
        this.bag = new PositionsBag(this.document);
        this.bag.addHighlightsChangeListener(this);
    }

    public String getLayerTypeId() {
        return this.layerId;
    }

    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        return this.bag.getHighlights(startOffset, endOffset);
    }

    @Override
    public void highlightChanged(HighlightsChangeEvent event) {
        this.fireHighlightsChange(event.getStartOffset(), event.getEndOffset());
    }

    public void highlightBlock(final int startOffset, final int endOffset, final String coloringName, final boolean extendsEol, final boolean extendsEmptyLine) {
        this.document.render(new Runnable(){

            @Override
            public void run() {
                if (startOffset < endOffset) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Highlighting block: [" + startOffset + ", " + endOffset + "]; " + BlockHighlighting.this.getLayerTypeId());
                    }
                    try {
                        PositionsBag newBag = new PositionsBag(BlockHighlighting.this.document);
                        newBag.addHighlight(BlockHighlighting.this.document.createPosition(startOffset), BlockHighlighting.this.document.createPosition(endOffset), BlockHighlighting.this.getAttribs(coloringName, extendsEol, extendsEmptyLine));
                        BlockHighlighting.this.bag.setHighlights(newBag);
                    }
                    catch (BadLocationException e) {
                        LOG.log(Level.FINE, "Can't add highlight <" + startOffset + ", " + endOffset + ", " + coloringName + ">", e);
                    }
                } else {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Reseting block highlighs; " + BlockHighlighting.this.getLayerTypeId());
                    }
                    BlockHighlighting.this.bag.clear();
                }
            }
        });
    }

    public int[] gethighlightedBlock() {
        HighlightsSequence sequence = this.bag.getHighlights(Integer.MIN_VALUE, Integer.MAX_VALUE);
        if (sequence.moveNext()) {
            return new int[]{sequence.getStartOffset(), sequence.getEndOffset()};
        }
        return null;
    }

    private AttributeSet getAttribs(String coloringName, boolean extendsEol, boolean extendsEmptyLine) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)BlockHighlighting.getMimeType(this.component)).lookup(FontColorSettings.class);
        AttributeSet attribs = fcs.getFontColors(coloringName);
        if (attribs == null) {
            attribs = SimpleAttributeSet.EMPTY;
        } else if (extendsEol || extendsEmptyLine) {
            attribs = AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[]{attribs, AttributesUtilities.createImmutable((Object[])new Object[]{"org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", extendsEol, "org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EMPTY_LINE", extendsEmptyLine})});
        }
        return attribs;
    }

    static String getMimeType(JTextComponent component) {
        EditorKit kit;
        Document doc = component.getDocument();
        String mimeType = (String)doc.getProperty("mimeType");
        if (mimeType == null && (kit = component.getUI().getEditorKit(component)) != null) {
            mimeType = kit.getContentType();
        }
        return mimeType;
    }

}

