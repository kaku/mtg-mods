/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Rectangle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.RepaintManager;
import javax.swing.SwingUtilities;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.openide.util.WeakSet;

final class DebugRepaintManager
extends RepaintManager {
    private static final DebugRepaintManager INSTANCE = new DebugRepaintManager();
    private final Set<JComponent> logComponents = new WeakSet();

    public static void register(JComponent component) {
        if (RepaintManager.currentManager(component) != INSTANCE) {
            RepaintManager.setCurrentManager(INSTANCE);
        }
        INSTANCE.addLogComponent(component);
    }

    private DebugRepaintManager() {
    }

    public void addLogComponent(JComponent component) {
        this.logComponents.add(component);
    }

    @Override
    public void addDirtyRegion(JComponent c, int x, int y, int w, int h) {
        for (JComponent dc : this.logComponents) {
            if (!SwingUtilities.isDescendingFrom(dc, c)) continue;
            String boundsMsg = ViewUtils.toString(new Rectangle(x, y, w, h));
            ViewHierarchyImpl.REPAINT_LOG.log(Level.FINER, "Component-REPAINT: " + boundsMsg + " c:" + ViewUtils.toString(c), new Exception("Component-Repaint of " + boundsMsg + " cause:"));
            break;
        }
        super.addDirtyRegion(c, x, y, w, h);
    }
}

