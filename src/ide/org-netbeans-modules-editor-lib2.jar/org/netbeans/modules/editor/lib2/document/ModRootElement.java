/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.GapList
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.document.AbstractRootElement;
import org.netbeans.modules.editor.lib2.document.ModElement;

public final class ModRootElement
extends AbstractRootElement<ModElement>
implements DocumentListener {
    static final Logger LOG = Logger.getLogger(ModRootElement.class.getName());
    public static final String NAME = "mods";
    CharSequence docText;
    private int lastModElementIndex;
    private boolean enabled;

    public static ModRootElement get(Document doc) {
        return (ModRootElement)doc.getProperty("mods");
    }

    public ModRootElement(Document doc) {
        super(doc);
        this.docText = DocumentUtilities.getText((Document)doc);
        doc.putProperty("mods", this);
    }

    @Override
    public String getName() {
        return "mods";
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void resetMods(UndoableEdit compoundEdit) {
        ResetModsEdit edit = new ResetModsEdit();
        if (compoundEdit != null) {
            compoundEdit.addEdit(edit);
        }
        edit.run();
    }

    GapList<ModElement> emptyMods() {
        return new GapList(4);
    }

    @Override
    public void insertUpdate(DocumentEvent evt) {
        if (this.enabled) {
            UndoableEdit compoundEdit = (UndoableEdit)((Object)evt);
            int offset = evt.getOffset();
            int length = evt.getLength();
            boolean covered = false;
            if (this.lastModElementIndex >= 0 && this.lastModElementIndex < this.children.size()) {
                covered = this.isCovered(offset, length);
            }
            if (!covered) {
                this.lastModElementIndex = this.findModElementIndex(offset, false);
                if (this.lastModElementIndex >= 0) {
                    covered = this.isCovered(offset, length);
                }
            }
            if (!covered) {
                this.addModElement(compoundEdit, offset, offset + length);
            }
        }
    }

    @Override
    public void removeUpdate(DocumentEvent evt) {
    }

    @Override
    public void changedUpdate(DocumentEvent evt) {
    }

    private boolean isCovered(int offset, int length) {
        ModElement modElem = (ModElement)this.children.get(this.lastModElementIndex);
        if (modElem.getStartOffset() <= offset && offset + length <= modElem.getEndOffset()) {
            return true;
        }
        return false;
    }

    private ModElement addModElement(UndoableEdit compoundEdit, int startOffset, int endOffset) {
        ModElement modElement = new ModElement((Element)this, startOffset, endOffset);
        this.lastModElementIndex = this.findModElementIndex(startOffset, true);
        AddModElementEdit edit = new AddModElementEdit(this.lastModElementIndex, modElement);
        edit.run();
        compoundEdit.addEdit(edit);
        return modElement;
    }

    void addModElement(int index, ModElement modElem) {
        this.children.add(index, (Object)modElem);
    }

    private int findModElementIndex(int offset, boolean forInsert) {
        int low = 0;
        int high = this.children.size() - 1;
        while (low <= high) {
            int mid = low + high >>> 1;
            int midStartOffset = ((ModElement)this.children.get(mid)).getStartOffset();
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            while (++mid < this.children.size() && ((ModElement)this.children.get(mid)).getStartOffset() == offset) {
            }
            --mid;
            if (forInsert) {
                low = mid + 1;
                break;
            }
            high = mid;
            break;
        }
        return forInsert ? low : high;
    }

    GapList<ModElement> getModList() {
        return this.children;
    }

    public void checkConsistency() {
        int lastOffset = 0;
        for (int i = 0; i < this.children.size(); ++i) {
            ModElement modElem = (ModElement)this.children.get(i);
            int offset = modElem.getStartOffset();
            if (offset < lastOffset) {
                throw new IllegalStateException("modElement[" + i + "].getStartOffset()=" + offset + " < lastOffset=" + lastOffset);
            }
            lastOffset = offset;
            offset = modElem.getEndOffset();
            if (offset < lastOffset) {
                throw new IllegalStateException("modElement[" + i + "].getEndOffset()=" + offset + " < modElement.getStartOffset()=" + lastOffset);
            }
            lastOffset = offset;
        }
    }

    @Override
    public String toString() {
        int size = this.children.size();
        int digitCount = String.valueOf(size).length();
        StringBuilder sb = new StringBuilder(100);
        for (int i = 0; i < size; ++i) {
            ModElement modElem = (ModElement)this.children.get(i);
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            sb.append(modElem);
            sb.append('\n');
        }
        return sb.toString();
    }

    private final class ResetModsEdit
    extends AbstractUndoableEdit {
        private GapList<ModElement> oldModRegions;
        private GapList<ModElement> newModRegions;

        public ResetModsEdit() {
            this.oldModRegions = ModRootElement.this.getModList();
            this.newModRegions = ModRootElement.this.emptyMods();
        }

        public void run() {
            if (ModRootElement.LOG.isLoggable(Level.FINE)) {
                ModRootElement.LOG.fine("Abandoning old regions\n" + (Object)ModRootElement.this.children);
            }
            ModRootElement.this.children = this.newModRegions;
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            ModRootElement.this.children = this.oldModRegions;
            if (ModRootElement.LOG.isLoggable(Level.FINE)) {
                ModRootElement.LOG.fine("Restored old regions\n" + (Object)ModRootElement.this.children);
            }
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            this.run();
        }
    }

    private final class AddModElementEdit
    extends AbstractUndoableEdit {
        private int index;
        private ModElement modElement;

        public AddModElementEdit(int index, ModElement modElem) {
            this.index = index;
            this.modElement = modElem;
        }

        public void run() {
            ModRootElement.this.addModElement(this.index, this.modElement);
            if (ModRootElement.LOG.isLoggable(Level.FINE)) {
                ModRootElement.LOG.fine("Added modElement " + this.modElement + " at index=" + this.index + '\n');
                ModRootElement.LOG.fine("ModElements:\n" + (Object)ModRootElement.this.children + '\n');
            }
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            if (this.index >= ModRootElement.this.children.size() || ModRootElement.this.children.get(this.index) != this.modElement) {
                this.index = ModRootElement.this.findModElementIndex(this.modElement.getStartOffset(), false);
            }
            if (this.index >= 0 && ModRootElement.this.children.get(this.index) == this.modElement) {
                ModRootElement.this.children.remove(this.index);
            } else {
                ModRootElement.this.children.remove((Object)this.modElement);
            }
            if (ModRootElement.LOG.isLoggable(Level.FINE)) {
                ModRootElement.LOG.fine("Removed modElement " + this.modElement + " at index=" + this.index + '\n');
                ModRootElement.LOG.fine("ModElements:\n" + (Object)ModRootElement.this.children + '\n');
            }
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            this.index = ModRootElement.this.findModElementIndex(this.modElement.getStartOffset(), true);
            this.run();
        }
    }

}

