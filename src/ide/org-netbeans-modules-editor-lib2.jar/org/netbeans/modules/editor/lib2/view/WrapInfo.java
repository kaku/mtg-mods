/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.JTextComponent;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.PaintState;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewPart;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.modules.editor.lib2.view.WrapInfoUpdater;
import org.netbeans.modules.editor.lib2.view.WrapLine;

final class WrapInfo
extends GapList<WrapLine> {
    private static final Logger LOG = Logger.getLogger(WrapInfo.class.getName());
    private static final long serialVersionUID = 0;
    float width;
    WrapInfoUpdater updater;

    WrapInfo() {
        super(2);
    }

    int wrapLineCount() {
        return this.size();
    }

    float wrapLineHeight(ParagraphViewChildren children) {
        return children.childrenHeight();
    }

    float height(ParagraphViewChildren children) {
        return (float)this.size() * this.wrapLineHeight(children);
    }

    float width() {
        return this.width;
    }

    void setWidth(float width) {
        this.width = width;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void paintWrapLines(ParagraphViewChildren children, ParagraphView pView, int startIndex, int endIndex, Graphics2D g, Shape alloc, Rectangle clipBounds) {
        DocumentView docView = pView.getDocumentView();
        if (docView == null) {
            return;
        }
        JTextComponent textComponent = docView.getTextComponent();
        TextLayout lineContinuationTextLayout = docView.op.getLineContinuationCharTextLayout();
        Rectangle2D.Double allocBounds = ViewUtils.shape2Bounds(alloc);
        float wrapLineHeight = this.wrapLineHeight(children);
        double allocOrigX = allocBounds.x;
        allocBounds.y += (double)((float)startIndex * wrapLineHeight);
        allocBounds.height = wrapLineHeight;
        int lastWrapLineIndex = this.size() - 1;
        for (int i = startIndex; i < endIndex; ++i) {
            ViewPart endPart;
            WrapLine wrapLine = (WrapLine)this.get(i);
            ViewPart startPart = wrapLine.startPart;
            if (startPart != null) {
                allocBounds.width = startPart.width;
                startPart.view.paint(g, allocBounds, clipBounds);
                allocBounds.x += (double)startPart.width;
            }
            if (wrapLine.hasFullViews()) {
                double visualOffset = children.startVisualOffset(wrapLine.firstViewIndex);
                assert (wrapLine.endViewIndex <= children.size());
                allocBounds.x -= visualOffset;
                children.paintChildren(pView, g, allocBounds, clipBounds, wrapLine.firstViewIndex, wrapLine.endViewIndex);
                allocBounds.x += children.startVisualOffset(wrapLine.endViewIndex);
            }
            if ((endPart = wrapLine.endPart) != null) {
                allocBounds.width = endPart.width;
                endPart.view.paint(g, allocBounds, clipBounds);
                allocBounds.x += (double)endPart.width;
            }
            if (i != lastWrapLineIndex) {
                PaintState paintState = PaintState.save(g);
                try {
                    ViewUtils.applyForegroundColor(g, null, textComponent);
                    HighlightsViewUtils.paintTextLayout(g, allocBounds, lineContinuationTextLayout, docView);
                }
                finally {
                    paintState.restore();
                }
            }
            allocBounds.x = allocOrigX;
            allocBounds.y += (double)wrapLineHeight;
        }
    }

    public void checkIntegrity(ParagraphView paragraphView) {
        String err;
        if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINE) && (err = this.findIntegrityError(paragraphView)) != null) {
            String msg = "WrapInfo INTEGRITY ERROR! - " + err;
            ViewHierarchyImpl.CHECK_LOG.finer(msg + "\n");
            ViewHierarchyImpl.CHECK_LOG.finer(this.toString(paragraphView));
            if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINEST)) {
                throw new IllegalStateException(msg);
            }
            ViewHierarchyImpl.CHECK_LOG.log(Level.INFO, msg, new Exception());
        }
    }

    public String findIntegrityError(ParagraphView paragraphView) {
        String err = null;
        int lastOffset = paragraphView.getStartOffset();
        for (int i = 0; i < this.size(); ++i) {
            ViewPart endPart;
            int startViewIndex;
            int endViewIndex;
            WrapLine wrapLine = (WrapLine)this.get(i);
            ViewPart startPart = wrapLine.startPart;
            boolean nonEmptyLine = false;
            if (startPart != null) {
                nonEmptyLine = true;
                int startPartOffset = startPart.view.getStartOffset();
                if (startPartOffset != lastOffset) {
                    err = "startViewPart.getStartOffset()=" + startPartOffset + " != lastOffset=" + lastOffset;
                }
                lastOffset = startPart.view.getEndOffset();
            }
            if ((startViewIndex = wrapLine.firstViewIndex) != (endViewIndex = wrapLine.endViewIndex)) {
                nonEmptyLine = true;
                boolean validIndices = true;
                if (startViewIndex < 0) {
                    validIndices = false;
                    if (err == null) {
                        err = "startViewIndex=" + startViewIndex + " < 0; endViewIndex=" + endViewIndex;
                    }
                }
                if (endViewIndex < startViewIndex) {
                    validIndices = false;
                    if (err == null) {
                        err = "endViewIndex=" + endViewIndex + " < startViewIndex=" + startViewIndex;
                    }
                }
                if (endViewIndex > paragraphView.getViewCount()) {
                    validIndices = false;
                    if (err == null) {
                        err = "endViewIndex=" + endViewIndex + " > getViewCount()=" + paragraphView.getViewCount();
                    }
                }
                if (validIndices) {
                    EditorView childView = paragraphView.getEditorView(startViewIndex);
                    if (err == null && childView.getStartOffset() != lastOffset) {
                        err = "startChildView.getStartOffset()=" + childView.getStartOffset() + " != lastOffset=" + lastOffset;
                    }
                    childView = paragraphView.getEditorView(endViewIndex - 1);
                    lastOffset = childView.getEndOffset();
                }
            }
            if ((endPart = wrapLine.endPart) != null) {
                nonEmptyLine = true;
                int endPartOffset = endPart.view.getStartOffset();
                if (err == null && lastOffset != endPartOffset) {
                    err = "endViewPart.getStartOffset()=" + endPartOffset + " != lastOffset=" + lastOffset;
                }
                lastOffset = endPart.view.getEndOffset();
            }
            if (!nonEmptyLine && err == null) {
                err = "Empty";
            }
            if (err == null) continue;
            err = "WrapLine[" + i + "]: " + err;
            break;
        }
        if (err == null && lastOffset != paragraphView.getEndOffset()) {
            err = "Not all offsets covered: lastOffset=" + lastOffset + " != parEndOffset=" + paragraphView.getEndOffset();
        }
        return err;
    }

    String dumpWrapLine(ParagraphView pView, int wrapLineIndex) {
        return "Invalid wrapLine[" + wrapLineIndex + "]:\n" + this.toString(pView);
    }

    public String appendInfo(StringBuilder sb, ParagraphView paragraphView, int indent) {
        int wrapLineCount = this.size();
        int digitCount = ArrayUtilities.digitCount((int)wrapLineCount);
        for (int i = 0; i < wrapLineCount; ++i) {
            sb.append('\n');
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)(indent + 4));
            sb.append("WL");
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            WrapLine wrapLine = (WrapLine)this.get(i);
            sb.append("SV:");
            ViewPart startPart = wrapLine.startPart;
            if (startPart != null) {
                sb.append("<").append(startPart.view.getStartOffset()).append(",");
                sb.append(startPart.view.getEndOffset()).append(">");
            } else {
                sb.append("NULL");
            }
            sb.append("; x=").append(wrapLine.startPartWidth());
            int startViewIndex = wrapLine.firstViewIndex;
            int endViewIndex = wrapLine.endViewIndex;
            sb.append(" [").append(startViewIndex).append(",");
            sb.append(endViewIndex).append("] ");
            if (paragraphView != null && startViewIndex != endViewIndex) {
                if (startViewIndex > endViewIndex) {
                    sb.append("ERROR!!! startViewIndex=").append(startViewIndex);
                    sb.append(" > endViewIndex=").append(endViewIndex);
                } else {
                    int childCount = paragraphView.getViewCount();
                    if (startViewIndex == childCount) {
                        sb.append("<").append(paragraphView.getEndOffset()).append(">");
                    } else {
                        if (startViewIndex <= childCount) {
                            EditorView startChild = paragraphView.getEditorView(startViewIndex);
                            sb.append("<").append(startChild.getStartOffset());
                        } else {
                            sb.append("<invalid-index=" + startViewIndex);
                        }
                        sb.append(",");
                        if (endViewIndex <= childCount) {
                            EditorView lastChild = paragraphView.getEditorView(endViewIndex - 1);
                            sb.append(lastChild.getEndOffset());
                        } else {
                            sb.append("invalid-index=").append(endViewIndex);
                        }
                        sb.append("> ");
                    }
                }
            }
            sb.append("EV:");
            ViewPart endViewPart = wrapLine.endPart;
            if (endViewPart != null) {
                sb.append("<").append(endViewPart.view.getStartOffset()).append(",");
                sb.append(endViewPart.view.getEndOffset()).append(">");
                continue;
            }
            sb.append("NULL");
        }
        return sb.toString();
    }

    public String toString(ParagraphView paragraphView) {
        return this.appendInfo(new StringBuilder(200), paragraphView, 0);
    }

    public String toString() {
        return this.toString(null);
    }
}

