/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.actions;

import javax.swing.Action;
import javax.swing.event.ChangeListener;

public interface SearchableEditorKit {
    public Action getAction(String var1);

    public void addActionsChangeListener(ChangeListener var1);

    public void removeActionsChangeListener(ChangeListener var1);
}

