/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.annotations.common.NonNull;

public interface EditorDocumentServices {
    public void runExclusive(@NonNull Document var1, @NonNull Runnable var2);

    public void resetUndoMerge(@NonNull Document var1);

    public UndoableEdit startOnSaveTasks(@NonNull Document var1);

    public void endOnSaveTasks(@NonNull Document var1, boolean var2);
}

