/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.editor.lib2.typinghooks;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.lib2.typinghooks.DeletedTextInterceptorsManager;
import org.netbeans.modules.editor.lib2.typinghooks.TypingHooksSpiAccessor;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;

public final class TypedTextInterceptorsManager {
    private static final Logger LOG = Logger.getLogger(TypedTextInterceptorsManager.class.getName());
    private static TypedTextInterceptorsManager instance;
    private Transaction transaction = null;
    private final Map<MimePath, Reference<Collection<TypedTextInterceptor>>> cache = new WeakHashMap<MimePath, Reference<Collection<TypedTextInterceptor>>>();

    public static TypedTextInterceptorsManager getInstance() {
        if (instance == null) {
            instance = new TypedTextInterceptorsManager();
        }
        return instance;
    }

    public Transaction openTransaction(JTextComponent c, Position offset, String typedText, String replacedText) {
        TypedTextInterceptorsManager typedTextInterceptorsManager = this;
        synchronized (typedTextInterceptorsManager) {
            if (this.transaction == null) {
                this.transaction = new Transaction(c, offset, typedText, replacedText);
                return this.transaction;
            }
            throw new IllegalStateException("Too many transactions; only one at a time is allowed!");
        }
    }

    private TypedTextInterceptorsManager() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<? extends TypedTextInterceptor> getInterceptors(Document doc, Position offset) {
        MimePath mimePath = DeletedTextInterceptorsManager.getMimePath(doc, offset.getOffset());
        Map<MimePath, Reference<Collection<TypedTextInterceptor>>> map = this.cache;
        synchronized (map) {
            Collection<TypedTextInterceptor> interceptors;
            Reference<Collection<TypedTextInterceptor>> ref = this.cache.get((Object)mimePath);
            Collection<TypedTextInterceptor> collection = interceptors = ref == null ? null : ref.get();
            if (interceptors == null) {
                Collection factories = MimeLookup.getLookup((MimePath)mimePath).lookupAll(TypedTextInterceptor.Factory.class);
                interceptors = new HashSet<TypedTextInterceptor>(factories.size());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "TypedTextInterceptor.Factory instances for {0}:", mimePath.getPath());
                }
                for (TypedTextInterceptor.Factory f : factories) {
                    TypedTextInterceptor interceptor = f.createTypedTextInterceptor(mimePath);
                    if (interceptor != null) {
                        interceptors.add(interceptor);
                    }
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.log(Level.FINE, "    {0} created: {1}", new Object[]{f, interceptor});
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("");
                }
                this.cache.put(mimePath, new SoftReference<Collection<TypedTextInterceptor>>(interceptors));
            }
            return interceptors;
        }
    }

    public final class Transaction {
        private final TypedTextInterceptor.MutableContext context;
        private final Collection<? extends TypedTextInterceptor> interceptors;
        private int phase;

        public boolean beforeInsertion() {
            for (TypedTextInterceptor i : this.interceptors) {
                try {
                    if (!i.beforeInsert(this.context)) continue;
                    return true;
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "TypedTextInterceptor crashed in beforeInsert(): " + i, e);
                    continue;
                }
            }
            ++this.phase;
            return false;
        }

        public Object[] textTyped() {
            Object[] data = null;
            for (TypedTextInterceptor i : this.interceptors) {
                try {
                    i.insert(this.context);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "TypedTextInterceptor crashed in insert(): " + i, e);
                    TypingHooksSpiAccessor.get().resetTtiContextData(this.context);
                    continue;
                }
                data = TypingHooksSpiAccessor.get().getTtiContextData(this.context);
                if (data == null) continue;
                break;
            }
            ++this.phase;
            return data;
        }

        public void afterInsertion() {
            for (TypedTextInterceptor i : this.interceptors) {
                try {
                    i.afterInsert(this.context);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "TypedTextInterceptor crashed in afterInsert(): " + i, e);
                }
            }
            ++this.phase;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void close() {
            if (this.phase < 3) {
                for (TypedTextInterceptor i : this.interceptors) {
                    try {
                        i.cancelled(this.context);
                    }
                    catch (Exception e) {
                        LOG.log(Level.INFO, "TypedTextInterceptor crashed in cancelled(): " + i, e);
                    }
                }
            }
            TypedTextInterceptorsManager i$ = TypedTextInterceptorsManager.this;
            synchronized (i$) {
                TypedTextInterceptorsManager.this.transaction = null;
            }
        }

        private Transaction(JTextComponent c, Position offset, String typedText, String replacedText) {
            this.phase = 0;
            this.context = TypingHooksSpiAccessor.get().createTtiContext(c, offset, typedText, replacedText);
            this.interceptors = TypedTextInterceptorsManager.this.getInterceptors(c.getDocument(), offset);
        }
    }

}

