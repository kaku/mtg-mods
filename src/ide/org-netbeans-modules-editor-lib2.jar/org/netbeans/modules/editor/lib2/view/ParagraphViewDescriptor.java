/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Shape;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.ParagraphView;

public final class ParagraphViewDescriptor {
    private final DocumentView docView;
    private final int pViewIndex;

    ParagraphViewDescriptor(DocumentView docView, int pViewIndex) {
        this.docView = docView;
        this.pViewIndex = pViewIndex;
    }

    public int getStartOffset() {
        return this.docView.getParagraphView(this.pViewIndex).getStartOffset();
    }

    public int getLength() {
        return this.docView.getParagraphView(this.pViewIndex).getLength();
    }

    public Shape getAllocation() {
        return this.docView.getChildAllocation(this.pViewIndex);
    }

    public float getAscent() {
        return this.docView.op.getDefaultAscent();
    }
}

