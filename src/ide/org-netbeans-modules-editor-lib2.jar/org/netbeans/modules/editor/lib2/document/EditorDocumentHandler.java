/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import javax.swing.undo.UndoableEdit;
import org.netbeans.modules.editor.lib2.document.DefaultEditorCharacterServices;
import org.netbeans.modules.editor.lib2.document.EditorCharacterServices;
import org.netbeans.modules.editor.lib2.document.EditorDocumentServices;

public final class EditorDocumentHandler {
    private static Class editorDocClass;
    private static EditorDocumentServices editorDocServices;
    private static EditorCharacterServices charServices;

    private EditorDocumentHandler() {
    }

    public static void setEditorDocumentServices(Class docClass, EditorDocumentServices docServices) {
        if (editorDocClass != null) {
            throw new IllegalStateException("Only single registration expected. Already registered: " + editorDocClass);
        }
        editorDocClass = docClass;
        editorDocServices = docServices;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void runExclusive(Document doc, Runnable r) {
        if (editorDocClass != null && editorDocClass.isInstance(doc)) {
            editorDocServices.runExclusive(doc, r);
        } else {
            Document document = doc;
            synchronized (document) {
                r.run();
            }
        }
    }

    public static void resetUndoMerge(Document doc) {
        if (editorDocClass != null && editorDocClass.isInstance(doc)) {
            editorDocServices.resetUndoMerge(doc);
        }
    }

    public static UndoableEdit startOnSaveTasks(Document doc) {
        if (editorDocClass != null && editorDocClass.isInstance(doc)) {
            return editorDocServices.startOnSaveTasks(doc);
        }
        return null;
    }

    public static void endOnSaveTasks(Document doc, boolean success) {
        if (editorDocClass != null && editorDocClass.isInstance(doc)) {
            editorDocServices.endOnSaveTasks(doc, success);
        }
    }

    public static int getIdentifierEnd(Document doc, int offset, boolean backward) {
        return charServices.getIdentifierEnd(doc, offset, backward);
    }

    static {
        charServices = new DefaultEditorCharacterServices();
    }
}

