/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import javax.swing.text.Element;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.editor.lib2.document.AbstractPositionElement;
import org.netbeans.modules.editor.lib2.document.AbstractRootElement;

public class DocumentInternalUtils {
    private DocumentInternalUtils() {
    }

    public static Element customElement(Document doc, int startOffset, int endOffset) {
        return new CustomRootElement(doc, startOffset, endOffset);
    }

    private static final class CustomRootElement
    extends AbstractRootElement<CustomElement> {
        private final CustomElement customElement;

        public CustomRootElement(Document doc, int startOffset, int endOffset) {
            super(doc);
            this.customElement = new CustomElement(this, startOffset, endOffset);
        }

        @Override
        public String getName() {
            return "CustomRootElement";
        }

        @Override
        public Element getElement(int index) {
            if (index == 0) {
                return this.customElement;
            }
            return null;
        }

        @Override
        public int getElementCount() {
            return 1;
        }
    }

    private static final class CustomElement
    extends AbstractPositionElement {
        CustomElement(Element parent, int startOffset, int endOffset) {
            super(parent, startOffset, endOffset);
            CharSequenceUtilities.checkIndexesValid((int)startOffset, (int)endOffset, (int)(parent.getDocument().getLength() + 1));
        }

        @Override
        public String getName() {
            return "CustomElement";
        }
    }

}

