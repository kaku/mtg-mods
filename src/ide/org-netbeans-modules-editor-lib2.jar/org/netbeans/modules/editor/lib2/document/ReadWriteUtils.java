/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import org.netbeans.modules.editor.lib2.document.ReadWriteBuffer;

public final class ReadWriteUtils {
    private static final int INITIAL_BUFFER_SIZE = 4096;

    private ReadWriteUtils() {
    }

    public static ReadWriteBuffer read(Reader r) throws IOException {
        int readLen;
        char[] text = new char[4096];
        int length = 0;
        while ((readLen = r.read(text, length, text.length - length)) != -1) {
            if ((length += readLen) != text.length) continue;
            char[] tmp = new char[text.length << 1];
            System.arraycopy(text, 0, tmp, 0, length);
            text = tmp;
        }
        return new ReadWriteBuffer(text, length);
    }

    public static String findFirstLineSeparator(ReadWriteBuffer buffer) {
        for (int i = 0; i < buffer.length(); ++i) {
            switch (buffer.text[i]) {
                case '\n': {
                    return "\n";
                }
                case '\r': {
                    if (i + 1 < buffer.length && buffer.text[i + 1] == '\n') {
                        return "\r\n";
                    }
                    return "\r";
                }
            }
        }
        return null;
    }

    public static String getSystemLineSeparator() {
        return System.getProperty("line.separator");
    }

    public static void convertToNewlines(ReadWriteBuffer buffer) {
        char[] text = buffer.text;
        int len = buffer.length;
        int j = 0;
        for (int i = 0; i < len; ++i) {
            int ch = text[i];
            if (ch == 13) {
                if (i + 1 < len && text[i + 1] == '\n') {
                    ++i;
                }
                ch = 10;
            }
            text[j++] = ch;
        }
        buffer.length = j;
    }

    public static String convertToNewlines(CharSequence text) {
        int textLen = text.length();
        for (int i = 0; i < textLen; ++i) {
            int j;
            int ch = text.charAt(i);
            if (ch != 13) continue;
            char[] output = new char[textLen];
            for (j = 0; j < i; ++j) {
                output[j] = text.charAt(j);
            }
            output[j++] = 10;
            if (++i < textLen && text.charAt(i) == '\n') {
                ++i;
            }
            while (i < textLen) {
                ch = text.charAt(i);
                if (ch == 13) {
                    if (i + 1 < textLen && text.charAt(i + 1) == '\n') {
                        ++i;
                    }
                    ch = 10;
                }
                output[j++] = ch;
                ++i;
            }
            return new String(output, 0, j);
        }
        return text.toString();
    }

    public static ReadWriteBuffer convertFromNewlines(CharSequence text, String lineSeparator) {
        return ReadWriteUtils.convertFromNewlines(text, 0, text.length(), lineSeparator);
    }

    public static ReadWriteBuffer convertFromNewlines(CharSequence text, int start, int end, String lineSeparator) {
        char[] output;
        int j;
        int textLen = end - start;
        if ("\r\n".equals(lineSeparator)) {
            output = new char[textLen + (textLen >>> 3) + 4];
            int outputSafeLen = output.length - 2;
            for (int i = 0; i < textLen; ++i) {
                char ch;
                if (j >= outputSafeLen) {
                    char[] tmp = new char[output.length + (output.length >>> 1) + 4];
                    System.arraycopy(output, 0, tmp, 0, j);
                    output = tmp;
                    outputSafeLen = output.length - 2;
                }
                if ((ch = text.charAt(start + i)) == '\n') {
                    output[j++] = 13;
                }
                output[j++] = ch;
            }
        } else {
            output = new char[textLen];
            if ("\r".equals(lineSeparator)) {
                for (j = 0; j < textLen; ++j) {
                    int ch = text.charAt(start + j);
                    if (ch == 10) {
                        ch = 13;
                    }
                    output[j] = ch;
                }
            } else {
                while (j < textLen) {
                    output[j] = text.charAt(start + j);
                    ++j;
                }
            }
        }
        return new ReadWriteBuffer(output, j);
    }

    public static void write(Writer w, ReadWriteBuffer buffer) throws IOException {
        w.write(buffer.text(), 0, buffer.length());
    }
}

