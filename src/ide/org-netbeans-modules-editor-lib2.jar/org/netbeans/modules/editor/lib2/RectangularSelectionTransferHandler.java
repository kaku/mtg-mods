/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.lib2;

import java.awt.Component;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.DocUtils;
import org.netbeans.modules.editor.lib2.RectangularSelectionUtils;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class RectangularSelectionTransferHandler
extends TransferHandler {
    private static final DataFlavor RECTANGULAR_SELECTION_FLAVOR = new DataFlavor(RectangularSelectionData.class, NbBundle.getMessage(RectangularSelectionTransferHandler.class, (String)"MSG_RectangularSelectionClipboardFlavor"));
    private static final String RECTANGULAR_SELECTION_PROPERTY = "rectangular-selection";
    private static final Logger LOG = Logger.getLogger(RectangularSelectionTransferHandler.class.getName());
    private final TransferHandler delegate;

    public static void install(JTextComponent c) {
        TransferHandler origHandler = c.getTransferHandler();
        if (!(origHandler instanceof RectangularSelectionTransferHandler)) {
            c.setTransferHandler(new RectangularSelectionTransferHandler(c.getTransferHandler()));
        }
    }

    public static void uninstall(JTextComponent c) {
        TransferHandler origHandler = c.getTransferHandler();
        if (origHandler instanceof RectangularSelectionTransferHandler) {
            c.setTransferHandler(((RectangularSelectionTransferHandler)origHandler).getDelegate());
        }
    }

    public RectangularSelectionTransferHandler(TransferHandler delegate) {
        this.delegate = delegate;
    }

    TransferHandler getDelegate() {
        return this.delegate;
    }

    @Override
    public boolean canImport(TransferHandler.TransferSupport support) {
        return this.delegate.canImport(support);
    }

    @Override
    public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
        return this.delegate.canImport(comp, transferFlavors);
    }

    @Override
    protected Transferable createTransferable(JComponent c) {
        try {
            Method method = this.delegate.getClass().getDeclaredMethod("createTransferable", JComponent.class);
            method.setAccessible(true);
            return (Transferable)method.invoke(this.delegate, c);
        }
        catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void exportAsDrag(JComponent comp, InputEvent e, int action) {
        this.delegate.exportAsDrag(comp, e, action);
    }

    @Override
    protected void exportDone(JComponent source, Transferable data, int action) {
        try {
            Method method = this.delegate.getClass().getDeclaredMethod("exportDone", JComponent.class, Transferable.class, Integer.TYPE);
            method.setAccessible(true);
            method.invoke(this.delegate, source, data, new Integer(action));
        }
        catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void exportToClipboard(JComponent c, Clipboard clip, int action) throws IllegalStateException {
        List<Position> regions;
        if (c instanceof JTextComponent && Boolean.TRUE.equals(c.getClientProperty("rectangular-selection")) && (regions = RectangularSelectionUtils.regionsCopy(c)) != null) {
            String[] data;
            StringBuilder stringSelectionBuffer;
            AbstractDocument doc;
            JTextComponent tc = (JTextComponent)c;
            doc = (AbstractDocument)tc.getDocument();
            doc.readLock();
            try {
                CharSequence docText = DocumentUtilities.getText((Document)doc);
                stringSelectionBuffer = new StringBuilder(100);
                int size = regions.size();
                data = new String[size >>> 1];
                for (int i = 0; i < size; ++i) {
                    Position startPos = regions.get(i++);
                    Position endPos = regions.get(i);
                    CharSequence lineSel = docText.subSequence(startPos.getOffset(), endPos.getOffset());
                    int halfI = i >>> 1;
                    if (halfI != 0) {
                        stringSelectionBuffer.append('\n');
                    }
                    stringSelectionBuffer.append(lineSel);
                    data[halfI] = lineSel.toString();
                }
            }
            finally {
                doc.readUnlock();
            }
            clip.setContents(new WrappedTransferable(new StringSelection(stringSelectionBuffer.toString()), new RectangularSelectionData(data)), null);
            if (action == 2) {
                try {
                    RectangularSelectionUtils.removeSelection(doc, regions);
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            return;
        }
        this.delegate.exportToClipboard(c, clip, action);
    }

    @Override
    public int getSourceActions(JComponent c) {
        return this.delegate.getSourceActions(c);
    }

    @Override
    public Icon getVisualRepresentation(Transferable t) {
        return this.delegate.getVisualRepresentation(t);
    }

    @Override
    public boolean importData(TransferHandler.TransferSupport support) {
        Component c = support.getComponent();
        Transferable t = support.getTransferable();
        if (c instanceof JTextComponent) {
            JTextComponent tc = (JTextComponent)c;
            if (t.isDataFlavorSupported(RECTANGULAR_SELECTION_FLAVOR) && c instanceof JTextComponent) {
                boolean result = false;
                try {
                    if (Boolean.TRUE.equals(tc.getClientProperty("rectangular-selection"))) {
                        final RectangularSelectionData data = (RectangularSelectionData)t.getTransferData(RECTANGULAR_SELECTION_FLAVOR);
                        final List<Position> regions = RectangularSelectionUtils.regionsCopy(tc);
                        final Document doc = tc.getDocument();
                        DocUtils.runAtomicAsUser(doc, new Runnable(){

                            @Override
                            public void run() {
                                try {
                                    int doubleI;
                                    RectangularSelectionUtils.removeSelection(doc, regions);
                                    String[] strings = data.strings();
                                    for (int i = 0; i < strings.length && (doubleI = i << 1) < regions.size(); ++i) {
                                        Position linePos = (Position)regions.get(doubleI);
                                        doc.insertString(linePos.getOffset(), strings[i], null);
                                    }
                                }
                                catch (BadLocationException ex) {
                                    // empty catch block
                                }
                            }
                        });
                    } else {
                        String s = (String)t.getTransferData(DataFlavor.stringFlavor);
                        if (s != null) {
                            tc.replaceSelection("");
                        }
                    }
                    result = true;
                }
                catch (UnsupportedFlavorException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                return result;
            }
            if (RectangularSelectionUtils.isRectangularSelection(tc) && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    String s = (String)t.getTransferData(DataFlavor.stringFlavor);
                    final List<String> dataLines = RectangularSelectionTransferHandler.splitByLines(s);
                    final List<Position> regions = RectangularSelectionUtils.regionsCopy(tc);
                    final Document doc = tc.getDocument();
                    final int dataLinesSize = dataLines.size();
                    final int regionsSize = regions.size();
                    if (dataLinesSize > 0 && regionsSize > 0) {
                        DocUtils.runAtomicAsUser(doc, new Runnable(){

                            @Override
                            public void run() {
                                try {
                                    RectangularSelectionUtils.removeSelection(doc, regions);
                                    int dataLineIndex = 0;
                                    for (int i = 0; i < regionsSize; i += 2) {
                                        Position linePos = (Position)regions.get(i);
                                        doc.insertString(linePos.getOffset(), (String)dataLines.get(dataLineIndex++), null);
                                        if (dataLineIndex < dataLinesSize) continue;
                                        dataLineIndex = 0;
                                    }
                                }
                                catch (BadLocationException ex) {
                                    // empty catch block
                                }
                            }
                        });
                        return true;
                    }
                    return false;
                }
                catch (UnsupportedFlavorException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        return this.delegate.importData(support);
    }

    private static List<String> splitByLines(String s) {
        StringTokenizer splitByLines = new StringTokenizer(s, "\n", false);
        ArrayList<String> lines = new ArrayList<String>();
        while (splitByLines.hasMoreTokens()) {
            lines.add(splitByLines.nextToken());
        }
        return lines;
    }

    public static final class RectangularSelectionData {
        private final String[] strings;

        public RectangularSelectionData(String[] strings) {
            this.strings = strings;
        }

        public String[] strings() {
            return this.strings;
        }
    }

    private static final class WrappedTransferable
    implements Transferable {
        private final Transferable delegate;
        private final RectangularSelectionData rectangularSelectionData;
        private DataFlavor[] transferDataFlavorsCache;

        public WrappedTransferable(Transferable delegate, RectangularSelectionData rectangularSelectionData) {
            this.delegate = delegate;
            this.rectangularSelectionData = rectangularSelectionData;
        }

        @Override
        public synchronized DataFlavor[] getTransferDataFlavors() {
            if (this.transferDataFlavorsCache != null) {
                return this.transferDataFlavorsCache;
            }
            DataFlavor[] flavors = this.delegate.getTransferDataFlavors();
            DataFlavor[] result = Arrays.copyOf(flavors, flavors.length + 1);
            result[flavors.length] = RECTANGULAR_SELECTION_FLAVOR;
            this.transferDataFlavorsCache = result;
            return this.transferDataFlavorsCache;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return RECTANGULAR_SELECTION_FLAVOR.equals(flavor) || this.delegate.isDataFlavorSupported(flavor);
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (RECTANGULAR_SELECTION_FLAVOR.equals(flavor)) {
                return this.rectangularSelectionData;
            }
            return this.delegate.getTransferData(flavor);
        }
    }

}

