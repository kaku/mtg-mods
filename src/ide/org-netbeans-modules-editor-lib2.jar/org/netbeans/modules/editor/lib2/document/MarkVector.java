/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.document.CharContent;
import org.netbeans.modules.editor.lib2.document.EditorDocumentContent;
import org.netbeans.modules.editor.lib2.document.EditorPosition;
import org.netbeans.modules.editor.lib2.document.Mark;

final class MarkVector {
    private static final Logger LOG = Logger.getLogger(EditorDocumentContent.class.getName());
    private static final int INITIAL_OFFSET_GAP_SIZE = 536870911;
    private Mark[] markArray;
    private final boolean backwardBiasMarks;
    private int gapStart;
    private int gapLength;
    private int offsetGapStart;
    private int offsetGapLength;
    private final Object lock;
    private int disposedMarkCount;
    private static final EditorPosition zeroPos = new EditorPosition();
    private static final Mark zeroMark = new Mark(null, 0, zeroPos);

    MarkVector(Object lock, boolean backwardBias) {
        this.lock = lock;
        this.backwardBiasMarks = backwardBias;
        this.gapLength = 2;
        this.markArray = new Mark[2];
        this.offsetGapStart = 1;
        this.offsetGapLength = 536870911;
    }

    boolean isBackwardBiasMarks() {
        return this.backwardBiasMarks;
    }

    public EditorPosition position(int offset) {
        if (offset == 0) {
            return zeroPos;
        }
        int low = 0;
        int markCount = this.markCount();
        int high = markCount - 1;
        int rawOffset = this.rawOffset(offset);
        if (!this.backwardBiasMarks) {
            EditorPosition pos;
            Mark mark;
            while (low <= high) {
                int mid = low + high >>> 1;
                if (this.getMark(mid).rawOffset() <= rawOffset) {
                    low = mid + 1;
                    continue;
                }
                high = mid - 1;
            }
            if (high >= 0 && (mark = this.getMark(high)).rawOffset() == rawOffset && (pos = (EditorPosition)mark.get()) != null) {
                return pos;
            }
        } else {
            EditorPosition pos;
            Mark mark;
            while (low <= high) {
                int mid = low + high >>> 1;
                if (this.getMark(mid).rawOffset() < rawOffset) {
                    low = mid + 1;
                    continue;
                }
                high = mid - 1;
            }
            if (low < markCount && (mark = this.getMark(low)).rawOffset() == rawOffset && (pos = (EditorPosition)mark.get()) != null) {
                return pos;
            }
        }
        return this.createPosition(low, offset);
    }

    private EditorPosition createPosition(int index, int offset) {
        if (index != this.gapStart) {
            this.moveGap(index);
        }
        if (this.gapLength == 0) {
            this.reallocate(Math.max(4, this.markArray.length >>> 1));
        }
        if (offset >= this.offsetGapStart) {
            offset += this.offsetGapLength;
        }
        EditorPosition pos = new EditorPosition();
        this.markArray[this.gapStart++] = new Mark(this, offset, pos);
        --this.gapLength;
        return pos;
    }

    void insertUpdate(int offset, int length, MarkUpdate[] markUpdates) {
        block18 : {
            boolean backwardBiasHandling;
            int index;
            boolean bl = backwardBiasHandling = this.isBackwardBiasMarks() || offset == 0;
            if (!backwardBiasHandling) {
                index = this.findFirstIndex(offset);
                if (this.offsetGapStart != offset) {
                    this.moveOffsetGap(offset, index);
                }
            } else {
                int newGapOffset = offset + 1;
                index = this.findFirstIndex(newGapOffset);
                if (this.offsetGapStart != newGapOffset) {
                    this.moveOffsetGap(newGapOffset, index);
                }
            }
            this.offsetGapStart += length;
            this.offsetGapLength -= length;
            if (markUpdates != null) {
                int activeMarkUpdatesCount = 0;
                for (int i = 0; i < markUpdates.length; ++i) {
                    MarkUpdate update = markUpdates[i];
                    if (!update.mark.isActive()) continue;
                    update.restoreRawOffset();
                    markUpdates[activeMarkUpdatesCount++] = update;
                }
                if (activeMarkUpdatesCount > 0) {
                    int foundRestoredMarkCount = 0;
                    if (!backwardBiasHandling) {
                        int rawOffset = this.offsetGapStart + this.offsetGapLength;
                        int i2 = index;
                        do {
                            if (this.getMark(i2).rawOffset() != rawOffset && ++foundRestoredMarkCount == activeMarkUpdatesCount) {
                                if (i2 >= index + activeMarkUpdatesCount) {
                                    int tgtI = i2--;
                                    do {
                                        Mark mark;
                                        if ((mark = this.getMark(i2)).rawOffset() != rawOffset) continue;
                                        this.setMark(tgtI--, mark);
                                    } while (--i2 >= index);
                                }
                                for (int j = activeMarkUpdatesCount - 1; j >= 0; --j) {
                                    this.setMark(index + j, markUpdates[j].mark);
                                }
                                break block18;
                            }
                            ++i2;
                        } while (true);
                    }
                    int i3 = index - 1;
                    do {
                        if (this.getMark(i3).rawOffset() != offset && ++foundRestoredMarkCount == activeMarkUpdatesCount) {
                            if (i3 < index - activeMarkUpdatesCount) {
                                int tgtI = i3++;
                                do {
                                    Mark mark;
                                    if ((mark = this.getMark(i3)).rawOffset() != offset) continue;
                                    this.setMark(tgtI++, mark);
                                } while (++i3 < index);
                            }
                            i3 = index - activeMarkUpdatesCount;
                            for (int j = activeMarkUpdatesCount - 1; j >= 0; --j) {
                                this.setMark(i3 + j, markUpdates[j].mark);
                            }
                            break;
                        }
                        --i3;
                    } while (true);
                }
            }
        }
    }

    MarkUpdate[] removeUpdate(int offset, int length) {
        int index;
        int updateCount;
        Mark mark;
        boolean backwardBiasHandling;
        MarkUpdate[] updates;
        int biasOffset = offset;
        boolean bl = backwardBiasHandling = this.isBackwardBiasMarks() || offset == 0;
        if (backwardBiasHandling) {
            ++biasOffset;
        }
        int newGapOffset = biasOffset + length;
        int endIndex = this.findFirstIndex(newGapOffset);
        if (newGapOffset != this.offsetGapStart) {
            this.moveOffsetGap(newGapOffset, endIndex);
        }
        this.offsetGapStart -= length;
        this.offsetGapLength += length;
        for (index = endIndex - 1; index >= 0 && (mark = this.getMark(index)).rawOffset() >= biasOffset; --index) {
        }
        if ((updateCount = endIndex - ++index) > 0) {
            updates = new MarkUpdate[updateCount];
            int newRawOffset = offset;
            if (!backwardBiasHandling) {
                newRawOffset += this.offsetGapLength;
            }
            for (int i = updateCount - 1; i >= 0; --i) {
                Mark mark2 = this.getMark(index + i);
                updates[i] = new MarkUpdate(mark2);
                mark2.rawOffset = newRawOffset;
            }
        } else {
            updates = null;
        }
        return updates;
    }

    private void moveOffsetGap(int newOffsetGapStart, int index) {
        int rawIndex = this.rawIndex(index);
        int markArrayLength = this.markArray.length;
        int origOffsetGapStart = this.offsetGapStart;
        this.offsetGapStart = newOffsetGapStart;
        if (rawIndex == markArrayLength || this.markArray[rawIndex].rawOffset() > origOffsetGapStart) {
            if (rawIndex >= this.gapStart) {
                int gapEnd = this.gapStart + this.gapLength;
                while (--rawIndex >= gapEnd) {
                    Mark mark = this.markArray[rawIndex];
                    if (mark.rawOffset() > origOffsetGapStart) {
                        mark.rawOffset -= this.offsetGapLength;
                        continue;
                    }
                    return;
                }
                rawIndex = this.gapStart;
            }
            while (--rawIndex >= 0) {
                Mark mark = this.markArray[rawIndex];
                if (mark.rawOffset() > origOffsetGapStart) {
                    mark.rawOffset -= this.offsetGapLength;
                    continue;
                }
                return;
            }
        } else {
            Mark mark;
            if (rawIndex < this.gapStart) {
                while (rawIndex < this.gapStart) {
                    if ((mark = this.markArray[rawIndex++]).rawOffset() <= origOffsetGapStart) {
                        mark.rawOffset += this.offsetGapLength;
                        continue;
                    }
                    return;
                }
                rawIndex += this.gapLength;
            }
            while (rawIndex < markArrayLength) {
                if ((mark = this.markArray[rawIndex++]).rawOffset() <= origOffsetGapStart) {
                    mark.rawOffset += this.offsetGapLength;
                    continue;
                }
                return;
            }
        }
    }

    private void moveGap(int index) {
        if (index <= this.gapStart) {
            int moveSize = this.gapStart - index;
            System.arraycopy(this.markArray, index, this.markArray, this.gapStart + this.gapLength - moveSize, moveSize);
        } else {
            int moveSize = index - this.gapStart;
            System.arraycopy(this.markArray, this.gapStart + this.gapLength, this.markArray, this.gapStart, moveSize);
        }
        this.gapStart = index;
    }

    void compact() {
        if (this.gapLength > 4) {
            this.reallocate(4);
        }
    }

    private void reallocate(int newGapLength) {
        int gapEnd = this.gapStart + this.gapLength;
        int aboveGapLength = this.markArray.length - gapEnd;
        int newLength = this.gapStart + aboveGapLength + newGapLength;
        Mark[] newMarkArray = new Mark[newLength];
        System.arraycopy(this.markArray, 0, newMarkArray, 0, this.gapStart);
        System.arraycopy(this.markArray, gapEnd, newMarkArray, newLength - aboveGapLength, aboveGapLength);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("MarkVector.reallocate() from markArray.length=" + this.markArray.length + " to newLength=" + newLength + "\n");
        }
        this.gapLength = newGapLength;
        this.markArray = newMarkArray;
    }

    private int findFirstIndex(int offset) {
        int low = 0;
        int markCount = this.markCount();
        int high = markCount - 1;
        int rawOffset = this.rawOffset(offset);
        while (low <= high) {
            int mid = low + high >>> 1;
            if (this.getMark(mid).rawOffset() < rawOffset) {
                low = mid + 1;
                continue;
            }
            high = mid - 1;
        }
        return low;
    }

    int offset(int rawOffset) {
        return rawOffset < this.offsetGapStart ? rawOffset : rawOffset - this.offsetGapLength;
    }

    int rawOffset(int offset) {
        return offset < this.offsetGapStart ? offset : offset + this.offsetGapLength;
    }

    private int rawIndex(int index) {
        return index < this.gapStart ? index : index + this.gapLength;
    }

    private int markCount() {
        return this.markArray.length - this.gapLength;
    }

    private Mark getMark(int index) {
        return this.markArray[this.rawIndex(index)];
    }

    private void setMark(int index, Mark mark) {
        this.markArray[this.rawIndex((int)index)] = mark;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void notifyMarkDisposed() {
        Object object = this.lock;
        synchronized (object) {
            ++this.disposedMarkCount;
            if (this.disposedMarkCount > Math.max(5, this.markCount() >> 3)) {
                this.removeDisposedMarks();
            }
        }
    }

    private void removeDisposedMarks() {
        int newGapLength;
        int rawIndex;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("MarkVector.removeDisposedMarks() disposedMarkCount=" + this.disposedMarkCount + "\n");
        }
        int validIndex = 0;
        int gapEnd = this.gapStart + this.gapLength;
        for (rawIndex = 0; rawIndex < this.gapStart; ++rawIndex) {
            Mark mark = this.markArray[rawIndex];
            if (mark.get() != null) {
                if (rawIndex != validIndex) {
                    this.markArray[validIndex] = mark;
                }
                ++validIndex;
                continue;
            }
            mark.clearMarkVector();
        }
        this.gapStart = validIndex;
        int topValidIndex = rawIndex = this.markArray.length;
        while (--rawIndex >= gapEnd) {
            Mark mark = this.markArray[rawIndex];
            if (mark.get() != null) {
                if (rawIndex == --topValidIndex) continue;
                this.markArray[topValidIndex] = mark;
                continue;
            }
            mark.clearMarkVector();
        }
        this.gapLength = newGapLength = topValidIndex - this.gapStart;
        while (validIndex < topValidIndex) {
            this.markArray[validIndex++] = null;
        }
        this.disposedMarkCount = 0;
    }

    String consistencyError(int maxOffset) {
        int markCount = this.markCount();
        int lastOffset = 0;
        for (int i = 0; i < markCount; ++i) {
            Mark mark = this.getMark(i);
            int offset = mark.getOffset();
            int rawOffset = mark.rawOffset();
            String err = null;
            if (offset < lastOffset) {
                err = "offset=" + offset + " < lastOffset=" + lastOffset;
            } else if (rawOffset < 0) {
                err = "rawOffset=" + rawOffset + " < 0";
            } else if (offset > maxOffset) {
                err = "offset=" + offset + " > maxOffset=" + maxOffset;
            } else if (offset < this.offsetGapStart && rawOffset >= this.offsetGapStart) {
                err = "offset=" + offset + " but rawOffset=" + rawOffset + " >= offsetGapStart=" + this.offsetGapStart;
            } else if (offset >= this.offsetGapStart && rawOffset < this.offsetGapStart + this.offsetGapLength) {
                err = "offset=" + offset + " but rawOffset=" + rawOffset + " < offsetGapStart=" + this.offsetGapStart + " + offsetGapLength=" + this.offsetGapLength;
            }
            if (err != null) {
                return (this.isBackwardBiasMarks() ? "BB-" : "") + "markArray[" + i + "]: " + err;
            }
            lastOffset = offset;
        }
        if (zeroPos.getOffset() != 0) {
            return "zeroPos.getOffset()=" + zeroPos.getOffset() + " != 0";
        }
        return null;
    }

    public String toString() {
        return (this.isBackwardBiasMarks() ? "BB:" : "") + "markCount=" + this.markCount() + ", gap:" + CharContent.gapToString(this.markArray.length, this.gapStart, this.gapLength) + ", OGap:<0," + this.offsetGapStart + ")" + this.offsetGapLength + '<' + (this.offsetGapStart + this.offsetGapLength) + ",...>";
    }

    public String toStringDetail(Mark accentMark) {
        StringBuilder sb = new StringBuilder(200);
        sb.append(this.toString()).append(", IHC=").append(System.identityHashCode(this)).append('\n');
        int markCount = this.markCount();
        int digitCount = ArrayUtilities.digitCount((int)markCount);
        for (int i = 0; i < markCount; ++i) {
            Mark mark = this.getMark(i);
            sb.append(mark == accentMark ? "**" : "  ");
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            sb.append(mark.toStringDetail()).append('\n');
        }
        return sb.toString();
    }

    static StringBuilder markUpdatesToString(StringBuilder sb, MarkUpdate[] markUpdates, int length) {
        int digitCount = ArrayUtilities.digitCount((int)length);
        for (int i = 0; i < length; ++i) {
            sb.append("    ");
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            sb.append(markUpdates[i]).append('\n');
        }
        return sb;
    }

    static final class MarkUpdate {
        final Mark mark;
        final int origRawOffset;

        MarkUpdate(Mark mark) {
            this.mark = mark;
            this.origRawOffset = mark.rawOffset();
        }

        void restoreRawOffset() {
            this.mark.rawOffset = this.origRawOffset;
        }

        public String toString() {
            return this.mark.toStringDetail() + " <= OrigRaw:" + this.origRawOffset;
        }
    }

}

