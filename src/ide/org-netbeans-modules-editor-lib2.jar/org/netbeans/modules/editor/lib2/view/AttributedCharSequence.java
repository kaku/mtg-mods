/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.openide.util.Exceptions;

public final class AttributedCharSequence
implements AttributedCharacterIterator {
    private static final Map<Object, AttributeTranslateHandler> attr2Handler = new HashMap<Object, AttributeTranslateHandler>(20, 0.5f);
    private final List<TextRun> textRuns = new ArrayList<TextRun>();
    private CharSequence text;
    private int charIndex;
    private int textRunIndex;
    private Set<AttributedCharacterIterator.Attribute> allAttributeKeys;

    public static Map<AttributedCharacterIterator.Attribute, Object> translate(AttributeSet attrs) {
        int attrCount = attrs.getAttributeCount();
        HashMap<AttributedCharacterIterator.Attribute, Object> ret = new HashMap<AttributedCharacterIterator.Attribute, Object>(attrCount);
        Enumeration attrNames = attrs.getAttributeNames();
        while (attrNames.hasMoreElements()) {
            Object textAttrValue;
            Object attrName = attrNames.nextElement();
            AttributeTranslateHandler handler = attr2Handler.get(attrName);
            if (handler == null || (textAttrValue = handler.getTextAttrValue(attrs.getAttribute(attrName))) == null) continue;
            ret.put(handler.getTextAttr(), textAttrValue);
        }
        return ret;
    }

    public static Map<AttributedCharacterIterator.Attribute, Object> merge(Map<AttributedCharacterIterator.Attribute, Object> textAttrs0, Map<AttributedCharacterIterator.Attribute, Object> textAttrs1) {
        HashMap<AttributedCharacterIterator.Attribute, Object> ret = new HashMap<AttributedCharacterIterator.Attribute, Object>(textAttrs0);
        ret.putAll(textAttrs1);
        return ret;
    }

    void addTextRun(int endIndex, Map<AttributedCharacterIterator.Attribute, Object> textAttrs) {
        if (endIndex <= this.endIndex()) {
            throw new IllegalArgumentException("endIndex=" + endIndex + ", endIndex()=" + this.endIndex());
        }
        this.textRuns.add(new TextRun(endIndex, textAttrs));
    }

    void setText(CharSequence text, Map<AttributedCharacterIterator.Attribute, Object> defaultTextAttrs) {
        int textLen;
        this.text = text;
        int endIndex = this.endIndex();
        if (endIndex > (textLen = text.length())) {
            throw new IllegalStateException("endIndex=" + endIndex + " > text.length()=" + textLen);
        }
        if (endIndex < textLen) {
            this.addTextRun(textLen, defaultTextAttrs);
        }
        ((ArrayList)this.textRuns).trimToSize();
    }

    private int endIndex() {
        return this.textRuns.size() > 0 ? this.textRuns.get((int)(this.textRuns.size() - 1)).endIndex : 0;
    }

    @Override
    public int getRunStart() {
        return this.textRunIndex > 0 ? this.textRuns.get((int)(this.textRunIndex - 1)).endIndex : 0;
    }

    @Override
    public int getRunStart(AttributedCharacterIterator.Attribute attribute) {
        return this.getRunStart();
    }

    @Override
    public int getRunStart(Set<? extends AttributedCharacterIterator.Attribute> attributes) {
        return this.getRunStart();
    }

    @Override
    public int getRunLimit() {
        return this.textRunIndex < this.textRuns.size() ? this.textRuns.get((int)this.textRunIndex).endIndex : this.textRuns.get((int)(this.textRuns.size() - 1)).endIndex;
    }

    @Override
    public int getRunLimit(AttributedCharacterIterator.Attribute attribute) {
        return this.getRunLimit();
    }

    @Override
    public int getRunLimit(Set<? extends AttributedCharacterIterator.Attribute> attributes) {
        return this.getRunLimit();
    }

    @Override
    public Map<AttributedCharacterIterator.Attribute, Object> getAttributes() {
        if (this.textRunIndex >= this.textRuns.size()) {
            return Collections.emptyMap();
        }
        return this.textRuns.get((int)this.textRunIndex).attrs;
    }

    @Override
    public Object getAttribute(AttributedCharacterIterator.Attribute attribute) {
        return this.getAttributes().get(attribute);
    }

    @Override
    public Set<AttributedCharacterIterator.Attribute> getAllAttributeKeys() {
        if (this.allAttributeKeys == null) {
            HashSet<AttributedCharacterIterator.Attribute> allKeys = new HashSet<AttributedCharacterIterator.Attribute>();
            for (int i = this.textRuns.size() - 1; i >= 0; --i) {
                allKeys.addAll(this.textRuns.get((int)i).attrs.keySet());
            }
            this.allAttributeKeys = allKeys;
        }
        return this.allAttributeKeys;
    }

    @Override
    public char first() {
        this.setIndex(0);
        return this.current();
    }

    @Override
    public char last() {
        this.setIndex(Math.max(0, this.text.length() - 1));
        return this.current();
    }

    @Override
    public char current() {
        return this.charIndex < this.text.length() ? this.text.charAt(this.charIndex) : '\uffff';
    }

    @Override
    public char next() {
        if (this.charIndex == this.text.length()) {
            return '\uffff';
        }
        ++this.charIndex;
        if (this.charIndex == this.textRuns.get((int)this.textRunIndex).endIndex) {
            ++this.textRunIndex;
        }
        return this.current();
    }

    @Override
    public char previous() {
        if (this.charIndex == 0) {
            return '\uffff';
        }
        --this.charIndex;
        if (this.textRunIndex > 0 && this.charIndex < this.textRuns.get((int)(this.textRunIndex - 1)).endIndex) {
            --this.textRunIndex;
        }
        return this.current();
    }

    @Override
    public char setIndex(int position) {
        if (position < 0 || position > this.text.length()) {
            throw new IllegalArgumentException("position=" + position + " not within <0," + this.text.length() + ">");
        }
        return this.setIndexImpl(position);
    }

    private char setIndexImpl(int position) {
        this.charIndex = position;
        if (position == 0) {
            this.textRunIndex = 0;
        } else {
            int last = this.textRuns.size() - 1;
            int low = 0;
            int high = last;
            while (low <= high) {
                int mid = low + high >>> 1;
                TextRun textRun = this.textRuns.get(mid);
                if (textRun.endIndex < position) {
                    low = mid + 1;
                    continue;
                }
                if (textRun.endIndex > position) {
                    high = mid - 1;
                    continue;
                }
                low = mid + 1;
                break;
            }
            this.textRunIndex = Math.min(low, last);
        }
        return this.current();
    }

    @Override
    public int getBeginIndex() {
        return 0;
    }

    @Override
    public int getEndIndex() {
        return this.text.length();
    }

    @Override
    public int getIndex() {
        return this.charIndex;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(100);
        sb.append("text(").append(this.text.length()).append(")=\"").append(CharSequenceUtilities.debugText((CharSequence)this.text)).append("\"; ").append(this.textRuns.size()).append(" text runs:\n");
        int textRunsSize = this.textRuns.size();
        int maxDigitCount = ArrayUtilities.digitCount((int)textRunsSize);
        for (int i = 0; i < textRunsSize; ++i) {
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)maxDigitCount);
            sb.append(": ");
            this.textRuns.get(i).appendInfo(sb);
            sb.append("\n");
        }
        return sb.toString();
    }

    static {
        attr2Handler.put(StyleConstants.Family, new AttributeTranslateHandler(TextAttribute.FAMILY));
        attr2Handler.put(StyleConstants.Italic, new AttributeTranslateHandler(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE));
        attr2Handler.put(StyleConstants.Bold, new AttributeTranslateHandler(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD));
        attr2Handler.put(StyleConstants.Size, new AttributeTranslateHandler(TextAttribute.SIZE));
        attr2Handler.put(StyleConstants.Superscript, new AttributeTranslateHandler(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER));
        attr2Handler.put(StyleConstants.Subscript, new AttributeTranslateHandler(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUB));
        attr2Handler.put(StyleConstants.Underline, new AttributeTranslateHandler(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON));
        attr2Handler.put(StyleConstants.StrikeThrough, new AttributeTranslateHandler(TextAttribute.STRIKETHROUGH));
        attr2Handler.put(StyleConstants.StrikeThrough, new AttributeTranslateHandler(TextAttribute.STRIKETHROUGH));
        attr2Handler.put(StyleConstants.Foreground, new AttributeTranslateHandler(TextAttribute.FOREGROUND));
        attr2Handler.put(StyleConstants.Background, new AttributeTranslateHandler(TextAttribute.BACKGROUND));
        attr2Handler.put(StyleConstants.Background, new AttributeTranslateHandler(TextAttribute.BACKGROUND));
    }

    private static final class AttributeTranslateHandler {
        private final AttributedCharacterIterator.Attribute textAttr;
        private final Object textAttrValue;

        AttributeTranslateHandler(AttributedCharacterIterator.Attribute textAttr) {
            this(textAttr, null);
        }

        AttributeTranslateHandler(AttributedCharacterIterator.Attribute textAttr, Object textAttrValue) {
            this.textAttr = textAttr;
            this.textAttrValue = textAttrValue;
        }

        AttributedCharacterIterator.Attribute getTextAttr() {
            return this.textAttr;
        }

        Object getTextAttrValue(Object attrValue) {
            Object ret = this.textAttrValue == null ? attrValue : (Boolean.TRUE.equals(attrValue) ? this.textAttrValue : null);
            return ret;
        }
    }

    private static final class TextRun {
        final int endIndex;
        final Map<AttributedCharacterIterator.Attribute, Object> attrs;

        public TextRun(int endIndex, Map<AttributedCharacterIterator.Attribute, Object> attrs) {
            this.endIndex = endIndex;
            this.attrs = attrs;
        }

        StringBuilder appendInfo(StringBuilder sb) {
            sb.append("endIndex=").append(this.endIndex).append(", attrs=").append(this.attrs);
            return sb;
        }

        public String toString() {
            return this.appendInfo(new StringBuilder()).toString();
        }
    }

}

