/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Utilities
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.editor.lib2.actions;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeListener;
import javax.swing.text.Caret;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.modules.editor.lib2.actions.SearchableEditorKit;
import org.netbeans.modules.editor.lib2.actions.SearchableEditorKitImpl;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;

public final class EditorActionUtilities {
    private static final Logger LOG = Logger.getLogger(EditorActionUtilities.class.getName());
    private static Map<String, Map<String, KeyStroke>> mimeType2actionName2KeyStroke;
    private static Map<String, Boolean> mimeType2ListenerPresent;
    private static SearchableEditorKit globalActionsKit;
    private static final Map<EditorKit, SearchableEditorKit> kit2searchable;
    static final int LARGE_ICON_SIZE = 24;
    static final String LARGE_ICON_SIZE_STRING = "24";

    private EditorActionUtilities() {
    }

    public static void resetCaretMagicPosition(JTextComponent component) {
        Caret caret;
        if (component != null && (caret = component.getCaret()) != null) {
            caret.setMagicCaretPosition(null);
        }
    }

    public static boolean isUseLargeIcon(JComponent c) {
        Object prefIconSize = c.getClientProperty("PreferredIconSize");
        return prefIconSize instanceof Integer && (Integer)prefIconSize >= 24;
    }

    public static Icon getIcon(Action a, boolean large) {
        return large ? EditorActionUtilities.getLargeIcon(a) : EditorActionUtilities.getSmallIcon(a);
    }

    public static Icon getSmallIcon(Action a) {
        return (Icon)a.getValue("SmallIcon");
    }

    public static Icon getLargeIcon(Action a) {
        return (Icon)a.getValue("SwingLargeIconKey");
    }

    public static Icon createSmallIcon(Action a) {
        String iconBase = (String)a.getValue("iconBase");
        if (iconBase != null) {
            return ImageUtilities.loadImageIcon((String)iconBase, (boolean)true);
        }
        return null;
    }

    public static Icon createLargeIcon(Action a) {
        String iconBase = (String)a.getValue("iconBase");
        if (iconBase != null) {
            iconBase = iconBase + "24";
            return ImageUtilities.loadImageIcon((String)iconBase, (boolean)true);
        }
        return null;
    }

    static void updateButtonIcons(AbstractButton button, Icon icon, boolean useLargeIcon, String iconResource) {
        button.setIcon(icon);
        if (iconResource != null) {
            ImageIcon pressedIcon;
            ImageIcon rolloverIcon;
            ImageIcon disabledIcon;
            ImageIcon disabledSelectedIcon;
            ImageIcon rolloverSelectedIcon;
            String base = iconResource;
            String suffix = "";
            int dotIndex = iconResource.lastIndexOf(46);
            if (dotIndex >= 0) {
                suffix = iconResource.substring(dotIndex, iconResource.length());
                base = iconResource.substring(0, dotIndex);
            }
            if (useLargeIcon) {
                base = base + "24";
            }
            if ((pressedIcon = ImageUtilities.loadImageIcon((String)(base + "_pressed" + suffix), (boolean)true)) != null) {
                button.setPressedIcon(pressedIcon);
            }
            if ((rolloverIcon = ImageUtilities.loadImageIcon((String)(base + "_rollover" + suffix), (boolean)true)) != null) {
                button.setRolloverIcon(rolloverIcon);
            }
            if ((disabledIcon = ImageUtilities.loadImageIcon((String)(base + "_disabled" + suffix), (boolean)true)) != null) {
                button.setDisabledIcon(disabledIcon);
            } else {
                button.setDisabledIcon(ImageUtilities.createDisabledIcon((Icon)icon));
            }
            ImageIcon selectedIcon = ImageUtilities.loadImageIcon((String)(base + "_selected" + suffix), (boolean)true);
            if (selectedIcon != null) {
                button.setSelectedIcon(selectedIcon);
            }
            if ((rolloverSelectedIcon = ImageUtilities.loadImageIcon((String)(base + "_rolloverSelected" + suffix), (boolean)true)) != null) {
                button.setRolloverSelectedIcon(rolloverSelectedIcon);
            }
            if ((disabledSelectedIcon = ImageUtilities.loadImageIcon((String)(base + "_disabledSelected" + suffix), (boolean)true)) != null) {
                button.setDisabledSelectedIcon(disabledSelectedIcon);
            }
        }
    }

    static String insertBeforeSuffix(String path, String toInsert) {
        int dotIndex = path.lastIndexOf(46);
        path = dotIndex >= 0 ? path.substring(0, dotIndex) + toInsert + path.substring(dotIndex, path.length()) : path + toInsert;
        return path;
    }

    public static String getKeyMnemonic(MultiKeyBinding binding) {
        return EditorActionUtilities.getKeyMnemonic(binding.getKeyStrokeList());
    }

    public static String getKeyMnemonic(KeyStroke key) {
        return EditorActionUtilities.appendKeyMnemonic(new StringBuilder(20), key).toString();
    }

    public static String getKeyMnemonic(List<KeyStroke> keys) {
        StringBuilder sb = new StringBuilder(40);
        for (KeyStroke key : keys) {
            if (sb.length() > 0) {
                sb.append(' ');
            }
            EditorActionUtilities.appendKeyMnemonic(sb, key);
        }
        return sb.toString();
    }

    public static String appendKeyMnemonic(StringBuilder sb, KeyStroke key) {
        int i;
        String sk = Utilities.keyToString((KeyStroke)key);
        int mods = key.getModifiers();
        if ((mods & 2) != 0) {
            sb.append("Ctrl+");
        }
        if ((mods & 8) != 0) {
            sb.append("Alt+");
        }
        if ((mods & 1) != 0) {
            sb.append("Shift+");
        }
        if ((mods & 4) != 0) {
            sb.append("Meta+");
        }
        if ((i = sk.indexOf(45)) != -1) {
            sk = sk.substring(i + 1);
        }
        sb.append(sk);
        return sb.toString();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static SearchableEditorKit getGlobalActionsKit() {
        Class<EditorActionUtilities> class_ = EditorActionUtilities.class;
        synchronized (EditorActionUtilities.class) {
            if (globalActionsKit == null) {
                globalActionsKit = new SearchableEditorKitImpl("");
            }
            // ** MonitorExit[var0] (shouldn't be in output)
            return globalActionsKit;
        }
    }

    public static EditorKit getKit(String mimeType) {
        Lookup.Result result = MimeLookup.getLookup((String)mimeType).lookupResult(EditorKit.class);
        Iterator instancesIterator = result.allInstances().iterator();
        EditorKit kit = instancesIterator.hasNext() ? (EditorKit)instancesIterator.next() : null;
        return kit;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void registerSearchableKit(EditorKit kit, SearchableEditorKit searchableKit) {
        Map<EditorKit, SearchableEditorKit> map = kit2searchable;
        synchronized (map) {
            kit2searchable.put(kit, searchableKit);
        }
    }

    public static Action getAction(EditorKit kit, String actionName) {
        return EditorActionUtilities.getSearchableKit(kit).getAction(actionName);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static SearchableEditorKit getSearchableKit(EditorKit kit) {
        SearchableEditorKit searchableKit;
        if (kit instanceof SearchableEditorKit) {
            searchableKit = (SearchableEditorKit)((Object)kit);
        } else {
            Map<EditorKit, SearchableEditorKit> map = kit2searchable;
            synchronized (map) {
                searchableKit = kit2searchable.get(kit);
                if (searchableKit == null) {
                    searchableKit = new DefaultSearchableKit(kit);
                    EditorActionUtilities.registerSearchableKit(kit, searchableKit);
                }
            }
        }
        return searchableKit;
    }

    public static Lookup.Result<Action> createActionsLookupResult(String mimeType) {
        if (!MimePath.validate((CharSequence)mimeType)) {
            throw new IllegalArgumentException("\u00cfnvalid mimeType=\"" + mimeType + "\"");
        }
        Lookup lookup = Lookups.forPath((String)EditorActionUtilities.getPath(mimeType, "Actions"));
        return lookup.lookupResult(Action.class);
    }

    private static String getPath(String mimeType, String subFolder) {
        StringBuilder path = new StringBuilder(50);
        path.append("Editors/");
        if (mimeType.length() > 0) {
            path.append('/').append(mimeType);
        }
        if (subFolder.length() > 0) {
            path.append('/').append(subFolder);
        }
        return path.toString();
    }

    public static Preferences getPreferences(Map<String, ?> attrs) {
        Lookup mimeLookup;
        String mimeType = (String)attrs.get("mimeType");
        if (mimeType != null) {
            mimeType = "";
        }
        return (mimeLookup = MimeLookup.getLookup((String)mimeType)) != null ? (Preferences)mimeLookup.lookup(Preferences.class) : null;
    }

    public static Preferences getGlobalPreferences() {
        Lookup globalMimeLookup = MimeLookup.getLookup((MimePath)MimePath.EMPTY);
        return globalMimeLookup != null ? (Preferences)globalMimeLookup.lookup(Preferences.class) : null;
    }

    public static KeyStroke getAccelerator(FileObject fo) {
        if (fo == null) {
            throw new IllegalArgumentException("Must be called with non-null fileObject");
        }
        boolean fineLoggable = LOG.isLoggable(Level.FINE);
        String path = fo.getParent().getPath();
        String actionName = (String)fo.getAttribute("Name");
        KeyStroke ks = null;
        if (path.startsWith("Editors/")) {
            if ((path = path.substring(7)).endsWith("/Actions")) {
                String mimeType;
                if ((path = path.substring(0, path.length() - 8)).startsWith("/")) {
                    path = path.substring(1);
                }
                if (!MimePath.validate((CharSequence)(mimeType = path))) {
                    LOG.info("Invalid mime-type='" + mimeType + "' of action's fileObject=" + (Object)fo);
                }
                ks = EditorActionUtilities.getAccelerator(mimeType, actionName);
            } else if (fineLoggable) {
                LOG.fine("No \"/Actions\" at end of mime-type='" + path + "' of action's fileObject=" + (Object)fo);
            }
        } else if (fineLoggable) {
            LOG.fine("No \"Editors/\" at begining of mime-type='" + path + "' of action's fileObject=" + (Object)fo);
        }
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("Accelerator for action \"" + actionName + "\" is " + ks);
        }
        return ks;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static KeyStroke getAccelerator(String mimeType, String actionName) {
        KeyStroke ks = null;
        if (actionName == null) return ks;
        Class<EditorActionUtilities> class_ = EditorActionUtilities.class;
        synchronized (EditorActionUtilities.class) {
            Map<String, KeyStroke> actionName2KeyStrokeList;
            if (mimeType2actionName2KeyStroke == null) {
                mimeType2actionName2KeyStroke = new HashMap<String, Map<String, KeyStroke>>();
            }
            if ((actionName2KeyStrokeList = mimeType2actionName2KeyStroke.get(mimeType)) != null) return actionName2KeyStrokeList.get(actionName);
            actionName2KeyStrokeList = new HashMap<String, KeyStroke>();
            Lookup.Result result = MimeLookup.getLookup((String)mimeType).lookupResult(KeyBindingSettings.class);
            Collection instances = result.allInstances();
            if (!instances.isEmpty()) {
                KeyBindingSettings kbs = (KeyBindingSettings)instances.iterator().next();
                for (MultiKeyBinding kb : kbs.getKeyBindings()) {
                    if (actionName2KeyStrokeList.containsKey(kb.getActionName()) || kb.getKeyStrokeCount() != 1) continue;
                    actionName2KeyStrokeList.put(kb.getActionName(), kb.getKeyStroke(0));
                }
            }
            mimeType2actionName2KeyStroke.put(mimeType, actionName2KeyStrokeList);
            if (Boolean.TRUE.equals(mimeType2ListenerPresent.get(mimeType))) return actionName2KeyStrokeList.get(actionName);
            mimeType2ListenerPresent.put(mimeType, true);
            result.addLookupListener((LookupListener)KeyBindingSettingsListener.INSTANCE);
            return actionName2KeyStrokeList.get(actionName);
        }
    }

    public static Action createEmptyAction() {
        return new EmptyAction();
    }

    public static String getGlobalActionDisplayName(Map<String, ?> attrs) {
        return (String)EditorActionUtilities.getGlobalActionProperty(attrs, "displayName");
    }

    public static String getGlobalActionShortDescription(Map<String, ?> attrs) {
        return (String)EditorActionUtilities.getGlobalActionProperty(attrs, "ShortDescription");
    }

    public static String getGlobalActionIconResource(Map<String, ?> attrs) {
        return (String)EditorActionUtilities.getGlobalActionProperty(attrs, "iconBase");
    }

    public static String getGlobalActionMenuText(Map<String, ?> attrs) {
        return (String)EditorActionUtilities.getGlobalActionProperty(attrs, "menuText");
    }

    public static String getGlobalActionPopupText(Map<String, ?> attrs) {
        return (String)EditorActionUtilities.getGlobalActionProperty(attrs, "popupText");
    }

    public static Object getGlobalActionProperty(Map<String, ?> attrs, String key) {
        Action a;
        Object value = null;
        String actionName = (String)attrs.get("Name");
        SearchableEditorKit globalKit = EditorActionUtilities.getGlobalActionsKit();
        if (globalKit != null && (a = globalKit.getAction(actionName)) != null) {
            value = a.getValue(key);
        }
        return value;
    }

    static {
        mimeType2ListenerPresent = new HashMap<String, Boolean>();
        kit2searchable = new WeakHashMap<EditorKit, SearchableEditorKit>();
    }

    private static final class EmptyAction
    extends TextAction {
        EmptyAction() {
            super("empty-action");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        }
    }

    private static final class DefaultSearchableKit
    implements SearchableEditorKit {
        private final Map<String, Reference<Action>> name2actionRef = new WeakHashMap<String, Reference<Action>>();

        DefaultSearchableKit(EditorKit kit) {
            for (Action action : kit.getActions()) {
                if (action == null) continue;
                this.name2actionRef.put((String)action.getValue("Name"), new WeakReference<Action>(action));
            }
        }

        @Override
        public Action getAction(String actionName) {
            Reference<Action> actionRef = this.name2actionRef.get(actionName);
            return actionRef != null ? actionRef.get() : null;
        }

        @Override
        public void addActionsChangeListener(ChangeListener listener) {
        }

        @Override
        public void removeActionsChangeListener(ChangeListener listener) {
        }
    }

    private static final class KeyBindingSettingsListener
    implements LookupListener {
        static final KeyBindingSettingsListener INSTANCE = new KeyBindingSettingsListener();

        private KeyBindingSettingsListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void resultChanged(LookupEvent ev) {
            Class<EditorActionUtilities> class_ = EditorActionUtilities.class;
            synchronized (EditorActionUtilities.class) {
                mimeType2actionName2KeyStroke = null;
                LOG.fine("mimeType2actionName2KeyStroke cleared.");
                // ** MonitorExit[var2_2] (shouldn't be in output)
                return;
            }
        }
    }

}

