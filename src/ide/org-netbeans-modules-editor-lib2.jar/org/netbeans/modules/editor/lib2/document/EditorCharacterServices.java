/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;

public abstract class EditorCharacterServices {
    public abstract int getIdentifierEnd(@NonNull Document var1, int var2, boolean var3);
}

