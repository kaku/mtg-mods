/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 */
package org.netbeans.modules.editor.lib2.actions;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.lib2.actions.EditorRegistryWatcher;
import org.openide.awt.Mnemonics;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;

public final class PresenterUpdater
implements PropertyChangeListener,
ActionListener {
    private static final Logger LOG = Logger.getLogger(PresenterUpdater.class.getName());
    private static final int MENU = 0;
    private static final int POPUP = 1;
    private static final int TOOLBAR = 2;
    private static final boolean ON_MAC = Utilities.isMac();
    private final int type;
    private final String actionName;
    private final Action action;
    private Action contextAction;
    final AbstractButton presenter;
    private final boolean useActionSelectedProperty;
    private final Set<Action> listenedContextActions;
    private boolean updatesPending;
    private boolean presenterActive;

    public static JMenuItem createMenuPresenter(Action a) {
        return (JMenuItem)new PresenterUpdater((int)0, (Action)a).presenter;
    }

    public static JMenuItem createPopupPresenter(Action a) {
        return (JMenuItem)new PresenterUpdater((int)1, (Action)a).presenter;
    }

    public static AbstractButton createToolbarPresenter(Action a) {
        return new PresenterUpdater((int)2, (Action)a).presenter;
    }

    private PresenterUpdater(int type, Action action) {
        if (action == null) {
            throw new IllegalArgumentException("action must not be null");
        }
        this.type = type;
        this.actionName = (String)action.getValue("Name");
        this.action = action;
        if (type == 2) {
            this.presenter = new JButton();
            this.useActionSelectedProperty = false;
        } else {
            boolean bl = this.useActionSelectedProperty = action.getValue("preferencesKey") != null;
            if (this.useActionSelectedProperty) {
                this.presenter = new LazyJCheckBoxMenuItem();
                this.presenter.setSelected(this.isActionSelected());
            } else {
                this.presenter = new LazyJMenuItem();
            }
        }
        action.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)action));
        if (type == 0) {
            this.listenedContextActions = new WeakSet();
            EditorRegistryWatcher.get().registerPresenterUpdater(this);
        } else {
            this.listenedContextActions = null;
        }
        this.presenter.addActionListener(this);
        this.updatePresenter(null);
    }

    public String getActionName() {
        return this.actionName;
    }

    private boolean isActionSelected() {
        return Boolean.TRUE.equals(this.activeAction().getValue("SwingSelectedKey"));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Action a = (Action)evt.getSource();
        if (a != this.action && a != this.contextAction) {
            return;
        }
        this.updatePresenter(evt.getPropertyName());
    }

    void presenterActivated() {
        if (!this.presenterActive) {
            this.presenterActive = true;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Presenter for " + this.action + " activated. updatesPending=" + this.updatesPending + '\n');
            }
            if (this.updatesPending) {
                this.updatesPending = false;
                this.updatePresenter(null);
            }
        }
    }

    void presenterDeactivated() {
        this.presenterActive = false;
    }

    private void updatePresenter(String propName) {
        List firstMkb;
        if (!ON_MAC && this.type == 0 && !this.presenterActive) {
            this.updatesPending = true;
            if (SwingUtilities.isEventDispatchThread()) {
                this.presenter.invalidate();
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        PresenterUpdater.this.presenter.invalidate();
                    }
                });
            }
            return;
        }
        Action cAction = this.contextAction;
        if (propName == null || "enabled".equals(propName)) {
            boolean enabled = (cAction != null ? cAction : this.action).isEnabled();
            this.presenter.setEnabled(enabled);
        }
        if ((propName == null || "displayName".equals(propName) || "menuText".equals(propName) || "popupText".equals(propName)) && this.type != 2) {
            String text = null;
            if (this.type == 1) {
                if (cAction != null) {
                    text = (String)cAction.getValue("popupText");
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("POPUP_TEXT_KEY for context " + cAction + ": \"" + text + "\"\n");
                    }
                }
                if (text == null) {
                    text = (String)this.action.getValue("popupText");
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("POPUP_TEXT_KEY for action " + this.action + ": \"" + text + "\"\n");
                    }
                }
            }
            if (text == null && cAction != null) {
                text = (String)cAction.getValue("menuText");
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("MENU_TEXT_KEY for context " + cAction + ": \"" + text + "\"\n");
                }
            }
            if (text == null) {
                text = (String)this.action.getValue("menuText");
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("MENU_TEXT_KEY for " + this.action + ": \"" + text + "\"\n");
                }
            }
            if (text != null) {
                Mnemonics.setLocalizedText((AbstractButton)this.presenter, (String)text);
                this.presenter.getAccessibleContext().setAccessibleName(text);
            } else {
                if (cAction != null) {
                    text = (String)cAction.getValue("displayName");
                }
                if (text == null) {
                    text = (String)this.action.getValue("displayName");
                }
                if (text != null) {
                    this.presenter.setText(text);
                    this.presenter.getAccessibleContext().setAccessibleName(text);
                }
            }
        }
        if (propName == null || "SmallIcon".equals(propName) || "SwingLargeIconKey".equals(propName) || "iconBase".equals(propName)) {
            Icon icon = null;
            if (this.isMenuItem()) {
                if (cAction != null && Boolean.TRUE.equals(cAction.getValue("noIconInMenu"))) {
                    return;
                }
                if (cAction != null) {
                    icon = EditorActionUtilities.getSmallIcon(cAction);
                }
                if (icon == null && Boolean.TRUE.equals(this.action.getValue("noIconInMenu"))) {
                    return;
                }
                if (icon == null) {
                    icon = EditorActionUtilities.getSmallIcon(this.action);
                }
                if (icon != null) {
                    this.presenter.setIcon(icon);
                }
            } else {
                boolean useLargeIcon = EditorActionUtilities.isUseLargeIcon(this.presenter);
                if (useLargeIcon) {
                    if (cAction != null) {
                        icon = EditorActionUtilities.getLargeIcon(cAction);
                    }
                    if (icon == null) {
                        icon = EditorActionUtilities.getLargeIcon(this.action);
                    }
                }
                if (icon == null) {
                    useLargeIcon = false;
                    if (cAction != null) {
                        icon = EditorActionUtilities.getSmallIcon(cAction);
                    }
                    if (icon == null) {
                        icon = EditorActionUtilities.getSmallIcon(this.action);
                    }
                }
                if (icon != null) {
                    String iconResource = null;
                    if (cAction != null) {
                        iconResource = (String)cAction.getValue("iconBase");
                    }
                    if (iconResource == null) {
                        iconResource = (String)this.action.getValue("iconBase");
                    }
                    EditorActionUtilities.updateButtonIcons(this.presenter, icon, useLargeIcon, iconResource);
                }
            }
        }
        if ((propName == null || "AcceleratorKey".equals(propName) || "MultiAcceleratorListKey".equals(propName)) && this.isMenuItem()) {
            Action a = cAction != null ? cAction : this.action;
            List mkbList = (List)a.getValue("MultiAcceleratorListKey");
            KeyStroke accelerator = null;
            if (mkbList != null && mkbList.size() > 0) {
                firstMkb = (List)mkbList.get(0);
                accelerator = (KeyStroke)firstMkb.get(0);
            } else {
                accelerator = (KeyStroke)a.getValue("AcceleratorKey");
            }
            ((JMenuItem)this.presenter).setAccelerator(accelerator);
        }
        if ((propName == null || "AcceleratorKey".equals(propName) || "MultiAcceleratorListKey".equals(propName) || "ShortDescription".equals(propName)) && this.type == 2) {
            String toolTipText = null;
            if (cAction != null) {
                toolTipText = (String)cAction.getValue("ShortDescription");
            }
            if (toolTipText == null) {
                toolTipText = (String)this.action.getValue("ShortDescription");
            }
            if (toolTipText == null) {
                toolTipText = "";
            }
            if (toolTipText.length() > 0) {
                Action a = cAction != null ? cAction : this.action;
                List mkbList = (List)a.getValue("MultiAcceleratorListKey");
                if (mkbList != null && mkbList.size() > 0) {
                    firstMkb = (List)mkbList.get(0);
                    toolTipText = toolTipText + " (" + EditorActionUtilities.getKeyMnemonic(firstMkb) + ")";
                } else {
                    KeyStroke accelerator = (KeyStroke)a.getValue("AcceleratorKey");
                    if (accelerator != null) {
                        toolTipText = toolTipText + " (" + EditorActionUtilities.getKeyMnemonic(accelerator) + ")";
                    }
                }
            }
            this.presenter.setToolTipText(toolTipText);
        }
        if (this.useActionSelectedProperty && (propName == null || "SwingSelectedKey".equals(propName)) && this.isMenuItem()) {
            this.presenter.setSelected(this.isActionSelected());
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        JTextComponent c = (JTextComponent)this.presenter.getClientProperty(JTextComponent.class);
        if (c != null) {
            evt = new ActionEvent(c, evt.getID(), evt.getActionCommand(), evt.getWhen(), evt.getModifiers());
        }
        this.activeAction().actionPerformed(evt);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setActiveAction(Action a) {
        if (a == this.action) {
            a = null;
        }
        PresenterUpdater presenterUpdater = this;
        synchronized (presenterUpdater) {
            if (a != this.contextAction) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("setActiveAction(): from " + this.contextAction + " to " + a + "\n");
                }
                this.contextAction = a;
                if (a != null && !this.listenedContextActions.contains(a)) {
                    this.listenedContextActions.add(a);
                    a.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)a));
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("setActiveAction(): started listening on " + a + "\n");
                    }
                }
                this.updatePresenter(null);
            }
        }
    }

    private Action activeAction() {
        return this.contextAction != null ? this.contextAction : this.action;
    }

    private boolean isMenuItem() {
        return this.type == 0 || this.type == 1;
    }

    private final class LazyJCheckBoxMenuItem
    extends JCheckBoxMenuItem {
        private LazyJCheckBoxMenuItem() {
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            PresenterUpdater.this.presenterDeactivated();
        }

        @Override
        public Dimension getPreferredSize() {
            PresenterUpdater.this.presenterActivated();
            return super.getPreferredSize();
        }
    }

    private final class LazyJMenuItem
    extends JMenuItem {
        private LazyJMenuItem() {
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            PresenterUpdater.this.presenterDeactivated();
        }

        @Override
        public Dimension getPreferredSize() {
            PresenterUpdater.this.presenterActivated();
            return super.getPreferredSize();
        }
    }

}

