/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.lib2;

import java.util.Collection;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.lib2.EditorImplementationProvider;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class EditorImplementation {
    private static final Logger LOG = Logger.getLogger(EditorImplementation.class.getName());
    private static final EditorImplementationProvider DEFAULT = new DefaultImplementationProvider();
    private static EditorImplementation instance = null;
    private static EditorImplementationProvider externalProvider = null;
    private Lookup.Result<EditorImplementationProvider> result = Lookup.getDefault().lookup(new Lookup.Template(EditorImplementationProvider.class));

    public static synchronized EditorImplementation getDefault() {
        if (instance == null) {
            instance = new EditorImplementation();
        }
        return instance;
    }

    public void setExternalProvider(EditorImplementationProvider provider) {
        externalProvider = provider;
    }

    public ResourceBundle getResourceBundle(String localizer) {
        return this.getProvider().getResourceBundle(localizer);
    }

    public Action[] getGlyphGutterActions(JTextComponent target) {
        return this.getProvider().getGlyphGutterActions(target);
    }

    public boolean activateComponent(JTextComponent c) {
        return this.getProvider().activateComponent(c);
    }

    private EditorImplementation() {
    }

    private EditorImplementationProvider getProvider() {
        if (externalProvider != null) {
            return externalProvider;
        }
        Collection providers = this.result.allInstances();
        if (providers.isEmpty()) {
            LOG.warning("Can't find any EditorImplementationProvider; using default.");
            return DEFAULT;
        }
        return (EditorImplementationProvider)providers.iterator().next();
    }

    private static final class DefaultImplementationProvider
    implements EditorImplementationProvider {
        private static final Action[] NOACTIONS = new Action[0];

        private DefaultImplementationProvider() {
        }

        @Override
        public ResourceBundle getResourceBundle(String localizer) {
            return NbBundle.getBundle((String)localizer);
        }

        @Override
        public Action[] getGlyphGutterActions(JTextComponent target) {
            return NOACTIONS;
        }

        @Override
        public boolean activateComponent(JTextComponent c) {
            return false;
        }
    }

}

