/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import org.netbeans.modules.editor.lib2.highlighting.AbstractOffsetGapList;

public final class OffsetGapList<E extends Offset>
extends AbstractOffsetGapList<E> {
    public OffsetGapList() {
    }

    public OffsetGapList(boolean fixedZeroOffset) {
        super(fixedZeroOffset);
    }

    @Override
    protected int elementRawOffset(E elem) {
        return elem.getRawOffset();
    }

    @Override
    protected void setElementRawOffset(E elem, int rawOffset) {
        elem.setRawOffset(rawOffset);
    }

    @Override
    protected int attachElement(E elem) {
        return elem.attach(this);
    }

    @Override
    protected void detachElement(E elem) {
        elem.detach(this);
    }

    @Override
    protected E getAttachedElement(Object o) {
        if (o instanceof Offset && ((Offset)o).checkOwner(this)) {
            Offset element = (Offset)o;
            return (E)element;
        }
        return null;
    }

    public static class Offset {
        private int originalOrRawOffset;
        private OffsetGapList list;

        public Offset(int offset) {
            this.originalOrRawOffset = offset;
        }

        public final int getOffset() {
            if (this.list == null) {
                return this.originalOrRawOffset;
            }
            return this.list.raw2Offset(this.getRawOffset());
        }

        int attach(OffsetGapList list) {
            assert (this.list == null);
            this.list = list;
            return this.originalOrRawOffset;
        }

        void detach(OffsetGapList list) {
            assert (this.list == list);
            this.list = null;
        }

        private boolean checkOwner(OffsetGapList list) {
            return this.list == list;
        }

        int getRawOffset() {
            return this.originalOrRawOffset;
        }

        void setRawOffset(int rawOffset) {
            this.originalOrRawOffset = rawOffset;
        }
    }

}

