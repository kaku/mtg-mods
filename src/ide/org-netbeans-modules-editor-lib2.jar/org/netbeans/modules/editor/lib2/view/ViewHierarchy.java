/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.view;

import javax.swing.text.JTextComponent;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewApiPackageAccessor;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyListener;

public final class ViewHierarchy {
    private final ViewHierarchyImpl impl;

    @NonNull
    public static ViewHierarchy get(@NonNull JTextComponent component) {
        return ViewHierarchyImpl.get(component).viewHierarchy();
    }

    ViewHierarchy(ViewHierarchyImpl impl) {
        this.impl = impl;
    }

    @NonNull
    public JTextComponent getTextComponent() {
        return this.impl.textComponent();
    }

    public LockedViewHierarchy lock() {
        return this.impl.lock();
    }

    public void addViewHierarchyListener(@NonNull ViewHierarchyListener l) {
        this.impl.addViewHierarchyListener(l);
    }

    public void removeViewHierarchyListener(@NonNull ViewHierarchyListener l) {
        this.impl.removeViewHierarchyListener(l);
    }

    public String toString() {
        return this.impl.toString();
    }

    static {
        ViewApiPackageAccessor.register(new PackageAccessor());
    }

    private static final class PackageAccessor
    extends ViewApiPackageAccessor {
        private PackageAccessor() {
        }

        @Override
        public ViewHierarchy createViewHierarchy(ViewHierarchyImpl impl) {
            return new ViewHierarchy(impl);
        }

        @Override
        public LockedViewHierarchy createLockedViewHierarchy(ViewHierarchyImpl impl) {
            return new LockedViewHierarchy(impl);
        }

        @Override
        public ViewHierarchyEvent createEvent(ViewHierarchy viewHierarchy, ViewHierarchyChange change) {
            return new ViewHierarchyEvent(viewHierarchy, change);
        }
    }

}

