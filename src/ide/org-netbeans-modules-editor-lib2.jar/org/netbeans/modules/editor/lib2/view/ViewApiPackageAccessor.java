/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.lib2.view;

import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.openide.util.Exceptions;

public abstract class ViewApiPackageAccessor {
    private static ViewApiPackageAccessor INSTANCE;

    public static ViewApiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName(ViewHierarchy.class.getName(), true, ViewApiPackageAccessor.class.getClassLoader());
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        return INSTANCE;
    }

    public static void register(ViewApiPackageAccessor accessor) {
        INSTANCE = accessor;
    }

    public abstract ViewHierarchy createViewHierarchy(ViewHierarchyImpl var1);

    public abstract LockedViewHierarchy createLockedViewHierarchy(ViewHierarchyImpl var1);

    public abstract ViewHierarchyEvent createEvent(ViewHierarchy var1, ViewHierarchyChange var2);
}

