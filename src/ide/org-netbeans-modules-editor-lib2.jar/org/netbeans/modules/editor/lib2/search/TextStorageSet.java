/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.CharSubSequence
 *  org.netbeans.lib.editor.util.CharSubSequence$StringLike
 */
package org.netbeans.modules.editor.lib2.search;

import java.util.HashMap;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.CharSubSequence;

public final class TextStorageSet {
    private final CharSequencesMap textMap = new CharSequencesMap();

    public CharSequence add(CharSequence text) {
        return this.textMap.put(text, text);
    }

    public CharSequence get(CharSequence text) {
        return this.get(text, 0, text.length());
    }

    public CharSequence get(CharSequence text, int startIndex, int endIndex) {
        return this.textMap.get(text, startIndex, endIndex);
    }

    public CharSequence remove(CharSequence text) {
        return this.remove(text, 0, text.length());
    }

    public CharSequence remove(CharSequence text, int startIndex, int endIndex) {
        return this.textMap.remove(text, startIndex, endIndex);
    }

    public int size() {
        return this.textMap.size();
    }

    public void clear() {
        this.textMap.clear();
    }

    private static final class CharSequencesMap
    extends HashMap<CharSequence, CharSequence>
    implements CharSequence {
        CharSequence compareText;
        int compareIndex;
        int compareLength;
        static final long serialVersionUID = 0;

        private CharSequencesMap() {
        }

        public CharSequence get(CharSequence text, int startIndex, int endIndex) {
            this.compareText = text;
            this.compareIndex = startIndex;
            this.compareLength = endIndex - startIndex;
            CharSequence ret = (CharSequence)this.get(this);
            this.compareText = null;
            return ret;
        }

        public boolean containsKey(CharSequence text, int startIndex, int endIndex) {
            this.compareText = text;
            this.compareIndex = startIndex;
            this.compareLength = endIndex - startIndex;
            boolean ret = this.containsKey(this);
            this.compareText = null;
            return ret;
        }

        public CharSequence remove(CharSequence text, int startIndex, int endIndex) {
            this.compareText = text;
            this.compareIndex = startIndex;
            this.compareLength = endIndex - startIndex;
            CharSequence ret = (CharSequence)this.remove(this);
            this.compareText = null;
            return ret;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o instanceof CharSequence) {
                CharSequence text = (CharSequence)o;
                if (this.compareLength == text.length()) {
                    for (int index = this.compareLength - 1; index >= 0; --index) {
                        if (this.compareText.charAt(this.compareIndex + index) == text.charAt(index)) continue;
                        return false;
                    }
                    return true;
                }
                return false;
            }
            return false;
        }

        @Override
        public int hashCode() {
            int h = 0;
            CharSequence text = this.compareText;
            int endIndex = this.compareIndex + this.compareLength;
            for (int i = this.compareIndex; i < endIndex; ++i) {
                h = 31 * h + text.charAt(i);
            }
            return h;
        }

        @Override
        public int length() {
            return this.compareLength;
        }

        @Override
        public char charAt(int index) {
            CharSequenceUtilities.checkIndexValid((int)index, (int)this.length());
            return this.compareText.charAt(this.compareIndex + index);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return new CharSubSequence.StringLike((CharSequence)this, start, end);
        }
    }

}

