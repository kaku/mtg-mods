/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.actions;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import javax.swing.plaf.TextUI;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.lib2.WeakReferenceStableList;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.lib2.actions.PresenterUpdater;
import org.netbeans.modules.editor.lib2.actions.SearchableEditorKit;

public class EditorRegistryWatcher
implements PropertyChangeListener {
    private static final EditorRegistryWatcher INSTANCE = new EditorRegistryWatcher();
    private static final Logger LOG = Logger.getLogger(EditorRegistryWatcher.class.getName());
    private WeakReferenceStableList<PresenterUpdater> presenterUpdaters = new WeakReferenceStableList();
    private Reference<JTextComponent> activeTextComponentRef;

    public static EditorRegistryWatcher get() {
        return INSTANCE;
    }

    private EditorRegistryWatcher() {
        EditorRegistry.addPropertyChangeListener(this);
        this.activeTextComponentRef = new WeakReference<JTextComponent>(EditorRegistry.focusedComponent());
    }

    public void registerPresenterUpdater(PresenterUpdater updater) {
        this.presenterUpdaters.add(updater);
        JTextComponent activeTextComponent = this.activeTextComponentRef.get();
        if (activeTextComponent != null) {
            EditorKit kit = activeTextComponent.getUI().getEditorKit(activeTextComponent);
            updater.setActiveAction(EditorActionUtilities.getAction(kit, updater.getActionName()));
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if (!"focusLost".equals(propName) && "focusGained".equals(propName)) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("EditorRegistryWatcher: EditorRegistry.FOCUS_GAINED\n");
            }
            this.updateActiveActionInPresenters((JTextComponent)evt.getNewValue());
        }
    }

    private void updateActiveActionInPresenters(JTextComponent c) {
        if (c == this.activeTextComponentRef.get()) {
            return;
        }
        this.activeTextComponentRef = new WeakReference<JTextComponent>(c);
        EditorKit kit = c != null ? c.getUI().getEditorKit(c) : null;
        SearchableEditorKit searchableKit = kit != null ? EditorActionUtilities.getSearchableKit(kit) : null;
        for (PresenterUpdater updater : this.presenterUpdaters.getList()) {
            Action a = searchableKit != null ? searchableKit.getAction(updater.getActionName()) : null;
            updater.setActiveAction(a);
        }
    }

    public void notifyActiveTopComponentChanged(Component activeTopComponent) {
        JTextComponent activeTextComponent;
        if (activeTopComponent != null && (activeTextComponent = this.activeTextComponentRef.get()) != null && !SwingUtilities.isDescendingFrom(activeTextComponent, activeTopComponent)) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("EditorRegistryWatcher: TopComponent without active JTextComponent\n");
            }
            this.updateActiveActionInPresenters(null);
        }
    }
}

