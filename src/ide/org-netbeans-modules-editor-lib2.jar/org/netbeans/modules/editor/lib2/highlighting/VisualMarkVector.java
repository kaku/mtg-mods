/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.modules.editor.lib2.highlighting.VisualMark;

public class VisualMarkVector<M extends VisualMark> {
    private static final Logger LOG = Logger.getLogger(VisualMarkVector.class.getName());
    private static final double INITIAL_Y_GAP_LENGTH = 2.147483647E9;
    private double yGapStart = 2.147483647E9;
    private double yGapLength = 2.147483647E9;
    int yGapIndex = 0;
    private GapList<M> markList = new GapList();

    VisualMarkVector() {
    }

    public final int markCount() {
        return this.markList.size();
    }

    public final M getMark(int index) {
        return (M)((VisualMark)this.markList.get(index));
    }

    double raw2Y(double rawY) {
        return rawY < this.yGapStart ? rawY : rawY - this.yGapLength;
    }

    private void moveVisualGap(int index) {
        if (LOG.isLoggable(Level.FINE)) {
            this.checkGapConsistency();
        }
        if (index < this.yGapIndex) {
            double lastY = 0.0;
            for (int i = this.yGapIndex - 1; i >= index; --i) {
                M mark = this.getMark(i);
                lastY = mark.rawY();
                mark.setRawY(lastY + this.yGapLength);
            }
            this.yGapStart = lastY;
        } else {
            for (int i = this.yGapIndex; i < index; ++i) {
                M mark = this.getMark(i);
                mark.setRawY(mark.rawY() - this.yGapLength);
            }
            if (index < this.markCount()) {
                M mark = this.getMark(index);
                this.yGapStart = mark.rawY() - this.yGapLength;
            } else {
                this.yGapStart = 2.147483647E9;
            }
        }
        this.yGapIndex = index;
        if (LOG.isLoggable(Level.FINE)) {
            this.checkGapConsistency();
        }
    }

    private void checkGapConsistency() {
        String error = this.gapConsistency();
        if (error != null) {
            throw new IllegalStateException("");
        }
    }

    private String gapConsistency() {
        String error = null;
        for (int i = 0; i < this.markCount(); ++i) {
            M mark = this.getMark(i);
            double rawY = mark.rawY();
            double y = mark.getY();
            if (i < this.yGapIndex) {
                if (rawY >= this.yGapStart) {
                    error = "Not below y-gap: rawY=" + rawY + " >= yGapStart=" + this.yGapStart;
                }
            } else {
                if (rawY < this.yGapStart) {
                    error = "Not above y-gap: rawY=" + rawY + " < yGapStart=" + this.yGapStart;
                }
                if (i == this.yGapIndex && y != this.yGapStart) {
                    error = "y=" + y + " != yGapStart=" + this.yGapStart;
                }
            }
            if (error != null) break;
        }
        return error;
    }

    StringBuilder appendInfo(StringBuilder sb) {
        int markCount = this.markList.size();
        int digitCount = ArrayUtilities.digitCount((int)markCount);
        for (int i = 0; i < markCount; ++i) {
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)2);
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            VisualMark mark = (VisualMark)this.markList.get(i);
            sb.append(": ").append(mark);
            sb.append('\n');
        }
        return sb;
    }

    public String toStringDetail() {
        return this.appendInfo(new StringBuilder().append(this.toString())).toString();
    }

    public String toString() {
        return new StringBuilder(80).append(", vis<").append(this.yGapStart).append("|").append(this.yGapLength).append(">\n").toString();
    }
}

