/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextHitInfo;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.Bidi;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

final class TextLayoutUtils {
    private static final Logger LOG = Logger.getLogger(TextLayoutUtils.class.getName());

    private TextLayoutUtils() {
    }

    public static float getHeight(TextLayout textLayout) {
        float height = textLayout.getAscent() + textLayout.getDescent() + textLayout.getLeading();
        return (float)Math.ceil(height);
    }

    public static float getWidth(TextLayout textLayout, String textLayoutText, Font font) {
        float width;
        int tlLen = textLayoutText.length();
        if (!font.isItalic() || tlLen == 0 || Character.isWhitespace(textLayoutText.charAt(tlLen - 1)) || Bidi.requiresBidi(textLayoutText.toCharArray(), 0, textLayoutText.length())) {
            width = textLayout.getAdvance();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("TLUtils.getWidth(\"" + CharSequenceUtilities.debugText((CharSequence)textLayoutText) + "\"): Using TL.getAdvance()=" + width + '\n');
            }
        } else {
            Rectangle pixelBounds = textLayout.getPixelBounds(null, 0.0f, 0.0f);
            width = (float)pixelBounds.getMaxX();
            float tlAdvance = textLayout.getAdvance();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("TLUtils.getWidth(\"" + CharSequenceUtilities.debugText((CharSequence)textLayoutText) + "\"): Using minimum of TL.getPixelBounds().getMaxX()=" + width + " or TL.getAdvance()=" + tlAdvance + TextLayoutUtils.textLayoutDump(textLayout) + '\n');
            }
            width = Math.min(width, tlAdvance);
        }
        width = (float)Math.ceil(width);
        return width;
    }

    private static String textLayoutDump(TextLayout textLayout) {
        return "\n  TL.getAdvance()=" + textLayout.getAdvance() + "\n  TL.getBounds()=" + textLayout.getBounds() + "\n  TL: " + textLayout;
    }

    public static Shape getRealAlloc(TextLayout textLayout, Rectangle2D textLayoutRect, TextHitInfo startHit, TextHitInfo endHit) {
        textLayoutRect = new Rectangle2D.Double(textLayoutRect.getX(), textLayoutRect.getY(), textLayoutRect.getWidth() + 2.0, textLayoutRect.getHeight());
        Rectangle2D.Double zeroBasedRect = ViewUtils.shape2Bounds(textLayoutRect);
        zeroBasedRect.x = 0.0;
        zeroBasedRect.y = 0.0;
        Shape ret = textLayout.getVisualHighlightShape(startHit, endHit, zeroBasedRect);
        AffineTransform transform = AffineTransform.getTranslateInstance(textLayoutRect.getX(), textLayoutRect.getY());
        ret = transform.createTransformedShape(ret);
        return ret;
    }

    public static String toStringShort(TextLayout textLayout) {
        return "[" + textLayout.getCharacterCount() + "]W=" + textLayout.getAdvance();
    }

    public static String toString(TextLayout textLayout) {
        return TextLayoutUtils.toStringShort(textLayout) + "; " + textLayout.toString();
    }
}

