/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ListenerList
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.modules.editor.lib2.actions;

import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultEditorKit;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.lib2.actions.SearchableEditorKit;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class SearchableEditorKitImpl
extends DefaultEditorKit
implements SearchableEditorKit {
    private static final Logger LOG = Logger.getLogger(SearchableEditorKitImpl.class.getName());
    private final String mimeType;
    private final Map<String, Action> name2Action = new HashMap<String, Action>();
    private Action[] actions;
    private LookupListener actionsListener;
    private ListenerList<ChangeListener> listenerList = new ListenerList();

    SearchableEditorKitImpl(String mimeType) {
        this.mimeType = mimeType;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("SearchableEditorKitImpl created for \"" + mimeType + "\"\n");
        }
        this.updateActions();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Action getAction(String actionName) {
        Map<String, Action> map = this.name2Action;
        synchronized (map) {
            return this.name2Action.get(actionName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateActions() {
        Map<String, Action> map = this.name2Action;
        synchronized (map) {
            Lookup.Result<Action> actionsResult = EditorActionUtilities.createActionsLookupResult(this.mimeType);
            Collection actionColl = actionsResult.allInstances();
            this.actions = new Action[actionColl.size()];
            actionColl.toArray(this.actions);
            this.name2Action.clear();
            for (Action action : this.actions) {
                String actionName;
                if (action == null || (actionName = (String)action.getValue("Name")) == null) continue;
                this.name2Action.put(actionName, action);
                if (!LOG.isLoggable(Level.FINER)) continue;
                LOG.finer("Mime-type: \"" + this.mimeType + "\", registerAction(\"" + actionName + "\", " + action + ")\n");
            }
            if (this.actionsListener == null) {
                this.actionsListener = new LookupListener(){

                    public void resultChanged(LookupEvent ev) {
                        SearchableEditorKitImpl.this.updateActions();
                    }
                };
                actionsResult.addLookupListener(this.actionsListener);
            }
        }
        this.fireActionsChange();
    }

    @Override
    public String getContentType() {
        return this.mimeType;
    }

    @Override
    public void addActionsChangeListener(ChangeListener listener) {
        this.listenerList.add((EventListener)listener);
    }

    @Override
    public void removeActionsChangeListener(ChangeListener listener) {
        this.listenerList.remove((EventListener)listener);
    }

    private void fireActionsChange() {
        ChangeEvent evt = new ChangeEvent(this);
        for (ChangeListener listener : this.listenerList.getListeners()) {
            listener.stateChanged(evt);
        }
    }

}

