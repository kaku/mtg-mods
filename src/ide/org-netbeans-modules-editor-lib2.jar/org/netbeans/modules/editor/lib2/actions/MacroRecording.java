/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.lib2.actions;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.openide.util.Utilities;

public final class MacroRecording {
    public static final String NO_MACRO_RECORDING_PROPERTY = "NoMacroRecording";
    private static final MacroRecording INSTANCE = new MacroRecording();
    private StringBuilder macroBuffer;
    private StringBuilder textBuffer;

    public static MacroRecording get() {
        return INSTANCE;
    }

    private MacroRecording() {
    }

    public synchronized boolean startRecording() {
        if (this.isRecording()) {
            return false;
        }
        this.macroBuffer = new StringBuilder(100);
        this.textBuffer = new StringBuilder(20);
        return true;
    }

    public synchronized String stopRecording() {
        if (!this.isRecording()) {
            return null;
        }
        if (this.textBuffer.length() > 0) {
            if (this.macroBuffer.length() > 0) {
                this.macroBuffer.append(' ');
            }
            MacroRecording.appendEncodedText(this.macroBuffer, this.textBuffer);
        }
        String completeMacroText = this.macroBuffer.toString();
        this.textBuffer = null;
        this.macroBuffer = null;
        return completeMacroText;
    }

    public synchronized void recordAction(Action action, ActionEvent evt, JTextComponent target) {
        if (!this.isRecording() || Boolean.TRUE.equals(action.getValue("NoMacroRecording"))) {
            return;
        }
        String actionName = MacroRecording.actionName(action);
        if (action == target.getKeymap().getDefaultAction()) {
            if (MacroRecording.isValidDefaultTypedAction(evt) && MacroRecording.isValidDefaultTypedCommand(evt)) {
                this.textBuffer.append(evt.getActionCommand());
            }
        } else {
            if (this.textBuffer.length() > 0) {
                if (this.macroBuffer.length() > 0) {
                    this.macroBuffer.append(' ');
                }
                MacroRecording.appendEncodedText(this.macroBuffer, this.textBuffer);
                this.textBuffer.setLength(0);
            }
            if (this.macroBuffer.length() > 0) {
                this.macroBuffer.append(' ');
            }
            for (int i = 0; i < actionName.length(); ++i) {
                char c = actionName.charAt(i);
                if (Character.isWhitespace(c) || c == '\\') {
                    this.macroBuffer.append('\\');
                }
                this.macroBuffer.append(c);
            }
        }
    }

    static boolean isValidDefaultTypedAction(ActionEvent evt) {
        boolean ctrl;
        int mod = evt.getModifiers();
        boolean bl = ctrl = (mod & 2) != 0;
        boolean alt = Utilities.isMac() ? (mod & 4) != 0 : (mod & 8) != 0;
        return !alt && !ctrl;
    }

    static boolean isValidDefaultTypedCommand(ActionEvent evt) {
        String cmd = evt.getActionCommand();
        return cmd != null && cmd.length() == 1 && cmd.charAt(0) >= ' ' && cmd.charAt(0) != '';
    }

    private boolean isRecording() {
        return this.macroBuffer != null;
    }

    private static String actionName(Action action) {
        return (String)action.getValue("Name");
    }

    private static String getFilteredActionCommand(String cmd) {
        if (cmd == null || cmd.length() == 0) {
            return "";
        }
        char ch = cmd.charAt(0);
        if (ch >= ' ' && ch != '') {
            return cmd;
        }
        return "";
    }

    private static void appendEncodedText(StringBuilder sb, StringBuilder text) {
        sb.append('\"');
        for (int i = 0; i < text.length(); ++i) {
            char c = text.charAt(i);
            if (c == '\"' || c == '\\') {
                sb.append('\\');
            }
            sb.append(c);
        }
        sb.append('\"');
    }
}

