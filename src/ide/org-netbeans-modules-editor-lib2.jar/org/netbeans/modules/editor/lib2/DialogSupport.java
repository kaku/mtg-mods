/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.modules.editor.lib2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.modules.editor.lib2.DialogFactory;
import org.openide.util.Lookup;

public final class DialogSupport {
    private static DialogSupport instance;
    private DialogFactory externalFactory;
    private Lookup.Result<DialogFactory> result = Lookup.getDefault().lookup(new Lookup.Template(DialogFactory.class));

    public static synchronized DialogSupport getInstance() {
        if (instance == null) {
            instance = new DialogSupport();
        }
        return instance;
    }

    private DialogSupport() {
    }

    public Dialog createDialog(String title, JPanel panel, boolean modal, JButton[] buttons, boolean sidebuttons, int defaultIndex, int cancelIndex, ActionListener listener) {
        Collection factories;
        DialogFactory factory = null;
        factory = this.externalFactory != null ? this.externalFactory : ((factories = this.result.allInstances()).isEmpty() ? new DefaultDialogFactory() : (DialogFactory)factories.iterator().next());
        return factory.createDialog(title, panel, modal, buttons, sidebuttons, defaultIndex, cancelIndex, listener);
    }

    public void setExternalDialogFactory(DialogFactory factory) {
        this.externalFactory = factory;
    }

    private static class DefaultDialogFactory
    extends WindowAdapter
    implements DialogFactory,
    ActionListener {
        private JButton cancelButton;

        private DefaultDialogFactory() {
        }

        private JPanel createButtonPanel(JButton[] buttons, boolean sidebuttons) {
            int count = buttons.length;
            JPanel outerPanel = new JPanel(new BorderLayout());
            outerPanel.setBorder(new EmptyBorder(new Insets(sidebuttons ? 5 : 0, sidebuttons ? 0 : 5, 5, 5)));
            GridLayout lm = new GridLayout(sidebuttons ? count : 1, sidebuttons ? 1 : count, 5, 5);
            JPanel innerPanel = new JPanel(lm);
            for (int i = 0; i < count; ++i) {
                innerPanel.add(buttons[i]);
            }
            outerPanel.add((Component)innerPanel, sidebuttons ? "North" : "East");
            return outerPanel;
        }

        @Override
        public Dialog createDialog(String title, JPanel panel, boolean modal, JButton[] buttons, boolean sidebuttons, int defaultIndex, int cancelIndex, ActionListener listener) {
            JDialog d = new JDialog((Frame)null, title, modal);
            d.setDefaultCloseOperation(0);
            d.getContentPane().add((Component)panel, "Center");
            JPanel buttonPanel = this.createButtonPanel(buttons, sidebuttons);
            String buttonAlign = sidebuttons ? "East" : "South";
            d.getContentPane().add((Component)buttonPanel, buttonAlign);
            if (listener != null) {
                for (int i = 0; i < buttons.length; ++i) {
                    buttons[i].addActionListener(listener);
                }
            }
            if (defaultIndex >= 0) {
                d.getRootPane().setDefaultButton(buttons[defaultIndex]);
            }
            if (cancelIndex >= 0) {
                this.cancelButton = buttons[cancelIndex];
                d.getRootPane().registerKeyboardAction(this, KeyStroke.getKeyStroke(27, 0, true), 2);
                d.addWindowListener(this);
            }
            d.pack();
            return d;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            this.cancelButton.doClick(10);
        }

        @Override
        public void windowClosing(WindowEvent evt) {
            this.cancelButton.doClick(10);
        }
    }

}

