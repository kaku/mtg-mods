/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import javax.swing.undo.UndoableEdit;
import org.netbeans.spi.editor.document.OnSaveTask;
import org.openide.util.Exceptions;

public abstract class DocumentSpiPackageAccessor {
    private static DocumentSpiPackageAccessor INSTANCE;

    public static DocumentSpiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName(OnSaveTask.Context.class.getName(), true, DocumentSpiPackageAccessor.class.getClassLoader());
            }
            catch (ClassNotFoundException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
            assert (INSTANCE != null);
        }
        return INSTANCE;
    }

    public static void register(DocumentSpiPackageAccessor accessor) {
        INSTANCE = accessor;
    }

    public abstract OnSaveTask.Context createContext(Document var1);

    public abstract void setUndoEdit(OnSaveTask.Context var1, UndoableEdit var2);

    public abstract void setTaskStarted(OnSaveTask.Context var1, boolean var2);
}

