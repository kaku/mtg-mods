/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewRenderContext;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public abstract class EditorView
extends View {
    private static final Logger LOG = Logger.getLogger(EditorView.class.getName());
    private double rawEndVisualOffset;

    public EditorView(Element element) {
        super(element);
    }

    public abstract int getRawEndOffset();

    public abstract void setRawEndOffset(int var1);

    public int getLength() {
        return this.getEndOffset() - this.getStartOffset();
    }

    public final double getRawEndVisualOffset() {
        return this.rawEndVisualOffset;
    }

    public final void setRawEndVisualOffset(double rawEndVisualOffset) {
        this.rawEndVisualOffset = rawEndVisualOffset;
    }

    public abstract void paint(Graphics2D var1, Shape var2, Rectangle var3);

    @Override
    public final void paint(Graphics g, Shape alloc) {
        if (alloc != null && g instanceof Graphics2D) {
            this.paint((Graphics2D)g, alloc, g.getClipBounds());
        }
    }

    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        try {
            return super.getNextVisualPositionFrom(offset, bias, alloc, direction, biasRet);
        }
        catch (BadLocationException e) {
            return this.getStartOffset();
        }
    }

    @Override
    public final int getNextVisualPositionFrom(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) throws BadLocationException {
        if (offset != -1) {
            this.checkBounds(offset);
        }
        this.checkBias(bias);
        if (alloc != null) {
            offset = this.getNextVisualPositionFromChecked(offset, bias, alloc, direction, biasRet);
        }
        return offset;
    }

    public abstract Shape modelToViewChecked(int var1, Shape var2, Position.Bias var3);

    @Override
    public final Shape modelToView(int offset, Shape alloc, Position.Bias bias) throws BadLocationException {
        this.checkBounds(offset);
        this.checkBias(bias);
        if (alloc != null) {
            return this.modelToViewChecked(offset, alloc, bias);
        }
        return null;
    }

    public Shape modelToViewChecked(int offset0, Position.Bias bias0, int offset1, Position.Bias bias1, Shape alloc) {
        Shape start = this.modelToViewChecked(offset0, alloc, bias0);
        Shape end = this.modelToViewChecked(offset1, alloc, bias1);
        Rectangle2D.Double mutableBounds = null;
        if (start != null) {
            mutableBounds = ViewUtils.shape2Bounds(start);
            if (end != null) {
                Rectangle2D endRect = ViewUtils.shapeAsRect(end);
                if (mutableBounds.getY() != endRect.getY()) {
                    Rectangle2D allocRect = ViewUtils.shapeAsRect(alloc);
                    mutableBounds.x = allocRect.getX();
                    mutableBounds.width = allocRect.getWidth();
                }
                mutableBounds.add(endRect);
            }
        }
        return mutableBounds;
    }

    @Override
    public final Shape modelToView(int offset0, Position.Bias bias0, int offset1, Position.Bias bias1, Shape alloc) throws BadLocationException {
        this.checkBounds(offset0);
        this.checkBias(bias0);
        this.checkBounds(offset1);
        this.checkBias(bias1);
        if (alloc != null) {
            return this.modelToViewChecked(offset0, bias0, offset1, bias1, alloc);
        }
        return null;
    }

    public abstract int viewToModelChecked(double var1, double var3, Shape var5, Position.Bias[] var6);

    @Override
    public final int viewToModel(float x, float y, Shape alloc, Position.Bias[] biasReturn) {
        if (alloc != null) {
            return this.viewToModelChecked(x, y, alloc, biasReturn);
        }
        return this.getStartOffset();
    }

    public String getToolTipTextChecked(double x, double y, Shape allocation) {
        return null;
    }

    @Override
    public String getToolTipText(float x, float y, Shape allocation) {
        return this.getToolTipTextChecked(x, y, allocation);
    }

    public JComponent getToolTip(double x, double y, Shape allocation) {
        return null;
    }

    public int getViewIndexChecked(double x, double y, Shape alloc) {
        return -1;
    }

    @Override
    public int getViewIndex(float x, float y, Shape alloc) {
        if (alloc != null) {
            return this.getViewIndexChecked(x, y, alloc);
        }
        return -1;
    }

    @Override
    public Document getDocument() {
        View parent = this.getParent();
        return parent != null ? parent.getDocument() : null;
    }

    protected String getDumpName() {
        return "EV";
    }

    public final String getDumpId() {
        int viewCount;
        return this.getDumpName() + "@" + ViewUtils.toStringId(this) + ((viewCount = this.getViewCount()) > 0 ? new StringBuilder().append("#").append(this.getViewCount()).toString() : "");
    }

    public void checkIntegrityIfLoggable() {
        if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINE)) {
            this.checkIntegrity();
        }
    }

    public void checkIntegrity() {
        String err = this.findTreeIntegrityError();
        if (err != null) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("View hierarchy INTEGRITY ERROR! - ").append(err);
            sb.append("\nErrorneous view hierarchy:\n");
            this.appendViewInfo(sb, 0, "", -2);
            if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINEST)) {
                throw new IllegalStateException(sb.toString());
            }
            ViewHierarchyImpl.CHECK_LOG.log(Level.INFO, sb.toString(), new Exception());
        }
    }

    public String findIntegrityError() {
        String err = null;
        if (this.getStartOffset() + this.getLength() != this.getEndOffset()) {
            err = "getStartOffset()=" + this.getStartOffset() + " + getLength()=" + this.getLength() + " != getEndOffset()=" + this.getEndOffset();
        }
        return err;
    }

    public String findTreeIntegrityError() {
        String err = this.findIntegrityError();
        if (err == null) {
            int viewCount = this.getViewCount();
            int startOffset = this.getStartOffset();
            int endOffset = this.getEndOffset();
            int lastOffset = startOffset;
            for (int i = 0; i < viewCount; ++i) {
                EditorView child = (EditorView)this.getView(i);
                if (child.getParent() != this) {
                    err = "child.getParent() != this";
                }
                if (err == null) {
                    int childViewCount = child.getViewCount();
                    for (int j = 0; j < childViewCount; ++j) {
                        EditorView childChild = (EditorView)child.getView(j);
                        EditorView childChildParent = (EditorView)childChild.getParent();
                        if (childChildParent == child) continue;
                        String ccpStr = childChildParent != null ? childChildParent.getDumpId() : "<NULL>";
                        err = "childChild[" + j + "].getParent()=" + ccpStr + " != child=" + child.getDumpId();
                        break;
                    }
                }
                int childStartOffset = child.getStartOffset();
                int childEndOffset = child.getEndOffset();
                boolean noChildInfo = false;
                if (err == null) {
                    if (childStartOffset != lastOffset) {
                        err = "childStartOffset=" + childStartOffset + ", lastOffset=" + lastOffset;
                    } else if (childStartOffset < 0) {
                        err = "childStartOffset=" + childStartOffset + " < 0";
                    } else if (childStartOffset > childEndOffset) {
                        err = "childStartOffset=" + childStartOffset + " > childEndOffset=" + childEndOffset;
                    } else if (childEndOffset > endOffset) {
                        err = "childEndOffset=" + childEndOffset + " > parentEndOffset=" + endOffset;
                    } else {
                        err = child.findTreeIntegrityError();
                        noChildInfo = true;
                    }
                }
                if (err != null) {
                    return this.getDumpId() + "[" + i + "]=" + (noChildInfo ? "" : new StringBuilder().append(child.getDumpId()).append(": ").toString()) + err + '\n';
                }
                lastOffset = childEndOffset;
            }
            if (viewCount > 0 && lastOffset != endOffset) {
                err = "lastChild.getEndOffset()=" + lastOffset + " != endOffset=" + endOffset;
            }
        }
        return err;
    }

    protected StringBuilder appendViewInfo(StringBuilder sb, int indent, String xyInfo, int importantChildIndex) {
        sb.append(this.getDumpId()).append(" ");
        int startOffset = this.getStartOffset();
        int endOffset = this.getEndOffset();
        sb.append('<').append(startOffset);
        sb.append(',');
        sb.append(endOffset).append('>');
        sb.append(xyInfo);
        return sb;
    }

    private void checkBounds(int offset) throws BadLocationException {
        Document doc = this.getDocument();
        if (offset < 0 || offset > doc.getLength() + 1) {
            throw new BadLocationException("Invalid offset=" + offset + ", docLen=" + doc.getLength(), offset);
        }
    }

    private void checkBias(Position.Bias bias) {
        if (bias == null) {
            throw new IllegalArgumentException("Null bias prohibited.");
        }
    }

    public static interface Parent {
        public int getViewEndOffset(int var1);

        public ViewRenderContext getViewRenderContext();
    }

}

