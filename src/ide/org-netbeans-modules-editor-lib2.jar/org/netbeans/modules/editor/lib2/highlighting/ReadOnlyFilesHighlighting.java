/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.editor.lib2.highlighting.CaretBasedBlockHighlighting;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.util.WeakListeners;

public final class ReadOnlyFilesHighlighting
extends AbstractHighlightsContainer
implements FileChangeListener {
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.lib2.highlighting.ReadOnlyFilesHighlighting";
    private static final AttributeSet EXTENDS_EOL_OR_EMPTY_ATTR_SET = AttributesUtilities.createImmutable((Object[])new Object[]{"org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EMPTY_LINE", Boolean.TRUE, "org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", Boolean.TRUE});
    private static final Logger LOG = Logger.getLogger(ReadOnlyFilesHighlighting.class.getName());
    private final Document document;
    private final AttributeSet attribs;
    private boolean fileReadOnly;
    private WeakReference<FileObject> lastFile;

    public ReadOnlyFilesHighlighting(Document doc) {
        AttributeSet readOnlyFilesColoring;
        this.document = doc;
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FontColorSettings.class);
        this.attribs = fcs != null ? ((readOnlyFilesColoring = fcs.getFontColors("readonly-files")) != null ? AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[]{readOnlyFilesColoring, EXTENDS_EOL_OR_EMPTY_ATTR_SET}) : null) : null;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("~~~ this=" + ReadOnlyFilesHighlighting.s2s(this) + ", doc=" + ReadOnlyFilesHighlighting.s2s(doc) + ", file=" + (Object)ReadOnlyFilesHighlighting.fileFromDoc(doc) + ", attribs=" + this.attribs + (this.attribs != null ? new StringBuilder().append(", bg=").append(this.attribs.getAttribute(StyleConstants.Background)).toString() : ""));
        }
    }

    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        FileObject file = ReadOnlyFilesHighlighting.fileFromDoc(this.document);
        if (this.attribs != null && file != null) {
            this.checkFileStatus(file);
            if (this.fileReadOnly) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Highlighting file " + (Object)file + " in <" + startOffset + ", " + endOffset + ">");
                }
                return new CaretBasedBlockHighlighting.SimpleHighlightsSequence(Math.max(0, startOffset), Math.min(this.document.getLength(), endOffset), this.attribs);
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("No highlights for file " + (Object)file + ", doc=" + ReadOnlyFilesHighlighting.s2s(this.document));
        }
        return HighlightsSequence.EMPTY;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkFileStatus(FileObject fo) {
        boolean update = false;
        ReadOnlyFilesHighlighting readOnlyFilesHighlighting = this;
        synchronized (readOnlyFilesHighlighting) {
            if (this.lastFile == null || this.lastFile.get() != fo) {
                this.lastFile = new WeakReference<FileObject>(fo);
                update = true;
            }
        }
        if (update) {
            fo.addFileChangeListener((FileChangeListener)WeakListeners.create(FileChangeListener.class, (EventListener)((Object)this), (Object)fo));
            boolean readOnly = !fo.canWrite();
            this.updateFileReadOnly(readOnly);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateFileReadOnly(boolean readOnly) {
        boolean fire = false;
        ReadOnlyFilesHighlighting readOnlyFilesHighlighting = this;
        synchronized (readOnlyFilesHighlighting) {
            boolean origReadOnly = this.fileReadOnly;
            this.fileReadOnly = readOnly;
            fire = this.fileReadOnly != origReadOnly;
        }
        if (fire) {
            this.fireHighlightsChange(0, this.document.getLength() + 1);
        }
    }

    public void fileFolderCreated(FileEvent fe) {
    }

    public void fileDataCreated(FileEvent fe) {
    }

    public void fileChanged(FileEvent fe) {
    }

    public void fileDeleted(FileEvent fe) {
    }

    public void fileRenamed(FileRenameEvent fe) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void fileAttributeChanged(FileAttributeEvent fe) {
        if ("DataEditorSupport.read-only.refresh".equals(fe.getName())) {
            ReadOnlyFilesHighlighting readOnlyFilesHighlighting = this;
            synchronized (readOnlyFilesHighlighting) {
                FileObject fo = fe.getFile();
                assert (this.lastFile != null);
                if (this.lastFile.get() == fo) {
                    final boolean readOnly = !fo.canWrite();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            ReadOnlyFilesHighlighting.this.updateFileReadOnly(readOnly);
                        }
                    });
                }
            }
        }
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    private static FileObject fileFromDoc(Document d) {
        Object streamDescription = d.getProperty("stream");
        if (d instanceof FileObject) {
            return (FileObject)d;
        }
        if (streamDescription != null) {
            try {
                Method m = streamDescription.getClass().getMethod("getPrimaryFile", new Class[0]);
                return (FileObject)m.invoke(streamDescription, new Object[0]);
            }
            catch (Exception e) {
                // empty catch block
            }
        }
        return null;
    }

}

