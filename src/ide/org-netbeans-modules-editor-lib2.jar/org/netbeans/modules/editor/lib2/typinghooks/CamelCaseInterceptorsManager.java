/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.typinghooks;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.typinghooks.TypingHooksSpiAccessor;
import org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor;

public final class CamelCaseInterceptorsManager {
    private static final Logger LOG = Logger.getLogger(CamelCaseInterceptorsManager.class.getName());
    private static CamelCaseInterceptorsManager instance;
    private Transaction transaction = null;
    private final Map<MimePath, Reference<Collection<CamelCaseInterceptor>>> cache = new WeakHashMap<MimePath, Reference<Collection<CamelCaseInterceptor>>>();

    static MimePath getMimePath(final Document doc, final int offset) {
        final MimePath[] mimePathR = new MimePath[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                List seqs = TokenHierarchy.get((Document)doc).embeddedTokenSequences(offset, true);
                TokenSequence seq = seqs.isEmpty() ? null : (TokenSequence)seqs.get(seqs.size() - 1);
                seq = seq == null ? TokenHierarchy.get((Document)doc).tokenSequence() : seq;
                mimePathR[0] = seq == null ? MimePath.parse((String)DocumentUtilities.getMimeType((Document)doc)) : MimePath.parse((String)seq.languagePath().mimePath());
            }
        });
        return mimePathR[0];
    }

    public static CamelCaseInterceptorsManager getInstance() {
        if (instance == null) {
            instance = new CamelCaseInterceptorsManager();
        }
        return instance;
    }

    public Transaction openTransaction(JTextComponent c, int offset, boolean backward) {
        CamelCaseInterceptorsManager camelCaseInterceptorsManager = this;
        synchronized (camelCaseInterceptorsManager) {
            if (this.transaction == null) {
                this.transaction = new Transaction(c, offset, backward);
                return this.transaction;
            }
            throw new IllegalStateException("Too many transactions; only one at a time is allowed!");
        }
    }

    private CamelCaseInterceptorsManager() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<? extends CamelCaseInterceptor> getInterceptors(Document doc, int offset) {
        MimePath mimePath = CamelCaseInterceptorsManager.getMimePath(doc, offset);
        Map<MimePath, Reference<Collection<CamelCaseInterceptor>>> map = this.cache;
        synchronized (map) {
            Collection<CamelCaseInterceptor> interceptors;
            Reference<Collection<CamelCaseInterceptor>> ref = this.cache.get((Object)mimePath);
            Collection<CamelCaseInterceptor> collection = interceptors = ref == null ? null : ref.get();
            if (interceptors == null) {
                Collection factories = MimeLookup.getLookup((MimePath)mimePath).lookupAll(CamelCaseInterceptor.Factory.class);
                interceptors = new HashSet<CamelCaseInterceptor>(factories.size());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "CamelCaseInterceptor.Factory instances for {0}:", mimePath.getPath());
                }
                for (CamelCaseInterceptor.Factory f : factories) {
                    CamelCaseInterceptor interceptor = f.createCamelCaseInterceptor(mimePath);
                    if (interceptor != null) {
                        interceptors.add(interceptor);
                    }
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.log(Level.FINE, "    {0} created: {1}", new Object[]{f, interceptor});
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("");
                }
                this.cache.put(mimePath, new SoftReference<Collection<CamelCaseInterceptor>>(interceptors));
            }
            return interceptors;
        }
    }

    public final class Transaction {
        private final CamelCaseInterceptor.MutableContext context;
        private final Collection<? extends CamelCaseInterceptor> interceptors;
        private int phase;

        public boolean beforeChange() {
            for (CamelCaseInterceptor i : this.interceptors) {
                try {
                    if (!i.beforeChange(this.context)) continue;
                    return true;
                }
                catch (BadLocationException e) {
                    LOG.log(Level.INFO, "DeleteWordInterceptor crashed in beforeRemove(): " + i, e);
                    continue;
                }
            }
            ++this.phase;
            return false;
        }

        public Object[] change() {
            Object[] data = null;
            for (CamelCaseInterceptor i : this.interceptors) {
                try {
                    i.change(this.context);
                }
                catch (BadLocationException e) {
                    LOG.log(Level.INFO, "DeleteWordInterceptor crashed in remove(): " + i, e);
                    continue;
                }
                data = TypingHooksSpiAccessor.get().getDwiContextData(this.context);
                if (data == null) continue;
                break;
            }
            ++this.phase;
            return data;
        }

        public void afterChange() {
            for (CamelCaseInterceptor i : this.interceptors) {
                try {
                    i.afterChange(this.context);
                }
                catch (BadLocationException e) {
                    LOG.log(Level.INFO, "DeleteWordInterceptor crashed in afterRemove(): " + i, e);
                }
            }
            ++this.phase;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void close() {
            if (this.phase < 3) {
                for (CamelCaseInterceptor i : this.interceptors) {
                    try {
                        i.cancelled(this.context);
                    }
                    catch (Exception e) {
                        LOG.log(Level.INFO, "DeleteWordInterceptor crashed in cancelled(): " + i, e);
                    }
                }
            }
            CamelCaseInterceptorsManager i$ = CamelCaseInterceptorsManager.this;
            synchronized (i$) {
                CamelCaseInterceptorsManager.this.transaction = null;
            }
        }

        private Transaction(JTextComponent c, int offset, boolean backwardDelete) {
            this.phase = 0;
            this.context = TypingHooksSpiAccessor.get().createDwiContext(c, offset, backwardDelete);
            this.interceptors = CamelCaseInterceptorsManager.this.getInterceptors(c.getDocument(), offset);
        }
    }

}

