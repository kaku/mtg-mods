/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.TabExpander;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;

public final class EditorTabExpander
implements TabExpander {
    private static final Logger LOG = Logger.getLogger(EditorTabExpander.class.getName());
    private final DocumentView documentView;
    private int tabSize;

    public EditorTabExpander(DocumentView documentView) {
        this.documentView = documentView;
        this.updateTabSize();
    }

    void updateTabSize() {
        Integer tabSizeInteger = (Integer)this.documentView.getDocument().getProperty("tab-size");
        this.tabSize = tabSizeInteger != null ? tabSizeInteger : 8;
    }

    @Override
    public float nextTabStop(float x, int tabOffset) {
        float defaultCharWidth = this.documentView.op.getDefaultCharWidth();
        int charIndex = (int)(x / defaultCharWidth);
        charIndex = (charIndex + this.tabSize) / this.tabSize * this.tabSize;
        return (float)charIndex * defaultCharWidth;
    }
}

