/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import javax.swing.text.Document;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;

public interface MultiLayerContainer
extends HighlightsContainer {
    public void setLayers(Document var1, HighlightsContainer[] var2);
}

