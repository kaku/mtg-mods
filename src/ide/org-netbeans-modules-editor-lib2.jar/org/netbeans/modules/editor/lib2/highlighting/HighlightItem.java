/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import javax.swing.text.AttributeSet;

public final class HighlightItem {
    private final int endOffset;
    private final AttributeSet attrs;

    public HighlightItem(int endOffset, AttributeSet attrs) {
        this.endOffset = endOffset;
        this.attrs = attrs;
    }

    public int getEndOffset() {
        return this.endOffset;
    }

    public AttributeSet getAttributes() {
        return this.attrs;
    }

    public String toString() {
        return "<?," + this.endOffset + ">: " + this.attrs;
    }

    public String toString(int startOffset) {
        return "<" + startOffset + "," + this.endOffset + ">: " + this.attrs;
    }
}

