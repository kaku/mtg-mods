/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.event.DocumentEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewChildren;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.ViewRenderContext;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public final class ParagraphView
extends EditorView
implements EditorView.Parent {
    private static final Logger LOG = Logger.getLogger(ParagraphView.class.getName());
    private static final int CHILDREN_VALID = 1;
    private static final int LAYOUT_VALID = 2;
    private static final int CONTAINS_TABABLE_VIEWS = 4;
    private float width;
    private float height;
    private Position startPos;
    private int length;
    ParagraphViewChildren children;
    private int statusBits;

    public ParagraphView(Position startPos) {
        super(null);
        this.setStartPosition(startPos);
    }

    @Override
    public int getStartOffset() {
        return this.startPos.getOffset();
    }

    void setStartPosition(Position startPos) {
        this.startPos = startPos;
    }

    @Override
    public int getEndOffset() {
        return this.getStartOffset() + this.getLength();
    }

    @Override
    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public int getRawEndOffset() {
        return -1;
    }

    @Override
    public void setRawEndOffset(int rawOffset) {
        throw new IllegalStateException("setRawOffset() must not be called on ParagraphView.");
    }

    float getWidth() {
        return this.width;
    }

    void setWidth(float width) {
        this.width = width;
    }

    float getHeight() {
        return this.height;
    }

    void setHeight(float height) {
        this.height = height;
    }

    @Override
    public float getPreferredSpan(int axis) {
        return axis == 0 ? this.width : this.height;
    }

    @Override
    public final int getViewCount() {
        return this.children != null ? this.children.size() : 0;
    }

    @Override
    public View getView(int index) {
        EditorView v = this.children != null ? this.getEditorView(index) : null;
        return v;
    }

    public final EditorView getEditorView(int index) {
        if (index >= this.getViewCount()) {
            throw new IndexOutOfBoundsException("View index=" + index + " >= " + this.getViewCount());
        }
        return (EditorView)this.children.get(index);
    }

    @Override
    public AttributeSet getAttributes() {
        return null;
    }

    @Override
    public void replace(int index, int removeLength, View[] addViews) {
        if (this.children == null) {
            assert (removeLength == 0);
            this.children = new ParagraphViewChildren(addViews.length);
        }
        this.children.replace(this, index, removeLength, addViews);
    }

    boolean checkLayoutUpdate(int pIndex, Shape pAlloc) {
        if (!this.isLayoutValid()) {
            return this.updateLayoutAndScheduleRepaint(pIndex, pAlloc);
        }
        return false;
    }

    boolean updateLayoutAndScheduleRepaint(int pIndex, Shape pAlloc) {
        Rectangle2D pViewRect = ViewUtils.shapeAsRect(pAlloc);
        DocumentView docView = this.getDocumentView();
        this.children.updateLayout(docView, this);
        boolean spanUpdated = false;
        float newWidth = this.children.width();
        float newHeight = this.children.height();
        float origWidth = this.getWidth();
        float origHeight = this.getHeight();
        if (newWidth != origWidth) {
            spanUpdated = true;
            this.setWidth(newWidth);
            docView.children.childWidthUpdated(docView, pIndex, newWidth);
        }
        boolean repaintHeightChange = false;
        double deltaY = newHeight - origHeight;
        if (deltaY != 0.0) {
            spanUpdated = true;
            repaintHeightChange = true;
            this.setHeight(newHeight);
            docView.children.childHeightUpdated(docView, pIndex, newHeight, pViewRect);
        }
        Rectangle visibleRect = docView.op.getVisibleRect();
        docView.op.notifyRepaint(pViewRect.getX(), pViewRect.getY(), visibleRect.getMaxX(), repaintHeightChange ? visibleRect.getMaxY() : pViewRect.getMaxY());
        this.markLayoutValid();
        return spanUpdated;
    }

    @Override
    public void preferenceChanged(View childView, boolean widthChange, boolean heightChange) {
        if (childView == null) {
            View parent = this.getParent();
            if (parent != null) {
                parent.preferenceChanged(this, widthChange, heightChange);
            }
        } else if (this.children != null) {
            this.children.preferenceChanged(this, (EditorView)childView, widthChange, heightChange);
        }
    }

    @Override
    public Shape getChildAllocation(int index, Shape alloc) {
        this.checkChildrenNotNull();
        return this.children.getChildAllocation(index, alloc);
    }

    @Override
    public int getViewIndex(int offset, Position.Bias b) {
        if (b == Position.Bias.Backward) {
            --offset;
        }
        return this.getViewIndex(offset);
    }

    public int getViewIndex(int offset) {
        this.checkChildrenNotNull();
        return this.children.getViewIndex(this, offset);
    }

    int getViewIndexLocalOffset(int localOffset) {
        return this.children.viewIndexFirst(localOffset);
    }

    int getLocalOffset(int index) {
        return this.children.startOffset(index);
    }

    @Override
    public int getViewIndexChecked(double x, double y, Shape alloc) {
        this.checkChildrenNotNull();
        return this.children.getViewIndex(this, x, y, alloc);
    }

    @Override
    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        this.checkChildrenNotNull();
        return this.children.modelToViewChecked(this, offset, alloc, bias);
    }

    @Override
    public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        this.checkChildrenNotNull();
        return this.children.viewToModelChecked(this, x, y, alloc, biasReturn);
    }

    @Override
    public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
        this.checkChildrenNotNull();
        this.children.paint(this, g, alloc, clipBounds);
        if (this.getDocumentView().op.isGuideLinesEnable()) {
            int firstNonWhite;
            DocumentView docView = this.getDocumentView();
            CharSequence docText = DocumentUtilities.getText((Document)docView.getDocument());
            int textlength = this.getEndOffset() - this.getStartOffset();
            int prefixlength = 0;
            for (firstNonWhite = 0; firstNonWhite < textlength && Character.isWhitespace(docText.charAt(firstNonWhite + this.getStartOffset())); ++firstNonWhite) {
                if ('\t' == docText.charAt(firstNonWhite + this.getStartOffset())) {
                    prefixlength += docView.op.getTabSize();
                    continue;
                }
                ++prefixlength;
            }
            if (firstNonWhite >= textlength) {
                int[] guideLinesCache = docView.op.getGuideLinesCache();
                if (guideLinesCache[0] != -1 && guideLinesCache[0] <= this.getStartOffset() && guideLinesCache[1] >= this.getEndOffset()) {
                    prefixlength = guideLinesCache[2];
                    firstNonWhite = 0;
                    textlength = prefixlength != -1 ? 1 : -1;
                } else {
                    int secondNonWhite;
                    firstNonWhite = 0;
                    prefixlength = 0;
                    for (secondNonWhite = this.getEndOffset(); secondNonWhite < docText.length(); ++secondNonWhite) {
                        char currentChar = docText.charAt(secondNonWhite);
                        ++firstNonWhite;
                        if (!Character.isWhitespace(currentChar)) break;
                        prefixlength = '\t' == currentChar ? (prefixlength += docView.op.getTabSize()) : ++prefixlength;
                        if (currentChar != '\n' && currentChar != '\r') continue;
                        firstNonWhite = 0;
                        prefixlength = 0;
                    }
                    if (secondNonWhite >= docText.length()) {
                        docView.op.setGuideLinesCache(this.getStartOffset(), secondNonWhite - firstNonWhite + 1, -1);
                        textlength = -1;
                    } else {
                        textlength = firstNonWhite + 1;
                        docView.op.setGuideLinesCache(this.getStartOffset(), secondNonWhite - firstNonWhite + 1, prefixlength);
                    }
                }
            } else {
                docView.op.setGuideLinesCache(-1, -1, -1);
            }
            if (firstNonWhite < textlength) {
                Color oldColor = g.getColor();
                Stroke oldStroke = g.getStroke();
                g.setColor(docView.op.getGuideLinesColor());
                g.setStroke(new BasicStroke(1.0f, 0, 2, 0.0f, new float[]{1.0f}, 0.0f));
                float textsize = docView.op.getDefaultCharWidth() * (float)prefixlength;
                float tabwidth = docView.op.getDefaultCharWidth() * (float)docView.op.getIndentLevelSize();
                int rowHeight = (int)docView.op.getDefaultRowHeight();
                if (tabwidth > 0.0f) {
                    int x = alloc.getBounds().x;
                    while (x < alloc.getBounds().x + alloc.getBounds().width && (float)x < textsize) {
                        g.drawLine(x, alloc.getBounds().y, x, alloc.getBounds().y + rowHeight);
                        x = (int)((float)x + tabwidth);
                    }
                }
                g.setColor(oldColor);
                g.setStroke(oldStroke);
            }
        }
    }

    @Override
    public JComponent getToolTip(double x, double y, Shape allocation) {
        this.checkChildrenNotNull();
        return this.children.getToolTip(this, x, y, allocation);
    }

    @Override
    public String getToolTipTextChecked(double x, double y, Shape allocation) {
        this.checkChildrenNotNull();
        return this.children.getToolTipTextChecked(this, x, y, allocation);
    }

    @Override
    public int getViewEndOffset(int rawChildEndOffset) {
        return this.getStartOffset() + this.children.raw2Offset(rawChildEndOffset);
    }

    @Override
    public ViewRenderContext getViewRenderContext() {
        EditorView.Parent parent = (EditorView.Parent)((Object)this.getParent());
        return parent != null ? parent.getViewRenderContext() : null;
    }

    @Override
    public void insertUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
    }

    @Override
    public void removeUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
    }

    @Override
    public void changedUpdate(DocumentEvent e, Shape a, ViewFactory f) {
    }

    DocumentView getDocumentView() {
        return (DocumentView)this.getParent();
    }

    @Override
    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        int retOffset;
        switch (direction) {
            case 3: 
            case 7: {
                retOffset = this.children.getNextVisualPositionX(this, offset, bias, alloc, direction == 3, biasRet);
                break;
            }
            case 1: 
            case 5: {
                DocumentView docView = this.getDocumentView();
                if (docView != null) {
                    retOffset = this.children.getNextVisualPositionY(this, offset, bias, alloc, direction == 5, biasRet, HighlightsViewUtils.getMagicX(docView, this, offset, bias, alloc));
                    break;
                }
                retOffset = offset;
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction " + direction);
            }
        }
        return retOffset;
    }

    void releaseTextLayouts() {
        this.children = null;
        this.markChildrenInvalid();
    }

    boolean isChildrenNull() {
        return this.children == null;
    }

    boolean isChildrenValid() {
        return this.isAnyStatusBit(1);
    }

    void markChildrenValid() {
        this.setStatusBits(1);
    }

    void markChildrenInvalid() {
        this.clearStatusBits(1);
    }

    boolean containsTabableViews() {
        return this.isAnyStatusBit(4);
    }

    void markContainsTabableViews() {
        this.setStatusBits(4);
    }

    boolean isLayoutValid() {
        return this.isAnyStatusBit(2);
    }

    void markLayoutValid() {
        this.setStatusBits(2);
    }

    void markLayoutInvalid() {
        this.clearStatusBits(2);
    }

    private void checkChildrenNotNull() {
        if (this.children == null) {
            throw new IllegalStateException("Null children in " + this.getDumpId());
        }
    }

    private void setStatusBits(int bits) {
        this.statusBits |= bits;
    }

    private void clearStatusBits(int bits) {
        this.statusBits &= ~ bits;
    }

    private boolean isAnyStatusBit(int bits) {
        return (this.statusBits & bits) != 0;
    }

    @Override
    public String findIntegrityError() {
        String err = super.findIntegrityError();
        if (err == null && this.children != null) {
            int childrenLength = this.children.getLength();
            if (this.getLength() != childrenLength) {
                err = "length=" + this.getLength() + " != childrenLength=" + childrenLength;
            }
            if (err == null) {
                err = this.children.findIntegrityError(this);
            }
        }
        if (err != null) {
            err = this.getDumpName() + ": " + err;
        }
        return err;
    }

    @Override
    protected StringBuilder appendViewInfo(StringBuilder sb, int indent, String xyInfo, int importantChildIndex) {
        super.appendViewInfo(sb, indent, xyInfo, importantChildIndex);
        sb.append(", WxH:").append(this.getWidth()).append("x").append(this.getHeight());
        if (this.children != null) {
            this.children.appendViewInfo(this, sb);
            if (importantChildIndex != -1) {
                this.children.appendChildrenInfo(this, sb, indent + 8, importantChildIndex);
            }
        } else {
            sb.append(", children=null");
        }
        return sb;
    }

    public String toString() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }

    public String toStringDetail() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -2).toString();
    }

    @Override
    protected String getDumpName() {
        return "PV";
    }
}

