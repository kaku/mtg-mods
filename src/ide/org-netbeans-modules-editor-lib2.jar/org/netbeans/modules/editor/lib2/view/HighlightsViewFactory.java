/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Font;
import java.awt.font.TextLayout;
import java.util.EventListener;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.highlighting.DirectMergeContainer;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsList;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsReader;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange;
import org.netbeans.modules.editor.lib2.view.HighlightsView;
import org.netbeans.modules.editor.lib2.view.NewlineView;
import org.netbeans.modules.editor.lib2.view.TabView;
import org.netbeans.modules.editor.lib2.view.TextLayoutBreakInfo;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewStats;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.openide.util.WeakListeners;

public final class HighlightsViewFactory
extends EditorViewFactory
implements HighlightsChangeListener {
    private static final int SPLIT_TEXT_LAYOUT_LENGTH = 1024;
    private static final int MAX_TEXT_LAYOUT_LENGTH = 1280;
    private static final int MODIFICATION_TOLERANCE = 100;
    private static final Logger LOG = Logger.getLogger(HighlightsViewFactory.class.getName());
    private static final int UNKNOWN_CHAR_TYPE = 0;
    private static final int LTR_CHAR_TYPE = 1;
    private static final int RTL_CHAR_TYPE = 2;
    private static final int TAB_CHAR_TYPE = 3;
    private final DocumentView docView;
    private final HighlightingManager highlightingManager;
    private HighlightsContainer highlightsContainer;
    private HighlightsContainer paintHighlightsContainer;
    private HighlightsChangeListener weakHL;
    private HighlightsChangeListener paintWeakHL;
    private CharSequence docText;
    private Element lineElementRoot;
    private int lineIndex;
    private int lineEndOffset;
    private HighlightsReader highlightsReader;
    private Font defaultFont;
    private int nextTabOrRTLOffset;
    private int charType;
    private int nextCharType;
    private boolean createViews;
    private int usageCount = 0;

    public HighlightsViewFactory(View documentView) {
        super(documentView);
        this.docView = (DocumentView)documentView;
        this.highlightingManager = HighlightingManager.getInstance(this.textComponent());
        this.highlightingManager.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent e) {
                HighlightsViewFactory.this.notifyStaleCreation();
                HighlightsViewFactory.this.updateHighlightsContainer();
                HighlightsViewFactory.this.fireEvent(EditorViewFactoryChange.createList(0, HighlightsViewFactory.this.document().getLength() + 1, EditorViewFactoryChange.Type.REBUILD));
            }
        });
        this.updateHighlightsContainer();
    }

    private void updateHighlightsContainer() {
        if (this.highlightsContainer != null && this.weakHL != null) {
            this.highlightsContainer.removeHighlightsChangeListener(this.weakHL);
            this.paintHighlightsContainer.removeHighlightsChangeListener(this.paintWeakHL);
            this.weakHL = null;
            this.paintWeakHL = null;
        }
        this.highlightsContainer = this.highlightingManager.getBottomHighlights();
        this.weakHL = (HighlightsChangeListener)WeakListeners.create(HighlightsChangeListener.class, (EventListener)this, (Object)this.highlightsContainer);
        this.highlightsContainer.addHighlightsChangeListener(this.weakHL);
        this.paintHighlightsContainer = this.highlightingManager.getTopHighlights();
        this.paintWeakHL = (HighlightsChangeListener)WeakListeners.create(HighlightsChangeListener.class, (EventListener)this, (Object)this.paintHighlightsContainer);
        this.paintHighlightsContainer.addHighlightsChangeListener(this.paintWeakHL);
    }

    @Override
    public void restart(int startOffset, int endOffset, boolean createViews) {
        if (this.usageCount != 0) {
            throw new IllegalStateException("Race condition: usageCount = " + this.usageCount);
        }
        ++this.usageCount;
        this.createViews = createViews;
        this.docText = DocumentUtilities.getText((Document)this.document());
        this.lineElementRoot = this.document().getDefaultRootElement();
        assert (this.lineElementRoot != null);
        this.lineIndex = this.lineElementRoot.getElementIndex(startOffset);
        this.lineEndOffset = this.lineElementRoot.getElement(this.lineIndex).getEndOffset();
        this.defaultFont = this.textComponent().getFont();
        this.nextTabOrRTLOffset = -1;
        if (createViews) {
            this.highlightsReader = new HighlightsReader(this.highlightsContainer, startOffset, endOffset);
            this.highlightsReader.readUntil(endOffset);
        }
    }

    @Override
    public int nextViewStartOffset(int offset) {
        return offset;
    }

    @Override
    public EditorView createView(int startOffset, int limitOffset, boolean forcedLimit, EditorView origView, int nextOrigViewOffset) {
        HighlightsView origHView;
        TextLayout origTextLayout;
        assert (startOffset >= 0);
        assert (startOffset < limitOffset);
        this.updateLineEndOffset(startOffset);
        HighlightsList hList = this.highlightsReader.highlightsList();
        if (hList.startOffset() < startOffset) {
            hList.skip(startOffset);
        }
        if (startOffset == this.lineEndOffset - 1) {
            AttributeSet attrs = hList.cutSingleChar();
            return new NewlineView(attrs);
        }
        this.updateTabsAndHighlightsAndRTL(startOffset);
        if (this.charType == 3) {
            int tabsEndOffset = this.nextTabOrRTLOffset;
            if (limitOffset > tabsEndOffset) {
                limitOffset = tabsEndOffset;
            }
            AttributeSet attrs = hList.cut(limitOffset);
            return new TabView(limitOffset - startOffset, attrs);
        }
        int wsEndOffset = limitOffset = Math.min(limitOffset, this.nextTabOrRTLOffset);
        if (limitOffset - startOffset > 924) {
            if (nextOrigViewOffset <= limitOffset && nextOrigViewOffset - startOffset >= 924 && nextOrigViewOffset - startOffset <= 1380) {
                limitOffset = nextOrigViewOffset;
                wsEndOffset = nextOrigViewOffset;
            } else {
                limitOffset = Math.min(limitOffset, startOffset + 1280);
                wsEndOffset = Math.min(wsEndOffset, startOffset + 1024);
            }
        }
        AttributeSet attrs = hList.cutSameFont(this.defaultFont, limitOffset, wsEndOffset, this.docText);
        int length = hList.startOffset() - startOffset;
        HighlightsView view = new HighlightsView(length, attrs);
        if (origView instanceof HighlightsView && origView.getLength() == length && (origTextLayout = (origHView = (HighlightsView)origView).getTextLayout()) != null) {
            CharSequence text;
            String origText;
            if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINE) && (origText = this.docView.getTextLayoutVerifier().get(origTextLayout)) != null && !CharSequenceUtilities.textEquals((CharSequence)(text = this.docText.subSequence(startOffset, startOffset + length)), (CharSequence)origText)) {
                throw new IllegalStateException("TextLayout text differs:\n current:" + CharSequenceUtilities.debugText((CharSequence)text) + "\n!=\n" + CharSequenceUtilities.debugText((CharSequence)origText) + "\n");
            }
            Font font = ViewUtils.getFont(attrs, this.defaultFont);
            Font origFont = ViewUtils.getFont(origView.getAttributes(), this.defaultFont);
            if (font != null && font.equals(origFont)) {
                float origWidth = origHView.getWidth();
                view.setTextLayout(origTextLayout, origWidth);
                view.setBreakInfo(origHView.getBreakInfo());
                ViewStats.incrementTextLayoutReused(length);
            }
        }
        return view;
    }

    private void updateTabsAndHighlightsAndRTL(int offset) {
        if (offset >= this.nextTabOrRTLOffset) {
            char ch;
            if (this.nextCharType == 0 || offset > this.nextTabOrRTLOffset) {
                ch = this.docText.charAt(offset);
                this.charType = this.getCharType(ch);
            } else {
                this.charType = this.nextCharType;
            }
            this.nextTabOrRTLOffset = offset + 1;
            while (this.nextTabOrRTLOffset < this.lineEndOffset - 1) {
                ch = this.docText.charAt(this.nextTabOrRTLOffset);
                this.nextCharType = this.getCharType(ch);
                if (this.charType == 2 && Character.isWhitespace(ch)) {
                    this.nextCharType = 2;
                }
                if (this.nextCharType != this.charType) break;
                ++this.nextTabOrRTLOffset;
            }
        }
    }

    private int getCharType(char ch) {
        if (ch == '\t') {
            return 3;
        }
        byte dir = Character.getDirectionality(ch);
        switch (dir) {
            case 1: 
            case 2: 
            case 16: 
            case 17: {
                return 2;
            }
        }
        return 1;
    }

    @Override
    public int viewEndOffset(int startOffset, int limitOffset, boolean forcedLimit) {
        this.updateLineEndOffset(startOffset);
        return Math.min(this.lineEndOffset, limitOffset);
    }

    @Override
    public void continueCreation(int startOffset, int endOffset) {
        if (this.createViews) {
            this.highlightsReader = new HighlightsReader(this.highlightsContainer, startOffset, endOffset);
            this.highlightsReader.readUntil(endOffset);
        }
    }

    private void updateLineEndOffset(int offset) {
        while (offset >= this.lineEndOffset) {
            ++this.lineIndex;
            Element line = this.lineElementRoot.getElement(this.lineIndex);
            this.lineEndOffset = line.getEndOffset();
        }
    }

    @Override
    public void finishCreation() {
        this.highlightsReader = null;
        this.docText = null;
        this.lineElementRoot = null;
        this.lineIndex = -1;
        this.lineEndOffset = -1;
        --this.usageCount;
    }

    @Override
    public void highlightChanged(final HighlightsChangeEvent evt) {
        this.document().render(new Runnable(){

            @Override
            public void run() {
                int startOffset = evt.getStartOffset();
                int endOffset = evt.getEndOffset();
                if (evt.getSource() == HighlightsViewFactory.this.highlightsContainer) {
                    if (HighlightsViewFactory.this.usageCount != 0) {
                        HighlightsViewFactory.this.notifyStaleCreation();
                    }
                    int docTextLength = HighlightsViewFactory.this.document().getLength() + 1;
                    assert (startOffset >= 0);
                    assert (endOffset >= 0);
                    startOffset = Math.min(startOffset, docTextLength);
                    endOffset = Math.min(endOffset, docTextLength);
                    if (ViewHierarchyImpl.CHANGE_LOG.isLoggable(Level.FINE)) {
                        HighlightsChangeEvent layerEvent = HighlightsViewFactory.this.highlightsContainer instanceof DirectMergeContainer ? ((DirectMergeContainer)HighlightsViewFactory.this.highlightsContainer).layerEvent() : null;
                        String layerInfo = layerEvent != null ? " " + HighlightsViewFactory.this.highlightingManager.findLayer((HighlightsContainer)layerEvent.getSource()) : "";
                        ViewUtils.log(ViewHierarchyImpl.CHANGE_LOG, "VIEW-REBUILD-HC:<" + startOffset + "," + endOffset + ">" + layerInfo + "\n");
                    }
                    if (startOffset <= endOffset) {
                        HighlightsViewFactory.this.fireEvent(EditorViewFactoryChange.createList(startOffset, endOffset, EditorViewFactoryChange.Type.CHARACTER_CHANGE));
                    }
                } else if (evt.getSource() == HighlightsViewFactory.this.paintHighlightsContainer) {
                    if (ViewHierarchyImpl.CHANGE_LOG.isLoggable(Level.FINE)) {
                        HighlightsChangeEvent layerEvent = HighlightsViewFactory.this.paintHighlightsContainer instanceof DirectMergeContainer ? ((DirectMergeContainer)HighlightsViewFactory.this.paintHighlightsContainer).layerEvent() : null;
                        String layerInfo = layerEvent != null ? " " + HighlightsViewFactory.this.highlightingManager.findLayer((HighlightsContainer)layerEvent.getSource()) : "";
                        ViewUtils.log(ViewHierarchyImpl.CHANGE_LOG, "REPAINT-HC:<" + startOffset + "," + endOffset + ">" + layerInfo + "\n");
                    }
                    HighlightsViewFactory.this.offsetRepaint(startOffset, endOffset);
                }
            }
        });
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(100);
        sb.append("lineIndex=").append(this.lineIndex).append(", lineEndOffset=").append(this.lineEndOffset).append(", charType=").append(this.charType).append(", nextTabOrRTLOffset=").append(this.nextTabOrRTLOffset).append(", nextCharType=").append(this.nextCharType);
        sb.append(", ").append(super.toString());
        return sb.toString();
    }

    public static final class HighlightsFactory
    implements EditorViewFactory.Factory {
        @Override
        public EditorViewFactory createEditorViewFactory(View documentView) {
            return new HighlightsViewFactory(documentView);
        }

        @Override
        public int weight() {
            return 0;
        }
    }

}

