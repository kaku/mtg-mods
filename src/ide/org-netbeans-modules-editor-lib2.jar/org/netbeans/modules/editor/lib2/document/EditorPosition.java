/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Position;
import org.netbeans.modules.editor.lib2.document.Mark;

final class EditorPosition
implements Position {
    private Mark mark;

    @Override
    public int getOffset() {
        return this.mark.getOffset();
    }

    public boolean isBackwardBias() {
        return this.mark.isBackwardBias();
    }

    public Mark getMark() {
        return this.mark;
    }

    void initMark(Mark mark) {
        this.mark = mark;
    }

    public String toString() {
        return this.mark.toString();
    }

    public String toStringDetail() {
        return this.mark.toStringDetail();
    }
}

