/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import org.netbeans.modules.editor.lib2.highlighting.VisualMarkVector;

public abstract class VisualMark {
    private double rawY;
    private final VisualMarkVector<?> markVector;

    protected VisualMark(VisualMarkVector<?> markVector) {
        this.markVector = markVector;
    }

    public abstract int getOffset();

    public final double getY() {
        return this.markVector.raw2Y(this.rawY);
    }

    protected final VisualMarkVector<?> markVector() {
        return this.markVector;
    }

    double rawY() {
        return this.rawY;
    }

    void setRawY(double rawY) {
        this.rawY = rawY;
    }
}

