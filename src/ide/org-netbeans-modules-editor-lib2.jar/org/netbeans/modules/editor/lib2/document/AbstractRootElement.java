/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.lib.editor.util.GapList;

public abstract class AbstractRootElement<E extends Element>
implements Element {
    protected final Document doc;
    protected GapList<E> children;

    public AbstractRootElement(Document doc) {
        this.doc = doc;
        this.children = new GapList();
    }

    @Override
    public int getElementCount() {
        return this.children.size();
    }

    @Override
    public Element getElement(int index) {
        return (Element)this.children.get(index);
    }

    public void copyElements(int srcBegin, int srcEnd, Element[] dst, int dstBegin) {
        this.children.copyElements(srcBegin, srcEnd, (Object[])dst, dstBegin);
    }

    @Override
    public int getElementIndex(int offset) {
        int low = 0;
        int high = this.getElementCount() - 1;
        if (high == -1) {
            return -1;
        }
        while (low <= high) {
            int mid = (low + high) / 2;
            int elemStartOffset = this.getElement(mid).getStartOffset();
            if (elemStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (elemStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        if (high < 0) {
            high = 0;
        }
        return high;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    protected void replace(int index, int removeCount, Element[] addedElems) {
        if (removeCount > 0) {
            this.children.remove(index, removeCount);
        }
        if (addedElems != null) {
            this.children.addArray(index, (Object[])addedElems);
        }
    }

    @Override
    public Document getDocument() {
        return this.doc;
    }

    @Override
    public Element getParentElement() {
        return null;
    }

    @Override
    public AttributeSet getAttributes() {
        return SimpleAttributeSet.EMPTY;
    }

    @Override
    public int getStartOffset() {
        return 0;
    }

    @Override
    public int getEndOffset() {
        return this.doc.getLength() + 1;
    }

    public String toString() {
        return this.children.toString();
    }
}

