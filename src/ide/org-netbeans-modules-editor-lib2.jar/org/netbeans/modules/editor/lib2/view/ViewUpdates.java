/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.BlockCompare
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.swing.BlockCompare;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryEvent;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryListener;
import org.netbeans.modules.editor.lib2.view.OffsetRegion;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.ViewBuilder;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class ViewUpdates
implements DocumentListener,
EditorViewFactoryListener {
    private static final Logger LOG = Logger.getLogger(ViewUpdates.class.getName());
    private static final int REBUILD_DELAY = 5;
    private static final int MAX_VIEW_REBUILD_ATTEMPTS = 10;
    private static final RequestProcessor rebuildRegionRP = new RequestProcessor("ViewHierarchy-Region-Rebuilding", 1, false, false);
    private final Object rebuildRegionsLock = new String("rebuild-region-lock");
    private boolean rebuildRegionsScheduled;
    private final DocumentView docView;
    private EditorViewFactory[] viewFactories;
    private DocumentListener incomingModificationListener;
    private OffsetRegion charRebuildRegion;
    private OffsetRegion paragraphRebuildRegion;
    private boolean rebuildAll;
    private DocumentEvent incomingEvent;
    private final RequestProcessor.Task rebuildRegionTask;
    private boolean listenerPriorityAwareDoc;

    public ViewUpdates(DocumentView docView) {
        this.rebuildRegionTask = rebuildRegionRP.create((Runnable)new RebuildViews());
        this.docView = docView;
        this.incomingModificationListener = new IncomingModificationListener();
        Document doc = docView.getDocument();
        this.listenerPriorityAwareDoc = DocumentUtilities.addPriorityDocumentListener((Document)doc, (DocumentListener)((DocumentListener)WeakListeners.create(DocumentListener.class, (EventListener)this.incomingModificationListener, (Object)null)), (DocumentListenerPriority)DocumentListenerPriority.FIRST);
        DocumentUtilities.addDocumentListener((Document)doc, (DocumentListener)((DocumentListener)WeakListeners.create(DocumentListener.class, (EventListener)this, (Object)doc)), (DocumentListenerPriority)DocumentListenerPriority.VIEW);
    }

    void initFactories() {
        List<EditorViewFactory.Factory> factoryFactories = EditorViewFactory.factories();
        int size = factoryFactories.size();
        ArrayList<EditorViewFactory> factoryList = new ArrayList<EditorViewFactory>(size);
        for (int i = 0; i < size; ++i) {
            EditorViewFactory factory;
            EditorViewFactory.Factory factoryFactory = factoryFactories.get(i);
            if (factoryFactories == null || (factory = factoryFactory.createEditorViewFactory(this.docView)) == null) continue;
            factory.addEditorViewFactoryListener((EditorViewFactoryListener)WeakListeners.create(EditorViewFactoryListener.class, (EventListener)this, (Object)factory));
            factoryList.add(factory);
        }
        this.viewFactories = factoryList.toArray(new EditorViewFactory[factoryList.size()]);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "ViewUpdates initializing for {0}, factories: {1}", new Object[]{this.docView.getTextComponent(), Arrays.asList(this.viewFactories)});
        }
    }

    private ViewBuilder startBuildViews() {
        ViewBuilder viewBuilder = new ViewBuilder(this.docView, this.viewFactories);
        this.docView.checkMutexAcquiredIfLogging();
        return viewBuilder;
    }

    private void finishBuildViews(ViewBuilder viewBuilder, boolean allowCheck) {
        viewBuilder.finish();
        if (allowCheck) {
            this.docView.checkIntegrityIfLoggable();
        }
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finer("ViewUpdates.buildViews(): UPDATED-DOC-VIEW:\n" + this.docView);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void reinitAllViews() {
        for (int i = 10; i >= 0; --i) {
            ViewBuilder viewBuilder = this.startBuildViews();
            boolean noException = false;
            try {
                this.fetchCharRebuildRegion();
                viewBuilder.initFullRebuild();
                boolean replaceSuccessful = viewBuilder.createReplaceRepaintViews(i == 0);
                noException = true;
                if (!replaceSuccessful) continue;
                break;
            }
            finally {
                this.finishBuildViews(viewBuilder, noException);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void initParagraphs(int startIndex, int endIndex) {
        for (int i = 10; i >= 0; --i) {
            ViewBuilder viewBuilder = this.startBuildViews();
            boolean noException = false;
            try {
                viewBuilder.initParagraphs(startIndex, endIndex);
                boolean replaceSuccessful = viewBuilder.createReplaceRepaintViews(i == 0);
                noException = true;
                if (!replaceSuccessful) continue;
                break;
            }
            finally {
                this.finishBuildViews(viewBuilder, noException);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertUpdate(DocumentEvent evt) {
        this.clearIncomingEvent(evt);
        if (this.docView.lock()) {
            this.docView.checkDocumentLockedIfLogging();
            try {
                if (!this.docView.op.isUpdatable()) {
                    return;
                }
                Document doc = this.docView.getDocument();
                assert (doc == evt.getDocument());
                int insertOffset = evt.getOffset();
                int insertLength = evt.getLength();
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("\nDOCUMENT-INSERT-evt: offset=" + insertOffset + ", length=" + insertLength + ", cRegion=" + this.charRebuildRegion + ", current-docViewEndOffset=" + (evt.getDocument().getLength() + 1) + '\n');
                }
                this.updateViewsByModification(insertOffset, insertLength, evt);
            }
            finally {
                this.docView.op.clearIncomingModification();
                this.docView.unlock();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeUpdate(DocumentEvent evt) {
        this.clearIncomingEvent(evt);
        if (this.docView.lock()) {
            this.docView.checkDocumentLockedIfLogging();
            try {
                if (!this.docView.op.isUpdatable() || this.docView.getViewCount() == 0) {
                    return;
                }
                Document doc = this.docView.getDocument();
                assert (doc == evt.getDocument());
                int removeOffset = evt.getOffset();
                int removeLength = evt.getLength();
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("\nDOCUMENT-REMOVE-evt: offset=" + removeOffset + ", length=" + removeLength + ", cRegion=" + this.charRebuildRegion + ", current-docViewEndOffset=" + (evt.getDocument().getLength() + 1) + '\n');
                }
                this.updateViewsByModification(removeOffset, - removeLength, evt);
            }
            finally {
                this.docView.op.clearIncomingModification();
                this.docView.unlock();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void changedUpdate(DocumentEvent evt) {
        this.clearIncomingEvent(evt);
        if (this.docView.lock()) {
            this.docView.checkDocumentLockedIfLogging();
            try {
                if (!this.docView.op.isUpdatable()) {
                    return;
                }
                this.docView.checkIntegrityIfLoggable();
            }
            finally {
                this.docView.unlock();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateViewsByModification(int modOffset, int modLength, DocumentEvent evt) {
        ViewBuilder viewBuilder = this.startBuildViews();
        boolean success = false;
        try {
            OffsetRegion cRegion = this.fetchCharRebuildRegion();
            if (viewBuilder.initModUpdate(modOffset, modLength, cRegion)) {
                boolean replaced = viewBuilder.createReplaceRepaintViews(true);
                assert (replaced);
                this.docView.validChange().documentEvent = evt;
                int startCreationOffset = viewBuilder.getStartCreationOffset();
                int matchOffset = viewBuilder.getMatchOffset();
                Document doc = this.docView.getDocument();
                if (cRegion != null) {
                    BlockCompare bc = BlockCompare.get((int)cRegion.startOffset(), (int)cRegion.endOffset(), (int)startCreationOffset, (int)matchOffset);
                    if (bc.inside()) {
                        cRegion = null;
                    } else if (bc.overlapStart()) {
                        cRegion = OffsetRegion.create(cRegion.startPosition(), ViewUtils.createPosition(doc, startCreationOffset));
                    } else if (bc.overlapEnd()) {
                        cRegion = OffsetRegion.create(ViewUtils.createPosition(doc, matchOffset), cRegion.endPosition());
                    }
                }
            }
            if (cRegion != null) {
                this.extendCharRebuildRegion(cRegion);
            }
            success = true;
        }
        finally {
            this.finishBuildViews(viewBuilder, success);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void viewFactoryChanged(EditorViewFactoryEvent evt) {
        Object object = this.rebuildRegionsLock;
        synchronized (object) {
            JTextComponent c = this.docView.getTextComponent();
            if (c == null || c.getDocument() != this.docView.getDocument()) {
                return;
            }
            this.docView.checkDocumentLockedIfLogging();
            block8 : for (EditorViewFactoryChange change : evt.getChanges()) {
                int startOffset = change.getStartOffset();
                int endOffset = change.getEndOffset();
                Document doc = this.docView.getDocument();
                int docTextLen = doc.getLength() + 1;
                startOffset = Math.min(startOffset, docTextLen);
                endOffset = Math.min(endOffset, docTextLen);
                switch (change.getType()) {
                    case CHARACTER_CHANGE: {
                        this.charRebuildRegion = OffsetRegion.union(this.charRebuildRegion, doc, startOffset, endOffset, true);
                        continue block8;
                    }
                    case PARAGRAPH_CHANGE: {
                        this.paragraphRebuildRegion = OffsetRegion.union(this.paragraphRebuildRegion, doc, startOffset, endOffset, true);
                        continue block8;
                    }
                    case REBUILD: {
                        this.rebuildAll = true;
                        continue block8;
                    }
                }
                throw new IllegalStateException("Unexpected type=" + (Object)((Object)change.getType()));
            }
            if (!this.rebuildRegionsScheduled) {
                this.rebuildRegionsScheduled = true;
                this.rebuildRegionTask.schedule(5);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void viewsRebuildOrMarkInvalidNeedsLock() {
        this.docView.checkDocumentLockedIfLogging();
        if (this.docView.op.isActive()) {
            OffsetRegion cRegion;
            boolean rebuildAllLocal;
            OffsetRegion pRegion;
            Object object = this.rebuildRegionsLock;
            synchronized (object) {
                rebuildAllLocal = this.rebuildAll;
                this.rebuildAll = false;
            }
            if (rebuildAllLocal) {
                for (int i = 10; i >= 0; --i) {
                    ViewBuilder viewBuilder = this.startBuildViews();
                    boolean noException = false;
                    try {
                        viewBuilder.initFullRebuild();
                        boolean replaceSuccessful = viewBuilder.createReplaceRepaintViews(i == 0);
                        noException = true;
                        if (!replaceSuccessful) continue;
                        break;
                    }
                    finally {
                        this.finishBuildViews(viewBuilder, noException);
                    }
                }
            }
            if ((pRegion = this.fetchParagraphRebuildRegion()) != null && !pRegion.isEmpty()) {
                for (int i = 10; i >= 0; --i) {
                    ViewBuilder viewBuilder = this.startBuildViews();
                    boolean noException = false;
                    try {
                        if (viewBuilder.initRebuildParagraphs(pRegion)) {
                            if (viewBuilder.createReplaceRepaintViews(i == 0)) {
                                noException = true;
                            }
                            OffsetRegion newPRegion = this.fetchParagraphRebuildRegion();
                            if (newPRegion != null) {
                                pRegion = pRegion.union(newPRegion, true);
                            }
                        }
                        noException = true;
                    }
                    finally {
                        this.finishBuildViews(viewBuilder, noException);
                    }
                }
            }
            if ((cRegion = this.fetchCharRebuildRegion()) != null && !cRegion.isEmpty()) {
                int pCount = this.docView.getViewCount();
                int startOffset = cRegion.startOffset();
                int endOffset = cRegion.endOffset();
                if (pCount > 0 && endOffset > this.docView.getStartOffset() && startOffset < this.docView.getEndOffset()) {
                    int startPIndex;
                    int pIndex = startPIndex = this.docView.getViewIndex(startOffset);
                    ParagraphView pView = this.docView.getParagraphView(pIndex);
                    double startY = this.docView.getY(pIndex);
                    int pViewStartOffset = pView.getStartOffset();
                    Rectangle2D repaintRect = null;
                    boolean localRepaint = false;
                    while (startOffset < endOffset) {
                        int pViewLength = pView.getLength();
                        if (!pView.isChildrenNull()) {
                            int lStartOffset = Math.max(startOffset - pViewStartOffset, 0);
                            int lEndOffset = Math.min(endOffset - pViewStartOffset, pViewLength);
                            if (pView.isChildrenValid()) {
                                pView.markChildrenInvalid();
                            } else {
                                lStartOffset = Math.min(lStartOffset, pView.children.getStartInvalidChildrenLocalOffset());
                                lEndOffset = Math.max(lEndOffset, pView.children.getEndInvalidChildrenLocalOffset());
                            }
                            pView.children.setInvalidChildrenLocalRange(lStartOffset, lEndOffset);
                            if (pIndex == startPIndex && lEndOffset < pViewLength) {
                                localRepaint = true;
                                Shape pAlloc = this.docView.getChildAllocation(pIndex, this.docView.getAllocation());
                                if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                                    pAlloc = this.docView.getChildAllocation(pIndex, this.docView.getAllocation());
                                }
                                Shape s = pView.modelToViewChecked(startOffset, Position.Bias.Forward, endOffset, Position.Bias.Forward, pAlloc);
                                repaintRect = ViewUtils.shapeAsRect(s);
                            }
                        }
                        pViewStartOffset += pViewLength;
                        if (++pIndex >= pCount) break;
                        pView = this.docView.getParagraphView(pIndex);
                        startOffset = pViewStartOffset;
                    }
                    if (!localRepaint) {
                        Rectangle2D.Double r = this.docView.getAllocationCopy();
                        r.y += startY;
                        r.height = this.docView.getY(pIndex) - startY;
                        this.docView.op.extendToVisibleWidth(r);
                        repaintRect = r;
                    }
                    assert (repaintRect != null);
                    this.docView.op.notifyRepaint(repaintRect);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    OffsetRegion fetchCharRebuildRegion() {
        Object object = this.rebuildRegionsLock;
        synchronized (object) {
            OffsetRegion region = this.charRebuildRegion;
            this.charRebuildRegion = null;
            return region;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void extendCharRebuildRegion(OffsetRegion newRegion) {
        Object object = this.rebuildRegionsLock;
        synchronized (object) {
            this.charRebuildRegion = OffsetRegion.union(this.charRebuildRegion, newRegion, true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    OffsetRegion fetchParagraphRebuildRegion() {
        Object object = this.rebuildRegionsLock;
        synchronized (object) {
            OffsetRegion region = this.paragraphRebuildRegion;
            this.paragraphRebuildRegion = null;
            return region;
        }
    }

    void incomingEvent(DocumentEvent evt) {
        if (this.incomingEvent != null) {
            this.docView.op.releaseChildrenNeedsLock();
            LOG.log(Level.INFO, "View hierarchy rebuild due to pending document event", new Exception("Pending incoming event: " + this.incomingEvent));
        }
        this.incomingEvent = evt;
    }

    private void clearIncomingEvent(DocumentEvent evt) {
        if (this.listenerPriorityAwareDoc) {
            if (this.incomingEvent == null) {
                throw new IllegalStateException("Incoming event already cleared");
            }
            if (this.incomingEvent != evt) {
                throw new IllegalStateException("Invalid incomingEvent=" + this.incomingEvent + " != evt=" + evt);
            }
            this.incomingEvent = null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    StringBuilder appendInfo(StringBuilder sb) {
        sb.append("Regions:");
        int len = sb.length();
        Object object = this.rebuildRegionsLock;
        synchronized (object) {
            if (this.charRebuildRegion != null) {
                sb.append(" C").append(this.charRebuildRegion);
            }
            if (this.paragraphRebuildRegion != null) {
                sb.append(" P").append(this.paragraphRebuildRegion);
            }
            if (this.rebuildAll) {
                sb.append(" A");
            }
        }
        if (sb.length() == len) {
            sb.append(" <NONE>");
        }
        return sb;
    }

    public String toString() {
        return this.appendInfo(new StringBuilder(200)).toString();
    }

    private final class RebuildViews
    implements Runnable {
        private RebuildViews() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            ViewUpdates.this.docView;
            if (DocumentView.testRun != null) {
                return;
            }
            Object object = ViewUpdates.this.rebuildRegionsLock;
            synchronized (object) {
                ViewUpdates.this.rebuildRegionsScheduled = false;
            }
            ViewUpdates.access$200((ViewUpdates)ViewUpdates.this).op.viewsRebuildOrMarkInvalid();
        }
    }

    private final class IncomingModificationListener
    implements DocumentListener {
        private IncomingModificationListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            ViewUpdates.this.incomingEvent(e);
            ViewUpdates.access$200((ViewUpdates)ViewUpdates.this).op.markIncomingModification();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            ViewUpdates.this.incomingEvent(e);
            ViewUpdates.access$200((ViewUpdates)ViewUpdates.this).op.markIncomingModification();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            ViewUpdates.this.incomingEvent(e);
        }
    }

}

