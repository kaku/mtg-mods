/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenHierarchyEvent
 *  org.netbeans.api.lexer.TokenHierarchyEventType
 *  org.netbeans.api.lexer.TokenHierarchyListener
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.ListenerList
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public final class SyntaxHighlighting
extends AbstractHighlightsContainer
implements TokenHierarchyListener,
ChangeListener {
    private static final Logger LOG = Logger.getLogger(SyntaxHighlighting.class.getName());
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.lib2.highlighting.SyntaxHighlighting";
    private static final HashMap<String, FCSInfo<?>> globalFCSCache = new HashMap();
    private final HashMap<String, FCSInfo<?>> fcsCache = new HashMap();
    private final Document document;
    private final String mimeTypeForOptions;
    private final TokenHierarchy<? extends Document> hierarchy;
    private long version = 0;
    static AttributeSet TEST_FALLBACK_COLORING;

    public SyntaxHighlighting(Document document) {
        this.document = document;
        String mimeType = (String)document.getProperty("mimeType");
        this.mimeTypeForOptions = mimeType != null && mimeType.startsWith("test") ? mimeType : null;
        this.findFCSInfo("", null);
        this.hierarchy = TokenHierarchy.get((Document)document);
        this.hierarchy.addTokenHierarchyListener((TokenHierarchyListener)WeakListeners.create(TokenHierarchyListener.class, (EventListener)this, this.hierarchy));
    }

    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        long lVersion = this.getVersion();
        if (this.hierarchy.isActive()) {
            return new HSImpl(lVersion, this.hierarchy, startOffset, endOffset);
        }
        return HighlightsSequence.EMPTY;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void tokenHierarchyChanged(TokenHierarchyEvent evt) {
        if (evt.type() == TokenHierarchyEventType.LANGUAGE_PATHS) {
            return;
        }
        SyntaxHighlighting syntaxHighlighting = this;
        synchronized (syntaxHighlighting) {
            ++this.version;
        }
        if (LOG.isLoggable(Level.FINEST)) {
            StringBuilder sb = new StringBuilder();
            TokenSequence ts = this.hierarchy.tokenSequence();
            sb.append("\n");
            sb.append("Tokens after change: <").append(evt.affectedStartOffset()).append(", ").append(evt.affectedEndOffset()).append(">\n");
            SyntaxHighlighting.dumpSequence(ts, sb);
            sb.append("--------------------------------------------\n\n");
            LOG.finest(sb.toString());
        }
        this.fireHighlightsChange(evt.affectedStartOffset(), evt.affectedEndOffset());
    }

    private String languagePathToMimePathOptions(LanguagePath languagePath) {
        if (languagePath.size() == 1) {
            return this.mimeTypeForOptions;
        }
        if (languagePath.size() > 1) {
            return this.mimeTypeForOptions + "/" + languagePath.subPath(1).mimePath();
        }
        throw new IllegalStateException("LanguagePath should not be empty.");
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.fireHighlightsChange(0, Integer.MAX_VALUE);
    }

    synchronized long getVersion() {
        return this.version;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <T extends TokenId> FCSInfo<T> findFCSInfo(String mimePath, Language<T> language) {
        FCSInfo fcsInfo = this.fcsCache.get(mimePath);
        if (fcsInfo == null) {
            HashMap hashMap = globalFCSCache;
            synchronized (hashMap) {
                FCSInfo fcsI = globalFCSCache.get(mimePath);
                fcsInfo = fcsI;
                if (fcsInfo == null) {
                    fcsInfo = new FCSInfo<T>(mimePath, language);
                    if (this.mimeTypeForOptions == null) {
                        globalFCSCache.put(mimePath, fcsInfo);
                    }
                }
            }
            fcsInfo.addChangeListener(WeakListeners.change((ChangeListener)this, fcsInfo));
            this.fcsCache.put(mimePath, fcsInfo);
        }
        return fcsInfo;
    }

    private static void dumpSequence(TokenSequence<?> seq, StringBuilder sb) {
        if (seq == null) {
            sb.append("Inactive TokenHierarchy");
        } else {
            seq.moveStart();
            while (seq.moveNext()) {
                TokenSequence emSeq = seq.embedded();
                if (emSeq != null) {
                    SyntaxHighlighting.dumpSequence(emSeq, sb);
                    continue;
                }
                Token token = seq.token();
                sb.append("<");
                sb.append(String.format("%3s", seq.offset())).append(", ");
                sb.append(String.format("%3s", seq.offset() + token.length())).append(", ");
                sb.append(String.format("%+3d", token.length())).append("> : ");
                sb.append(SyntaxHighlighting.tokenId(token.id(), true)).append(" : '");
                sb.append(SyntaxHighlighting.tokenText(token));
                sb.append("'\n");
            }
        }
    }

    private static String tokenId(TokenId tokenId, boolean format) {
        if (format) {
            return String.format("%20s.%-15s", tokenId.getClass().getSimpleName(), tokenId.name());
        }
        return tokenId.getClass().getSimpleName() + "." + tokenId.name();
    }

    private static String tokenText(Token<?> token) {
        CharSequence text = token.text();
        StringBuilder sb = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); ++i) {
            char ch = text.charAt(i);
            if (Character.isISOControl(ch)) {
                switch (ch) {
                    case '\n': {
                        sb.append("\\n");
                        break;
                    }
                    case '\t': {
                        sb.append("\\t");
                        break;
                    }
                    case '\r': {
                        sb.append("\\r");
                        break;
                    }
                    default: {
                        sb.append("\\").append(Integer.toOctalString(ch));
                        break;
                    }
                }
                continue;
            }
            sb.append(ch);
        }
        return sb.toString();
    }

    private static String attributeSet(AttributeSet as) {
        if (as == null) {
            return "AttributeSet is null";
        }
        StringBuilder sb = new StringBuilder();
        Enumeration keys = as.getAttributeNames();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = as.getAttribute(key);
            if (key == null) {
                sb.append("null");
            } else {
                sb.append("'").append(key.toString()).append("'");
            }
            sb.append(" = ");
            if (value == null) {
                sb.append("null");
            } else {
                sb.append("'").append(value.toString()).append("'");
            }
            if (!keys.hasMoreElements()) continue;
            sb.append(", ");
        }
        return sb.toString();
    }

    private final class TSInfo<T extends TokenId> {
        final TokenSequence<T> ts;
        final FCSInfo<T> fcsInfo;
        int tokenOffset;
        int tokenEndOffset;
        AttributeSet tokenAttrs;

        public TSInfo(TokenSequence<T> ts) {
            this.ts = ts;
            LanguagePath languagePath = ts.languagePath();
            Language innerLanguage = languagePath.innerLanguage();
            String mimePathExt = SyntaxHighlighting.this.mimeTypeForOptions != null ? SyntaxHighlighting.this.languagePathToMimePathOptions(languagePath) : languagePath.mimePath();
            this.fcsInfo = SyntaxHighlighting.this.findFCSInfo(mimePathExt, innerLanguage);
        }

        boolean moveNextToken(int limitStartOffset, int limitEndOffset) {
            if (this.ts.moveNext()) {
                Token token = this.ts.token();
                int nextTokenOffset = this.ts.offset();
                if (nextTokenOffset < 0) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Invalid token offset=" + nextTokenOffset + " < 0. TokenSequence:\n" + this.ts);
                    }
                    return false;
                }
                int nextTokenLength = token.length();
                if (nextTokenOffset < 0) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Invalid token length=" + nextTokenLength + " < 0. TokenSequence:\n" + this.ts);
                    }
                    return false;
                }
                if (nextTokenOffset < this.tokenEndOffset && nextTokenLength < 0) {
                    return false;
                }
                this.tokenOffset = nextTokenOffset;
                this.tokenEndOffset = this.tokenOffset + nextTokenLength;
                if (this.tokenEndOffset <= limitStartOffset) {
                    this.ts.move(limitStartOffset);
                    if (!this.ts.moveNext()) {
                        return false;
                    }
                    token = this.ts.token();
                    this.tokenOffset = this.ts.offset();
                    this.tokenEndOffset = this.tokenOffset + token.length();
                }
                this.tokenOffset = Math.max(this.tokenOffset, limitStartOffset);
                TokenId id = token.id();
                if (this.tokenEndOffset > limitEndOffset) {
                    if (this.tokenOffset >= limitEndOffset) {
                        return false;
                    }
                    this.tokenEndOffset = limitEndOffset;
                }
                this.tokenAttrs = this.fcsInfo.findAttrs(id);
                if (LOG.isLoggable(Level.FINE)) {
                    this.tokenAttrs = AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.Tooltip, "<html><b>Token:</b> " + token.text() + "<br><b>Id:</b> " + id.name() + "<br><b>Category:</b> " + id.primaryCategory() + "<br><b>Ordinal:</b> " + id.ordinal() + "<br><b>Mimepath:</b> " + this.ts.languagePath().mimePath()}), this.tokenAttrs});
                }
                return true;
            }
            this.tokenOffset = this.tokenEndOffset;
            return false;
        }
    }

    private static final class FCSInfo<T extends TokenId>
    implements LookupListener {
        private volatile ChangeEvent changeEvent;
        private final Language<T> innerLanguage;
        private final String mimePath;
        private final ListenerList<ChangeListener> listeners;
        private final Lookup.Result<FontColorSettings> result;
        private AttributeSet[] tokenId2attrs;
        FontColorSettings fcs;

        public FCSInfo(String mimePath, Language<T> innerLanguage) {
            this.innerLanguage = innerLanguage;
            this.mimePath = mimePath;
            this.listeners = new ListenerList();
            Lookup lookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)mimePath));
            this.result = lookup.lookupResult(FontColorSettings.class);
            if (SyntaxHighlighting.TEST_FALLBACK_COLORING == null) {
                this.result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.result));
            }
            this.updateFCS();
        }

        synchronized AttributeSet findAttrs(T tokenId) {
            AttributeSet attrs = this.tokenId2attrs[tokenId.ordinal()];
            if (attrs == null && this.fcs != null) {
                String primary;
                String name = tokenId.name();
                attrs = this.fcs.getTokenFontColors(name);
                if (attrs == null && (primary = tokenId.primaryCategory()) != null) {
                    attrs = this.fcs.getTokenFontColors(primary);
                }
                if (attrs == null) {
                    String c;
                    List categories = this.innerLanguage.nonPrimaryTokenCategories(tokenId);
                    Iterator i$ = categories.iterator();
                    while (i$.hasNext() && (attrs = this.fcs.getTokenFontColors(c = (String)i$.next())) == null) {
                    }
                }
                if (attrs == null) {
                    attrs = SyntaxHighlighting.TEST_FALLBACK_COLORING;
                }
                this.tokenId2attrs[tokenId.ordinal()] = attrs;
            }
            return attrs;
        }

        public void addChangeListener(ChangeListener l) {
            this.listeners.add((EventListener)l);
        }

        public void removeChangeListener(ChangeListener l) {
            this.listeners.remove((EventListener)l);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void updateFCS() {
            FontColorSettings newFCS = (FontColorSettings)this.result.allInstances().iterator().next();
            if (newFCS == null && LOG.isLoggable(Level.WARNING)) {
                LOG.warning("No FontColorSettings for '" + this.mimePath + "' mime path.");
            }
            FCSInfo fCSInfo = this;
            synchronized (fCSInfo) {
                this.fcs = newFCS;
                if (this.innerLanguage != null) {
                    this.tokenId2attrs = new AttributeSet[this.innerLanguage.maxOrdinal() + 1];
                }
            }
        }

        private ChangeEvent createChangeEvent() {
            if (this.changeEvent == null) {
                this.changeEvent = new ChangeEvent(this);
            }
            return this.changeEvent;
        }

        public void resultChanged(LookupEvent ev) {
            this.updateFCS();
            ChangeEvent e = this.createChangeEvent();
            for (ChangeListener l : this.listeners.getListeners()) {
                l.stateChanged(e);
            }
        }
    }

    private static final class LogHelper {
        int tokenCount;
        long startTime;

        private LogHelper() {
        }
    }

    private final class HSImpl
    implements HighlightsSequence {
        private static final int S_INIT = 0;
        private static final int S_TOKEN = 1;
        private static final int S_NEXT_TOKEN = 2;
        private static final int S_EMBEDDED_HEAD = 3;
        private static final int S_EMBEDDED_TAIL = 4;
        private static final int S_DONE = 5;
        private final long version;
        private final TokenHierarchy<? extends Document> scanner;
        private final int startOffset;
        private final int endOffset;
        private final CharSequence docText;
        private int newlineOffset;
        private int partsEndOffset;
        private int hiStartOffset;
        private int hiEndOffset;
        private AttributeSet hiAttrs;
        private List<TSInfo<?>> sequences;
        private int state;
        private LogHelper logHelper;

        public HSImpl(long version, TokenHierarchy<? extends Document> scanner, int startOffset, int endOffset) {
            this.state = 0;
            this.version = version;
            this.scanner = scanner;
            this.startOffset = startOffset = Math.max(startOffset, 0);
            this.sequences = new ArrayList(4);
            this.hiStartOffset = startOffset;
            this.hiEndOffset = startOffset;
            Document doc = (Document)scanner.inputSource();
            this.docText = DocumentUtilities.getText((Document)doc);
            this.endOffset = endOffset = Math.min(endOffset, this.docText.length());
            this.newlineOffset = -1;
            this.updateNewlineOffset(startOffset);
            TokenSequence seq = scanner.tokenSequence();
            if (seq != null) {
                seq.move(startOffset);
                TSInfo tsInfo = new TSInfo(seq);
                this.sequences.add(tsInfo);
                this.state = 2;
            } else {
                this.state = 5;
            }
            if (LOG.isLoggable(Level.FINE)) {
                this.logHelper = new LogHelper();
                this.logHelper.startTime = System.currentTimeMillis();
                LOG.fine("SyntaxHighlighting.HSImpl <" + startOffset + "," + endOffset + ">\n");
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.log(Level.FINEST, "Highlighting caller", new Exception());
                }
            }
        }

        @Override
        public boolean moveNext() {
            if (this.state == 5) {
                return false;
            }
            if (SyntaxHighlighting.this.getVersion() != this.version) {
                this.finish();
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("SyntaxHighlighting: Version changed => HSImpl finished at offset=" + this.hiEndOffset);
                }
                return false;
            }
            if (this.partsEndOffset != 0) {
                while (this.hiEndOffset == this.newlineOffset) {
                    ++this.hiEndOffset;
                    if (this.updateNewlineOffset(this.hiEndOffset)) {
                        this.finish();
                        return false;
                    }
                    if (this.hiEndOffset < this.partsEndOffset) continue;
                    this.finishParts();
                    return this.moveTheSequence();
                }
                this.hiStartOffset = this.hiEndOffset;
                if (this.newlineOffset < this.partsEndOffset) {
                    this.hiEndOffset = this.newlineOffset;
                } else {
                    this.hiEndOffset = this.partsEndOffset;
                    this.finishParts();
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("  SH.moveNext(): part-Highlight: <" + this.hiStartOffset + "," + this.hiEndOffset + "> attrs=" + this.hiAttrs + " " + this.stateToString() + ", pEOffset=" + this.partsEndOffset + ", seq#=" + this.sequences.size() + "\n");
                }
                return true;
            }
            return this.moveTheSequence();
        }

        @Override
        public int getStartOffset() {
            return this.hiStartOffset;
        }

        @Override
        public int getEndOffset() {
            return this.hiEndOffset;
        }

        @Override
        public AttributeSet getAttributes() {
            return this.hiAttrs;
        }

        private boolean moveTheSequence() {
            boolean done = false;
            boolean log = LOG.isLoggable(Level.FINE);
            block6 : do {
                TSInfo tsInfo = this.sequences.get(this.sequences.size() - 1);
                switch (this.state) {
                    case 1: {
                        TokenSequence embeddedSeq = tsInfo.ts.embedded();
                        if (embeddedSeq != null) {
                            TSInfo embeddedTSInfo = new TSInfo(embeddedSeq);
                            this.sequences.add(embeddedTSInfo);
                            if (embeddedTSInfo.moveNextToken(this.startOffset, this.endOffset)) {
                                int headLen = embeddedTSInfo.tokenOffset - this.hiEndOffset;
                                if (headLen <= 0) continue block6;
                                this.state = 3;
                                done = this.assignHighlightOrPart(embeddedTSInfo.tokenOffset, tsInfo.tokenAttrs);
                                if (!log) continue block6;
                                LOG.fine(" S_TOKEN -> S_EMBEDDED_HEAD, token<" + tsInfo.tokenOffset + "," + tsInfo.tokenEndOffset + "> headLen=" + headLen + "\n");
                                break;
                            }
                            this.state = 4;
                            done = this.assignHighlightOrPart(tsInfo);
                            if (!log) continue block6;
                            LOG.fine(" S_TOKEN -> S_EMBEDDED_TAIL\n");
                            break;
                        }
                        this.state = 2;
                        done = this.assignHighlightOrPart(tsInfo);
                        break;
                    }
                    case 2: {
                        if (tsInfo.moveNextToken(this.startOffset, this.endOffset)) {
                            this.state = 1;
                            if (!log) continue block6;
                            ++this.logHelper.tokenCount;
                            break;
                        }
                        if (this.sequences.size() > 1) {
                            TSInfo outerTSInfo = this.sequences.get(this.sequences.size() - 2);
                            this.state = 4;
                            if (tsInfo.tokenEndOffset >= outerTSInfo.tokenEndOffset) continue block6;
                            done = this.assignHighlightOrPart(outerTSInfo);
                            break;
                        }
                        this.sequences.clear();
                        this.finish();
                        if (log) {
                            LOG.fine("SyntaxHighlighting: " + this.scanner.inputSource() + ":\n-> returned " + this.logHelper.tokenCount + " token highlights for <" + this.startOffset + "," + this.endOffset + "> in " + (System.currentTimeMillis() - this.logHelper.startTime) + " ms.\n");
                            LOG.finer(tsInfo.ts.toString());
                            LOG.fine("\n");
                        }
                        return false;
                    }
                    case 3: {
                        this.state = 1;
                        break;
                    }
                    case 4: {
                        this.state = 2;
                        this.sequences.remove(this.sequences.size() - 1);
                        if (!log) continue block6;
                        LOG.fine("S_EMBEDDED_TAIL -> S_NEXT_TOKEN; sequences.size()=" + this.sequences.size() + "\n");
                        break;
                    }
                    default: {
                        throw new IllegalStateException("Invalid state: " + this.state);
                    }
                }
            } while (!done);
            if (log) {
                LOG.fine("SH.moveTheSequence(): Highlight: <" + this.hiStartOffset + "," + this.hiEndOffset + "> attrs=" + this.hiAttrs + " " + this.stateToString() + ", seq#=" + this.sequences.size() + "\n");
            }
            return true;
        }

        private boolean assignHighlightOrPart(TSInfo<?> tsInfo) {
            return this.assignHighlightOrPart(tsInfo.tokenEndOffset, tsInfo.tokenAttrs);
        }

        private boolean assignHighlightOrPart(int tokenEndOffset, AttributeSet attrs) {
            while (this.hiEndOffset == this.newlineOffset) {
                ++this.hiEndOffset;
                if (!this.updateNewlineOffset(this.hiEndOffset) && this.hiEndOffset < tokenEndOffset) continue;
                this.hiStartOffset = this.hiEndOffset;
                return false;
            }
            this.hiStartOffset = this.hiEndOffset;
            if (this.newlineOffset < tokenEndOffset) {
                this.hiEndOffset = this.newlineOffset;
                this.partsEndOffset = tokenEndOffset;
            } else {
                this.hiEndOffset = tokenEndOffset;
            }
            this.hiAttrs = attrs;
            return true;
        }

        private boolean updateNewlineOffset(int offset) {
            while (offset < this.endOffset) {
                if (this.docText.charAt(offset) == '\n') {
                    this.newlineOffset = offset;
                    return false;
                }
                ++offset;
            }
            this.newlineOffset = this.endOffset;
            return true;
        }

        private void finishParts() {
            this.partsEndOffset = 0;
        }

        private void finish() {
            this.state = 5;
            this.hiStartOffset = this.endOffset;
            this.hiEndOffset = this.endOffset;
            this.hiAttrs = null;
        }

        private String stateToString() {
            switch (this.state) {
                case 0: {
                    return "S_INIT";
                }
                case 1: {
                    return "S_TOKEN";
                }
                case 2: {
                    return "S_NEXT_TOKEN";
                }
                case 3: {
                    return "S_EMBEDDED_HEAD";
                }
                case 4: {
                    return "S_EMBEDDED_TAIL";
                }
                case 5: {
                    return "S_DONE";
                }
            }
            throw new IllegalStateException("Unknown state=" + this.state);
        }
    }

}

