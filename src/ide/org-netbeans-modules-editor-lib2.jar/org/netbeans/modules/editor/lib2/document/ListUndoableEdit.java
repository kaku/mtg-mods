/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import java.util.AbstractList;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

public class ListUndoableEdit
extends AbstractList<UndoableEdit>
implements UndoableEdit {
    private UndoableEdit[] edits;

    public ListUndoableEdit(UndoableEdit e) {
        this.edits = new UndoableEdit[]{e};
    }

    public ListUndoableEdit(UndoableEdit e0, UndoableEdit e1) {
        this.edits = new UndoableEdit[]{e0, e1};
    }

    public void setDelegate(UndoableEdit edit) {
        UndoableEdit[] newEdits = new UndoableEdit[this.edits.length + 1];
        System.arraycopy(this.edits, 0, newEdits, 0, this.edits.length);
        newEdits[this.edits.length] = edit;
        this.edits = newEdits;
    }

    @Override
    public UndoableEdit get(int index) {
        return this.edits[index];
    }

    @Override
    public int size() {
        return this.edits.length;
    }

    public UndoableEdit delegate() {
        return this.edits[this.edits.length - 1];
    }

    @Override
    public void undo() throws CannotUndoException {
        this.delegate().undo();
    }

    @Override
    public boolean canUndo() {
        return this.delegate().canUndo();
    }

    @Override
    public void redo() throws CannotRedoException {
        this.delegate().redo();
    }

    @Override
    public boolean canRedo() {
        return this.delegate().canRedo();
    }

    @Override
    public void die() {
        this.delegate().die();
    }

    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        return this.delegate().addEdit(anEdit);
    }

    @Override
    public boolean replaceEdit(UndoableEdit anEdit) {
        return this.delegate().replaceEdit(anEdit);
    }

    @Override
    public boolean isSignificant() {
        return this.delegate().isSignificant();
    }

    @Override
    public String getPresentationName() {
        return this.delegate().getPresentationName();
    }

    @Override
    public String getUndoPresentationName() {
        return this.delegate().getUndoPresentationName();
    }

    @Override
    public String getRedoPresentationName() {
        return this.delegate().getRedoPresentationName();
    }
}

