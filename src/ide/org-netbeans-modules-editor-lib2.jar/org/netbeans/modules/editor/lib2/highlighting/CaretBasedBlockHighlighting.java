/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.BlockCompare
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.swing.BlockCompare;
import org.netbeans.modules.editor.lib2.DocUtils;
import org.netbeans.modules.editor.lib2.RectangularSelectionUtils;
import org.netbeans.modules.editor.lib2.highlighting.BlockHighlighting;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.netbeans.spi.editor.highlighting.support.PositionsBag;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public abstract class CaretBasedBlockHighlighting
extends AbstractHighlightsContainer
implements ChangeListener,
PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(CaretBasedBlockHighlighting.class.getName());
    private boolean inited;
    private final MimePath mimePath;
    private final JTextComponent component;
    private Caret caret;
    private ChangeListener caretListener;
    private final String coloringName;
    private final boolean extendsEOL;
    private final boolean extendsEmptyLine;
    private Position currentBlockStart;
    private Position currentBlockEnd;
    private AttributeSet attribs;
    private LookupListener lookupListener;

    protected CaretBasedBlockHighlighting(JTextComponent component, String coloringName, boolean extendsEOL, boolean extendsEmptyLine) {
        String mimeType = BlockHighlighting.getMimeType(component);
        this.mimePath = mimeType == null ? MimePath.EMPTY : MimePath.parse((String)mimeType);
        this.coloringName = coloringName;
        this.extendsEOL = extendsEOL;
        this.extendsEmptyLine = extendsEmptyLine;
        this.component = component;
    }

    private void init() {
        this.component.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.component));
        this.caret = this.component.getCaret();
        if (this.caret != null) {
            this.caretListener = WeakListeners.change((ChangeListener)this, (Object)this.caret);
            this.caret.addChangeListener(this.caretListener);
        }
        this.updateLineInfo(false);
    }

    protected final JTextComponent component() {
        return this.component;
    }

    protected final Caret caret() {
        return this.caret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        Position blockEnd;
        Position blockStart;
        if (!this.inited) {
            this.inited = true;
            this.init();
        }
        CaretBasedBlockHighlighting caretBasedBlockHighlighting = this;
        synchronized (caretBasedBlockHighlighting) {
            blockStart = this.currentBlockStart;
            blockEnd = this.currentBlockEnd;
        }
        if (blockStart != null && blockEnd != null && endOffset >= blockStart.getOffset() && startOffset <= blockEnd.getOffset()) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Queried for highlights in <" + startOffset + ", " + endOffset + ">, returning <" + CaretBasedBlockHighlighting.positionToString(blockStart) + ", " + CaretBasedBlockHighlighting.positionToString(blockEnd) + ">" + ", layer=" + CaretBasedBlockHighlighting.s2s(this) + '\n');
            }
            return new SimpleHighlightsSequence(Math.max(blockStart.getOffset(), startOffset), Math.min(blockEnd.getOffset(), endOffset), this.getAttribs());
        }
        return HighlightsSequence.EMPTY;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == null || "caret".equals(evt.getPropertyName())) {
            if (this.caret != null) {
                this.caret.removeChangeListener(this.caretListener);
                this.caretListener = null;
            }
            this.caret = this.component.getCaret();
            if (this.caret != null) {
                this.caretListener = WeakListeners.change((ChangeListener)this, (Object)this.caret);
                this.caret.addChangeListener(this.caretListener);
            }
            this.updateLineInfo(true);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.updateLineInfo(true);
    }

    protected abstract Position[] getCurrentBlockPositions(Document var1);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private final void updateLineInfo(boolean fire) {
        ((AbstractDocument)this.component.getDocument()).readLock();
        try {
            Position changeStart;
            Position changeEnd;
            Position[] newBlock = this.getCurrentBlockPositions(this.component.getDocument());
            CaretBasedBlockHighlighting caretBasedBlockHighlighting = this;
            synchronized (caretBasedBlockHighlighting) {
                Position newEnd;
                Position newStart;
                if (newBlock != null) {
                    newStart = newBlock[0];
                    newEnd = newBlock[1];
                    if (this.currentBlockStart == null) {
                        if (newStart.getOffset() < newEnd.getOffset()) {
                            changeStart = newStart;
                            changeEnd = newEnd;
                        } else {
                            changeStart = null;
                            changeEnd = null;
                        }
                    } else {
                        BlockCompare compare = BlockCompare.get((int)newStart.getOffset(), (int)newEnd.getOffset(), (int)this.currentBlockStart.getOffset(), (int)this.currentBlockEnd.getOffset());
                        if (compare.invalidX()) {
                            changeStart = null;
                            changeEnd = null;
                        } else if (compare.equal()) {
                            changeStart = null;
                            changeEnd = null;
                        } else if (compare.equalStart()) {
                            if (compare.containsStrict()) {
                                changeStart = this.currentBlockEnd;
                                changeEnd = newEnd;
                            } else {
                                assert (compare.insideStrict());
                                changeStart = newEnd;
                                changeEnd = this.currentBlockEnd;
                            }
                        } else if (compare.equalEnd()) {
                            if (compare.containsStrict()) {
                                changeStart = newStart;
                                changeEnd = this.currentBlockStart;
                            } else {
                                assert (compare.insideStrict());
                                changeStart = this.currentBlockStart;
                                changeEnd = newStart;
                            }
                        } else {
                            changeStart = compare.lowerStart() ? newStart : this.currentBlockStart;
                            changeEnd = compare.lowerEnd() ? this.currentBlockEnd : newEnd;
                        }
                    }
                } else {
                    newStart = null;
                    newEnd = null;
                    changeStart = this.currentBlockStart;
                    changeEnd = this.currentBlockEnd;
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Caret selection block changed from [" + CaretBasedBlockHighlighting.positionToString(this.currentBlockStart) + ", " + CaretBasedBlockHighlighting.positionToString(this.currentBlockEnd) + "] to [" + CaretBasedBlockHighlighting.positionToString(newStart) + ", " + CaretBasedBlockHighlighting.positionToString(newEnd) + "]" + ", layer=" + CaretBasedBlockHighlighting.s2s(this) + '\n');
                }
                this.currentBlockStart = newStart;
                this.currentBlockEnd = newEnd;
            }
            if (changeStart != null && fire) {
                this.fireHighlightsChange(changeStart.getOffset(), changeEnd.getOffset());
            }
        }
        finally {
            ((AbstractDocument)this.component.getDocument()).readUnlock();
        }
    }

    protected final AttributeSet getAttribs() {
        if (this.lookupListener == null) {
            this.lookupListener = new LookupListener(){

                public void resultChanged(LookupEvent ev) {
                    Lookup.Result result = (Lookup.Result)ev.getSource();
                    CaretBasedBlockHighlighting.this.setAttrs(result);
                }
            };
            Lookup lookup = MimeLookup.getLookup((MimePath)this.mimePath);
            Lookup.Result result = lookup.lookupResult(FontColorSettings.class);
            this.setAttrs(result);
            result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupListener, (Object)result));
        }
        return this.attribs;
    }

    void setAttrs(Lookup.Result<FontColorSettings> result) {
        if (Boolean.TRUE.equals(this.component.getClientProperty("AsTextField"))) {
            this.attribs = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Background, (Color)UIManager.get("TextField.selectionBackground"), StyleConstants.Foreground, (Color)UIManager.get("TextField.selectionForeground")});
            return;
        }
        FontColorSettings fcs = (FontColorSettings)result.allInstances().iterator().next();
        this.attribs = fcs.getFontColors(this.coloringName);
        if (this.attribs == null) {
            this.attribs = SimpleAttributeSet.EMPTY;
        } else if (this.extendsEOL || this.extendsEmptyLine) {
            this.attribs = AttributesUtilities.createImmutable((AttributeSet[])new AttributeSet[]{this.attribs, AttributesUtilities.createImmutable((Object[])new Object[]{"org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EOL", this.extendsEOL, "org.netbeans.spi.editor.highlighting.HighlightsContainer.ATTR_EXTENDS_EMPTY_LINE", this.extendsEmptyLine})});
        }
    }

    private static String positionToString(Position p) {
        return p == null ? "null" : p.toString();
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getSimpleName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    public static final class TextSelectionHighlighting
    extends CaretBasedBlockHighlighting
    implements HighlightsChangeListener {
        public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.lib2.highlighting.TextSelectionHighlighting";
        private int hlChangeStartOffset = -1;
        private int hlChangeEndOffset;
        private PositionsBag rectangularSelectionBag;

        public TextSelectionHighlighting(JTextComponent component) {
            super(component, "selection", true, true);
        }

        @Override
        protected Position[] getCurrentBlockPositions(Document document) {
            int markOffset;
            int caretOffset;
            Caret caret = this.caret();
            if (document != null && caret != null && (caretOffset = caret.getDot()) != (markOffset = caret.getMark())) {
                try {
                    return new Position[]{document.createPosition(Math.min(caretOffset, markOffset)), document.createPosition(Math.max(caretOffset, markOffset))};
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, e.getMessage(), e);
                }
            }
            return null;
        }

        @Override
        public HighlightsSequence getHighlights(int startOffset, int endOffset) {
            if (!RectangularSelectionUtils.isRectangularSelection(this.component())) {
                return super.getHighlights(startOffset, endOffset);
            }
            return this.rectangularSelectionBag != null ? this.rectangularSelectionBag.getHighlights(startOffset, endOffset) : HighlightsSequence.EMPTY;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            super.propertyChange(evt);
            if (RectangularSelectionUtils.getRectangularSelectionProperty().equals(evt.getPropertyName())) {
                this.fireHighlightsChange(0, this.component().getDocument().getLength());
            }
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            Document doc;
            super.stateChanged(evt);
            JTextComponent c = this.component();
            if (RectangularSelectionUtils.isRectangularSelection(c) && (doc = c.getDocument()) != null) {
                List<Position> regions;
                if (this.rectangularSelectionBag == null) {
                    this.rectangularSelectionBag = new PositionsBag(doc);
                    this.rectangularSelectionBag.addHighlightsChangeListener(this);
                }
                if ((regions = RectangularSelectionUtils.regionsCopy(c)) != null) {
                    AttributeSet attrs = this.getAttribs();
                    this.rectangularSelectionBag.clear();
                    int size = regions.size();
                    int i = 0;
                    while (i < size) {
                        Position startPos = regions.get(i++);
                        Position endPos = regions.get(i++);
                        this.rectangularSelectionBag.addHighlight(startPos, endPos, attrs);
                    }
                    if (this.hlChangeStartOffset != -1) {
                        this.fireHighlightsChange(this.hlChangeStartOffset, this.hlChangeEndOffset);
                        this.hlChangeStartOffset = -1;
                    }
                }
            }
        }

        @Override
        public void highlightChanged(HighlightsChangeEvent evt) {
            if (this.hlChangeStartOffset == -1) {
                this.hlChangeStartOffset = evt.getStartOffset();
                this.hlChangeEndOffset = evt.getEndOffset();
            } else {
                this.hlChangeStartOffset = Math.min(this.hlChangeStartOffset, evt.getStartOffset());
                this.hlChangeEndOffset = Math.max(this.hlChangeEndOffset, evt.getEndOffset());
            }
        }
    }

    public static final class CaretRowHighlighting
    extends CaretBasedBlockHighlighting {
        public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.lib2.highlighting.CaretRowHighlighting";

        public CaretRowHighlighting(JTextComponent component) {
            super(component, "highlight-caret-row", true, false);
        }

        @Override
        protected Position[] getCurrentBlockPositions(Document document) {
            Caret caret = this.caret();
            if (document != null && caret != null) {
                int caretOffset = caret.getDot();
                try {
                    int startOffset = DocUtils.getRowStart(document, caretOffset, 0);
                    int endOffset = DocUtils.getRowEnd(document, caretOffset);
                    return new Position[]{document.createPosition(startOffset), document.createPosition(++endOffset)};
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, e.getMessage(), e);
                }
            }
            return null;
        }
    }

    public static final class SimpleHighlightsSequence
    implements HighlightsSequence {
        private int startOffset;
        private int endOffset;
        private AttributeSet attribs;
        private boolean end = false;

        public SimpleHighlightsSequence(int startOffset, int endOffset, AttributeSet attribs) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.attribs = attribs;
        }

        @Override
        public boolean moveNext() {
            if (!this.end) {
                this.end = true;
                return true;
            }
            return false;
        }

        @Override
        public int getStartOffset() {
            return this.startOffset;
        }

        @Override
        public int getEndOffset() {
            return this.endOffset;
        }

        @Override
        public AttributeSet getAttributes() {
            return this.attribs;
        }
    }

}

