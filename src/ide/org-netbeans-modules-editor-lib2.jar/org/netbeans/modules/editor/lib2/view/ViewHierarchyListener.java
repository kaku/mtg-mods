/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.EventListener;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;

public interface ViewHierarchyListener
extends EventListener {
    public void viewHierarchyChanged(@NonNull ViewHierarchyEvent var1);
}

