/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import org.netbeans.modules.editor.lib2.view.EditorView;

final class ViewPart {
    final EditorView view;
    final float width;
    final float xShift;
    final int index;

    ViewPart(EditorView view, float width) {
        this(view, width, 0.0f, -1);
    }

    ViewPart(EditorView part, float width, float xShift, int index) {
        assert (part != null);
        this.view = part;
        this.width = width;
        this.xShift = xShift;
        this.index = index;
    }

    boolean isPart() {
        return this.index != -1;
    }

    boolean isFirstPart() {
        return this.index == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(100);
        sb.append("view=").append(this.view).append(", width=").append(this.width).append(", xShift=").append(this.xShift).append(", index=").append(this.index);
        return sb.toString();
    }
}

