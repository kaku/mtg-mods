/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.editor.lib2.typinghooks;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.lib2.typinghooks.DeletedTextInterceptorsManager;
import org.netbeans.modules.editor.lib2.typinghooks.TypingHooksSpiAccessor;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;

public final class TypedBreakInterceptorsManager {
    private static final Logger LOG = Logger.getLogger(TypedBreakInterceptorsManager.class.getName());
    private static TypedBreakInterceptorsManager instance;
    private Transaction transaction = null;
    private final Map<MimePath, Reference<Collection<TypedBreakInterceptor>>> cache = new WeakHashMap<MimePath, Reference<Collection<TypedBreakInterceptor>>>();

    public static TypedBreakInterceptorsManager getInstance() {
        if (instance == null) {
            instance = new TypedBreakInterceptorsManager();
        }
        return instance;
    }

    public Transaction openTransaction(JTextComponent c, int caretOffset, int insertBreakOffset) {
        TypedBreakInterceptorsManager typedBreakInterceptorsManager = this;
        synchronized (typedBreakInterceptorsManager) {
            if (this.transaction == null) {
                this.transaction = new Transaction(c, caretOffset, insertBreakOffset);
                return this.transaction;
            }
            throw new IllegalStateException("Too many transactions; only one at a time is allowed!");
        }
    }

    private TypedBreakInterceptorsManager() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<? extends TypedBreakInterceptor> getInterceptors(Document doc, int offset) {
        MimePath mimePath = DeletedTextInterceptorsManager.getMimePath(doc, offset);
        Map<MimePath, Reference<Collection<TypedBreakInterceptor>>> map = this.cache;
        synchronized (map) {
            Collection<TypedBreakInterceptor> interceptors;
            Reference<Collection<TypedBreakInterceptor>> ref = this.cache.get((Object)mimePath);
            Collection<TypedBreakInterceptor> collection = interceptors = ref == null ? null : ref.get();
            if (interceptors == null) {
                Collection factories = MimeLookup.getLookup((MimePath)mimePath).lookupAll(TypedBreakInterceptor.Factory.class);
                interceptors = new HashSet<TypedBreakInterceptor>(factories.size());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "TypedBreakInterceptor.Factory instances for {0}:", mimePath.getPath());
                }
                for (TypedBreakInterceptor.Factory f : factories) {
                    TypedBreakInterceptor interceptor = f.createTypedBreakInterceptor(mimePath);
                    if (interceptor != null) {
                        interceptors.add(interceptor);
                    }
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.log(Level.FINE, "    {0} created: {1}", new Object[]{f, interceptor});
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("");
                }
                this.cache.put(mimePath, new SoftReference<Collection<TypedBreakInterceptor>>(interceptors));
            }
            return interceptors;
        }
    }

    public final class Transaction {
        private final TypedBreakInterceptor.MutableContext context;
        private final Collection<? extends TypedBreakInterceptor> interceptors;
        private int phase;

        public boolean beforeInsertion() {
            for (TypedBreakInterceptor i : this.interceptors) {
                try {
                    if (!i.beforeInsert(this.context)) continue;
                    return true;
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "TypedBreakInterceptor crashed in beforeInsert(): " + i, e);
                    continue;
                }
            }
            ++this.phase;
            return false;
        }

        public Object[] textTyped() {
            Object[] data = null;
            for (TypedBreakInterceptor i : this.interceptors) {
                try {
                    i.insert(this.context);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "TypedBreakInterceptor crashed in insert(): " + i, e);
                    TypingHooksSpiAccessor.get().resetTbiContextData(this.context);
                    continue;
                }
                data = TypingHooksSpiAccessor.get().getTbiContextData(this.context);
                if (data == null) continue;
                break;
            }
            ++this.phase;
            return data;
        }

        public void afterInsertion() {
            for (TypedBreakInterceptor i : this.interceptors) {
                try {
                    i.afterInsert(this.context);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "TypedBreakInterceptor crashed in afterInsert(): " + i, e);
                }
            }
            ++this.phase;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void close() {
            if (this.phase < 3) {
                for (TypedBreakInterceptor i : this.interceptors) {
                    try {
                        i.cancelled(this.context);
                    }
                    catch (Exception e) {
                        LOG.log(Level.INFO, "TypedBreakInterceptor crashed in cancelled(): " + i, e);
                    }
                }
            }
            TypedBreakInterceptorsManager i$ = TypedBreakInterceptorsManager.this;
            synchronized (i$) {
                TypedBreakInterceptorsManager.this.transaction = null;
            }
        }

        private Transaction(JTextComponent c, int caretOffset, int insertBreakOffset) {
            this.phase = 0;
            this.context = TypingHooksSpiAccessor.get().createTbiContext(c, caretOffset, insertBreakOffset);
            this.interceptors = TypedBreakInterceptorsManager.this.getInterceptors(c.getDocument(), insertBreakOffset);
        }
    }

}

