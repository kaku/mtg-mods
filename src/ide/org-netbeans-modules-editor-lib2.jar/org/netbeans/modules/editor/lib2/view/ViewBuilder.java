/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewChildren;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.OffsetRegion;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.TextLayoutCache;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewReplace;
import org.netbeans.modules.editor.lib2.view.ViewStats;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

final class ViewBuilder {
    static final int MAX_CHARS_FOR_CREATE_LOCAL_VIEWS = 2000;
    private static final Logger LOG = Logger.getLogger(ViewBuilder.class.getName());
    private final ViewReplace<DocumentView, ParagraphView> docReplace;
    private final EditorViewFactory[] viewFactories;
    private FactoryState[] factoryStates;
    private boolean createLocalViews;
    private boolean forceCreateLocalViews;
    private int startCreationOffset;
    private int creationOffset;
    private int matchOffset;
    private int endCreationOffset;
    private int bmReuseOffset;
    private ParagraphView bmReusePView;
    private int bmReuseLocalIndex;
    private int amReuseOffset;
    private int amReusePIndex;
    private ParagraphView amReusePView;
    private int amReuseLocalIndex;
    private int modOffset;
    private int endModOffset;
    private boolean insertAtPViewStart;
    private Element lineRoot;
    private int lineIndex;
    private int lineEndOffset;
    private Element lineForParagraphView;
    private ViewReplace<ParagraphView, EditorView> firstReplace;
    private ViewReplace<ParagraphView, EditorView> localReplace;
    private List<ViewReplace<ParagraphView, EditorView>> allReplaces;
    private volatile boolean staleCreation;
    private RebuildCause rebuildCause;

    ViewBuilder(DocumentView docView, EditorViewFactory[] viewFactories) {
        this.docReplace = new ViewReplace(docView);
        this.viewFactories = viewFactories;
        this.forceCreateLocalViews = docView.op.isAccurateSpan();
    }

    int getStartCreationOffset() {
        return this.startCreationOffset;
    }

    int getMatchOffset() {
        return this.matchOffset;
    }

    boolean isStaleCreation() {
        return this.staleCreation;
    }

    void initFullRebuild() {
        this.rebuildCause = RebuildCause.FULL_REBUILD;
        DocumentView docView = (DocumentView)this.docReplace.view;
        this.endModOffset = Integer.MIN_VALUE;
        this.modOffset = Integer.MIN_VALUE;
        this.startCreationOffset = docView.getStartOffset();
        this.endCreationOffset = this.matchOffset = docView.getEndOffset();
        int buildLen = this.endCreationOffset - this.startCreationOffset;
        this.createLocalViews = this.forceCreateLocalViews || buildLen <= 2000;
        this.docReplace.removeTillEnd();
        this.amReuseOffset = Integer.MAX_VALUE;
    }

    void initParagraphs(int startRebuildIndex, int endRebuildIndex) {
        DocumentView docView = (DocumentView)this.docReplace.view;
        int startOffset = docView.getParagraphView(startRebuildIndex).getStartOffset();
        int endOffset = docView.getParagraphView(endRebuildIndex - 1).getEndOffset();
        this.rebuildCause = RebuildCause.INIT_PARAGRAPHS;
        this.endModOffset = Integer.MIN_VALUE;
        this.modOffset = Integer.MIN_VALUE;
        this.createLocalViews = this.forceCreateLocalViews = true;
        this.docReplace.index = startRebuildIndex;
        this.docReplace.setRemoveCount(endRebuildIndex - startRebuildIndex);
        this.startCreationOffset = startOffset;
        this.matchOffset = endOffset;
        this.endCreationOffset = endOffset;
        this.amReuseOffset = startOffset;
        this.amReusePIndex = startRebuildIndex;
        this.amReusePView = ((DocumentView)this.docReplace.view).getParagraphView(this.amReusePIndex);
        this.amReuseLocalIndex = 0;
    }

    boolean initRebuildParagraphs(OffsetRegion pRegion) {
        int startOffset;
        this.rebuildCause = RebuildCause.REBUILD_PARAGRAPHS;
        DocumentView docView = (DocumentView)this.docReplace.view;
        int startAffectedOffset = pRegion.startOffset();
        int endAffectedOffset = pRegion.endOffset();
        int startCreationIndex = endAffectedOffset < (startOffset = docView.getStartOffset()) ? -1 : (startAffectedOffset >= docView.getEndOffset() ? -1 : docView.getViewIndex(startAffectedOffset));
        if (startCreationIndex != -1) {
            ParagraphView pView = docView.getParagraphView(startCreationIndex);
            this.startCreationOffset = pView.getStartOffset();
            int endCreationIndex = docView.getViewIndex(endAffectedOffset);
            ParagraphView endPView = docView.getParagraphView(endCreationIndex);
            this.endCreationOffset = endPView.getStartOffset();
            if (this.endCreationOffset < endAffectedOffset) {
                this.endCreationOffset += endPView.getLength();
                ++endCreationIndex;
            }
            this.endModOffset = Integer.MIN_VALUE;
            this.modOffset = Integer.MIN_VALUE;
            this.matchOffset = this.endCreationOffset;
            int buildLen = this.endCreationOffset - this.startCreationOffset;
            this.createLocalViews = this.forceCreateLocalViews || buildLen <= 2000;
            this.docReplace.index = startCreationIndex;
            this.docReplace.setRemoveCount(endCreationIndex - startCreationIndex);
            this.amReuseOffset = this.startCreationOffset;
            this.amReusePIndex = startCreationIndex;
            this.amReusePView = ((DocumentView)this.docReplace.view).getParagraphView(this.amReusePIndex);
            this.amReuseLocalIndex = 0;
            return true;
        }
        return false;
    }

    boolean initModUpdate(int modOffset, int modLength, OffsetRegion cRegion) {
        int startCreationPIndex;
        this.rebuildCause = RebuildCause.MOD_UPDATE;
        this.modOffset = modOffset;
        DocumentView docView = (DocumentView)this.docReplace.view;
        int pViewCount = docView.getViewCount();
        int startOffset = docView.getStartOffset();
        int endOffset = docView.getEndOffset();
        ParagraphView startCreationPView = null;
        int startCreationPViewOffset = -1;
        int startCreationLocalOffset = 0;
        int lastAffectedPIndex = -1;
        ParagraphView lastAffectedPView = null;
        int lastAffectedLocalOffset = -1;
        if (modLength > 0) {
            this.createLocalViews = this.forceCreateLocalViews || modLength < 2000;
            this.endModOffset = modOffset + modLength;
            if (modOffset < startOffset) {
                docView.setStartOffset(startOffset + modLength);
                docView.setEndOffset(endOffset + modLength);
                startCreationPIndex = -1;
            } else if (modOffset == startOffset) {
                startCreationPIndex = 0;
                startCreationPView = pViewCount > 0 ? docView.getParagraphView(0) : null;
                startCreationPViewOffset = startOffset;
            } else if (modOffset <= endOffset) {
                startCreationPIndex = docView.getViewIndex(modOffset);
                startCreationPView = docView.getParagraphView(startCreationPIndex);
                startCreationPViewOffset = startCreationPView.getStartOffset();
                if (startCreationPIndex + 1 < pViewCount && startCreationPViewOffset + startCreationPView.getLength() == modOffset) {
                    startCreationPView = docView.getParagraphView(++startCreationPIndex);
                    startCreationPViewOffset = modOffset;
                }
            } else {
                startCreationPIndex = -1;
            }
            if (startCreationPIndex != -1) {
                startCreationLocalOffset = modOffset - startCreationPViewOffset;
                lastAffectedPIndex = startCreationPIndex;
                lastAffectedPView = startCreationPView;
                lastAffectedLocalOffset = startCreationLocalOffset;
                docView.setEndOffset(endOffset + modLength);
            }
        } else {
            int newEndOffset;
            this.createLocalViews = true;
            int removeLen = - modLength;
            int endRemoveOffset = modOffset + removeLen;
            this.endModOffset = modOffset;
            int n = newEndOffset = endOffset > modOffset ? Math.max(modOffset, endOffset - removeLen) : endOffset;
            if (pViewCount == 0) {
                docView.setStartOffset(newEndOffset);
                startCreationPIndex = -1;
            } else if (modOffset < startOffset) {
                if (endRemoveOffset > startOffset) {
                    docView.setStartOffset(modOffset);
                    startCreationPViewOffset = startOffset;
                    startCreationPView = docView.getParagraphView(0);
                    startCreationPIndex = 0;
                    startCreationLocalOffset = 0;
                } else {
                    docView.setStartOffset(startOffset - removeLen);
                    startCreationPIndex = -1;
                }
            } else if (endOffset <= modOffset) {
                startCreationPIndex = -1;
            } else {
                if (modOffset == startOffset) {
                    startCreationPIndex = 0;
                } else {
                    startCreationPIndex = docView.getViewIndex(modOffset - 1);
                    startCreationPView = docView.getParagraphView(startCreationPIndex);
                    startCreationPViewOffset = startCreationPView.getStartOffset();
                    startCreationLocalOffset = modOffset - startCreationPViewOffset;
                    if (startCreationLocalOffset >= startCreationPView.getLength()) {
                        ++startCreationPIndex;
                        startCreationPView = null;
                    }
                }
                if (startCreationPView == null) {
                    startCreationPView = docView.getParagraphView(startCreationPIndex);
                    startCreationPViewOffset = startCreationPView.getStartOffset();
                    startCreationLocalOffset = modOffset - startCreationPViewOffset;
                }
            }
            docView.setEndOffset(newEndOffset);
            if (startCreationPIndex != -1) {
                lastAffectedPIndex = startCreationPIndex;
                lastAffectedPView = startCreationPView;
                int lastAffectedPViewOffset = startCreationPViewOffset;
                int pViewLen = lastAffectedPView.getLength();
                lastAffectedLocalOffset = endRemoveOffset - lastAffectedPViewOffset;
                while (lastAffectedLocalOffset > pViewLen && lastAffectedPIndex + 1 < pViewCount) {
                    lastAffectedPView = docView.getParagraphView(++lastAffectedPIndex);
                    pViewLen = lastAffectedPView.getLength();
                    lastAffectedLocalOffset = endRemoveOffset - (lastAffectedPViewOffset += pViewLen);
                }
                lastAffectedLocalOffset = Math.min(lastAffectedLocalOffset, pViewLen);
            }
        }
        if (startCreationPIndex != -1) {
            this.docReplace.index = startCreationPIndex;
            if (startCreationPView != null) {
                int shift;
                int cRegionStartLocalOffset;
                int endRemoveLocalIndex;
                int cRegionEndLocalOffset;
                boolean reuseNextPView;
                if (cRegion != null) {
                    cRegionStartLocalOffset = cRegion.startOffset() - startCreationPViewOffset;
                    cRegionEndLocalOffset = cRegion.endOffset() - startCreationPViewOffset;
                    if (cRegionEndLocalOffset == cRegionStartLocalOffset || cRegionEndLocalOffset <= 0 || cRegionStartLocalOffset >= startCreationPView.getLength()) {
                        cRegion = null;
                    }
                } else {
                    cRegionEndLocalOffset = 0;
                    cRegionStartLocalOffset = 0;
                }
                this.docReplace.setRemoveCount(lastAffectedPIndex + 1 - this.docReplace.index);
                int lastAffectedPViewLength = lastAffectedPView.getLength();
                int tillLastAffectedEnd = lastAffectedPViewLength - lastAffectedLocalOffset;
                this.endCreationOffset = this.endModOffset + tillLastAffectedEnd;
                if (tillLastAffectedEnd == 0 && lastAffectedPIndex + 1 < pViewCount) {
                    this.endCreationOffset += docView.getParagraphView(lastAffectedPIndex + 1).getLength();
                }
                if (startCreationPView.isChildrenNull()) {
                    this.createLocalViews = this.forceCreateLocalViews;
                    this.bmReuseOffset = modOffset;
                    this.startCreationOffset = modOffset - startCreationLocalOffset;
                } else {
                    int shiftBack = 0;
                    this.bmReusePView = startCreationPView;
                    if (startCreationLocalOffset > 0) {
                        if (!this.createLocalViews) {
                            shiftBack = startCreationLocalOffset;
                        } else {
                            if (cRegion != null) {
                                shiftBack = Math.min(Math.max(cRegionStartLocalOffset, 0), startCreationLocalOffset);
                            }
                            if (!startCreationPView.isChildrenValid()) {
                                int startInvalidOffset = startCreationPView.children.getStartInvalidChildrenLocalOffset();
                                shiftBack = Math.max(shiftBack, startCreationLocalOffset - startInvalidOffset);
                            }
                        }
                        this.bmReuseLocalIndex = startCreationPView.getViewIndexLocalOffset(startCreationLocalOffset -= shiftBack);
                        int viewLocalStartOffset = startCreationPView.getLocalOffset(this.bmReuseLocalIndex);
                        int viewStartShiftBack = startCreationLocalOffset - viewLocalStartOffset;
                        if (viewStartShiftBack == 0 && this.bmReuseLocalIndex > 0) {
                            --this.bmReuseLocalIndex;
                            viewStartShiftBack += startCreationPView.getEditorView(this.bmReuseLocalIndex).getLength();
                        }
                        this.bmReuseOffset = this.startCreationOffset = modOffset - (shiftBack += viewStartShiftBack);
                    } else {
                        if (modLength > 0) {
                            this.insertAtPViewStart = true;
                        }
                        this.bmReuseLocalIndex = 0;
                        this.bmReuseOffset = modOffset;
                        this.startCreationOffset = modOffset;
                    }
                    if (this.createLocalViews) {
                        this.firstReplace = new ViewReplace(startCreationPView);
                        this.firstReplace.index = this.bmReuseLocalIndex;
                        this.localReplace = this.firstReplace;
                        ++this.docReplace.index;
                        this.docReplace.setRemoveCount(this.docReplace.getRemoveCount() - 1);
                    }
                }
                this.amReusePIndex = lastAffectedPIndex;
                this.amReusePView = lastAffectedPView;
                if (lastAffectedPView.isChildrenNull() || !this.createLocalViews) {
                    shift = tillLastAffectedEnd;
                    this.amReuseOffset = this.endModOffset + tillLastAffectedEnd;
                    endRemoveLocalIndex = lastAffectedPView.getViewCount();
                    reuseNextPView = true;
                } else {
                    endRemoveLocalIndex = lastAffectedPView.getViewIndexLocalOffset(lastAffectedLocalOffset);
                    int startLocalOffset = lastAffectedPView.getLocalOffset(endRemoveLocalIndex);
                    if (lastAffectedLocalOffset > startLocalOffset) {
                        int localViewLength = lastAffectedPView.getEditorView(endRemoveLocalIndex).getLength();
                        shift = startLocalOffset + localViewLength - lastAffectedLocalOffset;
                        this.amReuseOffset = this.endModOffset + shift;
                        reuseNextPView = ++endRemoveLocalIndex == lastAffectedPView.getViewCount();
                    } else {
                        shift = 0;
                        this.amReuseOffset = this.endModOffset;
                        reuseNextPView = false;
                    }
                    this.amReuseLocalIndex = endRemoveLocalIndex;
                }
                if (reuseNextPView) {
                    this.amReuseLocalIndex = 0;
                    ++this.amReusePIndex;
                    if (this.amReusePIndex < pViewCount) {
                        this.amReusePView = docView.getParagraphView(this.amReusePIndex);
                    } else {
                        this.amReusePView = null;
                        this.amReuseOffset = Integer.MAX_VALUE;
                    }
                }
                int origShift = shift;
                if (shift < tillLastAffectedEnd) {
                    if (startCreationPView != lastAffectedPView) {
                        shift = tillLastAffectedEnd;
                    } else {
                        if (cRegion != null) {
                            shift = Math.max(shift, Math.min(cRegion.endOffset() - this.endModOffset, tillLastAffectedEnd));
                        }
                        if (!lastAffectedPView.isChildrenValid()) {
                            int endInvalidLocalOffset = lastAffectedPView.children.getEndInvalidChildrenLocalOffset();
                            shift = Math.max(shift, endInvalidLocalOffset - lastAffectedLocalOffset);
                        }
                    }
                }
                if (shift != origShift) {
                    if (shift < tillLastAffectedEnd) {
                        int endALocalOffset = lastAffectedLocalOffset + shift;
                        endRemoveLocalIndex = lastAffectedPView.getViewIndexLocalOffset(endALocalOffset);
                        int startLocalOffset = lastAffectedPView.getLocalOffset(endRemoveLocalIndex);
                        if (endALocalOffset > startLocalOffset) {
                            int localViewLength = lastAffectedPView.getEditorView(endRemoveLocalIndex).getLength();
                            shift = startLocalOffset + localViewLength - lastAffectedLocalOffset;
                            ++endRemoveLocalIndex;
                        }
                    } else {
                        endRemoveLocalIndex = lastAffectedPView.getViewCount();
                    }
                }
                this.matchOffset = this.endModOffset + shift;
                if (this.firstReplace != null) {
                    int endIndex = startCreationPView == lastAffectedPView ? endRemoveLocalIndex : startCreationPView.getViewCount();
                    this.firstReplace.setRemoveCount(endIndex - this.firstReplace.index);
                }
            } else {
                this.docReplace.setRemoveCount(0);
                this.startCreationOffset = startOffset;
                this.matchOffset = this.endCreationOffset = docView.getEndOffset();
                this.bmReuseOffset = modOffset;
                this.amReuseOffset = Integer.MAX_VALUE;
            }
            assert (this.amReuseOffset >= this.endModOffset);
            return true;
        }
        return false;
    }

    boolean createReplaceRepaintViews(boolean force) {
        if (!this.createViews(force)) {
            return false;
        }
        this.replaceRepaintViews();
        return true;
    }

    boolean createViews(boolean force) {
        if (this.startCreationOffset > this.matchOffset) {
            throw new IllegalStateException("startCreationOffset=" + this.startCreationOffset + " > matchOffset=" + this.matchOffset);
        }
        if (this.firstReplace != null && !this.createLocalViews) {
            throw new IllegalStateException("firstReplace != null && !createLocalViews");
        }
        DocumentView docView = (DocumentView)this.docReplace.view;
        if (DocumentView.testRun != null) {
            DocumentView.testValues = new Object[]{this.rebuildCause, this.createLocalViews, this.startCreationOffset, this.matchOffset, this.endCreationOffset, this.bmReuseOffset, this.bmReusePView, this.bmReuseLocalIndex, this.amReuseOffset, this.amReusePIndex, this.amReusePView, this.amReuseLocalIndex};
            DocumentView.testRun.run();
        }
        Document doc = docView.getDocument();
        this.lineRoot = doc.getDefaultRootElement();
        this.lineIndex = this.lineRoot.getElementIndex(this.startCreationOffset);
        Element line = this.lineRoot.getElement(this.lineIndex);
        this.lineEndOffset = line.getEndOffset();
        this.lineForParagraphView = line;
        this.factoryStates = new FactoryState[this.viewFactories.length];
        for (int i = 0; i < this.viewFactories.length; ++i) {
            FactoryState state;
            this.factoryStates[i] = state = new FactoryState(this.viewFactories[i]);
            state.init(this, this.startCreationOffset, this.endCreationOffset, this.createLocalViews);
        }
        this.allReplaces = new ArrayList<ViewReplace<ParagraphView, EditorView>>(2);
        this.creationOffset = this.startCreationOffset;
        if (this.creationOffset < this.matchOffset) {
            while (this.createNextView()) {
                if (!this.staleCreation || force) continue;
                ViewStats.incrementStaleViewCreations();
                if (ViewHierarchyImpl.BUILD_LOG.isLoggable(Level.FINE)) {
                    ViewHierarchyImpl.BUILD_LOG.fine("STALE-CREATION notified => View Rebuild Terminated\n");
                }
                return false;
            }
        }
        if (this.localReplace != null && this.localReplace != this.firstReplace) {
            int length = this.creationOffset - ((ParagraphView)this.localReplace.view).getStartOffset();
            ((ParagraphView)this.localReplace.view).setLength(length);
            this.localReplace = null;
        }
        if (this.firstReplace != null && this.firstReplace.isMakingViewEmpty()) {
            --this.docReplace.index;
            this.docReplace.setRemoveCount(this.docReplace.getRemoveCount() + 1);
            this.firstReplace = null;
        }
        if (ViewHierarchyImpl.BUILD_LOG.isLoggable(Level.FINE)) {
            if (ViewHierarchyImpl.BUILD_LOG.isLoggable(Level.FINEST)) {
                ViewHierarchyImpl.BUILD_LOG.finer("ViewBuilder: DocView-Original-Content:\n" + docView.toStringDetailNeedsLock() + '\n');
            }
            StringBuilder sb = new StringBuilder(200);
            sb.append("ViewBuilder.createViews(): in <").append(this.startCreationOffset);
            sb.append(",").append(this.creationOffset).append("> cause: ").append((Object)this.rebuildCause).append("\n");
            sb.append("Document:").append(doc).append('\n');
            if (this.firstReplace != null) {
                sb.append("FirstReplace[").append(this.docReplace.index - 1).append("]: ").append(this.firstReplace);
            } else {
                sb.append("No-FirstReplace\n");
            }
            sb.append("DocReplace: ").append(this.docReplace);
            sb.append("allReplaces:\n");
            int digitCount = ArrayUtilities.digitCount((int)this.allReplaces.size());
            for (int i2 = 0; i2 < this.allReplaces.size(); ++i2) {
                ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i2, (int)digitCount);
                sb.append(this.allReplaces.get(i2));
            }
            sb.append("-------------END-OF-VIEW-REBUILD-------------\n");
            ViewUtils.log(ViewHierarchyImpl.BUILD_LOG, sb.toString());
        }
        return true;
    }

    boolean createNextView() {
        if (this.creationOffset >= this.endCreationOffset) {
            this.endCreationOffset = this.matchOffset;
            for (int i = this.factoryStates.length - 1; i >= 0; --i) {
                FactoryState state = this.factoryStates[i];
                state.factory.continueCreation(this.creationOffset, this.endCreationOffset);
            }
        }
        DocumentView docView = (DocumentView)this.docReplace.view;
        int limitOffset = this.endCreationOffset;
        boolean forcedLimit = false;
        for (int i = this.factoryStates.length - 1; i >= 0; --i) {
            FactoryState state = this.factoryStates[i];
            int cmp = state.nextViewStartOffset - this.creationOffset;
            if (cmp < 0) {
                state.updateNextViewStartOffset(this.creationOffset);
                cmp = state.nextViewStartOffset - this.creationOffset;
            }
            if (cmp == 0) {
                EditorView createdView;
                int createdViewEndOffset;
                boolean eolView;
                boolean inFirstReplace;
                createdView = null;
                if (this.createLocalViews) {
                    EditorView origView;
                    int nextOrigViewOffset;
                    if (this.creationOffset >= this.endModOffset) {
                        int viewCount;
                        while (this.creationOffset > this.amReuseOffset) {
                            boolean reuseNextPView = true;
                            if (this.amReusePView.isChildrenNull()) {
                                this.amReuseOffset += this.amReusePView.getLength();
                            } else {
                                int viewCount2 = this.amReusePView.getViewCount();
                                while (this.amReuseLocalIndex < viewCount2) {
                                    EditorView view = this.amReusePView.getEditorView(this.amReuseLocalIndex);
                                    this.amReuseOffset += view.getLength();
                                    ++this.amReuseLocalIndex;
                                    if (this.amReuseOffset < this.creationOffset) continue;
                                    reuseNextPView = this.amReuseLocalIndex == viewCount2;
                                    break;
                                }
                            }
                            if (!reuseNextPView) continue;
                            this.amReuseLocalIndex = 0;
                            ++this.amReusePIndex;
                            if (this.amReusePIndex < docView.getViewCount()) {
                                this.amReusePView = docView.getParagraphView(this.amReusePIndex);
                                continue;
                            }
                            this.amReusePView = null;
                            this.amReuseOffset = Integer.MAX_VALUE;
                        }
                        origView = this.creationOffset == this.amReuseOffset ? (this.amReuseLocalIndex < (viewCount = this.amReusePView.getViewCount()) ? this.amReusePView.getEditorView(this.amReuseLocalIndex) : null) : null;
                        nextOrigViewOffset = this.amReuseOffset;
                    } else if (this.creationOffset < this.modOffset) {
                        while (this.creationOffset > this.bmReuseOffset) {
                            EditorView view = this.bmReusePView.getEditorView(this.bmReuseLocalIndex);
                            this.bmReuseOffset += view.getLength();
                            ++this.bmReuseLocalIndex;
                        }
                        if (this.creationOffset == this.bmReuseOffset) {
                            EditorView view = this.bmReusePView.getEditorView(this.bmReuseLocalIndex);
                            this.bmReuseOffset += view.getLength();
                            ++this.bmReuseLocalIndex;
                            if (this.bmReuseOffset <= this.modOffset) {
                                origView = view;
                                nextOrigViewOffset = this.bmReuseOffset;
                            } else {
                                origView = null;
                                nextOrigViewOffset = this.amReuseOffset;
                            }
                        } else {
                            origView = null;
                            nextOrigViewOffset = this.bmReuseOffset <= this.modOffset ? this.bmReuseOffset : this.amReuseOffset;
                        }
                    } else {
                        origView = null;
                        nextOrigViewOffset = Integer.MAX_VALUE;
                    }
                    createdView = state.factory.createView(this.creationOffset, limitOffset, forcedLimit, origView, nextOrigViewOffset);
                    if (createdView == null) {
                        createdViewEndOffset = -1;
                    } else {
                        int viewLength = createdView.getLength();
                        createdViewEndOffset = this.creationOffset + viewLength;
                        assert (viewLength > 0);
                    }
                } else {
                    createdViewEndOffset = state.factory.viewEndOffset(this.creationOffset, limitOffset, forcedLimit);
                }
                if (createdViewEndOffset == -1) {
                    state.updateNextViewStartOffset(this.creationOffset + 1);
                    if (state.nextViewStartOffset > limitOffset) continue;
                    forcedLimit = true;
                    limitOffset = state.nextViewStartOffset;
                    continue;
                }
                this.updateLine(createdViewEndOffset);
                eolView = createdViewEndOffset == this.lineEndOffset;
                boolean bl = inFirstReplace = this.localReplace == this.firstReplace && this.firstReplace != null;
                if (eolView && inFirstReplace && !this.firstReplace.isRemovedTillEnd()) {
                    int remainingLenOnParagraph = ((ParagraphView)this.firstReplace.view).getLength() - ((ParagraphView)this.firstReplace.view).getLocalOffset(this.firstReplace.removeEndIndex());
                    this.matchOffset += remainingLenOnParagraph;
                    this.firstReplace.removeTillEnd();
                }
                if (createdViewEndOffset > this.matchOffset) {
                    boolean matchOffsetValid = false;
                    if (inFirstReplace) {
                        while (!this.firstReplace.isRemovedTillEnd()) {
                            this.matchOffset += ((ParagraphView)this.localReplace.view).getEditorView(this.firstReplace.removeEndIndex()).getLength();
                            this.localReplace.setRemoveCount(this.localReplace.getRemoveCount() + 1);
                            if (createdViewEndOffset > this.matchOffset) continue;
                            matchOffsetValid = true;
                            break;
                        }
                    }
                    if (!matchOffsetValid) {
                        while (!this.docReplace.isRemovedTillEnd()) {
                            this.matchOffset += docView.getParagraphView(this.docReplace.removeEndIndex()).getLength();
                            this.docReplace.setRemoveCount(this.docReplace.getRemoveCount() + 1);
                            if (createdViewEndOffset > this.matchOffset) continue;
                            break;
                        }
                    }
                } else if (createdViewEndOffset == this.matchOffset && inFirstReplace && !eolView && this.localReplace.isRemovedTillEnd() && !this.docReplace.isRemovedTillEnd()) {
                    this.matchOffset += docView.getParagraphView(this.docReplace.removeEndIndex()).getLength();
                    this.docReplace.setRemoveCount(this.docReplace.getRemoveCount() + 1);
                }
                if (this.localReplace == null) {
                    Position startPos;
                    if (this.lineForParagraphView instanceof Position && this.creationOffset == this.lineForParagraphView.getStartOffset()) {
                        startPos = (Position)((Object)this.lineForParagraphView);
                    } else {
                        try {
                            startPos = docView.getDocument().createPosition(this.creationOffset);
                        }
                        catch (BadLocationException e) {
                            throw new IllegalStateException("Cannot create position at offset=" + this.creationOffset, e);
                        }
                    }
                    ParagraphView paragraphView = new ParagraphView(startPos);
                    this.docReplace.add(paragraphView);
                    this.localReplace = new ViewReplace(paragraphView);
                    if (this.createLocalViews) {
                        this.allReplaces.add(this.localReplace);
                    }
                }
                if (this.createLocalViews) {
                    this.localReplace.add(createdView);
                }
                if (eolView) {
                    if (this.localReplace != this.firstReplace) {
                        int length = createdViewEndOffset - ((ParagraphView)this.localReplace.view).getStartOffset();
                        ((ParagraphView)this.localReplace.view).setLength(length);
                    }
                    this.localReplace = null;
                    this.lineForParagraphView = this.lineIndex + 1 < this.lineRoot.getElementCount() ? this.lineRoot.getElement(this.lineIndex + 1) : null;
                }
                this.creationOffset = createdViewEndOffset;
                return this.creationOffset < this.matchOffset;
            }
            if (state.nextViewStartOffset > limitOffset) continue;
            forcedLimit = true;
            limitOffset = state.nextViewStartOffset;
        }
        throw new IllegalStateException("No factory returned view for offset=" + this.creationOffset);
    }

    private void transcribe(ParagraphView origPView, ParagraphView newPView) {
        float origWidth = origPView.getWidth();
        newPView.setWidth(origWidth);
        float origHeight = origPView.getHeight();
        newPView.setHeight(origHeight);
    }

    private void replaceRepaintViews() {
        double startY;
        double deltaY;
        boolean firstReplaceValid;
        double endY;
        DocumentView docView = (DocumentView)this.docReplace.view;
        TextLayoutCache tlCache = docView.op.getTextLayoutCache();
        JTextComponent textComponent = docView.getTextComponent();
        assert (textComponent != null);
        boolean bl = firstReplaceValid = this.firstReplace != null && this.firstReplace.isChanged();
        if (firstReplaceValid) {
            ParagraphView pView = (ParagraphView)this.firstReplace.view;
            if (this.insertAtPViewStart) {
                pView.setStartPosition(ViewUtils.createPosition(docView.getDocument(), this.modOffset));
            }
            if (!pView.isChildrenValid()) {
                int invalidLOffset = pView.children.getStartInvalidChildrenLocalOffset();
                assert (pView.children.startOffset(this.firstReplace.index) <= invalidLOffset);
                int invalidEndLOffset = pView.children.getEndInvalidChildrenLocalOffset();
                assert (pView.children.startOffset(this.firstReplace.removeEndIndex()) >= invalidEndLOffset);
                pView.markChildrenValid();
            }
            pView.replace(this.firstReplace.index, this.firstReplace.getRemoveCount(), this.firstReplace.addedViews());
            tlCache.activate(pView);
        }
        List<ParagraphView> addedPViews = this.docReplace.added();
        int addedCount = this.docReplace.addedSize();
        int removeCount = this.docReplace.getRemoveCount();
        int index = this.docReplace.index;
        int i0 = 0;
        int i1 = addedCount;
        int i1Orig = removeCount;
        int commonCount = Math.min(addedCount, removeCount);
        if (commonCount > 0) {
            int endCount;
            ParagraphView origPView = docView.getParagraphView(index);
            ParagraphView newPView = addedPViews.get(0);
            if (origPView.getStartOffset() == newPView.getStartOffset()) {
                while (origPView.getLength() == newPView.getLength()) {
                    this.transcribe(origPView, newPView);
                    if (++i0 >= commonCount) break;
                    origPView = docView.getParagraphView(index + i0);
                    newPView = addedPViews.get(i0);
                }
            }
            if ((endCount = commonCount - i0) > 0) {
                int i = 1;
                origPView = docView.getParagraphView(index + removeCount - i);
                newPView = addedPViews.get(addedCount - i);
                if (origPView.getEndOffset() == newPView.getEndOffset()) {
                    do {
                        if (origPView.getLength() != newPView.getLength()) {
                            --i;
                            break;
                        }
                        this.transcribe(origPView, newPView);
                        if (i >= commonCount - i0) break;
                        origPView = docView.getParagraphView(index + removeCount - ++i);
                        newPView = addedPViews.get(addedCount - i);
                    } while (true);
                    i1 = addedCount - i;
                    i1Orig = removeCount - i;
                }
            }
        }
        if (i0 != i1) {
            float defaultRowHeight = docView.op.getDefaultRowHeight();
            for (int i = i0; i < i1; ++i) {
                ParagraphView addedPView = addedPViews.get(i);
                addedPView.setHeight(defaultRowHeight);
            }
        }
        if (ViewHierarchyImpl.BUILD_LOG.isLoggable(Level.FINE)) {
            ViewHierarchyImpl.BUILD_LOG.fine("Non-Retained Views: " + (i0 != i1 ? new StringBuilder().append("<").append(i0).append(",").append(i1).append(">").toString() : "NONE") + " of " + addedCount + " new pViews\n");
        }
        ViewHierarchyChange change = docView.validChange();
        int changeEndOffset = firstReplaceValid && !this.docReplace.isChanged() ? ((ParagraphView)this.firstReplace.view).getEndOffset() : this.matchOffset;
        change.addChange(this.startCreationOffset, changeEndOffset);
        if (this.docReplace.isChanged()) {
            boolean changeY = false;
            double y0 = 0.0;
            double y1 = 0.0;
            if (removeCount != addedCount || i0 != i1) {
                changeY = true;
                y0 = docView.children != null ? docView.getY(index + i0) : 0.0;
                y1 = i0 != i1Orig ? docView.getY(index + i1Orig) : y0;
            }
            double[] startEndDeltaY = docView.replaceViews(this.docReplace.index, this.docReplace.getRemoveCount(), this.docReplace.addedViews());
            startY = startEndDeltaY[0];
            endY = startEndDeltaY[1];
            deltaY = startEndDeltaY[2];
            int allReplacesSize = this.allReplaces.size();
            if (this.forceCreateLocalViews && allReplacesSize > tlCache.capacity()) {
                tlCache.setCapacityOrDefault(allReplacesSize);
            }
            for (int pIndex = 0; pIndex < allReplacesSize; ++pIndex) {
                ViewReplace<ParagraphView, EditorView> replace = this.allReplaces.get(pIndex);
                if (!replace.isChanged()) continue;
                ParagraphView pView = (ParagraphView)replace.view;
                pView.replace(replace.index, replace.getRemoveCount(), replace.addedViews());
                pView.markChildrenValid();
                tlCache.activate(pView);
            }
            if (changeY) {
                boolean realChange = true;
                if (deltaY == 0.0 && i1 - i0 == 1) {
                    realChange = false;
                }
                if (realChange) {
                    change.addChangeY(y0, y1, deltaY);
                }
            }
        } else {
            int startIndex = firstReplaceValid ? this.docReplace.index - 1 : this.docReplace.index;
            startY = docView.getY(startIndex);
            endY = docView.getY(this.docReplace.index);
            deltaY = 0.0;
        }
        docView.updateEndOffset();
        Rectangle2D.Double docViewRect = docView.getAllocationCopy();
        if (docView.op.isAccurateSpan()) {
            int pIndex = this.docReplace.index;
            int endIndex = this.docReplace.addEndIndex();
            if (firstReplaceValid) {
                --pIndex;
            }
            while (pIndex < endIndex) {
                ParagraphView pView = docView.getParagraphView(pIndex);
                Shape pAlloc = docView.getChildAllocation(pIndex, docViewRect);
                if (pView.isChildrenNull()) {
                    LOG.info("Null children for accurate span at pIndex=" + pIndex + "\nviewBuilder:\n" + this);
                    break;
                }
                pView.checkLayoutUpdate(pIndex, ViewUtils.shapeAsRect(pAlloc));
                ++pIndex;
            }
        }
        docViewRect.y = startY;
        double endRepaintY = deltaY != 0.0 ? docViewRect.getMaxY() + Math.max(0.0, deltaY) : endY;
        docViewRect.height = endRepaintY - docViewRect.y;
        docView.op.extendToVisibleWidth(docViewRect);
        docView.op.notifyRepaint(docViewRect);
    }

    void finish() {
        if (this.factoryStates != null) {
            for (FactoryState factoryState : this.factoryStates) {
                if (factoryState == null) continue;
                factoryState.finish();
            }
        }
    }

    void updateLine(int offset) {
        while (offset > this.lineEndOffset) {
            ++this.lineIndex;
            Element line = this.lineRoot.getElement(this.lineIndex);
            this.lineEndOffset = line.getEndOffset();
        }
    }

    void notifyStaleCreation() {
        this.staleCreation = true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("-------- ViewBuilder dump -------\n").append("docLen=").append(((DocumentView)this.docReplace.view).getDocument().getLength()).append('\n').append("start/endCreationOffset:<").append(this.startCreationOffset).append(",").append(this.endCreationOffset).append(">\n").append("creationOffset=").append(this.creationOffset).append('\n').append("matchOffset=").append(this.matchOffset).append('\n').append("modOffset=").append(this.modOffset).append('\n').append("endModOffset=").append(this.endModOffset).append('\n').append("bmReuseOffset=").append(this.bmReuseOffset).append('\n').append("bmReusePView=").append(this.bmReusePView).append('\n').append("bmReuseLocalIndex=").append(this.bmReuseLocalIndex).append('\n').append("amReuseOffset=").append(this.amReuseOffset).append('\n').append("amReusePIndex=").append(this.amReusePIndex).append('\n').append("amReusePView=").append(this.amReusePView).append('\n').append("amReuseLocalIndex=").append(this.amReuseLocalIndex).append('\n').append("docView <").append(((DocumentView)this.docReplace.view).getStartOffset()).append(',').append(((DocumentView)this.docReplace.view).getEndOffset()).append(">\n").append("lineIndex=").append(this.lineIndex).append('\n').append("lineEndOffset=").append(this.lineEndOffset).append('\n').append("firstReplace=").append(this.firstReplace != null ? this.firstReplace : "<NULL>\n").append("docReplace=").append(this.docReplace).append("localReplace=").append(this.localReplace != null ? this.localReplace : "<NULL>\n").append("allocation=").append(this.allReplaces).append("\n-------- End of ViewBuilder dump -------\n");
        return sb.toString();
    }

    private static final class FactoryState {
        final EditorViewFactory factory;
        int nextViewStartOffset;

        FactoryState(EditorViewFactory factory) {
            this.factory = factory;
        }

        void init(ViewBuilder viewBuilder, int startOffset, int matchOffset, boolean createViews) {
            this.factory.setViewBuilder(viewBuilder);
            this.factory.restart(startOffset, matchOffset, createViews);
            this.updateNextViewStartOffset(startOffset);
        }

        void updateNextViewStartOffset(int offset) {
            this.nextViewStartOffset = this.factory.nextViewStartOffset(offset);
            if (this.nextViewStartOffset < offset) {
                throw new IllegalStateException("Editor view factory " + this.factory + " returned nextViewStartOffset=" + this.nextViewStartOffset + " < offset=" + offset);
            }
        }

        void finish() {
            this.factory.finishCreation();
            this.factory.setViewBuilder(null);
        }
    }

    static enum RebuildCause {
        FULL_REBUILD,
        REBUILD_PARAGRAPHS,
        INIT_PARAGRAPHS,
        MOD_UPDATE;
        

        private RebuildCause() {
        }
    }

}

