/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Segment;
import org.netbeans.lib.editor.util.CharSequenceUtilities;

final class CharContent
implements CharSequence {
    private static final char[] NEWLINE_CHAR_ARRAY = new char[]{'\n'};
    private char[] buffer = NEWLINE_CHAR_ARRAY;
    private int gapStart;
    private int gapLength;

    CharContent() {
    }

    @Override
    public char charAt(int index) {
        return this.buffer[this.rawOffset(index)];
    }

    @Override
    public int length() {
        return this.buffer.length - this.gapLength;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        CharSequenceUtilities.checkIndexesValid((int)start, (int)end, (int)this.length());
        return new SubSequence(start, end);
    }

    @Override
    public String toString() {
        return this.getString(0, this.length());
    }

    void insertText(int offset, String text) {
        int textLength = text.length();
        this.moveGapForInsert(offset, textLength);
        text.getChars(0, textLength, this.buffer, this.gapStart);
        this.gapStart += textLength;
        this.gapLength -= textLength;
    }

    void insertText(int offset, char[] text) {
        this.moveGapForInsert(offset, text.length);
        System.arraycopy(text, 0, this.buffer, this.gapStart, text.length);
        this.gapStart += text.length;
        this.gapLength -= text.length;
    }

    private void moveGapForInsert(int offset, int textLength) {
        if (textLength > this.gapLength) {
            this.reallocate((this.buffer.length >>> 3) + textLength);
        }
        if (offset != this.gapStart) {
            this.moveGap(offset);
        }
    }

    void removeText(int offset, int length) {
        int endOffset = offset + length;
        if (offset < 0) {
            throw new IndexOutOfBoundsException("offset=" + offset + " < 0");
        }
        if (endOffset > this.length()) {
            throw new IndexOutOfBoundsException("(offset=" + offset + " + length=" + length + ")=" + endOffset + " > length()=" + this.length());
        }
        if (offset >= this.gapStart) {
            if (offset > this.gapStart) {
                this.moveGap(offset);
            }
        } else if (endOffset <= this.gapStart) {
            if (endOffset < this.gapStart) {
                this.moveGap(endOffset);
            }
            this.gapStart -= length;
        } else {
            this.gapStart = offset;
        }
        this.gapLength += length;
    }

    String getString(int offset, int length) {
        String ret = offset + length <= this.gapStart ? new String(this.buffer, offset, length) : (offset >= this.gapStart ? new String(this.buffer, offset + this.gapLength, length) : new String(this.getChars(offset, length)));
        return ret;
    }

    void getChars(int offset, int length, Segment txt) {
        if (offset + length <= this.gapStart) {
            txt.array = this.buffer;
            txt.offset = offset;
        } else if (offset >= this.gapStart) {
            txt.array = this.buffer;
            txt.offset = offset + this.gapLength;
        } else {
            txt.array = this.getChars(offset, length);
            txt.offset = 0;
        }
        txt.count = length;
    }

    char[] getChars(int offset, int length) {
        char[] ret = new char[length];
        int belowGap = this.gapStart - offset;
        System.arraycopy(this.buffer, offset, ret, 0, belowGap);
        System.arraycopy(this.buffer, this.gapStart + this.gapLength, ret, belowGap, length - belowGap);
        return ret;
    }

    void compact() {
        if (this.gapLength > 0) {
            this.reallocate(0);
            this.gapStart = this.buffer.length;
        }
    }

    int gapStart() {
        return this.gapStart;
    }

    private int rawOffset(int index) {
        return index < this.gapStart ? index : index + this.gapLength;
    }

    private void moveGap(int index) {
        if (index <= this.gapStart) {
            int moveSize = this.gapStart - index;
            System.arraycopy(this.buffer, index, this.buffer, this.gapStart + this.gapLength - moveSize, moveSize);
        } else {
            int moveSize = index - this.gapStart;
            System.arraycopy(this.buffer, this.gapStart + this.gapLength, this.buffer, this.gapStart, moveSize);
        }
        this.gapStart = index;
    }

    private void reallocate(int newGapLength) {
        int gapEnd = this.gapStart + this.gapLength;
        int aboveGapLength = this.buffer.length - gapEnd;
        int newLength = this.gapStart + aboveGapLength + newGapLength;
        char[] newBuffer = new char[newLength];
        System.arraycopy(this.buffer, 0, newBuffer, 0, this.gapStart);
        System.arraycopy(this.buffer, gapEnd, newBuffer, newLength - aboveGapLength, aboveGapLength);
        this.gapLength = newGapLength;
        this.buffer = newBuffer;
    }

    String consistencyError() {
        String err = null;
        if (this.gapStart < 0) {
            err = "gapStart=" + this.gapStart + " < 0";
        } else if (this.gapLength < 0) {
            err = "gapLength=" + this.gapLength + " < 0";
        } else if (this.gapStart + this.gapLength > this.buffer.length) {
            err = "gapStart=" + this.gapStart + " + gapLength=" + this.gapLength + " > buffer.length=" + this.buffer.length;
        }
        return err;
    }

    static String gapToString(int arrayLength, int gapStart, int gapLength) {
        return "[0," + gapStart + ")" + gapLength + '[' + (gapStart + gapLength) + ',' + Integer.toString(arrayLength) + ']';
    }

    public String toStringDescription() {
        return CharContent.gapToString(this.buffer.length, this.gapStart, this.gapLength);
    }

    private final class SubSequence
    implements CharSequence {
        final int start;
        final int end;

        public SubSequence(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public int length() {
            return this.end - this.start;
        }

        @Override
        public char charAt(int index) {
            return CharContent.this.charAt(this.start + index);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            CharSequenceUtilities.checkIndexesValid((int)start, (int)end, (int)this.length());
            return new SubSequence(this.start + start, this.start + end);
        }

        @Override
        public String toString() {
            return CharContent.this.getString(this.start, this.end - this.start);
        }
    }

}

