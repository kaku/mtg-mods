/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.lib2;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.openide.util.Utilities;

public final class WeakReferenceStableList<E> {
    private static final int EMPTY_REF_COUNT_THRESHOLD = 3;
    private ImmutableList<E> list;
    private int emptyRefCount;

    public WeakReferenceStableList() {
        this.list = new ImmutableList<E>(this.newArray(0));
    }

    public synchronized List<E> getList() {
        return this.list;
    }

    public synchronized void add(E e) {
        assert (e != null);
        WeakRef<E>[] array = this.list.array;
        WeakRef<E>[] newArray = this.newArray(array.length + 1);
        System.arraycopy(array, 0, newArray, 0, array.length);
        newArray[array.length] = new WeakRef<E>(e, this);
        this.assignList(newArray);
    }

    public synchronized void addAll(Collection<E> c) {
        WeakRef<E>[] array = this.list.array;
        WeakRef<E>[] newArray = this.newArray(array.length + c.size());
        System.arraycopy(array, 0, newArray, 0, array.length);
        int i = array.length;
        for (E e : c) {
            assert (e != null);
            newArray[i++] = new WeakRef<E>(e, this);
        }
        this.assignList(newArray);
    }

    public synchronized boolean remove(E e) {
        assert (e != null);
        WeakRef<E>[] array = this.list.array;
        for (int i = 0; i < array.length; ++i) {
            if (!e.equals(array[i].get())) continue;
            this.removeImpl(i);
            return true;
        }
        return false;
    }

    private void removeImpl(int index) {
        WeakRef<E>[] array = this.list.array;
        WeakRef<E>[] newArray = this.newArray(array.length - 1);
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index + 1, newArray, index, array.length - index - 1);
        this.assignList(newArray);
    }

    private void assignList(WeakRef<E>[] array) {
        this.list = new ImmutableList<E>(array);
    }

    private WeakRef<E>[] newArray(int size) {
        WeakRef[] array = new WeakRef[size];
        return array;
    }

    synchronized void incrementEmptyRefCount() {
        if (++this.emptyRefCount > 3) {
            WeakRef<E>[] array = this.list.array;
            WeakRef<E>[] newArray = this.newArray(array.length - this.emptyRefCount);
            int j = 0;
            for (int i = 0; i < array.length; ++i) {
                WeakRef ref = array[i];
                if (ref == null || ref.get() == null) continue;
                newArray[j++] = ref;
            }
            this.assignList(newArray);
            this.emptyRefCount = 0;
        }
    }

    public synchronized String toString() {
        return this.list.toString();
    }

    private static final class WeakRef<E>
    extends WeakReference<E>
    implements Runnable {
        private final WeakReferenceStableList list;

        public WeakRef(E e, WeakReferenceStableList list) {
            super(e, Utilities.activeReferenceQueue());
            this.list = list;
        }

        @Override
        public void run() {
            this.list.incrementEmptyRefCount();
        }
    }

    private static final class ImmutableList<E>
    extends AbstractList<E> {
        WeakRef<E>[] array;

        public ImmutableList(WeakRef<E>[] array) {
            this.array = array;
        }

        @Override
        public E get(int index) {
            WeakRef<E> eRef = this.array[index];
            return eRef != null ? (E)eRef.get() : null;
        }

        @Override
        public int size() {
            return this.array.length;
        }

        @Override
        public Iterator<E> iterator() {
            return new Itr();
        }

        private final class Itr
        implements Iterator<E> {
            int index;
            E next;

            Itr() {
                this.updateNext();
            }

            @Override
            public boolean hasNext() {
                return this.next != null;
            }

            @Override
            public E next() {
                E e = this.next;
                if (e == null) {
                    throw new NoSuchElementException();
                }
                this.updateNext();
                return e;
            }

            private void updateNext() {
                int size = ImmutableList.this.size();
                while (this.index < size) {
                    this.next = ImmutableList.this.get(this.index++);
                    if (this.next == null) continue;
                    return;
                }
                this.next = null;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        }

    }

}

