/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.ArrayList;
import java.util.List;
import org.netbeans.modules.editor.lib2.view.EditorView;

final class ViewReplace<V extends EditorView, CV extends EditorView> {
    final V view;
    int index;
    private int removeCount;
    private List<CV> added;
    private final int childViewCount;

    ViewReplace(V view) {
        assert (view != null);
        this.view = view;
        this.childViewCount = view.getViewCount();
    }

    void add(CV childView) {
        if (this.added == null) {
            this.added = new ArrayList<CV>();
        }
        this.added.add(childView);
    }

    int addedSize() {
        return this.added != null ? this.added.size() : 0;
    }

    List<CV> added() {
        return this.added;
    }

    EditorView[] addedViews() {
        EditorView[] views;
        if (this.added != null) {
            views = new EditorView[this.added.size()];
            this.added.toArray(views);
        } else {
            views = null;
        }
        return views;
    }

    int getRemoveCount() {
        return this.removeCount;
    }

    void setRemoveCount(int removeCount) {
        if (this.index + removeCount > this.childViewCount) {
            throw new IllegalStateException("removeCount=" + removeCount + ", this:\n" + this);
        }
        this.removeCount = removeCount;
    }

    int removeEndIndex() {
        return this.index + this.getRemoveCount();
    }

    int addEndIndex() {
        return this.index + this.addedSize();
    }

    void removeTillEnd() {
        this.setRemoveCount(this.childViewCount - this.index);
    }

    boolean isRemovedTillEnd() {
        return this.index + this.removeCount == this.childViewCount;
    }

    boolean isChanged() {
        return this.added != null || this.getRemoveCount() > 0;
    }

    boolean isMakingViewEmpty() {
        return this.index == 0 && this.getRemoveCount() == this.childViewCount && this.addedSize() == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append(this.view.getDumpId());
        sb.append(": index=").append(this.index);
        sb.append(", remove=").append(this.getRemoveCount());
        EditorView[] addedViews = this.addedViews();
        sb.append(", added=");
        if (addedViews != null && addedViews.length > 0) {
            sb.append(addedViews.length);
        } else {
            sb.append("0");
        }
        if (!this.isChanged()) {
            sb.append(", NonChanged");
        }
        sb.append('\n');
        return sb.toString();
    }
}

