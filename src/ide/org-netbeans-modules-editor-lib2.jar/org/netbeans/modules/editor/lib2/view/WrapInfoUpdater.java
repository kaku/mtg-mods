/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Rectangle;
import java.awt.font.TextLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.LineWrapType;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.ViewPart;
import org.netbeans.modules.editor.lib2.view.WrapInfo;
import org.netbeans.modules.editor.lib2.view.WrapLine;

final class WrapInfoUpdater {
    private static final Logger LOG = Logger.getLogger(WrapInfoUpdater.class.getName());
    private static final long serialVersionUID = 0;
    private final WrapInfo wrapInfo;
    private final ParagraphView pView;
    private final DocumentView docView;
    private float availableWidth;
    private float maxWrapLineWidth;
    private boolean wrapTypeWords;
    private StringBuilder logMsgBuilder;
    private List<WrapLine> wrapLines;
    private WrapLine wrapLine;
    private boolean wrapLineNonEmpty;
    private float wrapLineX;
    private int childIndex;
    private double childX;
    private double nextChildX;

    WrapInfoUpdater(WrapInfo wrapInfo, ParagraphView paragraphView) {
        this.wrapInfo = wrapInfo;
        this.pView = paragraphView;
        this.docView = paragraphView.getDocumentView();
        assert (this.docView != null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void initWrapInfo() {
        this.wrapLines = new ArrayList<WrapLine>(2);
        this.wrapLine = new WrapLine();
        this.wrapTypeWords = this.docView.op.getLineWrapType() == LineWrapType.WORD_BOUND;
        float visibleWidth = this.docView.op.getVisibleRect().width;
        TextLayout lineContinuationTextLayout = this.docView.op.getLineContinuationCharTextLayout();
        this.availableWidth = Math.max(visibleWidth - lineContinuationTextLayout.getAdvance(), this.docView.op.getDefaultCharWidth() * 4.0f);
        StringBuilder stringBuilder = this.logMsgBuilder = LOG.isLoggable(Level.FINE) ? new StringBuilder(100) : null;
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("Building wrapLines: availWidth=").append(this.availableWidth);
            this.logMsgBuilder.append(", lineContCharWidth=").append(lineContinuationTextLayout.getAdvance());
            this.logMsgBuilder.append("\n");
        }
        try {
            ViewPart viewOrPart = this.initChildVars(0, 0.0);
            do {
                if (this.wrapLineX + viewOrPart.width <= this.availableWidth) {
                    this.addViewOrPart(viewOrPart);
                    viewOrPart = this.fetchNextView();
                    continue;
                }
                boolean regularBreak = false;
                if (this.wrapTypeWords) {
                    int wrapLineStartOffset;
                    int viewOrPartStartOffset = viewOrPart.view.getStartOffset();
                    WordInfo wordInfo = this.getWordInfo(viewOrPartStartOffset, wrapLineStartOffset = this.wrapLineNonEmpty ? this.wrapLine.startView(this.pView).getStartOffset() : viewOrPartStartOffset);
                    if (wordInfo != null) {
                        ViewSplit split = this.breakView(viewOrPart, false);
                        if (split != null) {
                            this.addPart(split.startPart);
                            this.finishWrapLine();
                            viewOrPart = split.endPart;
                        } else {
                            if (wrapLineStartOffset == wordInfo.wordStartOffset()) {
                                int wordEndOffset = wordInfo.wordEndOffset();
                                while (viewOrPart != null) {
                                    int endOffset = viewOrPart.view.getEndOffset();
                                    if (wordEndOffset >= endOffset) {
                                        this.addViewOrPart(viewOrPart);
                                        viewOrPart = this.fetchNextView();
                                        continue;
                                    }
                                    ViewSplit wordEndSplit = this.createFragment(viewOrPart, wordEndOffset, true);
                                    if (wordEndSplit != null) {
                                        this.addPart(wordEndSplit.startPart);
                                        viewOrPart = wordEndSplit.endPart;
                                    } else {
                                        this.addViewOrPart(viewOrPart);
                                        viewOrPart = this.fetchNextView();
                                    }
                                    break;
                                }
                            } else {
                                ViewPart aboveWordStartPart = this.removeViewsAndSplitAtWordStart(wordInfo.wordStartOffset());
                                viewOrPart = aboveWordStartPart != null ? aboveWordStartPart : this.fetchNextView();
                            }
                            this.finishWrapLine();
                        }
                    } else {
                        regularBreak = true;
                    }
                } else {
                    regularBreak = true;
                }
                if (!regularBreak) continue;
                ViewSplit split = this.breakView(viewOrPart, false);
                if (split != null) {
                    this.addPart(split.startPart);
                    viewOrPart = split.endPart;
                    this.finishWrapLine();
                    continue;
                }
                if (!this.wrapLineNonEmpty) {
                    this.addViewOrPart(viewOrPart);
                    viewOrPart = this.fetchNextView();
                }
                this.finishWrapLine();
            } while (this.childIndex < this.pView.getViewCount());
            this.finishWrapLine();
        }
        finally {
            if (this.logMsgBuilder != null) {
                this.logMsgBuilder.append('\n');
                LOG.fine(this.logMsgBuilder.toString());
            }
        }
        this.wrapInfo.addAll(this.wrapLines);
        this.wrapInfo.checkIntegrity(this.pView);
        if (this.logMsgBuilder != null) {
            LOG.fine("Resulting wrapInfo:" + this.wrapInfo.toString(this.pView) + "\n");
        }
        this.wrapInfo.setWidth(this.maxWrapLineWidth);
    }

    private void finishWrapLine() {
        if (this.wrapLineNonEmpty) {
            if (this.wrapLineX > this.maxWrapLineWidth) {
                this.maxWrapLineWidth = this.wrapLineX;
            }
            this.wrapLines.add(this.wrapLine);
            this.wrapLine = new WrapLine();
            this.wrapLineNonEmpty = false;
            this.wrapLineX = 0.0f;
        }
    }

    private ViewPart initChildVars(int childIndex, double childX) {
        this.childIndex = childIndex;
        this.childX = childX;
        return this.assignChild();
    }

    private ViewPart assignChild() {
        this.nextChildX = this.pView.children.startVisualOffset(this.childIndex + 1);
        EditorView childView = this.pView.getEditorView(this.childIndex);
        float childWidth = (float)(this.nextChildX - this.childX);
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("child[").append(this.childIndex).append("]:").append(childView.getDumpId());
            int startOffset = childView.getStartOffset();
            this.logMsgBuilder.append(" <").append(startOffset).append(",").append(startOffset + childView.getLength());
            this.logMsgBuilder.append("> W=").append(childWidth);
            this.logMsgBuilder.append(":\n");
        }
        return new ViewPart(childView, childWidth);
    }

    private ViewPart fetchNextView() {
        ++this.childIndex;
        if (this.childIndex < this.pView.getViewCount()) {
            this.childX = this.nextChildX;
            return this.assignChild();
        }
        return null;
    }

    private void addView(ViewPart part) {
        assert (!part.isPart());
        assert (this.wrapLine.endPart == null);
        if (this.wrapLineNonEmpty) {
            assert (this.wrapLine.endViewIndex == this.childIndex);
            ++this.wrapLine.endViewIndex;
        } else {
            this.wrapLineNonEmpty = true;
            this.wrapLine.firstViewIndex = this.childIndex;
            this.wrapLine.endViewIndex = this.childIndex + 1;
        }
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("  child added");
        }
        this.wrapLineX += part.width;
        if (this.logMsgBuilder != null) {
            this.logWrapLineAndX();
        }
    }

    private void addStartPart(ViewPart part) {
        assert (part.isPart());
        assert (this.wrapLine.startPart == null);
        assert (!this.wrapLineNonEmpty);
        this.wrapLineNonEmpty = true;
        this.wrapLine.firstViewIndex = this.childIndex + 1;
        this.wrapLine.endViewIndex = this.childIndex + 1;
        this.wrapLine.startPart = part;
        this.wrapLineX += part.width;
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("  startPart set");
            this.logWrapLineAndX();
        }
    }

    private void addEndPart(ViewPart part) {
        assert (part.isPart());
        assert (this.wrapLine.endPart == null);
        if (!this.wrapLineNonEmpty) {
            this.wrapLine.firstViewIndex = this.childIndex;
            this.wrapLine.endViewIndex = this.childIndex;
            this.wrapLineNonEmpty = true;
        }
        this.wrapLine.endPart = part;
        this.wrapLineX += part.width;
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("  endPart set");
            this.logWrapLineAndX();
        }
    }

    private void addPart(ViewPart part) {
        if (part.isFirstPart()) {
            this.addEndPart(part);
        } else {
            this.addStartPart(part);
        }
    }

    private void addViewOrPart(ViewPart viewOrPart) {
        if (viewOrPart.isPart()) {
            this.addPart(viewOrPart);
        } else {
            this.addView(viewOrPart);
        }
    }

    private ViewPart removeViewsAndSplitAtWordStart(int wordStartOffset) {
        ViewSplit split;
        assert (this.wrapLineNonEmpty);
        assert (this.wrapLine.endPart == null);
        if (this.wrapLine.hasFullViews()) {
            boolean isFirstView = false;
            do {
                --this.wrapLine.endViewIndex;
                int lastViewIndex = this.wrapLine.endViewIndex;
                isFirstView = lastViewIndex == this.wrapLine.firstViewIndex;
                EditorView view = this.pView.getEditorView(lastViewIndex);
                int viewStartOffset = view.getStartOffset();
                if (wordStartOffset >= viewStartOffset + view.getLength()) continue;
                double startChildX = this.pView.children.startVisualOffset(lastViewIndex);
                double childWidth = this.childX - startChildX;
                this.wrapLineX = (float)((double)this.wrapLineX - childWidth);
                if (isFirstView && this.wrapLine.startPart == null) {
                    this.wrapLineNonEmpty = false;
                }
                ViewPart viewPart = this.initChildVars(lastViewIndex, startChildX);
                if (wordStartOffset > viewStartOffset) {
                    ViewSplit wordStartSplit = this.createFragment(viewPart, wordStartOffset, true);
                    if (wordStartSplit != null) {
                        this.addPart(wordStartSplit.startPart);
                        return wordStartSplit.endPart;
                    }
                    this.addView(viewPart);
                    return null;
                }
                if (wordStartOffset != viewStartOffset) continue;
                return null;
            } while (!isFirstView);
        }
        assert (this.wrapLine.startPart != null);
        if (this.wrapLine.startPart.view.getEndOffset() == wordStartOffset) {
            return null;
        }
        ViewPart startPart = this.wrapLine.startPart;
        this.wrapLine.startPart = null;
        this.wrapLineX = 0.0f;
        this.wrapLineNonEmpty = false;
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("  Removed startPart.");
        }
        if ((split = this.createFragment(startPart, wordStartOffset, true)) != null) {
            this.addStartPart(split.startPart);
            return split.endPart;
        }
        this.addStartPart(startPart);
        return null;
    }

    private ViewSplit breakView(ViewPart part, boolean allowWider) {
        EditorView startView;
        EditorView view = part.view;
        int viewStartOffset = view.getStartOffset();
        float breakViewX = (float)(this.childX + (double)part.xShift);
        if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("  breakView<").append(viewStartOffset).append(",").append(viewStartOffset + view.getLength()).append("> x=").append(breakViewX).append(" W=").append(this.availableWidth - this.wrapLineX).append(" => ");
        }
        if ((startView = (EditorView)view.breakView(0, viewStartOffset, breakViewX, this.availableWidth - this.wrapLineX)) != null && startView != view) {
            int viewLength;
            assert (startView.getStartOffset() == viewStartOffset);
            int startViewLength = startView.getLength();
            if (startViewLength != (viewLength = view.getLength())) {
                if (this.logMsgBuilder != null) {
                    this.logMsgBuilder.append("startPart<").append(startView.getStartOffset()).append(",").append(startView.getEndOffset()).append(">");
                }
                float startViewWidth = startView.getPreferredSpan(0);
                if (allowWider || startViewWidth <= this.availableWidth - this.wrapLineX) {
                    EditorView endView = (EditorView)view.createFragment(viewStartOffset + startViewLength, viewStartOffset + viewLength);
                    if (endView != null && endView != view && endView.getLength() == viewLength - startViewLength) {
                        if (this.logMsgBuilder != null) {
                            this.logMsgBuilder.append("\n");
                        }
                    } else {
                        if (this.logMsgBuilder != null) {
                            this.logMsgBuilder.append("createFragment <" + (viewStartOffset + startViewLength) + "," + (viewStartOffset + viewLength) + "> not allowed by view\n");
                        }
                        startView = null;
                        endView = null;
                    }
                    int index = part.isPart() ? part.index : 0;
                    return new ViewSplit(new ViewPart(startView, startViewWidth, part.xShift, index), new ViewPart(endView, endView.getPreferredSpan(0), part.xShift + startViewWidth, index + 1));
                }
                if (this.logMsgBuilder != null) {
                    this.logMsgBuilder.append("Fragment too wide(pW=" + startViewWidth + ">aW=" + this.availableWidth + "-x=" + this.wrapLineX + ")\n");
                }
            } else if (this.logMsgBuilder != null) {
                this.logMsgBuilder.append("startPart same length as view\n");
            }
        } else if (this.logMsgBuilder != null) {
            this.logMsgBuilder.append("Break not allowed by view\n");
        }
        return null;
    }

    private ViewSplit createFragment(ViewPart part, int breakOffset, boolean allowWider) {
        EditorView view = part.isPart() ? this.pView.getEditorView(this.childIndex) : part.view;
        int fragStartOffset = part.view.getStartOffset();
        int viewEndOffset = view.getEndOffset();
        assert (fragStartOffset < breakOffset);
        assert (breakOffset < viewEndOffset);
        EditorView startView = (EditorView)view.createFragment(fragStartOffset, breakOffset);
        assert (startView != null);
        if (startView != view) {
            if (this.logMsgBuilder != null) {
                this.logMsgBuilder.append(" createFragment<").append(startView.getStartOffset()).append(",").append(startView.getEndOffset()).append(">");
            }
            float startViewWidth = startView.getPreferredSpan(0);
            if (allowWider || startViewWidth <= this.availableWidth - this.wrapLineX) {
                EditorView endView = (EditorView)view.createFragment(breakOffset, viewEndOffset);
                assert (endView != null);
                if (endView != view) {
                    int index = part.isPart() ? part.index : 0;
                    return new ViewSplit(new ViewPart(startView, startViewWidth, part.xShift, index), new ViewPart(endView, endView.getPreferredSpan(0), part.xShift + startViewWidth, index + 1));
                }
            }
        }
        return null;
    }

    private WordInfo getWordInfo(int boundaryOffset, int startLimitOffset) {
        boolean prevCharIsWordPart;
        boolean nextCharIsWordPart;
        int docTextLength;
        CharSequence docText = DocumentUtilities.getText((Document)this.docView.getDocument());
        boolean bl = prevCharIsWordPart = boundaryOffset > startLimitOffset && Character.isLetterOrDigit(docText.charAt(boundaryOffset - 1));
        if (prevCharIsWordPart && boundaryOffset < (docTextLength = docText.length()) && (nextCharIsWordPart = Character.isLetterOrDigit(docText.charAt(boundaryOffset)))) {
            int wordEndOffset;
            for (wordEndOffset = boundaryOffset + 1; wordEndOffset < docTextLength && Character.isLetterOrDigit(docText.charAt(wordEndOffset)); ++wordEndOffset) {
            }
            return new WordInfo(docText, boundaryOffset, startLimitOffset, wordEndOffset);
        }
        return null;
    }

    private void logWrapLineAndX() {
        this.logMsgBuilder.append(" to WL[").append(this.wrapLines.size()).append("] endX=").append(this.wrapLineX).append('\n');
    }

    private static final class ViewSplit {
        final ViewPart startPart;
        final ViewPart endPart;

        ViewSplit(ViewPart startPart, ViewPart endPart) {
            this.startPart = startPart;
            this.endPart = endPart;
        }
    }

    private static final class WordInfo {
        private CharSequence docText;
        private int boundaryOffset;
        private int startOffset;
        private int wordEndOffset;
        private int wordStartOffset = -1;

        WordInfo(CharSequence docText, int boundaryOffset, int startOffset, int wordEndOffset) {
            this.docText = docText;
            this.boundaryOffset = boundaryOffset;
            this.startOffset = startOffset;
            this.wordEndOffset = wordEndOffset;
        }

        int wordEndOffset() {
            return this.wordEndOffset;
        }

        int wordStartOffset() {
            if (this.wordStartOffset == -1) {
                this.wordStartOffset = this.boundaryOffset - 2;
                while (this.wordStartOffset >= this.startOffset && Character.isLetterOrDigit(this.docText.charAt(this.wordStartOffset))) {
                    --this.wordStartOffset;
                }
                ++this.wordStartOffset;
            }
            return this.wordStartOffset;
        }
    }

}

