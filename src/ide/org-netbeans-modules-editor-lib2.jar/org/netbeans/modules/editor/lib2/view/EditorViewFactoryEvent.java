/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.EventObject;
import java.util.List;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange;

public final class EditorViewFactoryEvent
extends EventObject {
    private final List<EditorViewFactoryChange> changes;

    EditorViewFactoryEvent(EditorViewFactory factory, List<EditorViewFactoryChange> changes) {
        super(factory);
        this.changes = changes;
    }

    public List<EditorViewFactoryChange> getChanges() {
        return this.changes;
    }
}

