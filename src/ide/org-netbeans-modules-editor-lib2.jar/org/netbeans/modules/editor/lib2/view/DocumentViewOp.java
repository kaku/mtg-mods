/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.RepaintManager;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.StyleConstants;
import javax.swing.text.TabExpander;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DebugRepaintManager;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.EditorTabExpander;
import org.netbeans.modules.editor.lib2.view.FontInfo;
import org.netbeans.modules.editor.lib2.view.LineWrapType;
import org.netbeans.modules.editor.lib2.view.TextLayoutCache;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewStats;
import org.netbeans.modules.editor.lib2.view.ViewUpdates;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public final class DocumentViewOp
implements PropertyChangeListener,
ChangeListener,
MouseWheelListener {
    private static final Logger LOG;
    static final Map<Object, Object> extraRenderingHints;
    static final Boolean doubleBuffered;
    static final char PRINTING_SPACE = '\u00b7';
    static final char PRINTING_TAB = '\u2192';
    static final char PRINTING_TAB_ALTERNATE = '\u00bb';
    static final char PRINTING_NEWLINE = '\u00b6';
    static final char LINE_CONTINUATION = '\u21a9';
    static final char LINE_CONTINUATION_ALTERNATE = '\u2190';
    private static final int ALLOCATION_WIDTH_CHANGE = 1;
    private static final int ALLOCATION_HEIGHT_CHANGE = 2;
    private static final int WIDTH_CHANGE = 4;
    private static final int HEIGHT_CHANGE = 8;
    private static final int CHILDREN_VALID = 16;
    private static final int INCOMING_MODIFICATION = 32;
    private static final int FIRING_CHANGE = 64;
    private static final int ACCURATE_SPAN = 128;
    private static final int AVAILABLE_WIDTH_VALID = 256;
    private static final int NON_PRINTABLE_CHARACTERS_VISIBLE = 512;
    private static final int UPDATE_VISIBLE_DIMENSION_PENDING = 1024;
    private final DocumentView docView;
    private int statusBits;
    ViewUpdates viewUpdates;
    private final TextLayoutCache textLayoutCache;
    private float newAllocationWidth;
    private float newAllocationHeight;
    private Rectangle visibleRect = new Rectangle(0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
    private float availableWidth;
    private float renderWrapWidth;
    private double repaintX0;
    private double repaintY0;
    private double repaintX1;
    private double repaintY1;
    private FontRenderContext fontRenderContext;
    private AttributeSet defaultColoring;
    private int defaultRowHeightInt;
    private int defaultAscentInt;
    private float defaultCharWidth;
    private Color textLimitLineColor;
    private int textLimitLineX;
    private LineWrapType lineWrapType;
    private TextLayout newlineTextLayout;
    private TextLayout tabTextLayout;
    private TextLayout singleCharTabTextLayout;
    private TextLayout lineContinuationTextLayout;
    private LookupListener lookupListener;
    private JViewport listeningOnViewport;
    private Preferences prefs;
    private PreferenceChangeListener prefsListener;
    Map<?, ?> renderingHints;
    private int lengthyAtomicEdit;
    ViewHierarchyImpl viewHierarchyImpl;
    private Map<Font, FontInfo> fontInfos = new HashMap<Font, FontInfo>(4);
    private Font defaultFont;
    private boolean fontRenderContextFromPaint;
    private float rowHeightCorrection = 1.0f;
    private MouseWheelListener origMouseWheelListener;
    private int textZoom;
    private float extraVirtualHeight;
    boolean asTextField;
    private boolean guideLinesEnable;
    private int indentLevelSize;
    private int tabSize;
    private Color guideLinesColor;
    private int[] guideLinesCache = new int[]{-1, -1, -1};

    public DocumentViewOp(DocumentView docView) {
        this.docView = docView;
        this.textLayoutCache = new TextLayoutCache();
    }

    public ViewHierarchyImpl viewHierarchyImpl() {
        return this.viewHierarchyImpl;
    }

    public boolean isChildrenValid() {
        return this.isAnyStatusBit(16);
    }

    public void viewsRebuildOrMarkInvalid() {
        this.docView.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                if (DocumentViewOp.this.viewUpdates != null) {
                    DocumentViewOp.this.viewUpdates.viewsRebuildOrMarkInvalidNeedsLock();
                }
            }
        });
    }

    void notifyWidthChange() {
        this.setStatusBits(4);
        if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "DV-WIDTH changed\n");
        }
    }

    boolean isWidthChange() {
        return this.isAnyStatusBit(4);
    }

    private void resetWidthChange() {
        this.clearStatusBits(4);
    }

    private boolean checkRealWidthChange() {
        if (this.isWidthChange()) {
            this.resetWidthChange();
            return this.docView.updatePreferredWidth();
        }
        return false;
    }

    void notifyHeightChange() {
        this.setStatusBits(8);
        if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "DV-HEIGHT changed\n");
        }
    }

    boolean isHeightChange() {
        return this.isAnyStatusBit(8);
    }

    private void resetHeightChange() {
        this.clearStatusBits(8);
    }

    private boolean checkRealHeightChange() {
        if (this.isHeightChange()) {
            this.resetHeightChange();
            return this.docView.updatePreferredHeight();
        }
        return false;
    }

    void markAllocationWidthChange(float newWidth) {
        this.setStatusBits(1);
        this.newAllocationWidth = newWidth;
    }

    void markAllocationHeightChange(float newHeight) {
        this.setStatusBits(2);
        this.newAllocationHeight = newHeight;
    }

    private void setStatusBits(int bits) {
        this.statusBits |= bits;
    }

    private void clearStatusBits(int bits) {
        this.statusBits &= ~ bits;
    }

    private void updateStatusBits(int bits, boolean value) {
        if (value) {
            this.setStatusBits(bits);
        } else {
            this.clearStatusBits(bits);
        }
    }

    private boolean isAnyStatusBit(int bits) {
        return (this.statusBits & bits) != 0;
    }

    void lockCheck() {
        if (this.isAnyStatusBit(2)) {
            this.clearStatusBits(2);
            this.docView.setAllocationHeight(this.newAllocationHeight);
        }
        if (this.isAnyStatusBit(1)) {
            this.docView.setAllocationWidth(this.newAllocationWidth);
            if (!this.isAnyStatusBit(1024)) {
                this.setStatusBits(1024);
                if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
                    ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "DVOp.logCheck: invokeLater(updateVisibleDimension())\n");
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        DocumentViewOp.this.updateVisibleDimension(true);
                    }
                });
            }
        }
    }

    void unlockCheck() {
        this.checkRealSpanChange();
        this.checkRepaint();
        if (this.isAnyStatusBit(12)) {
            LOG.log(Level.INFO, "DocumentView invalid state upon unlock.", new Exception());
        }
    }

    void checkRealSpanChange() {
        boolean widthChange = this.checkRealWidthChange();
        boolean heightChange = this.checkRealHeightChange();
        if (widthChange || heightChange) {
            if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
                String msg = "TC-preferenceChanged(" + (widthChange ? "W" : "-") + "x" + (heightChange ? "H" : "-") + ")\n";
                ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, msg);
            }
            this.docView.superPreferenceChanged(widthChange, heightChange);
        }
    }

    public void notifyRepaint(double x0, double y0, double x1, double y1) {
        if (this.repaintX1 == 0.0) {
            this.repaintX0 = x0;
            this.repaintY0 = y0;
            this.repaintX1 = x1;
            this.repaintY1 = y1;
        } else {
            this.repaintX0 = Math.min(this.repaintX0, x0);
            this.repaintX1 = Math.max(this.repaintX1, x1);
            this.repaintY0 = Math.min(this.repaintY0, y0);
            this.repaintY1 = Math.max(this.repaintY1, y1);
        }
        if (ViewHierarchyImpl.REPAINT_LOG.isLoggable(Level.FINE)) {
            String msg = "NOTIFY-REPAINT XYWH[" + x0 + ";" + y0 + ";" + (x1 - x0) + ";" + (y1 - y0) + "] => [" + this.repaintX0 + ";" + this.repaintY0 + ";" + (this.repaintX1 - this.repaintX0) + ";" + (this.repaintY1 - this.repaintY0) + "]\n";
            ViewUtils.log(ViewHierarchyImpl.REPAINT_LOG, msg);
        }
    }

    void notifyRepaint(Rectangle2D repaintRect) {
        this.notifyRepaint(repaintRect.getX(), repaintRect.getY(), repaintRect.getMaxX(), repaintRect.getMaxY());
    }

    final void checkRepaint() {
        if (this.repaintX1 != 0.0) {
            final int x0 = (int)this.repaintX0;
            final int x1 = (int)Math.ceil(this.repaintX1);
            final int y0 = (int)this.repaintY0;
            final int y1 = (int)Math.ceil(this.repaintY1);
            this.resetRepaintRegion();
            ViewUtils.runInEDT(new Runnable(){

                @Override
                public void run() {
                    JTextComponent textComponent = DocumentViewOp.this.docView.getTextComponent();
                    if (textComponent != null) {
                        if (ViewHierarchyImpl.REPAINT_LOG.isLoggable(Level.FINE)) {
                            ViewHierarchyImpl.REPAINT_LOG.finer("REPAINT [x0,y0][x1,y1]: [" + x0 + "," + y0 + "][" + x1 + "," + y1 + "]\n");
                        }
                        textComponent.repaint(x0, y0, x1 - x0, y1 - y0);
                    }
                }
            });
        }
    }

    private void resetRepaintRegion() {
        this.repaintX1 = 0.0;
    }

    void extendToVisibleWidth(Rectangle2D.Double r) {
        r.width = this.getVisibleRect().getMaxX();
    }

    void parentViewSet() {
        JTextComponent textComponent = this.docView.getTextComponent();
        assert (textComponent != null);
        this.updateStatusBits(128, Boolean.TRUE.equals(textComponent.getClientProperty("document-view-accurate-span")));
        this.updateTextZoom(textComponent);
        this.viewUpdates = new ViewUpdates(this.docView);
        this.viewUpdates.initFactories();
        this.asTextField = Boolean.TRUE.equals(textComponent.getClientProperty("AsTextField"));
        textComponent.addPropertyChangeListener(this);
        this.viewHierarchyImpl = ViewHierarchyImpl.get(textComponent);
        this.viewHierarchyImpl.setDocumentView(this.docView);
        if (doubleBuffered != null) {
            textComponent.setDoubleBuffered(doubleBuffered);
            RepaintManager rm = RepaintManager.currentManager(textComponent);
            boolean doubleBufferingOrig = rm.isDoubleBufferingEnabled();
            ViewHierarchyImpl.SETTINGS_LOG.fine("RepaintManager.setDoubleBuffered() from " + doubleBufferingOrig + " to " + doubleBuffered + "\n");
            rm.setDoubleBufferingEnabled(doubleBuffered);
        }
        if (ViewHierarchyImpl.REPAINT_LOG.isLoggable(Level.FINER)) {
            DebugRepaintManager.register(textComponent);
        }
    }

    void parentCleared() {
        JTextComponent textComponent = this.docView.getTextComponent();
        this.viewHierarchyImpl.setDocumentView(null);
        this.uninstallFromViewport();
        textComponent.removePropertyChangeListener(this);
        this.viewUpdates = null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void checkViewsInited() {
        if (!this.isChildrenValid() && this.docView.getTextComponent() != null) {
            this.checkSettingsInfo();
            if (this.docView.getTextComponent() != null) {
                if (this.checkFontRenderContext()) {
                    this.updateCharMetrics();
                }
                this.docView.updateStartEndOffsets();
                ((EditorTabExpander)this.docView.getTabExpander()).updateTabSize();
                if (this.isBuildable()) {
                    LOG.fine("viewUpdates.reinitViews()\n");
                    this.setStatusBits(16);
                    boolean success = false;
                    try {
                        this.viewUpdates.reinitAllViews();
                        success = true;
                    }
                    finally {
                        if (!success) {
                            this.markChildrenInvalid();
                        }
                    }
                }
            }
        }
    }

    void initParagraphs(int startIndex, int endIndex) {
        this.viewUpdates.initParagraphs(startIndex, endIndex);
    }

    private boolean checkFontRenderContext() {
        if (this.fontRenderContext == null) {
            Graphics graphics;
            JTextComponent textComponent = this.docView.getTextComponent();
            Graphics graphics2 = graphics = textComponent != null ? textComponent.getGraphics() : null;
            if (graphics instanceof Graphics2D) {
                this.updateFontRenderContext((Graphics2D)graphics, false);
                return this.fontRenderContext != null;
            }
        }
        return false;
    }

    void updateFontRenderContext(Graphics2D g, boolean paint) {
        if (g != null) {
            if (!paint && ViewHierarchyImpl.SETTINGS_LOG.isLoggable(Level.FINE)) {
                ViewHierarchyImpl.SETTINGS_LOG.fine("DocumentView.updateFontColorSettings() Antialiasing Rendering Hints:\n    Graphics: " + g.getRenderingHints() + "\n    Desktop Hints: " + this.renderingHints + "\n    Extra Hints: " + extraRenderingHints + '\n');
            }
            if (this.renderingHints != null) {
                g.addRenderingHints(this.renderingHints);
            }
            if (extraRenderingHints.size() > 0) {
                g.addRenderingHints(extraRenderingHints);
            }
            if (paint) {
                if (!this.fontRenderContextFromPaint) {
                    this.fontRenderContextFromPaint = true;
                    this.fontRenderContext = g.getFontRenderContext();
                    this.releaseChildrenNeedsLock();
                }
            } else {
                this.fontRenderContext = g.getFontRenderContext();
            }
        }
    }

    void releaseChildrenNeedsLock() {
        this.markChildrenInvalid();
    }

    private void markChildrenInvalid() {
        this.clearStatusBits(16);
    }

    public void releaseChildren(final boolean updateFonts) {
        this.docView.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                DocumentViewOp.this.releaseChildrenNeedsLock();
                if (updateFonts) {
                    DocumentViewOp.this.updateCharMetrics();
                }
            }
        });
    }

    boolean isAccurateSpan() {
        return this.isAnyStatusBit(128);
    }

    void updateVisibleDimension(final boolean clearAllocationWidthChange) {
        float extraHeight;
        Rectangle newVisibleRect;
        JTextComponent textComponent = this.docView.getTextComponent();
        if (textComponent == null) {
            return;
        }
        Container parent = textComponent.getParent();
        if (parent instanceof JViewport) {
            JViewport viewport = (JViewport)parent;
            if (this.listeningOnViewport != viewport) {
                this.uninstallFromViewport();
                this.listeningOnViewport = viewport;
                if (this.listeningOnViewport != null) {
                    this.listeningOnViewport.addChangeListener(this);
                    Container scrollPane = this.listeningOnViewport.getParent();
                    MouseWheelListener[] mwls = (MouseWheelListener[])scrollPane.getListeners(MouseWheelListener.class);
                    if (mwls.length == 1) {
                        this.origMouseWheelListener = mwls[0];
                        scrollPane.removeMouseWheelListener(this.origMouseWheelListener);
                    }
                    this.listeningOnViewport.getParent().addMouseWheelListener(this);
                }
            }
            newVisibleRect = viewport.getViewRect();
        } else {
            this.uninstallFromViewport();
            Dimension size = textComponent.getSize();
            newVisibleRect = new Rectangle(0, 0, size.width, size.height);
        }
        if (!DocumentView.DISABLE_END_VIRTUAL_SPACE && this.listeningOnViewport != null && !this.asTextField) {
            JScrollBar hScrollBar;
            float eHeight = this.listeningOnViewport.getExtentSize().height;
            parent = this.listeningOnViewport.getParent();
            if (parent instanceof JScrollPane && (hScrollBar = ((JScrollPane)parent).getHorizontalScrollBar()) != null && hScrollBar.isVisible()) {
                eHeight += (float)hScrollBar.getHeight();
            }
            extraHeight = eHeight / 3.0f;
        } else {
            extraHeight = 0.0f;
        }
        this.docView.runTransaction(new Runnable(){

            @Override
            public void run() {
                boolean heightDiffers;
                boolean widthDiffers = newVisibleRect.width != DocumentViewOp.access$200((DocumentViewOp)DocumentViewOp.this).width;
                boolean bl = heightDiffers = newVisibleRect.height != DocumentViewOp.access$200((DocumentViewOp)DocumentViewOp.this).height;
                if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
                    ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "DVOp.updateVisibleDimension: widthDiffers=" + widthDiffers + ", newVisibleRect=" + ViewUtils.toString(newVisibleRect) + ", extraHeight=" + extraHeight + "\n");
                }
                if (clearAllocationWidthChange) {
                    DocumentViewOp.this.clearStatusBits(1025);
                }
                DocumentViewOp.this.extraVirtualHeight = extraHeight;
                DocumentViewOp.this.visibleRect = newVisibleRect;
                if (widthDiffers) {
                    DocumentViewOp.this.clearStatusBits(256);
                    DocumentViewOp.this.docView.markChildrenLayoutInvalid();
                }
                if (DocumentViewOp.this.asTextField && heightDiffers) {
                    DocumentViewOp.this.docView.updateBaseY();
                }
            }
        });
    }

    float getExtraVirtualHeight() {
        return this.extraVirtualHeight;
    }

    private void uninstallFromViewport() {
        if (this.listeningOnViewport != null) {
            this.listeningOnViewport.getParent().removeMouseWheelListener(this);
            if (this.origMouseWheelListener != null) {
                this.listeningOnViewport.getParent().addMouseWheelListener(this.origMouseWheelListener);
            }
            this.listeningOnViewport.removeChangeListener(this);
            this.listeningOnViewport = null;
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JTextComponent textComponent = this.docView.getTextComponent();
        if (textComponent != null) {
            this.updateVisibleDimension(false);
        }
    }

    private void checkSettingsInfo() {
        String mimeType;
        JTextComponent textComponent = this.docView.getTextComponent();
        if (textComponent == null) {
            return;
        }
        if (this.prefs == null) {
            mimeType = DocumentUtilities.getMimeType((JTextComponent)textComponent);
            this.prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
            this.prefsListener = new PreferenceChangeListener(){

                @Override
                public void preferenceChange(PreferenceChangeEvent evt) {
                    DocumentViewOp.this.updatePreferencesSettings(true);
                }
            };
            this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs));
            this.updatePreferencesSettings(false);
        }
        if (this.lookupListener == null) {
            this.lookupListener = new LookupListener(){

                public void resultChanged(LookupEvent ev) {
                    final Lookup.Result result = (Lookup.Result)ev.getSource();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            DocumentViewOp.this.docView.runReadLockTransaction(new Runnable(){

                                @Override
                                public void run() {
                                    JTextComponent textComponent = DocumentViewOp.this.docView.getTextComponent();
                                    if (textComponent != null) {
                                        textComponent.putClientProperty("text-zoom", null);
                                        DocumentViewOp.this.updateFontColorSettings(result, true);
                                    }
                                }
                            });
                        }

                    });
                }

            };
            mimeType = DocumentUtilities.getMimeType((JTextComponent)textComponent);
            Lookup lookup = MimeLookup.getLookup((String)mimeType);
            Lookup.Result result = lookup.lookupResult(FontColorSettings.class);
            this.updateFontColorSettings(result, false);
            result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupListener, (Object)result));
        }
        if (this.lineWrapType == null) {
            this.updateLineWrapType();
            Document doc = this.docView.getDocument();
            this.updateTextLimitLine(doc);
            this.clearStatusBits(256);
            DocumentUtilities.addPropertyChangeListener((Document)doc, (PropertyChangeListener)WeakListeners.propertyChange((PropertyChangeListener)this, (Object)doc));
        }
    }

    void updatePreferencesSettings(boolean nonInitialUpdate) {
        boolean nonPrintableCharactersVisibleOrig = this.isAnyStatusBit(512);
        boolean nonPrintableCharactersVisible = Boolean.TRUE.equals(this.prefs.getBoolean("non-printable-characters-visible", false));
        this.updateStatusBits(512, nonPrintableCharactersVisible);
        float lineHeightCorrectionOrig = this.rowHeightCorrection;
        this.rowHeightCorrection = this.prefs.getFloat("line-height-correction", 1.0f);
        boolean updateMetrics = this.rowHeightCorrection != lineHeightCorrectionOrig;
        boolean releaseChildren = nonInitialUpdate && (nonPrintableCharactersVisible != nonPrintableCharactersVisibleOrig || this.rowHeightCorrection != lineHeightCorrectionOrig);
        this.indentLevelSize = this.getIndentSize();
        this.tabSize = this.prefs.getInt("tab-size", 8);
        if (updateMetrics) {
            this.updateCharMetrics();
        }
        if (releaseChildren) {
            this.releaseChildren(false);
        }
        boolean currentGuideLinesEnable = Boolean.TRUE.equals(this.prefs.getBoolean("enable.guide.lines", true));
        if (nonInitialUpdate && this.guideLinesEnable != currentGuideLinesEnable) {
            this.docView.op.notifyRepaint(this.visibleRect.getMinX(), this.visibleRect.getMinY(), this.visibleRect.getMaxX(), this.visibleRect.getMaxY());
        }
        this.guideLinesEnable = currentGuideLinesEnable;
    }

    void updateFontColorSettings(Lookup.Result<FontColorSettings> result, boolean nonInitialUpdate) {
        boolean repaint;
        JTextComponent textComponent = this.docView.getTextComponent();
        if (textComponent == null) {
            return;
        }
        AttributeSet defaultColoringOrig = this.defaultColoring;
        FontColorSettings fcs = (FontColorSettings)result.allInstances().iterator().next();
        AttributeSet attribs = fcs.getFontColors("indent-guide-lines");
        this.guideLinesColor = attribs != null ? (Color)attribs.getAttribute(StyleConstants.Foreground) : Color.LIGHT_GRAY;
        AttributeSet newDefaultColoring = fcs.getFontColors("default");
        if (newDefaultColoring != null) {
            this.defaultColoring = newDefaultColoring;
        }
        this.renderingHints = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
        if (this.asTextField) {
            return;
        }
        Color textLimitLineColorOrig = this.textLimitLineColor;
        AttributeSet textLimitLineColoring = fcs.getFontColors("text-limit-line-color");
        Color color = this.textLimitLineColor = textLimitLineColoring != null ? (Color)textLimitLineColoring.getAttribute(StyleConstants.Foreground) : null;
        if (this.textLimitLineColor == null) {
            this.textLimitLineColor = Color.PINK;
        }
        final boolean applyDefaultColoring = this.defaultColoring != defaultColoringOrig;
        final boolean releaseChildren = nonInitialUpdate && applyDefaultColoring;
        boolean bl = repaint = nonInitialUpdate && (this.textLimitLineColor == null || !this.textLimitLineColor.equals(textLimitLineColorOrig));
        if (applyDefaultColoring || releaseChildren || repaint) {
            ViewUtils.runInEDT(new Runnable(){

                @Override
                public void run() {
                    JTextComponent textComponent = DocumentViewOp.this.docView.getTextComponent();
                    if (textComponent != null) {
                        if (applyDefaultColoring) {
                            DocumentViewOp.this.applyDefaultColoring(textComponent);
                        }
                        if (releaseChildren) {
                            DocumentViewOp.this.releaseChildren(true);
                        }
                        if (repaint) {
                            textComponent.repaint();
                        }
                    }
                }
            });
        }
    }

    void applyDefaultColoring(JTextComponent textComponent) {
        Color backColor;
        Color foreColor;
        AttributeSet coloring = this.defaultColoring;
        Font font = ViewUtils.getFont(coloring);
        if (font != null) {
            textComponent.setFont(font);
        }
        if ((foreColor = (Color)coloring.getAttribute(StyleConstants.Foreground)) != null) {
            textComponent.setForeground(foreColor);
        }
        if ((backColor = (Color)coloring.getAttribute(StyleConstants.Background)) != null) {
            textComponent.setBackground(backColor);
        }
    }

    private void updateTextLimitLine(Document doc) {
        Integer dllw = (Integer)doc.getProperty("text-limit-width");
        int textLimitLineColumn = dllw != null ? dllw : 80;
        Preferences prefsLocal = this.prefs;
        if (prefsLocal != null) {
            boolean drawTextLimitLine = prefsLocal.getBoolean("text-limit-line-visible", true);
            this.textLimitLineX = drawTextLimitLine ? (int)((float)textLimitLineColumn * this.defaultCharWidth) : -1;
        }
    }

    private void updateLineWrapType() {
        String lwt = null;
        JTextComponent textComponent = this.docView.getTextComponent();
        if (textComponent != null) {
            lwt = (String)textComponent.getClientProperty("text-line-wrap");
        }
        if (lwt == null) {
            Document doc = this.docView.getDocument();
            lwt = (String)doc.getProperty("text-line-wrap");
        }
        if (lwt != null) {
            this.lineWrapType = LineWrapType.fromSettingValue(lwt);
        }
        if (this.asTextField || this.lineWrapType == null) {
            this.lineWrapType = LineWrapType.NONE;
        }
        this.clearStatusBits(256);
    }

    private void updateCharMetrics() {
        Font font;
        this.checkFontRenderContext();
        FontRenderContext frc = this.getFontRenderContext();
        JTextComponent textComponent = this.docView.getTextComponent();
        if (frc != null && textComponent != null && (font = textComponent.getFont()) != null) {
            this.fontInfos.clear();
            FontInfo defaultFontInfo = new FontInfo(font, textComponent, frc, this.rowHeightCorrection, this.textZoom);
            this.fontInfos.put(font, defaultFontInfo);
            this.fontInfos.put(null, defaultFontInfo);
            this.updateRowHeight(defaultFontInfo, true);
            this.defaultFont = font;
            this.defaultCharWidth = defaultFontInfo.charWidth;
            this.tabTextLayout = null;
            this.singleCharTabTextLayout = null;
            this.newlineTextLayout = null;
            this.lineContinuationTextLayout = null;
            this.updateTextLimitLine(this.docView.getDocument());
            this.clearStatusBits(256);
            ViewHierarchyImpl.SETTINGS_LOG.fine("updateCharMetrics(): FontRenderContext: AA=" + frc.isAntiAliased() + ", AATransformed=" + frc.isTransformed() + ", AAFractMetrics=" + frc.usesFractionalMetrics() + ", AAHint=" + frc.getAntiAliasingHint() + "\n");
        }
    }

    private void updateRowHeight(FontInfo fontInfo, boolean force) {
        if (force || this.defaultAscentInt < fontInfo.ascentInt) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("DV.updateRowHeight()" + (force ? "-forced" : "") + ": defaultAscentInt from " + this.defaultAscentInt + " to " + fontInfo.ascentInt + "\n");
            }
            this.defaultAscentInt = fontInfo.ascentInt;
        }
        if (force || this.defaultRowHeightInt < fontInfo.rowHeightInt) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("DV.updateRowHeight()" + (force ? "-forced" : "") + ": defaultRowHeightInt from " + this.defaultRowHeightInt + " to " + fontInfo.rowHeightInt + "\n");
            }
            this.defaultRowHeightInt = fontInfo.rowHeightInt;
        }
    }

    void markIncomingModification() {
        this.setStatusBits(32);
    }

    void clearIncomingModification() {
        this.clearStatusBits(32);
    }

    boolean isActive() {
        if (this.isAnyStatusBit(64)) {
            throw new IllegalStateException("View hierarchy must not be queried during change firing");
        }
        if (this.isAnyStatusBit(32)) {
            if (ViewHierarchyImpl.OP_LOG.isLoggable(Level.FINER)) {
                ViewHierarchyImpl.OP_LOG.log(Level.INFO, "View Hierarchy Query during Incoming Modification\n", new Exception());
            }
            return false;
        }
        return this.isUpdatable();
    }

    boolean isUpdatable() {
        JTextComponent textComponent = this.docView.getTextComponent();
        return textComponent != null && this.isChildrenValid() && this.lengthyAtomicEdit <= 0;
    }

    boolean isBuildable() {
        JTextComponent textComponent = this.docView.getTextComponent();
        return textComponent != null && this.fontRenderContext != null && this.fontInfos.size() > 0 && this.lengthyAtomicEdit <= 0 && !this.isAnyStatusBit(32);
    }

    private int getIndentSize() {
        Integer indentLevelInteger = (Integer)this.docView.getDocument().getProperty("indent-shift-width");
        if (indentLevelInteger != null && indentLevelInteger > 0) {
            return indentLevelInteger;
        }
        int indentLevel = this.prefs.getInt("indent-shift-width", 0);
        if (indentLevel > 0) {
            return indentLevel;
        }
        Boolean expandTabsBoolean = (Boolean)this.docView.getDocument().getProperty("expand-tabs");
        if (expandTabsBoolean != null) {
            if (Boolean.TRUE.equals(expandTabsBoolean)) {
                indentLevelInteger = (Integer)this.docView.getDocument().getProperty("spaces-per-tab");
                if (indentLevelInteger == null) {
                    return this.prefs.getInt("spaces-per-tab", 4);
                }
            } else {
                indentLevelInteger = (Integer)this.docView.getDocument().getProperty("tab-size");
                if (indentLevelInteger == null) {
                    return this.prefs.getInt("tab-size", 8);
                }
            }
            return indentLevelInteger;
        }
        boolean expandTabs = this.prefs.getBoolean("expand-tabs", true);
        indentLevel = expandTabs ? this.prefs.getInt("spaces-per-tab", 4) : this.prefs.getInt("tab-size", 8);
        return indentLevel;
    }

    public boolean isGuideLinesEnable() {
        return this.guideLinesEnable && !this.asTextField;
    }

    public int getIndentLevelSize() {
        return this.indentLevelSize;
    }

    public int getTabSize() {
        return this.tabSize;
    }

    public Color getGuideLinesColor() {
        return this.guideLinesColor;
    }

    public void setGuideLinesCache(int cacheAtOffset, int foundAtOffset, int length) {
        this.guideLinesCache = new int[]{cacheAtOffset, foundAtOffset, length};
    }

    public int[] getGuideLinesCache() {
        return this.guideLinesCache;
    }

    public void updateLengthyAtomicEdit(int delta) {
        this.lengthyAtomicEdit += delta;
        if (LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(LOG, "updateLengthyAtomicEdit: delta=" + delta + " lengthyAtomicEdit=" + this.lengthyAtomicEdit + "\n");
        }
        if (this.lengthyAtomicEdit == 0) {
            this.releaseChildren(false);
        }
    }

    Rectangle getVisibleRect() {
        return this.visibleRect;
    }

    float getAvailableWidth() {
        if (!this.isAnyStatusBit(256)) {
            this.setStatusBits(256);
            this.renderWrapWidth = this.availableWidth = 2.14748365E9f;
            TextLayout lineContTextLayout = this.getLineContinuationCharTextLayout();
            if (lineContTextLayout != null && this.getLineWrapType() != LineWrapType.NONE) {
                this.availableWidth = Math.max((float)this.getVisibleRect().width, 4.0f * this.getDefaultCharWidth() + lineContTextLayout.getAdvance());
                this.renderWrapWidth = this.availableWidth - lineContTextLayout.getAdvance();
            }
        }
        return this.availableWidth;
    }

    float getRenderWrapWidth() {
        return this.renderWrapWidth;
    }

    TextLayoutCache getTextLayoutCache() {
        return this.textLayoutCache;
    }

    FontRenderContext getFontRenderContext() {
        return this.fontRenderContext;
    }

    public Font getDefaultFont() {
        return this.defaultFont;
    }

    public float getDefaultRowHeight() {
        this.checkSettingsInfo();
        return this.defaultRowHeightInt;
    }

    public float getDefaultAscent() {
        this.checkSettingsInfo();
        return this.defaultAscentInt;
    }

    public float[] getUnderlineAndStrike(Font font) {
        this.checkSettingsInfo();
        FontInfo fontInfo = this.fontInfos.get(font);
        if (fontInfo == null) {
            fontInfo = this.fontInfos.get(null);
        }
        return fontInfo.underlineAndStrike;
    }

    public float getDefaultCharWidth() {
        this.checkSettingsInfo();
        return this.defaultCharWidth;
    }

    public boolean isNonPrintableCharactersVisible() {
        this.checkSettingsInfo();
        return this.isAnyStatusBit(512) && !this.asTextField;
    }

    LineWrapType getLineWrapType() {
        this.checkSettingsInfo();
        return this.lineWrapType;
    }

    Color getTextLimitLineColor() {
        this.checkSettingsInfo();
        return this.textLimitLineColor;
    }

    int getTextLimitLineX() {
        return this.textLimitLineX;
    }

    TextLayout getNewlineCharTextLayout() {
        if (this.newlineTextLayout == null) {
            this.newlineTextLayout = this.createTextLayout(String.valueOf('\u00b6'), this.defaultFont);
        }
        return this.newlineTextLayout;
    }

    TextLayout getTabCharTextLayout(double availableWidth) {
        if (this.tabTextLayout == null) {
            char tabChar = this.defaultFont.canDisplay('\u2192') ? '\u2192' : '\u00bb';
            this.tabTextLayout = this.createTextLayout(String.valueOf(tabChar), this.defaultFont);
        }
        TextLayout ret = this.tabTextLayout;
        if (this.tabTextLayout != null && availableWidth > 0.0 && (double)this.tabTextLayout.getAdvance() > availableWidth) {
            if (this.singleCharTabTextLayout == null) {
                for (int i = this.defaultFont.getSize() - 1; i >= 0; --i) {
                    Font font = new Font(this.defaultFont.getName(), this.defaultFont.getStyle(), i);
                    char tabChar = font.canDisplay('\u2192') ? '\u2192' : '\u00bb';
                    this.singleCharTabTextLayout = this.createTextLayout(String.valueOf(tabChar), font);
                    if (this.singleCharTabTextLayout == null) break;
                    if (this.singleCharTabTextLayout.getAdvance() > this.getDefaultCharWidth()) continue;
                    LOG.log(Level.FINE, "singleChar font size={0}\n", i);
                    break;
                }
            }
            ret = this.singleCharTabTextLayout;
        }
        return ret;
    }

    TextLayout getLineContinuationCharTextLayout() {
        if (this.lineContinuationTextLayout == null) {
            char lineContinuationChar = '\u21a9';
            if (!this.defaultFont.canDisplay(lineContinuationChar)) {
                lineContinuationChar = '\u2190';
            }
            this.lineContinuationTextLayout = this.createTextLayout(String.valueOf(lineContinuationChar), this.defaultFont);
        }
        return this.lineContinuationTextLayout;
    }

    public FontInfo getFontInfo(Font font) {
        FontInfo fontInfo = this.fontInfos.get(font);
        if (fontInfo == null) {
            fontInfo = new FontInfo(font, this.docView.getTextComponent(), this.getFontRenderContext(), this.rowHeightCorrection, this.textZoom);
            this.fontInfos.put(font, fontInfo);
        }
        return fontInfo;
    }

    TextLayout createTextLayout(String text, Font font) {
        this.checkSettingsInfo();
        if (this.fontRenderContext != null && font != null) {
            ViewStats.incrementTextLayoutCreated(text.length());
            FontInfo fontInfo = this.getFontInfo(font);
            TextLayout textLayout = new TextLayout(text, fontInfo.renderFont, this.fontRenderContext);
            if (fontInfo.updateRowHeight(textLayout, this.rowHeightCorrection)) {
                this.updateRowHeight(fontInfo, false);
                LOG.fine("RowHeight Updated -> release children");
                this.releaseChildren(false);
            }
            return textLayout;
        }
        return null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JTextComponent textComponent = this.docView.getTextComponent();
        if (textComponent == null) {
            return;
        }
        boolean releaseChildren = false;
        boolean updateFonts = false;
        if (evt.getSource() instanceof Document) {
            String propName = evt.getPropertyName();
            if (propName == null || "text-line-wrap".equals(propName)) {
                LineWrapType origLineWrapType = this.lineWrapType;
                this.updateLineWrapType();
                if (origLineWrapType != this.lineWrapType) {
                    LOG.log(Level.FINE, "Changing lineWrapType from {0} to {1}", new Object[]{origLineWrapType, this.lineWrapType});
                    releaseChildren = true;
                }
            }
            if (propName == null || "tab-size".equals(propName)) {
                releaseChildren = true;
                Integer tabSizeInteger = (Integer)this.docView.getDocument().getProperty("tab-size");
                int n = this.tabSize = tabSizeInteger != null ? tabSizeInteger : this.tabSize;
            }
            if (propName == null || "spaces-per-tab".equals(propName) || "tab-size".equals(propName) || "indent-shift-width".equals(propName)) {
                releaseChildren = true;
                this.indentLevelSize = this.getIndentSize();
            }
            if (propName == null || "text-limit-width".equals(propName)) {
                this.updateTextLimitLine(this.docView.getDocument());
                releaseChildren = true;
            }
        } else {
            String propName = evt.getPropertyName();
            if (!"ancestor".equals(propName) && !"document".equals(propName)) {
                if ("font".equals(propName)) {
                    releaseChildren = true;
                    updateFonts = true;
                } else if ("foreground".equals(propName)) {
                    releaseChildren = true;
                } else if ("background".equals(propName)) {
                    releaseChildren = true;
                } else if ("text-line-wrap".equals(propName)) {
                    this.updateLineWrapType();
                    releaseChildren = true;
                } else if ("document-view-start-position".equals(propName) || "document-view-end-position".equals(propName)) {
                    this.docView.runReadLockTransaction(new Runnable(){

                        @Override
                        public void run() {
                            DocumentViewOp.this.docView.updateStartEndOffsets();
                            DocumentViewOp.this.releaseChildrenNeedsLock();
                        }
                    });
                } else if ("text-zoom".equals(propName)) {
                    this.updateTextZoom(textComponent);
                    releaseChildren = true;
                    updateFonts = true;
                } else if ("AsTextField".equals(propName)) {
                    this.asTextField = Boolean.TRUE.equals(textComponent.getClientProperty("AsTextField"));
                    this.updateLineWrapType();
                    this.docView.updateBaseY();
                    releaseChildren = true;
                }
            }
        }
        if (releaseChildren) {
            this.releaseChildren(updateFonts);
        }
    }

    private void updateTextZoom(JTextComponent textComponent) {
        Integer textZoomInteger = (Integer)textComponent.getClientProperty("text-zoom");
        this.textZoom = textZoomInteger != null ? textZoomInteger : 0;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent evt) {
        if (evt.getScrollType() != 0) {
            this.origMouseWheelListener.mouseWheelMoved(evt);
            return;
        }
        int modifiers = 0;
        if (evt.isControlDown()) {
            modifiers |= 128;
        }
        if (evt.isAltDown()) {
            modifiers |= 512;
        }
        if (evt.isShiftDown()) {
            modifiers |= 64;
        }
        if (evt.isMetaDown()) {
            modifiers |= 256;
        }
        Keymap keymap = this.docView.getTextComponent().getKeymap();
        int wheelRotation = evt.getWheelRotation();
        if (wheelRotation < 0) {
            Action action = keymap.getAction(KeyStroke.getKeyStroke(656, modifiers));
            if (action != null) {
                action.actionPerformed(new ActionEvent(this.docView.getTextComponent(), 0, ""));
            } else {
                this.origMouseWheelListener.mouseWheelMoved(evt);
            }
        } else if (wheelRotation > 0) {
            Action action = keymap.getAction(KeyStroke.getKeyStroke(657, modifiers));
            if (action != null) {
                action.actionPerformed(new ActionEvent(this.docView.getTextComponent(), 0, ""));
            } else {
                this.origMouseWheelListener.mouseWheelMoved(evt);
            }
        }
    }

    StringBuilder appendInfo(StringBuilder sb) {
        sb.append(" incomMod=").append(this.isAnyStatusBit(32));
        sb.append(", lengthyAE=").append(this.lengthyAtomicEdit);
        sb.append('\n');
        sb.append(this.viewUpdates);
        sb.append(", ChgFlags:");
        int len = sb.length();
        if (this.isWidthChange()) {
            sb.append(" W");
        }
        if (this.isHeightChange()) {
            sb.append(" H");
        }
        if (sb.length() == len) {
            sb.append(" <NONE>");
        }
        sb.append("; visWidth:").append(this.getVisibleRect().width);
        sb.append(", rowHeight=").append(this.getDefaultRowHeight());
        sb.append(", ascent=").append(this.getDefaultAscent());
        sb.append(", charWidth=").append(this.getDefaultCharWidth());
        return sb;
    }

    public String toString() {
        return this.appendInfo(new StringBuilder(200)).toString();
    }

    static /* synthetic */ Rectangle access$200(DocumentViewOp x0) {
        return x0.visibleRect;
    }

    static {
        String dBuffered;
        String rendering;
        String aaText;
        String useFractionalMetrics;
        String strokeControl;
        String contrast;
        LOG = Logger.getLogger(DocumentViewOp.class.getName());
        extraRenderingHints = new HashMap<Object, Object>();
        String aa = System.getProperty("org.netbeans.editor.aa");
        if (aa != null) {
            extraRenderingHints.put(RenderingHints.KEY_ANTIALIASING, Boolean.parseBoolean(aa) || "on".equals(aa) ? RenderingHints.VALUE_ANTIALIAS_ON : ("false".equalsIgnoreCase(aa) || "off".equals(aa) ? RenderingHints.VALUE_ANTIALIAS_OFF : RenderingHints.VALUE_ANTIALIAS_DEFAULT));
        }
        if ((aaText = System.getProperty("org.netbeans.editor.aa.text")) != null) {
            extraRenderingHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, Boolean.parseBoolean(aaText) || "on".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : ("gasp".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_GASP : ("hbgr".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HBGR : ("hrgb".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB : ("vbgr".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VBGR : ("vrgb".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VRGB : ("false".equalsIgnoreCase(aaText) || "off".equals(aaText) ? RenderingHints.VALUE_TEXT_ANTIALIAS_OFF : RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT)))))));
        }
        if ((useFractionalMetrics = System.getProperty("org.netbeans.editor.aa.fractional")) != null) {
            extraRenderingHints.put(RenderingHints.KEY_FRACTIONALMETRICS, Boolean.parseBoolean(useFractionalMetrics) || "on".equals(useFractionalMetrics) ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : ("false".equalsIgnoreCase(useFractionalMetrics) || "off".equals(useFractionalMetrics) ? RenderingHints.VALUE_FRACTIONALMETRICS_OFF : RenderingHints.VALUE_FRACTIONALMETRICS_DEFAULT));
        }
        if ((rendering = System.getProperty("org.netbeans.editor.aa.rendering")) != null) {
            extraRenderingHints.put(RenderingHints.KEY_RENDERING, "quality".equals(rendering) ? RenderingHints.VALUE_RENDER_QUALITY : ("speed".equals(rendering) ? RenderingHints.VALUE_RENDER_SPEED : RenderingHints.VALUE_RENDER_DEFAULT));
        }
        if ((strokeControl = System.getProperty("org.netbeans.editor.aa.stroke")) != null) {
            extraRenderingHints.put(RenderingHints.KEY_STROKE_CONTROL, "normalize".equals(strokeControl) ? RenderingHints.VALUE_STROKE_NORMALIZE : ("pure".equals(strokeControl) ? RenderingHints.VALUE_STROKE_PURE : RenderingHints.VALUE_STROKE_DEFAULT));
        }
        if ((contrast = System.getProperty("org.netbeans.editor.aa.contrast")) != null) {
            try {
                extraRenderingHints.put(RenderingHints.KEY_TEXT_LCD_CONTRAST, Integer.parseInt(contrast));
            }
            catch (NumberFormatException ex) {
                // empty catch block
            }
        }
        doubleBuffered = (dBuffered = System.getProperty("org.netbeans.editor.double.buffered")) != null ? Boolean.valueOf(Boolean.parseBoolean(dBuffered)) : null;
    }

}

