/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.view.ParagraphView;

public final class TextLayoutCache {
    private static final Logger LOG = Logger.getLogger(TextLayoutCache.class.getName());
    private static final int DEFAULT_CAPACITY = 300;
    private final Map<ParagraphView, Entry> paragraph2entry = new HashMap<ParagraphView, Entry>();
    private Entry head;
    private Entry tail;
    private int capacity = 300;

    void clear() {
        this.paragraph2entry.clear();
        this.tail = null;
        this.head = null;
    }

    int size() {
        return this.paragraph2entry.size();
    }

    int capacity() {
        return this.capacity;
    }

    void setCapacityOrDefault(int capacity) {
        this.capacity = Math.max(capacity, 300);
        for (int i = this.size() - this.capacity; i >= 0; --i) {
            this.removeTailEntry();
        }
    }

    boolean contains(ParagraphView paragraphView) {
        assert (paragraphView != null);
        Entry entry = this.paragraph2entry.get(paragraphView);
        return entry != null;
    }

    void activate(ParagraphView paragraphView) {
        assert (paragraphView != null);
        Entry entry = this.paragraph2entry.get(paragraphView);
        if (entry == null) {
            entry = new Entry(paragraphView);
            this.paragraph2entry.put(paragraphView, entry);
            if (this.paragraph2entry.size() > this.capacity) {
                this.removeTailEntry();
            }
            this.addChainEntryFirst(entry);
        }
        if (this.head != entry) {
            this.removeChainEntry(entry);
            this.addChainEntryFirst(entry);
        }
    }

    private void removeTailEntry() {
        Entry tailEntry = this.paragraph2entry.remove(this.tail.paragraphView);
        assert (tailEntry == this.tail);
        this.removeChainEntry(tailEntry);
        tailEntry.release();
    }

    void replace(ParagraphView origPView, ParagraphView newPView) {
        assert (origPView != null);
        Entry entry = this.paragraph2entry.get(origPView);
        if (entry == null) {
            throw new IllegalStateException("origPView=" + origPView + " not cached!");
        }
        entry.paragraphView = newPView;
        this.paragraph2entry.put(newPView, entry);
    }

    void remove(ParagraphView paragraphView, boolean clearTextLayouts) {
        Entry entry = this.paragraph2entry.remove(paragraphView);
        if (entry != null) {
            this.removeChainEntry(entry);
            if (clearTextLayouts) {
                entry.release();
            }
        }
    }

    String findIntegrityError() {
        Entry entry = this.head;
        if (this.head != null) {
            if (this.tail == null) {
                return "Null tail but non-null head";
            }
        } else if (this.tail != null) {
            return "Null head but non-null tail";
        }
        Entry lastEntry = entry;
        HashSet<Entry> entriesCopy = new HashSet<Entry>(this.paragraph2entry.values());
        while (entry != null) {
            ParagraphView pView = entry.paragraphView;
            if (!entriesCopy.remove(entry)) {
                return "TextLayoutCache: Chain entry not contained (or double contained) in map: " + pView + ", parent=" + pView.getParent();
            }
            if (pView.getParent() == null) {
                return "TextLayoutCache: Null parent for " + pView;
            }
            lastEntry = entry;
            entry = entry.next;
        }
        if (lastEntry != this.tail) {
            return "lastEntry=" + Entry.toString(lastEntry) + " != tail=" + Entry.toString(this.tail);
        }
        if (0 != entriesCopy.size()) {
            return "TextLayoutCache: unchained entryCount=" + entriesCopy.size();
        }
        return null;
    }

    private void addChainEntryFirst(Entry entry) {
        assert (entry.previous == null && entry.next == null);
        if (this.head == null) {
            assert (this.tail == null);
            this.head = this.tail = entry;
        } else {
            assert (this.tail != null);
            entry.next = this.head;
            this.head.previous = entry;
            this.head = entry;
        }
    }

    private void removeChainEntry(Entry entry) {
        if (entry != this.head) {
            entry.previous.next = entry.next;
        } else {
            assert (entry.previous == null);
            this.head = entry.next;
        }
        if (entry != this.tail) {
            entry.next.previous = entry.previous;
        } else {
            assert (entry.next == null);
            this.tail = entry.previous;
        }
        entry.next = null;
        entry.previous = null;
    }

    public String toString() {
        return "size=" + this.size() + ", capacity=" + this.capacity + ", head=" + this.head + ", tail=" + this.tail;
    }

    private static final class Entry {
        ParagraphView paragraphView;
        Entry previous;
        Entry next;

        Entry(ParagraphView paragraphView) {
            this.paragraphView = paragraphView;
        }

        void release() {
            this.paragraphView.releaseTextLayouts();
        }

        public String toString() {
            return "Entry[pView=" + this.paragraphView + "\n" + "previous=" + Entry.toString(this.previous) + ", next=" + Entry.toString(this.next) + "]";
        }

        public String toStringShort() {
            return "Entry@" + System.identityHashCode(this);
        }

        private static String toString(Entry entry) {
            return entry != null ? entry.toStringShort() : "null";
        }
    }

}

