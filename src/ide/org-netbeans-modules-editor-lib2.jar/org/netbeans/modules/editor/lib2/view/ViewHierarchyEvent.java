/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.EventObject;
import javax.swing.event.DocumentEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;

public final class ViewHierarchyEvent
extends EventObject {
    private final ViewHierarchyChange change;

    ViewHierarchyEvent(ViewHierarchy source, ViewHierarchyChange change) {
        super(source);
        this.change = change;
    }

    public ViewHierarchy viewHierarchy() {
        return (ViewHierarchy)this.getSource();
    }

    public DocumentEvent documentEvent() {
        return this.change.documentEvent();
    }

    public int changeStartOffset() {
        return this.change.changeStartOffset();
    }

    public int changeEndOffset() {
        return this.change.changeEndOffset();
    }

    public boolean isChangeY() {
        return this.change.isChangeY();
    }

    public double startY() {
        return this.change.startY();
    }

    public double endY() {
        return this.change.endY();
    }

    public double deltaY() {
        return this.change.deltaY();
    }

    @Override
    public String toString() {
        return this.viewHierarchy().toString() + " " + this.change.toString();
    }
}

