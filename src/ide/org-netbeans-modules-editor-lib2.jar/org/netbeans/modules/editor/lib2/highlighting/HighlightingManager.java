/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.openide.ErrorManager
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.TopologicalSortException
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.highlighting.DirectMergeContainer;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingSpiPackageAccessor;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsLayerAccessor;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsLayerFilter;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.openide.ErrorManager;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.TopologicalSortException;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.ProxyLookup;

public final class HighlightingManager {
    private static final Logger LOG = Logger.getLogger(HighlightingManager.class.getName());
    private final Highlighting highlighting;

    public static synchronized HighlightingManager getInstance(JTextComponent pane) {
        HighlightingManager highlightingManager = (HighlightingManager)pane.getClientProperty(HighlightingManager.class);
        if (highlightingManager == null) {
            highlightingManager = new HighlightingManager(pane);
            pane.putClientProperty(HighlightingManager.class, highlightingManager);
        }
        return highlightingManager;
    }

    public HighlightsContainer getBottomHighlights() {
        return this.highlighting.bottomHighlights();
    }

    public HighlightsContainer getTopHighlights() {
        return this.highlighting.topHighlights();
    }

    public HighlightsLayer findLayer(HighlightsContainer container) {
        return this.highlighting.findLayer(container);
    }

    HighlightsContainer getHighlights(HighlightsLayerFilter filter) {
        return this.highlighting.filteredHighlights(filter);
    }

    public void addChangeListener(ChangeListener listener) {
        this.highlighting.changeListeners.add(listener);
    }

    public void removeChangeListener(ChangeListener listener) {
        this.highlighting.changeListeners.remove(listener);
    }

    public void testSetSingleContainer(HighlightsContainer testSingleContainer) {
        this.highlighting.testSingleContainer = testSingleContainer;
        this.highlighting.rebuildAllLayers();
    }

    private HighlightingManager(JTextComponent pane) {
        this.highlighting = new Highlighting(this, pane);
    }

    private static String simpleToString(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    private static /* varargs */ String mimePathsToString(MimePath ... mimePaths) {
        if (mimePaths == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (MimePath mp : mimePaths) {
            sb.append('\'').append(mp.getPath()).append('\'');
            sb.append(",");
        }
        sb.append('}');
        return sb.toString();
    }

    private static final class RegExpFilter
    implements HighlightsLayerFilter {
        private final List<Pattern> includes;
        private final List<Pattern> excludes;

        public RegExpFilter(Object includes, Object excludes) {
            this.includes = RegExpFilter.buildPatterns(includes);
            this.excludes = RegExpFilter.buildPatterns(excludes);
        }

        @Override
        public List<? extends HighlightsLayer> filterLayers(List<? extends HighlightsLayer> layers) {
            List<? extends HighlightsLayer> includedLayers = this.includes.isEmpty() ? layers : RegExpFilter.filter(layers, this.includes, true);
            List<? extends HighlightsLayer> filteredLayers = this.excludes.isEmpty() ? includedLayers : RegExpFilter.filter(includedLayers, this.excludes, false);
            return filteredLayers;
        }

        private static List<? extends HighlightsLayer> filter(List<? extends HighlightsLayer> layers, List<Pattern> patterns, boolean includeMatches) {
            ArrayList<HighlightsLayer> filtered = new ArrayList<HighlightsLayer>();
            for (HighlightsLayer layer : layers) {
                HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
                boolean matchesExcludes = false;
                for (Pattern pattern : patterns) {
                    boolean matches = pattern.matcher(layerAccessor.getLayerTypeId()).matches();
                    if (matches && includeMatches) {
                        filtered.add(layer);
                    }
                    matchesExcludes = matches ? true : matchesExcludes;
                }
                if (patterns.isEmpty() || matchesExcludes || includeMatches) continue;
                filtered.add(layer);
            }
            return filtered;
        }

        private static List<Pattern> buildPatterns(Object expressions) {
            ArrayList<Pattern> patterns = new ArrayList<Pattern>();
            if (expressions instanceof String) {
                try {
                    patterns.add(Pattern.compile((String)expressions));
                }
                catch (PatternSyntaxException e) {
                    LOG.log(Level.WARNING, "Ignoring invalid regexp for the HighlightsLayer filtering.", e);
                }
            } else if (expressions instanceof String[]) {
                for (String expression : (String[])expressions) {
                    try {
                        patterns.add(Pattern.compile(expression));
                        continue;
                    }
                    catch (PatternSyntaxException e) {
                        LOG.log(Level.WARNING, "Ignoring invalid regexp for the HighlightsLayer filtering.", e);
                    }
                }
            }
            return patterns;
        }
    }

    private static final class Highlighting
    implements PropertyChangeListener {
        private static final String PROP_MIME_TYPE = "mimeType";
        private static final String PROP_DOCUMENT = "document";
        private static final String PROP_HL_INCLUDES = "HighlightsLayerIncludes";
        private static final String PROP_HL_EXCLUDES = "HighlightsLayerExcludes";
        private Lookup.Result<HighlightsLayerFactory> factories = null;
        private final LookupListener factoriesTracker;
        private LookupListener weakFactoriesTracker;
        private Lookup.Result<FontColorSettings> settings;
        private final LookupListener settingsTracker;
        private LookupListener weakSettingsTracker;
        private final HighlightingManager manager;
        private final JTextComponent pane;
        private HighlightsLayerFilter paneFilter;
        private Reference<Document> lastKnownDocumentRef;
        private MimePath[] lastKnownMimePaths;
        private boolean inRebuildAllLayers;
        private List<? extends HighlightsLayer> sortedLayers;
        private DirectMergeContainer bottomHighlights;
        private DirectMergeContainer topHighlights;
        List<ChangeListener> changeListeners;
        HighlightsContainer testSingleContainer;

        public Highlighting(HighlightingManager manager, JTextComponent pane) {
            this.factoriesTracker = new LookupListener(){

                public void resultChanged(LookupEvent ev) {
                    Highlighting.this.rebuildAllLayers();
                }
            };
            this.weakFactoriesTracker = null;
            this.settings = null;
            this.settingsTracker = new LookupListener(){

                public void resultChanged(LookupEvent ev) {
                    Highlighting.this.rebuildAllLayers();
                }
            };
            this.weakSettingsTracker = null;
            this.lastKnownMimePaths = null;
            this.inRebuildAllLayers = false;
            this.changeListeners = new CopyOnWriteArrayList<ChangeListener>();
            this.manager = manager;
            this.pane = pane;
            this.paneFilter = new RegExpFilter(pane.getClientProperty("HighlightsLayerIncludes"), pane.getClientProperty("HighlightsLayerExcludes"));
            this.pane.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)pane));
            this.rebuildAll();
        }

        synchronized HighlightsContainer bottomHighlights() {
            return this.bottomHighlights;
        }

        synchronized HighlightsContainer topHighlights() {
            return this.topHighlights;
        }

        synchronized HighlightsContainer filteredHighlights(HighlightsLayerFilter filter) {
            List<? extends HighlightsLayer> layers = filter == null ? this.sortedLayers : filter.filterLayers(this.sortedLayers);
            ArrayList<HighlightsContainer> containers = new ArrayList<HighlightsContainer>(layers.size());
            for (HighlightsLayer layer : layers) {
                HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
                containers.add(layerAccessor.getContainer());
            }
            return new DirectMergeContainer(containers.toArray(new HighlightsContainer[containers.size()]));
        }

        synchronized HighlightsLayer findLayer(HighlightsContainer container) {
            for (HighlightsLayer layer : this.sortedLayers) {
                if (HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer).getContainer() != container) continue;
                return layer;
            }
            return null;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Document doc;
            if ((evt.getPropertyName() == null || "document".equals(evt.getPropertyName())) && (doc = this.pane.getDocument()) != null) {
                doc.render(new Runnable(){

                    @Override
                    public void run() {
                        Highlighting.this.rebuildAll();
                    }
                });
            }
            if ("HighlightsLayerIncludes".equals(evt.getPropertyName()) || "HighlightsLayerExcludes".equals(evt.getPropertyName())) {
                this.rebuildAllLayers();
            }
        }

        private MimePath[] getAllDocumentMimePath() {
            Document doc = this.pane.getDocument();
            Object propMimeType = doc.getProperty("mimeType");
            String mainMimeType = propMimeType != null ? propMimeType.toString() : this.pane.getUI().getEditorKit(this.pane).getContentType();
            return new MimePath[]{MimePath.parse((String)mainMimeType)};
        }

        private synchronized void rebuildAll() {
            Document lastKnownDocument;
            Object[] mimePaths = this.getAllDocumentMimePath();
            Document document = lastKnownDocument = this.lastKnownDocumentRef == null ? null : this.lastKnownDocumentRef.get();
            if (!Utilities.compareObjects((Object)lastKnownDocument, (Object)this.pane.getDocument()) || !Arrays.equals(this.lastKnownMimePaths, mimePaths)) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("rebuildAll: lastKnownDocument = " + HighlightingManager.simpleToString(lastKnownDocument) + ", document = " + HighlightingManager.simpleToString(this.pane.getDocument()) + ", lastKnownMimePaths = " + HighlightingManager.mimePathsToString(this.lastKnownMimePaths) + ", mimePaths = " + HighlightingManager.mimePathsToString((MimePath[])mimePaths) + "\n");
                }
                if (this.factories != null && this.weakFactoriesTracker != null) {
                    this.factories.removeLookupListener(this.weakFactoriesTracker);
                    this.weakFactoriesTracker = null;
                }
                if (this.settings != null && this.weakSettingsTracker != null) {
                    this.settings.removeLookupListener(this.weakSettingsTracker);
                    this.weakSettingsTracker = null;
                }
                if (mimePaths != null) {
                    ArrayList<Lookup> lookups = new ArrayList<Lookup>();
                    for (Object mimePath : mimePaths) {
                        lookups.add(MimeLookup.getLookup((MimePath)mimePath));
                    }
                    ProxyLookup lookup = new ProxyLookup(lookups.toArray((T[])new Lookup[lookups.size()]));
                    this.factories = lookup.lookup(new Lookup.Template(HighlightsLayerFactory.class));
                    this.settings = lookup.lookup(new Lookup.Template(FontColorSettings.class));
                } else {
                    this.factories = null;
                    this.settings = null;
                }
                if (this.factories != null) {
                    this.weakFactoriesTracker = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.factoriesTracker, this.factories);
                    this.factories.addLookupListener(this.weakFactoriesTracker);
                    this.factories.allItems();
                }
                if (this.settings != null) {
                    this.weakSettingsTracker = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.settingsTracker, this.settings);
                    this.settings.addLookupListener(this.weakSettingsTracker);
                    this.settings.allItems();
                }
                lastKnownDocument = this.pane.getDocument();
                this.lastKnownMimePaths = mimePaths;
                this.rebuildAllLayers();
            }
        }

        private void fireChangeListeners() {
            ChangeEvent evt = new ChangeEvent(this.manager);
            for (ChangeListener l : this.changeListeners) {
                l.stateChanged(evt);
            }
        }

        synchronized void rebuildAllLayers() {
            Document doc = this.pane.getDocument();
            if (doc != null) {
                doc.render(new Runnable(){

                    @Override
                    public void run() {
                        Highlighting.this.rebuildAllLayersImpl();
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void rebuildAllLayersImpl() {
            if (this.inRebuildAllLayers) {
                return;
            }
            this.inRebuildAllLayers = true;
            try {
                void topContainers22;
                Document doc = this.pane.getDocument();
                Collection all = this.factories.allInstances();
                HashMap<String, HighlightsLayer> layers = new HashMap<String, HighlightsLayer>();
                HighlightsLayerFactory.Context context = HighlightingSpiPackageAccessor.get().createFactoryContext(doc, this.pane);
                for (HighlightsLayerFactory factory : all) {
                    HighlightsLayer[] factoryLayers = factory.createLayers(context);
                    if (factoryLayers == null) continue;
                    for (HighlightsLayer layer : factoryLayers) {
                        HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
                        String layerTypeId = layerAccessor.getLayerTypeId();
                        if (layers.containsKey(layerTypeId)) continue;
                        layers.put(layerTypeId, layer);
                    }
                }
                try {
                    this.sortedLayers = HighlightingSpiPackageAccessor.get().sort(layers.values());
                }
                catch (TopologicalSortException tse) {
                    List sl;
                    ErrorManager.getDefault().notify((Throwable)tse);
                    this.sortedLayers = sl = tse.partialSort();
                }
                this.sortedLayers = this.paneFilter.filterLayers(this.sortedLayers);
                int topStartIndex = 0;
                for (int i = 0; i < this.sortedLayers.size(); ++i) {
                    HighlightsLayer layer = this.sortedLayers.get(i);
                    HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
                    if (layerAccessor.isFixedSize()) continue;
                    topStartIndex = i + 1;
                }
                ArrayList<HighlightsContainer> layerContainers = new ArrayList<HighlightsContainer>();
                for (HighlightsLayer layer : this.sortedLayers) {
                    HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
                    layerContainers.add(layerAccessor.getContainer());
                }
                List bottomContainers = layerContainers.subList(0, topStartIndex);
                List topContainers22 = layerContainers.subList(topStartIndex, this.sortedLayers.size());
                if (LOG.isLoggable(Level.FINER)) {
                    StringBuilder sb = new StringBuilder(300);
                    Highlighting.dumpInfo(sb, doc, this.lastKnownMimePaths);
                    Highlighting.dumpLayers(sb, "Bottom", this.sortedLayers.subList(0, topStartIndex));
                    Highlighting.dumpLayers(sb, "Top", this.sortedLayers.subList(topStartIndex, this.sortedLayers.size()));
                    LOG.finer(sb.toString());
                }
                if (this.testSingleContainer != null) {
                    bottomContainers = Collections.singletonList(this.testSingleContainer);
                    List topContainers22 = Collections.emptyList();
                }
                this.bottomHighlights = new DirectMergeContainer(bottomContainers.toArray(new HighlightsContainer[bottomContainers.size()]));
                this.topHighlights = new DirectMergeContainer(topContainers22.toArray(new HighlightsContainer[topContainers22.size()]));
            }
            finally {
                this.inRebuildAllLayers = false;
            }
            this.fireChangeListeners();
        }

        private static void dumpInfo(StringBuilder sb, Document doc, MimePath[] mimePaths) {
            sb.append(" HighlighsLayers:\n");
            sb.append(" * document : ");
            sb.append(doc.getClass().getName()).append('@').append(Integer.toHexString(System.identityHashCode(doc)));
            Object streamDescriptor = doc.getProperty("stream");
            sb.append(" [").append(streamDescriptor == null ? "no stream descriptor" : streamDescriptor.toString()).append(']');
            sb.append("\n");
            sb.append(" * mime paths : \n");
            for (MimePath mimePath : mimePaths) {
                sb.append("    ");
                sb.append(mimePath.getPath());
                sb.append("\n");
            }
        }

        private static void dumpLayers(StringBuilder sb, String prefix, List<? extends HighlightsLayer> layers) {
            sb.append(prefix).append(" layers:\n");
            int digitCount = ArrayUtilities.digitCount((int)layers.size());
            for (int i = 0; i < layers.size(); ++i) {
                HighlightsLayer layer = layers.get(i);
                HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
                sb.append("  ");
                ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
                sb.append(layerAccessor.getLayerTypeId());
                sb.append('[');
                sb.append(layerAccessor.getZOrder().toString());
                sb.append(layerAccessor.isFixedSize() ? ",Fixed" : ",NonFixed");
                sb.append(']');
                sb.append('@');
                sb.append(Integer.toHexString(System.identityHashCode(layer)));
                sb.append("\n");
            }
        }

    }

}

