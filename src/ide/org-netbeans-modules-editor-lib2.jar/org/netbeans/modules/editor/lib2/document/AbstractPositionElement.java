/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;

public abstract class AbstractPositionElement
implements Element {
    private final Element parent;
    private final Position startPos;
    private final Position endPos;

    public static Position createPosition(Document doc, int offset) {
        try {
            return doc.createPosition(offset);
        }
        catch (BadLocationException ex) {
            throw new IndexOutOfBoundsException(ex.getMessage());
        }
    }

    AbstractPositionElement(Element parent, Position startPos, Position endPos) {
        assert (startPos != null);
        assert (endPos != null);
        this.parent = parent;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    AbstractPositionElement(Element parent, int startOffset, int endOffset) {
        this(parent, AbstractPositionElement.createPosition(parent.getDocument(), startOffset), AbstractPositionElement.createPosition(parent.getDocument(), endOffset));
    }

    @Override
    public Document getDocument() {
        return this.parent.getDocument();
    }

    @Override
    public int getStartOffset() {
        return this.startPos.getOffset();
    }

    public Position getStartPosition() {
        return this.startPos;
    }

    @Override
    public int getEndOffset() {
        return this.endPos.getOffset();
    }

    public Position getEndPosition() {
        return this.endPos;
    }

    @Override
    public Element getParentElement() {
        return this.parent;
    }

    @Override
    public AttributeSet getAttributes() {
        return SimpleAttributeSet.EMPTY;
    }

    @Override
    public int getElementIndex(int offset) {
        return -1;
    }

    @Override
    public int getElementCount() {
        return 0;
    }

    @Override
    public Element getElement(int index) {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    public String toString() {
        return "getStartOffset()=" + this.getStartOffset() + ", getEndOffset()=" + this.getEndOffset();
    }
}

