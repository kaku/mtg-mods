/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

import java.awt.Dimension;
import java.awt.Insets;
import org.netbeans.modules.editor.lib2.Acceptor;
import org.netbeans.modules.editor.lib2.AcceptorFactory;

public final class EditorPreferencesDefaults {
    public static final String LINE_CARET = "line-caret";
    public static final String BLOCK_CARET = "block-caret";
    public static final boolean defaultToolbarVisible = true;
    public static final boolean defaultPopupMenuEnabled = true;
    public static final int defaultCaretBlinkRate = 300;
    public static final int defaultTabSize = 8;
    public static final int defaultSpacesPerTab = 4;
    public static final int defaultShiftWidth = 4;
    public static final int defaultStatusBarCaretDelay = 200;
    public static final int defaultTextLimitWidth = 80;
    public static final Acceptor defaultIdentifierAcceptor = AcceptorFactory.LETTER_DIGIT;
    public static final Acceptor defaultWhitespaceAcceptor = AcceptorFactory.WHITESPACE;
    public static final float defaultLineHeightCorrection = 1.0f;
    public static final int defaultTextLeftMarginWidth = 2;
    public static final Insets defaultMargin = new Insets(0, 0, 0, 0);
    public static final Insets defaultScrollJumpInsets = new Insets(-5, -10, -5, -30);
    public static final Insets defaultScrollFindInsets = new Insets(-10, -10, -10, -10);
    public static final Dimension defaultComponentSizeIncrement = new Dimension(-5, -30);
    public static final int defaultReadBufferSize = 16384;
    public static final int defaultWriteBufferSize = 16384;
    public static final int defaultReadMarkDistance = 180;
    public static final int defaultMarkDistance = 100;
    public static final int defaultMaxMarkDistance = 150;
    public static final int defaultMinMarkDistance = 50;
    public static final int defaultSyntaxUpdateBatchSize = 700;
    public static final int defaultLineBatchSize = 2;
    public static final boolean defaultExpandTabs = true;
    public static final String defaultCaretTypeInsertMode = "line-caret";
    public static final String defaultCaretTypeOverwriteMode = "block-caret";
    public static final boolean defaultCaretItalicInsertMode = false;
    public static final boolean defaultCaretItalicOverwriteMode = false;
    public static final int defaultThickCaretWidth = 5;
    public static final Acceptor defaultAbbrevExpandAcceptor = AcceptorFactory.WHITESPACE;
    public static final Acceptor defaultAbbrevAddTypedCharAcceptor = AcceptorFactory.NL;
    public static final Acceptor defaultAbbrevResetAcceptor = AcceptorFactory.NON_JAVA_IDENTIFIER;
    public static final boolean defaultStatusBarVisible = true;
    public static final boolean defaultLineNumberVisible = true;
    public static final boolean defaultPrintLineNumberVisible = true;
    public static final boolean defaultTextLimitLineVisible = true;
    public static final boolean defaultHomeKeyColumnOne = false;
    public static final boolean defaultWordMoveNewlineStop = true;
    public static final boolean defaultInputMethodsEnabled = true;
    public static final boolean defaultFindHighlightSearch = true;
    public static final boolean defaultFindIncSearch = true;
    public static final boolean defaultFindBackwardSearch = false;
    public static final boolean defaultFindWrapSearch = true;
    public static final boolean defaultFindMatchCase = false;
    public static final boolean defaultFindWholeWords = false;
    public static final boolean defaultFindRegExp = false;
    public static final int defaultFindHistorySize = 30;
    public static final int defaultWordMatchSearchLen = Integer.MAX_VALUE;
    public static final boolean defaultWordMatchWrapSearch = true;
    public static final boolean defaultWordMatchMatchOneChar = true;
    public static final boolean defaultWordMatchMatchCase = false;
    public static final boolean defaultWordMatchSmartCase = false;
    public static final boolean defaultCodeFoldingEnable = true;

    private EditorPreferencesDefaults() {
    }
}

