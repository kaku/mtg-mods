/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsSequenceEx;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.openide.util.WeakListeners;

public final class DirectMergeContainer
implements HighlightsContainer,
HighlightsChangeListener {
    private static final Logger LOG = Logger.getLogger(DirectMergeContainer.class.getName());
    static final int MAX_EMPTY_HIGHLIGHT_COUNT = 10000;
    private final HighlightsContainer[] layers;
    private final List<HighlightsChangeListener> listeners = new CopyOnWriteArrayList<HighlightsChangeListener>();
    private final List<Reference<HlSequence>> activeHlSeqs = new ArrayList<Reference<HlSequence>>();
    private HighlightsChangeEvent layerEvent;

    public DirectMergeContainer(HighlightsContainer[] layers) {
        this.layers = layers;
        for (int i = 0; i < layers.length; ++i) {
            HighlightsContainer layer = layers[i];
            layer.addHighlightsChangeListener((HighlightsChangeListener)WeakListeners.create(HighlightsChangeListener.class, (EventListener)this, (Object)layer));
        }
    }

    public HighlightsContainer[] getLayers() {
        return this.layers;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        HlSequence hs = new HlSequence(this.layers, startOffset, endOffset);
        List<Reference<HlSequence>> list = this.activeHlSeqs;
        synchronized (list) {
            this.activeHlSeqs.add(new WeakReference<HlSequence>(hs));
        }
        return hs;
    }

    @Override
    public void addHighlightsChangeListener(HighlightsChangeListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public void removeHighlightsChangeListener(HighlightsChangeListener listener) {
        this.listeners.remove(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void highlightChanged(HighlightsChangeEvent event) {
        this.layerEvent = event;
        try {
            if (!this.listeners.isEmpty()) {
                event = new HighlightsChangeEvent(this, event.getStartOffset(), event.getEndOffset());
                for (HighlightsChangeListener l : this.listeners) {
                    l.highlightChanged(event);
                }
            }
            List<Reference<HlSequence>> i$ = this.activeHlSeqs;
            synchronized (i$) {
                for (Reference<HlSequence> hlSeqRef : this.activeHlSeqs) {
                    HlSequence seq = hlSeqRef.get();
                    if (seq == null) continue;
                    seq.notifyLayersChanged();
                }
                this.activeHlSeqs.clear();
            }
        }
        finally {
            this.layerEvent = null;
        }
    }

    public HighlightsChangeEvent layerEvent() {
        return this.layerEvent;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        int digitCount = ArrayUtilities.digitCount((int)this.layers.length);
        for (int i = 0; i < this.layers.length; ++i) {
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            sb.append(this.layers[i]);
            sb.append('\n');
        }
        return sb.toString();
    }

    static final class Wrapper {
        final HighlightsContainer layer;
        final HighlightsSequence hlSequence;
        int hlStartOffset;
        int hlEndOffset;
        AttributeSet hlAttrs;
        int nextChangeOffset;
        AttributeSet currentAttrs;
        int mNextChangeOffset;
        AttributeSet mAttrs;
        private int emptyHighlightCount;

        public Wrapper(HighlightsContainer layer, HighlightsSequence hlSequence, int startOffset) {
            this.layer = layer;
            this.hlSequence = hlSequence;
            this.fetchNextHighlight(startOffset);
            this.updateCurrentState(startOffset);
            this.mNextChangeOffset = startOffset;
        }

        boolean isFinished() {
            return this.hlStartOffset == Integer.MAX_VALUE;
        }

        boolean updateCurrentState(int offset) {
            if (offset < this.hlStartOffset) {
                this.currentAttrs = null;
                this.nextChangeOffset = this.hlStartOffset;
                return false;
            }
            if (offset < this.hlEndOffset) {
                this.currentAttrs = this.hlAttrs;
                this.nextChangeOffset = this.hlEndOffset;
                return false;
            }
            return true;
        }

        boolean fetchNextHighlight(int offset) {
            assert (this.hlStartOffset != Integer.MAX_VALUE);
            do {
                if (this.hlSequence.moveNext()) {
                    this.hlStartOffset = this.hlSequence.getStartOffset();
                    if (this.hlStartOffset < this.hlEndOffset) {
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.fine("Disabled an invalid highlighting layer: hlStartOffset=" + this.hlStartOffset + " < previous hlEndOffset=" + this.hlEndOffset + " for layer=" + this.layer);
                        }
                        this.hlEndOffset = Integer.MAX_VALUE;
                        this.hlStartOffset = Integer.MAX_VALUE;
                        return false;
                    }
                    this.hlEndOffset = this.hlSequence.getEndOffset();
                    if (this.hlEndOffset <= this.hlStartOffset) {
                        if (this.hlEndOffset < this.hlStartOffset) {
                            if (LOG.isLoggable(Level.FINE)) {
                                LOG.fine("Disabled an invalid highlighting layer: hlStartOffset=" + this.hlStartOffset + " > hlEndOffset=" + this.hlEndOffset + " for layer=" + this.layer);
                            }
                            this.hlEndOffset = Integer.MAX_VALUE;
                            this.hlStartOffset = Integer.MAX_VALUE;
                            return false;
                        }
                        ++this.emptyHighlightCount;
                        if (this.emptyHighlightCount >= 10000) {
                            if (LOG.isLoggable(Level.FINE)) {
                                LOG.fine("Disabled an invalid highlighting layer: too many empty highlights=" + this.emptyHighlightCount);
                            }
                            this.hlEndOffset = Integer.MAX_VALUE;
                            this.hlStartOffset = Integer.MAX_VALUE;
                            return false;
                        }
                    }
                    this.hlAttrs = this.hlSequence.getAttributes();
                    if (!LOG.isLoggable(Level.FINER)) continue;
                    LOG.fine("Fetched highlight: <" + this.hlStartOffset + "," + this.hlEndOffset + "> for layer=" + this.layer + '\n');
                    continue;
                }
                this.hlEndOffset = Integer.MAX_VALUE;
                this.hlStartOffset = Integer.MAX_VALUE;
                return false;
            } while (this.hlEndOffset <= offset);
            return true;
        }

        public String toString() {
            return "M[" + this.mNextChangeOffset + ",A=" + this.mAttrs + "]  Next[" + this.nextChangeOffset + ",A=" + this.currentAttrs + "]  HL:<" + this.hlStartOffset + "," + this.hlEndOffset + ">,A=" + this.hlAttrs;
        }
    }

    static final class HlSequence
    implements HighlightsSequenceEx {
        private final Wrapper[] wrappers;
        private int topWrapperIndex;
        private final int endOffset;
        int mergedHighlightStartOffset;
        int mergedHighlightEndOffset;
        AttributeSet mergedAttrs;
        volatile boolean finished;

        public HlSequence(HighlightsContainer[] layers, int startOffset, int endOffset) {
            this.endOffset = endOffset;
            this.mergedHighlightStartOffset = startOffset;
            this.mergedHighlightEndOffset = startOffset;
            this.wrappers = new Wrapper[layers.length];
            for (int i = 0; i < layers.length; ++i) {
                HighlightsContainer container = layers[i];
                HighlightsSequence hlSequence = container.getHighlights(startOffset, endOffset);
                Wrapper wrapper = new Wrapper(container, hlSequence, startOffset);
                if (wrapper.isFinished()) continue;
                this.wrappers[this.topWrapperIndex++] = wrapper;
            }
            --this.topWrapperIndex;
            this.updateMergeVars(-1, startOffset);
        }

        @Override
        public boolean moveNext() {
            Wrapper topWrapper;
            if (this.finished || this.topWrapperIndex < 0) {
                return false;
            }
            int lastHighlightEndOffset = this.mergedHighlightEndOffset;
            while ((topWrapper = this.nextMerge(lastHighlightEndOffset)) != null) {
                int nextChangeOffset = topWrapper.mNextChangeOffset;
                if (nextChangeOffset <= lastHighlightEndOffset) {
                    this.finished = true;
                    return false;
                }
                AttributeSet attrs = topWrapper.mAttrs;
                if (attrs != null) {
                    this.mergedHighlightStartOffset = lastHighlightEndOffset;
                    this.mergedHighlightEndOffset = nextChangeOffset;
                    this.mergedAttrs = attrs;
                    return true;
                }
                lastHighlightEndOffset = nextChangeOffset;
            }
            return false;
        }

        @Override
        public int getStartOffset() {
            return this.mergedHighlightStartOffset;
        }

        @Override
        public int getEndOffset() {
            return this.mergedHighlightEndOffset;
        }

        @Override
        public AttributeSet getAttributes() {
            return this.mergedAttrs;
        }

        @Override
        public boolean isStale() {
            return this.finished;
        }

        void notifyLayersChanged() {
            this.finished = true;
        }

        Wrapper nextMerge(int offset) {
            int i;
            for (i = this.topWrapperIndex; i >= 0 && this.wrappers[i].mNextChangeOffset <= offset; --i) {
            }
            return this.updateMergeVars(i, offset);
        }

        /*
         * Unable to fully structure code
         * Enabled aggressive block sorting
         * Lifted jumps to return sites
         */
        Wrapper updateMergeVars(int startIndex, int offset) {
            wrapper = null;
            if (startIndex < 0) {
                nextChangeOffset = this.endOffset;
                lastAttrs = null;
            } else {
                wrapper = this.wrappers[startIndex];
                nextChangeOffset = wrapper.mNextChangeOffset;
                lastAttrs = wrapper.mAttrs;
            }
            ++startIndex;
            while (startIndex <= this.topWrapperIndex) {
                wrapper = this.wrappers[startIndex];
                if (wrapper.nextChangeOffset > offset || !wrapper.updateCurrentState(offset)) ** GOTO lbl19
                if (!wrapper.fetchNextHighlight(offset)) {
                    this.removeWrapper(startIndex);
                    if (--startIndex == this.topWrapperIndex) {
                        wrapper = startIndex >= 0 ? this.wrappers[startIndex] : null;
                    }
                } else {
                    wrapper.updateCurrentState(offset);
lbl19: // 2 sources:
                    if (wrapper.nextChangeOffset < nextChangeOffset) {
                        nextChangeOffset = wrapper.nextChangeOffset;
                    }
                    wrapper.mNextChangeOffset = nextChangeOffset;
                    wrapper.mAttrs = lastAttrs = lastAttrs != null ? (wrapper.currentAttrs != null ? AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{wrapper.currentAttrs, lastAttrs}) : lastAttrs) : wrapper.currentAttrs;
                }
                ++startIndex;
            }
            return wrapper;
        }

        private void removeWrapper(int index) {
            System.arraycopy(this.wrappers, index + 1, this.wrappers, index, this.topWrapperIndex - index);
            this.wrappers[this.topWrapperIndex] = null;
            --this.topWrapperIndex;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(200);
            sb.append("endO=").append(this.endOffset);
            if (this.finished) {
                sb.append("; FINISHED");
            }
            sb.append('\n');
            int digitCount = ArrayUtilities.digitCount((int)(this.topWrapperIndex + 1));
            for (int i = 0; i <= this.topWrapperIndex; ++i) {
                sb.append("  ");
                ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
                sb.append(this.wrappers[i]);
                sb.append('\n');
            }
            return sb.toString();
        }
    }

}

