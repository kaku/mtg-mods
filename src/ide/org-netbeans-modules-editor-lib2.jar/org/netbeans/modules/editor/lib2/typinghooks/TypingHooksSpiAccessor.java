/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.typinghooks;

import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;

public abstract class TypingHooksSpiAccessor {
    private static TypingHooksSpiAccessor ACCESSOR = null;

    public static synchronized void register(TypingHooksSpiAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized TypingHooksSpiAccessor get() {
        try {
            Class clazz = Class.forName(TypedTextInterceptor.MutableContext.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected TypingHooksSpiAccessor() {
    }

    public abstract TypedTextInterceptor.MutableContext createTtiContext(JTextComponent var1, Position var2, String var3, String var4);

    public abstract Object[] getTtiContextData(TypedTextInterceptor.MutableContext var1);

    public abstract void resetTtiContextData(TypedTextInterceptor.MutableContext var1);

    public abstract DeletedTextInterceptor.Context createDtiContext(JTextComponent var1, int var2, String var3, boolean var4);

    public abstract Object[] getDwiContextData(CamelCaseInterceptor.MutableContext var1);

    public abstract CamelCaseInterceptor.MutableContext createDwiContext(JTextComponent var1, int var2, boolean var3);

    public abstract TypedBreakInterceptor.MutableContext createTbiContext(JTextComponent var1, int var2, int var3);

    public abstract Object[] getTbiContextData(TypedBreakInterceptor.MutableContext var1);

    public abstract void resetTbiContextData(TypedBreakInterceptor.MutableContext var1);
}

