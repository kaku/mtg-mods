/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.modules.editor.lib2.view.DocumentView;

public final class ViewFactoryImpl
implements ViewFactory {
    public static final ViewFactory INSTANCE = new ViewFactoryImpl();

    private ViewFactoryImpl() {
    }

    @Override
    public View create(Element elem) {
        String kind = elem.getName();
        if (kind != null) {
            if (kind.equals("content")) {
                return new LabelView(elem);
            }
            if (kind.equals("paragraph")) {
                return null;
            }
            if (kind.equals("section")) {
                return new DocumentView(elem);
            }
            if (kind.equals("component")) {
                return new ComponentView(elem);
            }
            if (kind.equals("icon")) {
                return new IconView(elem);
            }
        }
        return null;
    }
}

