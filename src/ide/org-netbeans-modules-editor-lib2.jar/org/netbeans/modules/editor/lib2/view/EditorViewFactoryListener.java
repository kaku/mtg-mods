/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.EventListener;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryEvent;

public interface EditorViewFactoryListener
extends EventListener {
    public void viewFactoryChanged(EditorViewFactoryEvent var1);
}

