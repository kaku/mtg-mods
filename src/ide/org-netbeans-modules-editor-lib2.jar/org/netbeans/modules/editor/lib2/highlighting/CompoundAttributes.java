/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.Enumeration;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.modules.editor.lib2.highlighting.HighlightItem;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public final class CompoundAttributes
implements AttributeSet {
    private final HighlightItem[] highlightItems;
    private final int startOffset;

    private static void checkHighlightItemsNonNull(HighlightItem[] highlightItems) {
        for (int i = 0; i < highlightItems.length; ++i) {
            if (highlightItems[i] != null) continue;
            throw new IllegalStateException("highlightItems[" + i + "] == null");
        }
    }

    public CompoundAttributes(int startOffset, HighlightItem[] highlightItems) {
        assert (highlightItems.length >= 2);
        this.highlightItems = highlightItems;
        this.startOffset = startOffset;
    }

    public int startOffset() {
        return this.startOffset;
    }

    public int endOffset() {
        return this.highlightItems[this.highlightItems.length - 1].getEndOffset();
    }

    public HighlightItem[] highlightItems() {
        return this.highlightItems;
    }

    public HighlightsSequence createHighlightsSequence(int viewStartOffset, int startOffset, int endOffsets) {
        return new HiSequence(this.highlightItems, viewStartOffset - startOffset, startOffset, startOffset);
    }

    private AttributeSet firstItemAttrs() {
        AttributeSet attrs = this.highlightItems[0].getAttributes();
        if (attrs == null) {
            attrs = SimpleAttributeSet.EMPTY;
        }
        return attrs;
    }

    @Override
    public int getAttributeCount() {
        return this.firstItemAttrs().getAttributeCount();
    }

    @Override
    public boolean isDefined(Object attrName) {
        return this.firstItemAttrs().isDefined(attrName);
    }

    @Override
    public boolean isEqual(AttributeSet attr) {
        return this.firstItemAttrs().isEqual(attr);
    }

    @Override
    public AttributeSet copyAttributes() {
        return this.firstItemAttrs().copyAttributes();
    }

    @Override
    public Object getAttribute(Object key) {
        return this.firstItemAttrs().getAttribute(key);
    }

    @Override
    public Enumeration<?> getAttributeNames() {
        return this.firstItemAttrs().getAttributeNames();
    }

    @Override
    public boolean containsAttribute(Object name, Object value) {
        return this.firstItemAttrs().containsAttribute(name, value);
    }

    @Override
    public boolean containsAttributes(AttributeSet attributes) {
        return this.firstItemAttrs().containsAttributes(attributes);
    }

    @Override
    public AttributeSet getResolveParent() {
        return this.firstItemAttrs().getResolveParent();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        int size = this.highlightItems.length;
        int digitCount = ArrayUtilities.digitCount((int)size);
        int lastOffset = this.startOffset;
        for (int i = 0; i < size; ++i) {
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            sb.append(this.highlightItems[i].toString(lastOffset));
            sb.append('\n');
            lastOffset = this.highlightItems[i].getEndOffset();
        }
        return sb.toString();
    }

    static class HiSequence
    implements HighlightsSequence {
        private final HighlightItem[] highlightItems;
        private final int shift;
        private final int endOffset;
        private int highlightIndex;
        private int hlStartOffset;
        private int hlEndOffset;
        private AttributeSet hlAttrs;

        HiSequence(HighlightItem[] highlightItems, int shift, int startOffset, int endOffset) {
            this.highlightItems = highlightItems;
            this.shift = shift;
            this.endOffset = endOffset;
            this.hlEndOffset = startOffset;
            if (highlightItems.length <= 10) {
                while (this.highlightIndex < highlightItems.length && highlightItems[this.highlightIndex].getEndOffset() + shift <= startOffset) {
                    ++this.highlightIndex;
                }
            }
        }

        @Override
        public boolean moveNext() {
            int lastOffset = this.hlEndOffset;
            while (this.highlightIndex < this.highlightItems.length) {
                AttributeSet attrs;
                HighlightItem highlight;
                if ((attrs = (highlight = this.highlightItems[this.highlightIndex++]).getAttributes()) != null) {
                    this.hlStartOffset = lastOffset;
                    this.hlEndOffset = highlight.getEndOffset() + this.shift;
                    if (this.hlEndOffset > this.endOffset) {
                        this.hlEndOffset = this.endOffset;
                        if (this.hlEndOffset <= this.hlStartOffset) {
                            this.highlightIndex = this.highlightItems.length;
                            return false;
                        }
                    }
                    this.hlAttrs = attrs;
                    return true;
                }
                lastOffset = highlight.getEndOffset() + this.shift;
            }
            return false;
        }

        @Override
        public int getStartOffset() {
            return this.hlStartOffset;
        }

        @Override
        public int getEndOffset() {
            return this.hlEndOffset;
        }

        @Override
        public AttributeSet getAttributes() {
            return this.hlAttrs;
        }
    }

}

