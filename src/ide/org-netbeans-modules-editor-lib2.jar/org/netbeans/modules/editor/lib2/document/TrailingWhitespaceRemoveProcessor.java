/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.document.ModRootElement;

public class TrailingWhitespaceRemoveProcessor {
    private static final Logger LOG = Logger.getLogger(TrailingWhitespaceRemoveProcessor.class.getName());
    private static final boolean REMOVE_WHITESPACE_ON_CURRENT_LINE = Boolean.getBoolean("org.netbeans.editor.remove.whitespace.on.current.line");
    private static final int GET_ELEMENT_INDEX_THRESHOLD = 100;
    private final Document doc;
    private final boolean removeFromModifiedLinesOnly;
    private final CharSequence docText;
    private final Element lineRootElement;
    private Element modRootElement;
    private int modElementIndex;
    private int modElementStartOffset;
    private int modElementEndOffset;
    private int lineIndex;
    private int lineStartOffset;
    private int lineLastOffset;
    private final int caretLineIndex;
    private final int caretRelativeOffset;
    private final AtomicBoolean canceled;

    private static void removeWhitespaceOnLine(int lineStartOffset, int lineLastOffset, int caretRelativeOffset, int lineIndex, int caretLineIndex, Document doc, CharSequence docText) {
        int offset;
        char c;
        int startOffset = lineStartOffset;
        if (lineIndex == caretLineIndex) {
            startOffset += caretRelativeOffset;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Line index " + lineIndex + " contains caret at relative offset " + caretRelativeOffset + ".\n");
            }
        }
        for (offset = lineLastOffset - 1; offset >= startOffset && ((c = docText.charAt(offset)) == ' ' || c == '\t'); --offset) {
        }
        if (++offset < lineLastOffset) {
            BadLocationException ble = null;
            try {
                doc.remove(offset, lineLastOffset - offset);
            }
            catch (BadLocationException e) {
                ble = e;
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Remove between " + DocumentUtilities.debugOffset((Document)doc, (int)offset) + " and " + DocumentUtilities.debugOffset((Document)doc, (int)lineLastOffset) + (ble == null ? " succeeded." : " failed.") + '\n');
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.log(Level.INFO, "Exception thrown during removal:", ble);
                }
            }
        }
    }

    public TrailingWhitespaceRemoveProcessor(Document doc, boolean removeFromModifiedLinesOnly, AtomicBoolean canceled) {
        this.doc = doc;
        this.removeFromModifiedLinesOnly = removeFromModifiedLinesOnly;
        this.canceled = canceled;
        this.docText = DocumentUtilities.getText((Document)doc);
        this.lineRootElement = DocumentUtilities.getParagraphRootElement((Document)doc);
        this.modRootElement = ModRootElement.get(doc);
        JTextComponent lastFocusedComponent = EditorRegistry.lastFocusedComponent();
        if (lastFocusedComponent != null && lastFocusedComponent.getDocument() == doc && !REMOVE_WHITESPACE_ON_CURRENT_LINE) {
            int caretOffset = lastFocusedComponent.getCaretPosition();
            this.caretLineIndex = this.lineRootElement.getElementIndex(caretOffset);
            this.caretRelativeOffset = caretOffset - this.lineRootElement.getElement(this.caretLineIndex).getStartOffset();
        } else {
            this.caretLineIndex = -1;
            this.caretRelativeOffset = 0;
        }
    }

    public void removeWhitespace() {
        if (this.removeFromModifiedLinesOnly) {
            this.modElementIndex = this.modRootElement.getElementCount();
            this.lineStartOffset = Integer.MAX_VALUE;
            while (this.fetchPreviousNonEmptyRegion()) {
                int regionLastOffset = this.modElementEndOffset - 1;
                int lastLineIndex = this.lineIndex;
                if (regionLastOffset + 100 < this.lineStartOffset) {
                    this.lineIndex = this.lineRootElement.getElementIndex(this.modElementEndOffset - 1);
                    this.fetchLineElement();
                } else {
                    while (this.lineStartOffset > regionLastOffset) {
                        --this.lineIndex;
                        this.fetchLineElement();
                    }
                }
                if (lastLineIndex == this.lineIndex) continue;
                TrailingWhitespaceRemoveProcessor.removeWhitespaceOnLine(this.lineStartOffset, this.lineLastOffset, this.caretRelativeOffset, this.lineIndex, this.caretLineIndex, this.doc, this.docText);
                while (this.modElementStartOffset < this.lineStartOffset) {
                    --this.lineIndex;
                    this.fetchLineElement();
                    TrailingWhitespaceRemoveProcessor.removeWhitespaceOnLine(this.lineStartOffset, this.lineLastOffset, this.caretRelativeOffset, this.lineIndex, this.caretLineIndex, this.doc, this.docText);
                }
            }
        } else {
            this.lineIndex = this.lineRootElement.getElementCount() - 1;
            while (this.lineIndex >= 0) {
                this.fetchLineElement();
                TrailingWhitespaceRemoveProcessor.removeWhitespaceOnLine(this.lineStartOffset, this.lineLastOffset, this.caretRelativeOffset, this.lineIndex, this.caretLineIndex, this.doc, this.docText);
                --this.lineIndex;
            }
        }
    }

    private boolean fetchPreviousNonEmptyRegion() {
        while (--this.modElementIndex >= 0) {
            Element modElement = this.modRootElement.getElement(this.modElementIndex);
            this.modElementStartOffset = modElement.getStartOffset();
            this.modElementEndOffset = modElement.getEndOffset();
            if (this.modElementStartOffset >= this.modElementEndOffset) continue;
            return true;
        }
        return false;
    }

    private void fetchLineElement() {
        Element lineElement = this.lineRootElement.getElement(this.lineIndex);
        this.lineStartOffset = lineElement.getStartOffset();
        this.lineLastOffset = lineElement.getEndOffset() - 1;
    }
}

