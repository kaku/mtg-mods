/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.lib2.EditorImplementation;

public final class ComponentUtils {
    private static final Logger LOG = Logger.getLogger(Logger.class.getName());
    private static final String STATUS_BAR_TEXT_PROPERTY = "statusBarText";

    public static boolean isGuardedException(BadLocationException exc) {
        return exc.getClass().getName().equals("org.netbeans.editor.GuardedException");
    }

    public static void returnFocus() {
        JTextComponent c = EditorRegistry.lastFocusedComponent();
        if (c != null) {
            ComponentUtils.requestFocus(c);
        }
    }

    public static void requestFocus(JTextComponent c) {
        if (c != null && !EditorImplementation.getDefault().activateComponent(c)) {
            Frame f = ComponentUtils.getParentFrame(c);
            if (f != null) {
                f.requestFocus();
            }
            c.requestFocus();
        }
    }

    public static void setStatusText(JTextComponent c, String text) {
        try {
            Object editorUI = ComponentUtils.getEditorUI(c);
            if (editorUI == null) {
                c.putClientProperty("statusBarText", text);
                return;
            }
            Method getSbMethod = editorUI.getClass().getMethod("getStatusBar", new Class[0]);
            Object statusBar = getSbMethod.invoke(editorUI, new Object[0]);
            Method setTextMethod = statusBar.getClass().getMethod("setText", String.class, String.class);
            setTextMethod.invoke(statusBar, "main", text);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static void setStatusText(JTextComponent c, String text, int importance) {
        try {
            Object editorUI = ComponentUtils.getEditorUI(c);
            if (editorUI == null) {
                c.putClientProperty("statusBarText", text);
                return;
            }
            Method getSbMethod = editorUI.getClass().getMethod("getStatusBar", new Class[0]);
            Object statusBar = getSbMethod.invoke(editorUI, new Object[0]);
            Method setTextMethod = statusBar.getClass().getMethod("setText", String.class, Integer.TYPE);
            setTextMethod.invoke(statusBar, text, importance);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static void setStatusBoldText(JTextComponent c, String text) {
        try {
            Object editorUI = ComponentUtils.getEditorUI(c);
            if (editorUI == null) {
                c.putClientProperty("statusBarText", text);
                return;
            }
            Method getSbMethod = editorUI.getClass().getMethod("getStatusBar", new Class[0]);
            Object statusBar = getSbMethod.invoke(editorUI, new Object[0]);
            Method setTextMethod = statusBar.getClass().getMethod("setBoldText", String.class, String.class);
            setTextMethod.invoke(statusBar, "main", text);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static String getStatusText(JTextComponent c) {
        try {
            Object editorUI = ComponentUtils.getEditorUI(c);
            if (editorUI == null) {
                return "";
            }
            Method getSbMethod = editorUI.getClass().getMethod("getStatusBar", new Class[0]);
            Object statusBar = getSbMethod.invoke(editorUI, new Object[0]);
            Method getTextMethod = statusBar.getClass().getMethod("getText", String.class);
            return (String)getTextMethod.invoke(statusBar, "main");
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
            return "";
        }
    }

    public static void clearStatusText(JTextComponent c) {
        ComponentUtils.setStatusText(c, "");
    }

    private static Object getEditorUI(JTextComponent c) throws Exception {
        TextUI textUI = c.getUI();
        Method getEuiMethod = null;
        try {
            getEuiMethod = textUI.getClass().getMethod("getEditorUI", new Class[0]);
        }
        catch (NoSuchMethodException nsme) {
            LOG.log(Level.INFO, nsme.getMessage(), nsme);
        }
        if (getEuiMethod != null) {
            return getEuiMethod.invoke(textUI, new Object[0]);
        }
        return null;
    }

    private static Frame getParentFrame(Component c) {
        do {
            if (!((c = c.getParent()) instanceof Frame)) continue;
            return (Frame)c;
        } while (c != null);
        return null;
    }

    private ComponentUtils() {
    }
}

