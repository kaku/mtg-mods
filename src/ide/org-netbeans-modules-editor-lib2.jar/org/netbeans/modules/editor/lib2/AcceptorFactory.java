/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

import org.netbeans.modules.editor.lib2.Acceptor;

public class AcceptorFactory {
    public static final Acceptor TRUE = new Fixed(true);
    public static final Acceptor FALSE = new Fixed(false);
    public static final Acceptor NL = new Char('\n');
    public static final Acceptor SPACE_NL = new TwoChar(' ', '\n');
    public static final Acceptor WHITESPACE = new Acceptor(){

        @Override
        public final boolean accept(char ch) {
            return Character.isWhitespace(ch);
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.WHITESPACE";
        }
    };
    public static final Acceptor LETTER_DIGIT = new Acceptor(){

        @Override
        public final boolean accept(char ch) {
            return Character.isLetterOrDigit(ch);
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.LETTER_DIGIT";
        }
    };
    public static final Acceptor UNICODE_IDENTIFIER = new Acceptor(){

        @Override
        public final boolean accept(char ch) {
            return Character.isUnicodeIdentifierPart(ch);
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.UNICODE_IDENTIFIER";
        }
    };
    public static final Acceptor JAVA_IDENTIFIER = new Acceptor(){

        @Override
        public final boolean accept(char ch) {
            return Character.isJavaIdentifierPart(ch);
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.JAVA_IDENTIFIER";
        }
    };
    public static final Acceptor NON_JAVA_IDENTIFIER = new Acceptor(){

        @Override
        public final boolean accept(char ch) {
            return !Character.isJavaIdentifierPart(ch);
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.NON_JAVA_IDENTIFIER";
        }
    };

    private static final class TwoChar
    implements Acceptor {
        private char hit1;
        private char hit2;

        public TwoChar(char hit1, char hit2) {
            this.hit1 = hit1;
            this.hit2 = hit2;
        }

        @Override
        public final boolean accept(char ch) {
            return ch == this.hit1 || ch == this.hit2;
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.TwoChar@" + Integer.toHexString(System.identityHashCode(this)) + " : hit1 = " + this.hit1 + ", hit2 = " + this.hit2;
        }
    }

    private static final class Char
    implements Acceptor {
        private char hit;

        public Char(char hit) {
            this.hit = hit;
        }

        @Override
        public final boolean accept(char ch) {
            return ch == this.hit;
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.Char@" + Integer.toHexString(System.identityHashCode(this)) + " : hit = " + this.hit;
        }
    }

    private static final class Fixed
    implements Acceptor {
        private boolean state;

        public Fixed(boolean state) {
            this.state = state;
        }

        @Override
        public final boolean accept(char ch) {
            return this.state;
        }

        public String toString() {
            return "o.n.m.e.lib2.AcceptorFactory.Fixed@" + Integer.toHexString(System.identityHashCode(this)) + " : state = " + this.state;
        }
    }

}

