/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewPart;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.TextLayoutBreakInfo;
import org.netbeans.modules.editor.lib2.view.TextLayoutUtils;
import org.netbeans.modules.editor.lib2.view.ViewRenderContext;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public class HighlightsView
extends EditorView {
    private static final Logger LOG = Logger.getLogger(HighlightsView.class.getName());
    private int rawEndOffset;
    private int length;
    private final AttributeSet attributes;
    private TextLayout textLayout;
    private float width;
    private TextLayoutBreakInfo breakInfo;

    public HighlightsView(int length, AttributeSet attributes) {
        super(null);
        assert (length > 0);
        this.length = length;
        this.attributes = attributes;
    }

    @Override
    public float getPreferredSpan(int axis) {
        this.checkTextLayoutValid();
        if (axis == 0) {
            return this.getWidth();
        }
        EditorView.Parent parent = (EditorView.Parent)((Object)this.getParent());
        return parent != null ? parent.getViewRenderContext().getDefaultRowHeight() : 0.0f;
    }

    float getWidth() {
        return this.width;
    }

    @Override
    public int getRawEndOffset() {
        return this.rawEndOffset;
    }

    @Override
    public void setRawEndOffset(int rawOffset) {
        this.rawEndOffset = rawOffset;
    }

    @Override
    public int getLength() {
        return this.length;
    }

    @Override
    public int getStartOffset() {
        return this.getEndOffset() - this.getLength();
    }

    @Override
    public int getEndOffset() {
        EditorView.Parent parent = (EditorView.Parent)((Object)this.getParent());
        return parent != null ? parent.getViewEndOffset(this.rawEndOffset) : this.rawEndOffset;
    }

    @Override
    public Document getDocument() {
        View parent = this.getParent();
        return parent != null ? parent.getDocument() : null;
    }

    @Override
    public AttributeSet getAttributes() {
        return this.attributes;
    }

    TextLayout getTextLayout() {
        return this.textLayout;
    }

    void setTextLayout(TextLayout textLayout, float width) {
        this.textLayout = textLayout;
        this.width = width;
    }

    TextLayoutBreakInfo getBreakInfo() {
        return this.breakInfo;
    }

    public void setBreakInfo(TextLayoutBreakInfo breakInfo) {
        this.breakInfo = breakInfo;
    }

    TextLayout createPartTextLayout(int shift, int length) {
        TextLayout partTextLayout;
        this.checkTextLayoutValid();
        if (this.breakInfo == null) {
            this.breakInfo = new TextLayoutBreakInfo(this.textLayout.getCharacterCount());
        }
        if ((partTextLayout = this.breakInfo.findPartTextLayout(shift, length)) == null) {
            DocumentView docView = this.getDocumentView();
            Document doc = docView.getDocument();
            CharSequence docText = DocumentUtilities.getText((Document)doc);
            int startOffset = this.getStartOffset();
            String text = docText.subSequence(startOffset + shift, startOffset + shift + length).toString();
            if (docView.op.isNonPrintableCharactersVisible()) {
                text = text.replace(' ', '\u00b7');
            }
            AttributeSet attrs = ViewUtils.getFirstAttributes(this.getAttributes());
            Font font = ViewUtils.getFont(attrs, docView.op.getDefaultFont());
            partTextLayout = docView.op.createTextLayout(text, font);
            this.breakInfo.add(shift, length, partTextLayout);
        }
        return partTextLayout;
    }

    ParagraphView getParagraphView() {
        return (ParagraphView)this.getParent();
    }

    DocumentView getDocumentView() {
        ParagraphView paragraphView = this.getParagraphView();
        return paragraphView != null ? paragraphView.getDocumentView() : null;
    }

    @Override
    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        this.checkTextLayoutValid();
        int relOffset = Math.max(0, offset - this.getStartOffset());
        Shape ret = HighlightsViewUtils.indexToView(this.textLayout, null, relOffset, bias, this.getLength(), alloc);
        return ret;
    }

    @Override
    public int viewToModelChecked(double x, double y, Shape hViewAlloc, Position.Bias[] biasReturn) {
        this.checkTextLayoutValid();
        int offset = this.getStartOffset() + HighlightsViewUtils.viewToIndex(this.textLayout, x, hViewAlloc, biasReturn);
        return offset;
    }

    @Override
    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        this.checkTextLayoutValid();
        int startOffset = this.getStartOffset();
        return HighlightsViewUtils.getNextVisualPosition(offset, bias, alloc, direction, biasRet, this.textLayout, startOffset, startOffset, this.getLength(), this.getDocumentView());
    }

    @Override
    public void paint(Graphics2D g, Shape hViewAlloc, Rectangle clipBounds) {
        this.checkTextLayoutValid();
        int viewStartOffset = this.getStartOffset();
        DocumentView docView = this.getDocumentView();
        HighlightsViewUtils.paintHiglighted(g, hViewAlloc, clipBounds, docView, this, viewStartOffset, false, this.textLayout, viewStartOffset, 0, this.getLength());
    }

    @Override
    public View breakView(int axis, int offset, float x, float len) {
        this /* !! */ .checkTextLayoutValid();
        View part = HighlightsViewUtils.breakView(axis, offset, x, len, this /* !! */ , 0, this /* !! */ .getLength(), this /* !! */ .textLayout);
        return part != null ? part : this /* !! */ ;
    }

    @Override
    public View createFragment(int p0, int p1) {
        this.checkTextLayoutValid();
        int startOffset = this.getStartOffset();
        ViewUtils.checkFragmentBounds(p0, p1, startOffset, this.getLength());
        return new HighlightsViewPart(this, p0 - startOffset, p1 - p0);
    }

    private void checkTextLayoutValid() {
        if (!$assertionsDisabled && this.textLayout == null) {
            DocumentView docView;
            throw new AssertionError((Object)("TextLayout is null in " + this + ((docView = this.getDocumentView()) != null ? new StringBuilder().append("\nDOC-VIEW:\n").append(docView.toStringDetailNeedsLock()).toString() : "")));
        }
    }

    @Override
    protected String getDumpName() {
        return "HV";
    }

    @Override
    protected StringBuilder appendViewInfo(StringBuilder sb, int indent, String xyInfo, int importantChildIndex) {
        super.appendViewInfo(sb, indent, xyInfo, importantChildIndex);
        sb.append(" TL=");
        if (this.textLayout == null) {
            sb.append("<NULL>");
        } else {
            sb.append(TextLayoutUtils.toStringShort(this.textLayout));
        }
        return sb;
    }

    public String toString() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }
}

