/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.TokenHierarchy
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.ArrayList;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.modules.editor.lib2.highlighting.BlockHighlighting;
import org.netbeans.modules.editor.lib2.highlighting.CaretBasedBlockHighlighting;
import org.netbeans.modules.editor.lib2.highlighting.ReadOnlyFilesHighlighting;
import org.netbeans.modules.editor.lib2.highlighting.SyntaxHighlighting;
import org.netbeans.modules.editor.lib2.highlighting.WhitespaceHighlighting;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

public class Factory
implements HighlightsLayerFactory {
    public static final String BLOCK_SEARCH_LAYER = "org.netbeans.modules.editor.lib2.highlighting.BlockHighlighting/BLOCK_SEARCH";
    public static final String INC_SEARCH_LAYER = "org.netbeans.modules.editor.lib2.highlighting.BlockHighlighting/INC_SEARCH";

    @Override
    public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
        ArrayList<HighlightsLayer> layers = new ArrayList<HighlightsLayer>();
        layers.add(HighlightsLayer.create("org.netbeans.modules.editor.lib2.highlighting.ReadOnlyFilesHighlighting", ZOrder.BOTTOM_RACK.forPosition(-1000), true, new ReadOnlyFilesHighlighting(context.getDocument())));
        layers.add(HighlightsLayer.create("org.netbeans.modules.editor.lib2.highlighting.CaretRowHighlighting", ZOrder.CARET_RACK, true, new CaretBasedBlockHighlighting.CaretRowHighlighting(context.getComponent())));
        layers.add(HighlightsLayer.create("org.netbeans.modules.editor.lib2.highlighting.BlockHighlighting/INC_SEARCH", ZOrder.SHOW_OFF_RACK.forPosition(300), true, new BlockHighlighting("org.netbeans.modules.editor.lib2.highlighting.BlockHighlighting/INC_SEARCH", context.getComponent())));
        layers.add(HighlightsLayer.create("org.netbeans.modules.editor.lib2.highlighting.TextSelectionHighlighting", ZOrder.SHOW_OFF_RACK.forPosition(500), true, new CaretBasedBlockHighlighting.TextSelectionHighlighting(context.getComponent())));
        if (TokenHierarchy.get((Document)context.getDocument()) != null) {
            layers.add(HighlightsLayer.create("org.netbeans.modules.editor.lib2.highlighting.SyntaxHighlighting", ZOrder.SYNTAX_RACK, true, new SyntaxHighlighting(context.getDocument())));
        }
        layers.add(HighlightsLayer.create("org.netbeans.modules.editor.lib2.highlighting.WhitespaceHighlighting", ZOrder.CARET_RACK.forPosition(-100), true, new WhitespaceHighlighting(context.getComponent())));
        return layers.toArray(new HighlightsLayer[layers.size()]);
    }
}

