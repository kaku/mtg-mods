/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.editor.lib2.document.DefaultDocumentCharacterAcceptor;

public abstract class DocumentCharacterAcceptor {
    @NonNull
    public static DocumentCharacterAcceptor get(Document doc) {
        DocumentCharacterAcceptor acceptor = (DocumentCharacterAcceptor)doc.getProperty(DocumentCharacterAcceptor.class);
        if (acceptor == null) {
            acceptor = DefaultDocumentCharacterAcceptor.INSTANCE;
        }
        return acceptor;
    }

    public abstract boolean isIdentifier(char var1);

    public abstract boolean isWhitespace(char var1);
}

