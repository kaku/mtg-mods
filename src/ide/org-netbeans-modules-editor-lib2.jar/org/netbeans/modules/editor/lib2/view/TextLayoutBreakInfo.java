/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.font.TextLayout;

public class TextLayoutBreakInfo {
    private static final int MIN_ITEMS = 4;
    private static final int MAX_ITEMS = 32;
    private static final int TYPICAL_LINE_LENGTH = 80;
    private final Item[] items;
    private int itemsCount;

    TextLayoutBreakInfo(int textLayoutLength) {
        int itemsLength = Math.max(4, Math.min(32, textLayoutLength / 80 + 2 << 1));
        this.items = new Item[itemsLength];
    }

    TextLayout findPartTextLayout(int shift, int length) {
        for (int i = 0; i < this.itemsCount; ++i) {
            Item item = this.items[i];
            if (item.shift != shift || item.length != length) continue;
            if (i != 0) {
                System.arraycopy(this.items, 0, this.items, 1, i);
                this.items[0] = item;
            }
            return item.partTextLayout;
        }
        return null;
    }

    void add(int shift, int length, TextLayout partTextLayout) {
        if (this.itemsCount < this.items.length) {
            ++this.itemsCount;
        }
        if (this.itemsCount > 1) {
            System.arraycopy(this.items, 0, this.items, 1, this.itemsCount - 1);
        }
        this.items[0] = new Item(shift, length, partTextLayout);
    }

    private static final class Item {
        final int shift;
        final int length;
        final TextLayout partTextLayout;

        public Item(int shift, int length, TextLayout partTextLayout) {
            this.shift = shift;
            this.length = length;
            this.partTextLayout = partTextLayout;
        }

        public boolean equals(Object obj) {
            return super.equals(obj);
        }
    }

}

