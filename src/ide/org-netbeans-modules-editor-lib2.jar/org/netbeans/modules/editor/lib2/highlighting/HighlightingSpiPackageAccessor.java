/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.TopologicalSortException
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.Collection;
import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsLayerAccessor;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.openide.util.TopologicalSortException;

public abstract class HighlightingSpiPackageAccessor {
    private static HighlightingSpiPackageAccessor ACCESSOR = null;

    public static synchronized void register(HighlightingSpiPackageAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized HighlightingSpiPackageAccessor get() {
        try {
            Class clazz = Class.forName(HighlightsLayer.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected HighlightingSpiPackageAccessor() {
    }

    public abstract HighlightsLayerFactory.Context createFactoryContext(Document var1, JTextComponent var2);

    public abstract List<? extends HighlightsLayer> sort(Collection<? extends HighlightsLayer> var1) throws TopologicalSortException;

    public abstract HighlightsLayerAccessor getHighlightsLayerAccessor(HighlightsLayer var1);

    public abstract int getZOrderRack(ZOrder var1);
}

