/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

public enum LineWrapType {
    NONE("none"),
    CHARACTER_BOUND("chars"),
    WORD_BOUND("words");
    
    private final String settingValue;

    private LineWrapType(String settingValue) {
        this.settingValue = settingValue;
    }

    public static LineWrapType fromSettingValue(String settingValue) {
        if (settingValue != null) {
            for (LineWrapType lwt : LineWrapType.values()) {
                if (!lwt.settingValue.equals(settingValue)) continue;
                return lwt;
            }
        }
        return null;
    }
}

