/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.lang.ref.WeakReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.StyleConstants;
import org.netbeans.modules.editor.lib2.highlighting.CheckedHighlightsSequence;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsSequenceEx;
import org.netbeans.modules.editor.lib2.highlighting.MultiLayerContainer;
import org.netbeans.modules.editor.lib2.highlighting.OffsetGapList;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.WeakListeners;

public final class CompoundHighlightsContainer
extends AbstractHighlightsContainer
implements MultiLayerContainer {
    private static final Logger LOG = Logger.getLogger(CompoundHighlightsContainer.class.getName());
    private static final int MIN_CACHE_SIZE = 128;
    private Document doc;
    private HighlightsContainer[] layers;
    private boolean[] blacklisted;
    private long version = 0;
    private final Object LOCK = new String("CompoundHighlightsContainer.LOCK");
    private final LayerListener listener;
    private OffsetsBag cache;
    private boolean cacheObsolete;
    private CacheBoundaries cacheBoundaries;
    private final boolean assertions;

    public CompoundHighlightsContainer() {
        this(null, null);
    }

    public CompoundHighlightsContainer(Document doc, HighlightsContainer[] layers) {
        this.listener = new LayerListener(this);
        this.setLayers(doc, layers);
        boolean a = false;
        if (!$assertionsDisabled) {
            a = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        this.assertions = a;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        assert (0 <= startOffset);
        assert (0 <= endOffset);
        assert (startOffset <= endOffset);
        Object object = this.LOCK;
        synchronized (object) {
            OffsetsBag bag;
            block28 : {
                if (this.doc == null || this.layers == null || this.layers.length == 0 || startOffset < 0 || endOffset < 0 || startOffset >= endOffset || startOffset > this.doc.getLength()) {
                    return HighlightsSequence.EMPTY;
                }
                int[] update = null;
                int lowest = -1;
                int highest = -1;
                if (this.cacheObsolete) {
                    this.cacheObsolete = false;
                    this.discardCache();
                } else {
                    lowest = this.cacheBoundaries.getLowerBoundary();
                    highest = this.cacheBoundaries.getUpperBoundary();
                    if (lowest == -1 || highest == -1) {
                        this.discardCache();
                    } else if (endOffset <= highest && startOffset < lowest) {
                        update = new int[]{CompoundHighlightsContainer.expandBelow(startOffset, lowest), lowest};
                    } else if (startOffset >= lowest && endOffset > highest) {
                        update = new int[]{highest, CompoundHighlightsContainer.expandAbove(highest, endOffset)};
                    } else if (startOffset < lowest && endOffset > highest) {
                        update = new int[]{CompoundHighlightsContainer.expandBelow(startOffset, lowest), lowest, highest, CompoundHighlightsContainer.expandAbove(highest, endOffset)};
                    } else if (startOffset < lowest || endOffset > highest) {
                        this.discardCache();
                    }
                }
                block3 : do {
                    int i;
                    if ((bag = this.cache) == null) {
                        this.cache = bag = new OffsetsBag(this.doc, true);
                        highest = -1;
                        lowest = -1;
                        update = new int[]{CompoundHighlightsContainer.expandBelow(startOffset, endOffset), CompoundHighlightsContainer.expandAbove(startOffset, endOffset)};
                    }
                    if (update == null) break block28;
                    for (i = 0; i < update.length / 2; ++i) {
                        if (update[2 * i] <= this.doc.getLength()) continue;
                        if (this.assertions && LOG.isLoggable(Level.WARNING)) {
                            String msg = "Inconsistent cache update boundaries: startOffset=" + startOffset + ", endOffset=" + endOffset + ", lowest=" + lowest + ", highest=" + highest + ", doc.length=" + this.doc.getLength() + ", update[]=" + update;
                            LOG.log(Level.WARNING, null, new Throwable(msg));
                        }
                        update = new int[]{0, Integer.MAX_VALUE};
                        break;
                    }
                    for (i = 0; i < update.length / 2; ++i) {
                        if (update[2 * i + 1] >= this.doc.getLength()) {
                            update[2 * i + 1] = Integer.MAX_VALUE;
                        }
                        if (!this.updateCache(update[2 * i], update[2 * i + 1], bag)) {
                            this.discardCache();
                            continue block3;
                        }
                        if (update[2 * i + 1] == Integer.MAX_VALUE) break block3;
                    }
                    break;
                } while (true);
                if (lowest == -1 || highest == -1) {
                    this.cacheBoundaries.setBoundaries(update[0], update[update.length - 1]);
                } else {
                    this.cacheBoundaries.setBoundaries(Math.min(lowest, update[0]), Math.max(highest, update[update.length - 1]));
                }
                if (LOG.isLoggable(Level.FINE)) {
                    int lower = this.cacheBoundaries.getLowerBoundary();
                    int upper = this.cacheBoundaries.getUpperBoundary();
                    LOG.fine("Cache boundaries: <" + (lower == -1 ? "-" : Integer.valueOf(lower)) + ", " + (upper == -1 ? "-" : Integer.valueOf(upper)) + "> " + "when asked for <" + startOffset + ", " + endOffset + ">");
                }
            }
            return new Seq(this.version, bag.getHighlights(startOffset, endOffset));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public HighlightsContainer[] getLayers() {
        Object object = this.LOCK;
        synchronized (object) {
            return this.layers;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setLayers(Document doc, HighlightsContainer[] layers) {
        Document docForEvents = null;
        Object object = this.LOCK;
        synchronized (object) {
            int i;
            if (doc == null && !$assertionsDisabled && layers != null) {
                throw new AssertionError((Object)"If doc is null the layers must be null too.");
            }
            Document document = docForEvents = doc != null ? doc : this.doc;
            if (this.layers != null) {
                for (i = 0; i < this.layers.length; ++i) {
                    this.layers[i].removeHighlightsChangeListener(this.listener);
                }
            }
            this.doc = doc;
            this.layers = layers;
            this.blacklisted = layers == null ? null : new boolean[layers.length];
            this.cacheObsolete = true;
            this.cacheBoundaries = doc == null ? null : new CacheBoundaries(doc);
            this.increaseVersion();
            if (this.layers != null) {
                for (i = 0; i < this.layers.length; ++i) {
                    this.layers[i].addHighlightsChangeListener(this.listener);
                }
            }
        }
        if (docForEvents != null) {
            docForEvents.render(new Runnable(){

                @Override
                public void run() {
                    CompoundHighlightsContainer.this.fireHighlightsChange(0, Integer.MAX_VALUE);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void layerChanged(HighlightsContainer layer, final int changeStartOffset, final int changeEndOffset) {
        Document docForEvents = null;
        Object object = this.LOCK;
        synchronized (object) {
            LOG.log(Level.FINE, "Cache obsoleted by changes in {0}", layer);
            this.cacheObsolete = true;
            this.increaseVersion();
            docForEvents = this.doc;
        }
        if (docForEvents != null) {
            docForEvents.render(new Runnable(){

                @Override
                public void run() {
                    CompoundHighlightsContainer.this.fireHighlightsChange(changeStartOffset, changeEndOffset);
                }
            });
        }
    }

    private boolean updateCache(int startOffset, int endOffset, OffsetsBag bag) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Updating cache: <" + startOffset + ", " + endOffset + ">");
        }
        for (int i = 0; i < this.layers.length; ++i) {
            if (this.blacklisted[i]) continue;
            try {
                CheckedHighlightsSequence checked = new CheckedHighlightsSequence(this.layers[i].getHighlights(startOffset, endOffset), startOffset, endOffset);
                if (LOG.isLoggable(Level.FINE)) {
                    checked.setContainerDebugId("CHC.Layer[" + i + "]=" + this.layers[i]);
                }
                bag.addAllHighlights(checked);
                if (bag != this.cache) {
                    return false;
                }
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine(CompoundHighlightsContainer.dumpLayerHighlights(this.layers[i], startOffset, endOffset));
                continue;
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable t) {
                this.blacklisted[i] = true;
                LOG.log(Level.WARNING, "The layer failed to supply highlights: " + this.layers[i], t);
            }
        }
        return true;
    }

    private void increaseVersion() {
        ++this.version;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("CHC@" + Integer.toHexString(System.identityHashCode(this)) + ", OB@" + (this.cache == null ? "null" : Integer.toHexString(System.identityHashCode(this.cache))) + ", doc@" + Integer.toHexString(System.identityHashCode(this.doc)) + " version=" + this.version);
        }
    }

    private void discardCache() {
        if (this.cache != null) {
            this.cache.discard();
        }
        this.cache = null;
    }

    private static int expandBelow(int startOffset, int endOffset) {
        if (startOffset == 0 || endOffset == Integer.MAX_VALUE) {
            return startOffset;
        }
        int expandBy = Math.max(endOffset - startOffset >> 2, 128);
        return Math.max(startOffset - expandBy, 0);
    }

    private static int expandAbove(int startOffset, int endOffset) {
        if (endOffset == Integer.MAX_VALUE) {
            return endOffset;
        }
        int expandBy = Math.max(endOffset - startOffset >> 2, 128);
        return endOffset + expandBy;
    }

    private static String dumpLayerHighlights(HighlightsContainer layer, int startOffset, int endOffset) {
        StringBuilder sb = new StringBuilder();
        sb.append("Highlights in ").append(layer).append(": {\n");
        HighlightsSequence seq = layer.getHighlights(startOffset, endOffset);
        while (seq.moveNext()) {
            sb.append("  ");
            CompoundHighlightsContainer.dumpHighlight(seq, sb);
            sb.append("\n");
        }
        sb.append("} End of Highlights in ").append(layer);
        sb.append("\n");
        return sb.toString();
    }

    static StringBuilder dumpHighlight(HighlightsSequence seq, StringBuilder sb) {
        if (sb == null) {
            sb = new StringBuilder();
        }
        sb.append("<");
        sb.append(seq.getStartOffset());
        sb.append(", ");
        sb.append(seq.getEndOffset());
        sb.append(", ");
        sb.append(seq.getAttributes().getAttribute(StyleConstants.NameAttribute));
        sb.append(">");
        return sb;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        for (int i = 0; i < this.layers.length; ++i) {
            sb.append('[').append(i).append("]: ").append(this.layers[i]);
            sb.append('\n');
        }
        return sb.toString();
    }

    private static final class CacheBoundaries
    implements DocumentListener {
        private final OffsetGapList<OffsetGapList.Offset> boundaries = new OffsetGapList(false);
        private final Document doc;

        public CacheBoundaries(Document doc) {
            this.doc = doc;
            this.doc.addDocumentListener(WeakListeners.document((DocumentListener)this, (Object)this.doc));
        }

        public int getLowerBoundary() {
            if (this.boundaries.size() == 2) {
                OffsetGapList.Offset lower = (OffsetGapList.Offset)this.boundaries.get(0);
                int lowerOffset = lower.getOffset();
                return lowerOffset >= this.doc.getLength() ? -1 : lowerOffset;
            }
            return -1;
        }

        public int getUpperBoundary() {
            if (this.boundaries.size() == 2) {
                OffsetGapList.Offset higher = (OffsetGapList.Offset)this.boundaries.get(1);
                int higherOffset = higher.getOffset();
                return higherOffset >= this.doc.getLength() ? Integer.MAX_VALUE : higherOffset;
            }
            return -1;
        }

        public void setBoundaries(int lowerOffset, int higherOffset) {
            this.boundaries.clear();
            this.boundaries.add(new OffsetGapList.Offset(lowerOffset));
            this.boundaries.add(new OffsetGapList.Offset(Math.min(higherOffset, this.doc.getLength() + 1)));
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.boundaries.defaultInsertUpdate(e.getOffset(), e.getLength());
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.boundaries.defaultRemoveUpdate(e.getOffset(), e.getLength());
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }

    private final class Seq
    implements HighlightsSequenceEx {
        private HighlightsSequence seq;
        private long version;
        private int startOffset;
        private int endOffset;
        private AttributeSet attibutes;

        public Seq(long version, HighlightsSequence seq) {
            this.startOffset = -1;
            this.endOffset = -1;
            this.attibutes = null;
            this.version = version;
            this.seq = seq;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean moveNext() {
            Object object = CompoundHighlightsContainer.this.LOCK;
            synchronized (object) {
                if (this.checkVersion() && this.seq.moveNext()) {
                    this.startOffset = this.seq.getStartOffset();
                    this.endOffset = this.seq.getEndOffset();
                    this.attibutes = this.seq.getAttributes();
                    return true;
                }
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getStartOffset() {
            Object object = CompoundHighlightsContainer.this.LOCK;
            synchronized (object) {
                assert (this.startOffset != -1);
                return this.startOffset;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getEndOffset() {
            Object object = CompoundHighlightsContainer.this.LOCK;
            synchronized (object) {
                assert (this.endOffset != -1);
                return this.endOffset;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public AttributeSet getAttributes() {
            Object object = CompoundHighlightsContainer.this.LOCK;
            synchronized (object) {
                assert (this.attibutes != null);
                return this.attibutes;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean isStale() {
            Object object = CompoundHighlightsContainer.this.LOCK;
            synchronized (object) {
                return !this.checkVersion();
            }
        }

        private boolean checkVersion() {
            return this.version == CompoundHighlightsContainer.this.version;
        }
    }

    private static final class LayerListener
    implements HighlightsChangeListener {
        private WeakReference<CompoundHighlightsContainer> ref;

        public LayerListener(CompoundHighlightsContainer container) {
            this.ref = new WeakReference<CompoundHighlightsContainer>(container);
        }

        @Override
        public void highlightChanged(HighlightsChangeEvent event) {
            CompoundHighlightsContainer container = this.ref.get();
            if (container != null) {
                container.layerChanged((HighlightsContainer)event.getSource(), event.getStartOffset(), event.getEndOffset());
            }
        }
    }

}

