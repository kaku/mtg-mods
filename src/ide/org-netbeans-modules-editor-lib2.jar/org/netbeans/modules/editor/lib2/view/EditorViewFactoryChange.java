/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class EditorViewFactoryChange {
    private final int startOffset;
    private final int endOffset;
    private final Type type;

    public static EditorViewFactoryChange create(int startOffset, int endOffset, Type type) {
        return new EditorViewFactoryChange(startOffset, endOffset, type);
    }

    public static List<EditorViewFactoryChange> createList(int startOffset, int endOffset, Type type) {
        return Collections.singletonList(EditorViewFactoryChange.create(startOffset, endOffset, type));
    }

    public static /* varargs */ List<EditorViewFactoryChange> createList(EditorViewFactoryChange ... changes) {
        return Arrays.asList(changes);
    }

    private EditorViewFactoryChange(int startOffset, int endOffset, Type type) {
        this.startOffset = startOffset;
        this.endOffset = endOffset;
        this.type = type;
    }

    int getStartOffset() {
        return this.startOffset;
    }

    int getEndOffset() {
        return this.endOffset;
    }

    Type getType() {
        return this.type;
    }

    public static enum Type {
        CHARACTER_CHANGE,
        PARAGRAPH_CHANGE,
        REBUILD;
        

        private Type() {
        }
    }

}

