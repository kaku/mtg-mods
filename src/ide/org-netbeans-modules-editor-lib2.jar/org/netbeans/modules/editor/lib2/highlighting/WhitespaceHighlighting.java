/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public class WhitespaceHighlighting
extends AbstractHighlightsContainer
implements DocumentListener,
LookupListener {
    static final int FIRST_CHAR = 0;
    static final int WS_BEFORE = 1;
    static final int NON_WS_BEFORE = 2;
    public static final String LAYER_TYPE_ID = "org.netbeans.modules.editor.lib2.highlighting.WhitespaceHighlighting";
    private static final String INDENT_ATTRS_FCS_NAME = "indent-whitespace";
    private static final String TRAILING_ATTRS_FCS_NAME = "trailing-whitespace";
    private final JTextComponent component;
    private final Document doc;
    private final CharSequence docText;
    private AttributeSet indentAttrs;
    private AttributeSet trailingAttrs;
    private boolean active;
    private boolean customAttrs;
    private Lookup.Result<FontColorSettings> result;

    WhitespaceHighlighting(JTextComponent c) {
        this.component = c;
        this.doc = c.getDocument();
        this.docText = DocumentUtilities.getText((Document)this.doc);
        this.doc.addDocumentListener(this);
        String mimeType = (String)this.doc.getProperty("mimeType");
        if (mimeType == null) {
            mimeType = "";
        }
        Lookup lookup = MimeLookup.getLookup((String)mimeType);
        this.result = lookup.lookupResult(FontColorSettings.class);
        this.result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.result));
        this.resultChanged(null);
    }

    @Override
    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        return this.active ? new HS(startOffset, endOffset) : HighlightsSequence.EMPTY;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        if (this.active) {
            int offset = e.getOffset();
            int len = e.getLength();
            int startChangeOffset = -1;
            int endChangeOffset = offset + len;
            boolean checkForWSAboveModText = false;
            for (int i = offset - 1; i >= 0; --i) {
                char ch = this.docText.charAt(i);
                if (ch == '\n') {
                    startChangeOffset = i + 1;
                    break;
                }
                if (Character.isWhitespace(ch)) continue;
                startChangeOffset = i + 1;
                break;
            }
            if (startChangeOffset == -1) {
                startChangeOffset = 0;
                checkForWSAboveModText = true;
            }
            int docTextLen = this.docText.length();
            for (int i2 = endChangeOffset; i2 < docTextLen; ++i2) {
                char ch = this.docText.charAt(i2);
                if (ch == '\n') {
                    endChangeOffset = i2;
                    break;
                }
                if (Character.isWhitespace(ch)) continue;
                endChangeOffset = i2;
                break;
            }
            this.fireHighlightsChange(startChangeOffset, endChangeOffset);
        }
    }

    private int lineStartOffset(int offset) {
        while (offset > 0) {
            char ch;
            if ((ch = this.docText.charAt(--offset)) != '\n') continue;
            return offset + 1;
        }
        return 0;
    }

    private int beforeOffsetState(int offset) {
        int i;
        char ch;
        for (i = offset - 1; i >= 0 && (ch = this.docText.charAt(i)) != '\n'; --i) {
            if (Character.isWhitespace(ch)) continue;
            return 2;
        }
        return i == offset - 1 ? 0 : 1;
    }

    private boolean isWSTillFirstNL(CharSequence text) {
        char ch;
        for (int i = 0; i < text.length() && (ch = text.charAt(i)) != '\n'; ++i) {
            if (Character.isWhitespace(ch)) continue;
            return false;
        }
        return true;
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if (this.active) {
            int offset = e.getOffset();
            int len = e.getLength();
            int startChangeOffset = -1;
            int endChangeOffset = offset;
            for (int i = offset - 1; i >= 0; --i) {
                char ch = this.docText.charAt(i);
                if (ch == '\n') {
                    startChangeOffset = i + 1;
                    break;
                }
                if (Character.isWhitespace(ch)) continue;
                startChangeOffset = i + 1;
                break;
            }
            if (startChangeOffset == -1) {
                startChangeOffset = 0;
            }
            int docTextLen = this.docText.length();
            for (int i2 = offset; i2 < docTextLen; ++i2) {
                char ch = this.docText.charAt(i2);
                if (ch == '\n') {
                    endChangeOffset = i2;
                    break;
                }
                if (Character.isWhitespace(ch)) continue;
                endChangeOffset = i2;
                break;
            }
            this.fireHighlightsChange(startChangeOffset, endChangeOffset);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void assignAttrs(AttributeSet indentAttrs, AttributeSet trailingAttrs) {
        WhitespaceHighlighting whitespaceHighlighting = this;
        synchronized (whitespaceHighlighting) {
            this.indentAttrs = indentAttrs;
            this.trailingAttrs = trailingAttrs;
            this.active = indentAttrs != null && trailingAttrs != null;
        }
    }

    void testInitEnv(AttributeSet indentAttrs, AttributeSet trailingAttrs) {
        this.customAttrs = true;
        this.assignAttrs(indentAttrs, trailingAttrs);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resultChanged(LookupEvent ev) {
        FontColorSettings fcs = (FontColorSettings)this.result.allInstances().iterator().next();
        WhitespaceHighlighting whitespaceHighlighting = this;
        synchronized (whitespaceHighlighting) {
            if (!this.customAttrs) {
                this.assignAttrs(fcs.getFontColors("indent-whitespace"), fcs.getFontColors("trailing-whitespace"));
            }
        }
    }

    private final class HS
    implements HighlightsSequence {
        private int startOffset;
        private int endOffset;
        private int hltStartOffset;
        private int hltEndOffset;
        private AttributeSet hltAttrs;
        private int state;

        public HS(int startOffset, int endOffset) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.hltStartOffset = -1;
            this.hltEndOffset = startOffset;
        }

        @Override
        public boolean moveNext() {
            int docTextLen = WhitespaceHighlighting.this.docText.length();
            if (this.endOffset > docTextLen) {
                this.endOffset = docTextLen;
            }
            if (this.hltEndOffset >= this.endOffset) {
                return false;
            }
            if (this.hltStartOffset == -1) {
                this.state = WhitespaceHighlighting.this.beforeOffsetState(this.startOffset);
            }
            this.hltStartOffset = this.hltEndOffset;
            do {
                block0 : switch (this.state) {
                    int i;
                    char ch;
                    case 0: 
                    case 1: {
                        for (i = this.hltStartOffset; i < this.endOffset; ++i) {
                            ch = WhitespaceHighlighting.this.docText.charAt(i);
                            if (ch == '\n') {
                                if (i != this.hltStartOffset) {
                                    this.hltEndOffset = i;
                                    this.hltAttrs = WhitespaceHighlighting.this.trailingAttrs;
                                    if (this.hltAttrs == null) break;
                                    return true;
                                }
                                this.hltEndOffset = i + 1;
                                this.state = 0;
                                break;
                            }
                            if (!Character.isWhitespace(ch)) {
                                this.state = 2;
                                this.hltEndOffset = i;
                                if (i == this.hltStartOffset) break;
                                this.hltAttrs = WhitespaceHighlighting.this.indentAttrs;
                                if (this.hltAttrs == null) break;
                                return true;
                            }
                            this.state = 1;
                        }
                        if (this.state != 1) break;
                        this.hltEndOffset = this.endOffset;
                        for (i = this.endOffset; i < docTextLen; ++i) {
                            ch = WhitespaceHighlighting.this.docText.charAt(i);
                            if (ch == '\n') {
                                this.hltAttrs = WhitespaceHighlighting.this.trailingAttrs;
                                if (this.hltAttrs == null) break block0;
                                return true;
                            }
                            if (Character.isWhitespace(ch)) continue;
                            this.hltAttrs = WhitespaceHighlighting.this.indentAttrs;
                            if (this.hltAttrs == null) break block0;
                            return true;
                        }
                        break;
                    }
                    case 2: {
                        for (i = this.hltStartOffset; i < docTextLen; ++i) {
                            ch = WhitespaceHighlighting.this.docText.charAt(i);
                            if (ch == '\n') {
                                if (i != this.hltStartOffset) {
                                    this.hltEndOffset = Math.min(i, this.endOffset);
                                    this.hltAttrs = WhitespaceHighlighting.this.trailingAttrs;
                                    if (this.hltAttrs == null) break block0;
                                    return true;
                                }
                                this.hltEndOffset = Math.min(i + 1, this.endOffset);
                                this.state = 0;
                                break block0;
                            }
                            if (!Character.isWhitespace(ch)) {
                                this.hltEndOffset = this.hltStartOffset = i + 1;
                                if (i < this.endOffset) continue;
                                break block0;
                            }
                            this.hltEndOffset = i + 1;
                        }
                        break;
                    }
                    default: {
                        throw new IllegalStateException("Unknown state=" + this.state);
                    }
                }
                this.hltStartOffset = this.hltEndOffset;
            } while (this.hltStartOffset < this.endOffset);
            return false;
        }

        @Override
        public int getStartOffset() {
            return this.hltStartOffset;
        }

        @Override
        public int getEndOffset() {
            return this.hltEndOffset;
        }

        @Override
        public AttributeSet getAttributes() {
            return this.hltAttrs;
        }
    }

}

