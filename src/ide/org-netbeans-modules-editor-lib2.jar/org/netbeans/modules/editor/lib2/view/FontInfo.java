/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.font.TextLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.JTextComponent;

final class FontInfo {
    private static final Logger LOG = Logger.getLogger(FontInfo.class.getName());
    final Font renderFont;
    float ascent;
    int ascentInt;
    float descent;
    float leading;
    int rowHeightInt;
    float charWidth;
    final float[] underlineAndStrike = new float[4];

    FontInfo(Font origFont, JTextComponent textComponent, FontRenderContext frc, float rowHeightCorrection, int textZoom) {
        this.renderFont = textZoom != 0 ? new Font(origFont.getName(), origFont.getStyle(), Math.max(origFont.getSize() + textZoom, 1)) : origFont;
        char defaultChar = 'A';
        String defaultCharText = String.valueOf(defaultChar);
        TextLayout defaultCharTextLayout = new TextLayout(defaultCharText, this.renderFont, frc);
        TextLayout rowHeightTextLayout = new TextLayout("A_|B", this.renderFont, frc);
        this.updateRowHeight(rowHeightTextLayout, rowHeightCorrection);
        this.charWidth = (float)Math.ceil(defaultCharTextLayout.getAdvance());
        LineMetrics lineMetrics = this.renderFont.getLineMetrics(defaultCharText, frc);
        this.underlineAndStrike[0] = lineMetrics.getUnderlineOffset() * rowHeightCorrection;
        this.underlineAndStrike[1] = lineMetrics.getUnderlineThickness();
        this.underlineAndStrike[2] = lineMetrics.getStrikethroughOffset() * rowHeightCorrection;
        this.underlineAndStrike[3] = lineMetrics.getStrikethroughThickness();
        if (LOG.isLoggable(Level.FINE)) {
            FontMetrics fm = textComponent.getFontMetrics(origFont);
            LOG.fine("Orig Font=" + origFont + "\n  " + this + ", charWidth=" + this.charWidth + ", textZoom=" + textZoom + "\n  rowHeightCorrection=" + rowHeightCorrection + ", underlineO/T=" + this.underlineAndStrike[0] + "/" + this.underlineAndStrike[1] + ", strikethroughO/T=" + this.underlineAndStrike[2] + "/" + this.underlineAndStrike[3] + "\n  FontMetrics (for comparison; without-RHC): fm-line-height=" + fm.getHeight() + ", fm-ascent,descent,leading=" + fm.getAscent() + "," + fm.getDescent() + "," + fm.getLeading() + "\n");
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.FINEST, "FontInfo creation stacktrace", new Exception());
            }
        }
    }

    boolean updateRowHeight(TextLayout textLayout, float rowHeightCorrection) {
        return this.updateRowHeight(textLayout.getAscent(), textLayout.getDescent(), textLayout.getLeading(), rowHeightCorrection);
    }

    boolean updateRowHeight(float textAscent, float textDescent, float textLeading, float rowHeightCorrection) {
        boolean change;
        int rowHeightNew;
        textDescent *= rowHeightCorrection;
        textLeading *= rowHeightCorrection;
        boolean bl = change = (textAscent *= rowHeightCorrection) > this.ascent;
        if (change) {
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("FontInfo: Ascent change from " + this.ascent + " to " + textAscent + " for " + this + "\n");
            }
            this.ascent = textAscent;
            if (this.ascent > (float)this.ascentInt) {
                this.ascentInt = (int)Math.ceil(this.ascent);
            }
        }
        if (textDescent > this.descent) {
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("FontInfo: Descent change from " + this.descent + " to " + textDescent + " for " + this + "\n");
            }
            this.descent = textDescent;
            change = true;
        }
        if (textLeading > this.leading) {
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("FontInfo: Leading change from " + this.leading + " to " + textLeading + " for " + this);
            }
            this.leading = textLeading;
            change = true;
        }
        if (change && (rowHeightNew = this.ascentInt + (int)Math.ceil(this.descent + this.leading)) > this.rowHeightInt) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("FontInfo: rowHeightInt change from " + this.rowHeightInt + " to " + rowHeightNew + " for " + this);
            }
            this.rowHeightInt = rowHeightNew;
            return true;
        }
        return false;
    }

    public String toString() {
        return "renderFont=" + this.renderFont + "\n  ascent(Int)=" + this.ascent + "(" + this.ascentInt + "), descent=" + this.descent + ", leading=" + this.leading + ", rowHeightInt=" + this.rowHeightInt;
    }
}

