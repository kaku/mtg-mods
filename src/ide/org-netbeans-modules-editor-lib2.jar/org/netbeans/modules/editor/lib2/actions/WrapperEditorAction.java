/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.actions;

import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.AbstractEditorAction;

public class WrapperEditorAction
extends AbstractEditorAction {
    public static WrapperEditorAction create(Map<String, ?> attrs) {
        return new WrapperEditorAction(attrs);
    }

    WrapperEditorAction(Map<String, ?> attrs) {
        super(attrs);
    }

    @Override
    protected void actionPerformed(ActionEvent evt, JTextComponent component) {
    }
}

