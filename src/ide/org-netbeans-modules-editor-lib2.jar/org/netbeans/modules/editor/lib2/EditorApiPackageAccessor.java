/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;

public abstract class EditorApiPackageAccessor {
    private static EditorApiPackageAccessor INSTANCE;

    public static EditorApiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName(EditorRegistry.class.getName(), true, EditorRegistry.class.getClassLoader());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        return INSTANCE;
    }

    public static void register(EditorApiPackageAccessor accessor) {
        INSTANCE = accessor;
    }

    public abstract void register(JTextComponent var1);

    public abstract void setIgnoredAncestorClass(Class var1);

    public abstract void notifyClose(JComponent var1);
}

