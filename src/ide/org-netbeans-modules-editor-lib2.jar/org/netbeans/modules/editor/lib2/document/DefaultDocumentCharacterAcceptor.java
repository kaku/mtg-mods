/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import org.netbeans.modules.editor.lib2.document.DocumentCharacterAcceptor;

public class DefaultDocumentCharacterAcceptor
extends DocumentCharacterAcceptor {
    public static final DocumentCharacterAcceptor INSTANCE = new DefaultDocumentCharacterAcceptor();

    private DefaultDocumentCharacterAcceptor() {
    }

    @Override
    public boolean isIdentifier(char ch) {
        return Character.isJavaIdentifierPart(ch);
    }

    @Override
    public boolean isWhitespace(char ch) {
        return Character.isWhitespace(ch);
    }
}

