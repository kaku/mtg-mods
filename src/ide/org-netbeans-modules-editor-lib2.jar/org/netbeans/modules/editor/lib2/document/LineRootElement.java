/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.GapBranchElement
 *  org.netbeans.lib.editor.util.swing.GapBranchElement$Edit
 */
package org.netbeans.modules.editor.lib2.document;

import java.util.ArrayList;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.undo.UndoableEdit;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.GapBranchElement;
import org.netbeans.modules.editor.lib2.document.AbstractPositionElement;
import org.netbeans.modules.editor.lib2.document.LineElement;

public final class LineRootElement
extends GapBranchElement {
    private static final String NAME = "section";
    private Document doc;

    public LineRootElement(Document doc) {
        this.doc = doc;
        assert (doc.getLength() == 0);
        Position startPos = doc.getStartPosition();
        assert (startPos.getOffset() == 0);
        Position endPos = doc.getEndPosition();
        assert (endPos.getOffset() == 1);
        LineElement line = new LineElement(this, startPos, endPos);
        this.replace(0, 0, new Element[]{line});
    }

    public Element getElement(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("Invalid line index=" + index + " < 0");
        }
        int elementCount = this.getElementCount();
        if (index >= elementCount) {
            throw new IndexOutOfBoundsException("Invalid line index=" + index + " >= lineCount=" + elementCount);
        }
        return super.getElement(index);
    }

    public int getElementIndex(int offset) {
        if (offset == 0) {
            return 0;
        }
        return super.getElementIndex(offset);
    }

    public void insertUpdate(AbstractDocument.DefaultDocumentEvent evt, AttributeSet attr) {
        int insertOffset = evt.getOffset();
        int insertEndOffset = insertOffset + evt.getLength();
        CharSequence text = DocumentUtilities.getText((Document)this.doc);
        if (insertOffset > 0) {
            --insertOffset;
        }
        try {
            int index = -1;
            ArrayList<LineElement> addedLines = null;
            AbstractPositionElement removedLine = null;
            Position lastAddedLineEndPos = null;
            for (int offset = insertOffset; offset < insertEndOffset; ++offset) {
                if (text.charAt(offset) != '\n') continue;
                if (index == -1) {
                    index = this.getElementIndex(offset);
                    removedLine = (LineElement)this.getElement(index);
                    lastAddedLineEndPos = removedLine.getStartPosition();
                    addedLines = new ArrayList<LineElement>(2);
                }
                Position lineEndPos = this.doc.createPosition(offset + 1);
                addedLines.add(new LineElement(this, lastAddedLineEndPos, lineEndPos));
                lastAddedLineEndPos = lineEndPos;
            }
            if (index != -1) {
                Element[] removed;
                Position removedLineEndPos = removedLine.getEndPosition();
                int removedLineEndOffset = removedLineEndPos.getOffset();
                int lastAddedLineEndOffset = lastAddedLineEndPos.getOffset();
                if (insertEndOffset == removedLineEndOffset && lastAddedLineEndOffset != removedLineEndOffset) {
                    LineElement removedLine2 = (LineElement)this.getElement(index + 1);
                    removed = new Element[]{removedLine, removedLine2};
                    removedLineEndPos = removedLine2.getEndPosition();
                    removedLineEndOffset = removedLineEndPos.getOffset();
                } else {
                    removed = new Element[]{removedLine};
                }
                if (lastAddedLineEndOffset < removedLineEndOffset) {
                    addedLines.add(new LineElement(this, lastAddedLineEndPos, removedLineEndPos));
                }
                Element[] added = new Element[addedLines.size()];
                addedLines.toArray(added);
                evt.addEdit((UndoableEdit)new GapBranchElement.Edit((GapBranchElement)this, index, removed, added));
                this.replace(index, removed.length, added);
            }
        }
        catch (BadLocationException e) {
            throw new IllegalStateException(e.toString());
        }
    }

    public void removeUpdate(AbstractDocument.DefaultDocumentEvent evt) {
        UndoableEdit edit = this.legacyRemoveUpdate(evt);
        if (edit != null) {
            evt.addEdit(edit);
        }
    }

    public UndoableEdit legacyRemoveUpdate(AbstractDocument.DefaultDocumentEvent evt) {
        int line1;
        int removeOffset = evt.getOffset();
        int removeEndOffset = removeOffset + evt.getLength();
        int line0 = this.getElementIndex(removeOffset);
        if (line0 != (line1 = this.getElementIndex(removeEndOffset))) {
            Element[] removed = new Element[++line1 - line0];
            this.copyElements(line0, line1, removed, 0);
            Element[] added = new Element[]{new LineElement(this, ((LineElement)removed[0]).getStartPosition(), ((LineElement)removed[removed.length - 1]).getEndPosition())};
            GapBranchElement.Edit edit = new GapBranchElement.Edit((GapBranchElement)this, line0, removed, added);
            this.replace(line0, removed.length, added);
            return edit;
        }
        return null;
    }

    public Document getDocument() {
        return this.doc;
    }

    public Element getParentElement() {
        return null;
    }

    public String getName() {
        return "section";
    }

    public AttributeSet getAttributes() {
        return SimpleAttributeSet.EMPTY;
    }

    public int getStartOffset() {
        return 0;
    }

    public int getEndOffset() {
        return this.doc.getLength() + 1;
    }
}

