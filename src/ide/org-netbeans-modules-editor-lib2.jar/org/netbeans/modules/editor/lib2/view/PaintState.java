/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public final class PaintState {
    private final Graphics2D g;
    private final Font font;
    private final Color color;

    public static PaintState save(Graphics2D g) {
        return new PaintState(g);
    }

    private PaintState(Graphics2D g) {
        this.g = g;
        this.font = g.getFont();
        this.color = g.getColor();
    }

    public void restore() {
        this.g.setColor(this.color);
        this.g.setFont(this.font);
    }
}

