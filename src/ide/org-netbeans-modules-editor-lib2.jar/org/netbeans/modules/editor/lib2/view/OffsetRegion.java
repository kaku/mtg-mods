/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;

public final class OffsetRegion {
    private final Position startPos;
    private final Position endPos;

    public static OffsetRegion create(Document doc, int startOffset, int endOffset) {
        OffsetRegion.checkBounds(startOffset, endOffset);
        return new OffsetRegion(OffsetRegion.createPos(doc, startOffset), OffsetRegion.createPos(doc, endOffset));
    }

    public static OffsetRegion create(Position startPos, Position endPos) {
        OffsetRegion.checkBounds(startPos.getOffset(), endPos.getOffset());
        return new OffsetRegion(startPos, endPos);
    }

    public static OffsetRegion union(OffsetRegion region, Document doc, int startOffset, int endOffset, boolean ignoreEmpty) {
        return region != null ? region.union(doc, startOffset, endOffset, ignoreEmpty) : OffsetRegion.create(doc, startOffset, endOffset);
    }

    public static OffsetRegion union(OffsetRegion region1, OffsetRegion region2, boolean ignoreEmpty) {
        return region1 != null ? region1.union(region2, ignoreEmpty) : region2;
    }

    private static Position createPos(Document doc, int offset) {
        try {
            return doc.createPosition(offset);
        }
        catch (BadLocationException ex) {
            throw new IllegalArgumentException("Invalid offset=" + offset + " in doc: " + doc, ex);
        }
    }

    private static void checkBounds(int startOffset, int endOffset) {
        if (startOffset > endOffset) {
            throw new IllegalArgumentException("startOffset=" + startOffset + " > endOffset=" + endOffset);
        }
    }

    private OffsetRegion(Position startPos, Position endPos) {
        this.startPos = startPos;
        this.endPos = endPos;
    }

    public int startOffset() {
        return this.startPos.getOffset();
    }

    public Position startPosition() {
        return this.startPos;
    }

    public int endOffset() {
        return this.endPos.getOffset();
    }

    public Position endPosition() {
        return this.endPos;
    }

    public int length() {
        return this.endOffset() - this.startOffset();
    }

    public boolean isEmpty() {
        return this.length() == 0;
    }

    public OffsetRegion union(Document doc, int startOffset, int endOffset, boolean ignoreEmpty) {
        OffsetRegion.checkBounds(startOffset, endOffset);
        int thisStartOffset = this.startOffset();
        int thisEndOffset = this.endOffset();
        if (ignoreEmpty) {
            if (startOffset == endOffset) {
                return this;
            }
            if (thisStartOffset == thisEndOffset) {
                return OffsetRegion.create(doc, startOffset, endOffset);
            }
        }
        if (startOffset >= thisStartOffset) {
            if (endOffset <= thisEndOffset) {
                return this;
            }
            return new OffsetRegion(this.startPos, OffsetRegion.createPos(doc, endOffset));
        }
        Position endP = endOffset > thisEndOffset ? OffsetRegion.createPos(doc, endOffset) : this.endPos;
        return new OffsetRegion(OffsetRegion.createPos(doc, startOffset), endP);
    }

    public OffsetRegion union(OffsetRegion region, boolean ignoreEmpty) {
        int thisStartOffset = this.startOffset();
        int thisEndOffset = this.endOffset();
        if (ignoreEmpty) {
            if (region.isEmpty()) {
                return this;
            }
            if (thisStartOffset == thisEndOffset) {
                return region;
            }
        }
        if (region.startOffset() >= thisStartOffset) {
            if (region.endOffset() <= thisEndOffset) {
                return this;
            }
            return new OffsetRegion(this.startPos, region.endPos);
        }
        Position endP = region.endOffset() > thisEndOffset ? region.endPos : this.endPos;
        return new OffsetRegion(region.startPos, endP);
    }

    public OffsetRegion intersection(Document doc, int startOffset, int endOffset, boolean nullIfOutside) {
        OffsetRegion.checkBounds(startOffset, endOffset);
        int thisStartOffset = this.startOffset();
        int thisEndOffset = this.endOffset();
        if (thisStartOffset >= startOffset) {
            if (thisEndOffset <= endOffset) {
                return this;
            }
            if (thisStartOffset > endOffset) {
                return nullIfOutside ? null : new OffsetRegion(this.startPos, this.startPos);
            }
            Position end = thisStartOffset == endOffset ? this.startPos : OffsetRegion.createPos(doc, endOffset);
            return new OffsetRegion(this.startPos, end);
        }
        if (thisEndOffset <= endOffset) {
            if (thisEndOffset < startOffset) {
                return nullIfOutside ? null : new OffsetRegion(this.endPos, this.endPos);
            }
            Position start = thisEndOffset == startOffset ? this.endPos : OffsetRegion.createPos(doc, startOffset);
            return new OffsetRegion(start, this.endPos);
        }
        return new OffsetRegion(OffsetRegion.createPos(doc, startOffset), OffsetRegion.createPos(doc, endOffset));
    }

    public OffsetRegion intersection(OffsetRegion region, boolean nullIfOutside) {
        int thisStartOffset = this.startOffset();
        int thisEndOffset = this.endOffset();
        int startOffset = region.startOffset();
        int endOffset = region.endOffset();
        if (thisStartOffset > startOffset) {
            if (thisEndOffset <= endOffset) {
                return this;
            }
            if (nullIfOutside && thisStartOffset > endOffset) {
                return nullIfOutside ? null : new OffsetRegion(this.startPos, this.startPos);
            }
            Position end = thisStartOffset == endOffset ? this.startPos : region.endPos;
            return new OffsetRegion(this.startPos, end);
        }
        if (thisStartOffset < startOffset) {
            if (thisEndOffset < endOffset) {
                if (thisEndOffset < startOffset) {
                    return nullIfOutside ? null : new OffsetRegion(this.endPos, this.endPos);
                }
                Position start = thisEndOffset == startOffset ? this.endPos : region.startPos;
                return new OffsetRegion(start, this.endPos);
            }
            return region;
        }
        if (thisEndOffset <= endOffset) {
            return this;
        }
        return region;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof OffsetRegion) {
            OffsetRegion region = (OffsetRegion)obj;
            return region.startOffset() == this.startOffset() && region.endOffset() == this.endOffset();
        }
        return false;
    }

    public String toString() {
        return this.isEmpty() ? "<E:" + this.startPos.getOffset() + ">" : "<" + this.startPos.getOffset() + "," + this.endPos.getOffset() + ">";
    }
}

