/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.typinghooks;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.typinghooks.TypingHooksSpiAccessor;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;

public final class DeletedTextInterceptorsManager {
    private static final Logger LOG = Logger.getLogger(DeletedTextInterceptorsManager.class.getName());
    private static DeletedTextInterceptorsManager instance;
    private Transaction transaction = null;
    private final Map<MimePath, Reference<Collection<DeletedTextInterceptor>>> cache = new WeakHashMap<MimePath, Reference<Collection<DeletedTextInterceptor>>>();

    static MimePath getMimePath(final Document doc, final int offset) {
        final MimePath[] mimePathR = new MimePath[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                List seqs = TokenHierarchy.get((Document)doc).embeddedTokenSequences(offset, true);
                TokenSequence seq = seqs.isEmpty() ? null : (TokenSequence)seqs.get(seqs.size() - 1);
                seq = seq == null ? TokenHierarchy.get((Document)doc).tokenSequence() : seq;
                mimePathR[0] = seq == null ? MimePath.parse((String)DocumentUtilities.getMimeType((Document)doc)) : MimePath.parse((String)seq.languagePath().mimePath());
            }
        });
        return mimePathR[0];
    }

    public static DeletedTextInterceptorsManager getInstance() {
        if (instance == null) {
            instance = new DeletedTextInterceptorsManager();
        }
        return instance;
    }

    public Transaction openTransaction(JTextComponent c, int offset, String removedText, boolean backwardDelete) {
        DeletedTextInterceptorsManager deletedTextInterceptorsManager = this;
        synchronized (deletedTextInterceptorsManager) {
            if (this.transaction == null) {
                this.transaction = new Transaction(c, offset, removedText, backwardDelete);
                return this.transaction;
            }
            throw new IllegalStateException("Too many transactions; only one at a time is allowed!");
        }
    }

    private DeletedTextInterceptorsManager() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<? extends DeletedTextInterceptor> getInterceptors(Document doc, int offset) {
        MimePath mimePath = DeletedTextInterceptorsManager.getMimePath(doc, offset);
        Map<MimePath, Reference<Collection<DeletedTextInterceptor>>> map = this.cache;
        synchronized (map) {
            Collection<DeletedTextInterceptor> interceptors;
            Reference<Collection<DeletedTextInterceptor>> ref = this.cache.get((Object)mimePath);
            Collection<DeletedTextInterceptor> collection = interceptors = ref == null ? null : ref.get();
            if (interceptors == null) {
                Collection factories = MimeLookup.getLookup((MimePath)mimePath).lookupAll(DeletedTextInterceptor.Factory.class);
                interceptors = new HashSet<DeletedTextInterceptor>(factories.size());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "DeletedTextInterceptor.Factory instances for {0}:", mimePath.getPath());
                }
                for (DeletedTextInterceptor.Factory f : factories) {
                    DeletedTextInterceptor interceptor = f.createDeletedTextInterceptor(mimePath);
                    if (interceptor != null) {
                        interceptors.add(interceptor);
                    }
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.log(Level.FINE, "    {0} created: {1}", new Object[]{f, interceptor});
                }
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("");
                }
                this.cache.put(mimePath, new SoftReference<Collection<DeletedTextInterceptor>>(interceptors));
            }
            return interceptors;
        }
    }

    public final class Transaction {
        private final DeletedTextInterceptor.Context context;
        private final Collection<? extends DeletedTextInterceptor> interceptors;
        private int phase;

        public boolean beforeRemove() {
            for (DeletedTextInterceptor i : this.interceptors) {
                try {
                    if (!i.beforeRemove(this.context)) continue;
                    return true;
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "DeletedTextInterceptor crashed in beforeRemove(): " + i, e);
                    continue;
                }
            }
            ++this.phase;
            return false;
        }

        public void textDeleted() {
            Object data = null;
            for (DeletedTextInterceptor i : this.interceptors) {
                try {
                    i.remove(this.context);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "DeletedTextInterceptor crashed in remove(): " + i, e);
                }
            }
            ++this.phase;
        }

        public void afterRemove() {
            for (DeletedTextInterceptor i : this.interceptors) {
                try {
                    i.afterRemove(this.context);
                }
                catch (Exception e) {
                    LOG.log(Level.INFO, "DeletedTextInterceptor crashed in afterRemove(): " + i, e);
                }
            }
            ++this.phase;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void close() {
            if (this.phase < 3) {
                for (DeletedTextInterceptor i : this.interceptors) {
                    try {
                        i.cancelled(this.context);
                    }
                    catch (Exception e) {
                        LOG.log(Level.INFO, "DeletedTextInterceptor crashed in cancelled(): " + i, e);
                    }
                }
            }
            DeletedTextInterceptorsManager i$ = DeletedTextInterceptorsManager.this;
            synchronized (i$) {
                DeletedTextInterceptorsManager.this.transaction = null;
            }
        }

        private Transaction(JTextComponent c, int offset, String removedText, boolean backwardDelete) {
            this.phase = 0;
            this.context = TypingHooksSpiAccessor.get().createDtiContext(c, offset, removedText, backwardDelete);
            this.interceptors = DeletedTextInterceptorsManager.this.getInterceptors(c.getDocument(), offset);
        }
    }

}

