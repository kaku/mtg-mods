/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.AttributesUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import javax.swing.text.AttributeSet;
import javax.swing.text.View;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.modules.editor.lib2.highlighting.CompoundAttributes;
import org.netbeans.modules.editor.lib2.highlighting.HighlightItem;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsList;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

class ViewPaintHighlights
implements HighlightsSequence {
    private static final HighlightItem[] EMPTY = new HighlightItem[0];
    private final HighlightsList paintHighlights;
    private int phIndex;
    private int phStartOffset;
    private int phEndOffset;
    private AttributeSet phAttrs;
    private int viewEndOffset;
    private HighlightItem[] cahItems;
    private int cahIndex;
    private int cahEndOffset;
    private AttributeSet cahAttrs;
    private int offsetDiff;
    private int hiStartOffset;
    private int hiEndOffset;
    private AttributeSet hiAttrs;

    ViewPaintHighlights(HighlightsList paintHighlights) {
        this.paintHighlights = paintHighlights;
        this.updatePH(0);
    }

    void reset(EditorView view, int shift) {
        assert (shift >= 0);
        int viewStartOffset = view.getStartOffset();
        int viewLength = view.getLength();
        assert (shift < viewLength);
        this.viewEndOffset = viewStartOffset + viewLength;
        AttributeSet attrs = view.getAttributes();
        int startOffset = viewStartOffset + shift;
        if (ViewUtils.isCompoundAttributes(attrs)) {
            CompoundAttributes cAttrs = (CompoundAttributes)attrs;
            this.offsetDiff = viewStartOffset - cAttrs.startOffset();
            this.cahItems = cAttrs.highlightItems();
            if (shift == 0) {
                this.cahIndex = 0;
            } else {
                int cahOffset = startOffset - this.offsetDiff;
                this.cahIndex = this.findCAHIndex(cahOffset);
            }
            if (this.cahIndex >= this.cahItems.length) {
                throw new IllegalStateException("offsetDiff=" + this.offsetDiff + ", view=" + view + ", shift=" + shift + ", viewStartOffset+shift=" + startOffset + "\ncAttrs:\n" + cAttrs + "\n" + this + "docView:\n" + ((DocumentView)view.getParent().getParent()).toStringDetail());
            }
            HighlightItem cahItem = this.cahItems[this.cahIndex];
            this.cahEndOffset = cahItem.getEndOffset() + this.offsetDiff;
            this.cahAttrs = cahItem.getAttributes();
        } else {
            this.cahItems = EMPTY;
            this.cahIndex = -1;
            this.cahEndOffset = this.viewEndOffset;
            this.cahAttrs = attrs == null ? null : attrs;
        }
        if (startOffset < this.phStartOffset) {
            this.updatePH(this.findPHIndex(startOffset));
        } else if (startOffset >= this.phEndOffset) {
            this.fetchNextPH();
            if (startOffset >= this.phEndOffset) {
                this.updatePH(this.findPHIndex(startOffset));
            }
        }
        this.hiStartOffset = this.hiEndOffset = startOffset;
    }

    @Override
    public boolean moveNext() {
        if (this.hiEndOffset >= this.viewEndOffset) {
            return false;
        }
        if (this.hiEndOffset >= this.phEndOffset) {
            this.fetchNextPH();
        }
        if (this.hiEndOffset >= this.cahEndOffset) {
            ++this.cahIndex;
            if (this.cahIndex >= this.cahItems.length) {
                return false;
            }
            HighlightItem hItem = this.cahItems[this.cahIndex];
            this.cahEndOffset = hItem.getEndOffset() + this.offsetDiff;
            this.cahAttrs = hItem.getAttributes();
        }
        this.hiStartOffset = this.hiEndOffset;
        this.hiEndOffset = this.phEndOffset < this.cahEndOffset ? Math.min(this.phEndOffset, this.viewEndOffset) : this.cahEndOffset;
        this.hiAttrs = this.cahAttrs;
        if (this.phAttrs != null) {
            this.hiAttrs = this.hiAttrs != null ? AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{this.phAttrs, this.hiAttrs}) : this.phAttrs;
        }
        return true;
    }

    @Override
    public int getStartOffset() {
        return this.hiStartOffset;
    }

    @Override
    public int getEndOffset() {
        return this.hiEndOffset;
    }

    @Override
    public AttributeSet getAttributes() {
        return this.hiAttrs;
    }

    private int findCAHIndex(int cahOffset) {
        int low = 0;
        int high = this.cahItems.length - 1;
        while (low <= high) {
            int mid = low + high >>> 1;
            int hEndOffset = this.cahItems[mid].getEndOffset();
            if (hEndOffset < cahOffset) {
                low = mid + 1;
                continue;
            }
            if (hEndOffset > cahOffset) {
                high = mid - 1;
                continue;
            }
            low = mid + 1;
            break;
        }
        return low;
    }

    private void updatePH(int index) {
        this.phIndex = index;
        this.phStartOffset = this.phIndex > 0 ? this.paintHighlights.get(this.phIndex - 1).getEndOffset() : this.paintHighlights.startOffset();
        HighlightItem phItem = this.paintHighlights.get(this.phIndex);
        this.phEndOffset = phItem.getEndOffset();
        this.phAttrs = phItem.getAttributes();
    }

    private void fetchNextPH() {
        ++this.phIndex;
        if (this.phIndex >= this.paintHighlights.size()) {
            throw new IllegalStateException("phIndex=" + this.phIndex + " >= paintHighlights.size()=" + this.paintHighlights.size() + "\n" + this);
        }
        this.phStartOffset = this.phEndOffset;
        HighlightItem hItem = this.paintHighlights.get(this.phIndex);
        this.phEndOffset = hItem.getEndOffset();
        this.phAttrs = hItem.getAttributes();
    }

    private int findPHIndex(int offset) {
        int low = 0;
        int high = this.paintHighlights.size() - 1;
        while (low <= high) {
            int mid = low + high >>> 1;
            int hEndOffset = this.paintHighlights.get(mid).getEndOffset();
            if (hEndOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (hEndOffset > offset) {
                high = mid - 1;
                continue;
            }
            low = mid + 1;
            break;
        }
        return low;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("ViewPaintHighlights: ph[").append(this.phIndex).append("]<").append(this.phStartOffset).append(",").append(this.phEndOffset).append("> attrs=").append(this.phAttrs).append('\n');
        sb.append("cah[").append(this.cahIndex).append("]#").append(this.cahItems.length);
        sb.append(" <?,").append(this.cahEndOffset).append("> attrs=").append(this.cahAttrs);
        sb.append(", viewEndOffset=").append(this.viewEndOffset).append(", offsetDiff=").append(this.offsetDiff);
        sb.append("\npaintHighlights:").append(this.paintHighlights);
        return sb.toString();
    }
}

