/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.EmbeddingPresence
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.lib2;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.EmbeddingPresence;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;
import org.openide.filesystems.FileObject;

public enum DialogBindingTokenId implements TokenId
{
    TEXT;
    
    private static final Language<DialogBindingTokenId> language;

    private DialogBindingTokenId() {
    }

    public String primaryCategory() {
        return "text";
    }

    public static Language<DialogBindingTokenId> language() {
        return language;
    }

    static {
        language = new LanguageHierarchy<DialogBindingTokenId>(){

            protected Collection<DialogBindingTokenId> createTokenIds() {
                return EnumSet.allOf(DialogBindingTokenId.class);
            }

            protected Map<String, Collection<DialogBindingTokenId>> createTokenCategories() {
                return null;
            }

            protected Lexer<DialogBindingTokenId> createLexer(LexerRestartInfo<DialogBindingTokenId> info) {
                return new LexerImpl(info);
            }

            protected LanguageEmbedding<?> embedding(Token<DialogBindingTokenId> token, LanguagePath languagePath, InputAttributes inputAttributes) {
                if (inputAttributes == null) {
                    return null;
                }
                String mimeType = null;
                Document doc = (Document)inputAttributes.getValue(languagePath, (Object)"dialogBinding.document");
                if (doc != null) {
                    mimeType = (String)doc.getProperty("mimeType");
                } else {
                    FileObject fo = (FileObject)inputAttributes.getValue(languagePath, (Object)"dialogBinding.fileObject");
                    if (fo != null) {
                        mimeType = fo.getMIMEType();
                    }
                }
                if (mimeType == null) {
                    return null;
                }
                Language l = (Language)MimeLookup.getLookup((String)mimeType).lookup(Language.class);
                return l != null ? LanguageEmbedding.create((Language)l, (int)0, (int)0) : null;
            }

            protected EmbeddingPresence embeddingPresence(DialogBindingTokenId id) {
                return EmbeddingPresence.ALWAYS_QUERY;
            }

            protected String mimeType() {
                return "text/x-dialog-binding";
            }
        }.language();
    }

    private static final class LexerImpl
    implements Lexer<DialogBindingTokenId> {
        private TokenFactory<DialogBindingTokenId> tokenFactory;
        private LexerInput input;

        public LexerImpl(LexerRestartInfo<DialogBindingTokenId> info) {
            this.tokenFactory = info.tokenFactory();
            this.input = info.input();
        }

        public Token<DialogBindingTokenId> nextToken() {
            int actChar;
            while ((actChar = this.input.read()) != -1) {
            }
            if (this.input.readLengthEOF() == 1) {
                return null;
            }
            this.input.backup(1);
            return this.tokenFactory.createToken((TokenId)DialogBindingTokenId.TEXT);
        }

        public Object state() {
            return null;
        }

        public void release() {
        }
    }

}

