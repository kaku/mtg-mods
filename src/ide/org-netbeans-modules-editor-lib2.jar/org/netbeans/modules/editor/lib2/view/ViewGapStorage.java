/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

final class ViewGapStorage {
    static final int GAP_STORAGE_THRESHOLD = 20;
    static final double INITIAL_VISUAL_GAP_LENGTH = 1.099511627776E12;
    static final int INITIAL_OFFSET_GAP_LENGTH = 1073741823;
    double visualGapStart;
    double visualGapLength;
    int visualGapIndex;
    int offsetGapStart;
    int offsetGapLength;

    ViewGapStorage() {
    }

    void initVisualGap(int visualGapIndex, double visualGapStart) {
        this.visualGapIndex = visualGapIndex;
        this.visualGapStart = visualGapStart;
        this.visualGapLength = 1.099511627776E12;
    }

    void initOffsetGap(int offsetGapStart) {
        this.offsetGapStart = offsetGapStart;
        this.offsetGapLength = 1073741823;
    }

    int raw2Offset(int rawEndOffset) {
        return rawEndOffset <= this.offsetGapStart ? rawEndOffset : rawEndOffset - this.offsetGapLength;
    }

    int offset2Raw(int offset) {
        return offset <= this.offsetGapStart ? offset : offset + this.offsetGapLength;
    }

    double raw2VisualOffset(double rawVisualOffset) {
        return rawVisualOffset <= this.visualGapStart ? rawVisualOffset : rawVisualOffset - this.visualGapLength;
    }

    double visualOffset2Raw(double visualOffset) {
        return visualOffset <= this.visualGapStart ? visualOffset : visualOffset + this.visualGapLength;
    }

    boolean isBelowVisualGap(double rawVisualOffset) {
        return rawVisualOffset <= this.visualGapStart;
    }

    StringBuilder appendInfo(StringBuilder sb) {
        sb.append("<").append(this.offsetGapStart).append("|").append(this.offsetGapLength).append(", vis[").append(this.visualGapIndex).append("]<").append(this.visualGapStart).append("|").append(this.visualGapLength);
        return sb;
    }

    public String toString() {
        return this.appendInfo(new StringBuilder(100)).toString();
    }
}

