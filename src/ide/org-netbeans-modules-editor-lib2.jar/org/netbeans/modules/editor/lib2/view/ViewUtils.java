/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.highlighting.CompoundAttributes;
import org.netbeans.modules.editor.lib2.highlighting.HighlightItem;

public final class ViewUtils {
    private ViewUtils() {
    }

    public static Rectangle2D.Double shape2Bounds(Shape s) {
        Rectangle2D r = s instanceof Rectangle2D ? (Rectangle2D)s : s.getBounds2D();
        return new Rectangle2D.Double(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }

    public static Rectangle2D shapeAsRect(Shape s) {
        Rectangle2D r = s instanceof Rectangle2D ? (Rectangle2D)s : s.getBounds2D();
        return r;
    }

    public static Rectangle toRect(Rectangle2D r2d) {
        Rectangle r = new Rectangle();
        r.setRect(r2d);
        return r;
    }

    public static void fillRect(Graphics2D g, Rectangle2D r) {
        g.fillRect((int)r.getX(), (int)r.getY(), (int)Math.ceil(r.getWidth()), (int)Math.ceil(r.getHeight()));
    }

    public static boolean applyBackgroundColor(Graphics2D g, AttributeSet attrs, JTextComponent c) {
        Color background;
        boolean overrides = false;
        if (attrs != null) {
            background = (Color)attrs.getAttribute(StyleConstants.Background);
            if (background != null) {
                if (!background.equals(c.getBackground())) {
                    overrides = true;
                }
            } else {
                background = c.getBackground();
            }
        } else {
            background = c.getBackground();
        }
        if (background != null) {
            g.setColor(background);
        }
        return overrides;
    }

    public static void applyForegroundColor(Graphics2D g, AttributeSet attrs, JTextComponent c) {
        Color foreground;
        if (attrs != null) {
            foreground = (Color)attrs.getAttribute(StyleConstants.Foreground);
            if (foreground == null) {
                foreground = c.getForeground();
            }
        } else {
            foreground = c.getForeground();
        }
        if (foreground != null) {
            g.setColor(foreground);
        }
    }

    public static void applyFont(Graphics2D g, AttributeSet attrs, JTextComponent c) {
        Font font = c.getFont();
        if (attrs != null) {
            font = ViewUtils.getFont(attrs, font);
        }
        if (font != null) {
            g.setFont(font);
        }
    }

    public static Font getFont(AttributeSet attrs, Font defaultFont) {
        if (attrs != null) {
            String fontName = (String)attrs.getAttribute(StyleConstants.FontFamily);
            Boolean bold = (Boolean)attrs.getAttribute(StyleConstants.Bold);
            Boolean italic = (Boolean)attrs.getAttribute(StyleConstants.Italic);
            Integer fontSizeInteger = (Integer)attrs.getAttribute(StyleConstants.FontSize);
            if (fontName != null || bold != null || italic != null || fontSizeInteger != null) {
                int fontStyle;
                if (fontName == null) {
                    fontName = defaultFont.getFontName();
                }
                int n = fontStyle = defaultFont != null ? defaultFont.getStyle() : 0;
                if (bold != null) {
                    fontStyle &= -2;
                    if (bold.booleanValue()) {
                        fontStyle |= 1;
                    }
                }
                if (italic != null) {
                    fontStyle &= -3;
                    if (italic.booleanValue()) {
                        fontStyle |= 2;
                    }
                }
                int fontSize = fontSizeInteger != null ? fontSizeInteger.intValue() : defaultFont.getSize();
                defaultFont = new Font(fontName, fontStyle, fontSize);
            }
        }
        return defaultFont;
    }

    public static Font getFont(AttributeSet attrs) {
        Font font = null;
        if (attrs != null) {
            String fontName = (String)attrs.getAttribute(StyleConstants.FontFamily);
            Integer fontSizeInteger = (Integer)attrs.getAttribute(StyleConstants.FontSize);
            if (fontName != null && fontSizeInteger != null) {
                Boolean bold = (Boolean)attrs.getAttribute(StyleConstants.Bold);
                Boolean italic = (Boolean)attrs.getAttribute(StyleConstants.Italic);
                int fontStyle = 0;
                if (bold != null && bold.booleanValue()) {
                    fontStyle |= true;
                }
                if (italic != null && italic.booleanValue()) {
                    fontStyle |= 2;
                }
                font = new Font(fontName, fontStyle, fontSizeInteger);
            }
        }
        return font;
    }

    public static String toStringAxis(int axis) {
        return axis == 0 ? "X" : "Y";
    }

    public static String toStringDirection(int direction) {
        switch (direction) {
            case 7: {
                return "WEST";
            }
            case 3: {
                return "EAST";
            }
            case 1: {
                return "NORTH";
            }
            case 5: {
                return "SOUTH";
            }
        }
        return "<INVALID-DIRECTION>";
    }

    public static void repaint(JComponent component, Rectangle2D r) {
        component.repaint((int)r.getX(), (int)r.getY(), (int)r.getWidth(), (int)r.getHeight());
    }

    public static String toString(Color c) {
        return c != null ? "RGB[" + c.getRed() + ';' + c.getGreen() + ';' + c.getBlue() + ']' : "<NULL>";
    }

    public static String toString(Rectangle2D r) {
        return "XYWH[" + r.getX() + ';' + r.getY() + ';' + r.getWidth() + ';' + r.getHeight() + ']';
    }

    public static String toString(Shape s) {
        if (s instanceof Rectangle2D) {
            return ViewUtils.toString((Rectangle2D)s);
        }
        if (s != null) {
            return ViewUtils.appendPath(new StringBuilder(200), 0, s.getPathIterator(null)).toString();
        }
        return "<NULL>";
    }

    public static String toStringHex8(int i) {
        String s = Integer.toHexString(i);
        while (s.length() < 8) {
            s = "0" + s;
        }
        return s;
    }

    public static String toStringId(Object o) {
        return o != null ? ViewUtils.toStringHex8(System.identityHashCode(o)) : "<NULL>";
    }

    public static String toStringNameId(Object o) {
        if (o == null) {
            return "<NULL>";
        }
        String className = o.getClass().getName();
        className = className.substring(className.lastIndexOf(46) + 1);
        return className + "@" + ViewUtils.toStringId(o);
    }

    public static String toStringPrec1(double d) {
        String s = Double.toString(d);
        int dotIndex = s.indexOf(46);
        if (dotIndex >= 0 && dotIndex < s.length() - 2) {
            s = s.substring(0, dotIndex + 2);
        }
        return s;
    }

    public static String toString(AttributeSet attributes) {
        Color backColor;
        Color foreColor;
        boolean nonFirst = false;
        StringBuilder sb = new StringBuilder(200);
        String fontName = (String)attributes.getAttribute(StyleConstants.FontFamily);
        Boolean bold = (Boolean)attributes.getAttribute(StyleConstants.Bold);
        Boolean italic = (Boolean)attributes.getAttribute(StyleConstants.Italic);
        Integer fontSizeInteger = (Integer)attributes.getAttribute(StyleConstants.FontSize);
        if (fontName != null || bold != null || italic != null || fontSizeInteger != null) {
            sb.append("Font[");
            sb.append((Object)(fontName != null ? "" + '\"' + fontName + '\"' : Character.valueOf('?'))).append(',');
            if (bold != null || italic != null) {
                if (bold != null) {
                    sb.append('B');
                }
                if (italic != null) {
                    sb.append('I');
                }
            } else {
                sb.append('?');
            }
            sb.append(',');
            sb.append(fontSizeInteger != null ? fontSizeInteger : 63);
            sb.append("], ");
            nonFirst = true;
        }
        if ((foreColor = (Color)attributes.getAttribute(StyleConstants.Foreground)) != null) {
            if (nonFirst) {
                sb.append(", ");
            }
            sb.append("fg=").append(ViewUtils.toString(foreColor));
            nonFirst = true;
        }
        if ((backColor = (Color)attributes.getAttribute(StyleConstants.Background)) != null) {
            if (nonFirst) {
                sb.append(", ");
            }
            sb.append("bg=").append(ViewUtils.toString(backColor));
            nonFirst = true;
        }
        return sb.toString();
    }

    private static StringBuilder appendPath(StringBuilder sb, int indent, PathIterator pathIterator) {
        double[] coords = new double[6];
        while (!pathIterator.isDone()) {
            int endIndex;
            String typeStr;
            int type = pathIterator.currentSegment(coords);
            switch (type) {
                case 4: {
                    typeStr = "SEG_CLOSE";
                    endIndex = 0;
                    break;
                }
                case 3: {
                    typeStr = "SEG_CUBICTO";
                    endIndex = 6;
                    break;
                }
                case 1: {
                    typeStr = "SEG_LINETO";
                    endIndex = 2;
                    break;
                }
                case 0: {
                    typeStr = "SEG_MOVETO";
                    endIndex = 2;
                    break;
                }
                case 2: {
                    typeStr = "SEG_QUADTO";
                    endIndex = 4;
                    break;
                }
                default: {
                    throw new IllegalStateException("Invalid type=" + type);
                }
            }
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
            sb.append(typeStr).append(": ");
            int i = 0;
            while (i < endIndex) {
                sb.append("[").append(coords[i++]).append(",").append(coords[i++]).append("] ");
            }
            sb.append('\n');
            pathIterator.next();
        }
        return sb;
    }

    public static String toString(JComponent component) {
        if (component == null) {
            return "<NULL>";
        }
        StringBuilder sb = new StringBuilder(100);
        sb.append(ViewUtils.toStringNameId(component));
        if (component instanceof JTextComponent) {
            JTextComponent textComponent = (JTextComponent)component;
            sb.append("\n  doc: ").append(ViewUtils.toString(textComponent.getDocument()));
        }
        return sb.toString();
    }

    public static String toString(Document doc) {
        if (doc == null) {
            return "<NULL>";
        }
        StringBuilder sb = new StringBuilder(100);
        sb.append(ViewUtils.toStringNameId(doc));
        sb.append(", Length=").append(doc.getLength());
        sb.append(", Version=").append(DocumentUtilities.getDocumentVersion((Document)doc));
        sb.append("\n    StreamDesc: ");
        Object streamDesc = doc.getProperty("stream");
        if (streamDesc != null) {
            sb.append(streamDesc);
        } else {
            sb.append("<NULL>");
        }
        return sb.toString();
    }

    public static void checkFragmentBounds(int p0, int p1, int startOffset, int length) {
        if (p0 < startOffset || p0 > p1 || p1 > startOffset + length) {
            throw new IllegalArgumentException("Illegal bounds: <" + p0 + "," + p1 + "> outside of <" + startOffset + "," + (startOffset + length) + ">");
        }
    }

    public static boolean isCompoundAttributes(AttributeSet attrs) {
        return attrs instanceof CompoundAttributes;
    }

    public static AttributeSet getFirstAttributes(AttributeSet attrs) {
        return attrs instanceof CompoundAttributes ? ((CompoundAttributes)attrs).highlightItems()[0].getAttributes() : attrs;
    }

    public static void runInEDT(Runnable r) {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }

    public static Position createPosition(Document doc, int offset) {
        try {
            return doc.createPosition(offset);
        }
        catch (BadLocationException ex) {
            throw new IndexOutOfBoundsException(ex.getMessage());
        }
    }

    public static void log(Logger logger, String msg) {
        if (logger.isLoggable(Level.FINEST)) {
            logger.log(Level.INFO, "Cause of " + msg, new Exception());
        } else {
            logger.fine(msg);
        }
    }
}

