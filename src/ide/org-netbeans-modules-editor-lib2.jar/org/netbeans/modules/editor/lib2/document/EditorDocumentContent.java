/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.undo.UndoableEdit;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.editor.lib2.document.CharContent;
import org.netbeans.modules.editor.lib2.document.ContentEdit;
import org.netbeans.modules.editor.lib2.document.EditorPosition;
import org.netbeans.modules.editor.lib2.document.Mark;
import org.netbeans.modules.editor.lib2.document.MarkVector;

public final class EditorDocumentContent
implements AbstractDocument.Content {
    private static final Logger LOG = Logger.getLogger(EditorDocumentContent.class.getName());
    private final CharContent charContent = new CharContent();
    private final MarkVector markVector;
    private MarkVector bbMarkVector;

    public EditorDocumentContent() {
        this.markVector = new MarkVector(this, false);
    }

    public void init(Document doc) {
        doc.putProperty(CharSequence.class, this.charContent);
    }

    @Override
    public UndoableEdit insertString(int offset, String text) throws BadLocationException {
        if (text.length() == 0) {
            throw new IllegalArgumentException("EditorDocumentContent: Empty insert");
        }
        this.checkOffsetInDoc(offset);
        ContentEdit.InsertEdit insertEdit = new ContentEdit.InsertEdit(this, offset, text);
        this.insertEdit(insertEdit, "");
        return insertEdit;
    }

    synchronized void insertEdit(ContentEdit edit, String opType) {
        this.charContent.insertText(edit.offset, edit.text);
        this.markVector.insertUpdate(edit.offset, edit.length(), edit.markUpdates);
        edit.markUpdates = null;
        if (this.bbMarkVector != null) {
            this.bbMarkVector.insertUpdate(edit.offset, edit.length(), edit.bbMarkUpdates);
            edit.bbMarkUpdates = null;
        }
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(200);
            sb.append(opType).append("insertEdit(off=").append(edit.offset).append(", len=").append(edit.length()).append(") text=\"").append(CharSequenceUtilities.debugText((CharSequence)edit.text)).append("\"\n");
            this.logMarkUpdates(sb, edit.markUpdates, false, false);
            this.logMarkUpdates(sb, edit.bbMarkUpdates, true, false);
            LOG.fine(sb.toString());
            if (LOG.isLoggable(Level.FINER)) {
                this.checkConsistency();
            }
        }
    }

    @Override
    public UndoableEdit remove(int offset, int length) throws BadLocationException {
        this.checkBoundsInDoc(offset, length);
        String removedText = this.getString(offset, length);
        return this.remove(offset, removedText);
    }

    public UndoableEdit remove(int offset, String removedText) throws BadLocationException {
        ContentEdit.RemoveEdit removeEdit = new ContentEdit.RemoveEdit(this, offset, removedText);
        this.removeEdit(removeEdit, "");
        return removeEdit;
    }

    synchronized void removeEdit(ContentEdit edit, String opType) {
        int len = edit.length();
        this.charContent.removeText(edit.offset, len);
        edit.markUpdates = this.markVector.removeUpdate(edit.offset, len);
        if (this.bbMarkVector != null) {
            edit.bbMarkUpdates = this.bbMarkVector.removeUpdate(edit.offset, len);
        }
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(200);
            sb.append(opType).append("removeEdit(off=").append(edit.offset).append(", len=").append(len).append(")\n");
            this.logMarkUpdates(sb, edit.markUpdates, false, true);
            this.logMarkUpdates(sb, edit.markUpdates, true, true);
            LOG.fine(sb.toString());
            if (LOG.isLoggable(Level.FINER)) {
                this.checkConsistency();
            }
        }
    }

    @Override
    public synchronized Position createPosition(int offset) throws BadLocationException {
        this.checkOffsetInContent(offset);
        EditorPosition pos = this.markVector.position(offset);
        return pos;
    }

    public synchronized Position createBackwardBiasPosition(int offset) throws BadLocationException {
        this.checkOffsetInContent(offset);
        if (this.bbMarkVector == null) {
            this.bbMarkVector = new MarkVector(this, true);
        }
        return this.bbMarkVector.position(offset);
    }

    @Override
    public int length() {
        return this.charContent.length();
    }

    private int docLen() {
        return this.length() - 1;
    }

    public CharSequence getText() {
        return this.charContent;
    }

    public int getCharContentGapStart() {
        return this.charContent.gapStart();
    }

    @Override
    public synchronized void getChars(int offset, int length, Segment txt) throws BadLocationException {
        this.checkBoundsInContent(offset, length);
        this.charContent.getChars(offset, length, txt);
    }

    @Override
    public synchronized String getString(int offset, int length) throws BadLocationException {
        this.checkBoundsInContent(offset, length);
        return this.charContent.getString(offset, length);
    }

    public synchronized void compact() {
        this.charContent.compact();
        this.markVector.compact();
        if (this.bbMarkVector != null) {
            this.bbMarkVector.compact();
        }
    }

    public synchronized String consistencyError() {
        String err = this.charContent.consistencyError();
        if (err == null) {
            err = this.markVector.consistencyError(this.length());
        }
        if (err == null && this.bbMarkVector != null) {
            err = this.bbMarkVector.consistencyError(this.length());
        }
        return err;
    }

    public void checkConsistency() {
        String err = this.consistencyError();
        if (err != null) {
            throw new IllegalStateException("Content inconsistency: " + err + "\nContent:\n" + this.toStringDetail());
        }
    }

    private void checkOffsetNonNegative(int offset) throws BadLocationException {
        if (offset < 0) {
            throw new BadLocationException("Invalid offset=" + offset + " < 0; docLen=" + this.docLen(), offset);
        }
    }

    private void checkOffsetInDoc(int offset) throws BadLocationException {
        this.checkOffsetNonNegative(offset);
        if (offset > this.docLen()) {
            throw new BadLocationException("Invalid offset=" + offset + " > docLen=" + this.docLen(), offset);
        }
    }

    private void checkOffsetInContent(int offset) throws BadLocationException {
        this.checkOffsetNonNegative(offset);
        if (offset > this.length()) {
            throw new BadLocationException("Invalid offset=" + offset + " > (docLen+1)=" + this.docLen(), offset);
        }
    }

    private void checkLengthNonNegative(int length) throws BadLocationException {
        if (length < 0) {
            throw new BadLocationException("Invalid length=" + length + " < 0", length);
        }
    }

    private void checkBoundsInDoc(int offset, int length) throws BadLocationException {
        this.checkOffsetNonNegative(offset);
        this.checkLengthNonNegative(length);
        if (offset + length > this.docLen()) {
            throw new BadLocationException("Invalid (offset=" + offset + " + length=" + length + ")=" + (offset + length) + " > docLen=" + this.docLen(), offset + length);
        }
    }

    private void checkBoundsInContent(int offset, int length) throws BadLocationException {
        this.checkOffsetNonNegative(offset);
        this.checkLengthNonNegative(length);
        if (offset + length > this.length()) {
            throw new BadLocationException("Invalid (offset=" + offset + " + length=" + length + ")=" + (offset + length) + " > (docLen+1)=" + this.length(), offset + length);
        }
    }

    private void logMarkUpdates(StringBuilder sb, MarkVector.MarkUpdate[] updates, boolean bbMarks, boolean forRemove) {
        if (updates != null) {
            for (int i = 0; i < updates.length; ++i) {
                MarkVector.MarkUpdate update = updates[i];
                if (forRemove) {
                    sb.append("    ").append(bbMarks ? "BB" : "").append("Mark's original offset saved: ").append(update).append('\n');
                    continue;
                }
                if (!update.mark.isActive()) continue;
                sb.append("    Restoring offset for ").append(bbMarks ? "BB" : "").append("MarkUpdate: ").append(update).append('\n');
            }
        }
    }

    public synchronized String toString() {
        return "chars: " + this.charContent.toStringDescription() + ", marks: " + this.markVector + ", bbMarks: " + (this.bbMarkVector != null ? this.bbMarkVector : "<NULL>");
    }

    public synchronized String toStringDetail() {
        return "marks: " + this.markVector.toStringDetail(null) + ", bbMarks: " + (this.bbMarkVector != null ? this.bbMarkVector.toStringDetail(null) : "<NULL>");
    }
}

