/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Document;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.document.DocumentCharacterAcceptor;
import org.netbeans.modules.editor.lib2.document.EditorCharacterServices;

public final class DefaultEditorCharacterServices
extends EditorCharacterServices {
    @Override
    public int getIdentifierEnd(Document doc, int offset, boolean backward) {
        DocumentCharacterAcceptor characterAcceptor = DocumentCharacterAcceptor.get(doc);
        CharSequence docText = DocumentUtilities.getText((Document)doc);
        if (backward) {
            while (--offset >= 0 && characterAcceptor.isIdentifier(docText.charAt(offset))) {
            }
            return offset + 1;
        }
        int docTextLen = docText.length();
        while (offset < docTextLen && characterAcceptor.isIdentifier(docText.charAt(offset))) {
            ++offset;
        }
        return offset;
    }
}

