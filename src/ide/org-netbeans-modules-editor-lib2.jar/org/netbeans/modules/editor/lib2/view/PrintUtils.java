/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.modules.editor.lib2.view;

import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.highlighting.HighlightItem;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsList;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsReader;
import org.netbeans.modules.editor.lib2.view.AttributedCharSequence;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.openide.util.Lookup;

public final class PrintUtils {
    private PrintUtils() {
    }

    public static List<AttributedCharacterIterator> printDocument(final Document doc, final boolean printLineNumbers, final int startOffset, final int endOffset) {
        assert (SwingUtilities.isEventDispatchThread());
        final ArrayList<AttributedCharacterIterator> attributedLines = new ArrayList<AttributedCharacterIterator>();
        doc.render(new Runnable(){

            @Override
            public void run() {
                int startLineIndex;
                int endLineIndex;
                Element lineRoot = doc.getDefaultRootElement();
                int n = startLineIndex = startOffset > 0 ? lineRoot.getElementIndex(startOffset) : 0;
                if (endOffset < doc.getLength()) {
                    endLineIndex = lineRoot.getElementIndex(endOffset);
                    if (endLineIndex >= 0 && endOffset > lineRoot.getElement(endLineIndex).getStartOffset()) {
                        ++endLineIndex;
                    }
                } else {
                    endLineIndex = lineRoot.getElementCount();
                }
                JEditorPane pane = new JEditorPane();
                pane.setDocument(doc);
                CharSequence docText = DocumentUtilities.getText((Document)doc);
                String mimeType = DocumentUtilities.getMimeType((JTextComponent)pane);
                Lookup lookup = MimeLookup.getLookup((String)mimeType);
                Lookup.Result result = lookup.lookupResult(FontColorSettings.class);
                Collection fcsInstances = result.allInstances();
                HighlightsContainer bottomHighlights = HighlightingManager.getInstance(pane).getBottomHighlights();
                HighlightsReader hiReader = new HighlightsReader(bottomHighlights, startOffset, endOffset);
                hiReader.readUntil(endOffset);
                HighlightsList hiList = hiReader.highlightsList();
                Integer tabSize = (Integer)doc.getProperty("tabSize");
                if (tabSize == null) {
                    tabSize = 8;
                }
                if (endOffset > startOffset && fcsInstances.size() > 0) {
                    assert (hiList.size() > 0);
                    int hiItemIndex = 0;
                    HighlightItem hiItem = hiList.get(hiItemIndex);
                    FontColorSettings fcs = (FontColorSettings)fcsInstances.iterator().next();
                    AttributeSet defaultAttrs = fcs.getFontColors("default");
                    AttributeSet lineNumberAttrs = fcs.getFontColors("line-number");
                    lineNumberAttrs = AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{defaultAttrs, lineNumberAttrs});
                    HashMap cache = new HashMap();
                    Map defaultTextAttrs = PrintUtils.translate(defaultAttrs, cache);
                    Map lineNumberTextAttrs = PrintUtils.translate(lineNumberAttrs, cache);
                    int maxDigitCount = String.valueOf(endLineIndex + 1).length();
                    StringBuilder sb = new StringBuilder(100);
                    while (startLineIndex < endLineIndex) {
                        int offset0;
                        sb.setLength(0);
                        int startColumn = 0;
                        Element lineElement = lineRoot.getElement(startLineIndex);
                        if (printLineNumbers) {
                            ArrayUtilities.appendIndex((StringBuilder)sb, (int)(startLineIndex + 1), (int)maxDigitCount);
                            sb.append(' ');
                        }
                        int lineTextStartIndex = sb.length();
                        for (offset0 = lineElement.getStartOffset(); offset0 < startOffset; ++offset0) {
                            if (docText.charAt(offset0) == '\t') {
                                startColumn = (startColumn + tabSize) % tabSize * tabSize;
                                continue;
                            }
                            ++startColumn;
                        }
                        int offset1 = Math.min(lineElement.getEndOffset() - 1, endOffset);
                        AttributedCharSequence acs = new AttributedCharSequence();
                        if (printLineNumbers) {
                            acs.addTextRun(maxDigitCount, lineNumberTextAttrs);
                            acs.addTextRun(maxDigitCount + 1, defaultTextAttrs);
                        }
                        int offset = offset0;
                        int column = startColumn;
                        while (offset < offset1) {
                            while (hiItem.getEndOffset() <= offset) {
                                hiItem = hiList.get(++hiItemIndex);
                            }
                            int hiEndOffset = Math.min(hiItem.getEndOffset(), offset1);
                            while (offset < hiEndOffset) {
                                char ch = docText.charAt(offset);
                                if (ch == '\t') {
                                    int newColumn = (column + tabSize) / tabSize * tabSize;
                                    while (column < newColumn) {
                                        sb.append(' ');
                                        ++column;
                                    }
                                } else {
                                    sb.append(ch);
                                    ++column;
                                }
                                ++offset;
                            }
                            Map textAttrs = hiItem.getAttributes() != null ? PrintUtils.translate(AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{hiItem.getAttributes(), defaultAttrs}), cache) : defaultTextAttrs;
                            acs.addTextRun(lineTextStartIndex + (column - startColumn), textAttrs);
                        }
                        acs.setText(sb.toString(), defaultTextAttrs);
                        attributedLines.add(acs);
                        ++startLineIndex;
                    }
                }
            }
        });
        return attributedLines;
    }

    private static Map<AttributedCharacterIterator.Attribute, Object> translate(AttributeSet attrs, Map<AttributeSet, Map<AttributedCharacterIterator.Attribute, Object>> cache) {
        Map<AttributedCharacterIterator.Attribute, Object> textAttrs = cache.get(attrs);
        if (textAttrs == null) {
            textAttrs = AttributedCharSequence.translate(attrs);
            cache.put(attrs, textAttrs);
        }
        return textAttrs;
    }

}

