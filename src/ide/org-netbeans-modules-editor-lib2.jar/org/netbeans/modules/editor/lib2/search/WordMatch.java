/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.GapList
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2.search;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.document.DocumentCharacterAcceptor;
import org.netbeans.modules.editor.lib2.document.EditorDocumentHandler;
import org.netbeans.modules.editor.lib2.search.TextStorageSet;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public final class WordMatch {
    private static final Logger LOG = Logger.getLogger(WordMatch.class.getName());
    private final Document doc;
    private List<Reference<Document>> documents = new ArrayList<Reference<Document>>();
    private int documentIndex = -1;
    private Map<Document, Boolean> documentSet = new WeakHashMap<Document, Boolean>();
    private WordInfo baseWordInfo;
    private final TextStorageSet wordSet = new TextStorageSet();
    private final List<WordInfo> wordInfoList = new GapList(4);
    private int wordInfoListIndex;
    private boolean matchCase;
    private boolean smartCase;
    private boolean realMatchCase;
    private Preferences prefs = null;
    private final PreferenceChangeListener prefsListener;
    private PreferenceChangeListener weakListener;

    public static synchronized WordMatch get(Document doc) {
        WordMatch wordMatch = (WordMatch)doc.getProperty(WordMatch.class);
        if (wordMatch == null) {
            wordMatch = new WordMatch(doc);
        }
        return wordMatch;
    }

    private WordMatch(Document doc) {
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                WordMatch.this.matchCase = WordMatch.this.prefs.getBoolean("word-match-match-case", false);
                WordMatch.this.smartCase = WordMatch.this.prefs.getBoolean("word-match-smart-case", false);
            }
        };
        this.weakListener = null;
        this.doc = doc;
    }

    private void checkInitPrefs() {
        String mimeType;
        if (this.weakListener == null && (mimeType = DocumentUtilities.getMimeType((Document)this.doc)) != null) {
            this.prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
            this.weakListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs);
            this.prefs.addPreferenceChangeListener(this.weakListener);
            this.prefsListener.preferenceChange(null);
        }
    }

    public synchronized void reset() {
        if (this.baseWordInfo != null) {
            this.baseWordInfo = null;
            this.documents.clear();
            this.documentIndex = -1;
            this.documentSet.clear();
            this.wordSet.clear();
            this.wordInfoList.clear();
            this.wordInfoListIndex = 0;
        }
    }

    public synchronized void matchWord(int caretOffset, boolean forward) {
        int baseWordStartOffset;
        boolean searchWord;
        int baseWordEndOffset;
        if (this.baseWordInfo == null) {
            this.checkInitPrefs();
            int identStartOffset = EditorDocumentHandler.getIdentifierEnd(this.doc, caretOffset, true);
            CharSequence docText = DocumentUtilities.getText((Document)this.doc);
            String baseWord = docText.subSequence(identStartOffset, caretOffset).toString();
            baseWordStartOffset = identStartOffset;
            baseWordEndOffset = caretOffset;
            if (this.smartCase && !this.matchCase) {
                this.realMatchCase = false;
                for (int i = baseWord.length() - 1; i >= 0; --i) {
                    if (!Character.isUpperCase(baseWord.charAt(i))) continue;
                    this.realMatchCase = true;
                }
            } else {
                this.realMatchCase = this.matchCase;
            }
            if (!this.realMatchCase) {
                baseWord = baseWord.toLowerCase();
            }
            this.baseWordInfo = new WordInfo(baseWord);
            this.baseWordInfo.pos = this.createPosition(this.doc, identStartOffset);
            this.wordSet.add(baseWord);
            this.wordInfoList.add(this.baseWordInfo);
        } else {
            baseWordStartOffset = this.baseWordInfo.pos.getOffset();
            baseWordEndOffset = baseWordStartOffset + this.baseWordInfo.word.length();
        }
        WordInfo origWordInfo = this.wordInfoList.get(this.wordInfoListIndex);
        WordInfo wordInfo = null;
        boolean bl = forward ? this.wordInfoListIndex == this.wordInfoList.size() - 1 : (searchWord = this.wordInfoListIndex == 0);
        if (searchWord) {
            Document d = null;
            int searchStartOffset = 0;
            int searchEndOffset = 0;
            boolean searchRest = false;
            CharSequence docText = null;
            if (this.documentSet.isEmpty()) {
                d = this.doc;
                docText = DocumentUtilities.getText((Document)d);
                int offset = origWordInfo.pos.getOffset();
                if (forward) {
                    int endOffset = offset + origWordInfo.word.length();
                    if (offset >= baseWordStartOffset) {
                        searchStartOffset = endOffset;
                        searchEndOffset = docText.length();
                    } else {
                        searchStartOffset = endOffset;
                        searchEndOffset = baseWordStartOffset;
                        searchRest = true;
                    }
                } else if (offset <= baseWordStartOffset) {
                    searchStartOffset = 0;
                    searchEndOffset = offset;
                } else {
                    searchStartOffset = baseWordEndOffset;
                    searchEndOffset = offset;
                    searchRest = true;
                }
            }
            DocumentCharacterAcceptor charAcceptor = DocumentCharacterAcceptor.get(this.doc);
            do {
                int wordStartOffset;
                int wordEndOffset;
                char ch;
                if (d == null) {
                    d = this.getNextDocument();
                    if (d == null) break;
                    docText = DocumentUtilities.getText((Document)d);
                    searchStartOffset = 0;
                    searchEndOffset = docText.length();
                }
                wordStartOffset = -1;
                wordEndOffset = -1;
                if (forward || d != this.doc) {
                    while (searchStartOffset < searchEndOffset) {
                        if (charAcceptor.isIdentifier(ch = docText.charAt(searchStartOffset++))) {
                            if (wordStartOffset == -1) {
                                wordStartOffset = searchStartOffset - 1;
                            }
                            wordEndOffset = searchStartOffset;
                            continue;
                        }
                        if (wordStartOffset == -1) continue;
                        break;
                    }
                } else {
                    while (searchStartOffset < searchEndOffset) {
                        if (charAcceptor.isIdentifier(ch = docText.charAt(--searchEndOffset))) {
                            if (wordEndOffset == -1) {
                                wordEndOffset = searchEndOffset + 1;
                            }
                            wordStartOffset = searchEndOffset;
                            continue;
                        }
                        if (wordStartOffset == -1) continue;
                        break;
                    }
                }
                if (wordStartOffset != -1) {
                    if (!this.checkWord(docText, wordStartOffset, wordEndOffset - wordStartOffset)) continue;
                    String word = docText.subSequence(wordStartOffset, wordEndOffset).toString();
                    wordInfo = new WordInfo(word);
                    wordInfo.pos = this.createPosition(this.doc, wordStartOffset);
                    this.wordSet.add(word);
                    if (forward) {
                        this.wordInfoList.add(wordInfo);
                    } else {
                        this.wordInfoList.add(0, wordInfo);
                        ++this.wordInfoListIndex;
                    }
                    if (d != this.doc) continue;
                    break;
                }
                if (d == this.doc) {
                    if (forward) {
                        if (searchRest) {
                            d = null;
                            continue;
                        }
                        searchStartOffset = 0;
                        searchEndOffset = baseWordStartOffset;
                        searchRest = true;
                        continue;
                    }
                    if (searchRest) {
                        d = null;
                        continue;
                    }
                    searchStartOffset = baseWordEndOffset;
                    searchEndOffset = docText.length();
                    searchRest = true;
                    continue;
                }
                if (wordInfo != null) break;
                d = null;
            } while (true);
        }
        if (!searchWord || wordInfo != null) {
            this.wordInfoListIndex = forward ? ++this.wordInfoListIndex : --this.wordInfoListIndex;
        }
        if ((wordInfo = this.wordInfoList.get(this.wordInfoListIndex)) != null && wordInfo != origWordInfo) {
            try {
                int offset = baseWordStartOffset;
                int len = origWordInfo.word.length();
                if (this.doc.getLength() >= offset + len) {
                    String origWord = this.doc.getText(offset, len);
                    if (origWord.equals(origWordInfo.word)) {
                        this.doc.remove(offset, len);
                        this.doc.insertString(offset, wordInfo.word, null);
                        this.baseWordInfo.pos = this.doc.createPosition(offset);
                    } else {
                        LOG.info("Cannot replace word: origWord=\"" + CharSequenceUtilities.debugText((CharSequence)origWord) + "\" != \"" + CharSequenceUtilities.debugText((CharSequence)origWordInfo.word) + "\"\n");
                    }
                } else {
                    LOG.info("Cannot replace word: offset=" + offset + ", len=" + len + ", docLen=" + this.doc.getLength() + '\n');
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    private Position createPosition(Document doc, int offset) {
        try {
            return doc.createPosition(offset);
        }
        catch (BadLocationException ex) {
            throw new IndexOutOfBoundsException("Position creation failed at offset=" + offset + ", doc=" + doc + "\n" + ex.getLocalizedMessage());
        }
    }

    private boolean checkWord(CharSequence text, int index, int wordLen) {
        String baseWord = this.baseWordInfo.word;
        int baseWordLen = baseWord.length();
        if (baseWordLen > 0) {
            if (wordLen < baseWordLen) {
                return false;
            }
            for (int i = 0; i < baseWordLen; ++i) {
                char ch = text.charAt(index + i);
                if (!(this.realMatchCase ? ch != baseWord.charAt(i) : Character.toLowerCase(ch) != baseWord.charAt(i))) continue;
                return false;
            }
        }
        if (this.wordSet.get(text, index, index + wordLen) != null) {
            return false;
        }
        return true;
    }

    private Document getNextDocument() {
        if (this.documentIndex == this.documents.size() - 1) {
            if (this.documentSet.isEmpty()) {
                this.documentSet.put(this.doc, Boolean.TRUE);
            }
            for (JTextComponent tc : EditorRegistry.componentList()) {
                Document d = tc.getDocument();
                if (this.documentSet.containsKey(d)) continue;
                this.documentSet.put(d, Boolean.TRUE);
                this.documents.add(new WeakReference<Document>(d));
            }
        }
        Document retDoc = null;
        while (this.documentIndex < this.documents.size() - 1) {
            ++this.documentIndex;
            retDoc = this.documents.get(this.documentIndex).get();
            if (retDoc == null) continue;
        }
        return retDoc;
    }

    public String toString() {
        return "baseWordInfo=" + this.baseWordInfo + ", matchCase=" + this.matchCase + ", smartCase=" + this.smartCase + ", realMatchCase=" + this.realMatchCase + ", wordSet=" + this.wordSet + "\nwordInfoList=" + this.wordInfoList + "\nwordInfoListIndex=" + this.wordInfoListIndex;
    }

    private static final class WordInfo {
        final String word;
        Position pos;

        public WordInfo(String word) {
            this.word = word;
        }

        public String toString() {
            return "word=\"" + CharSequenceUtilities.debugText((CharSequence)this.word) + "\", pos=" + this.pos;
        }
    }

}

