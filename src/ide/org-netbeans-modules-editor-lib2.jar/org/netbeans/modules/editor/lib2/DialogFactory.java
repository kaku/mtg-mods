/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2;

import java.awt.Dialog;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

public interface DialogFactory {
    public Dialog createDialog(String var1, JPanel var2, boolean var3, JButton[] var4, boolean var5, int var6, int var7, ActionListener var8);
}

