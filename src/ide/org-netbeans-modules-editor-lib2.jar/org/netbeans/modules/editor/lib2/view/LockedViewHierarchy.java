/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Shape;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewDescriptor;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;

public final class LockedViewHierarchy {
    private ViewHierarchyImpl impl;
    private final DocumentView docView;

    LockedViewHierarchy(ViewHierarchyImpl impl) {
        assert (impl != null);
        this.impl = impl;
        this.docView = impl.getDocumentView();
        if (this.docView != null) {
            this.docView.lock();
        }
    }

    public void unlock() {
        this.checkValid();
        if (this.docView != null) {
            this.docView.unlock();
        }
        this.impl = null;
    }

    @NonNull
    public JTextComponent getTextComponent() {
        this.checkValid();
        return this.impl.textComponent();
    }

    public double modelToY(int offset) {
        this.checkValid();
        return this.impl.modelToY(this.docView, offset);
    }

    public double[] modelToY(int[] offsets) {
        this.checkValid();
        return this.impl.modelToY(this.docView, offsets);
    }

    public Shape modelToView(int offset, Position.Bias bias) {
        this.checkValid();
        return this.impl.modelToView(this.docView, offset, bias);
    }

    public int viewToModel(double x, double y, Position.Bias[] biasReturn) {
        this.checkValid();
        return this.impl.viewToModel(this.docView, x, y, biasReturn);
    }

    public int modelToParagraphViewIndex(int offset) {
        this.checkValid();
        return this.impl.modelToParagraphViewIndex(this.docView, offset);
    }

    public int yToParagraphViewIndex(double y) {
        this.checkValid();
        return this.impl.yToParagraphViewIndex(this.docView, y);
    }

    public ParagraphViewDescriptor getParagraphViewDescriptor(int paragraphViewIndex) {
        this.checkValid();
        return this.impl.verifyParagraphViewIndexValid(this.docView, paragraphViewIndex) ? new ParagraphViewDescriptor(this.docView, paragraphViewIndex) : null;
    }

    public int getParagraphViewCount() {
        this.checkValid();
        return this.impl.getParagraphViewCount(this.docView);
    }

    public float getDefaultRowHeight() {
        this.checkValid();
        return this.impl.getDefaultRowHeight(this.docView);
    }

    public float getDefaultCharWidth() {
        this.checkValid();
        return this.impl.getDefaultCharWidth(this.docView);
    }

    public boolean isActive() {
        this.checkValid();
        return this.impl.isActive(this.docView);
    }

    private void checkValid() {
        if (this.impl == null) {
            throw new IllegalStateException("Inactive LockedViewHierarchy: unlock() already called.");
        }
    }
}

