/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib2;

import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.modules.editor.lib2.highlighting.AbstractOffsetGapList;
import org.netbeans.modules.editor.lib2.highlighting.OffsetGapList;
import org.openide.util.WeakListeners;

public final class WeakPositions {
    private static final Map<Document, OffsetGapList<WeakP>> OGLS = new WeakHashMap<Document, OffsetGapList<WeakP>>();
    private static final DocumentListener documentsTracker = new DocumentListener(){

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void insertUpdate(DocumentEvent e) {
            Map map = OGLS;
            synchronized (map) {
                OffsetGapList<WeakP> ogl = this.getOgl(e);
                if (ogl != null) {
                    ogl.defaultInsertUpdate(e.getOffset(), e.getLength());
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void removeUpdate(DocumentEvent e) {
            Map map = OGLS;
            synchronized (map) {
                OffsetGapList<WeakP> ogl = this.getOgl(e);
                if (ogl != null) {
                    ogl.defaultRemoveUpdate(e.getOffset(), e.getLength());
                }
            }
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        private OffsetGapList<WeakP> getOgl(DocumentEvent e) {
            Document doc = e.getDocument();
            return (OffsetGapList)((Object)OGLS.get(doc));
        }
    };

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Position get(Document doc, int offset) throws BadLocationException {
        doc.createPosition(offset);
        Map<Document, OffsetGapList<WeakP>> map = OGLS;
        synchronized (map) {
            int index;
            WeakP pos;
            AbstractOffsetGapList ogl = OGLS.get(doc);
            if (ogl == null) {
                ogl = new OffsetGapList();
                OGLS.put(doc, (OffsetGapList<WeakP>)ogl);
                doc.addDocumentListener(WeakListeners.document((DocumentListener)documentsTracker, (Object)doc));
            }
            WeakP weakP = pos = (index = ogl.findElementIndex(offset)) >= 0 ? (WeakP)ogl.get(index) : null;
            if (pos == null) {
                pos = new WeakP(offset);
                ogl.add(pos);
            }
            return pos;
        }
    }

    private WeakPositions() {
    }

    private static final class WeakP
    extends OffsetGapList.Offset
    implements Position {
        public WeakP(int offset) {
            super(offset);
        }
    }

}

