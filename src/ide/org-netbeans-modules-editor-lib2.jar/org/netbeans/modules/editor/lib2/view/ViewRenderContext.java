/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.FontInfo;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public final class ViewRenderContext {
    private final DocumentView docView;

    ViewRenderContext(DocumentView docView) {
        this.docView = docView;
    }

    public FontRenderContext getFontRenderContext() {
        return this.docView.getFontRenderContext();
    }

    public HighlightsSequence getPaintHighlights(EditorView view, int shift) {
        return this.docView.getPaintHighlights(view, shift);
    }

    public float getDefaultRowHeight() {
        return this.docView.op.getDefaultRowHeight();
    }

    public float getDefaultAscent() {
        return this.docView.op.getDefaultAscent();
    }

    public Font getRenderFont(Font font) {
        return this.docView.op.getFontInfo((Font)font).renderFont;
    }
}

