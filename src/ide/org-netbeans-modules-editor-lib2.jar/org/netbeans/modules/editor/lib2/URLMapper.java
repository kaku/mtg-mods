/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 */
package org.netbeans.modules.editor.lib2;

import java.net.URL;
import java.util.Collection;
import javax.swing.text.JTextComponent;
import org.openide.util.Lookup;

public abstract class URLMapper {
    private static final String LOCK = new String("URLMapper.LOCK");
    private static Lookup.Result<URLMapper> mappers = null;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static JTextComponent findTextComponenet(URL url) {
        String string = LOCK;
        synchronized (string) {
            Collection<? extends URLMapper> mappers = URLMapper.getMappers();
            for (URLMapper m : mappers) {
                JTextComponent jtc = m.getTextComponent(url);
                if (jtc == null) continue;
                return jtc;
            }
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static URL findUrl(JTextComponent component) {
        String string = LOCK;
        synchronized (string) {
            Collection<? extends URLMapper> mappers = URLMapper.getMappers();
            for (URLMapper m : mappers) {
                URL url = m.getUrl(component);
                if (url == null) continue;
                return url;
            }
            return null;
        }
    }

    protected URLMapper() {
    }

    protected abstract JTextComponent getTextComponent(URL var1);

    protected abstract URL getUrl(JTextComponent var1);

    private static Collection<? extends URLMapper> getMappers() {
        if (mappers == null) {
            mappers = Lookup.getDefault().lookupResult(URLMapper.class);
        }
        return mappers.allInstances();
    }
}

