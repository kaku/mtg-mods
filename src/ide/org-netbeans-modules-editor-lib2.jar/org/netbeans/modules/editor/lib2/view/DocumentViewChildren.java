/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsList;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsReader;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.TextLayoutCache;
import org.netbeans.modules.editor.lib2.view.ViewChildren;
import org.netbeans.modules.editor.lib2.view.ViewGapStorage;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewPaintHighlights;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;

public class DocumentViewChildren
extends ViewChildren<ParagraphView> {
    private static final Logger LOG = Logger.getLogger(DocumentViewChildren.class.getName());
    protected static final double EXTEND_TO_END = 1.073741823E9;
    private static final long serialVersionUID = 0;
    private ViewPaintHighlights viewPaintHighlights;
    private float childrenWidth;
    private float baseY;

    DocumentViewChildren(int capacity) {
        super(capacity);
    }

    float width() {
        return this.childrenWidth;
    }

    float height() {
        return (float)this.startVisualOffset(this.size());
    }

    float getBaseY() {
        return this.baseY;
    }

    void setBaseY(float baseY) {
        this.baseY = baseY;
    }

    double getY(int pIndex) {
        return (double)this.baseY + this.startVisualOffset(pIndex);
    }

    double[] replace(DocumentView docView, int index, int removeCount, View[] addedViews) {
        if (index + removeCount > this.size()) {
            throw new IllegalArgumentException("index=" + index + ", removeCount=" + removeCount + ", viewCount=" + this.size());
        }
        int endAddedIndex = index;
        int removeEndIndex = index + removeCount;
        double startYR = this.startVisualOffset(index);
        double origEndYR = removeCount == 0 ? startYR : this.endVisualOffset(removeEndIndex - 1);
        double endYR = startYR;
        this.moveVisualGap(removeEndIndex, origEndYR);
        if (removeCount != 0) {
            TextLayoutCache tlCache = docView.op.getTextLayoutCache();
            for (int i = removeCount - 1; i >= 0; --i) {
                tlCache.remove((ParagraphView)this.get(index + i), false);
            }
            this.remove(index, removeCount);
        }
        if (addedViews != null && addedViews.length != 0) {
            endAddedIndex = index + addedViews.length;
            this.addArray(index, (Object[])addedViews);
            for (int i = 0; i < addedViews.length; ++i) {
                ParagraphView view = (ParagraphView)addedViews[i];
                view.setParent(docView);
                view.setRawEndVisualOffset(endYR += (double)view.getPreferredSpan(1));
            }
        }
        double deltaY = endYR - origEndYR;
        this.heightChangeUpdate(endAddedIndex, endYR, deltaY);
        if (deltaY != 0.0) {
            docView.op.notifyHeightChange();
        }
        return new double[]{(double)this.baseY + startYR, (double)this.baseY + origEndYR, deltaY};
    }

    private void heightChangeUpdate(int endIndex, double endYR, double deltaY) {
        if (this.gapStorage != null) {
            this.gapStorage.visualGapStart = endYR;
            this.gapStorage.visualGapLength -= deltaY;
            this.gapStorage.visualGapIndex = endIndex;
        } else if (deltaY != 0.0) {
            int pCount = this.size();
            if (pCount > 20) {
                this.gapStorage = new ViewGapStorage();
                this.gapStorage.initVisualGap(endIndex, endYR);
                deltaY += this.gapStorage.visualGapLength;
            }
            while (endIndex < pCount) {
                EditorView view = (EditorView)this.get(endIndex);
                view.setRawEndVisualOffset(view.getRawEndVisualOffset() + deltaY);
                ++endIndex;
            }
        }
    }

    void childWidthUpdated(DocumentView docView, int index, float newWidth) {
        if (newWidth > this.childrenWidth) {
            this.childrenWidth = newWidth;
            docView.op.notifyWidthChange();
        }
    }

    void childHeightUpdated(DocumentView docView, int index, float newHeight, Rectangle2D pViewRect) {
        double startYR = this.startVisualOffset(index);
        double endYR = this.endVisualOffset(index);
        double deltaY = (double)newHeight - (endYR - startYR);
        if (deltaY != 0.0) {
            ParagraphView pView = (ParagraphView)this.get(index);
            this.moveVisualGap(++index, endYR);
            pView.setRawEndVisualOffset(endYR += deltaY);
            this.heightChangeUpdate(index, endYR, deltaY);
            docView.validChange().addChangeY(pViewRect.getY(), pViewRect.getMaxY(), deltaY);
            docView.op.notifyHeightChange();
        }
    }

    Shape getChildAllocation(DocumentView docView, int index, Shape docViewAlloc) {
        Rectangle2D.Double mutableBounds = ViewUtils.shape2Bounds(docViewAlloc);
        double startYR = this.startVisualOffset(index);
        double endYR = this.endVisualOffset(index);
        mutableBounds.y += (double)this.baseY + startYR;
        mutableBounds.height = endYR - startYR;
        return mutableBounds;
    }

    int viewIndexFirstByStartOffset(int offset, int low) {
        int high = this.size() - 1;
        if (high == -1) {
            return -1;
        }
        while (low <= high) {
            int mid = low + high >>> 1;
            int midStartOffset = ((ParagraphView)this.get(mid)).getStartOffset();
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            while (mid > 0) {
                if ((midStartOffset = ((ParagraphView)this.get(--mid)).getStartOffset()) >= offset) continue;
                ++mid;
                break;
            }
            high = mid;
            break;
        }
        return Math.max(high, 0);
    }

    public int viewIndexAtY(double y, Shape alloc) {
        Rectangle2D allocRect = ViewUtils.shapeAsRect(alloc);
        return this.viewIndexFirstVisual(y -= allocRect.getY() + (double)this.baseY, this.size());
    }

    boolean ensureParagraphViewChildrenValid(DocumentView docView, int pIndex, ParagraphView pView) {
        if (!pView.isChildrenValid()) {
            this.ensureParagraphsChildrenAndLayoutValid(docView, pIndex, pIndex + 1, 0, 5);
            pView = (ParagraphView)this.get(pIndex);
            assert (pView.isChildrenValid());
            return true;
        }
        return false;
    }

    public Shape modelToViewChecked(DocumentView docView, int offset, Shape docViewAlloc, Position.Bias bias) {
        int pIndex = this.viewIndexFirstByStartOffset(offset, 0);
        Shape ret = docViewAlloc;
        if (pIndex >= 0) {
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            Shape pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            if (pView.isChildrenNull()) {
                Rectangle2D.Double pRect = ViewUtils.shape2Bounds(pAlloc);
                pRect.width = docView.op.getDefaultCharWidth();
                ret = pRect;
            } else {
                if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                    pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
                }
                docView.op.getTextLayoutCache().activate(pView);
                ret = pView.modelToViewChecked(offset, pAlloc, bias);
            }
        }
        return ret;
    }

    public int viewToModelChecked(DocumentView docView, double x, double y, Shape docViewAlloc, Position.Bias[] biasReturn) {
        int pIndex = this.viewIndexAtY(y, docViewAlloc);
        int offset = 0;
        if (pIndex >= 0) {
            Shape pAlloc;
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            if (this.ensureParagraphViewChildrenValid(docView, pIndex, pView)) {
                pView = (ParagraphView)this.get(pIndex);
            }
            if (pView.checkLayoutUpdate(pIndex, pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc))) {
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            docView.op.getTextLayoutCache().activate(pView);
            offset = x == 0.0 && !pView.children.isWrapped() ? pView.getStartOffset() : pView.viewToModelChecked(x, y, pAlloc, biasReturn);
        } else {
            offset = docView.getStartOffset();
        }
        return offset;
    }

    int getNextVisualPositionY(DocumentView docView, int offset, Position.Bias bias, Shape docViewAlloc, boolean southDirection, Position.Bias[] biasRet) {
        double x = HighlightsViewUtils.getMagicX(docView, docView, offset, bias, docViewAlloc);
        int viewCount = this.size();
        int increment = southDirection ? 1 : -1;
        int retOffset = -1;
        for (int pIndex = docView.getViewIndex((int)offset, (Position.Bias)bias); retOffset == -1 && pIndex >= 0 && pIndex < viewCount; pIndex += increment) {
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            Shape pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            if (this.ensureParagraphViewChildrenValid(docView, pIndex, pView)) {
                pView = (ParagraphView)this.get(pIndex);
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            docView.op.getTextLayoutCache().activate(pView);
            retOffset = pView.children.getNextVisualPositionY(pView, offset, bias, pAlloc, southDirection, biasRet, x);
            if (retOffset != -1) continue;
            offset = -1;
        }
        return retOffset;
    }

    int getNextVisualPositionX(DocumentView docView, int offset, Position.Bias bias, Shape docViewAlloc, boolean eastDirection, Position.Bias[] biasRet) {
        int viewCount = this.size();
        int increment = eastDirection ? 1 : -1;
        int retOffset = -1;
        for (int pIndex = docView.getViewIndex((int)offset, (Position.Bias)bias); retOffset == -1 && pIndex >= 0 && pIndex < viewCount; pIndex += increment) {
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            Shape pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            if (this.ensureParagraphViewChildrenValid(docView, pIndex, pView)) {
                pView = (ParagraphView)this.get(pIndex);
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            if ((retOffset = pView.children.getNextVisualPositionX(pView, offset, bias, pAlloc, eastDirection, biasRet)) != -1) continue;
            offset = -1;
        }
        return retOffset;
    }

    public String getToolTipTextChecked(DocumentView docView, double x, double y, Shape docViewAlloc) {
        int pIndex = this.viewIndexAtY(y, docViewAlloc);
        String toolTipText = null;
        if (pIndex >= 0) {
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            if (pView.isChildrenNull()) {
                return null;
            }
            Shape pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            docView.op.getTextLayoutCache().activate(pView);
            toolTipText = pView.getToolTipTextChecked(x, y, pAlloc);
        }
        return toolTipText;
    }

    public JComponent getToolTip(DocumentView docView, double x, double y, Shape docViewAlloc) {
        int pIndex = this.viewIndexAtY(y, docViewAlloc);
        JComponent toolTip = null;
        if (pIndex >= 0) {
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            if (pView.isChildrenNull()) {
                return null;
            }
            Shape pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                pAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            }
            docView.op.getTextLayoutCache().activate(pView);
            Shape childAlloc = this.getChildAllocation(docView, pIndex, docViewAlloc);
            toolTip = pView.getToolTip(x, y, childAlloc);
        }
        return toolTip;
    }

    void ensureLayoutValidForInitedChildren(DocumentView docView) {
        int pCount = this.size();
        for (int pIndex = 0; pIndex < pCount; ++pIndex) {
            ParagraphView pView = (ParagraphView)this.get(pIndex);
            if (pView.isChildrenNull() || pView.isLayoutValid()) continue;
            Shape pAlloc = docView.getChildAllocation(pIndex);
            pView.updateLayoutAndScheduleRepaint(pIndex, pAlloc);
        }
    }

    boolean ensureParagraphsChildrenAndLayoutValid(DocumentView docView, int startIndex, int endIndex, int extraStartCount, int extraEndCount) {
        int pCount = this.size();
        assert (startIndex < endIndex);
        assert (endIndex <= pCount);
        int rStartIndex = startIndex;
        int rEndIndex = endIndex;
        boolean updated = false;
        TextLayoutCache tlCache = docView.op.getTextLayoutCache();
        if (pCount > 0) {
            ParagraphView pView;
            tlCache.setCapacityOrDefault(endIndex - startIndex + extraStartCount + extraEndCount);
            pView = (ParagraphView)this.get(rStartIndex);
            if (pView.isChildrenValid()) {
                tlCache.activate(pView);
                while (++rStartIndex < rEndIndex && (pView = (ParagraphView)this.get(rStartIndex)).isChildrenValid()) {
                    tlCache.activate(pView);
                }
            } else {
                while (rStartIndex > 0 && extraStartCount > 0) {
                    if ((pView = (ParagraphView)this.get(--rStartIndex)).isChildrenValid()) {
                        tlCache.activate(pView);
                        ++rStartIndex;
                        break;
                    }
                    --extraStartCount;
                }
            }
            if (rStartIndex < rEndIndex) {
                pView = (ParagraphView)this.get(rEndIndex - 1);
                if (pView.isChildrenValid()) {
                    tlCache.activate(pView);
                    --rEndIndex;
                    while (rEndIndex > rStartIndex && (pView = (ParagraphView)this.get(rEndIndex - 1)).isChildrenValid()) {
                        tlCache.activate(pView);
                        --rEndIndex;
                    }
                } else {
                    while (rEndIndex < pCount && extraEndCount > 0) {
                        if ((pView = (ParagraphView)this.get(rEndIndex++)).isChildrenValid()) {
                            tlCache.activate(pView);
                            break;
                        }
                        --extraEndCount;
                    }
                }
                docView.op.initParagraphs(rStartIndex, rEndIndex);
                updated = true;
                endIndex = Math.min(endIndex, docView.getViewCount());
            }
            Rectangle2D docViewRect = docView.getAllocation();
            for (int pIndex = startIndex; pIndex < endIndex; ++pIndex) {
                pView = (ParagraphView)this.get(pIndex);
                if (!pView.isChildrenValid()) {
                    updated = true;
                    break;
                }
                if (pView.isLayoutValid()) continue;
                Shape pAlloc = docView.getChildAllocation(pIndex, docViewRect);
                pView.updateLayoutAndScheduleRepaint(pIndex, ViewUtils.shapeAsRect(pAlloc));
                updated = true;
            }
        }
        return updated;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void paint(DocumentView docView, Graphics2D g, Shape docViewAlloc, Rectangle clipBounds) {
        if (this.size() > 0) {
            boolean logPaintTime;
            int startIndex;
            int endIndex;
            long nanoTime;
            int phEndOffset;
            double startY = clipBounds.y;
            double endY = clipBounds.getMaxY();
            if (ViewHierarchyImpl.PAINT_LOG.isLoggable(Level.FINE)) {
                ViewHierarchyImpl.PAINT_LOG.fine("\nDocumentViewChildren.paint(): clipBounds: " + ViewUtils.toString(clipBounds) + "\n");
            }
            do {
                startIndex = this.viewIndexAtY(startY, docViewAlloc);
                endIndex = this.viewIndexAtY(endY - 0.1, docViewAlloc) + 1;
                if (!ViewHierarchyImpl.PAINT_LOG.isLoggable(Level.FINE)) continue;
                ViewHierarchyImpl.PAINT_LOG.fine("  paint:docView:[" + startIndex + "," + endIndex + "] for y:<" + startY + "," + endY + ">\n");
            } while (this.ensureParagraphsChildrenAndLayoutValid(docView, startIndex, endIndex, 5, 5));
            logPaintTime = ViewHierarchyImpl.PAINT_LOG.isLoggable(Level.FINE);
            nanoTime = 0;
            if (logPaintTime) {
                nanoTime = System.nanoTime();
            }
            int startOffset = ((ParagraphView)this.get(startIndex)).getStartOffset();
            int endOffset = ((ParagraphView)this.get(endIndex - 1)).getEndOffset();
            HighlightsList paintHighlights = null;
            int maxPHReads = 10;
            do {
                HighlightsContainer phContainer = HighlightingManager.getInstance(docView.getTextComponent()).getTopHighlights();
                final boolean[] phStale = new boolean[1];
                HighlightsChangeListener hChangeListener = new HighlightsChangeListener(){

                    @Override
                    public void highlightChanged(HighlightsChangeEvent event) {
                        phStale[0] = true;
                    }
                };
                phContainer.addHighlightsChangeListener(hChangeListener);
                try {
                    HighlightsReader reader = new HighlightsReader(phContainer, startOffset, endOffset);
                    reader.readUntil(endOffset);
                    paintHighlights = reader.highlightsList();
                    if (!phStale[0]) break;
                    phStale[0] = false;
                }
                finally {
                    phContainer.removeHighlightsChangeListener(hChangeListener);
                }
            } while (--maxPHReads >= 0);
            assert ((phEndOffset = paintHighlights.endOffset()) == endOffset);
            this.viewPaintHighlights = new ViewPaintHighlights(paintHighlights);
            try {
                for (int i = startIndex; i < endIndex; ++i) {
                    ParagraphView pView = (ParagraphView)this.get(i);
                    Shape childAlloc = this.getChildAllocation(docView, i, docViewAlloc);
                    if (ViewHierarchyImpl.PAINT_LOG.isLoggable(Level.FINER)) {
                        ViewHierarchyImpl.PAINT_LOG.finer("    pView[" + i + "]: pAlloc=" + ViewUtils.toString(childAlloc) + "\n");
                    }
                    pView.paint(g, childAlloc, clipBounds);
                }
            }
            finally {
                this.viewPaintHighlights = null;
            }
            if (logPaintTime) {
                nanoTime = System.nanoTime() - nanoTime;
                ViewHierarchyImpl.PAINT_LOG.fine("Painted " + (endIndex - startIndex) + " lines <" + startIndex + "," + endIndex + "> in " + (double)nanoTime / 1000000.0 + " ms\n");
                if (ViewHierarchyImpl.PAINT_LOG.isLoggable(Level.FINEST)) {
                    ViewHierarchyImpl.PAINT_LOG.log(Level.FINE, "----- PAINT FINISHED -----", new Exception("Cause of just performed paint"));
                }
            }
        }
    }

    ViewPaintHighlights getPaintHighlights(EditorView view, int shift) {
        assert (this.viewPaintHighlights != null);
        this.viewPaintHighlights.reset(view, shift);
        return this.viewPaintHighlights;
    }

    void markChildrenLayoutInvalid() {
        int viewCount = this.size();
        for (int i = 0; i < viewCount; ++i) {
            ParagraphView pView = (ParagraphView)this.get(i);
            pView.markLayoutInvalid();
        }
    }

    @Override
    protected String checkSpanIntegrity(double span, ParagraphView view) {
        String err = null;
        float prefSpan = view.getHeight();
        if (span != (double)prefSpan) {
            err = "PVChildren: span=" + span + " != prefSpan=" + prefSpan;
        }
        return err;
    }

    public StringBuilder appendChildrenInfo(DocumentView docView, StringBuilder sb, int indent, int importantIndex) {
        return this.appendChildrenInfo(sb, indent, importantIndex);
    }

    @Override
    protected String getXYInfo(int index) {
        return new StringBuilder(10).append(" y=").append(this.getY(index)).toString();
    }

}

