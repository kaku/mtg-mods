/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import javax.swing.text.AttributeSet;
import org.netbeans.modules.editor.lib2.highlighting.HighlightItem;
import org.netbeans.modules.editor.lib2.highlighting.HighlightsList;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public final class HighlightsReader {
    private final HighlightsList highlightsList;
    private final HighlightsSequence highlightsSequence;
    private final int endOffset;

    public HighlightsReader(HighlightsContainer highlightsContainer, int startOffset, int endOffset) {
        this.highlightsSequence = highlightsContainer.getHighlights(startOffset, endOffset);
        this.highlightsList = new HighlightsList(startOffset);
        this.endOffset = endOffset;
    }

    public HighlightsSequence highlightsSequence() {
        return this.highlightsSequence;
    }

    public HighlightsList highlightsList() {
        return this.highlightsList;
    }

    public void readUntil(int offset) {
        int hlEndOffset = this.highlightsList.endOffset();
        while (this.highlightsSequence.moveNext()) {
            int hlStartOffset = this.highlightsSequence.getStartOffset();
            if (hlStartOffset > hlEndOffset) {
                this.highlightsList.add(new HighlightItem(hlStartOffset, null));
            }
            hlEndOffset = this.highlightsSequence.getEndOffset();
            this.highlightsList.add(new HighlightItem(hlEndOffset, this.highlightsSequence.getAttributes()));
            if (hlEndOffset < offset) continue;
            return;
        }
        if (hlEndOffset < this.endOffset) {
            this.highlightsList.add(new HighlightItem(this.endOffset, null));
        }
    }
}

