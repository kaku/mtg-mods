/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.Element;
import javax.swing.text.Position;
import org.netbeans.modules.editor.lib2.document.AbstractPositionElement;

public final class ModElement
extends AbstractPositionElement {
    public static final String NAME = "mod";

    ModElement(Element parent, Position startPos, Position endPos) {
        super(parent, startPos, endPos);
    }

    ModElement(Element parent, int startOffset, int endOffset) {
        super(parent, startOffset, endOffset);
    }

    @Override
    public String getName() {
        return "mod";
    }
}

