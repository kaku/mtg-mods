/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Position;
import javax.swing.text.TabExpander;
import javax.swing.text.TabableView;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsView;
import org.netbeans.modules.editor.lib2.view.LineWrapType;
import org.netbeans.modules.editor.lib2.view.NewlineView;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.TextLayoutUtils;
import org.netbeans.modules.editor.lib2.view.ViewChildren;
import org.netbeans.modules.editor.lib2.view.ViewGapStorage;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewPart;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.modules.editor.lib2.view.WrapInfo;
import org.netbeans.modules.editor.lib2.view.WrapInfoUpdater;
import org.netbeans.modules.editor.lib2.view.WrapLine;

final class ParagraphViewChildren
extends ViewChildren<EditorView> {
    private static final Logger LOG = Logger.getLogger(ParagraphViewChildren.class.getName());
    private static final long serialVersionUID = 0;
    private WrapInfo wrapInfo;
    private float childrenHeight;
    private int startInvalidChildrenLocalOffset;
    private int endInvalidChildrenLocalOffset;

    public ParagraphViewChildren(int capacity) {
        super(capacity);
    }

    boolean isWrapped() {
        return this.wrapInfo != null;
    }

    float height() {
        return this.wrapInfo == null ? this.childrenHeight : this.wrapInfo.height(this);
    }

    float width() {
        return this.wrapInfo == null ? (float)this.childrenWidth() : this.wrapInfo.width();
    }

    double childrenWidth() {
        return this.startVisualOffset(this.size());
    }

    float childrenHeight() {
        return this.childrenHeight;
    }

    int length() {
        return this.startOffset(this.size());
    }

    Shape getChildAllocation(int index, Shape alloc) {
        Rectangle2D.Double mutableBounds = ViewUtils.shape2Bounds(alloc);
        double startX = this.startVisualOffset(index);
        double endX = this.endVisualOffset(index);
        mutableBounds.x += startX;
        mutableBounds.width = endX - startX;
        mutableBounds.height = this.childrenHeight;
        return mutableBounds;
    }

    int getViewIndex(ParagraphView pView, int offset) {
        return this.viewIndexFirst(offset -= pView.getStartOffset());
    }

    int getViewIndex(ParagraphView pView, double x, double y, Shape pAlloc) {
        IndexAndAlloc indexAndAlloc = this.findIndexAndAlloc(pView, x, y, pAlloc);
        return indexAndAlloc.index;
    }

    int viewIndexNoWrap(ParagraphView pView, double x, Shape pAlloc) {
        return this.viewIndexFirstVisual(x, this.size());
    }

    void replace(ParagraphView pView, int index, int removeCount, View[] addedViews) {
        int addedViewsLength;
        int viewCount;
        if (index + removeCount > this.size()) {
            throw new IllegalArgumentException("index=" + index + ", removeCount=" + removeCount + ", viewCount=" + this.size());
        }
        int n = addedViewsLength = addedViews != null ? addedViews.length : 0;
        if (removeCount == 0 && addedViewsLength == 0) {
            return;
        }
        int removeEndIndex = index + removeCount;
        int addEndIndex = index + addedViewsLength;
        int relEndOffset = this.startOffset(index);
        int removeEndRelOffset = removeCount == 0 ? relEndOffset : this.endOffset(removeEndIndex - 1);
        this.moveOffsetGap(removeEndIndex, removeEndRelOffset);
        double endX = this.startVisualOffset(index);
        double removeEndX = removeCount == 0 ? endX : this.endVisualOffset(removeEndIndex - 1);
        this.moveVisualGap(removeEndIndex, endX);
        boolean tabableViewsAboveAddedViews = pView.containsTabableViews();
        DocumentView docView = pView.getDocumentView();
        if (removeCount != 0) {
            this.remove(index, removeCount);
        }
        if (addedViewsLength > 0) {
            this.addArray(index, (Object[])addedViews);
            CharSequence docText = null;
            int pViewOffset = pView.getStartOffset();
            boolean nonPrintableCharsVisible = false;
            boolean tabViewAdded = false;
            for (int i = 0; i < addedViews.length; ++i) {
                HighlightsView hView;
                double width;
                EditorView view = (EditorView)addedViews[i];
                int viewLen = view.getLength();
                view.setRawEndOffset(relEndOffset += viewLen);
                view.setParent(pView);
                if (view instanceof HighlightsView && (hView = (HighlightsView)view).getTextLayout() == null) {
                    String text;
                    if (docText == null) {
                        docText = DocumentUtilities.getText((Document)docView.getDocument());
                        nonPrintableCharsVisible = docView.op.isNonPrintableCharactersVisible();
                    }
                    int startOffset = pViewOffset + relEndOffset - viewLen;
                    String tlText = text = docText.subSequence(startOffset, startOffset + viewLen).toString();
                    if (nonPrintableCharsVisible) {
                        tlText = text.replace(' ', '\u00b7');
                    }
                    Font font = ViewUtils.getFont(hView.getAttributes(), docView.op.getDefaultFont());
                    TextLayout textLayout = docView.op.createTextLayout(tlText, font);
                    float width2 = TextLayoutUtils.getWidth(textLayout, tlText, font);
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("PVChildren.replace(): Width of hView-Id=" + hView.getDumpId() + ", startOffset=" + hView.getStartOffset() + ", width=" + width2 + ", text='" + CharSequenceUtilities.debugText((CharSequence)text) + "', font=" + font + "\n");
                    }
                    hView.setTextLayout(textLayout, width2);
                    if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINE)) {
                        docView.getTextLayoutVerifier().put(textLayout, text);
                    }
                }
                if (view instanceof TabableView) {
                    width = ((TabableView)((Object)view)).getTabbedSpan((float)endX, docView.getTabExpander());
                    tabViewAdded = true;
                } else {
                    width = view.getPreferredSpan(0);
                }
                width = Math.ceil(width);
                view.setRawEndVisualOffset(endX += width);
                float height = view.getPreferredSpan(1);
                if (height <= this.childrenHeight) continue;
                width = Math.ceil(width);
                this.childrenHeight = height;
            }
            if (tabViewAdded) {
                pView.markContainsTabableViews();
            }
        }
        int offsetDelta = relEndOffset - removeEndRelOffset;
        boolean updateAboveAddedViews = true;
        if (this.gapStorage != null) {
            this.gapStorage.offsetGapStart = relEndOffset;
            this.gapStorage.offsetGapLength -= offsetDelta;
            this.gapStorage.visualGapIndex = addEndIndex;
            this.gapStorage.visualGapStart = endX;
            this.gapStorage.visualGapLength -= endX - removeEndX;
            offsetDelta = 0;
            updateAboveAddedViews = false;
        } else {
            viewCount = this.size();
            if ((index > 0 || removeCount > 0) && viewCount > 20) {
                this.gapStorage = new ViewGapStorage();
                this.gapStorage.initOffsetGap(relEndOffset);
                this.gapStorage.initVisualGap(addEndIndex, endX);
                offsetDelta += this.gapStorage.offsetGapLength;
            }
        }
        if (tabableViewsAboveAddedViews || updateAboveAddedViews) {
            viewCount = this.size();
            for (int i = addEndIndex; i < viewCount; ++i) {
                EditorView view = (EditorView)this.get(i);
                if (offsetDelta != 0) {
                    view.setRawEndOffset(view.getRawEndOffset() + offsetDelta);
                }
                float width = tabableViewsAboveAddedViews && view instanceof TabableView ? ((TabableView)((Object)view)).getTabbedSpan((float)endX, docView.getTabExpander()) : view.getPreferredSpan(0);
                double rawEndX = this.gapStorage != null ? endX + this.gapStorage.visualGapLength : (endX += (double)width);
                view.setRawEndVisualOffset(rawEndX);
                float height = view.getPreferredSpan(1);
                if (height <= this.childrenHeight) continue;
                this.childrenHeight = height;
            }
        }
        pView.markLayoutInvalid();
        int newLength = this.getLength();
        if (newLength != pView.getLength()) {
            if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINER)) {
                ViewHierarchyImpl.SPAN_LOG.finer(pView.getDumpId() + ": update length: " + pView.getLength() + " => " + newLength + "\n");
            }
            pView.setLength(newLength);
        }
    }

    int getStartInvalidChildrenLocalOffset() {
        return this.startInvalidChildrenLocalOffset;
    }

    int getEndInvalidChildrenLocalOffset() {
        return this.endInvalidChildrenLocalOffset;
    }

    void setInvalidChildrenLocalRange(int startInvalidChildrenLocalOffset, int endInvalidChildrenLocalOffset) {
        this.startInvalidChildrenLocalOffset = startInvalidChildrenLocalOffset;
        this.endInvalidChildrenLocalOffset = endInvalidChildrenLocalOffset;
    }

    void fixSpans(ParagraphView pView, int startIndex, int endIndex) {
        double startX = this.startVisualOffset(startIndex);
        double endX = this.startVisualOffset(endIndex);
        this.moveVisualGap(endIndex, endX);
        double x = startX;
        DocumentView docView = pView.getDocumentView();
        boolean containsTabableViews = pView.containsTabableViews();
        for (int i = startIndex; i < endIndex; ++i) {
            EditorView view = (EditorView)this.get(i);
            float width = containsTabableViews && view instanceof TabableView ? ((TabableView)((Object)view)).getTabbedSpan((float)x, docView.getTabExpander()) : view.getPreferredSpan(0);
            view.setRawEndVisualOffset(x += (double)width);
            float height = view.getPreferredSpan(1);
            if (height <= this.childrenHeight) continue;
            this.childrenHeight = height;
        }
        double deltaX = x - endX;
        if (deltaX != 0.0) {
            if (containsTabableViews || this.gapStorage == null) {
                int viewCount = this.size();
                for (int i2 = endIndex; i2 < viewCount; ++i2) {
                    EditorView view = (EditorView)this.get(i2);
                    float width = containsTabableViews && view instanceof TabableView ? ((TabableView)((Object)view)).getTabbedSpan((float)x, docView.getTabExpander()) : view.getPreferredSpan(0);
                    double rawEndX = this.gapStorage != null ? x + this.gapStorage.visualGapLength : (x += (double)width);
                    view.setRawEndVisualOffset(rawEndX);
                }
            } else {
                this.gapStorage.visualGapLength -= deltaX;
            }
        }
        pView.markLayoutInvalid();
    }

    void updateLayout(DocumentView docView, ParagraphView pView) {
        if (this.wrapInfo != null || this.childrenWidth() > (double)docView.op.getAvailableWidth() && docView.op.getLineWrapType() != LineWrapType.NONE) {
            this.wrapInfo = new WrapInfo();
            this.buildWrapLines(pView);
        }
    }

    private void buildWrapLines(ParagraphView pView) {
        this.wrapInfo.updater = new WrapInfoUpdater(this.wrapInfo, pView);
        this.wrapInfo.updater.initWrapInfo();
        this.wrapInfo.updater = null;
    }

    void preferenceChanged(ParagraphView pView, EditorView view, boolean widthChange, boolean heightChange) {
        int index = this.viewIndexFirst(this.raw2Offset(view.getRawEndOffset()));
        if (index >= 0 && this.get(index) == view) {
            if (widthChange) {
                this.fixSpans(pView, index, index + 1);
            }
            if (heightChange) {
                float newHeight = view.getPreferredSpan(1);
                if (newHeight > this.childrenHeight) {
                    this.childrenHeight = newHeight;
                } else {
                    heightChange = false;
                }
            }
            if (widthChange || heightChange) {
                pView.preferenceChanged(null, widthChange, heightChange);
            }
        }
    }

    void paint(ParagraphView pView, Graphics2D g, Shape pAlloc, Rectangle clipBounds) {
        Rectangle2D.Double pRect = ViewUtils.shape2Bounds(pAlloc);
        if (this.wrapInfo != null) {
            double wrapY = (double)clipBounds.y - pRect.y;
            float wrapLineHeight = this.wrapInfo.wrapLineHeight(this);
            int startWrapLineIndex = wrapY < (double)wrapLineHeight ? 0 : (int)(wrapY / (double)wrapLineHeight);
            int endWrapLineIndex = (wrapY += (double)((float)clipBounds.height + (wrapLineHeight - 1.0f))) >= (double)this.height() ? this.wrapInfo.wrapLineCount() : (int)(wrapY / (double)wrapLineHeight) + 1;
            this.wrapInfo.paintWrapLines(this, pView, startWrapLineIndex, endWrapLineIndex, g, pAlloc, clipBounds);
        } else {
            double startX = (double)clipBounds.x - pRect.x;
            double endX = startX + (double)clipBounds.width;
            if (this.size() > 0) {
                int startIndex = this.viewIndexNoWrap(pView, startX, pAlloc);
                int endIndex = this.viewIndexNoWrap(pView, endX, pAlloc) + 1;
                this.paintChildren(pView, g, pAlloc, clipBounds, startIndex, endIndex);
            }
        }
    }

    void paintChildren(ParagraphView pView, Graphics2D g, Shape pAlloc, Rectangle clipBounds, int startIndex, int endIndex) {
        while (startIndex < endIndex) {
            EditorView view = (EditorView)this.get(startIndex);
            Shape childAlloc = this.getChildAllocation(startIndex, pAlloc);
            if (view.getClass() == NewlineView.class) {
                Rectangle2D.Double childRect = ViewUtils.shape2Bounds(childAlloc);
                DocumentView docView = pView.getDocumentView();
                double maxX = Math.max(Math.max(docView.op.getVisibleRect().getMaxX(), clipBounds.getMaxX()), childRect.getMaxX());
                childRect.width = maxX - childRect.x;
                childAlloc = childRect;
            }
            view.paint(g, childAlloc, clipBounds);
            ++startIndex;
        }
    }

    Shape modelToViewChecked(ParagraphView pView, int offset, Shape pAlloc, Position.Bias bias) {
        int index = pView.getViewIndex(offset, bias);
        if (index < 0) {
            return pAlloc;
        }
        if (this.wrapInfo != null) {
            int wrapLineIndex = this.findWrapLineIndex(pView, offset);
            WrapLine wrapLine = (WrapLine)this.wrapInfo.get(wrapLineIndex);
            Rectangle2D.Double wrapLineBounds = this.wrapLineAlloc(pAlloc, wrapLineIndex);
            Shape ret = null;
            StringBuilder logBuilder = null;
            if (LOG.isLoggable(Level.FINE)) {
                logBuilder = new StringBuilder(100);
                logBuilder.append("ParagraphViewChildren.modelToViewChecked(): offset=").append(offset).append(", wrapLineIndex=").append(wrapLineIndex).append(", orig-pAlloc=").append(ViewUtils.toString(pAlloc)).append("\n    ");
            }
            if (wrapLine.startPart != null && offset < wrapLine.startPart.view.getEndOffset()) {
                Shape startPartAlloc = this.startPartAlloc(wrapLineBounds, wrapLine);
                if (logBuilder != null) {
                    logBuilder.append("START-part:").append(ViewUtils.toString(startPartAlloc));
                }
                ret = wrapLine.startPart.view.modelToViewChecked(offset, startPartAlloc, bias);
            } else if (!(wrapLine.endPart == null || offset < wrapLine.endPart.view.getStartOffset() && wrapLine.hasFullViews())) {
                Shape endPartAlloc = this.endPartAlloc(wrapLineBounds, wrapLine, pView);
                if (logBuilder != null) {
                    logBuilder.append("END-part:").append(ViewUtils.toString(endPartAlloc));
                }
                ret = wrapLine.endPart.view.modelToViewChecked(offset, endPartAlloc, bias);
            } else {
                for (int i = wrapLine.firstViewIndex; i < wrapLine.endViewIndex; ++i) {
                    EditorView view = pView.getEditorView(i);
                    if (offset >= view.getEndOffset()) continue;
                    Shape viewAlloc = this.wrapAlloc(wrapLineBounds, wrapLine, i, pView);
                    ret = view.modelToViewChecked(offset, viewAlloc, bias);
                    assert (ret != null);
                    break;
                }
                if (ret == null && wrapLine.hasFullViews()) {
                    EditorView view = pView.getEditorView(wrapLine.endViewIndex - 1);
                    Shape viewAlloc = this.wrapAlloc(wrapLineBounds, wrapLine, wrapLine.endViewIndex - 1, pView);
                    ret = view.modelToViewChecked(view.getEndOffset() - 1, viewAlloc, bias);
                }
            }
            if (logBuilder != null) {
                logBuilder.append("\n    RET=").append(ViewUtils.toString(ret)).append('\n');
                LOG.fine(logBuilder.toString());
            }
            return ret;
        }
        EditorView view = (EditorView)this.get(index);
        Shape childAlloc = this.getChildAllocation(index, pAlloc);
        return view.modelToViewChecked(offset, childAlloc, bias);
    }

    public int viewToModelChecked(ParagraphView pView, double x, double y, Shape pAlloc, Position.Bias[] biasReturn) {
        IndexAndAlloc indexAndAlloc = this.findIndexAndAlloc(pView, x, y, pAlloc);
        int offset = indexAndAlloc != null ? indexAndAlloc.viewOrPart.viewToModelChecked(x, y, indexAndAlloc.alloc, biasReturn) : pView.getStartOffset();
        return offset;
    }

    public String getToolTipTextChecked(ParagraphView pView, double x, double y, Shape pAlloc) {
        IndexAndAlloc indexAndAlloc = this.findIndexAndAlloc(pView, x, y, pAlloc);
        String toolTipText = indexAndAlloc != null ? indexAndAlloc.viewOrPart.getToolTipTextChecked(x, y, indexAndAlloc.alloc) : null;
        return toolTipText;
    }

    public JComponent getToolTip(ParagraphView pView, double x, double y, Shape pAlloc) {
        IndexAndAlloc indexAndAlloc = this.findIndexAndAlloc(pView, x, y, pAlloc);
        JComponent toolTip = indexAndAlloc != null ? indexAndAlloc.viewOrPart.getToolTip(x, y, indexAndAlloc.alloc) : null;
        return toolTip;
    }

    int getNextVisualPositionY(ParagraphView pView, int offset, Position.Bias bias, Shape pAlloc, boolean southDirection, Position.Bias[] biasRet, double x) {
        int retOffset;
        if (offset == -1) {
            if (this.wrapInfo != null) {
                int wrapLine = southDirection ? 0 : this.wrapInfo.wrapLineCount() - 1;
                retOffset = this.visualPositionOnWrapLine(pView, pAlloc, biasRet, x, wrapLine);
            } else {
                retOffset = this.visualPositionNoWrap(pView, pAlloc, biasRet, x);
            }
        } else if (this.wrapInfo != null) {
            int wrapLineIndex = this.findWrapLineIndex(pView, offset);
            retOffset = !southDirection && wrapLineIndex > 0 ? this.visualPositionOnWrapLine(pView, pAlloc, biasRet, x, wrapLineIndex - 1) : (southDirection && wrapLineIndex < this.wrapInfo.wrapLineCount() - 1 ? this.visualPositionOnWrapLine(pView, pAlloc, biasRet, x, wrapLineIndex + 1) : -1);
        } else {
            retOffset = -1;
        }
        return retOffset;
    }

    int getNextVisualPositionX(ParagraphView pView, int offset, Position.Bias bias, Shape pAlloc, boolean eastDirection, Position.Bias[] biasRet) {
        int viewCount = this.size();
        int n = offset == -1 ? (eastDirection ? 0 : viewCount - 1) : this.getViewIndex(pView, offset);
        int increment = eastDirection ? 1 : -1;
        int retOffset = -1;
        for (int index = v38116; retOffset == -1 && index >= 0 && index < viewCount; index += increment) {
            Shape viewAlloc;
            EditorView view = (EditorView)this.get(index);
            retOffset = view.getNextVisualPositionFromChecked(offset, bias, viewAlloc = this.getChildAllocation(index, pAlloc), eastDirection ? 3 : 7, biasRet);
            if (retOffset != -1) continue;
            offset = -1;
        }
        return retOffset;
    }

    private int visualPositionNoWrap(ParagraphView pView, Shape alloc, Position.Bias[] biasRet, double x) {
        int childIndex = this.viewIndexNoWrap(pView, x, alloc);
        EditorView child = pView.getEditorView(childIndex);
        Shape childAlloc = pView.getChildAllocation(childIndex, alloc);
        Rectangle2D r = ViewUtils.shapeAsRect(childAlloc);
        return child.viewToModelChecked(x, r.getY(), childAlloc, biasRet);
    }

    private int visualPositionOnWrapLine(ParagraphView pView, Shape alloc, Position.Bias[] biasRet, double x, int wrapLineIndex) {
        WrapLine wrapLine = (WrapLine)this.wrapInfo.get(wrapLineIndex);
        Rectangle2D.Double wrapLineAlloc = this.wrapLineAlloc(alloc, wrapLineIndex);
        IndexAndAlloc indexAndAlloc = this.findIndexAndAlloc(pView, x, wrapLineAlloc, wrapLine);
        double y = ViewUtils.shapeAsRect(indexAndAlloc.alloc).getY();
        return indexAndAlloc.viewOrPart.viewToModelChecked(x, y, indexAndAlloc.alloc, biasRet);
    }

    private int findWrapLineIndex(Rectangle2D pAllocRect, double y) {
        int wrapLineIndex;
        float wrapLineHeight;
        double relY = y - pAllocRect.getY();
        if (relY < (double)(wrapLineHeight = this.wrapInfo.wrapLineHeight(this))) {
            wrapLineIndex = 0;
        } else {
            wrapLineIndex = (int)(relY / (double)wrapLineHeight);
            int wrapLineCount = this.wrapInfo.wrapLineCount();
            if (wrapLineIndex >= wrapLineCount) {
                wrapLineIndex = wrapLineCount - 1;
            }
        }
        return wrapLineIndex;
    }

    private int findWrapLineIndex(ParagraphView pView, int offset) {
        int wrapLineCount = this.wrapInfo.wrapLineCount();
        int wrapLineIndex = 0;
        WrapLine wrapLine = null;
        while (++wrapLineIndex < wrapLineCount && this.wrapLineStartOffset(pView, wrapLine = (WrapLine)this.wrapInfo.get(wrapLineIndex)) <= offset) {
        }
        return --wrapLineIndex;
    }

    private Shape startPartAlloc(Shape wrapLineAlloc, WrapLine wrapLine) {
        Rectangle2D.Double startPartBounds = ViewUtils.shape2Bounds(wrapLineAlloc);
        startPartBounds.width = wrapLine.startPartWidth();
        return startPartBounds;
    }

    private Shape endPartAlloc(Shape wrapLineAlloc, WrapLine wrapLine, ParagraphView pView) {
        Rectangle2D.Double endPartBounds = ViewUtils.shape2Bounds(wrapLineAlloc);
        endPartBounds.width = wrapLine.endPart.width;
        endPartBounds.x += (double)wrapLine.startPartWidth();
        if (wrapLine.hasFullViews()) {
            endPartBounds.x += this.startVisualOffset(wrapLine.endViewIndex) - this.startVisualOffset(wrapLine.firstViewIndex);
        }
        return endPartBounds;
    }

    private Shape wrapAlloc(Shape wrapLineAlloc, WrapLine wrapLine, int viewIndex, ParagraphView pView) {
        double startX = this.startVisualOffset(wrapLine.firstViewIndex);
        double x = viewIndex != wrapLine.firstViewIndex ? this.startVisualOffset(viewIndex) : startX;
        Rectangle2D.Double viewBounds = ViewUtils.shape2Bounds(wrapLineAlloc);
        viewBounds.x += (double)wrapLine.startPartWidth() + (x - startX);
        viewBounds.width = this.endVisualOffset(viewIndex) - x;
        return viewBounds;
    }

    private IndexAndAlloc findIndexAndAlloc(ParagraphView pView, double x, double y, Shape pAlloc) {
        if (this.size() == 0) {
            return null;
        }
        Rectangle2D pRect = ViewUtils.shapeAsRect(pAlloc);
        if (this.wrapInfo == null) {
            int index;
            IndexAndAlloc indexAndAlloc = new IndexAndAlloc();
            indexAndAlloc.index = index = this.viewIndexNoWrap(pView, x, pAlloc);
            indexAndAlloc.viewOrPart = (EditorView)this.get(index);
            indexAndAlloc.alloc = this.getChildAllocation(index, pAlloc);
            return indexAndAlloc;
        }
        int wrapLineIndex = this.findWrapLineIndex(pRect, y);
        WrapLine wrapLine = (WrapLine)this.wrapInfo.get(wrapLineIndex);
        Rectangle2D.Double wrapLineAlloc = this.wrapLineAlloc(pAlloc, wrapLineIndex);
        return this.findIndexAndAlloc(pView, x, wrapLineAlloc, wrapLine);
    }

    private IndexAndAlloc findIndexAndAlloc(ParagraphView pView, double x, Shape wrapLineAlloc, WrapLine wrapLine) {
        IndexAndAlloc indexAndAlloc = new IndexAndAlloc();
        if (wrapLine.startPart != null && (x < (double)wrapLine.startPartWidth() || !wrapLine.hasFullViews() && wrapLine.endPart == null)) {
            indexAndAlloc.index = -1;
            indexAndAlloc.viewOrPart = wrapLine.startPart.view;
            indexAndAlloc.alloc = this.startPartAlloc(wrapLineAlloc, wrapLine);
            return indexAndAlloc;
        }
        if (wrapLine.hasFullViews()) {
            Rectangle2D.Double viewBounds = ViewUtils.shape2Bounds(wrapLineAlloc);
            viewBounds.x += (double)wrapLine.startPartWidth();
            double lastX = this.startVisualOffset(wrapLine.firstViewIndex);
            for (int i = wrapLine.firstViewIndex; i < wrapLine.endViewIndex; ++i) {
                double nextX = this.startVisualOffset(i + 1);
                viewBounds.width = nextX - lastX;
                if (x < viewBounds.x + viewBounds.width || i == wrapLine.endViewIndex - 1 && wrapLine.endPart == null) {
                    indexAndAlloc.index = i;
                    indexAndAlloc.viewOrPart = pView.getEditorView(i);
                    indexAndAlloc.alloc = viewBounds;
                    return indexAndAlloc;
                }
                viewBounds.x += viewBounds.width;
                lastX = nextX;
            }
        }
        assert (wrapLine.endPart != null);
        indexAndAlloc.index = -2;
        indexAndAlloc.viewOrPart = wrapLine.endPart.view;
        indexAndAlloc.alloc = this.endPartAlloc(wrapLineAlloc, wrapLine, pView);
        return indexAndAlloc;
    }

    private Rectangle2D.Double wrapLineAlloc(Shape pAlloc, int wrapLineIndex) {
        Rectangle2D.Double pRect = ViewUtils.shape2Bounds(pAlloc);
        float wrapLineHeight = this.wrapInfo.wrapLineHeight(this);
        pRect.y += (double)((float)wrapLineIndex * wrapLineHeight);
        pRect.height = wrapLineHeight;
        return pRect;
    }

    private int wrapLineStartOffset(ParagraphView pView, WrapLine wrapLine) {
        if (wrapLine.startPart != null) {
            return wrapLine.startPart.view.getStartOffset();
        }
        if (wrapLine.hasFullViews()) {
            return pView.getEditorView(wrapLine.firstViewIndex).getStartOffset();
        }
        assert (wrapLine.endPart != null);
        return wrapLine.endPart.view.getStartOffset();
    }

    @Override
    protected String findIntegrityError(EditorView parent) {
        String err = ViewChildren.super.findIntegrityError(parent);
        return err;
    }

    @Override
    protected String checkSpanIntegrity(double span, EditorView view) {
        String err = null;
        float prefSpan = view.getPreferredSpan(0);
        if (span != (double)prefSpan) {
            err = "PVChildren: span=" + span + " != prefSpan=" + prefSpan;
        }
        return err;
    }

    public StringBuilder appendViewInfo(ParagraphView pView, StringBuilder sb) {
        if (!pView.isChildrenValid()) {
            int startOffset = pView.getStartOffset();
            sb.append(" I<").append(startOffset + this.getStartInvalidChildrenLocalOffset()).append(',').append(startOffset + this.getEndInvalidChildrenLocalOffset()).append(">");
        }
        sb.append(", chWxH=").append(this.width()).append("x").append(this.height());
        if (this.wrapInfo != null) {
            sb.append(", Wrapped");
        }
        return sb;
    }

    public StringBuilder appendChildrenInfo(ParagraphView pView, StringBuilder sb, int indent, int importantIndex) {
        if (this.wrapInfo != null) {
            this.wrapInfo.appendInfo(sb, pView, indent);
        }
        return this.appendChildrenInfo(sb, indent, importantIndex);
    }

    @Override
    protected String getXYInfo(int index) {
        return new StringBuilder(10).append(" x=").append(this.startVisualOffset(index)).toString();
    }

    private static final class IndexAndAlloc {
        int index;
        EditorView viewOrPart;
        Shape alloc;

        private IndexAndAlloc() {
        }
    }

}

