/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.util.List;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;

public interface HighlightsLayerFilter {
    public static final HighlightsLayerFilter IDENTITY = new HighlightsLayerFilter(){

        @Override
        public List<? extends HighlightsLayer> filterLayers(List<? extends HighlightsLayer> layers) {
            return layers;
        }

        public String toString() {
            return this.getClass().getName() + ".IDENTITY";
        }
    };

    public List<? extends HighlightsLayer> filterLayers(List<? extends HighlightsLayer> var1);

}

