/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.lib2.document;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import org.netbeans.modules.editor.lib2.document.EditorPosition;
import org.netbeans.modules.editor.lib2.document.MarkVector;
import org.openide.util.Utilities;

final class Mark
extends WeakReference<EditorPosition>
implements Runnable {
    int rawOffset;
    private MarkVector markVector;

    Mark(MarkVector markVector, int rawOffset, EditorPosition pos) {
        super(pos, Utilities.activeReferenceQueue());
        this.markVector = markVector;
        this.rawOffset = rawOffset;
        pos.initMark(this);
    }

    public int getOffset() {
        MarkVector lMarkVector = this.markVector;
        int offset = lMarkVector != null ? lMarkVector.offset(this.rawOffset) : this.rawOffset;
        return offset;
    }

    public boolean isBackwardBias() {
        MarkVector lMarkVector = this.markVector;
        return lMarkVector != null ? lMarkVector.isBackwardBiasMarks() : false;
    }

    public int rawOffset() {
        return this.rawOffset;
    }

    @Override
    public void run() {
        MarkVector lMarkVector = this.markVector;
        if (lMarkVector != null) {
            lMarkVector.notifyMarkDisposed();
        }
    }

    void clearMarkVector() {
        this.markVector = null;
    }

    boolean isActive() {
        return this.markVector != null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(8);
        sb.append(this.getOffset());
        EditorPosition pos = (EditorPosition)this.get();
        sb.append(';');
        if (this.isBackwardBias()) {
            sb.append("B");
        }
        if (this.get() == null) {
            sb.append('D');
        }
        sb.append("M@").append(System.identityHashCode(this));
        return sb.toString();
    }

    public String toStringDetail() {
        return this.toString() + ";R:" + this.rawOffset;
    }
}

