/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ViewPart;

final class WrapLine {
    ViewPart startPart;
    ViewPart endPart;
    int firstViewIndex;
    int endViewIndex;

    WrapLine() {
    }

    boolean hasFullViews() {
        return this.firstViewIndex != this.endViewIndex;
    }

    EditorView startView(ParagraphView pView) {
        return this.startPart != null ? this.startPart.view : (this.firstViewIndex != this.endViewIndex ? pView.getEditorView(this.firstViewIndex) : this.endPart.view);
    }

    float startPartWidth() {
        return this.startPart != null ? this.startPart.width : 0.0f;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("startPart=").append(this.startPart);
        sb.append(" [").append(this.firstViewIndex).append(",").append(this.endViewIndex).append("]");
        sb.append(" endPart=").append(this.endPart);
        return sb.toString();
    }
}

