/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.PriorityMutex
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.plaf.TextUI;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.TabExpander;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.lib.editor.util.PriorityMutex;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentViewChildren;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorTabExpander;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.HighlightsViewFactory;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.ParagraphViewChildren;
import org.netbeans.modules.editor.lib2.view.TextLayoutCache;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyChange;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewPaintHighlights;
import org.netbeans.modules.editor.lib2.view.ViewRenderContext;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public final class DocumentView
extends EditorView
implements EditorView.Parent {
    private static final Logger LOG = Logger.getLogger(DocumentView.class.getName());
    static final boolean LOG_SOURCE_TEXT = Boolean.getBoolean("org.netbeans.editor.log.source.text");
    static final boolean DISABLE_END_VIRTUAL_SPACE = Boolean.getBoolean("org.netbeans.editor.disable.end.virtual.space");
    private static final String MUTEX_CLIENT_PROPERTY = "foldHierarchyMutex";
    static final String START_POSITION_PROPERTY = "document-view-start-position";
    static final String END_POSITION_PROPERTY = "document-view-end-position";
    static final String ACCURATE_SPAN_PROPERTY = "document-view-accurate-span";
    static final String TEXT_ZOOM_PROPERTY = "text-zoom";
    final DocumentViewOp op;
    private PriorityMutex pMutex;
    final DocumentViewChildren children;
    private JTextComponent textComponent;
    final ViewRenderContext viewRenderContext;
    private int startOffset;
    private int endOffset;
    private float preferredWidth;
    private float preferredHeight;
    private Rectangle2D.Float allocation = new Rectangle2D.Float();
    private final TabExpander tabExpander;
    private ViewHierarchyChange change;
    private Map<TextLayout, String> textLayoutVerifier;
    static Runnable testRun;
    static Object[] testValues;

    public static DocumentView get(JTextComponent component) {
        View rootView;
        View view;
        TextUI textUI = component.getUI();
        if (textUI != null && (rootView = textUI.getRootView(component)) != null && rootView.getViewCount() > 0 && (view = rootView.getView(0)) instanceof DocumentView) {
            return (DocumentView)view;
        }
        return null;
    }

    static DocumentView get(View view) {
        while (view != null && !(view instanceof DocumentView)) {
            view = view.getParent();
        }
        return (DocumentView)view;
    }

    public DocumentView(Element elem) {
        super(elem);
        assert (elem != null);
        this.op = new DocumentViewOp(this);
        this.tabExpander = new EditorTabExpander(this);
        this.children = new DocumentViewChildren(1);
        this.viewRenderContext = new ViewRenderContext(this);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runTransaction(Runnable r) {
        if (this.lock()) {
            try {
                r.run();
            }
            finally {
                this.unlock();
            }
        }
        r.run();
    }

    public void runReadLockTransaction(final Runnable r) {
        this.getDocument().render(new Runnable(){

            @Override
            public void run() {
                DocumentView.this.runTransaction(r);
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public float getPreferredSpan(int axis) {
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                this.op.checkViewsInited();
                this.op.checkRealSpanChange();
                if (!this.op.isChildrenValid()) {
                    float f = 1.0f;
                    return f;
                }
                float span = axis == 0 ? this.preferredWidth : this.preferredHeight + this.op.getExtraVirtualHeight();
                float f = span;
                return f;
            }
            finally {
                this.unlock();
            }
        }
        return 1.0f;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean lock() {
        if (this.pMutex != null) {
            this.pMutex.lock();
            boolean success = false;
            try {
                this.op.lockCheck();
                success = true;
            }
            finally {
                if (!success) {
                    this.pMutex.unlock();
                }
            }
            return success;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void unlock() {
        try {
            this.op.unlockCheck();
            this.checkFireEvent();
        }
        finally {
            this.pMutex.unlock();
        }
    }

    @Override
    public Document getDocument() {
        return this.getElement().getDocument();
    }

    @Override
    public int getStartOffset() {
        return this.startOffset;
    }

    void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    @Override
    public int getEndOffset() {
        return this.endOffset;
    }

    void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    void updateEndOffset() {
        int viewCount = this.getViewCount();
        int endOffset = viewCount > 0 ? this.getParagraphView(viewCount - 1).getEndOffset() : this.getStartOffset();
        this.setEndOffset(endOffset);
    }

    @Override
    public int getViewCount() {
        return this.children.size();
    }

    @Override
    public View getView(int index) {
        this.checkDocumentLockedIfLogging();
        this.checkMutexAcquiredIfLogging();
        return index < this.children.size() ? (ParagraphView)this.children.get(index) : null;
    }

    public ParagraphView getParagraphView(int index) {
        if (index >= this.getViewCount()) {
            throw new IndexOutOfBoundsException("View index=" + index + " >= " + this.getViewCount());
        }
        return (ParagraphView)this.children.get(index);
    }

    void ensureLayoutValidForInitedChildren() {
        this.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                DocumentView.this.op.checkViewsInited();
                DocumentView.this.children.ensureLayoutValidForInitedChildren(DocumentView.this);
            }
        });
    }

    void ensureAllParagraphsChildrenAndLayoutValid() {
        this.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                DocumentView.this.op.checkViewsInited();
                DocumentView.this.children.ensureParagraphsChildrenAndLayoutValid(DocumentView.this, 0, DocumentView.this.getViewCount(), 0, 0);
            }
        });
    }

    Shape getChildAllocation(int index) {
        return this.getChildAllocation(index, this.getAllocation());
    }

    @Override
    public Shape getChildAllocation(int index, Shape alloc) {
        return this.children.getChildAllocation(this, index, alloc);
    }

    @Override
    public int getViewIndex(int offset, Position.Bias b) {
        if (b == Position.Bias.Backward) {
            --offset;
        }
        return this.getViewIndex(offset);
    }

    public int getViewIndex(int offset) {
        return this.children.viewIndexFirstByStartOffset(offset, 0);
    }

    public int getViewIndex(double y) {
        if (this.op.isActive()) {
            Rectangle2D alloc = this.getAllocation();
            return this.children.viewIndexAtY(y, alloc);
        }
        return -1;
    }

    public double getY(int pViewIndex) {
        return this.children.getY(pViewIndex);
    }

    @Override
    public int getViewEndOffset(int rawChildEndOffset) {
        throw new IllegalStateException("Raw end offsets storage not maintained for DocumentView.");
    }

    @Override
    public void replace(int index, int length, View[] views) {
        this.replaceViews(index, length, views);
    }

    ViewHierarchyChange validChange() {
        if (this.change == null) {
            this.change = new ViewHierarchyChange();
        }
        return this.change;
    }

    void checkFireEvent() {
        if (this.change != null) {
            this.op.viewHierarchyImpl().fireChange(this.change);
            this.change = null;
        }
    }

    public TabExpander getTabExpander() {
        return this.tabExpander;
    }

    double[] replaceViews(int index, int length, View[] views) {
        return this.children.replace(this, index, length, views);
    }

    @Override
    public int getRawEndOffset() {
        return -1;
    }

    @Override
    public void setRawEndOffset(int rawOffset) {
        throw new IllegalStateException("Unexpected");
    }

    @Override
    public void setSize(float width, float height) {
        if (width != this.allocation.width) {
            this.op.markAllocationWidthChange(width);
            if (SwingUtilities.isEventDispatchThread()) {
                this.op.updateVisibleDimension(false);
            }
        }
        if (height != this.allocation.height) {
            this.op.markAllocationHeightChange(height);
        }
    }

    void setAllocationWidth(float width) {
        this.allocation.width = width;
    }

    void setAllocationHeight(float height) {
        if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "DV.setAllocationHeight(): " + this.allocation.height + " to " + height + '\n');
        }
        this.allocation.height = height;
        this.updateBaseY();
    }

    public Rectangle2D getAllocation() {
        return this.allocation;
    }

    public Rectangle2D.Double getAllocationCopy() {
        return new Rectangle2D.Double(0.0, 0.0, this.allocation.getWidth(), this.allocation.getHeight());
    }

    public HighlightsSequence getPaintHighlights(EditorView view, int shift) {
        return this.children.getPaintHighlights(view, shift);
    }

    @Override
    public void preferenceChanged(View childView, boolean widthChange, boolean heightChange) {
        if (childView == null) {
            if (widthChange) {
                this.op.notifyWidthChange();
            }
            if (heightChange) {
                this.op.notifyHeightChange();
            }
        }
    }

    void superPreferenceChanged(boolean widthChange, boolean heightChange) {
        super.preferenceChanged(this, widthChange, heightChange);
    }

    boolean updatePreferredWidth() {
        float newWidth = this.children.width();
        if (newWidth != this.preferredWidth) {
            this.preferredWidth = newWidth;
            return true;
        }
        return false;
    }

    boolean updatePreferredHeight() {
        float newHeight = this.children.height();
        if (newHeight != this.preferredHeight) {
            this.preferredHeight = newHeight;
            this.updateBaseY();
            return true;
        }
        return false;
    }

    void updateBaseY() {
        float baseY;
        float f = baseY = this.op.asTextField ? (float)Math.floor((this.allocation.height - this.preferredHeight) / 2.0f) : 0.0f;
        if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "DV.updateBaseY(): " + this.children.getBaseY() + " to " + baseY + ", asTextField=" + this.op.asTextField + '\n');
        }
        this.children.setBaseY(baseY);
    }

    void markChildrenLayoutInvalid() {
        if (ViewHierarchyImpl.SPAN_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.SPAN_LOG, "Component width differs => children.recomputeChildrenWidths()\n");
        }
        this.children.markChildrenLayoutInvalid();
    }

    @Override
    public ViewRenderContext getViewRenderContext() {
        return this.viewRenderContext;
    }

    public FontRenderContext getFontRenderContext() {
        return this.op.getFontRenderContext();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void offsetRepaint(int startOffset, int endOffset) {
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                if (ViewHierarchyImpl.REPAINT_LOG.isLoggable(Level.FINE)) {
                    ViewHierarchyImpl.REPAINT_LOG.fine("OFFSET-REPAINT: <" + startOffset + "," + endOffset + ">\n");
                }
                if (this.op.isActive() && startOffset < endOffset && this.getViewCount() > 0) {
                    Rectangle2D repaintRect2;
                    Rectangle2D repaintRect2;
                    Rectangle2D.Double docViewRect = this.getAllocationCopy();
                    int pIndex = this.getViewIndex(startOffset);
                    ParagraphView pView = this.getParagraphView(pIndex);
                    int pViewEndOffset = pView.getEndOffset();
                    if (endOffset <= pViewEndOffset) {
                        Shape pAlloc = this.getChildAllocation(pIndex, docViewRect);
                        if (pView.children != null) {
                            if (pView.checkLayoutUpdate(pIndex, pAlloc)) {
                                pAlloc = this.getChildAllocation(pIndex, docViewRect);
                            }
                            Shape s = pView.modelToViewChecked(startOffset, Position.Bias.Forward, endOffset, Position.Bias.Forward, pAlloc);
                            if (endOffset == pViewEndOffset) {
                                Rectangle2D.Double r = ViewUtils.shape2Bounds(s);
                                this.op.extendToVisibleWidth(r);
                                repaintRect2 = r;
                            } else {
                                repaintRect2 = ViewUtils.shapeAsRect(s);
                            }
                        } else {
                            Rectangle2D.Double r = ViewUtils.shape2Bounds(pAlloc);
                            this.op.extendToVisibleWidth(r);
                            repaintRect2 = r;
                        }
                    } else {
                        docViewRect.y = this.getY(pIndex);
                        int endIndex = this.getViewIndex(endOffset) + 1;
                        docViewRect.height = this.getY(endIndex) - docViewRect.y;
                        this.op.extendToVisibleWidth(docViewRect);
                        repaintRect2 = docViewRect;
                    }
                    if (repaintRect2 != null) {
                        this.op.notifyRepaint(repaintRect2);
                    }
                }
            }
            finally {
                this.unlock();
            }
        }
    }

    @Override
    public void setParent(final View parent) {
        if (parent != null) {
            Container container = parent.getContainer();
            assert (container != null);
            assert (container instanceof JTextComponent);
            final JTextComponent tc = (JTextComponent)container;
            this.pMutex = (PriorityMutex)tc.getClientProperty("foldHierarchyMutex");
            if (this.pMutex == null) {
                this.pMutex = new PriorityMutex();
                tc.putClientProperty("foldHierarchyMutex", (Object)this.pMutex);
            }
            this.runReadLockTransaction(new Runnable(){

                @Override
                public void run() {
                    DocumentView.this.setParent(parent);
                    DocumentView.this.textComponent = tc;
                    DocumentView.this.op.parentViewSet();
                    DocumentView.this.updateStartEndOffsets();
                }
            });
        } else {
            this.runReadLockTransaction(new Runnable(){

                @Override
                public void run() {
                    if (DocumentView.this.textComponent != null) {
                        DocumentView.this.op.parentCleared();
                        DocumentView.this.textComponent = null;
                    }
                    DocumentView.this.setParent(null);
                }
            });
        }
    }

    Position getExtraStartPosition() {
        return (Position)this.textComponent.getClientProperty("document-view-start-position");
    }

    Position getExtraEndPosition() {
        return (Position)this.textComponent.getClientProperty("document-view-end-position");
    }

    void updateStartEndOffsets() {
        Position docEndPos;
        Position startPos = this.getExtraStartPosition();
        Position endPos = this.getExtraEndPosition();
        Document doc = this.getDocument();
        int n = this.startOffset = startPos != null ? startPos.getOffset() : 0;
        int n2 = endPos != null ? endPos.getOffset() : (doc != null && (docEndPos = doc.getEndPosition()) != null ? docEndPos.getOffset() : 0);
        this.endOffset = Math.max(this.startOffset, n2);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolTipTextChecked(double x, double y, Shape alloc) {
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                this.op.checkViewsInited();
                if (this.op.isActive()) {
                    String string = this.children.getToolTipTextChecked(this, x, y, alloc);
                    return string;
                }
            }
            finally {
                this.unlock();
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public JComponent getToolTip(double x, double y, Shape alloc) {
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                this.op.checkViewsInited();
                if (this.op.isActive()) {
                    JComponent jComponent = this.children.getToolTip(this, x, y, alloc);
                    return jComponent;
                }
            }
            finally {
                this.unlock();
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                this.op.checkViewsInited();
                if (this.op.isActive()) {
                    this.op.updateFontRenderContext(g, true);
                    this.children.paint(this, g, alloc, clipBounds);
                }
            }
            finally {
                this.unlock();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        if (this.lock()) {
            try {
                Shape shape = this.modelToViewNeedsLock(offset, alloc, bias);
                return shape;
            }
            finally {
                this.unlock();
            }
        }
        return null;
    }

    public Shape modelToViewNeedsLock(int offset, Shape alloc, Position.Bias bias) {
        Rectangle2D.Double rect = ViewUtils.shape2Bounds(alloc);
        Shape retShape = null;
        this.checkDocumentLockedIfLogging();
        this.op.checkViewsInited();
        if (this.op.isActive()) {
            retShape = this.children.modelToViewChecked(this, offset, alloc, bias);
        } else {
            int index = this.getViewIndex(offset);
            if (index >= 0) {
                rect.y = this.getY(index);
            }
        }
        if (retShape == null) {
            float defaultRowHeight = this.op.getDefaultRowHeight();
            if (defaultRowHeight > 0.0f) {
                rect.height = defaultRowHeight;
            }
            retShape = rect;
        }
        if (ViewHierarchyImpl.OP_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.OP_LOG, "modelToView(" + offset + ")=" + retShape + "\n");
        }
        return retShape;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public double modelToY(int offset) {
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                double d = this.modelToYNeedsLock(offset);
                return d;
            }
            finally {
                this.unlock();
            }
        }
        return this.children.getBaseY();
    }

    public double modelToYNeedsLock(int offset) {
        int index;
        double retY = this.children.getBaseY();
        this.op.checkViewsInited();
        if (this.op.isActive() && (index = this.getViewIndex(offset)) >= 0) {
            retY = this.getY(index);
        }
        if (ViewHierarchyImpl.OP_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.OP_LOG, "modelToY(" + offset + ")=" + retY + "\n");
        }
        return retY;
    }

    public double[] modelToYNeedsLock(int[] offsets) {
        double[] retYs = new double[offsets.length];
        this.op.checkViewsInited();
        if (this.op.isActive() && offsets.length > 0) {
            int lastOffset = 0;
            int lastIndex = 0;
            double lastY = this.children.getBaseY();
            for (int i = 0; i < offsets.length; ++i) {
                double y;
                int offset = offsets[i];
                if (offset == lastOffset) {
                    y = lastY;
                } else {
                    int startIndex = offset > lastOffset ? lastIndex : 0;
                    int index = this.children.viewIndexFirstByStartOffset(offset, startIndex);
                    y = this.getY(index);
                }
                retYs[i] = y;
            }
        }
        return retYs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        if (this.lock()) {
            try {
                int n = this.viewToModelNeedsLock(x, y, alloc, biasReturn);
                return n;
            }
            finally {
                this.unlock();
            }
        }
        return 0;
    }

    public int viewToModelNeedsLock(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        int retOffset = 0;
        this.checkDocumentLockedIfLogging();
        this.op.checkViewsInited();
        if (this.op.isActive()) {
            retOffset = this.children.viewToModelChecked(this, x, y, alloc, biasReturn);
        }
        if (ViewHierarchyImpl.OP_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log(ViewHierarchyImpl.OP_LOG, "viewToModel: [x,y]=" + x + "," + y + " => " + retOffset + "\n");
        }
        return retOffset;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    @Override
    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        retOffset = offset;
        if (this.lock()) {
            try {
                this.checkDocumentLockedIfLogging();
                this.op.checkViewsInited();
                if (this.op.isActive()) {
                    switch (direction) {
                        case 3: {
                            if (offset == -1) {
                                retOffset = this.getEndOffset() - 1;
                                ** break;
                            }
                        }
                        case 7: {
                            if (offset == -1) {
                                retOffset = this.getStartOffset();
                                ** break;
                            }
                            retOffset = this.children.getNextVisualPositionX(this, offset, bias, alloc, direction == 3, biasRet);
                            ** break;
                        }
                        case 1: {
                            if (offset == -1) {
                                retOffset = this.getEndOffset() - 1;
                                ** break;
                            }
                        }
                        case 5: {
                            if (offset == -1) {
                                retOffset = this.getStartOffset();
                                ** break;
                            }
                            retOffset = this.children.getNextVisualPositionY(this, offset, bias, alloc, direction == 5, biasRet);
                            ** break;
                        }
                    }
                    throw new IllegalArgumentException("Bad direction " + direction);
                }
            }
            finally {
                this.unlock();
            }
        }
        if (ViewHierarchyImpl.OP_LOG.isLoggable(Level.FINE) == false) return retOffset;
        ViewUtils.log(ViewHierarchyImpl.OP_LOG, "nextVisualPosition(" + offset + "," + ViewUtils.toStringDirection(direction) + ")=" + retOffset + "\n");
        return retOffset;
    }

    public void updateLengthyAtomicEdit(int delta) {
        this.op.updateLengthyAtomicEdit(delta);
    }

    @Override
    public void insertUpdate(DocumentEvent evt, Shape alloc, ViewFactory viewFactory) {
    }

    @Override
    public void removeUpdate(DocumentEvent evt, Shape alloc, ViewFactory viewFactory) {
    }

    @Override
    public void changedUpdate(DocumentEvent evt, Shape alloc, ViewFactory viewFactory) {
    }

    JTextComponent getTextComponent() {
        return this.textComponent;
    }

    void checkDocumentLockedIfLogging() {
        if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINE)) {
            this.checkDocumentLocked();
        }
    }

    void checkDocumentLocked() {
        Document doc = this.getDocument();
        if (!DocumentUtilities.isReadLocked((Document)doc)) {
            String msg = "Document " + doc.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(doc)) + " not locked";
            ViewHierarchyImpl.CHECK_LOG.log(Level.INFO, msg, new Exception(msg));
        }
    }

    boolean isDocumentLocked() {
        return DocumentUtilities.isReadLocked((Document)this.getDocument());
    }

    void checkMutexAcquiredIfLogging() {
        if (ViewHierarchyImpl.CHECK_LOG.isLoggable(Level.FINE)) {
            this.checkLocked();
        }
    }

    void checkLocked() {
        Thread mutexThread;
        PriorityMutex mutex = this.pMutex;
        if (mutex != null && (mutexThread = mutex.getLockThread()) != Thread.currentThread()) {
            String msg = mutexThread == null ? "Mutex not acquired" : "Mutex already acquired for different thread: " + mutexThread;
            ViewHierarchyImpl.CHECK_LOG.log(Level.INFO, msg + " for textComponent=" + this.textComponent, new Exception());
        }
    }

    boolean isLocked() {
        PriorityMutex mutex = this.pMutex;
        return mutex != null && mutex.getLockThread() == Thread.currentThread();
    }

    public Map<TextLayout, String> getTextLayoutVerifier() {
        if (this.textLayoutVerifier == null) {
            this.textLayoutVerifier = new WeakHashMap<TextLayout, String>(256);
        }
        return this.textLayoutVerifier;
    }

    @Override
    protected String getDumpName() {
        return "DV";
    }

    @Override
    public String findIntegrityError() {
        String err = super.findIntegrityError();
        if (err == null) {
            int viewCount = this.getViewCount();
            if (viewCount > 0) {
                TextLayoutCache tlCache;
                ParagraphView firstView = this.getParagraphView(0);
                if (firstView.getStartOffset() != this.startOffset) {
                    err = "firstView.getStartOffset()=" + firstView.getStartOffset() + " != startOffset=" + this.startOffset;
                }
                ParagraphView lastView = this.getParagraphView(viewCount - 1);
                int lastViewEndOffset = lastView.getEndOffset();
                if (err == null && lastViewEndOffset != this.endOffset) {
                    err = "lastView.endOffset=" + lastViewEndOffset + " != endOffset=" + this.endOffset;
                }
                int docTextEndOffset = this.getDocument().getLength() + 1;
                if (err == null && lastViewEndOffset > docTextEndOffset) {
                    err = "lastViewEndOffset=" + lastViewEndOffset + " > docTextEndOffset=" + docTextEndOffset;
                }
                if (err == null) {
                    err = this.children.findIntegrityError(this);
                }
                if (err == null && (err = (tlCache = this.op.getTextLayoutCache()).findIntegrityError()) == null) {
                    for (int i = 0; i < viewCount; ++i) {
                        ParagraphView pView = this.getParagraphView(i);
                        boolean inCache = tlCache.contains(pView);
                        if (!pView.isChildrenNull() == inCache) continue;
                        err = "Invalid TLCaching for pView[" + i + "]: inCache=" + inCache;
                        break;
                    }
                }
            }
            if (err == null && this.startOffset > this.endOffset) {
                err = "startOffset=" + this.startOffset + " > endOffset=" + this.endOffset;
            }
        }
        if (err != null) {
            err = this.getDumpName() + ": " + err;
        }
        return err;
    }

    @Override
    public String findTreeIntegrityError() {
        final String[] ret = new String[1];
        this.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                ret[0] = DocumentView.this.op.isChildrenValid() ? DocumentView.this.findTreeIntegrityError() : null;
            }
        });
        return ret[0];
    }

    @Override
    protected StringBuilder appendViewInfo(StringBuilder sb, int indent, String xyInfo, int importantChildIndex) {
        this.appendViewInfo(sb, indent, xyInfo, importantChildIndex);
        if (this.getParent() == null) {
            sb.append("; NULL-PARENT");
        }
        if (!this.op.isChildrenValid()) {
            sb.append("; INVALID-CHILDREN");
        }
        Position startPos = this.getExtraStartPosition();
        Position endPos = this.getExtraEndPosition();
        if (startPos != null || endPos != null) {
            sb.append("; ExtraBounds:<");
            sb.append(startPos != null ? Integer.valueOf(startPos.getOffset()) : "START");
            sb.append(",");
            sb.append(endPos != null ? Integer.valueOf(endPos.getOffset()) : "END");
            sb.append(">, ");
        }
        this.op.appendInfo(sb);
        if (LOG_SOURCE_TEXT) {
            Document doc = this.getDocument();
            sb.append("\nDoc: ").append(ViewUtils.toString(doc));
        }
        if (importantChildIndex != -1) {
            this.children.appendChildrenInfo(this, sb, indent, importantChildIndex);
        }
        return sb;
    }

    public String toString() {
        final String[] s = new String[1];
        this.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                s[0] = DocumentView.this.toStringNeedsLock();
            }
        });
        return s[0];
    }

    public String toStringNeedsLock() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }

    public String toStringDetail() {
        final String[] s = new String[1];
        this.runReadLockTransaction(new Runnable(){

            @Override
            public void run() {
                s[0] = DocumentView.this.toStringDetailNeedsLock();
            }
        });
        return s[0];
    }

    public String toStringDetailNeedsLock() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -2).toString();
    }

    static {
        EditorViewFactory.registerFactory(new HighlightsViewFactory.HighlightsFactory());
    }

}

