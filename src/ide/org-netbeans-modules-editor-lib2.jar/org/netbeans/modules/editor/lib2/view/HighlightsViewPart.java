/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewUtils;
import org.netbeans.modules.editor.lib2.view.ParagraphView;
import org.netbeans.modules.editor.lib2.view.TextLayoutUtils;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

public final class HighlightsViewPart
extends EditorView {
    private static final Logger LOG = Logger.getLogger(HighlightsViewPart.class.getName());
    private HighlightsView fullView;
    private int shift;
    private int length;
    private TextLayout partTextLayout;

    public HighlightsViewPart(HighlightsView fullView, int shift, int length) {
        super(null);
        int fullViewLength = fullView.getLength();
        if (shift < 0 || length < 0 || shift + length > fullViewLength) {
            throw new IllegalArgumentException("shift=" + shift + ", length=" + length + ", fullViewLength=" + fullViewLength);
        }
        this.fullView = fullView;
        this.shift = shift;
        this.length = length;
        this.setParent(fullView.getParent());
    }

    @Override
    public void setParent(View view) {
        super.setParent(view);
    }

    @Override
    public float getPreferredSpan(int axis) {
        TextLayout textLayout = this.getTextLayout();
        if (textLayout == null) {
            return 0.0f;
        }
        float span = axis == 0 ? textLayout.getAdvance() : TextLayoutUtils.getHeight(textLayout);
        return span;
    }

    @Override
    public int getRawEndOffset() {
        return this.getLength();
    }

    @Override
    public void setRawEndOffset(int rawOffset) {
        throw new IllegalStateException();
    }

    @Override
    public int getLength() {
        return this.length;
    }

    @Override
    public int getStartOffset() {
        return this.fullView.getStartOffset() + this.shift;
    }

    @Override
    public int getEndOffset() {
        return this.getStartOffset() + this.getLength();
    }

    @Override
    public Document getDocument() {
        return this.fullView.getDocument();
    }

    @Override
    public AttributeSet getAttributes() {
        return this.fullView.getAttributes();
    }

    ParagraphView getParagraphView() {
        return (ParagraphView)this.getParent();
    }

    DocumentView getDocumentView() {
        ParagraphView paragraphView = this.getParagraphView();
        return paragraphView != null ? paragraphView.getDocumentView() : null;
    }

    TextLayout getTextLayout() {
        if (this.partTextLayout == null) {
            this.partTextLayout = this.fullView.createPartTextLayout(this.shift, this.getLength());
        }
        return this.partTextLayout;
    }

    @Override
    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        return HighlightsViewUtils.indexToView(this.getTextLayout(), null, offset - this.getStartOffset(), bias, this.getLength(), alloc);
    }

    @Override
    public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        return HighlightsViewUtils.viewToIndex(this.getTextLayout(), x, alloc, biasReturn) + this.getStartOffset();
    }

    @Override
    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        int startOffset = this.getStartOffset();
        return HighlightsViewUtils.getNextVisualPosition(offset, bias, alloc, direction, biasRet, this.getTextLayout(), startOffset, startOffset, this.getLength(), this.getDocumentView());
    }

    @Override
    public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
        int fullViewStartOffset = this.fullView.getStartOffset();
        DocumentView docView = this.getDocumentView();
        HighlightsViewUtils.paintHiglighted(g, alloc, clipBounds, docView, this.fullView, fullViewStartOffset, false, this.getTextLayout(), fullViewStartOffset + this.shift, 0, this.getLength());
    }

    @Override
    public View breakView(int axis, int offset, float x, float len) {
        View part = HighlightsViewUtils.breakView(axis, offset, x, len, this /* !! */ .fullView, this /* !! */ .shift, this /* !! */ .getLength(), this /* !! */ .getTextLayout());
        return part != null ? part : this /* !! */ ;
    }

    @Override
    public View createFragment(int p0, int p1) {
        int startOffset = this.getStartOffset();
        ViewUtils.checkFragmentBounds(p0, p1, startOffset, this.getLength());
        if (ViewHierarchyImpl.BUILD_LOG.isLoggable(Level.FINE)) {
            ViewHierarchyImpl.BUILD_LOG.fine("HVP.createFragment(" + p0 + "," + p1 + "): <" + startOffset + "," + this.getEndOffset() + ">\n");
        }
        return new HighlightsViewPart(this.fullView, this.shift + p0 - startOffset, p1 - p0);
    }

    @Override
    protected String getDumpName() {
        return "HVP";
    }

    public String toString() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }
}

