/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.logging.Logger;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.ViewGapStorage;

class ViewChildren<V extends EditorView>
extends GapList<V> {
    private static final Logger LOG = Logger.getLogger(ViewChildren.class.getName());
    private static final long serialVersionUID = 0;
    ViewGapStorage gapStorage;

    ViewChildren(int capacity) {
        super(capacity);
    }

    int raw2Offset(int rawEndOffset) {
        return this.gapStorage == null ? rawEndOffset : this.gapStorage.raw2Offset(rawEndOffset);
    }

    int offset2Raw(int offset) {
        return this.gapStorage == null ? offset : this.gapStorage.offset2Raw(offset);
    }

    int startOffset(int index) {
        return index > 0 ? this.raw2Offset(((EditorView)this.get(index - 1)).getRawEndOffset()) : 0;
    }

    int endOffset(int index) {
        return this.raw2Offset(((EditorView)this.get(index)).getRawEndOffset());
    }

    int viewIndexFirst(int offset) {
        offset = this.offset2Raw(offset);
        int last = this.size() - 1;
        int low = 0;
        int high = last;
        while (low <= high) {
            int mid = low + high >>> 1;
            EditorView view = (EditorView)this.get(mid);
            int rawEndOffset = view.getRawEndOffset();
            if (rawEndOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (rawEndOffset > offset) {
                high = mid - 1;
                continue;
            }
            while (view.getLength() == 0 && mid > 0) {
                view = (EditorView)this.get(--mid);
            }
            low = mid + 1;
            break;
        }
        return Math.min(low, last);
    }

    void moveOffsetGap(int index, int newOffsetGapStart) {
        if (this.gapStorage == null) {
            return;
        }
        int origStart = this.gapStorage.offsetGapStart;
        int shift = this.gapStorage.offsetGapLength;
        this.gapStorage.offsetGapStart = newOffsetGapStart;
        int viewCount = this.size();
        if (index == viewCount || ((EditorView)this.get(index)).getRawEndOffset() > origStart) {
            EditorView view;
            int offset;
            while (--index >= 0 && (offset = (view = (EditorView)this.get(index)).getRawEndOffset()) > origStart) {
                view.setRawEndOffset(offset - shift);
            }
        } else {
            EditorView view;
            int offset;
            while (index < viewCount && (offset = (view = (EditorView)this.get(index)).getRawEndOffset()) <= origStart) {
                view.setRawEndOffset(offset + shift);
                ++index;
            }
        }
    }

    int getLength() {
        return this.startOffset(this.size());
    }

    double raw2VisualOffset(double rawVisualOffset) {
        return this.gapStorage == null ? rawVisualOffset : this.gapStorage.raw2VisualOffset(rawVisualOffset);
    }

    double visualOffset2Raw(double visualOffset) {
        return this.gapStorage == null ? visualOffset : this.gapStorage.visualOffset2Raw(visualOffset);
    }

    final double startVisualOffset(int index) {
        return index > 0 ? this.raw2VisualOffset(((EditorView)this.get(index - 1)).getRawEndVisualOffset()) : 0.0;
    }

    final double endVisualOffset(int index) {
        return this.raw2VisualOffset(((EditorView)this.get(index)).getRawEndVisualOffset());
    }

    final int viewIndexFirstVisual(double visualOffset, int measuredViewCount) {
        int last = measuredViewCount - 1;
        if (last == -1) {
            return -1;
        }
        visualOffset = this.visualOffset2Raw(visualOffset);
        int low = 0;
        int high = last;
        while (low <= high) {
            int mid = low + high >>> 1;
            double rawEndVisualOffset = ((EditorView)this.get(mid)).getRawEndVisualOffset();
            if (rawEndVisualOffset < visualOffset) {
                low = mid + 1;
                continue;
            }
            if (rawEndVisualOffset > visualOffset) {
                high = mid - 1;
                continue;
            }
            while (mid > 0 && ((EditorView)this.get(mid - 1)).getRawEndVisualOffset() == visualOffset) {
                --mid;
            }
            low = mid + 1;
            break;
        }
        return Math.min(low, last);
    }

    void moveVisualGap(int index, double newVisualGapStart) {
        if (this.gapStorage == null) {
            return;
        }
        this.gapStorage.visualGapStart = newVisualGapStart;
        if (index != this.gapStorage.visualGapIndex) {
            if (index < this.gapStorage.visualGapIndex) {
                for (int i = this.gapStorage.visualGapIndex - 1; i >= index; --i) {
                    EditorView view = (EditorView)this.get(i);
                    view.setRawEndVisualOffset(view.getRawEndVisualOffset() + this.gapStorage.visualGapLength);
                }
            } else {
                for (int i = this.gapStorage.visualGapIndex; i < index; ++i) {
                    EditorView view = (EditorView)this.get(i);
                    view.setRawEndVisualOffset(view.getRawEndVisualOffset() - this.gapStorage.visualGapLength);
                }
            }
            this.gapStorage.visualGapIndex = index;
        }
    }

    protected String findIntegrityError(EditorView parent) {
        String err = null;
        int lastRawEndOffset = 0;
        int lastLocalEndOffset = 0;
        int lastEndOffset = parent.getStartOffset();
        double lastRawEndVisualOffset = 0.0;
        double lastEndVisualOffset = 0.0;
        for (int i = 0; i < this.size(); ++i) {
            EditorView view = (EditorView)this.get(i);
            View p = view.getParent();
            if (err == null && p != parent) {
                err = "view.getParent()=" + p + " != parent=" + parent;
            }
            int viewLength = view.getLength();
            int rawEndOffset = view.getRawEndOffset();
            int childStartOffset = view.getStartOffset();
            int childEndOffset = view.getEndOffset();
            double rawEndVisualOffset = view.getRawEndVisualOffset();
            double endVisualOffset = this.raw2VisualOffset(rawEndVisualOffset);
            if (err == null && rawEndOffset != -1 && rawEndOffset < lastRawEndOffset) {
                err = "rawEndOffset=" + rawEndOffset + " < lastRawEndOffset=" + lastRawEndOffset;
            }
            if (err == null && childStartOffset != lastEndOffset) {
                err = "childStartOffset=" + childStartOffset + " != lastEndOffset=" + lastEndOffset;
            }
            if (err == null && childEndOffset < childStartOffset) {
                err = "childEndOffset=" + childEndOffset + " < childStartOffset=" + childStartOffset;
            }
            if (err == null && childEndOffset - childStartOffset != viewLength) {
                err = "(childEndOffset-childStartOffset)=" + (childEndOffset - childStartOffset) + " != view.getLength()=" + viewLength;
            }
            lastEndOffset = childEndOffset;
            if (err == null && rawEndVisualOffset < lastRawEndVisualOffset) {
                err = "rawEndVisualOffset=" + rawEndVisualOffset + " < lastRawEndVisualOffset=" + lastRawEndVisualOffset;
            }
            if (err == null && endVisualOffset < lastEndVisualOffset) {
                err = "visualOffset=" + endVisualOffset + " < lastVisualOffset=" + lastEndVisualOffset;
            }
            if (err == null) {
                err = this.checkSpanIntegrity(endVisualOffset - lastEndVisualOffset, view);
            }
            lastEndVisualOffset = endVisualOffset;
            if (err == null && this.gapStorage != null && rawEndOffset != -1) {
                int localEndOffset = this.gapStorage.raw2Offset(rawEndOffset);
                if (lastLocalEndOffset + viewLength != localEndOffset) {
                    err = "lastLocalEndOffset=" + lastLocalEndOffset + " + viewLength=" + viewLength + " != localEndOffset=" + localEndOffset;
                }
                if (i < this.gapStorage.visualGapIndex) {
                    if (err == null && !this.gapStorage.isBelowVisualGap(rawEndVisualOffset)) {
                        err = "Not below visual-gap: rawEndVisualOffset=" + rawEndVisualOffset + "(minus gap: " + (rawEndVisualOffset - this.gapStorage.visualGapLength) + "), gap:" + this.gapStorage;
                    }
                } else if (err == null && this.gapStorage.isBelowVisualGap(rawEndVisualOffset)) {
                    err = "Not above visual-gap: rawEndVisualOffset=" + rawEndVisualOffset + ", gap:" + this.gapStorage;
                }
                lastLocalEndOffset = localEndOffset;
            }
            if (err == null) continue;
            err = "ViewChildren[" + i + "]: " + err;
            break;
        }
        return err;
    }

    protected String checkSpanIntegrity(double span, V view) {
        return null;
    }

    public StringBuilder appendChildrenInfo(StringBuilder sb, int indent, int importantIndex) {
        if (this.gapStorage != null) {
            sb.append("Gap: ");
            this.gapStorage.appendInfo(sb);
        }
        int viewCount = this.size();
        int digitCount = ArrayUtilities.digitCount((int)viewCount);
        int importantLastIndex = -1;
        int childImportantIndex = importantIndex == -2 ? -2 : -1;
        for (int i = 0; i < viewCount; ++i) {
            sb.append('\n');
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            EditorView view = (EditorView)this.get(i);
            String xyInfo = this.getXYInfo(i);
            view.appendViewInfo(sb, indent, xyInfo, childImportantIndex);
            boolean appendDots = false;
            if (i == 4) {
                if (importantIndex == -1) {
                    if (i < viewCount - 6) {
                        appendDots = true;
                        i = viewCount - 6;
                    }
                } else if (importantIndex >= 0) {
                    importantLastIndex = importantIndex + 3;
                    if (i < (importantIndex -= 3) - 1) {
                        appendDots = true;
                        i = importantIndex - 1;
                    }
                }
            } else if (i == importantLastIndex && i < viewCount - 6) {
                appendDots = true;
                i = viewCount - 6;
            }
            if (!appendDots) continue;
            sb.append('\n');
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
            sb.append("...");
        }
        return sb;
    }

    protected String getXYInfo(int index) {
        return "";
    }
}

