/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib2.document;

import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.modules.editor.lib2.document.AbstractPositionElement;
import org.netbeans.modules.editor.lib2.document.LineRootElement;

public final class LineElement
extends AbstractPositionElement
implements Position {
    private Object attributes;

    LineElement(LineRootElement root, Position startPos, Position endPos) {
        super((Element)((Object)root), startPos, endPos);
    }

    @Override
    public int getOffset() {
        return this.getStartOffset();
    }

    @Override
    public String getName() {
        return "paragraph";
    }

    @Override
    public AttributeSet getAttributes() {
        return this.attributes instanceof AttributeSet ? (AttributeSet)this.attributes : SimpleAttributeSet.EMPTY;
    }

    public void setAttributes(AttributeSet attributes) {
        this.attributes = attributes;
    }

    public Object legacyGetAttributesObject() {
        return this.attributes;
    }

    public void legacySetAttributesObject(Object attributes) {
        this.attributes = attributes;
    }
}

