/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib2.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.TextHitInfo;
import java.awt.font.TextLayout;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.DocumentViewOp;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.HighlightsView;
import org.netbeans.modules.editor.lib2.view.HighlightsViewPart;
import org.netbeans.modules.editor.lib2.view.LineWrapType;
import org.netbeans.modules.editor.lib2.view.PaintState;
import org.netbeans.modules.editor.lib2.view.TextLayoutUtils;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyImpl;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;

public class HighlightsViewUtils {
    private static final Logger LOG = Logger.getLogger(HighlightsViewUtils.class.getName());

    private HighlightsViewUtils() {
    }

    private static Color foreColor(AttributeSet attrs) {
        return attrs != null ? (Color)attrs.getAttribute(StyleConstants.Foreground) : null;
    }

    private static Color validForeColor(AttributeSet attrs, JTextComponent textComponent) {
        Color foreColor = HighlightsViewUtils.foreColor(attrs);
        if (foreColor == null) {
            foreColor = textComponent.getForeground();
        }
        if (foreColor == null) {
            foreColor = Color.BLACK;
        }
        return foreColor;
    }

    static Shape indexToView(TextLayout textLayout, Rectangle2D textLayoutBounds, int index, Position.Bias bias, int maxIndex, Shape alloc) {
        TextHitInfo endHit;
        if (textLayout == null) {
            return alloc;
        }
        assert (maxIndex <= textLayout.getCharacterCount());
        int charIndex = Math.min(index, maxIndex);
        charIndex = Math.max(charIndex, 0);
        TextHitInfo startHit = bias == Position.Bias.Forward ? TextHitInfo.leading(charIndex) : TextHitInfo.trailing(charIndex - 1);
        TextHitInfo textHitInfo = endHit = charIndex < maxIndex ? TextHitInfo.trailing(charIndex) : startHit;
        if (textLayoutBounds == null) {
            textLayoutBounds = ViewUtils.shapeAsRect(alloc);
        }
        return TextLayoutUtils.getRealAlloc(textLayout, textLayoutBounds, startHit, endHit);
    }

    static int viewToIndex(TextLayout textLayout, double x, Shape alloc, Position.Bias[] biasReturn) {
        Rectangle2D bounds = ViewUtils.shapeAsRect(alloc);
        TextHitInfo hitInfo = HighlightsViewUtils.x2Index(textLayout, (float)(x - bounds.getX()));
        if (biasReturn != null) {
            biasReturn[0] = hitInfo.isLeadingEdge() ? Position.Bias.Forward : Position.Bias.Backward;
        }
        return hitInfo.getInsertionIndex();
    }

    static TextHitInfo x2Index(TextLayout textLayout, float x) {
        TextHitInfo hit = textLayout.hitTestChar(x, 0.0f);
        if (!hit.isLeadingEdge()) {
            hit = TextHitInfo.leading(hit.getInsertionIndex());
        }
        return hit;
    }

    static double getMagicX(DocumentView docView, EditorView view, int offset, Position.Bias bias, Shape alloc) {
        Shape offsetBounds;
        Point magicCaretPoint;
        JTextComponent textComponent = docView.getTextComponent();
        if (textComponent == null) {
            return 0.0;
        }
        Caret caret = textComponent.getCaret();
        Point point = magicCaretPoint = caret != null ? caret.getMagicCaretPosition() : null;
        double x = magicCaretPoint == null ? ((offsetBounds = view.modelToViewChecked(offset, alloc, bias)) == null ? 0.0 : offsetBounds.getBounds2D().getX()) : (double)magicCaretPoint.x;
        return x;
    }

    static int getNextVisualPosition(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet, TextLayout textLayout, int textLayoutOffset, int viewStartOffset, int viewLength, DocumentView docView) {
        int retOffset = -1;
        biasRet[0] = Position.Bias.Forward;
        switch (direction) {
            case 3: {
                int insertionIndex;
                TextHitInfo currentHit;
                TextHitInfo nextHit;
                if (offset == -1) {
                    retOffset = textLayout.isLeftToRight() ? viewStartOffset : viewStartOffset + viewLength - 1;
                    break;
                }
                int index = offset - viewStartOffset;
                if (index < 0 || index > viewLength || (nextHit = textLayout.getNextRightHit(currentHit = TextHitInfo.afterOffset(index))) == null || (insertionIndex = nextHit.getInsertionIndex()) == viewLength) break;
                retOffset = viewStartOffset + insertionIndex;
                break;
            }
            case 7: {
                TextHitInfo nextHit;
                TextHitInfo currentHit;
                if (offset == -1) {
                    retOffset = textLayout.isLeftToRight() ? viewStartOffset + viewLength - 1 : viewStartOffset;
                    break;
                }
                int index = offset - viewStartOffset;
                if (index < 0 || index > viewLength || (nextHit = textLayout.getNextLeftHit(currentHit = TextHitInfo.afterOffset(index))) == null) break;
                int insertionIndex = nextHit.getInsertionIndex();
                if (!textLayout.isLeftToRight() && insertionIndex == viewLength) break;
                retOffset = viewStartOffset + insertionIndex;
                break;
            }
            case 1: 
            case 5: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return retOffset;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void paintHiglighted(Graphics2D g, Shape textLayoutAlloc, Rectangle clipBounds, DocumentView docView, EditorView view, int viewStartOffset, boolean newline, TextLayout textLayout, int textLayoutOffset, int startIndex, int endIndex) {
        Rectangle2D.Double textLayoutRect = ViewUtils.shape2Bounds(textLayoutAlloc);
        PaintState paintState = PaintState.save(g);
        Shape origClip = g.getClip();
        try {
            JTextComponent textComponent = docView.getTextComponent();
            HighlightsSequence highlights = docView.getPaintHighlights(view, textLayoutOffset + startIndex - viewStartOffset);
            TextLayout renderTextLayout = textLayout;
            Boolean showNonPrintingChars = null;
            Map foreColor2Area = null;
            boolean done = false;
            block3 : while (!done && highlights.moveNext()) {
                int renderEndOffset;
                int hiStartOffset = highlights.getStartOffset();
                int hiEndOffset = Math.min(highlights.getEndOffset(), textLayoutOffset + endIndex);
                if (hiEndOffset <= hiStartOffset) break;
                do {
                    boolean hitsClip;
                    Shape renderPartAlloc;
                    renderEndOffset = hiEndOffset;
                    AttributeSet attrs = highlights.getAttributes();
                    Rectangle2D.Double specialTextLayoutRect = null;
                    if (textLayout != null) {
                        TextHitInfo startHit = TextHitInfo.leading(hiStartOffset - textLayoutOffset);
                        TextHitInfo endHit = TextHitInfo.leading(renderEndOffset - textLayoutOffset);
                        renderPartAlloc = TextLayoutUtils.getRealAlloc(textLayout, textLayoutRect, startHit, endHit);
                        if (ViewHierarchyImpl.PAINT_LOG.isLoggable(Level.FINER)) {
                            ViewHierarchyImpl.PAINT_LOG.finer("      View-Id=" + view.getDumpId() + ", startOffset=" + view.getStartOffset() + ", Fragment: hit<" + startHit.getCharIndex() + "," + endHit.getCharIndex() + ">, text='" + DocumentUtilities.getText((Document)docView.getDocument()).subSequence(hiStartOffset, renderEndOffset) + "', fAlloc=" + ViewUtils.toString(renderPartAlloc.getBounds()) + ", Ascent=" + ViewUtils.toStringPrec1(docView.op.getDefaultAscent()) + ", Color=" + ViewUtils.toString(g.getColor()) + '\n');
                        }
                    } else {
                        if (showNonPrintingChars == null) {
                            showNonPrintingChars = docView.op.isNonPrintableCharactersVisible();
                        }
                        if (newline) {
                            renderPartAlloc = textLayoutAlloc;
                            renderTextLayout = showNonPrintingChars != false ? docView.op.getNewlineCharTextLayout() : null;
                        } else {
                            if (showNonPrintingChars.booleanValue()) {
                                renderEndOffset = hiStartOffset + 1;
                            }
                            Shape renderStartAlloc = view.modelToViewChecked(hiStartOffset, textLayoutRect, Position.Bias.Forward);
                            Rectangle2D.Double r = ViewUtils.shape2Bounds(renderStartAlloc);
                            Shape renderEndAlloc = view.modelToViewChecked(renderEndOffset, textLayoutRect, Position.Bias.Forward);
                            Rectangle2D rEnd = ViewUtils.shapeAsRect(renderEndAlloc);
                            r.width = rEnd.getX() - r.x;
                            renderPartAlloc = r;
                            specialTextLayoutRect = r;
                            renderTextLayout = showNonPrintingChars != false ? docView.op.getTabCharTextLayout(r.width) : null;
                        }
                    }
                    Rectangle renderPartBounds = renderPartAlloc.getBounds();
                    boolean bl = hitsClip = clipBounds == null || renderPartAlloc.intersects(clipBounds);
                    if (hitsClip) {
                        Object strikeThroughValue;
                        HighlightsViewUtils.fillBackground(g, renderPartAlloc, attrs, textComponent);
                        g.clip(renderPartAlloc);
                        HighlightsViewUtils.paintBackgroundHighlights(g, renderPartAlloc, attrs, docView);
                        g.setColor(HighlightsViewUtils.validForeColor(attrs, textComponent));
                        Object object = strikeThroughValue = attrs != null ? attrs.getAttribute(StyleConstants.StrikeThrough) : null;
                        if (renderTextLayout != null) {
                            if (foreColor2Area != null && strikeThroughValue == null) {
                                Area renderArea = new Area(renderPartAlloc);
                                Area compoundArea = (Area)foreColor2Area.get(g.getColor());
                                if (compoundArea == null) {
                                    compoundArea = renderArea;
                                    foreColor2Area.put(g.getColor(), compoundArea);
                                } else {
                                    compoundArea.add(renderArea);
                                }
                            } else if (renderTextLayout != null) {
                                Rectangle2D.Double tlRect = specialTextLayoutRect != null ? specialTextLayoutRect : textLayoutRect;
                                HighlightsViewUtils.paintTextLayout(g, tlRect, renderTextLayout, docView);
                            }
                        }
                        if (strikeThroughValue != null) {
                            HighlightsViewUtils.paintStrikeThrough(g, textLayoutRect, strikeThroughValue, attrs, docView);
                        }
                        g.setClip(origClip);
                    } else if (clipBounds != null && renderPartBounds.getX() > clipBounds.getMaxX()) {
                        done = true;
                        continue block3;
                    }
                    hiStartOffset = renderEndOffset;
                } while (!done && renderEndOffset < hiEndOffset);
            }
            if (foreColor2Area != null) {
                for (Map.Entry entry : foreColor2Area.entrySet()) {
                    Color foreColor = (Color)entry.getKey();
                    g.setColor(foreColor);
                    Area compoundArea = (Area)entry.getValue();
                    g.clip(compoundArea);
                    HighlightsViewUtils.paintTextLayout(g, textLayoutRect, renderTextLayout, docView);
                    g.setClip(origClip);
                }
            }
        }
        finally {
            g.setClip(origClip);
            paintState.restore();
        }
    }

    static void fillBackground(Graphics2D g, Shape partAlloc, AttributeSet attrs, JTextComponent c) {
        if (ViewUtils.applyBackgroundColor(g, attrs, c)) {
            g.fill(partAlloc);
        }
    }

    static void paintBackgroundHighlights(Graphics2D g, Shape partAlloc, AttributeSet attrs, DocumentView docView) {
        Rectangle2D partAllocBounds = ViewUtils.shapeAsRect(partAlloc);
        int x = (int)partAllocBounds.getX();
        int y = (int)partAllocBounds.getY();
        int lastX = (int)(Math.ceil(partAllocBounds.getMaxX()) - 1.0);
        int lastY = (int)(Math.ceil(partAllocBounds.getMaxY()) - 1.0);
        HighlightsViewUtils.paintTextLimitLine(g, docView, x, y, lastX, lastY);
        if (attrs != null) {
            Color waveUnderlineColor;
            Color leftBorderLineColor = (Color)attrs.getAttribute(EditorStyleConstants.LeftBorderLineColor);
            Color rightBorderLineColor = (Color)attrs.getAttribute(EditorStyleConstants.RightBorderLineColor);
            Color topBorderLineColor = (Color)attrs.getAttribute(EditorStyleConstants.TopBorderLineColor);
            Color bottomBorderLineColor = (Color)attrs.getAttribute(EditorStyleConstants.BottomBorderLineColor);
            Object underlineValue = attrs.getAttribute(StyleConstants.Underline);
            if (underlineValue != null) {
                Color underlineColor = underlineValue instanceof Boolean ? (Boolean.TRUE.equals(underlineValue) ? docView.getTextComponent().getForeground() : null) : (Color)underlineValue;
                if (underlineColor != null) {
                    g.setColor(underlineColor);
                    Font font = ViewUtils.getFont(attrs, docView.getTextComponent().getFont());
                    float[] underlineAndStrike = docView.op.getUnderlineAndStrike(font);
                    g.fillRect((int)partAllocBounds.getX(), (int)(partAllocBounds.getY() + (double)docView.op.getDefaultAscent() + (double)underlineAndStrike[0]), (int)partAllocBounds.getWidth(), Math.max(1, Math.round(underlineAndStrike[1])));
                }
            }
            if ((waveUnderlineColor = (Color)attrs.getAttribute(EditorStyleConstants.WaveUnderlineColor)) != null && bottomBorderLineColor == null) {
                g.setColor(waveUnderlineColor);
                float ascent = docView.op.getDefaultAscent();
                Font font = ViewUtils.getFont(attrs, docView.getTextComponent().getFont());
                float[] underlineAndStrike = docView.op.getUnderlineAndStrike(font);
                int yU = (int)(partAllocBounds.getY() + (double)underlineAndStrike[0] + (double)ascent + 0.5);
                int wavePixelCount = (int)partAllocBounds.getWidth() + 1;
                if (wavePixelCount > 0) {
                    int[] waveForm = new int[]{0, 0, -1, -1};
                    int[] xArray = new int[wavePixelCount];
                    int[] yArray = new int[wavePixelCount];
                    int waveFormIndex = x % 4;
                    for (int i = 0; i < wavePixelCount; ++i) {
                        xArray[i] = x + i;
                        yArray[i] = yU + waveForm[waveFormIndex];
                        ++waveFormIndex;
                        waveFormIndex &= 3;
                    }
                    g.drawPolyline(xArray, yArray, wavePixelCount - 1);
                }
            }
            if (leftBorderLineColor != null) {
                g.setColor(leftBorderLineColor);
                g.drawLine(x, y, x, lastY);
            }
            if (rightBorderLineColor != null) {
                g.setColor(rightBorderLineColor);
                g.drawLine(lastX, y, lastX, lastY);
            }
            if (topBorderLineColor != null) {
                g.setColor(topBorderLineColor);
                g.drawLine(x, y, lastX, y);
            }
            if (bottomBorderLineColor != null) {
                g.setColor(bottomBorderLineColor);
                g.drawLine(x, lastY, lastX, lastY);
            }
        }
    }

    static void paintTextLimitLine(Graphics2D g, DocumentView docView, int x, int y, int lastX, int lastY) {
        int textLimitLineX = docView.op.getTextLimitLineX();
        if (textLimitLineX > 0 && textLimitLineX >= x && textLimitLineX <= lastX) {
            g.setColor(docView.op.getTextLimitLineColor());
            g.drawLine(textLimitLineX, y, textLimitLineX, lastY);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static void paintStrikeThrough(Graphics2D g, Rectangle2D textLayoutBounds, Object strikeThroughValue, AttributeSet attrs, DocumentView docView) {
        Color strikeThroughColor;
        if (strikeThroughValue instanceof Boolean) {
            JTextComponent c = docView.getTextComponent();
            strikeThroughColor = Boolean.TRUE.equals(strikeThroughValue) ? g.getColor() : null;
        } else {
            strikeThroughColor = (Color)strikeThroughValue;
        }
        if (strikeThroughColor != null) {
            Color origColor = g.getColor();
            try {
                g.setColor(strikeThroughColor);
                Font font = ViewUtils.getFont(attrs, docView.getTextComponent().getFont());
                float[] underlineAndStrike = docView.op.getUnderlineAndStrike(font);
                g.fillRect((int)textLayoutBounds.getX(), (int)(textLayoutBounds.getY() + (double)docView.op.getDefaultAscent() + (double)underlineAndStrike[2]), (int)textLayoutBounds.getWidth(), Math.max(1, Math.round(underlineAndStrike[3])));
            }
            finally {
                g.setColor(origColor);
            }
        }
    }

    static void paintTextLayout(Graphics2D g, Rectangle2D textLayoutBounds, TextLayout textLayout, DocumentView docView) {
        float x = (float)textLayoutBounds.getX();
        float ascentedY = (float)(textLayoutBounds.getY() + (double)docView.op.getDefaultAscent());
        textLayout.draw(g, x, ascentedY);
    }

    static View breakView(int axis, int breakPartStartOffset, float x, float len, HighlightsView fullView, int partShift, int partLength, TextLayout partTextLayout) {
        if (axis == 0) {
            DocumentView docView = fullView.getDocumentView();
            assert (partTextLayout != null);
            if (docView != null && partLength > 1) {
                float[] locs;
                int breakPartEndOffset;
                boolean breakFailed;
                float breakCharIndexX;
                int fullViewStartOffset = fullView.getStartOffset();
                int partStartOffset = fullViewStartOffset + partShift;
                if (breakPartStartOffset - partStartOffset < 0 || breakPartStartOffset - partStartOffset > partLength) {
                    throw new IllegalArgumentException("offset=" + breakPartStartOffset + "partStartOffset=" + partStartOffset + ", partLength=" + partLength);
                }
                int breakCharIndex = breakPartStartOffset - partStartOffset;
                assert (breakCharIndex >= 0);
                if (breakCharIndex != 0) {
                    TextHitInfo hit = TextHitInfo.leading(breakCharIndex);
                    locs = partTextLayout.getCaretInfo(hit);
                    breakCharIndexX = locs[0];
                } else {
                    breakCharIndexX = 0.0f;
                }
                TextHitInfo hitInfo = HighlightsViewUtils.x2Index(partTextLayout, breakCharIndexX + len);
                locs = partTextLayout.getCaretInfo(hitInfo);
                float endX = locs[0];
                if (endX - breakCharIndexX > len && hitInfo.getCharIndex() > 0) {
                    hitInfo = TextHitInfo.leading(hitInfo.getCharIndex() - 1);
                }
                if ((breakPartEndOffset = partStartOffset + hitInfo.getCharIndex()) > breakPartStartOffset && docView.op.getLineWrapType() == LineWrapType.WORD_BOUND) {
                    CharSequence docText = DocumentUtilities.getText((Document)docView.getDocument());
                    if (breakPartEndOffset > breakPartStartOffset) {
                        boolean searchNonLetterForward = false;
                        char ch = docText.charAt(breakPartEndOffset - 1);
                        if (Character.isLetterOrDigit(ch) && breakPartEndOffset < docText.length() && Character.isLetterOrDigit(docText.charAt(breakPartEndOffset))) {
                            int offset;
                            for (offset = breakPartEndOffset - 1; offset >= breakPartStartOffset && Character.isLetterOrDigit(docText.charAt(offset)); --offset) {
                            }
                            if (++offset == breakPartStartOffset) {
                                searchNonLetterForward = true;
                            } else {
                                breakPartEndOffset = offset;
                            }
                        }
                        if (searchNonLetterForward) {
                            ++breakPartEndOffset;
                            while (breakPartEndOffset < partStartOffset + partLength && Character.isLetterOrDigit(docText.charAt(breakPartEndOffset))) {
                                ++breakPartEndOffset;
                            }
                        }
                    }
                }
                boolean bl = breakFailed = breakPartEndOffset - breakPartStartOffset == 0 || breakPartEndOffset - breakPartStartOffset >= partLength;
                if (breakFailed) {
                    return null;
                }
                return new HighlightsViewPart(fullView, breakPartStartOffset - fullViewStartOffset, breakPartEndOffset - breakPartStartOffset);
            }
        }
        return null;
    }
}

