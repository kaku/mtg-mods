/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.lib2;

import java.awt.Toolkit;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.Acceptor;
import org.netbeans.modules.editor.lib2.AcceptorFactory;
import org.openide.util.NbBundle;

public final class DocUtils {
    private static final Logger LOG = Logger.getLogger(DocUtils.class.getName());

    public static int getRowStart(Document doc, int offset, int lineShift) throws BadLocationException {
        DocUtils.checkOffsetValid(doc, offset);
        if (lineShift != 0) {
            Element lineRoot = doc.getDefaultRootElement();
            int line = lineRoot.getElementIndex(offset);
            if ((line += lineShift) < 0 || line >= lineRoot.getElementCount()) {
                return -1;
            }
            return lineRoot.getElement(line).getStartOffset();
        }
        return doc.getDefaultRootElement().getElement(doc.getDefaultRootElement().getElementIndex(offset)).getStartOffset();
    }

    public static int getRowEnd(Document doc, int offset) throws BadLocationException {
        DocUtils.checkOffsetValid(doc, offset);
        return doc.getDefaultRootElement().getElement(doc.getDefaultRootElement().getElementIndex(offset)).getEndOffset() - 1;
    }

    public static int getLineOffset(Document doc, int offset) throws BadLocationException {
        DocUtils.checkOffsetValid(offset, doc.getLength() + 1);
        return DocUtils.getLineIndex(doc, offset);
    }

    public static int getLineIndex(Document doc, int offset) {
        Element lineRoot = doc.getDefaultRootElement();
        return lineRoot.getElementIndex(offset);
    }

    public static String debugPosition(Document doc, int offset) {
        String ret;
        if (offset >= 0) {
            try {
                int line = DocUtils.getLineIndex(doc, offset) + 1;
                int col = DocUtils.getVisualColumn(doc, offset) + 1;
                ret = String.valueOf(line) + ":" + String.valueOf(col);
            }
            catch (BadLocationException e) {
                ret = NbBundle.getBundle(DocUtils.class).getString("wrong_position") + ' ' + offset + " > " + doc.getLength();
            }
        } else {
            ret = String.valueOf(offset);
        }
        return ret;
    }

    public static int getVisualColumn(Document doc, int offset) throws BadLocationException {
        int docLen = doc.getLength();
        if (offset == docLen + 1) {
            offset = docLen;
        }
        try {
            Method m = DocUtils.findDeclaredMethod(doc.getClass(), "getVisColFromPos", Integer.TYPE);
            m.setAccessible(true);
            int col = (Integer)m.invoke(doc, offset);
            return col;
        }
        catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean transposeLetters(@NonNull Document doc, int offset) {
        if (offset >= 0 && offset <= doc.getLength() - 2) {
            CharSequence text = DocumentUtilities.getText((Document)doc);
            char ch = text.charAt(offset);
            try {
                doc.remove(offset, 1);
                doc.insertString(offset + 1, String.valueOf(ch), null);
            }
            catch (BadLocationException ex) {
                LOG.log(Level.FINE, null, ex);
                Toolkit.getDefaultToolkit().beep();
            }
            return true;
        }
        return false;
    }

    private static /* varargs */ Method findDeclaredMethod(Class<?> clazz, String name, Class ... parameters) throws NoSuchMethodException {
        while (clazz != null) {
            try {
                return clazz.getDeclaredMethod(name, parameters);
            }
            catch (NoSuchMethodException e) {
                clazz = clazz.getSuperclass();
                continue;
            }
        }
        throw new NoSuchMethodException("Method: " + name);
    }

    public static boolean isIdentifierPart(Document doc, char ch) {
        return AcceptorFactory.UNICODE_IDENTIFIER.accept(ch);
    }

    public static boolean isWhitespace(char ch) {
        return AcceptorFactory.WHITESPACE.accept(ch);
    }

    public static void atomicLock(Document doc) {
        try {
            Method lockMethod = doc.getClass().getMethod("atomicLock", new Class[0]);
            lockMethod.invoke(doc, new Object[0]);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static void atomicUnlock(Document doc) {
        try {
            Method unlockMethod = doc.getClass().getMethod("atomicUnlock", new Class[0]);
            unlockMethod.invoke(doc, new Object[0]);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static void runAtomicAsUser(Document doc, Runnable r) {
        try {
            Method m = doc.getClass().getMethod("runAtomicAsUser", Runnable.class);
            m.invoke(doc, r);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, e.getMessage(), e);
        }
    }

    private static void checkOffsetValid(Document doc, int offset) throws BadLocationException {
        DocUtils.checkOffsetValid(offset, doc.getLength());
    }

    private static void checkOffsetValid(int offset, int limitOffset) throws BadLocationException {
        if (offset < 0 || offset > limitOffset) {
            throw new BadLocationException("Invalid offset=" + offset + " not within <0, " + limitOffset + ">", offset);
        }
    }

    private DocUtils() {
    }
}

