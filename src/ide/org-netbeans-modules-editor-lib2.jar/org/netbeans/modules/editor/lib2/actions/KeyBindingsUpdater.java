/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.lib2.actions;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;

public final class KeyBindingsUpdater
implements LookupListener {
    private static Map<String, KeyBindingsUpdater> mime2bindings = new HashMap<String, KeyBindingsUpdater>();
    private final String mimeType;
    private Map<String, List<List<KeyStroke>>> actionName2Binding;
    private final ArrayList<KitReference> kitRefs = new ArrayList(2);
    private final Lookup.Result<KeyBindingSettings> lookupResult;

    public static synchronized KeyBindingsUpdater get(String mimeType) {
        KeyBindingsUpdater bindings = mime2bindings.get(mimeType);
        if (bindings == null) {
            bindings = new KeyBindingsUpdater(mimeType);
            mime2bindings.put(mimeType, bindings);
        }
        return bindings;
    }

    private KeyBindingsUpdater(String mimeType) {
        this.mimeType = mimeType;
        this.lookupResult = MimeLookup.getLookup((String)mimeType).lookup(new Lookup.Template(KeyBindingSettings.class));
        this.lookupResult.addLookupListener((LookupListener)this);
        this.updateActionsAndKits();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addKit(EditorKit kit) {
        Map<String, List<List<KeyStroke>>> actionName2BindingLocal;
        KeyBindingsUpdater keyBindingsUpdater = this;
        synchronized (keyBindingsUpdater) {
            actionName2BindingLocal = this.actionName2Binding;
            this.kitRefs.add(new KitReference(kit));
        }
        KeyBindingsUpdater.updateKits(actionName2BindingLocal, Collections.singletonList(new KitReference(kit)));
    }

    public synchronized void removeKit(EditorKit kit) {
        Iterator<KitReference> it = this.kitRefs.iterator();
        while (it.hasNext()) {
            KitReference ref = it.next();
            if (ref.get() != kit) continue;
            it.remove();
        }
        this.checkEmpty();
    }

    synchronized void removeKitRef(KitReference kitRef) {
        this.kitRefs.remove(kitRef);
        this.checkEmpty();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void checkEmpty() {
        if (!this.kitRefs.isEmpty()) return;
        this.lookupResult.removeLookupListener((LookupListener)this);
        Class<KeyBindingsUpdater> class_ = KeyBindingsUpdater.class;
        synchronized (KeyBindingsUpdater.class) {
            mime2bindings.remove(this.mimeType);
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateActionsAndKits() {
        Collection instances = this.lookupResult.allInstances();
        if (!instances.isEmpty()) {
            Map<String, List<List<KeyStroke>>> actionName2BindingLocal;
            List kitRefsCopy;
            this.updateActions((KeyBindingSettings)instances.iterator().next());
            KeyBindingsUpdater keyBindingsUpdater = this;
            synchronized (keyBindingsUpdater) {
                List krc;
                actionName2BindingLocal = this.actionName2Binding;
                kitRefsCopy = krc = (List)this.kitRefs.clone();
            }
            KeyBindingsUpdater.updateKits(actionName2BindingLocal, kitRefsCopy);
        }
    }

    private synchronized void updateActions(KeyBindingSettings settings) {
        List multiKeyBindings = settings.getKeyBindings();
        this.actionName2Binding = new HashMap<String, List<List<KeyStroke>>>(multiKeyBindings.size() << 1);
        for (MultiKeyBinding mkb : multiKeyBindings) {
            String actionName = mkb.getActionName();
            List mkbList = this.actionName2Binding.get(actionName);
            if (mkbList == null) {
                mkbList = Collections.singletonList(mkb.getKeyStrokeList());
            } else {
                Object[] mkbArray = new List[mkbList.size() + 1];
                mkbList.toArray(mkbArray);
                mkbArray[mkbList.size()] = mkb.getKeyStrokeList();
                mkbList = ArrayUtilities.unmodifiableList((Object[])mkbArray);
            }
            this.actionName2Binding.put(actionName, mkbList);
        }
    }

    private static void updateKits(Map<String, List<List<KeyStroke>>> actionName2binding, List<KitReference> kitRefs) {
        for (KitReference kitRef : kitRefs) {
            EditorKit kit = (EditorKit)kitRef.get();
            if (kit == null) continue;
            Action[] actions = kit.getActions();
            for (int i = 0; i < actions.length; ++i) {
                Action a = actions[i];
                String actionName = (String)a.getValue("Name");
                List origAccels = (List)a.getValue("MultiAcceleratorListKey");
                List accels = actionName2binding.get(actionName);
                if (accels == null) {
                    accels = Collections.emptyList();
                }
                if (origAccels != null && origAccels.equals(accels)) continue;
                a.putValue("MultiAcceleratorListKey", accels);
                if (accels.size() <= 0) continue;
                a.putValue("AcceleratorKey", ((List)accels.get(0)).get(0));
            }
        }
    }

    public void resultChanged(LookupEvent ev) {
        this.updateActionsAndKits();
    }

    private final class KitReference
    extends WeakReference<EditorKit>
    implements Runnable {
        KitReference(EditorKit kit) {
            super(kit, Utilities.activeReferenceQueue());
        }

        @Override
        public void run() {
            KeyBindingsUpdater.this.removeKitRef(this);
        }
    }

}

