/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.editor.lib2;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import org.netbeans.api.editor.EditorActionRegistration;
import org.netbeans.api.editor.EditorActionRegistrations;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public final class EditorActionRegistrationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(EditorActionRegistration.class.getCanonicalName(), EditorActionRegistrations.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(EditorActionRegistration.class)) {
            EditorActionRegistration annotation = e2.getAnnotation(EditorActionRegistration.class);
            this.register(e2, annotation);
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(EditorActionRegistrations.class)) {
            EditorActionRegistrations annotationArray = e.getAnnotation(EditorActionRegistrations.class);
            for (EditorActionRegistration annotation : annotationArray.value()) {
                this.register(e, annotation);
            }
        }
        return true;
    }

    private void register(Element e, EditorActionRegistration annotation) throws LayerGenerationException {
        String createActionMethodName;
        int weight;
        String menuText;
        String actionClassName;
        boolean noKeyBinding;
        String shortDescription;
        int toolBarPosition;
        DeclaredType swingActionType = this.processingEnv.getTypeUtils().getDeclaredType(this.processingEnv.getElementUtils().getTypeElement("javax.swing.Action"), new TypeMirror[0]);
        DeclaredType utilMapType = this.processingEnv.getTypeUtils().getDeclaredType(this.processingEnv.getElementUtils().getTypeElement("java.util.Map"), new TypeMirror[0]);
        boolean directActionCreation = false;
        switch (e.getKind()) {
            case CLASS: {
                boolean mapCtorPublic;
                actionClassName = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e).toString();
                if (e.getModifiers().contains((Object)Modifier.ABSTRACT)) {
                    throw new LayerGenerationException(actionClassName + " must not be abstract", e);
                }
                if (!e.getModifiers().contains((Object)Modifier.PUBLIC)) {
                    throw new LayerGenerationException(actionClassName + " is not public", e);
                }
                Element defaultCtor = null;
                Element mapCtor = null;
                for (ExecutableElement constructor : ElementFilter.constructorsIn(e.getEnclosedElements())) {
                    List<? extends VariableElement> params = constructor.getParameters();
                    if (params.isEmpty()) {
                        defaultCtor = constructor;
                        continue;
                    }
                    if (params.size() != 1 || !this.processingEnv.getTypeUtils().isAssignable(params.get(0).asType(), utilMapType)) continue;
                    mapCtor = constructor;
                }
                String msgBase = "No-argument constructor";
                if (defaultCtor == null) {
                    throw new LayerGenerationException(msgBase + " not present in " + actionClassName, e);
                }
                boolean defaultCtorPublic = defaultCtor != null && defaultCtor.getModifiers().contains((Object)Modifier.PUBLIC);
                boolean bl = mapCtorPublic = mapCtor != null && mapCtor.getModifiers().contains((Object)Modifier.PUBLIC);
                if (!defaultCtorPublic && !mapCtorPublic) {
                    throw new LayerGenerationException(msgBase + " not public in " + actionClassName, e);
                }
                if (!this.processingEnv.getTypeUtils().isAssignable(e.asType(), swingActionType)) {
                    throw new LayerGenerationException(actionClassName + " is not assignable to javax.swing.Action", e);
                }
                createActionMethodName = null;
                break;
            }
            case METHOD: {
                boolean mapParam;
                actionClassName = this.processingEnv.getElementUtils().getBinaryName((TypeElement)e.getEnclosingElement()).toString();
                createActionMethodName = e.getSimpleName().toString();
                if (!e.getModifiers().contains((Object)Modifier.STATIC)) {
                    throw new LayerGenerationException(actionClassName + "." + createActionMethodName + " must be static", e);
                }
                List<? extends VariableElement> params = ((ExecutableElement)e).getParameters();
                boolean emptyParams = params.isEmpty();
                boolean bl = mapParam = params.size() == 1 && this.processingEnv.getTypeUtils().isAssignable(params.get(0).asType(), utilMapType);
                if (!emptyParams && !mapParam) {
                    throw new LayerGenerationException(actionClassName + "." + createActionMethodName + " must not take arguments (or have a single-argument \"Map<String,?> attrs\")", e);
                }
                TypeMirror returnType = ((ExecutableElement)e).getReturnType();
                if (swingActionType != null && !this.processingEnv.getTypeUtils().isAssignable(returnType, swingActionType)) {
                    throw new LayerGenerationException(actionClassName + "." + createActionMethodName + " is not assignable to javax.swing.Action", e);
                }
                if (!mapParam) break;
                directActionCreation = true;
                break;
            }
            default: {
                throw new IllegalArgumentException("Annotated element is not loadable as an instance: " + e);
            }
        }
        String actionName = annotation.name();
        StringBuilder actionFilePathBuilder = new StringBuilder(50);
        String mimeType = annotation.mimeType();
        actionFilePathBuilder.append("Editors");
        if (mimeType.length() > 0) {
            actionFilePathBuilder.append("/").append(mimeType);
        }
        actionFilePathBuilder.append("/Actions/").append(actionName).append(".instance");
        LayerBuilder layer = this.layer(new Element[]{e});
        String actionFilePath = actionFilePathBuilder.toString();
        LayerBuilder.File actionFile = layer.file(actionFilePath);
        String preferencesKey = annotation.preferencesKey();
        String iconResource = annotation.iconResource();
        if (iconResource.length() > 0) {
            actionFile.stringvalue("iconBase", iconResource);
        }
        if ((shortDescription = annotation.shortDescription()).length() > 0) {
            if ("INHERIT".equals(shortDescription)) {
                actionFile.methodvalue("displayName", EditorActionUtilities.class.getName(), "getGlobalActionShortDescription");
                actionFile.methodvalue("ShortDescription", EditorActionUtilities.class.getName(), "getGlobalActionShortDescription");
            } else {
                if ("BY_ACTION_NAME".equals(shortDescription)) {
                    shortDescription = "#" + actionName;
                }
                actionFile.bundlevalue("displayName", shortDescription);
                actionFile.bundlevalue("ShortDescription", shortDescription);
            }
        }
        if ((menuText = annotation.menuText()).length() > 0) {
            actionFile.bundlevalue("menuText", menuText);
        } else if (shortDescription.length() > 0) {
            menuText = shortDescription;
            actionFile.bundlevalue("menuText", menuText);
        }
        String popupText = annotation.popupText();
        if (popupText.length() > 0) {
            actionFile.bundlevalue("popupText", popupText);
        } else if (menuText.length() > 0) {
            popupText = menuText;
            actionFile.bundlevalue("popupText", popupText);
        }
        String menuPath = annotation.menuPath();
        int menuPosition = annotation.menuPosition();
        if (menuPosition != Integer.MAX_VALUE) {
            StringBuilder menuPresenterFilePath = new StringBuilder(50);
            menuPresenterFilePath.append("Menu/");
            if (menuPath.length() > 0) {
                menuPresenterFilePath.append(menuPath).append('/');
            }
            menuPresenterFilePath.append(actionName).append(".shadow");
            LayerBuilder.File menuPresenterShadowFile = layer.file(menuPresenterFilePath.toString());
            menuPresenterShadowFile.stringvalue("originalFile", actionFilePath);
            menuPresenterShadowFile.intvalue("position", menuPosition);
            menuPresenterShadowFile.write();
        }
        String popupPath = annotation.popupPath();
        int popupPosition = annotation.popupPosition();
        if (popupPosition != Integer.MAX_VALUE) {
            StringBuilder popupPresenterFilePath = new StringBuilder(50);
            popupPresenterFilePath.append("Editors/Popup/");
            if (mimeType.length() > 0) {
                popupPresenterFilePath.append(mimeType).append("/");
            }
            if (popupPath.length() > 0) {
                popupPresenterFilePath.append(popupPath).append('/');
            }
            popupPresenterFilePath.append(actionName).append(".shadow");
            LayerBuilder.File popupPresenterShadowFile = layer.file(popupPresenterFilePath.toString());
            popupPresenterShadowFile.stringvalue("originalFile", actionFilePath);
            popupPresenterShadowFile.intvalue("position", popupPosition);
            popupPresenterShadowFile.write();
        }
        if ((toolBarPosition = annotation.toolBarPosition()) != Integer.MAX_VALUE) {
            StringBuilder toolBarPresenterFilePath = new StringBuilder(50);
            toolBarPresenterFilePath.append("Editors/Toolbar/");
            if (mimeType.length() > 0) {
                toolBarPresenterFilePath.append(mimeType).append("/");
            }
            toolBarPresenterFilePath.append(actionName).append(".shadow");
            LayerBuilder.File toolBarPresenterShadowFile = layer.file(toolBarPresenterFilePath.toString());
            toolBarPresenterShadowFile.stringvalue("originalFile", actionFilePath);
            toolBarPresenterShadowFile.intvalue("position", toolBarPosition);
            toolBarPresenterShadowFile.write();
        }
        if (preferencesKey.length() > 0) {
            actionFile.stringvalue("preferencesKey", preferencesKey);
            actionFile.methodvalue("preferencesNode", EditorActionUtilities.class.getName(), "getGlobalPreferences");
            actionFile.boolvalue("preferencesDefault", annotation.preferencesDefault());
        }
        actionFile.stringvalue("Name", actionName);
        actionFile.stringvalue("helpID", actionName);
        boolean noIconInMenu = annotation.noIconInMenu();
        if (noIconInMenu) {
            actionFile.boolvalue("noIconInMenu", noIconInMenu);
        }
        if (noKeyBinding = annotation.noKeyBinding()) {
            actionFile.boolvalue("no-keybinding", noKeyBinding);
        }
        if ((weight = annotation.weight()) != 0) {
            actionFile.intvalue("weight", weight);
        }
        if (directActionCreation) {
            actionFile.methodvalue("instanceCreate", actionClassName, createActionMethodName);
        } else {
            actionFile.methodvalue("instanceCreate", "org.netbeans.modules.editor.lib2.actions.WrapperEditorAction", "create");
            actionFile.boolvalue("WrapperActionKey", true);
            if (createActionMethodName != null) {
                actionFile.methodvalue("delegate", actionClassName, createActionMethodName);
            } else {
                actionFile.newvalue("delegate", actionClassName);
            }
        }
        actionFile.write();
    }

}

