/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.modules.editor.lib2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.lib.editor.util.ArrayUtilities;

public class RectangularSelectionUtils {
    public static final String RECTANGULAR_DO_NOT_RESET_AFTER_DOCUMENT_CHANGE = "rectangular-document-change-allowed";
    private static final String RECTANGULAR_SELECTION_PROPERTY = "rectangular-selection";
    private static final String RECTANGULAR_SELECTION_REGIONS_PROPERTY = "rectangular-selection-regions";

    public static void resetRectangularSelection(JTextComponent c) {
        c.getCaretPosition();
        c.putClientProperty("rectangular-selection-regions", new ArrayList());
        boolean value = !RectangularSelectionUtils.isRectangularSelection(c);
        RectangularSelectionUtils.setRectangularSelection(c, value);
        RectangularSelectionUtils.setRectangularSelection(c, !value);
    }

    public static boolean isRectangularSelection(JComponent c) {
        return Boolean.TRUE.equals(c.getClientProperty("rectangular-selection"));
    }

    public static void setRectangularSelection(JComponent c, boolean value) {
        c.putClientProperty("rectangular-selection", value);
    }

    public static String getRectangularSelectionProperty() {
        return "rectangular-selection";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static List<Position> regionsCopy(JComponent c) {
        ArrayList lRegions;
        List regions = (List)c.getClientProperty("rectangular-selection-regions");
        if (regions != null) {
            List list = regions;
            synchronized (list) {
                lRegions = new ArrayList(regions);
            }
        } else {
            lRegions = null;
        }
        return lRegions;
    }

    public static boolean removeSelection(JTextComponent tc) throws BadLocationException {
        List<Position> regions = RectangularSelectionUtils.regionsCopy(tc);
        return RectangularSelectionUtils.removeSelection(tc.getDocument(), regions);
    }

    public static boolean removeSelection(Document doc, List<Position> regions) throws BadLocationException {
        boolean textRemoved = false;
        int regionsLength = regions.size();
        int i = 0;
        while (i < regionsLength) {
            int startOffset = regions.get(i++).getOffset();
            int endOffset = regions.get(i++).getOffset();
            int len = endOffset - startOffset;
            doc.remove(startOffset, len);
            textRemoved |= len > 0;
        }
        return textRemoved;
    }

    public static void removeChar(JTextComponent tc, boolean nextChar) throws BadLocationException {
        List<Position> regions = RectangularSelectionUtils.regionsCopy(tc);
        Document doc = tc.getDocument();
        doc.putProperty("rectangular-document-change-allowed", Boolean.TRUE);
        int regionsLength = regions.size();
        Element lineRoot = null;
        int lineIndex = 0;
        for (int i = 1; i < regionsLength; i += 2) {
            int offset = regions.get(i).getOffset();
            if (lineRoot == null) {
                lineRoot = doc.getDefaultRootElement();
                lineIndex = lineRoot.getElementIndex(offset);
            }
            Element line = lineRoot.getElement(lineIndex++);
            if (nextChar) {
                if (offset >= line.getEndOffset() - 1) continue;
                doc.remove(offset, 1);
                continue;
            }
            if (offset <= line.getStartOffset()) continue;
            doc.remove(offset - 1, 1);
        }
    }

    public static void insertText(Document doc, List<Position> regions, String text) throws BadLocationException {
        doc.putProperty("rectangular-document-change-allowed", Boolean.TRUE);
        int regionsLength = regions.size();
        for (int i = 1; i < regionsLength; i += 2) {
            int offset = regions.get(i).getOffset();
            doc.insertString(offset, text, null);
        }
    }

    public static String regionsToString(List<Position> regions) {
        StringBuilder sb = new StringBuilder(200);
        int regionsLength = regions.size();
        int digitCount = ArrayUtilities.digitCount((int)(regionsLength >>> 1));
        int i = 0;
        while (i < regionsLength) {
            int startOffset = regions.get(i++).getOffset();
            int endOffset = regions.get(i++).getOffset();
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)(i >>> 1), (int)digitCount);
            sb.append('<').append(startOffset).append(',').append(endOffset).append(">\n");
        }
        return sb.toString();
    }
}

