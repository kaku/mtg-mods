/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.modules.editor.lib2.highlighting;

import java.io.PrintStream;
import java.util.Collection;
import org.netbeans.lib.editor.util.GapList;

public abstract class AbstractOffsetGapList<E>
extends GapList<E> {
    private int offsetGapStart;
    private int offsetGapLength = 1073741823;
    private final boolean fixedZeroOffset;

    public AbstractOffsetGapList() {
        this(false);
    }

    public AbstractOffsetGapList(boolean fixedZeroOffset) {
        this.fixedZeroOffset = fixedZeroOffset;
    }

    public final boolean add(E element) {
        int originalOffset = this.attachElement(element);
        this.setElementRawOffset(element, this.offset2raw(originalOffset));
        int index = this.findOffsetIndex(originalOffset);
        GapList.super.add(index, element);
        return true;
    }

    public final void add(int index, E element) {
        if (index < 0 || index > this.size()) {
            throw new IndexOutOfBoundsException("index = " + index + " size = " + this.size());
        }
        int originalOffset = this.attachElement(element);
        this.setElementRawOffset(element, this.offset2raw(originalOffset));
        boolean ok = true;
        if (index > 0 && this.elementOffset(index - 1) > originalOffset) {
            System.out.println("[" + (index - 1) + "] = " + this.elementOffset(index - 1) + " > {" + index + "} = " + originalOffset);
            ok = false;
        }
        if (index < this.size() && this.elementOffset(index) < originalOffset) {
            System.out.println("[" + index + "] = " + this.elementOffset(index) + " < {" + index + "} = " + originalOffset);
            ok = false;
        }
        if (!ok) {
            this.detachElement(element);
            throw new IllegalStateException("Can't insert element at index: " + index);
        }
        GapList.super.add(index, element);
    }

    public final boolean addAll(Collection<? extends E> c) {
        for (E e : c) {
            this.add(e);
        }
        return true;
    }

    public final boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("This is an illegal operation on OffsetGapList.");
    }

    public final boolean addArray(int index, Object[] elements) {
        throw new UnsupportedOperationException("This is an illegal operation on OffsetGapList.");
    }

    public final boolean addArray(int index, Object[] elements, int off, int len) {
        throw new UnsupportedOperationException("This is an illegal operation on OffsetGapList.");
    }

    public final int indexOf(Object o) {
        int idx;
        int offset;
        E element = this.getAttachedElement(o);
        if (element != null && (idx = this.findElementIndex(offset = this.elementOffset(element))) >= 0) {
            for (int i = idx; i < this.size() && this.elementOffset(i) == offset; ++i) {
                if (element != this.get(i)) continue;
                return i;
            }
        }
        return -1;
    }

    public final int lastIndexOf(Object element) {
        return this.indexOf(element);
    }

    public final E set(int index, E element) {
        if (index < 0 || index >= this.size()) {
            throw new IndexOutOfBoundsException("index = " + index + " size = " + this.size());
        }
        int originalOffset = this.attachElement(element);
        this.setElementRawOffset(element, this.offset2raw(originalOffset));
        if (index > 0 && this.elementOffset(index - 1) > originalOffset) {
            String log = "[" + (index - 1) + "] = " + this.elementOffset(index - 1) + " > {" + index + "} = " + originalOffset;
            this.detachElement(element);
            throw new IllegalStateException("Can't insert element at index: " + index + log);
        }
        if (index + 1 < this.size() && this.elementOffset(index + 1) < originalOffset) {
            String log = "[" + (index + 1) + "] = " + this.elementOffset(index + 1) + " < {" + index + "} = " + originalOffset;
            this.detachElement(element);
            throw new IllegalStateException("Can't insert element at index: " + index + log);
        }
        Object oldElement = GapList.super.set(index, element);
        this.detachElement(oldElement);
        return (E)oldElement;
    }

    public final void swap(int index1, int index2) {
        throw new UnsupportedOperationException("This is an illegal operation on OffsetGapList.");
    }

    protected abstract int elementRawOffset(E var1);

    protected abstract void setElementRawOffset(E var1, int var2);

    protected abstract int attachElement(E var1);

    protected abstract void detachElement(E var1);

    protected abstract E getAttachedElement(Object var1);

    protected int elementOffset(E elem) {
        return this.raw2Offset(this.elementRawOffset(elem));
    }

    protected int elementOffset(int index) {
        return this.elementOffset(this.get(index));
    }

    public void defaultInsertUpdate(int offset, int length) {
        assert (length >= 0);
        if (offset != this.offsetGapStart()) {
            this.moveOffsetGap(offset, this.findOffsetIndex(offset));
        }
        this.updateOffsetGapLength(- length);
        this.updateOffsetGapStart(length);
    }

    public void defaultRemoveUpdate(int offset, int length) {
        Object elem;
        int rawOffset;
        assert (length >= 0);
        int index = this.findOffsetIndex(offset);
        if (offset != this.offsetGapStart()) {
            this.moveOffsetGap(offset, index);
        }
        int size = this.size();
        int removeAreaEndRawOffset = offset + this.offsetGapLength + length;
        while (index < size && (rawOffset = this.elementRawOffset(elem = this.get(index++))) < removeAreaEndRawOffset) {
            if (this.fixedZeroOffset && rawOffset == 0) continue;
            this.setElementRawOffset(elem, removeAreaEndRawOffset);
        }
        this.updateOffsetGapLength(length);
    }

    protected final void moveOffsetGap(int offset, int index) {
        if (offset < this.offsetGapStart) {
            Object elem;
            int rawOffset;
            int bound = this.size();
            for (int i = index; i < bound && (rawOffset = this.elementRawOffset(elem = this.get(i))) < this.offsetGapStart; ++i) {
                if (this.fixedZeroOffset && rawOffset == 0) continue;
                this.setElementRawOffset(elem, rawOffset + this.offsetGapLength);
            }
        } else {
            Object elem;
            int rawOffset;
            for (int i = index - 1; i >= 0 && (rawOffset = this.elementRawOffset(elem = this.get(i))) >= this.offsetGapStart; --i) {
                if (this.fixedZeroOffset && rawOffset == 0) continue;
                this.setElementRawOffset(elem, rawOffset - this.offsetGapLength);
            }
        }
        this.offsetGapStart = offset;
    }

    protected final int offsetGapStart() {
        return this.offsetGapStart;
    }

    protected final void updateOffsetGapStart(int offsetDelta) {
        this.offsetGapStart += offsetDelta;
    }

    protected final int offsetGapLength() {
        return this.offsetGapLength;
    }

    protected final void updateOffsetGapLength(int offsetGapLengthDelta) {
        this.offsetGapLength += offsetGapLengthDelta;
        assert (this.offsetGapLength >= 0);
    }

    protected final int findOffsetIndex(int offset) {
        int index = this.findElementIndex(offset);
        return index < 0 ? - index - 1 : index;
    }

    public final int findElementIndex(int offset, int lowIdx, int highIdx) {
        if (lowIdx < 0 || highIdx > this.size() - 1) {
            throw new IndexOutOfBoundsException("lowIdx = " + lowIdx + ", highIdx = " + highIdx + ", size = " + this.size());
        }
        int low = lowIdx;
        int high = highIdx;
        while (low <= high) {
            int index = low + high >> 1;
            int elemOffset = this.elementOffset(index);
            if (elemOffset < offset) {
                low = index + 1;
                continue;
            }
            if (elemOffset > offset) {
                high = index - 1;
                continue;
            }
            while (index > 0) {
                if (this.elementOffset(--index) >= offset) continue;
                ++index;
                break;
            }
            return index;
        }
        return - low + 1;
    }

    public final int findElementIndex(int offset) {
        return this.findElementIndex(offset, 0, this.size() - 1);
    }

    protected void updateElementOffsetAdd(E elem) {
        int rawOffset = this.elementRawOffset(elem);
        if (rawOffset >= this.offsetGapStart) {
            this.setElementRawOffset(elem, rawOffset + this.offsetGapLength);
        }
    }

    protected void updateElementOffsetRemove(E elem) {
        int rawOffset = this.elementRawOffset(elem);
        if (rawOffset >= this.offsetGapStart) {
            this.setElementRawOffset(elem, rawOffset - this.offsetGapLength);
        }
    }

    protected final int raw2Offset(int rawOffset) {
        if (this.fixedZeroOffset && rawOffset == 0) {
            return 0;
        }
        return rawOffset < this.offsetGapStart ? rawOffset : rawOffset - this.offsetGapLength;
    }

    protected final int offset2raw(int offset) {
        if (this.fixedZeroOffset && offset == 0) {
            return 0;
        }
        return offset < this.offsetGapStart ? offset : offset + this.offsetGapLength;
    }

    protected void consistencyCheck() {
        GapList.super.consistencyCheck();
        if (this.offsetGapLength < 0) {
            this.consistencyError("offsetGapLength < 0");
        }
        int lastRawOffset = Integer.MIN_VALUE;
        int lastOffset = Integer.MIN_VALUE;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            Object elem = this.get(i);
            int rawOffset = this.elementRawOffset(elem);
            int offset = this.raw2Offset(rawOffset);
            if (rawOffset < lastRawOffset) {
                this.consistencyError("Invalid rawOffset=" + rawOffset + " >= lastRawOffset=" + lastRawOffset + " at index=" + i);
            }
            if (offset < lastOffset) {
                this.consistencyError("Invalid offset=" + offset + " >= lastOffset=" + lastOffset + " at index=" + i);
            }
            lastRawOffset = rawOffset;
            lastOffset = offset;
        }
    }

    protected String dumpInternals() {
        return GapList.super.dumpInternals() + ", offGap(s=" + this.offsetGapStart + ", l=" + this.offsetGapLength + ")";
    }
}

