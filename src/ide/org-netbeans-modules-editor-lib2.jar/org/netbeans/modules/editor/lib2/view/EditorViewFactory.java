/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ListenerList
 */
package org.netbeans.modules.editor.lib2.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryEvent;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryListener;
import org.netbeans.modules.editor.lib2.view.ViewBuilder;

public abstract class EditorViewFactory {
    private static volatile List<Factory> viewFactoryFactories = Collections.emptyList();
    private final DocumentView docView;
    private final JTextComponent component;
    private ViewBuilder viewBuilder;
    private final ListenerList<EditorViewFactoryListener> listenerList = new ListenerList();

    public static synchronized void registerFactory(Factory factory) {
        ArrayList<Factory> copy = new ArrayList<Factory>(viewFactoryFactories);
        copy.add(factory);
        Collections.sort(copy, new Comparator<Factory>(){

            @Override
            public int compare(Factory f0, Factory f1) {
                return f0.weight() - f1.weight();
            }
        });
        viewFactoryFactories = Collections.unmodifiableList(copy);
    }

    public static List<Factory> factories() {
        return viewFactoryFactories;
    }

    protected EditorViewFactory(View documentView) {
        assert (documentView instanceof DocumentView);
        this.docView = (DocumentView)documentView;
        this.component = this.docView.getTextComponent();
    }

    protected final JTextComponent textComponent() {
        return this.component;
    }

    protected final Document document() {
        return this.docView.getDocument();
    }

    public abstract void restart(int var1, int var2, boolean var3);

    public abstract void continueCreation(int var1, int var2);

    public abstract int nextViewStartOffset(int var1);

    public abstract EditorView createView(int var1, int var2, boolean var3, EditorView var4, int var5);

    public abstract int viewEndOffset(int var1, int var2, boolean var3);

    public abstract void finishCreation();

    public void addEditorViewFactoryListener(EditorViewFactoryListener listener) {
        this.listenerList.add((EventListener)listener);
    }

    public void removeEditorViewFactoryListener(EditorViewFactoryListener listener) {
        this.listenerList.remove((EventListener)listener);
    }

    protected void fireEvent(List<EditorViewFactoryChange> changes) {
        EditorViewFactoryEvent evt = new EditorViewFactoryEvent(this, changes);
        for (EditorViewFactoryListener listener : this.listenerList.getListeners()) {
            listener.viewFactoryChanged(evt);
        }
    }

    public void offsetRepaint(int startOffset, int endOffset) {
        this.docView.offsetRepaint(startOffset, endOffset);
    }

    protected final void notifyStaleCreation() {
        ViewBuilder builder = this.viewBuilder;
        if (builder != null) {
            builder.notifyStaleCreation();
        }
    }

    void setViewBuilder(ViewBuilder viewBuilder) {
        this.viewBuilder = viewBuilder;
    }

    public String toString() {
        ViewBuilder vb = this.viewBuilder;
        return "viewBuilder:\n" + vb;
    }

    public static interface Factory {
        public EditorViewFactory createEditorViewFactory(View var1);

        public int weight();
    }

    public static final class Change {
        private final int startOffset;
        private final int endOffset;

        Change(int startOffset, int endOffset) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
        }

        public int getStartOffset() {
            return this.startOffset;
        }

        public int getEndOffset() {
            return this.endOffset;
        }

        public String toString() {
            return "<" + this.getStartOffset() + "," + this.getEndOffset() + ">";
        }
    }

}

