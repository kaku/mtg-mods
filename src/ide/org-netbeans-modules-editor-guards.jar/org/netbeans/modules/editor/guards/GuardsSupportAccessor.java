/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import org.netbeans.spi.editor.guards.support.AbstractGuardedSectionsProvider;

public abstract class GuardsSupportAccessor {
    public static GuardsSupportAccessor DEFAULT;

    public abstract boolean isUseReadersWritersOnSet(AbstractGuardedSectionsProvider var1);

    static {
        Class<AbstractGuardedSectionsProvider> clazz = AbstractGuardedSectionsProvider.class;
        try {
            Class.forName(clazz.getName(), true, clazz.getClassLoader());
        }
        catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }
}

