/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.PositionBounds;

public final class SimpleSectionImpl
extends GuardedSectionImpl {
    private PositionBounds bounds;

    SimpleSectionImpl(String name, PositionBounds bounds, GuardedSectionsImpl guards) {
        super(name, guards);
        this.bounds = bounds;
    }

    @Override
    public void setName(String name) throws PropertyVetoException {
        super.setName(name);
        this.setText(this.getText());
    }

    public void setText(String text) {
        this.setText(this.bounds, text, true, new GuardedSectionImpl.ContentGetter(){

            @Override
            public PositionBounds getContent(GuardedSectionImpl t) {
                return ((SimpleSectionImpl)t).bounds;
            }
        });
    }

    @Override
    void markGuarded(StyledDocument doc) {
        this.markGuarded(doc, this.bounds, true);
    }

    @Override
    void unmarkGuarded(StyledDocument doc) {
        this.markGuarded(doc, this.bounds, false);
    }

    @Override
    public Position getCaretPosition() {
        return this.bounds.getBegin();
    }

    @Override
    public String getText() {
        String text = "";
        try {
            text = this.bounds.getText();
        }
        catch (BadLocationException ex) {
            Logger.getLogger("guards").log(Level.ALL, null, ex);
        }
        return text;
    }

    @Override
    public Position getEndPosition() {
        return this.bounds.getEnd();
    }

    @Override
    public boolean contains(Position pos, boolean allowHoles) {
        return this.bounds.getBegin().getOffset() <= pos.getOffset() && this.bounds.getEnd().getOffset() >= pos.getOffset();
    }

    @Override
    public Position getStartPosition() {
        return this.bounds.getBegin();
    }

    @Override
    public void resolvePositions() throws BadLocationException {
        this.bounds.resolvePositions();
    }

}

