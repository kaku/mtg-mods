/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.editor.guards;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.GuardsAccessor;
import org.netbeans.modules.editor.guards.PositionBounds;
import org.netbeans.spi.editor.guards.support.AbstractGuardedSectionsProvider;
import org.openide.text.NbDocument;

public abstract class GuardedSectionImpl {
    String name;
    boolean valid = false;
    final GuardedSectionsImpl guards;
    GuardedSection guard;

    public String getName() {
        return this.name;
    }

    GuardedSectionImpl(String name, GuardedSectionsImpl guards) {
        this.name = name;
        this.guards = guards;
    }

    public final void attach(GuardedSection guard) {
        this.guard = guard;
        this.valid = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setName(String name) throws PropertyVetoException {
        if (!this.name.equals(name)) {
            Map<String, GuardedSectionImpl> map = this.guards.sections;
            synchronized (map) {
                if (this.valid) {
                    if (this.guards.sections.get(name) != null) {
                        throw new PropertyVetoException("", new PropertyChangeEvent(this, "name", this.name, name));
                    }
                    this.guards.sections.remove(this.name);
                    this.name = name;
                    this.guards.sections.put(name, this);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void deleteSection() {
        Map<String, GuardedSectionImpl> map = this.guards.sections;
        synchronized (map) {
            if (this.valid) {
                try {
                    this.guards.sections.remove(this.name);
                    this.unmarkGuarded(this.guards.getDocument());
                    this.deleteText();
                    this.valid = false;
                }
                catch (BadLocationException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    public boolean isValid() {
        return this.valid;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeSection() {
        Map<String, GuardedSectionImpl> map = this.guards.sections;
        synchronized (map) {
            if (this.valid) {
                this.guards.sections.remove(this.name);
                this.unmarkGuarded(this.guards.getDocument());
                this.valid = false;
            }
        }
    }

    protected boolean setText(PositionBounds bounds, String text, boolean minLen, ContentGetter contentGetter) {
        if (!this.valid) {
            return false;
        }
        if (minLen && (text.length() == 0 || text.length() == 1 && text.equals("\n"))) {
            text = " ";
        }
        if (text.endsWith("\n")) {
            text = text.substring(0, text.length() - 1);
        }
        try {
            char[] data;
            int offset;
            AbstractGuardedSectionsProvider.Result result;
            List<GuardedSection> guardedSections;
            bounds.setText(text);
            if (this.guards.gr != null && (guardedSections = (result = this.guards.gr.readSections(data = this.guards.gr.writeSections(Collections.singletonList(GuardsAccessor.DEFAULT.clone(this.guard, (offset = this.getStartPosition().getOffset()) - 1)), ("\n" + new PositionBounds(this.getStartPosition(), this.getEndPosition(), this.guards).getText() + "\n").toCharArray()))).getGuardedSections()).size() == 1) {
                PositionBounds contentBounds = contentGetter.getContent(GuardsAccessor.DEFAULT.getImpl(guardedSections.get(0)));
                bounds.setText(new String(result.getContent(), contentBounds.getBegin().getOffset(), contentBounds.getEnd().getOffset() - contentBounds.getBegin().getOffset()));
            }
            return true;
        }
        catch (BadLocationException e) {
            return false;
        }
    }

    void markGuarded(StyledDocument doc, PositionBounds bounds, boolean mark) {
        int begin = bounds.getBegin().getOffset();
        int end = bounds.getEnd().getOffset();
        if (mark) {
            NbDocument.markGuarded((StyledDocument)doc, (int)begin, (int)(end - begin + 1));
        } else {
            NbDocument.unmarkGuarded((StyledDocument)doc, (int)begin, (int)(end - begin + 1));
        }
    }

    abstract void markGuarded(StyledDocument var1);

    abstract void unmarkGuarded(StyledDocument var1);

    final void deleteText() throws BadLocationException {
        if (this.valid) {
            final StyledDocument doc = this.guards.getDocument();
            final BadLocationException[] blex = new BadLocationException[1];
            NbDocument.runAtomic((StyledDocument)doc, (Runnable)new Runnable(){

                @Override
                public void run() {
                    try {
                        int start = GuardedSectionImpl.this.getStartPosition().getOffset();
                        if (start > 0 && "\n".equals(doc.getText(start - 1, 1))) {
                            --start;
                        }
                        doc.remove(start, GuardedSectionImpl.this.getEndPosition().getOffset() - start + 1);
                    }
                    catch (BadLocationException ex) {
                        blex[0] = ex;
                    }
                }
            });
            if (blex[0] != null) {
                throw blex[0];
            }
        }
    }

    public abstract Position getCaretPosition();

    public abstract String getText();

    public abstract boolean contains(Position var1, boolean var2);

    public abstract Position getEndPosition();

    public abstract Position getStartPosition();

    public abstract void resolvePositions() throws BadLocationException;

    static interface ContentGetter<T extends GuardedSectionImpl> {
        public PositionBounds getContent(GuardedSectionImpl var1);
    }

}

