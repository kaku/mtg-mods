/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

enum NewLine {
    N,
    R,
    RN;
    

    private NewLine() {
    }

    public static NewLine resolve(String newline) {
        if (newline.equals("\r")) {
            return R;
        }
        if (newline.equals("\r\n")) {
            return RN;
        }
        return N;
    }
}

