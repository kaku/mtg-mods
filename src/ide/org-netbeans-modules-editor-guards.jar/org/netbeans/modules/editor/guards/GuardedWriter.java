/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.spi.editor.guards.support.AbstractGuardedSectionsProvider;

final class GuardedWriter
extends Writer {
    private Writer writer;
    private CharArrayWriter buffer;
    private final AbstractGuardedSectionsProvider gw;
    private boolean isClosed = false;
    private final List<GuardedSection> sections;

    public GuardedWriter(AbstractGuardedSectionsProvider gw, OutputStream os, List<GuardedSection> list, Charset encoding) {
        this.writer = encoding == null ? new OutputStreamWriter(os) : new OutputStreamWriter(os, encoding);
        this.gw = gw;
        this.sections = list;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        if (this.buffer == null) {
            this.buffer = new CharArrayWriter(10240);
        }
        this.buffer.write(cbuf, off, len);
    }

    @Override
    public void close() throws IOException {
        if (this.isClosed) {
            return;
        }
        this.isClosed = true;
        if (this.buffer != null) {
            char[] content = this.gw.writeSections(this.sections, this.buffer.toCharArray());
            this.writer.write(content);
        }
        this.writer.close();
    }

    @Override
    public void flush() throws IOException {
    }
}

