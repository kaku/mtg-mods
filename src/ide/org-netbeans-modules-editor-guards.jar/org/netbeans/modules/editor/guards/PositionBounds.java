/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.editor.guards;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.openide.text.NbDocument;

public final class PositionBounds {
    private Position begin;
    private Position end;
    private final GuardedSectionsImpl guards;

    public PositionBounds(Position begin, Position end, GuardedSectionsImpl guards) {
        this.begin = begin;
        this.end = end;
        this.guards = guards;
        this.assertPositionBounds();
    }

    public static PositionBounds create(int begin, int end, GuardedSectionsImpl guards) throws BadLocationException {
        StyledDocument doc = guards.getDocument();
        return new PositionBounds(doc.createPosition(begin), doc.createPosition(end), guards);
    }

    public static PositionBounds createBodyBounds(int begin, int end, GuardedSectionsImpl guards) throws BadLocationException {
        StyledDocument doc = guards.getDocument();
        return new PositionBounds(new BiasedPosition(doc.createPosition(begin - 1), Position.Bias.Backward), new BiasedPosition(doc.createPosition(end + 1), Position.Bias.Forward), guards);
    }

    public static PositionBounds createUnresolved(int begin, int end, GuardedSectionsImpl guards) throws BadLocationException {
        StyledDocument doc = guards.getDocument();
        return new PositionBounds(new UnresolvedPosition(begin), new UnresolvedPosition(end), guards);
    }

    public static PositionBounds createBodyUnresolved(int begin, int end, GuardedSectionsImpl guards) throws BadLocationException {
        return new PositionBounds(new BiasedPosition(new UnresolvedPosition(begin - 1), Position.Bias.Backward), new BiasedPosition(new UnresolvedPosition(end + 1), Position.Bias.Forward), guards);
    }

    public void resolvePositions() throws BadLocationException {
        StyledDocument doc = this.guards.getDocument();
        if (this.begin instanceof UnresolvedPosition) {
            this.begin = doc.createPosition(this.begin.getOffset());
        } else if (this.begin instanceof BiasedPosition) {
            ((BiasedPosition)this.begin).resolve(doc);
        }
        if (this.end instanceof UnresolvedPosition) {
            this.end = doc.createPosition(this.end.getOffset());
        } else if (this.end instanceof BiasedPosition) {
            ((BiasedPosition)this.end).resolve(doc);
        }
        this.assertPositionBounds();
    }

    public Position getBegin() {
        return this.begin;
    }

    public Position getEnd() {
        return this.end;
    }

    public void setText(final String text) throws BadLocationException {
        final StyledDocument doc = this.guards.getDocument();
        final BadLocationException[] hold = new BadLocationException[]{null};
        Runnable run = new Runnable(){

            @Override
            public void run() {
                try {
                    int p1 = PositionBounds.this.begin.getOffset();
                    int p2 = PositionBounds.this.end.getOffset();
                    int len = text.length();
                    if (len == 0) {
                        if (p2 > p1) {
                            doc.remove(p1, p2 - p1);
                        }
                    } else {
                        int docLen = doc.getLength();
                        if (p2 - p1 >= 1) {
                            doc.insertString(p1 + 1, text, null);
                            len = doc.getLength() - docLen;
                            doc.remove(p1 + 1 + len, p2 - p1 - 1);
                            doc.remove(p1, 1);
                        } else {
                            doc.insertString(p1, text, null);
                            len = doc.getLength() - docLen;
                            if (p2 > p1) {
                                doc.remove(p1 + len, p2 - p1);
                            }
                            if (PositionBounds.this.begin.getOffset() != p1) {
                                PositionBounds.this.begin = doc.createPosition(p1);
                            }
                            if (PositionBounds.this.end.getOffset() - p1 != len) {
                                PositionBounds.this.end = doc.createPosition(p1 + len);
                            }
                            PositionBounds.this.assertPositionBounds();
                        }
                    }
                }
                catch (BadLocationException e) {
                    hold[0] = e;
                }
            }
        };
        NbDocument.runAtomic((StyledDocument)doc, (Runnable)run);
        if (hold[0] != null) {
            throw hold[0];
        }
    }

    public String getText() throws BadLocationException {
        int p2;
        StyledDocument doc = this.guards.getDocument();
        int p1 = this.begin.getOffset();
        return p1 <= (p2 = this.end.getOffset()) ? doc.getText(p1, p2 - p1) : "";
    }

    private void assertPositionBounds() {
    }

    public String toString() {
        StringBuilder buf = new StringBuilder("Position bounds[");
        try {
            String content = this.getText();
            buf.append(this.begin);
            buf.append(",");
            buf.append(this.end);
            buf.append(",\"");
            buf.append(content);
            buf.append("\"");
        }
        catch (BadLocationException e) {
            buf.append("Invalid: ");
            buf.append(e.getMessage());
        }
        buf.append("]");
        return buf.toString();
    }

    private static final class BiasedPosition
    implements Position {
        private Position delegate;
        private Position.Bias bias;

        public BiasedPosition(Position delegate, Position.Bias bias) {
            this.delegate = delegate;
            this.bias = bias;
        }

        @Override
        public int getOffset() {
            return this.bias == Position.Bias.Backward ? this.delegate.getOffset() + 1 : this.delegate.getOffset() - 1;
        }

        void resolve(StyledDocument doc) throws BadLocationException {
            if (this.delegate instanceof UnresolvedPosition) {
                this.delegate = doc.createPosition(this.delegate.getOffset());
            }
        }
    }

    private static final class UnresolvedPosition
    implements Position {
        private int offset;

        public UnresolvedPosition(int offset) {
            this.offset = offset;
        }

        @Override
        public int getOffset() {
            return this.offset;
        }
    }

}

