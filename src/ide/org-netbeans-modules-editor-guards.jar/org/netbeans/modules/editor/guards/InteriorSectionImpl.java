/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import java.beans.PropertyVetoException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.PositionBounds;

public final class InteriorSectionImpl
extends GuardedSectionImpl {
    private PositionBounds header;
    private PositionBounds body;
    private PositionBounds footer;

    InteriorSectionImpl(String name, PositionBounds header, PositionBounds body, PositionBounds footer, GuardedSectionsImpl guards) {
        super(name, guards);
        this.header = header;
        this.body = body;
        this.footer = footer;
    }

    @Override
    public void setName(String name) throws PropertyVetoException {
        super.setName(name);
        this.setHeader(this.getHeader());
        this.setFooter(this.getFooter());
    }

    public void setBody(String text) {
        this.setText(this.body, text, false, new GuardedSectionImpl.ContentGetter(){

            @Override
            public PositionBounds getContent(GuardedSectionImpl t) {
                return ((InteriorSectionImpl)t).body;
            }
        });
    }

    public String getBody() {
        String s = null;
        if (this.isValid()) {
            try {
                s = this.body.getText();
            }
            catch (BadLocationException ex) {
                throw new IllegalStateException(ex);
            }
        }
        return s;
    }

    public void setHeader(String text) {
        this.setText(this.header, text, true, new GuardedSectionImpl.ContentGetter(){

            @Override
            public PositionBounds getContent(GuardedSectionImpl t) {
                return ((InteriorSectionImpl)t).header;
            }
        });
    }

    public String getHeader() {
        String s = null;
        if (this.isValid()) {
            try {
                s = this.header.getText();
            }
            catch (BadLocationException ex) {
                throw new IllegalStateException(ex);
            }
        }
        return s;
    }

    public void setFooter(String text) {
        int lastEol;
        boolean endsWithEol = text.endsWith("\n");
        int firstEol = text.indexOf(10);
        if (firstEol != (lastEol = text.lastIndexOf(10)) || endsWithEol && firstEol != -1) {
            if (endsWithEol) {
                text = text.substring(0, text.length() - 1);
            }
            text = text.replace('\n', ' ');
        }
        this.setText(this.footer, text, true, new GuardedSectionImpl.ContentGetter(){

            @Override
            public PositionBounds getContent(GuardedSectionImpl t) {
                return ((InteriorSectionImpl)t).footer;
            }
        });
    }

    public String getFooter() {
        String s = null;
        if (this.isValid()) {
            try {
                s = this.footer.getText();
            }
            catch (BadLocationException ex) {
                throw new IllegalStateException(ex);
            }
        }
        return s;
    }

    @Override
    public Position getCaretPosition() {
        return this.body.getBegin();
    }

    @Override
    void markGuarded(StyledDocument doc) {
        this.markGuarded(doc, this.header, true);
        this.markGuarded(doc, this.footer, true);
    }

    @Override
    void unmarkGuarded(StyledDocument doc) {
        this.markGuarded(doc, this.header, false);
        this.markGuarded(doc, this.footer, false);
    }

    @Override
    public String getText() {
        StringBuffer buf = new StringBuffer();
        try {
            buf.append(this.header.getText());
            buf.append(this.body.getText());
            buf.append(this.footer.getText());
        }
        catch (Exception e) {
            // empty catch block
        }
        return buf.toString();
    }

    @Override
    public boolean contains(Position pos, boolean allowHoles) {
        if (!allowHoles) {
            return this.header.getBegin().getOffset() <= pos.getOffset() && this.footer.getEnd().getOffset() >= pos.getOffset();
        }
        if (this.header.getBegin().getOffset() <= pos.getOffset() && this.header.getEnd().getOffset() >= pos.getOffset()) {
            return true;
        }
        return this.footer.getBegin().getOffset() <= pos.getOffset() && this.footer.getEnd().getOffset() >= pos.getOffset();
    }

    @Override
    public Position getStartPosition() {
        return this.header.getBegin();
    }

    @Override
    public Position getEndPosition() {
        return this.footer.getEnd();
    }

    public Position getBodyStartPosition() {
        return this.body.getBegin();
    }

    public Position getBodyEndPosition() {
        return this.body.getEnd();
    }

    @Override
    public void resolvePositions() throws BadLocationException {
        this.header.resolvePositions();
        this.body.resolvePositions();
        this.footer.resolvePositions();
    }

    public PositionBounds getHeaderBounds() {
        return this.header;
    }

    public PositionBounds getBodyBounds() {
        return this.body;
    }

    public PositionBounds getFooterBounds() {
        return this.footer;
    }

}

