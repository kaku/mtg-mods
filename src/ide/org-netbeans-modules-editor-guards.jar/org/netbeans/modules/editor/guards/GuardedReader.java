/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.NewLine;
import org.netbeans.spi.editor.guards.support.AbstractGuardedSectionsProvider;

final class GuardedReader
extends Reader {
    Reader reader;
    private NewLineInputStream newLineStream;
    char[] charBuff;
    int howmany;
    boolean justFilter;
    int position;
    private final GuardedSectionsImpl callback;
    private final AbstractGuardedSectionsProvider gr;
    private boolean isClosed = false;
    private AbstractGuardedSectionsProvider.Result result;

    GuardedReader(AbstractGuardedSectionsProvider gr, InputStream is, boolean justFilter, Charset encoding, GuardedSectionsImpl guards) {
        this.newLineStream = new NewLineInputStream(is);
        this.reader = encoding == null ? new InputStreamReader(this.newLineStream) : new InputStreamReader((InputStream)this.newLineStream, encoding);
        this.justFilter = justFilter;
        this.callback = guards;
        this.gr = gr;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        if (this.charBuff == null) {
            char[] readBuff = this.readCharBuff();
            this.result = this.gr.readSections(readBuff);
            this.charBuff = this.result.getContent();
            this.howmany = this.charBuff.length;
        }
        if (this.howmany <= 0) {
            return -1;
        }
        int min = Math.min(len, this.howmany);
        System.arraycopy(this.charBuff, this.position, cbuf, off, min);
        this.howmany -= min;
        this.position += min;
        return min;
    }

    final char[] readCharBuff() throws IOException {
        int read;
        char[] tmp = new char[2048];
        ArrayList<char[]> buffs = new ArrayList<char[]>(20);
        do {
            read = this.readFully(tmp);
            buffs.add(tmp);
            if (read < 2048) break;
            tmp = new char[2048];
        } while (true);
        int listsize = buffs.size() - 1;
        int size = listsize * 2048 + read;
        char[] readBuff = new char[size];
        this.charBuff = new char[size];
        int copy = 0;
        for (int i = 0; i < listsize; ++i) {
            char[] tmp2 = (char[])buffs.get(i);
            System.arraycopy(tmp2, 0, readBuff, copy, 2048);
            copy += 2048;
        }
        System.arraycopy(tmp, 0, readBuff, copy, read);
        return readBuff;
    }

    final int readFully(char[] buff) throws IOException {
        int read = 0;
        int sum = 0;
        while ((sum += (read = this.reader.read(buff, sum, buff.length - sum))) < buff.length && read > 0) {
        }
        return sum + 1;
    }

    @Override
    public void close() throws IOException {
        if (!this.isClosed) {
            this.isClosed = true;
            this.reader.close();
            if (this.result != null) {
                this.callback.fillSections(this.gr, this.result.getGuardedSections(), this.newLineStream.getNewLineType());
            }
        }
    }

    private final class NewLineInputStream
    extends InputStream {
        private final InputStream stream;
        private int b;
        private int lookahead;
        private boolean isLookahead;
        final int[] newLineTypes;

        public NewLineInputStream(InputStream source) {
            this.isLookahead = false;
            this.stream = source;
            this.newLineTypes = new int[]{0, 0, 0};
        }

        public NewLine getNewLineType() {
            if (this.newLineTypes[NewLine.N.ordinal()] == this.newLineTypes[NewLine.R.ordinal()] && this.newLineTypes[NewLine.R.ordinal()] == this.newLineTypes[NewLine.RN.ordinal()]) {
                String s = System.getProperty("line.separator");
                return NewLine.resolve(s);
            }
            if (this.newLineTypes[NewLine.N.ordinal()] > this.newLineTypes[NewLine.R.ordinal()]) {
                return this.newLineTypes[NewLine.N.ordinal()] > this.newLineTypes[NewLine.RN.ordinal()] ? NewLine.N : NewLine.RN;
            }
            return this.newLineTypes[NewLine.R.ordinal()] > this.newLineTypes[NewLine.RN.ordinal()] ? NewLine.R : NewLine.RN;
        }

        @Override
        public int read() throws IOException {
            this.b = this.isLookahead ? this.lookahead : this.stream.read();
            this.isLookahead = false;
            switch (this.b) {
                case 10: {
                    int[] arrn = this.newLineTypes;
                    int n = NewLine.N.ordinal();
                    arrn[n] = arrn[n] + 1;
                    return this.b;
                }
                case 13: {
                    this.lookahead = this.stream.read();
                    if (this.lookahead != 10) {
                        int[] arrn = this.newLineTypes;
                        int n = NewLine.R.ordinal();
                        arrn[n] = arrn[n] + 1;
                        this.isLookahead = true;
                    } else {
                        int[] arrn = this.newLineTypes;
                        int n = NewLine.RN.ordinal();
                        arrn[n] = arrn[n] + 1;
                    }
                    return 10;
                }
            }
            return this.b;
        }

        @Override
        public void close() throws IOException {
            super.close();
            this.stream.close();
        }
    }

}

