/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.NbDocument
 */
package org.netbeans.modules.editor.guards;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.api.editor.guards.GuardedSectionManager;
import org.netbeans.api.editor.guards.InteriorSection;
import org.netbeans.api.editor.guards.SimpleSection;
import org.netbeans.modules.editor.guards.GuardedPositionComparator;
import org.netbeans.modules.editor.guards.GuardedReader;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.GuardedWriter;
import org.netbeans.modules.editor.guards.GuardsAccessor;
import org.netbeans.modules.editor.guards.GuardsSupportAccessor;
import org.netbeans.modules.editor.guards.InteriorSectionImpl;
import org.netbeans.modules.editor.guards.NewLine;
import org.netbeans.modules.editor.guards.PositionBounds;
import org.netbeans.modules.editor.guards.SimpleSectionImpl;
import org.netbeans.spi.editor.guards.GuardedEditorSupport;
import org.netbeans.spi.editor.guards.support.AbstractGuardedSectionsProvider;
import org.openide.text.NbDocument;

public final class GuardedSectionsImpl {
    Map<String, GuardedSectionImpl> sections = new HashMap<String, GuardedSectionImpl>(10);
    final GuardedEditorSupport editor;
    private NewLine newLineType;
    AbstractGuardedSectionsProvider gr;

    public GuardedSectionsImpl(GuardedEditorSupport ces) {
        this.editor = ces;
    }

    public Reader createGuardedReader(AbstractGuardedSectionsProvider gr, InputStream stream, Charset encoding) {
        GuardedReader greader = new GuardedReader(gr, stream, false, encoding, this);
        StyledDocument doc = this.getDocument();
        if (doc.getProperty(GuardedSectionManager.class) == null) {
            GuardedSectionManager api = GuardsAccessor.DEFAULT.createGuardedSections(this);
            doc.putProperty(GuardedSectionManager.class, api);
        }
        return greader;
    }

    public Writer createGuardedWriter(AbstractGuardedSectionsProvider gw, OutputStream stream, Charset encoding) {
        ArrayList<GuardedSection> list;
        NewLineOutputStream os = new NewLineOutputStream(stream, this.newLineType);
        if (this.sections != null && (list = new ArrayList<GuardedSection>(this.getGuardedSections())).size() > 0) {
            GuardedWriter writer = new GuardedWriter(gw, os, list, encoding);
            return writer;
        }
        OutputStreamWriter w = encoding == null ? new OutputStreamWriter(os) : new OutputStreamWriter((OutputStream)os, encoding);
        return w;
    }

    public StyledDocument getDocument() {
        return this.editor.getDocument();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public GuardedSection findSection(String name) {
        StyledDocument doc = this.editor.getDocument();
        Map<String, GuardedSectionImpl> map = this.sections;
        synchronized (map) {
            GuardedSectionImpl gsi = this.sections.get(name);
            if (gsi != null) {
                return gsi.guard;
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<GuardedSection> getGuardedSections() {
        StyledDocument doc = this.editor.getDocument();
        Map<String, GuardedSectionImpl> map = this.sections;
        synchronized (map) {
            TreeSet<GuardedSection> sortedGuards = new TreeSet<GuardedSection>(new GuardedPositionComparator());
            for (GuardedSectionImpl gsi : this.sections.values()) {
                sortedGuards.add(gsi.guard);
            }
            return sortedGuards;
        }
    }

    public SimpleSection createSimpleSectionObject(String name, PositionBounds bounds) {
        return (SimpleSection)this.createSimpleSectionImpl((String)name, (PositionBounds)bounds).guard;
    }

    public InteriorSection createInteriorSectionObject(String name, PositionBounds header, PositionBounds body, PositionBounds footer) {
        return (InteriorSection)this.createInteriorSectionImpl((String)name, (PositionBounds)header, (PositionBounds)body, (PositionBounds)footer).guard;
    }

    public SimpleSection createSimpleSection(Position pos, String name) throws BadLocationException {
        this.checkNewSection(pos, name);
        return this.doCreateSimpleSection(pos, name);
    }

    public InteriorSection createInteriorSection(Position pos, String name) throws BadLocationException {
        this.checkNewSection(pos, name);
        return this.doCreateInteriorSection(pos, name);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private SimpleSection doCreateSimpleSection(final Position pos, final String name) throws BadLocationException {
        StyledDocument loadedDoc = null;
        final StyledDocument doc = loadedDoc = this.editor.getDocument();
        final SimpleSectionImpl[] sect = new SimpleSectionImpl[1];
        final BadLocationException[] blex = new BadLocationException[1];
        NbDocument.runAtomic((StyledDocument)doc, (Runnable)new Runnable(){

            @Override
            public void run() {
                try {
                    int where = pos.getOffset();
                    doc.insertString(where, "\n \n", null);
                    sect[0] = GuardedSectionsImpl.this.createSimpleSectionImpl(name, PositionBounds.create(where + 1, where + 2, GuardedSectionsImpl.this));
                    sect[0].markGuarded(doc);
                }
                catch (BadLocationException ex) {
                    blex[0] = ex;
                }
            }
        });
        if (blex[0] == null) {
            Map<String, GuardedSectionImpl> map = this.sections;
            synchronized (map) {
                this.sections.put(name, sect[0]);
                return (SimpleSection)sect[0].guard;
            }
        }
        throw (BadLocationException)new BadLocationException("wrong offset", blex[0].offsetRequested()).initCause(blex[0]);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private InteriorSection doCreateInteriorSection(final Position pos, final String name) throws IllegalArgumentException, BadLocationException {
        StyledDocument loadedDoc = null;
        final StyledDocument doc = loadedDoc = this.editor.getDocument();
        final InteriorSectionImpl[] sect = new InteriorSectionImpl[1];
        final BadLocationException[] blex = new BadLocationException[1];
        NbDocument.runAtomic((StyledDocument)doc, (Runnable)new Runnable(){

            @Override
            public void run() {
                try {
                    int where = pos.getOffset();
                    doc.insertString(where, "\n \n \n \n", null);
                    sect[0] = GuardedSectionsImpl.this.createInteriorSectionImpl(name, PositionBounds.create(where + 1, where + 2, GuardedSectionsImpl.this), PositionBounds.createBodyBounds(where + 3, where + 4, GuardedSectionsImpl.this), PositionBounds.create(where + 5, where + 6, GuardedSectionsImpl.this));
                    GuardedSectionsImpl.this.sections.put(sect[0].getName(), sect[0]);
                    sect[0].markGuarded(doc);
                }
                catch (BadLocationException ex) {
                    blex[0] = ex;
                }
            }
        });
        if (blex[0] == null) {
            Map<String, GuardedSectionImpl> map = this.sections;
            synchronized (map) {
                this.sections.put(name, sect[0]);
                return (InteriorSection)sect[0].guard;
            }
        }
        throw (BadLocationException)new BadLocationException("wrong offset", blex[0].offsetRequested()).initCause(blex[0]);
    }

    void fillSections(AbstractGuardedSectionsProvider gr, List<GuardedSection> l, NewLine newLineType) {
        this.gr = GuardsSupportAccessor.DEFAULT.isUseReadersWritersOnSet(gr) ? gr : null;
        this.newLineType = newLineType;
        this.sections.clear();
        for (GuardedSection gs : l) {
            try {
                GuardedSectionImpl gsi = GuardsAccessor.DEFAULT.getImpl(gs);
                gsi.resolvePositions();
                this.sections.put(gs.getName(), gsi);
                StyledDocument doc = this.getDocument();
                gsi.markGuarded(doc);
            }
            catch (BadLocationException ex) {
                Logger.getLogger(GuardedSectionsImpl.class.getName()).log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
        }
    }

    private SimpleSectionImpl createSimpleSectionImpl(String name, PositionBounds bounds) {
        SimpleSectionImpl sect = new SimpleSectionImpl(name, bounds, this);
        GuardsAccessor.DEFAULT.createSimpleSection(sect);
        return sect;
    }

    private InteriorSectionImpl createInteriorSectionImpl(String name, PositionBounds header, PositionBounds body, PositionBounds footer) {
        InteriorSectionImpl sect = new InteriorSectionImpl(name, header, body, footer, this);
        GuardsAccessor.DEFAULT.createInteriorSection(sect);
        return sect;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkNewSection(Position p, String name) {
        Map<String, GuardedSectionImpl> map = this.sections;
        synchronized (map) {
            this.checkOverlap(p);
            GuardedSectionImpl gs = this.sections.get(name);
            if (gs != null) {
                throw new IllegalArgumentException("name exists");
            }
        }
    }

    private void checkOverlap(Position p) throws IllegalArgumentException {
        for (GuardedSectionImpl gs : this.sections.values()) {
            if (!gs.contains(p, false)) continue;
            throw new IllegalArgumentException("Sections overlap");
        }
    }

    private static class NewLineOutputStream
    extends OutputStream {
        OutputStream stream;
        NewLine newLineType;

        public NewLineOutputStream(OutputStream stream, NewLine newLineType) {
            this.stream = stream;
            this.newLineType = newLineType;
        }

        @Override
        public void write(int b) throws IOException {
            if (b == 10) {
                switch (this.newLineType) {
                    case R: {
                        this.stream.write(13);
                        break;
                    }
                    case RN: {
                        this.stream.write(13);
                    }
                    case N: {
                        this.stream.write(10);
                    }
                }
            } else {
                this.stream.write(b);
            }
        }

        @Override
        public void close() throws IOException {
            super.close();
            this.stream.close();
        }

        @Override
        public void flush() throws IOException {
            this.stream.flush();
        }
    }

}

