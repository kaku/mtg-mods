/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import javax.swing.text.Position;

public class OffsetPosition
implements Position {
    private final Position delegate;
    private final int offset;

    public OffsetPosition(Position delegate, int offset) {
        this.delegate = delegate;
        this.offset = offset;
    }

    @Override
    public int getOffset() {
        return this.delegate.getOffset() - this.offset;
    }
}

