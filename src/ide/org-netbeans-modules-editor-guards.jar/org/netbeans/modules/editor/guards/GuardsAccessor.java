/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.api.editor.guards.GuardedSectionManager;
import org.netbeans.api.editor.guards.InteriorSection;
import org.netbeans.api.editor.guards.SimpleSection;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.InteriorSectionImpl;
import org.netbeans.modules.editor.guards.SimpleSectionImpl;

public abstract class GuardsAccessor {
    public static GuardsAccessor DEFAULT;

    public abstract GuardedSectionManager createGuardedSections(GuardedSectionsImpl var1);

    public abstract SimpleSection createSimpleSection(SimpleSectionImpl var1);

    public abstract InteriorSection createInteriorSection(InteriorSectionImpl var1);

    public abstract GuardedSectionImpl getImpl(GuardedSection var1);

    public abstract GuardedSection clone(GuardedSection var1, int var2);

    static {
        Class<GuardedSectionManager> clazz = GuardedSectionManager.class;
        try {
            Class.forName(clazz.getName(), true, clazz.getClassLoader());
        }
        catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }
}

