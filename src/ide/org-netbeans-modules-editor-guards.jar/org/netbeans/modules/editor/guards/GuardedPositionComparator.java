/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.guards;

import java.util.Comparator;
import javax.swing.text.Position;
import org.netbeans.api.editor.guards.GuardedSection;

final class GuardedPositionComparator
implements Comparator<GuardedSection> {
    GuardedPositionComparator() {
    }

    @Override
    public int compare(GuardedSection o1, GuardedSection o2) {
        return this.getOffset(o1) - this.getOffset(o2);
    }

    private int getOffset(GuardedSection o) {
        return o.getStartPosition().getOffset();
    }
}

