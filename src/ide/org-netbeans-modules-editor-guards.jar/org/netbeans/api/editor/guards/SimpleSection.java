/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.guards;

import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.SimpleSectionImpl;

public final class SimpleSection
extends GuardedSection {
    SimpleSection(SimpleSectionImpl impl) {
        super(impl);
    }

    SimpleSection(GuardedSection delegate, int offset) {
        super(delegate, offset);
    }

    public void setText(String text) {
        if (this.getImpl() == null) {
            throw new IllegalStateException();
        }
        this.getImpl().setText(text);
    }

    @Override
    SimpleSectionImpl getImpl() {
        return (SimpleSectionImpl)super.getImpl();
    }

    @Override
    GuardedSection clone(int offset) {
        return new SimpleSection(this, offset);
    }
}

