/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.guards;

import javax.swing.text.Position;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.InteriorSectionImpl;
import org.netbeans.modules.editor.guards.OffsetPosition;

public final class InteriorSection
extends GuardedSection {
    InteriorSection(InteriorSectionImpl impl) {
        super(impl);
    }

    InteriorSection(GuardedSection delegate, int offset) {
        super(delegate, offset);
    }

    public void setBody(String text) {
        if (this.getImpl() == null) {
            throw new IllegalStateException();
        }
        this.getImpl().setBody(text);
    }

    public String getBody() {
        if (this.getImpl() == null) {
            return this.getDelegate().getBody();
        }
        return this.getImpl().getBody();
    }

    public void setHeader(String text) {
        if (this.getImpl() == null) {
            throw new IllegalStateException();
        }
        this.getImpl().setHeader(text);
    }

    public String getHeader() {
        if (this.getImpl() == null) {
            return this.getDelegate().getHeader();
        }
        return this.getImpl().getHeader();
    }

    public void setFooter(String text) {
        if (this.getImpl() == null) {
            throw new IllegalStateException();
        }
        this.getImpl().setFooter(text);
    }

    public String getFooter() {
        if (this.getImpl() == null) {
            return this.getDelegate().getFooter();
        }
        return this.getImpl().getFooter();
    }

    public Position getBodyStartPosition() {
        if (this.getImpl() == null) {
            return new OffsetPosition(this.getDelegate().getBodyStartPosition(), this.offset);
        }
        return this.getImpl().getBodyStartPosition();
    }

    public Position getBodyEndPosition() {
        if (this.getImpl() == null) {
            return new OffsetPosition(this.getDelegate().getBodyEndPosition(), this.offset);
        }
        return this.getImpl().getBodyEndPosition();
    }

    @Override
    InteriorSectionImpl getImpl() {
        return (InteriorSectionImpl)super.getImpl();
    }

    @Override
    InteriorSection getDelegate() {
        return (InteriorSection)super.getDelegate();
    }

    @Override
    GuardedSection clone(int offset) {
        return new InteriorSection(this, offset);
    }
}

