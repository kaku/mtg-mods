/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.guards;

import java.util.Set;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.api.editor.guards.InteriorSection;
import org.netbeans.api.editor.guards.SimpleSection;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.GuardsAccessor;
import org.netbeans.modules.editor.guards.InteriorSectionImpl;
import org.netbeans.modules.editor.guards.SimpleSectionImpl;

public final class GuardedSectionManager {
    private final GuardedSectionsImpl impl;

    public static GuardedSectionManager getInstance(StyledDocument doc) {
        return (GuardedSectionManager)doc.getProperty(GuardedSectionManager.class);
    }

    public SimpleSection findSimpleSection(String name) {
        GuardedSection s = this.impl.findSection(name);
        return s instanceof SimpleSection ? (SimpleSection)s : null;
    }

    public InteriorSection findInteriorSection(String name) {
        GuardedSection s = this.impl.findSection(name);
        return s instanceof InteriorSection ? (InteriorSection)s : null;
    }

    public SimpleSection createSimpleSection(Position pos, String name) throws IllegalArgumentException, BadLocationException {
        return this.impl.createSimpleSection(pos, name);
    }

    public InteriorSection createInteriorSection(Position pos, String name) throws IllegalArgumentException, BadLocationException {
        return this.impl.createInteriorSection(pos, name);
    }

    public Iterable<GuardedSection> getGuardedSections() {
        return this.impl.getGuardedSections();
    }

    private GuardedSectionManager(GuardedSectionsImpl impl) {
        this.impl = impl;
    }

    static {
        GuardsAccessor.DEFAULT = new GuardsAccessor(){

            @Override
            public GuardedSectionManager createGuardedSections(GuardedSectionsImpl impl) {
                return new GuardedSectionManager(impl);
            }

            @Override
            public SimpleSection createSimpleSection(SimpleSectionImpl impl) {
                return new SimpleSection(impl);
            }

            @Override
            public InteriorSection createInteriorSection(InteriorSectionImpl impl) {
                return new InteriorSection(impl);
            }

            @Override
            public GuardedSectionImpl getImpl(GuardedSection gs) {
                return gs.getImpl();
            }

            @Override
            public GuardedSection clone(GuardedSection gs, int offset) {
                return gs.clone(offset);
            }
        };
    }

}

