/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.guards;

import java.beans.PropertyVetoException;
import javax.swing.text.Position;
import org.netbeans.modules.editor.guards.GuardedSectionImpl;
import org.netbeans.modules.editor.guards.OffsetPosition;

public abstract class GuardedSection {
    private final GuardedSectionImpl impl;
    private final GuardedSection delegate;
    final int offset;

    GuardedSection(GuardedSectionImpl impl) {
        assert (impl != null);
        this.impl = impl;
        impl.attach(this);
        this.delegate = null;
        this.offset = 0;
    }

    GuardedSection(GuardedSection delegate, int offset) {
        this.impl = null;
        this.delegate = delegate;
        this.offset = offset;
    }

    public String getName() {
        return this.impl != null ? this.impl.getName() : this.delegate.getName();
    }

    public void setName(String name) throws PropertyVetoException {
        if (this.impl == null) {
            throw new IllegalStateException();
        }
        this.impl.setName(name);
    }

    public void deleteSection() {
        if (this.impl == null) {
            throw new IllegalStateException();
        }
        this.impl.deleteSection();
    }

    public boolean isValid() {
        return this.impl != null ? this.impl.isValid() : this.delegate.isValid();
    }

    public void removeSection() {
        if (this.impl == null) {
            throw new IllegalStateException();
        }
        this.impl.removeSection();
    }

    public Position getCaretPosition() {
        return this.impl != null ? this.impl.getCaretPosition() : new OffsetPosition(this.delegate.getCaretPosition(), this.offset);
    }

    public String getText() {
        return this.impl != null ? this.impl.getText() : this.delegate.getText();
    }

    public boolean contains(Position pos, boolean permitHoles) {
        return this.impl != null ? this.impl.contains(pos, permitHoles) : this.delegate.contains(new OffsetPosition(pos, - this.offset), permitHoles);
    }

    public Position getEndPosition() {
        return this.impl != null ? this.impl.getEndPosition() : new OffsetPosition(this.delegate.getEndPosition(), this.offset);
    }

    public Position getStartPosition() {
        return this.impl != null ? this.impl.getStartPosition() : new OffsetPosition(this.delegate.getStartPosition(), this.offset);
    }

    GuardedSectionImpl getImpl() {
        return this.impl;
    }

    GuardedSection getDelegate() {
        return this.delegate;
    }

    abstract GuardedSection clone(int var1);
}

