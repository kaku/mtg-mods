/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.guards;

import javax.swing.text.StyledDocument;

public interface GuardedEditorSupport {
    public StyledDocument getDocument();
}

