/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.guards;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;

public interface GuardedSectionsProvider {
    public Reader createGuardedReader(InputStream var1, Charset var2);

    public Writer createGuardedWriter(OutputStream var1, Charset var2);
}

