/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.guards.support;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;
import javax.swing.text.BadLocationException;
import org.netbeans.api.editor.guards.GuardedSection;
import org.netbeans.api.editor.guards.InteriorSection;
import org.netbeans.api.editor.guards.SimpleSection;
import org.netbeans.modules.editor.guards.GuardedSectionsImpl;
import org.netbeans.modules.editor.guards.GuardsSupportAccessor;
import org.netbeans.modules.editor.guards.PositionBounds;
import org.netbeans.spi.editor.guards.GuardedEditorSupport;
import org.netbeans.spi.editor.guards.GuardedSectionsProvider;

public abstract class AbstractGuardedSectionsProvider
implements GuardedSectionsProvider {
    private final GuardedSectionsImpl impl;
    private final boolean useReadersWritersOnSet;

    protected AbstractGuardedSectionsProvider(GuardedEditorSupport editor) {
        this(editor, false);
    }

    protected AbstractGuardedSectionsProvider(GuardedEditorSupport editor, boolean useReadersWritersOnSet) {
        this.impl = new GuardedSectionsImpl(editor);
        this.useReadersWritersOnSet = useReadersWritersOnSet;
    }

    @Override
    public final Reader createGuardedReader(InputStream stream, Charset charset) {
        return this.impl.createGuardedReader(this, stream, charset);
    }

    @Override
    public Writer createGuardedWriter(OutputStream stream, Charset charset) {
        return this.impl.createGuardedWriter(this, stream, charset);
    }

    public abstract char[] writeSections(List<GuardedSection> var1, char[] var2);

    public abstract Result readSections(char[] var1);

    public final SimpleSection createSimpleSection(String name, int begin, int end) throws BadLocationException {
        return this.impl.createSimpleSectionObject(name, PositionBounds.createUnresolved(begin, end, this.impl));
    }

    public final InteriorSection createInteriorSection(String name, int headerBegin, int headerEnd, int footerBegin, int footerEnd) throws BadLocationException {
        return this.impl.createInteriorSectionObject(name, PositionBounds.createUnresolved(headerBegin, headerEnd, this.impl), PositionBounds.createBodyUnresolved(headerEnd + 1, footerBegin - 1, this.impl), PositionBounds.createUnresolved(footerBegin, footerEnd, this.impl));
    }

    static {
        GuardsSupportAccessor.DEFAULT = new GuardsSupportAccessor(){

            @Override
            public boolean isUseReadersWritersOnSet(AbstractGuardedSectionsProvider impl) {
                return impl.useReadersWritersOnSet;
            }
        };
    }

    public final class Result {
        private final char[] content;
        private final List<GuardedSection> sections;

        public Result(char[] content, List<GuardedSection> sections) {
            this.content = content;
            this.sections = sections;
        }

        public char[] getContent() {
            return this.content;
        }

        public List<GuardedSection> getGuardedSections() {
            return this.sections;
        }
    }

}

