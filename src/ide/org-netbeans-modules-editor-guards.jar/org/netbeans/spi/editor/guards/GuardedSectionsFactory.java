/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.spi.editor.guards;

import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.spi.editor.guards.GuardedEditorSupport;
import org.netbeans.spi.editor.guards.GuardedSectionsProvider;

public abstract class GuardedSectionsFactory {
    public static GuardedSectionsFactory find(String mimePath) {
        MimePath mp = MimePath.get((String)mimePath);
        GuardedSectionsFactory factory = null;
        if (mp != null) {
            factory = (GuardedSectionsFactory)MimeLookup.getLookup((MimePath)mp).lookup(GuardedSectionsFactory.class);
        }
        return factory;
    }

    public abstract GuardedSectionsProvider create(GuardedEditorSupport var1);
}

