/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeEvent
 *  org.netbeans.spi.editor.highlighting.HighlightsChangeListener
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.HighlightsSequence
 *  org.netbeans.spi.editor.highlighting.ZOrder
 *  org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.bracesmatching;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.editor.bracesmatching.MasterMatcher;
import org.netbeans.spi.editor.highlighting.HighlightsChangeEvent;
import org.netbeans.spi.editor.highlighting.HighlightsChangeListener;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.netbeans.spi.editor.highlighting.support.AbstractHighlightsContainer;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.WeakListeners;

public class BracesMatchHighlighting
extends AbstractHighlightsContainer
implements ChangeListener,
PropertyChangeListener,
HighlightsChangeListener,
DocumentListener {
    private static final Logger LOG = Logger.getLogger(BracesMatchHighlighting.class.getName());
    private static final String BRACES_MATCH_COLORING = "nbeditor-bracesMatching-match";
    private static final String BRACES_MISMATCH_COLORING = "nbeditor-bracesMatching-mismatch";
    private static final String BRACES_MATCH_MULTICHAR_COLORING = "nbeditor-bracesMatching-match-multichar";
    private static final String BRACES_MISMATCH_MULTICHAR_COLORING = "nbeditor-bracesMatching-mismatch-multichar";
    private final JTextComponent component;
    private final Document document;
    private Caret caret = null;
    private ChangeListener caretListener;
    private final OffsetsBag bag;
    private final AttributeSet bracesMatchColoring;
    private final AttributeSet bracesMismatchColoring;
    private final AttributeSet bracesMatchMulticharColoring;
    private final AttributeSet bracesMismatchMulticharColoring;

    public BracesMatchHighlighting(JTextComponent component, Document document) {
        this.document = document;
        String mimeType = BracesMatchHighlighting.getMimeType(component);
        MimePath mimePath = mimeType == null ? MimePath.EMPTY : MimePath.parse((String)mimeType);
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)mimePath).lookup(FontColorSettings.class);
        AttributeSet match = fcs.getFontColors("nbeditor-bracesMatching-match");
        AttributeSet mismatch = fcs.getFontColors("nbeditor-bracesMatching-mismatch");
        AttributeSet matchMultichar = fcs.getFontColors("nbeditor-bracesMatching-match-multichar");
        AttributeSet mismatchMultichar = fcs.getFontColors("nbeditor-bracesMatching-mismatch-multichar");
        this.bracesMatchColoring = match != null ? match : SimpleAttributeSet.EMPTY;
        this.bracesMismatchColoring = mismatch != null ? mismatch : SimpleAttributeSet.EMPTY;
        this.bracesMatchMulticharColoring = matchMultichar != null ? matchMultichar : SimpleAttributeSet.EMPTY;
        this.bracesMismatchMulticharColoring = mismatchMultichar != null ? mismatchMultichar : SimpleAttributeSet.EMPTY;
        this.bag = new OffsetsBag(document, true);
        this.bag.addHighlightsChangeListener((HighlightsChangeListener)this);
        this.component = component;
        this.component.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.component));
        this.caret = component.getCaret();
        if (this.caret != null) {
            this.caretListener = WeakListeners.change((ChangeListener)this, (Object)this.caret);
            this.caret.addChangeListener(this.caretListener);
        }
        this.refresh();
    }

    public HighlightsSequence getHighlights(int startOffset, int endOffset) {
        return this.bag.getHighlights(startOffset, endOffset);
    }

    public void highlightChanged(HighlightsChangeEvent event) {
        this.fireHighlightsChange(event.getStartOffset(), event.getEndOffset());
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.refresh();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.refresh();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        this.refresh();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.refresh();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == null || "caret".equals(evt.getPropertyName())) {
            if (this.caret != null) {
                this.caret.removeChangeListener(this.caretListener);
                this.caretListener = null;
            }
            this.caret = this.component.getCaret();
            if (this.caret != null) {
                this.caretListener = WeakListeners.change((ChangeListener)this, (Object)this.caret);
                this.caret.addChangeListener(this.caretListener);
            }
            this.refresh();
        } else if ("nbeditor-bracesMatching-searchDirection".equals(evt.getPropertyName()) || "nbeditor-bracesMatching-caretBias".equals(evt.getPropertyName()) || "nbeditor-bracesMatching-maxBackwardLookahead".equals(evt.getPropertyName()) || "nbeditor-bracesMatching-maxForwardLookahead".equals(evt.getPropertyName())) {
            this.refresh();
        }
    }

    private void refresh() {
        Caret c = this.caret;
        if (c == null) {
            this.bag.clear();
        } else {
            MasterMatcher.get(this.component).highlight(this.document, c.getDot(), this.bag, this.bracesMatchColoring, this.bracesMismatchColoring, this.bracesMatchMulticharColoring, this.bracesMismatchMulticharColoring);
        }
    }

    private static String getMimeType(JTextComponent component) {
        EditorKit kit;
        Document doc = component.getDocument();
        String mimeType = (String)doc.getProperty("mimeType");
        if (mimeType == null && (kit = component.getUI().getEditorKit(component)) != null) {
            mimeType = kit.getContentType();
        }
        return mimeType;
    }

    public static final class Factory
    implements HighlightsLayerFactory {
        public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
            return new HighlightsLayer[]{HighlightsLayer.create((String)"org-netbeans-modules-editor-bracesmatching-BracesMatchHighlighting", (ZOrder)ZOrder.SHOW_OFF_RACK.forPosition(400), (boolean)true, (HighlightsContainer)new BracesMatchHighlighting(context.getComponent(), context.getDocument()))};
        }
    }

}

