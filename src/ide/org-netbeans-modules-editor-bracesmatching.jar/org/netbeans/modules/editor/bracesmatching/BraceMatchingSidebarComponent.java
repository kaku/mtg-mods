/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.PopupManager
 *  org.netbeans.editor.PopupManager$HorizontalBounds
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ToolTipSupport
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyListener
 *  org.openide.text.NbDocument
 *  org.openide.text.NbDocument$CustomEditor
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.PopupManager;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.bracesmatching.BraceToolTip;
import org.netbeans.modules.editor.bracesmatching.MasterMatcher;
import org.netbeans.modules.editor.bracesmatching.MatchEvent;
import org.netbeans.modules.editor.bracesmatching.MatchListener;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyListener;
import org.netbeans.spi.editor.bracesmatching.BraceContext;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public class BraceMatchingSidebarComponent
extends JComponent
implements MatchListener,
FocusListener,
ViewHierarchyListener,
ChangeListener {
    private static final int TOOLTIP_CHECK_DELAY = 700;
    public static final String BRACES_COLORING = "nbeditor-bracesMatching-sidebar";
    private static final String MATCHED_BRACES = "showMatchedBrace";
    private final JTextComponent editor;
    private final JEditorPane editorPane;
    private final String mimeType;
    private final BaseTextUI baseUI;
    private int lineWidth = 2;
    private int leftMargin = 1;
    private int barWidth;
    private Position[] origin;
    private Position[] matches;
    private BraceContext braceContext;
    private int lineHeight;
    private boolean showOutline;
    private boolean showToolTip;
    private Preferences prefs;
    private LookupListener lookupListenerGC;
    private PreferenceChangeListener prefListenerGC;
    private JViewport viewport;
    private Coloring coloring;
    private static final RequestProcessor RP = new RequestProcessor(BraceMatchingSidebarComponent.class);
    private RequestProcessor.Task scrollUpdater;
    private boolean autoHidden;
    private int tooltipYAnchor;

    public BraceMatchingSidebarComponent(JTextComponent editor) {
        TextUI ui;
        this.scrollUpdater = RP.create(new Runnable(){

            @Override
            public void run() {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Rectangle visible = BraceMatchingSidebarComponent.this.getVisibleRect();
                        if (BraceMatchingSidebarComponent.this.tooltipYAnchor < Integer.MAX_VALUE) {
                            if (visible.y <= BraceMatchingSidebarComponent.this.tooltipYAnchor) {
                                BraceMatchingSidebarComponent.this.hideToolTip(true);
                            } else if (BraceMatchingSidebarComponent.this.autoHidden) {
                                BraceMatchingSidebarComponent.this.showTooltip();
                            }
                        }
                    }
                });
            }

        });
        this.editor = editor;
        this.mimeType = DocumentUtilities.getMimeType((JTextComponent)editor);
        this.prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        final Lookup.Result r = MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)editor)).lookupResult(FontColorSettings.class);
        this.lookupListenerGC = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                Iterator fcsIt = r.allInstances().iterator();
                if (fcsIt.hasNext()) {
                    BraceMatchingSidebarComponent.this.updateColors(r);
                }
            }
        };
        this.prefListenerGC = new PrefListener();
        r.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupListenerGC, (Object)r));
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefListenerGC, (Object)this.prefs));
        this.loadPreferences();
        this.editorPane = BraceMatchingSidebarComponent.findEditorPane(editor);
        Container parent = editor.getParent();
        if (parent instanceof JViewport) {
            this.viewport = (JViewport)parent;
            this.viewport.addChangeListener(this);
        }
        if ((ui = editor.getUI()) instanceof BaseTextUI) {
            this.baseUI = (BaseTextUI)ui;
            MasterMatcher.get(editor).addMatchListener(this);
            this.updateColors(r);
        } else {
            this.baseUI = null;
        }
        this.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        this.updatePreferredSize();
    }

    private void loadPreferences() {
        this.showOutline = this.prefs.getBoolean("editor-brace-outline", true);
        this.showToolTip = this.prefs.getBoolean("editor-brace-first-tooltip", true);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.scrollUpdater.schedule(700);
    }

    private static JEditorPane findEditorPane(JTextComponent editor) {
        Container c = editor.getUI().getRootView(editor).getContainer();
        return c instanceof JEditorPane ? (JEditorPane)c : null;
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        this.hideToolTip(false);
    }

    public void viewHierarchyChanged(ViewHierarchyEvent evt) {
        this.checkRepaint(evt);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        ViewHierarchy.get((JTextComponent)this.editor).addViewHierarchyListener((ViewHierarchyListener)this);
        this.editor.addFocusListener(this);
    }

    @Override
    public void removeNotify() {
        ViewHierarchy.get((JTextComponent)this.editor).removeViewHierarchyListener((ViewHierarchyListener)this);
        this.editor.removeFocusListener(this);
        super.removeNotify();
    }

    private void updateColors(Lookup.Result r) {
        Iterator fcsIt = r.allInstances().iterator();
        if (!fcsIt.hasNext()) {
            return;
        }
        FontColorSettings fcs = (FontColorSettings)fcsIt.next();
        AttributeSet as = fcs.getFontColors("nbeditor-bracesMatching-sidebar");
        as = as == null ? fcs.getFontColors("default") : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{as, fcs.getFontColors("default")});
        this.coloring = Coloring.fromAttributeSet((AttributeSet)as);
        int w = 0;
        if (this.coloring.getFont() != null) {
            w = this.coloring.getFont().getSize();
        } else if (this.baseUI != null) {
            w = this.baseUI.getEditorUI().getLineNumberDigitWidth();
        }
        this.barWidth = Math.max(4, w / 2);
        this.updatePreferredSize();
    }

    private void updatePreferredSize() {
        if (this.showOutline && this.baseUI != null) {
            this.setPreferredSize(new Dimension(this.barWidth, this.editor.getHeight()));
            this.lineHeight = this.baseUI.getEditorUI().getLineHeight();
        } else {
            this.setPreferredSize(new Dimension(0, 0));
        }
    }

    public void checkRepaint(ViewHierarchyEvent evt) {
        if (!evt.isChangeY() || this.baseUI == null) {
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                BraceMatchingSidebarComponent.this.updatePreferredSize();
                BraceMatchingSidebarComponent.this.repaint();
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void paintComponent(Graphics g) {
        if (!this.showOutline) {
            return;
        }
        Rectangle clip = this.getVisibleRect();
        g.setColor(this.coloring.getBackColor());
        g.fillRect(clip.x, clip.y, clip.width, clip.height);
        g.setColor(this.coloring.getForeColor());
        AbstractDocument adoc = (AbstractDocument)this.editor.getDocument();
        adoc.readLock();
        try {
            int[] points = this.findLinePoints(this.origin, this.matches);
            if (points == null) {
                return;
            }
            int dist = this.lineHeight / 2;
            Graphics2D g2d = (Graphics2D)g;
            BasicStroke s = new BasicStroke(this.lineWidth, 2, 1);
            if (this.coloring.getForeColor() != null) {
                g.setColor(this.coloring.getForeColor());
            }
            int start = points[0] + dist;
            int end = points[points.length - 1] - dist;
            int x = this.leftMargin + (this.lineWidth + 1) / 2;
            g2d.setStroke(s);
            g2d.drawLine(this.barWidth, start, x, start);
            g2d.drawLine(x, start, x, end);
            g2d.drawLine(x, end, this.barWidth, end);
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return;
        }
        finally {
            adoc.readUnlock();
        }
    }

    @Override
    public void matchHighlighted(final MatchEvent evt) {
        final BraceContext[] ctx = new BraceContext[1];
        if (evt.getLocator() != null) {
            ctx[0] = evt.getLocator().findContext(evt.getOrigin()[0].getOffset());
        }
        this.editor.getDocument().render(new Runnable(){

            @Override
            public void run() {
                if (ctx[0] == null) {
                    Position[] range = BraceMatchingSidebarComponent.findTooltipRange(evt.getOrigin(), evt.getMatches());
                    if (range == null) {
                        ctx[0] = null;
                        return;
                    }
                    ctx[0] = BraceContext.create(range[0], range[1]);
                }
            }
        });
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                BraceMatchingSidebarComponent.this.braceContext = ctx[0];
                if (ctx[0] == null) {
                    return;
                }
                if (!BraceMatchingSidebarComponent.this.isEditorValid()) {
                    return;
                }
                BraceMatchingSidebarComponent.this.origin = evt.getOrigin();
                BraceMatchingSidebarComponent.this.matches = evt.getMatches();
                BraceMatchingSidebarComponent.this.repaint();
                BraceMatchingSidebarComponent.this.showTooltip();
            }
        });
    }

    @Override
    public void matchCleared(MatchEvent evt) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                BraceMatchingSidebarComponent.this.braceContext = null;
                BraceMatchingSidebarComponent.this.origin = null;
                BraceMatchingSidebarComponent.this.repaint();
            }
        });
    }

    private int[] findLinePoints(Position[] origin, Position[] matches) throws BadLocationException {
        int maxY;
        Object o;
        boolean lineDown = false;
        if (this.editorPane != null && (o = this.editorPane.getClientProperty("showMatchedBrace")) instanceof Position[]) {
            origin = (Position[])o;
            matches = null;
            lineDown = true;
        }
        if (this.baseUI == null || origin == null || matches == null && !lineDown) {
            return null;
        }
        int minOffset = origin[0].getOffset();
        int maxOffset = origin[1].getOffset();
        if (lineDown) {
            maxY = this.getSize().height;
        } else {
            for (int i = 0; i < matches.length; i += 2) {
                minOffset = Math.min(minOffset, matches[i].getOffset());
                maxOffset = Math.max(maxOffset, matches[i + 1].getOffset());
            }
            maxY = this.baseUI.getYFromPos(maxOffset);
        }
        int minY = this.baseUI.getYFromPos(minOffset);
        if (minY == maxY) {
            return null;
        }
        int height = this.baseUI.getEditorUI().getLineHeight();
        return new int[]{minY += this.lineWidth, maxY += height - this.lineWidth};
    }

    private void hideToolTip(boolean autoHidden) {
        if (this.isMatcherTooltipVisible()) {
            ToolTipSupport tts = this.baseUI.getEditorUI().getToolTipSupport();
            tts.setToolTipVisible(false);
            this.autoHidden = autoHidden;
        }
    }

    private static Position[] findTooltipRange(Position[] origin, Position[] matches) {
        if (origin == null || matches == null) {
            return null;
        }
        int start = Integer.MAX_VALUE;
        int end = -1;
        Position sp = null;
        Position ep = null;
        for (int i = 0; i < Math.min(matches.length, 4); i += 2) {
            int e2;
            Position s = matches[i];
            Position e = matches[i + 1];
            int s2 = s.getOffset();
            if (s2 < start) {
                sp = s;
                start = s2;
            }
            if ((e2 = e.getOffset()) <= end) continue;
            ep = e;
            end = e2;
        }
        return new Position[]{sp, ep};
    }

    private boolean isEditorValid() {
        if (this.editor.isVisible() && Utilities.getEditorUI((JTextComponent)this.editor) != null) {
            return this.editor.hasFocus();
        }
        return false;
    }

    public JComponent createToolTipView(int start, int end, int[] suppressRanges) {
        JEditorPane tooltipPane = new JEditorPane();
        EditorKit kit = this.editorPane.getEditorKit();
        Document doc = this.editor.getDocument();
        if (kit == null || !(doc instanceof NbDocument.CustomEditor)) {
            return null;
        }
        NbDocument.CustomEditor ed = (NbDocument.CustomEditor)doc;
        Element lineRootElement = doc.getDefaultRootElement();
        tooltipPane.setEditable(false);
        tooltipPane.setFocusable(false);
        tooltipPane.putClientProperty("nbeditorui.vScrollPolicy", 21);
        tooltipPane.putClientProperty("nbeditorui.hScrollPolicy", 31);
        tooltipPane.putClientProperty("nbeditorui.selectSidebarLocations", "West");
        try {
            int lineIndex = lineRootElement.getElementIndex(start);
            Position pos = doc.createPosition(lineRootElement.getElement(lineIndex).getStartOffset());
            tooltipPane.putClientProperty("document-view-start-position", pos);
            lineIndex = lineRootElement.getElementIndex(end);
            pos = doc.createPosition(lineRootElement.getElement(lineIndex).getEndOffset());
            tooltipPane.putClientProperty("document-view-end-position", pos);
            tooltipPane.putClientProperty("document-view-accurate-span", true);
            tooltipPane.setEditorKit(kit);
            tooltipPane.setDocument(doc);
            EditorUI editorUI = Utilities.getEditorUI((JTextComponent)tooltipPane);
            if (editorUI == null) {
                return null;
            }
            if (this.braceContext != null) {
                tooltipPane.putClientProperty("showMatchedBrace", this.origin);
            }
            if (suppressRanges != null) {
                tooltipPane.putClientProperty("nbeditorui.braces.suppressLines", suppressRanges);
            }
            tooltipPane.addMouseListener(new MouseAdapter(){

                @Override
                public void mouseEntered(MouseEvent e) {
                    BraceMatchingSidebarComponent.this.hideToolTip(false);
                }
            });
            JComponent c = (JComponent)ed.createEditor(tooltipPane);
            Color foreColor = tooltipPane.getForeground();
            c.setBorder(new LineBorder(foreColor));
            c.setOpaque(true);
            return new BraceToolTip(c, tooltipPane);
        }
        catch (BadLocationException e) {
            return null;
        }
    }

    private boolean isMatcherTooltipVisible() {
        if (this.baseUI == null) {
            return false;
        }
        ToolTipSupport tts = this.baseUI.getEditorUI().getToolTipSupport();
        if (!tts.isEnabled() || !tts.isToolTipVisible()) {
            return false;
        }
        return tts.getToolTip() instanceof BraceToolTip;
    }

    private void showTooltip() {
        int contentHeight;
        if (!this.showToolTip) {
            return;
        }
        if (this.braceContext == null) {
            this.autoHidden = false;
            this.tooltipYAnchor = Integer.MAX_VALUE;
            return;
        }
        int yFrom = this.braceContext.getStart().getOffset();
        int yTo = this.braceContext.getEnd().getOffset();
        int[] suppress = null;
        Rectangle visible = this.getVisibleRect();
        BaseDocument bdoc = this.baseUI.getEditorUI().getDocument();
        if (bdoc == null) {
            return;
        }
        try {
            int yPos;
            this.tooltipYAnchor = yPos = this.baseUI.getYFromPos(yFrom);
            if (yPos >= visible.y) {
                this.autoHidden = true;
                return;
            }
            int yPos2 = this.baseUI.getYFromPos(yTo);
            contentHeight = yPos2 - yPos + this.lineHeight;
            if (this.braceContext.getRelated() != null) {
                BraceContext rel = this.braceContext.getRelated();
                int y1 = this.baseUI.getYFromPos(rel.getStart().getOffset());
                int y2 = this.baseUI.getYFromPos(rel.getEnd().getOffset());
                if (y2 < yFrom) {
                    contentHeight += y2 - y1 + this.lineHeight;
                    contentHeight += this.lineHeight;
                    int startAfterRelated = Utilities.getRowStart((BaseDocument)bdoc, (int)rel.getEnd().getOffset(), (int)1);
                    int startAtContext = Utilities.getRowStart((BaseDocument)bdoc, (int)yFrom);
                    int indent = Utilities.getRowIndent((BaseDocument)bdoc, (int)startAfterRelated);
                    int nextLine = Utilities.getRowStart((BaseDocument)bdoc, (int)startAfterRelated, (int)1);
                    if (nextLine < startAtContext) {
                        suppress = new int[]{startAfterRelated, startAtContext, indent};
                    }
                    yFrom = rel.getStart().getOffset();
                }
            }
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return;
        }
        JComponent tooltip = this.createToolTipView(yFrom, yTo, suppress);
        if (tooltip == null) {
            return;
        }
        int x = 1;
        int y = contentHeight + 5;
        if (tooltip.getBorder() != null) {
            Insets in = tooltip.getBorder().getBorderInsets(tooltip);
            x += in.left;
            y += in.bottom;
        }
        ToolTipSupport tts = this.baseUI.getEditorUI().getToolTipSupport();
        tts.setToolTipVisible(true, false);
        tts.setToolTip(tooltip, PopupManager.ScrollBarBounds, new Point(- x, - y), 0, 0, 0);
        tts.setToolTipVisible(true, false);
    }

    private class PrefListener
    implements PreferenceChangeListener {
        private PrefListener() {
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            String prefName;
            String string = prefName = evt == null ? null : evt.getKey();
            if ("editor-brace-outline".equals(prefName)) {
                BraceMatchingSidebarComponent.this.showOutline = BraceMatchingSidebarComponent.this.prefs.getBoolean("editor-brace-outline", true);
                BraceMatchingSidebarComponent.this.updatePreferredSize();
                BraceMatchingSidebarComponent.this.repaint();
            } else if ("editor-brace-first-tooltip".equals(prefName)) {
                BraceMatchingSidebarComponent.this.showToolTip = BraceMatchingSidebarComponent.this.prefs.getBoolean("editor-brace-first-tooltip", true);
            }
        }
    }

}

