/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.event.ActionEvent;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.bracesmatching.MasterMatcher;
import org.openide.util.NbBundle;

public final class BracesMatchAction
extends BaseAction {
    private final boolean select;

    public static BracesMatchAction createNavigateAction() {
        return new BracesMatchAction(false);
    }

    public static BracesMatchAction createSelectAction() {
        return new BracesMatchAction(true);
    }

    public BracesMatchAction() {
        this(false);
    }

    public BracesMatchAction(boolean select) {
        super(select ? "selection-match-brace" : "match-brace");
        this.select = select;
        this.putValue("ShortDescription", (Object)NbBundle.getMessage(BracesMatchAction.class, (String)((String)this.getValue("Name"))));
    }

    public void actionPerformed(ActionEvent e, JTextComponent component) {
        Document document = component.getDocument();
        Caret caret = component.getCaret();
        MasterMatcher.get(component).navigate(document, caret.getDot(), caret, this.select);
    }
}

