/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.openide.util.NbBundle;

public class ControlPanel
extends JPanel {
    private static String[][] SEARCH_DIRECTIONS = new String[][]{{"backward-preferred", "Backward Preferred"}, {"forward-preferred", "Forward Preferred"}};
    private static String[][] CARET_BIAS = new String[][]{{"backward", "Backward (before caret)"}, {"forward", "Forward (after caret)"}};
    private JTextComponent component;
    private JTextField backwardLookahead;
    private JComboBox caretBias;
    private JTextField forwardLookahead;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JComboBox searchDirection;
    private JCheckBox showParameters;

    public ControlPanel(JTextComponent component) {
        this.component = component;
        this.initComponents();
        this.backwardLookahead.setText(ControlPanel.getBwdLookahead(component));
        this.forwardLookahead.setText(ControlPanel.getFwdLookahead(component));
        this.searchDirection.setSelectedItem(ControlPanel.getSearchDirection(component));
        this.caretBias.setSelectedItem(ControlPanel.getCaretBias(component));
        this.showParameters.setSelected(ControlPanel.getShowParameters(component));
    }

    public void applyChanges() {
        ControlPanel.setBwdLookahead(this.component, this.backwardLookahead.getText());
        ControlPanel.setFwdLookahead(this.component, this.forwardLookahead.getText());
        ControlPanel.setSearchDirection(this.component, (String)this.searchDirection.getSelectedItem());
        ControlPanel.setCaretBias(this.component, (String)this.caretBias.getSelectedItem());
        ControlPanel.setShowParameters(this.component, this.showParameters.isSelected());
    }

    private static String getBwdLookahead(JTextComponent component) {
        Object value = component.getClientProperty("nbeditor-bracesMatching-maxBackwardLookahead");
        return value == null ? "" : value.toString();
    }

    private static void setBwdLookahead(JTextComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            component.putClientProperty("nbeditor-bracesMatching-maxBackwardLookahead", null);
        } else {
            component.putClientProperty("nbeditor-bracesMatching-maxBackwardLookahead", value);
        }
    }

    private static String getFwdLookahead(JTextComponent component) {
        Object value = component.getClientProperty("nbeditor-bracesMatching-maxForwardLookahead");
        return value == null ? "" : value.toString();
    }

    private static void setFwdLookahead(JTextComponent component, String value) {
        if (value == null || value.trim().length() == 0) {
            component.putClientProperty("nbeditor-bracesMatching-maxForwardLookahead", null);
        } else {
            component.putClientProperty("nbeditor-bracesMatching-maxForwardLookahead", value);
        }
    }

    private static String getSearchDirection(JTextComponent component) {
        Object value = component.getClientProperty("nbeditor-bracesMatching-searchDirection");
        if (value != null) {
            String s = value.toString();
            for (String[] pair : SEARCH_DIRECTIONS) {
                if (!pair[0].equals(s)) continue;
                return pair[1];
            }
        }
        return "";
    }

    private static void setSearchDirection(JTextComponent component, String value) {
        String s = null;
        if (value != null) {
            for (String[] pair : SEARCH_DIRECTIONS) {
                if (!pair[1].equals(value)) continue;
                s = pair[0];
                break;
            }
        }
        component.putClientProperty("nbeditor-bracesMatching-searchDirection", s);
    }

    private static String getCaretBias(JTextComponent component) {
        Object value = component.getClientProperty("nbeditor-bracesMatching-caretBias");
        if (value != null) {
            String s = value.toString();
            for (String[] pair : CARET_BIAS) {
                if (!pair[0].equals(s)) continue;
                return pair[1];
            }
        }
        return "";
    }

    private static void setCaretBias(JTextComponent component, String value) {
        String s = null;
        if (value != null) {
            for (String[] pair : CARET_BIAS) {
                if (!pair[1].equals(value)) continue;
                s = pair[0];
                break;
            }
        }
        component.putClientProperty("nbeditor-bracesMatching-caretBias", s);
    }

    private static boolean getShowParameters(JTextComponent component) {
        return Boolean.valueOf((String)component.getClientProperty("debug-showSearchParameters-dont-ever-use-it-or-you-will-die"));
    }

    private static void setShowParameters(JTextComponent component, boolean show) {
        component.putClientProperty("debug-showSearchParameters-dont-ever-use-it-or-you-will-die", Boolean.toString(show));
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.backwardLookahead = new JTextField();
        this.forwardLookahead = new JTextField();
        this.searchDirection = new JComboBox();
        this.jLabel4 = new JLabel();
        this.caretBias = new JComboBox();
        this.showParameters = new JCheckBox();
        this.jLabel5 = new JLabel();
        this.jLabel1.setText(NbBundle.getMessage(ControlPanel.class, (String)"jLabel1.text"));
        this.jLabel2.setText(NbBundle.getMessage(ControlPanel.class, (String)"jLabel2.text"));
        this.jLabel3.setText(NbBundle.getMessage(ControlPanel.class, (String)"jLabel3.text"));
        this.backwardLookahead.setText(NbBundle.getMessage(ControlPanel.class, (String)"backwardLookahead.text"));
        this.forwardLookahead.setText(NbBundle.getMessage(ControlPanel.class, (String)"forwardLookahead.text"));
        this.searchDirection.setModel(new DefaultComboBoxModel<String>(new String[]{"", "Backward Preferred", "Forward Preferred"}));
        this.jLabel4.setText(NbBundle.getMessage(ControlPanel.class, (String)"jLabel4.text"));
        this.caretBias.setModel(new DefaultComboBoxModel<String>(new String[]{"", "Backward (before caret)", "Forward (after caret)"}));
        this.showParameters.setText(NbBundle.getMessage(ControlPanel.class, (String)"jCheckBox1.text_1"));
        this.showParameters.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.showParameters.setMargin(new Insets(0, 0, 0, 0));
        this.jLabel5.setText(NbBundle.getMessage(ControlPanel.class, (String)"jLabel5.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.jLabel1, -2, 183, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.backwardLookahead, -1, 181, 32767)).addGroup(layout.createSequentialGroup().addComponent(this.jLabel2, -2, 183, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.forwardLookahead, -1, 181, 32767)).addGroup(layout.createSequentialGroup().addComponent(this.jLabel3, -2, 183, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.searchDirection, 0, 181, 32767)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel4, -2, 183, -2).addComponent(this.jLabel5, -2, 183, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.showParameters).addComponent(this.caretBias, 0, 181, 32767)))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.backwardLookahead, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.forwardLookahead, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.searchDirection, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this.caretBias, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.showParameters).addComponent(this.jLabel5)).addContainerGap(-1, 32767)));
    }
}

