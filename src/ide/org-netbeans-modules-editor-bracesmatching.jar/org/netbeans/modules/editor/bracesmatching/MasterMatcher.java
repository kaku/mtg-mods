/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.bracesmatching.MatchEvent;
import org.netbeans.modules.editor.bracesmatching.MatchListener;
import org.netbeans.modules.editor.bracesmatching.SpiAccessor;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.RequestProcessor;

public final class MasterMatcher {
    private static final Logger LOG = Logger.getLogger(MasterMatcher.class.getName());
    public static final String PROP_SEARCH_DIRECTION = "nbeditor-bracesMatching-searchDirection";
    public static final String D_BACKWARD = "backward-preferred";
    public static final String D_FORWARD = "forward-preferred";
    public static final String PROP_CARET_BIAS = "nbeditor-bracesMatching-caretBias";
    public static final String B_BACKWARD = "backward";
    public static final String B_FORWARD = "forward";
    public static final String PROP_MAX_BACKWARD_LOOKAHEAD = "nbeditor-bracesMatching-maxBackwardLookahead";
    public static final String PROP_MAX_FORWARD_LOOKAHEAD = "nbeditor-bracesMatching-maxForwardLookahead";
    private static final int DEFAULT_MAX_LOOKAHEAD = 1;
    private static final int MAX_MAX_LOOKAHEAD = 256;
    public static final String PROP_SHOW_SEARCH_PARAMETERS = "debug-showSearchParameters-dont-ever-use-it-or-you-will-die";
    private static final AttributeSet CARET_BIAS_HIGHLIGHT = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Underline, Color.BLACK});
    private static final AttributeSet MAX_LOOKAHEAD_HIGHLIGHT = AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.WaveUnderlineColor, Color.BLUE});
    private List<MatchListener> matchListeners = new LinkedList<MatchListener>();
    private static final RequestProcessor PR = new RequestProcessor("EditorBracesMatching", 5, true);
    static final Map<Thread, Result> THREAD_RESULTS = Collections.synchronizedMap(new HashMap());
    private final String LOCK = new String("MasterMatcher.LOCK");
    private final JTextComponent component;
    private RequestProcessor.Task task = null;
    private Result lastResult = null;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addMatchListener(MatchListener l) {
        String string = this.LOCK;
        synchronized (string) {
            this.matchListeners.add(l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeMatchListener(MatchListener l) {
        String string = this.LOCK;
        synchronized (string) {
            this.matchListeners.remove(l);
        }
    }

    public static synchronized MasterMatcher get(JTextComponent component) {
        MasterMatcher mm = (MasterMatcher)component.getClientProperty(MasterMatcher.class);
        if (mm == null) {
            mm = new MasterMatcher(component);
            component.putClientProperty(MasterMatcher.class, mm);
        }
        return mm;
    }

    public static boolean isTaskCanceled() {
        Result threadTask = THREAD_RESULTS.get(Thread.currentThread());
        assert (threadTask != null);
        return threadTask.isCanceled();
    }

    public static void markTestThread() {
        MasterMatcher mm;
        MasterMatcher masterMatcher = mm = new MasterMatcher(null);
        masterMatcher.getClass();
        THREAD_RESULTS.put(Thread.currentThread(), masterMatcher.new Result(null, -1, null, null, -1, -1));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void highlight(Document document, int caretOffset, OffsetsBag highlights, AttributeSet matchedColoring, AttributeSet mismatchedColoring, AttributeSet matchedMulticharColoring, AttributeSet mismatchedMulticharColoring) {
        assert (document != null);
        assert (highlights != null);
        assert (matchedColoring != null);
        assert (mismatchedColoring != null);
        assert (matchedMulticharColoring != null);
        assert (mismatchedMulticharColoring != null);
        assert (caretOffset >= 0);
        this.fireMatchCleared();
        String string = this.LOCK;
        synchronized (string) {
            Object allowedSearchDirection = this.getAllowedDirection();
            Object caretBias = this.getCaretBias();
            int maxBwdLookahead = this.getMaxLookahead(true);
            int maxFwdLookahead = this.getMaxLookahead(false);
            if (this.task != null) {
                if (this.lastResult.getCaretOffset() == caretOffset && this.lastResult.getAllowedDirection() == allowedSearchDirection && this.lastResult.getCaretBias() == caretBias && this.lastResult.getMaxBwdLookahead() == maxBwdLookahead && this.lastResult.getMaxFwdLookahead() == maxBwdLookahead) {
                    this.lastResult.addHighlightingJob(highlights, matchedColoring, mismatchedColoring, matchedMulticharColoring, mismatchedMulticharColoring);
                } else {
                    this.lastResult.cancel();
                    this.task = null;
                }
            }
            if (this.task == null) {
                this.lastResult = new Result(document, caretOffset, allowedSearchDirection, caretBias, maxBwdLookahead, maxFwdLookahead);
                this.lastResult.addHighlightingJob(highlights, matchedColoring, mismatchedColoring, matchedMulticharColoring, mismatchedMulticharColoring);
                this.task = PR.post((Runnable)this.lastResult);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void navigate(Document document, int caretOffset, Caret caret, boolean select) {
        assert (document != null);
        assert (caret != null);
        assert (caretOffset >= 0);
        RequestProcessor.Task waitFor = null;
        String string = this.LOCK;
        synchronized (string) {
            Object allowedSearchDirection = this.getAllowedDirection();
            Object caretBias = this.getCaretBias();
            int maxBwdLookahead = this.getMaxLookahead(true);
            int maxFwdLookahead = this.getMaxLookahead(false);
            boolean documentLocked = DocumentUtilities.isReadLocked((Document)document);
            if (this.task != null) {
                if (!documentLocked && this.lastResult.getCaretOffset() == caretOffset && this.lastResult.getAllowedDirection() == allowedSearchDirection && this.lastResult.getCaretBias() == caretBias && this.lastResult.getMaxBwdLookahead() == maxBwdLookahead && this.lastResult.getMaxFwdLookahead() == maxBwdLookahead) {
                    this.lastResult.addNavigationJob(caret, select);
                    waitFor = this.task;
                } else {
                    this.lastResult.cancel();
                    this.task = null;
                }
            }
            if (this.task == null) {
                this.lastResult = new Result(document, caretOffset, allowedSearchDirection, caretBias, maxBwdLookahead, maxFwdLookahead);
                this.lastResult.addNavigationJob(caret, select);
                if (documentLocked) {
                    this.lastResult.run();
                } else {
                    waitFor = this.task = PR.post((Runnable)this.lastResult);
                }
            }
        }
        if (waitFor != null) {
            waitFor.waitFinished();
        }
    }

    private MasterMatcher(JTextComponent component) {
        this.component = component;
    }

    private Object getAllowedDirection() {
        Object allowedDirection = this.component.getClientProperty("nbeditor-bracesMatching-searchDirection");
        return allowedDirection != null ? allowedDirection : "backward-preferred";
    }

    private Object getCaretBias() {
        Object caretBias = this.component.getClientProperty("nbeditor-bracesMatching-caretBias");
        return caretBias != null ? caretBias : "backward";
    }

    private int getMaxLookahead(boolean backward) {
        String propName = backward ? "nbeditor-bracesMatching-maxBackwardLookahead" : "nbeditor-bracesMatching-maxForwardLookahead";
        int maxLookahead = 1;
        Object value = this.component.getClientProperty(propName);
        if (value instanceof Integer) {
            maxLookahead = (Integer)value;
        } else if (value != null) {
            try {
                maxLookahead = Integer.valueOf(value.toString());
            }
            catch (NumberFormatException nfe) {
                LOG.log(Level.WARNING, "Can't parse the value of " + propName + ": '" + value + "'", nfe);
            }
        }
        if (maxLookahead >= 0 && maxLookahead <= 256) {
            return maxLookahead;
        }
        LOG.warning("Invalid value of " + propName + ": " + maxLookahead);
        return 256;
    }

    private static void highlightAreas(int[] origin, int[] matches, OffsetsBag highlights, AttributeSet matchedColoring, AttributeSet mismatchedColoring, int maxOffset) {
        highlights.clear();
        if (matches != null && matches.length >= 2) {
            MasterMatcher.placeHighlights(origin, true, highlights, matchedColoring, maxOffset);
            MasterMatcher.placeHighlights(matches, false, highlights, matchedColoring, maxOffset);
        } else if (origin != null && origin.length >= 2) {
            MasterMatcher.placeHighlights(origin, true, highlights, mismatchedColoring, maxOffset);
        }
    }

    private Position[] toPositions(JTextComponent c, int[] offsets) throws BadLocationException {
        if (offsets == null) {
            return null;
        }
        Position[] ret = new Position[offsets.length];
        for (int i = 0; i < ret.length; ++i) {
            ret[i] = c.getDocument().createPosition(offsets[i]);
        }
        return ret;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireMatchesHighlighted(Position[] origin, Position[] matches, BracesMatcher.ContextLocator locator) {
        MatchListener[] ll;
        String string = this.LOCK;
        synchronized (string) {
            if (this.matchListeners.isEmpty()) {
                return;
            }
            ll = this.matchListeners.toArray(new MatchListener[this.matchListeners.size()]);
        }
        if (ll.length == 0) {
            return;
        }
        MatchEvent evt = new MatchEvent(this.component, locator, this);
        evt.setHighlights(origin, matches);
        for (int i = 0; i < ll.length; ++i) {
            MatchListener matchListener = ll[i];
            matchListener.matchHighlighted(evt);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireMatchCleared() {
        MatchListener[] ll;
        String string = this.LOCK;
        synchronized (string) {
            if (this.matchListeners.isEmpty()) {
                return;
            }
            ll = this.matchListeners.toArray(new MatchListener[this.matchListeners.size()]);
        }
        MatchEvent evt = new MatchEvent(this.component, null, this);
        for (int i = 0; i < ll.length; ++i) {
            MatchListener matchListener = ll[i];
            matchListener.matchCleared(evt);
        }
    }

    private static void placeHighlights(int[] offsets, boolean skipFirst, OffsetsBag highlights, AttributeSet coloring, int max) {
        int startIdx = skipFirst && offsets.length > 2 ? 2 : 0;
        for (int i = startIdx; i < offsets.length; i += 2) {
            try {
                int from = Math.min(offsets[i], max);
                int to = Math.min(offsets[i + 1], max);
                if (from == to) {
                    return;
                }
                highlights.addHighlight(from, to, coloring);
                continue;
            }
            catch (Throwable t) {
                LOG.log(Level.FINE, null, t);
            }
        }
    }

    private static boolean isMultiChar(int[] offsets, boolean skipFirst) {
        if (offsets != null) {
            int startIdx = skipFirst && offsets.length > 2 ? 1 : 0;
            for (int i = startIdx; i < offsets.length / 2; ++i) {
                if (offsets[i * 2 + 1] - offsets[i * 2] <= 1) continue;
                return true;
            }
        }
        return false;
    }

    private static void navigateAreas(int[] origin, int[] matches, int caretOffset, Object caretBias, Caret caret, boolean select) {
        if (matches != null && matches.length >= 2) {
            int set;
            int newDotBackwardIdx = -1;
            int newDotForwardIdx = -1;
            for (int i = 0; i < matches.length / 2; ++i) {
                if (matches[i * 2] <= origin[0] && (newDotBackwardIdx == -1 || matches[i * 2] > matches[newDotBackwardIdx * 2])) {
                    newDotBackwardIdx = i;
                }
                if (matches[i * 2] < origin[1] || newDotForwardIdx != -1 && matches[i * 2] >= matches[newDotForwardIdx * 2]) continue;
                newDotForwardIdx = i;
            }
            if (newDotBackwardIdx != -1) {
                if (select) {
                    int move;
                    if (caretOffset < origin[1]) {
                        set = origin[0];
                        move = matches[2 * newDotBackwardIdx + 1];
                    } else {
                        set = origin[1];
                        move = matches[2 * newDotBackwardIdx];
                    }
                    if (caret.getDot() == caret.getMark()) {
                        caret.setDot(set);
                    }
                    caret.moveDot(move);
                } else if ("backward".equalsIgnoreCase(caretBias.toString())) {
                    caret.setDot(matches[2 * newDotBackwardIdx + 1]);
                } else {
                    caret.setDot(matches[2 * newDotBackwardIdx]);
                }
            } else if (newDotForwardIdx != -1) {
                if (select) {
                    int move;
                    if (caretOffset > origin[0]) {
                        set = origin[1];
                        move = matches[2 * newDotForwardIdx];
                    } else {
                        set = origin[0];
                        move = matches[2 * newDotForwardIdx + 1];
                    }
                    if (caret.getDot() == caret.getMark()) {
                        caret.setDot(set);
                    }
                    caret.moveDot(move);
                } else if ("backward".equalsIgnoreCase(caretBias.toString())) {
                    caret.setDot(matches[2 * newDotForwardIdx + 1]);
                } else {
                    caret.setDot(matches[2 * newDotForwardIdx]);
                }
            }
        }
    }

    private static Collection<? extends BracesMatcherFactory> findFactories(final Document document, final int offset, final boolean backward) {
        final MimePath[] mimePath = new MimePath[]{null};
        document.render(new Runnable(){

            @Override
            public void run() {
                TokenHierarchy th = TokenHierarchy.get((Document)document);
                if (th.isActive()) {
                    List sequences = th.embeddedTokenSequences(offset, backward);
                    if (!sequences.isEmpty()) {
                        String path = ((TokenSequence)sequences.get(sequences.size() - 1)).languagePath().mimePath();
                        mimePath[0] = MimePath.parse((String)path);
                    }
                } else {
                    String mimeType = (String)document.getProperty("mimeType");
                    mimePath[0] = mimeType != null ? MimePath.parse((String)mimeType) : MimePath.EMPTY;
                }
            }
        });
        List factories = mimePath[0] == null ? Collections.emptyList() : MimeLookup.getLookup((MimePath)mimePath[0]).lookupAll(BracesMatcherFactory.class);
        return factories;
    }

    private void scheduleMatchHighlighted(Result r, int[] origin, int[] matches, BracesMatcher.ContextLocator locator, Document d) throws BadLocationException {
        PR.post((Runnable)new Firer(r, this.toPositions(this.component, origin), this.toPositions(this.component, matches), locator), 200);
    }

    private final class Result
    implements Runnable {
        private final Document document;
        private final int caretOffset;
        private final Object allowedDirection;
        private final Object caretBias;
        private final int maxBwdLookahead;
        private final int maxFwdLookahead;
        private volatile boolean canceled;
        private final List<Object[]> highlightingJobs;
        private final List<Object[]> navigationJobs;

        public Result(Document document, int caretOffset, Object allowedDirection, Object caretBias, int maxBwdLookahead, int maxFwdLookahead) {
            this.canceled = false;
            this.highlightingJobs = new ArrayList<Object[]>();
            this.navigationJobs = new ArrayList<Object[]>();
            this.document = document;
            this.caretOffset = caretOffset;
            this.allowedDirection = allowedDirection;
            this.caretBias = caretBias;
            this.maxBwdLookahead = maxBwdLookahead;
            this.maxFwdLookahead = maxFwdLookahead;
        }

        public void addHighlightingJob(OffsetsBag highlights, AttributeSet matchedColoring, AttributeSet mismatchedColoring, AttributeSet matchedMulticharColoring, AttributeSet mismatchedMulticharColoring) {
            this.highlightingJobs.add(new Object[]{highlights, matchedColoring, mismatchedColoring, matchedMulticharColoring, mismatchedMulticharColoring});
        }

        public void addNavigationJob(Caret caret, boolean select) {
            this.navigationJobs.add(new Object[]{caret, select});
        }

        public int getCaretOffset() {
            return this.caretOffset;
        }

        public Object getAllowedDirection() {
            return this.allowedDirection;
        }

        public Object getCaretBias() {
            return this.caretBias;
        }

        public int getMaxBwdLookahead() {
            return this.maxBwdLookahead;
        }

        public int getMaxFwdLookahead() {
            return this.maxFwdLookahead;
        }

        public boolean isCanceled() {
            return this.canceled;
        }

        public void cancel() {
            this.canceled = true;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            MasterMatcher.THREAD_RESULTS.put(Thread.currentThread(), this);
            try {
                this._run();
            }
            finally {
                MasterMatcher.THREAD_RESULTS.remove(Thread.currentThread());
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void _run() {
            if (this.canceled) {
                return;
            }
            if (this.caretOffset > this.document.getLength()) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Invalid offset, braces matching request ignored. Offset = " + this.caretOffset + ", doc.getLength() = " + this.document.getLength());
                }
                return;
            }
            int[] origin = null;
            int[] matches = null;
            BracesMatcher.ContextLocator locator = null;
            try {
                BracesMatcher[] matcher = new BracesMatcher[1];
                if ("backward-preferred".equalsIgnoreCase(this.allowedDirection.toString())) {
                    origin = this.findOrigin(true, matcher);
                    if (origin == null && !this.canceled) {
                        origin = this.findOrigin(false, matcher);
                    }
                } else if ("forward-preferred".equalsIgnoreCase(this.allowedDirection.toString()) && (origin = this.findOrigin(false, matcher)) == null && !this.canceled) {
                    origin = this.findOrigin(true, matcher);
                }
                if (origin != null && !this.canceled && (matches = matcher[0].findMatches()) != null) {
                    if (matches.length == 0) {
                        matches = null;
                    } else if (matches.length % 2 != 0) {
                        if (LOG.isLoggable(Level.WARNING)) {
                            LOG.log(Level.WARNING, "Invalid match found by matcher {0}: {1}", new Object[]{matcher[0], Arrays.asList(matches)});
                        }
                        matches = null;
                    } else if (matcher[0] instanceof BracesMatcher.ContextLocator) {
                        locator = (BracesMatcher.ContextLocator)((Object)matcher[0]);
                    }
                }
            }
            catch (ThreadDeath td) {
                throw td;
            }
            catch (Throwable e2) {
                for (Throwable t = e2; t != null; t = t.getCause()) {
                    if (!(t instanceof InterruptedException)) continue;
                    return;
                }
                LOG.log(Level.FINE, null, e2);
                return;
            }
            String e2 = MasterMatcher.this.LOCK;
            synchronized (e2) {
                if (this.canceled) {
                    return;
                }
                MasterMatcher.this.task = null;
            }
            final int[] _origin = origin;
            final int[] _matches = matches;
            final BracesMatcher.ContextLocator _locator = locator;
            this.document.render(new Runnable(){

                @Override
                public void run() {
                    try {
                        for (Object[] job2 : Result.this.highlightingJobs) {
                            AttributeSet matchedColoring;
                            AttributeSet mismatchedColoring;
                            if (MasterMatcher.isMultiChar(_origin, true) || MasterMatcher.isMultiChar(_matches, false)) {
                                matchedColoring = (AttributeSet)job2[3];
                                mismatchedColoring = (AttributeSet)job2[4];
                            } else {
                                matchedColoring = (AttributeSet)job2[1];
                                mismatchedColoring = (AttributeSet)job2[2];
                            }
                            MasterMatcher.highlightAreas(_origin, _matches, (OffsetsBag)job2[0], matchedColoring, mismatchedColoring, Result.this.document.getLength());
                            if (!Boolean.valueOf((String)MasterMatcher.this.component.getClientProperty("debug-showSearchParameters-dont-ever-use-it-or-you-will-die")).booleanValue()) continue;
                            Result.this.showSearchParameters((OffsetsBag)job2[0]);
                        }
                        MasterMatcher.this.scheduleMatchHighlighted(Result.this, _origin, _matches, _locator, Result.this.document);
                        for (Object[] job2 : Result.this.navigationJobs) {
                            MasterMatcher.navigateAreas(_origin, _matches, Result.this.caretOffset, Result.this.caretBias, (Caret)job2[0], (Boolean)job2[1]);
                        }
                    }
                    catch (Exception e) {
                        LOG.log(Level.FINE, null, e);
                        for (Object[] job : Result.this.highlightingJobs) {
                            ((OffsetsBag)job[0]).clear();
                        }
                    }
                }
            });
        }

        private int[] findOrigin(boolean backward, BracesMatcher[] matcher) throws InterruptedException {
            int maxLookahead;
            Element paragraph = DocumentUtilities.getParagraphElement((Document)this.document, (int)this.caretOffset);
            int adjustedCaretOffset = this.caretOffset;
            int lookahead = 0;
            if (backward) {
                maxLookahead = this.maxBwdLookahead;
                if ("forward".equalsIgnoreCase(this.caretBias.toString())) {
                    if (adjustedCaretOffset < paragraph.getEndOffset() - 1) {
                        ++adjustedCaretOffset;
                        ++maxLookahead;
                    }
                } else if (maxLookahead == 0) {
                    maxLookahead = 1;
                }
                if ((lookahead = adjustedCaretOffset - paragraph.getStartOffset()) > maxLookahead) {
                    lookahead = maxLookahead;
                }
            } else {
                maxLookahead = this.maxFwdLookahead;
                if ("backward".equalsIgnoreCase(this.caretBias.toString())) {
                    if (adjustedCaretOffset > paragraph.getStartOffset()) {
                        --adjustedCaretOffset;
                        ++maxLookahead;
                    }
                } else if (maxLookahead == 0) {
                    maxLookahead = 1;
                }
                if ((lookahead = paragraph.getEndOffset() - 1 - adjustedCaretOffset) > maxLookahead) {
                    lookahead = maxLookahead;
                }
            }
            Collection factories = Collections.emptyList();
            if (lookahead > 0) {
                factories = MasterMatcher.findFactories(this.document, adjustedCaretOffset, backward);
            }
            if (!factories.isEmpty()) {
                MatcherContext context = SpiAccessor.get().createCaretContext(this.document, adjustedCaretOffset, backward, lookahead);
                for (BracesMatcherFactory factory : factories) {
                    matcher[0] = factory.createMatcher(context);
                    if (matcher[0] == null) continue;
                    break;
                }
            }
            if (matcher[0] != null) {
                int[] origin = null;
                try {
                    origin = matcher[0].findOrigin();
                }
                catch (BadLocationException ble) {
                    LOG.log(Level.FINE, null, ble);
                }
                if (origin != null) {
                    if (origin.length == 0) {
                        origin = null;
                    } else if (origin.length % 2 != 0) {
                        if (LOG.isLoggable(Level.WARNING)) {
                            LOG.warning("Invalid BracesMatcher implementation, findOrigin() should return nothing or offset pairs. Offending BracesMatcher: " + matcher[0]);
                        }
                        origin = null;
                    } else {
                        for (int i = 0; i < origin.length / 2; ++i) {
                            if (origin[2 * i] >= 0 && origin[2 * i + 1] <= this.document.getLength() && origin[2 * i] <= origin[2 * i + 1]) continue;
                            if (LOG.isLoggable(Level.WARNING)) {
                                LOG.warning("Invalid origin offsets [" + origin[2 * i] + ", " + origin[2 * i + 1] + "]. " + "Offending BracesMatcher: " + matcher[0]);
                            }
                            origin = null;
                            break;
                        }
                    }
                    if (origin != null) {
                        if (backward) {
                            if (origin[1] < this.caretOffset - lookahead || origin[0] > this.caretOffset) {
                                if (LOG.isLoggable(Level.WARNING)) {
                                    LOG.warning("Origin offsets out of range, origin = [" + origin[0] + ", " + origin[1] + "], " + "caretOffset = " + this.caretOffset + ", lookahead = " + lookahead + ", searching backwards. " + "Offending BracesMatcher: " + matcher[0]);
                                }
                                origin = null;
                            }
                        } else if (origin[1] < this.caretOffset || origin[0] > this.caretOffset + lookahead) {
                            if (LOG.isLoggable(Level.WARNING)) {
                                LOG.warning("Origin offsets out of range, origin = [" + origin[0] + ", " + origin[1] + "], " + "caretOffset = " + this.caretOffset + ", lookahead = " + lookahead + ", searching forward. " + "Offending BracesMatcher: " + matcher[0]);
                            }
                            origin = null;
                        }
                    }
                }
                if (LOG.isLoggable(Level.FINE)) {
                    if (origin != null) {
                        LOG.fine("[" + origin[0] + ", " + origin[1] + "] for caret = " + this.caretOffset + ", lookahead = " + (backward ? "-" : "") + lookahead);
                    } else {
                        LOG.fine("[null] for caret = " + this.caretOffset + ", lookahead = " + (backward ? "-" : "") + lookahead);
                    }
                }
                return origin;
            }
            return null;
        }

        private void showSearchParameters(OffsetsBag bag) {
            Element paragraph = DocumentUtilities.getParagraphElement((Document)this.document, (int)this.caretOffset);
            if ("backward".equalsIgnoreCase(this.caretBias.toString())) {
                if (this.caretOffset > paragraph.getStartOffset()) {
                    bag.addHighlight(this.caretOffset - 1, this.caretOffset, CARET_BIAS_HIGHLIGHT);
                }
            } else if (this.caretOffset < paragraph.getEndOffset() - 1) {
                bag.addHighlight(this.caretOffset, this.caretOffset + 1, CARET_BIAS_HIGHLIGHT);
            }
            int bwdLookahead = Math.min(this.maxBwdLookahead, this.caretOffset - paragraph.getStartOffset());
            int fwdLookahead = Math.min(this.maxFwdLookahead, paragraph.getEndOffset() - 1 - this.caretOffset);
            bag.addHighlight(this.caretOffset - bwdLookahead, this.caretOffset, MAX_LOOKAHEAD_HIGHLIGHT);
            bag.addHighlight(this.caretOffset, this.caretOffset + fwdLookahead, MAX_LOOKAHEAD_HIGHLIGHT);
        }

    }

    private final class Firer
    implements Runnable {
        private Result myResult;
        private Position[] origin;
        private Position[] matches;
        private BracesMatcher.ContextLocator locator;

        public Firer(Result myResult, Position[] origin, Position[] matches, BracesMatcher.ContextLocator locator) {
            this.myResult = myResult;
            this.origin = origin;
            this.matches = matches;
            this.locator = locator;
        }

        @Override
        public void run() {
            if (MasterMatcher.this.lastResult != this.myResult) {
                return;
            }
            MasterMatcher.this.fireMatchesHighlighted(this.origin, this.matches, this.locator);
        }
    }

}

