/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import org.netbeans.modules.editor.bracesmatching.ControlPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;

public class ControlPanelAction
extends TextAction {
    public ControlPanelAction() {
        super("match-brace-control-properties");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTextComponent component = this.getTextComponent(e);
        ControlPanel panel = new ControlPanel(component);
        DialogDescriptor dd = new DialogDescriptor((Object)panel, "Braces Matching Control Panel", true, null);
        Dialog d = DialogDisplayer.getDefault().createDialog(dd);
        d.setVisible(true);
        if (dd.getValue() == DialogDescriptor.OK_OPTION) {
            panel.applyChanges();
        }
    }
}

