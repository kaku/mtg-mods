/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class BraceToolTip
extends JPanel {
    private JEditorPane previewPane;
    private int editorPaneWidth;

    public BraceToolTip(JComponent c, JEditorPane pane) {
        this.previewPane = pane;
        this.setLayout(new BorderLayout());
        this.add((Component)c, "Center");
    }

    public BraceToolTip(JEditorPane editorPane, JEditorPane previewPane) {
        this.previewPane = previewPane;
        this.setLayout(new BorderLayout());
        this.add((Component)previewPane, "Center");
        this.putClientProperty("tooltip-type", "fold-preview");
        this.addGlyphGutter(previewPane);
        this.editorPaneWidth = editorPane.getSize().width;
        Color foreColor = previewPane.getForeground();
        this.setBorder(new LineBorder(foreColor));
        this.setOpaque(true);
    }

    private void addGlyphGutter(JTextComponent jtx) {
        ClassLoader cls = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        JComponent gutter = null;
        try {
            Class clazz = Class.forName("org.netbeans.editor.GlyphGutter", true, cls);
            Object o = clazz.newInstance();
            Method m = clazz.getDeclaredMethod("createSideBar", JTextComponent.class);
            gutter = (JComponent)m.invoke(o, jtx);
        }
        catch (IllegalArgumentException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (SecurityException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InstantiationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (ClassNotFoundException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        if (gutter != null) {
            this.add((Component)gutter, "West");
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        return new Dimension(d.width, d.height);
    }
}

