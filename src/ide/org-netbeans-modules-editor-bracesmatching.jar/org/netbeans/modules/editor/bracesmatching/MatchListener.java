/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.bracesmatching;

import java.util.EventListener;
import org.netbeans.modules.editor.bracesmatching.MatchEvent;

public interface MatchListener
extends EventListener {
    public void matchHighlighted(MatchEvent var1);

    public void matchCleared(MatchEvent var1);
}

