/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.SideBarFactory
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.modules.editor.bracesmatching;

import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.modules.editor.bracesmatching.BraceMatchingSidebarComponent;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="SideBars")
public class BraceMatchingSidebarFactory
implements SideBarFactory {
    public JComponent createSideBar(JTextComponent target) {
        return new BraceMatchingSidebarComponent(target);
    }
}

