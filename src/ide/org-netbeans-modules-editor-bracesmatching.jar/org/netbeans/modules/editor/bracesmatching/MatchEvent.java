/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.bracesmatching;

import java.util.EventObject;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;

public class MatchEvent
extends EventObject {
    private JTextComponent component;
    private Position[] origin;
    private Position[] matches;
    private BracesMatcher.ContextLocator locator;

    public MatchEvent(JTextComponent component, BracesMatcher.ContextLocator locator, Object source) {
        super(source);
        this.component = component;
        this.locator = locator;
    }

    public BracesMatcher.ContextLocator getLocator() {
        return this.locator;
    }

    public void setHighlights(Position[] origin, Position[] matches) {
        this.origin = origin;
        this.matches = matches;
    }

    public JTextComponent getComponent() {
        return this.component;
    }

    public Position[] getOrigin() {
        return this.origin;
    }

    public Position[] getMatches() {
        return this.matches;
    }
}

