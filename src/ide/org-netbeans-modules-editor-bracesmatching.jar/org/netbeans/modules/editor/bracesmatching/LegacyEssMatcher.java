/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 */
package org.netbeans.modules.editor.bracesmatching;

import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;

public final class LegacyEssMatcher
implements BracesMatcher,
BracesMatcherFactory {
    private final MatcherContext context;
    private final ExtSyntaxSupport ess;
    private int[] block;

    public LegacyEssMatcher() {
        this(null, null);
    }

    private LegacyEssMatcher(MatcherContext context, ExtSyntaxSupport ess) {
        this.context = context;
        this.ess = ess;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int[] findOrigin() throws BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            int[] arrn;
            int offset = this.context.isSearchingBackward() ? this.context.getSearchOffset() - 1 : this.context.getSearchOffset();
            this.block = this.ess.findMatchingBlock(offset, false);
            if (this.block == null) {
                arrn = null;
            } else {
                int[] arrn2 = new int[2];
                arrn2[0] = offset;
                arrn = arrn2;
                arrn2[1] = offset;
            }
            int[] arrn3 = arrn;
            return arrn3;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    @Override
    public int[] findMatches() throws InterruptedException {
        return this.block;
    }

    @Override
    public BracesMatcher createMatcher(MatcherContext context) {
        SyntaxSupport ss;
        Document d = context.getDocument();
        if (d instanceof BaseDocument && (ss = ((BaseDocument)d).getSyntaxSupport()) instanceof ExtSyntaxSupport && ss.getClass() != ExtSyntaxSupport.class) {
            return new LegacyEssMatcher(context, (ExtSyntaxSupport)ss);
        }
        return null;
    }
}

