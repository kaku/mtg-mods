/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactory
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactory$Factory
 */
package org.netbeans.modules.editor.bracesmatching;

import javax.swing.text.Document;
import org.netbeans.modules.editor.bracesmatching.SkipLinesViewFactory;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;

public abstract class SpiAccessor {
    private static SpiAccessor ACCESSOR = null;

    public static synchronized void register(SpiAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized SpiAccessor get() {
        try {
            Class clazz = Class.forName(MatcherContext.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected SpiAccessor() {
    }

    public abstract MatcherContext createCaretContext(Document var1, int var2, boolean var3, int var4);

    static {
        EditorViewFactory.registerFactory((EditorViewFactory.Factory)new SkipLinesViewFactory.Factory());
    }
}

