/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.view.EditorView
 *  org.netbeans.modules.editor.lib2.view.EditorView$Parent
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactory
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactory$Factory
 *  org.netbeans.modules.editor.lib2.view.ViewRenderContext
 *  org.netbeans.modules.editor.lib2.view.ViewUtils
 */
package org.netbeans.modules.editor.bracesmatching;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.ViewRenderContext;
import org.netbeans.modules.editor.lib2.view.ViewUtils;

final class SkipLinesViewFactory
extends EditorViewFactory {
    public static final String PROP_SUPPRESS_RANGES = "nbeditorui.braces.suppressLines";
    private int[] suppressRanges = null;

    public SkipLinesViewFactory(View documentView) {
        super(documentView);
        JTextComponent comp = this.textComponent();
        this.suppressRanges = (int[])comp.getClientProperty("nbeditorui.braces.suppressLines");
    }

    public void restart(int startOffset, int endOffset, boolean createViews) {
        JTextComponent comp = this.textComponent();
        this.suppressRanges = (int[])comp.getClientProperty("nbeditorui.braces.suppressLines");
    }

    public void continueCreation(int startOffset, int endOffset) {
    }

    public int nextViewStartOffset(int offset) {
        if (this.suppressRanges == null) {
            return Integer.MAX_VALUE;
        }
        if (offset >= this.suppressRanges[0]) {
            return Integer.MAX_VALUE;
        }
        return this.suppressRanges[0];
    }

    public EditorView createView(int startOffset, int limitOffset, boolean forcedLimit, EditorView origView, int nextOrigViewOffset) {
        return new NullView(this.textComponent(), this.suppressRanges[0], this.suppressRanges[1], this.suppressRanges[2]);
    }

    public int viewEndOffset(int startOffset, int limitOffset, boolean forcedLimit) {
        if (this.suppressRanges != null && this.suppressRanges[1] <= limitOffset) {
            return this.suppressRanges[1];
        }
        return -1;
    }

    public void finishCreation() {
    }

    public static class Factory
    implements EditorViewFactory.Factory {
        public EditorViewFactory createEditorViewFactory(View documentView) {
            return new SkipLinesViewFactory(documentView);
        }

        public int weight() {
            return 200;
        }
    }

    private static class NullView
    extends EditorView {
        int start;
        int end;
        int rawOffset;
        int indent;
        JTextComponent component;
        private TextLayout collapsedTextLayout;

        public NullView(JTextComponent component, int start, int end, int indent) {
            super(null);
            this.start = start;
            this.end = end;
            this.indent = indent;
            this.component = component;
        }

        public int getStartOffset() {
            return this.start;
        }

        public int getEndOffset() {
            return this.end;
        }

        public int getRawEndOffset() {
            return this.rawOffset;
        }

        public int getLength() {
            return this.end - this.start;
        }

        public void setRawEndOffset(int rawEndOffset) {
            this.rawOffset = rawEndOffset;
        }

        public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
            Rectangle2D.Double allocBounds = ViewUtils.shape2Bounds((Shape)alloc);
            TextLayout textLayout = this.getTextLayout();
            if (textLayout != null) {
                g.setColor(this.component.getForeground());
                EditorView.Parent parent = (EditorView.Parent)this.getParent();
                float ascent = parent.getViewRenderContext().getDefaultAscent();
                float x = (float)allocBounds.getX();
                float y = (float)allocBounds.getY();
                textLayout.draw(g, x, y + ascent);
            }
        }

        public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
            return alloc;
        }

        public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
            return this.start;
        }

        public float getPreferredSpan(int axis) {
            EditorView.Parent parent = (EditorView.Parent)this.getParent();
            if (axis == 0) {
                float advance = 0.0f;
                TextLayout textLayout = this.getTextLayout();
                if (textLayout == null) {
                    return 0.0f;
                }
                return textLayout.getAdvance();
            }
            return parent != null ? parent.getViewRenderContext().getDefaultRowHeight() : 0.0f;
        }

        private TextLayout getTextLayout() {
            if (this.collapsedTextLayout == null) {
                EditorView.Parent parent = (EditorView.Parent)this.getParent();
                ViewRenderContext context = parent.getViewRenderContext();
                FontRenderContext frc = context.getFontRenderContext();
                assert (frc != null);
                Font font = context.getRenderFont(this.component.getFont());
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < this.indent; ++i) {
                    sb.append(' ');
                }
                sb.append("...");
                String text = sb.toString();
                this.collapsedTextLayout = new TextLayout(text, font, frc);
            }
            return this.collapsedTextLayout;
        }
    }

}

