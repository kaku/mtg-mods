/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor.bracesmatching;

import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="BracesMatchers")
public interface BracesMatcherFactory {
    public BracesMatcher createMatcher(MatcherContext var1);
}

