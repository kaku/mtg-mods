/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.bracesmatching.support;

import java.util.logging.Logger;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.bracesmatching.support.BracesMatcherSupport;

final class CharacterMatcher
implements BracesMatcher {
    private static final Logger LOG = Logger.getLogger(CharacterMatcher.class.getName());
    private final MatcherContext context;
    private final char[] matchingPairs;
    private final int lowerBound;
    private final int upperBound;
    private int originOffset;
    private char originalChar;
    private char matchingChar;
    private boolean backward;

    public /* varargs */ CharacterMatcher(MatcherContext context, int lowerBound, int upperBound, char ... matchingPairs) {
        this.context = context;
        this.lowerBound = lowerBound == -1 ? Integer.MIN_VALUE : lowerBound;
        int n = this.upperBound = upperBound == -1 ? Integer.MAX_VALUE : upperBound;
        assert (matchingPairs.length % 2 == 0);
        this.matchingPairs = matchingPairs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int[] findOrigin() throws BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            int[] result = BracesMatcherSupport.findChar(this.context.getDocument(), this.context.getSearchOffset(), this.context.isSearchingBackward() ? Math.max(this.context.getLimitOffset(), this.lowerBound) : Math.min(this.context.getLimitOffset(), this.upperBound), this.matchingPairs);
            if (result != null) {
                this.originOffset = result[0];
                this.originalChar = this.matchingPairs[result[1]];
                this.matchingChar = this.matchingPairs[result[1] + result[2]];
                this.backward = result[2] < 0;
                int[] arrn = new int[]{this.originOffset, this.originOffset + 1};
                return arrn;
            }
            int[] arrn = null;
            return arrn;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int[] findMatches() throws BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            int[] arrn;
            int offset = BracesMatcherSupport.matchChar(this.context.getDocument(), this.backward ? this.originOffset : this.originOffset + 1, this.backward ? Math.max(0, this.lowerBound) : Math.min(this.context.getDocument().getLength(), this.upperBound), this.originalChar, this.matchingChar);
            if (offset != -1) {
                int[] arrn2 = new int[2];
                arrn2[0] = offset;
                arrn = arrn2;
                arrn2[1] = offset + 1;
            } else {
                arrn = null;
            }
            int[] arrn3 = arrn;
            return arrn3;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }
}

