/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.bracesmatching;

import javax.swing.text.Document;
import org.netbeans.modules.editor.bracesmatching.MasterMatcher;
import org.netbeans.modules.editor.bracesmatching.SpiAccessor;

public final class MatcherContext {
    private final Document document;
    private final int offset;
    private final boolean backward;
    private final int lookahead;

    private MatcherContext(Document document, int offset, boolean backward, int lookahead) {
        this.document = document;
        this.offset = offset;
        this.backward = backward;
        this.lookahead = lookahead;
    }

    public Document getDocument() {
        return this.document;
    }

    public int getSearchOffset() {
        return this.offset;
    }

    public boolean isSearchingBackward() {
        return this.backward;
    }

    public int getLimitOffset() {
        return this.backward ? this.offset - this.lookahead : this.offset + this.lookahead;
    }

    public int getSearchLookahead() {
        return this.lookahead;
    }

    public static boolean isTaskCanceled() {
        return MasterMatcher.isTaskCanceled();
    }

    static {
        SpiAccessor.register(new SpiAccessorImpl());
    }

    private static final class SpiAccessorImpl
    extends SpiAccessor {
        private SpiAccessorImpl() {
        }

        @Override
        public MatcherContext createCaretContext(Document document, int offset, boolean backward, int lookahead) {
            return new MatcherContext(document, offset, backward, lookahead);
        }
    }

}

