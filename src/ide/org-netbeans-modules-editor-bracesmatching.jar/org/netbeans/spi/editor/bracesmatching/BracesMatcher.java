/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.bracesmatching;

import javax.swing.text.BadLocationException;
import org.netbeans.spi.editor.bracesmatching.BraceContext;

public interface BracesMatcher {
    public int[] findOrigin() throws InterruptedException, BadLocationException;

    public int[] findMatches() throws InterruptedException, BadLocationException;

    public static interface ContextLocator {
        public BraceContext findContext(int var1);
    }

}

