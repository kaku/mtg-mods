/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.bracesmatching;

import javax.swing.text.Position;

public final class BraceContext {
    private final Position start;
    private final Position end;
    private final BraceContext related;

    public Position getStart() {
        return this.start;
    }

    public Position getEnd() {
        return this.end;
    }

    public BraceContext getRelated() {
        return this.related;
    }

    public BraceContext createRelated(Position start, Position end) {
        return new BraceContext(start, end, this);
    }

    public static BraceContext create(Position start, Position end) {
        return new BraceContext(start, end, null);
    }

    private BraceContext(Position openContextStart, Position closeContextEnd, BraceContext related) {
        this.start = openContextStart;
        this.end = closeContextEnd;
        this.related = related;
    }
}

