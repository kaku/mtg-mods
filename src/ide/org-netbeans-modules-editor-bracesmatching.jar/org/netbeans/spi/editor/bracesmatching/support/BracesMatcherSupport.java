/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.bracesmatching.support;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Segment;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.bracesmatching.support.CharacterMatcher;

public final class BracesMatcherSupport {
    private static final char[] DEFAULT_CHARS = new char[]{'(', ')', '[', ']', '{', '}', '<', '>'};

    public static BracesMatcher defaultMatcher(MatcherContext context, int lowerBound, int upperBound) {
        return new CharacterMatcher(context, lowerBound, upperBound, DEFAULT_CHARS);
    }

    public static /* varargs */ BracesMatcher characterMatcher(MatcherContext context, int lowerBound, int upperBound, char ... matchingPairs) {
        return new CharacterMatcher(context, lowerBound, upperBound, matchingPairs);
    }

    public static /* varargs */ int[] findChar(Document document, int offset, int limit, char ... pairs) throws BadLocationException {
        assert (pairs.length % 2 == 0);
        boolean backward = limit < offset;
        int lookahead = backward ? offset - limit : limit - offset;
        int[] result = new int[3];
        if (backward) {
            Segment text = new Segment();
            document.getText(offset - lookahead, lookahead, text);
            for (int i = lookahead - 1; i >= 0; --i) {
                if (MatcherContext.isTaskCanceled()) {
                    return null;
                }
                if (!BracesMatcherSupport.detectOrigin(result, text.array[text.offset + i], pairs)) continue;
                result[0] = offset - (lookahead - i);
                return result;
            }
        } else {
            Segment text = new Segment();
            document.getText(offset, lookahead, text);
            for (int i = 0; i < lookahead; ++i) {
                if (MatcherContext.isTaskCanceled()) {
                    return null;
                }
                if (!BracesMatcherSupport.detectOrigin(result, text.array[text.offset + i], pairs)) continue;
                result[0] = offset + i;
                return result;
            }
        }
        return null;
    }

    public static int matchChar(Document document, int offset, int limit, char origin, char matching) throws BadLocationException {
        int lookahead;
        boolean backward = limit < offset;
        int n = lookahead = backward ? offset - limit : limit - offset;
        if (backward) {
            Segment text = new Segment();
            document.getText(offset - lookahead, lookahead, text);
            int count = 0;
            for (int i = lookahead - 1; i >= 0; --i) {
                if (MatcherContext.isTaskCanceled()) {
                    return -1;
                }
                if (origin == text.array[text.offset + i]) {
                    ++count;
                    continue;
                }
                if (matching != text.array[text.offset + i]) continue;
                if (count == 0) {
                    return offset - (lookahead - i);
                }
                --count;
            }
        } else {
            Segment text = new Segment();
            document.getText(offset, lookahead, text);
            int count = 0;
            for (int i = 0; i < lookahead; ++i) {
                if (MatcherContext.isTaskCanceled()) {
                    return -1;
                }
                if (origin == text.array[text.offset + i]) {
                    ++count;
                    continue;
                }
                if (matching != text.array[text.offset + i]) continue;
                if (count == 0) {
                    return offset + i;
                }
                --count;
            }
        }
        return -1;
    }

    private static /* varargs */ boolean detectOrigin(int[] results, char ch, char ... pairs) {
        int cnt = pairs.length / 2;
        for (int idx = 0; idx < 2; ++idx) {
            for (int i = 0; i < cnt; ++i) {
                int i2 = 2 * i + idx;
                if (ch != pairs[i2]) continue;
                results[1] = i2;
                results[2] = idx == 0 ? 1 : -1;
                return true;
            }
        }
        return false;
    }

    private static BracesMatcherFactory defaultMatcherFactory() {
        return new BracesMatcherFactory(){

            @Override
            public BracesMatcher createMatcher(MatcherContext context) {
                return BracesMatcherSupport.defaultMatcher(context, -1, -1);
            }
        };
    }

    private BracesMatcherSupport() {
    }

}

