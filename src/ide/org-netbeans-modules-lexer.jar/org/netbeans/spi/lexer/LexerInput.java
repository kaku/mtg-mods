/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.AbstractCharSequence
 *  org.netbeans.lib.editor.util.AbstractCharSequence$StringLike
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.spi.lexer;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.AbstractCharSequence;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;

public final class LexerInput {
    public static final int EOF = -1;
    private LexerInputOperation<?> operation;
    private ReadText readText;
    private int eof;
    static final Logger LOG = Logger.getLogger(LexerInput.class.getName());
    private static boolean loggable;

    LexerInput(LexerInputOperation operation) {
        this.operation = operation;
        loggable = LOG.isLoggable(Level.FINE);
    }

    public int read() {
        int c = this.operation.read();
        if (c == -1) {
            this.eof = 1;
        }
        if (loggable) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("+LexerInput.read(");
            if (c == -1) {
                sb.append("EOF");
            } else {
                sb.append('\'');
                CharSequenceUtilities.debugChar((StringBuilder)sb, (char)((char)c));
                sb.append('\'');
            }
            sb.append(")\n");
            LOG.fine(sb.toString());
        }
        return c;
    }

    public void backup(int count) {
        if (count < 0) {
            throw new IndexOutOfBoundsException("count=" + count + " <0");
        }
        LexerUtilsConstants.checkValidBackup(count, this.readLengthEOF());
        if (this.eof != 0) {
            this.eof = 0;
            --count;
            if (loggable) {
                LOG.fine("-LexerInput.backup(EOF)\n");
            }
        }
        if (loggable && count > 0) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("-LexerInput.backup(").append(count);
            sb.append(")\n");
            LOG.fine(sb.toString());
        }
        this.operation.backup(count);
    }

    public int readLength() {
        return this.operation.readLength();
    }

    public int readLengthEOF() {
        return this.readLength() + this.eof;
    }

    public CharSequence readText(int start, int end) {
        assert (start >= 0 && end >= start && end <= this.readLength());
        if (this.readText == null) {
            this.readText = new ReadText();
        }
        this.readText.reinit(start, end);
        return this.readText;
    }

    public CharSequence readText() {
        return this.readText(0, this.readLength());
    }

    public boolean consumeNewline() {
        if (this.read() == 10) {
            return true;
        }
        this.backup(1);
        return false;
    }

    private final class ReadText
    extends AbstractCharSequence.StringLike {
        private int start;
        private int length;

        private ReadText() {
        }

        private void reinit(int start, int end) {
            this.start = start;
            this.length = end - start;
        }

        public int length() {
            return this.length;
        }

        public char charAt(int index) {
            if (index < 0 || index >= this.length) {
                throw new IndexOutOfBoundsException("index=" + index + ", length=" + this.length);
            }
            return LexerInput.this.operation.readExistingAtIndex(this.start + index);
        }
    }

}

