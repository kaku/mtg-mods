/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.LanguageEmbedding;

public abstract class LanguageProvider {
    public static final String PROP_LANGUAGE = "LanguageProvider.PROP_LANGUAGE";
    public static final String PROP_EMBEDDED_LANGUAGE = "LanguageProvider.PROP_EMBEDDED_LANGUAGE";
    private final PropertyChangeSupport pcs;

    public abstract Language<?> findLanguage(String var1);

    public abstract LanguageEmbedding<?> findLanguageEmbedding(Token<?> var1, LanguagePath var2, InputAttributes var3);

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName) {
        this.pcs.firePropertyChange(propertyName, null, null);
    }

    protected LanguageProvider() {
        this.pcs = new PropertyChangeSupport(this);
    }
}

