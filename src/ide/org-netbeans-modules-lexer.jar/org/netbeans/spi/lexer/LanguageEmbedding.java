/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;

public final class LanguageEmbedding<T extends TokenId> {
    private final Language<T> language;
    private final int startSkipLength;
    private final int endSkipLength;
    private final boolean joinSections;

    public static <T extends TokenId> LanguageEmbedding<T> create(Language<T> language, int startSkipLength, int endSkipLength) {
        return LanguageEmbedding.create(language, startSkipLength, endSkipLength, false);
    }

    public static <T extends TokenId> LanguageEmbedding<T> create(Language<T> language, int startSkipLength, int endSkipLength, boolean joinSections) {
        if (language == null) {
            throw new IllegalArgumentException("language may not be null");
        }
        if (startSkipLength < 0) {
            throw new IllegalArgumentException("startSkipLength=" + startSkipLength + " < 0");
        }
        if (endSkipLength < 0) {
            throw new IllegalArgumentException("endSkipLength=" + endSkipLength + " < 0");
        }
        LanguageOperation<T> op = LexerApiPackageAccessor.get().languageOperation(language);
        return op.getEmbedding(startSkipLength, endSkipLength, joinSections);
    }

    LanguageEmbedding(Language<T> language, int startSkipLength, int endSkipLength, boolean joinSections) {
        assert (language != null);
        assert (startSkipLength >= 0 && endSkipLength >= 0);
        this.language = language;
        this.startSkipLength = startSkipLength;
        this.endSkipLength = endSkipLength;
        this.joinSections = joinSections;
    }

    public Language<T> language() {
        return this.language;
    }

    public int startSkipLength() {
        return this.startSkipLength;
    }

    public int endSkipLength() {
        return this.endSkipLength;
    }

    public boolean joinSections() {
        return this.joinSections;
    }

    public String toString() {
        return "language: " + this.language() + ", skip[" + this.startSkipLength() + ", " + this.endSkipLength + "];" + (this.joinSections ? "join" : "no-join");
    }
}

