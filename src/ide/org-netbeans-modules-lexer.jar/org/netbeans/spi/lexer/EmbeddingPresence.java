/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

public enum EmbeddingPresence {
    CACHED_FIRST_QUERY,
    ALWAYS_QUERY,
    NONE;
    

    private EmbeddingPresence() {
    }
}

