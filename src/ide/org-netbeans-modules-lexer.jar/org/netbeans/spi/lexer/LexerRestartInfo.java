/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.TokenFactory;

public final class LexerRestartInfo<T extends TokenId> {
    private final LexerInput input;
    private final TokenFactory<T> tokenFactory;
    private final Object state;
    private final LanguagePath languagePath;
    private final InputAttributes inputAttributes;

    LexerRestartInfo(LexerInput input, TokenFactory<T> tokenFactory, Object state, LanguagePath languagePath, InputAttributes inputAttributes) {
        this.input = input;
        this.tokenFactory = tokenFactory;
        this.state = state;
        this.languagePath = languagePath;
        this.inputAttributes = inputAttributes;
    }

    public LexerInput input() {
        return this.input;
    }

    public TokenFactory<T> tokenFactory() {
        return this.tokenFactory;
    }

    public Object state() {
        return this.state;
    }

    public LanguagePath languagePath() {
        return this.languagePath;
    }

    public InputAttributes inputAttributes() {
        return this.inputAttributes;
    }

    public Object getAttributeValue(Object key) {
        return this.inputAttributes != null ? this.inputAttributes.getValue(this.languagePath, key) : null;
    }
}

