/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.spi.lexer.TokenHierarchyControl;

public abstract class MutableTextInput<I> {
    private TokenHierarchyControl<I> thc;

    protected abstract Language<?> language();

    protected abstract CharSequence text();

    protected abstract InputAttributes inputAttributes();

    protected abstract I inputSource();

    protected abstract boolean isReadLocked();

    protected abstract boolean isWriteLocked();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final TokenHierarchyControl<I> tokenHierarchyControl() {
        MutableTextInput mutableTextInput = this;
        synchronized (mutableTextInput) {
            if (this.thc == null) {
                this.thc = new TokenHierarchyControl<I>(this);
            }
            return this.thc;
        }
    }
}

