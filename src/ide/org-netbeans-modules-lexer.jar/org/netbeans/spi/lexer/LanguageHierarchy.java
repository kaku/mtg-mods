/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import java.util.Collection;
import java.util.Map;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.TokenIdImpl;
import org.netbeans.spi.lexer.EmbeddingPresence;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenFactory;
import org.netbeans.spi.lexer.TokenValidator;

public abstract class LanguageHierarchy<T extends TokenId> {
    private Language<T> language;

    public static TokenId newId(String name, int ordinal) {
        return LanguageHierarchy.newId(name, ordinal, null);
    }

    public static TokenId newId(String name, int ordinal, String primaryCategory) {
        return new TokenIdImpl(name, ordinal, primaryCategory);
    }

    protected abstract Collection<T> createTokenIds();

    protected Map<String, Collection<T>> createTokenCategories() {
        return null;
    }

    protected abstract Lexer<T> createLexer(LexerRestartInfo<T> var1);

    protected abstract String mimeType();

    protected LanguageEmbedding<?> embedding(Token<T> token, LanguagePath languagePath, InputAttributes inputAttributes) {
        return null;
    }

    protected EmbeddingPresence embeddingPresence(T id) {
        return EmbeddingPresence.CACHED_FIRST_QUERY;
    }

    protected TokenValidator<T> createTokenValidator(T tokenId) {
        return null;
    }

    protected boolean isRetainTokenText(T tokenId) {
        return false;
    }

    public final synchronized Language<T> language() {
        if (this.language == null) {
            this.language = LexerApiPackageAccessor.get().createLanguage(this);
        }
        return this.language;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public final boolean equals(Object o) {
        return super.equals(o);
    }

    public String toString() {
        return this.getClass().getName();
    }

    static {
        LexerSpiPackageAccessor.register(new Accessor());
    }

    private static final class Accessor
    extends LexerSpiPackageAccessor {
        private Accessor() {
        }

        @Override
        public <T extends TokenId> Collection<T> createTokenIds(LanguageHierarchy<T> languageHierarchy) {
            return languageHierarchy.createTokenIds();
        }

        @Override
        public <T extends TokenId> Map<String, Collection<T>> createTokenCategories(LanguageHierarchy<T> languageHierarchy) {
            return languageHierarchy.createTokenCategories();
        }

        @Override
        public String mimeType(LanguageHierarchy<?> languageHierarchy) {
            return languageHierarchy.mimeType();
        }

        @Override
        public <T extends TokenId> LanguageEmbedding<?> embedding(LanguageHierarchy<T> languageHierarchy, Token<T> token, LanguagePath languagePath, InputAttributes inputAttributes) {
            return languageHierarchy.embedding(token, languagePath, inputAttributes);
        }

        @Override
        public <T extends TokenId> EmbeddingPresence embeddingPresence(LanguageHierarchy<T> languageHierarchy, T id) {
            return languageHierarchy.embeddingPresence(id);
        }

        @Override
        public <T extends TokenId> Lexer<T> createLexer(LanguageHierarchy<T> languageHierarchy, LexerRestartInfo<T> info) {
            Lexer<T> lexer = languageHierarchy.createLexer(info);
            if (lexer == null) {
                throw new IllegalStateException("Null from createLexer() for languageHierarchy=" + languageHierarchy);
            }
            return lexer;
        }

        @Override
        public <T extends TokenId> LexerRestartInfo<T> createLexerRestartInfo(LexerInput input, TokenFactory<T> tokenFactory, Object state, LanguagePath languagePath, InputAttributes inputAttributes) {
            return new LexerRestartInfo<T>(input, tokenFactory, state, languagePath, inputAttributes);
        }

        @Override
        public <T extends TokenId> TokenValidator<T> createTokenValidator(LanguageHierarchy<T> languageHierarchy, T id) {
            return languageHierarchy.createTokenValidator(id);
        }

        @Override
        public <T extends TokenId> boolean isRetainTokenText(LanguageHierarchy<T> languageHierarchy, T id) {
            return languageHierarchy.isRetainTokenText(id);
        }

        @Override
        public LexerInput createLexerInput(LexerInputOperation<?> operation) {
            return new LexerInput(operation);
        }

        @Override
        public Language<?> language(MutableTextInput<?> mti) {
            return mti.language();
        }

        @Override
        public <T extends TokenId> LanguageEmbedding<T> createLanguageEmbedding(Language<T> language, int startSkipLength, int endSkipLength, boolean joinSections) {
            return new LanguageEmbedding<T>(language, startSkipLength, endSkipLength, joinSections);
        }

        @Override
        public CharSequence text(MutableTextInput<?> mti) {
            return mti.text();
        }

        @Override
        public InputAttributes inputAttributes(MutableTextInput<?> mti) {
            return mti.inputAttributes();
        }

        @Override
        public <I> I inputSource(MutableTextInput<I> mti) {
            return mti.inputSource();
        }

        @Override
        public boolean isReadLocked(MutableTextInput<?> mti) {
            return mti.isReadLocked();
        }

        @Override
        public boolean isWriteLocked(MutableTextInput<?> mti) {
            return mti.isWriteLocked();
        }

        @Override
        public <T extends TokenId> TokenFactory<T> createTokenFactory(LexerInputOperation<T> lexerInputOperation) {
            return new TokenFactory<T>(lexerInputOperation);
        }
    }

}

