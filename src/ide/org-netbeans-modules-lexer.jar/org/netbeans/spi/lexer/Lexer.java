/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;

public interface Lexer<T extends TokenId> {
    public Token<T> nextToken();

    public Object state();

    public void release();
}

