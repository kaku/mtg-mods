/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;

public interface TokenPropertyProvider<T extends TokenId> {
    public Object getValue(Token<T> var1, Object var2);
}

