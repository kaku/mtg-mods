/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public final class TokenFactory<T extends TokenId> {
    public static final Token SKIP_TOKEN = LexerUtilsConstants.SKIP_TOKEN;
    private final LexerInputOperation<T> operation;

    TokenFactory(LexerInputOperation<T> operation) {
        this.operation = operation;
    }

    public Token<T> createToken(T id) {
        return this.createToken(id, this.operation.readLength());
    }

    public Token<T> createToken(T id, int length) {
        return this.operation.createToken(id, length);
    }

    public Token<T> createToken(T id, int length, PartType partType) {
        return this.operation.createToken(id, length, partType);
    }

    public Token<T> getFlyweightToken(T id, String text) {
        return this.operation.getFlyweightToken(id, text);
    }

    public Token<T> createPropertyToken(T id, int length, TokenPropertyProvider<T> propertyProvider) {
        return this.operation.createPropertyToken(id, length, propertyProvider, PartType.COMPLETE);
    }

    public Token<T> createPropertyToken(T id, int length, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        return this.operation.createPropertyToken(id, length, propertyProvider, partType);
    }

    public Token<T> createCustomTextToken(T id, CharSequence text, int length, PartType partType) {
        if (partType != null) {
            throw new IllegalArgumentException("This method is deprecated and it should only be used with partType==null (see its javadoc).");
        }
        return this.operation.createCustomTextToken(id, length, text);
    }

    public boolean isSkipToken(Token<T> token) {
        return token == SKIP_TOKEN;
    }
}

