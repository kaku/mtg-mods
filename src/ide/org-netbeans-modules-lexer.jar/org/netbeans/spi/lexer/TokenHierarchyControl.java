/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.spi.lexer.MutableTextInput;

public final class TokenHierarchyControl<I> {
    private TokenHierarchyOperation<I, ?> operation;

    TokenHierarchyControl(MutableTextInput<I> input) {
        this.operation = new TokenHierarchyOperation(input);
    }

    public TokenHierarchy<I> tokenHierarchy() {
        return this.operation.tokenHierarchy();
    }

    public void textModified(int offset, int removedLength, CharSequence removedText, int insertedLength) {
        this.operation.textModified(offset, removedLength, removedText, insertedLength);
    }

    public void setActive(boolean active) {
        this.operation.setActive(active);
    }

    public boolean isActive() {
        return this.operation.isActive();
    }

    public void rebuild() {
        this.operation.rebuild();
    }
}

