/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.lexer;

import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.TokenFactory;

public interface TokenValidator<T extends TokenId> {
    public Token<T> validateToken(Token<T> var1, TokenFactory<T> var2, CharSequence var3, int var4, int var5, CharSequence var6, int var7, CharSequence var8);
}

