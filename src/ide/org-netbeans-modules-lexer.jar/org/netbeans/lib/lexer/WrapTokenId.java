/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LanguageIds;

public final class WrapTokenId<T extends TokenId> {
    private final T id;
    private final LanguageIds languageIds;

    public WrapTokenId(T id) {
        this(id, LanguageIds.EMPTY);
    }

    public WrapTokenId(T id, LanguageIds languageIds) {
        this.id = id;
        this.languageIds = languageIds;
    }

    public T id() {
        return this.id;
    }

    public LanguageIds languageIds() {
        return this.languageIds;
    }
}

