/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.lib.lexer;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.WrapTokenIdCache;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.CustomTextToken;
import org.netbeans.lib.lexer.token.DefaultToken;
import org.netbeans.lib.lexer.token.PropertyToken;
import org.netbeans.lib.lexer.token.TextToken;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public abstract class LexerInputOperation<T extends TokenId> {
    static final Logger LOG = Logger.getLogger(LexerInputOperation.class.getName());
    static final Logger LexerInputLOG = Logger.getLogger(LexerInput.class.getName());
    protected final TokenList<T> tokenList;
    protected int readOffset;
    protected int tokenStartOffset;
    private int lookaheadOffset;
    protected int tokenLength;
    protected Lexer<T> lexer;
    protected final LanguageOperation<T> languageOperation;
    private int flyTokenSequenceLength;
    protected final WrapTokenIdCache<T> wrapTokenIdCache;

    public LexerInputOperation(TokenList<T> tokenList, int tokenIndex, Object lexerRestartState) {
        this.tokenList = tokenList;
        LanguagePath languagePath = tokenList.languagePath();
        Language<T> language = tokenList.language();
        this.languageOperation = LexerUtilsConstants.languageOperation(language);
        while (--tokenIndex >= 0 && tokenList.tokenOrEmbedding(tokenIndex).token().isFlyweight()) {
            ++this.flyTokenSequenceLength;
        }
        LanguageHierarchy languageHierarchy = LexerApiPackageAccessor.get().languageHierarchy(LexerUtilsConstants.innerLanguage(languagePath));
        TokenFactory tokenFactory = LexerSpiPackageAccessor.get().createTokenFactory(this);
        LexerInput lexerInput = LexerSpiPackageAccessor.get().createLexerInput(this);
        LexerRestartInfo info = LexerSpiPackageAccessor.get().createLexerRestartInfo(lexerInput, tokenFactory, lexerRestartState, languagePath, tokenList.inputAttributes());
        this.lexer = LexerSpiPackageAccessor.get().createLexer(languageHierarchy, info);
        this.wrapTokenIdCache = tokenList.tokenHierarchyOperation().getWrapTokenIdCache(language);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.INFO, "LexerInputOperation created for " + tokenList.tokenHierarchyOperation().inputSource(), new Exception());
        }
    }

    public abstract int read(int var1);

    public abstract char readExisting(int var1);

    protected abstract void fillTokenData(AbstractToken<T> var1);

    public final int read() {
        int c;
        if ((c = this.read(this.readOffset++)) == -1) {
            this.lookaheadOffset = this.readOffset--;
        }
        return c;
    }

    public final int readLength() {
        return this.readOffset - this.tokenStartOffset;
    }

    public final char readExistingAtIndex(int index) {
        return this.readExisting(this.tokenStartOffset + index);
    }

    public final void backup(int count) {
        if (this.lookaheadOffset < this.readOffset) {
            this.lookaheadOffset = this.readOffset;
        }
        this.readOffset -= count;
    }

    public final int lookahead() {
        return Math.max(this.lookaheadOffset, this.readOffset) - this.tokenStartOffset;
    }

    public AbstractToken<T> nextToken() {
        AbstractToken token;
        if (this.lexer == null) {
            return null;
        }
        do {
            if ((token = (AbstractToken)this.lexer.nextToken()) == null) {
                this.checkLexerInputFinished();
                return null;
            }
            Language<T> language = this.languageOperation.language();
            if (!this.isSkipToken(token) && !language.tokenIds().contains(token.id())) {
                String msgPrefix = "Invalid TokenId=" + token.id() + " returned from lexer=" + this.lexer + " for language=" + language + ":\n";
                if (token.id().ordinal() > language.maxOrdinal()) {
                    throw new IllegalStateException(msgPrefix + "Language.maxOrdinal()=" + language.maxOrdinal() + " < " + token.id().ordinal());
                }
                throw new IllegalStateException(msgPrefix + "Language contains no or different tokenId with ordinal=" + token.id().ordinal() + ": " + language.tokenId(token.id().ordinal()));
            }
            this.tokenStartOffset += this.tokenLength;
        } while (this.isSkipToken(token));
        return token;
    }

    public int lastTokenEndOffset() {
        return this.tokenStartOffset;
    }

    public AbstractToken<T> getFlyweightToken(T id, String text) {
        if (text.length() > this.readLength()) {
            throw new IllegalArgumentException("getFlyweightToken(): Creating token  for unread characters: text=\"" + CharSequenceUtilities.debugText((CharSequence)text) + "\"; text.length()=" + text.length() + " > readLength()=" + this.readLength());
        }
        if (LOG.isLoggable(Level.FINE)) {
            for (int i = 0; i < text.length(); ++i) {
                if (text.charAt(i) == this.readExistingAtIndex(i)) continue;
                throw new IllegalArgumentException("Flyweight text in TokenFactory.getFlyweightToken(" + id + ", \"" + CharSequenceUtilities.debugText((CharSequence)text) + "\") " + "differs from recognized text: '" + CharSequenceUtilities.debugChar((char)this.readExisting(i)) + "' != '" + CharSequenceUtilities.debugChar((char)text.charAt(i)) + "' at index=" + i);
            }
        }
        this.logTokenContent("getFlyweightToken", id, text.length());
        this.assignTokenLength(text.length());
        AbstractToken<T> token = this.checkSkipToken(id);
        if (token == null) {
            if (this.isFlyTokenAllowed()) {
                token = this.languageOperation.getFlyweightToken(this.wrapTokenIdCache.plainWid(id), text);
                ++this.flyTokenSequenceLength;
            } else {
                token = this.createDefaultTokenInstance(id);
                this.fillTokenData(token);
                this.flyTokenSequenceLength = 0;
            }
        }
        return token;
    }

    private AbstractToken<T> checkSkipToken(T id) {
        if (this.isSkipTokenId(id)) {
            this.flyTokenSequenceLength = 5;
            return this.skipToken();
        }
        return null;
    }

    private void checkTokenIdNonNull(T id) {
        if (id == null) {
            throw new IllegalArgumentException("Token id must not be null. Fix lexer " + this.lexer);
        }
    }

    public AbstractToken<T> createToken(T id, int length) {
        this.checkTokenIdNonNull(id);
        this.logTokenContent("createToken", id, length);
        this.assignTokenLength(length);
        AbstractToken<T> token = this.checkSkipToken(id);
        if (token == null) {
            token = this.createDefaultTokenInstance(id);
            this.fillTokenData(token);
            this.flyTokenSequenceLength = 0;
        }
        return token;
    }

    private void logTokenContent(String opName, T id, int length) {
        if (LexerInputLOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("TokenFactory.").append(opName).append("(");
            sb.append(id).append(", ").append(length);
            sb.append("): \"");
            for (int i = 0; i < length; ++i) {
                CharSequenceUtilities.debugChar((StringBuilder)sb, (char)this.readExistingAtIndex(i));
            }
            sb.append("\"\n");
            LexerInputLOG.fine(sb.toString());
        }
    }

    protected AbstractToken<T> createDefaultTokenInstance(T id) {
        return new DefaultToken<T>(this.wrapTokenIdCache.plainWid(id), this.tokenLength);
    }

    public AbstractToken<T> createToken(T id, int length, PartType partType) {
        if (partType == null) {
            throw new IllegalArgumentException("partType must be non-null");
        }
        if (partType == PartType.COMPLETE) {
            return this.createToken(id, length);
        }
        this.checkTokenIdNonNull(id);
        return this.createPropertyToken(id, length, null, partType);
    }

    public AbstractToken<T> createPropertyToken(T id, int length, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        if (partType == null) {
            partType = PartType.COMPLETE;
        }
        this.logTokenContent("createPropertyToken", id, length);
        this.assignTokenLength(length);
        AbstractToken<T> token = this.checkSkipToken(id);
        if (token == null) {
            token = this.createPropertyTokenInstance(id, propertyProvider, partType);
            this.fillTokenData(token);
            this.flyTokenSequenceLength = 0;
        }
        return token;
    }

    protected AbstractToken<T> createPropertyTokenInstance(T id, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        return new PropertyToken<T>(this.wrapTokenIdCache.plainWid(id), this.tokenLength, propertyProvider, partType);
    }

    public AbstractToken<T> createCustomTextToken(T id, int length, CharSequence customText) {
        this.logTokenContent("createCustomTextToken", id, length);
        this.assignTokenLength(length);
        AbstractToken<T> token = this.checkSkipToken(id);
        if (token == null) {
            token = this.createCustomTextTokenInstance(id, customText);
            this.fillTokenData(token);
            this.flyTokenSequenceLength = 0;
        }
        return token;
    }

    protected AbstractToken<T> createCustomTextTokenInstance(T id, CharSequence customText) {
        return new CustomTextToken<T>(this.wrapTokenIdCache.plainWid(id), customText, this.tokenLength);
    }

    public boolean isSkipTokenId(T id) {
        Set<T> skipTokenIds = this.tokenList.skipTokenIds();
        return skipTokenIds != null && skipTokenIds.contains(id);
    }

    protected final int tokenLength() {
        return this.tokenLength;
    }

    public void assignTokenLength(int tokenLength) {
        if (tokenLength > this.readLength()) {
            throw new IndexOutOfBoundsException("tokenLength=" + tokenLength + " > " + this.readLength() + ". Fix the lexer implementation to use proper token length value.");
        }
        if (tokenLength <= 0) {
            throw new IndexOutOfBoundsException("tokenLength=" + tokenLength + " <= 0. Fix the lexer implementation to use proper token length value.");
        }
        this.tokenLength = tokenLength;
    }

    public final Object lexerState() {
        return this.lexer.state();
    }

    protected boolean isFlyTokenAllowed() {
        return this.flyTokenSequenceLength < 5;
    }

    public final boolean isSkipToken(AbstractToken<T> token) {
        return token == LexerUtilsConstants.SKIP_TOKEN;
    }

    public final AbstractToken<T> skipToken() {
        return LexerUtilsConstants.SKIP_TOKEN;
    }

    public final void release() {
        if (this.lexer != null) {
            this.lexer.release();
            this.lexer = null;
        }
    }

    private void checkLexerInputFinished() {
        if (this.read() != -1 || this.readLength() > 0) {
            StringBuilder sb = new StringBuilder(100);
            int readLen = this.readLength();
            sb.append("Lexer ").append(this.lexer);
            sb.append("\n  returned null token but lexerInput.readLength()=");
            sb.append(readLen);
            sb.append("\n  lexer-state: ").append(this.lexer.state());
            sb.append("\n  ").append(this);
            sb.append("\n  Chars: \"");
            for (int i = 0; i < readLen; ++i) {
                sb.append(CharSequenceUtilities.debugChar((char)this.readExistingAtIndex(i)));
            }
            sb.append("\" - these characters need to be tokenized.");
            sb.append("\nFix the lexer to not return null token in this state.");
            throw new IllegalStateException(sb.toString());
        }
    }

    public String toString() {
        return "tokenStartOffset=" + this.tokenStartOffset + ", readOffset=" + this.readOffset + ", lookaheadOffset=" + this.lookaheadOffset;
    }
}

