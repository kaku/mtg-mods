/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.CharSubSequence
 */
package org.netbeans.lib.lexer;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.CharSubSequence;
import org.netbeans.lib.lexer.TokenList;

public abstract class PreprocessedTextStorage
implements CharSequence {
    private final CharSequence rawText;
    private final char[] preprocessedChars;
    private final int preprocessedStartIndex;
    private final int totalRawLengthShift;
    private final int length;

    public static PreprocessedTextStorage create(CharSequence rawText, char[] preprocessedChars, int preprocessedCharsLength, int preprocessedStartIndex, int[] preprocessedRawLengthShifts) {
        PreprocessedTextStorage storage;
        char[] preprocessedCharsCopy = ArrayUtilities.charArray((char[])preprocessedChars, (int)preprocessedCharsLength);
        int totalRawLengthShift = preprocessedRawLengthShifts[preprocessedCharsLength - 1];
        if (totalRawLengthShift <= 127) {
            byte[] arr = new byte[preprocessedCharsLength];
            for (int i = preprocessedCharsLength - 1; i >= 0; --i) {
                arr[i] = (byte)preprocessedRawLengthShifts[i];
            }
            storage = new ByteRawIndex(rawText, preprocessedCharsCopy, preprocessedStartIndex, preprocessedCharsLength, totalRawLengthShift, arr);
        } else if (totalRawLengthShift <= 32767) {
            short[] arr = new short[preprocessedCharsLength];
            for (int i = preprocessedCharsLength - 1; i >= 0; --i) {
                arr[i] = (short)preprocessedRawLengthShifts[i];
            }
            storage = new ShortRawIndex(rawText, preprocessedCharsCopy, preprocessedStartIndex, preprocessedCharsLength, totalRawLengthShift, arr);
        } else {
            int[] arr = new int[preprocessedCharsLength];
            System.arraycopy(preprocessedChars, 0, arr, 0, preprocessedCharsLength);
            storage = new IntRawIndex(rawText, preprocessedCharsCopy, preprocessedStartIndex, preprocessedCharsLength, totalRawLengthShift, arr);
        }
        if (TokenList.LOG.isLoggable(Level.FINE)) {
            storage.consistencyCheck();
        }
        return storage;
    }

    public static PreprocessedTextStorage create(CharSequence rawText, char[] preprocessedChars, int preprocessedCharsLength, int preprocessedStartIndex, int[] preprocessedRawLengthShifts, char[] extraPreprocessedChars, int[] extraRawLengthShifts, int preStartIndex, int postEndIndex) {
        PreprocessedTextStorage storage;
        int extraPreCharsLength = extraPreprocessedChars.length - preStartIndex;
        preprocessedStartIndex -= extraPreCharsLength;
        int length = extraPreCharsLength + preprocessedCharsLength + postEndIndex;
        char[] preprocessedCharsCopy = new char[length];
        System.arraycopy(extraPreprocessedChars, preStartIndex, preprocessedCharsCopy, 0, extraPreCharsLength);
        System.arraycopy(preprocessedChars, 0, preprocessedCharsCopy, extraPreCharsLength, preprocessedCharsLength);
        System.arraycopy(extraPreprocessedChars, 0, preprocessedCharsCopy, extraPreCharsLength + preprocessedCharsLength, postEndIndex);
        int totalRawLengthShift = postEndIndex > 0 ? extraRawLengthShifts[postEndIndex - 1] : (preprocessedCharsLength > 0 ? preprocessedRawLengthShifts[preprocessedCharsLength - 1] : extraRawLengthShifts[extraPreprocessedChars.length - 1]);
        int ind = length - 1;
        if (totalRawLengthShift <= 127) {
            int i;
            byte[] arr = new byte[length];
            for (i = postEndIndex - 1; i >= 0; --i) {
                arr[ind--] = (byte)extraRawLengthShifts[i];
            }
            for (i = preprocessedCharsLength - 1; i >= 0; --i) {
                arr[ind--] = (byte)preprocessedRawLengthShifts[i];
            }
            for (i = extraPreprocessedChars.length - 1; i >= preStartIndex; --i) {
                arr[ind--] = (byte)extraRawLengthShifts[i];
            }
            storage = new ByteRawIndex(rawText, preprocessedCharsCopy, preprocessedStartIndex, length, totalRawLengthShift, arr);
        } else if (totalRawLengthShift <= 32767) {
            int i;
            short[] arr = new short[length];
            for (i = postEndIndex - 1; i >= 0; --i) {
                arr[ind--] = (short)extraRawLengthShifts[i];
            }
            for (i = preprocessedCharsLength - 1; i >= 0; --i) {
                arr[ind--] = (short)preprocessedRawLengthShifts[i];
            }
            for (i = extraPreprocessedChars.length - 1; i >= preStartIndex; --i) {
                arr[ind--] = (short)extraRawLengthShifts[i];
            }
            storage = new ShortRawIndex(rawText, preprocessedCharsCopy, preprocessedStartIndex, length, totalRawLengthShift, arr);
        } else {
            int i;
            int[] arr = new int[length];
            for (i = postEndIndex - 1; i >= 0; --i) {
                arr[ind--] = extraRawLengthShifts[i];
            }
            for (i = preprocessedCharsLength - 1; i >= 0; --i) {
                arr[ind--] = preprocessedRawLengthShifts[i];
            }
            for (i = extraPreprocessedChars.length - 1; i >= preStartIndex; --i) {
                arr[ind--] = extraRawLengthShifts[i];
            }
            storage = new IntRawIndex(rawText, preprocessedCharsCopy, preprocessedStartIndex, length, totalRawLengthShift, arr);
        }
        if (TokenList.LOG.isLoggable(Level.FINE)) {
            storage.consistencyCheck();
        }
        return storage;
    }

    protected PreprocessedTextStorage(CharSequence rawText, char[] preprocessedChars, int preprocessedStartIndex, int length, int totalRawLengthShift) {
        this.rawText = rawText;
        this.preprocessedChars = preprocessedChars;
        this.preprocessedStartIndex = preprocessedStartIndex;
        this.totalRawLengthShift = totalRawLengthShift;
        this.length = length;
    }

    protected abstract int prepRawLengthShift(int var1);

    public final int rawLength(int length) {
        if (length > this.preprocessedStartIndex) {
            int prepLength = length - this.preprocessedStartIndex;
            length = prepLength <= this.preprocessedChars.length ? (length += this.prepRawLengthShift(prepLength - 1)) : (length += this.totalRawLengthShift);
        }
        return length;
    }

    public final int rawLengthShift(int index) {
        if (index < this.preprocessedStartIndex) {
            return index;
        }
        if ((index -= this.preprocessedStartIndex) <= this.preprocessedChars.length) {
            return this.prepRawLengthShift(index);
        }
        return this.totalRawLengthShift;
    }

    @Override
    public final char charAt(int index) {
        CharSequenceUtilities.checkIndexValid((int)index, (int)this.length);
        if (index < this.preprocessedStartIndex) {
            return this.rawText.charAt(index);
        }
        int prepIndex = index - this.preprocessedStartIndex;
        if (prepIndex < this.preprocessedChars.length) {
            return this.preprocessedChars[prepIndex];
        }
        return this.rawText.charAt(index + this.totalRawLengthShift);
    }

    @Override
    public final CharSequence subSequence(int start, int end) {
        return new CharSubSequence((CharSequence)this, start, end);
    }

    @Override
    public final int length() {
        return this.length;
    }

    private void consistencyCheck() {
        int lastRLS = 0;
        for (int i = 0; i < this.preprocessedChars.length; ++i) {
            int rls = this.prepRawLengthShift(i);
            if (rls < lastRLS) {
                throw new IllegalStateException("rls=" + rls + " < lastRLS=" + lastRLS + " at index=" + i);
            }
            lastRLS = rls;
        }
    }

    private static final class IntRawIndex
    extends PreprocessedTextStorage {
        private final int[] preprocessedRawLengthShifts;

        IntRawIndex(CharSequence rawText, char[] preprocessedChars, int preprocessedStartIndex, int length, int totalRawLengthShift, int[] preprocessedRawLengthShifts) {
            super(rawText, preprocessedChars, preprocessedStartIndex, length, totalRawLengthShift);
            this.preprocessedRawLengthShifts = preprocessedRawLengthShifts;
        }

        @Override
        protected final int prepRawLengthShift(int index) {
            return this.preprocessedRawLengthShifts[index];
        }
    }

    private static final class ShortRawIndex
    extends PreprocessedTextStorage {
        private final short[] preprocessedRawLengthShifts;

        ShortRawIndex(CharSequence rawText, char[] preprocessedChars, int preprocessedStartIndex, int length, int totalRawLengthShift, short[] preprocessedRawLengthShifts) {
            super(rawText, preprocessedChars, preprocessedStartIndex, length, totalRawLengthShift);
            this.preprocessedRawLengthShifts = preprocessedRawLengthShifts;
        }

        @Override
        protected final int prepRawLengthShift(int index) {
            return this.preprocessedRawLengthShifts[index];
        }
    }

    private static final class ByteRawIndex
    extends PreprocessedTextStorage {
        private final byte[] preprocessedRawLengthShifts;

        ByteRawIndex(CharSequence rawText, char[] preprocessedChars, int preprocessedStartIndex, int length, int totalRawLengthShift, byte[] preprocessedRawLengthShifts) {
            super(rawText, preprocessedChars, preprocessedStartIndex, length, totalRawLengthShift);
            this.preprocessedRawLengthShifts = preprocessedRawLengthShifts;
        }

        @Override
        protected final int prepRawLengthShift(int index) {
            return this.preprocessedRawLengthShifts[index];
        }
    }

}

