/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.Language;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;

public final class LanguageIds {
    private static final LanguageIds[] EMPTY_ARR = new LanguageIds[0];
    public static final LanguageIds EMPTY = new LanguageIds(EMPTY_ARR, 0);
    public static final LanguageIds NULL_LANGUAGE_ONLY = LanguageIds.get(0);
    private final LanguageIds[] arr;
    private final int hash;
    private LanguageIds[] ext = EMPTY_ARR;

    public static LanguageIds get(Language<?> language) {
        return LanguageIds.getImpl(LanguageIds.id(language));
    }

    public static LanguageIds get(int languageId) {
        LanguageIds.checkLanguageId(languageId);
        return LanguageIds.getImpl(languageId);
    }

    private static LanguageIds getImpl(int id) {
        return EMPTY.getExtended(id);
    }

    public static LanguageIds get(LanguageIds ids, Language<?> language) {
        return LanguageIds.getImpl(ids, LanguageIds.id(language));
    }

    public static LanguageIds get(LanguageIds ids, int languageId) {
        LanguageIds.checkLanguageId(languageId);
        return LanguageIds.getImpl(ids, languageId);
    }

    private static LanguageIds getImpl(LanguageIds ids, int id) {
        return ids.getExtended(id);
    }

    public static LanguageIds getRemoved(LanguageIds ids, Language<?> removedLanguage) {
        return LanguageIds.getRemovedImpl(ids, LanguageIds.id(removedLanguage));
    }

    public static LanguageIds getRemoved(LanguageIds ids, int removedLanguageId) {
        LanguageIds.checkLanguageId(removedLanguageId);
        return LanguageIds.getRemovedImpl(ids, removedLanguageId);
    }

    private static LanguageIds getRemovedImpl(LanguageIds ids, int removedId) {
        return ids.getRemoved(removedId);
    }

    private static void checkLanguageId(int languageId) {
        if (languageId < 0) {
            throw new IllegalArgumentException("Invalid id=" + languageId + " < 0");
        }
    }

    private static LanguageIds create(int id) {
        LanguageIds[] ids = new LanguageIds[id + 1];
        ids[id] = EMPTY;
        return new LanguageIds(ids, id);
    }

    private static LanguageIds create(LanguageIds ids, int id) {
        LanguageIds[] arr = new LanguageIds[id + 1];
        LanguageIds[] parentArr = ids.arr;
        System.arraycopy(parentArr, 0, arr, 0, parentArr.length);
        arr[id] = ids;
        int hash = ids.hashCode() << 5 ^ id;
        return new LanguageIds(arr, hash);
    }

    static int id(Language language) {
        return language != null ? LexerApiPackageAccessor.get().languageId(language) : 0;
    }

    private LanguageIds(LanguageIds[] arr, int hash) {
        this.arr = arr;
        this.hash = hash;
    }

    public int hashCode() {
        return this.hash;
    }

    public boolean equals(Object o) {
        return o == this;
    }

    public boolean containsLanguage(Language<?> language) {
        return this.containsId(LanguageIds.id(language));
    }

    public boolean containsId(int id) {
        return id < this.arr.length && this.arr[id] != null;
    }

    public int maxId() {
        return this.arr.length - 1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private LanguageIds getExtended(int id) {
        LanguageIds ids;
        if (id < this.arr.length) {
            if (this.arr[id] != null) {
                ids = this;
            } else {
                int i = id + 1;
                while ((ids = this.arr[i]) == null) {
                    ++i;
                }
                ids = ids.getExtended(id);
                while (i < this.arr.length) {
                    if (this.arr[i] != null) {
                        ids = ids.getExtended(i);
                    }
                    ++i;
                }
            }
        } else {
            int extIndex = id - this.arr.length;
            LanguageIds languageIds = this;
            synchronized (languageIds) {
                if (extIndex >= this.ext.length) {
                    LanguageIds[] n = new LanguageIds[extIndex + 1];
                    System.arraycopy(this.ext, 0, n, 0, this.ext.length);
                    this.ext = n;
                }
                if ((ids = this.ext[extIndex]) == null) {
                    this.ext[extIndex] = ids = LanguageIds.create(this, id);
                }
            }
        }
        return ids;
    }

    private LanguageIds getRemoved(int removedId) {
        LanguageIds ids;
        if (removedId < this.arr.length) {
            ids = this.arr[removedId];
            if (ids == null) {
                ids = this;
            } else {
                for (int i = removedId + 1; i < this.arr.length; ++i) {
                    if (this.arr[i] == null) continue;
                    ids = ids.getExtended(i);
                }
            }
        } else {
            ids = this;
        }
        return ids;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        if (this == EMPTY) {
            sb.append("EMPTY");
        } else if (this == NULL_LANGUAGE_ONLY) {
            sb.append("NULL_LANGUAGE_ONLY");
        } else {
            sb.append("LanguageIds");
        }
        sb.append(":\n");
        for (int i = 0; i < this.arr.length; ++i) {
            sb.append("  arr[").append(i).append("]=").append(this.containsId(i)).append('\n');
        }
        sb.append("  hash=").append(this.hash);
        sb.append(", ext.length=").append(this.ext.length);
        return sb.toString();
    }
}

