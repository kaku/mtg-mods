/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.lib.lexer.IntegerCache;

public abstract class LAState {
    private static final LAState EMPTY = new NoState(0);
    int gapStart;
    int gapLength;

    public static LAState empty() {
        return EMPTY;
    }

    static int withExtraCapacity(int capacity) {
        return capacity * 3 / 2 + 4;
    }

    static boolean isByteState(Object state) {
        int intState;
        return state.getClass() == Integer.class && (intState = ((Integer)state).intValue()) >= 0 && intState <= 127;
    }

    public LAState(int capacity) {
        this.gapLength = capacity;
    }

    public abstract int lookahead(int var1);

    public abstract Object state(int var1);

    public final LAState trimToSize() {
        if (this.gapLength > 0) {
            LAState laState = this.upgrade(this.size(), this.getClass());
            this.reallocate(laState, this.size());
            return laState;
        }
        return this;
    }

    public final int size() {
        return this.capacity() - this.gapLength;
    }

    public final LAState add(int lookahead, Object state) {
        LAState ret;
        if (this.gapLength > 0) {
            this.moveGap(this.size());
            ret = this;
        } else {
            ret = this.upgrade(this.capacity() + 1 << 1, this.getClass());
            this.reallocate(ret, this.size());
        }
        Class laStateCls = ret.addToGapStart(lookahead, state);
        if (laStateCls != null) {
            ret = this.upgrade(this.capacity() + 1, laStateCls);
            this.reallocate(ret, this.size());
            ret.addToGapStart(lookahead, state);
        }
        ++ret.gapStart;
        --ret.gapLength;
        return ret;
    }

    public final LAState addAll(int index, LAState laState) {
        LAState ret;
        int laStateSize = laState.size();
        if (!this.isUpgrade(laState.getClass()) && this.gapLength > laStateSize) {
            this.moveGap(index);
            ret = this;
        } else {
            ret = this.upgrade((int)((long)(this.capacity() + laStateSize) * 110 / 100), laState.getClass());
            this.reallocate(ret, index);
        }
        laState.copyData(0, ret, ret.gapStart, laState.gapStart);
        int laStateGapEnd = laState.gapStart + laState.gapLength;
        laState.copyData(laStateGapEnd, ret, ret.gapStart + laState.gapStart, laState.capacity() - laStateGapEnd);
        ret.gapStart += laStateSize;
        ret.gapLength -= laStateSize;
        return ret;
    }

    protected abstract LAState upgrade(int var1, Class var2);

    protected abstract boolean isUpgrade(Class var1);

    protected abstract Class addToGapStart(int var1, Object var2);

    public final void remove(int index, int count) {
        this.moveGap(index + count);
        this.removeUpdate(index, count);
        this.gapStart -= count;
        this.gapLength += count;
    }

    protected void removeUpdate(int index, int count) {
    }

    protected final int rawIndex(int index) {
        return index < this.gapStart ? index : index + this.gapLength;
    }

    final void reallocate(LAState tgt, int newGapStart) {
        tgt.gapStart = newGapStart;
        tgt.gapLength = this.gapLength + tgt.capacity() - this.capacity();
        int gapEnd = this.gapStart + this.gapLength;
        if (newGapStart < this.gapStart) {
            this.copyData(0, tgt, 0, newGapStart);
            int tgtRawIndex = newGapStart + tgt.gapLength;
            int len = this.gapStart - newGapStart;
            this.copyData(newGapStart, tgt, tgtRawIndex, len);
            this.copyData(gapEnd, tgt, tgtRawIndex += len, this.capacity() - gapEnd);
        } else {
            this.copyData(0, tgt, 0, this.gapStart);
            int len = newGapStart - this.gapStart;
            this.copyData(gapEnd, tgt, this.gapStart, len);
            this.copyData(gapEnd, tgt, newGapStart + tgt.gapLength, this.capacity() - (gapEnd += len));
        }
    }

    protected abstract void copyData(int var1, LAState var2, int var3, int var4);

    protected abstract int capacity();

    final void moveGap(int index) {
        if (index == this.gapStart) {
            return;
        }
        if (this.gapLength > 0) {
            if (index < this.gapStart) {
                int moveSize = this.gapStart - index;
                this.moveData(index, this.gapStart + this.gapLength - moveSize, moveSize);
            } else {
                int gapEnd = this.gapStart + this.gapLength;
                int moveSize = index - this.gapStart;
                this.moveData(gapEnd, this.gapStart, moveSize);
                if (index < gapEnd) {
                    // empty if block
                }
            }
        }
        this.gapStart = index;
    }

    protected abstract void moveData(int var1, int var2, int var3);

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < this.size(); ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(this.lookahead(i));
            sb.append(", ");
            sb.append(this.state(i));
        }
        sb.append(']');
        return sb.toString();
    }

    static final class LargeState
    extends LAState {
        int[] lookaheads;
        Object[] states;

        LargeState(int capacity) {
            super(capacity);
            this.lookaheads = new int[capacity];
            this.states = new Object[capacity];
        }

        @Override
        public int lookahead(int index) {
            return this.lookaheads[this.rawIndex(index)];
        }

        @Override
        public Object state(int index) {
            return this.states[this.rawIndex(index)];
        }

        @Override
        protected LAState upgrade(int capacity, Class laStateClass) {
            return new LargeState(capacity);
        }

        @Override
        protected boolean isUpgrade(Class laStateClass) {
            return false;
        }

        @Override
        protected void copyData(int srcRawIndex, LAState tgt, int dstRawIndex, int len) {
            System.arraycopy(this.lookaheads, srcRawIndex, ((LargeState)tgt).lookaheads, dstRawIndex, len);
            System.arraycopy(this.states, srcRawIndex, ((LargeState)tgt).states, dstRawIndex, len);
        }

        @Override
        protected void moveData(int srcRawIndex, int dstRawIndex, int len) {
            System.arraycopy(this.lookaheads, srcRawIndex, this.lookaheads, dstRawIndex, len);
            System.arraycopy(this.states, srcRawIndex, this.states, dstRawIndex, len);
        }

        @Override
        protected Class addToGapStart(int lookahead, Object state) {
            this.lookaheads[this.gapStart] = lookahead;
            this.states[this.gapStart] = state;
            return null;
        }

        @Override
        protected void removeUpdate(int index, int count) {
            while (--count >= 0) {
                this.states[index + count] = null;
            }
        }

        @Override
        protected int capacity() {
            return this.lookaheads.length;
        }
    }

    static final class ByteState
    extends LAState {
        short[] laStateShorts;

        ByteState(int capacity) {
            super(capacity);
            this.laStateShorts = new short[capacity];
        }

        @Override
        public int lookahead(int index) {
            return this.laStateShorts[this.rawIndex(index)] & 255;
        }

        @Override
        public Object state(int index) {
            int val = this.laStateShorts[this.rawIndex(index)] & 65280;
            return val == 65280 ? null : IntegerCache.integer(val >> 8);
        }

        @Override
        protected LAState upgrade(int capacity, Class laStateClass) {
            if (laStateClass == LargeState.class) {
                return new LargeState(capacity);
            }
            return new ByteState(capacity);
        }

        @Override
        protected boolean isUpgrade(Class laStateClass) {
            return laStateClass == LargeState.class;
        }

        @Override
        protected void copyData(int srcRawIndex, LAState tgt, int dstRawIndex, int len) {
            if (tgt.getClass() == this.getClass()) {
                System.arraycopy(this.laStateShorts, srcRawIndex, ((ByteState)tgt).laStateShorts, dstRawIndex, len);
            } else {
                int[] las = ((LargeState)tgt).lookaheads;
                Object[] states = ((LargeState)tgt).states;
                while (--len >= 0) {
                    int val = this.laStateShorts[srcRawIndex++] & 65535;
                    las[dstRawIndex] = val & 255;
                    if ((val &= 65280) != 65280) {
                        states[dstRawIndex] = IntegerCache.integer(val >> 8);
                    }
                    ++dstRawIndex;
                }
            }
        }

        @Override
        protected void moveData(int srcRawIndex, int dstRawIndex, int len) {
            System.arraycopy(this.laStateShorts, srcRawIndex, this.laStateShorts, dstRawIndex, len);
        }

        @Override
        protected Class addToGapStart(int lookahead, Object state) {
            if (lookahead <= 127) {
                int intState;
                if (state == null) {
                    intState = 65280;
                } else if (ByteState.isByteState(state)) {
                    intState = (Integer)state << 8;
                } else {
                    return LargeState.class;
                }
                this.laStateShorts[this.gapStart] = (short)(intState | lookahead);
                return null;
            }
            return LargeState.class;
        }

        @Override
        protected int capacity() {
            return this.laStateShorts.length;
        }
    }

    static final class NoState
    extends LAState {
        byte[] laBytes;

        NoState(int capacity) {
            super(capacity);
            this.laBytes = new byte[capacity];
        }

        @Override
        public int lookahead(int index) {
            int rawIndex = this.rawIndex(index);
            return this.laBytes[rawIndex];
        }

        @Override
        public Object state(int index) {
            return null;
        }

        @Override
        protected LAState upgrade(int capacity, Class laStateClass) {
            if (laStateClass == LargeState.class) {
                return new LargeState(capacity);
            }
            if (laStateClass == ByteState.class) {
                return new ByteState(capacity);
            }
            return new NoState(capacity);
        }

        @Override
        protected boolean isUpgrade(Class laStateClass) {
            return laStateClass == LargeState.class || laStateClass == ByteState.class;
        }

        @Override
        protected void copyData(int srcRawIndex, LAState tgt, int dstRawIndex, int len) {
            if (tgt.getClass() == this.getClass()) {
                System.arraycopy(this.laBytes, srcRawIndex, ((NoState)tgt).laBytes, dstRawIndex, len);
            } else if (tgt.getClass() == ByteState.class) {
                short[] laStateShorts = ((ByteState)tgt).laStateShorts;
                while (--len >= 0) {
                    laStateShorts[dstRawIndex++] = (short)(this.laBytes[srcRawIndex++] | 65280);
                }
            } else {
                int[] las = ((LargeState)tgt).lookaheads;
                while (--len >= 0) {
                    las[dstRawIndex++] = this.laBytes[srcRawIndex++];
                }
            }
        }

        @Override
        protected void moveData(int srcRawIndex, int dstRawIndex, int len) {
            System.arraycopy(this.laBytes, srcRawIndex, this.laBytes, dstRawIndex, len);
        }

        @Override
        protected Class addToGapStart(int lookahead, Object state) {
            if (lookahead <= 127) {
                if (state == null) {
                    this.laBytes[this.gapStart] = (byte)lookahead;
                    return null;
                }
                if (NoState.isByteState(state)) {
                    return ByteState.class;
                }
            }
            return LargeState.class;
        }

        @Override
        protected int capacity() {
            return this.laBytes.length;
        }
    }

}

