/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer;

import java.util.Set;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.LanguageManager;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenIdImpl;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.inc.IncTokenList;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.SnapshotTokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.TextToken;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;

public final class LexerUtilsConstants {
    public static final int MAX_FLY_SEQUENCE_LENGTH = 5;
    public static final int MOD_COUNT_IMMUTABLE_INPUT = -1;
    public static final int MOD_COUNT_REMOVED = -2;
    public static final int READER_TEXT_BUFFER_SIZE = 16384;
    public static final AbstractToken<?> SKIP_TOKEN = new TextToken<TokenIdImpl>(new WrapTokenId<TokenIdImpl>(new TokenIdImpl("skip-token-id; special id of TokenFactory.SKIP_TOKEN;  It should never be part of token sequence", 0, null)), "");
    public static final int MOD_COUNT_EMBEDDED_INITIAL = -3;

    public static void tokenLengthZeroOrNegative(int tokenLength) {
        if (tokenLength == 0) {
            throw new IllegalArgumentException("Tokens with zero length are not supported by the framework. Fix the lexer.");
        }
        throw new IllegalArgumentException("Negative token length " + tokenLength);
    }

    public static void throwFlyTokenProhibited() {
        throw new IllegalStateException("Flyweight token created but prohibited. Lexer needs to check lexerInput.isFlyTokenAllowed().");
    }

    public static void throwBranchTokenFlyProhibited(AbstractToken token) {
        throw new IllegalStateException("Language embedding cannot be created for flyweight token=" + token + "\nFix the lexer to not create flyweight token instance when" + " language embedding exists for the token.");
    }

    public static void checkValidBackup(int count, int maxCount) {
        if (count > maxCount) {
            throw new IndexOutOfBoundsException("Cannot backup " + count + " characters. Maximum: " + maxCount + '.');
        }
    }

    public static <T extends TokenId> Language<T> innerLanguage(LanguagePath languagePath) {
        Language l = languagePath.innerLanguage();
        return l;
    }

    public static <T extends TokenId> LanguageHierarchy<T> innerLanguageHierarchy(LanguagePath languagePath) {
        Language<T> language = LexerUtilsConstants.innerLanguage(languagePath);
        return LexerApiPackageAccessor.get().languageHierarchy(language);
    }

    public static <T extends TokenId> LanguageOperation<T> innerLanguageOperation(LanguagePath languagePath) {
        Language<T> language = LexerUtilsConstants.innerLanguage(languagePath);
        return LexerApiPackageAccessor.get().languageOperation(language);
    }

    public static <T extends TokenId> LanguageOperation<T> languageOperation(Language<T> language) {
        return LexerApiPackageAccessor.get().languageOperation(language);
    }

    public static <T extends TokenId> LanguageHierarchy<T> languageHierarchy(Language<T> language) {
        return LexerApiPackageAccessor.get().languageHierarchy(language);
    }

    public static <T extends TokenId> LanguageEmbedding<?> findEmbedding(LanguageHierarchy<T> languageHierarchy, AbstractToken<T> token, LanguagePath languagePath, InputAttributes inputAttributes) {
        LanguageEmbedding embedding = LexerSpiPackageAccessor.get().embedding(languageHierarchy, token, languagePath, inputAttributes);
        if (embedding == null) {
            embedding = LanguageManager.getInstance().findLanguageEmbedding(token, languagePath, inputAttributes);
        }
        return embedding;
    }

    public static int maxLanguagePathSize(Set<LanguagePath> paths) {
        int maxPathSize = 0;
        for (LanguagePath lp : paths) {
            maxPathSize = Math.max(lp.size(), maxPathSize);
        }
        return maxPathSize;
    }

    public static Object languageOrArrayAdd(Object languageOrArray, Language language) {
        if (languageOrArray == null) {
            return language;
        }
        if (languageOrArray.getClass() == Language.class) {
            Language[] arrlanguage;
            if (languageOrArray != language) {
                Language[] arrlanguage2 = new Language[2];
                arrlanguage2[0] = (Language)languageOrArray;
                arrlanguage = arrlanguage2;
                arrlanguage2[1] = language;
            } else {
                arrlanguage = languageOrArray;
            }
            return arrlanguage;
        }
        Language[] languageArray = languageOrArray;
        for (int i = languageArray.length - 1; i >= 0; --i) {
            if (languageArray[i] != language) continue;
            return languageOrArray;
        }
        Language[] ret = new Language[languageArray.length + 1];
        System.arraycopy(languageArray, 0, ret, 0, languageArray.length);
        ret[languageArray.length] = language;
        return ret;
    }

    public static Object languageOrArrayRemove(Object languageOrArray, Language language) {
        if (languageOrArray == null) {
            return null;
        }
        if (languageOrArray.getClass() == Language.class) {
            return languageOrArray == language ? null : languageOrArray;
        }
        Language[] languageArray = (Language[])languageOrArray;
        for (int i = languageArray.length - 1; i >= 0; --i) {
            if (languageArray[i] != language) continue;
            Language[] ret = new Language[languageArray.length - 1];
            System.arraycopy(languageArray, 0, ret, 0, i);
            System.arraycopy(languageArray, i + 1, ret, i, languageArray.length - i - 1);
            return ret;
        }
        return languageOrArray;
    }

    public static int languageOrArraySize(Object languageOrArray) {
        if (languageOrArray == null) {
            return 0;
        }
        if (languageOrArray.getClass() == Language.class) {
            return 1;
        }
        return ((Language[])languageOrArray).length;
    }

    public static boolean languageOrArrayContains(Object languageOrArray, Language language) {
        if (languageOrArray == null) {
            return false;
        }
        if (languageOrArray.getClass() == Language.class) {
            return (Language)languageOrArray == language;
        }
        Language[] languageArray = (Language[])languageOrArray;
        for (int i = languageArray.length - 1; i >= 0; --i) {
            if (languageArray[i] != language) continue;
            return true;
        }
        return false;
    }

    public static Language languageOrArrayGet(Object languageOrArray, int index) {
        if (languageOrArray != null) {
            if (languageOrArray.getClass() == Language.class) {
                if (index == 0) {
                    return (Language)languageOrArray;
                }
            } else {
                Language[] languageArray = (Language[])languageOrArray;
                if (index >= 0 && index < languageArray.length) {
                    return languageArray[index];
                }
            }
        }
        throw new IndexOutOfBoundsException("Invalid index=" + index + ", length=" + LexerUtilsConstants.languageOrArraySize(languageOrArray));
    }

    public static int[] tokenIndexLazyTokenCreation(TokenList<?> tokenList, int offset) {
        int prevTokenOffset;
        int tokenCount = tokenList.tokenCountCurrent();
        if (tokenCount == 0) {
            if (tokenList.tokenOrEmbedding(0) == null) {
                return new int[]{-1, 0};
            }
            tokenCount = tokenList.tokenCountCurrent();
        }
        if (offset > (prevTokenOffset = tokenList.tokenOffset(tokenCount - 1))) {
            int tokenLength = tokenList.tokenOrEmbedding(tokenCount - 1).token().length();
            while (offset >= prevTokenOffset + tokenLength) {
                TokenOrEmbedding tokenOrEmbedding = tokenList.tokenOrEmbedding(tokenCount);
                if (tokenOrEmbedding != null) {
                    AbstractToken t = tokenOrEmbedding.token();
                    prevTokenOffset = t.isFlyweight() ? (prevTokenOffset += tokenLength) : tokenList.tokenOffset(tokenCount);
                    tokenLength = t.length();
                    ++tokenCount;
                    continue;
                }
                return new int[]{tokenCount, prevTokenOffset + tokenLength};
            }
            return new int[]{tokenCount - 1, prevTokenOffset};
        }
        return LexerUtilsConstants.tokenIndexBinSearch(tokenList, offset, tokenCount);
    }

    public static int[] tokenIndexBinSearch(TokenList<?> tokenList, int offset, int tokenCount) {
        int low = 0;
        int high = tokenCount - 1;
        int mid = -1;
        int midStartOffset = -1;
        while (low <= high) {
            mid = low + high >>> 1;
            midStartOffset = tokenList.tokenOffset(mid);
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            return new int[]{mid, midStartOffset};
        }
        if (high >= 0) {
            if (low == tokenCount) {
                AbstractToken t = tokenList.tokenOrEmbedding(high).token();
                if (offset >= midStartOffset + t.length()) {
                    ++high;
                    midStartOffset += t.length();
                } else if (mid != high) {
                    midStartOffset = tokenList.tokenOffset(high);
                }
            } else if (mid != high) {
                midStartOffset = tokenList.tokenOffset(high);
            }
        } else {
            if (tokenCount == 0) {
                return new int[]{-1, 0};
            }
            high = 0;
        }
        return new int[]{high, midStartOffset};
    }

    public static int updatedStartOffset(EmbeddedTokenList<?, ?> etl, int rootModCount, TokenHierarchyEventInfo eventInfo) {
        etl.updateModCount();
        int startOffset = etl.startOffset();
        return etl.isRemoved() && startOffset > eventInfo.modOffset() ? Math.max(startOffset - eventInfo.removedLength(), eventInfo.modOffset()) : startOffset;
    }

    public static <T extends TokenId> StringBuilder appendTokenList(StringBuilder sb, TokenList<T> tokenList) {
        return LexerUtilsConstants.appendTokenList(sb, tokenList, -1, 0, Integer.MAX_VALUE, true, 0, true);
    }

    public static <T extends TokenId> StringBuilder appendTokenListIndented(StringBuilder sb, TokenList<T> tokenList, int indent) {
        return LexerUtilsConstants.appendTokenList(sb, tokenList, -1, 0, Integer.MAX_VALUE, false, indent, true);
    }

    public static <T extends TokenId> StringBuilder appendTokenList(StringBuilder sb, TokenList<T> tokenList, int currentIndex, int startIndex, int endIndex, boolean appendEmbedded, int indent, boolean dumpTokenText) {
        if (sb == null) {
            sb = new StringBuilder(200);
        }
        TokenHierarchy tokenHierarchy = tokenList instanceof SnapshotTokenList ? ((SnapshotTokenList)tokenList).snapshot().tokenHierarchy() : null;
        endIndex = Math.min(tokenList.tokenCountCurrent(), endIndex);
        int digitCount = ArrayUtilities.digitCount((int)(endIndex - 1));
        for (int i = Math.max((int)startIndex, (int)0); i < endIndex; ++i) {
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
            sb.append(i == currentIndex ? '*' : 'T');
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            try {
                LexerUtilsConstants.appendTokenInfo(sb, tokenList, i, tokenHierarchy, appendEmbedded, indent, dumpTokenText);
            }
            catch (IndexOutOfBoundsException e) {
                sb.append("<IOOBE occurred!!!>\n");
                break;
            }
            sb.append('\n');
        }
        return sb;
    }

    public static boolean statesEqual(Object state1, Object state2) {
        return state1 == null && state2 == null || state1 != null && state1.equals(state2);
    }

    public static String idToString(TokenId id) {
        return id.name() + '[' + id.ordinal() + ']';
    }

    public static <T extends TokenId> void appendTokenInfo(StringBuilder sb, TokenList<T> tokenList, int index, TokenHierarchy tokenHierarchy, boolean appendEmbedded, int indent, boolean dumpTokenText) {
        LexerUtilsConstants.appendTokenInfo(sb, tokenList.tokenOrEmbedding(index), tokenList.lookahead(index), tokenList.state(index), tokenHierarchy, appendEmbedded, indent, dumpTokenText);
    }

    public static <T extends TokenId> void appendTokenInfo(StringBuilder sb, TokenOrEmbedding<T> tokenOrEmbedding, int lookahead, Object state, TokenHierarchy<?> tokenHierarchy, boolean appendEmbedded, int indent, boolean dumpTokenText) {
        if (tokenOrEmbedding == null) {
            sb.append("<NULL-TOKEN>");
        } else {
            EmbeddedTokenList etl = tokenOrEmbedding.embedding();
            AbstractToken<T> token = tokenOrEmbedding.token();
            token.dumpInfo(sb, tokenHierarchy, dumpTokenText, true, indent);
            LexerUtilsConstants.appendLAState(sb, lookahead, state);
            sb.append(", ");
            LexerUtilsConstants.appendIdentityHashCode(sb, token);
            if (etl != null) {
                int index = 0;
                do {
                    sb.append('\n');
                    ArrayUtilities.appendSpaces((StringBuilder)sb, (int)(indent += 4));
                    sb.append("  Embedding[").append(index).append("]: \"");
                    sb.append(etl.languagePath().mimePath()).append("\", ");
                    LexerUtilsConstants.appendIdentityHashCode(sb, etl);
                    sb.append("\n");
                    if (appendEmbedded) {
                        LexerUtilsConstants.appendTokenList(sb, etl, -1, 0, Integer.MAX_VALUE, appendEmbedded, indent, true);
                    }
                    etl = etl.nextEmbeddedTokenList();
                    ++index;
                } while (etl != null);
            }
        }
    }

    public static void appendIdentityHashCode(StringBuilder sb, Object o) {
        sb.append("IHC=");
        sb.append(System.identityHashCode(o));
    }

    public static void appendLAState(StringBuilder sb, TokenList<?> tokenList, int index) {
        LexerUtilsConstants.appendLAState(sb, tokenList.lookahead(index), tokenList.state(index));
    }

    public static void appendLAState(StringBuilder sb, int lookahead, Object state) {
        if (lookahead > 0) {
            sb.append(", la=");
            sb.append(lookahead);
        }
        if (state != null) {
            sb.append(", st=");
            sb.append(state);
        }
    }

    public static String checkConsistencyTokenList(TokenList<?> tokenList, boolean checkEmbedded) {
        int startOffset;
        String error;
        int tokenCountCurrent = tokenList.tokenCountCurrent();
        boolean continuous = tokenList.isContinuous();
        if (tokenList instanceof EmbeddedTokenList) {
            ((EmbeddedTokenList)tokenList).updateModCount();
        }
        if (tokenList instanceof IncTokenList && (error = ((IncTokenList)tokenList).checkConsistency()) != null) {
            return error;
        }
        int lastOffset = startOffset = tokenList.startOffset();
        for (int i = 0; i < tokenCountCurrent; ++i) {
            int offset;
            TokenOrEmbedding tokenOrEmbedding = tokenList.tokenOrEmbedding(i);
            if (tokenOrEmbedding == null) {
                tokenOrEmbedding = tokenList.tokenOrEmbedding(i);
                return LexerUtilsConstants.dumpContext("Null token", tokenList, i);
            }
            AbstractToken token = tokenOrEmbedding.token();
            if (token.isRemoved()) {
                return LexerUtilsConstants.dumpContext("Token is removed", tokenList, i);
            }
            if (i == 0 && continuous && tokenCountCurrent > 0 && !token.isFlyweight() && token.offset(null) != tokenList.startOffset()) {
                return LexerUtilsConstants.dumpContext("firstToken.offset()=" + token.offset(null) + " != tokenList.startOffset()=" + tokenList.startOffset(), tokenList, i);
            }
            if (!token.isFlyweight() && token.tokenList() != tokenList && !(tokenList instanceof JoinTokenList)) {
                return LexerUtilsConstants.dumpContext("Invalid token.tokenList()=" + token.tokenList(), tokenList, i);
            }
            if (token.text() == null) {
                return LexerUtilsConstants.dumpContext("Null token.text()", tokenList, i);
            }
            if (token.text().toString() == null) {
                return LexerUtilsConstants.dumpContext("Null token.text().toString()", tokenList, i);
            }
            int n = offset = token.isFlyweight() ? lastOffset : token.offset(null);
            if (offset < 0) {
                return LexerUtilsConstants.dumpContext("Token offset=" + offset + " < 0", tokenList, i);
            }
            if (offset < lastOffset) {
                return LexerUtilsConstants.dumpContext("Token offset=" + offset + " < lastOffset=" + lastOffset, tokenList, i);
            }
            if (offset > lastOffset && continuous) {
                return LexerUtilsConstants.dumpContext("Gap between tokens; offset=" + offset + ", lastOffset=" + lastOffset, tokenList, i);
            }
            lastOffset = offset + token.length();
            EmbeddedTokenList etl = tokenOrEmbedding.embedding();
            if (etl == null || !checkEmbedded) continue;
            while (etl != null) {
                String error2 = LexerUtilsConstants.checkConsistencyTokenList(etl, checkEmbedded);
                if (error2 != null) {
                    return error2;
                }
                etl = etl.nextEmbeddedTokenList();
            }
        }
        if (tokenList instanceof MutableTokenList && ((MutableTokenList)tokenList).isFullyLexed()) {
            int endOffset = tokenList.endOffset();
            if (tokenList.isContinuous() && startOffset != endOffset && tokenCountCurrent == 0) {
                String msg = "Non-empty " + tokenList.dumpInfoType() + " <" + startOffset + "," + endOffset + "> " + " does not contain any tokens";
                return LexerUtilsConstants.dumpContext(msg, tokenList, 0);
            }
            if (continuous && lastOffset != endOffset) {
                return LexerUtilsConstants.dumpContext("lastOffset=" + lastOffset + " != endOffset=" + endOffset, tokenList, tokenCountCurrent);
            }
        }
        return null;
    }

    private static String dumpContext(String msg, TokenList<?> tokenList, int index) {
        StringBuilder sb = new StringBuilder();
        sb.append(msg);
        sb.append(" at index=");
        sb.append(index);
        sb.append(" of tokenList with ");
        LexerUtilsConstants.appendIdentityHashCode(sb, tokenList);
        sb.append(" of language-path ");
        sb.append(tokenList.languagePath().mimePath());
        sb.append(", ").append(tokenList.getClass());
        sb.append('\n');
        LexerUtilsConstants.appendTokenList(sb, tokenList, index, index - 2, index + 3, false, 0, true);
        return sb.toString();
    }

    private LexerUtilsConstants() {
    }
}

