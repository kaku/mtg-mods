/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.token.AbstractToken;

public class TextLexerInputOperation<T extends TokenId>
extends LexerInputOperation<T> {
    private final CharSequence inputSourceText;
    private int readEndOffset;

    public TextLexerInputOperation(TokenList<T> tokenList) {
        this(tokenList, 0, null, 0, -1);
    }

    public TextLexerInputOperation(TokenList<T> tokenList, int tokenIndex, Object lexerRestartState, int startOffset, int endOffset) {
        super(tokenList, tokenIndex, lexerRestartState);
        this.inputSourceText = tokenList.inputSourceText();
        if (endOffset == -1) {
            endOffset = this.inputSourceText.length();
        }
        if (startOffset < 0 || startOffset > endOffset || endOffset > this.inputSourceText.length()) {
            throw new IndexOutOfBoundsException("startOffset=" + startOffset + ", endOffset=" + endOffset + ", inputSourceText.length()=" + this.inputSourceText.length());
        }
        this.readOffset = this.tokenStartOffset = startOffset;
        this.readEndOffset = endOffset;
    }

    @Override
    public int read(int offset) {
        if (offset < this.readEndOffset) {
            return this.inputSourceText.charAt(offset);
        }
        return -1;
    }

    @Override
    public char readExisting(int offset) {
        return this.inputSourceText.charAt(offset);
    }

    @Override
    protected void fillTokenData(AbstractToken<T> token) {
        token.setRawOffset(this.tokenStartOffset);
    }

    protected final int readEndIndex() {
        return this.readEndOffset;
    }
}

