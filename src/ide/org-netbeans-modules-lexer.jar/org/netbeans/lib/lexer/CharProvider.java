/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer;

import org.netbeans.lib.editor.util.ArrayUtilities;

public interface CharProvider {
    public int read();

    public char readExisting(int var1);

    public int readIndex();

    public int deepRawLength(int var1);

    public int deepRawLengthShift(int var1);

    public void backup(int var1);

    public int tokenLength();

    public void assignTokenLength(int var1, boolean var2);

    public void consumeTokenLength();

    public void collectExtraPreprocessedChars(ExtraPreprocessedChars var1, int var2, int var3, int var4);

    public static final class ExtraPreprocessedChars {
        private int preStartIndex;
        private int postEndIndex;
        private char[] extraPrepChars = ArrayUtilities.emptyCharArray();
        private int[] extraRawLengthShifts;

        public void ensureExtraLength(int length) {
            int preLength = this.extraPrepChars.length - this.preStartIndex;
            if ((length += this.postEndIndex + preLength) > this.extraPrepChars.length) {
                this.extraPrepChars = ArrayUtilities.charArray((char[])this.extraPrepChars, (int)(length <<= 1), (int)this.postEndIndex, (int)(this.preStartIndex - this.postEndIndex));
                this.extraRawLengthShifts = ArrayUtilities.intArray((int[])this.extraRawLengthShifts, (int)length, (int)this.postEndIndex, (int)(this.preStartIndex - this.postEndIndex));
                this.preStartIndex = this.extraPrepChars.length - preLength;
            }
        }

        public void insert(char ch, int rawLengthShift) {
            --this.preStartIndex;
            this.extraPrepChars[this.extraPrepChars.length - this.preStartIndex] = ch;
            this.extraRawLengthShifts[this.extraPrepChars.length - this.preStartIndex] = rawLengthShift;
        }

        public void append(char ch, int rawLengthShift) {
            this.extraPrepChars[this.postEndIndex] = ch;
            this.extraRawLengthShifts[this.postEndIndex] = rawLengthShift;
            ++this.postEndIndex;
        }

        public void clear() {
            this.preStartIndex = this.extraPrepChars.length;
            this.postEndIndex = 0;
        }

        public int preStartIndex() {
            return this.preStartIndex;
        }

        public int postEndIndex() {
            return this.postEndIndex;
        }

        public char[] extraPrepChars() {
            return this.extraPrepChars;
        }

        public int[] extraRawLengthShifts() {
            return this.extraRawLengthShifts;
        }
    }

}

