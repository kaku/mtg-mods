/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.FlyOffsetGapList
 */
package org.netbeans.lib.lexer;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.FlyOffsetGapList;
import org.netbeans.lib.lexer.EmbeddedJoinInfo;
import org.netbeans.lib.lexer.LAState;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TextLexerInputOperation;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.lib.lexer.token.PartToken;
import org.netbeans.lib.lexer.token.TextToken;
import org.netbeans.spi.lexer.LanguageEmbedding;

public final class EmbeddedTokenList<T extends TokenId, ET extends TokenId>
extends FlyOffsetGapList<TokenOrEmbedding<ET>>
implements MutableTokenList<ET>,
TokenOrEmbedding<T> {
    private AbstractToken<T> branchToken;
    private int branchTokenStartOffset;
    private final TokenList<?> rootTokenList;
    private int rootModCount;
    private int extraModCount;
    private final LanguageEmbedding<ET> embedding;
    private final LanguagePath languagePath;
    private LAState laState;
    private EmbeddedTokenList<T, ?> nextEmbeddedTokenList;
    private EmbeddedJoinInfo<ET> joinInfo;

    public EmbeddedTokenList(TokenList<?> rootTokenList, LanguagePath languagePath, LanguageEmbedding<ET> embedding) {
        super(1);
        this.rootTokenList = rootTokenList;
        this.languagePath = languagePath;
        this.embedding = embedding;
        this.initLAState();
    }

    void reinit(AbstractToken<T> branchToken, int branchTokenStartOffset, int modCount) {
        this.branchToken = branchToken;
        this.branchTokenStartOffset = branchTokenStartOffset;
        this.rootModCount = modCount;
    }

    public void reinitChain(AbstractToken<T> branchToken, int branchTokenStartOffset, int modCount) {
        this.reinit(branchToken, branchTokenStartOffset, modCount);
        if (this.nextEmbeddedTokenList != null) {
            this.nextEmbeddedTokenList.reinitChain(branchToken, branchTokenStartOffset, modCount);
        }
    }

    public void initAllTokens() {
        assert (!this.embedding.joinSections());
        LexerInputOperation<ET> lexerInputOperation = this.createLexerInputOperation(0, this.startOffset(), null);
        AbstractToken<ET> token = lexerInputOperation.nextToken();
        while (token != null) {
            this.addToken(token, lexerInputOperation);
            token = lexerInputOperation.nextToken();
        }
        lexerInputOperation.release();
        this.trimStorageToSize();
    }

    private void initLAState() {
        this.laState = this.rootModCount != -1 || TokenList.LOG.isLoggable(Level.FINE) ? LAState.empty() : null;
    }

    public void addToken(AbstractToken<ET> token) {
        if (!token.isFlyweight()) {
            token.setTokenList(this);
        }
        this.updateElementOffsetAdd(token);
        this.add(token);
    }

    public void addToken(AbstractToken<ET> token, LexerInputOperation<ET> lexerInputOperation) {
        this.addToken(token);
        if (this.laState != null) {
            this.laState = this.laState.add(lexerInputOperation.lookahead(), lexerInputOperation.lexerState());
        }
    }

    public void addToken(AbstractToken<ET> token, int lookahead, Object state) {
        this.addToken(token);
        if (this.laState != null) {
            this.laState = this.laState.add(lookahead, state);
        }
    }

    public void trimStorageToSize() {
        this.trimToSize();
        if (this.laState != null) {
            this.laState.trimToSize();
        }
    }

    public EmbeddedTokenList<T, ?> nextEmbeddedTokenList() {
        return this.nextEmbeddedTokenList;
    }

    public void setNextEmbeddedTokenList(EmbeddedTokenList<T, ?> nextEmbeddedTokenList) {
        this.nextEmbeddedTokenList = nextEmbeddedTokenList;
    }

    @Override
    public Language<ET> language() {
        return this.embedding.language();
    }

    @Override
    public LanguagePath languagePath() {
        return this.languagePath;
    }

    public LanguageEmbedding languageEmbedding() {
        return this.embedding;
    }

    @Override
    public int tokenCount() {
        return this.tokenCountCurrent();
    }

    @Override
    public int tokenCountCurrent() {
        return this.size();
    }

    public EmbeddedJoinInfo<ET> joinInfo() {
        return this.joinInfo;
    }

    public void setJoinInfo(EmbeddedJoinInfo<ET> joinInfo) {
        this.joinInfo = joinInfo;
    }

    public int joinTokenCount() {
        int tokenCount = this.tokenCountCurrent();
        if (tokenCount > 0 && this.joinInfo.joinTokenLastPartShift() > 0) {
            --tokenCount;
        }
        return tokenCount;
    }

    public boolean joinBackward() {
        if (this.tokenCountCurrent() > 0) {
            AbstractToken<ET> token = this.tokenOrEmbeddingDirect(0).token();
            return token.getClass() == PartToken.class && ((PartToken)token).partTokenIndex() > 0;
        }
        return this.joinInfo.joinTokenLastPartShift() > 0;
    }

    @Override
    public TokenOrEmbedding<ET> tokenOrEmbedding(int index) {
        return index < this.size() ? (TokenOrEmbedding)this.get(index) : null;
    }

    @Override
    public AbstractToken<T> token() {
        return this.branchToken;
    }

    @Override
    public EmbeddedTokenList<T, ET> embedding() {
        return this;
    }

    @Override
    public int lookahead(int index) {
        return this.laState != null ? this.laState.lookahead(index) : -1;
    }

    @Override
    public Object state(int index) {
        return this.laState != null ? this.laState.state(index) : null;
    }

    @Override
    public int tokenOffset(int index) {
        return this.elementOffset(index);
    }

    @Override
    public int tokenOffset(AbstractToken<ET> token) {
        if (token.getClass() == JoinToken.class) {
            return token.offset(null);
        }
        int rawOffset = token.rawOffset();
        int relOffset = rawOffset < this.offsetGapStart() ? rawOffset : rawOffset - this.offsetGapLength();
        return this.startOffset() + relOffset;
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexBinSearch(this, offset, this.tokenCountCurrent());
    }

    int branchTokenStartOffset() {
        return this.branchTokenStartOffset;
    }

    @Override
    public int modCount() {
        return this.rootModCount + this.extraModCount;
    }

    public void updateModCount() {
        this.updateModCount(this.rootTokenList.modCount());
    }

    public void updateModCount(int rootModCount) {
        if (this.rootModCount != -2 && rootModCount != this.rootModCount) {
            this.rootModCount = rootModCount;
            TokenList<T> parentTokenList = this.branchToken.tokenList();
            if (parentTokenList.getClass() == EmbeddedTokenList.class) {
                ((EmbeddedTokenList)parentTokenList).updateModCount(rootModCount);
            }
            this.branchTokenStartOffset = parentTokenList.tokenOffset(this.branchToken);
        }
    }

    public void incrementExtraModCount() {
        ++this.extraModCount;
    }

    public void markRemoved() {
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("ETL.markRemoved(): ETL-");
            LexerUtilsConstants.appendIdentityHashCode(sb, this);
            sb.append('\n');
            LOG.fine(sb.toString());
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.INFO, "Embedded token list marked as removed:", new Exception());
            }
        }
        this.rootModCount = -2;
        this.extraModCount = 0;
    }

    public void markRemoved(int branchTokenStartOffset) {
        this.branchTokenStartOffset = branchTokenStartOffset;
        this.markRemoved();
    }

    public void markRemovedChain(int branchTokenStartOffset) {
        this.markRemoved(branchTokenStartOffset);
        if (this.nextEmbeddedTokenList != null) {
            this.nextEmbeddedTokenList.markRemoved(branchTokenStartOffset);
        }
    }

    @Override
    public int startOffset() {
        return this.branchTokenStartOffset + this.embedding.startSkipLength();
    }

    @Override
    public int endOffset() {
        return this.branchTokenStartOffset + this.branchToken.length() - this.embedding.endSkipLength();
    }

    public int textLength() {
        return this.branchToken.length() - this.embedding.startSkipLength() - this.embedding.endSkipLength();
    }

    @Override
    public boolean isRemoved() {
        return this.rootModCount == -2;
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this.rootTokenList;
    }

    @Override
    public CharSequence inputSourceText() {
        return this.rootTokenList.inputSourceText();
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.rootTokenList.tokenHierarchyOperation();
    }

    protected int elementRawOffset(TokenOrEmbedding<ET> elem) {
        return elem.token().rawOffset();
    }

    protected void setElementRawOffset(TokenOrEmbedding<ET> elem, int rawOffset) {
        elem.token().setRawOffset(rawOffset);
    }

    protected boolean isElementFlyweight(TokenOrEmbedding<ET> elem) {
        return elem.token().isFlyweight();
    }

    protected int elementLength(TokenOrEmbedding<ET> elem) {
        return elem.token().length();
    }

    @Override
    public AbstractToken<ET> replaceFlyToken(int index, AbstractToken<ET> flyToken, int offset) {
        TextToken nonFlyToken = ((TextToken)flyToken).createCopy(this, this.offset2Raw(offset));
        this.set(index, nonFlyToken);
        return nonFlyToken;
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<ET> t) {
        this.set(index, t);
    }

    @Override
    public InputAttributes inputAttributes() {
        return this.rootTokenList.inputAttributes();
    }

    @Override
    public TokenOrEmbedding<ET> tokenOrEmbeddingDirect(int index) {
        return (TokenOrEmbedding)this.get(index);
    }

    @Override
    public LexerInputOperation<ET> createLexerInputOperation(int tokenIndex, int relexOffset, Object relexState) {
        int endOffset = this.endOffset();
        assert (this.isModCountUpdated());
        assert (relexOffset <= endOffset);
        return new TextLexerInputOperation(this, tokenIndex, relexState, relexOffset, endOffset);
    }

    @Override
    public boolean isFullyLexed() {
        return true;
    }

    @Override
    public void replaceTokens(TokenListChange<ET> change, TokenHierarchyEventInfo eventInfo, boolean modInside) {
        List<TokenOrEmbedding<ET>> addedTokenOrEmbeddings;
        assert (this.isModCountUpdated());
        this.incrementExtraModCount();
        int index = change.index();
        int removedTokenCount = change.removedTokenCount();
        AbstractToken firstRemovedToken = null;
        if (removedTokenCount > 0) {
            Object[] removedTokensOrEmbeddings = new TokenOrEmbedding[removedTokenCount];
            this.copyElements(index, index + removedTokenCount, removedTokensOrEmbeddings, 0);
            firstRemovedToken = removedTokensOrEmbeddings[0].token();
            int removedOffsetShift = 0;
            if (!this.isRemoved() && this.branchTokenStartOffset >= eventInfo.modOffset() + eventInfo.insertedLength() && !change.parentChangeIsBoundsChange()) {
                removedOffsetShift -= eventInfo.diffLength();
            }
            for (int i = 0; i < removedTokenCount; ++i) {
                Object tokenOrEmbedding = removedTokensOrEmbeddings[i];
                AbstractToken token = tokenOrEmbedding.token();
                if (token.isFlyweight()) continue;
                this.updateElementOffsetRemove(token);
                if (removedOffsetShift != 0) {
                    token.setRawOffset(token.rawOffset() + removedOffsetShift);
                }
                token.setTokenList(null);
                EmbeddedTokenList etl = tokenOrEmbedding.embedding();
                if (etl == null) continue;
                etl.markRemovedChain(token.rawOffset());
            }
            this.remove(index, removedTokenCount);
            this.laState.remove(index, removedTokenCount);
            change.setRemovedTokens((TokenOrEmbedding<T>[])removedTokensOrEmbeddings);
        } else {
            change.setRemovedTokensEmpty();
        }
        if (modInside) {
            int startOffset = this.startOffset();
            if (this.offsetGapStart() != change.offset() - startOffset) {
                this.moveOffsetGap(change.offset() - startOffset, change.index());
            }
            this.updateOffsetGapLength(- eventInfo.diffLength());
        }
        if ((addedTokenOrEmbeddings = change.addedTokenOrEmbeddings()) != null) {
            for (TokenOrEmbedding<ET> tokenOrEmbedding : addedTokenOrEmbeddings) {
                AbstractToken<ET> token = tokenOrEmbedding.token();
                if (!token.isFlyweight()) {
                    token.setTokenList(this);
                }
                this.updateElementOffsetAdd(token);
            }
            this.addAll(index, addedTokenOrEmbeddings);
            this.laState = this.laState.addAll(index, change.laState());
            change.syncAddedTokenCount();
            if (removedTokenCount == 1 && addedTokenOrEmbeddings.size() == 1) {
                AbstractToken<ET> addedToken = change.addedTokenOrEmbeddings().get(0).token();
                if (firstRemovedToken.id() == addedToken.id() && firstRemovedToken.partType() == addedToken.partType()) {
                    change.markBoundsChange();
                }
            }
        }
    }

    void markChildrenRemovedDeep() {
        int rootModCount = this.rootTokenList.modCount();
        for (int i = this.tokenCountCurrent() - 1; i >= 0; --i) {
            EmbeddedTokenList etl = this.tokenOrEmbeddingDirect(i).embedding();
            if (etl == null) continue;
            etl.updateModCount(rootModCount);
            etl.markRemovedChain(etl.branchTokenStartOffset());
        }
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public Set<ET> skipTokenIds() {
        return null;
    }

    public boolean isModCountUpdated() {
        return this.rootModCount == -2 || this.rootModCount == this.rootTokenList.modCount();
    }

    public String modCountErrorInfo() {
        return "!!!INTERNAL LEXER ERROR!!! Obsolete modCount in ETL " + this + "\nin token hierarchy\n" + this.rootTokenList.tokenHierarchyOperation();
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        if (this.isRemoved()) {
            sb.append("REMOVED-");
        }
        sb.append(this.dumpInfoType());
        if (this.embedding.joinSections()) {
            sb.append('j');
        }
        sb.append('<').append(this.startOffset());
        sb.append(",").append(this.endOffset());
        sb.append("> TC=").append(this.tokenCountCurrent());
        if (this.joinInfo != null) {
            sb.append("(").append(this.joinTokenCount()).append(')');
            sb.append(" JI:");
            this.joinInfo.dumpInfo(sb, this);
        }
        sb.append(", IHC=").append(System.identityHashCode(this));
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "ETL";
    }

    private String dumpRelatedTLL() {
        TokenListList tll = this.rootTokenList.tokenHierarchyOperation().existingTokenListList(this.languagePath);
        return tll != null ? tll.toString() : "<No TokenListList for " + this.languagePath.mimePath() + ">";
    }

    public int hashCode() {
        return System.identityHashCode(this);
    }

    public boolean equals(Object o) {
        return this == o;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(256);
        this.dumpInfo(sb);
        LexerUtilsConstants.appendTokenList(sb, this);
        return sb.toString();
    }
}

