/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.CharProvider;
import org.netbeans.lib.lexer.PreprocessedTextStorage;
import org.netbeans.lib.lexer.TextLexerInputOperation;
import org.netbeans.lib.lexer.TokenList;

public final class PreprocessedTextLexerInputOperation<T extends TokenId>
extends TextLexerInputOperation<T> {
    private final PreprocessedTextStorage preprocessedText;
    private int prepStartIndex;
    private int prepEndIndex;
    private int tokenStartRawLengthShift;
    private int lastRawLengthShift;
    private int tokenEndRawLengthShift;
    private int tokenStartIndex;

    public PreprocessedTextLexerInputOperation(TokenList<T> tokenList, int tokenIndex, Object lexerRestartState, PreprocessedTextStorage prepText, int prepTextStartOffset, int startOffset, int endOffset) {
        super(tokenList, tokenIndex, lexerRestartState, startOffset, endOffset);
        this.preprocessedText = prepText;
        int index = startOffset - prepTextStartOffset;
        if (index > 0) {
            this.lastRawLengthShift = this.tokenStartRawLengthShift = this.preprocessedText.rawLengthShift(index);
        }
    }

    @Override
    public int read(int index) {
        if ((index += this.tokenStartIndex) < this.readEndIndex()) {
            int rls = this.preprocessedText.rawLengthShift(index);
            if (rls != this.lastRawLengthShift) {
                this.lastRawLengthShift = rls;
                if (this.prepStartIndex >= index) {
                    this.prepStartIndex = index;
                }
                this.prepEndIndex = index + 1;
            }
            return this.preprocessedText.charAt(index);
        }
        return -1;
    }

    @Override
    public void assignTokenLength(int tokenLength) {
        this.tokenEndRawLengthShift = this.preprocessedText.rawLengthShift(this.tokenStartIndex + tokenLength - 1);
    }

    protected void tokenApproved() {
        this.tokenStartRawLengthShift += this.tokenEndRawLengthShift;
        if (this.prepStartIndex != Integer.MAX_VALUE) {
            if (this.prepStartIndex < this.tokenLength()) {
                if (this.prepEndIndex <= this.tokenLength()) {
                    this.prepStartIndex = Integer.MAX_VALUE;
                } else {
                    this.prepStartIndex = 0;
                    this.prepEndIndex -= this.tokenLength();
                }
            } else {
                this.prepStartIndex -= this.tokenLength();
                this.prepEndIndex -= this.tokenLength();
            }
        }
    }

    public void collectExtraPreprocessedChars(CharProvider.ExtraPreprocessedChars epc, int prepStartIndex, int prepEndIndex, int topPrepEndIndex) {
        if (prepStartIndex < this.tokenLength()) {
            int postCount;
            int preCount = Math.max(prepStartIndex - this.prepStartIndex, 0);
            if (this.prepEndIndex > this.tokenLength()) {
                postCount = this.tokenLength() - prepEndIndex;
                if (postCount > 0) {
                    int i = this.tokenLength() - 2;
                    while (--i >= prepStartIndex && postCount > 0 && this.preprocessedText.rawLengthShift(i + this.tokenStartIndex) == this.tokenEndRawLengthShift) {
                        --postCount;
                    }
                } else {
                    postCount = 0;
                }
            } else {
                postCount = this.prepEndIndex - prepEndIndex;
            }
            assert (preCount >= 0 && postCount >= 0);
            epc.ensureExtraLength(preCount + postCount);
            while (--preCount >= 0) {
                --prepStartIndex;
            }
            while (--postCount >= 0) {
                ++prepEndIndex;
                ++topPrepEndIndex;
            }
        }
    }
}

