/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerUtilsConstants;

public final class TokenIdImpl
implements TokenId {
    private final String name;
    private final int ordinal;
    private final String primaryCategory;

    public TokenIdImpl(String name, int ordinal, String primaryCategory) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }
        if (ordinal < 0) {
            throw new IllegalArgumentException("ordinal=" + ordinal + " of token=" + name + " cannot be < 0");
        }
        this.name = name;
        this.ordinal = ordinal;
        this.primaryCategory = primaryCategory;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public int ordinal() {
        return this.ordinal;
    }

    @Override
    public String primaryCategory() {
        return this.primaryCategory;
    }

    public String toString() {
        return LexerUtilsConstants.idToString(this);
    }

    public String toStringDetail() {
        return this.name() + "[" + this.ordinal() + (this.primaryCategory != null ? new StringBuilder().append(", \"").append(this.primaryCategory).append("\"").toString() : "") + "]";
    }
}

