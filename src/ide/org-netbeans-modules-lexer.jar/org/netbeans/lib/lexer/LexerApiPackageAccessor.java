/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.spi.lexer.LanguageHierarchy;

public abstract class LexerApiPackageAccessor {
    private static LexerApiPackageAccessor INSTANCE;

    public static LexerApiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName(Language.class.getName(), true, LexerApiPackageAccessor.class.getClassLoader());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        return INSTANCE;
    }

    public static void register(LexerApiPackageAccessor accessor) {
        INSTANCE = accessor;
    }

    public abstract <T extends TokenId> Language<T> createLanguage(LanguageHierarchy<T> var1);

    public abstract <T extends TokenId> LanguageHierarchy<T> languageHierarchy(Language<T> var1);

    public abstract <T extends TokenId> LanguageOperation<T> languageOperation(Language<T> var1);

    public abstract int languageId(Language<?> var1);

    public abstract <I> TokenHierarchy<I> createTokenHierarchy(TokenHierarchyOperation<I, ?> var1);

    public abstract TokenHierarchyEvent createTokenChangeEvent(TokenHierarchyEventInfo var1);

    public abstract <T extends TokenId> TokenChange<T> createTokenChange(TokenChangeInfo<T> var1);

    public abstract <T extends TokenId> TokenChangeInfo<T> tokenChangeInfo(TokenChange<T> var1);

    public abstract <I> TokenHierarchyOperation<I, ?> tokenHierarchyOperation(TokenHierarchy<I> var1);

    public abstract <T extends TokenId> TokenSequence<T> createTokenSequence(TokenList<T> var1);
}

