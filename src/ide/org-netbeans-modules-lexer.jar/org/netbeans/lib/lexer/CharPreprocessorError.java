/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

public final class CharPreprocessorError {
    private final String message;
    private int index;

    public CharPreprocessorError(String message, int index) {
        if (message == null) {
            throw new IllegalArgumentException("message cannot be null");
        }
        this.message = message;
        this.index = index;
    }

    public String message() {
        return this.message;
    }

    public int index() {
        return this.index;
    }

    public void updateIndex(int diff) {
        this.index += diff;
    }

    public String description() {
        return this.message + " at index=" + this.index;
    }
}

