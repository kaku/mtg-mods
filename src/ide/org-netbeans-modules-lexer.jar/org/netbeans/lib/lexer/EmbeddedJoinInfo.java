/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinTokenList;

public final class EmbeddedJoinInfo<T extends TokenId> {
    public final JoinTokenList<T> joinTokenList;
    private int rawJoinTokenIndex;
    private int rawTokenListIndex;
    private int joinTokenLastPartShift;

    public EmbeddedJoinInfo(JoinTokenList<T> joinTokenList, int rawJoinTokenIndex, int rawTokenListIndex) {
        assert (joinTokenList != null);
        this.joinTokenList = joinTokenList;
        this.rawJoinTokenIndex = rawJoinTokenIndex;
        this.rawTokenListIndex = rawTokenListIndex;
    }

    public int joinTokenIndex() {
        return this.joinTokenList.joinTokenIndex(this.rawJoinTokenIndex);
    }

    public int getRawJoinTokenIndex() {
        return this.rawJoinTokenIndex;
    }

    public void setRawJoinTokenIndex(int rawJoinTokenIndex) {
        this.rawJoinTokenIndex = rawJoinTokenIndex;
    }

    public int tokenListIndex() {
        return this.joinTokenList.tokenListIndex(this.rawTokenListIndex);
    }

    public int getRawTokenListIndex() {
        return this.rawTokenListIndex;
    }

    public void setRawTokenListIndex(int rawTokenListIndex) {
        this.rawTokenListIndex = rawTokenListIndex;
    }

    public void setJoinTokenLastPartShift(int joinTokenLastPartShift) {
        this.joinTokenLastPartShift = joinTokenLastPartShift;
    }

    public int joinTokenLastPartShift() {
        return this.joinTokenLastPartShift;
    }

    public StringBuilder dumpInfo(StringBuilder sb, EmbeddedTokenList<?, ?> etl) {
        if (sb == null) {
            sb = new StringBuilder(70);
        }
        sb.append("<").append(this.joinTokenIndex()).append("(R").append(this.rawJoinTokenIndex).append("),");
        if (etl != null) {
            sb.append(this.joinTokenIndex() + etl.joinTokenCount());
        } else {
            sb.append("?");
        }
        sb.append(">, tli=").append(this.tokenListIndex()).append("(R").append(this.rawTokenListIndex);
        sb.append("), lps=").append(this.joinTokenLastPartShift());
        return sb;
    }

    public String toString() {
        return this.dumpInfo(null, null).toString();
    }
}

