/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.Collection;
import java.util.Map;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.spi.lexer.EmbeddingPresence;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenFactory;
import org.netbeans.spi.lexer.TokenValidator;

public abstract class LexerSpiPackageAccessor {
    private static LexerSpiPackageAccessor INSTANCE;

    public static LexerSpiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class.forName(LanguageHierarchy.class.getName(), true, LexerSpiPackageAccessor.class.getClassLoader());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        return INSTANCE;
    }

    public static void register(LexerSpiPackageAccessor accessor) {
        INSTANCE = accessor;
    }

    public abstract <T extends TokenId> Collection<T> createTokenIds(LanguageHierarchy<T> var1);

    public abstract <T extends TokenId> Map<String, Collection<T>> createTokenCategories(LanguageHierarchy<T> var1);

    public abstract <T extends TokenId> Lexer<T> createLexer(LanguageHierarchy<T> var1, LexerRestartInfo<T> var2);

    public abstract <T extends TokenId> LexerRestartInfo<T> createLexerRestartInfo(LexerInput var1, TokenFactory<T> var2, Object var3, LanguagePath var4, InputAttributes var5);

    public abstract String mimeType(LanguageHierarchy<?> var1);

    public abstract <T extends TokenId> LanguageEmbedding<?> embedding(LanguageHierarchy<T> var1, Token<T> var2, LanguagePath var3, InputAttributes var4);

    public abstract <T extends TokenId> EmbeddingPresence embeddingPresence(LanguageHierarchy<T> var1, T var2);

    public abstract <T extends TokenId> TokenValidator<T> createTokenValidator(LanguageHierarchy<T> var1, T var2);

    public abstract <T extends TokenId> boolean isRetainTokenText(LanguageHierarchy<T> var1, T var2);

    public abstract LexerInput createLexerInput(LexerInputOperation<?> var1);

    public abstract Language<?> language(MutableTextInput<?> var1);

    public abstract <T extends TokenId> LanguageEmbedding<T> createLanguageEmbedding(Language<T> var1, int var2, int var3, boolean var4);

    public abstract CharSequence text(MutableTextInput<?> var1);

    public abstract InputAttributes inputAttributes(MutableTextInput<?> var1);

    public abstract <I> I inputSource(MutableTextInput<I> var1);

    public abstract boolean isReadLocked(MutableTextInput<?> var1);

    public abstract boolean isWriteLocked(MutableTextInput<?> var1);

    public abstract <T extends TokenId> TokenFactory<T> createTokenFactory(LexerInputOperation<T> var1);
}

