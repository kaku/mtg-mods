/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.lib.lexer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.LanguageManager;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.DefaultToken;
import org.netbeans.lib.lexer.token.TextToken;
import org.netbeans.spi.lexer.EmbeddingPresence;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.TokenFactory;
import org.netbeans.spi.lexer.TokenValidator;
import org.openide.util.WeakListeners;

public final class LanguageOperation<T extends TokenId>
implements PropertyChangeListener {
    private static final int MAX_START_SKIP_LENGTH_CACHED = 10;
    private static final int MAX_END_SKIP_LENGTH_CACHED = 10;
    private static final TokenValidator<TokenId> NULL_VALIDATOR = new TokenValidator<TokenId>(){

        @Override
        public Token<TokenId> validateToken(Token<TokenId> token, TokenFactory<TokenId> factory, CharSequence tokenText, int modRelOffset, int removedLength, CharSequence removedText, int insertedLength, CharSequence insertedText) {
            return null;
        }
    };
    private final LanguageHierarchy<T> languageHierarchy;
    private final Language<T> language;
    private LanguageEmbedding<T>[][] cachedEmbeddings;
    private EmbeddingPresence[] embeddingPresences;
    private LanguageEmbedding<T>[][] cachedJoinSectionsEmbeddings;
    private TokenValidator<T>[] tokenValidators;
    private Set<LanguagePath> languagePaths;
    private Set<Language<?>> exploredLanguages;
    private FlyItem<T>[] flyItems;

    public static <T extends TokenId> void findLanguagePaths(Set<LanguagePath> existingLanguagePaths, Set<LanguagePath> newLanguagePaths, Set<Language<?>> exploredLanguages, LanguagePath lp) {
        Language language;
        if (!existingLanguagePaths.contains(lp)) {
            newLanguagePaths.add(lp);
        }
        if (!exploredLanguages.contains(language = LexerUtilsConstants.innerLanguage(lp))) {
            exploredLanguages.add(language);
            Set ids = language.tokenIds();
            for (TokenId id : ids) {
                DefaultToken<TokenId> emptyToken = new DefaultToken<TokenId>(new WrapTokenId<TokenId>(id));
                LanguageHierarchy languageHierarchy = LexerUtilsConstants.innerLanguageHierarchy(lp);
                LanguageEmbedding embedding = LexerUtilsConstants.findEmbedding(languageHierarchy, emptyToken, lp, null);
                if (embedding == null) continue;
                LanguagePath elp = LanguagePath.get(lp, embedding.language());
                LanguageOperation.findLanguagePaths(existingLanguagePaths, newLanguagePaths, exploredLanguages, elp);
            }
        }
    }

    public LanguageOperation(LanguageHierarchy<T> languageHierarchy, Language<T> language) {
        this.languageHierarchy = languageHierarchy;
        this.language = language;
        LanguageManager.getInstance().addPropertyChangeListener((PropertyChangeListener)WeakListeners.create(PropertyChangeListener.class, (EventListener)this, (Object)LanguageManager.getInstance()));
    }

    public Language<T> language() {
        return this.language;
    }

    public synchronized TokenValidator<T> tokenValidator(T id) {
        TokenValidator<T> validator;
        if (this.tokenValidators == null) {
            this.tokenValidators = this.allocateTokenValidatorArray(this.language.maxOrdinal() + 1);
        }
        if ((validator = this.tokenValidators[id.ordinal()]) == null) {
            validator = LexerSpiPackageAccessor.get().createTokenValidator(this.languageHierarchy, id);
            if (validator == null) {
                validator = this.nullValidator();
            }
            this.tokenValidators[id.ordinal()] = validator;
        }
        return validator == this.nullValidator() ? null : validator;
    }

    public synchronized TextToken<T> getFlyweightToken(WrapTokenId<T> wid, String text) {
        FlyItem<T> item;
        if (this.flyItems == null) {
            FlyItem[] arr = new FlyItem[this.language.maxOrdinal() + 1];
            this.flyItems = arr;
        }
        if ((item = this.flyItems[wid.id().ordinal()]) == null) {
            item = new FlyItem<T>(wid, text);
            this.flyItems[wid.id().ordinal()] = item;
        }
        return item.flyToken(wid, text);
    }

    public synchronized EmbeddingPresence embeddingPresence(T id) {
        EmbeddingPresence ep;
        if (this.embeddingPresences == null) {
            this.embeddingPresences = new EmbeddingPresence[this.language.maxOrdinal() + 1];
        }
        if ((ep = this.embeddingPresences[id.ordinal()]) == null) {
            this.embeddingPresences[id.ordinal()] = ep = LexerSpiPackageAccessor.get().embeddingPresence(this.languageHierarchy, id);
        }
        return ep;
    }

    public synchronized void setEmbeddingPresence(T id, EmbeddingPresence ep) {
        this.embeddingPresences[id.ordinal()] = ep;
    }

    public synchronized LanguageEmbedding<T> getEmbedding(int startSkipLength, int endSkipLength, boolean joinSections) {
        LanguageEmbedding<T>[][] ce;
        LanguageEmbedding<T>[] byESL;
        LanguageEmbedding<T> e;
        LanguageEmbedding<T>[][] arrlanguageEmbedding = ce = joinSections ? this.cachedJoinSectionsEmbeddings : this.cachedEmbeddings;
        if (ce == null || startSkipLength >= ce.length) {
            if (startSkipLength > 10) {
                return this.createEmbedding(startSkipLength, endSkipLength, joinSections);
            }
            LanguageEmbedding[][] tmp = new LanguageEmbedding[startSkipLength + 1][];
            if (ce != null) {
                System.arraycopy(ce, 0, tmp, 0, ce.length);
            }
            ce = tmp;
            if (joinSections) {
                this.cachedJoinSectionsEmbeddings = ce;
            } else {
                this.cachedEmbeddings = ce;
            }
        }
        if ((byESL = ce[startSkipLength]) == null || endSkipLength >= byESL.length) {
            if (endSkipLength > 10) {
                return this.createEmbedding(startSkipLength, endSkipLength, joinSections);
            }
            LanguageEmbedding[] tmp = new LanguageEmbedding[endSkipLength + 1];
            if (byESL != null) {
                System.arraycopy(byESL, 0, tmp, 0, byESL.length);
            }
            byESL = tmp;
            ce[startSkipLength] = byESL;
        }
        if ((e = byESL[endSkipLength]) == null) {
            byESL[endSkipLength] = e = this.createEmbedding(startSkipLength, endSkipLength, joinSections);
        }
        return e;
    }

    private LanguageEmbedding<T> createEmbedding(int startSkipLength, int endSkipLength, boolean joinSections) {
        return LexerSpiPackageAccessor.get().createLanguageEmbedding(this.language, startSkipLength, endSkipLength, joinSections);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<LanguagePath> languagePaths() {
        Set<LanguagePath> lps;
        LanguageOperation languageOperation = this;
        synchronized (languageOperation) {
            lps = this.languagePaths;
        }
        if (lps == null) {
            lps = new HashSet<LanguagePath>();
            Set<LanguagePath> existingLps = Collections.emptySet();
            HashSet exploredLangs = new HashSet();
            LanguageOperation.findLanguagePaths(existingLps, lps, exploredLangs, LanguagePath.get(this.language));
            LanguageOperation languageOperation2 = this;
            synchronized (languageOperation2) {
                this.languagePaths = lps;
                this.exploredLanguages = exploredLangs;
            }
        }
        return lps;
    }

    public Set<Language<?>> exploredLanguages() {
        this.languagePaths();
        return this.exploredLanguages;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        LanguageOperation languageOperation = this;
        synchronized (languageOperation) {
            this.languagePaths = null;
            this.exploredLanguages = null;
        }
    }

    private final TokenValidator<T> nullValidator() {
        return NULL_VALIDATOR;
    }

    private final TokenValidator<T>[] allocateTokenValidatorArray(int length) {
        return new TokenValidator[length];
    }

    private static final class FlyItem<T extends TokenId> {
        private TextToken<T> token;
        private TextToken<T> token2;

        FlyItem(WrapTokenId<T> wid, String text) {
            this.newToken(wid, text);
            this.token2 = this.token;
        }

        TextToken<T> flyToken(WrapTokenId<T> wid, String text) {
            if (this.token.text() != text) {
                if (this.token2.text() == text) {
                    this.swap();
                } else if (!CharSequenceUtilities.textEquals((CharSequence)this.token.text(), (CharSequence)text)) {
                    if (!CharSequenceUtilities.textEquals((CharSequence)this.token2.text(), (CharSequence)text)) {
                        this.token2 = this.token;
                        this.newToken(wid, text);
                    } else {
                        this.swap();
                    }
                }
            }
            return this.token;
        }

        void newToken(WrapTokenId<T> wid, String text) {
            this.token = new TextToken<T>(wid, text);
            this.token.makeFlyweight();
        }

        private void swap() {
            TextToken<T> tmp = this.token;
            this.token = this.token2;
            this.token2 = tmp;
        }
    }

}

