/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class StackElementArray {
    private final StackTraceElement[] stackTrace;
    private final int hashCode;

    public static Set<StackElementArray> createSet() {
        return Collections.synchronizedSet(new HashSet());
    }

    public static boolean addStackIfNew(Set<StackElementArray> stacks, int stackCompareSize) {
        StackTraceElement[] stackElems = new Exception().getStackTrace();
        int startIndex = 2;
        int endIndex = Math.min(stackElems.length, startIndex + stackCompareSize);
        StackTraceElement[] compareElems = new StackTraceElement[endIndex - startIndex];
        System.arraycopy(stackElems, startIndex, compareElems, 0, endIndex - startIndex);
        StackElementArray stackElementArray = new StackElementArray(compareElems);
        if (!stacks.contains(stackElementArray)) {
            stacks.add(stackElementArray);
            return true;
        }
        return false;
    }

    private StackElementArray(StackTraceElement[] stackTrace) {
        this.stackTrace = stackTrace;
        int hc = 0;
        for (int i = 0; i < stackTrace.length; ++i) {
            hc ^= stackTrace[i].hashCode();
        }
        this.hashCode = hc;
    }

    int length() {
        return this.stackTrace.length;
    }

    StackTraceElement element(int i) {
        return this.stackTrace[i];
    }

    public int hashCode() {
        return this.hashCode;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof StackElementArray)) {
            return false;
        }
        StackElementArray sea = (StackElementArray)obj;
        if (sea.length() != this.length()) {
            return false;
        }
        for (int i = 0; i < this.stackTrace.length; ++i) {
            if (this.element(i).equals(sea.element(i))) continue;
            return false;
        }
        return true;
    }
}

