/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.lib.lexer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.LanguageProvider;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class LanguageManager
extends LanguageProvider
implements LookupListener,
PropertyChangeListener {
    private static Language<TokenId> NO_LANG = null;
    private static LanguageEmbedding<TokenId> NO_LANG_EMBEDDING = null;
    private static LanguageManager instance = null;
    private Lookup.Result<LanguageProvider> lookupResult = Lookup.getDefault().lookup(new Lookup.Template(LanguageProvider.class));
    private List<LanguageProvider> providers = Collections.emptyList();
    private HashMap<String, WeakReference<Language<?>>> langCache = new HashMap();
    private WeakHashMap<Token, LanguageEmbedding<?>> tokenLangCache = new WeakHashMap();
    private final String LOCK = new String("LanguageManager.LOCK");

    private static Language<TokenId> NO_LANG() {
        if (NO_LANG == null) {
            NO_LANG = new LanguageHierarchy<TokenId>(){

                @Override
                protected Lexer<TokenId> createLexer(LexerRestartInfo<TokenId> info) {
                    return null;
                }

                @Override
                protected Collection<TokenId> createTokenIds() {
                    return Collections.emptyList();
                }

                @Override
                protected String mimeType() {
                    return "obscure/no-language-marker";
                }
            }.language();
        }
        return NO_LANG;
    }

    private static LanguageEmbedding<TokenId> NO_LANG_EMBEDDING() {
        if (NO_LANG_EMBEDDING == null) {
            NO_LANG_EMBEDDING = LanguageEmbedding.create(LanguageManager.NO_LANG(), 0, 0);
        }
        return NO_LANG_EMBEDDING;
    }

    public static synchronized LanguageManager getInstance() {
        if (instance == null) {
            instance = new LanguageManager();
        }
        return instance;
    }

    private LanguageManager() {
        this.lookupResult.addLookupListener((LookupListener)this);
        this.refreshProviders();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Language<?> findLanguage(String mimeType) {
        assert (mimeType != null);
        if (mimeType.startsWith("test")) {
            int idx = mimeType.indexOf(95);
            assert (idx != -1);
            mimeType = mimeType.substring(idx + 1);
        }
        String idx = this.LOCK;
        synchronized (idx) {
            Language lang;
            WeakReference ref = this.langCache.get(mimeType);
            Language language = lang = ref == null ? null : ref.get();
            if (lang == null) {
                LanguageProvider p;
                Iterator<LanguageProvider> i$ = this.providers.iterator();
                while (i$.hasNext() && null == (lang = (p = i$.next()).findLanguage(mimeType))) {
                }
                if (lang == null) {
                    lang = LanguageManager.NO_LANG();
                }
                this.langCache.put(mimeType, new WeakReference(lang));
            }
            return lang == LanguageManager.NO_LANG() ? null : lang;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public LanguageEmbedding<?> findLanguageEmbedding(Token<?> token, LanguagePath languagePath, InputAttributes inputAttributes) {
        String string = this.LOCK;
        synchronized (string) {
            LanguageEmbedding lang = this.tokenLangCache.get(token);
            if (lang == null) {
                LanguageProvider p;
                Iterator<LanguageProvider> i$ = this.providers.iterator();
                while (i$.hasNext() && null == (lang = (p = i$.next()).findLanguageEmbedding(token, languagePath, inputAttributes))) {
                }
                if (lang == null) {
                    lang = LanguageManager.NO_LANG_EMBEDDING();
                }
                this.tokenLangCache.put(token, lang);
            }
            return lang == LanguageManager.NO_LANG_EMBEDDING() ? null : lang;
        }
    }

    public void resultChanged(LookupEvent ev) {
        this.refreshProviders();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == null) {
            String string = this.LOCK;
            synchronized (string) {
                this.langCache.clear();
                this.tokenLangCache.clear();
            }
        }
        if ("LanguageProvider.PROP_LANGUAGE".equals(evt.getPropertyName())) {
            String string = this.LOCK;
            synchronized (string) {
                this.langCache.clear();
            }
        }
        if ("LanguageProvider.PROP_EMBEDDED_LANGUAGE".equals(evt.getPropertyName())) {
            String string = this.LOCK;
            synchronized (string) {
                this.tokenLangCache.clear();
            }
        }
        this.firePropertyChange(evt.getPropertyName());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void refreshProviders() {
        Collection newProviders = this.lookupResult.allInstances();
        String string = this.LOCK;
        synchronized (string) {
            for (LanguageProvider p2 : this.providers) {
                p2.removePropertyChangeListener(this);
            }
            this.providers = new ArrayList<LanguageProvider>(newProviders);
            for (LanguageProvider p2 : this.providers) {
                p2.addPropertyChangeListener(this);
            }
            this.langCache.clear();
            this.tokenLangCache.clear();
        }
    }

}

