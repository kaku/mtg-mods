/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.lib.lexer.inc;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.EmbeddedJoinInfo;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinLexerInputOperation;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.LAState;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.JoinTokenListChange;
import org.netbeans.lib.lexer.inc.MutableJoinLexerInputOperation;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.inc.TokenListListUpdate;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.lib.lexer.token.PartToken;

public final class TokenListUpdater {
    private static final Logger LOG = Logger.getLogger(TokenListUpdater.class.getName());

    static <T extends TokenId> void updateRegular(TokenListChange<T> change, TokenHierarchyEventInfo eventInfo) {
        boolean relex;
        int lookahead;
        MutableTokenList<T> tokenList = change.tokenList();
        int tokenCount = tokenList.tokenCountCurrent();
        boolean loggable = LOG.isLoggable(Level.FINE);
        if (loggable) {
            TokenListUpdater.logModification(tokenList, eventInfo, false);
        }
        int[] indexAndTokenOffset = LexerUtilsConstants.tokenIndexBinSearch(tokenList, eventInfo.modOffset(), tokenCount);
        int relexIndex = indexAndTokenOffset[0];
        int relexOffset = indexAndTokenOffset[1];
        if (relexIndex == -1) {
            relexIndex = 0;
            relexOffset = tokenList.startOffset();
        }
        int matchIndex = relexIndex;
        int matchOffset = relexOffset;
        if (relexIndex == tokenCount) {
            if (!tokenList.isFullyLexed() && eventInfo.modOffset() >= relexOffset + (relexIndex > 0 ? tokenList.lookahead(relexIndex - 1) : 0)) {
                if (loggable) {
                    LOG.log(Level.FINE, "UPDATE-REGULAR FINISHED: Not fully lexed yet. rOff=" + relexOffset + ", modOff=" + eventInfo.modOffset() + "\n");
                }
                change.setIndex(relexIndex);
                change.setOffset(relexOffset);
                change.setMatchIndex(matchIndex);
                change.setMatchOffset(matchOffset);
                tokenList.replaceTokens(change, eventInfo, true);
                return;
            }
            if (tokenList.isFullyLexed()) {
                matchOffset = Integer.MAX_VALUE;
            }
        } else {
            if (eventInfo.removedLength() > 0) {
                matchOffset += tokenList.tokenOrEmbeddingDirect(matchIndex++).token().length();
                int removedEndOffset = eventInfo.modOffset() + eventInfo.removedLength();
                while (matchOffset < removedEndOffset && matchIndex < tokenCount) {
                    matchOffset += tokenList.tokenOrEmbeddingDirect(matchIndex++).token().length();
                }
            } else if (matchOffset < eventInfo.modOffset()) {
                matchOffset += tokenList.tokenOrEmbeddingDirect(matchIndex++).token().length();
            }
            matchOffset += eventInfo.diffLength();
        }
        while (relexIndex > 0 && relexOffset + tokenList.lookahead(relexIndex - 1) > eventInfo.modOffset()) {
            --relexIndex;
            if (loggable) {
                LOG.log(Level.FINE, "    Token at reInd=" + relexIndex + " affected (la=" + tokenList.lookahead(relexIndex) + ") => relex it\n");
            }
            AbstractToken<T> token = tokenList.tokenOrEmbeddingDirect(relexIndex).token();
            relexOffset -= token.length();
        }
        Object relexState = relexIndex > 0 ? tokenList.state(relexIndex - 1) : null;
        change.setIndex(relexIndex);
        change.setOffset(relexOffset);
        change.setMatchIndex(matchIndex);
        change.setMatchOffset(matchOffset);
        boolean bl = relex = relexOffset != matchOffset || eventInfo.insertedLength() > 0 || matchIndex == 0 || !LexerUtilsConstants.statesEqual(relexState, tokenList.state(matchIndex - 1));
        if (!relex && (lookahead = tokenList.lookahead(matchIndex - 1)) > 1 && matchIndex < tokenCount) {
            boolean bl2 = relex = lookahead > tokenList.tokenOrEmbeddingDirect(matchIndex).token().length();
        }
        if (loggable) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("  BEFORE-RELEX:\n");
            sb.append("  relex=").append(relex);
            sb.append(", reInd=").append(relexIndex).append(", reOff=").append(relexOffset);
            sb.append(", reSta=").append(relexState).append('\n');
            sb.append("  maInd=").append(matchIndex).append(", maOff=").append(matchOffset);
            sb.append('\n');
            LOG.log(Level.FINE, sb.toString());
        }
        assert (relexIndex >= 0);
        if (relex) {
            if (relexOffset < 0) {
                TokenListUpdater.logModification(tokenList, eventInfo, false);
                LOG.info("relexIndex=" + relexIndex + ", relexOffset=" + relexOffset + ", relexState=" + relexState + ", indexAndTokenOffset: [" + indexAndTokenOffset[0] + ", " + indexAndTokenOffset[1] + "]\n");
                LOG.info("\n\n" + eventInfo.modificationDescription(true) + "\n");
            }
            LexerInputOperation<T> lexerInputOperation = tokenList.createLexerInputOperation(relexIndex, relexOffset, relexState);
            TokenListUpdater.relex(change, lexerInputOperation, tokenCount);
        }
        tokenList.replaceTokens(change, eventInfo, true);
        if (loggable) {
            LOG.log(Level.FINE, "\nTLChange: " + change + "\nMods:" + change.toStringMods(4) + "UPDATE-REGULAR FINISHED\n");
        }
    }

    static <T extends TokenId> void updateJoined(JoinTokenListChange<T> change, TokenHierarchyEventInfo eventInfo) {
        boolean checkPrevTokenListJoined;
        int matchOffset;
        int relexOffset;
        int relModOffset;
        int relexLocalIndex;
        int relexIndex;
        int relexTokenListIndex;
        int matchIndex;
        JoinTokenList jtl = (JoinTokenList)change.tokenList();
        TokenListListUpdate<T> tokenListListUpdate = change.tokenListListUpdate();
        int tokenCount = jtl.tokenCountCurrent();
        boolean loggable = LOG.isLoggable(Level.FINE);
        if (loggable) {
            TokenListUpdater.logModification(jtl, eventInfo, true);
        }
        int modOffset = eventInfo.modOffset();
        boolean relex = true;
        int modTokenListIndex = tokenListListUpdate.modTokenListIndex;
        if (tokenListListUpdate.isTokenListsMod()) {
            int afterUpdateTokenListCount;
            relModOffset = 0;
            if (modTokenListIndex < jtl.tokenListCount()) {
                jtl.setActiveTokenListIndex(modTokenListIndex);
                relexIndex = jtl.activeStartJoinIndex();
            } else {
                relexIndex = jtl.tokenCountCurrent();
            }
            relexLocalIndex = 0;
            relexTokenListIndex = modTokenListIndex;
            checkPrevTokenListJoined = true;
            int modEndTokenListIndex = modTokenListIndex + tokenListListUpdate.removedTokenListCount;
            boolean lastRemovedJoined = false;
            if (modEndTokenListIndex > 0) {
                jtl.setActiveTokenListIndex(modEndTokenListIndex - 1);
                matchIndex = jtl.activeEndJoinIndex();
                matchOffset = jtl.activeTokenList().endOffset();
                if (matchOffset > modOffset) {
                    matchOffset = Math.max(matchOffset - eventInfo.removedLength(), modOffset);
                }
                if (jtl.activeTokenList().joinInfo().joinTokenLastPartShift() > 0) {
                    matchOffset = ((JoinToken)jtl.tokenOrEmbeddingDirect(matchIndex).token()).endOffset();
                    ++matchIndex;
                    lastRemovedJoined = true;
                }
            } else {
                matchIndex = 0;
                matchOffset = -1;
            }
            if (!lastRemovedJoined && tokenListListUpdate.addedTokenListCount() > 0) {
                matchOffset = tokenListListUpdate.afterUpdateTokenList(jtl, modTokenListIndex + tokenListListUpdate.addedTokenListCount() - 1).endOffset();
            }
            if (relexTokenListIndex < (afterUpdateTokenListCount = tokenListListUpdate.afterUpdateTokenListCount(jtl))) {
                relexOffset = tokenListListUpdate.afterUpdateTokenList(jtl, modTokenListIndex).startOffset();
            } else {
                relex = false;
                relexOffset = Integer.MAX_VALUE;
            }
            if (matchOffset < relexOffset && relexTokenListIndex > 0) {
                relex = false;
            }
        } else {
            jtl.setActiveTokenListIndex(modTokenListIndex);
            EmbeddedTokenList modEtl = jtl.activeTokenList();
            assert (eventInfo.insertedLength() > 0 || eventInfo.removedLength() > 0);
            assert (modOffset >= modEtl.startOffset());
            assert (modOffset + eventInfo.diffLengthOrZero() <= modEtl.endOffset());
            change.charModTokenList = modEtl;
            int[] indexAndTokenOffset = modEtl.tokenIndex(modOffset);
            relexTokenListIndex = modTokenListIndex;
            relexLocalIndex = indexAndTokenOffset[0];
            relexOffset = indexAndTokenOffset[1];
            if (relexLocalIndex == -1) {
                relexLocalIndex = 0;
                relexOffset = modEtl.startOffset();
            }
            relModOffset = modOffset - relexOffset;
            matchIndex = relexIndex = jtl.activeStartJoinIndex() + relexLocalIndex;
            matchOffset = relexOffset;
            int matchLocalIndex = relexLocalIndex;
            checkPrevTokenListJoined = false;
            int modEtlTokenCount = modEtl.tokenCountCurrent();
            if (eventInfo.removedLength() > 0) {
                ++matchIndex;
                matchOffset += modEtl.tokenOrEmbeddingDirect(matchLocalIndex++).token().length();
                int removedEndOffset = eventInfo.modOffset() + eventInfo.removedLength();
                while (matchOffset < removedEndOffset && matchLocalIndex < modEtlTokenCount) {
                    ++matchIndex;
                    matchOffset += modEtl.tokenOrEmbeddingDirect(matchLocalIndex++).token().length();
                }
            } else if (matchOffset < modOffset) {
                ++matchIndex;
                matchOffset += modEtl.tokenOrEmbeddingDirect(matchLocalIndex++).token().length();
            }
            matchOffset += eventInfo.diffLength();
            assert (relexLocalIndex <= matchLocalIndex);
            assert (matchLocalIndex <= modEtlTokenCount);
            if (matchLocalIndex == modEtlTokenCount && modEtl.joinInfo().joinTokenLastPartShift() > 0) {
                matchIndex = jtl.activeEndJoinIndex() + 1;
                JoinToken joinToken = (JoinToken)jtl.tokenOrEmbeddingDirect(matchIndex - 1).token();
                jtl.setActiveTokenListIndex(modTokenListIndex);
                matchOffset = joinToken.endOffset();
            }
            if (relexLocalIndex == 0) {
                checkPrevTokenListJoined = true;
            }
            if (relexLocalIndex == matchLocalIndex && relexLocalIndex == modEtlTokenCount && modEtl.joinInfo().joinTokenLastPartShift() > 0) {
                assert (eventInfo.removedLength() == 0);
                if (modEtlTokenCount > 0) {
                    assert (relexIndex == jtl.activeEndJoinIndex() + 1);
                    --relexIndex;
                    relexOffset = modEtl.tokenOffset(--relexLocalIndex);
                }
                if (relexLocalIndex == 0) {
                    checkPrevTokenListJoined = true;
                }
            }
        }
        if (checkPrevTokenListJoined && relexTokenListIndex > 0) {
            int lps;
            jtl.setActiveTokenListIndex(relexTokenListIndex - 1);
            while ((lps = jtl.activeTokenList().joinInfo().joinTokenLastPartShift()) > 0) {
                if (jtl.activeTokenList().tokenCountCurrent() > 0) {
                    relexLocalIndex = jtl.activeTokenList().tokenCountCurrent() - 1;
                    relexTokenListIndex = jtl.activeTokenListIndex();
                    PartToken partToken = (PartToken)jtl.activeTokenList().tokenOrEmbeddingDirect(relexLocalIndex).token();
                    relModOffset = partToken.partTextEndOffset();
                    relexOffset = partToken.joinToken().offset(null);
                    if (partToken.partTokenIndex() != 0) {
                        relexLocalIndex = jtl.tokenStartLocalIndex(relexIndex);
                        relexTokenListIndex = jtl.activeTokenListIndex();
                    }
                    relex = true;
                    if (relexIndex != matchIndex) break;
                    ++matchIndex;
                    matchOffset = partToken.joinToken().endOffset();
                    lps = jtl.activeTokenList().joinInfo().joinTokenLastPartShift();
                    if (jtl.activeTokenListIndex() + lps != modTokenListIndex) break;
                    matchOffset += eventInfo.diffLength();
                    break;
                }
                if (jtl.activeTokenListIndex() <= 0) break;
                jtl.setPrevActiveTokenListIndex();
            }
        }
        int origRelexIndex = relexIndex;
        while (relexIndex > 0 && jtl.lookahead(relexIndex - 1) > relModOffset) {
            AbstractToken relexToken = jtl.tokenOrEmbeddingDirect(--relexIndex).token();
            relModOffset += relexToken.length();
            if (!loggable) continue;
            LOG.log(Level.FINE, "    Token at reInd=" + relexIndex + " affected (la=" + jtl.lookahead(relexIndex) + ") => relex it\n");
        }
        if (relexIndex != origRelexIndex) {
            relexOffset = jtl.tokenOffset(relexIndex);
            relexLocalIndex = jtl.tokenStartLocalIndex(relexIndex);
            relexTokenListIndex = jtl.activeTokenListIndex();
            relex = true;
        }
        change.setMatchIndex(matchIndex);
        change.setMatchOffset(matchOffset);
        Object relexState = relexIndex > 0 ? jtl.state(relexIndex - 1) : null;
        MutableJoinLexerInputOperation<T> lexerInputOperation = null;
        if (loggable) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("  BEFORE-RELEX:\n");
            sb.append("  relex=").append(relex);
            sb.append(", reInd=").append(relexIndex).append(", reOff=").append(relexOffset);
            sb.append(", reSta=").append(relexState).append('\n');
            sb.append(", maInd=").append(matchIndex).append(", maOff=").append(matchOffset);
            sb.append('\n');
            LOG.log(Level.FINE, sb.toString());
        }
        change.setIndex(relexIndex);
        change.setOffset(relexOffset);
        if (relex) {
            lexerInputOperation = new MutableJoinLexerInputOperation<T>(jtl, relexIndex, relexState, relexTokenListIndex, relexOffset, tokenListListUpdate);
            lexerInputOperation.init();
            change.setStartInfo(lexerInputOperation, relexLocalIndex);
            TokenListUpdater.relex(change, lexerInputOperation, tokenCount);
        } else {
            change.setNoRelexStartInfo();
        }
        jtl.replaceTokens(change, eventInfo, true);
        if (loggable) {
            LOG.log(Level.FINE, "\nTLChange:" + change + "\nMods:" + change.toStringMods(4) + "UPDATE-JOINED FINISHED\n");
        }
    }

    private static <T extends TokenId> void relex(TokenListChange<T> change, LexerInputOperation<T> lexerInputOperation, int tokenCount) {
        int relexOffset;
        AbstractToken<T> token;
        boolean loggable = LOG.isLoggable(Level.FINE);
        MutableTokenList<T> tokenList = change.tokenList();
        int lowestMatchIndex = change.matchIndex;
        while ((token = lexerInputOperation.nextToken()) != null) {
            int matchPointOrigLookahead;
            int afterMatchPointTokenLength;
            int afterMatchPointOrigTokenLookahead;
            int lookahead = lexerInputOperation.lookahead();
            Object state = lexerInputOperation.lexerState();
            if (loggable) {
                StringBuilder sb = new StringBuilder(100);
                sb.append("    LEXED-TOKEN: ");
                int tokenEndOffset = lexerInputOperation.lastTokenEndOffset();
                CharSequence inputSourceText = tokenList.inputSourceText();
                if (tokenEndOffset > inputSourceText.length()) {
                    sb.append(tokenEndOffset).append("!! => ");
                    tokenEndOffset = inputSourceText.length();
                    sb.append(tokenEndOffset);
                }
                sb.append('\"');
                token.dumpText(sb, inputSourceText);
                sb.append('\"');
                token.dumpInfo(sb, null, false, false, 0);
                sb.append("\n");
                LOG.log(Level.FINE, sb.toString());
            }
            change.addToken(token, lookahead, state);
            relexOffset = lexerInputOperation.lastTokenEndOffset();
            if (relexOffset > change.matchOffset) {
                do {
                    if (change.matchIndex == tokenCount) {
                        if (tokenList.isFullyLexed()) {
                            change.matchOffset = Integer.MAX_VALUE;
                            break;
                        }
                        change.matchOffset = relexOffset;
                        state = tokenList.state(change.matchIndex - 1);
                        break;
                    }
                    change.increaseMatchIndex();
                } while (relexOffset > change.matchOffset);
            }
            if (relexOffset != change.matchOffset || !LexerUtilsConstants.statesEqual(state, change.matchIndex > 0 ? tokenList.state(change.matchIndex - 1) : null)) continue;
            if (change.matchIndex == tokenCount) break;
            int n = matchPointOrigLookahead = change.matchIndex > 0 ? tokenList.lookahead(change.matchIndex - 1) : 0;
            if (lookahead == matchPointOrigLookahead || matchPointOrigLookahead <= 1 && lookahead <= 1 || matchPointOrigLookahead <= (afterMatchPointTokenLength = tokenList.tokenOrEmbeddingDirect(change.matchIndex).token().length()) && lookahead <= afterMatchPointTokenLength || lookahead - afterMatchPointTokenLength <= (afterMatchPointOrigTokenLookahead = tokenList.lookahead(change.matchIndex)) && (matchPointOrigLookahead <= afterMatchPointTokenLength || lookahead >= matchPointOrigLookahead)) break;
            if (loggable) {
                LOG.log(Level.FINE, "    EXTRA-RELEX: maInd=" + change.matchIndex + ", LA=" + lookahead + "\n");
            }
            change.increaseMatchIndex();
        }
        lexerInputOperation.release();
        if (change.matchOffset != Integer.MAX_VALUE) {
            for (int lastAddedTokenIndex = change.addedTokenOrEmbeddingsCount() - 1; lastAddedTokenIndex >= 1 && change.matchIndex > lowestMatchIndex; --lastAddedTokenIndex) {
                AbstractToken<T> lastAddedToken = change.addedTokenOrEmbeddings().get(lastAddedTokenIndex).token();
                AbstractToken<T> lastRemovedToken = tokenList.tokenOrEmbeddingDirect(change.matchIndex - 1).token();
                if (lastAddedToken.id() == lastRemovedToken.id() && lastAddedToken.length() == lastRemovedToken.length() && change.laState().lookahead(lastAddedTokenIndex) == tokenList.lookahead(change.matchIndex - 1) && LexerUtilsConstants.statesEqual(change.laState().state(lastAddedTokenIndex), tokenList.state(change.matchIndex - 1))) {
                    if (loggable) {
                        LOG.log(Level.FINE, "    RETAIN-ORIGINAL at (maInd-1)=" + (change.matchIndex - 1) + ", id=" + lastRemovedToken.id() + "\n");
                    }
                    change.removeLastAddedToken();
                    relexOffset = change.addedEndOffset;
                    continue;
                }
                break;
            }
        } else {
            change.setMatchOffset(tokenList.endOffset());
        }
    }

    private static <T extends TokenId> void logModification(MutableTokenList<T> tokenList, TokenHierarchyEventInfo eventInfo, boolean updateJoined) {
        int modOffset = eventInfo.modOffset();
        int removedLength = eventInfo.removedLength();
        int insertedLength = eventInfo.insertedLength();
        CharSequence inputSourceText = tokenList.inputSourceText();
        String insertedText = "";
        if (insertedLength > 0) {
            insertedText = ", insTxt:\"" + CharSequenceUtilities.debugText((CharSequence)inputSourceText.subSequence(modOffset, modOffset + insertedLength)) + '\"';
        }
        int afterInsertOffset = modOffset + insertedLength;
        CharSequence beforeText = inputSourceText.subSequence(Math.max(afterInsertOffset - 5, 0), afterInsertOffset);
        CharSequence afterText = inputSourceText.subSequence(afterInsertOffset, Math.min(afterInsertOffset + 5, inputSourceText.length()));
        StringBuilder sb = new StringBuilder(200);
        sb.append(updateJoined ? "JOINED" : "REGULAR");
        sb.append("-UPDATE: \"");
        sb.append(tokenList.languagePath().mimePath()).append("\"\n");
        sb.append("  modOff=").append(modOffset);
        sb.append(", text-around:\"").append(beforeText).append('|');
        sb.append(afterText).append("\", insLen=");
        sb.append(insertedLength).append(insertedText);
        sb.append(", remLen=").append(removedLength);
        sb.append(", tCnt=").append(tokenList.tokenCountCurrent()).append('\n');
        LOG.log(Level.INFO, sb.toString());
    }
}

