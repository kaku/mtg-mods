/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinLexerInputOperation;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.inc.TokenListListUpdate;

class MutableJoinLexerInputOperation<T extends TokenId>
extends JoinLexerInputOperation<T> {
    private final TokenListListUpdate<T> tokenListListUpdate;

    MutableJoinLexerInputOperation(JoinTokenList<T> joinTokenList, int relexJoinIndex, Object lexerRestartState, int activeTokenListIndex, int relexOffset, TokenListListUpdate<T> tokenListListUpdate) {
        super(joinTokenList, relexJoinIndex, lexerRestartState, activeTokenListIndex, relexOffset);
        this.tokenListListUpdate = tokenListListUpdate;
    }

    @Override
    public EmbeddedTokenList<?, T> tokenList(int tokenListIndex) {
        return this.tokenListListUpdate.afterUpdateTokenList((JoinTokenList)this.tokenList, tokenListIndex);
    }

    @Override
    protected int tokenListCount() {
        return this.tokenListListUpdate.afterUpdateTokenListCount((JoinTokenList)this.tokenList);
    }

    @Override
    public String toString() {
        return JoinLexerInputOperation.super.toString() + ", tokenListListUpdate: " + this.tokenListListUpdate;
    }
}

