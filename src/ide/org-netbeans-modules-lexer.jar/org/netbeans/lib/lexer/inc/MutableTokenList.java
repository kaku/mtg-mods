/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;

public interface MutableTokenList<T extends TokenId>
extends TokenList<T> {
    public TokenOrEmbedding<T> tokenOrEmbeddingDirect(int var1);

    public LexerInputOperation<T> createLexerInputOperation(int var1, int var2, Object var3);

    public boolean isFullyLexed();

    public void replaceTokens(TokenListChange<T> var1, TokenHierarchyEventInfo var2, boolean var3);
}

