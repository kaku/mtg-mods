/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.EmbeddingOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.IncTokenList;
import org.netbeans.lib.lexer.inc.JoinTokenListChange;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.OriginalText;
import org.netbeans.lib.lexer.inc.RemovedTokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.inc.TokenListListUpdate;
import org.netbeans.lib.lexer.inc.TokenListUpdater;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.spi.lexer.LanguageEmbedding;

public final class TokenHierarchyUpdate {
    static final Logger LOG = Logger.getLogger(TokenHierarchyUpdate.class.getName());
    private static final UpdateItem<?> NO_ITEM = new UpdateItem(null, null, null);
    final TokenHierarchyEventInfo eventInfo;
    private List<List<UpdateItem<?>>> itemLevels;
    private Map<LanguagePath, UpdateItem<?>> path2Item;
    private LanguagePath lastPath2ItemPath;
    private UpdateItem<?> lastPath2ItemItem;

    public static <T extends TokenId> UpdateItem<T> createUpdateItem(TokenListChange<T> change) {
        UpdateItem updateItem = new UpdateItem(null, null, null);
        updateItem.tokenListChange = change;
        return updateItem;
    }

    public TokenHierarchyUpdate(TokenHierarchyEventInfo eventInfo) {
        this.eventInfo = eventInfo;
    }

    public void update() {
        TokenHierarchyOperation operation = this.eventInfo.tokenHierarchyOperation();
        IncTokenList incTokenList = (IncTokenList)operation.rootTokenList();
        if (LOG.isLoggable(Level.FINE)) {
            if (LOG.isLoggable(Level.FINEST)) {
                CharSequence text = incTokenList.inputSourceText();
                assert (text != null);
                incTokenList.setInputSourceText((CharSequence)((Object)this.eventInfo.originalText()));
                LOG.finest("\n\nBEFORE UPDATE:\n" + operation.toString() + '\n');
                incTokenList.setInputSourceText(text);
            }
            StringBuilder sb = new StringBuilder(150);
            sb.append("<<<<<<<<<<<<<<<<<< LEXER CHANGE START ------------------\n");
            sb.append(this.eventInfo.modificationDescription(false));
            LOG.fine(sb.toString());
        }
        this.updateImpl(incTokenList, operation.rootChildrenLanguages());
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("AFFECTED: " + this.eventInfo.dumpAffected() + "\n");
            String extraMsg = "";
            if (LOG.isLoggable(Level.FINER)) {
                String error = operation.checkConsistency();
                if (error != null) {
                    String msg = "\n!!!CONSISTENCY-ERROR!!!: " + error + "\n\n" + "INCONSISTENT TOKEN HIERARCHY:\n" + operation + "\n\n";
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine(msg);
                    }
                    throw new IllegalStateException("INCONSISTENCY in token hierarchy occurred");
                }
                extraMsg = "(TokenHierarchy Check OK) ";
            }
            LOG.fine(">>>>>>>>>>>>>>>>>> LEXER CHANGE END " + extraMsg + "------------------\n");
        }
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("\n\nAFTER UPDATE:\n" + operation.toString() + '\n');
        }
    }

    private <T extends TokenId> void updateImpl(IncTokenList<T> incTokenList, Object rootChildrenLanguages) {
        incTokenList.incrementModCount();
        this.itemLevels = new ArrayList(3);
        UpdateItem rootItem = new UpdateItem(this, null, rootChildrenLanguages);
        rootItem.tokenListChange = new TokenListChange<T>(incTokenList);
        this.addItem(rootItem, 0);
        this.processLevelInfos();
    }

    public <T extends TokenId> void updateCreateOrRemoveEmbedding(EmbeddedTokenList<?, T> addedOrRemovedTokenList, boolean add) {
        LanguagePath languagePath = addedOrRemovedTokenList.languagePath();
        int level = languagePath.size() - 1;
        this.itemLevels = new ArrayList(level + 2);
        UpdateItem<T> item = this.tokenListListItem(languagePath);
        if (item != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("THU.updateCreateOrRemoveEmbedding() add=" + add + ": " + addedOrRemovedTokenList.dumpInfo(new StringBuilder(256)));
            }
            if (add) {
                item.tokenListListUpdate.markAddedMember(addedOrRemovedTokenList);
            } else {
                item.tokenListListUpdate.markRemovedMember(addedOrRemovedTokenList, this.eventInfo);
            }
            this.processLevelInfos();
        }
    }

    private void processLevelInfos() {
        for (int i = 0; i < this.itemLevels.size(); ++i) {
            List items = this.itemLevels.get(i);
            for (UpdateItem item : items) {
                item.update();
            }
        }
    }

    void addItem(UpdateItem<?> item, int level) {
        while (level >= this.itemLevels.size()) {
            this.itemLevels.add(new ArrayList(3));
        }
        List items = this.itemLevels.get(level);
        items.add(item);
    }

    private <T extends TokenId> UpdateItem<T> tokenListListItem(LanguagePath languagePath) {
        UpdateItem item;
        if (languagePath == this.lastPath2ItemPath) {
            UpdateItem item2 = this.lastPath2ItemItem;
            return item2;
        }
        if (this.path2Item == null) {
            this.path2Item = new HashMap(4, 0.5f);
        }
        if ((item = this.path2Item.get(languagePath)) == NO_ITEM) {
            item = null;
        } else if (item == null) {
            TokenListList tokenListList = this.eventInfo.tokenHierarchyOperation().existingTokenListList(languagePath);
            if (tokenListList != null) {
                item = new UpdateItem(this, tokenListList, tokenListList.childrenLanguages());
                int level = languagePath.size() - 1;
                this.addItem(item, level);
                this.path2Item.put(languagePath, item);
            } else {
                this.path2Item.put(languagePath, NO_ITEM);
            }
        }
        this.lastPath2ItemItem = item;
        return item;
    }

    public static final class UpdateItem<T extends TokenId> {
        final TokenHierarchyUpdate update;
        final TokenListListUpdate<T> tokenListListUpdate;
        final Object childrenLanguages;
        UpdateItem<?> parentItem;
        TokenListChange<T> tokenListChange;

        public UpdateItem(TokenHierarchyUpdate update, TokenListList<T> tokenListList, Object childrenLanguages) {
            this.update = update;
            this.tokenListListUpdate = tokenListList != null ? new TokenListListUpdate<T>(tokenListList) : null;
            this.childrenLanguages = childrenLanguages;
        }

        void setParentItem(UpdateItem<?> parentItem) {
            assert (this.parentItem == null);
            this.parentItem = parentItem;
        }

        void initTokenListChange(EmbeddedTokenList<?, T> etl) {
            assert (this.tokenListChange == null);
            this.tokenListChange = this.tokenListListUpdate != null ? this.tokenListListUpdate.createTokenListChange(etl) : new TokenListChange(etl);
        }

        void update() {
            TokenHierarchyEventInfo eventInfo = this.update.eventInfo;
            if (this.tokenListChange == null) {
                assert (this.tokenListListUpdate != null);
                if (this.tokenListListUpdate.tokenListList.joinSections()) {
                    this.tokenListChange = this.tokenListListUpdate.createJoinTokenListChange();
                }
            }
            if (this.tokenListListUpdate != null && this.tokenListListUpdate.addedTokenLists == null) {
                this.tokenListListUpdate.addedTokenLists = Collections.emptyList();
            }
            if (this.tokenListChange != null) {
                JoinTokenListChange jChange;
                this.tokenListChange.setParentChangeIsBoundsChange(this.parentItem != null && this.parentItem.tokenListChange != null && this.parentItem.tokenListChange.isBoundsChange());
                if (this.tokenListChange.getClass() == JoinTokenListChange.class) {
                    jChange = (JoinTokenListChange)this.tokenListChange;
                    assert (this.tokenListListUpdate != null);
                    assert (this.tokenListListUpdate.modTokenListIndex != -1);
                    jChange.setTokenListListUpdate(this.tokenListListUpdate);
                    TokenListUpdater.updateJoined(jChange, eventInfo);
                } else {
                    TokenListUpdater.updateRegular(this.tokenListChange, eventInfo);
                    if (this.parentItem == null) {
                        eventInfo.setTokenChangeInfo(this.tokenListChange.tokenChangeInfo());
                    }
                }
                if (this.tokenListChange.isBoundsChange()) {
                    TokenListChange<T> change;
                    if (this.tokenListChange.getClass() == JoinTokenListChange.class) {
                        JoinTokenListChange jChange2 = (JoinTokenListChange)this.tokenListChange;
                        assert (jChange2.relexChanges().size() == 1);
                        change = jChange2.relexChanges().get(0);
                    } else {
                        change = this.tokenListChange;
                    }
                    Object attemptEmbeddingLanguages = this.processBoundsChange(change);
                    if (attemptEmbeddingLanguages != null) {
                        this.collectAddedEmbeddings(change, attemptEmbeddingLanguages);
                    }
                } else {
                    eventInfo.setMinAffectedStartOffset(this.tokenListChange.offset());
                    eventInfo.setMaxAffectedEndOffset(this.tokenListChange.addedEndOffset());
                    if (this.childrenLanguages != null) {
                        if (this.tokenListChange.getClass() == JoinTokenListChange.class) {
                            jChange = (JoinTokenListChange)this.tokenListChange;
                            jChange.collectAddedRemovedEmbeddings(this);
                        } else {
                            this.collectRemovedEmbeddings(this.tokenListChange);
                            this.collectAddedEmbeddings(this.tokenListChange);
                        }
                    }
                }
            } else if (this.tokenListListUpdate != null) {
                this.tokenListListUpdate.replaceTokenLists();
                this.tokenListListUpdate.collectRemovedEmbeddings(this);
                this.tokenListListUpdate.collectAddedEmbeddings(this);
            }
            if (this.parentItem != null && this.tokenListChange != null) {
                UpdateItem parent = this.parentItem;
                while (parent.tokenListChange == null) {
                    parent = parent.parentItem;
                }
                assert (parent != null);
                parent.tokenListChange.tokenChangeInfo().addEmbeddedChange(this.tokenListChange.tokenChangeInfo());
            }
        }

        Object processBoundsChange(TokenListChange<T> change) {
            Object attemptEmbeddingLanguages = this.childrenLanguages;
            RemovedTokenList<T> removedTokenList = change.tokenChangeInfo().removedTokenList();
            TokenOrEmbedding<T> t = removedTokenList.tokenOrEmbedding(0);
            int tokenStartOffset = removedTokenList.tokenOffset(t.token());
            EmbeddedTokenList etl = t.embedding();
            if (etl != null) {
                AbstractToken<T> addedToken = change.addedTokenOrEmbeddings().get(0).token();
                int rootModCount = etl.rootTokenList().modCount();
                etl.reinitChain(addedToken, tokenStartOffset, rootModCount);
                MutableTokenList<T> tokenList = change.tokenList();
                int index = change.index();
                tokenList.setTokenOrEmbedding(index, etl);
                TokenHierarchyEventInfo eventInfo = this.update.eventInfo;
                int modRelOffset = eventInfo.modOffset() - change.offset();
                int beyondModLength = change.addedEndOffset() - (eventInfo.modOffset() + eventInfo.diffLengthOrZero());
                EmbeddedTokenList prevEtl = null;
                do {
                    if (this.processBoundsChangeEmbeddedTokenList(etl, modRelOffset, beyondModLength)) {
                        Language lang;
                        if (attemptEmbeddingLanguages != null && LexerUtilsConstants.languageOrArrayContains(attemptEmbeddingLanguages, lang = etl.language())) {
                            attemptEmbeddingLanguages = LexerUtilsConstants.languageOrArraySize(attemptEmbeddingLanguages) == 1 ? null : LexerUtilsConstants.languageOrArrayRemove(attemptEmbeddingLanguages, lang);
                        }
                        prevEtl = etl;
                        etl = prevEtl.nextEmbeddedTokenList();
                        continue;
                    }
                    EmbeddedTokenList next = etl.nextEmbeddedTokenList();
                    if (prevEtl == null) {
                        if (next == null) {
                            tokenList.setTokenOrEmbedding(index, addedToken);
                        } else {
                            tokenList.setTokenOrEmbedding(index, next);
                        }
                    } else {
                        prevEtl.setNextEmbeddedTokenList(next);
                    }
                    etl.setNextEmbeddedTokenList(null);
                    etl = next;
                } while (etl != null);
                return attemptEmbeddingLanguages;
            }
            return this.childrenLanguages;
        }

        private <ET extends TokenId> boolean processBoundsChangeEmbeddedTokenList(EmbeddedTokenList<?, ET> etl, int modRelOffset, int beyondModLength) {
            UpdateItem<ET> childItem;
            UpdateItem<ET> updateItem = childItem = this.childrenLanguages != null ? this.update.tokenListListItem(etl.languagePath()) : null;
            if (modRelOffset >= etl.languageEmbedding().startSkipLength() && beyondModLength >= etl.languageEmbedding().endSkipLength()) {
                if (childItem == null) {
                    childItem = new UpdateItem<ET>(this.update, null, null);
                    int level = etl.languagePath().size() - 1;
                    this.update.addItem(childItem, level);
                } else {
                    childItem.tokenListListUpdate.markChangedMember(etl);
                }
                childItem.setParentItem(this);
                childItem.initTokenListChange(etl);
                return true;
            }
            if (childItem != null) {
                childItem.tokenListListUpdate.markRemovedMember(etl, this.update.eventInfo);
            }
            return false;
        }

        void collectRemovedEmbeddings(TokenListChange<?> change) {
            RemovedTokenList removedTokenList = change.tokenChangeInfo().removedTokenList();
            if (removedTokenList != null) {
                this.collectRemovedEmbeddings(removedTokenList);
            }
        }

        void collectRemovedEmbeddings(TokenList<?> removedTokenList) {
            int tokenCount = removedTokenList.tokenCountCurrent();
            for (int i = 0; i < tokenCount; ++i) {
                EmbeddedTokenList etl = removedTokenList.tokenOrEmbedding(i).embedding();
                if (etl == null) continue;
                do {
                    int rootModCount = etl.rootTokenList().modCount();
                    etl.updateModCount(rootModCount);
                    this.internalMarkRemovedMember(etl);
                } while ((etl = etl.nextEmbeddedTokenList()) != null);
            }
        }

        void collectAddedEmbeddings(TokenListChange<?> change) {
            this.collectAddedEmbeddings(change, this.childrenLanguages);
        }

        void collectAddedEmbeddings(TokenListChange<?> change, Object attemptLanguages) {
            MutableTokenList currentTokenList = change.tokenList();
            this.collectAddedEmbeddings(currentTokenList, change.index(), change.addedTokenOrEmbeddingsCount(), attemptLanguages);
        }

        void collectAddedEmbeddings(TokenList<?> tokenList, int index, int addedCount, Object attemptLanguages) {
            int attemptLanguagesSize = LexerUtilsConstants.languageOrArraySize(attemptLanguages);
            if (attemptLanguagesSize > 0) {
                for (int i = 0; i < addedCount; ++i) {
                    for (int j = 0; j < attemptLanguagesSize; ++j) {
                        Language attemptLanguage = LexerUtilsConstants.languageOrArrayGet(attemptLanguages, j);
                        EmbeddedTokenList etl = EmbeddingOperation.embeddedTokenList(tokenList, index + i, attemptLanguage, false);
                        if (etl == null) continue;
                        this.internalMarkAddedMember(etl);
                    }
                }
            }
        }

        private <ET extends TokenId> void internalMarkRemovedMember(EmbeddedTokenList<?, ET> etl) {
            UpdateItem item = this.update.tokenListListItem(etl.languagePath());
            if (item != null) {
                item.tokenListListUpdate.markRemovedMember(etl, this.update.eventInfo);
                if (item.parentItem == null) {
                    item.setParentItem(this);
                }
            }
        }

        private <ET extends TokenId> void internalMarkAddedMember(EmbeddedTokenList<?, ET> etl) {
            UpdateItem item = this.update.tokenListListItem(etl.languagePath());
            if (item != null) {
                item.tokenListListUpdate.markAddedMember(etl);
                if (item.parentItem == null) {
                    item.setParentItem(this);
                }
            }
        }
    }

}

