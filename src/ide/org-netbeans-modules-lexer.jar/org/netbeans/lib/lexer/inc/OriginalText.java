/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.AbstractCharSequence
 *  org.netbeans.lib.editor.util.AbstractCharSequence$StringLike
 */
package org.netbeans.lib.lexer.inc;

import org.netbeans.lib.editor.util.AbstractCharSequence;

public final class OriginalText
extends AbstractCharSequence.StringLike {
    private final CharSequence currentText;
    private final int offset;
    private final int insertedTextLength;
    private final CharSequence removedText;
    private final int origLength;

    public OriginalText(CharSequence currentText, int offset, CharSequence removedText, int insertedTextLength) {
        this.currentText = currentText;
        this.offset = offset;
        this.removedText = removedText != null ? removedText : "";
        this.insertedTextLength = insertedTextLength;
        this.origLength = currentText.length() - insertedTextLength + this.removedText.length();
    }

    public int length() {
        return this.origLength;
    }

    public char charAt(int index) {
        if (index < this.offset) {
            return this.currentText.charAt(index);
        }
        if ((index -= this.offset) < this.removedText.length()) {
            return this.removedText.charAt(index);
        }
        return this.currentText.charAt(this.offset + index - this.removedText.length() + this.insertedTextLength);
    }

    public char[] toCharArray(int start, int end) {
        int bound;
        char[] chars = new char[end - start];
        int charsIndex = 0;
        if (start < this.offset) {
            int n = bound = end < this.offset ? end : this.offset;
            while (start < bound) {
                chars[charsIndex++] = this.currentText.charAt(start++);
            }
            if (end == bound) {
                return chars;
            }
        }
        end -= this.offset;
        bound = this.removedText.length();
        if ((start -= this.offset) < bound) {
            if (end < bound) {
                bound = end;
            }
            while (start < bound) {
                chars[charsIndex++] = this.removedText.charAt(start++);
            }
            if (end == bound) {
                return chars;
            }
        }
        bound = this.offset - this.removedText.length() + this.insertedTextLength;
        start += bound;
        while (start < (bound += end)) {
            chars[charsIndex++] = this.currentText.charAt(start++);
        }
        return chars;
    }
}

