/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedJoinInfo;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinLexerInputOperation;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.RemovedTokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyUpdate;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.inc.TokenListListUpdate;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.lib.lexer.token.PartToken;

public final class JoinTokenListChange<T extends TokenId>
extends TokenListChange<T> {
    EmbeddedTokenList<?, T> charModTokenList;
    private TokenListListUpdate<T> tokenListListUpdate;
    private int relexTokenListIndex;
    private List<RelexTokenListChange<T>> relexChanges;
    private RelexTokenListChange<T> lastRelexChange;
    private JoinLexerInputOperation<T> joinLexerInputOperation;

    public JoinTokenListChange(JoinTokenList<T> tokenList) {
        super(tokenList);
    }

    public List<? extends TokenListChange<T>> relexChanges() {
        return this.relexChanges;
    }

    public TokenListListUpdate<T> tokenListListUpdate() {
        return this.tokenListListUpdate;
    }

    public void setTokenListListUpdate(TokenListListUpdate<T> tokenListListUpdate) {
        this.tokenListListUpdate = tokenListListUpdate;
    }

    public void setStartInfo(JoinLexerInputOperation<T> joinLexerInputOperation, int localIndex) {
        this.joinLexerInputOperation = joinLexerInputOperation;
        this.relexTokenListIndex = joinLexerInputOperation.activeTokenListIndex();
        this.relexChanges = new ArrayList<RelexTokenListChange<T>>(this.tokenListListUpdate.addedTokenListCount() + 3);
        this.lastRelexChange = new RelexTokenListChange<T>(joinLexerInputOperation.tokenList(this.relexTokenListIndex));
        this.lastRelexChange.setIndex(localIndex);
        int relexOffset = joinLexerInputOperation.lastTokenEndOffset();
        this.lastRelexChange.setOffset(relexOffset);
        this.lastRelexChange.setMatchOffset(relexOffset);
        this.lastRelexChange.setParentChangeIsBoundsChange(this.parentChangeIsBoundsChange);
        this.relexChanges.add(this.lastRelexChange);
    }

    public void setNoRelexStartInfo() {
        this.relexTokenListIndex = this.tokenListListUpdate.modTokenListIndex;
        this.relexChanges = new ArrayList<RelexTokenListChange<T>>(1);
    }

    @Override
    public void addToken(AbstractToken<T> token, int lookahead, Object state) {
        int skipTokenListCount = this.joinLexerInputOperation.skipTokenListCount();
        if (skipTokenListCount > 0) {
            while (--skipTokenListCount >= 0) {
                this.lastRelexChange.finish();
                this.addRelexChange();
            }
            this.joinLexerInputOperation.clearSkipTokenListCount();
        }
        if (token.getClass() == JoinToken.class) {
            JoinToken joinToken = (JoinToken)token;
            List joinedParts = joinToken.joinedParts();
            int extraTokenListSpanCount = joinToken.extraTokenListSpanCount();
            int joinedPartIndex = 0;
            for (int i = 0; i < extraTokenListSpanCount; ++i) {
                this.lastRelexChange.joinTokenLastPartShift = extraTokenListSpanCount - i;
                if (((EmbeddedTokenList)this.lastRelexChange.tokenList()).textLength() > 0) {
                    PartToken partToken = joinedParts.get(joinedPartIndex++);
                    this.lastRelexChange.addToken(partToken, 0, null);
                }
                this.addRelexChange();
            }
            token = joinedParts.get(joinedPartIndex);
        }
        this.lastRelexChange.addToken(token, lookahead, state);
        this.addedEndOffset = this.lastRelexChange.addedEndOffset;
        this.tokenChangeInfo().updateAddedTokenCount(1);
    }

    private void addRelexChange() {
        EmbeddedTokenList etl = this.joinLexerInputOperation.tokenList(this.relexTokenListIndex + this.relexChanges.size());
        this.lastRelexChange = new RelexTokenListChange<T>(etl);
        int startOffset = etl.startOffset();
        this.lastRelexChange.setOffset(startOffset);
        this.lastRelexChange.setParentChangeIsBoundsChange(this.parentChangeIsBoundsChange);
        this.relexChanges.add(this.lastRelexChange);
    }

    @Override
    public int increaseMatchIndex() {
        EmbeddedTokenList etl;
        JoinTokenList jtl = (JoinTokenList)this.tokenList();
        AbstractToken token = jtl.tokenOrEmbeddingDirect(this.matchIndex).token();
        this.matchOffset = token.getClass() == JoinToken.class ? ((JoinToken)token).endOffset() : (this.matchIndex == jtl.activeStartJoinIndex() ? ((etl = jtl.activeTokenList()) != this.charModTokenList ? etl.startOffset() + token.length() : (this.matchOffset += token.length())) : (this.matchOffset += token.length()));
        ++this.matchIndex;
        return this.matchOffset;
    }

    @Override
    public AbstractToken<T> removeLastAddedToken() {
        AbstractToken<T> lastRemovedToken = this.lastRelexChange.removeLastAddedToken();
        if (lastRemovedToken.getClass() == PartToken.class) {
            int extraCount = ((PartToken)lastRemovedToken).joinToken().extraTokenListSpanCount();
            for (int i = extraCount - 1; i >= 0; --i) {
                this.relexChanges.remove(this.relexChanges.size() - 1);
            }
            this.lastRelexChange = this.relexChanges.get(this.relexChanges.size() - 1);
            lastRemovedToken = this.lastRelexChange.removeLastAddedToken();
        }
        if (this.lastRelexChange.addedTokenOrEmbeddings().size() == 0) {
            this.relexChanges.remove(this.relexChanges.size() - 1);
        }
        this.addedEndOffset = this.lastRelexChange.addedEndOffset;
        this.tokenChangeInfo().updateAddedTokenCount(-1);
        return lastRemovedToken;
    }

    void replaceTokenLists() {
        JoinTokenList jtl = (JoinTokenList)this.tokenList();
        jtl.moveIndexGap(this.tokenListListUpdate.modTokenListIndex + this.tokenListListUpdate.removedTokenListCount);
        this.tokenListListUpdate.replaceTokenLists();
        jtl.tokenListsModified(this.tokenListListUpdate.modTokenListCountDiff());
    }

    public void replaceTokens(TokenHierarchyEventInfo eventInfo) {
        int i;
        int joinTokenIndex;
        int matchTokenListIndex;
        TokenListChange change;
        int localMatchIndex;
        JoinTokenList jtl = (JoinTokenList)this.tokenList();
        if (this.matchIndex == jtl.tokenCount()) {
            localMatchIndex = 0;
            matchTokenListIndex = jtl.tokenListCount();
        } else {
            localMatchIndex = jtl.tokenStartLocalIndex(this.matchIndex);
            matchTokenListIndex = jtl.activeTokenListIndex();
        }
        int removedEndTokenListIndex = this.tokenListListUpdate.modTokenListIndex + this.tokenListListUpdate.removedTokenListCount;
        if (matchTokenListIndex < removedEndTokenListIndex) {
            matchTokenListIndex = removedEndTokenListIndex;
            localMatchIndex = 0;
        }
        int afterUpdateMatchEndIndex = matchTokenListIndex + this.tokenListListUpdate.modTokenListCountDiff();
        if (localMatchIndex != 0) {
            ++afterUpdateMatchEndIndex;
        }
        int relexChangesEndIndex = this.relexTokenListIndex + this.relexChanges.size();
        while (relexChangesEndIndex < afterUpdateMatchEndIndex) {
            RelexTokenListChange<T> change2 = new RelexTokenListChange<T>(this.tokenListListUpdate.afterUpdateTokenList(jtl, relexChangesEndIndex++));
            change2.setParentChangeIsBoundsChange(this.parentChangeIsBoundsChange);
            this.relexChanges.add(change2);
        }
        int index = afterUpdateMatchEndIndex - 1;
        if (localMatchIndex != 0 && index >= this.relexTokenListIndex) {
            change = this.relexChanges.get(index - this.relexTokenListIndex);
            change.setMatchIndex(localMatchIndex);
            --index;
        }
        while (index >= this.relexTokenListIndex) {
            change = this.relexChanges.get(index - this.relexTokenListIndex);
            change.setMatchIndex(change.tokenList().tokenCountCurrent());
            --index;
        }
        if (this.tokenListListUpdate.isTokenListsMod()) {
            this.replaceTokenLists();
        }
        jtl.moveIndexGap(this.relexTokenListIndex + this.relexChanges.size());
        if (this.relexTokenListIndex > 0) {
            EmbeddedTokenList etl = jtl.tokenList(this.relexTokenListIndex - 1);
            joinTokenIndex = etl.joinInfo().joinTokenIndex() + etl.joinTokenCount();
        } else {
            joinTokenIndex = 0;
        }
        boolean collectRemovedETLs = this.tokenListListUpdate.isTokenListsMod() && this.tokenListListUpdate.removedTokenListCount > 0;
        RemovedTokensCollector removedTokensCollector = new RemovedTokensCollector();
        for (i = 0; i < this.relexChanges.size(); ++i) {
            RelexTokenListChange<T> change3 = this.relexChanges.get(i);
            EmbeddedTokenList etl = (EmbeddedTokenList)change3.tokenList();
            if (etl.joinInfo() == null) {
                etl.setJoinInfo(new EmbeddedJoinInfo(jtl, joinTokenIndex, this.relexTokenListIndex + i));
            } else {
                etl.joinInfo().setRawJoinTokenIndex(joinTokenIndex);
            }
            if (i < this.relexChanges.size() - 1 || change3.index() + change3.removedTokenCount() == etl.tokenCountCurrent()) {
                etl.joinInfo().setJoinTokenLastPartShift(change3.joinTokenLastPartShift);
            }
            etl.replaceTokens(change3, eventInfo, etl == this.charModTokenList);
            joinTokenIndex += etl.joinTokenCount();
            if (collectRemovedETLs && i == this.tokenListListUpdate.modTokenListIndex - this.relexTokenListIndex) {
                removedTokensCollector.collectRemovedTokenLists();
                collectRemovedETLs = false;
            }
            removedTokensCollector.collectRelexChange(change3);
        }
        if (collectRemovedETLs) {
            removedTokensCollector.collectRemovedTokenLists();
        }
        removedTokensCollector.finish();
        int origJoinTokenIndex = (i += this.relexTokenListIndex) < jtl.tokenListCount() ? jtl.tokenList(i).joinInfo().joinTokenIndex() : jtl.tokenCountCurrent();
        int joinTokenCountDiff = joinTokenIndex - origJoinTokenIndex;
        jtl.updateJoinTokenCount(joinTokenCountDiff);
        if (this.relexChanges.size() == 1 && !this.tokenListListUpdate.isTokenListsMod() && this.relexChanges.get(0).isBoundsChange()) {
            this.markBoundsChange();
        }
        jtl.resetActiveAfterUpdate();
    }

    void collectAddedRemovedEmbeddings(TokenHierarchyUpdate.UpdateItem<T> updateItem) {
        RelexTokenListChange<T> change;
        int i;
        int modIndexInRelexChanges = this.tokenListListUpdate.modTokenListIndex - this.relexTokenListIndex;
        for (i = 0; i < modIndexInRelexChanges; ++i) {
            change = this.relexChanges.get(i);
            updateItem.collectRemovedEmbeddings(change);
        }
        this.tokenListListUpdate.collectRemovedEmbeddings(updateItem);
        while (i < this.relexChanges.size()) {
            change = this.relexChanges.get(i);
            updateItem.collectRemovedEmbeddings(change);
            ++i;
        }
        for (i = 0; i < this.relexChanges.size(); ++i) {
            change = this.relexChanges.get(i);
            updateItem.collectAddedEmbeddings(change);
        }
    }

    @Override
    public String toString() {
        return TokenListChange.super.toString() + "\nTLLUpdate: " + this.tokenListListUpdate + ", relexTLInd=" + this.relexTokenListIndex + ", relexChgs.size()=" + this.relexChanges.size();
    }

    @Override
    public String toStringMods(int indent) {
        StringBuilder sb = new StringBuilder(100);
        for (RelexTokenListChange<T> change : this.relexChanges) {
            sb.append(change.toStringMods(indent));
            sb.append('\n');
        }
        return sb.toString();
    }

    private final class RemovedTokensCollector {
        TokenOrEmbedding<T>[] removedTokensOrEs;
        int removedTokensIndex;
        AbstractToken<T> lastBranchToken;

        private RemovedTokensCollector() {
            this.removedTokensOrEs = new TokenOrEmbedding[JoinTokenListChange.this.removedTokenCount()];
        }

        void collectRelexChange(RelexTokenListChange<T> relexChange) {
            RemovedTokenList<T> rtl = relexChange.tokenChangeInfo().removedTokenList();
            if (rtl.tokenCount() > 0) {
                TokenOrEmbedding<T> tokenOrE = rtl.tokenOrEmbedding(0);
                if (tokenOrE.token().getClass() == PartToken.class) {
                    if (((PartToken)tokenOrE.token()).joinToken() != this.lastBranchToken) {
                        this.removedTokensOrEs[this.removedTokensIndex++] = tokenOrE;
                    }
                } else {
                    this.removedTokensOrEs[this.removedTokensIndex++] = tokenOrE;
                }
                int tokenCountM1 = rtl.tokenCount() - 1;
                System.arraycopy(rtl.tokenOrEmbeddings(), 1, this.removedTokensOrEs, this.removedTokensIndex, tokenCountM1);
                this.removedTokensIndex += tokenCountM1;
                this.lastBranchToken = rtl.tokenOrEmbedding(tokenCountM1).token();
                this.lastBranchToken = this.lastBranchToken.getClass() == PartToken.class ? ((PartToken)this.lastBranchToken).joinToken() : null;
            }
        }

        void collectRemovedTokenLists() {
            EmbeddedTokenList<?, T>[] removedTokenLists = JoinTokenListChange.this.tokenListListUpdate.removedTokenLists();
            for (int j = 0; j < removedTokenLists.length; ++j) {
                EmbeddedTokenList removedEtl = removedTokenLists[j];
                int tokenCountM1 = removedEtl.tokenCountCurrent() - 1;
                if (tokenCountM1 < 0) continue;
                TokenOrEmbedding tokenOrE = removedEtl.tokenOrEmbedding(0);
                if (tokenOrE.token().getClass() == PartToken.class) {
                    if (((PartToken)tokenOrE.token()).joinToken() != this.lastBranchToken) {
                        this.removedTokensOrEs[this.removedTokensIndex++] = tokenOrE;
                    }
                } else {
                    this.removedTokensOrEs[this.removedTokensIndex++] = tokenOrE;
                }
                removedEtl.copyElements(1, tokenCountM1 + 1, this.removedTokensOrEs, this.removedTokensIndex);
                this.removedTokensIndex += tokenCountM1;
                this.lastBranchToken = removedEtl.tokenOrEmbedding(tokenCountM1).token();
                this.lastBranchToken = this.lastBranchToken.getClass() == PartToken.class ? ((PartToken)this.lastBranchToken).joinToken() : null;
            }
        }

        void finish() {
            if (this.removedTokensIndex != this.removedTokensOrEs.length) {
                throw new IndexOutOfBoundsException("Invalid removedTokensIndex=" + this.removedTokensIndex + " != removedTokens.length=" + this.removedTokensOrEs.length);
            }
            JoinTokenListChange.this.setRemovedTokens(this.removedTokensOrEs);
        }
    }

    static final class RelexTokenListChange<T extends TokenId>
    extends TokenListChange<T> {
        int joinTokenLastPartShift;

        RelexTokenListChange(EmbeddedTokenList<?, T> tokenList) {
            super(tokenList);
        }

        void finish() {
            this.setMatchIndex(this.tokenList().tokenCountCurrent());
        }

        @Override
        public String toString() {
            return TokenListChange.super.toString() + ", lps=" + this.joinTokenLastPartShift;
        }
    }

}

