/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CompactMap
 *  org.netbeans.lib.editor.util.CompactMap$MapEntry
 */
package org.netbeans.lib.lexer.inc;

import java.util.Set;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CompactMap;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.IncTokenList;
import org.netbeans.lib.lexer.inc.RemovedTokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.TextToken;

public final class SnapshotTokenList<T extends TokenId>
implements TokenList<T> {
    private TokenHierarchyOperation<?, T> snapshot;
    private IncTokenList<T> liveTokenList;
    private int liveTokenGapStart = -1;
    private int liveTokenGapEnd;
    private int liveTokenGapStartOffset;
    private int liveTokenOffsetDiff;
    private TokenOrEmbedding<T>[] origTokenOrEmbeddings;
    private int[] origOffsets;
    private int origTokenStartIndex;
    private int origTokenCount;
    private CompactMap<AbstractToken<T>, Token2OffsetEntry<T>> token2offset;

    public int liveTokenGapStart() {
        return this.liveTokenGapStart;
    }

    public int liveTokenGapEnd() {
        return this.liveTokenGapEnd;
    }

    public SnapshotTokenList(TokenHierarchyOperation<?, T> snapshot) {
        this.snapshot = snapshot;
        this.token2offset = new CompactMap();
    }

    public TokenHierarchyOperation<?, T> snapshot() {
        return this.snapshot;
    }

    @Override
    public Language<T> language() {
        return this.liveTokenList.language();
    }

    @Override
    public LanguagePath languagePath() {
        return this.liveTokenList.languagePath();
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        if (this.liveTokenGapStart == -1 || index < this.liveTokenGapStart) {
            return this.liveTokenList.tokenOrEmbedding(index);
        }
        if ((index -= this.liveTokenGapStart) < this.origTokenCount) {
            return this.origTokenOrEmbeddings[this.origTokenStartIndex + index];
        }
        return this.liveTokenList.tokenOrEmbedding(this.liveTokenGapEnd + index - this.origTokenCount);
    }

    @Override
    public int lookahead(int index) {
        return -1;
    }

    @Override
    public Object state(int index) {
        return null;
    }

    @Override
    public int tokenOffset(int index) {
        int offset;
        if (this.liveTokenGapStart == -1 || index < this.liveTokenGapStart) {
            return this.liveTokenList.tokenOffset(index);
        }
        if ((index -= this.liveTokenGapStart) < this.origTokenCount) {
            return this.origOffsets[this.origTokenStartIndex + index];
        }
        AbstractToken<T> token = this.liveTokenList.tokenOrEmbeddingDirect(this.liveTokenGapEnd + (index -= this.origTokenCount)).token();
        if (token.isFlyweight()) {
            offset = token.length();
            while (--index >= 0) {
                token = this.liveTokenList.tokenOrEmbeddingDirect(this.liveTokenGapEnd + index).token();
                if (token.isFlyweight()) {
                    offset += token.length();
                    continue;
                }
                offset += this.tokenOffset(token, this.liveTokenList);
                break;
            }
            if (index == -1 && (index += this.liveTokenGapStart + this.origTokenCount) >= 0) {
                offset += this.tokenOffset(index);
            }
        } else {
            offset = this.tokenOffset(token, this.liveTokenList);
        }
        return offset;
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexLazyTokenCreation(this, offset);
    }

    public <TT extends TokenId> int tokenOffset(AbstractToken<TT> token, TokenList<TT> tokenList) {
        if (tokenList.getClass() == EmbeddedTokenList.class) {
            EmbeddedTokenList etl = (EmbeddedTokenList)tokenList;
            Object rootBranchToken = null;
            Token2OffsetEntry entry = (Token2OffsetEntry)((Object)this.token2offset.get((Object)rootBranchToken));
            if (entry != null) {
                return entry.offset();
            }
            int offset = etl.tokenOffset(token);
            TokenList rootTokenList = etl.rootTokenList();
            if (rootTokenList != null && rootTokenList.getClass() == IncTokenList.class && offset >= this.liveTokenGapStartOffset) {
                offset += this.liveTokenOffsetDiff;
            }
            return offset;
        }
        Token2OffsetEntry entry = (Token2OffsetEntry)((Object)this.token2offset.get(token));
        if (entry != null) {
            return entry.offset();
        }
        int offset = tokenList.tokenOffset(token);
        if (tokenList.getClass() == IncTokenList.class && offset >= this.liveTokenGapStartOffset) {
            offset += this.liveTokenOffsetDiff;
        }
        return offset;
    }

    @Override
    public int tokenCount() {
        return this.liveTokenGapStart == -1 ? this.liveTokenList.tokenCount() : this.liveTokenList.tokenCount() - (this.liveTokenGapEnd - this.liveTokenGapStart) + this.origTokenCount;
    }

    @Override
    public int tokenCountCurrent() {
        return this.liveTokenGapStart == -1 ? this.liveTokenList.tokenCountCurrent() : this.liveTokenList.tokenCountCurrent() - (this.liveTokenGapEnd - this.liveTokenGapStart) + this.origTokenCount;
    }

    @Override
    public int modCount() {
        return -1;
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        int rawOffset = token.rawOffset();
        return rawOffset;
    }

    public char charAt(int offset) {
        throw new IllegalStateException("Not expected to be called");
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        if (this.liveTokenGapStart == -1 || index < this.liveTokenGapStart) {
            this.liveTokenList.setTokenOrEmbedding(index, t);
        } else if ((index -= this.liveTokenGapStart) < this.origTokenCount) {
            this.origTokenOrEmbeddings[this.origTokenStartIndex + index] = t;
        } else {
            this.liveTokenList.setTokenOrEmbedding(this.liveTokenGapEnd + index - this.origTokenCount, t);
        }
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        AbstractToken<T> nonFlyToken;
        if (this.liveTokenGapStart == -1 || index < this.liveTokenGapStart) {
            nonFlyToken = this.liveTokenList.replaceFlyToken(index, flyToken, offset);
        } else if ((index -= this.liveTokenGapStart) < this.origTokenCount) {
            nonFlyToken = ((TextToken)flyToken).createCopy(this, offset);
            this.origTokenOrEmbeddings[this.origTokenStartIndex + index] = nonFlyToken;
        } else {
            nonFlyToken = this.liveTokenList.replaceFlyToken(this.liveTokenGapEnd + index - this.origTokenCount, flyToken, offset - this.liveTokenOffsetDiff);
        }
        return nonFlyToken;
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this;
    }

    @Override
    public CharSequence inputSourceText() {
        return this.rootTokenList().inputSourceText();
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.snapshot;
    }

    @Override
    public InputAttributes inputAttributes() {
        return this.liveTokenList.inputAttributes();
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public Set<T> skipTokenIds() {
        return null;
    }

    @Override
    public int startOffset() {
        if (this.tokenCountCurrent() > 0 || this.tokenCount() > 0) {
            return this.tokenOffset(0);
        }
        return 0;
    }

    @Override
    public int endOffset() {
        int cntM1 = this.tokenCount() - 1;
        if (cntM1 >= 0) {
            return this.tokenOffset(cntM1) + this.tokenOrEmbedding(cntM1).token().length();
        }
        return 0;
    }

    @Override
    public boolean isRemoved() {
        return false;
    }

    public boolean canModifyToken(int index, AbstractToken token) {
        return this.liveTokenGapStart != -1 && index >= this.liveTokenGapStart && index < this.liveTokenGapEnd && !this.token2offset.containsKey((Object)token);
    }

    public void update(TokenHierarchyEventInfo eventInfo, TokenListChange<T> change) {
        int extraOrigTokenCount;
        RemovedTokenList<T> removedTokenList = change.tokenChangeInfo().removedTokenList();
        int startRemovedIndex = change.index();
        int endRemovedIndex = startRemovedIndex + removedTokenList.tokenCount();
        if (this.liveTokenGapStart == -1) {
            this.liveTokenGapStart = startRemovedIndex;
            this.liveTokenGapEnd = startRemovedIndex;
            this.liveTokenGapStartOffset = change.offset();
            TokenOrEmbedding[] tokenOrEmbeddings = new TokenOrEmbedding[removedTokenList.tokenCount()];
            this.origTokenOrEmbeddings = tokenOrEmbeddings;
            this.origOffsets = new int[this.origTokenOrEmbeddings.length];
        }
        int liveTokenIndexDiff = change.tokenChangeInfo().addedTokenCount() - removedTokenList.tokenCount();
        if (startRemovedIndex < this.liveTokenGapStart) {
            TokenOrEmbedding<T> tokenOrEmbedding;
            int index;
            int offset;
            extraOrigTokenCount = this.liveTokenGapStart - startRemovedIndex;
            this.ensureOrigTokensStartCapacity(extraOrigTokenCount);
            this.origTokenStartIndex -= extraOrigTokenCount;
            this.origTokenCount += extraOrigTokenCount;
            int bound = Math.min(endRemovedIndex, this.liveTokenGapStart);
            this.liveTokenGapStartOffset = offset = change.offset();
            for (index = startRemovedIndex; index < bound; ++index) {
                TokenList<T> tokenList;
                tokenOrEmbedding = removedTokenList.tokenOrEmbedding(index - startRemovedIndex);
                AbstractToken<T> token = tokenOrEmbedding.token();
                if (!token.isFlyweight() && (tokenList = token.tokenList()) == null) {
                    tokenList = null;
                    if (!token.isFlyweight()) {
                        token.setTokenList(tokenList);
                    }
                }
                this.origOffsets[this.origTokenStartIndex] = offset;
                this.origTokenOrEmbeddings[this.origTokenStartIndex++] = tokenOrEmbedding;
                offset += token.length();
            }
            while (index < this.liveTokenGapStart) {
                tokenOrEmbedding = this.liveTokenList.tokenOrEmbeddingDirect(index + liveTokenIndexDiff);
                AbstractToken<T> t = tokenOrEmbedding.token();
                if (!t.isFlyweight()) {
                    this.token2offset.putEntry(new Token2OffsetEntry<T>(t, offset));
                }
                this.origOffsets[this.origTokenStartIndex] = offset;
                this.origTokenOrEmbeddings[this.origTokenStartIndex++] = tokenOrEmbedding;
                offset += t.length();
                ++index;
            }
            this.liveTokenGapStart = startRemovedIndex;
        }
        if (endRemovedIndex > this.liveTokenGapEnd) {
            AbstractToken<T> token;
            TokenOrEmbedding<T> tokenOrEmbedding;
            extraOrigTokenCount = endRemovedIndex - this.liveTokenGapEnd;
            this.ensureOrigTokensEndCapacity(extraOrigTokenCount);
            this.origTokenCount += extraOrigTokenCount;
            int origTokenIndex = this.origTokenStartIndex + this.origTokenCount - 1;
            int bound = Math.max(startRemovedIndex, this.liveTokenGapEnd);
            int index = endRemovedIndex;
            int offset = change.removedEndOffset();
            for (index = endRemovedIndex - 1; index >= bound; --index) {
                TokenList<T> tokenList;
                tokenOrEmbedding = removedTokenList.tokenOrEmbedding(index - startRemovedIndex);
                token = tokenOrEmbedding.token();
                offset -= token.length();
                if (!token.isFlyweight() && (tokenList = token.tokenList()) == null) {
                    tokenList = null;
                    if (!token.isFlyweight()) {
                        token.setTokenList(tokenList);
                    }
                }
                this.origOffsets[origTokenIndex] = offset + this.liveTokenOffsetDiff;
                if (this.liveTokenOffsetDiff != 0) {
                    this.token2offset.putEntry(new Token2OffsetEntry<T>(token, this.origOffsets[origTokenIndex]));
                }
                this.origTokenOrEmbeddings[origTokenIndex--] = tokenOrEmbedding;
            }
            while (index >= this.liveTokenGapEnd) {
                tokenOrEmbedding = this.liveTokenList.tokenOrEmbeddingDirect(index + liveTokenIndexDiff);
                token = tokenOrEmbedding.token();
                offset -= token.length();
                if (!token.isFlyweight()) {
                    this.token2offset.putEntry(new Token2OffsetEntry<T>(token, offset));
                }
                this.origOffsets[origTokenIndex] = offset + this.liveTokenOffsetDiff;
                this.token2offset.putEntry(new Token2OffsetEntry<T>(token, this.origOffsets[origTokenIndex]));
                this.origTokenOrEmbeddings[origTokenIndex--] = tokenOrEmbedding;
                --index;
            }
            this.liveTokenGapEnd = endRemovedIndex;
        }
        this.liveTokenOffsetDiff += eventInfo.removedLength() - eventInfo.insertedLength();
        this.liveTokenGapEnd += liveTokenIndexDiff;
    }

    private void ensureOrigTokensStartCapacity(int extraOrigTokenCount) {
        if (extraOrigTokenCount > this.origTokenOrEmbeddings.length - this.origTokenCount) {
            TokenOrEmbedding[] newOrigTokensOrBranches = new TokenOrEmbedding[this.origTokenOrEmbeddings.length * 3 / 2 + extraOrigTokenCount];
            int[] newOrigOffsets = new int[newOrigTokensOrBranches.length];
            int newIndex = Math.max(extraOrigTokenCount, (newOrigTokensOrBranches.length - (this.origTokenCount + extraOrigTokenCount)) / 2);
            System.arraycopy(this.origTokenOrEmbeddings, this.origTokenStartIndex, newOrigTokensOrBranches, newIndex, this.origTokenCount);
            System.arraycopy(this.origOffsets, this.origTokenStartIndex, newOrigOffsets, newIndex, this.origTokenCount);
            this.origTokenOrEmbeddings = newOrigTokensOrBranches;
            this.origOffsets = newOrigOffsets;
            this.origTokenStartIndex = newIndex;
        } else if (extraOrigTokenCount > this.origTokenStartIndex) {
            int newIndex = this.origTokenOrEmbeddings.length - this.origTokenCount;
            System.arraycopy(this.origTokenOrEmbeddings, this.origTokenStartIndex, this.origTokenOrEmbeddings, newIndex, this.origTokenCount);
            System.arraycopy(this.origOffsets, this.origTokenStartIndex, this.origOffsets, newIndex, this.origTokenCount);
            this.origTokenStartIndex = this.origTokenOrEmbeddings.length - this.origTokenCount;
        }
    }

    private void ensureOrigTokensEndCapacity(int extraOrigTokenCount) {
        if (extraOrigTokenCount > this.origTokenOrEmbeddings.length - this.origTokenCount) {
            TokenOrEmbedding[] newOrigTokensOrBranches = new TokenOrEmbedding[this.origTokenOrEmbeddings.length * 3 / 2 + extraOrigTokenCount];
            int[] newOrigOffsets = new int[newOrigTokensOrBranches.length];
            int newIndex = (newOrigTokensOrBranches.length - (this.origTokenCount + extraOrigTokenCount)) / 2;
            System.arraycopy(this.origTokenOrEmbeddings, this.origTokenStartIndex, newOrigTokensOrBranches, newIndex, this.origTokenCount);
            System.arraycopy(this.origOffsets, this.origTokenStartIndex, newOrigOffsets, newIndex, this.origTokenCount);
            this.origTokenOrEmbeddings = newOrigTokensOrBranches;
            this.origOffsets = newOrigOffsets;
            this.origTokenStartIndex = newIndex;
        } else if (extraOrigTokenCount > this.origTokenOrEmbeddings.length - this.origTokenCount - this.origTokenStartIndex) {
            System.arraycopy(this.origTokenOrEmbeddings, this.origTokenStartIndex, this.origTokenOrEmbeddings, 0, this.origTokenCount);
            System.arraycopy(this.origOffsets, this.origTokenStartIndex, this.origOffsets, 0, this.origTokenCount);
            this.origTokenStartIndex = 0;
        }
    }

    public String toString() {
        return "liveTokenGapStart=" + this.liveTokenGapStart + ", liveTokenGapEnd=" + this.liveTokenGapEnd + ", liveTokenGapStartOffset=" + this.liveTokenGapStartOffset + ", liveTokenOffsetDiff=" + this.liveTokenOffsetDiff + ",\n origTokenStartIndex=" + this.origTokenStartIndex + ", origTokenCount=" + this.origTokenCount + ", token2offset: " + this.token2offset;
    }

    public int tokenShiftStartOffset() {
        return this.liveTokenGapStartOffset;
    }

    public int tokenShiftEndOffset() {
        return this.liveTokenGapStartOffset + this.liveTokenOffsetDiff;
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "SnapshotTL";
    }

    private static final class Token2OffsetEntry<T extends TokenId>
    extends CompactMap.MapEntry<AbstractToken<T>, Token2OffsetEntry<T>> {
        private final AbstractToken<T> token;
        private final int offset;

        Token2OffsetEntry(AbstractToken<T> token, int offset) {
            this.token = token;
            this.offset = offset;
        }

        public AbstractToken<T> getKey() {
            return this.token;
        }

        public Token2OffsetEntry<T> getValue() {
            return this;
        }

        protected int valueHashCode() {
            return this.offset;
        }

        protected boolean valueEquals(Object value2) {
            return value2 instanceof Token2OffsetEntry && ((Token2OffsetEntry)((Object)value2)).offset() == this.offset();
        }

        public int offset() {
            return this.offset;
        }

        public Token2OffsetEntry<T> setValue(Token2OffsetEntry<T> value) {
            throw new IllegalStateException("Prohibited");
        }

        public String toString() {
            return String.valueOf(this.offset);
        }
    }

}

