/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.FlyOffsetGapList
 */
package org.netbeans.lib.lexer.inc;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.FlyOffsetGapList;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.LAState;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TextLexerInputOperation;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.TextToken;
import org.netbeans.spi.lexer.MutableTextInput;

public final class IncTokenList<T extends TokenId>
extends FlyOffsetGapList<TokenOrEmbedding<T>>
implements MutableTokenList<T> {
    private final TokenHierarchyOperation<?, T> tokenHierarchyOperation;
    private Language<T> language;
    private LanguagePath languagePath;
    private CharSequence inputSourceText;
    private LexerInputOperation<T> lexerInputOperation;
    private int rootModCount;
    private LAState laState;

    public IncTokenList(TokenHierarchyOperation<?, T> tokenHierarchyOperation) {
        this.tokenHierarchyOperation = tokenHierarchyOperation;
        this.laState = LAState.empty();
    }

    public void reinit() {
        if (this.languagePath != null) {
            MutableTextInput input = this.tokenHierarchyOperation.mutableTextInput();
            this.inputSourceText = LexerSpiPackageAccessor.get().text(input);
            this.lexerInputOperation = new TextLexerInputOperation(this);
        } else {
            this.inputSourceText = null;
            this.releaseLexerInputOperation();
        }
    }

    public void releaseLexerInputOperation() {
        if (this.lexerInputOperation != null) {
            this.lexerInputOperation.release();
            this.lexerInputOperation = null;
        }
    }

    @Override
    public Language<T> language() {
        return this.language;
    }

    @Override
    public LanguagePath languagePath() {
        return this.languagePath;
    }

    public void setLanguagePath(LanguagePath languagePath) {
        this.languagePath = languagePath;
        this.language = languagePath != null ? LexerUtilsConstants.innerLanguage(languagePath) : null;
    }

    @Override
    public int tokenCount() {
        if (this.lexerInputOperation != null) {
            this.tokenOrEmbeddingImpl(Integer.MAX_VALUE);
        }
        return this.size();
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        int rawOffset = token.rawOffset();
        return rawOffset < this.offsetGapStart() ? rawOffset : rawOffset - this.offsetGapLength();
    }

    @Override
    public int tokenOffset(int index) {
        return this.elementOffset(index);
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexLazyTokenCreation(this, offset);
    }

    @Override
    public int modCount() {
        return this.rootModCount;
    }

    public void incrementModCount() {
        ++this.rootModCount;
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        return this.tokenOrEmbeddingImpl(index);
    }

    private TokenOrEmbedding<T> tokenOrEmbeddingImpl(int index) {
        while (this.lexerInputOperation != null && index >= this.size()) {
            AbstractToken<T> token = this.lexerInputOperation.nextToken();
            if (token != null) {
                if (!token.isFlyweight()) {
                    token.setTokenList(this);
                }
                this.updateElementOffsetAdd(token);
                this.add(token);
                this.laState = this.laState.add(this.lexerInputOperation.lookahead(), this.lexerInputOperation.lexerState());
                continue;
            }
            this.releaseLexerInputOperation();
            this.trimToSize();
            this.laState.trimToSize();
        }
        return index < this.size() ? (TokenOrEmbedding)this.get(index) : null;
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        TextToken nonFlyToken = ((TextToken)flyToken).createCopy(this, this.offset2Raw(offset));
        this.set(index, nonFlyToken);
        return nonFlyToken;
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        this.set(index, t);
    }

    @Override
    public InputAttributes inputAttributes() {
        return LexerSpiPackageAccessor.get().inputAttributes(this.tokenHierarchyOperation.mutableTextInput());
    }

    protected int elementRawOffset(TokenOrEmbedding<T> elem) {
        return elem.token().rawOffset();
    }

    protected void setElementRawOffset(TokenOrEmbedding<T> elem, int rawOffset) {
        elem.token().setRawOffset(rawOffset);
    }

    protected boolean isElementFlyweight(TokenOrEmbedding<T> elem) {
        return elem.embedding() == null && elem.token().isFlyweight();
    }

    protected int elementLength(TokenOrEmbedding<T> elem) {
        return elem.token().length();
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbeddingDirect(int index) {
        return (TokenOrEmbedding)this.get(index);
    }

    @Override
    public int lookahead(int index) {
        return this.laState.lookahead(index);
    }

    @Override
    public Object state(int index) {
        return this.laState.state(index);
    }

    @Override
    public int tokenCountCurrent() {
        return this.size();
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this;
    }

    @Override
    public CharSequence inputSourceText() {
        return this.inputSourceText;
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.tokenHierarchyOperation;
    }

    @Override
    public LexerInputOperation<T> createLexerInputOperation(int tokenIndex, int relexOffset, Object relexState) {
        if (this.lexerInputOperation != null) {
            this.lexerInputOperation.release();
        }
        return new TextLexerInputOperation(this, tokenIndex, relexState, relexOffset, this.inputSourceText.length());
    }

    @Override
    public boolean isFullyLexed() {
        return this.lexerInputOperation == null;
    }

    @Override
    public void replaceTokens(TokenListChange<T> change, TokenHierarchyEventInfo eventInfo, boolean modInside) {
        AbstractToken token;
        int index = change.index();
        int removeTokenCount = change.removedTokenCount();
        AbstractToken firstRemovedToken = null;
        if (removeTokenCount > 0) {
            Object[] removedTokensOrEmbeddings = new TokenOrEmbedding[removeTokenCount];
            this.copyElements(index, index + removeTokenCount, removedTokensOrEmbeddings, 0);
            firstRemovedToken = removedTokensOrEmbeddings[0].token();
            for (int i = 0; i < removeTokenCount; ++i) {
                TokenOrEmbedding<T> tokenOrEmbedding = removedTokensOrEmbeddings[i];
                token = tokenOrEmbedding.token();
                if (token.isFlyweight()) continue;
                this.updateElementOffsetRemove(token);
                token.setTokenList(null);
                EmbeddedTokenList etl = tokenOrEmbedding.embedding();
                if (etl == null) continue;
                etl.markRemovedChain(token.rawOffset());
            }
            this.remove(index, removeTokenCount);
            this.laState.remove(index, removeTokenCount);
            change.setRemovedTokens((TokenOrEmbedding<T>[])removedTokensOrEmbeddings);
        } else {
            change.setRemovedTokensEmpty();
        }
        if (this.offsetGapStart() != change.offset()) {
            this.moveOffsetGap(change.offset(), change.index());
        }
        this.updateOffsetGapLength(- eventInfo.diffLength());
        List<TokenOrEmbedding<T>> addedTokensOrEmbeddings = change.addedTokenOrEmbeddings();
        if (addedTokensOrEmbeddings != null && addedTokensOrEmbeddings.size() > 0) {
            for (TokenOrEmbedding<T> tokenOrEmbedding : addedTokensOrEmbeddings) {
                token = tokenOrEmbedding.token();
                if (!token.isFlyweight()) {
                    token.setTokenList(this);
                }
                this.updateElementOffsetAdd(token);
            }
            this.addAll(index, addedTokensOrEmbeddings);
            this.laState = this.laState.addAll(index, change.laState());
            change.syncAddedTokenCount();
            if (removeTokenCount == 1 && addedTokensOrEmbeddings.size() == 1) {
                AbstractToken<T> addedToken = change.addedTokenOrEmbeddings().get(0).token();
                if (firstRemovedToken.id() == addedToken.id() && firstRemovedToken.partType() == addedToken.partType()) {
                    change.markBoundsChange();
                }
            }
        }
        if (this.lexerInputOperation != null) {
            int tokenCount;
            this.lexerInputOperation = this.createLexerInputOperation(tokenCount, this.elementOrEndOffset(tokenCount), (tokenCount = this.tokenCountCurrent()) > 0 ? this.state(tokenCount - 1) : null);
        }
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public Set<T> skipTokenIds() {
        return null;
    }

    @Override
    public int startOffset() {
        return 0;
    }

    @Override
    public int endOffset() {
        return this.inputSourceText != null ? this.inputSourceText.length() : 0;
    }

    @Override
    public boolean isRemoved() {
        return false;
    }

    public void setInputSourceText(CharSequence text) {
        this.inputSourceText = text;
    }

    public String checkConsistency() {
        if (this.offsetGapLength() < 0) {
            return "offsetGapLength=" + this.offsetGapLength() + " < 0; offsetGapStart=" + this.offsetGapStart();
        }
        return null;
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        sb.append("offGap(o=").append(this.offsetGapStart()).append(",l=").append(this.offsetGapLength()).append(")");
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "ITL";
    }

    public String toString() {
        return LexerUtilsConstants.appendTokenList(null, this).toString();
    }
}

