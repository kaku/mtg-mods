/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.inc.RemovedTokenList;

public final class TokenChangeInfo<T extends TokenId> {
    private static final TokenChange<?>[] EMPTY_EMBEDDED_CHANGES = new TokenChange[0];
    private TokenChange<?>[] embeddedChanges = EMPTY_EMBEDDED_CHANGES;
    private final TokenList<T> currentTokenList;
    private RemovedTokenList<T> removedTokenList;
    private int addedTokenCount;
    private int index;
    private int offset;
    private boolean boundsChange;

    public TokenChangeInfo(TokenList<T> currentTokenList) {
        this.currentTokenList = currentTokenList;
    }

    public TokenChange<?>[] embeddedChanges() {
        return this.embeddedChanges;
    }

    public void addEmbeddedChange(TokenChangeInfo<?> change) {
        TokenChange[] tmp = new TokenChange[this.embeddedChanges.length + 1];
        System.arraycopy(this.embeddedChanges, 0, tmp, 0, this.embeddedChanges.length);
        tmp[this.embeddedChanges.length] = LexerApiPackageAccessor.get().createTokenChange(change);
        this.embeddedChanges = tmp;
    }

    public int index() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int offset() {
        return this.offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public RemovedTokenList<T> removedTokenList() {
        return this.removedTokenList;
    }

    public void setRemovedTokenList(RemovedTokenList<T> removedTokenList) {
        this.removedTokenList = removedTokenList;
    }

    public int addedTokenCount() {
        return this.addedTokenCount;
    }

    public void setAddedTokenCount(int addedTokenCount) {
        this.addedTokenCount = addedTokenCount;
    }

    public void updateAddedTokenCount(int diff) {
        this.addedTokenCount += diff;
    }

    public TokenList<T> currentTokenList() {
        return this.currentTokenList;
    }

    public boolean isBoundsChange() {
        return this.boundsChange;
    }

    public void markBoundsChange() {
        this.boundsChange = true;
    }
}

