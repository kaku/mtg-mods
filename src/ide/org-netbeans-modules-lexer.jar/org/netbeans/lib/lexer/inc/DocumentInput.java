/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.lib.lexer.inc;

import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.lexer.LanguageManager;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenHierarchyControl;

public final class DocumentInput<D extends Document>
extends MutableTextInput<D>
implements DocumentListener {
    private static final Logger LOG = Logger.getLogger(TokenHierarchyOperation.class.getName());
    private static final String PROP_MIME_TYPE = "mimeType";
    private D doc;
    private CharSequence text;

    public static synchronized <D extends Document> DocumentInput<D> get(D doc) {
        DocumentInput<D> di = (DocumentInput<D>)doc.getProperty(MutableTextInput.class);
        if (di == null) {
            di = new DocumentInput<D>(doc);
            doc.putProperty(MutableTextInput.class, di);
        }
        return di;
    }

    public DocumentInput(D doc) {
        this.doc = doc;
        this.text = DocumentUtilities.getText(doc);
        DocumentUtilities.addDocumentListener(doc, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.LEXER);
    }

    @Override
    protected Language<?> language() {
        String mimeType;
        Language lang = (Language)this.doc.getProperty(Language.class);
        if (lang == null && (mimeType = (String)this.doc.getProperty("mimeType")) != null) {
            lang = LanguageManager.getInstance().findLanguage(mimeType);
        }
        return lang;
    }

    @Override
    protected CharSequence text() {
        return this.text;
    }

    @Override
    protected InputAttributes inputAttributes() {
        return (InputAttributes)this.doc.getProperty(InputAttributes.class);
    }

    @Override
    protected D inputSource() {
        return this.doc;
    }

    @Override
    protected boolean isReadLocked() {
        return DocumentUtilities.isReadLocked(this.doc);
    }

    @Override
    protected boolean isWriteLocked() {
        return DocumentUtilities.isWriteLocked(this.doc);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.textModified(e.getOffset(), 0, null, e.getLength());
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.textModified(e.getOffset(), e.getLength(), DocumentUtilities.getModificationText((DocumentEvent)e), 0);
    }

    private void textModified(int offset, int length, CharSequence removedText, int insertedLength) {
        this.tokenHierarchyControl().textModified(offset, length, removedText, insertedLength);
    }
}

