/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.lib.lexer.inc;

import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.inc.OriginalText;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;

public final class TokenHierarchyEventInfo {
    static final boolean LOG_SOURCE_TEXT = Boolean.getBoolean("org.netbeans.editor.log.source.text");
    private final TokenHierarchyOperation<?, ?> tokenHierarchyOperation;
    private final TokenHierarchyEventType type;
    private TokenChange<?> tokenChange;
    private final int modOffset;
    private final int removedLength;
    private final CharSequence removedText;
    private final int insertedLength;
    private final int diffLengthOrZero;
    private OriginalText originalText;
    private int affectedStartOffset;
    private int affectedEndOffset;

    public TokenHierarchyEventInfo(TokenHierarchyOperation<?, ?> tokenHierarchyOperation, TokenHierarchyEventType type, int modificationOffset, int removedLength, CharSequence removedText, int insertedLength) {
        if (modificationOffset < 0) {
            throw new IllegalArgumentException("modificationOffset=" + modificationOffset + " < 0");
        }
        if (removedLength < 0) {
            throw new IllegalArgumentException("removedLength=" + removedLength + " < 0");
        }
        if (insertedLength < 0) {
            throw new IllegalArgumentException("insertedLength=" + insertedLength + " < 0");
        }
        this.tokenHierarchyOperation = tokenHierarchyOperation;
        this.type = type;
        this.modOffset = modificationOffset;
        this.removedLength = removedLength;
        this.removedText = removedText;
        this.insertedLength = insertedLength;
        this.diffLengthOrZero = Math.max(0, insertedLength - removedLength);
        this.affectedStartOffset = modificationOffset;
        this.affectedEndOffset = modificationOffset + this.diffLengthOrZero;
    }

    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.tokenHierarchyOperation;
    }

    public TokenHierarchyEventType type() {
        return this.type;
    }

    public TokenChange<?> tokenChange() {
        return this.tokenChange;
    }

    public void setTokenChangeInfo(TokenChangeInfo<?> info) {
        this.tokenChange = LexerApiPackageAccessor.get().createTokenChange(info);
    }

    public int affectedStartOffset() {
        return this.affectedStartOffset;
    }

    public void setMinAffectedStartOffset(int affectedStartOffset) {
        if (affectedStartOffset < this.affectedStartOffset) {
            this.affectedStartOffset = affectedStartOffset;
        }
    }

    public int affectedEndOffset() {
        return this.affectedEndOffset;
    }

    public void setMaxAffectedEndOffset(int affectedEndOffset) {
        if (affectedEndOffset > this.affectedEndOffset) {
            this.affectedEndOffset = affectedEndOffset;
        }
    }

    public int modOffset() {
        return this.modOffset;
    }

    public int removedLength() {
        return this.removedLength;
    }

    public CharSequence removedText() {
        return this.removedText;
    }

    public int insertedLength() {
        return this.insertedLength;
    }

    public CharSequence insertedText() {
        return this.currentText().subSequence(this.modOffset(), this.modOffset() + this.insertedLength());
    }

    public int diffLength() {
        return this.insertedLength - this.removedLength;
    }

    public int diffLengthOrZero() {
        return this.diffLengthOrZero;
    }

    public OriginalText originalText() {
        if (this.originalText == null) {
            if (this.removedLength != 0 && this.removedText == null) {
                throw new IllegalStateException("Cannot obtain removed text for " + this.tokenHierarchyOperation.inputSource() + " which breaks token snapshots operation and" + " token text retaining after token's removal." + " Valid removedText in TokenHierarchyControl.textModified()" + " should be provided.");
            }
            this.originalText = new OriginalText(this.currentText(), this.modOffset, this.removedText, this.insertedLength);
        }
        return this.originalText;
    }

    public CharSequence currentText() {
        return this.tokenHierarchyOperation.text();
    }

    public String modificationDescription(boolean detail) {
        StringBuilder sb = new StringBuilder(this.originalText().length() + 300);
        if (this.removedLength() > 0) {
            sb.append("TEXT REMOVED at ").append(this.modOffset()).append(" len=").append(this.removedLength());
            if (this.removedText() != null) {
                sb.append(" \"");
                CharSequenceUtilities.debugText((StringBuilder)sb, (CharSequence)this.removedText());
                sb.append('\"');
            }
            sb.append('\n');
        }
        if (this.insertedLength() > 0) {
            sb.append("TEXT INSERTED at ").append(this.modOffset()).append(" len=").append(this.insertedLength()).append(" \"");
            CharSequenceUtilities.debugText((StringBuilder)sb, (CharSequence)this.insertedText());
            sb.append("\"\n");
        }
        if (LOG_SOURCE_TEXT && detail) {
            sb.append("\n\n----------------- ORIGINAL TEXT -----------------\n" + (Object)((Object)this.originalText()) + "\n----------------- BEFORE-CARET TEXT -----------------\n" + this.originalText().subSequence(0, this.modOffset()) + "|<--CARET\n");
        }
        return sb.toString();
    }

    public String dumpAffected() {
        StringBuilder sb = new StringBuilder("Affected(");
        sb.append(this.affectedStartOffset());
        sb.append(",");
        sb.append(this.affectedEndOffset());
        sb.append(')');
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("modOffset=");
        sb.append(this.modOffset());
        if (this.removedLength() > 0) {
            sb.append(", removedLength=");
            sb.append(this.removedLength());
        }
        if (this.insertedLength() > 0) {
            sb.append(", insertedLength=");
            sb.append(this.insertedLength());
        }
        sb.append(", ").append(this.dumpAffected());
        return sb.toString();
    }
}

