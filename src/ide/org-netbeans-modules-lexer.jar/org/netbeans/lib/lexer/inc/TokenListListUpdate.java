/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;
import org.netbeans.lib.lexer.inc.JoinTokenListChange;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyUpdate;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.spi.lexer.LanguageEmbedding;

final class TokenListListUpdate<T extends TokenId> {
    private static final Logger LOG = Logger.getLogger(TokenListListUpdate.class.getName());
    final TokenListList<T> tokenListList;
    int modTokenListIndex;
    int removedTokenListCount;
    private EmbeddedTokenList<?, T>[] removedTokenLists;
    List<EmbeddedTokenList<?, T>> addedTokenLists;
    boolean addedJoined;

    TokenListListUpdate(TokenListList<T> tokenListList) {
        this.tokenListList = tokenListList;
        this.modTokenListIndex = -1;
    }

    public boolean isTokenListsMod() {
        return this.removedTokenListCount != 0 || this.addedTokenLists.size() > 0;
    }

    public int modTokenListCountDiff() {
        return this.addedTokenLists.size() - this.removedTokenListCount;
    }

    public int addedTokenListCount() {
        return this.addedTokenLists.size();
    }

    public EmbeddedTokenList<?, T>[] removedTokenLists() {
        return this.removedTokenLists;
    }

    public EmbeddedTokenList<?, T> afterUpdateTokenList(JoinTokenList<T> jtl, int tokenListIndex) {
        EmbeddedTokenList etl;
        if (tokenListIndex < this.modTokenListIndex) {
            etl = jtl.tokenList(tokenListIndex);
            etl.updateModCount();
        } else if (tokenListIndex < this.modTokenListIndex + this.addedTokenLists.size()) {
            etl = this.addedTokenLists.get(tokenListIndex - this.modTokenListIndex);
        } else {
            etl = jtl.tokenList(tokenListIndex + this.removedTokenListCount - this.addedTokenLists.size());
            etl.updateModCount();
        }
        return etl;
    }

    protected int afterUpdateTokenListCount(JoinTokenList<T> jtl) {
        return jtl.tokenListCount() - this.removedTokenListCount + this.addedTokenLists.size();
    }

    void markChangedMember(EmbeddedTokenList<?, T> changedTokenList) {
        assert (this.modTokenListIndex == -1);
        this.modTokenListIndex = this.tokenListList.findIndex(changedTokenList.startOffset());
        assert (this.tokenListList.get(this.modTokenListIndex) == changedTokenList);
    }

    void markChageBetween(int offset) {
        assert (this.modTokenListIndex == -1);
        this.modTokenListIndex = this.tokenListList.findIndex(offset);
    }

    void markRemovedMember(EmbeddedTokenList<?, T> removedTokenList, TokenHierarchyEventInfo eventInfo) {
        EmbeddedTokenList markedForRemoveTokenList;
        boolean indexWasMinusOne;
        removedTokenList.updateModCount();
        if (this.modTokenListIndex == -1) {
            assert (this.removedTokenListCount == 0);
            indexWasMinusOne = true;
            this.modTokenListIndex = this.tokenListList.findIndexDuringUpdate(removedTokenList, eventInfo);
            assert (this.modTokenListIndex >= 0);
        } else {
            indexWasMinusOne = false;
        }
        if ((markedForRemoveTokenList = this.tokenListList.getOrNull(this.modTokenListIndex + this.removedTokenListCount)) != removedTokenList) {
            int realIndex = this.tokenListList.indexOf(removedTokenList);
            String msg = "\n\nLEXER-INTERNAL-ERROR: Removing at tokenListIndex=" + this.modTokenListIndex + " but real tokenListIndex is " + realIndex + " (indexWasMinusOne=" + indexWasMinusOne + ").\n" + "Wishing to remove tokenList\n" + (removedTokenList != null ? removedTokenList.dumpInfo(new StringBuilder(256)) : "!!<NULL>!!") + "\nbut marked-for-remove tokenList is \n" + (markedForRemoveTokenList != null ? markedForRemoveTokenList.dumpInfo(new StringBuilder(256)) : "!!<NULL>!!") + "\nfrom tokenListList\n" + this.tokenListList + "\nModification description:\n" + eventInfo.modificationDescription(true);
            if (LOG.isLoggable(Level.WARNING)) {
                LOG.warning(msg);
            }
            if (indexWasMinusOne) {
                this.modTokenListIndex = realIndex;
                if (TokenList.LOG.isLoggable(Level.FINE)) {
                    throw new IllegalStateException("Invalid modTokenListIndex");
                }
            } else {
                throw new IllegalStateException("Cannot fix modTokenListIndex");
            }
        }
        ++this.removedTokenListCount;
    }

    void markAddedMember(EmbeddedTokenList<?, T> addedTokenList) {
        if (this.addedTokenLists == null) {
            if (this.modTokenListIndex == -1) {
                this.modTokenListIndex = this.tokenListList.findIndex(addedTokenList.startOffset());
                assert (this.modTokenListIndex >= 0);
            }
            this.addedTokenLists = new ArrayList(4);
        }
        this.addedTokenLists.add(addedTokenList);
    }

    void replaceTokenLists() {
        assert (this.removedTokenListCount > 0 || this.addedTokenLists != null);
        this.removedTokenLists = this.tokenListList.replace(this.modTokenListIndex, this.removedTokenListCount, this.addedTokenLists);
    }

    void collectRemovedEmbeddings(TokenHierarchyUpdate.UpdateItem<T> updateItem) {
        if (this.tokenListList.hasChildren() && this.removedTokenLists != null) {
            for (int i = 0; i < this.removedTokenLists.length; ++i) {
                EmbeddedTokenList etl = this.removedTokenLists[i];
                updateItem.collectRemovedEmbeddings(etl);
            }
        }
    }

    void collectAddedEmbeddings(TokenHierarchyUpdate.UpdateItem<T> updateItem) {
        EmbeddedTokenList addedEtl;
        int i;
        boolean becomeJoining = false;
        for (i = 0; i < this.addedTokenLists.size(); ++i) {
            addedEtl = this.addedTokenLists.get(i);
            becomeJoining |= addedEtl.languageEmbedding().joinSections();
        }
        if (becomeJoining) {
            this.tokenListList.setJoinSections(true);
            this.tokenListList.checkCreateJoinTokenList();
        }
        for (i = 0; i < this.addedTokenLists.size(); ++i) {
            addedEtl = this.addedTokenLists.get(i);
            if (!becomeJoining) {
                addedEtl.initAllTokens();
            }
            if (!this.tokenListList.hasChildren()) continue;
            updateItem.collectAddedEmbeddings(addedEtl, 0, addedEtl.tokenCountCurrent(), updateItem.childrenLanguages);
        }
    }

    TokenListChange<T> createTokenListChange(EmbeddedTokenList<?, T> etl) {
        TokenListChange etlTokenListChange;
        assert (etl != null);
        if (this.tokenListList.joinSections()) {
            JoinTokenList<T> jtl = this.tokenListList.joinTokenList();
            etlTokenListChange = new JoinTokenListChange<T>(jtl);
        } else {
            etlTokenListChange = new TokenListChange(etl);
        }
        return etlTokenListChange;
    }

    TokenListChange<T> createJoinTokenListChange() {
        assert (this.tokenListList.joinSections());
        int etlIndex = Math.min(this.modTokenListIndex, this.tokenListList.size() - 1);
        JoinTokenList<T> jtl = this.tokenListList.joinTokenList();
        jtl.setActiveTokenListIndex(etlIndex);
        return new JoinTokenListChange<T>(jtl);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(80);
        sb.append("modTLInd=").append(this.modTokenListIndex).append("; ");
        if (this.isTokenListsMod()) {
            sb.append("Rem:").append(this.removedTokenListCount);
            sb.append(" Add:").append(this.addedTokenLists.size());
        } else {
            sb.append("NoTLMod");
        }
        sb.append(" Size:").append(this.tokenListList.size());
        return sb.toString();
    }
}

