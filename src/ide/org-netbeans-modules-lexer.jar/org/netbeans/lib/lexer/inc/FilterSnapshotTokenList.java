/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import java.util.Set;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.token.AbstractToken;

public final class FilterSnapshotTokenList<T extends TokenId>
implements TokenList<T> {
    private TokenList<T> tokenList;
    private int tokenOffsetDiff;

    public FilterSnapshotTokenList(TokenList<T> tokenList, int tokenOffsetDiff) {
        this.tokenList = tokenList;
        this.tokenOffsetDiff = tokenOffsetDiff;
    }

    public TokenList delegate() {
        return this.tokenList;
    }

    public int tokenOffsetDiff() {
        return this.tokenOffsetDiff;
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        return this.tokenList.tokenOrEmbedding(index);
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        return this.tokenList.replaceFlyToken(index, flyToken, offset);
    }

    @Override
    public int tokenOffset(int index) {
        return this.tokenOffsetDiff + this.tokenList.tokenOffset(index);
    }

    @Override
    public int modCount() {
        return -1;
    }

    @Override
    public int tokenCount() {
        return this.tokenList.tokenCount();
    }

    @Override
    public int tokenCountCurrent() {
        return this.tokenList.tokenCountCurrent();
    }

    @Override
    public Language<T> language() {
        return this.tokenList.language();
    }

    @Override
    public LanguagePath languagePath() {
        return this.tokenList.languagePath();
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        return this.tokenList.tokenOffset(token);
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexBinSearch(this, offset, this.tokenCount());
    }

    public char charAt(int offset) {
        throw new IllegalStateException("Unexpected call.");
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        this.tokenList.setTokenOrEmbedding(index, t);
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this.tokenList.rootTokenList();
    }

    @Override
    public CharSequence inputSourceText() {
        return this.rootTokenList().inputSourceText();
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.tokenList.tokenHierarchyOperation();
    }

    @Override
    public InputAttributes inputAttributes() {
        return this.tokenList.inputAttributes();
    }

    @Override
    public int lookahead(int index) {
        return this.tokenList.lookahead(index);
    }

    @Override
    public Object state(int index) {
        return this.tokenList.state(index);
    }

    @Override
    public boolean isContinuous() {
        return this.tokenList.isContinuous();
    }

    @Override
    public Set<T> skipTokenIds() {
        return this.tokenList.skipTokenIds();
    }

    @Override
    public int startOffset() {
        return this.tokenOffsetDiff + this.tokenList.startOffset();
    }

    @Override
    public int endOffset() {
        return this.tokenOffsetDiff + this.tokenList.endOffset();
    }

    @Override
    public boolean isRemoved() {
        return false;
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "FilterSnapshotTL";
    }
}

