/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer.inc;

import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.LAState;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.RemovedTokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.token.AbstractToken;

public class TokenListChange<T extends TokenId> {
    private static final TokenOrEmbedding<?>[] EMPTY_TOKENS = new TokenOrEmbedding[0];
    private final TokenChangeInfo<T> tokenChangeInfo;
    private List<TokenOrEmbedding<T>> addedTokenOrEmbeddings;
    private LAState laState;
    int removedEndOffset;
    protected int matchIndex;
    protected int matchOffset;
    protected int addedEndOffset;
    boolean parentChangeIsBoundsChange;

    public static <T extends TokenId> TokenListChange<T> createEmptyChange(MutableTokenList<T> tokenList) {
        TokenListChange<T> change = new TokenListChange<T>(tokenList);
        change.setRemovedTokensEmpty();
        return change;
    }

    public static <T extends TokenId> TokenListChange<T> createRebuildChange(MutableTokenList<T> tokenList) {
        TokenListChange<T> change = new TokenListChange<T>(tokenList);
        change.matchIndex = tokenList.tokenCountCurrent();
        return change;
    }

    public TokenListChange(MutableTokenList<T> tokenList) {
        this.tokenChangeInfo = new TokenChangeInfo<T>(tokenList);
    }

    public void setParentChangeIsBoundsChange(boolean parentChangeIsBoundsChange) {
        this.parentChangeIsBoundsChange = parentChangeIsBoundsChange;
    }

    public boolean parentChangeIsBoundsChange() {
        return this.parentChangeIsBoundsChange;
    }

    public TokenChangeInfo<T> tokenChangeInfo() {
        return this.tokenChangeInfo;
    }

    public MutableTokenList<T> tokenList() {
        return (MutableTokenList)this.tokenChangeInfo.currentTokenList();
    }

    public void setMatchIndex(int matchIndex) {
        this.matchIndex = matchIndex;
    }

    public void setMatchOffset(int matchOffset) {
        this.matchOffset = matchOffset;
    }

    public int increaseMatchIndex() {
        this.matchOffset += this.tokenList().tokenOrEmbeddingDirect(this.matchIndex++).token().length();
        return this.matchOffset;
    }

    public LanguagePath languagePath() {
        return this.tokenList().languagePath();
    }

    public int index() {
        return this.tokenChangeInfo.index();
    }

    public void setIndex(int index) {
        this.tokenChangeInfo.setIndex(index);
    }

    public int offset() {
        return this.tokenChangeInfo.offset();
    }

    public void setOffset(int offset) {
        this.tokenChangeInfo.setOffset(offset);
        this.addedEndOffset = offset;
    }

    public int removedTokenCount() {
        return this.matchIndex - this.index();
    }

    public int removedEndOffset() {
        return this.matchOffset;
    }

    public int addedEndOffset() {
        return this.addedEndOffset;
    }

    public void setAddedEndOffset(int addedEndOffset) {
        this.addedEndOffset = addedEndOffset;
    }

    public void addToken(AbstractToken<T> token, int lookahead, Object state) {
        if (this.addedTokenOrEmbeddings == null) {
            this.addedTokenOrEmbeddings = new ArrayList<TokenOrEmbedding<T>>(2);
            this.laState = LAState.empty();
        }
        this.addedTokenOrEmbeddings.add(token);
        this.laState = this.laState.add(lookahead, state);
        this.addedEndOffset += token.length();
    }

    public List<TokenOrEmbedding<T>> addedTokenOrEmbeddings() {
        return this.addedTokenOrEmbeddings;
    }

    public int addedTokenOrEmbeddingsCount() {
        return this.addedTokenOrEmbeddings != null ? this.addedTokenOrEmbeddings.size() : 0;
    }

    public AbstractToken<T> removeLastAddedToken() {
        int lastIndex = this.addedTokenOrEmbeddings.size() - 1;
        AbstractToken<T> token = this.addedTokenOrEmbeddings.remove(lastIndex).token();
        this.laState.remove(lastIndex, 1);
        --this.matchIndex;
        int tokenLength = token.length();
        this.matchOffset -= tokenLength;
        this.addedEndOffset -= tokenLength;
        return token;
    }

    public AbstractToken<T> addedToken(int index) {
        return this.addedTokenOrEmbeddings.get(0).token();
    }

    public void syncAddedTokenCount() {
        this.tokenChangeInfo.setAddedTokenCount(this.addedTokenOrEmbeddings.size());
    }

    public void setRemovedTokens(TokenOrEmbedding<T>[] removedTokensOrBranches) {
        this.tokenChangeInfo.setRemovedTokenList(new RemovedTokenList<T>(this.tokenChangeInfo.currentTokenList().rootTokenList(), this.languagePath(), removedTokensOrBranches));
    }

    public void setRemovedTokensEmpty() {
        TokenOrEmbedding<?>[] empty = EMPTY_TOKENS;
        this.setRemovedTokens(empty);
    }

    public boolean isBoundsChange() {
        return this.tokenChangeInfo.isBoundsChange();
    }

    public void markBoundsChange() {
        this.tokenChangeInfo.markBoundsChange();
    }

    public LAState laState() {
        return this.laState;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('\"').append(this.languagePath().mimePath());
        sb.append("\", ind=").append(this.index());
        sb.append(", off=").append(this.offset());
        sb.append(", maInd=").append(this.matchIndex);
        sb.append(", maOff=").append(this.matchOffset);
        sb.append(", Add:").append(this.addedTokenOrEmbeddingsCount());
        sb.append(", tCnt=").append(this.tokenList().tokenCountCurrent());
        if (this.isBoundsChange()) {
            sb.append(", BoChan");
        }
        return sb.toString();
    }

    public String toStringMods(int indent) {
        int digitCount;
        int i;
        StringBuilder sb = new StringBuilder();
        RemovedTokenList<T> removedTL = this.tokenChangeInfo.removedTokenList();
        if (removedTL != null && removedTL.tokenCount() > 0) {
            digitCount = ArrayUtilities.digitCount((int)(removedTL.tokenCount() - 1));
            for (i = 0; i < removedTL.tokenCount(); ++i) {
                sb.append('\n');
                ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
                sb.append("Rem[");
                ArrayUtilities.appendIndex((StringBuilder)sb, (int)i, (int)digitCount);
                sb.append("]: ");
                LexerUtilsConstants.appendTokenInfo(sb, removedTL, i, null, false, 0, true);
            }
        }
        if (this.addedTokenOrEmbeddings() != null) {
            digitCount = ArrayUtilities.digitCount((int)(this.addedTokenOrEmbeddings().size() - 1));
            for (i = 0; i < this.addedTokenOrEmbeddings().size(); ++i) {
                sb.append('\n');
                ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
                sb.append("Add[");
                ArrayUtilities.appendIndex((StringBuilder)sb, (int)i, (int)digitCount);
                sb.append("]: ");
                LexerUtilsConstants.appendTokenInfo(sb, this.addedTokenOrEmbeddings.get(i), this.laState.lookahead(i), this.laState.state(i), null, false, 0, true);
            }
        }
        return sb.toString();
    }
}

