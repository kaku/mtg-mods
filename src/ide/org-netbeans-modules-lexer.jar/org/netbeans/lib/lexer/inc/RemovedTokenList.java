/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.inc;

import java.util.Set;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.TextToken;

public final class RemovedTokenList<T extends TokenId>
implements TokenList<T> {
    private final TokenList<?> rootTokenList;
    private final LanguagePath languagePath;
    private final TokenOrEmbedding<T>[] tokenOrEmbeddings;
    private int removedTokensStartOffset;

    public RemovedTokenList(TokenList<?> rootTokenList, LanguagePath languagePath, TokenOrEmbedding<T>[] tokensOrBranches) {
        this.rootTokenList = rootTokenList;
        this.languagePath = languagePath;
        this.tokenOrEmbeddings = tokensOrBranches;
    }

    @Override
    public Language<T> language() {
        return LexerUtilsConstants.innerLanguage(this.languagePath);
    }

    @Override
    public LanguagePath languagePath() {
        return this.languagePath;
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        return index < this.tokenOrEmbeddings.length ? this.tokenOrEmbeddings[index] : null;
    }

    public TokenOrEmbedding<T>[] tokenOrEmbeddings() {
        return this.tokenOrEmbeddings;
    }

    @Override
    public int lookahead(int index) {
        return -1;
    }

    @Override
    public Object state(int index) {
        return null;
    }

    @Override
    public int tokenOffset(int index) {
        Token<T> token = this.existingToken(index);
        if (token.isFlyweight()) {
            int offset = 0;
            while (--index >= 0) {
                token = this.existingToken(index);
                offset += token.length();
                if (token.isFlyweight()) continue;
                return offset + token.offset(null);
            }
            return this.removedTokensStartOffset + offset;
        }
        return token.offset(null);
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexBinSearch(this, offset, this.tokenCountCurrent());
    }

    private Token<T> existingToken(int index) {
        return this.tokenOrEmbeddings[index].token();
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        TextToken nonFlyToken = ((TextToken)flyToken).createCopy(this, offset);
        this.tokenOrEmbeddings[index] = nonFlyToken;
        return nonFlyToken;
    }

    @Override
    public int tokenCount() {
        return this.tokenCountCurrent();
    }

    @Override
    public int tokenCountCurrent() {
        return this.tokenOrEmbeddings.length;
    }

    @Override
    public int modCount() {
        return -1;
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        int rawOffset = token.rawOffset();
        return rawOffset;
    }

    public char charAt(int offset) {
        throw new IllegalStateException("Querying of text for removed tokens not supported");
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        throw new IllegalStateException("Branching of removed tokens not supported");
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this.rootTokenList;
    }

    @Override
    public CharSequence inputSourceText() {
        return null;
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return null;
    }

    @Override
    public InputAttributes inputAttributes() {
        return null;
    }

    @Override
    public int startOffset() {
        if (this.tokenCountCurrent() > 0 || this.tokenCount() > 0) {
            return this.tokenOffset(0);
        }
        return 0;
    }

    @Override
    public int endOffset() {
        int cntM1 = this.tokenCount() - 1;
        if (cntM1 >= 0) {
            return this.tokenOffset(cntM1) + this.tokenOrEmbedding(cntM1).token().length();
        }
        return 0;
    }

    @Override
    public boolean isRemoved() {
        return true;
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public Set<T> skipTokenIds() {
        return null;
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "RemovedTL";
    }

    public String toString() {
        return LexerUtilsConstants.appendTokenList(null, this).toString();
    }
}

