/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.lib.lexer;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.EmbeddingOperation;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.spi.lexer.LanguageEmbedding;

public final class TokenListList<T extends TokenId>
extends GapList<EmbeddedTokenList<?, T>> {
    private final TokenList<?> rootTokenList;
    private final LanguagePath languagePath;
    private boolean joinSections;
    private Object childrenLanguages;
    private JoinTokenList<T> joinTokenList;
    private static final EmbeddedTokenList<?, ?>[] EMPTY_TOKEN_LIST_ARRAY = new EmbeddedTokenList[0];

    public TokenListList(TokenList<?> rootTokenList, LanguagePath languagePath) {
        super(4);
        this.rootTokenList = rootTokenList;
        this.languagePath = languagePath;
        this.childrenLanguages = null;
        assert (languagePath.size() >= 2);
        Language language = LexerUtilsConstants.innerLanguage(languagePath);
        if (languagePath.size() > 2) {
            TokenListList parentTokenList = rootTokenList.tokenHierarchyOperation().tokenListList(languagePath.parent());
            for (int parentIndex = 0; parentIndex < parentTokenList.size(); ++parentIndex) {
                TokenList tokenList = (TokenList)parentTokenList.get(parentIndex);
                this.scanTokenList(tokenList, language);
            }
        } else {
            this.scanTokenList(rootTokenList, language);
        }
        this.checkCreateJoinTokenList();
        if (this.joinTokenList == null) {
            Iterator i$ = this.iterator();
            while (i$.hasNext()) {
                EmbeddedTokenList etl = (EmbeddedTokenList)i$.next();
                assert (!etl.languageEmbedding().joinSections());
                if (etl.tokenCountCurrent() != 0 || etl.textLength() <= 0) continue;
                etl.initAllTokens();
            }
        }
    }

    private void scanTokenList(TokenList<?> tokenList, Language<T> language) {
        int tokenCount = tokenList.tokenCount();
        for (int i = 0; i < tokenCount; ++i) {
            EmbeddedTokenList etl = EmbeddingOperation.embeddedTokenList(tokenList, i, language, false);
            if (etl == null) continue;
            this.add(etl);
            if (!etl.languageEmbedding().joinSections()) continue;
            this.joinSections = true;
        }
    }

    public JoinTokenList<T> joinTokenList() {
        return this.joinTokenList;
    }

    public void checkCreateJoinTokenList() {
        if (this.joinSections && this.joinTokenList == null) {
            this.joinTokenList = JoinTokenList.create(this);
        }
    }

    public LanguagePath languagePath() {
        return this.languagePath;
    }

    public boolean joinSections() {
        return this.joinSections;
    }

    public void setJoinSections(boolean joinSections) {
        this.joinSections = joinSections;
    }

    public void notifyChildAdded(Language<?> language) {
        this.childrenLanguages = LexerUtilsConstants.languageOrArrayAdd(this.childrenLanguages, language);
    }

    public void notifyChildRemoved(Language<?> language) {
        this.childrenLanguages = LexerUtilsConstants.languageOrArrayRemove(this.childrenLanguages, language);
    }

    public boolean hasChildren() {
        return this.childrenLanguages != null;
    }

    public Object childrenLanguages() {
        return this.childrenLanguages;
    }

    public EmbeddedTokenList<?, T> getOrNull(int index) {
        return index < this.size() ? (EmbeddedTokenList)this.get(index) : null;
    }

    public EmbeddedTokenList<?, T>[] replace(int index, int removeTokenListCount, List<EmbeddedTokenList<?, T>> addTokenLists) {
        Object[] removed;
        Object[] arrobject = removed = removeTokenListCount > 0 ? new EmbeddedTokenList[removeTokenListCount] : EMPTY_TOKEN_LIST_ARRAY;
        if (removeTokenListCount > 0) {
            this.copyElements(index, index + removeTokenListCount, removed, 0);
            this.remove(index, removeTokenListCount);
        }
        this.addAll(index, addTokenLists);
        return removed;
    }

    public TokenList<?> rootTokenList() {
        return this.rootTokenList;
    }

    void childAdded() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public int findIndex(int offset) {
        int high = this.size() - 1;
        int low = 0;
        int rootModCount = this.rootTokenList.modCount();
        while (low <= high) {
            int mid = low + high >>> 1;
            EmbeddedTokenList etl = (EmbeddedTokenList)this.get(mid);
            etl.updateModCount(rootModCount);
            int cmp = etl.startOffset() - offset;
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            low = mid;
            break;
        }
        return low;
    }

    public int findIndexDuringUpdate(EmbeddedTokenList<?, T> targetEtl, TokenHierarchyEventInfo eventInfo) {
        int high = this.size() - 1;
        int low = 0;
        int rootModCount = this.rootTokenList.modCount();
        int targetStartOffset = LexerUtilsConstants.updatedStartOffset(targetEtl, rootModCount, eventInfo);
        block0 : while (low <= high) {
            int mid = low + high >>> 1;
            EmbeddedTokenList etl = (EmbeddedTokenList)this.get(mid);
            int startOffset = LexerUtilsConstants.updatedStartOffset(etl, rootModCount, eventInfo);
            int cmp = startOffset - targetStartOffset;
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            low = mid;
            if (etl == targetEtl) break;
            while (--low >= 0) {
                etl = (EmbeddedTokenList)this.get(low);
                if (etl == targetEtl) {
                    return low;
                }
                if (LexerUtilsConstants.updatedStartOffset(etl, rootModCount, eventInfo) == targetStartOffset) continue;
            }
            low = mid;
            while (++low < this.size()) {
                etl = (EmbeddedTokenList)this.get(low);
                if (etl == targetEtl) {
                    return low;
                }
                if (LexerUtilsConstants.updatedStartOffset(etl, rootModCount, eventInfo) == targetStartOffset) continue;
                break block0;
            }
            break block0;
        }
        return low;
    }

    public String checkConsistency() {
        int lastEndOffset = 0;
        for (int i = 0; i < this.size(); ++i) {
            EmbeddedTokenList etl = (EmbeddedTokenList)this.get(i);
            etl.updateModCount();
            if (etl.isRemoved()) {
                return "TOKEN-LIST-LIST Removed token list at index=" + i + '\n' + (Object)((Object)this);
            }
            if (etl.startOffset() < lastEndOffset) {
                return "TOKEN-LIST-LIST Invalid start offset at index=" + i + ": etl[" + i + "].startOffset()=" + etl.startOffset() + " < lastEndOffset=" + lastEndOffset + "\n" + (Object)((Object)this);
            }
            if (etl.startOffset() > etl.endOffset()) {
                return "TOKEN-LIST-LIST Invalid end offset at index=" + i + ": etl[" + i + "].startOffset()=" + etl.startOffset() + " > etl[" + i + "].endOffset()=" + etl.endOffset() + "\n" + (Object)((Object)this);
            }
            if (etl.isRemoved()) {
                return "TOKEN-LIST-LIST Removed ec at index=" + i + "\n" + (Object)((Object)this);
            }
            lastEndOffset = etl.endOffset();
        }
        if (this.joinSections() && this.size() > 0) {
            return this.joinTokenList().checkConsistency();
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(2048);
        if (this.joinSections()) {
            sb.append("J");
        }
        sb.append("TLL for \"");
        sb.append(this.languagePath().mimePath()).append('\"');
        if (this.hasChildren()) {
            sb.append(", hasChildren");
        }
        sb.append('\n');
        int digitCount = ArrayUtilities.digitCount((int)this.size());
        for (int i = 0; i < this.size(); ++i) {
            EmbeddedTokenList etl = (EmbeddedTokenList)this.get(i);
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            etl.updateModCount();
            etl.dumpInfo(sb);
            sb.append('\n');
        }
        return sb.toString();
    }
}

