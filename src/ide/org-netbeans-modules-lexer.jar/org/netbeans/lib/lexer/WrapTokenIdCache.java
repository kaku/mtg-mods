/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LanguageIds;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.WrapTokenId;

public class WrapTokenIdCache<T extends TokenId> {
    private static final int MAX_LAST_CACHED_IDS = 4;
    private static Reference<WrapTokenIdCache<?>>[] cacheRefs;
    private final Language<T> language;
    private final WrapTokenId<T>[] plainWids;
    private final WrapTokenId<T>[] noDefaultEmbeddingWids;
    private final WrapTokenId<T>[][] lastWids;

    private static int id(Language<?> language) {
        return LexerApiPackageAccessor.get().languageId(language);
    }

    public static synchronized <T extends TokenId> WrapTokenIdCache<T> get(Language<T> language) {
        WrapTokenIdCache cache;
        Reference cacheRef;
        int lid = WrapTokenIdCache.id(language);
        if (cacheRefs == null || lid >= cacheRefs.length) {
            Reference[] n = new Reference[lid + 1];
            if (cacheRefs != null) {
                System.arraycopy(cacheRefs, 0, n, 0, cacheRefs.length);
            }
            cacheRefs = n;
        }
        if ((cacheRef = cacheRefs[lid]) == null || (cache = cacheRef.get()) == null) {
            cache = new WrapTokenIdCache<T>(language);
            WrapTokenIdCache.cacheRefs[lid] = new WeakReference(cache);
        }
        return cache;
    }

    private WrapTokenIdCache(Language<T> language) {
        this.language = language;
        int arrayLen = language.maxOrdinal() + 1;
        this.plainWids = new WrapTokenId[arrayLen];
        this.noDefaultEmbeddingWids = new WrapTokenId[arrayLen];
        for (int i = 0; i < arrayLen; ++i) {
            T id = language.tokenId(i);
            this.plainWids[i] = new WrapTokenId<T>(id);
            this.noDefaultEmbeddingWids[i] = new WrapTokenId<T>(id, LanguageIds.NULL_LANGUAGE_ONLY);
        }
        this.lastWids = new WrapTokenId[arrayLen][];
    }

    public WrapTokenId<T> findWid(T id, LanguageIds failedEmbeddingLanguageIds) {
        if (failedEmbeddingLanguageIds == LanguageIds.EMPTY) {
            return this.plainWid(id);
        }
        if (failedEmbeddingLanguageIds == LanguageIds.NULL_LANGUAGE_ONLY) {
            return this.noDefaultEmbeddingWid(id);
        }
        return this.wid(id, failedEmbeddingLanguageIds);
    }

    public WrapTokenId<T> plainWid(T id) {
        return this.plainWids[id.ordinal()];
    }

    public WrapTokenId<T> noDefaultEmbeddingWid(T id) {
        return this.noDefaultEmbeddingWids[id.ordinal()];
    }

    public WrapTokenId<T> wid(T id, LanguageIds failedEmbeddingLanguageIds) {
        WrapTokenId<T> wid;
        WrapTokenId<T>[] ids = this.lastWids[id.ordinal()];
        if (ids == null) {
            WrapTokenId[] idsL = new WrapTokenId[4];
            ids = idsL;
            this.lastWids[id.ordinal()] = ids;
        }
        for (int i = 0; i < 4 && (wid = ids[i]) != null; ++i) {
            if (wid.languageIds() != failedEmbeddingLanguageIds) continue;
            if (i != 0) {
                System.arraycopy(ids, 0, ids, 1, i);
                ids[0] = wid;
            }
            return wid;
        }
        System.arraycopy(ids, 0, ids, 1, 3);
        WrapTokenId<T> wid2 = new WrapTokenId<T>(id, failedEmbeddingLanguageIds);
        ids[0] = wid2;
        return wid2;
    }
}

