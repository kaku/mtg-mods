/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

public final class IntegerCache {
    private static final int MAX_CACHED_INTEGER = 511;
    private static final Integer[] cache = new Integer[512];

    public static Integer integer(int i) {
        Integer integer;
        if (i <= 511) {
            integer = cache[i];
            if (integer == null) {
                IntegerCache.cache[i] = integer = Integer.valueOf(i);
            }
        } else {
            integer = new Integer(i);
        }
        return integer;
    }
}

