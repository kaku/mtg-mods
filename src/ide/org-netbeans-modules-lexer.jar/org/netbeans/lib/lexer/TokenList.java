/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.token.AbstractToken;

public interface TokenList<T extends TokenId> {
    public static final Logger LOG = Logger.getLogger(TokenList.class.getName());

    public Language<T> language();

    public LanguagePath languagePath();

    public TokenOrEmbedding<T> tokenOrEmbedding(int var1);

    public AbstractToken<T> replaceFlyToken(int var1, AbstractToken<T> var2, int var3);

    public void setTokenOrEmbedding(int var1, TokenOrEmbedding<T> var2);

    public int tokenOffset(int var1);

    public int tokenOffset(AbstractToken<T> var1);

    public int[] tokenIndex(int var1);

    public int tokenCount();

    public int tokenCountCurrent();

    public int modCount();

    public TokenList<?> rootTokenList();

    public CharSequence inputSourceText();

    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation();

    public InputAttributes inputAttributes();

    public int lookahead(int var1);

    public Object state(int var1);

    public boolean isContinuous();

    public Set<T> skipTokenIds();

    public int startOffset();

    public int endOffset();

    public boolean isRemoved();

    public StringBuilder dumpInfo(StringBuilder var1);

    public String dumpInfoType();
}

