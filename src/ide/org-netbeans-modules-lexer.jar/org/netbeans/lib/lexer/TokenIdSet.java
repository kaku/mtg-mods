/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerUtilsConstants;

public final class TokenIdSet<T extends TokenId>
extends AbstractSet<T> {
    final T[] indexedIds;
    private int size = -1;

    public static int findMaxOrdinal(Collection<? extends TokenId> ids) {
        int maxOrdinal = -1;
        for (TokenId id : ids) {
            maxOrdinal = Math.max(maxOrdinal, id.ordinal());
        }
        return maxOrdinal;
    }

    public static <T extends TokenId> void checkIdsFromLanguage(Collection<T> ids, Set<T> languageIds) {
        for (TokenId id : ids) {
            if (id == null || languageIds.contains(id)) continue;
            throw new IllegalArgumentException(id + "not contained in " + languageIds);
        }
    }

    public TokenIdSet(Collection<T> ids, int maxOrdinal, boolean checkDupOrdinals) {
        this.indexedIds = this.allocateIds(maxOrdinal + 1);
        if (ids != null) {
            for (TokenId id : ids) {
                if (id == null) continue;
                if (checkDupOrdinals && this.indexedIds[id.ordinal()] != null) {
                    throw new IllegalStateException(id + " has duplicate ordinal with " + this.indexedIds[id.ordinal()]);
                }
                this.indexedIds[id.ordinal()] = id;
            }
        }
    }

    private T[] allocateIds(int size) {
        return new TokenId[size];
    }

    @Override
    public boolean add(T id) {
        T origId = this.indexedIds[id.ordinal()];
        this.indexedIds[id.ordinal()] = id;
        this.size = -1;
        return origId != null;
    }

    @Override
    public boolean remove(T id) {
        T origId = this.indexedIds[id.ordinal()];
        this.indexedIds[id.ordinal()] = null;
        this.size = -1;
        return origId != null;
    }

    public T[] indexedIds() {
        return this.indexedIds;
    }

    @Override
    public int size() {
        int cnt = this.size;
        if (cnt < 0) {
            cnt = 0;
            Iterator<T> it = this.iterator();
            while (it.hasNext()) {
                it.next();
                ++cnt;
            }
            this.size = cnt;
        }
        return cnt;
    }

    @Override
    public Iterator<T> iterator() {
        return new SkipNullsIterator();
    }

    public boolean containsTokenId(TokenId id) {
        int ordinal = id.ordinal();
        return ordinal >= 0 && ordinal < this.indexedIds.length && this.indexedIds[ordinal] == id;
    }

    @Override
    public boolean contains(Object o) {
        return o instanceof TokenId ? this.containsTokenId((TokenId)o) : false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{\n");
        for (TokenId id : this) {
            sb.append("    ");
            sb.append(LexerUtilsConstants.idToString(id));
            sb.append('\n');
        }
        sb.append("}\n");
        return sb.toString();
    }

    private final class SkipNullsIterator
    implements Iterator<T> {
        private int index;
        private int lastRetIndex;

        SkipNullsIterator() {
            this.lastRetIndex = -1;
        }

        @Override
        public boolean hasNext() {
            while (this.index < TokenIdSet.this.indexedIds.length) {
                if (TokenIdSet.this.indexedIds[this.index] != null) {
                    return true;
                }
                ++this.index;
            }
            return false;
        }

        @Override
        public T next() {
            while (this.index < TokenIdSet.this.indexedIds.length) {
                Object tokenId;
                if ((tokenId = TokenIdSet.this.indexedIds[this.index++]) == null) continue;
                this.lastRetIndex = this.index - 1;
                return tokenId;
            }
            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            if (this.lastRetIndex < 0) {
                throw new IllegalStateException();
            }
            TokenIdSet.this.indexedIds[this.lastRetIndex] = null;
            TokenIdSet.this.size = -1;
        }
    }

}

