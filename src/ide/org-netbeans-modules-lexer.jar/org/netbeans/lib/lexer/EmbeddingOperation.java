/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.LanguageIds;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.WrapTokenIdCache;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyUpdate;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.spi.lexer.EmbeddingPresence;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;

public class EmbeddingOperation {
    private static final Logger LOG = Logger.getLogger(EmbeddingOperation.class.getName());

    public static <T extends TokenId, ET extends TokenId> EmbeddedTokenList<T, ET> embeddedTokenList(TokenList<T> tokenList, int index, Language<?> embeddedLanguage, boolean initTokens) {
        AbstractToken<T> token;
        EmbeddingPresence ep;
        TokenOrEmbedding<T> tokenOrEmbedding = tokenList.tokenOrEmbedding(index);
        EmbeddedTokenList existingEtl = tokenOrEmbedding.embedding();
        if (existingEtl != null) {
            do {
                if (embeddedLanguage == null || embeddedLanguage == existingEtl.language()) {
                    EmbeddedTokenList etlUC = existingEtl;
                    return etlUC;
                }
                EmbeddedTokenList next = existingEtl.nextEmbeddedTokenList();
                if (next == null) break;
                existingEtl = next;
            } while (true);
        }
        if ((token = tokenOrEmbedding.token()).isFlyweight()) {
            return null;
        }
        WrapTokenId<T> wid = token.wid();
        LanguageIds failedEmbeddingLanguageIds = wid.languageIds();
        if (failedEmbeddingLanguageIds == LanguageIds.NULL_LANGUAGE_ONLY) {
            return null;
        }
        if (token.getClass() == JoinToken.class) {
            return null;
        }
        if (tokenList.isRemoved()) {
            return null;
        }
        TokenList rootTokenList = tokenList.rootTokenList();
        TokenHierarchyOperation op = rootTokenList.tokenHierarchyOperation();
        LanguagePath languagePath = tokenList.languagePath();
        Language<T> language = tokenList.language();
        LanguageHierarchy<T> languageHierarchy = LexerUtilsConstants.languageHierarchy(language);
        LanguageOperation<T> languageOperation = LexerUtilsConstants.languageOperation(language);
        if (existingEtl != null) {
            ep = null;
        } else {
            ep = languageOperation.embeddingPresence(token.id());
            if (ep == EmbeddingPresence.NONE) {
                return null;
            }
        }
        LanguageEmbedding embedding = LexerUtilsConstants.findEmbedding(languageHierarchy, token, languagePath, tokenList.inputAttributes());
        if (embedding != null) {
            if (ep == EmbeddingPresence.CACHED_FIRST_QUERY) {
                languageOperation.setEmbeddingPresence(token.id(), EmbeddingPresence.ALWAYS_QUERY);
            }
            if (embeddedLanguage != null && embeddedLanguage != embedding.language()) {
                return null;
            }
            if (embedding.startSkipLength() + embedding.endSkipLength() > token.length()) {
                EmbeddingOperation.addFailedEmbedding(op, token, language, wid, failedEmbeddingLanguageIds, 0);
                return null;
            }
            LanguagePath embeddedLanguagePath = LanguagePath.get(languagePath, embedding.language());
            EmbeddedTokenList etl = new EmbeddedTokenList(rootTokenList, embeddedLanguagePath, embedding);
            int rootModCount = rootTokenList.modCount();
            if (tokenList instanceof EmbeddedTokenList) {
                ((EmbeddedTokenList)tokenList).updateModCount(rootModCount);
            }
            int tokenStartOffset = tokenList.tokenOffset(index);
            etl.reinit(token, tokenStartOffset, rootModCount);
            if (existingEtl == null) {
                tokenList.setTokenOrEmbedding(index, etl);
            } else {
                existingEtl.setNextEmbeddedTokenList(etl);
            }
            if (initTokens) {
                if (embedding.joinSections()) {
                    op.tokenListList(embeddedLanguagePath);
                } else {
                    TokenListList tll;
                    assert ((tll = op.existingTokenListList(embeddedLanguagePath)) == null);
                    etl.initAllTokens();
                }
            }
            if (LOG.isLoggable(Level.FINE)) {
                StringBuilder sb = new StringBuilder(200);
                sb.append("@@@@@@@@@@ NATURAL-EMBEDDING-CREATED ETL-");
                LexerUtilsConstants.appendIdentityHashCode(sb, etl);
                sb.append(" ROOT-");
                LexerUtilsConstants.appendIdentityHashCode(sb, rootTokenList);
                sb.append(" for ").append(embeddedLanguagePath.mimePath()).append(", ").append(embedding).append(": ");
                etl.dumpInfo(sb);
                LOG.fine(sb.toString());
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.log(Level.INFO, "Natural embedding created by:", new Exception());
                }
            }
            return etl;
        }
        EmbeddingOperation.addFailedEmbedding(op, token, language, wid, failedEmbeddingLanguageIds, 0);
        if (ep == EmbeddingPresence.CACHED_FIRST_QUERY) {
            languageOperation.setEmbeddingPresence(token.id(), EmbeddingPresence.NONE);
        }
        return null;
    }

    private static <T extends TokenId> void addFailedEmbedding(TokenHierarchyOperation<?, ?> op, AbstractToken<T> token, Language<T> language, WrapTokenId<T> wid, LanguageIds failedEmbeddingLanguageIds, int failedEmbeddingLanguageId) {
        WrapTokenIdCache cache = op.getWrapTokenIdCache(language);
        failedEmbeddingLanguageIds = LanguageIds.get(failedEmbeddingLanguageIds, failedEmbeddingLanguageId);
        wid = cache.findWid(wid.id(), failedEmbeddingLanguageIds);
        token.setWid(wid);
    }

    public static <T extends TokenId, ET extends TokenId> boolean createEmbedding(TokenList<T> tokenList, int index, Language<ET> embeddedLanguage, int startSkipLength, int endSkipLength, boolean joinSections) {
        TokenListList tll;
        if (embeddedLanguage == null) {
            throw new IllegalArgumentException("embeddedLanguage parameter cannot be null");
        }
        TokenList rootTokenList = tokenList.rootTokenList();
        if (tokenList.isRemoved()) {
            return false;
        }
        TokenHierarchyOperation tokenHierarchyOperation = tokenList.tokenHierarchyOperation();
        tokenHierarchyOperation.ensureWriteLocked();
        LanguagePath languagePath = tokenList.languagePath();
        LanguagePath embeddedLanguagePath = LanguagePath.get(languagePath, embeddedLanguage);
        TokenOrEmbedding<T> tokenOrEmbedding = tokenList.tokenOrEmbedding(index);
        EmbeddedTokenList existingEtl = tokenOrEmbedding.embedding();
        if (existingEtl != null) {
            do {
                if (embeddedLanguagePath == existingEtl.languagePath()) {
                    return true;
                }
                EmbeddedTokenList next = existingEtl.nextEmbeddedTokenList();
                if (next == null) break;
                existingEtl = next;
            } while (true);
        }
        if ((tll = tokenHierarchyOperation.existingTokenListList(embeddedLanguagePath)) != null && tll.joinSections()) {
            joinSections = true;
        }
        LanguageEmbedding<ET> embedding = LanguageEmbedding.create(embeddedLanguage, startSkipLength, endSkipLength, joinSections);
        tokenHierarchyOperation.addLanguagePath(embeddedLanguagePath);
        AbstractToken<T> token = tokenOrEmbedding.token();
        if (startSkipLength + endSkipLength > token.length()) {
            return false;
        }
        if (token.isFlyweight()) {
            return false;
        }
        EmbeddedTokenList<T, ET> etl = new EmbeddedTokenList<T, ET>(rootTokenList, embeddedLanguagePath, embedding);
        int rootModCount = rootTokenList.modCount();
        if (tokenList instanceof EmbeddedTokenList) {
            ((EmbeddedTokenList)tokenList).updateModCount(rootModCount);
        }
        int tokenStartOffset = tokenList.tokenOffset(index);
        etl.reinit(token, tokenStartOffset, rootModCount);
        if (existingEtl == null) {
            tokenList.setTokenOrEmbedding(index, etl);
        } else {
            existingEtl.setNextEmbeddedTokenList(etl);
        }
        TokenHierarchyEventInfo eventInfo = new TokenHierarchyEventInfo(tokenHierarchyOperation, TokenHierarchyEventType.EMBEDDING_CREATED, tokenStartOffset, 0, "", 0);
        eventInfo.setMaxAffectedEndOffset(tokenStartOffset + token.length());
        if (tll != null) {
            new TokenHierarchyUpdate(eventInfo).updateCreateOrRemoveEmbedding(etl, true);
        } else if (joinSections) {
            tll = tokenHierarchyOperation.tokenListList(embeddedLanguagePath);
        } else {
            etl.initAllTokens();
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("@@@@@@@@@@ EXPLICIT-EMBEDDING-CREATED for " + embeddedLanguagePath.mimePath() + ", " + embedding + ": " + etl.dumpInfo(new StringBuilder(256)) + '\n');
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.INFO, "Explicit embedding created by:", new Exception());
            }
        }
        TokenChangeInfo<T> info = new TokenChangeInfo<T>(tokenList);
        info.setIndex(index);
        info.setOffset(tokenStartOffset);
        eventInfo.setTokenChangeInfo(info);
        TokenChangeInfo embeddedInfo = new TokenChangeInfo(etl);
        embeddedInfo.setIndex(0);
        embeddedInfo.setOffset(tokenStartOffset + embedding.startSkipLength());
        info.addEmbeddedChange(embeddedInfo);
        tokenHierarchyOperation.fireTokenHierarchyChanged(eventInfo);
        return true;
    }

    public static <T extends TokenId, ET extends TokenId> boolean removeEmbedding(TokenList<T> tokenList, int index, Language<ET> embeddedLanguage) {
        EmbeddedTokenList etl;
        TokenList rootTokenList = tokenList.rootTokenList();
        if (tokenList.isRemoved()) {
            return false;
        }
        TokenHierarchyOperation tokenHierarchyOperation = tokenList.tokenHierarchyOperation();
        tokenHierarchyOperation.ensureWriteLocked();
        TokenOrEmbedding<T> tokenOrEmbedding = tokenList.tokenOrEmbedding(index);
        if (etl != null) {
            int rootModCount = rootTokenList.modCount();
            EmbeddedTokenList lastEtl = null;
            for (etl = tokenOrEmbedding.embedding(); etl != null; etl = etl.nextEmbeddedTokenList()) {
                etl.updateModCount(rootModCount);
                if (embeddedLanguage != etl.language()) continue;
                EmbeddedTokenList next = etl.nextEmbeddedTokenList();
                if (lastEtl == null) {
                    if (next == null) {
                        tokenList.setTokenOrEmbedding(index, etl.token());
                    } else {
                        tokenList.setTokenOrEmbedding(index, next);
                    }
                } else {
                    lastEtl.setNextEmbeddedTokenList(next);
                }
                etl.setNextEmbeddedTokenList(null);
                etl.markRemoved();
                etl.markChildrenRemovedDeep();
                int startOffset = etl.branchTokenStartOffset();
                TokenHierarchyEventInfo eventInfo = new TokenHierarchyEventInfo(tokenHierarchyOperation, TokenHierarchyEventType.EMBEDDING_REMOVED, startOffset, 0, "", 0);
                eventInfo.setMaxAffectedEndOffset(startOffset + etl.token().length());
                TokenChangeInfo<T> info = new TokenChangeInfo<T>(tokenList);
                info.setIndex(index);
                info.setOffset(startOffset);
                eventInfo.setTokenChangeInfo(info);
                EmbeddedTokenList etlET = etl;
                TokenChangeInfo embeddedInfo = new TokenChangeInfo(etlET);
                embeddedInfo.setIndex(0);
                embeddedInfo.setOffset(startOffset + etl.languageEmbedding().startSkipLength());
                info.addEmbeddedChange(embeddedInfo);
                TokenListList tll = tokenHierarchyOperation.existingTokenListList(etl.languagePath());
                if (tll != null) {
                    new TokenHierarchyUpdate(eventInfo).updateCreateOrRemoveEmbedding(etl, false);
                }
                tokenHierarchyOperation.fireTokenHierarchyChanged(eventInfo);
                return true;
            }
        }
        return false;
    }
}

