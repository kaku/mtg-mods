/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.CharPreprocessor;
import org.netbeans.lib.lexer.CharProvider;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.PreprocessedTextStorage;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.token.AbstractToken;

public final class CharPreprocessorOperation
implements CharProvider {
    private CharProvider parent;
    private CharPreprocessor preprocessor;
    private int readIndex;
    private int lookaheadIndex;
    private int prepStartIndex;
    private int prepEndIndex;
    private char[] prepChars = ArrayUtilities.emptyCharArray();
    private int[] rawLengthShifts;
    private int lastOutputChar;
    private int tokenLength;
    private LexerInputOperation lexerInputOperation;
    private int tokenEndRawLengthShift;

    CharPreprocessorOperation(CharProvider parent, CharPreprocessor preprocessor, LexerInputOperation lexerInputOperation) {
        this.parent = parent;
        this.preprocessor = preprocessor;
        this.lexerInputOperation = lexerInputOperation;
    }

    public void initApprovedToken(AbstractToken token) {
    }

    public int inputRead() {
        return this.parent.read();
    }

    public void inputBackup(int count) {
        this.parent.backup(count);
    }

    public void outputOriginal(int ch) {
        this.lastOutputChar = ch;
        if (ch != -1) {
            if (this.prepStartIndex == this.lookaheadIndex) {
                ++this.prepStartIndex;
            }
            ++this.lookaheadIndex;
        }
    }

    public void outputPreprocessed(char ch, int extraInputLength) {
        this.lastOutputChar = ch;
        if (this.prepStartIndex == this.lookaheadIndex) {
            this.prepEndIndex = this.prepStartIndex;
        } else if (this.prepEndIndex < this.lookaheadIndex) {
            do {
                this.addPrepChar(this.parent.readExisting(this.prepEndIndex), 0);
            } while (this.prepEndIndex < this.lookaheadIndex);
        }
        this.addPrepChar(ch, extraInputLength);
        ++this.lookaheadIndex;
    }

    @Override
    public int deepRawLength(int length) {
        return this.parent.deepRawLength(this.parentLength(length));
    }

    @Override
    public int deepRawLengthShift(int index) {
        return this.rawLengthShift(index) + this.parent.deepRawLengthShift(index);
    }

    private int rawLengthShift(int index) {
        if (index < this.prepStartIndex) {
            return index;
        }
        if (index < this.prepEndIndex) {
            return this.rawLengthShifts[index - this.prepStartIndex];
        }
        return this.totalRawLengthShift();
    }

    private int parentLength(int length) {
        if (length > this.prepStartIndex) {
            length = length <= this.prepEndIndex ? (length += this.rawLengthShifts[length - 1 - this.prepStartIndex]) : (length += this.totalRawLengthShift());
        }
        return length;
    }

    private int totalRawLengthShift() {
        return this.rawLengthShifts[this.prepEndIndex - 1 - this.prepStartIndex];
    }

    public void notifyError(String errorMessage) {
        if (this.lexerInputOperation != null) {
            int parentIndex = this.parent.readIndex();
        }
    }

    @Override
    public int read() {
        if (this.readIndex == this.lookaheadIndex) {
            ++this.readIndex;
            if (this.readIndex == this.lookaheadIndex) {
                return this.lastOutputChar;
            }
            --this.readIndex;
            if (this.readIndex == this.lookaheadIndex && this.lastOutputChar == -1) {
                return -1;
            }
        }
        return this.readExisting(this.readIndex++);
    }

    @Override
    public char readExisting(int index) {
        return index < this.prepStartIndex ? this.parent.readExisting(index) : (index < this.prepEndIndex ? this.prepChars[index - this.prepStartIndex] : this.parent.readExisting(index + this.totalRawLengthShift()));
    }

    @Override
    public int readIndex() {
        return this.readIndex;
    }

    @Override
    public void backup(int count) {
        this.readIndex -= count;
    }

    @Override
    public int tokenLength() {
        return this.tokenLength;
    }

    @Override
    public void assignTokenLength(int tokenLength, boolean skipToken) {
        this.tokenLength = tokenLength;
        this.parent.assignTokenLength(this.parentLength(tokenLength), skipToken);
    }

    public PreprocessedTextStorage createPreprocessedTextStorage(CharSequence rawText, CharProvider.ExtraPreprocessedChars epc) {
        int pEndIndex;
        int pStartIndex;
        PreprocessedTextStorage prepStorage;
        int topEndIndex;
        if (this.prepStartIndex >= this.tokenLength) {
            if (this.prepEndIndex > this.tokenLength) {
                this.updateTokenEndRawLengthShift();
                pEndIndex = this.tokenLength - 1;
                while (--pEndIndex >= this.prepStartIndex && this.rawLengthShifts[pEndIndex] == this.tokenEndRawLengthShift) {
                }
                pEndIndex += 2;
            } else {
                pEndIndex = this.prepEndIndex;
            }
            topEndIndex = this.parentLength(pEndIndex);
            for (int i = this.prepStartIndex; i < pEndIndex; ++i) {
                this.rawLengthShifts[i - this.prepStartIndex] = this.deepRawLength(i + 1) - (i + 1);
            }
            pStartIndex = this.prepStartIndex;
        } else {
            pStartIndex = this.tokenLength;
            pEndIndex = this.tokenLength;
            topEndIndex = this.tokenLength;
        }
        if (epc != null) {
            this.parent.collectExtraPreprocessedChars(epc, pStartIndex, pEndIndex, topEndIndex);
            prepStorage = PreprocessedTextStorage.create(rawText, this.prepChars, pEndIndex - pStartIndex, pStartIndex, this.rawLengthShifts, epc.extraPrepChars(), epc.extraRawLengthShifts(), epc.preStartIndex(), epc.postEndIndex());
            epc.clear();
        } else {
            prepStorage = PreprocessedTextStorage.create(rawText, this.prepChars, pEndIndex - pStartIndex, pStartIndex, this.rawLengthShifts);
        }
        return prepStorage;
    }

    private void updateTokenEndRawLengthShift() {
        this.tokenEndRawLengthShift = this.rawLengthShifts[this.tokenLength - 1 - this.prepStartIndex];
    }

    @Override
    public void collectExtraPreprocessedChars(CharProvider.ExtraPreprocessedChars epc, int prepStartIndex, int prepEndIndex, int topPrepEndIndex) {
        if (prepStartIndex < this.tokenLength) {
            int postCount;
            int preCount = Math.max(prepStartIndex - this.prepStartIndex, 0);
            if (this.prepEndIndex > this.tokenLength) {
                this.updateTokenEndRawLengthShift();
                if (postCount > 0) {
                    int i = this.tokenLength - 2;
                    for (postCount = this.tokenLength - prepEndIndex; --i >= prepStartIndex && postCount > 0 && this.rawLengthShifts[i] == this.tokenEndRawLengthShift; --postCount) {
                    }
                } else {
                    postCount = 0;
                }
            } else {
                postCount = this.prepEndIndex - prepEndIndex;
            }
            assert (preCount >= 0 && postCount >= 0);
            epc.ensureExtraLength(preCount + postCount);
            while (--preCount >= 0) {
                epc.insert(this.readExisting(prepStartIndex - 1), this.deepRawLength(prepStartIndex) - prepStartIndex);
                --prepStartIndex;
            }
            while (--postCount >= 0) {
                epc.append(this.readExisting(prepEndIndex), this.deepRawLength(prepEndIndex) - topPrepEndIndex);
                ++prepEndIndex;
                ++topPrepEndIndex;
            }
        }
        this.parent.collectExtraPreprocessedChars(epc, prepStartIndex, prepEndIndex, topPrepEndIndex);
    }

    @Override
    public void consumeTokenLength() {
        if (this.prepStartIndex != this.lookaheadIndex) {
            if (this.prepStartIndex < this.tokenLength) {
                if (this.prepEndIndex <= this.tokenLength) {
                    this.prepStartIndex = this.lookaheadIndex;
                } else {
                    int i = this.tokenLength;
                    while (i < this.prepEndIndex) {
                        int[] arrn = this.rawLengthShifts;
                        int n = i++;
                        arrn[n] = arrn[n] - this.tokenEndRawLengthShift;
                    }
                    System.arraycopy(this.prepChars, this.prepStartIndex, this.prepChars, 0, this.prepEndIndex - this.prepStartIndex);
                    System.arraycopy(this.rawLengthShifts, this.prepStartIndex, this.rawLengthShifts, 0, this.prepEndIndex - this.prepStartIndex);
                    this.prepStartIndex = 0;
                    this.prepEndIndex -= this.tokenLength;
                }
            } else {
                this.prepStartIndex -= this.tokenLength;
                this.prepEndIndex -= this.tokenLength;
            }
        } else {
            this.prepStartIndex -= this.tokenLength;
        }
        this.readIndex -= this.tokenLength;
        this.lookaheadIndex -= this.tokenLength;
        this.parent.consumeTokenLength();
        if (TokenList.LOG.isLoggable(Level.FINE)) {
            this.consistencyCheck();
        }
    }

    private void addPrepChar(char ch, int extraInputLength) {
        int prepCharsLength = this.prepEndIndex - this.prepStartIndex;
        if (prepCharsLength == this.prepChars.length) {
            this.prepChars = ArrayUtilities.charArray((char[])this.prepChars);
            this.rawLengthShifts = ArrayUtilities.intArray((int[])this.rawLengthShifts);
        }
        this.prepChars[prepCharsLength] = ch;
        int prevRawLengthShift = prepCharsLength > 0 ? this.rawLengthShifts[prepCharsLength - 1] : 0;
        this.rawLengthShifts[prepCharsLength] = prevRawLengthShift + extraInputLength;
        ++this.prepEndIndex;
    }

    private void consistencyCheck() {
        if (this.readIndex > this.lookaheadIndex) {
            throw new IllegalStateException("readIndex > lookaheadIndex: " + this);
        }
        if (this.prepStartIndex > this.lookaheadIndex) {
            throw new IllegalStateException("prepStartIndex > lookaheadIndex: " + this);
        }
        if (this.prepStartIndex != this.lookaheadIndex && this.prepStartIndex >= this.prepEndIndex) {
            throw new IllegalStateException("prepStartIndex >= prepEndIndex: " + this);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("readIndex=");
        sb.append(this.readIndex);
        sb.append(", lookaheadIndex=");
        sb.append(this.lookaheadIndex);
        sb.append(", prepStartIndex=");
        sb.append(this.prepStartIndex);
        sb.append(", prepEndIndex=");
        sb.append(this.prepEndIndex);
        return sb.toString();
    }
}

