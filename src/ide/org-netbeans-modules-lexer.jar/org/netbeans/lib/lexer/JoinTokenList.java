/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.EmbeddedJoinInfo;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinLexerInputOperation;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.inc.JoinTokenListChange;
import org.netbeans.lib.lexer.inc.MutableTokenList;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.lib.lexer.token.PartToken;

public final class JoinTokenList<T extends TokenId>
implements MutableTokenList<T> {
    private static final Logger LOG = Logger.getLogger(JoinTokenList.class.getName());
    private static final int INDEX_GAP_LENGTH_INITIAL_SIZE = 1073741823;
    protected final TokenListList<T> tokenListList;
    private int tokenListCount;
    private int joinTokenCount;
    private int indexGapsIndex;
    private int joinTokenIndexGapLength = 1073741823;
    private int tokenListIndexGapLength = 1073741823;
    protected int activeTokenListIndex;
    protected EmbeddedTokenList<?, T> activeTokenList;
    protected int activeStartJoinIndex;
    protected int activeEndJoinIndex;
    private int extraModCount;
    static boolean tested = false;

    static <T extends TokenId> JoinTokenList<T> create(TokenListList<T> tokenListList) {
        JoinTokenList<T> jtl = new JoinTokenList<T>(tokenListList);
        jtl.init();
        return jtl;
    }

    private JoinTokenList(TokenListList<T> tokenListList) {
        this.tokenListList = tokenListList;
        this.tokenListCount = tokenListList.size();
        this.resetActiveTokenList();
    }

    @Override
    public Language<T> language() {
        return LexerUtilsConstants.innerLanguage(this.languagePath());
    }

    @Override
    public LanguagePath languagePath() {
        return this.tokenListList.languagePath();
    }

    public TokenListList<T> tokenListList() {
        return this.tokenListList;
    }

    public EmbeddedTokenList<?, T> tokenList(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("index=" + index + " < 0");
        }
        if (index >= this.tokenListCount) {
            throw new IndexOutOfBoundsException("index=" + index + " >= size()=" + this.tokenListCount);
        }
        return (EmbeddedTokenList)this.tokenListList.get(index);
    }

    public int tokenListCount() {
        return this.tokenListCount;
    }

    @Override
    public int tokenCountCurrent() {
        return this.joinTokenCount;
    }

    @Override
    public int tokenCount() {
        return this.tokenCountCurrent();
    }

    public int activeStartJoinIndex() {
        return this.activeStartJoinIndex;
    }

    public int activeEndJoinIndex() {
        return this.activeEndJoinIndex;
    }

    public int activeTokenListIndex() {
        return this.activeTokenListIndex;
    }

    public void setActiveTokenListIndex(int activeTokenListIndex) {
        if (this.activeTokenListIndex != activeTokenListIndex) {
            this.forceActiveTokenListIndex(activeTokenListIndex);
        }
    }

    private void forceActiveTokenListIndex(int activeTokenListIndex) {
        this.activeTokenListIndex = activeTokenListIndex;
        this.fetchActiveTokenListData();
    }

    private void fetchActiveTokenListData() {
        if (this.activeTokenListIndex >= this.tokenListCount) {
            if (!tested) {
                tested = true;
                StringBuilder sb = new StringBuilder(200);
                this.dumpInfo(sb);
                throw new IllegalStateException("Bad index activeTokenListIndex=" + this.activeTokenListIndex + "; " + sb);
            }
            return;
        }
        this.activeTokenList = this.tokenList(this.activeTokenListIndex);
        this.activeTokenList.updateModCount(this.rootTokenList().modCount());
        this.activeStartJoinIndex = this.activeTokenList.joinInfo().joinTokenIndex();
        this.activeEndJoinIndex = this.activeStartJoinIndex + this.activeTokenList.joinTokenCount();
    }

    void resetActiveTokenList() {
        this.activeTokenListIndex = -1;
        this.activeTokenList = null;
        this.activeEndJoinIndex = 0;
        this.activeStartJoinIndex = 0;
    }

    public EmbeddedTokenList<?, T> activeTokenList() {
        return this.activeTokenList;
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        AbstractToken<T> token;
        if (index >= this.joinTokenCount) {
            return null;
        }
        this.findTokenListByJoinIndex(index);
        TokenOrEmbedding<T> tokenOrEmbedding = this.activeTokenList.tokenOrEmbedding(index - this.activeStartJoinIndex);
        if (index == this.activeStartJoinIndex && (token = tokenOrEmbedding.token()).getClass() == PartToken.class) {
            tokenOrEmbedding = ((PartToken)token).joinTokenOrEmbedding();
        }
        return tokenOrEmbedding;
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        throw new IllegalStateException("Internal error - should never be called");
    }

    @Override
    public int tokenOffset(int index) {
        AbstractToken<T> token;
        this.findTokenListByJoinIndex(index);
        if (index == this.activeStartJoinIndex && (token = this.activeTokenList.tokenOrEmbedding(index - this.activeStartJoinIndex).token()).getClass() == PartToken.class) {
            return ((PartToken)token).joinToken().offset(null);
        }
        return this.activeTokenList.tokenOffset(index - this.activeStartJoinIndex);
    }

    public int tokenListIndex(int offset, int startIndex, int endIndex) {
        int low = startIndex;
        int high = endIndex - 1;
        while (low <= high) {
            int mid = low + high >>> 1;
            int midStartOffset = this.tokenList(mid).startOffset();
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            high = mid;
            break;
        }
        return high;
    }

    @Override
    public int[] tokenIndex(int offset) {
        boolean activeStartsBelowOffset;
        AbstractToken<T> token;
        boolean bl = activeStartsBelowOffset = offset >= this.activeTokenList.startOffset() || this.activeTokenListIndex == 0;
        if (activeStartsBelowOffset) {
            if (offset >= this.activeTokenList.endOffset() && this.activeTokenListIndex + 1 != this.tokenListCount() && offset >= this.tokenList(this.activeTokenListIndex + 1).startOffset() && this.activeTokenListIndex + 1 < this.tokenListCount()) {
                this.activeTokenListIndex = this.tokenListIndex(offset, this.activeTokenListIndex + 1, this.tokenListCount);
                if (this.activeTokenListIndex == -1 && this.tokenListCount > 0) {
                    this.activeTokenListIndex = 0;
                }
                this.fetchActiveTokenListData();
            }
        } else if (this.activeTokenListIndex > 0) {
            this.activeTokenListIndex = this.tokenListIndex(offset, 0, this.activeTokenListIndex);
            if (this.activeTokenListIndex == -1 && this.tokenListCount > 0) {
                this.activeTokenListIndex = 0;
            }
            this.fetchActiveTokenListData();
        }
        EmbeddedJoinInfo<T> joinInfo = this.activeTokenList.joinInfo();
        int joinTokenLastPartShift = joinInfo.joinTokenLastPartShift();
        int searchETLTokenCount = this.activeTokenList.joinTokenCount();
        int[] indexAndTokenOffset = LexerUtilsConstants.tokenIndexBinSearch(this.activeTokenList, offset, searchETLTokenCount);
        int etlIndex = indexAndTokenOffset[0];
        int[] arrn = indexAndTokenOffset;
        arrn[0] = arrn[0] + joinInfo.joinTokenIndex();
        if (etlIndex == searchETLTokenCount && joinTokenLastPartShift > 0) {
            this.activeTokenListIndex += joinTokenLastPartShift;
            this.fetchActiveTokenListData();
            PartToken lastPartToken = (PartToken)this.activeTokenList.tokenOrEmbeddingDirect(0).token();
            indexAndTokenOffset[1] = lastPartToken.joinToken().offset(null);
        } else if (etlIndex == 0 && (token = this.activeTokenList.tokenOrEmbedding(0).token()).getClass() == PartToken.class) {
            indexAndTokenOffset[1] = ((PartToken)token).joinToken().offset(null);
        }
        return indexAndTokenOffset;
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        this.findTokenListByJoinIndex(index);
        return this.activeTokenList.replaceFlyToken(index - this.activeStartJoinIndex, flyToken, offset);
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        this.findTokenListByJoinIndex(index);
        this.activeTokenList.setTokenOrEmbedding(index - this.activeStartJoinIndex, t);
    }

    @Override
    public final int modCount() {
        return this.rootTokenList().modCount() + this.extraModCount;
    }

    @Override
    public InputAttributes inputAttributes() {
        return this.rootTokenList().inputAttributes();
    }

    @Override
    public int lookahead(int index) {
        this.findTokenListByJoinIndex(index);
        return this.activeTokenList.lookahead(index - this.activeStartJoinIndex);
    }

    @Override
    public Object state(int index) {
        this.findTokenListByJoinIndex(index);
        return this.activeTokenList.state(index - this.activeStartJoinIndex);
    }

    @Override
    public final TokenList<?> rootTokenList() {
        return this.tokenListList.rootTokenList();
    }

    @Override
    public CharSequence inputSourceText() {
        return this.rootTokenList().inputSourceText();
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.rootTokenList().tokenHierarchyOperation();
    }

    @Override
    public boolean isContinuous() {
        return false;
    }

    @Override
    public Set<T> skipTokenIds() {
        return null;
    }

    @Override
    public int startOffset() {
        if (this.tokenListCount == 0) {
            return 0;
        }
        if (this.activeTokenListIndex == 0) {
            return this.activeTokenList.startOffset();
        }
        EmbeddedTokenList firstEtl = this.tokenList(0);
        firstEtl.updateModCount();
        return firstEtl.startOffset();
    }

    @Override
    public int endOffset() {
        int tokenListCountM1 = this.tokenListCount() - 1;
        if (tokenListCountM1 < 0) {
            return 0;
        }
        if (this.activeTokenListIndex == tokenListCountM1) {
            return this.activeTokenList.endOffset();
        }
        EmbeddedTokenList lastEtl = this.tokenList(tokenListCountM1);
        lastEtl.updateModCount();
        return lastEtl.endOffset();
    }

    @Override
    public boolean isRemoved() {
        return false;
    }

    public int tokenStartLocalIndex(int index) {
        this.findTokenListByJoinIndex(index);
        AbstractToken<T> token = this.activeTokenList.tokenOrEmbeddingDirect(index - this.activeStartJoinIndex).token();
        if (token.getClass() == PartToken.class) {
            PartToken partToken = (PartToken)token;
            this.activeTokenListIndex -= partToken.joinToken().extraTokenListSpanCount();
            this.fetchActiveTokenListData();
            return this.activeTokenList.tokenCountCurrent() - 1;
        }
        return index - this.activeStartJoinIndex;
    }

    protected final void findTokenListByJoinIndex(int joinIndex) {
        if (joinIndex < this.activeStartJoinIndex) {
            if (joinIndex < 0) {
                throw new IndexOutOfBoundsException("index=" + joinIndex + " < 0");
            }
            --this.activeTokenListIndex;
            this.fetchActiveTokenListData();
            if (joinIndex < this.activeStartJoinIndex) {
                this.findTokenListBinSearch(joinIndex, 0, this.activeTokenListIndex - 1);
            }
        } else if (joinIndex == this.activeEndJoinIndex) {
            int lps;
            int n = lps = this.activeTokenList != null ? this.activeTokenList.joinInfo().joinTokenLastPartShift() : 0;
            if (lps > 0) {
                this.activeTokenListIndex += lps;
                this.fetchActiveTokenListData();
            } else {
                ++this.activeTokenListIndex;
                this.fetchActiveTokenListData();
                this.adjustJoinedOrSkipEmptyUp(joinIndex);
            }
        } else if (joinIndex > this.activeEndJoinIndex) {
            ++this.activeTokenListIndex;
            this.fetchActiveTokenListData();
            if (joinIndex >= this.activeEndJoinIndex) {
                this.findTokenListBinSearch(joinIndex, this.activeTokenListIndex + 1, this.tokenListCount - 1);
            }
        }
    }

    private void findTokenListBinSearch(int joinIndex, int low, int high) {
        while (low <= high) {
            this.activeTokenListIndex = low + high >>> 1;
            this.fetchActiveTokenListData();
            if (this.activeStartJoinIndex < joinIndex) {
                low = this.activeTokenListIndex + 1;
                continue;
            }
            if (this.activeStartJoinIndex > joinIndex) {
                high = this.activeTokenListIndex - 1;
                continue;
            }
            this.adjustJoinedOrSkipEmptyUp(joinIndex);
            return;
        }
        if (this.activeTokenListIndex != high) {
            this.activeTokenListIndex = high;
            this.fetchActiveTokenListData();
        }
    }

    private void adjustJoinedOrSkipEmptyUp(int joinIndex) {
        while (joinIndex == this.activeEndJoinIndex) {
            if (this.activeTokenList.tokenCountCurrent() == 0) {
                ++this.activeTokenListIndex;
                this.fetchActiveTokenListData();
                continue;
            }
            int lps = this.activeTokenList.joinInfo().joinTokenLastPartShift();
            if (lps <= 0) continue;
            this.activeTokenListIndex += lps;
            this.fetchActiveTokenListData();
        }
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbeddingDirect(int index) {
        return this.tokenOrEmbedding(index);
    }

    @Override
    public boolean isFullyLexed() {
        return true;
    }

    @Override
    public void replaceTokens(TokenListChange<T> change, TokenHierarchyEventInfo eventInfo, boolean modInside) {
        this.markModified();
        ((JoinTokenListChange)change).replaceTokens(eventInfo);
    }

    private void markModified() {
        ++this.extraModCount;
    }

    @Override
    public LexerInputOperation<T> createLexerInputOperation(int tokenIndex, int relexOffset, Object relexState) {
        throw new IllegalStateException("Should never be called");
    }

    public void setPrevActiveTokenListIndex() {
        --this.activeTokenListIndex;
        this.fetchActiveTokenListData();
    }

    public void resetActiveAfterUpdate() {
        if (this.activeTokenListIndex != -1) {
            if (this.tokenListCount == 0) {
                this.resetActiveTokenList();
            } else {
                this.activeTokenListIndex = Math.min(this.activeTokenListIndex, this.tokenListCount - 1);
                this.fetchActiveTokenListData();
            }
        }
    }

    int joinTokenIndex(int rawJoinTokenIndex) {
        return rawJoinTokenIndex < this.joinTokenIndexGapLength ? rawJoinTokenIndex : rawJoinTokenIndex - this.joinTokenIndexGapLength;
    }

    int tokenListIndex(int rawTokenListIndex) {
        return rawTokenListIndex < this.tokenListIndexGapLength ? rawTokenListIndex : rawTokenListIndex - this.tokenListIndexGapLength;
    }

    public void moveIndexGap(int index) {
        if (index < this.indexGapsIndex) {
            int i = index;
            do {
                EmbeddedJoinInfo joinInfo = ((EmbeddedTokenList)this.tokenListList.get(i++)).joinInfo();
                joinInfo.setRawTokenListIndex(joinInfo.getRawTokenListIndex() + this.tokenListIndexGapLength);
                joinInfo.setRawJoinTokenIndex(joinInfo.getRawJoinTokenIndex() + this.joinTokenIndexGapLength);
            } while (i < this.indexGapsIndex);
            this.indexGapsIndex = index;
        } else if (index > this.indexGapsIndex) {
            int i = index;
            do {
                EmbeddedJoinInfo joinInfo = ((EmbeddedTokenList)this.tokenListList.get(--i)).joinInfo();
                joinInfo.setRawTokenListIndex(joinInfo.getRawTokenListIndex() - this.tokenListIndexGapLength);
                joinInfo.setRawJoinTokenIndex(joinInfo.getRawJoinTokenIndex() - this.joinTokenIndexGapLength);
            } while (i > this.indexGapsIndex);
            this.indexGapsIndex = index;
        }
    }

    public void tokenListsModified(int tokenListCountDiff) {
        this.indexGapsIndex += tokenListCountDiff;
        this.tokenListCount += tokenListCountDiff;
        this.tokenListIndexGapLength -= tokenListCountDiff;
    }

    public void updateJoinTokenCount(int joinTokenCountDiff) {
        this.joinTokenCount += joinTokenCountDiff;
        this.joinTokenIndexGapLength -= joinTokenCountDiff;
    }

    private JoinLexerInputOperation<T> createLexerInputOperation() {
        JoinLexerInputOperation lexerInputOperation = new JoinLexerInputOperation(this, 0, null, 0, this.startOffset());
        lexerInputOperation.init();
        return lexerInputOperation;
    }

    private void init() {
        JoinLexerInputOperation<T> lexerInputOperation = this.createLexerInputOperation();
        int tokenListIndex = 0;
        int newJoinTokenCount = 0;
        if (this.tokenListCount > 0) {
            AbstractToken token;
            boolean loggable = LOG.isLoggable(Level.FINE);
            EmbeddedTokenList tokenList = this.initTokenList(tokenListIndex, newJoinTokenCount);
            while ((token = lexerInputOperation.nextToken()) != null) {
                int skipTokenListCount = lexerInputOperation.skipTokenListCount();
                if (skipTokenListCount > 0) {
                    while (--skipTokenListCount >= 0) {
                        tokenList = this.initTokenList(++tokenListIndex, newJoinTokenCount);
                    }
                    lexerInputOperation.clearSkipTokenListCount();
                }
                if (token.getClass() == JoinToken.class) {
                    JoinToken joinToken = (JoinToken)token;
                    List joinedParts = joinToken.joinedParts();
                    int extraTokenListSpanCount = joinToken.extraTokenListSpanCount();
                    int joinedPartIndex = 0;
                    for (int i = 0; i < extraTokenListSpanCount; ++i) {
                        tokenList.joinInfo().setJoinTokenLastPartShift(extraTokenListSpanCount - i);
                        if (tokenList.textLength() > 0) {
                            tokenList.addToken(joinedParts.get(joinedPartIndex++), 0, null);
                        }
                        tokenList = this.initTokenList(++tokenListIndex, newJoinTokenCount);
                    }
                    token = joinedParts.get(joinedPartIndex);
                }
                tokenList.addToken(token, lexerInputOperation);
                if (loggable) {
                    StringBuilder sb = new StringBuilder(50);
                    ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)newJoinTokenCount, (int)2);
                    token.dumpInfo(sb, null, true, true, 0);
                    sb.append('\n');
                    LOG.fine(sb.toString());
                }
                ++newJoinTokenCount;
            }
            if (loggable) {
                LOG.fine("JoinTokenList created for " + this.tokenListList.languagePath() + " with " + newJoinTokenCount + " tokens\n");
            }
            while (++tokenListIndex < this.tokenListCount) {
                tokenList = this.initTokenList(tokenListIndex, newJoinTokenCount);
            }
            for (int i = this.tokenListCount - 1; i >= 0; --i) {
                EmbeddedTokenList etl = (EmbeddedTokenList)this.tokenListList.get(i);
                etl.trimStorageToSize();
                assert (etl.joinInfo() != null);
            }
        }
        this.indexGapsIndex = tokenListIndex;
        this.joinTokenCount = newJoinTokenCount;
    }

    private EmbeddedTokenList<?, T> initTokenList(int tokenListIndex, int joinTokenCount) {
        EmbeddedTokenList tokenList = this.tokenList(tokenListIndex);
        if (tokenList.tokenCountCurrent() > 0) {
            tokenList.clear();
        }
        assert (tokenList.joinInfo() == null);
        tokenList.setJoinInfo(new EmbeddedJoinInfo(this, joinTokenCount, tokenListIndex));
        return tokenList;
    }

    public String checkConsistency() {
        String error = LexerUtilsConstants.checkConsistencyTokenList(this, false);
        if (error == null) {
            int realJoinTokenCount = 0;
            Token activeJoinToken = null;
            int joinedPartCount = 0;
            int nextCheckPartIndex = 0;
            for (int tokenListIndex = 0; tokenListIndex < this.tokenListCount(); ++tokenListIndex) {
                int etlTokenCount;
                EmbeddedTokenList etl = this.tokenList(tokenListIndex);
                error = LexerUtilsConstants.checkConsistencyTokenList(etl, false);
                if (error != null) {
                    return error;
                }
                EmbeddedJoinInfo<T> joinInfo = etl.joinInfo();
                if (joinInfo == null) {
                    return "Null joinInfo for ETL at token-list-index " + tokenListIndex;
                }
                if (realJoinTokenCount != joinInfo.joinTokenIndex()) {
                    return "joinTokenIndex=" + realJoinTokenCount + " != etl.joinInfo.joinTokenIndex()=" + joinInfo.joinTokenIndex() + " at token-list-index " + tokenListIndex;
                }
                if (tokenListIndex != joinInfo.tokenListIndex()) {
                    return "token-list-index=" + tokenListIndex + " != etl.joinInfo.tokenListIndex()=" + joinInfo.tokenListIndex();
                }
                int etlJoinTokenCount = etlTokenCount = etl.tokenCount();
                if (etlTokenCount > 0) {
                    AbstractToken<T> token = etl.tokenOrEmbeddingDirect(0).token();
                    int startCheckIndex = 0;
                    if (activeJoinToken != null) {
                        if (token.getClass() != PartToken.class) {
                            return "Unfinished joinToken at token-list-index=" + tokenListIndex;
                        }
                        if ((error = this.checkConsistencyJoinToken((JoinToken<T>)activeJoinToken, token, nextCheckPartIndex++, tokenListIndex)) != null) {
                            return error;
                        }
                        if (nextCheckPartIndex == joinedPartCount) {
                            activeJoinToken = null;
                        } else {
                            if (etlTokenCount > 1) {
                                return "More than one token and non-last part of unfinished join token at token-list-index " + tokenListIndex;
                            }
                            --etlJoinTokenCount;
                        }
                        startCheckIndex = 1;
                    }
                    if (etlTokenCount > startCheckIndex) {
                        assert (activeJoinToken == null);
                        token = etl.tokenOrEmbeddingDirect(etlTokenCount - 1).token();
                        if (token.getClass() == PartToken.class) {
                            --etlJoinTokenCount;
                            activeJoinToken = ((PartToken)token).joinToken();
                            joinedPartCount = activeJoinToken.joinedParts().size();
                            nextCheckPartIndex = 0;
                            if (joinedPartCount < 2) {
                                return "joinedPartCount=" + joinedPartCount + " < 2";
                            }
                            if ((error = this.checkConsistencyJoinToken((JoinToken<T>)activeJoinToken, token, nextCheckPartIndex++, tokenListIndex)) != null) {
                                return error;
                            }
                        }
                    }
                    for (int j = startCheckIndex; j < etlJoinTokenCount; ++j) {
                        if (etl.tokenOrEmbeddingDirect(j).token().getClass() != PartToken.class) continue;
                        return "Inside PartToken at index " + j + "; joinTokenCount=" + etlJoinTokenCount;
                    }
                }
                if (etlJoinTokenCount != etl.joinTokenCount()) {
                    return "joinTokenCount=" + etlJoinTokenCount + " != etl.joinTokenCount()=" + etl.joinTokenCount() + " at token-list-index " + tokenListIndex;
                }
                realJoinTokenCount += etlJoinTokenCount;
            }
            if (activeJoinToken != null) {
                return "Unfinished join token at end";
            }
            if (realJoinTokenCount != this.joinTokenCount) {
                return "realJoinTokenCount=" + realJoinTokenCount + " != joinTokenCount=" + this.joinTokenCount;
            }
        }
        return error;
    }

    private String checkConsistencyJoinToken(JoinToken<T> joinToken, AbstractToken<T> token, int partIndex, int tokenListIndex) {
        PartToken partToken = (PartToken)token;
        if (joinToken.joinedParts().get(partIndex) != token) {
            return "activeJoinToken.joinedParts().get(" + partIndex + ") != token at token-list-index " + tokenListIndex;
        }
        if (partToken.joinToken() != joinToken) {
            return "Invalid join token of part at partIndex " + partIndex + " at token-list-index " + tokenListIndex;
        }
        EmbeddedTokenList etl = this.tokenList(tokenListIndex);
        int lps = etl.joinInfo().joinTokenLastPartShift();
        if (lps < 0) {
            return "lps=" + lps + " < 0";
        }
        if (tokenListIndex + lps >= this.tokenListCount()) {
            return "Invalid lps=" + lps + " at token-list-index " + tokenListIndex + "; tokenListCount=" + this.tokenListCount();
        }
        AbstractToken<T> lastPart = this.tokenList(tokenListIndex + lps).tokenOrEmbeddingDirect(0).token();
        if (lastPart.getClass() != PartToken.class) {
            return "Invalid lps: lastPart not PartToken " + lastPart.dumpInfo(null, null, true, true, 0) + " at token-list-index " + tokenListIndex;
        }
        if (((PartToken)lastPart).joinToken().lastPart() != lastPart) {
            return "Invalid lps: Not last part " + lastPart.dumpInfo(null, null, true, true, 0) + " at token-list-index " + tokenListIndex;
        }
        return null;
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        sb.append("joinTokenCount=").append(this.joinTokenCount).append(", activeTokenListIndex=").append(this.activeTokenListIndex).append(", JI<").append(this.activeStartJoinIndex).append(",").append(this.activeEndJoinIndex).append(">\n");
        sb.append("gapIndex=").append(this.indexGapsIndex).append(", joinGapLen=").append(this.joinTokenIndexGapLength).append(", tokenListGapLen=").append(this.tokenListIndexGapLength).append("\n");
        int digitCount = String.valueOf(this.tokenListCount - 1).length();
        for (int i = 0; i < this.tokenListCount; ++i) {
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            this.tokenList(i).dumpInfo(sb);
            sb.append('\n');
        }
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "JTL";
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(512);
        this.dumpInfo(sb);
        return LexerUtilsConstants.appendTokenList(sb, this).toString();
    }
}

