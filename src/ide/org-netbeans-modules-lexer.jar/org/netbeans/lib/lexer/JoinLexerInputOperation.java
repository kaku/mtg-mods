/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer;

import java.util.List;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.WrapTokenIdCache;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.lib.lexer.token.PartToken;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public class JoinLexerInputOperation<T extends TokenId>
extends LexerInputOperation<T> {
    CharSequence inputSourceText;
    private JoinLexerInputOperation<T> readText;
    private JoinLexerInputOperation<T> readExistingText;
    private EmbeddedTokenList<?, T> activeTokenList;
    private int activeTokenListIndex;
    private int activeTokenListEndOffset;
    private int realTokenStartOffset;
    private boolean recognizedTokenJoined;
    private int skipTokenListCount;

    public JoinLexerInputOperation(JoinTokenList<T> joinTokenList, int relexJoinIndex, Object lexerRestartState, int activeTokenListIndex, int relexOffset) {
        super(joinTokenList, relexJoinIndex, lexerRestartState);
        this.inputSourceText = joinTokenList.inputSourceText();
        this.activeTokenListIndex = activeTokenListIndex;
        this.tokenStartOffset = relexOffset;
        this.readOffset = relexOffset;
    }

    public final void init() {
        this.fetchActiveTokenList();
        this.readText = new TokenListText(this, this.activeTokenListIndex);
        this.realTokenStartOffset = this.readOffset;
    }

    public EmbeddedTokenList<?, T> activeTokenList() {
        return this.activeTokenList;
    }

    public int activeTokenListIndex() {
        return this.activeTokenListIndex;
    }

    public int skipTokenListCount() {
        return this.skipTokenListCount;
    }

    public void clearSkipTokenListCount() {
        this.skipTokenListCount = 0;
    }

    public boolean recognizedTokenLastInTokenList() {
        return this.realTokenStartOffset == this.activeTokenListEndOffset;
    }

    @Override
    public int lastTokenEndOffset() {
        return this.realTokenStartOffset;
    }

    @Override
    public int read(int offset) {
        return this.readText.read(offset);
    }

    @Override
    public char readExisting(int offset) {
        if (this.readText.isInBounds(offset)) {
            return this.readText.inBoundsChar(offset);
        }
        if (this.readExistingText == null) {
            this.readExistingText = new TokenListText(this, this.readText);
        }
        return this.readExistingText.existingChar(offset);
    }

    @Override
    public void assignTokenLength(int tokenLength) {
        LexerInputOperation.super.assignTokenLength(tokenLength);
        if (this.recognizedTokenLastInTokenList()) {
            do {
                ++this.skipTokenListCount;
                ++this.activeTokenListIndex;
                this.fetchActiveTokenList();
            } while (this.realTokenStartOffset == this.activeTokenListEndOffset);
        }
        this.realTokenStartOffset += tokenLength;
        this.recognizedTokenJoined = this.realTokenStartOffset > this.activeTokenListEndOffset;
    }

    private void fetchActiveTokenList() {
        this.activeTokenList = this.tokenList(this.activeTokenListIndex);
        this.activeTokenList.updateModCount();
        this.realTokenStartOffset = this.activeTokenList.startOffset();
        this.activeTokenListEndOffset = this.activeTokenList.endOffset();
    }

    public EmbeddedTokenList<?, T> tokenList(int tokenListIndex) {
        return ((JoinTokenList)this.tokenList).tokenList(tokenListIndex);
    }

    protected int tokenListCount() {
        return ((JoinTokenList)this.tokenList).tokenListCount();
    }

    @Override
    protected void fillTokenData(AbstractToken<T> token) {
        if (!this.recognizedTokenJoined) {
            token.setRawOffset(this.realTokenStartOffset - this.tokenLength);
        }
    }

    @Override
    protected boolean isFlyTokenAllowed() {
        return LexerInputOperation.super.isFlyTokenAllowed() && !this.recognizedTokenJoined;
    }

    @Override
    protected AbstractToken<T> createDefaultTokenInstance(T id) {
        if (this.recognizedTokenJoined) {
            return this.createJoinToken(id, null, PartType.COMPLETE);
        }
        return LexerInputOperation.super.createDefaultTokenInstance(id);
    }

    @Override
    protected AbstractToken<T> createPropertyTokenInstance(T id, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        if (this.recognizedTokenJoined) {
            return this.createJoinToken(id, propertyProvider, partType);
        }
        return LexerInputOperation.super.createPropertyTokenInstance(id, propertyProvider, partType);
    }

    private AbstractToken<T> createJoinToken(T id, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        this.realTokenStartOffset -= this.tokenLength;
        WrapTokenId<T> wid = this.wrapTokenIdCache.plainWid(id);
        JoinToken<T> joinToken = new JoinToken<T>(wid, this.tokenLength, propertyProvider, partType);
        int joinPartCountEstimate = this.readText.tokenListIndex - this.activeTokenListIndex + 1;
        Object[] parts = new PartToken[joinPartCountEstimate];
        int partLength = this.activeTokenListEndOffset - this.realTokenStartOffset;
        PartToken<T> partToken = new PartToken<T>(wid, partLength, propertyProvider, PartType.START, joinToken, 0, 0);
        partToken.setRawOffset(this.realTokenStartOffset);
        parts[0] = partToken;
        int partIndex = 1;
        int partTextOffset = partLength;
        int firstPartTokenListIndex = this.activeTokenListIndex;
        do {
            PartType partPartType;
            ++this.activeTokenListIndex;
            this.fetchActiveTokenList();
            partLength = this.activeTokenListEndOffset - this.realTokenStartOffset;
            if (partLength == 0) continue;
            if (partTextOffset + partLength >= this.tokenLength) {
                partLength = this.tokenLength - partTextOffset;
                partPartType = partType == PartType.START ? PartType.MIDDLE : PartType.END;
            } else {
                partPartType = PartType.MIDDLE;
            }
            partToken = new PartToken<T>(wid, partLength, propertyProvider, partPartType, joinToken, partIndex, partTextOffset);
            partToken.setRawOffset(this.realTokenStartOffset);
            partTextOffset += partLength;
            parts[partIndex++] = partToken;
        } while (partTextOffset < this.tokenLength);
        this.realTokenStartOffset += partLength;
        if (partIndex < parts.length) {
            PartToken[] tmp = new PartToken[partIndex];
            System.arraycopy(parts, 0, tmp, 0, partIndex);
            parts = tmp;
        }
        List partList = ArrayUtilities.unmodifiableList((Object[])parts);
        joinToken.setJoinedParts(partList, this.activeTokenListIndex - firstPartTokenListIndex);
        return joinToken;
    }

    @Override
    public String toString() {
        return LexerInputOperation.super.toString() + ", realTokenStartOffset=" + this.realTokenStartOffset + ", activeTokenListIndex=" + this.activeTokenListIndex + ", activeTokenListEndOffset=" + this.activeTokenListEndOffset;
    }

    final class TokenListText {
        int tokenListIndex;
        int tokenListStartOffset;
        int tokenListEndOffset;
        int readOffsetShift;
        final /* synthetic */ JoinLexerInputOperation this$0;

        TokenListText(JoinLexerInputOperation joinLexerInputOperation, int tokenListIndex) {
            this.this$0 = joinLexerInputOperation;
            this.tokenListIndex = tokenListIndex;
            EmbeddedTokenList etl = joinLexerInputOperation.tokenList(tokenListIndex);
            etl.updateModCount();
            this.tokenListStartOffset = etl.startOffset();
            this.tokenListEndOffset = etl.endOffset();
            this.readOffsetShift = 0;
        }

        TokenListText(JoinLexerInputOperation<T> var1_1) {
            this.this$0 = var1_1;
            this.tokenListIndex = text.tokenListIndex;
            this.tokenListStartOffset = text.tokenListStartOffset;
            this.tokenListEndOffset = text.tokenListEndOffset;
            this.readOffsetShift = text.readOffsetShift;
        }

        int read(int offset) {
            if ((offset += this.readOffsetShift) < this.tokenListEndOffset) {
                if (offset >= this.tokenListStartOffset) {
                    return this.this$0.inputSourceText.charAt(offset);
                }
                while ((offset -= this.movePreviousTokenList()) < this.tokenListStartOffset) {
                }
                return this.this$0.inputSourceText.charAt(offset);
            }
            while (this.tokenListIndex + 1 < this.this$0.tokenListCount()) {
                if ((offset += this.moveNextTokenList()) >= this.tokenListEndOffset) continue;
                return this.this$0.inputSourceText.charAt(offset);
            }
            return -1;
        }

        boolean isInBounds(int offset) {
            return (offset += this.readOffsetShift) >= this.tokenListStartOffset && offset < this.tokenListEndOffset;
        }

        char inBoundsChar(int offset) {
            return this.this$0.inputSourceText.charAt(offset += this.readOffsetShift);
        }

        char existingChar(int offset) {
            if ((offset += this.readOffsetShift) < this.tokenListStartOffset) {
                while ((offset -= this.movePreviousTokenList()) < this.tokenListStartOffset) {
                }
                return this.this$0.inputSourceText.charAt(offset);
            }
            if (offset >= this.tokenListEndOffset) {
                while ((offset += this.moveNextTokenList()) >= this.tokenListEndOffset) {
                }
                return this.this$0.inputSourceText.charAt(offset);
            }
            return this.this$0.inputSourceText.charAt(offset);
        }

        private int movePreviousTokenList() {
            --this.tokenListIndex;
            EmbeddedTokenList etl = this.this$0.tokenList(this.tokenListIndex);
            etl.updateModCount();
            this.tokenListEndOffset = etl.endOffset();
            int shift = this.tokenListStartOffset - this.tokenListEndOffset;
            this.readOffsetShift -= shift;
            this.tokenListStartOffset = etl.startOffset();
            return shift;
        }

        private int moveNextTokenList() {
            ++this.tokenListIndex;
            EmbeddedTokenList etl = this.this$0.tokenList(this.tokenListIndex);
            etl.updateModCount();
            this.tokenListStartOffset = etl.startOffset();
            int shift = this.tokenListStartOffset - this.tokenListEndOffset;
            this.readOffsetShift += shift;
            this.tokenListEndOffset = etl.endOffset();
            return shift;
        }

        public String toString() {
            return "tlInd=" + this.tokenListIndex + ", <" + this.tokenListStartOffset + "," + this.tokenListEndOffset + ">";
        }
    }

}

