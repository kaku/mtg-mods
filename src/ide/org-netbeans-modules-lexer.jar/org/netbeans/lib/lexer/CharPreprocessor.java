/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.lib.lexer.CharPreprocessorOperation;
import org.netbeans.lib.lexer.UnicodeEscapesPreprocessor;

public abstract class CharPreprocessor {
    private CharPreprocessorOperation operation;

    public static CharPreprocessor createUnicodeEscapesPreprocessor() {
        return new UnicodeEscapesPreprocessor();
    }

    protected abstract void preprocessChar();

    protected abstract boolean isSensitiveChar(char var1);

    protected abstract int maxLookahead();

    protected final int inputRead() {
        return this.operation.inputRead();
    }

    protected final void inputBackup(int count) {
        this.operation.inputBackup(count);
    }

    protected final void outputOriginal(int ch) {
        this.operation.outputOriginal(ch);
    }

    protected final void outputPreprocessed(char ch, int extraInputLength) {
        assert (extraInputLength > 0);
        this.operation.outputPreprocessed(ch, extraInputLength);
    }

    protected final void notifyError(String errorMessage) {
        this.operation.notifyError(errorMessage);
    }

    void init(CharPreprocessorOperation operation) {
        this.operation = operation;
    }
}

