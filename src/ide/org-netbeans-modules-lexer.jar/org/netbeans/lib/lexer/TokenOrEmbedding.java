/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.token.AbstractToken;

public interface TokenOrEmbedding<T extends TokenId> {
    public AbstractToken<T> token();

    public EmbeddedTokenList<T, ?> embedding();
}

