/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LAState;
import org.netbeans.lib.lexer.LexerInputOperation;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TextLexerInputOperation;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.TextToken;

public final class BatchTokenList<T extends TokenId>
extends ArrayList<TokenOrEmbedding<T>>
implements TokenList<T> {
    private static boolean maintainLAState;
    private final TokenHierarchyOperation<?, T> tokenHierarchyOperation;
    private final CharSequence inputSourceText;
    private final Language<T> language;
    private final LanguagePath languagePath;
    private final Set<T> skipTokenIds;
    private final InputAttributes inputAttributes;
    private LexerInputOperation<T> lexerInputOperation;
    private LAState laState;

    public static boolean isMaintainLAState() {
        return maintainLAState;
    }

    public static void setMaintainLAState(boolean maintainLAState) {
        BatchTokenList.maintainLAState = maintainLAState;
    }

    public BatchTokenList(TokenHierarchyOperation<?, T> tokenHierarchyOperation, CharSequence inputText, Language<T> language, Set<T> skipTokenIds, InputAttributes inputAttributes) {
        this.tokenHierarchyOperation = tokenHierarchyOperation;
        this.inputSourceText = inputText;
        this.language = language;
        this.languagePath = LanguagePath.get(language);
        this.skipTokenIds = skipTokenIds;
        this.inputAttributes = inputAttributes;
        if (TokenList.LOG.isLoggable(Level.FINE)) {
            this.laState = LAState.empty();
        }
        this.lexerInputOperation = this.createLexerInputOperation();
    }

    protected LexerInputOperation<T> createLexerInputOperation() {
        return new TextLexerInputOperation(this);
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this;
    }

    @Override
    public CharSequence inputSourceText() {
        return this.inputSourceText;
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.tokenHierarchyOperation;
    }

    @Override
    public Language<T> language() {
        return this.language;
    }

    @Override
    public LanguagePath languagePath() {
        return this.languagePath;
    }

    @Override
    public int tokenCount() {
        if (this.lexerInputOperation != null) {
            this.tokenOrEmbeddingImpl(Integer.MAX_VALUE);
        }
        return this.size();
    }

    @Override
    public int tokenCountCurrent() {
        return this.size();
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        int rawOffset = token.rawOffset();
        return rawOffset;
    }

    @Override
    public int tokenOffset(int index) {
        int offset;
        AbstractToken<T> token = this.existingToken(index);
        if (token.isFlyweight()) {
            offset = 0;
            while (--index >= 0) {
                token = this.existingToken(index);
                offset += token.length();
                if (token.isFlyweight()) continue;
                offset += token.offset(null);
                break;
            }
        } else {
            offset = token.offset(null);
        }
        return offset;
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexLazyTokenCreation(this, offset);
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        return this.tokenOrEmbeddingImpl(index);
    }

    private TokenOrEmbedding<T> tokenOrEmbeddingImpl(int index) {
        while (this.lexerInputOperation != null && index >= this.size()) {
            AbstractToken<T> token = this.lexerInputOperation.nextToken();
            if (token != null) {
                if (!token.isFlyweight()) {
                    token.setTokenList(this);
                }
                this.add(token);
                if (this.laState == null) continue;
                this.laState = this.laState.add(this.lexerInputOperation.lookahead(), this.lexerInputOperation.lexerState());
                continue;
            }
            this.lexerInputOperation.release();
            this.lexerInputOperation = null;
            this.trimToSize();
        }
        return index < this.size() ? (TokenOrEmbedding)this.get(index) : null;
    }

    private AbstractToken<T> existingToken(int index) {
        return ((TokenOrEmbedding)this.get(index)).token();
    }

    @Override
    public int lookahead(int index) {
        return this.laState != null ? this.laState.lookahead(index) : -1;
    }

    @Override
    public Object state(int index) {
        return this.laState != null ? this.laState.state(index) : null;
    }

    @Override
    public int startOffset() {
        return 0;
    }

    @Override
    public int endOffset() {
        int cntM1 = this.tokenCount() - 1;
        if (cntM1 >= 0) {
            return this.tokenOffset(cntM1) + this.tokenOrEmbeddingImpl(cntM1).token().length();
        }
        return 0;
    }

    @Override
    public boolean isRemoved() {
        return false;
    }

    @Override
    public int modCount() {
        return -1;
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        TextToken nonFlyToken = ((TextToken)flyToken).createCopy(this, offset);
        this.set(index, nonFlyToken);
        return nonFlyToken;
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        this.set(index, t);
    }

    @Override
    public InputAttributes inputAttributes() {
        return this.inputAttributes;
    }

    @Override
    public boolean isContinuous() {
        return this.skipTokenIds == null;
    }

    @Override
    public Set<T> skipTokenIds() {
        return this.skipTokenIds;
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "BTL";
    }
}

