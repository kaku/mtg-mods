/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.Set;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.token.AbstractToken;

public final class SubSequenceTokenList<T extends TokenId>
implements TokenList<T> {
    private TokenList<T> tokenList;
    private final int limitStartOffset;
    private final int limitEndOffset;
    private int limitStartIndex;
    private int limitEndIndex;

    public static <T extends TokenId> SubSequenceTokenList<T> create(TokenList<T> tokenList, int limitStartOffset, int limitEndOffset) {
        return new SubSequenceTokenList<T>(tokenList, limitStartOffset, limitEndOffset);
    }

    public SubSequenceTokenList(TokenList<T> tokenList, int limitStartOffset, int limitEndOffset) {
        int[] indexAndTokenOffset;
        this.tokenList = tokenList;
        this.limitStartOffset = limitStartOffset;
        this.limitEndOffset = limitEndOffset;
        if (limitEndOffset == Integer.MAX_VALUE) {
            this.limitEndIndex = tokenList.tokenCount();
        } else {
            indexAndTokenOffset = tokenList.tokenIndex(limitEndOffset);
            this.limitEndIndex = indexAndTokenOffset[0];
            if (this.limitEndIndex != -1) {
                if (limitEndOffset > indexAndTokenOffset[1] && this.limitEndIndex < tokenList.tokenCountCurrent()) {
                    ++this.limitEndIndex;
                }
            } else {
                this.limitEndIndex = 0;
            }
        }
        if (this.limitEndIndex > 0 && limitStartOffset > 0) {
            indexAndTokenOffset = tokenList.tokenIndex(limitStartOffset);
            this.limitStartIndex = indexAndTokenOffset[0];
            if (this.limitStartIndex < tokenList.tokenCountCurrent() && indexAndTokenOffset[1] + tokenList.tokenOrEmbedding(this.limitStartIndex).token().length() <= limitStartOffset) {
                ++this.limitStartIndex;
            }
        }
    }

    public TokenList<T> delegate() {
        return this.tokenList;
    }

    public int limitStartOffset() {
        return this.limitStartOffset;
    }

    public int limitEndOffset() {
        return this.limitEndOffset;
    }

    @Override
    public TokenOrEmbedding<T> tokenOrEmbedding(int index) {
        return (index += this.limitStartIndex) < this.limitEndIndex ? this.tokenList.tokenOrEmbedding(index) : null;
    }

    @Override
    public int tokenOffset(int index) {
        if ((index += this.limitStartIndex) >= this.limitEndIndex) {
            throw new IndexOutOfBoundsException("index=" + index + " >= limitEndIndex=" + this.limitEndIndex);
        }
        return this.tokenList.tokenOffset(index);
    }

    @Override
    public int[] tokenIndex(int offset) {
        return LexerUtilsConstants.tokenIndexBinSearch(this, offset, this.tokenCountCurrent());
    }

    @Override
    public int tokenCount() {
        return this.tokenCountCurrent();
    }

    @Override
    public int tokenCountCurrent() {
        return this.limitEndIndex - this.limitStartIndex;
    }

    @Override
    public AbstractToken<T> replaceFlyToken(int index, AbstractToken<T> flyToken, int offset) {
        return this.tokenList.replaceFlyToken(index + this.limitStartIndex, flyToken, offset);
    }

    @Override
    public int modCount() {
        return this.tokenList.modCount();
    }

    @Override
    public Language<T> language() {
        return this.tokenList.language();
    }

    @Override
    public LanguagePath languagePath() {
        return this.tokenList.languagePath();
    }

    @Override
    public int tokenOffset(AbstractToken<T> token) {
        return this.tokenList.tokenOffset(token);
    }

    @Override
    public void setTokenOrEmbedding(int index, TokenOrEmbedding<T> t) {
        this.tokenList.setTokenOrEmbedding(this.limitStartIndex + index, t);
    }

    @Override
    public TokenList<?> rootTokenList() {
        return this.tokenList.rootTokenList();
    }

    @Override
    public CharSequence inputSourceText() {
        return this.rootTokenList().inputSourceText();
    }

    @Override
    public TokenHierarchyOperation<?, ?> tokenHierarchyOperation() {
        return this.tokenList.tokenHierarchyOperation();
    }

    @Override
    public InputAttributes inputAttributes() {
        return this.tokenList.inputAttributes();
    }

    @Override
    public int lookahead(int index) {
        return this.tokenList.lookahead(index);
    }

    @Override
    public Object state(int index) {
        return this.tokenList.state(index);
    }

    @Override
    public boolean isContinuous() {
        return this.tokenList.isContinuous();
    }

    @Override
    public Set<T> skipTokenIds() {
        return this.tokenList.skipTokenIds();
    }

    @Override
    public int startOffset() {
        if (this.tokenCountCurrent() > 0 || this.tokenCount() > 0) {
            return this.tokenOffset(0);
        }
        return this.limitStartOffset;
    }

    @Override
    public int endOffset() {
        int cntM1 = this.tokenCount() - 1;
        if (cntM1 >= 0) {
            return this.tokenOffset(cntM1) + this.tokenList.tokenOrEmbedding(cntM1).token().length();
        }
        return this.limitStartOffset;
    }

    @Override
    public boolean isRemoved() {
        return this.tokenList.isRemoved();
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb) {
        return sb;
    }

    @Override
    public String dumpInfoType() {
        return "SubSeqTL";
    }
}

