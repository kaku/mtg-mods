/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.SubSequenceTokenList;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;

public final class TokenSequenceList
extends AbstractList<TokenSequence<?>> {
    private TokenList<?> rootTokenList;
    private final TokenListList<?> tokenListList;
    private final List<TokenSequence<?>> tokenSequences;
    private final int endOffset;
    private final int rootModCount;
    private int tokenListIndex;

    public TokenSequenceList(TokenList<?> rootTokenList, LanguagePath languagePath, int startOffset, int endOffset) {
        this.rootTokenList = rootTokenList;
        this.endOffset = endOffset;
        this.rootModCount = rootTokenList.modCount();
        if (languagePath.size() == 1) {
            this.tokenListList = null;
            this.tokenListIndex = Integer.MAX_VALUE;
            if (rootTokenList.languagePath() == languagePath) {
                TokenList tl = this.checkWrapTokenList(rootTokenList, startOffset, endOffset);
                TokenSequence rootTS = LexerApiPackageAccessor.get().createTokenSequence(tl);
                this.tokenSequences = Collections.singletonList(rootTS);
            } else {
                this.tokenSequences = Collections.emptyList();
            }
        } else if (rootTokenList.language() != languagePath.topLanguage()) {
            this.tokenListList = null;
            this.tokenListIndex = Integer.MAX_VALUE;
            this.tokenSequences = Collections.emptyList();
        } else {
            EmbeddedTokenList firstTokenList;
            this.tokenListList = rootTokenList.tokenHierarchyOperation().tokenListList(languagePath);
            int size = this.tokenListList.size();
            int high = size - 1;
            if (startOffset > 0) {
                while (this.tokenListIndex <= high) {
                    int mid = (this.tokenListIndex + high) / 2;
                    EmbeddedTokenList etl = (EmbeddedTokenList)this.tokenListList.get(mid);
                    etl.updateModCount(this.rootModCount);
                    int tlEndOffset = etl.endOffset();
                    if (tlEndOffset < startOffset) {
                        this.tokenListIndex = mid + 1;
                        continue;
                    }
                    if (tlEndOffset > startOffset) {
                        high = mid - 1;
                        continue;
                    }
                    this.tokenListIndex = mid + 1;
                    break;
                }
                firstTokenList = this.tokenListList.getOrNull(this.tokenListIndex);
                if (this.tokenListIndex == size) {
                    while (firstTokenList != null) {
                        firstTokenList.updateModCount(this.rootModCount);
                        if (firstTokenList.endOffset() < startOffset) {
                            firstTokenList = this.tokenListList.getOrNull(++this.tokenListIndex);
                            continue;
                        }
                        break;
                    }
                }
            } else {
                firstTokenList = this.tokenListList.getOrNull(0);
            }
            if (firstTokenList != null) {
                firstTokenList.updateModCount(this.rootModCount);
                this.tokenSequences = new ArrayList(4);
                TokenSequence ts = LexerApiPackageAccessor.get().createTokenSequence(this.checkWrapTokenList(firstTokenList, startOffset, endOffset));
                this.tokenSequences.add(ts);
            } else {
                this.tokenSequences = Collections.emptyList();
                this.tokenListIndex = Integer.MAX_VALUE;
            }
        }
    }

    private TokenList<?> checkWrapTokenList(TokenList<?> tokenList, int startOffset, int endOffset) {
        boolean wrapEnd;
        boolean wrapStart = startOffset > 0 && tokenList.startOffset() < startOffset && startOffset < tokenList.endOffset();
        boolean bl = wrapEnd = endOffset != Integer.MAX_VALUE && tokenList.startOffset() < endOffset && endOffset < tokenList.endOffset();
        if (wrapStart || wrapEnd) {
            tokenList = SubSequenceTokenList.create(tokenList, startOffset, endOffset);
        }
        if (wrapEnd) {
            this.tokenListIndex = Integer.MAX_VALUE;
        }
        return tokenList;
    }

    @Override
    public Iterator<TokenSequence<?>> iterator() {
        return new Itr();
    }

    @Override
    public TokenSequence<?> get(int index) {
        this.findTokenSequenceWithIndex(index);
        return this.tokenSequences.get(index);
    }

    public TokenSequence<?> getOrNull(int index) {
        this.findTokenSequenceWithIndex(index);
        return index < this.tokenSequences.size() ? this.tokenSequences.get(index) : null;
    }

    @Override
    public int size() {
        this.findTokenSequenceWithIndex(Integer.MAX_VALUE);
        return this.tokenSequences.size();
    }

    private void findTokenSequenceWithIndex(int index) {
        while (index >= this.tokenSequences.size() && this.tokenListIndex != Integer.MAX_VALUE) {
            EmbeddedTokenList etl;
            if ((etl = this.tokenListList.getOrNull(++this.tokenListIndex)) != null) {
                etl.updateModCount();
                if (this.endOffset == Integer.MAX_VALUE || etl.startOffset() < this.endOffset) {
                    boolean wrapEnd;
                    boolean bl = wrapEnd = this.endOffset != Integer.MAX_VALUE && etl.startOffset() < this.endOffset && this.endOffset < etl.endOffset();
                    if (wrapEnd) {
                        this.tokenSequences.add(LexerApiPackageAccessor.get().createTokenSequence(SubSequenceTokenList.create(etl, 0, this.endOffset)));
                        this.tokenListIndex = Integer.MAX_VALUE;
                        continue;
                    }
                    this.tokenSequences.add(LexerApiPackageAccessor.get().createTokenSequence(etl));
                    continue;
                }
                this.tokenListIndex = Integer.MAX_VALUE;
                continue;
            }
            this.tokenListIndex = Integer.MAX_VALUE;
        }
    }

    void checkForComodification() {
        if (this.rootModCount != this.rootTokenList.modCount()) {
            throw new ConcurrentModificationException("Caller uses obsolete TokenSequenceList: expectedModCount=" + this.rootModCount + " != modCount=" + this.rootTokenList.modCount());
        }
    }

    @Override
    public String toString() {
        return this.tokenListList.toString();
    }

    private class Itr
    implements Iterator<TokenSequence<?>> {
        private int cursor;
        private TokenSequence<?> next;

        private Itr() {
            this.cursor = 0;
        }

        @Override
        public boolean hasNext() {
            this.checkFetchNext();
            return this.next != null;
        }

        @Override
        public TokenSequence<?> next() {
            this.checkFetchNext();
            if (this.next == null) {
                throw new NoSuchElementException();
            }
            TokenSequence ret = this.next;
            this.next = null;
            return ret;
        }

        private void checkFetchNext() {
            if (this.next == null) {
                TokenSequenceList.this.checkForComodification();
                this.next = TokenSequenceList.this.getOrNull(this.cursor++);
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}

