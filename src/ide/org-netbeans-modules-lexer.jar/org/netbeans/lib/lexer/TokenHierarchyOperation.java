/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.BatchTokenList;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.StackElementArray;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenListList;
import org.netbeans.lib.lexer.TokenSequenceList;
import org.netbeans.lib.lexer.WrapTokenIdCache;
import org.netbeans.lib.lexer.inc.IncTokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyUpdate;
import org.netbeans.lib.lexer.inc.TokenListChange;
import org.netbeans.spi.lexer.MutableTextInput;

public final class TokenHierarchyOperation<I, T extends TokenId> {
    static final Logger LOG = Logger.getLogger(TokenHierarchyOperation.class.getName());
    private static final Logger LOG_LOCK = Logger.getLogger(MutableTextInput.class.getName());
    private static final Logger LOG_EVENT = Logger.getLogger(TokenHierarchyEvent.class.getName());
    private static final Set<StackElementArray> missingLockStacks = StackElementArray.createSet();
    private final I inputSource;
    private TokenHierarchy<I> tokenHierarchy;
    private MutableTextInput<I> mutableTextInput;
    private final TokenList<T> rootTokenList;
    private Activity activity;
    private LanguagePath lastActiveLanguagePath;
    private EventListenerList listenerList;
    private Set<LanguagePath> languagePaths;
    private Set<Language<?>> exploredLanguages;
    private Map<LanguagePath, TokenListList<?>> path2tokenListList;
    private Object rootChildrenLanguages;
    private int maxTokenListListPathSize;
    private WrapTokenIdCache<?>[] wrapTokenIdCaches;
    private Language<?> lastQueryLanguage;
    private WrapTokenIdCache<?> lastQueryCache;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public TokenHierarchyOperation(Reader inputReader, Language<T> language, Set<T> skipTokenIds, InputAttributes inputAttributes) {
        super();
        if (inputReader == null) {
            throw new IllegalArgumentException("inputReader cannot be null");
        }
        if (language == null) {
            throw new IllegalArgumentException("language cannot be null");
        }
        input = inputReader;
        this.inputSource = input;
        chars = new char[16384];
        offset = 0;
        while ((readLen = inputReader.read(chars, offset, chars.length - offset)) != -1) {
            if ((offset += readLen) != chars.length) continue;
            chars = ArrayUtilities.charArray((char[])chars);
        }
        try {
            inputReader.close();
        }
        catch (IOException e) {}
        ** GOTO lbl33
        catch (IOException e) {
            try {
                inputReader.close();
            }
            catch (IOException e) {}
            catch (Throwable var9_12) {
                try {
                    inputReader.close();
                    throw var9_12;
                }
                catch (IOException e) {
                    // empty catch block
                }
                throw var9_12;
            }
        }
lbl33: // 4 sources:
        inputText = new String(chars, 0, offset);
        this.rootTokenList = new BatchTokenList<T>(this, inputText, language, skipTokenIds, inputAttributes);
        this.init();
        this.activity = Activity.ACTIVE;
    }

    public TokenHierarchyOperation(CharSequence inputText, boolean copyInputText, Language<T> language, Set<T> skipTokenIds, InputAttributes inputAttributes) {
        if (inputText == null) {
            throw new IllegalArgumentException("inputText cannot be null");
        }
        if (language == null) {
            throw new IllegalArgumentException("language cannot be null");
        }
        CharSequence input = inputText;
        this.inputSource = input;
        if (copyInputText) {
            inputText = inputText.toString();
        }
        this.rootTokenList = new BatchTokenList<T>(this, inputText, language, skipTokenIds, inputAttributes);
        this.init();
        this.activity = Activity.ACTIVE;
    }

    public TokenHierarchyOperation(MutableTextInput<I> mutableTextInput) {
        this.inputSource = LexerSpiPackageAccessor.get().inputSource(mutableTextInput);
        this.mutableTextInput = mutableTextInput;
        this.rootTokenList = new IncTokenList(this);
        this.init();
        this.activity = Activity.NOT_INITED;
    }

    private void init() {
        assert (this.tokenHierarchy == null);
        this.tokenHierarchy = LexerApiPackageAccessor.get().createTokenHierarchy(this);
        this.listenerList = new EventListenerList();
        this.rootChildrenLanguages = null;
    }

    public TokenHierarchy<I> tokenHierarchy() {
        return this.tokenHierarchy;
    }

    public TokenList<T> rootTokenList() {
        return this.rootTokenList;
    }

    public int modCount() {
        return this.rootTokenList.modCount();
    }

    public boolean isMutable() {
        return this.mutableTextInput != null;
    }

    public MutableTextInput mutableTextInput() {
        return this.mutableTextInput;
    }

    public I inputSource() {
        return this.inputSource;
    }

    public CharSequence text() {
        if (this.mutableTextInput != null) {
            return LexerSpiPackageAccessor.get().text(this.mutableTextInput);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setActive(boolean active) {
        this.ensureWriteLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.setActiveImpl(active);
        }
    }

    public void setActiveImpl(boolean active) {
        Activity newActivity;
        assert (this.isMutable());
        Activity activity = newActivity = active ? Activity.ACTIVE : Activity.INACTIVE;
        if (this.activity != newActivity) {
            TokenHierarchyEventInfo eventInfo;
            boolean notInited = this.activity == Activity.NOT_INITED;
            IncTokenList incTokenList = (IncTokenList)this.rootTokenList;
            boolean doFire = !notInited && this.listenerList.getListenerCount() > 0;
            TokenListChange change = null;
            TokenHierarchyEventInfo tokenHierarchyEventInfo = eventInfo = notInited ? null : new TokenHierarchyEventInfo(this, TokenHierarchyEventType.ACTIVITY, 0, 0, "", 0);
            if (!notInited) {
                incTokenList.incrementModCount();
            }
            if (active) {
                if (this.lastActiveLanguagePath != null) {
                    incTokenList.setLanguagePath(this.lastActiveLanguagePath);
                    change = TokenListChange.createRebuildChange(incTokenList);
                    incTokenList.replaceTokens(change, eventInfo, true);
                    CharSequence text = LexerSpiPackageAccessor.get().text(this.mutableTextInput);
                    eventInfo.setMaxAffectedEndOffset(text.length());
                    this.invalidatePath2TokenListList();
                } else {
                    Language language = LexerSpiPackageAccessor.get().language(this.mutableTextInput());
                    if (language != null) {
                        incTokenList.setLanguagePath(language != null ? LanguagePath.get(language) : null);
                        if (!notInited) {
                            change = TokenListChange.createEmptyChange(incTokenList);
                        }
                    } else {
                        return;
                    }
                }
                incTokenList.reinit();
            } else if (!notInited) {
                change = TokenListChange.createEmptyChange(incTokenList);
            }
            if (change != null) {
                eventInfo.setTokenChangeInfo(change.tokenChangeInfo());
            }
            this.activity = newActivity;
            if (doFire) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Firing ACTIVITY change to " + this.listenerList.getListenerCount() + " listeners: " + (Object)((Object)this.activity));
                }
                this.fireTokenHierarchyChanged(eventInfo);
                if (!active) {
                    this.lastActiveLanguagePath = incTokenList.languagePath();
                    incTokenList.setLanguagePath(null);
                }
            }
        }
    }

    private void invalidatePath2TokenListList() {
        this.path2tokenListList = null;
        this.rootChildrenLanguages = null;
        this.maxTokenListListPathSize = 0;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isActive() {
        this.ensureReadLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            return this.isActiveImpl();
        }
    }

    public boolean isActiveImpl() {
        if (this.activity == Activity.NOT_INITED) {
            this.setActiveImpl(true);
        }
        return this.isActiveNoInit();
    }

    public boolean isActiveNoInit() {
        return this.activity == Activity.ACTIVE;
    }

    public void recreateAfterError(RuntimeException e) {
        if (TokenList.LOG.isLoggable(Level.FINE)) {
            throw e;
        }
        LOG.log(Level.INFO, "Runtime exception occurred during token hierarchy updating. Token hierarchy will be rebuilt from scratch.", e);
        if (this.isActiveNoInit()) {
            this.rebuild();
        }
    }

    public void ensureReadLocked() {
        if (this.isMutable() && !LexerSpiPackageAccessor.get().isReadLocked(this.mutableTextInput) && StackElementArray.addStackIfNew(missingLockStacks, 4)) {
            LOG_LOCK.log(Level.INFO, "!!WARNING!! Missing READ-LOCK when accessing TokenHierarchy: input-source:" + LexerSpiPackageAccessor.get().inputSource(this.mutableTextInput), new Exception());
        }
    }

    public void ensureWriteLocked() {
        if (this.isMutable() && !LexerSpiPackageAccessor.get().isWriteLocked(this.mutableTextInput) && StackElementArray.addStackIfNew(missingLockStacks, 4)) {
            LOG_LOCK.log(Level.INFO, "!!WARNING!! Missing WRITE-LOCK when accessing TokenHierarchy: input-source:" + LexerSpiPackageAccessor.get().inputSource(this.mutableTextInput), new Exception());
        }
    }

    public TokenSequence<T> tokenSequence() {
        return this.tokenSequence(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenSequence<T> tokenSequence(Language<?> language) {
        this.ensureReadLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            TokenSequence<T> ts = this.isActiveImpl() && (language == null || this.rootTokenList.language() == language) ? LexerApiPackageAccessor.get().createTokenSequence(this.rootTokenList) : null;
            return ts;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<TokenSequence<?>> tokenSequenceList(LanguagePath languagePath, int startOffset, int endOffset) {
        if (languagePath == null) {
            throw new IllegalArgumentException("languagePath cannot be null");
        }
        this.ensureReadLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            return this.isActiveImpl() ? new TokenSequenceList(this.rootTokenList, languagePath, startOffset, endOffset) : null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<TokenSequence<?>> embeddedTokenSequences(int offset, boolean backwardBias) {
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            TokenSequence seq = this.tokenSequence();
            ArrayList sequences = new ArrayList();
            while (seq != null) {
                seq.move(offset);
                if (seq.moveNext()) {
                    if (seq.offset() == offset && backwardBias) {
                        if (seq.movePrevious()) {
                            sequences.add(seq);
                            seq = seq.embedded();
                            continue;
                        }
                        seq = null;
                        continue;
                    }
                    sequences.add(seq);
                    seq = seq.embedded();
                    continue;
                }
                if (backwardBias && seq.movePrevious()) {
                    sequences.add(seq);
                    seq = seq.embedded();
                    continue;
                }
                seq = null;
            }
            return sequences;
        }
    }

    public <ET extends TokenId> TokenListList<ET> tokenListList(LanguagePath languagePath) {
        assert (this.isActiveNoInit());
        TokenListList tll = this.path2tokenListList().get(languagePath);
        if (tll == null) {
            tll = new TokenListList(this.rootTokenList, languagePath);
            this.path2tokenListList.put(languagePath, tll);
            this.maxTokenListListPathSize = Math.max(languagePath.size(), this.maxTokenListListPathSize);
            Language innerLanguage = languagePath.innerLanguage();
            if (languagePath.size() >= 3) {
                this.tokenListList(languagePath.parent()).notifyChildAdded(innerLanguage);
            } else {
                assert (languagePath.size() == 2);
                assert (languagePath.parent() == this.rootTokenList.languagePath());
                this.rootChildrenLanguages = LexerUtilsConstants.languageOrArrayAdd(this.rootChildrenLanguages, innerLanguage);
            }
        }
        return tll;
    }

    public <ET extends TokenId> TokenListList<ET> existingTokenListList(LanguagePath languagePath) {
        TokenListList tll = this.path2tokenListList != null ? this.path2tokenListList.get(languagePath) : null;
        return tll;
    }

    public Object rootChildrenLanguages() {
        return this.rootChildrenLanguages;
    }

    private Map<LanguagePath, TokenListList<?>> path2tokenListList() {
        if (this.path2tokenListList == null) {
            this.path2tokenListList = new HashMap(4, 0.5f);
        }
        return this.path2tokenListList;
    }

    public int maxTokenListListPathSize() {
        return this.maxTokenListListPathSize;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void rebuild() {
        this.ensureWriteLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            if (this.isActiveNoInit()) {
                IncTokenList incTokenList = (IncTokenList)this.rootTokenList;
                incTokenList.incrementModCount();
                CharSequence text = LexerSpiPackageAccessor.get().text(this.mutableTextInput);
                TokenHierarchyEventInfo eventInfo = new TokenHierarchyEventInfo(this, TokenHierarchyEventType.REBUILD, 0, 0, "", 0);
                TokenListChange change = TokenListChange.createRebuildChange(incTokenList);
                incTokenList.replaceTokens(change, eventInfo, true);
                incTokenList.reinit();
                eventInfo.setTokenChangeInfo(change.tokenChangeInfo());
                eventInfo.setMaxAffectedEndOffset(text.length());
                this.invalidatePath2TokenListList();
                this.fireTokenHierarchyChanged(eventInfo);
            }
        }
    }

    public void fireTokenHierarchyChanged(TokenHierarchyEventInfo eventInfo) {
        TokenHierarchyEvent evt = LexerApiPackageAccessor.get().createTokenChangeEvent(eventInfo);
        Object[] listeners = this.listenerList.getListenerList();
        int listenersLength = listeners.length;
        boolean loggable = LOG_EVENT.isLoggable(Level.FINE);
        long tm = 0;
        if (loggable) {
            LOG_EVENT.fine("Firing " + evt + " to " + listenersLength / 2 + " listeners:\n");
        }
        for (int i = 1; i < listenersLength; i += 2) {
            if (loggable) {
                tm = System.currentTimeMillis();
            }
            ((TokenHierarchyListener)listeners[i]).tokenHierarchyChanged(evt);
            if (!loggable) continue;
            LOG_EVENT.fine(String.valueOf(System.currentTimeMillis() - tm) + "ms: Fired to " + listeners[i] + "\n");
        }
        if (loggable) {
            LOG_EVENT.fine("----- Finished firing of " + evt + "\n");
        }
    }

    public void addTokenHierarchyListener(TokenHierarchyListener listener) {
        this.listenerList.add(TokenHierarchyListener.class, listener);
    }

    public void removeTokenHierarchyListener(TokenHierarchyListener listener) {
        this.listenerList.remove(TokenHierarchyListener.class, listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void textModified(int offset, int removedLength, CharSequence removedText, int insertedLength) {
        this.ensureWriteLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            boolean active = this.isActiveNoInit();
            if (!active && this.listenerList.getListenerCount() > 0) {
                active = this.isActive();
            }
            if (active) {
                TokenHierarchyEventInfo eventInfo = new TokenHierarchyEventInfo(this, TokenHierarchyEventType.MODIFICATION, offset, removedLength, removedText, insertedLength);
                new TokenHierarchyUpdate(eventInfo).update();
                this.fireTokenHierarchyChanged(eventInfo);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<LanguagePath> languagePaths() {
        Set lps;
        this.ensureReadLocked();
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            lps = this.languagePaths;
            if (lps == null) {
                Set cel;
                Set clps;
                if (!this.isActiveImpl()) {
                    return Collections.emptySet();
                }
                Language<T> lang = this.rootTokenList.language();
                LanguageOperation<T> langOp = LexerApiPackageAccessor.get().languageOperation(lang);
                lps = clps = (Set)((HashSet)langOp.languagePaths()).clone();
                this.exploredLanguages = cel = (Set)((HashSet)langOp.exploredLanguages()).clone();
                this.languagePaths = lps;
            }
        }
        return lps;
    }

    public void addLanguagePath(LanguagePath lp) {
        Set<LanguagePath> elps = this.languagePaths();
        if (!elps.contains(lp)) {
            HashSet<LanguagePath> lps = new HashSet<LanguagePath>();
            LanguageOperation.findLanguagePaths(elps, lps, this.exploredLanguages, lp);
            elps.addAll(lps);
        }
    }

    public <T extends TokenId> WrapTokenIdCache<T> getWrapTokenIdCache(Language<T> language) {
        WrapTokenIdCache cache;
        if (language == this.lastQueryLanguage) {
            WrapTokenIdCache cache2 = this.lastQueryCache;
            return cache2;
        }
        int lid = LexerApiPackageAccessor.get().languageId(language);
        if (this.wrapTokenIdCaches == null || lid >= this.wrapTokenIdCaches.length) {
            WrapTokenIdCache[] n = new WrapTokenIdCache[lid + 1];
            if (this.wrapTokenIdCaches != null) {
                System.arraycopy(this.wrapTokenIdCaches, 0, n, 0, this.wrapTokenIdCaches.length);
            }
            this.wrapTokenIdCaches = n;
        }
        if ((cache = this.wrapTokenIdCaches[lid]) == null) {
            this.wrapTokenIdCaches[lid] = cache = WrapTokenIdCache.get(language);
        }
        this.lastQueryLanguage = language;
        this.lastQueryCache = cache;
        return cache;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString() {
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            String errors;
            StringBuilder sb = this.toStringNoTokens(null);
            sb.append(":\n");
            LexerUtilsConstants.appendTokenList(sb, this.rootTokenList);
            if (this.path2tokenListList != null && this.path2tokenListList.size() > 0) {
                sb.append(this.path2tokenListList.size());
                sb.append(" TokenListList(s) maintained:\n");
                for (TokenListList tll : this.path2tokenListList.values()) {
                    sb.append((Object)tll).append('\n');
                }
            }
            if ((errors = this.checkConsistency()) != null) {
                sb.append("!!! CONSISTENCY ERRORS FOUND in TOKEN HIERARCHY:\n");
                sb.append(errors);
                sb.append('\n');
            }
            return sb.toString();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public StringBuilder toStringNoTokens(StringBuilder sb) {
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            if (sb == null) {
                sb = new StringBuilder(200);
            }
            sb.append("TOKEN HIERARCHY");
            if (this.inputSource() != null) {
                sb.append(" for " + this.inputSource());
            }
            if (!this.isActive()) {
                sb.append(" is NOT ACTIVE.");
            } else {
                CharSequence inputSourceText = this.rootTokenList.inputSourceText();
                sb.append("\nText: ").append(inputSourceText.getClass());
                sb.append(", length=").append(inputSourceText.length());
            }
            return sb;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String checkConsistency() {
        TokenList<T> tokenList = this.rootTokenList;
        synchronized (tokenList) {
            String error = LexerUtilsConstants.checkConsistencyTokenList(this.rootTokenList(), true);
            if (error == null && this.path2tokenListList != null) {
                for (TokenListList tll : this.path2tokenListList.values()) {
                    error = tll.checkConsistency();
                    if (error != null) {
                        return error;
                    }
                    Iterator i$ = tll.iterator();
                    while (i$.hasNext()) {
                        TokenList tl = (TokenList)i$.next();
                        error = LexerUtilsConstants.checkConsistencyTokenList(tl, false);
                        if (error == null) continue;
                        return error;
                    }
                }
            }
            return error;
        }
    }

    public void ensureConsistency() {
        String errors = this.checkConsistency();
        if (errors != null) {
            throw new IllegalStateException("!!! CONSISTENCY ERRORS FOUND in TOKEN HIERARCHY:\n" + errors);
        }
    }

    static enum Activity {
        NOT_INITED,
        INACTIVE,
        ACTIVE;
        

        private Activity() {
        }
    }

}

