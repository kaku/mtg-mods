/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer;

import org.netbeans.lib.lexer.CharPreprocessor;

public final class UnicodeEscapesPreprocessor
extends CharPreprocessor {
    @Override
    protected void preprocessChar() {
        int ch = this.inputRead();
        block0 : switch (ch) {
            case 92: {
                ch = this.inputRead();
                switch (ch) {
                    case 117: {
                        int i;
                        block12 : for (i = 4; i > 0; --i) {
                            int c = this.inputRead();
                            switch (c) {
                                case 48: 
                                case 49: 
                                case 50: 
                                case 51: 
                                case 52: 
                                case 53: 
                                case 54: 
                                case 55: 
                                case 56: 
                                case 57: {
                                    ch = (ch << 4) + (c - 48);
                                    continue block12;
                                }
                                case 97: 
                                case 98: 
                                case 99: 
                                case 100: 
                                case 101: 
                                case 102: {
                                    ch = (ch << 4) + (c - 97 + 10);
                                    continue block12;
                                }
                                case 65: 
                                case 66: 
                                case 67: 
                                case 68: 
                                case 69: 
                                case 70: {
                                    ch = (ch << 4) + (c - 65 + 10);
                                    continue block12;
                                }
                                case -1: {
                                    i = - i;
                                    continue block12;
                                }
                                default: {
                                    this.inputBackup(1);
                                    i = - i;
                                }
                            }
                        }
                        if (i < 0) {
                            this.outputPreprocessed('\uffff', 5 + i);
                            this.notifyError("Invalid unicode sequence");
                            break block0;
                        }
                        this.outputPreprocessed((char)ch, 5);
                        break block0;
                    }
                }
                this.outputOriginal(92);
                this.outputOriginal(ch);
                break;
            }
            default: {
                this.outputOriginal(ch);
            }
        }
    }

    @Override
    protected boolean isSensitiveChar(char ch) {
        switch (ch) {
            case '0': 
            case '1': 
            case '2': 
            case '3': 
            case '4': 
            case '5': 
            case '6': 
            case '7': 
            case '8': 
            case '9': 
            case 'A': 
            case 'B': 
            case 'C': 
            case 'D': 
            case 'E': 
            case 'F': 
            case '\\': 
            case 'a': 
            case 'b': 
            case 'c': 
            case 'd': 
            case 'e': 
            case 'f': 
            case 'u': {
                return true;
            }
        }
        return false;
    }

    @Override
    protected int maxLookahead() {
        return 1;
    }
}

