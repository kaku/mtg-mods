/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.lib.lexer.token;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.token.PartToken;

public final class JoinTokenText<T extends TokenId>
implements CharSequence {
    private static final Logger LOG = Logger.getLogger(JoinTokenText.class.getName());
    private final List<PartToken<T>> joinedParts;
    private final int length;
    private int activePartIndex;
    private CharSequence activeInputText;
    private int activeStartCharIndex;
    private int activeEndCharIndex;

    public JoinTokenText(List<PartToken<T>> joinedParts, int length) {
        this.joinedParts = joinedParts;
        this.activeInputText = joinedParts.get(0).text();
        this.activeEndCharIndex = this.activeInputText.length();
        this.length = length;
    }

    @Override
    public synchronized char charAt(int index) {
        if (index < this.activeStartCharIndex) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("index=" + index + " < 0");
            }
            do {
                --this.activePartIndex;
                if (this.activePartIndex < 0) {
                    LOG.log(Level.WARNING, "Internal error: index=" + index + ", " + this.dumpState());
                }
                this.activeInputText = this.joinedParts.get(this.activePartIndex).text();
                int len = this.activeInputText.length();
                this.activeEndCharIndex = this.activeStartCharIndex;
                this.activeStartCharIndex -= len;
            } while (index < this.activeStartCharIndex);
        } else if (index >= this.activeEndCharIndex) {
            if (index >= this.length) {
                throw new IndexOutOfBoundsException("index=" + index + " >= length()=" + this.length);
            }
            do {
                ++this.activePartIndex;
                this.activeInputText = this.joinedParts.get(this.activePartIndex).text();
                int len = this.activeInputText.length();
                this.activeStartCharIndex = this.activeEndCharIndex;
                this.activeEndCharIndex += len;
            } while (index >= this.activeEndCharIndex);
        }
        return this.activeInputText.charAt(index - this.activeStartCharIndex);
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return CharSequenceUtilities.toString((CharSequence)this, (int)start, (int)end);
    }

    @Override
    public synchronized String toString() {
        return CharSequenceUtilities.toString((CharSequence)this);
    }

    private String dumpState() {
        return "activeTokenListIndex=" + this.activePartIndex + ", activeStartCharIndex=" + this.activeStartCharIndex + ", activeEndCharIndex=" + this.activeEndCharIndex + ", length=" + this.length;
    }
}

