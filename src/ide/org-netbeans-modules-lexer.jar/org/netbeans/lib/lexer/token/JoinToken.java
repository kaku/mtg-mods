/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 */
package org.netbeans.lib.lexer.token;

import java.util.List;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.JoinTokenText;
import org.netbeans.lib.lexer.token.PartToken;
import org.netbeans.lib.lexer.token.PropertyToken;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public final class JoinToken<T extends TokenId>
extends PropertyToken<T> {
    private List<PartToken<T>> joinedParts;
    private int completeLength;
    private int extraTokenListSpanCount;

    public JoinToken(WrapTokenId<T> wid, int length, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        super(wid, length, propertyProvider, partType);
    }

    @Override
    public List<PartToken<T>> joinedParts() {
        return this.joinedParts;
    }

    public void setJoinedParts(List<PartToken<T>> joinedParts, int extraTokenListSpanCount) {
        assert (joinedParts != null);
        this.joinedParts = joinedParts;
        for (PartToken<T> partToken : joinedParts) {
            this.completeLength += partToken.length();
        }
        this.extraTokenListSpanCount = extraTokenListSpanCount;
    }

    public PartToken<T> lastPart() {
        return this.joinedParts.get(this.joinedParts.size() - 1);
    }

    public int extraTokenListSpanCount() {
        return this.extraTokenListSpanCount;
    }

    @Override
    public int offset(TokenHierarchy<?> tokenHierarchy) {
        return this.joinedParts.get(0).offset(tokenHierarchy);
    }

    public int endOffset() {
        PartToken<T> partToken = this.joinedParts.get(this.joinedParts.size() - 1);
        return partToken.offset(null) + partToken.length();
    }

    @Override
    public int length() {
        return this.completeLength;
    }

    @Override
    public CharSequence text() {
        return new JoinTokenText<T>(this.joinedParts, this.completeLength);
    }

    @Override
    public boolean isRemoved() {
        return this.lastPart().isRemoved();
    }

    @Override
    public StringBuilder dumpInfo(StringBuilder sb, TokenHierarchy<?> tokenHierarchy, boolean dumpTokenText, boolean dumpRealOffset, int indent) {
        sb = PropertyToken.super.dumpInfo(sb, tokenHierarchy, dumpTokenText, dumpRealOffset, indent);
        sb.append(", ").append(this.joinedParts.size()).append(" parts");
        int digitCount = String.valueOf(this.joinedParts.size() - 1).length();
        for (int i = 0; i < this.joinedParts.size(); ++i) {
            sb.append('\n');
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)(indent + 2));
            ArrayUtilities.appendBracketedIndex((StringBuilder)sb, (int)i, (int)digitCount);
            this.joinedParts.get(i).dumpInfo(sb, tokenHierarchy, dumpTokenText, dumpRealOffset, indent + 4);
        }
        return sb;
    }

    @Override
    public StringBuilder dumpText(StringBuilder sb, CharSequence inputSourceText) {
        for (int i = 0; i < this.joinedParts.size(); ++i) {
            this.joinedParts.get(i).dumpText(sb, inputSourceText);
        }
        return sb;
    }

    @Override
    protected String dumpInfoTokenType() {
        return "JoiT";
    }
}

