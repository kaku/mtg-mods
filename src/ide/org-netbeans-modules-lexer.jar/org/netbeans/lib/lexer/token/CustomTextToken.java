/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.token;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.TextToken;

public class CustomTextToken<T extends TokenId>
extends TextToken<T> {
    private int length;

    public CustomTextToken(WrapTokenId<T> wid, CharSequence text, int length) {
        super(wid, text);
        this.length = length;
    }

    @Override
    public boolean isCustomText() {
        return true;
    }

    @Override
    public final int length() {
        return this.length;
    }

    @Override
    protected String dumpInfoTokenType() {
        return "CusT";
    }
}

