/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.token;

import java.util.EnumSet;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public final class PartTypePropertyProvider
implements TokenPropertyProvider<TokenId> {
    private static final PartTypePropertyProvider[] partTypeOrdinal2Provider = new PartTypePropertyProvider[PartType.class.getEnumConstants().length];
    private PartType partType;

    public static <T extends TokenId> TokenPropertyProvider<T> get(PartType partType) {
        PartTypePropertyProvider provider = partTypeOrdinal2Provider[partType.ordinal()];
        return provider;
    }

    public static <T extends TokenId> TokenPropertyProvider<T> createDelegating(PartType partType, TokenPropertyProvider<T> delegate) {
        return new Delegating<T>(partType, delegate);
    }

    public PartTypePropertyProvider(PartType partType) {
        this.partType = partType;
    }

    @Override
    public Object getValue(Token<TokenId> token, Object key) {
        if (key == PartType.class) {
            return this.partType;
        }
        return null;
    }

    static {
        for (PartType partType : EnumSet.allOf(PartType.class)) {
            PartTypePropertyProvider.partTypeOrdinal2Provider[partType.ordinal()] = new PartTypePropertyProvider(partType);
        }
    }

    private static final class Delegating<T extends TokenId>
    implements TokenPropertyProvider<T> {
        private final PartType partType;
        private final TokenPropertyProvider<T> delegate;

        Delegating(PartType partType, TokenPropertyProvider<T> delegate) {
            assert (delegate != null);
            this.partType = partType;
            this.delegate = delegate;
        }

        @Override
        public Object getValue(Token<T> token, Object key) {
            if (key == PartType.class) {
                return this.partType;
            }
            return this.delegate.getValue(token, key);
        }
    }

}

