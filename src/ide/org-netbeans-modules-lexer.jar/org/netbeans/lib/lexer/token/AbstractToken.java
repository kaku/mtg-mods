/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.lib.lexer.token;

import java.util.List;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.WrapTokenId;

public abstract class AbstractToken<T extends TokenId>
extends Token<T>
implements TokenOrEmbedding<T> {
    private WrapTokenId<T> wid;
    protected TokenList<T> tokenList;
    protected int rawOffset;

    public AbstractToken(WrapTokenId<T> wid) {
        assert (wid != null);
        this.wid = wid;
    }

    AbstractToken(WrapTokenId<T> wid, int rawOffset) {
        this.wid = wid;
        this.setRawOffset(rawOffset);
    }

    @Override
    public final T id() {
        return this.wid.id();
    }

    public WrapTokenId<T> wid() {
        return this.wid;
    }

    public void setWid(WrapTokenId<T> wid) {
        assert (this.rawOffset != -1);
        this.wid = wid;
    }

    public final TokenList<T> tokenList() {
        return this.tokenList;
    }

    public final void setTokenList(TokenList<T> tokenList) {
        this.tokenList = tokenList;
    }

    public final int rawOffset() {
        return this.rawOffset;
    }

    public final void setRawOffset(int rawOffset) {
        this.rawOffset = rawOffset;
    }

    @Override
    public final boolean isFlyweight() {
        return this.rawOffset == -1;
    }

    public final void makeFlyweight() {
        this.setRawOffset(-1);
    }

    @Override
    public PartType partType() {
        return PartType.COMPLETE;
    }

    @Override
    public boolean isCustomText() {
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int offset(TokenHierarchy<?> tokenHierarchy) {
        TokenList<T> tList = this.tokenList;
        if (tList != null) {
            TokenList rootTokenList;
            TokenList tokenList = rootTokenList = tList.rootTokenList();
            synchronized (tokenList) {
                if (tList.getClass() == EmbeddedTokenList.class) {
                    ((EmbeddedTokenList)tList).updateModCount();
                }
                return tList.tokenOffset(this);
            }
        }
        return this.rawOffset;
    }

    @Override
    public boolean hasProperties() {
        return false;
    }

    @Override
    public Object getProperty(Object key) {
        return null;
    }

    @Override
    public Token<T> joinToken() {
        return null;
    }

    @Override
    public List<? extends Token<T>> joinedParts() {
        return null;
    }

    @Override
    public final AbstractToken<T> token() {
        return this;
    }

    @Override
    public final EmbeddedTokenList<T, ?> embedding() {
        return null;
    }

    @Override
    public boolean isRemoved() {
        TokenList<T> tList = this.tokenList;
        if (tList != null) {
            return tList.getClass() == EmbeddedTokenList.class && tList.isRemoved();
        }
        return !this.isFlyweight();
    }

    public String dumpInfo() {
        return this.dumpInfo(null, null, true, true, 0).toString();
    }

    public StringBuilder dumpInfo(StringBuilder sb, TokenHierarchy<?> tokenHierarchy, boolean dumpText, boolean dumpRealOffset, int indent) {
        if (sb == null) {
            sb = new StringBuilder(50);
        }
        if (dumpText) {
            try {
                CharSequence text = this.text();
                if (text != null) {
                    sb.append('\"');
                    this.dumpTextImpl(sb, text);
                    sb.append('\"');
                } else {
                    sb.append("<null-text>");
                }
            }
            catch (NullPointerException e) {
                sb.append("NPE in Token.text()!!!");
            }
            sb.append(' ');
        }
        if (this.isFlyweight()) {
            sb.append("F(").append(this.length()).append(')');
        } else {
            int offset = dumpRealOffset ? this.offset(tokenHierarchy) : this.rawOffset();
            sb.append('<').append(offset);
            sb.append(",").append(offset + this.length()).append('>');
        }
        sb.append(' ').append(this.wid != null ? this.id().name() + '[' + this.id().ordinal() + ']' : "<null-id>");
        sb.append(" ").append(this.dumpInfoTokenType());
        return sb;
    }

    public StringBuilder dumpText(StringBuilder sb, CharSequence inputSourceText) {
        assert (this.tokenList == null);
        int length = this.length();
        if (sb == null) {
            sb = new StringBuilder(length + 10);
        }
        CharSequence text = this.isFlyweight() ? this.text() : inputSourceText.subSequence(this.rawOffset, this.rawOffset + length);
        this.dumpTextImpl(sb, text);
        return sb;
    }

    private void dumpTextImpl(StringBuilder sb, CharSequence text) {
        int textLength = text.length();
        for (int i = 0; i < textLength; ++i) {
            if (textLength > 400 && i >= 200 && i < textLength - 200) {
                i = textLength - 200;
                sb.append(" ...<TEXT-SHORTENED>... ");
                continue;
            }
            try {
                CharSequenceUtilities.debugChar((StringBuilder)sb, (char)text.charAt(i));
                continue;
            }
            catch (IndexOutOfBoundsException e) {
                sb.append("IOOBE at index=").append(i).append("!!!");
                break;
            }
        }
    }

    protected String dumpInfoTokenType() {
        return "AbsT";
    }
}

