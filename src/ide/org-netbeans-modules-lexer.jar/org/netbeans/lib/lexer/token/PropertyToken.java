/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.token;

import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.DefaultToken;
import org.netbeans.lib.lexer.token.PartTypePropertyProvider;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public class PropertyToken<T extends TokenId>
extends DefaultToken<T> {
    private final TokenPropertyProvider<T> propertyProvider;

    public PropertyToken(WrapTokenId<T> wid, int length, TokenPropertyProvider<T> propertyProvider, PartType partType) {
        super(wid, length);
        assert (partType != null);
        this.propertyProvider = propertyProvider != null ? PartTypePropertyProvider.createDelegating(partType, propertyProvider) : PartTypePropertyProvider.get(partType);
    }

    @Override
    public boolean hasProperties() {
        return this.propertyProvider != null;
    }

    @Override
    public Object getProperty(Object key) {
        return this.propertyProvider != null ? this.propertyProvider.getValue(this, key) : null;
    }

    @Override
    public PartType partType() {
        return (PartType)((Object)this.getProperty(PartType.class));
    }

    @Override
    protected String dumpInfoTokenType() {
        return "ProT";
    }
}

