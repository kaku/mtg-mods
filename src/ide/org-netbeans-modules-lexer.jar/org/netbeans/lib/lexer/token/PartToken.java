/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.token;

import java.util.List;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.AbstractToken;
import org.netbeans.lib.lexer.token.JoinToken;
import org.netbeans.lib.lexer.token.PropertyToken;
import org.netbeans.spi.lexer.TokenPropertyProvider;

public final class PartToken<T extends TokenId>
extends PropertyToken<T> {
    private TokenOrEmbedding<T> joinTokenOrEmbedding;
    private final int partTokenIndex;
    private final int partTextOffset;

    public PartToken(WrapTokenId<T> wid, int length, TokenPropertyProvider<T> propertyProvider, PartType partType, TokenOrEmbedding<T> joinToken, int partTokenIndex, int partTextOffset) {
        super(wid, length, propertyProvider, partType);
        this.setJoinTokenOrEmbedding(joinToken);
        this.partTokenIndex = partTokenIndex;
        this.partTextOffset = partTextOffset;
    }

    @Override
    public JoinToken<T> joinToken() {
        return (JoinToken)this.joinTokenOrEmbedding.token();
    }

    public boolean isLastPart() {
        return this.joinToken().lastPart() == this;
    }

    public TokenOrEmbedding<T> joinTokenOrEmbedding() {
        return this.joinTokenOrEmbedding;
    }

    public void setJoinTokenOrEmbedding(TokenOrEmbedding<T> joinTokenOrEmbedding) {
        this.joinTokenOrEmbedding = joinTokenOrEmbedding;
    }

    public int partTokenIndex() {
        return this.partTokenIndex;
    }

    public int partTextOffset() {
        return this.partTextOffset;
    }

    public int partTextEndOffset() {
        return this.partTextOffset + this.length();
    }

    @Override
    protected String dumpInfoTokenType() {
        return "ParT[" + (this.partTokenIndex + 1) + "/" + this.joinToken().joinedParts().size() + "]";
    }
}

