/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.lib.lexer.token;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.lexer.StackElementArray;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.AbstractToken;

public class DefaultToken<T extends TokenId>
extends AbstractToken<T> {
    private static final Logger LOG;
    private static final boolean LOG_TOKEN_TEXT_TO_STRING;
    private static final int TOKEN_TEXT_TO_STRING_STACK_LENGTH;
    private static final Set<StackElementArray> toStringStacks;
    int tokenLength;
    private static boolean textFailureLogged;

    public DefaultToken(WrapTokenId<T> wid, int length) {
        super(wid);
        assert (length > 0);
        this.tokenLength = length;
    }

    public DefaultToken(WrapTokenId<T> wid) {
        super(wid);
        this.tokenLength = 0;
    }

    @Override
    public int length() {
        return this.tokenLength;
    }

    @Override
    protected String dumpInfoTokenType() {
        return "DefT";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public CharSequence text() {
        InputSourceSubsequence text;
        TokenList tList = this.tokenList;
        if (!this.isRemoved()) {
            TokenList rootTokenList;
            TokenList tokenList = rootTokenList = tList.rootTokenList();
            synchronized (tokenList) {
                int tokenOffset;
                CharSequence inputSourceText = tList.inputSourceText();
                int start = tokenOffset = tList.tokenOffset(this);
                int end = tokenOffset + this.length();
                if (!LOG_TOKEN_TEXT_TO_STRING) {
                    try {
                        return inputSourceText.subSequence(start, end);
                    }
                    catch (IndexOutOfBoundsException ex) {
                        if (!textFailureLogged) {
                            textFailureLogged = true;
                            LOG.log(Level.INFO, "Obtaining of token text failed.", ex);
                            LOG.info("Errorneous token: IHC=" + System.identityHashCode(this) + ", rawOffset=" + this.rawOffset() + ", tokenLength=" + this.tokenLength + ", start=" + start + ", end=" + end);
                            LOG.info("Errorneous token hierarchy:\n" + rootTokenList.tokenHierarchyOperation().toString());
                        }
                        return "";
                    }
                }
                CharSequenceUtilities.checkIndexesValid((CharSequence)inputSourceText, (int)start, (int)end);
                text = new InputSourceSubsequence(this, inputSourceText, start, end);
            }
        } else {
            text = null;
        }
        return text;
    }

    static {
        int val;
        LOG = Logger.getLogger(DefaultToken.class.getName());
        try {
            val = Integer.parseInt(System.getProperty("org.netbeans.lexer.token.text.to.string"));
        }
        catch (NumberFormatException ex) {
            val = 0;
        }
        LOG_TOKEN_TEXT_TO_STRING = val > 0;
        TOKEN_TEXT_TO_STRING_STACK_LENGTH = val;
        toStringStacks = LOG_TOKEN_TEXT_TO_STRING ? StackElementArray.createSet() : null;
    }

    private static final class InputSourceSubsequence
    implements CharSequence {
        private final DefaultToken<?> token;
        private final CharSequence inputSourceText;
        private final int start;
        private final int end;

        public InputSourceSubsequence(DefaultToken token, CharSequence text, int start, int end) {
            this.token = token;
            this.inputSourceText = text;
            this.start = start;
            this.end = end;
        }

        @Override
        public int length() {
            return this.end - this.start;
        }

        @Override
        public char charAt(int index) {
            CharSequenceUtilities.checkIndexValid((int)index, (int)this.length());
            try {
                return this.inputSourceText.charAt(this.start + index);
            }
            catch (IndexOutOfBoundsException ex) {
                TokenHierarchyOperation op;
                Object inputSource;
                StringBuilder sb = new StringBuilder(200);
                sb.append("Internal lexer error: index=").append(index).append(", length()=").append(this.length()).append("\n  start=").append(this.start).append(", end=").append(this.end).append("\n  tokenOffset=").append(this.token.offset(null)).append(", tokenLength=").append(this.token.length()).append(", inputSourceLength=").append(this.inputSourceText.length()).append('\n');
                TokenList tokenList = this.token.tokenList();
                if (tokenList != null && (op = tokenList.tokenHierarchyOperation()) != null && (inputSource = op.inputSource()) != null) {
                    sb.append("  inputSource: ").append(inputSource.getClass());
                    if (inputSource instanceof Document) {
                        Document doc = (Document)inputSource;
                        sb.append("  document-locked: ").append(DocumentUtilities.isReadLocked((Document)doc));
                    }
                    sb.append('\n');
                }
                throw new IndexOutOfBoundsException(sb.toString());
            }
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            CharSequenceUtilities.checkIndexesValid((CharSequence)this, (int)start, (int)end);
            return new InputSourceSubsequence(this.token, this.inputSourceText, this.start + start, this.start + end);
        }

        @Override
        public String toString() {
            if (LOG_TOKEN_TEXT_TO_STRING && StackElementArray.addStackIfNew(toStringStacks, TOKEN_TEXT_TO_STRING_STACK_LENGTH)) {
                LOG.log(Level.INFO, "Token.text().toString() called", new Exception());
            }
            return this.inputSourceText.subSequence(this.start, this.end).toString();
        }
    }

}

