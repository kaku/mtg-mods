/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.lexer.token;

import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.WrapTokenId;
import org.netbeans.lib.lexer.token.AbstractToken;

public class TextToken<T extends TokenId>
extends AbstractToken<T> {
    private final CharSequence text;

    public TextToken(WrapTokenId<T> wid, CharSequence text) {
        super(wid);
        assert (text != null);
        this.text = text;
    }

    private TextToken(WrapTokenId<T> wid, int rawOffset, CharSequence text) {
        super(wid, rawOffset);
        assert (text != null);
        this.text = text;
    }

    @Override
    public int length() {
        return this.text.length();
    }

    @Override
    public final CharSequence text() {
        return this.text;
    }

    public final TextToken<T> createCopy(TokenList<T> tokenList, int rawOffset) {
        TextToken<T> token = new TextToken<T>(this.wid(), rawOffset, this.text);
        token.setTokenList(tokenList);
        return token;
    }

    @Override
    protected String dumpInfoTokenType() {
        return this.isFlyweight() ? "FlyT" : "TexT";
    }

    public String toString() {
        return this.text.toString();
    }
}

