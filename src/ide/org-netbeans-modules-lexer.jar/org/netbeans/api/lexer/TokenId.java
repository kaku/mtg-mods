/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

public interface TokenId {
    public String name();

    public int ordinal();

    public String primaryCategory();
}

