/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.util.EventObject;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEventType;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;

public final class TokenHierarchyEvent
extends EventObject {
    private final TokenHierarchyEventInfo info;

    TokenHierarchyEvent(TokenHierarchyEventInfo info) {
        super(info.tokenHierarchyOperation().tokenHierarchy());
        this.info = info;
    }

    public TokenHierarchy<?> tokenHierarchy() {
        return (TokenHierarchy)this.getSource();
    }

    public TokenHierarchyEventType type() {
        return this.info.type();
    }

    public TokenChange<?> tokenChange() {
        return this.info.tokenChange();
    }

    public <T extends TokenId> TokenChange<T> tokenChange(Language<T> language) {
        TokenChange tc = this.tokenChange();
        TokenChange tcl = tc != null && tc.language() == language ? tc : null;
        return tcl;
    }

    public int affectedStartOffset() {
        return this.info.affectedStartOffset();
    }

    public int affectedEndOffset() {
        return this.info.affectedEndOffset();
    }

    public int modificationOffset() {
        return this.info.modOffset();
    }

    public int insertedLength() {
        return this.info.insertedLength();
    }

    public int removedLength() {
        return this.info.removedLength();
    }

    @Override
    public String toString() {
        return "THEvent IHC=" + System.identityHashCode(this) + "; " + this.info;
    }
}

