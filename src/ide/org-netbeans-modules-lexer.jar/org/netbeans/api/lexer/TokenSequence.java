/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.util.ConcurrentModificationException;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.EmbeddedJoinInfo;
import org.netbeans.lib.lexer.EmbeddedTokenList;
import org.netbeans.lib.lexer.EmbeddingOperation;
import org.netbeans.lib.lexer.JoinTokenList;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.SubSequenceTokenList;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.TokenOrEmbedding;
import org.netbeans.lib.lexer.token.AbstractToken;

public final class TokenSequence<T extends TokenId> {
    private final TokenList<T> tokenList;
    private AbstractToken<T> token;
    private int tokenIndex;
    private int tokenOffset = -1;
    private final TokenList<?> rootTokenList;
    private final int modCount;
    private final EmbeddedTokenList<?, T> embeddedTokenList;

    TokenSequence(TokenList<T> tokenList) {
        this.tokenList = tokenList;
        this.rootTokenList = tokenList.rootTokenList();
        assert (this.rootTokenList != null);
        if (tokenList instanceof EmbeddedTokenList) {
            this.embeddedTokenList = (EmbeddedTokenList)tokenList;
            this.embeddedTokenList.updateModCount(this.rootTokenList.modCount());
        } else {
            this.embeddedTokenList = null;
        }
        this.modCount = tokenList.modCount();
    }

    public Language<T> language() {
        return this.tokenList.language();
    }

    public LanguagePath languagePath() {
        return this.tokenList.languagePath();
    }

    public Token<T> token() {
        return this.token;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Token<T> offsetToken() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkTokenNotNull();
            if (this.token.isFlyweight()) {
                this.token = this.tokenList.replaceFlyToken(this.tokenIndex, this.token, this.offset());
            }
        }
        return this.token;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int offset() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkTokenNotNull();
            if (this.tokenOffset == -1) {
                this.tokenOffset = this.tokenList.tokenOffset(this.tokenIndex);
            }
            return this.tokenOffset;
        }
    }

    public int index() {
        return this.tokenIndex;
    }

    public TokenSequence<?> embedded() {
        return this.embeddedImpl(null, false);
    }

    public <ET extends TokenId> TokenSequence<ET> embedded(Language<ET> embeddedLanguage) {
        return this.embeddedImpl(embeddedLanguage, false);
    }

    public TokenSequence<?> embeddedJoined() {
        return this.embeddedImpl(null, true);
    }

    public <ET extends TokenId> TokenSequence<ET> embeddedJoined(Language<ET> embeddedLanguage) {
        return this.embeddedImpl(embeddedLanguage, true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <ET extends TokenId> TokenSequence<ET> embeddedImpl(Language<ET> embeddedLanguage, boolean joined) {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkTokenNotNull();
            if (this.token.isFlyweight()) {
                return null;
            }
            this.checkValid();
            EmbeddedTokenList etl = EmbeddingOperation.embeddedTokenList(this.tokenList, this.tokenIndex, embeddedLanguage, true);
            if (etl != null) {
                TokenSequence<T> tse;
                JoinTokenList jtl;
                etl.updateModCount(this.rootTokenList.modCount());
                EmbeddedJoinInfo joinInfo = etl.joinInfo();
                if (joined && joinInfo != null && (jtl = joinInfo.joinTokenList) != null) {
                    JoinTokenList joinTokenList = jtl;
                    tse = new TokenSequence(joinTokenList);
                    jtl.setActiveTokenListIndex(joinInfo.tokenListIndex());
                    tse.moveIndex(joinTokenList.activeStartJoinIndex());
                } else {
                    tse = new TokenSequence<T>(etl);
                }
                return tse;
            }
            return null;
        }
    }

    public boolean createEmbedding(Language<?> embeddedLanguage, int startSkipLength, int endSkipLength) {
        return this.createEmbedding(embeddedLanguage, startSkipLength, endSkipLength, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean createEmbedding(Language<?> embeddedLanguage, int startSkipLength, int endSkipLength, boolean joinSections) {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkTokenNotNull();
            this.checkValid();
            return EmbeddingOperation.createEmbedding(this.tokenList, this.tokenIndex, embeddedLanguage, startSkipLength, endSkipLength, joinSections);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean removeEmbedding(Language<?> embeddedLanguage) {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkTokenNotNull();
            this.checkValid();
            return EmbeddingOperation.removeEmbedding(this.tokenList, this.tokenIndex, embeddedLanguage);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean moveNext() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            TokenOrEmbedding<T> tokenOrEmbedding;
            this.checkValid();
            if (this.token != null) {
                ++this.tokenIndex;
            }
            if ((tokenOrEmbedding = this.tokenList.tokenOrEmbedding(this.tokenIndex)) != null) {
                AbstractToken<T> origToken = this.token;
                this.token = tokenOrEmbedding.token();
                if (this.tokenOffset != -1) {
                    this.tokenOffset = origToken != null ? (this.tokenList.isContinuous() || this.token.isFlyweight() ? (this.tokenOffset += origToken.length()) : -1) : -1;
                }
                return true;
            }
            if (this.token != null) {
                --this.tokenIndex;
            }
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean movePrevious() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            if (this.tokenIndex > 0) {
                AbstractToken<T> origToken = this.token;
                --this.tokenIndex;
                this.token = this.tokenList.tokenOrEmbedding(this.tokenIndex).token();
                if (this.tokenOffset != -1) {
                    this.tokenOffset = origToken != null ? (this.tokenList.isContinuous() || origToken.isFlyweight() ? (this.tokenOffset -= this.token.length()) : -1) : -1;
                }
                return true;
            }
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int moveIndex(int index) {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            return this.moveIndexImpl(index);
        }
    }

    private int moveIndexImpl(int index) {
        if (index >= 0) {
            TokenOrEmbedding<T> tokenOrEmbedding = this.tokenList.tokenOrEmbedding(index);
            if (tokenOrEmbedding != null) {
                this.resetTokenIndex(index, -1);
            } else {
                this.resetTokenIndex(this.tokenCount(), -1);
            }
        } else {
            this.resetTokenIndex(0, -1);
        }
        return index - this.tokenIndex;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void moveStart() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            this.moveIndex(0);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void moveEnd() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            this.moveIndex(this.tokenList.tokenCount());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int move(int offset) {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            int[] indexAndTokenOffset = this.tokenList.tokenIndex(offset);
            if (indexAndTokenOffset[0] != -1) {
                this.resetTokenIndex(indexAndTokenOffset[0], indexAndTokenOffset[1]);
            } else {
                this.resetTokenIndex(0, -1);
            }
            return offset - indexAndTokenOffset[1];
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isEmpty() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            return this.tokenIndex == 0 && this.tokenList.tokenOrEmbedding(0) == null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int tokenCount() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            this.checkValid();
            return this.tokenList.tokenCount();
        }
    }

    public TokenSequence<T> subSequence(int startOffset) {
        return this.subSequence(startOffset, Integer.MAX_VALUE);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenSequence<T> subSequence(int startOffset, int endOffset) {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            TokenList<T> tl;
            this.checkValid();
            if (this.tokenList.getClass() == SubSequenceTokenList.class) {
                SubSequenceTokenList stl = (SubSequenceTokenList)this.tokenList;
                tl = stl.delegate();
                startOffset = Math.max(startOffset, stl.limitStartOffset());
                endOffset = Math.min(endOffset, stl.limitEndOffset());
            } else {
                tl = this.tokenList;
            }
            return new TokenSequence<T>(new SubSequenceTokenList<T>(tl, startOffset, endOffset));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isValid() {
        TokenList tokenList = this.rootTokenList;
        synchronized (tokenList) {
            return !this.isInvalid();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("TokenSequence for ").append(this.tokenList.languagePath().mimePath());
        sb.append(" at tokenIndex=").append(this.tokenIndex);
        sb.append(". TokenList contains ").append(this.tokenList.tokenCount()).append(" tokens:\n");
        LexerUtilsConstants.appendTokenList(sb, this.tokenList, this.tokenIndex, 0, Integer.MAX_VALUE, true, 0, true);
        sb.append('\n');
        return sb.toString();
    }

    private void resetTokenIndex(int index, int offset) {
        this.tokenIndex = index;
        this.token = null;
        this.tokenOffset = offset;
    }

    private void checkTokenNotNull() {
        if (this.token == null) {
            throw new IllegalStateException("Caller of TokenSequence forgot to call moveNext/Previous() or it returned false (no more tokens)\n" + this);
        }
    }

    private void checkValid() {
        if (this.isInvalid()) {
            throw new ConcurrentModificationException("Caller uses obsolete token sequence which is no longer valid. Underlying token hierarchy has been modified by insertion or removal or a custom language embedding was created.TS.modCount=" + this.modCount + ", tokenList.modCount()=" + this.tokenList.modCount() + ", rootModCount=" + this.rootTokenList.modCount() + "\nPlease report a bug against a module that calls lexer's code e.g. java, php etc. " + "but not the lexer module itself.");
        }
    }

    private boolean isInvalid() {
        if (this.embeddedTokenList != null) {
            this.embeddedTokenList.updateModCount(this.rootTokenList.modCount());
        }
        return this.modCount != this.tokenList.modCount();
    }
}

