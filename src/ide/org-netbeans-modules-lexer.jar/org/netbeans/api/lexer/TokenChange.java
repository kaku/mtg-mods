/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.lexer.LexerUtilsConstants;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.inc.RemovedTokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;

public final class TokenChange<T extends TokenId> {
    private final TokenChangeInfo<T> info;

    TokenChange(TokenChangeInfo<T> info) {
        this.info = info;
    }

    public int embeddedChangeCount() {
        return this.info.embeddedChanges().length;
    }

    public TokenChange<?> embeddedChange(int index) {
        return this.info.embeddedChanges()[index];
    }

    public Language<T> language() {
        return LexerUtilsConstants.innerLanguage(this.languagePath());
    }

    public LanguagePath languagePath() {
        return this.info.currentTokenList().languagePath();
    }

    public int index() {
        return this.info.index();
    }

    public int offset() {
        return this.info.offset();
    }

    public int removedTokenCount() {
        RemovedTokenList<T> rtl = this.info.removedTokenList();
        return rtl != null ? rtl.tokenCount() : 0;
    }

    public TokenSequence<T> removedTokenSequence() {
        return new TokenSequence<T>(this.info.removedTokenList());
    }

    public int addedTokenCount() {
        return this.info.addedTokenCount();
    }

    public TokenSequence<T> currentTokenSequence() {
        TokenSequence<T> ts = new TokenSequence<T>(this.info.currentTokenList());
        ts.moveIndex(this.index());
        return ts;
    }

    public boolean isBoundsChange() {
        return this.info.isBoundsChange();
    }

    TokenChangeInfo<T> info() {
        return this.info;
    }

    public String toString() {
        return "index=" + this.index() + ", offset=" + this.offset() + "+T:" + this.addedTokenCount() + " -T:" + this.removedTokenCount() + " ECC:" + this.embeddedChangeCount() + (this.isBoundsChange() ? ", BC" : "");
    }
}

