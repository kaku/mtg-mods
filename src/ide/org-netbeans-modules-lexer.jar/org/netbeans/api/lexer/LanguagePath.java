/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.WeakHashMap;
import org.netbeans.api.lexer.Language;

public final class LanguagePath {
    private static final LanguagePath EMPTY = new LanguagePath();
    private final Language<?>[] languages;
    private Map<Object, Reference<LanguagePath>> language2path;
    private String mimePath;
    private LanguagePath parent;

    public static LanguagePath get(Language<?> language) {
        return LanguagePath.get(null, language);
    }

    public static LanguagePath get(LanguagePath prefix, Language<?> language) {
        if (prefix == null) {
            prefix = EMPTY;
        }
        return prefix.embedded(language);
    }

    private LanguagePath(LanguagePath prefix, Language<?> language) {
        int prefixSize = prefix.size();
        this.languages = this.allocateLanguageArray(prefixSize + 1);
        System.arraycopy(prefix.languages, 0, this.languages, 0, prefixSize);
        this.languages[prefixSize] = language;
        this.parent = prefix == EMPTY ? null : prefix;
    }

    private LanguagePath() {
        this.languages = this.allocateLanguageArray(0);
    }

    public int size() {
        return this.languages.length;
    }

    public Language<?> language(int index) {
        return this.languages[index];
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public LanguagePath embedded(Language<?> language) {
        if (language == null) {
            throw new IllegalArgumentException("language cannot be null");
        }
        Language<?>[] arrlanguage = this.languages;
        synchronized (arrlanguage) {
            LanguagePath lp;
            this.initLanguage2path();
            Reference<LanguagePath> lpRef = this.language2path.get(language);
            if (lpRef == null || (lp = lpRef.get()) == null) {
                lp = new LanguagePath(this, language);
                this.language2path.put(language, new SoftReference<LanguagePath>(lp));
            }
            return lp;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public LanguagePath embedded(LanguagePath suffix) {
        if (suffix == null) {
            throw new IllegalArgumentException("suffix cannot be null");
        }
        Language<?>[] arrlanguage = this.languages;
        synchronized (arrlanguage) {
            LanguagePath lp;
            this.initLanguage2path();
            Reference<LanguagePath> lpRef = this.language2path.get(suffix);
            if (lpRef == null || (lp = lpRef.get()) == null) {
                lp = this;
                for (int i = 0; i < suffix.size(); ++i) {
                    lp = lp.embedded(suffix.language(i));
                }
                this.language2path.put(suffix, new SoftReference<LanguagePath>(lp));
            }
            return lp;
        }
    }

    public LanguagePath parent() {
        return this.parent;
    }

    public Language<?> topLanguage() {
        return this.language(0);
    }

    public Language<?> innerLanguage() {
        return this.language(this.size() - 1);
    }

    public boolean endsWith(LanguagePath languagePath) {
        if (languagePath == this || languagePath == EMPTY) {
            return true;
        }
        int lpSize = languagePath.size();
        if (lpSize <= this.size()) {
            for (int i = 1; i <= lpSize; ++i) {
                if (this.language(this.size() - i) == languagePath.language(lpSize - i)) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    public LanguagePath subPath(int startIndex) {
        return this.subPath(startIndex, this.size());
    }

    public LanguagePath subPath(int startIndex, int endIndex) {
        if (startIndex < 0) {
            throw new IndexOutOfBoundsException("startIndex=" + startIndex + " < 0");
        }
        if (endIndex > this.size()) {
            throw new IndexOutOfBoundsException("endIndex=" + endIndex + " > size()=" + this.size());
        }
        if (startIndex >= endIndex) {
            throw new IndexOutOfBoundsException("startIndex=" + startIndex + " >= endIndex=" + endIndex);
        }
        if (startIndex == 0 && endIndex == this.size()) {
            return this;
        }
        LanguagePath lp = LanguagePath.get(this.language(startIndex++));
        while (startIndex < endIndex) {
            lp = LanguagePath.get(lp, this.language(startIndex++));
        }
        return lp;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String mimePath() {
        Language<?>[] arrlanguage = this.languages;
        synchronized (arrlanguage) {
            if (this.mimePath == null) {
                StringBuilder sb = new StringBuilder(15 * this.languages.length);
                for (Language language : this.languages) {
                    if (sb.length() > 0) {
                        sb.append('/');
                    }
                    sb.append(language.mimeType());
                }
                this.mimePath = sb.toString().intern();
            }
            return this.mimePath;
        }
    }

    private void initLanguage2path() {
        if (this.language2path == null) {
            this.language2path = new WeakHashMap<Object, Reference<LanguagePath>>();
        }
    }

    private Language<?>[] allocateLanguageArray(int length) {
        return new Language[length];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LanguagePath: size=").append(this.size());
        sb.append(", IHC=").append(System.identityHashCode(this));
        sb.append('\n');
        for (int i = 0; i < this.size(); ++i) {
            sb.append('[').append(i).append("]: ");
            sb.append(this.language(i)).append('\n');
        }
        return sb.toString();
    }
}

