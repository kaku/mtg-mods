/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.util.List;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.lib.lexer.token.AbstractToken;

public abstract class Token<T extends TokenId> {
    protected Token() {
        if (!(this instanceof AbstractToken)) {
            throw new IllegalStateException("Custom token implementations prohibited.");
        }
    }

    public abstract T id();

    public abstract CharSequence text();

    public abstract boolean isCustomText();

    public abstract int length();

    public abstract int offset(TokenHierarchy<?> var1);

    public abstract boolean isFlyweight();

    public boolean isRemoved() {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public abstract PartType partType();

    public Token<T> joinToken() {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public List<? extends Token<T>> joinedParts() {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public abstract boolean hasProperties();

    public abstract Object getProperty(Object var1);

    public final int hashCode() {
        return super.hashCode();
    }

    public final boolean equals(Object o) {
        return super.equals(o);
    }
}

