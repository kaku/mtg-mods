/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

public enum TokenHierarchyEventType {
    MODIFICATION,
    RELEX,
    REBUILD,
    ACTIVITY,
    EMBEDDING_CREATED,
    EMBEDDING_REMOVED,
    LANGUAGE_PATHS;
    

    private TokenHierarchyEventType() {
    }
}

