/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.util.EventListener;
import org.netbeans.api.lexer.TokenHierarchyEvent;

public interface TokenHierarchyListener
extends EventListener {
    public void tokenHierarchyChanged(TokenHierarchyEvent var1);
}

