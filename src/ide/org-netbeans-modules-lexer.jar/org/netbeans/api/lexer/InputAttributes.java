/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;

public final class InputAttributes {
    private final Map<LanguagePath, LPAttrs> lp2attrs = new HashMap<LanguagePath, LPAttrs>();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Object getValue(LanguagePath languagePath, Object attributeKey) {
        this.checkAttributeKeyNotNull(attributeKey);
        Map<LanguagePath, LPAttrs> map = this.lp2attrs;
        synchronized (map) {
            LPAttrs attrs = this.lp2attrs.get(languagePath);
            Object value = null;
            if (attrs != null && (value = attrs.getSpecific(attributeKey)) == null) {
                value = attrs.getGlobal(attributeKey);
            }
            while (value == null && languagePath.size() > 1) {
                attrs = this.lp2attrs.get(languagePath = languagePath.subPath(1));
                if (attrs == null) continue;
                value = attrs.getGlobal(attributeKey);
            }
            return value;
        }
    }

    public void setValue(Language<?> language, Object attributeKey, Object attributeValue, boolean global) {
        this.setValue(LanguagePath.get(language), attributeKey, attributeValue, global);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setValue(LanguagePath languagePath, Object attributeKey, Object attributeValue, boolean global) {
        this.checkAttributeKeyNotNull(attributeKey);
        Map<LanguagePath, LPAttrs> map = this.lp2attrs;
        synchronized (map) {
            LPAttrs attrs = this.lp2attrs.get(languagePath);
            if (attrs == null) {
                attrs = new LPAttrs();
                this.lp2attrs.put(languagePath, attrs);
            }
            if (global) {
                attrs.putGlobal(attributeKey, attributeValue);
            } else {
                attrs.putSpecific(attributeKey, attributeValue);
            }
        }
    }

    private void checkAttributeKeyNotNull(Object attributeKey) {
        if (attributeKey == null) {
            throw new IllegalArgumentException("attributeKey cannot be null");
        }
    }

    private static final class LPAttrs {
        private Map<Object, Object> specifics;
        private Map<Object, Object> globals;

        private LPAttrs() {
        }

        public Object getSpecific(Object key) {
            return this.specifics != null ? this.specifics.get(key) : null;
        }

        public Object getGlobal(Object key) {
            return this.globals != null ? this.globals.get(key) : null;
        }

        public void putSpecific(Object key, Object value) {
            if (this.specifics == null) {
                this.specifics = new HashMap<Object, Object>(4);
            }
            this.specifics.put(key, value);
        }

        public void putGlobal(Object key, Object value) {
            if (this.globals == null) {
                this.globals = new HashMap<Object, Object>(4);
            }
            this.globals.put(key, value);
        }
    }

}

