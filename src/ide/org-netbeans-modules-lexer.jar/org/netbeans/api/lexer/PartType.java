/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

public enum PartType {
    COMPLETE,
    START,
    MIDDLE,
    END;
    

    private PartType() {
    }
}

