/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.io.Reader;
import java.util.List;
import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchyListener;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.inc.DocumentInput;
import org.netbeans.spi.lexer.TokenHierarchyControl;

public final class TokenHierarchy<I> {
    private final TokenHierarchyOperation<I, ?> operation;

    public static <D extends Document> TokenHierarchy<D> get(D doc) {
        return DocumentInput.get(doc).tokenHierarchyControl().tokenHierarchy();
    }

    public static <I extends CharSequence> TokenHierarchy<I> create(I inputText, Language<?> language) {
        return TokenHierarchy.create(inputText, false, language, null, null);
    }

    public static <I extends CharSequence, T extends TokenId> TokenHierarchy<I> create(I inputText, boolean copyInputText, Language<T> language, Set<T> skipTokenIds, InputAttributes inputAttributes) {
        return new TokenHierarchyOperation((CharSequence)inputText, copyInputText, language, skipTokenIds, inputAttributes).tokenHierarchy();
    }

    public static <I extends Reader, T extends TokenId> TokenHierarchy<I> create(I inputReader, Language<T> language, Set<T> skipTokenIds, InputAttributes inputAttributes) {
        return new TokenHierarchyOperation((Reader)inputReader, language, skipTokenIds, inputAttributes).tokenHierarchy();
    }

    TokenHierarchy(TokenHierarchyOperation<I, ?> operation) {
        this.operation = operation;
    }

    public TokenSequence<?> tokenSequence() {
        return this.operation.tokenSequence();
    }

    public <T extends TokenId> TokenSequence<T> tokenSequence(Language<T> language) {
        TokenSequence ts = this.operation.tokenSequence(language);
        return ts;
    }

    public List<TokenSequence<?>> tokenSequenceList(LanguagePath languagePath, int startOffset, int endOffset) {
        return this.operation.tokenSequenceList(languagePath, startOffset, endOffset);
    }

    public List<TokenSequence<?>> embeddedTokenSequences(int offset, boolean backwardBias) {
        return this.operation.embeddedTokenSequences(offset, backwardBias);
    }

    public Set<LanguagePath> languagePaths() {
        return this.operation.languagePaths();
    }

    public boolean isMutable() {
        return this.operation.isMutable();
    }

    public I inputSource() {
        return this.operation.inputSource();
    }

    public boolean isActive() {
        return this.operation.isActive();
    }

    public void addTokenHierarchyListener(TokenHierarchyListener listener) {
        this.operation.addTokenHierarchyListener(listener);
    }

    public void removeTokenHierarchyListener(TokenHierarchyListener listener) {
        this.operation.removeTokenHierarchyListener(listener);
    }

    TokenHierarchyOperation<I, ?> operation() {
        return this.operation;
    }

    public String toString() {
        return this.operation.toString();
    }
}

