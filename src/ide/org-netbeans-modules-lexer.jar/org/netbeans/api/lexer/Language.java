/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.lexer;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.lexer.TokenChange;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenHierarchyEvent;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.lexer.LanguageManager;
import org.netbeans.lib.lexer.LanguageOperation;
import org.netbeans.lib.lexer.LexerApiPackageAccessor;
import org.netbeans.lib.lexer.LexerSpiPackageAccessor;
import org.netbeans.lib.lexer.TokenHierarchyOperation;
import org.netbeans.lib.lexer.TokenIdSet;
import org.netbeans.lib.lexer.TokenList;
import org.netbeans.lib.lexer.inc.TokenChangeInfo;
import org.netbeans.lib.lexer.inc.TokenHierarchyEventInfo;
import org.netbeans.spi.lexer.LanguageHierarchy;

public final class Language<T extends TokenId> {
    private static final List<Reference<Language<?>>> languageRefList;
    final int id;
    private final LanguageHierarchy<T> languageHierarchy;
    private final LanguageOperation<T> languageOperation;
    private final String mimeType;
    private final int maxOrdinal;
    private final Set<T> ids;
    private TokenIdSet<T> indexedIds;
    private final Map<String, T> idName2id;
    private final Map<String, Set<T>> cat2ids;
    private List<String>[] id2cats;
    private List<String>[] id2nonPrimaryCats;

    public static Language<? extends TokenId> find(String mimeType) {
        return mimeType != null ? LanguageManager.getInstance().findLanguage(mimeType) : null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Language(LanguageHierarchy<T> languageHierarchy) {
        int lid = 0;
        Class<Language> class_ = Language.class;
        synchronized (Language.class) {
            for (int i = 0; i < languageRefList.size(); ++i) {
                Reference langRef = languageRefList.get(i);
                if (langRef.get() != null) continue;
                lid = i + 1;
                languageRefList.set(i, new WeakReference<Language>(this));
                break;
            }
            if (lid == 0) {
                lid = languageRefList.size() + 1;
                languageRefList.add(new WeakReference<Language>(this));
            }
            // ** MonitorExit[var3_3] (shouldn't be in output)
            this.id = lid;
            this.languageHierarchy = languageHierarchy;
            this.languageOperation = new LanguageOperation<T>(languageHierarchy, this);
            this.mimeType = LexerSpiPackageAccessor.get().mimeType(languageHierarchy);
            Language.checkMimeTypeValid(this.mimeType);
            Collection<T> createdIds = LexerSpiPackageAccessor.get().createTokenIds(languageHierarchy);
            if (createdIds == null) {
                throw new IllegalArgumentException("Ids cannot be null");
            }
            this.maxOrdinal = TokenIdSet.findMaxOrdinal(createdIds);
            this.ids = createdIds instanceof EnumSet ? (Set)createdIds : new TokenIdSet<T>(createdIds, this.maxOrdinal, true);
            Map createdCat2ids = LexerSpiPackageAccessor.get().createTokenCategories(languageHierarchy);
            if (createdCat2ids == null) {
                createdCat2ids = Collections.emptyMap();
            }
            this.cat2ids = new HashMap<String, Set<T>>((int)((float)createdCat2ids.size() / 0.73f));
            for (Map.Entry entry : createdCat2ids.entrySet()) {
                Collection createdCatIds = (Collection)entry.getValue();
                TokenIdSet.checkIdsFromLanguage(createdCatIds, this.ids);
                TokenIdSet catIds = new TokenIdSet(createdCatIds, this.maxOrdinal, false);
                this.cat2ids.put((String)entry.getKey(), catIds);
            }
            this.idName2id = new HashMap<String, T>((int)((float)this.ids.size() / 0.73f));
            for (TokenId id : this.ids) {
                TokenId sameNameId = this.idName2id.put(id.name(), (TokenId)id);
                if (sameNameId != null && sameNameId != id) {
                    throw new IllegalArgumentException(id + " has duplicate name with " + sameNameId);
                }
                String cat = id.primaryCategory();
                if (cat == null) continue;
                Set<T> catIds = this.cat2ids.get(cat);
                if (catIds == null) {
                    catIds = new TokenIdSet(null, this.maxOrdinal, false);
                    this.cat2ids.put(cat, catIds);
                }
                catIds.add((TokenId)id);
            }
            return;
        }
    }

    public Set<T> tokenIds() {
        return this.ids;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public T tokenId(int ordinal) {
        Map<String, T> map = this.idName2id;
        synchronized (map) {
            if (this.indexedIds == null) {
                this.indexedIds = this.ids instanceof EnumSet ? new TokenIdSet<T>(this.ids, this.maxOrdinal, false) : (TokenIdSet<T>)this.ids;
            }
            return (T)this.indexedIds.indexedIds()[ordinal];
        }
    }

    public T validTokenId(int ordinal) {
        T id = this.tokenId(ordinal);
        if (id == null) {
            throw new IndexOutOfBoundsException("No tokenId for ordinal=" + ordinal + " in language " + this);
        }
        return id;
    }

    public T tokenId(String name) {
        return (T)((TokenId)this.idName2id.get(name));
    }

    public T validTokenId(String name) {
        T id = this.tokenId(name);
        if (id == null) {
            throw new IllegalArgumentException("No tokenId for name=\"" + name + "\" in language " + this);
        }
        return id;
    }

    public int maxOrdinal() {
        return this.maxOrdinal;
    }

    public Set<String> tokenCategories() {
        return Collections.unmodifiableSet(this.cat2ids.keySet());
    }

    public Set<T> tokenCategoryMembers(String tokenCategory) {
        return Collections.unmodifiableSet(this.cat2ids.get(tokenCategory));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<String> tokenCategories(T tokenId) {
        this.checkMemberId(tokenId);
        Map<String, T> map = this.idName2id;
        synchronized (map) {
            if (this.id2cats == null) {
                this.buildTokenIdCategories();
            }
            return this.id2cats[tokenId.ordinal()];
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<String> nonPrimaryTokenCategories(T tokenId) {
        this.checkMemberId(tokenId);
        Map<String, T> map = this.idName2id;
        synchronized (map) {
            if (this.id2nonPrimaryCats == null) {
                this.buildTokenIdCategories();
            }
            return this.id2nonPrimaryCats[tokenId.ordinal()];
        }
    }

    public Set<T> merge(Collection<T> tokenIds1, Collection<T> tokenIds2) {
        TokenIdSet.checkIdsFromLanguage(tokenIds1, this.ids);
        TokenIdSet<T> ret = new TokenIdSet<T>(tokenIds1, this.maxOrdinal, false);
        if (tokenIds2 != null) {
            TokenIdSet.checkIdsFromLanguage(tokenIds2, this.ids);
            ret.addAll(tokenIds2);
        }
        return ret;
    }

    public String mimeType() {
        return this.mimeType;
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public int hashCode() {
        return super.hashCode();
    }

    private void buildTokenIdCategories() {
        this.assignCatArrays();
        ArrayList<Map<String, Object>> catMapsList = new ArrayList<Map<String, Object>>(4);
        ArrayList<String> idCats = new ArrayList<String>(4);
        for (TokenId id : this.ids) {
            for (Map.Entry<String, Set<T>> e : this.cat2ids.entrySet()) {
                if (!e.getValue().contains(id)) continue;
                idCats.add(e.getKey());
            }
            this.id2cats[id.ordinal()] = Language.findCatList(catMapsList, idCats, 0);
            this.id2nonPrimaryCats[id.ordinal()] = Language.findCatList(catMapsList, idCats, 1);
            idCats.clear();
        }
    }

    private static List<String> findCatList(List<Map<String, Object>> catMapsList, List<String> idCats, int startIndex) {
        int size = idCats.size() - startIndex;
        if (size <= 0) {
            return Collections.emptyList();
        }
        while (catMapsList.size() < size) {
            catMapsList.add(new HashMap());
        }
        HashMap<String, Cloneable> m = catMapsList.get(--size);
        for (int i = startIndex; i < size; ++i) {
            HashMap<String, Cloneable> catMap = (HashMap<String, Cloneable>)m.get(idCats.get(i));
            if (catMap == null) {
                catMap = new HashMap<String, Cloneable>();
                m.put(idCats.get(i), catMap);
            }
            m = catMap;
        }
        ArrayList<String> catList = (ArrayList<String>)m.get(idCats.get(size));
        if (catList == null) {
            catList = new ArrayList<String>(idCats.size() - startIndex);
            catList.addAll(startIndex > 0 ? idCats.subList(startIndex, idCats.size()) : idCats);
            m.put(idCats.get(size), catList);
        }
        return catList;
    }

    private void assignCatArrays() {
        this.id2cats = new List[this.maxOrdinal + 1];
        this.id2nonPrimaryCats = new List[this.maxOrdinal + 1];
    }

    public String dumpInfo() {
        StringBuilder sb = new StringBuilder();
        for (TokenId id : this.ids) {
            sb.append(id);
            List<String> cats = this.tokenCategories(id);
            if (cats.size() <= 0) continue;
            sb.append(": ");
            for (int i = 0; i < cats.size(); ++i) {
                if (i > 0) {
                    sb.append(", ");
                }
                String cat = cats.get(i);
                sb.append('\"');
                sb.append(cat);
                sb.append('\"');
            }
        }
        return this.ids.toString();
    }

    public String toString() {
        return this.mimeType + ", LH: " + this.languageHierarchy;
    }

    private void checkMemberId(T id) {
        if (!this.ids.contains(id)) {
            throw new IllegalArgumentException(id + " does not belong to language " + this);
        }
    }

    private static void checkMimeTypeValid(String mimeType) {
        if (mimeType == null) {
            throw new IllegalStateException("mimeType cannot be null");
        }
        int slashIndex = mimeType.indexOf(47);
        if (slashIndex == -1) {
            throw new IllegalStateException("mimeType=" + mimeType + " does not contain '/'");
        }
        if (mimeType.indexOf(47, slashIndex + 1) != -1) {
            throw new IllegalStateException("mimeType=" + mimeType + " contains more than one '/'");
        }
    }

    LanguageHierarchy<T> languageHierarchy() {
        return this.languageHierarchy;
    }

    LanguageOperation<T> languageOperation() {
        return this.languageOperation;
    }

    static {
        LexerApiPackageAccessor.register(new Accessor());
        languageRefList = new ArrayList();
    }

    private static final class Accessor
    extends LexerApiPackageAccessor {
        private Accessor() {
        }

        @Override
        public <T extends TokenId> Language<T> createLanguage(LanguageHierarchy<T> languageHierarchy) {
            return new Language<T>(languageHierarchy);
        }

        @Override
        public <T extends TokenId> LanguageHierarchy<T> languageHierarchy(Language<T> language) {
            return language.languageHierarchy();
        }

        @Override
        public <T extends TokenId> LanguageOperation<T> languageOperation(Language<T> language) {
            return language.languageOperation();
        }

        @Override
        public int languageId(Language<?> language) {
            return language.id;
        }

        @Override
        public <I> TokenHierarchy<I> createTokenHierarchy(TokenHierarchyOperation<I, ?> tokenHierarchyOperation) {
            return new TokenHierarchy<I>(tokenHierarchyOperation);
        }

        @Override
        public TokenHierarchyEvent createTokenChangeEvent(TokenHierarchyEventInfo info) {
            return new TokenHierarchyEvent(info);
        }

        @Override
        public <T extends TokenId> TokenChange<T> createTokenChange(TokenChangeInfo<T> info) {
            return new TokenChange<T>(info);
        }

        @Override
        public <T extends TokenId> TokenChangeInfo<T> tokenChangeInfo(TokenChange<T> tokenChange) {
            return tokenChange.info();
        }

        @Override
        public <I> TokenHierarchyOperation<I, ?> tokenHierarchyOperation(TokenHierarchy<I> tokenHierarchy) {
            return tokenHierarchy.operation();
        }

        @Override
        public <T extends TokenId> TokenSequence<T> createTokenSequence(TokenList<T> tokenList) {
            return new TokenSequence<T>(tokenList);
        }
    }

}

