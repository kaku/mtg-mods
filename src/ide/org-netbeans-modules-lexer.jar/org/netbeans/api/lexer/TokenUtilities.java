/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 */
package org.netbeans.api.lexer;

import org.netbeans.lib.editor.util.CharSequenceUtilities;

public final class TokenUtilities {
    private TokenUtilities() {
    }

    public static boolean textEquals(CharSequence text1, CharSequence text2) {
        return CharSequenceUtilities.textEquals((CharSequence)text1, (CharSequence)text2);
    }

    public static boolean equals(CharSequence text, Object o) {
        return CharSequenceUtilities.equals((CharSequence)text, (Object)o);
    }

    public static int indexOf(CharSequence text, int ch) {
        return CharSequenceUtilities.indexOf((CharSequence)text, (int)ch);
    }

    public static int indexOf(CharSequence text, int ch, int fromIndex) {
        return CharSequenceUtilities.indexOf((CharSequence)text, (int)ch, (int)fromIndex);
    }

    public static int indexOf(CharSequence text, CharSequence seq) {
        return CharSequenceUtilities.indexOf((CharSequence)text, (CharSequence)seq);
    }

    public static int indexOf(CharSequence text, CharSequence seq, int fromIndex) {
        return CharSequenceUtilities.indexOf((CharSequence)text, (CharSequence)seq, (int)fromIndex);
    }

    public static int lastIndexOf(CharSequence text, CharSequence seq) {
        return CharSequenceUtilities.lastIndexOf((CharSequence)text, (CharSequence)seq);
    }

    public static int lastIndexOf(CharSequence text, CharSequence seq, int fromIndex) {
        return CharSequenceUtilities.lastIndexOf((CharSequence)text, (CharSequence)seq, (int)fromIndex);
    }

    public static int lastIndexOf(CharSequence text, int ch) {
        return CharSequenceUtilities.lastIndexOf((CharSequence)text, (int)ch);
    }

    public static int lastIndexOf(CharSequence text, int ch, int fromIndex) {
        return CharSequenceUtilities.lastIndexOf((CharSequence)text, (int)ch, (int)fromIndex);
    }

    public static boolean startsWith(CharSequence text, CharSequence prefix) {
        return CharSequenceUtilities.startsWith((CharSequence)text, (CharSequence)prefix);
    }

    public static boolean endsWith(CharSequence text, CharSequence suffix) {
        return CharSequenceUtilities.endsWith((CharSequence)text, (CharSequence)suffix);
    }

    public static CharSequence trim(CharSequence text) {
        return CharSequenceUtilities.trim((CharSequence)text);
    }

    public static String debugText(CharSequence text) {
        return CharSequenceUtilities.debugText((CharSequence)text);
    }
}

