/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.generaleditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import org.netbeans.modules.options.generaleditor.Model;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class GeneralEditorPanel
extends JPanel
implements ActionListener {
    private boolean changed = false;
    private boolean listen = false;
    private JCheckBox cbBraceTooltip;
    private JCheckBox cbCamelCaseBehavior;
    private JCheckBox cbShowBraceOutline;
    private JComboBox<String> cboEditorSearchType;
    private Box.Filler filler1;
    private JSeparator jSeparator3;
    private JSeparator jSeparator5;
    private JSeparator jSeparator6;
    private JLabel lBracesMatching;
    private JLabel lCamelCaseBehavior;
    private JLabel lCamelCaseBehaviorExample;
    private JLabel lEditorSearchType;
    private JLabel lSearch;
    private JLabel lSearchtypeTooltip;
    private Model model;

    public GeneralEditorPanel() {
        this.initComponents();
        GeneralEditorPanel.loc(this.lCamelCaseBehavior, "Camel_Case_Behavior");
        GeneralEditorPanel.loc(this.cbCamelCaseBehavior, "Enable_Camel_Case_In_Java");
        GeneralEditorPanel.loc(this.lCamelCaseBehaviorExample, "Camel_Case_Behavior_Example");
        GeneralEditorPanel.loc(this.lSearch, "Search");
        GeneralEditorPanel.loc(this.lEditorSearchType, "Editor_Search_Type");
        GeneralEditorPanel.loc(this.cboEditorSearchType, "Editor_Search_Type");
        GeneralEditorPanel.loc(this.cbBraceTooltip, "Brace_First_Tooltip");
        GeneralEditorPanel.loc(this.cbShowBraceOutline, "Brace_Show_Outline");
        this.cboEditorSearchType.setRenderer(new EditorSearchTypeRenderer(this.cboEditorSearchType.getRenderer()));
        this.cboEditorSearchType.setModel(new DefaultComboBoxModel<String>(new String[]{"default", "closing"}));
        this.cboEditorSearchType.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (GeneralEditorPanel.this.cboEditorSearchType.getSelectedItem().equals("default")) {
                    Mnemonics.setLocalizedText((JLabel)GeneralEditorPanel.this.lSearchtypeTooltip, (String)NbBundle.getMessage(GeneralEditorPanel.class, (String)"Editor_Search_Type_Tooltip_default"));
                } else {
                    Mnemonics.setLocalizedText((JLabel)GeneralEditorPanel.this.lSearchtypeTooltip, (String)NbBundle.getMessage(GeneralEditorPanel.class, (String)"Editor_Search_Type_Tooltip_closing"));
                }
            }
        });
        Mnemonics.setLocalizedText((JLabel)this.lSearchtypeTooltip, (String)NbBundle.getMessage(GeneralEditorPanel.class, (String)"Editor_Search_Type_Tooltip_closing"));
    }

    private void initComponents() {
        this.lBracesMatching = new JLabel();
        this.cbShowBraceOutline = new JCheckBox();
        this.cbBraceTooltip = new JCheckBox();
        this.jSeparator6 = new JSeparator();
        this.lCamelCaseBehavior = new JLabel();
        this.jSeparator3 = new JSeparator();
        this.cbCamelCaseBehavior = new JCheckBox();
        this.lCamelCaseBehaviorExample = new JLabel();
        this.lEditorSearchType = new JLabel();
        this.jSeparator5 = new JSeparator();
        this.lSearch = new JLabel();
        this.cboEditorSearchType = new JComboBox();
        this.lSearchtypeTooltip = new JLabel();
        this.filler1 = new Box.Filler(new Dimension(0, 0), new Dimension(0, 0), new Dimension(32767, 32767));
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setForeground(new Color(99, 130, 191));
        this.setLayout(new GridBagLayout());
        this.lBracesMatching.setText(NbBundle.getMessage(GeneralEditorPanel.class, (String)"BRACES_MATCHING"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        this.add((Component)this.lBracesMatching, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.cbShowBraceOutline, (String)"Sho&w outline");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(5, 15, 0, 0);
        this.add((Component)this.cbShowBraceOutline, gridBagConstraints);
        this.cbShowBraceOutline.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AN_Brace_Show_Outline"));
        this.cbShowBraceOutline.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AD_Brace_Show_Outline"));
        Mnemonics.setLocalizedText((AbstractButton)this.cbBraceTooltip, (String)"Show toolti&ps for invisible lines");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(2, 15, 18, 0);
        this.add((Component)this.cbBraceTooltip, gridBagConstraints);
        this.cbBraceTooltip.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AN_Brace_First_Tooltip"));
        this.cbBraceTooltip.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AD_Brace_First_Tooltip"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.weightx = 0.1;
        this.add((Component)this.jSeparator6, gridBagConstraints);
        this.lCamelCaseBehavior.setText("Camel Case  Behavior");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        this.add((Component)this.lCamelCaseBehavior, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.weightx = 0.1;
        this.add((Component)this.jSeparator3, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.cbCamelCaseBehavior, (String)"&Enable Camel Case Navigation");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(5, 15, 0, 0);
        this.add((Component)this.cbCamelCaseBehavior, gridBagConstraints);
        this.cbCamelCaseBehavior.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AN_Camel_Case_Navigation"));
        this.cbCamelCaseBehavior.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AD_Camel_Case_Navigation"));
        this.lCamelCaseBehaviorExample.setText("<html>Example: Caret stops at J, T, N in \"JavaTypeName\"<br>when using next/previous word acctions</html>");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(2, 36, 18, 0);
        this.add((Component)this.lCamelCaseBehaviorExample, gridBagConstraints);
        this.lEditorSearchType.setLabelFor(this.cboEditorSearchType);
        Mnemonics.setLocalizedText((JLabel)this.lEditorSearchType, (String)"Editor &Search Type:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.insets = new Insets(5, 15, 0, 5);
        this.add((Component)this.lEditorSearchType, gridBagConstraints);
        this.lEditorSearchType.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AN_Editor_Search_Type"));
        this.lEditorSearchType.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralEditorPanel.class, (String)"AD_Editor_Search_Type"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.weightx = 0.1;
        this.add((Component)this.jSeparator5, gridBagConstraints);
        this.lSearch.setText("Search");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.insets = new Insets(0, 0, 0, 5);
        this.add((Component)this.lSearch, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 1280;
        gridBagConstraints.insets = new Insets(3, 1, 0, 0);
        this.add(this.cboEditorSearchType, gridBagConstraints);
        this.lSearchtypeTooltip.setText("<html>In Closing type Enter accepts search match, Esc jumps to start. Both close searchbar. <br /> Default type closes searchbar by Esc or button. Enter means find a new instance.</html>");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new Insets(6, 1, 12, 0);
        this.add((Component)this.lSearchtypeTooltip, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = 3;
        gridBagConstraints.weighty = 0.1;
        this.add((Component)this.filler1, gridBagConstraints);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(GeneralEditorPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (!(c instanceof JLabel)) {
            c.getAccessibleContext().setAccessibleName(GeneralEditorPanel.loc("AN_" + key));
            c.getAccessibleContext().setAccessibleDescription(GeneralEditorPanel.loc("AD_" + key));
        }
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)GeneralEditorPanel.loc("CTL_" + key));
        } else if (c instanceof JLabel) {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)GeneralEditorPanel.loc("CTL_" + key));
        }
    }

    void update() {
        Boolean ccJava;
        this.listen = false;
        if (this.model == null) {
            this.model = new Model();
            this.cbCamelCaseBehavior.addActionListener(this);
            this.cboEditorSearchType.addActionListener(this);
            this.cbBraceTooltip.addActionListener(this);
            this.cbShowBraceOutline.addActionListener(this);
        }
        if ((ccJava = this.model.isCamelCaseJavaNavigation()) == null) {
            this.cbCamelCaseBehavior.setEnabled(false);
            this.cbCamelCaseBehavior.setSelected(false);
        } else {
            this.cbCamelCaseBehavior.setEnabled(true);
            this.cbCamelCaseBehavior.setSelected(ccJava);
        }
        this.cboEditorSearchType.setSelectedItem(this.model.getEditorSearchType());
        this.cbBraceTooltip.setSelected(this.model.isBraceTooltip());
        this.cbShowBraceOutline.setSelected(this.model.isBraceOutline());
        this.listen = true;
        this.changed = false;
    }

    void applyChanges() {
        if (this.model == null || !this.changed) {
            return;
        }
        this.model.setCamelCaseNavigation(this.cbCamelCaseBehavior.isSelected());
        this.model.setEditorSearchType((String)this.cboEditorSearchType.getSelectedItem());
        this.model.setBraceOutline(this.cbShowBraceOutline.isSelected());
        this.model.setBraceTooltip(this.cbBraceTooltip.isSelected());
        this.changed = false;
    }

    void cancel() {
        this.changed = false;
    }

    boolean dataValid() {
        return true;
    }

    boolean isChanged() {
        return this.changed;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!this.listen) {
            return;
        }
        this.changed = this.model.isCamelCaseJavaNavigation().booleanValue() != this.cbCamelCaseBehavior.isSelected() || !this.model.getEditorSearchType().equals((String)this.cboEditorSearchType.getSelectedItem()) || this.model.isBraceOutline().booleanValue() != this.cbShowBraceOutline.isSelected() || this.model.isBraceTooltip().booleanValue() != this.cbBraceTooltip.isSelected();
    }

    private static final class EditorSearchTypeRenderer
    implements ListCellRenderer {
        private final ListCellRenderer defaultRenderer;

        public EditorSearchTypeRenderer(ListCellRenderer defaultRenderer) {
            this.defaultRenderer = defaultRenderer;
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            return this.defaultRenderer.getListCellRendererComponent(list, NbBundle.getMessage(GeneralEditorPanel.class, (String)("EST_" + value)), index, isSelected, cellHasFocus);
        }
    }

}

