/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.generaleditor;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String KW_General_Editor() {
        return NbBundle.getMessage(Bundle.class, (String)"KW_General_Editor");
    }

    private void Bundle() {
    }
}

