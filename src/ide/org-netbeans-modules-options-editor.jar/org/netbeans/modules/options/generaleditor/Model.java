/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.options.generaleditor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class Model {
    private static final List<String> PRIVILEDGED_MIME_TYPES = Arrays.asList("text/x-java", "text/x-c++", "text/x-c", "text/x-ruby", "text/x-php5");

    boolean isShowCodeFolding() {
        return this.getParameter("code-folding-enable", true);
    }

    boolean isFoldImports() {
        return this.getParameter("code-folding-collapse-import", false);
    }

    boolean isFoldInitialComment() {
        return this.getParameter("code-folding-collapse-initial-comment", false);
    }

    boolean isFoldTag() {
        return this.getParameter("code-folding-collapse-tags", false);
    }

    boolean isFoldInnerClasses() {
        return this.getParameter("code-folding-collapse-innerclass", false);
    }

    boolean isFoldJavaDocComments() {
        return this.getParameter("code-folding-collapse-javadoc", false);
    }

    boolean isFoldMethods() {
        return this.getParameter("code-folding-collapse-method", false);
    }

    void setFoldingOptions(boolean showCodeFolding, boolean foldImports, boolean foldInitialComent, boolean foldInnerClasses, boolean foldJavaDoc, boolean foldMethods, boolean foldTags) {
        ArrayList<String> mimeTypes = new ArrayList<String>(EditorSettings.getDefault().getAllMimeTypes());
        mimeTypes.add("");
        for (String mimeType : mimeTypes) {
            Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
            prefs.putBoolean("code-folding-enable", showCodeFolding);
            prefs.putBoolean("code-folding-collapse-import", foldImports);
            prefs.putBoolean("code-folding-collapse-initial-comment", foldInitialComent);
            prefs.putBoolean("code-folding-collapse-innerclass", foldInnerClasses);
            prefs.putBoolean("code-folding-collapse-javadoc", foldJavaDoc);
            prefs.putBoolean("code-folding-collapse-method", foldMethods);
            prefs.putBoolean("code-folding-collapse-tags", foldTags);
        }
    }

    Boolean isCamelCaseJavaNavigation() {
        Preferences p = NbPreferences.root();
        if (p == null) {
            return null;
        }
        return p.getBoolean("useCamelCaseStyleNavigation", true) ? Boolean.TRUE : Boolean.FALSE;
    }

    void setCamelCaseNavigation(boolean value) {
        NbPreferences.root().putBoolean("useCamelCaseStyleNavigation", value);
    }

    Boolean isBraceOutline() {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        return prefs.getBoolean("editor-brace-outline", true);
    }

    Boolean isBraceTooltip() {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        return prefs.getBoolean("editor-brace-first-tooltip", true);
    }

    void setBraceOutline(Boolean show) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        prefs.putBoolean("editor-brace-outline", show);
    }

    void setBraceTooltip(Boolean show) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        prefs.putBoolean("editor-brace-first-tooltip", show);
    }

    String getEditorSearchType() {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        return prefs.get("editor-search-type", "default");
    }

    void setEditorSearchType(String value) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        prefs.put("editor-search-type", value);
    }

    private boolean getParameter(String parameterName, boolean defaultValue) {
        Set mimeTypes = EditorSettings.getDefault().getAllMimeTypes();
        ArrayList<String> list = new ArrayList<String>(PRIVILEDGED_MIME_TYPES);
        list.addAll(mimeTypes);
        for (String mimeType : list) {
            Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
            String value = prefs.get(parameterName, null);
            if (value == null) continue;
            return prefs.getBoolean(parameterName, false);
        }
        return defaultValue;
    }

    private Preferences getJavaModulePreferenes() {
        try {
            ClassLoader cl = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            Class accpClass = cl.loadClass("org.netbeans.modules.editor.java.AbstractCamelCasePosition");
            if (accpClass == null) {
                return null;
            }
            return NbPreferences.forModule(accpClass);
        }
        catch (ClassNotFoundException ex) {
            return null;
        }
    }
}

