/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.options.generaleditor;

import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import org.netbeans.modules.options.generaleditor.GeneralEditorPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class GeneralEditorPanelController
extends OptionsPanelController {
    private GeneralEditorPanel generalEditorPanel;

    public void update() {
        this.getGeneralEditorPanel().update();
    }

    public void applyChanges() {
        this.getGeneralEditorPanel().applyChanges();
    }

    public void cancel() {
        this.getGeneralEditorPanel().cancel();
    }

    public boolean isValid() {
        return this.getGeneralEditorPanel().dataValid();
    }

    public boolean isChanged() {
        return this.getGeneralEditorPanel().isChanged();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.editor.general");
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getGeneralEditorPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.getGeneralEditorPanel().addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.getGeneralEditorPanel().removePropertyChangeListener(l);
    }

    private GeneralEditorPanel getGeneralEditorPanel() {
        if (this.generalEditorPanel == null) {
            this.generalEditorPanel = new GeneralEditorPanel();
        }
        return this.generalEditorPanel;
    }
}

