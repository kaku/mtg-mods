/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.indentation;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.netbeans.modules.options.indentation.CustomizerSelector;
import org.netbeans.modules.options.indentation.IndentationPanel;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public final class IndentationPanelController
implements PreferencesCustomizer,
PreviewProvider {
    private static final Logger LOG = Logger.getLogger(IndentationPanelController.class.getName());
    private final MimePath mimePath;
    private final CustomizerSelector.PreferencesFactory prefsFactory;
    private final Preferences allLanguagesPreferences;
    private final Preferences preferences;
    private final PreferencesCustomizer delegate;
    private JComponent indentationPanel;

    public IndentationPanelController(Preferences prefs) {
        this(MimePath.EMPTY, null, prefs, null, null);
    }

    public IndentationPanelController(MimePath mimePath, CustomizerSelector.PreferencesFactory prefsFactory, Preferences prefs, Preferences allLangPrefs, PreferencesCustomizer delegate) {
        assert (mimePath != null);
        assert (prefs != null);
        assert (allLangPrefs == null && delegate == null || allLangPrefs != null && delegate != null);
        assert (delegate == null || delegate instanceof PreviewProvider);
        this.mimePath = mimePath;
        this.prefsFactory = prefsFactory;
        this.preferences = prefs;
        this.allLanguagesPreferences = allLangPrefs;
        this.delegate = delegate;
    }

    @Override
    public JComponent getComponent() {
        if (this.indentationPanel == null) {
            if (this.delegate != null) {
                this.indentationPanel = new JPanel();
                this.indentationPanel.setLayout(new BoxLayout(this.indentationPanel, 1));
                JComponent delegateComp = this.delegate.getComponent();
                this.indentationPanel.setName(delegateComp.getName());
                this.indentationPanel.add(new IndentationPanel(this.mimePath, this.prefsFactory, this.preferences, this.allLanguagesPreferences, (PreviewProvider)((Object)this.delegate)));
                this.indentationPanel.add(delegateComp);
                JPanel spacer = new JPanel();
                spacer.setPreferredSize(new Dimension(10, Integer.MAX_VALUE));
                this.indentationPanel.add(spacer);
            } else {
                this.indentationPanel = new IndentationPanel(this.mimePath, this.prefsFactory, this.preferences, null, null);
            }
        }
        return this.indentationPanel;
    }

    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(IndentationPanelController.class, (String)"indentation-customizer-display-name");
    }

    @Override
    public String getId() {
        return "tabs-and-indents";
    }

    @Override
    public HelpCtx getHelpCtx() {
        HelpCtx ctx = null;
        if (this.delegate != null) {
            ctx = this.delegate.getHelpCtx();
        }
        return ctx != null ? ctx : new HelpCtx("netbeans.optionsDialog.editor.identation");
    }

    @Override
    public JComponent getPreviewComponent() {
        if (this.delegate != null) {
            return ((PreviewProvider)((Object)this.delegate)).getPreviewComponent();
        }
        return this.getIndentationPanel().getPreviewProvider().getPreviewComponent();
    }

    @Override
    public void refreshPreview() {
        try {
            if (this.delegate != null) {
                ((PreviewProvider)((Object)this.delegate)).refreshPreview();
            } else {
                this.getIndentationPanel().scheduleRefresh();
            }
        }
        catch (ThreadDeath td) {
            throw td;
        }
        catch (Throwable t) {
            // empty catch block
        }
    }

    private IndentationPanel getIndentationPanel() {
        assert (this.indentationPanel instanceof IndentationPanel);
        return (IndentationPanel)this.indentationPanel;
    }
}

