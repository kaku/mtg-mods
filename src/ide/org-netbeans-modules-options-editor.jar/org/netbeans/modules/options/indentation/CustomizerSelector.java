/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.options.indentation;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.netbeans.modules.options.indentation.IndentationPanelController;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class CustomizerSelector {
    public static final String FORMATTING_CUSTOMIZERS_FOLDER = "OptionsDialog/Editor/Formatting/";
    public static final String PROP_MIMETYPE = "CustomizerSelector.PROP_MIMETYPE";
    public static final String PROP_CUSTOMIZER = "CustomizerSelector.PROP_CUSTOMIZER";
    private static final Logger LOG = Logger.getLogger(CustomizerSelector.class.getName());
    private final PropertyChangeSupport pcs;
    private final PreferencesFactory pf;
    private final boolean acceptOldControllers;
    private final Set<String> allowedMimeTypes;
    private String selectedMimeType;
    private String selectedCustomizerId;
    private Set<String> mimeTypes;
    private final Map<String, List<? extends PreferencesCustomizer>> allCustomizers;
    private final Map<PreferencesCustomizer, Preferences> c2p;

    public CustomizerSelector(PreferencesFactory pf, boolean acceptOldControllers, String allowedMimeTypes) {
        this.pcs = new PropertyChangeSupport(this);
        this.mimeTypes = null;
        this.allCustomizers = new HashMap<String, List<? extends PreferencesCustomizer>>();
        this.c2p = new HashMap<PreferencesCustomizer, Preferences>();
        this.pf = pf;
        this.acceptOldControllers = acceptOldControllers;
        if (allowedMimeTypes != null) {
            this.allowedMimeTypes = new HashSet<String>();
            for (String mimeType : allowedMimeTypes.split(",")) {
                if (MimePath.validate((CharSequence)(mimeType = mimeType.trim()))) {
                    this.allowedMimeTypes.add(mimeType);
                    continue;
                }
                LOG.warning("Ignoring invalid mimetype '" + mimeType + "'");
            }
        } else {
            this.allowedMimeTypes = null;
        }
    }

    public synchronized String getSelectedMimeType() {
        return this.selectedMimeType;
    }

    public synchronized void setSelectedMimeType(String mimeType) {
        assert (this.getMimeTypes().contains(mimeType));
        if (this.selectedMimeType == null || !this.selectedMimeType.equals(mimeType)) {
            String old = this.selectedMimeType;
            this.selectedMimeType = mimeType;
            this.selectedCustomizerId = null;
            this.pcs.firePropertyChange("CustomizerSelector.PROP_MIMETYPE", old, mimeType);
        }
    }

    public synchronized PreferencesCustomizer getSelectedCustomizer() {
        if (this.selectedCustomizerId != null) {
            for (PreferencesCustomizer c : this.getCustomizersFor(this.selectedMimeType)) {
                if (!this.selectedCustomizerId.equals(c.getId())) continue;
                return c;
            }
        }
        return null;
    }

    public synchronized void setSelectedCustomizer(String id) {
        if (this.selectedCustomizerId == null || !this.selectedCustomizerId.equals(id)) {
            for (PreferencesCustomizer c : this.getCustomizersFor(this.selectedMimeType)) {
                if (!id.equals(c.getId())) continue;
                String old = this.selectedCustomizerId;
                this.selectedCustomizerId = id;
                this.pcs.firePropertyChange("CustomizerSelector.PROP_CUSTOMIZER", old, id);
                break;
            }
        }
    }

    public synchronized Preferences getCustomizerPreferences(PreferencesCustomizer c) {
        Preferences prefs = this.c2p.get(c);
        assert (prefs != null);
        return prefs;
    }

    public synchronized Collection<? extends String> getMimeTypes() {
        if (this.mimeTypes == null) {
            this.mimeTypes = new HashSet<String>();
            this.mimeTypes.add("");
            for (String mimeType : EditorSettings.getDefault().getAllMimeTypes()) {
                Collection controllers;
                Lookup l = Lookups.forPath((String)("OptionsDialog/Editor/Formatting/" + mimeType));
                Collection factories = l.lookupAll(PreferencesCustomizer.Factory.class);
                if (!factories.isEmpty()) {
                    if (this.allowedMimeTypes != null && !this.allowedMimeTypes.contains(mimeType)) continue;
                    this.mimeTypes.add(mimeType);
                    continue;
                }
                if (!this.acceptOldControllers || (controllers = l.lookupAll(OptionsPanelController.class)).isEmpty() || this.allowedMimeTypes != null && !this.allowedMimeTypes.contains(mimeType)) continue;
                this.mimeTypes.add(mimeType);
            }
        }
        return this.mimeTypes;
    }

    public synchronized List<? extends PreferencesCustomizer> getCustomizers(String mimeType) {
        return this.getCustomizersFor(mimeType);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private List<? extends PreferencesCustomizer> getCustomizersFor(String mimeType) {
        List<? extends PreferencesCustomizer> list = this.allCustomizers.get(mimeType);
        if (list == null) {
            list = this.loadCustomizers(mimeType);
            this.allCustomizers.put(mimeType, list);
        }
        return list;
    }

    private List<? extends PreferencesCustomizer> loadCustomizers(String mimeType) {
        ArrayList<PreferencesCustomizer> list = new ArrayList<PreferencesCustomizer>();
        Preferences prefs = this.pf.getPreferences(mimeType);
        if (mimeType.length() > 0) {
            Lookup l = Lookups.forPath((String)("OptionsDialog/Editor/Formatting/" + mimeType));
            Collection factories = l.lookupAll(PreferencesCustomizer.Factory.class);
            for (PreferencesCustomizer.Factory f : factories) {
                PreferencesCustomizer c = f.create(prefs);
                if (c == null) continue;
                if (c.getId().equals("tabs-and-indents")) {
                    Preferences allLangPrefs = this.pf.getPreferences("");
                    c = new IndentationPanelController(MimePath.parse((String)mimeType), this.pf, prefs, allLangPrefs, c);
                }
                list.add(c);
                this.c2p.put(c, prefs);
            }
            if (this.acceptOldControllers) {
                Collection controllers = l.lookupAll(OptionsPanelController.class);
                for (OptionsPanelController controller : controllers) {
                    WrapperCustomizer c = controller instanceof PreviewProvider ? new WrapperCustomizerWithPreview(controller) : new WrapperCustomizer(controller);
                    list.add(c);
                    this.c2p.put(c, prefs);
                }
            }
        } else {
            IndentationPanelController c = new IndentationPanelController(prefs);
            list.add(c);
            this.c2p.put(c, prefs);
        }
        return list;
    }

    private static final class WrapperCustomizerWithPreview
    extends WrapperCustomizer
    implements PreviewProvider {
        private final PreviewProvider provider;

        public WrapperCustomizerWithPreview(OptionsPanelController controller) {
            super(controller);
            this.provider = (PreviewProvider)controller;
        }

        @Override
        public JComponent getPreviewComponent() {
            return this.provider.getPreviewComponent();
        }

        @Override
        public void refreshPreview() {
            this.provider.refreshPreview();
        }
    }

    static class WrapperCustomizer
    implements PreferencesCustomizer {
        private final OptionsPanelController controller;
        private JComponent component;

        public WrapperCustomizer(OptionsPanelController controller) {
            this.controller = controller;
        }

        @Override
        public String getId() {
            return this.controller.getClass() + "@" + Integer.toHexString(System.identityHashCode((Object)this.controller));
        }

        @Override
        public String getDisplayName() {
            return this.getComponent().getName();
        }

        @Override
        public HelpCtx getHelpCtx() {
            return this.controller.getHelpCtx();
        }

        @Override
        public JComponent getComponent() {
            if (this.component == null) {
                this.component = this.controller.getComponent(Lookup.EMPTY);
                this.controller.update();
            }
            return this.component;
        }

        public void applyChanges() {
            this.controller.applyChanges();
        }

        public void cancel() {
            this.controller.cancel();
        }

        boolean isChanged() {
            return this.controller.isChanged();
        }
    }

    public static interface PreferencesFactory {
        public Preferences getPreferences(String var1);

        public boolean isKeyOverridenForMimeType(String var1, String var2);
    }

}

