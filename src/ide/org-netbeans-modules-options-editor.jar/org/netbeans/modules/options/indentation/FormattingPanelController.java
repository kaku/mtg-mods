/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage
 *  org.netbeans.modules.editor.settings.storage.spi.TypedValue
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.indentation;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeEvent;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.indentation.CustomizerSelector;
import org.netbeans.modules.options.indentation.FormattingPanel;
import org.netbeans.modules.options.indentation.ProxyPreferences;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class FormattingPanelController
extends OptionsPanelController {
    public static final String OVERRIDE_GLOBAL_FORMATTING_OPTIONS = "FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS";
    private static final Logger LOG = Logger.getLogger(FormattingPanelController.class.getName());
    private static String[] BASIC_SETTINGS_NAMES = new String[]{"expand-tabs", "indent-shift-width", "spaces-per-tab", "tab-size", "text-limit-width", "text-line-wrap"};
    private final PropertyChangeSupport pcs;
    private MimeLookupPreferencesFactory pf;
    private CustomizerSelector selector;
    private FormattingPanel panel;
    private boolean changed;

    public FormattingPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
        this.changed = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void update() {
        boolean fire;
        FormattingPanelController formattingPanelController = this;
        synchronized (formattingPanelController) {
            LOG.fine("update");
            if (this.pf != null) {
                this.pf.destroy();
            }
            this.pf = new MimeLookupPreferencesFactory(new Callable(){

                public Object call() {
                    FormattingPanelController.this.notifyChanged(true);
                    return null;
                }
            });
            this.selector = new CustomizerSelector(this.pf, true, null);
            this.panel.setSelector(this.selector);
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void applyChanges() {
        boolean fire;
        FormattingPanelController formattingPanelController = this;
        synchronized (formattingPanelController) {
            LOG.fine("applyChanges");
            this.pf.applyChanges();
            for (String mimeType : this.pf.getAccessedMimeTypes()) {
                for (PreferencesCustomizer c : this.selector.getCustomizers(mimeType)) {
                    if (!(c instanceof CustomizerSelector.WrapperCustomizer)) continue;
                    ((CustomizerSelector.WrapperCustomizer)c).applyChanges();
                }
            }
            HashSet mimeTypes = new HashSet(EditorSettings.getDefault().getAllMimeTypes());
            mimeTypes.removeAll(this.selector.getMimeTypes());
            for (String mimeType2 : mimeTypes) {
                Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType2).lookup(Preferences.class);
                EditorSettingsStorage storage = EditorSettingsStorage.get((String)"Preferences");
                for (String key : BASIC_SETTINGS_NAMES) {
                    try {
                        Map mimePathLocalPrefs = storage.load(MimePath.parse((String)mimeType2), null, false);
                        Map moduleMimePathLocalPrefs = storage.load(MimePath.parse((String)mimeType2), null, true);
                        if (!mimePathLocalPrefs.containsKey(key) && !moduleMimePathLocalPrefs.containsKey(key)) continue;
                        TypedValue value = (TypedValue)moduleMimePathLocalPrefs.get(key);
                        if (value != null) {
                            if (value.getJavaType().equals(Integer.class.getName())) {
                                prefs.putInt(key, Integer.parseInt(value.getValue()));
                                continue;
                            }
                            if (value.getJavaType().equals(Boolean.class.getName())) {
                                prefs.putBoolean(key, Boolean.parseBoolean(value.getValue()));
                                continue;
                            }
                            prefs.put(key, value.getValue());
                            continue;
                        }
                        prefs.remove(key);
                        continue;
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.WARNING, null, ioe);
                    }
                }
            }
            this.pf.destroy();
            this.pf = null;
            this.panel.setSelector(null);
            this.selector = null;
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                JTextComponent lastFocused = EditorRegistry.lastFocusedComponent();
                if (lastFocused != null) {
                    lastFocused.getDocument().putProperty("text-line-wrap", "");
                    lastFocused.getDocument().putProperty("tab-size", "");
                    lastFocused.getDocument().putProperty("text-limit-width", "");
                }
                for (JTextComponent jtc : EditorRegistry.componentList()) {
                    if (lastFocused != null && lastFocused == jtc) continue;
                    jtc.getDocument().putProperty("text-line-wrap", "");
                    jtc.getDocument().putProperty("tab-size", "");
                    jtc.getDocument().putProperty("text-limit-width", "");
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        boolean fire;
        FormattingPanelController formattingPanelController = this;
        synchronized (formattingPanelController) {
            LOG.fine("cancel");
            for (String mimeType : this.pf.getAccessedMimeTypes()) {
                for (PreferencesCustomizer c : this.selector.getCustomizers(mimeType)) {
                    if (!(c instanceof CustomizerSelector.WrapperCustomizer)) continue;
                    ((CustomizerSelector.WrapperCustomizer)c).cancel();
                }
            }
            this.pf.destroy();
            this.pf = null;
            this.panel.setSelector(null);
            this.selector = null;
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.changed || this.areCNDPrefsChanged();
    }

    private boolean areCNDPrefsChanged() {
        boolean isChanged = false;
        if (this.pf == null || this.selector == null) {
            return isChanged;
        }
        for (String mimeType : this.pf.getAccessedMimeTypes()) {
            for (PreferencesCustomizer c : this.selector.getCustomizers(mimeType)) {
                if (!(c instanceof CustomizerSelector.WrapperCustomizer) || !(isChanged |= ((CustomizerSelector.WrapperCustomizer)c).isChanged())) continue;
                return true;
            }
        }
        return isChanged;
    }

    private void firePrefsChanged() {
        boolean isChanged = false;
        for (String mimeType : this.pf.getAccessedMimeTypes()) {
            for (PreferencesCustomizer c : this.selector.getCustomizers(mimeType)) {
                if (c instanceof CustomizerSelector.WrapperCustomizer) {
                    isChanged |= ((CustomizerSelector.WrapperCustomizer)c).isChanged();
                    continue;
                }
                if (!(isChanged |= this.arePrefsChanged(mimeType, c))) continue;
                this.changed = true;
                return;
            }
        }
        this.changed = isChanged;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    private boolean arePrefsChanged(String mimeType, PreferencesCustomizer c) {
        isChanged = false;
        prefs = this.selector.getCustomizerPreferences(c);
        savedPrefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        hashSet = new HashSet<String>();
        try {
            hashSet.addAll(Arrays.asList(prefs.keys()));
            hashSet.addAll(Arrays.asList(savedPrefs.keys()));
        }
        catch (BackingStoreException ex) {
            return false;
        }
        i$ = hashSet.iterator();
        while (i$.hasNext() != false) {
            key = (String)i$.next();
            if (key.equals("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS")) continue;
            if (prefs.get(key, null) != null) ** GOTO lbl18
            if (savedPrefs.get(key, null) == null) ** GOTO lbl19
            ** GOTO lbl-1000
lbl18: // 1 sources:
            if (!prefs.get(key, null).equals(savedPrefs.get(key, null))) ** GOTO lbl-1000
lbl19: // 2 sources:
            if (prefs.get(key, null) == null ? savedPrefs.get(key, null) != null : prefs.get(key, null).equals(savedPrefs.get(key, null)) == false) lbl-1000: // 3 sources:
            {
                v0 = true;
            } else {
                v0 = false;
            }
            isChanged |= v0;
        }
        return isChanged;
    }

    public HelpCtx getHelpCtx() {
        PreferencesCustomizer c = this.selector == null ? null : this.selector.getSelectedCustomizer();
        HelpCtx ctx = c == null ? null : c.getHelpCtx();
        return ctx != null ? ctx : new HelpCtx("netbeans.optionsDialog.editor.formatting");
    }

    public synchronized JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            this.panel = new FormattingPanel();
        }
        return this.panel;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void notifyChanged(boolean changed) {
        boolean fire;
        FormattingPanelController formattingPanelController = this;
        synchronized (formattingPanelController) {
            if (this.changed != changed) {
                this.changed = changed;
                fire = true;
            } else {
                fire = false;
            }
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", !changed, changed);
        }
        this.firePrefsChanged();
    }

    private static final class MimeLookupPreferencesFactory
    implements CustomizerSelector.PreferencesFactory,
    PreferenceChangeListener,
    NodeChangeListener {
        private final Map<String, ProxyPreferences> mimeTypePreferences = new HashMap<String, ProxyPreferences>();
        private final PreferenceChangeListener weakPrefL;
        private final NodeChangeListener weakNodeL;
        private final Callable callback;
        private final RequestProcessor PROCESSOR;

        public MimeLookupPreferencesFactory(Callable callback) {
            this.weakPrefL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)null);
            this.weakNodeL = (NodeChangeListener)WeakListeners.create(NodeChangeListener.class, (EventListener)this, (Object)null);
            this.PROCESSOR = new RequestProcessor(MimeLookupPreferencesFactory.class);
            this.callback = callback;
        }

        public Set<? extends String> getAccessedMimeTypes() {
            return this.mimeTypePreferences.keySet();
        }

        public void applyChanges() {
            for (String mimeType : this.mimeTypePreferences.keySet()) {
                ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
                pp.silence();
                if (mimeType.length() > 0) {
                    if (!pp.getBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", false)) {
                        for (String key : BASIC_SETTINGS_NAMES) {
                            pp.remove(key);
                        }
                    }
                    pp.remove("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS");
                } else assert (pp.get("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", null) == null);
                try {
                    LOG.fine("    flushing pp for '" + mimeType + "'");
                    pp.flush();
                }
                catch (BackingStoreException ex) {
                    LOG.log(Level.WARNING, "Can't flush preferences for '" + mimeType + "'", ex);
                }
            }
        }

        public void destroy() {
            for (String mimeType : this.mimeTypePreferences.keySet()) {
                ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
                pp.removeNodeChangeListener(this.weakNodeL);
                pp.removePreferenceChangeListener(this.weakPrefL);
                pp.destroy();
                LOG.fine("destroying pp for '" + mimeType + "'");
            }
            this.mimeTypePreferences.clear();
        }

        @Override
        public Preferences getPreferences(final String mimeType) {
            ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
            try {
                if (pp != null && !pp.nodeExists("")) {
                    pp = null;
                }
            }
            catch (BackingStoreException bse) {
                // empty catch block
            }
            if (pp == null) {
                Preferences p = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                pp = ProxyPreferences.getProxyPreferences(this, p);
                if (mimeType.length() > 0) {
                    final ProxyPreferences finalPP = pp;
                    this.PROCESSOR.post(new Runnable(){

                        @Override
                        public void run() {
                            boolean overriden = MimeLookupPreferencesFactory.this.isKeyOverridenForMimeType("expand-tabs", mimeType) || MimeLookupPreferencesFactory.this.isKeyOverridenForMimeType("indent-shift-width", mimeType) || MimeLookupPreferencesFactory.this.isKeyOverridenForMimeType("spaces-per-tab", mimeType) || MimeLookupPreferencesFactory.this.isKeyOverridenForMimeType("tab-size", mimeType) || MimeLookupPreferencesFactory.this.isKeyOverridenForMimeType("text-limit-width", mimeType) || MimeLookupPreferencesFactory.this.isKeyOverridenForMimeType("text-line-wrap", mimeType);
                            finalPP.putBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", overriden);
                        }
                    }, 1000);
                }
                pp.addPreferenceChangeListener(this.weakPrefL);
                pp.addNodeChangeListener(this.weakNodeL);
                this.mimeTypePreferences.put(mimeType, pp);
                LOG.fine("getPreferences(" + mimeType + ")");
            }
            return pp;
        }

        @Override
        public boolean isKeyOverridenForMimeType(String key, String mimeType) {
            EditorSettingsStorage storage = EditorSettingsStorage.get((String)"Preferences");
            try {
                Map mimePathLocalPrefs = storage.load(MimePath.parse((String)mimeType), null, false);
                return mimePathLocalPrefs.containsKey(key);
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
                return false;
            }
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

        @Override
        public void childAdded(NodeChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

        @Override
        public void childRemoved(NodeChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

    }

}

