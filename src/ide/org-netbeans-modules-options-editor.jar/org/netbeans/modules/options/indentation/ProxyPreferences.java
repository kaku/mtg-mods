/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.settings.storage.api.MemoryPreferences
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.indentation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeEvent;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.modules.editor.settings.storage.api.MemoryPreferences;
import org.openide.util.WeakListeners;

public final class ProxyPreferences
extends Preferences
implements PreferenceChangeListener,
NodeChangeListener {
    private final MemoryPreferences delegateRoot;
    private Preferences delegate;
    private boolean noEvents;
    private final ProxyPreferences parent;
    private final Map<String, ProxyPreferences> children = new HashMap<String, ProxyPreferences>();
    private PreferenceChangeListener weakPrefListener;
    private final Set<PreferenceChangeListener> prefListeners = new HashSet<PreferenceChangeListener>();
    private NodeChangeListener weakNodeListener;
    private final Set<NodeChangeListener> nodeListeners = new HashSet<NodeChangeListener>();

    public static ProxyPreferences getProxyPreferences(Object token, Preferences delegate) {
        return new ProxyPreferences(null, MemoryPreferences.get((Object)token, (Preferences)delegate), null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void silence() {
        Preferences preferences = this.delegate;
        synchronized (preferences) {
            this.noEvents = true;
        }
    }

    public void destroy() {
        this.delegateRoot.destroy();
    }

    @Override
    public void put(String key, String value) {
        this.delegate.put(key, value);
    }

    @Override
    public String get(String key, String def) {
        return this.delegate.get(key, def);
    }

    @Override
    public void remove(String key) {
        this.delegate.remove(key);
    }

    @Override
    public void clear() throws BackingStoreException {
        this.delegate.clear();
    }

    @Override
    public void putInt(String key, int value) {
        this.delegate.putInt(key, value);
    }

    @Override
    public int getInt(String key, int def) {
        return this.delegate.getInt(key, def);
    }

    @Override
    public void putLong(String key, long value) {
        this.delegate.putLong(key, value);
    }

    @Override
    public long getLong(String key, long def) {
        return this.delegate.getLong(key, def);
    }

    @Override
    public void putBoolean(String key, boolean value) {
        this.delegate.putBoolean(key, value);
    }

    @Override
    public boolean getBoolean(String key, boolean def) {
        return this.delegate.getBoolean(key, def);
    }

    @Override
    public void putFloat(String key, float value) {
        this.delegate.putFloat(key, value);
    }

    @Override
    public float getFloat(String key, float def) {
        return this.delegate.getFloat(key, def);
    }

    @Override
    public void putDouble(String key, double value) {
        this.delegate.putDouble(key, value);
    }

    @Override
    public double getDouble(String key, double def) {
        return this.delegate.getDouble(key, def);
    }

    @Override
    public void putByteArray(String key, byte[] value) {
        this.delegate.putByteArray(key, value);
    }

    @Override
    public byte[] getByteArray(String key, byte[] def) {
        return this.delegate.getByteArray(key, def);
    }

    @Override
    public String[] keys() throws BackingStoreException {
        return this.delegate.keys();
    }

    @Override
    public String[] childrenNames() throws BackingStoreException {
        return this.delegate.childrenNames();
    }

    @Override
    public Preferences parent() {
        return this.parent;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Preferences node(String pathName) {
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            Preferences pref = this.children.get(pathName);
            if (pref != null) {
                return pref;
            }
        }
        Preferences pref = this.delegate.node(pathName);
        ProxyPreferences proxyPreferences2 = this;
        synchronized (proxyPreferences2) {
            ProxyPreferences result = this.children.get(pathName);
            if (result != null) {
                return result;
            }
            result = new ProxyPreferences(this, this.delegateRoot, pref);
            this.children.put(pathName, result);
            return result;
        }
    }

    @Override
    public boolean nodeExists(String pathName) throws BackingStoreException {
        return this.delegate.nodeExists(pathName);
    }

    @Override
    public void removeNode() throws BackingStoreException {
        this.delegate.removeNode();
        if (this.parent != null) {
            this.parent.nodeRemoved(this);
        }
    }

    @Override
    public String name() {
        return this.delegate.name();
    }

    @Override
    public String absolutePath() {
        return this.delegate.absolutePath();
    }

    @Override
    public boolean isUserNode() {
        return this.delegate.isUserNode();
    }

    @Override
    public String toString() {
        return this.delegate.toString();
    }

    @Override
    public void flush() throws BackingStoreException {
        this.delegate.flush();
    }

    @Override
    public void sync() throws BackingStoreException {
        this.delegate.sync();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addPreferenceChangeListener(PreferenceChangeListener pcl) {
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            this.prefListeners.add(pcl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removePreferenceChangeListener(PreferenceChangeListener pcl) {
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            this.prefListeners.remove(pcl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addNodeChangeListener(NodeChangeListener ncl) {
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            this.nodeListeners.add(ncl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeNodeChangeListener(NodeChangeListener ncl) {
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            this.nodeListeners.remove(ncl);
        }
    }

    @Override
    public void exportNode(OutputStream os) throws IOException, BackingStoreException {
        throw new UnsupportedOperationException("exportNode not supported");
    }

    @Override
    public void exportSubtree(OutputStream os) throws IOException, BackingStoreException {
        throw new UnsupportedOperationException("exportSubtree not supported");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        PreferenceChangeListener[] listeners;
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            if (this.noEvents) {
                return;
            }
            listeners = this.prefListeners.toArray(new PreferenceChangeListener[this.prefListeners.size()]);
        }
        PreferenceChangeEvent myEvt = null;
        for (PreferenceChangeListener l : listeners) {
            if (myEvt == null) {
                myEvt = new PreferenceChangeEvent(this, evt.getKey(), evt.getNewValue());
            }
            l.preferenceChange(myEvt);
        }
    }

    private synchronized void changeDelegate(Preferences del) {
        this.delegate = del;
    }

    private synchronized void nodeRemoved(ProxyPreferences child) {
        this.children.values().remove(child);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void childAdded(NodeChangeEvent evt) {
        NodeChangeListener[] listeners;
        Preferences childNode;
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            String childName = evt.getChild().name();
            childNode = this.children.get(childName);
            if (childNode != null) {
                ((ProxyPreferences)childNode).changeDelegate(evt.getChild());
            } else {
                childNode = this.node(evt.getChild().name());
            }
            if (this.noEvents) {
                return;
            }
            listeners = this.nodeListeners.toArray(new NodeChangeListener[this.nodeListeners.size()]);
        }
        NodeChangeEvent myEvt = null;
        for (NodeChangeListener l : listeners) {
            if (myEvt == null) {
                myEvt = new NodeChangeEvent(this, childNode);
            }
            l.childAdded(evt);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void childRemoved(NodeChangeEvent evt) {
        NodeChangeListener[] listeners;
        Preferences childNode;
        ProxyPreferences proxyPreferences = this;
        synchronized (proxyPreferences) {
            String childName = evt.getChild().name();
            childNode = this.children.get(childName);
            if (childNode == null) {
                return;
            }
            if (this.noEvents) {
                return;
            }
            listeners = this.nodeListeners.toArray(new NodeChangeListener[this.nodeListeners.size()]);
        }
        NodeChangeEvent myEvt = null;
        for (NodeChangeListener l : listeners) {
            if (myEvt == null) {
                myEvt = new NodeChangeEvent(this, childNode);
            }
            l.childAdded(evt);
        }
    }

    private ProxyPreferences(ProxyPreferences parent, MemoryPreferences memoryPref, Preferences delegate) {
        this.parent = parent;
        this.delegateRoot = memoryPref;
        this.delegate = delegate == null ? memoryPref.getPreferences() : delegate;
        this.weakPrefListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)delegate);
        this.delegate.addPreferenceChangeListener(this.weakPrefListener);
        this.weakNodeListener = (NodeChangeListener)WeakListeners.create(NodeChangeListener.class, (EventListener)this, (Object)delegate);
        this.delegate.addNodeChangeListener(this.weakNodeListener);
    }
}

