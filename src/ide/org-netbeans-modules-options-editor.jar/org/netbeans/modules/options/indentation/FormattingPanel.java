/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.indentation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.netbeans.modules.options.indentation.CustomizerSelector;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public final class FormattingPanel
extends JPanel
implements PropertyChangeListener {
    private String storedMimeType = null;
    private String storedCategory = null;
    private JComboBox categoryCombo;
    private JLabel categoryLabel;
    private JPanel categoryPanel;
    private JSplitPane jSplitPane1;
    private JComboBox languageCombo;
    private JLabel languageLabel;
    private JPanel optionsPanel;
    private JLabel previewLabel;
    private JPanel previewPanel;
    private JScrollPane previewScrollPane;
    private CustomizerSelector selector;
    private PropertyChangeListener weakListener;

    public FormattingPanel() {
        this.initComponents();
        this.languageCombo.setRenderer(new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof String) {
                    value = ((String)value).length() > 0 ? EditorSettings.getDefault().getLanguageName((String)value) : NbBundle.getMessage(FormattingPanel.class, (String)"LBL_AllLanguages");
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
        this.categoryCombo.setRenderer(new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof PreferencesCustomizer) {
                    value = ((PreferencesCustomizer)value).getDisplayName();
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
    }

    public void setSelector(CustomizerSelector selector) {
        if (selector == null) {
            this.storedMimeType = (String)this.languageCombo.getSelectedItem();
            Object o = this.categoryCombo.getSelectedItem();
            if (o instanceof PreferencesCustomizer) {
                this.storedCategory = ((PreferencesCustomizer)o).getId();
            }
        }
        if (this.selector != null) {
            this.selector.removePropertyChangeListener(this.weakListener);
        }
        this.selector = selector;
        if (this.selector != null) {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
            ArrayList<? extends String> mimeTypes = new ArrayList<String>();
            mimeTypes.addAll(selector.getMimeTypes());
            Collections.sort(mimeTypes, new LanguagesComparator());
            String preSelectMimeType = null;
            for (String mimeType : mimeTypes) {
                model.addElement(mimeType);
                if (!mimeType.equals(this.storedMimeType)) continue;
                preSelectMimeType = mimeType;
            }
            this.languageCombo.setModel(model);
            if (preSelectMimeType == null) {
                JTextComponent pane = EditorRegistry.lastFocusedComponent();
                preSelectMimeType = pane != null ? (String)pane.getDocument().getProperty("mimeType") : "";
            }
            this.languageCombo.setSelectedItem(preSelectMimeType);
            if (!preSelectMimeType.equals(this.languageCombo.getSelectedItem())) {
                this.languageCombo.setSelectedIndex(0);
            }
            this.weakListener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.selector);
            this.selector.addPropertyChangeListener(this.weakListener);
            this.propertyChange(new PropertyChangeEvent(this.selector, "CustomizerSelector.PROP_MIMETYPE", null, null));
        } else {
            this.languageCombo.setModel(new DefaultComboBoxModel());
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == null || "CustomizerSelector.PROP_MIMETYPE".equals(evt.getPropertyName())) {
            DefaultComboBoxModel<PreferencesCustomizer> model = new DefaultComboBoxModel<PreferencesCustomizer>();
            List<? extends PreferencesCustomizer> nue = this.selector.getCustomizers(this.selector.getSelectedMimeType());
            int preSelectIndex = 0;
            int idx = 0;
            for (PreferencesCustomizer c : nue) {
                model.addElement(c);
                if (c.getId().equals(this.storedCategory)) {
                    preSelectIndex = idx;
                }
                ++idx;
            }
            this.categoryCombo.setModel(model);
            this.categoryCombo.setSelectedIndex(preSelectIndex);
        }
        if (evt.getPropertyName() == null || "CustomizerSelector.PROP_CUSTOMIZER".equals(evt.getPropertyName())) {
            JComponent previewComponent;
            this.categoryPanel.setVisible(false);
            this.categoryPanel.removeAll();
            this.previewScrollPane.setVisible(false);
            PreferencesCustomizer c = this.selector.getSelectedCustomizer();
            if (c != null) {
                this.categoryPanel.add((Component)c.getComponent(), "Center");
            }
            this.categoryPanel.setVisible(true);
            if (c instanceof PreviewProvider) {
                previewComponent = ((PreviewProvider)((Object)c)).getPreviewComponent();
                previewComponent.setDoubleBuffered(true);
                if (previewComponent instanceof JTextComponent) {
                    Document doc = ((JTextComponent)previewComponent).getDocument();
                    doc.putProperty("Tools-Options->Editor->Formatting->Preview - Preferences", this.selector.getCustomizerPreferences(c));
                }
            } else {
                JLabel noPreviewLabel = new JLabel(NbBundle.getMessage(FormattingPanel.class, (String)"MSG_no_preview_available"));
                noPreviewLabel.setOpaque(true);
                noPreviewLabel.setHorizontalAlignment(0);
                noPreviewLabel.setBorder(new EmptyBorder(new Insets(11, 11, 11, 11)));
                noPreviewLabel.setVisible(true);
                previewComponent = new JPanel(new BorderLayout());
                previewComponent.add((Component)noPreviewLabel, "Center");
            }
            this.previewScrollPane.setViewportView(previewComponent);
            this.previewScrollPane.setVisible(true);
            this.previewLabel.setLabelFor(previewComponent);
            if (c instanceof PreviewProvider) {
                final PreviewProvider pp = (PreviewProvider)((Object)c);
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        pp.refreshPreview();
                    }
                });
            }
            this.jSplitPane1.resetToPreferredSizes();
        }
    }

    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this.previewPanel = new JPanel();
        this.previewLabel = new JLabel();
        this.previewScrollPane = new JScrollPane();
        this.optionsPanel = new JPanel();
        this.languageLabel = new JLabel();
        this.languageCombo = new JComboBox();
        this.categoryLabel = new JLabel();
        this.categoryCombo = new JComboBox();
        this.categoryPanel = new JPanel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setLayout(new GridBagLayout());
        this.jSplitPane1.setBorder(null);
        this.previewPanel.setMinimumSize(new Dimension(150, 100));
        this.previewPanel.setOpaque(false);
        this.previewPanel.setPreferredSize(new Dimension(150, 100));
        this.previewPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.previewLabel, (String)NbBundle.getMessage(FormattingPanel.class, (String)"LBL_Preview"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 17;
        this.previewPanel.add((Component)this.previewLabel, gridBagConstraints);
        this.previewLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FormattingPanel.class, (String)"AN_Preview"));
        this.previewLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingPanel.class, (String)"AD_Preview"));
        this.previewScrollPane.setDoubleBuffered(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.previewPanel.add((Component)this.previewScrollPane, gridBagConstraints);
        this.previewScrollPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FormattingPanel.class, (String)"AN_Preview"));
        this.previewScrollPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingPanel.class, (String)"AD_Preview"));
        this.jSplitPane1.setRightComponent(this.previewPanel);
        this.optionsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 8));
        this.optionsPanel.setOpaque(false);
        this.languageLabel.setLabelFor(this.languageCombo);
        Mnemonics.setLocalizedText((JLabel)this.languageLabel, (String)NbBundle.getMessage(FormattingPanel.class, (String)"LBL_Language"));
        this.languageCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.languageCombo.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FormattingPanel.this.languageChanged(evt);
            }
        });
        this.categoryLabel.setLabelFor(this.categoryCombo);
        Mnemonics.setLocalizedText((JLabel)this.categoryLabel, (String)NbBundle.getMessage(FormattingPanel.class, (String)"LBL_Category"));
        this.categoryCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.categoryCombo.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FormattingPanel.this.categoryChanged(evt);
            }
        });
        this.categoryPanel.setOpaque(false);
        this.categoryPanel.setLayout(new BorderLayout());
        GroupLayout optionsPanelLayout = new GroupLayout(this.optionsPanel);
        this.optionsPanel.setLayout(optionsPanelLayout);
        optionsPanelLayout.setHorizontalGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(optionsPanelLayout.createSequentialGroup().addGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.categoryPanel, GroupLayout.Alignment.LEADING, -1, -1, 32767).addGroup(GroupLayout.Alignment.LEADING, optionsPanelLayout.createSequentialGroup().addGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.categoryLabel).addComponent(this.languageLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.languageCombo, -2, -1, -2).addComponent(this.categoryCombo, -2, -1, -2)))).addContainerGap()));
        optionsPanelLayout.linkSize(0, this.categoryCombo, this.languageCombo);
        optionsPanelLayout.setVerticalGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(optionsPanelLayout.createSequentialGroup().addGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.languageLabel).addComponent(this.languageCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(optionsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.categoryLabel).addComponent(this.categoryCombo, -2, -1, -2)).addGap(18, 18, 18).addComponent(this.categoryPanel, -1, 246, 32767)));
        this.languageLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingPanel.class, (String)"AD_Language"));
        this.languageCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FormattingPanel.class, (String)"FormattingPanel.languageCombo.AccessibleContext.accessibleName"));
        this.languageCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingPanel.class, (String)"FormattingPanel.languageCombo.AccessibleContext.accessibleDescription"));
        this.categoryLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingPanel.class, (String)"AD_Category"));
        this.categoryCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FormattingPanel.class, (String)"FormattingPanel.categoryCombo.AccessibleContext.accessibleName"));
        this.categoryCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingPanel.class, (String)"FormattingPanel.categoryCombo.AccessibleContext.accessibleDescription"));
        this.jSplitPane1.setLeftComponent(this.optionsPanel);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jSplitPane1, gridBagConstraints);
    }

    private void languageChanged(ActionEvent evt) {
        this.selector.setSelectedMimeType((String)this.languageCombo.getSelectedItem());
    }

    private void categoryChanged(ActionEvent evt) {
        PreferencesCustomizer selectedCustomizer = (PreferencesCustomizer)this.categoryCombo.getSelectedItem();
        if (selectedCustomizer != null) {
            this.selector.setSelectedCustomizer(selectedCustomizer.getId());
        }
    }

    private static final class LanguagesComparator
    implements Comparator<String> {
        private LanguagesComparator() {
        }

        @Override
        public int compare(String mimeType1, String mimeType2) {
            if (mimeType1.length() == 0) {
                return mimeType2.length() == 0 ? 0 : -1;
            }
            if (mimeType2.length() == 0) {
                return 1;
            }
            String langName1 = EditorSettings.getDefault().getLanguageName(mimeType1);
            String langName2 = EditorSettings.getDefault().getLanguageName(mimeType2);
            return langName1.compareTo(langName2);
        }
    }

}

