/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.indentation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.netbeans.modules.options.indentation.CustomizerSelector;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public class IndentationPanel
extends JPanel
implements ChangeListener,
ActionListener,
PreferenceChangeListener {
    private static final Logger LOG = Logger.getLogger(IndentationPanel.class.getName());
    private final MimePath mimePath;
    private final CustomizerSelector.PreferencesFactory prefsFactory;
    private final Preferences allLangPrefs;
    private final Preferences prefs;
    private final PreviewProvider preview;
    private final boolean showOverrideGlobalOptions;
    private static final int REFRESH_DELAY = 100;
    private static final RequestProcessor REFRESH_PROCESSOR = new RequestProcessor("Indent Preview Formatter");
    private final RequestProcessor.Task refreshTask;
    private JCheckBox cbExpandTabsToSpaces;
    private JCheckBox cbOverrideGlobalOptions;
    private JComboBox cboLineWrap;
    private JPanel jPanel1;
    private JLabel lLineWrap;
    private JLabel lNumberOfSpacesPerIndent;
    private JLabel lRightMargin;
    private JLabel lTabSize;
    private JSpinner sNumberOfSpacesPerIndent;
    private JSpinner sRightMargin;
    private JSpinner sTabSize;

    public IndentationPanel(MimePath mimePath, CustomizerSelector.PreferencesFactory prefsFactory, Preferences prefs, Preferences allLangPrefs, PreviewProvider preview) {
        this.refreshTask = REFRESH_PROCESSOR.create(new Runnable(){

            @Override
            public void run() {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(this);
                } else {
                    try {
                        IndentationPanel.this.preview.refreshPreview();
                    }
                    catch (ThreadDeath td) {
                        throw td;
                    }
                    catch (Throwable e) {
                        // empty catch block
                    }
                }
            }
        });
        this.mimePath = mimePath;
        this.prefsFactory = prefsFactory;
        this.prefs = prefs;
        this.allLangPrefs = allLangPrefs;
        if (this.allLangPrefs == null) {
            PreviewProvider pp2;
            PreviewProvider pp2;
            assert (preview == null);
            assert (mimePath == MimePath.EMPTY);
            try {
                pp2 = new TextPreview(prefs, "text/xml", this.getClass().getClassLoader(), "org/netbeans/modules/options/indentation/indentationExample");
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
                pp2 = new NoPreview();
            }
            this.preview = pp2;
            this.showOverrideGlobalOptions = false;
        } else {
            assert (preview != null);
            assert (mimePath != MimePath.EMPTY);
            this.preview = preview;
            this.showOverrideGlobalOptions = true;
            this.allLangPrefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.allLangPrefs));
        }
        this.initComponents();
        this.cbOverrideGlobalOptions.setVisible(this.showOverrideGlobalOptions);
        IndentationPanel.loc(this.cbOverrideGlobalOptions, "Override_Global_Options");
        IndentationPanel.loc(this.lNumberOfSpacesPerIndent, "Indent");
        IndentationPanel.loc(this.lTabSize, "TabSize");
        IndentationPanel.loc(this.cbExpandTabsToSpaces, "Expand_Tabs");
        IndentationPanel.loc(this.lRightMargin, "Right_Margin");
        IndentationPanel.loc(this.lLineWrap, "Line_Wrap");
        this.cbExpandTabsToSpaces.getAccessibleContext().setAccessibleName(IndentationPanel.loc("AN_Expand_Tabs"));
        this.cbExpandTabsToSpaces.getAccessibleContext().setAccessibleDescription(IndentationPanel.loc("AD_Expand_Tabs"));
        this.sNumberOfSpacesPerIndent.setModel(new SpinnerNumberModel(4, 0, 50, 1));
        this.sTabSize.setModel(new SpinnerNumberModel(4, 1, 50, 1));
        this.sRightMargin.setModel(new SpinnerNumberModel(120, 0, 200, 10));
        this.cboLineWrap.setRenderer(new LineWrapRenderer(this.cboLineWrap.getRenderer()));
        this.cboLineWrap.setModel(new DefaultComboBoxModel<Object>(new Object[]{"none", "words", "chars"}));
        if (this.showOverrideGlobalOptions && null == this.prefs.get("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", null)) {
            this.prefs.putBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", this.areBasicOptionsOverriden());
        }
        this.prefsChange(null);
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)prefs));
        this.cbOverrideGlobalOptions.addActionListener(this);
        this.cbExpandTabsToSpaces.addActionListener(this);
        this.sNumberOfSpacesPerIndent.addChangeListener(this);
        this.sTabSize.addChangeListener(this);
        this.sRightMargin.addChangeListener(this);
        this.cboLineWrap.addActionListener(this);
    }

    public PreviewProvider getPreviewProvider() {
        return this.preview;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.sNumberOfSpacesPerIndent == e.getSource()) {
            this.prefs.putInt("indent-shift-width", (Integer)this.sNumberOfSpacesPerIndent.getValue());
            this.prefs.putInt("spaces-per-tab", (Integer)this.sNumberOfSpacesPerIndent.getValue());
        } else if (this.sTabSize == e.getSource()) {
            this.prefs.putInt("tab-size", (Integer)this.sTabSize.getValue());
        } else if (this.sRightMargin == e.getSource()) {
            this.prefs.putInt("text-limit-width", (Integer)this.sRightMargin.getValue());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.cbOverrideGlobalOptions == e.getSource()) {
            this.prefs.putBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", !this.cbOverrideGlobalOptions.isSelected());
        } else if (this.cbExpandTabsToSpaces == e.getSource()) {
            this.prefs.putBoolean("expand-tabs", this.cbExpandTabsToSpaces.isSelected());
        } else if (this.cboLineWrap == e.getSource()) {
            this.prefs.put("text-line-wrap", (String)this.cboLineWrap.getSelectedItem());
        }
    }

    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        if (evt.getSource() == this.prefs) {
            this.prefsChange(evt);
        } else if (evt.getSource() == this.allLangPrefs) {
            this.allLangPrefsChange(evt);
        } else assert (false);
    }

    private void prefsChange(PreferenceChangeEvent evt) {
        int nue;
        String key = evt == null ? null : evt.getKey();
        boolean needsRefresh = false;
        if (key == null || "expand-tabs".equals(key)) {
            boolean value = this.prefs.getBoolean("expand-tabs", this.getDefBoolean("expand-tabs", true));
            if (value != this.cbExpandTabsToSpaces.isSelected()) {
                this.cbExpandTabsToSpaces.setSelected(value);
            }
            needsRefresh = true;
        }
        if (key == null || "indent-shift-width".equals(key)) {
            nue = this.prefs.getInt("indent-shift-width", this.getDefInt("indent-shift-width", 4));
            if (nue != (Integer)this.sNumberOfSpacesPerIndent.getValue()) {
                this.sNumberOfSpacesPerIndent.setValue(nue);
            }
            needsRefresh = true;
        }
        if ((key == null || "spaces-per-tab".equals(key)) && this.prefs.get("indent-shift-width", null) == null) {
            nue = this.prefs.getInt("spaces-per-tab", this.getDefInt("spaces-per-tab", 4));
            if (nue != (Integer)this.sNumberOfSpacesPerIndent.getValue()) {
                this.sNumberOfSpacesPerIndent.setValue(nue);
            }
            needsRefresh = true;
        }
        if (key == null || "tab-size".equals(key)) {
            nue = this.prefs.getInt("tab-size", this.getDefInt("tab-size", 8));
            if (nue != (Integer)this.sTabSize.getValue()) {
                this.sTabSize.setValue(nue);
            }
            needsRefresh = true;
        }
        if (key == null || "text-limit-width".equals(key)) {
            nue = this.prefs.getInt("text-limit-width", this.getDefInt("text-limit-width", 80));
            if (nue != (Integer)this.sRightMargin.getValue()) {
                this.sRightMargin.setValue(nue);
            }
            needsRefresh = true;
        }
        if (key == null || "text-line-wrap".equals(key)) {
            String nue2 = this.prefs.get("text-line-wrap", this.getDef("text-line-wrap", "none"));
            if (nue2 != this.cboLineWrap.getSelectedItem()) {
                this.cboLineWrap.setSelectedItem(nue2);
            }
            needsRefresh = true;
        }
        if (this.showOverrideGlobalOptions && (key == null || "FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS".equals(key))) {
            nue = (int)this.prefs.getBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", this.areBasicOptionsOverriden()) ? 1 : 0;
            if (nue == this.cbOverrideGlobalOptions.isSelected()) {
                this.cbOverrideGlobalOptions.setSelected(nue == 0);
            }
            if (!nue) {
                this.prefs.putBoolean("expand-tabs", this.allLangPrefs.getBoolean("expand-tabs", true));
                this.prefs.putInt("indent-shift-width", this.allLangPrefs.getInt("indent-shift-width", 4));
                this.prefs.putInt("spaces-per-tab", this.allLangPrefs.getInt("spaces-per-tab", 4));
                this.prefs.putInt("tab-size", this.allLangPrefs.getInt("tab-size", 4));
                this.prefs.putInt("text-limit-width", this.allLangPrefs.getInt("text-limit-width", 80));
                this.prefs.put("text-line-wrap", this.allLangPrefs.get("text-line-wrap", "none"));
            }
            needsRefresh = true;
            ((ControlledCheckBox)this.cbExpandTabsToSpaces).setEnabledInternal((boolean)nue);
            ((ControlledLabel)this.lNumberOfSpacesPerIndent).setEnabledInternal((boolean)nue);
            ((ControlledSpinner)this.sNumberOfSpacesPerIndent).setEnabledInternal((boolean)nue);
            ((ControlledLabel)this.lTabSize).setEnabledInternal((boolean)nue);
            ((ControlledSpinner)this.sTabSize).setEnabledInternal((boolean)nue);
            ((ControlledLabel)this.lRightMargin).setEnabledInternal((boolean)nue);
            ((ControlledSpinner)this.sRightMargin).setEnabledInternal((boolean)nue);
            ((ControlledLabel)this.lLineWrap).setEnabledInternal((boolean)nue);
            ((ControlledComboBox)this.cboLineWrap).setEnabledInternal((boolean)nue);
        }
        if (needsRefresh) {
            this.scheduleRefresh();
        }
    }

    void scheduleRefresh() {
        this.refreshTask.schedule(100);
    }

    private void allLangPrefsChange(PreferenceChangeEvent evt) {
        String key;
        String string = key = evt == null ? null : evt.getKey();
        if (this.prefs.getBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", this.areBasicOptionsOverriden())) {
            return;
        }
        if (key == null || "expand-tabs".equals(key)) {
            this.prefs.putBoolean("expand-tabs", this.allLangPrefs.getBoolean("expand-tabs", true));
        }
        if (key == null || "indent-shift-width".equals(key)) {
            this.prefs.putInt("indent-shift-width", this.allLangPrefs.getInt("indent-shift-width", 4));
        }
        if (key == null || "spaces-per-tab".equals(key)) {
            this.prefs.putInt("spaces-per-tab", this.allLangPrefs.getInt("spaces-per-tab", 4));
        }
        if (key == null || "tab-size".equals(key)) {
            this.prefs.putInt("tab-size", this.allLangPrefs.getInt("tab-size", 4));
        }
        if (key == null || "text-limit-width".equals(key)) {
            this.prefs.putInt("text-limit-width", this.allLangPrefs.getInt("text-limit-width", 80));
        }
        if (key == null || "text-line-wrap".equals(key)) {
            this.prefs.put("text-line-wrap", this.allLangPrefs.get("text-line-wrap", "none"));
        }
    }

    private void initComponents() {
        this.cbOverrideGlobalOptions = new JCheckBox();
        this.jPanel1 = new JPanel();
        this.cbExpandTabsToSpaces = new ControlledCheckBox();
        this.lNumberOfSpacesPerIndent = new ControlledLabel();
        this.sNumberOfSpacesPerIndent = new ControlledSpinner();
        this.lTabSize = new ControlledLabel();
        this.sTabSize = new ControlledSpinner();
        this.lRightMargin = new ControlledLabel();
        this.sRightMargin = new ControlledSpinner();
        this.lLineWrap = new ControlledLabel();
        this.cboLineWrap = new ControlledComboBox();
        this.setOpaque(false);
        Mnemonics.setLocalizedText((AbstractButton)this.cbOverrideGlobalOptions, (String)NbBundle.getMessage(IndentationPanel.class, (String)"CTL_Override_Global_Options"));
        Mnemonics.setLocalizedText((AbstractButton)this.cbExpandTabsToSpaces, (String)NbBundle.getMessage(IndentationPanel.class, (String)"CTL_Expand_Tabs"));
        this.lNumberOfSpacesPerIndent.setLabelFor(this.sNumberOfSpacesPerIndent);
        Mnemonics.setLocalizedText((JLabel)this.lNumberOfSpacesPerIndent, (String)NbBundle.getMessage(IndentationPanel.class, (String)"CTL_Indent"));
        this.lTabSize.setLabelFor(this.sTabSize);
        Mnemonics.setLocalizedText((JLabel)this.lTabSize, (String)NbBundle.getMessage(IndentationPanel.class, (String)"CTL_TabSize"));
        this.lRightMargin.setLabelFor(this.sRightMargin);
        Mnemonics.setLocalizedText((JLabel)this.lRightMargin, (String)NbBundle.getMessage(IndentationPanel.class, (String)"CTL_Right_Margin"));
        this.lLineWrap.setLabelFor(this.cboLineWrap);
        Mnemonics.setLocalizedText((JLabel)this.lLineWrap, (String)NbBundle.getMessage(IndentationPanel.class, (String)"CTL_Line_Wrap"));
        this.cboLineWrap.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addComponent(this.lNumberOfSpacesPerIndent, -2, 207, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.sNumberOfSpacesPerIndent, -2, 53, -2)).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addComponent(this.lTabSize, -2, 207, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.sTabSize, -2, 54, -2)).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addComponent(this.lRightMargin, -2, 207, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.sRightMargin, -2, 62, -2)).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addComponent(this.lLineWrap, -2, 117, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cboLineWrap, -2, 152, -2))).addContainerGap()).addGroup(jPanel1Layout.createSequentialGroup().addGap(20, 20, 20).addComponent(this.cbExpandTabsToSpaces, -1, -1, 32767).addGap(54, 54, 54)));
        jPanel1Layout.linkSize(0, this.sNumberOfSpacesPerIndent, this.sRightMargin, this.sTabSize);
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.cbExpandTabsToSpaces, -2, 19, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.sNumberOfSpacesPerIndent, -2, -1, -2).addComponent(this.lNumberOfSpacesPerIndent)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.sTabSize, -2, -1, -2).addComponent(this.lTabSize)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.sRightMargin, -2, -1, -2).addComponent(this.lRightMargin)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lLineWrap).addComponent(this.cboLineWrap, -2, -1, -2)).addContainerGap(-1, 32767)));
        jPanel1Layout.linkSize(1, this.sNumberOfSpacesPerIndent, this.sRightMargin, this.sTabSize);
        this.cbExpandTabsToSpaces.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_Expand_Tabs"));
        this.lNumberOfSpacesPerIndent.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_Indent"));
        this.lNumberOfSpacesPerIndent.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_Indent"));
        this.sNumberOfSpacesPerIndent.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_sNumberOfSpacesPerIndent"));
        this.sNumberOfSpacesPerIndent.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_sNumberOfSpacesPerIndent"));
        this.lTabSize.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_TabSize"));
        this.lTabSize.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_TabSize"));
        this.sTabSize.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_sTabSize"));
        this.sTabSize.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_sTabSize"));
        this.lRightMargin.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_Right_Margin"));
        this.lRightMargin.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_Right_Margin"));
        this.sRightMargin.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_sRightMargin"));
        this.sRightMargin.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_sRightMargin"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.cbOverrideGlobalOptions, -1, -1, 32767).addGap(66, 66, 66)).addComponent(this.jPanel1, -1, -1, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.cbOverrideGlobalOptions).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jPanel1, -1, -1, 32767)));
        this.cbOverrideGlobalOptions.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_Override_Global_Options"));
        this.cbOverrideGlobalOptions.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_Override_Global_Options"));
    }

    private static String loc(String key) {
        return NbBundle.getMessage(IndentationPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (!(c instanceof JLabel)) {
            c.getAccessibleContext().setAccessibleName(IndentationPanel.loc("AN_" + key));
            c.getAccessibleContext().setAccessibleDescription(IndentationPanel.loc("AD_" + key));
        }
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)IndentationPanel.loc("CTL_" + key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)IndentationPanel.loc("CTL_" + key));
        }
    }

    private boolean areBasicOptionsOverriden() {
        String mimeType = this.mimePath.getPath();
        return this.prefsFactory.isKeyOverridenForMimeType("expand-tabs", mimeType) || this.prefsFactory.isKeyOverridenForMimeType("indent-shift-width", mimeType) || this.prefsFactory.isKeyOverridenForMimeType("spaces-per-tab", mimeType) || this.prefsFactory.isKeyOverridenForMimeType("tab-size", mimeType) || this.prefsFactory.isKeyOverridenForMimeType("text-limit-width", mimeType) || this.prefsFactory.isKeyOverridenForMimeType("text-line-wrap", mimeType);
    }

    private boolean getDefBoolean(String key, boolean def) {
        return this.allLangPrefs != null ? this.allLangPrefs.getBoolean(key, def) : def;
    }

    private int getDefInt(String key, int def) {
        return this.allLangPrefs != null ? this.allLangPrefs.getInt(key, def) : def;
    }

    private String getDef(String key, String def) {
        return this.allLangPrefs != null ? this.allLangPrefs.get(key, def) : def;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    private static final class LineWrapRenderer
    implements ListCellRenderer {
        private final ListCellRenderer defaultRenderer;

        public LineWrapRenderer(ListCellRenderer defaultRenderer) {
            this.defaultRenderer = defaultRenderer;
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            return this.defaultRenderer.getListCellRendererComponent(list, NbBundle.getMessage(IndentationPanel.class, (String)("LWV_" + value)), index, isSelected, cellHasFocus);
        }
    }

    private static final class ControlledComboBox
    extends JComboBox {
        private boolean externallyEnabled = true;
        private boolean internallyEnabled = true;

        private ControlledComboBox() {
        }

        @Override
        public void setEnabled(boolean b) {
            if (this.externallyEnabled == b) {
                return;
            }
            this.externallyEnabled = b;
            if (this.externallyEnabled) {
                if (this.internallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }

        public void setEnabledInternal(boolean b) {
            if (this.internallyEnabled == b) {
                return;
            }
            this.internallyEnabled = b;
            if (this.internallyEnabled) {
                if (this.externallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }
    }

    private static final class ControlledSpinner
    extends JSpinner {
        private boolean externallyEnabled = true;
        private boolean internallyEnabled = true;

        private ControlledSpinner() {
        }

        @Override
        public void setEnabled(boolean b) {
            if (this.externallyEnabled == b) {
                return;
            }
            this.externallyEnabled = b;
            if (this.externallyEnabled) {
                if (this.internallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }

        public void setEnabledInternal(boolean b) {
            if (this.internallyEnabled == b) {
                return;
            }
            this.internallyEnabled = b;
            if (this.internallyEnabled) {
                if (this.externallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }
    }

    private static final class ControlledLabel
    extends JLabel {
        private boolean externallyEnabled = true;
        private boolean internallyEnabled = true;

        private ControlledLabel() {
        }

        @Override
        public void setEnabled(boolean b) {
            if (this.externallyEnabled == b) {
                return;
            }
            this.externallyEnabled = b;
            if (this.externallyEnabled) {
                if (this.internallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }

        public void setEnabledInternal(boolean b) {
            if (this.internallyEnabled == b) {
                return;
            }
            this.internallyEnabled = b;
            if (this.internallyEnabled) {
                if (this.externallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }
    }

    private static final class ControlledCheckBox
    extends JCheckBox {
        private boolean externallyEnabled = true;
        private boolean internallyEnabled = true;

        private ControlledCheckBox() {
        }

        @Override
        public void setEnabled(boolean b) {
            if (this.externallyEnabled == b) {
                return;
            }
            this.externallyEnabled = b;
            if (this.externallyEnabled) {
                if (this.internallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }

        public void setEnabledInternal(boolean b) {
            if (this.internallyEnabled == b) {
                return;
            }
            this.internallyEnabled = b;
            if (this.internallyEnabled) {
                if (this.externallyEnabled) {
                    super.setEnabled(true);
                }
            } else {
                super.setEnabled(false);
            }
        }
    }

    public static final class NoPreview
    implements PreviewProvider {
        private JComponent component = null;

        @Override
        public JComponent getPreviewComponent() {
            if (this.component == null) {
                JLabel noPreviewLabel = new JLabel(NbBundle.getMessage(IndentationPanel.class, (String)"MSG_no_preview_available"));
                noPreviewLabel.setOpaque(true);
                noPreviewLabel.setHorizontalAlignment(0);
                noPreviewLabel.setBorder(new EmptyBorder(new Insets(11, 11, 11, 11)));
                noPreviewLabel.setVisible(true);
                this.component = new JPanel(new BorderLayout());
                this.component.add((Component)noPreviewLabel, "Center");
            }
            return this.component;
        }

        @Override
        public void refreshPreview() {
        }
    }

    public static final class TextPreview
    implements PreviewProvider {
        private final Preferences prefs;
        private final String mimeType;
        private final String previewText;
        private JEditorPane jep;

        public TextPreview(Preferences prefs, FileObject previewFile) throws IOException {
            this(prefs, previewFile.getMIMEType(), TextPreview.loadPreviewText(previewFile.getInputStream()));
        }

        public TextPreview(Preferences prefs, String mimeType, FileObject previewFile) throws IOException {
            this(prefs, mimeType, TextPreview.loadPreviewText(previewFile.getInputStream()));
        }

        public TextPreview(Preferences prefs, String mimeType, String previewText) {
            this.prefs = prefs;
            this.mimeType = mimeType;
            this.previewText = previewText;
        }

        public TextPreview(Preferences prefs, String mimeType, ClassLoader loader, String resourceName) throws IOException {
            this(prefs, mimeType, TextPreview.loadPreviewText(loader.getResourceAsStream(resourceName)));
        }

        public TextPreview(Preferences prefs, String mimeType, Class clazz, String bundleKey) {
            this(prefs, mimeType, NbBundle.getMessage((Class)clazz, (String)bundleKey));
        }

        @Override
        public JComponent getPreviewComponent() {
            if (this.jep == null) {
                this.jep = new JEditorPane();
                this.jep.getAccessibleContext().setAccessibleName(NbBundle.getMessage(IndentationPanel.class, (String)"AN_Preview"));
                this.jep.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(IndentationPanel.class, (String)"AD_Preview"));
                this.jep.putClientProperty("HighlightsLayerIncludes", "^org\\.netbeans\\.modules\\.editor\\.lib2\\.highlighting\\.SyntaxHighlighting$");
                this.jep.setEditorKit(CloneableEditorSupport.getEditorKit((String)this.mimeType));
                this.jep.setEditable(false);
            }
            return this.jep;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void refreshPreview() {
            JEditorPane pane = (JEditorPane)this.getPreviewComponent();
            pane.getDocument().putProperty("text-line-wrap", "");
            pane.getDocument().putProperty("tab-size", "");
            pane.getDocument().putProperty("indent-shift-width", this.prefs.getInt("indent-shift-width", 4));
            pane.getDocument().putProperty("spaces-per-tab", this.prefs.getInt("spaces-per-tab", 4));
            pane.getDocument().putProperty("text-limit-width", "");
            pane.setText(this.previewText);
            final Document doc = pane.getDocument();
            if (doc instanceof BaseDocument) {
                final Reformat reformat = Reformat.get((Document)doc);
                reformat.lock();
                try {
                    ((BaseDocument)doc).runAtomic(new Runnable(){

                        @Override
                        public void run() {
                            if (LOG.isLoggable(Level.FINE)) {
                                LOG.fine("Refreshing preview: expandTabs=" + IndentUtils.isExpandTabs((Document)doc) + ", indentLevelSize=" + IndentUtils.indentLevelSize((Document)doc) + ", tabSize=" + IndentUtils.tabSize((Document)doc) + ", mimeType='" + doc.getProperty("mimeType") + "'" + ", doc=" + IndentationPanel.s2s(doc));
                            }
                            try {
                                reformat.reformat(0, doc.getLength());
                            }
                            catch (BadLocationException ble) {
                                LOG.log(Level.WARNING, null, ble);
                            }
                        }
                    });
                }
                finally {
                    reformat.unlock();
                }
            }
            LOG.warning("Can't format " + doc + "; it's not BaseDocument.");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private static String loadPreviewText(InputStream is) throws IOException {
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            try {
                StringBuilder sb = new StringBuilder();
                String line = r.readLine();
                while (line != null) {
                    sb.append(line).append('\n');
                    line = r.readLine();
                }
                line = sb.toString();
                return line;
            }
            finally {
                r.close();
            }
        }

    }

}

