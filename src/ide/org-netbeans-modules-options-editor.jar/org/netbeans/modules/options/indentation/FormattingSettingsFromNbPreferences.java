/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.spi.StorageFilter
 *  org.netbeans.modules.editor.settings.storage.spi.TypedValue
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package org.netbeans.modules.options.indentation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.spi.StorageFilter;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public final class FormattingSettingsFromNbPreferences
extends StorageFilter<String, TypedValue> {
    private static final Logger LOG = Logger.getLogger(FormattingSettingsFromNbPreferences.class.getName());
    private static final Map<String, String> affectedMimeTypes = new HashMap<String, String>();

    public FormattingSettingsFromNbPreferences() {
        super("Preferences");
    }

    public void afterLoad(Map<String, TypedValue> map, MimePath mimePath, String profile, boolean defaults) throws IOException {
        if (defaults || mimePath.size() != 1 || !affectedMimeTypes.containsKey(mimePath.getPath())) {
            return;
        }
        try {
            Preferences nbprefs = this.getNbPreferences(mimePath.getPath());
            if (nbprefs != null && nbprefs.nodeExists("CodeStyle/default")) {
                Preferences codestyle = nbprefs.node("CodeStyle/default");
                for (String key : codestyle.keys()) {
                    TypedValue typedValue;
                    if (map.containsKey(key) || (typedValue = this.guessTypedValue(codestyle.get(key, null))) == null) continue;
                    map.put(key, typedValue);
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.fine("Injecting '" + key + "' = '" + typedValue.getValue() + "' (" + typedValue.getJavaType() + ") for '" + mimePath.getPath() + "'");
                }
            }
        }
        catch (BackingStoreException bse) {
            LOG.log(Level.FINE, null, bse);
        }
    }

    public void beforeSave(Map<String, TypedValue> map, MimePath mimePath, String profile, boolean defaults) throws IOException {
        if (defaults || mimePath.size() != 1 || !affectedMimeTypes.containsKey(mimePath.getPath())) {
            return;
        }
        try {
            Preferences nbprefs = this.getNbPreferences(mimePath.getPath());
            if (nbprefs != null && nbprefs.nodeExists("CodeStyle/default")) {
                nbprefs.node("CodeStyle").removeNode();
                nbprefs.flush();
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Cleaning up NbPreferences/CodeStyle node for '" + mimePath.getPath() + "'");
                }
            }
        }
        catch (BackingStoreException bse) {
            LOG.log(Level.FINE, null, bse);
        }
    }

    private Preferences getNbPreferences(String mimeType) {
        ClassLoader loader;
        Preferences prefs = null;
        String className = affectedMimeTypes.get(mimeType);
        if (className != null && (loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class)) != null) {
            try {
                Class clazz = loader.loadClass(className);
                prefs = NbPreferences.forModule(clazz);
            }
            catch (ClassNotFoundException ex) {
                LOG.log(Level.FINE, null, ex);
            }
        }
        return prefs;
    }

    private TypedValue guessTypedValue(String value) {
        if (value == null) {
            return null;
        }
        if (value.equalsIgnoreCase("true")) {
            return new TypedValue(Boolean.TRUE.toString(), Boolean.class.getName());
        }
        if (value.equalsIgnoreCase("false")) {
            return new TypedValue(Boolean.FALSE.toString(), Boolean.class.getName());
        }
        try {
            Integer i = Integer.parseInt(value);
            return new TypedValue(value, Integer.class.getName());
        }
        catch (NumberFormatException nfe) {
            try {
                Long l = Long.parseLong(value);
                return new TypedValue(value, Long.class.getName());
            }
            catch (NumberFormatException nfe) {
                try {
                    Float f = Float.valueOf(Float.parseFloat(value));
                    return new TypedValue(value, Float.class.getName());
                }
                catch (NumberFormatException nfe) {
                    try {
                        Double d = Double.parseDouble(value);
                        return new TypedValue(value, Double.class.getName());
                    }
                    catch (NumberFormatException nfe) {
                        return new TypedValue(value, String.class.getName());
                    }
                }
            }
        }
    }

    static {
        affectedMimeTypes.put("text/x-java", "org.netbeans.api.java.source.CodeStyle");
        affectedMimeTypes.put("text/x-ruby", "org.netbeans.modules.ruby.options.CodeStyle");
    }
}

